<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Jobs\Model\Job;
use Jobs\Model\JobEmployee;
use Jobs\Model\JobManager;
use Jobs\Model\JobProduct;
use Jobs\Model\JobSupervisor;
use Jobs\Model\JobTask;
use Jobs\Model\JobVehicle;
use Jobs\Model\JobSubTask;
use Jobs\Model\VehicleSubModel;
use Jobs\Model\JobTaskProduct;
use Jobs\Model\JobContractor;
use Jobs\Model\JobTaskContractor;
use Zend\View\Model\ViewModel;
use Jobs\Model\ServiceVehicle;
use Jobs\Model\ServiceEmployee;
use Jobs\Model\JobTaskEmployee;
use Jobs\Model\JobTaskEmployeeWorkTime;

class JobService extends BaseService {

	/**
	 * This function is used to save job
	 * @param JSON Data
	 */
	public function saveJob($data, $userLocation, $userId, $type = 'construction') 
	{
		$customerID = (!empty($data['customer'])) ? (int)$data['customer']: 0;
		$jobType = $data['jobType'];
		$jobCode = $data['jobCode'];
		$jobName = $data['jobName'];
		$jobEditflag = $data['jobEditflag'];
		$contractor = $data['contractor'];
		$projectID = $data['projectID'];
		$jobContractor = $data['contractor'];

		$dueDate = $data['dueDate'];
		$jobValue = $data['jobValue'];
		$jobEstimatedCost = $data['jobEstimatedCost'];
		$jobEstimatedEndDate = $data['jobEstimatedEndDate'];
		$jobSupervisors = $data['jobSupervisors'][$jobCode];
		$jobManagers = $data['jobManagers'][$jobCode];
		$jobTasks = $data['jobTasks'];
		$jobResources = $data['jobResources'][$jobCode];

		$vehicleRegNo = $data['vehicleRegNo'];
		$vehicleModel = (!empty($data['vehicleModel'])) ? (int)$data['vehicleModel'] : 0;
		$vehicleType = (!empty($data['vehicleType'])) ? (int)$data['vehicleType'] : 0;
		$vehicleSubModelID = (!empty($data['vehicleSubModelID'])) ? (int)$data['vehicleSubModelID'] : 0;
		$vehicleSubModelNewName = $data['vehicleSubModelNewName'];
		$fuelType = (!empty($data['fuelType'])) ? (int)$data['fuelType'] : 0;
		$enginNo = $data['enginNo'];
		$kmCovered = $data['kmCovered'];
		$selectedServices = $data['selectedServices'];
		$serviceVehicleID = ($data['serviceVehicleID'] == "") ? null : $data['serviceVehicleID'];
		$customerNotes = $data['customerNotes'];
		$supervisorId = $data['supervisorId'];
		$serviceEmployees = $data['serviceEmployees'];
		$additionalDetail1 = (!empty($data['additionalDetail1'])) ? $data['additionalDetail1'] : null;
		$additionalDetail2 = (!empty($data['additionalDetail2'])) ? $data['additionalDetail2']: null;


		if ($type == 'service') {
			$jobTasks = $this->processJobServices($selectedServices);
		}

		if (empty($jobCode)) {
			return $this->returnError('ERR_JOB_CODE_EMPTY', null);
		}

		//check whether job code alredy in database or not.
		$referenceService = $this->getService('ReferenceService');
		$reference = $referenceService->getReferenceNumber('22', $userLocation);
		$locationReferenceID = $reference['data']['locationReferenceID'];
		while ($jobCode) {
			$result = $this->getModel('JobTable')->getJobByCode($jobCode);
			if (count($result) > 0) {
				if ($locationReferenceID) {
					$newJobCode = $referenceService->getReferencePrefixNumber($locationReferenceID);
					if ($jobCode == $newJobCode) {
						$referenceService->updateReferenceNumber($locationReferenceID);
						$jobCode = $referenceService->getReferencePrefixNumber($locationReferenceID);
					} else {
						$jobCode = $newJobCode;
					}
				} else {
					return $this->returnError('ERR_JOB_CODE_EXIST', null);
				}
			} else {
				break;
			}
		}

		//get entityID from entity service
		$entityService = $this->getService('EntityService');
		$entityID = $entityService->createEntity($userId)['data']['entityID'];
		$statusName = 'Open';
		$statusID = $this->getStatusIDByStatusName($statusName);
		$jData = array(
			'jobName' => $jobName,
			'customerID' => $customerID,
			'jobTypeId' => $jobType,
			'jobEstimatedCost' => $jobEstimatedCost,
			'projectId' => $projectID,
			'contactorID' => null,
			'jobEstimatedEndDate' => $this->convertDateToStandardFormat($jobEstimatedEndDate),
			'jobDueDate' => $this->convertDateToStandardFormat($dueDate),
			'jobReferenceNumber' => $jobCode,
			'jobWeight' => $jobValue,
			'locationID' => $userLocation,
			'entityID' => $entityID,
			'jobStatus' => $statusID,
			'customerNotes' => $customerNotes,
			'supervisorId' => $supervisorId,
			'jobAdditionalDetail1' => $additionalDetail1,
			'jobAdditionalDetail2'	=> $additionalDetail2

		);

		$jobData = new Job();
		$jobData->exchangeArray($jData);
		$jobData->customerID = (is_null($jobData->customerID)) ? 0 : $jobData->customerID;
		$jobID = $this->getModel('JobTable')->saveJob($jobData);


		if ($jobID) {
			//if the job supervisor is not empty, than save job supervisor details
			if (!is_null($jobSupervisors)) {
				$saveJobSupervisor = $this->saveJobSupervisor($jobSupervisors, $jobID);
				if (!$saveJobSupervisor) {
					return $this->returnError('ERR_SAVE_JOB_SUPERVISOR', null);
				}
			}

			//if the job contractor is not empty, than save job contractor details
			if (!is_null($jobContractor)) {
				$saveJobContractor = $this->saveJobContractor($jobContractor, $jobID);
				if (!$saveJobContractor) {
					return $this->returnError('ERR_SAVE_JOB_CONTRACTOR', null);
				}
			}

			//if the job managers is not empty, than save job managers
			if (!is_null($jobManagers)) {
				$saveJobManager = $this->saveJobManager($jobManagers, $jobID);
				if (!$saveJobManager) {
					return $this->returnError('ERR_SAVE_JOB_MANAGER', null);
				}
			}

			//If the job tasks is not empty, than save job tasks.
			if (!is_null($jobTasks)) {
				$saveJobTasks = $this->saveJobTasks($jobTasks, $jobID);
				if (!$saveJobTasks['status']) {
					return $this->returnError($saveJobTasks['msg'], null);
				}
			}

			//If the resources is not empty, than save resources that are not related to tasks
			if (!is_null($jobResources)) {
				foreach ($jobResources as $key => $value) {
					$productKey = explode('_', $key);
					if (sizeof($productKey) == 2) {
						$saveProducts = $this->saveJobProducts($value, null, $jobID, $productKey[1], null);
						if (!$saveProducts) {
							return $this->returnError('ERR_SAVE_JOB_PRODUCTS', null);
						}
					} else if (sizeof($productKey) == 4) {
						foreach ($jobTasks as $jobTaskKey => $jobtaskValue) {
							$taskProductKey = $productKey[1] . '_' . $productKey[2];
							if ($jobTaskKey == $taskProductKey) {
								$jobTaskDetails = $this->getModel('JobTaskTable')->getJobTaskDetails($jobID, $jobtaskValue['taskID'], $jobtaskValue['taskCardID'])->current();
								$jobTaskID = $jobTaskDetails['jobTaskID'];
								$saveTaskProducts = $this->saveJobProducts($value, $jobTaskID, $jobID, $productKey[3], $jobTaskDetails['jobTaskTaskCardID']);
								if (!$saveTaskProducts) {
									return $this->returnError('ERR_SAVE_JOB_TASK_PRODUCT', null);
								}
							}
						}
					}
				}
			}


			if ($type == 'service') {
				if (is_null($serviceVehicleID)) {
					if ($vehicleSubModelID == "new") {
						$subModelData = array(
							'vehicleSubModelID' => null,
							'vehicleSubModelName' => $vehicleSubModelNewName,
							'vehicleModelID' => $vehicleModel,
						);
						$vehicleSubModelData = new VehicleSubModel();
						$vehicleSubModelData->exchangeArray($subModelData);

						$vehicleSubModelID = $this->getModel('VehicleSubModelTable')->saveVehicleSubModel($vehicleSubModelData);
						if(!$vehicleSubModelID) {
							return $this->returnError('ERR_VEH_TYP_SAVE', null);
						}
					}
					$vehicleData = array(
						'vehicleType' => $vehicleType,
						'fuelTypeID' => $fuelType,
						'customerID' => $customerID,
						'vehicleModel' => $vehicleModel,
						'vehicleSubModel' => $vehicleSubModelID,
						'serviceVehicleRegNo' => $vehicleRegNo,
						'serviceVehicleEngineNo' => $enginNo,
					);

					$serviceVehicleData = new ServiceVehicle();
					$serviceVehicleData->exchangeArray($vehicleData);


					$serviceVehicleSave = $this->getModel('ServiceVehicleTable')->saveVehicle($serviceVehicleData);

					if (!$serviceVehicleSave) {
						return $this->returnError('ERR_SAVE_VEHICLE', null);
					}

					$saveJobVehicle = $this->saveJobVehicleData($jobID, $kmCovered, $serviceVehicleSave);

					if (!$saveJobVehicle) {
						return $this->returnError('ERR_SAVE_JOB_VEHICLE', null);
					}

				
				} else {
					$saveJobVehicle = $this->saveJobVehicleData($jobID, $kmCovered, $serviceVehicleID);

					if (!$saveJobVehicle) {
						return $this->returnError('ERR_SAVE_JOB_VEHICLE', null);
					}
					$vehicleUpdateData = array(
						'vehicleType' => (!empty($data['vehicleType'])) ? floatval($data['vehicleType']) : null,
						'fuelTypeID' => (!empty($data['fuelType'])) ? floatval($data['fuelType']) : null,
						'vehicleModel' => (!empty($data['vehicleModel'])) ? floatval($data['vehicleModel']) : null,
						'vehicleSubModel' => (!empty($data['vehicleSubModelID'])) ? floatval($data['vehicleSubModelID']) : null,
						'serviceVehicleEngineNo' => (!empty($data['enginNo'])) ? $data['enginNo'] : null,
					);

					$serviceVehicleUpdate = $this->getModel('ServiceVehicleTable')->updateVehicle($serviceVehicleID, $vehicleUpdateData);
					if (!$serviceVehicleUpdate) {
						return $this->returnError('ERR_UPDATE_VEHICLE', null);
					}
				}
				
				if ($serviceEmployees != "") {
					foreach ($serviceEmployees as $value) {
						$empData = array(
							'jobID' => $jobID,
							'employeeID' => $value
						);
						$empSaveData = new ServiceEmployee();
						$empSaveData->exchangeArray($empData);


						$serviceEmployeeID = $this->getModel('ServiceEmployeeTable')->saveServiceEmployeeData($empSaveData);
						if (!$serviceEmployeeID) {
							return $this->returnError('ERR_SAVE_JOB_SERVICE_EMP', null);
						}
					}
				}

				$saveServiceProducts = $this->saveJobRelatedServiceProducts($jobTasks, $jobID, $vehicleType);
				if (!$saveServiceProducts) {
					return $this->returnError('ERR_SAVE_JOB_PRO', null);
				}
			}

			if ($locationReferenceID) {
				$referenceService->updateReferenceNumber($locationReferenceID);
			}

			if (!is_null($projectID)) {
				$result = $this->updateJobDetailsInProject($projectID);
				if (!$result) {
					return $this->returnError('ERR_UPDATE_PROJECT_IN_JOB', null);
				}
			}

			return $this->returnSuccess($jobCode, 'SUCC_SAVE_JOB');
		} else {
			return $this->returnError('ERR_SAVE_JOB', null);
		}
	}

	/**
	 * This function is used to update job details in project
	 * @param projectIDs
	 */
	public function updateJobDetailsInProject($projectID)
	{
		$jobDetails = $this->getModel('JobTable')->getJobDetailsByProjectID($projectID);
		$jobCount = count($jobDetails);


		$projectEstimatedCost = 0;
		foreach ($jobDetails as $key => $value) {
			$projectEstimatedCost += floatval($value['jobEstimatedCost']);
		}

		$data = array(
			'noOfJobs' => $jobCount,
			'projectEstimatedCost' => $projectEstimatedCost
		);

		$result = $this->getModel('ProjectTable')->updateProjectByArray($data, $projectID);
		return $result;
	}


	/**
	 * This function is used to save job supervisor
	 * @param $supervisorData
	 * @param $jobCode
	 */
	public function saveJobSupervisor($supervisorData, $jobID) {

		foreach ($supervisorData as $key => $value) {
			$data = array(
				'jobSupervisorID' => $value['jobSupervisorID'],
				'jobID' => $jobID,
			);

			$jobSupervisorData = new JobSupervisor();
			$jobSupervisorData->exchangeArray($data);

			$saveSupervisor = $this->getModel('JobSupervisorTable')->saveSupervisor($jobSupervisorData);
			if (!$saveSupervisor) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job contarctor
	 * @param $contractorData
	 * @param $jobID
	 */
	public function saveJobContractor($contractorData, $jobID) {

		foreach ($contractorData as $key => $value) {
			$data = array(
				'contractorID' => $value,
				'jobID' => $jobID,
			);

			$jobContractorData = new JobContractor();
			$jobContractorData->exchangeArray($data);

			$saveContractor = $this->getModel('JobContractorTable')->saveContractor($jobContractorData);
			if (!$saveContractor) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job mangers
	 * @param $managerData
	 * @param $jobID
	 */
	public function saveJobManager($jobManagers, $jobID) {

		foreach ($jobManagers as $key => $value) {
			$data = array(
				'employeeID' => $value['jobManagerID'],
				'jobID' => $jobID,
			);

			$jobManagerData = new JobManager();
			$jobManagerData->exchangeArray($data);

			$saveManager = $this->getModel('JobManagerTable')->saveManager($jobManagerData);
			if (!$saveManager) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job tasks
	 * @param $jobTasks
	 * @param $jobID
	 */
	public function saveJobTasks($jobTasks, $jobID, $updateFlag = false) {
		$statusName = 'Open';
		$statusID = $this->getStatusIDByStatusName($statusName);
		foreach ($jobTasks as $key => $value) {
			if ($updateFlag) {
				$taskCardID = ($value['taskCardID'] == "") ? null : $value['taskCardID'];
				$checkForUpdate = $this->getModel('JobTaskTable')->getJobTaskDetails($jobID, $value['taskID'], $taskCardID)->current();
				if (!$checkForUpdate) {
					$result = $this->saveJobTaskData($value, $jobID, $statusID);
				} else {
					$result['status'] = true;
				} 
			} else {
				$result = $this->saveJobTaskData($value, $jobID, $statusID);
			}
			
			if (!$result['status']) {
				return ['status' => false, 'msg' => $result['msg']];
			}
		}

		return ['status' => true];
	}


	public function saveJobTaskData($value, $jobID, $statusID) {
		$taskRate = (empty($value['ratePerUnit1'])) ? $value['ratePerUnit2'] : $value['ratePerUnit1'];
		$data = array(
			'jobID' => $jobID,
			'taskID' => $value['taskID'],
			'uomID' => $value['UomId'],
			'jobTaskUnitCost' => $value['costPerUnit'],
			'jobTaskUnitRate' => $taskRate,
			'jobTaskEstQty' => $value['estQty'],
			'jobTaskStatus' => $statusID,
			'jobTaskTaskCardID' => $value['taskCardID'],
		);

		$jobTaskData = new JobTask();
		$jobTaskData->exchangeArray($data);

		$jobTaskID = $this->getModel('JobTaskTable')->saveJobTask($jobTaskData);

		if ($jobTaskID) {
			//If task employee is not empty, than save the task employees
			if (!is_null($value['employees'])) {
				$taskEmployeeResult = $this->saveTaskEmployees($value['employees'], $jobTaskID, $jobID);

				if (!$taskEmployeeResult) {
					return ['status' => false, 'msg' => "ERR_SAVE_JOB_TASK_EMPLOYEE"];
				}
			}

			//If task contractor is not empty, than save the task contractors
			if (!is_null($value['contactor'])) {
				$taskTaskContractorResult = $this->saveTaskContractors($value['contactor'], $jobTaskID, $jobID);

				if (!$taskTaskContractorResult) {
					return ['status' => false, 'msg' => "ERR_SAVE_JOB_TASK_CONTRACTOR"];
				}
			}

			if (!is_null($value['selectedSubTasks'])) {

				$jobSubTaskResult = $this->saveJobSubTasks($value['selectedSubTasks'], $jobTaskID);
				if (!$jobSubTaskResult) {
					return ['status' => false, 'msg' => "ERR_SAVE_JOB_SUB_TASK"];
				}
			}
		
			return ['status' => true];
		} else {
			return ['status' => false, 'msg' => 'ERR_SAVE_JOB_TASK'];
		}

	}
	/**
	 * This function is used to save job task employees
	 * @param $employeeData
	 * @param $jobID
	 * @param $jobTaskID
	 */
	public function saveTaskEmployees($employeeData, $jobTaskID, $jobID) {
		foreach ($employeeData as $key => $value) {
			$data = array(
				'jobID' => $jobID,
				'employeeDesignationID' => $value['employeeID'],
				'jobTaskID' => $jobTaskID,
			);

			$jobTaskEmployeeData = new JobEmployee();
			$jobTaskEmployeeData->exchangeArray($data);

			$jobTaskEmployeeID = $this->getModel('JobEmployeeTable')->saveJobTaskEmployee($jobTaskEmployeeData);

			if (!$jobTaskEmployeeID) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job task contractor
	 * @param $contractorData
	 * @param $jobID
	 * @param $jobTaskID
	 */
	public function saveTaskContractors($contractorData, $jobTaskID, $jobID) {
		foreach ($contractorData as $key => $value) {
			$data = array(
				'contractorID' => $value['contractorID'],
				'jobTaskID' => $jobTaskID,
			);

			$jobTaskContractorData = new JobTaskContractor();
			$jobTaskContractorData->exchangeArray($data);

			$jobTaskContractorID = $this->getModel('JobTaskContractorTable')->saveJobTaskContractor($jobTaskContractorData);

			if (!$jobTaskContractorID) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job products
	 * @param $productData
	 * @param $jobID
	 * @param $jobTaskID
	 * @param $materialType
	 */
	public function saveJobProducts($productData, $jobTaskID = null, $jobID, $materialType, $taskCardID = null) {
		$materialTypeID = null;
		if ($materialType == "traG") {
			$materialTypeID = $this->getMaterialTypeID('Trading Items');
		} else if ($materialType == "fixA") {
			$materialTypeID = $this->getMaterialTypeID('Fixed Assets');
		} else if ($materialType == "raw") {
			$materialTypeID = $this->getMaterialTypeID('Raw Materials');
		}
		$pData = array(
			'jobID' => $jobID,
			'locationProductID' => $productData['jobMaterialTaskLocationProductID'],
			'jobProductUnitPrice' => $productData['jobMaterialProUnitPrice'],
			'uomID' => $productData['jobMaterialSelectedUomID'],
			'jobProductAllocatedQty' => $productData['jobMaterialProTotalQty'],
			'jobProductEstimatedQty' => $productData['jobMaterialProTotalQty'],
			'jobProductReOrderLevel' => $productData['jobMaterialReorderQty'],
			'jobProductMaterialTypeID' => $materialTypeID,
			'jobProductTaskCardID' => $taskCardID,
			'jobTaskID' => $jobTaskID,
		);

		$jobProductData = new JobProduct();
		$jobProductData->exchangeArray($pData);
		$jobProductID = $this->getModel('JobProductTable')->saveJobProduct($jobProductData);

		if (!$jobProductID) {
			return false;
		} else {
			return $jobProductID;
		}
	}

	/**
	 * This function is used to search projects for dropdown
	 */
	public function searchJobsForDropdown($data, $userLocation) {

		$searchKey = $data['searchKey'];
		$filterFlag = ($data['addFlag']) ? $data['addFlag']: null;
		$status = null;
		if ($filterFlag == "closed") {
			$status = 4;
			$filterFlag = null;
		} else {
			$filterFlag = $filterFlag;
		}

		$jobs = $this->getModel('JobTable')->jobSearchByKey($searchKey, $userLocation, null, null, $status, null, $filterFlag);
		$jobList = array();
		foreach ($jobs as $job) {
			$temp['value'] = $job['jobId'];
			$temp['text'] = $job['jobReferenceNumber'] . '-' . $job['jobName'];
			$jobList[] = $temp;
		}
		return $jobList;
	}

	/**
	 * This function is used to load jobs for dropdown
	 */
	public function loadJobsForDropdown($data) {



		$projectId = $data['projectId'];
		$progressFlag = ($data['progressFlag']) ? true: false;
		
		$jobs = $this->getModel('JobTable')->getJobByProjectId($projectId, $progressFlag);
		if ($jobs) {
			$jobList = array();
			foreach ($jobs as $job) {
				$temp['value'] = $job['jobId'];
				$temp['text'] = $job['jobReferenceNumber'] . '-' . $job['jobName'];
				$jobList[] = $temp;
			}
            return $this->returnSuccess(array('jobList' => $jobList), 'SUCC_LOAD_JOBS');
		} else {
			return $this->returnError('ERR_LOAD_JOBS', null);
		}
	}

	/**
	 * This function is used to get job by search key
	 */
	public function getJobBySearchKey($data, $param, $locationID) {
		$paginated = false;
		$jobSearchKey = ($data['jobSearchKey'] == "") ? null: $data['jobSearchKey'];
		$projectID = ($data['projectID'] == "") ? null : $data['projectID'];
		$jobSupervisorID = ($data['jobSupervisorID'] == "") ? null : $data['jobSupervisorID'];
		$jobStatusID = ($data['jobStatusID'] == "") ? null : $data['jobStatusID'];

		if ($jobSearchKey != null || $projectID != null || $jobSupervisorID != null || $jobStatusID != null) {
			$tbl = $this->getModel('JobTable');
			$jobList = $tbl->jobSearchByKey($jobSearchKey, null, $projectID, $jobSupervisorID, $jobStatusID);
		} else {
			$job = $this->getServiceLocator()->get("JobController");
			$jobList = $job->getPaginatedJob($locationID, $param);
		}

		$view = new ViewModel(array(
			'jobList' => $jobList,
			'paginated' => $paginated,
            'status' => $this->getStatusesList(),
		));

		$view->setTerminal(true);
		$view->setTemplate('jobs/job/list-table.phtml');

		return $view;
	}

	/**
	 * This function is used to get job supervisor details by job ID
	 * @param $jobID
	 */
	public function getJobSupervisorsByJobID($data)
	{
		$jobSupervisors = $this->getModel('JobSupervisorTable')->getJobSupervisorsDetailsByJobID($data['jobID']);
		return $jobSupervisors;
	}

	/**
	 * This function is used to delete job by job ID
	 * @param $jobID
	 */
	public function deleteJob($data, $userId, $projectRelatedJobDeletion = false)
	{
		$jobID = $data['jobID'];
		$jobData = (object) $this->getModel('JobTable')->getJobDetails($jobID)->current();
        if ($jobData->jobStatus == 3) {
        	$entityService = $this->getService('EntityService');
			$updatedData = $entityService->updateDeleteInfoEntity($jobData->entityID,$userId);
            $statusName = 'Cancel';
            $statusID = $this->getStatusIDByStatusName($statusName);
            $updateJobData = $this->getModel('JobTable')->updateJobstatusID($jobID, $statusID);

            $projectID = ($projectRelatedJobDeletion) ? $data['projectID'] : $jobData->projectId;
            if ($updatedData && $updateJobData) {
            	if (!is_null($jobData->projectId)) {
					$result = $this->updateJobDetailsInProject($projectID);
					if (!$result) {
						return $this->returnError('ERR_UPDATE_PROJECT_IN_JOB', null);
					}
				}
                return $this->returnSuccess(null, 'SUCC_JOB_DELETE');
            } else {
            	return $this->returnError('ERR_JOB_DELETE', null);
            }
        } else {
        	return $this->returnError('ERR_JOB_USED_DELETE', null);
        }
	}



	/**
	 * This function is used to delete job by job ID
	 * @param $jobID
	 */
	public function cancelJob($data, $userId)
	{

		$jobID = $data['jobID'];

		$returnDeliveryNote = ($data['returnDeliveryNote'] == 'true') ? true : false;

		$jobData = (object) $this->getModel('JobTable')->getJobDetails($jobID)->current();

        if ($jobData->jobStatus == 3) {
        	$entityService = $this->getService('EntityService');
			$updatedData = $entityService->updateDeleteInfoEntity($jobData->entityID,$userId);
            $statusName = 'Cancel';
            $statusID = $this->getStatusIDByStatusName($statusName);
            $updateJobData = $this->getModel('JobTable')->updateJobstatusID($jobID, $statusID);


            return $this->returnSuccess(null, 'SUCC_JOB_DELETE');
   
        } else {
        	$entityService = $this->getService('EntityService');
			$updatedData = $entityService->updateDeleteInfoEntity($jobData->entityID,$userId);
            $statusName = 'Cancel';
            $statusID = $this->getStatusIDByStatusName($statusName);
            $updateJobData = $this->getModel('JobTable')->updateJobstatusID($jobID, $statusID);


            if ($updateJobData) {
            	if ($returnDeliveryNote) {
	            	$getrelatedDeliveryNotes = $this->getModel('DeliveryNoteTable')->getDeliveryNoteDetailsByJobId($jobID);
	            	if (sizeof($getrelatedDeliveryNotes) > 0) {
				    	foreach ($getrelatedDeliveryNotes as $key => $value) {
				    		$dataSet = $this->processDeliveryNoteToReturn($value, $jobData);
				    		$res = $this->getController('ServiceJobsControllerAPI')->saveDeliveryNoteReturn($dataSet);
				    		if (!$res['status']) {
				    			return $this->returnError($res['msg'], null);
				    		}
				    	}
	            	}
            	}

            	$jobTaskData = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobID);

            	foreach ($jobTaskData as $key2 => $value2) {
            		$getJobTaskEmpData = $this->getModel('JobTaskEmployeeTable')->getJobTaskRelatedEmployeesByJobTaskID($value2['jobTaskID']);

            		if (sizeof($getJobTaskEmpData) > 0) {
	            		$deleteData = $this->getModel('JobTaskEmployeeTable')->deleteJobTaskEmployeeRecord($value2['jobTaskID']);

	            		if ($deleteData == 0) {
	            			return $this->returnError($res['msg'], null);
	            		}
            		}

            	}
            }

            return $this->returnSuccess(null, 'SUCC_JOB_DELETE');
        }
	}

	public function processDeliveryNoteToReturn($data, $jobData) {

		$processedDlnData = $this->processDeliveryNoteData($data['deliveryNoteID']);

		$products = [];
		$subProducts = [];

		$salesReturnTotal = 0;
		foreach ($processedDlnData['deliveryNoteProduct'] as $key => $value) {
			$temp = [
				'productID' => $value->productID,
				'deliveryNoteproductID' => $value->deliveryNoteProductID,
				'productCode' => $value->productCode,
				'productName' => $value->productName,
				'productPrice' => $value->deliveryNoteProductPrice,
				'productDiscount' => $value->deliveryNoteProductDiscount,
				'productDiscountType' => $value->deliveryNoteProductDiscountType,
				'productTotal' => floatval($value->deliveryNoteProductQuantity) * floatval($value->deliveryNoteProductPrice),
				'pTax' => $value->tax,
				'productType' => $value->productType,
				'availableQuantity' => ["qty" => $value->deliveryNoteProductQuantity],
				'returnQuantity' => ["qty" => $value->deliveryNoteProductQuantity],
			];
			$products[$value->productID] = $temp;

			if ($value->subProduct && sizeof($value->subProduct) > 0) {
				foreach ($value->subProduct as $key2 => $value2) {
					$temp1 = [
						'batchID' => $value2->productBatchID,
						'serialID' => $value2->productSerialID,
						'qtyByBase' => $value2->deliveryNoteSubProductQuantity
					];
					$subProducts[$value->productID] = $temp1;
				}
			}
			$salesReturnTotal += floatval($value->deliveryNoteProductQuantity) * floatval($value->deliveryNoteProductPrice);

		}
		$locationID = $this->user_session->userActiveLocation["locationID"];
		$result = $this->getReferenceNoForLocation('7', $locationID);
        $locationReferenceID = $result['locRefID'];

        $newReturnCode = $this->getReferenceNumber($locationReferenceID);
		$dataSet = [];

		$dataSet['returnCode'] = $newReturnCode;
		$dataSet['locationID'] = $locationID;
		$dataSet['products'] = $products;
		$dataSet['subProducts'] = $subProducts;
		$dataSet['returnTotalPrice'] = $salesReturnTotal;
		$dataSet['date'] = $currentDate = date('Y-m-d');
		$dataSet['customerID'] = $jobData->cusID;
		$dataSet['customerName'] = $jobData->customerName.'-'.$jobData->customerCode;
		$dataSet['returnComment'] = 'This is generated for the cancelled job ('.$jobData->jobReferenceNumber.').';
		$dataSet['deliveryNoteID'] = $data['deliveryNoteID'];
		$dataSet['customCurrencyId'] = $jobData->customerCurrency;
		$dataSet['multipleDlnCopyFlag'] = "false";
		$dataSet['crossLocationFlag'] = "false";
		$dataSet['ignoreBudgetLimit'] = "false";
		$dataSet['salesReturnCustomCurrencyRate'] = $data['deliveryNoteCustomCurrencyRate'];


		return $dataSet;

	}

	public function processDeliveryNoteData($dlNID) {
        if ($dlNID) {
            $deliveryNoteID = $dlNID;
            $dparray;
            $dparrayQty;
            $iparray;
            $iparrayQty;
            $checkProduct = 0;
            $dproductType;
            $locationID = $this->user_session->userActiveLocation['locationID'];


            // $deliveryNote = $this->getModel('DeliveryNoteTable')->getDeliveryNoteByID($deliveryNoteID);
            $deliveryNoteProducts = $this->getModel('DeliveryNoteProductTable')->getDeliveryNoteProductByDeliveryNoteID($deliveryNoteID, false);
            $tax = array(
                'deliveryNoteTaxID' => '',
                'deliveryNoteTaxName' => '',
                'deliveryNoteTaxPrecentage' => '',
                'deliveryNoteTaxAmount' => '',
                );
            $subProduct = array(
                'deliveryNoteSubProductID' => '',
                'productBatchID' => '',
                'productSerialID' => '',
                'deliveryNoteSubProductQuantity' => '',
                'deliveryNoteSubProductsWarranty'=>'',
                );

            $Products = array(
                'deliveryNoteProductID' => '',
                'deliveryNoteID' => '',
                'productID' => '',
                'deliveryNoteProductPrice' => '',
                'deliveryNoteProductDiscount' => '',
                'deliveryNoteProductDiscountType' => '',
                'deliveryNoteProductQuantity' => '',
                'productCode' => '',
                'productName' => '',
                'productType' => '',
                'tax' => '',
                'subProduct' => '',
                'documentTypeID' => '',
                'deliveryNoteProductDocumentID' => '',
                );

            $incID = 1;
            $incFlag = FALSE;
            foreach ($deliveryNoteProducts as $key => $tt) {
                $t = (object) $tt;
                $deliveryNoteProduct[$t->productID . '_' . $incID] = (object) $Products;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductID = $t->deliveryNoteProductID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteID = $t->deliveryNoteID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productID = $t->productID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductPrice = $t->deliveryNoteProductPrice;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductDiscount = $t->deliveryNoteProductDiscount;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductDiscountType = $t->deliveryNoteProductDiscountType;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductQuantity = $t->deliveryNoteProductQuantity;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productCode = $t->productCode;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productName = $t->productName;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productType = $t->productTypeID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productDescription = $t->productDescription;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->documentTypeID = $t->documentTypeID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductDocumentID = $t->deliveryNoteProductDocumentID;

                $subIncFlag = $incID - 1;
                if ($t->deliveryNoteSubProductID != '') {

                    $subIncFlag = $incID - 1;
                    if (!empty($deliveryNoteProduct[$t->productID . '_' . $subIncFlag]->subProduct) && $deliveryNoteProduct[$t->productID . '_' . $subIncFlag]->deliveryNoteProductID == $t->deliveryNoteProductID) {
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID] = (object) $subProduct;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->deliveryNoteSubProductID = $t->deliveryNoteSubProductID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->productBatchID = $t->productBatchID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->productSerialID = $t->productSerialID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->deliveryNoteSubProductQuantity = $t->deliveryProductSubQuantity;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->deliveryNoteSubProductsWarranty = $t->deliveryNoteSubProductsWarranty;
                        $deliveryNoteProduct[$t->productID . '_' . $subIncFlag]->subProduct = $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag];
                        unset($deliveryNoteProduct[$t->productID . '_' . $incID]);
                        --$incID;
                        $incFlag = TRUE;
                    } else {
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID] = (object) $subProduct;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->deliveryNoteSubProductID = $t->deliveryNoteSubProductID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->productBatchID = $t->productBatchID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->productSerialID = $t->productSerialID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->deliveryNoteSubProductQuantity = $t->deliveryProductSubQuantity;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->deliveryNoteSubProductsWarranty = $t->deliveryNoteSubProductsWarranty;
                        $deliveryNoteProduct[$t->productID . '_' . $incID]->subProduct = $deliveryNoteSubProduct[$t->productID . '_' . $incID];
                    }
                }

                if ($t->deliveryNoteTaxID) {
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID] = (object) $tax;
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxID = $t->deliveryNoteTaxID;
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxName = $t->taxName;
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxPrecentage = $t->deliveryNoteTaxPrecentage;
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxAmount = $t->deliveryNoteTaxAmount;

                    $subTaxIncFlag = $incID - 1;

                    if (!empty($deliveryNoteProduct[$t->productID . '_' . $subTaxIncFlag]->tax) && $deliveryNoteProduct[$t->productID . '_' . $subTaxIncFlag]->deliveryNoteProductID == $t->deliveryNoteProductID) {
                        $deliveryNoteProduct[$t->productID . '_' . $subTaxIncFlag]->tax = $taxes[$t->deliveryNoteProductID];
                        unset($deliveryNoteProduct[$t->productID . '_' . $incID]);
                                //If there has sub products so dont need to reduce $incID
                                //because if it has a sub product then it already has reduce 1 that previous condition
                        if (!$incFlag) {
                            --$incID;
                        }
                    } else {

                        $deliveryNoteProduct[$t->productID . '_' . $incID]->tax = $taxes[$t->deliveryNoteProductID];
                    }
                }
                $locationID = $this->user_session->userActiveLocation['locationID'];

                // $products[$t->productID] = $this->getLocationProductDetailsForReturn($locationID, $t->productID);
                $incID++;
            }

           
            $locationProducts = Array();
            $inactiveItems = Array();
            foreach ($deliveryNoteProduct as $deleProducts) {
                $locationProducts[$deleProducts->productID] = $this->getLocationProductDetails($locationID, $deleProducts->productID);
                if ($locationProducts[$deleProducts->productID] == null) {
                    $inactiveItem = $inactiveItemDetails = $this->getModel('ProductTable')->getProductDetailsByProductID($deleProducts->productID);
                    $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                }
            }

            $inactiveItemFlag = false;
            $errorMsg = null;
            if (!empty($inactiveItems)) {
                $inactiveItemFlag = true;
                $errorItems = implode(",",$inactiveItems);
                $errorMsg = $this->getMessage('ERR_DLN_ITM_INACTIVE', [$errorItems]);
            }

            // $this->status = true;
            $data = array(
                'deliveryNoteProduct' => $deliveryNoteProduct,
                'currencySymbol' => $currencySymbol,
                'errorMsg' => $errorMsg,
                'inactiveItemFlag' => $inactiveItemFlag,
                );

            return $data;
        }
	}


	/**
	 * This function is used to get data for update job
	 * @param $jobID
	 */
	public function getJobDataForUpdate($data)
	{
		$jobData = $this->getModel('JobTable')->getJobByJobID($data['jobID']);
        $jobDetails = array();
        $jobID;
        foreach ($jobData as $row) {
            $tempG = array();
            $jobID = $row['jobId'];
            $tempG['jobId'] = $row['jobId'];
            $tempG['jobCode'] = $row['jobReferenceNumber'];
            $tempG['jobName'] = $row['jobName'];
            $tempG['projectId'] = $row['projectId'];
            $tempG['jobCustomer'] = $row['customerCode'].'-'.$row['customerName'];
            $tempG['jobCustomerID'] = $row['customerID'];
            $tempG['jobType'] = $row['jobTypeCode'].'-'.$row['jobTypeName'];
            $tempG['jobDueDate'] = $this->convertDateToUserFormat($row['jobDueDate']);
            $tempG['jobEstimatedEndDate'] = $this->convertDateToUserFormat($row['jobEstimatedEndDate']);
            $tempG['jobValue'] = $row['jobWeight'];
            $tempG['jobAdditionalDetail1'] = $row['jobAdditionalDetail1'];
            $tempG['jobAdditionalDetail2'] = $row['jobAdditionalDetail2'];
            $tempG['jobEstimatedCost'] = $row['jobEstimatedCost'];
            $jobTasks = (isset($jobDetails[$row['jobId']]['jobTasks'])) ? $jobDetails[$row['jobId']]['jobTasks'] : array();
            $jobProducts = (isset($jobDetails[$row['jobId']]['jobProducts'])) ? $jobDetails[$row['jobId']]['jobProducts'] : array();
            $jobSupervisors = (isset($jobDetails[$row['jobId']]['jobSupervisors'])) ? $jobDetails[$row['jobId']]['jobSupervisors'] : array();
            $jobManagers = (isset($jobDetails[$row['jobId']]['jobManagers'])) ? $jobDetails[$row['jobId']]['jobManagers'] : array();
            $jobContractors = (isset($jobDetails[$row['jobId']]['jobContractors'])) ? $jobDetails[$row['jobId']]['jobContractors'] : array();

            if (!is_null($row['jobManagerID'])) {
            	$jobManagers[$row['jobReferenceNumber']][$row['employeeIdOfJobManager']] = array('jobManagerID' => $row['employeeIdOfJobManager'], 'jobManagerCodeAndName' => $row['jobMangerFirstName'].' '.$row['jobMangerSecondName']);
            }

            if (!is_null($row['jobSupervisorTableID'])) {
            	$jobSupervisors[$row['jobReferenceNumber']][$row['jobSupervisorID']] = array('jobSupervisorID' => $row['jobSupervisorID'],'jobSupervisorCodeAndName' => $row['jobSupervisorFirstName'].' '.$row['jobSupervisorSecondName']);
            }

            if (!is_null($row['jobContractorID'])) {
            	$jobContractors[$row['contractorIdOfJob']] = array('contractorName' => $row['jobContractorFirstName'].' '.$row['jobContractorLastName']);
            }

            if (!is_null($row['jobProductID'])) {

            	$productUom = $this->getModel('ProductUomTable')->getProductUomListByProductID($row['productID']);

                $thisqty = $this->getProductQuantityViaDisplayUom($row['jobProductAllocatedQty'], $productUom);
                $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

            	$jobProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductAllocatedQty' => $thisqty['quantity'], 'jobProductReOrderLevel' => $row['jobProductReOrderLevel'], 'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'productEstimatedCost' => floatval($row['jobProductAllocatedQty']) * floatval($row['jobProductUnitPrice']), 'jobProductTaskID' => $row['jobProductTaskID'],'jobProductTaskCardID' => $row['jobProductTaskCardID'],'productID' => $row['productID'],'jobProductStatus' => $row['jobProductStatus'],'jobProductID' => $row['jobProductID']);
            }

            if ($row['jobTaskID'] != NULL) {
                $taskKey = $row['jobTaskID'];

                $taskEmployees = (isset($jobTasks[$taskKey]['jobEmployees'])) ? $jobTasks[$taskKey]['jobEmployees'] : array();

                 $taskContractors = (isset($jobTasks[$taskKey]['taskContractors'])) ? $jobTasks[$taskKey]['taskContractors'] : array();

                $jobTasks[$taskKey] = array('taskCode' => $row['taskCode'], 'taskName' => $row['taskName'], 'jobTaskUnitCost' => $row['jobTaskUnitCost'],'jobTaskUnitRate' => $row['jobTaskUnitRate'], 'jobTaskEstQty' => $row['jobTaskEstQty'], 'taskUomID' => $row['taskUomID'], 'taskUomAbbr' => $row['taskUomAbbr'],'taskEstimatedCost' => floatval($row['jobTaskEstQty']) * floatval($row['jobTaskUnitCost']),'jobTaskID' => $row['jobTaskID'],'taskCardID' => $row['jobTaskTaskCardID'],'taskID' => $row['taskID'],'jobTaskStatus' => $row['jobTaskStatus']);
                
                if ($row['jobEmployeeID'] != NULL && $row['jobTaskID'] == $row['employeeJobTaskID']) {
                    $taskEmployees[$row['jobEmployeeID']] = array('employeeName' => $row['jobEmployeeFirstName'].' '.$row['jobEmployeeSecondName'].' ('.$row['designationName'].')', 'employeeJobTaskID' => $row['employeeJobTaskID'], 'employeeDesignationID' => $row['employeeDesignationID']);
                }
                
                if ($row['jobTaskContractorID'] != NULL && $row['jobTaskID'] == $row['contactorJobTaskID']) {
                    $taskContractors[$row['jobTaskContractorID']] = array('contractorName' => $row['jobTaskContractorFirstName'].' '.$row['jobTaskContractorLastName'], 'contactorJobTaskID' => $row['contactorJobTaskID'],'contactorIdOfJobTask' => $row['contactorIdOfJobTask']);
                }

                $jobTasks[$taskKey]['jobEmployees'] = $taskEmployees;
                $jobTasks[$taskKey]['taskContractors'] = $taskContractors;
            }
            
            $tempG['jobManagers'] = $jobManagers;
            $tempG['jobContractors'] = $jobContractors;
            $tempG['jobSupervisors'] = $jobSupervisors;
            $tempG['jobProducts'] = $jobProducts;
            $tempG['jobTasks'] = $jobTasks;
            $jobDetails[$row['jobId']] = $tempG;
        }
        
        return $jobDetails[$jobID];
	}
	
	/**
	 * This function is used to get job contractor details by job ID
	 * @param $jobID
	 */
	public function getJobContractorsByJobID($data)
	{
		$jobContractors = $this->getModel('JobContractorTable')->getJobContractorDetailsByJobID($data['jobID']);
		$jobContractorData = [];

		foreach ($jobContractors as $key => $value) {
			$jobContractorData[] = $value;
		}

		return $jobContractorData;
	}

	/**
	 * This function is used to update job
	 * @param $JSON Request
	 * @param $userLocation
	 * @param $userID
	 */
	public function updateJob($data, $userLocation, $userId, $type="construction") 
	{
		$customerID = $data['customer'];
		$jobType = $data['jobType'];
		$jobCode = $data['jobCode'];
		$jobName = $data['jobName'];
		$jobEditflag = $data['jobEditflag'];
		$projectID = $data['projectID'];
		$jobContractor = $data['contractor'];
		$dueDate = $data['dueDate'];
		$jobValue = $data['jobValue'];
		$jobEstimatedCost = $data['jobEstimatedCost'];
		$jobEstimatedEndDate = $data['jobEstimatedEndDate'];
		$jobSupervisors = $data['jobSupervisors'][$jobCode];
		$jobManagers = $data['jobManagers'][$jobCode];
		$jobTasks = $data['jobTasks'];
		$jobResources = $data['jobResources'][$jobCode];
		$editedJobID = $data['editedJobID'];
		$vehicleRegNo = $data['vehicleRegNo'];
		$vehicleModel = $data['vehicleModel'];
		$vehicleSubModelID = $data['vehicleSubModelID'];
		$vehicleType = $data['vehicleType'];
		$fuelType = $data['fuelType'];
		$enginNo = $data['enginNo'];
		$kmCovered = $data['kmCovered'];
		$selectedServices = $data['selectedServices'];
		$serviceVehicleID = ($data['serviceVehicleID'] == "") ? null : $data['serviceVehicleID'];
		$customerNotes = $data['customerNotes'];
		$supervisorId = $data['supervisorId'];
		$jobStatusID = $data['jobStatus'];
		$serviceEmployees = $data['serviceEmployees'];
		$additionalDetail1 = (!empty($data['additionalDetail1'])) ? $data['additionalDetail1'] : null;
		$additionalDetail2 = (!empty($data['additionalDetail2'])) ? $data['additionalDetail2']: null;

		$jData = array(
			'jobName' => $jobName,
			'customerID' => $customerID,
			'jobTypeId' => $jobType,
			'jobEstimatedCost' => $jobEstimatedCost,
			'projectId' => $projectID,
			'jobEstimatedEndDate' => $this->convertDateToStandardFormat($jobEstimatedEndDate),
			'jobDueDate' => $this->convertDateToStandardFormat($dueDate),
			'jobWeight' => $jobValue,
			'locationID' => $userLocation,
			'customerNotes' => $customerNotes,
			'supervisorId' => $supervisorId,
			'jobAdditionalDetail1' => $additionalDetail1,
			'jobAdditionalDetail2'	=> $additionalDetail2

		);

		if ($type == 'service') {
			$jobTasks = $this->processJobServices($selectedServices);
		}
		$jobUpdateStatus = $this->getModel('JobTable')->updateJobData($editedJobID, $jData);

		if ($jobUpdateStatus) {
			// update entity for job update			
			$jobData = $this->getModel('JobTable')->getJobsByJobID($editedJobID);
			$entityService = $this->getService('EntityService');
        	$entityID = $entityService->updateEntity($jobData->entityID, $userId);

			//update job supervisor details
			$updateJobSupervisor = $this->updateJobSupervisor($jobSupervisors, $editedJobID);
			if (!$updateJobSupervisor) {
				return $this->returnError($updateJobSupervisor['msg'], null);
			}
			//update job contractor details
			$updateJobContractor = $this->updateJobContractor($jobContractor, $editedJobID);
			if (!$updateJobContractor) {
				return $this->returnError($updateJobContractor['msg'], null);
			}

			//update job manager details
			$updateJobManager = $this->updateJobManager($jobManagers, $editedJobID);
			if (!$updateJobManager) {
				return $this->returnError($updateJobManager['msg'], null);
			}

			//update job task details
			$updateJobTask = $this->updateJobTask($jobTasks, $editedJobID);
			if (!$updateJobTask) {
				return $this->returnError($updateJobTask['msg'], null);
			}

			if ($type == 'construction') {
				//update job product details
				$updateJobProduct = $this->updateJobProduct($jobResources, $editedJobID, $jobTasks);
				if (!$updateJobProduct) {
					return $this->returnError($updateJobProduct['msg'], null);
				}
			}
			
			if ($type == 'service') {


				if (is_null($serviceVehicleID)) {
					if ($vehicleSubModelID == "new") {
						$subModelData = array(
							'vehicleSubModelID' => null,
							'vehicleSubModelName' => $vehicleSubModelNewName,
							'vehicleModelID' => $vehicleModel,
						);
						$vehicleSubModelData = new VehicleSubModel();
						$vehicleSubModelData->exchangeArray($subModelData);

						$vehicleSubModelID = $this->getModel('VehicleSubModelTable')->saveVehicleSubModel($vehicleSubModelData);
						if(!$vehicleSubModelID) {
							return $this->returnError('ERR_VEH_TYP_SAVE', null);
						}
					}

					$vehicleData = array(
						'vehicleType' => $vehicleType,
						'fuelTypeID' => $fuelType,
						'customerID' => $customerID,
						'vehicleModel' => $vehicleModel,
						'vehicleSubModel' => $vehicleSubModelID,
						'serviceVehicleRegNo' => $vehicleRegNo,
						'serviceVehicleEngineNo' => $enginNo,
					);

					$serviceVehicleData = new ServiceVehicle();
					$serviceVehicleData->exchangeArray($vehicleData);


					$serviceVehicleSave = $this->getModel('ServiceVehicleTable')->saveVehicle($serviceVehicleData);

					if (!$serviceVehicleSave) {
						return $this->returnError('ERR_SAVE_VEHICLE', null);
					}

					$updateJobVehicleData = $this->updateJobVehicleData($editedJobID, $kmCovered, $serviceVehicleSave);

					if (!$updateJobVehicleData) {
						return $this->returnError('ERR_SAVE_JOB_VEHICLE', null);
					}

				
				} else {

					if ($jobStatusID != 8 && $jobStatusID != 3 && $jobStatusID != 17) {
						$openJobTasksCount = $this->getModel('JobTaskTable')->getJobTaskDetails($editedJobID, null, null, 3)->count();

						if ($openJobTasksCount > 0) {
							$updateJobState = $this->getModel('JobTable')->updateJobStatus($editedJobID, 8);
					        if (!$updateJobState) {
					            return $this->returnError('ERR_JOB_STATE_UPDATE');
					        }
						}
					}
					
					$updateServiceEmp = $this->getModel('ServiceEmployeeTable')->updateDeleted($editedJobID);
			        if (!$updateServiceEmp) {
			            return $this->returnError('ERR_JOB_STATE_UPDATE');
			        }

			        if ($serviceEmployees != "") {
						foreach ($serviceEmployees as $value) {
							$empData = array(
								'jobID' => $editedJobID,
								'employeeID' => $value
							);
							$empSaveData = new ServiceEmployee();
							$empSaveData->exchangeArray($empData);


							$serviceEmployeeID = $this->getModel('ServiceEmployeeTable')->saveServiceEmployeeData($empSaveData);
							if (!$serviceEmployeeID) {
								return $this->returnError('ERR_SAVE_JOB_SERVICE_EMP', null);
							}
						}
					}

					$vehicleData = array(
						'vehicleType' => $vehicleType,
						'fuelTypeID' => $fuelType,
						'customerID' => $customerID,
						'vehicleModel' => $vehicleModel,
						'vehicleSubModel' => $vehicleSubModelID,
						'serviceVehicleRegNo' => $vehicleRegNo,
						'serviceVehicleEngineNo' => $enginNo,
					);
					$serviceVehicleUpdate = $this->getModel('ServiceVehicleTable')->updateVehicle($serviceVehicleID, $vehicleData);
					if (!$serviceVehicleUpdate) {
						return $this->returnError('ERR_UPDATE_VEHICLE', null);
					}

			
					$updateJobVehicle = $this->updateJobVehicleData($editedJobID, $kmCovered, $serviceVehicleID);

					if (!$updateJobVehicle) {
						return $this->returnError('ERR_UPDATE_JOB_VEHICLE', null);
					}
				}
			
				$updateServiceJobProducts = $this->updateServiceJobProduct($jobTasks, $editedJobID, $vehicleType);

				if (!$updateServiceJobProducts) {
					return $this->returnError('ERR_UPDATE_JOB_PRO', null);
				}

				$allJobServices = $this->getModel('JobTaskTable')->getJobTaskDetails($editedJobID);

				foreach ($allJobServices as $value) {
					$startedAt = $value['startedAt'];
					$endedAt = $value['endedAt'];
					$existingJobTaskEmployee = $this->getModel('JobTaskEmployeeTable')->getJobTaskRelatedEmployees($value['jobTaskID']);

					foreach ($existingJobTaskEmployee as $emp) {
						if (!in_array($emp['employeeID'], $selectedServices[$value['taskID']]['selectedEmployeeList'])) {
							if (!is_null($emp['employeeID'])) {
								$employeeDelete = $this->getModel('JobTaskEmployeeTable')->deleteJobTaskEmployeeByJobTaskID($emp['jobTaskEmployeeID']);
							}
						} 
					}

					foreach ($selectedServices[$value['taskID']]['selectedEmployeeList'] as $val) {
						
						$existingEmp = $this->getModel('JobTaskEmployeeTable')->getJobTaskEmployee($value['jobTaskID'], $val);
						if (!$existingEmp) {
							$jobTaskEmpData = [
					            'employeeID' => $val,
					            'taskID' => $value['taskID'],
					            'jobTaskID' => $value['jobTaskID']
					        ];

					        $jobTaskEmployeeData = new JobTaskEmployee();
					        $jobTaskEmployeeData->exchangeArray($jobTaskEmpData);
					        $jobTaskEmployeeID = $this->getModel('JobTaskEmployeeTable')->saveJobTaskEmployee($jobTaskEmployeeData);
					        if (!$jobTaskEmployeeID) {
								return $this->returnError('ERR_JOB_TSK_EMP_CREATE', null);
							}


							$jobTaskEmpWorkTimeData = [
					            'jobTaskEmployeeID' => $jobTaskEmployeeID,
					            'startedAt' => $startedAt,
					            'endedAt' => $endedAt
					        ];

					        $jobTaskEmployeeWrkTmData = new JobTaskEmployeeWorkTime();
					        $jobTaskEmployeeWrkTmData->exchangeArray($jobTaskEmpWorkTimeData);
					        $jobTaskEmployeeWorkTimeID = $this->getModel('JobTaskEmployeeWorkTimeTable')->saveJobTaskEmployeeWorkTime($jobTaskEmployeeWrkTmData);

					        if (!$jobTaskEmployeeWorkTimeID) {
					            return $this->returnError('ERR_JOB_TSK_EMP_CREATE', null);
					        }	
						}
					}

					$jobTaskData = $this->getModel('JobTaskTable')->getTaskDetailsByJobTaskIdForService($value['jobTaskID']);
					$updateIncentive = $this->updateJobTaskEmployeeIncentive($value['jobTaskID'], $jobTaskData['taskID'], $jobTaskData['vehicleType']);

				}

			}

			if (!is_null($projectID)) {
				$result = $this->updateJobDetailsInProject($projectID);
				if (!$result) {
					return $this->returnError('ERR_UPDATE_PROJECT_IN_JOB', null);
				}
			}
			return $this->returnSuccess($jobCode,'SUCC_UPDATE_JOB');
		} else {
			return $this->returnError('ERR_UPDATE_JOB',null);
		}
	}

	/**
    * this function use to update job service employee incentive
    * @param int $jobTaskID
    * return boolean
    **/
    public function updateJobTaskEmployeeIncentive($jobTaskID, $serviceID = null, $vehicleType = null) {

        $jobTask = $this->getModel('JobTaskTable')->getTaskDetailsByJobTaskIdForService($jobTaskID);

        // if (!is_null($jobTask['jobTaskTaskCardID'])) {
            // $jobTask = $this->getModel('TaskCardTaskTable')->getTaskList($jobTask['jobTaskTaskCardID'], $jobTask['taskID'])->current();
        // }

        if (empty($jobTask['jobTaskTaskCardID'])) {

            $serviceIncentiveDetails = $this->getModel('ServiceInceniveRateTable')->getIncentiveByServiceIdAndVehicleType($serviceID, $vehicleType);
            if ($serviceIncentiveDetails) {
                $actulalJobTaskEmp = $this->getModel('jobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($jobTaskID, $unique = true);

                $numberOfEmp = $actulalJobTaskEmp->count();

                $allocateIncentiveVal = $serviceIncentiveDetails['incentive'] / $numberOfEmp;

                $jobTaskEmployeeList = $this->getModel('jobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($jobTaskID, $unique = false);
                foreach ($jobTaskEmployeeList as $key => $value) {
                    $updateJobTaskEmployeeIncentiveVal = $this->getModel('jobTaskEmployeeTable')->updateJobTaskEmployeeIncentiveVal($value['jobTaskEmployeeID'], $allocateIncentiveVal);
                }
            }
        } else {

            $jobCardIncentive = $this->getModel('JobCardIncentiveRateTable')->getJobCardIncentiveByJobCardIdAndVehicleType($jobTask['jobTaskTaskCardID'], $vehicleType);


            $jobCardServiceIncentive = $this->getModel('JobCardServiceIncentiveRateTable')->getJobCardServiceIncentiveByServiceIdAndJobCardRateID($jobCardIncentive['jobCardIncentiveRateID'],$serviceID);

            $actulalJobTaskEmp = $this->getModel('jobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($jobTaskID, $unique = true);

            $numberOfEmp = $actulalJobTaskEmp->count();

            $allocateIncentiveVal = $jobCardServiceIncentive['incentive'] / $numberOfEmp;

            $jobTaskEmployeeList = $this->getModel('jobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($jobTaskID, $unique = false);
            foreach ($jobTaskEmployeeList as $key => $value) {
                $updateJobTaskEmployeeIncentiveVal = $this->getModel('jobTaskEmployeeTable')->updateJobTaskEmployeeIncentiveVal($value['jobTaskEmployeeID'], $allocateIncentiveVal);
            }
        }
        return true;
    }


	/**
	 * This function is used to update job supervisors
	 * @param $jobSupervisorData
	 * @param $jobID
	 * @return resultSet
	 */
	public function updateJobSupervisor($jobSupervisors, $jobID)
	{
		$deleteExistingJobSupervisors = $this->getModel('JobSupervisorTable')->deleteJobSupervisorByJobID($jobID);
		if ($deleteExistingJobSupervisors >= 0) {
			//if the job supervisor is not empty, than save job supervisor details
			if (!is_null($jobSupervisors)) {
				$saveJobSupervisor = $this->saveJobSupervisor($jobSupervisors, $jobID);
				if (!$saveJobSupervisor) {
					return $this->returnError('ERR_SAVE_JOB_SUPERVISOR', null);
				}
				return $this->returnSuccess(null,'');
			}
			return $this->returnSuccess(null,'');
		}
	}

	/**
	 * This function is used to update job contractors
	 * @param $jobContractorData
	 * @param $jobID
	 * @return resultSet
	 */
	public function updateJobContractor($jobContractor, $jobID)
	{
		$deleteExistingJobContractor = $this->getModel('JobContractorTable')->deleteJobContractorByJobID($jobID);

		if ($deleteExistingJobContractor >= 0) {
			//if the job contractor is not empty, than save job contractor details
			if (!is_null($jobContractor)) {
				$saveJobContractor = $this->saveJobContractor($jobContractor, $jobID);
				if (!$saveJobContractor) {
					return $this->returnError('ERR_SAVE_JOB_CONTRACTOR', null);
				}
				return $this->returnSuccess(null,'');
			}
			return $this->returnSuccess(null,'');
		}
	}

	/**
	 * This function is used to update job mangaers
	 * @param $jobMangaerData
	 * @param $jobID
	 * @return resultSet
	 */
	public function updateJobManager($jobManagers, $jobID)
	{
		$deleteExistingJobManager = $this->getModel('JobManagerTable')->deleteJobManagerByJobID($jobID);

		if ($deleteExistingJobManager >= 0) {
			//if the job managers is not empty, than save job managers
			if (!is_null($jobManagers)) {
				$saveJobManager = $this->saveJobManager($jobManagers, $jobID);
				if (!$saveJobManager) {
					return $this->returnError('ERR_SAVE_JOB_MANAGER', null);
				}
				return $this->returnSuccess(null,'');
			}
			return $this->returnSuccess(null,'');
		}
	}

	/**
	 * This function is used to update job tasks
	 * @param $jobTaskData
	 * @param $jobID
	 * @return resultSet
	 */
	public function updateJobTask($jobTasks, $jobID)
	{
		$existingJobTasks = $this->getModel('JobTaskTable')->getJobTaskDetails($jobID, null, null, 3);
		
		foreach ($existingJobTasks as $key => $value) {
			$deleteExistingJobTaskEmployee = $this->getModel('JobEmployeeTable')->deleteJobTaskEmployeeByJobTaskID($value['jobTaskID']);
			$deleteExistingJobTaskContractor = $this->getModel('JobTaskContractorTable')->deleteJobTaskContractorByJobTaskID($value['jobTaskID']);

			$deleteExistingJobSubTasks = $this->getModel('JobSubTaskTable')->deleteJobSubTaskJobTaskID($value['jobTaskID']);
			$deleteExistingJobTask = $this->getModel('JobTaskTable')->deleteJobTaskByJobTaskID($value['jobTaskID']);
		}

		//If the job tasks is not empty, than save job tasks.
		if (!is_null($jobTasks)) {
			$saveJobTasks = $this->saveJobTasks($jobTasks, $jobID, true);
			if (!$saveJobTasks['status']) {
				return $this->returnError($saveJobTasks['msg'], null);
			}
		}
		return $this->returnSuccess(null,'');
	}

	/**
	 * This function is used to update job products
	 * @param $jobresourceData
	 * @param $jobID
	 * @param $jobtaskData
	 * @return resultSet
	 */
	public function updateJobProduct($jobResources, $jobID, $jobTasks) 
	{
		$existingJobProducts = $this->getModel('JobProductTable')->getJobProductByJobID($jobID);

		foreach ($existingJobProducts as $key => $value) {
			if (is_null($value->jobProductStatus)) {
				$deleteExistingJobProducts = $this->getModel('JobProductTable')->deleteJobProductByJobProductID($value->jobProductID);
			}
		}

		//If the resources is not empty, than save resources that are not related to tasks
		if (!is_null($jobResources)) {
			foreach ($jobResources as $key => $value) {
				$checkExistJobProduct = $this->getModel('JobProductTable')->getJobProductByJobProductID($value['jobMaterialEditedProductID']);
				$productKey = explode('_', $key);
				if (is_null($checkExistJobProduct)) {
					if (sizeof($productKey) == 2) {
						$saveProducts = $this->saveJobProducts($value, null, $jobID, $productKey[1], null);
						if (!$saveProducts) {
							return $this->returnError('ERR_SAVE_JOB_PRODUCTS', null);
						}
					} else if (sizeof($productKey) == 4) {
						foreach ($jobTasks as $jobTaskKey => $jobtaskValue) {
							$taskProductKey = $productKey[1] . '_' . $productKey[2];
							if ($jobTaskKey == $taskProductKey) {
								$jobTaskDetails = $this->getModel('JobTaskTable')->getJobTaskDetails($jobID, $jobtaskValue['taskID'], $jobtaskValue['taskCardID'])->current();
								$jobTaskID = $jobTaskDetails['jobTaskID'];
								$saveTaskProducts = $this->saveJobProducts($value, $jobTaskID, $jobID, $productKey[3], $jobTaskDetails['jobTaskTaskCardID']);
								if (!$saveTaskProducts) {
									return $this->returnError('ERR_SAVE_JOB_TASK_PRODUCT', null);
								}
							} 
						}
					}
				}
			}
		}
		return $this->returnSuccess(null,'');
	}

    public function searchJobProjectForDropdown($data)
    {
    	$jobSearchKey = $data['searchKey'];

        $locationID = $this->user_session->userActiveLocation["locationID"];
        $jobs = $this->getModel('JobTable')->searchJobsAndProJectForDropDown($locationID, $jobSearchKey);
        
        $jobList = array();
        foreach ($jobs as $job) {
        	if ($job['jobReferenceNumber'] != null) {
	            $temp['value'] = $job['jobId'];
	            if ($job['projectCode'] != null) {
	            	$temp['text'] = $job['jobReferenceNumber'] . " - " . $job['jobName']." of project ". $job['projectCode']." - ".$job['projectName'] ;
	            	
	            } else {
	            	$temp['text'] = $job['jobReferenceNumber'] . " - " . $job['jobName'];
	            }
	            $jobList[] = $temp;
        		
        	}
        }
        
        return $jobList;
    }

    public function getJobTaskWithProductDetails($data)
    {
    	$status = [18]; // status 18 mean ended.
    	// get current location ID
    	$locationID = $this->user_session->userActiveLocation["locationID"];
    	// get completed job task with their products for given jobID
    	$jobTaskDetails = $this->getModel('JobTaskTable')->getJobTaskWithProductDetails($data['jobId'], $locationID, $status);

    	// set job task details and job task product details in to the two arrays
    	$jobTask = [];
    	$jobTaskProduct = [];
    	
    	foreach ($jobTaskDetails as $key => $jobTaskSingleRecord) {
    		
    		// check all qty invoiced or not
    		if ($jobTaskSingleRecord['jobTaskInvoicedQty'] < $jobTaskSingleRecord['jobTaskUsedQty']) {
    			// set data to the job task array
	    		$jobTask[$jobTaskSingleRecord['jobTaskID']] = [
	    			'jobTaskID' => $jobTaskSingleRecord['jobTaskID'],
	    			'taskID' => $jobTaskSingleRecord['taskID'],
	    			'jobTaskUnitCost' => floatval($jobTaskSingleRecord['jobTaskUnitCost']),
	    			'jobTaskUnitRate' => floatval($jobTaskSingleRecord['jobTaskUnitRate']),
	    			'jobTaskQty' => floatval($jobTaskSingleRecord['jobTaskUsedQty']),
	    			'taskCode' => $jobTaskSingleRecord['taskCode'],
	    			'taskName' => $jobTaskSingleRecord['taskName'],
	    			'uomName' => $jobTaskSingleRecord['taskUomAbbr']
	    		];
    			
    		}
    		
    		// set data to the job task product array
    		if ($jobTaskSingleRecord['jobProductID'] != null) {
	    		$jobTaskProduct[$jobTaskSingleRecord['jobTaskID']][$jobTaskSingleRecord['jobProductID']] = [
	    			'productCodeName' => $jobTaskSingleRecord['productCode'].' - '.$jobTaskSingleRecord['productName'],
	    			'uom' => $jobTaskSingleRecord['productUomAbbr'],
	    			'jobProductID' => $jobTaskSingleRecord['jobProductID'],
	    			'jobTaskID' => $jobTaskSingleRecord['jobTaskID'],
	    			'jobProductUnitPrice' => $jobTaskSingleRecord['jobProductUnitPrice'],
	    			'jobProductUsedQty' => $jobTaskSingleRecord['jobProductUsedQty'],
	    			'jobProductRemInvoicedQty' => floatval($jobTaskSingleRecord['jobProductUsedQty']) - floatval($jobTaskSingleRecord['jobProductInvoicedQty']),
	    			'locationProductID' => $jobTaskSingleRecord['locationProductID']
	    			
	    		];
    			
    		}
    	}

    	// get non tasked items
    	$jobProductDetails = $this->getModel('JobProductTable')->getNonTaskProductByJobIDAndLocationID($data['jobId'], $locationID);
    	$itemSetArray = [];
    	$addNonTaskProductFlag = false;
    	foreach ($jobProductDetails as $key => $jobProductSingle) {
    		
    		if ($jobProductSingle['jobProductInvoicedQty'] < $jobProductSingle['jobProductUsedQty']) {
    			$jobTaskProduct['default'][$jobProductSingle['jobProductID']] = [
    				'productCodeName' => $jobProductSingle['productCode'].' - '.$jobProductSingle['productName'],
    				'uom' => $jobProductSingle['uomAbbr'],
	    			'jobProductID' => $jobProductSingle['jobProductID'],
	    			'jobTaskID' => 'default',
	    			'jobProductUnitPrice' => $jobProductSingle['jobProductUnitPrice'],
	    			'jobProductUsedQty' => $jobProductSingle['jobProductUsedQty'],
	    			'jobProductRemInvoicedQty' => floatval($jobProductSingle['jobProductUsedQty']) - floatval($jobProductSingle['jobProductInvoicedQty']),
	    			'locationProductID' => $jobProductSingle['locationProductID']
    			];
    			$addNonTaskProductFlag = true;
    		}
    	}
    	if ($addNonTaskProductFlag) {
    		$jobTask['default'] = [
    			'jobTaskID' => null,
    			'taskID' => null,
    			'jobTaskUnitCost' => floatval(0),
    			'jobTaskUnitRate' => floatval(0),
    			'jobTaskQty' => floatval(1),
    			'taskCode' => 'def',
    			'taskName' => 'default',
    			'uomName' => '-'
    		];
    	}

    	return [
    		'jobTaskDetails' => $jobTask,
    		'jobTaskProductDetails' => $jobTaskProduct,
    	];
    	
    }

    /**
     * This function is used to get vehicle details
     */
    public function getVehicleDetails($data)
    {
        $vehicleRegNo = $data['vehicleRegNo'];
        $result = $this->getModel('ServiceVehicleTable')->getVehicleDetails($vehicleRegNo);
        
        if ($result) {
        	$respondData = $result->current();
            return $this->returnSuccess($respondData,null);
        } else {
            return $this->returnError('ERR_RETRIVE_JOB_CRD_RELATED_SER',null);
        }

    }

    /**
	 * This function is used process selected services as job tasks
	 * @param array $selectedService
	 * return array
	 */
	public function processJobServices($selectedService) {
		$jobTasks = [];
		foreach ($selectedService as $key => $value) {
			$temp = [];
			$temp['taskCardID'] = $value['jobCardID'];
			$temp['taskID'] = $key;
			$temp['selectedSubTasks'] = $value['selectedSubTasks'];
			$jobTasks[] = $temp;

		}

		return $jobTasks;
		
	}

	/**
	 * This function is used to save job sub task
	 * @param $subTaskData
	 * @param $jobTaskID
	 * return boolean
	 */
	public function saveJobSubTasks($subTaskData, $jobTaskID) {
		foreach ($subTaskData as $key => $value) {
			$data = array(
				'subTaskId' => $key,
				'jobTaskId' => $jobTaskID,
			);

			$jobSubTaskData = new JobSubTask();
			$jobSubTaskData->exchangeArray($data);

			$jobSubTaskID = $this->getModel('JobSubTaskTable')->saveJobSubTask($jobSubTaskData);

			if (!$jobSubTaskID) {
				return false;
			}
		}
		return true;
	}


	/**
	 * This function is used to save job vehicle data
	 * @param $vehicleId
	 * @param $jobID
	 * @param $kmCovered
	 */
	public function saveJobVehicleData($jobId, $kmCovered, $vehicleId) {

		$data = array(
			'vehicleID' => $vehicleId,
			'jobVehicleKMs' => $kmCovered,
			'jobId' => $jobId,
		);


		$jobVehicleData = new JobVehicle();
		$jobVehicleData->exchangeArray($data);
		$jobVehicleSave = $this->getModel('JobVehicleTable')->saveJobVehicle($jobVehicleData);
		
		if (!$jobVehicleSave) {
			return false;
		}

		return true;
	}

	/**
	 * This function is used to update job vehicle data
	 * @param $vehicleId
	 * @param $jobID
	 * @param $kmCovered
	 */
	public function updateJobVehicleData($jobId,$kmCovered, $vehicleId) {

		$getExsistingJobVehicle = $this->getModel('JobVehicleTable')->getJobVehicleByJobID($jobId)->current();


		if (!($getExsistingJobVehicle['vehicleID'] == $vehicleId && $getExsistingJobVehicle['kmCovered'] == $kmCovered)) {
			$deleteJobVehicle = $this->getModel('JobVehicleTable')->deleteJobVehicle($getExsistingJobVehicle['jobVehicleID']);
			if ($deleteJobVehicle) {
				$saveJobVehicle = $this->saveJobVehicleData($jobId, $kmCovered, $vehicleId);

				if (!$saveJobVehicle) {
					return false;
				}
			} else {
				return false;
			}
		}
	
		return true;
	}

	/**
    * this function use to get job related services
    * @param array $data
    * return array
    **/
    public function getJobRelatedServicesByJobId($data, $jobDashboard = true)
    {
    	$jobId = $data['jobId'];
        $paginated = false;
            
        $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobId);

        $list = [];
        foreach ($serviceList as $key => $value) {
        	$value['startedAt'] = ($value['startedAt'] != NULL) ? $this->getUserDateTime($value['startedAt']) : NULL;
        	$value['endedAt'] = ($value['endedAt'] != NULL) ? $this->getUserDateTime($value['endedAt']) : NULL;
        	$value['departmentStationList'] = $this->getDepartmentStationList($value['departmentID']);
        	$value['departmentEmployeeList'] = $this->getDepartmentEmployeeList($value['departmentID']);
        	$value['employeeNames'] = $this->getServiceEmployees($value['jobTaskID']);

        	//check job sub task
        	$res = $this->getModel('jobSubTaskTable')->getSubTaskByJobTaskId($value['jobTaskID']);
        	$value['hasSubTasks'] = (sizeof($res) > 0) ? true : false;

        	$list [] = $value;
        }

        if ($serviceList) {
        	$view = new ViewModel(array(
	            'serviceList' => $list,
	            'status' => $this->getStatusesList(),
	        ));
	        $view->setTerminal(true);

	        if ($jobDashboard) {
	        	$view->setTemplate('jobs/service-job-dashboard/service-list.phtml');
	        } else {
	        	$view->setTemplate('jobs/service-jobs/view-services-modal.phtml');
	        }
	        
	        return [
	            'view' => $view,
	            'status' => true,
	            'data'=> $list
	        ];
        } else {
        	return $this->returnError('ERR_RETRIVE_JOB_RELATED_SER',null);
        }

    }

    public function getServiceEmployees($serviceID)
    {
    	$jobTaskEmpList = $this->getModel('JobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($serviceID, true);

    	$employeeNames = '';
    	foreach ($jobTaskEmpList as $value) {
    		if (is_null($value['employeeID'])) {
    			$employeeNames = 'Default Employee';
    		} else {
    			$employeeNames = ($employeeNames == "") ? $value['employeeCode']." ".$value['employeeFirstName'] : $employeeNames.', '.$value['employeeCode']." ".$value['employeeFirstName'];
    		}
    	}

    	return $employeeNames;
    }

    public function getJobRelatedMaterials($data)
    {
    	$materialData = $this->getModel('JobTempMaterialTable')->getJobRelatedMaterials($data['jobId']);

    	$data = [];
    	foreach ($materialData as $value) {
    		$data[$value['jobTempMaterialID']] = $value;
    	}

    	return $data;
    }


    public function getSelectedServiceEmployees($serviceID)
    {
    	$jobTaskEmpList = $this->getModel('JobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($serviceID, true);

    	$employeeNames = [];
    	foreach ($jobTaskEmpList as $value) {
    		if (!is_null($value['employeeID'])) {
    			$employeeNames[] = $value['employeeID'];
    		}
    	}

    	return $employeeNames;
    }


    public function getDepartmentStationList($departmentID) {
        $departmentStationList = [];

        $departmentStations = $this->getModel('DepartmentStationTable')->getDepartmentRelatedStations($departmentID);

        if (!$departmentStations) {
            return $this->returnError('ERR_RETRIVE_DEP_RELATED_STAT', null);
        }

        foreach ($departmentStations as $key => $value) {
            $result = $this->getModel('jobTaskTable')->getInprogressJobTaskByStation($value['departmentStationId']);

            if (sizeof($result) > 0) {
                $value['engaged'] = true;

            } else {
                $value['engaged'] = false;
            }
            $departmentStationList[] = $value;
            
        }

        return $departmentStationList;
    }

    public function getDepartmentEmployeeList($departmentID) {
        $departmentEmployeeList = [];
        $departmentEmployees = $this->getModel('EmployeeDepartmentTable')->getEmployeeDepartmentsByDepartmentId($departmentID, true);

        if (!$departmentEmployees) {
            return $this->returnError('ERR_RETRIVE_DEP_RELATED_EMP', null);
        }

        foreach ($departmentEmployees as $key => $value) {
            $result = $this->getModel('jobTaskTable')->getInprogressJobTaskByEmployeeId($value['employeeId']);

            if (sizeof($result) > 0) {
                $value['engaged'] = true;

            } else {
                $value['engaged'] = false;
            }
            $departmentEmployeeList[] = $value;
            
        }

        return $departmentEmployeeList;
    }

	/**
	 * This function is used to get job contractor details by job ID
	 * @param $jobID
	 */
	public function getServiceJobById($data, $locationID)
	{
		$jobID = $data['jobID'];
		$jobBasicDataData = $this->getModel('JobTable')->getJobsByJobID($jobID);
		$customerFlag = true;
		if (is_null($jobBasicDataData->customerID)) {
			$customerFlag = false;
		}
		$job = $this->getModel('JobTable')->getServiceJobById($jobID, $locationID, $customerFlag);

		$relatedServices = $this->getModel('TaskCardTaskTable')->getRelatedServices($job['jobTaskTaskCardID']);
		$subTaskVerifyArr = [];
		$allServices = [];
		// foreach ($relatedServices as $key => $value) {
		// 	$allServices [$value['taskID']] = [
		// 		'codeName' => $value['taskCode'].'-'.$value['taskName'],
		// 		'jobCard' => $job['jobTaskTaskCardID']

		// 	]; 
		// }

		$selsectedServiceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($job['jobId']);
		$selectedJobCardServices = [];
		$selectedotherService = [];
		$selectedServices = [];
		foreach ($selsectedServiceList as $key => $value) {
			$temp = [];
			$relatedSubTask = $this->getModel('ServiceSubTaskTable')->getRelatedSubTasks($value['taskID']);

			foreach ($relatedSubTask as $key => $val) {
				$isSelected = $this->getModel('JobSubTaskTable')->getSubTaskById($value['jobTaskID'], $val['subTaskId'])->current();

				$temp[$val['subTaskId']] = [
					'code' => $val['subTaskCode'],
					'selected' => ($isSelected) ? true : false
				]; 
			}
			$selectedServices [$value['taskID']] =  [
				'code' => $value['taskCode'],
				'name' => $value['taskName'],
				'jobCardID' => $value['jobTaskTaskCardID'],
				'departmentEmployeeList' => $this->getDepartmentEmployeeList($value['departmentID']),
				'selectedEmployeeList' => $this->getSelectedServiceEmployees($value['jobTaskID']),
				'selectedSubTasks' => $temp,
				'isStarted' => ($value['jobTaskStatus'] != 3) ? true : false
			];

			if ($value['jobTaskTaskCardID']) {
				$selectedJobCardServices [$value['taskID']] = $value['taskCode'].'-'.$value['taskName'];
			} else {
				$selectedotherService [$value['taskID']] = $value['taskCode'].'-'.$value['taskName'];
			}
		}


		// foreach ($selectedotherService as $key => $value) {
		// 	$allServices[$key] = $value;

		// 	$allServices [$key] = [
		// 		'codeName' => $value,
		// 		'jobCard' => ''
		// 	]; 
		// }

		foreach ($selectedServices as $key => $value) {
			$allServices [$key] = [
				'codeName' => $value['code'].'-'.$value['name'],
				'jobCard' => (empty($value['jobCardID'])) ? '' : $value['jobCardID']
			];
		}

		foreach ($allServices as $key => $value) {
			$result = $this->getModel('ServiceSubTaskTable')->getRelatedSubTasks($key);

			if ($result->count() > 0) {
				$subTaskVerifyArr[$key] = true;
			} else {
				$subTaskVerifyArr[$key] = false;
			}
		}

		$relatedEmployees = $this->getModel('ServiceEmployeeTable')->getRelatedServiceEmployees($jobID);
		$allEmployees = [];
		foreach ($relatedEmployees as $key => $value) {
			$allEmployees [] = $value['employeeID']; 
		}

		$job['selectedJobCardServices'] = $selectedJobCardServices;
		$job['selectedotherService'] = $selectedotherService;
		$job['allServices'] = $allServices;
		$job['allEmployees'] = $allEmployees;
		$job['subTaskVerifyArr'] = $subTaskVerifyArr;
		$job['selectedServices'] = $selectedServices;
	
		if ($job) {
			return $this->returnSuccess($job,null);
		} else {
			return $this->returnError('ERR_RETRIVE_JOB_RELATED_SER',null);
		}

	}


    /**
	 * This function is used to save job vehicle data
	 * @param $vehicleId
	 * @param $jobID
	 * @param $kmCovered
	 */
	public function saveJobRelatedServiceProducts($jobTasks, $jobId, $vehicleType) {

		foreach ($jobTasks as $key => $value) {
			$products = $this->getModel('ServiceProductTable')->getProductList($value['taskID'], $vehicleType);
			if (!$products) {
				return false;
			}

			$jobTaskDetails = $this->getModel('JobTaskTable')->getJobTaskDetails($jobId, $value['taskID'])->current();

			if (!$jobTaskDetails) {
				return false;
			}

			$jobTaskID = $jobTaskDetails['jobTaskID'];

			foreach ($products as $key => $val) {
				if ($val['locationProductID']) {
					$pData = array(
						'jobID' => $jobId,
						'locationProductID' => $val['locationProductID'],
						'jobProductUnitPrice' => $val['productDefaultSellingPrice'],
						'uomID' => $val['serviceProductUomID'],
						'jobProductAllocatedQty' => $val['serviceProductQty'],
						'jobProductRequestedQty' => $val['serviceProductQty'],
						'jobProductMaterialTypeID' => $val['serviceProductType'],
						'jobProductTaskCardID' => null,
						'jobTaskID' => $jobTaskID,
					);

					$jobProductData = new JobProduct();
					$jobProductData->exchangeArray($pData);
					$jobProductID = $this->getModel('JobProductTable')->saveJobProduct($jobProductData);
					
					if (!$jobProductID) {
						return false;
					}
				}
				
			}
				
		}
		return true;
	}

	/**
	 * This function is used to save job vehicle data
	 * @param $vehicleId
	 * @param $jobID
	 * @param $kmCovered
	 */
	public function updateServiceJobProduct($jobTasks, $jobId, $vehicleTypeID) {

		$existingJobProducts = $this->getModel('JobProductTable')->getJobProductByJobID($jobId);


		foreach ($existingJobProducts as $key => $value) {	
			if (is_null($value->jobProductStatus)) {
				$deleteExistingJobProducts = $this->getModel('JobProductTable')->deleteJobProductByJobProductID($value->jobProductID);
			}
		}
		$updatedJobTasks = [];

		foreach ($jobTasks as $key => $val) {
			$jobTaskDetails = $this->getModel('JobTaskTable')->getJobTaskDetails($jobId, $val['taskID'])->current();

			$checkHasJobProduct = $this->getModel('JobProductTable')->getTaskProductDatailsbyJobTaskID($jobTaskDetails['jobTaskID'], $jobId);

			if (count($checkHasJobProduct) == 0) {
				$updatedJobTasks[] = $val;
			}
		}
		
		if (!is_null($jobTasks)) {
			$saveServiceProducts = $this->saveJobRelatedServiceProducts($updatedJobTasks, $jobId, $vehicleTypeID);

			if (!$saveServiceProducts) {
				return false;
			}

		}
		return true;
	}

	/**
     * This function is used to get job by search key
     */
    public function getServiiceJobBySearchKey($data, $param, $locationID) {
        $paginated = false;
        $jobSearchKey = ($data['jobSearchKey'] == "") ? null: $data['jobSearchKey'];
        $jobCard = ($data['jobCard'] == "") ? null : $data['jobCard'];
        $serviceType = ($data['serviceType'] == "") ? null : $data['serviceType'];
        $toDate = ($data['toDate'] == "") ? null : $data['toDate'];
        $fromDate = ($data['fromDate'] == "") ? null : $data['fromDate'];

        if (!is_null($fromDate)) {
        	$fromDate = $this->getGMTDateTime('Y-m-d H:i:s', $fromDate)['data']['currentTime'];
        } 

        if (!is_null($toDate)) {
        	$toDate = $this->getGMTDateTime('Y-m-d H:i:s', $toDate)['data']['currentTime'];
        } 

        $status = $data['status'];
        $ServiceJobsController = $this->getServiceLocator()->get("ServiceJobsController");
        if ($jobSearchKey != null || $jobCard != null || $serviceType != null || $toDate != null) {
            $tbl = $this->getModel('JobTable');
            $jobList = $tbl->getJobBySearchKey($status, $jobSearchKey,$jobCard, $serviceType, $fromDate, $toDate);
        	$paginated = false;
        } else {
            $jobList = $ServiceJobsController->getPaginatedJob($this->locationID, $param, $status);
        	$paginated = true;
        }

        $view = new ViewModel(array(
            'jobList' => $jobList,
            'paginated' => $paginated,
            'status' => $this->getStatusesList(),
            'timeZone' => $this->timeZone
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-jobs/list-table.phtml');

        return $view;
    }

    /**
	 * this function use to save business type. this is a one time process.
	 * @param array $data
	 * return array
	 **/
	public function initiateBusinessType($data) {
		// save business type
		$displayData = [
			'jobBusinessType' => $data['id'],
			'jobWizardStatus' => 1,
		];
		$this->user_session->jobBusinessType = $data['id'];
		$saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);

		return $saveBusinessType;
	}

	public function getAllServiceVehicals($data) {
		$customerID =($data['customerID'] == "") ? null : $data['customerID'];
		$serviceVehicals = $this->getModel('ServiceVehicleTable')->fetchAll($customerID);
		$vehicalList = array();
		foreach ($serviceVehicals as $vehicle) {
			$vehicalList[] =['key'=>$vehicle['customerID'], 'value' => $vehicle['serviceVehicleRegNo']];
		}
		return $vehicalList;
	}


	public function getJobsByVehicleRegNo($data) 
	{
		$vehicleRegNo = $data['vehicleRegNo'];
        $result = $this->getModel('ServiceVehicleTable')->getJobsByVehicle($vehicleRegNo);

        $jobData = [];
        foreach ($result as $value) {
        	$jobData[] = $value;
        }

        $view = new ViewModel(array(
            'jobData' => $jobData,
        ));
        $view->setTerminal(true);
        $view->setTemplate('jobs/vehicle-history/job-list.phtml');


        return ['data' => $jobData, 'view' => $view];
	}


	public function getJobItemChangeSideView($data) 
	{
		$vehicleRegNo = $data['vehicleRegNo'];
		$item = $data['item'];
        $result = $this->getModel('ServiceVehicleTable')->getJobsByVehicleWithStatus($vehicleRegNo,$item);

        $jobData = [];
        foreach ($result as $value) {
        	$jobData[] = $value;
        }

        $view = new ViewModel(array(
            'jobData' => $jobData,
        ));
        $view->setTerminal(true);
        $view->setTemplate('jobs/vehicle-history/job-list-by-item.phtml');


        return ['data' => $jobData, 'view' => $view];
	}


	public function getJobSideViewByJobId($data) 
	{
		$jobId = $data['jobId'];

        $jobData = $this->getModel('JobTable')->getServiceJobById($jobId);
        $vehicleStatusChecks = $this->getModel('VehicleStatusCheckTable')->fetchAllByID();
        $jobVehicleStatus = $this->getModel('JobVehicleStatusTable')->getJobVehicleStatusByJobID($jobId)->current();
        $jobVehicleRemark = $this->getModel('JobVehicleRemarkTable')->getJobVehicleRemarksByJobVehicleID($jobData['jobVehicleID']);

        $jobVehicleRemarkData = [];
        foreach ($jobVehicleRemark as $value) {
            $jobVehicleRemarkData[] = $value;
        }


        $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobId);

        $list = [];
        foreach ($serviceList as $key => $val) {
        	$val['startedAt'] = ($val['startedAt'] != NULL) ? $this->getUserDateTime($val['startedAt']) : NULL;
        	$val['endedAt'] = ($val['endedAt'] != NULL) ? $this->getUserDateTime($val['endedAt']) : NULL;
        	$val['departmentStationList'] = $this->getDepartmentStationList($val['departmentID']);
        	$val['departmentEmployeeList'] = $this->getDepartmentEmployeeList($val['departmentID']);
        	$val['employeeNames'] = $this->getServiceEmployees($val['jobTaskID']);

        	//check job sub task
        	$res = $this->getModel('jobSubTaskTable')->getSubTaskByJobTaskId($val['jobTaskID']);
        	$val['hasSubTasks'] = (sizeof($res) > 0) ? true : false;

        	$list [] = $val;
        }

        $view = new ViewModel(array(
            'jobData' => $jobData,
            'serviceList' => $list,
            'status' => $this->getStatusesList(),
            'jobVehicleStatus' => $jobVehicleStatus,
            'vehicleStatusChecks' => $vehicleStatusChecks,
            'jobVehicleRemarkData' => $jobVehicleRemarkData,
        ));
        $view->setTerminal(true);
        $view->setTemplate('jobs/vehicle-history/job-side-view.phtml');

        return ['data' => $jobData, 'view' => $view];
	}

	public function searchVehiclesForDropdown($data)
	{
		$searchKey = $data['searchKey'];
		$customerID = $data['customerID'];
		$vehicleList = [];
		$vehicles = $this->getModel('ServiceVehicleTable')->searchVehiclesForDropDown($searchKey, $customerID);
        foreach ($vehicles as $vehicle) {
            $temp['value'] = $vehicle['serviceVehicleID'];
            $temp['text'] = $vehicle['serviceVehicleRegNo'];
            $vehicleList[] = $temp;
        }

        return array('list' => $vehicleList);
	}
}
