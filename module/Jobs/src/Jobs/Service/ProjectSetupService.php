<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Jobs\Model\ProjectType;

class ProjectSetupService extends BaseService
{
	/**
	* this function use to add and update project type.
	* @param array $data
	* return array
	**/
	public function saveProjectType($data, $userID)
	{
		$projectTypeCode = $data['projectTypeCode'];
        $projectTypeName = $data['projectTypeName'];
        $editMode = $data['editMode'];
        $tournamentFlag = $data['tournamentFlag'];
        //check wheter editMode is true or false. if it is true then update the project type otherwise save the project type
        if ($editMode == 'true') {
            $projectTypeID = $data['projectTypeID'];
            $oldData = $this->getModel('ProjectTypeTable')->getProjectTypeByProjectTypeID($projectTypeID)->current();

            $data = array(
                'projectTypeName' => $projectTypeName,
                'projectTypeID' => $projectTypeID,
            );

            $projectTypeData = new ProjectType();
            $projectTypeData->exchangeArray($data);
            $result = $this->getModel('ProjectTypeTable')->updateProjectType($projectTypeData);
            
            if ($tournamentFlag) {
                if ($result) {
                    $changeArray = $this->getTournamentTypeChageDetails($oldData, $data);
                    //set log details
                    $previousData = json_encode($changeArray['previousData']);
                    $newData = json_encode($changeArray['newData']);

                    $this->setLogDetailsArray($previousData,$newData);
                    $this->setLogMessage("Tournament type ".$oldData['projectTypeCode']." is updated.");
                    return $this->returnSuccess(array('projectTypeID' => $projectTypeID), 'SUCC_TOUR_TYPE_UPDATE');
                } else {
                    return $this->returnError('ERR_TOUR_TYPE_UPDATE', null);
                }
            } else {
                if ($result) {
                    $changeArray = $this->getProjectTypeChageDetails($oldData, $data);
                    //set log details
                    $previousData = json_encode($changeArray['previousData']);
                    $newData = json_encode($changeArray['newData']);

                    $this->setLogDetailsArray($previousData,$newData);
                    $this->setLogMessage("Project type ".$oldData['projectTypeCode']." is updated.");
                    return $this->returnSuccess(array('projectTypeID' => $projectTypeID), 'SUCC_PROTYPE_UPDATE');
                } else {
                    return $this->returnError('ERR_PROTYPE_UPDATE', null);
                }
            }

        } else {
            //check whether project type code alredy in database or not.
            $result = $this->getModel('ProjectTypeTable')->getProjectTypeByProjectTypeCode($projectTypeCode);
            if (!($result->count() > 0)) {
                $entityService = $this->getService('EntityService');
        		$entityID = $entityService->createEntity($userId)['data']['entityID'];
                $data = array(
                    'projectTypeCode' => $projectTypeCode,
                    'projectTypeName' => $projectTypeName,
                    'entityID' => $entityID
                );

                $projectTypeData = new ProjectType();
                $projectTypeData->exchangeArray($data);
                $projectTypeID = $this->getModel('ProjectTypeTable')->saveProjectType($projectTypeData);
                if ($tournamentFlag) {
                    if ($projectTypeID) {
                        $this->setLogMessage('Tournament type '.$projectTypeCode.' is created.');
                    	return $this->returnSuccess(array('projectTypeID' => $projectTypeID, 'projectTypeCode' => $projectTypeCode),'SUCC_TOUR_TYPE_SAVE');
                    } else {
                    	return $this->returnError('ERR_TOUR_TYPE_SAVE', null);
                    }
                } else {
                    if ($projectTypeID) {
                        $this->setLogMessage('Project type '.$projectTypeCode.' is created.');
                        return $this->returnSuccess(array('projectTypeID' => $projectTypeID),'SUCC_PROTYPE_SAVE');
                    } else {
                        return $this->returnError('ERR_PROTYPE_SAVE', null);
                    }
                }
            } else {
                if ($tournamentFlag) {
                   return $this->returnError('ERR_TOUR_TYPE_CODE_ALREDY_EXIXT','PTCERR');  
                } else {
            	   return $this->returnError('ERR_PROTYPE_CODE_ALREDY_EXIXT','PTCERR');
                }
            }
        } 	
	}

    public function getTournamentTypeChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Tournament type code'] = $row['projectTypeCode'];
        $previousData['Tournament type name'] = $row['projectTypeName'];

        $newData = [];
        $newData['Tournament type code'] = $row['projectTypeCode'];
        $newData['Tournament type name'] = $data['projectTypeName'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }

    public function getProjectTypeChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Project type code'] = $row['projectTypeCode'];
        $previousData['Project type name'] = $row['projectTypeName'];

        $newData = [];
        $newData['Project type code'] = $row['projectTypeCode'];
        $newData['Project type name'] = $data['projectTypeName'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }

	/**
	 * This function is used to get project type by search key
	 */
	public function getProjectTypesBySearchKey($data, $param)
	{
		$paginated = false;
        $projectTypeSearchKey = $data['projectTypeSearchKey'];

        if (!empty($projectTypeSearchKey)) {
            $tbl = $this->getModel('ProjectTypeTable');
            $projectTypeList = $tbl->getProjectTypeforSearch($projectTypeSearchKey);
        } else {
        	$projectSetup = $this->getServiceLocator()->get("ProjectSetupController");
        	$projectTypeList = $projectSetup->getPaginatedProjectTypes(6, $param);
            $paginated = true;
        }

        $view = new ViewModel(array(
            'projectTypeList' => $projectTypeList,
            'paginated' => $paginated
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/project-setup/list.phtml');

        return $view;
	}

	/**
	 * This function is used to update project type status
	 **/
	public function updateProjectTypeStatus($data)
	{
        $projectTypeID = $data['projectTypeID'];
        $status = $data['status'];
        $tournamentFlag = $data['tournamentFlag'];
        $updateState = $this->getModel('ProjectTypeTable')->updateProjectTypeState($projectTypeID, $status);
        $oldData = $this->getModel('ProjectTypeTable')->getProjectTypeByProjectTypeID($projectTypeID)->current();

        if ($tournamentFlag) {
            if ($updateState) {
                if ($status == 0) {
                    $this->setLogMessage("Status of tournament type ".$oldData['projectTypeCode']." is deactived");
                } else {
                    $this->setLogMessage("Status of tournament type ".$oldData['projectTypeCode']." is actived");
                }
            	return $this->returnSuccess(null, 'SUCC_TOUR_TYPE_STATUS_UPDATE');
            } else {
            	return $this->returnError('ERR_TOUR_TYPE_STATUS_UPDATE', null);
            }
        } else {
            if ($updateState) {
                return $this->returnSuccess(null, 'SUCC_PROTYPE_STATUS_UPDATE');
            } else {
                return $this->returnError('ERR_PROTYPE_STATUS_UPDATE', null);
            }
        }
	}

	/**
	 * This function is used to delete project type
	 **/
	public function deleteProjectTypeByProjectTypeID($data, $userId)
	{
		$projectTypeID = $data['projectTypeID'];
        $projectTypeCode = $data['projectTypeCode'];
        $tournamentFlag = $data['tournamentFlag'];
        $projectData = $this->getModel('ProjectTable')->getProjectDetailsByProjectTypeID($projectTypeID)->current();
        if (!$projectData) {
            if (!empty($projectTypeID)) {
                $result = $this->getModel('ProjectTypeTable')->getProjectTypeByProjectTypeID($projectTypeID)->current();
                $entityID = $result['entityID'];
                $entityService = $this->getService('EntityService');
        		$entityService->updateDeleteInfoEntity($entityID, $userId);

                if ($tournamentFlag) {
                    $this->setLogMessage('Tournament type '.$result['projectTypeCode'].' is deleted.');
                    return $this->returnSuccess(array($projectTypeCode),'SUCC_TOUR_TYPE_DELETE');
                } else {
                    return $this->returnSuccess(array($projectTypeCode),'SUCC_PROTYPE_DELETE');
                }
            } else {
                if ($tournamentFlag) {
            	    return $this->returnError('ERR_TOUR_TYPE_DELETE',null);
                } else {
                    return $this->returnError('ERR_PROTYPE_DELETE',null);
                }
            }
        } else {
        	return $this->returnError('','deactive');
        }
	}

    public function searchProjectTypesForDropdown($data)
    {

        $projectTypeSearchKey = $data['searchKey'];

        if (!empty($projectTypeSearchKey)) {
            $tbl = $this->getModel('ProjectTypeTable');
            $projectTypeList = $tbl->searchProjectTypesForDropDown($projectTypeSearchKey);
        } 
        $data = [];

        foreach ($projectTypeList as $projectType) {
            $temp['value'] = $projectType['projectTypeID'];
            $temp['text'] = $projectType['projectTypeName'] . '-' . $projectType['projectTypeCode'];
            $customerList[] = $temp;
        }

        $data = array('list' => $customerList);

        return $data;
    }
}