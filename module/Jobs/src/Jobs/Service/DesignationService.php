<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Designation;

class DesignationService extends BaseService
{
	/**
    * this function use to save designation..
    * @param array $data
    * return array
    **/
    public function createDesignation($data, $userId)
    {
        $designationName = $data['designationName'];
        $designationCode = $data['designationCode'];
        $tournamentFlag = $data['tournamentFlag'];
        $designationID = null;

        $result = $this->getModel('DesignationTable')->getDesignations($designationCode);

        if ($result->count() > 0) {
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_SUB_TYPE_CODE_ALREDY_EXIST', 'DESIGNATION_EXIST');
            } else {
                return $this->returnError('ERR_JOB_DESIG_CODE_ALREDY_EXIST', 'DESIGNATION_EXIST');
            }
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data = array(
            'designationCode' => $designationCode,
            'designationName' => $designationName,
            'designationID' => $designationID,
            'entityID' => $entityID,
        );

        $designationData = new Designation();
        $designationData->exchangeArray($data);
        
        $savedResult = $this->getModel('DesignationTable')->saveDesignations($designationData);


        if (!$savedResult) {
            if ($tournamentFlag) {
                return $this->returnError('ERR_SAVE_PART_SUB_TYPE', 'DESIGNATION_SAVE_FAILD');
            } else {
                return $this->returnError('ERR_SAVE_DESIGNATION', 'DESIGNATION_SAVE_FAILD');
            }
        } else {
            if ($tournamentFlag) {
                $this->setLogMessage('Participant sub type '.$designationCode.' is created.');
                return $this->returnSuccess('DESIGNATION_SAVE_SUCESSFULLY', 'SUCC_SAVE_PART_SUB_TYPE');
            } else {
                return $this->returnSuccess('DESIGNATION_SAVE_SUCESSFULLY', 'SUCC_SAVE_DESIGNATION');
            }
        }
    }

    /**
    * this function use to update designation.
    * @param array $data
    * return array
    **/
    public function updateDesignation($data, $userId)
    {
        $tournamentFlag = $data['tournamentFlag'];
        $checkCodeValid = $this->getModel('DesignationTable')->checkDesignationCodeValid($data['designationID'], $data['designationCode']);

        if ($checkCodeValid > 0) {
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_SUB_TYPE_CODE_ALREDY_EXIST', 'DESIGNATION_EXIST');
            } else {
                return $this->returnError('ERR_JOB_DESIG_CODE_ALREDY_EXIST', 'DESIGNATION_EXIST');
            }
        }

        $oldDesignationId = $data['designationID'];
        $entityService = $this->getService('EntityService');
        $deleteDesignation = $entityService->updateDeleteInfoEntity($data['entityID'], $userId); // delete old record
        $entityID = $entityService->createEntity($userId)['data']['entityID'];
        $data['entityID'] = $entityID;
        $oldData = $this->getModel('DesignationTable')->getDesignationByDesignationID($oldDesignationId)->current();

        $designationData = new Designation();
        $designationData->exchangeArray($data);
        $savedResult = $this->getModel('DesignationTable')->saveDesignations($designationData);

        if ($savedResult) {
            //update employee designation table
            $updataEmployeeDesignation = $this->getModel('EmployeeDesignationTable')->updateEmployeDesignationWithNewId($oldDesignationId, $savedResult);

            if (!$updataEmployeeDesignation) {
                if ($tournamentFlag) {
                    return $this->returnError('ERR_PART_SUB_TYPE_UPDATE', 'DESIGNATION_FAILD_UPDATE');
                } else {
                    return $this->returnError('ERR_JOB_DESIG_UPDATE', 'DESIGNATION_FAILD_UPDATE');
                }
            }
            if ($tournamentFlag) {
                $changeArray = $this->getParticipantSubTypeChageDetails($oldData, $data);
                //set log details
                $previousData = json_encode($changeArray['previousData']);
                $newData = json_encode($changeArray['newData']);

                $this->setLogDetailsArray($previousData,$newData);
                $this->setLogMessage("Participant sub type ".$oldData['designationCode']." is updated.");
                return $this->returnSuccess('DESIGNATION_SUCESSFULLY_UPDATE', 'SUCC_PART_SUB_TYPE_UPDATE');
            } else {
                return $this->returnSuccess('DESIGNATION_SUCESSFULLY_UPDATE', 'SUCC_JOB_DESIG_UPDATE');
            }
        } else {
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_SUB_TYPE_UPDATE', 'DESIGNATION_FAILD_UPDATE');
            } else {
                return $this->returnError('ERR_JOB_DESIG_UPDATE', 'DESIGNATION_FAILD_UPDATE');
            }
        }
    }

    public function getParticipantSubTypeChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Participant sub type code'] = $row['designationCode'];
        $previousData['Participant sub type name'] = $row['designationName'];

        $newData = [];
        $newData['Participant sub type code'] = $data['designationCode'];
        $newData['Participant sub type name'] = $data['designationName'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }

    /**
    * this function use to update designation state.
    * @param int $designationID
    * @param string $status
    * return array
    **/
    public function updateDesignationState($data)
    {
        $designationID = $data['designationID'];
        $status = $data['status'];
        $tournamentFlag = $data['tournamentFlag'];
        $oldData = $this->getModel('DesignationTable')->getDesignationByDesignationID($designationID)->current();
        $result = $this->getModel('DesignationTable')->updateDesignationState($designationID, $status);
        if ($result) {
            if ($tournamentFlag) {
                if ($status == 0) {
                    $this->setLogMessage("Status of participant sub type ".$oldData['designationCode']." is deactived");
                } else {
                    $this->setLogMessage("Status of participant sub type ".$oldData['designationCode']." is actived");
                }
                return $this->returnSuccess('DESIGNATION_STATE_SUCESSFULLY_UPDATE', 'SUCC_PART_SUB_TYPE_STATUS_UPDATE');
            } else {
                return $this->returnSuccess('DESIGNATION_STATE_SUCESSFULLY_UPDATE', 'SUCC_DES_STATUS_UPDATE');
            }
        } else {
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_SUB_TYPE_STATE_UPDATE', 'DESIGNATION_STATE_FAILD_UPDATE');
            } else {
                return $this->returnError('ERR_DES_STATE_UPDATE', 'DESIGNATION_STATE_FAILD_UPDATE');
            }
        }

    }

    /**
    * this function use to search designation by key.
    * @param string $designationSearchKey
    * @param int $param
    * return array
    **/
    public function designationSearchByKey($designationSearchKey, $param)
    {
        $paginated = false;
        if (!empty($designationSearchKey)) {
            $DesignationList = $this->getModel('DesignationTable')->designationSearchByKey($designationSearchKey);
            $status = true;
        } else {
            $status = true;
            $designationController = $this->getServiceLocator()->get("DesignationController");
            $DesignationList = $designationController->getPaginatedDesignations(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'designations' => $DesignationList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/designations/list.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
     * This function is used to delete designation
     **/
    public function deleteDesignation($data, $userId)
    {
        $designationID = $data['designationID'];
        $tournamentFlag = $data['tournamentFlag'];
        if (!empty($designationID)) {

            $checkDesignationUsed = $this->getModel('EmployeeDesignationTable')->checkDesignationUsed($designationID);
            $oldData = $this->getModel('DesignationTable')->getDesignationByDesignationID($designationID)->current();
            if ($checkDesignationUsed) {
                $entityService = $this->getService('EntityService');
                $result = $entityService->updateDeleteInfoEntity($data['entityID'], $userId);
                if ($result) {
                    if ($tournamentFlag) {
                        $this->setLogMessage('Participant sub type '.$oldData['designationCode'].' is deleted.');
                        return $this->returnSuccess(array($taskCode),'SUCC_PART_SUB_TYPE_DELETE');
                    } else {
                        return $this->returnSuccess(array($taskCode),'SUCC_JOB_DESIG_DELETE');
                    }
                } else {
                    if ($tournamentFlag) {
                        return $this->returnError('ERR_PART_SUB_TYPE_DELETE',null);
                    } else {
                        return $this->returnError('ERR_JOB_DESIG_DELETE',null);
                    }
                }
            } else {
                if ($tournamentFlag) {
                    return $this->returnError('ERR_PART_SUB_TYPE__ALREADY_USED',null);
                } else {
                    return $this->returnError('ERR_JOB_DESIG_ALREADY_USED',null);
                }
            }
           
        } else {
            if ($tournamentFlag) {
                return $this->returnError('ERR_PART_SUB_TYPE_DELETE',null);
            } else {
                return $this->returnError('ERR_JOB_DESIG_DELETE',null);
            }
        }
    }

}