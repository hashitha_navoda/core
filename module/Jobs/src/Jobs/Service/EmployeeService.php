<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Jobs\Model\Employee;
use Jobs\Model\EmployeeDesignation;
use Jobs\Model\EmployeeDepartment;
use Zend\View\Model\ViewModel;

class EmployeeService extends BaseService {
	
	/**
    * this function use to save employee.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function createEmployee($data, $userId, $type = 'constructEmp')
    {
        
        $employeeCode = $data['employeeCode'];
        $employeeFirstName = $data['employeeFirstName'];
        $employeeSecondName = $data['employeeSecondName'];
        $employeeAddress = $data['employeeAddress'];
        $employeeTP = $data['employeeTP'];
        $employeeEmail = $data['employeeEmail'];
        $employeeIDNo = $data['employeeIDNo'];
        $employeeDepartmentID = $data['employeeDepartmentID'];
        $employeeDesignations = $data['employeeDesignations'];
        $employeeDepartments = $data['employeeDepartments'];
        $employeeID = null;

        $result = $this->getModel('EmployeeTable')->getEmployeeByCode($employeeCode);
        $employeeIDNoDataset = $this->getModel('EmployeeTable')->checkEmployeeIDNovalid($employeeIDNo);

        if ($result->count() > 0) { 
            return $this->returnError('ERR_EMP_CODE_ALREDY_EXIST', 'EMPLOYEE_EXIST');
        }
        if ($employeeIDNoDataset->count() > 0) { 
            return $this->returnError('ERR_EMP_ID_NO_ALREDY_EXIST', 'EMPLOYEE_ID_NO_EXIST');
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data = array(
            'employeeID' => $employeeID,
            'employeeCode' => $employeeCode,
            'employeeFirstName' => $employeeFirstName,
            'employeeSecondName' => $employeeSecondName,
            'employeeAddress' => $employeeAddress,
            'employeeTP' => $employeeTP,
            'employeeEmail' => $employeeEmail,
            'employeeIDNo' => $employeeIDNo,
            'departmentID' => $employeeDepartmentID,
            'entityID' => $entityID
        );


        $employeeData = new Employee();
        $employeeData->exchangeArray($data);
        
        $savedResult = $this->getModel('EmployeeTable')->saveEmployees($employeeData);

        if (!$savedResult) {
            return $this->returnError('ERR_EMP_SAVE', 'EMPLOYEE_SAVE_FAILD');
        } else {

            foreach ($employeeDesignations as $key => $value) {
                $employeeDesignationID = null;
                $data = [
                    'employeeDesignationID' => $employeeDesignationID,
                    'employeeID' => $savedResult,
                    'designationID' => $value
                ];

                $employeeDesignationData = new EmployeeDesignation();
                $employeeDesignationData->exchangeArray($data);

                $saveEmployeeDesignations = $this->getModel('EmployeeDesignationTable')->saveEmployeeDesignation($employeeDesignationData);
            
                if (!$saveEmployeeDesignations) {
                    return $this->returnError('ERR_EMP_SAVE', 'EMPLOYEE_SAVE_FAILD');
                }
            }


            if ($type == 'serviceEmp') {
                foreach ($employeeDepartments as $key => $value) {
                    $employeeDepartmentID = null;
                    $empDeptdata = [
                        'employeeDepartmentID' => $employeeDepartmentID,
                        'employeeId' => $savedResult,
                        'departmentId' => $value
                    ];
                    $employeeDepartmentData = new EmployeeDepartment();
                    $employeeDepartmentData->exchangeArray($empDeptdata);


                    $saveEmployeeDepartments = $this->getModel('EmployeeDepartmentTable')->saveEmployeeDepartment($employeeDepartmentData);
                    
                    if (!$saveEmployeeDepartments) {
                        return $this->returnError('ERR_EMP_SAVE', 'EMPLOYEE_SAVE_FAILD');
                    }
                }
            }

            
            return $this->returnSuccess('EMPLOYEE_SAVE_SUCESSFULLY', 'SUCC_EMP_SAVE');
        }
    }

    /**
    * this function use to update employee.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function updateEmployee($data, $userId)
    {
        $checkCodeValid = $this->getModel('EmployeeTable')->checkEmployeeCodeValid($data['employeeID'], $data['employeeCode']);
        $employeeDesignations = $data['employeeDesignations'];
        $employeeDepartments = $data['employeeDepartments'];
        $employeeIDNo = $data['employeeIDNo'];


        $employeeIDNoDataset = $this->getModel('EmployeeTable')->checkEmployeeIDNovalid($employeeIDNo, $data['employeeID'], true);
        
        if ($checkCodeValid > 0) {
            return $this->returnError('ERR_EMP_CODE_ALREDY_EXIST', 'EMPLOYEE_EXIST');
        }

        if ($employeeIDNoDataset->count() > 0) { 
            return $this->returnError('ERR_EMP_ID_NO_ALREDY_EXIST', 'EMPLOYEE_ID_NO_EXIST');
        }

        $getInactiveEmployeeDesignations = $this->getModel('EmployeeDesignationTable')->getInactiveEmployeeDesignations($data['employeeID']);

        $getInactiveEmployeeDepartments = $this->getModel('EmployeeDepartmentTable')->getInactiveEmployeeDepartments($data['employeeID']);

        $oldEmpId = $data['employeeID'];
        $entityService = $this->getService('EntityService');
        $deleteRecord = $entityService->updateDeleteInfoEntity($data['entityID'], $userId);
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data['entityID'] = $entityID;

        $employeeData = new Employee();
        $employeeData->exchangeArray($data);

        $savedResult = $this->getModel('EmployeeTable')->saveEmployees($employeeData);

        if (!$savedResult) {
            return $this->returnError('ERR_EMP_UPDATE', 'EMPLOYEEE_FAILD_UPDATE');
        } else {

            $updateProjectManagersTable = $this->getModel('ProjectManagerTable')->updateProjectManagersTableWithNewEmpId($data['employeeID'], $savedResult);
            $updateProjectSupervisorTable = $this->getModel('ProjectSupervisorTable')->updateProjectSupervisorsTableWithNewEmpId($data['employeeID'], $savedResult);

            foreach ($getInactiveEmployeeDesignations as $key => $value) {
                array_push($employeeDesignations, $value['designationID']);
            }
    
            foreach ($employeeDesignations as $key => $value) {
                $employeeDesignationID = null;
                $data = [
                    'employeeDesignationID' => $employeeDesignationID,
                    'employeeID' => $savedResult,
                    'designationID' => $value
                ];

                $employeeDesignationData = new EmployeeDesignation();
                $employeeDesignationData->exchangeArray($data);

                $saveEmployeeDesignations = $this->getModel('EmployeeDesignationTable')->saveEmployeeDesignation($employeeDesignationData);
            
                if (!$saveEmployeeDesignations) {
                    return $this->returnError('ERR_EMP_SAVE', 'EMPLOYEE_SAVE_FAILD');
                }
            }

            foreach ($getInactiveEmployeeDepartments as $key => $value) {
                array_push($employeeDepartments, $value['departmentID']);
            }
            
            foreach ($employeeDepartments as $key => $value) { 
                $countOfEmpDeptByIds = $this->getModel('EmployeeDepartmentTable')->getEmpDeptByIds($value, $oldEmpId);

                if (count($countOfEmpDeptByIds) > 0) {
                    $updateEmpDeptRecord = $this->getModel('EmployeeDepartmentTable')->updateEmpRecordWithNewEmpID($value, $oldEmpId, $savedResult);

                    if (!$updateEmpDeptRecord) {
                        return $this->returnError('ERR_EMP_SAVE', 'EMPLOYEE_SAVE_FAILD');
                    }
                } else {
                    $employeeDepartmentID = null;
                    $empDeptdata = [
                        'employeeDepartmentID' => $employeeDepartmentID,
                        'employeeId' => $savedResult,
                        'departmentId' => $value
                    ];
                    $employeeDepartmentData = new EmployeeDepartment();
                    $employeeDepartmentData->exchangeArray($empDeptdata);


                    $saveEmployeeDepartments = $this->getModel('EmployeeDepartmentTable')->saveEmployeeDepartment($employeeDepartmentData);
                    
                    if (!$saveEmployeeDepartments) {
                        return $this->returnError('ERR_EMP_SAVE', 'EMPLOYEE_SAVE_FAILD');
                    }
                }
            }


            //jobTaskEmployeeTable
            $updateJobTaskEmployee = $this->getModel('JobTaskEmployeeTable')->updateJobTaskEmployeeWithNewEmpID($savedResult, $oldEmpId);

            if (!$updateJobTaskEmployee) {
                return $this->returnError('ERR_EMP_SAVE', 'EMPLOYEE_SAVE_FAILD');
            }
            
            return $this->returnSuccess('EMPLOYEE_SUCESSFULLY_UPDATE', 'SUCC_EMP_UPDATE');
        }
    }

    /**
    * this function use to update employee state.
    * @param array $data
    * return array
    **/
    public function updateEmployeeState($data)
    {
        $employeeID = $data['employeeID'];
        $status = $data['status'];
        $result = $this->getModel('EmployeeTable')->updateEmployeeState($employeeID, $status);

        if ($result) {
            return $this->returnSuccess('EMPLOYEE_STATE_SUCESSFULLY_UPDATE', 'SUCC_EMP_STATUS_UPDATE');
        } else {
            return $this->returnError('ERR_EMP_STATE_UPDATE', 'EMPLOYEE_STATE_FAILD_UPDATE');
        }

    }

    /**
    * this function use to search employee by key, Department, Designation
    * @param array $data
    * @param int $param
    * return array
    **/
    public function employeeSearchByKey($data, $param, $type = 'constructEmp')
    {
        $paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];
        $designationID = ($data['searchDesignationID'] == '') ? null : $data['searchDesignationID'];
        $departmentID = ($data['searchDepartmentID'] == '') ? null : $data['searchDepartmentID'];

        if (!empty($key) || !empty($designationID) || !empty($departmentID)) {
            $EmployeeList = $this->getModel('EmployeeTable')->employeeSearchByKey($key, $designationID, $departmentID, false, false, $type);
            $status = true;
        } else {
            $status = true;
            $employeeController = $this->getServiceLocator()->get("EmployeeController");
            $EmployeeList = $employeeController->getPaginatedEmployees(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'employees' => $EmployeeList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        if ($type == 'constructEmp') {
            $view->setTemplate('jobs/employees/list.phtml');
        } else {
            $view->setTemplate('jobs/service-employee/list.phtml');
        }
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
    * this function use to get designation details according to the employeeID
    * @param int $id
    * return array
    **/
    public function getEmployeeDesignations ($id) {
        $employeeDesignations = $this->getModel('EmployeeDesignationTable')->getEmployeeDesignations($id);
        $data = [];

        foreach ($employeeDesignations as $key => $value) {
            array_push($data, $value);
        }

        $view = new ViewModel(array(
            'designations' => $data,
            'paginated' => false,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/employees/employee-designations-view.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data' => $data
        ];
    }

    /**
     * This function is used to delete employee
     **/
    public function deleteEmployee($data, $userId)
    {
        $employeeID = $data['employeeID'];
        if (!empty($employeeID)) {

            $checkEmployeeUsedInTeam = $this->getModel('EmployeeTeamTable')->checkEmployeeUsed($employeeID);
            $checkEmployeeUsedInProjectManager = $this->getModel('ProjectManagerTable')->checkEmployeeUsed($employeeID);
            $checkEmployeeUsedInProjectSupervisor = $this->getModel('ProjectSupervisorTable')->checkEmployeeUsed($employeeID);

            if ($checkEmployeeUsedInTeam && $checkEmployeeUsedInProjectManager && $checkEmployeeUsedInProjectSupervisor) {
                $entityService = $this->getService('EntityService');
                $result = $entityService->updateDeleteInfoEntity($data['entityID'], $userId);
                if ($result) {
                    return $this->returnSuccess(array($taskCode),'SUCC_EMP_DELETE');
                } else {
                    return $this->returnError('ERR_EMP_DELETE',null);
                }
            } else {
                return $this->returnError('ERR_EMP_ALREADY_USED',null);
            }
           
        } else {
            return $this->returnError('ERR_EMP_DELETE',null);
        }
    }

    /**
	 * This function is used to search designation wise for dropdown
	 */
	public function searchJobDesignationWiseEmployeeForDropdown($data) {
		$searchKey = $data['searchKey'];
		$employees = $this->getModel('EmployeeDesignationTable')->getDesignationWiseEmployeeBySearchKey($searchKey);
		$employeeList = array();
		foreach ($employees as $emp) {
			$temp['value'] = $emp['employeeDesignationID'];
			$temp['text'] = $emp['employeeFirstName'] . ' ' . $emp['employeeSecondName'] . ' (' . $emp['designationName'] . ')';
			$employeeList[] = $temp;
		}
		return $employeeList;
	}

    /**
     * This function is used to search designation wise for dropdown
     */
    public function searchEmployeeForDropdown($data) {
        $searchKey = $data['searchKey'];

        $employee = $this->getModel('EmployeeTable')->searchEmployeeForDropdown($searchKey);
        $employeeList = array();
        foreach ($employee as $employee) {
            $temp['value'] = $employee['employeeID'];
            $temp['text'] = $employee['employeeFirstName'] . ' ' . $employee['employeeSecondName'];
            $employeeList[] = $temp;
        }

        return $employeeList;
    }

	public function getEmployeeDetailsByEmployeeID($data) {
		$superviosrDetails = $this->getModel('EmployeeTable')->getEmployeeByID($data['employeeID']);

		return $superviosrDetails;
	}

    /**
     * This function is used to search job supervisors for dropdown
     */
    public function searchJobEmployeeForDropdown($data, $superviosrFlag, $managerFlag) {
        $searchKey = $data['searchKey'];
        $superviosrs = $this->getModel('EmployeeTable')->employeeSearchByKey($searchKey, null, null, $superviosrFlag, $managerFlag);
        $supervisorList = array();
        foreach ($superviosrs as $sup) {
            $temp['value'] = $sup['employeeID'];
            $temp['text'] = $sup['employeeFirstName'] . ' ' . $sup['employeeSecondName'];
            $supervisorList[] = $temp;
        }
        return $supervisorList;
    }

    /**
    * this function use to get department details according to the employeeID
    * @param int $id
    * return array
    **/
    public function getEmployeeDepartments ($id) {
        $employeeDepartments = $this->getModel('EmployeeDepartmentTable')->getEmployeeDepartments($id);
        $data = [];

        foreach ($employeeDepartments as $key => $value) {
            array_push($data, $value);
        }

        $view = new ViewModel(array(
            'departments' => $data,
            'paginated' => false,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-employee/employee-department-view.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data' => $data
        ];
    }

    /**
     * This function is used to Employee related department
     **/
    public function changeStatusOfEmployeeeDepartment($data)
    {
        $employeeDeaprtmentID = ($data['employeeDeaprtmentID']) ? $data['employeeDeaprtmentID']: null;
        $status = $data['status'];
        $result = $this->getModel('EmployeeDepartmentTable')->changeStatusOfEmployeeeDepartment($employeeDeaprtmentID, $status);

        if ($result) {
            return ($type == 'service') ? $this->returnSuccess(null,'SUCC_SER_STATUS_CHANGE') : $this->returnSuccess(null,'SUCC_EMP_DEP_STATUS_CHANGE');
        } else {
            return ($type == 'service') ? $this->returnError('ERR_SER_STATUS_CHANGE',null) : $this->returnError('ERR_EMP_DEP_STATUS_CHANGE',null);
        }
            
    }

}