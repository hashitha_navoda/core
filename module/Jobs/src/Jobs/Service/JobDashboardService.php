<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Team;
use Jobs\Model\EmployeeTeam;
use Jobs\Model\JobTaskEmployee;
use Jobs\Model\JobVehicleStatus;
use Jobs\Model\JobVehicleRemark;
use Jobs\Model\JobTempMaterial;
use Jobs\Model\JobTaskEmployeeWorkTime;

class JobDashboardService extends BaseService
{
	/**
    * this function use to start job service
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function startJobRelatedServiceByJobTaskID($data, $userId, $locationID)
    {
        $gmtDate = $this->getGMTDateTime('Y-m-d H:i:s');
        $currentDateTime = $gmtDate['data']['currentTime'];
        $jobTaskID = $data['jobTaskId'];
        $jobID = $data['jobId'];
        $departmentStationID = ($data['departmentStationID']) ? $data['departmentStationID'] : null;
        $empArray = ($data['empArray']) ? $data['empArray'] : null;
        $changeFlag = $data['changeFlag'];

        $jData = $this->getModel('JobTable')->getJobByID($jobID)->current();
        $entityID = $jData['entityID'];

        $oldJobTask = $this->getModel('JobTaskTable')->getTaskDetailsByJobTaskIdForService($jobTaskID);
        //var_dump($oldJobTask['jobTaskStatus']).die();
        if (!$oldJobTask) {
            return $this->returnError('ERR_RETRIVE_JOB_TASK');
        }

        if($oldJobTask["jobTaskStatus"] == '8' || $oldJobTask["jobTaskStatus"] == '20' || $oldJobTask["jobTaskStatus"] == '18'){
			return $this->returnError('ERR_JOB_ALREADY_STARED');
		}
 
        $status = $this->getStatusIDByStatusName('in progress');
        $updateJobTaskState = $this->getModel('JobTaskTable')->updateJobTaskStatusByJobTaskID($jobTaskID, $status);

        if (!$updateJobTaskState) {
            return $this->returnError('ERR_UPDATE_JOB_TASK_STATE');
        }

        $updateJobProductStatus = $this->getModel('JobProductTable')->updateJobProductStatusByJobTaskID($jobTaskID, $status);
        
        if (!$updateJobProductStatus) {
            return $this->returnError('ERR_UPDATE_JOB_PRO_STATE');
        }

        if ($oldJobTask['jobTaskStatus'] == 3 && $oldJobTask['restarted'] != true) {
            $updateJobTaskStartTime = $this->getModel('JobTaskTable')->updateJobTaskStartTimeByJobTaskID($jobTaskID, $currentDateTime);

            if (!$updateJobTaskStartTime) {
                return $this->returnError('ERR_UPDATE_JOB_TASK_START_TIME');
            }

            if(!is_null($departmentStationID)) {
                $updateJobTaskStationId = $this->getModel('JobTaskTable')->updateJobTaskStationId($jobTaskID, $departmentStationID);
            }

            if (!is_null($empArray)) {
                foreach ($empArray as $key => $value) {
                    $createJobTaskEmployee = $this->createJobTaskEmployee($value, $oldJobTask['taskID'], $jobTaskID, $currentDateTime);

                    if (!$createJobTaskEmployee) {
                        return $this->returnError('ERR_JOB_TSK_EMP_CREATE');
                    }
                }   
            } else {
                $createJobTaskEmployee = $this->createJobTaskEmployee(null, $oldJobTask['taskID'], $jobTaskID, $currentDateTime);

                if (!$createJobTaskEmployee) {
                    return $this->returnError('ERR_JOB_TSK_EMP_CREATE');
                }
            }

            if ($data['issueMaterialFlag'] == "true") {
                $issueMaterials = $this->issueMaterialAtTaskStart($jobTaskID, $jobID);
                if (!$issueMaterials['status']) {
                    return $this->returnError($issueMaterials['msg']);
                } else {
                    // update job Task table 
                    $updatedDataSet = [
                        'issueMaterialFlag' => 1
                    ];

                    // update
                    $updateJobTaskRes = $this->getModel('JobTaskTable')->updateJobTask($updatedDataSet, $jobTaskID);

                    if (!$updateJobTaskRes) {
                        return $this->returnError('ERR_UPDATE_JOB_TASK');
                    }
                }
            }
        } else {
            if ($oldJobTask['jobTaskStatus'] != '17') {
                $updateJobTaskEmployee = $this->updateJobTaskEmployee($empArray, $jobTaskID, $oldJobTask['taskID'], $changeFlag);

                if (!$updateJobTaskEmployee) {
                    return $this->returnError('ERR_JOB_SRVCE_RESUME', 'JOB_TASK_EMPLOYEE_UPDATE_FAILD');
                }

                if(!is_null($departmentStationID)) {
                    $updateJobTaskStationId = $this->getModel('JobTaskTable')->updateJobTaskStationId($jobTaskID, $departmentStationID);
                }
            }

            if ($data['issueMaterialFlag'] == "true") {
                $issueMaterials = $this->issueMaterialAtTaskStart($jobTaskID, $jobID);
                if (!$issueMaterials['status']) {
                    return $this->returnError($issueMaterials['msg']);
                } else {
                    // update job Task table 
                    $updatedDataSet = [
                        'issueMaterialFlag' => 1
                    ];

                    // update
                    $updateJobTaskRes = $this->getModel('JobTaskTable')->updateJobTask($updatedDataSet, $jobTaskID);

                    if (!$updateJobTaskRes) {
                        return $this->returnError('ERR_UPDATE_JOB_TASK');
                    }
                }
            }
        }
        $updateJobTaskEmployeeIncentive = $this->updateJobTaskEmployeeIncentive($jobTaskID, $oldJobTask['taskID'], $oldJobTask['vehicleType']);

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->updateEntity($entityID, $userId);

        $updateJobState = $this->getModel('JobTable')->updateJobStatus($jobID, $status);
        if (!$updateJobState) {
            return $this->returnError('ERR_JOB_STATE_UPDATE');
        }

        $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobID);

        if (!$serviceList) {
            return $this->returnError('ERR_RETRIVE_SERVICES');    
        }

        $list = [];
        foreach ($serviceList as $key => $value) {
           $list [] = $value;
        }

        if (!isset($data['isFromDashboard'])) {
            return $this->returnSuccess($list, 'SUCC_SRVCE_START');
        }


        $data  = $this->getUpdatedDashboardJobList($locationID);
        $data['data']['serviceList'] = $list;
        
        return $data;
    }

    public function issueMaterialAtTaskStart($jobTaskID, $jobID)
    {
        $jobProductDetails = $this->getModel('JobProductTable')->getJobProductByjobTaskID($jobTaskID);
        $productData = [];
        $productDataErr = [];
        foreach ($jobProductDetails as $value) {
            $temp = [];
            if (floatval($value['jobProductAllocatedQty']) <= floatval($value['locationProductQuantity'])) {
                $temp['jobProductId'] = $value['jobProductID']; 
                $temp['issueQty'] = $value['jobProductAllocatedQty']; 
                $productData[] = $temp;          
            } else {
                if (floatval($value['locationProductQuantity']) != 0) {
                    $temp['jobProductId'] = $value['jobProductID']; 
                    $temp['issueQty'] = $value['locationProductQuantity']; 
                    $productData[] = $temp;
                } else {
                    $productDataErr[] = $value['jobProductID'];
                }
            }
        }

        if (sizeof($productDataErr) > 0) {
            return $this->returnError('SERVICE_MATERIAL_QTY', null);
        }
        if (sizeof($productData) > 0) {
            $materialRequisitionService = $this->getService('MaterialRequisitionService');
            $update = $materialRequisitionService->updateIssueQuantity($productData, $jobID);
            if ($update['status']) {
                return $this->returnSuccess(null, $update['msg']);
            } else {
                return $this->returnError($update['msg'], null);
            }
        } else {
            return $this->returnSuccess(null, "SUCCESS");
        }
    }


    /**
    * this function use to hold job service
    * @param array $data
    * return array
    **/
    public function holdJobRelatedServiceByJobTaskID($data)
    {
        $jobTaskID = $data['jobTaskId'];
        $jobID = $data['jobId'];
        $gmtDate = $this->getGMTDateTime('Y-m-d H:i:s');
        $currentDateTime = $gmtDate['data']['currentTime'];
 
        $status = $this->getStatusIDByStatusName('hold');
        $updateJobTaskState = $this->getModel('JobTaskTable')->updateJobTaskStatusByJobTaskID($jobTaskID, $status);

        if ($updateJobTaskState) {
            $checkJobStatusCanChange = $this->checkJobCanUpdateTheState($jobID, $status);
            
            if ($checkJobStatusCanChange) {
                $updateJobState = $this->getModel('JobTable')->updateJobStatus($jobID, $status);
                if (!$updateJobState) {
                    return $this->returnError('ERR_JOB_STATE_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
                }
            }

            $getJobTaskRelatedEmployees = $this->getModel('JobTaskEmployeeTable')->getJobTaskRelatedEmployees($jobTaskID);

            foreach ($getJobTaskRelatedEmployees as $key => $value) {
                $getJobTaskRelatedEmployees = $this->getModel('JobTaskEmployeeWorkTimeTable')->updateEndDateTime($value['jobTaskEmployeeWorkTimeId'], $currentDateTime);
                if (!$getJobTaskRelatedEmployees) {
                    return $this->returnError('ERR_UPDATE_JOB_TASK_STATE', 'JOB_TASK_EMPLOYEE_WORK_END_TIME_FAILD_UPDATE');
                }
            }

            $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobID);
            if (!$serviceList) {
                return $this->returnError('ERR_RETRIVE_SERVICES');    
            }

            $list = [];

            foreach ($serviceList as $key => $value) {
               $list [] = $value;
            }

            return $this->returnSuccess($list, 'SUCC_SRVC_HOLD');
        } else {
            return $this->returnError('ERR_UPDATE_JOB_TASK_STATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        }
    }


    /**
    * this function use to stop job service
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function stopJobRelatedServiceByJobTaskID($data, $userId, $locationID =null)
    {
        $jobTaskID = $data['jobTaskId'];
        $jobID = $data['jobId'];
        $gmtDate = $this->getGMTDateTime('Y-m-d H:i:s');
        $currentDateTime = $gmtDate['data']['currentTime'];

        $jData = $this->getModel('JobTable')->getJobByID($jobID)->current();
        $entityID = $jData['entityID'];
 
        $status = $this->getStatusIDByStatusName('Ended');
        $updateJobTaskState = $this->getModel('JobTaskTable')->updateJobTaskStatusByJobTaskID($jobTaskID, $status);

        if ($updateJobTaskState) {

            $updateJobTaskEndTime = $this->getModel('JobTaskTable')->updateJobTaskEndTimeByJobTaskID($jobTaskID, $currentDateTime);

            if (!$updateJobTaskEndTime) {
                return $this->returnError('ERR_UPDATE_JOB_TASK_END_TIME');
            }

            $getJobTaskRelatedEmployees = $this->getModel('JobTaskEmployeeTable')->getJobTaskRelatedEmployees($jobTaskID);

            foreach ($getJobTaskRelatedEmployees as $key => $value) {
                $getJobTaskRelatedEmployees = $this->getModel('JobTaskEmployeeWorkTimeTable')->updateEndDateTime($value['jobTaskEmployeeWorkTimeId'], $currentDateTime);
                if (!$getJobTaskRelatedEmployees) {
                    return $this->returnError('ERR_UPDATE_JOB_TASK_STATE', 'JOB_TASK_EMPLOYEE_WORK_END_TIME_FAILD_UPDATE');
                }
            }

            $jobTaskData = $this->getModel('JobTaskTable')->getTaskDetailsByJobTaskIdForService($jobTaskID);
            if ($jobTaskData['restarted']) {

                $updateJobTaskIsRestarted = $this->getModel('JobTaskTable')->updateJobTaskIsRestarted($jobTaskID, false);

                if (!$updateJobTaskIsRestarted) {
                    return $this->returnError('ERR_JOB_SRVCE_END', 'ERROR_OCCURED_WHILE_UPDATE_JOB_TASK_RESTARTED_FLAG');
                }

                $checkJobCanRemoveRestarted = $this->checkJobCanRemoveRestarted($jobID);

                if ($checkJobCanRemoveRestarted) {
                    $updateJobIsRestarted = $this->getModel('JobTable')->updateJobIsRestarted($jobID, false, null);

                    if (!$updateJobIsRestarted) {
                        return $this->returnError('ERR_JOB_SRVCE_END', 'ERROR_OCCURED_WHILE_UPDATE_JOB_TASK_RESTARTED_FLAG');
                    }
                }

            }

            $checkJobStatusCanChange = $this->checkJobCanUpdateTheState($jobID, $status);
            
            if ($checkJobStatusCanChange) {
                $status = $this->getStatusIDByStatusName('Completed');
                $jobData = $this->getModel('JobTable')->getJobByJobID($jobID)->current();

                $entityService = $this->getService('EntityService');
                $entityID = $entityService->updateEntity($entityID, $userId);

                if ($jobData['isRestarted']) {
                    $updateJobState = $this->getModel('JobTable')->updateJobStatus($jobID, $jobData['old_status']);
                } else {
                    $updateJobState = $this->getModel('JobTable')->updateJobStatus($jobID, $status);
                }
                if (!$updateJobState) {
                    return $this->returnError('ERR_JOB_STATE_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
                }
                $updateJobCompleted = $this->getModel('JobTable')->updateJobCompletedTimeByJobID($jobID, $currentDateTime);

                if (!$updateJobCompleted) {
                    return $this->returnError('ERR_UPDATE_JOB_TASK_STATE', 'JOB_COMPLETED_TIME_STAMP_FAILD_UPDATE');

                }
            }

            $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobID);
            if (!$serviceList) {
                return $this->returnError('ERR_RETRIVE_SERVICES');    
            }

            $list = [];

            foreach ($serviceList as $key => $value) {
               $list [] = $value;
            }


            if (!isset($data['isFromDashboard'])) {
                return $this->returnSuccess($list, 'SUCC_SRVC_END');
            }

            $data  = $this->getUpdatedDashboardJobList($locationID);
            $data['data']['serviceList'] = $list;

            return $data;
        } else {
            return $this->returnError('ERR_UPDATE_JOB_TASK_STATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        }
    }


     /**
     * This function is used to check whether job can change the status
     */
    public function checkJobCanUpdateTheState ($jobID, $status) {
        $taskList = $this->getModel('JobTaskTable')->getJobTaskDetails($jobID);
        $requestStatusTaskCount = 0;
        $taskCount = $taskList->count();

        foreach ($taskList as $key => $value) {
            if ($value['jobTaskStatus'] == $status) {
                $requestStatusTaskCount += 1;
            }
        }

        if ($taskCount == $requestStatusTaskCount) {
            return true;
        }
        return false;
    }

    /**
    * this function use to start all job service
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function startAllJobRelatedServiceByJobTaskID($data, $userId)
    {

        $jobID = $data['jobId'];
        $materialIssueTaskIds = $data['materialIssueTaskIds'];
        $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobID);

        if (!$serviceList) {
            return $this->returnError('ERR_RETRIVE_SERVICES');    
        }

        foreach ($serviceList as $key => $value) {
            $jobTaskID = $value['jobTaskID'];
            $temp = [
                'jobId' => $jobID,
                'jobTaskId' => $jobTaskID
            ];

            if ($value['jobTaskStatus'] == 3) {

                $updateStatus = $this->startJobRelatedServiceByJobTaskID($temp, $userId);
                if (!$updateStatus['status']) {
                    return $this->returnError('ERR_UPDATE_JOB_TASK_STATE', 'JOB_TASK_STATE_FAILD_UPDATE');
                }

            }
            
        }

        if (!is_null($materialIssueTaskIds)) {
            foreach ($materialIssueTaskIds as $value) {
                $issueMaterials = $this->issueMaterialAtTaskStart($value, $jobID);
                if (!$issueMaterials['status']) {
                    return $this->returnError($issueMaterials['msg']);
                }
            }
        }

        return $this->returnSuccess('SUCC_JOB_TASK_PROGRESS_UPDATE');
    }

    /**
    * this function use to end all job service
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function endAllJobRelatedServiceByJobTaskID($data, $userId)
    {

        $jobID = $data['jobId'];
        $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobID);
        if (!$serviceList) {
            return $this->returnError('ERR_RETRIVE_SERVICES');    
        }

        foreach ($serviceList as $key => $value) {
            $jobTaskID = $value['jobTaskID'];
            $temp = [
                'jobId' => $jobID,
                'jobTaskId' => $jobTaskID
            ];

            if ($value['jobTaskStatus'] == 8 || $value['jobTaskStatus'] == 17) {
                $updateStatus = $this->stopJobRelatedServiceByJobTaskID($temp, $userId);

                if (!$updateStatus['status']) {
                    return $this->returnError('ERR_UPDATE_JOB_TASK_STATE', 'JOB_TASK_STATE_FAILD_UPDATE');
                }                
            }
        }
       
        return $this->returnSuccess('SUCC_JOB_TASK_PROGRESS_UPDATE');
    }
    

    /**
    * this function use to get service related employees.
    * @param array $data
    * return array
    **/
    public function getJobTaskRelatedEmployees($data)
    {
        $jobID = $data['jobId'];
        $jobTaskID = $data['jobTaskID'];
        
        $jobTaskEmpList = $this->getModel('JobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($jobTaskID, true);
        if (!$jobTaskEmpList) {
            return $this->returnError('ERR_RETRIVE_SERVICES_EMP');    
        }

        $empList = [];
        foreach ($jobTaskEmpList as $key => $value) {
            if (is_null($value['employeeID'])) {
                $value['employeeCode'] = 'DEF_EMP';
                $value['employeeFirstName'] = 'Default';
                $value['employeeSecondName'] = 'Employee';
            }

            $empList [] = $value;
        }
        
        return $this->returnSuccess($empList, 'SUCC_JOB_TASK_EMP_LIST_RETRIVE');
    }


    /**
    * this function use to restart job service
    * @param array $data
    * return array
    **/
    public function restartJobRelatedServiceByJobTaskID($data, $locationID =null)
    {
        $jobTaskID = $data['jobTaskId'];
        $jobID = $data['jobId'];
        $jData = $this->getModel('JobTable')->getJobByID($jobID)->current();
        $entityID = $jData['entityID'];

        $status = $this->getStatusIDByStatusName('in progress');
        $updateJobTaskState = $this->getModel('JobTaskTable')->updateJobTaskStatusByJobTaskID($jobTaskID, 3);

        if (!$updateJobTaskState) {
            return $this->returnError('ERR_UPDATE_JOB_TASK_STATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        }

        $updateJobTaskIsRestarted = $this->getModel('JobTaskTable')->updateJobTaskIsRestarted($jobTaskID, true);

        if (!$updateJobTaskIsRestarted) {
            return $this->returnError('ERR_JOB_TASK_RESTART');
        }


        $updateJobTaskEndTime = $this->getModel('JobTaskTable')->updateJobTaskEndTimeByJobTaskID($jobTaskID, NULL);
        if (!$updateJobTaskEndTime) {
            return $this->returnError('ERR_UPDATE_JOB_TASK_END_TIME');
        }

        $jobData = $this->getModel('JobTable')->getJobByJobID($jobID)->current();

        if (!$jobData['isRestarted']) {
            $updateJobIsRestarted = $this->getModel('JobTable')->updateJobIsRestarted($jobID, true, $jobData['jobStatus']);
        }
        $updateJobCompleted = $this->getModel('JobTable')->updateJobCompletedTimeByJobID($jobID, NULL);
        $entityService = $this->getService('EntityService');
        $entityID = $entityService->updateEntity($entityID, $userId);

        $updateJobState = $this->getModel('JobTable')->updateJobStatus($jobID, $status);
        if (!$updateJobState) {
            return $this->returnError('ERR_JOB_STATE_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        }

        $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobID);

        if (!$serviceList) {
            return $this->returnError('ERR_RETRIVE_SERVICES');    
        }

        $list = [];

        foreach ($serviceList as $key => $value) {
           $list [] = $value;
        }


        if (!isset($data['isFromDashboard'])) {
            return $this->returnSuccess($list);
        }

        $data  = $this->getUpdatedDashboardJobList($locationID);
        $data['data']['serviceList'] = $list;

        return $data;

        // return $this->returnSuccess($list);
    }

    /**
    * this function use to get job status by jobID
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function getJobStatusByJobId($data)
    {
        $jobID = $data['jobId'];
        
        $jobList = $this->getModel('JobTable')->getJobDetails($jobID);

        $list = [];
        foreach ($jobList as $key => $value) {
            $list [] = $value;
        }

        if (!$jobList) {
            return $this->returnError('ERR_JOB_TASK_PROGRESS_UPDATE', 'JOB_TASK_STATE_FAILD_UPDATE');
        }
        
        return $this->returnSuccess($list, 'SUCC_JOB_TASK_EMP_LIST_RETRIVE');
    }

    /**
    * this function use to get job status by jobID
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function restartJobRelatedServices($data)
    {
        foreach ($data['serviceArray'] as $key => $value) {
            $dataSet = [
                'jobTaskId' => $value,
                'jobId' => $data['jobId']
            ];

            $restartJobTask = $this->restartJobRelatedServiceByJobTaskID($dataSet);
            if (!$restartJobTask['status']) {
                return $this->returnError('ERR_JOB_TASK_RESTART');
            }
        }
        return $this->returnSuccess(null, 'SUCC_JOB_TASK_RESTART');
    }

    /**
    * this function use to create job task employee
    * @param int $employeeID
    * @param int $taskID
    * @param int $jobTaskID
    * @param datetime $currentDateTime
    * return boolean
    **/
    public function createJobTaskEmployee($employeeID, $taskID, $jobTaskID, $currentDateTime) {
        $jobTaskEmpData = [
            'employeeID' => $employeeID,
            'taskID' => $taskID,
            'jobTaskID' => $jobTaskID
        ];

        $jobTaskEmployeeData = new JobTaskEmployee();
        $jobTaskEmployeeData->exchangeArray($jobTaskEmpData);
        $jobTaskEmployeeID = $this->getModel('JobTaskEmployeeTable')->saveJobTaskEmployee($jobTaskEmployeeData);
        if (!$jobTaskEmployeeID) {
            return false;
        }

        $jobTaskEmpWorkTimeData = [
            'jobTaskEmployeeID' => $jobTaskEmployeeID,
            'startedAt' => $currentDateTime,
            'endedAt' => null
        ];

        $jobTaskEmployeeWrkTmData = new JobTaskEmployeeWorkTime();
        $jobTaskEmployeeWrkTmData->exchangeArray($jobTaskEmpWorkTimeData);
        $jobTaskEmployeeWorkTimeID = $this->getModel('JobTaskEmployeeWorkTimeTable')->saveJobTaskEmployeeWorkTime($jobTaskEmployeeWrkTmData);

        if (!$jobTaskEmployeeWorkTimeID) {
            return false;
        }

        return true;
    }

    /**
    * this function use to updatejob task employee
    * @param array $empArray
    * @param int $jobTaskID
    * @param int $taskID
    * return boolean
    **/
    public function updateJobTaskEmployee($empArray, $jobTaskID, $taskID, $changeFlag) {

        $gmtDate = $this->getGMTDateTime('Y-m-d H:i:s');
        $currentDateTime = $gmtDate['data']['currentTime'];
        $oldEmpArray = [];

        $getJobTaskRelatedEmployees = $this->getModel('JobTaskEmployeeTable')->getJobTaskRelatedEmployees($jobTaskID);

        foreach ($getJobTaskRelatedEmployees as $key => $value) {
            $updateIsChangeFlag = $this->getModel('JobTaskEmployeeTable')->updateIsChangedFlag($value['jobTaskEmployeeID'], true);
            $oldEmpArray [] = $value['employeeID'];
            if (!$updateIsChangeFlag) {
                return false;
            }
        }
        
        if ($changeFlag == 'false') {
            $empArray = (is_null($empArray)) ? $oldEmpArray: $empArray;
        }

        if (is_null($empArray)) {
            $createJobTaskEmployee = $this->createJobTaskEmployee(null, $taskID, $jobTaskID, $currentDateTime);

            if (!$createJobTaskEmployee) {
                return $this->returnError('ERR_JOB_TSK_EMP_CREATE');
            }
        } else {
            foreach ($empArray as $key => $value) {
                $createJobTaskEmployee = $this->createJobTaskEmployee($value, $taskID, $jobTaskID, $currentDateTime);
                if (!$createJobTaskEmployee) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
    * this function use to check whether job can remove isRestart flag 
    * @param int $jobId
    * return boolean
    **/
    public function checkJobCanRemoveRestarted($jobId) {
        $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobId);

        foreach ($serviceList as $key => $value) {
            if ($value['restarted']) {
               return false;
            }
        }

        return true;
    } 


    /**
    * this function use to update job service employee incentive
    * @param int $jobTaskID
    * return boolean
    **/
    public function updateJobTaskEmployeeIncentive($jobTaskID, $serviceID = null, $vehicleType = null) {

        $jobTask = $this->getModel('JobTaskTable')->getTaskDetailsByJobTaskIdForService($jobTaskID);

        // if (!is_null($jobTask['jobTaskTaskCardID'])) {
            // $jobTask = $this->getModel('TaskCardTaskTable')->getTaskList($jobTask['jobTaskTaskCardID'], $jobTask['taskID'])->current();
        // }

        if (empty($jobTask['jobTaskTaskCardID'])) {

            $serviceIncentiveDetails = $this->getModel('ServiceInceniveRateTable')->getIncentiveByServiceIdAndVehicleType($serviceID, $vehicleType);
            if ($serviceIncentiveDetails) {
                $actulalJobTaskEmp = $this->getModel('jobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($jobTaskID, $unique = true);

                $numberOfEmp = $actulalJobTaskEmp->count();

                $allocateIncentiveVal = $serviceIncentiveDetails['incentive'] / $numberOfEmp;

                $jobTaskEmployeeList = $this->getModel('jobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($jobTaskID, $unique = false);
                foreach ($jobTaskEmployeeList as $key => $value) {
                    $updateJobTaskEmployeeIncentiveVal = $this->getModel('jobTaskEmployeeTable')->updateJobTaskEmployeeIncentiveVal($value['jobTaskEmployeeID'], $allocateIncentiveVal);
                }
            }
        } else {

            $jobCardIncentive = $this->getModel('JobCardIncentiveRateTable')->getJobCardIncentiveByJobCardIdAndVehicleType($jobTask['jobTaskTaskCardID'], $vehicleType);


            $jobCardServiceIncentive = $this->getModel('JobCardServiceIncentiveRateTable')->getJobCardServiceIncentiveByServiceIdAndJobCardRateID($jobCardIncentive['jobCardIncentiveRateID'],$serviceID);

            $actulalJobTaskEmp = $this->getModel('jobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($jobTaskID, $unique = true);

            $numberOfEmp = $actulalJobTaskEmp->count();

            $allocateIncentiveVal = $jobCardServiceIncentive['incentive'] / $numberOfEmp;

            $jobTaskEmployeeList = $this->getModel('jobTaskEmployeeTable')->getAllJobTaskRelatedEmployees($jobTaskID, $unique = false);
            foreach ($jobTaskEmployeeList as $key => $value) {
                $updateJobTaskEmployeeIncentiveVal = $this->getModel('jobTaskEmployeeTable')->updateJobTaskEmployeeIncentiveVal($value['jobTaskEmployeeID'], $allocateIncentiveVal);
            }
        }
        return true;
    }

    public function getJobsForDashboard($locationID)
    {
        $pendingJobList = $this->getDashboardJobList(3, $locationID,0);
        $inProgressJobList = $this->getDashboardJobList(8, $locationID,0);
        $completedJobList = $this->getDashboardJobList(9, $locationID,0);
        $allInProgressJobList = $inProgressJobList; 

        foreach ($completedJobList as $key => $part) {
           $sort[$key] = strtotime($part['completed_at']);
        }
        array_multisort($sort, SORT_DESC, $completedJobList);

        $pendingJobCount = $this->setListCount(sizeof($pendingJobList));
        $inprogressJobCount = $this->setListCount(sizeof($allInProgressJobList));
        $completedJobCount = $this->setListCount(sizeof($completedJobList));

        $overallJobs = array_merge($pendingJobList, $completedJobList, $allInProgressJobList);

        $data = array(
            'pendingJobList' => $pendingJobList,
            'inProgressJobList' => $allInProgressJobList,
            'completedJobList' => $completedJobList,
            'pendingJobCount' => $pendingJobCount,
            'inprogressJobCount' => $inprogressJobCount,
            'completedJobCount' => $completedJobCount,
            'overallJobs' => $overallJobs,
        );


        $view = new ViewModel(array(
            'pendingJobList' => $pendingJobList,
            'inProgressJobList' => $allInProgressJobList,
            'completedJobList' => $completedJobList,
            'pendingJobCount' => $pendingJobCount,
            'inprogressJobCount' => $inprogressJobCount,
            'completedJobCount' => $completedJobCount,
            'overallJobs' => $overallJobs,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-job-dashboard/service-side-list');

        return ['data' => $data, 'view' => $view];
    }

    public function getUpdatedDashboardJobList($locationID)
    {
        $pendingJobList = $this->getDashboardJobList(3, $locationID,0);
        $inProgressJobList = $this->getDashboardJobList(8, $locationID,0);
        $completedJobList = $this->getDashboardJobList(9, $locationID,0);
        // $closedJobList = $this->getDashboardJobList(4, $locationID,0);
        // $HoldJobList = $this->getDashboardJobList(17, $locationID);
        $allInProgressJobList = $inProgressJobList; 

        foreach ($completedJobList as $key => $part) {
           $sort[$key] = strtotime($part['completed_at']);
        }
        array_multisort($sort, SORT_DESC, $completedJobList);

        $pendingJobCount = $this->setListCount($this->getModel('JobTable')->getJobCounttByStatus(3, $locationID));
        $inprogressJobCount = $this->setListCount($this->getModel('JobTable')->getJobCounttByStatus(8, $locationID));
        $completedJobCount = $this->setListCount($this->getModel('JobTable')->getJobCounttByStatus(9, $locationID));
        // $closedJobCount = $this->setListCount($this->getModel('JobTable')->getJobCounttByStatus(4, $locationID));

        // $overallJobs = array_merge($pendingJobList, $completedJobList, $allInProgressJobList);

        $data = array(
            'pendingJobList' => $pendingJobList,
            'inProgressJobList' => $allInProgressJobList,
            'completedJobList' => $completedJobList,
            'pendingJobCount' => $pendingJobCount,
            'inprogressJobCount' => $inprogressJobCount,
            'completedJobCount' => $completedJobCount,
            // 'closedJobCount' => $closedJobCount,
            'overallJobs' => $overallJobs,
        );

        $view = new ViewModel(array(
            'pendingJobList' => $pendingJobList,
            'inProgressJobList' => $allInProgressJobList,
            'completedJobList' => $completedJobList,
            'pendingJobCount' => $pendingJobCount,
            'inprogressJobCount' => $inprogressJobCount,
            'completedJobCount' => $completedJobCount,
            // 'closedJobCount' => $closedJobCount,
            // 'overallJobs' => $overallJobs,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-job-dashboard/service-side-list');

        return ['data' => $data, 'view' => $view, 'status' => true];
    }

    public function getCompletedJobsForDashboard($locationID, $data)
    {
        
        $offset = $data['offset'];
        $completedJobList = $this->getDashboardJobList(9, $locationID, $offset);
        foreach ($completedJobList as $key => $part) {
           $sort[$key] = strtotime($part['completed_at']);
        }
        array_multisort($sort, SORT_DESC, $completedJobList);
        $data = array(
            'completedJobList' => $completedJobList,
        );

        $view = new ViewModel(array(
            'completedJobList' => $completedJobList,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-job-dashboard/service-side-list');

        return ['data' => $data, 'view' => $view];
    }

    public function getInprogressJobsForDashboard($locationID, $data)
    {

        $offset = $data['offset'];
        $inProgressJobList = $this->getDashboardJobList(8, $locationID, $offset);
        $data = array(
            'inProgressJobList' => $inProgressJobList,
        );

        $view = new ViewModel(array(
            'inProgressJobList' => $inProgressJobList,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-job-dashboard/service-side-list');

        return ['data' => $data, 'view' => $view];
    }

    public function getPendingJobsForDashboard($locationID, $data)
    {
        $offset = $data['offset'];
        $pendingJobList = $this->getDashboardJobList(3, $locationID,$offset);
        $data = array(
            'pendingJobList' => $pendingJobList,
        );

        $view = new ViewModel(array(
            'pendingJobList' => $pendingJobList,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-job-dashboard/service-side-list');

        return ['data' => $data, 'view' => $view];
    }

    public function getDashboardJobList($status, $locationID, $offset = null) {

        $userLocation = $locationID;
        $createdTimeStamp = $this->getGMTDateTime('Y-m-d H:i:s')['data']['currentTime'];
        $gmtStartDate = split(' ', $createdTimeStamp)[0]; 
        $startDate = $gmtStartDate.' 00:00:00';
        $endDate = date('Y-m-d H:i:s', strtotime("+1 day", strtotime($startDate)));
        $list = $this->getModel('JobTable')->getJobListByStatus($status, $userLocation, false, $startDate, $endDate, $offset, true);

        $currentDate = date('Y-m-d H:i:s');
        $date = $this->convertDateToStandardFormat($currentDate);

        $jobList = [];
        foreach ($list as $key => $value) {
            $dte = $this->getUserDateTime($value['createdTimeStamp'], 'Y-m-d H:i:s');
            $updtetime = $this->getUserDateTime($value['completed_at'], 'Y-m-d H:i:s');
            $value['createdTimeStamp'] = $dte;
            $value['completed_at'] = $updtetime;
            $jobList[] = $value;
           
        }
        return $jobList;
    }

    public function setListCount($length) {
        if (sizeof($length) == 0) {
            $count = '00';
        } else if ($length < 10) {
            $count = '0'.$length;
        } else {
            $count = $length;
        }

        return $count;
    }

    public function getJobVehicleRelatedStatus($data)
    {
        $jobID = $data['jobID'];
        $paginated = false;
        $vehicleStatusChecks = $this->getModel('VehicleStatusCheckTable')->fetchAllByID();
        $jobVehicleStatus = $this->getModel('JobVehicleStatusTable')->getJobVehicleStatusByJobID($jobID)->current();

        $jobVehicleDetails = $this->getModel('JobVehicleTable')->getJobVehicleByJobID($jobID)->current();

        $view = new ViewModel(array(
            'jobVehicleStatus' => $jobVehicleStatus,
            'vehicleStatusChecks' => $vehicleStatusChecks,
            'jobID' => $jobID,
            'jobVehicleID' => $jobVehicleDetails['jobVehicleID'],
            'jobVehicleKMs' => $jobVehicleDetails['jobVehicleKMs']
        ));
        $view->setTerminal(true);

        $view->setTemplate('jobs/service-job-dashboard/vehicle-status.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data'=> $jobVehicleStatus
        ];
    }

    public function getJobVehicleRelatedRemarks($data)
    {
        $jobVehicleID = $data['jobVehicleID'];
            
        $jobVehicleRemark = $this->getModel('JobVehicleRemarkTable')->getJobVehicleRemarksByJobVehicleID($jobVehicleID);

        $jobVehicleRemarkData = [];
        foreach ($jobVehicleRemark as $value) {
            $jobVehicleRemarkData[] = $value;
        }


        $view = new ViewModel(array(
            'jobVehicleRemarkData' => $jobVehicleRemarkData,
        ));
        $view->setTerminal(true);
        $view->setTemplate('jobs/service-job-dashboard/vehicle-remarks.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data'=> $jobVehicleRemarkData
        ];
    }

    public function saveVehicleStatus($data, $userId)
    {
        $vehicleStatus = $data['vehicleStatusData'];

        $existingJobVehicleStatus = $this->getModel('JobVehicleStatusTable')->getJobVehicleStatusByJobID($data['jobID'])->current();
        $VehicleStatusChecks = $this->getModel('VehicleStatusCheckTable')->fetchAll();

        $entityService = $this->getService('EntityService');
        if ($existingJobVehicleStatus) {
            $entityService->updateDeleteInfoEntity($existingJobVehicleStatus['entityID'], $userId);
        }

        $entityID = $entityService->createEntity($userId)['data']['entityID'];
        $vehicleStatData = [
            'jobVehicleID' => $data['jobVehicleID'],
            'airFilter' => ($VehicleStatusChecks[$vehicleStatus['airFilter']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['airFilter']],
            'engineOil' => ($VehicleStatusChecks[$vehicleStatus['engineOil']]  == null) ? "" : $VehicleStatusChecks[$vehicleStatus['engineOil']], 
            'wiperFluid' => ($VehicleStatusChecks[$vehicleStatus['wiperFluid']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['wiperFluid']],
            'wiperBlades' => ($VehicleStatusChecks[$vehicleStatus['wiperBlades']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['wiperBlades']],
            'tyres' => ($VehicleStatusChecks[$vehicleStatus['tyres']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['tyres']],
            'checkBelts' => ($VehicleStatusChecks[$vehicleStatus['checkBelts']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['checkBelts']],
            'coolant' => ($VehicleStatusChecks[$vehicleStatus['coolant']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['coolant']],
            'brakeFluid' => ($VehicleStatusChecks[$vehicleStatus['brakeFluid']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['brakeFluid']],
            'steeringFluid' => ($VehicleStatusChecks[$vehicleStatus['steeringFluid']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['steeringFluid']],
            'transmissionFluid' => ($VehicleStatusChecks[$vehicleStatus['transmissionFluid']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['transmissionFluid']],
            'frontLeftTyrePressure' => ($vehicleStatus['frontLeftTyrePressure'] == undefined) ? "" : $vehicleStatus['frontLeftTyrePressure'],
            'frontRightTyrePressure' => ($vehicleStatus['frontRightTyrePressure'] == undefined) ? "" : $vehicleStatus['frontRightTyrePressure'],
            'rearLeftTyrePressure' => ($vehicleStatus['rearLeftTyrePressure'] == undefined) ? "" : $vehicleStatus['rearLeftTyrePressure'],
            'rearRightTyrePressure' => ($vehicleStatus['rearRightTyrePressure'] == undefined) ? "" : $vehicleStatus['rearRightTyrePressure'],
            'battery' => ($VehicleStatusChecks[$vehicleStatus['battery']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['battery']],
            'oilFilter' => ($VehicleStatusChecks[$vehicleStatus['oilFilter']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['oilFilter']],
            'gearBoxOil' => ($VehicleStatusChecks[$vehicleStatus['gearBoxOil']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['gearBoxOil']],
            'differentialOil' => ($VehicleStatusChecks[$vehicleStatus['differentialOil']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['differentialOil']],
            'acCabinFilter' => ($VehicleStatusChecks[$vehicleStatus['acCabinFilter']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['acCabinFilter']],
            'fuelFilter' => ($VehicleStatusChecks[$vehicleStatus['fuelFilter']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['fuelFilter']],
            'radiatorCoolantLevel' => ($VehicleStatusChecks[$vehicleStatus['radiatorCoolantLevel']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['radiatorCoolantLevel']],
            'powerSteeringOil' => ($VehicleStatusChecks[$vehicleStatus['powerSteeringOil']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['powerSteeringOil']],
            'radiatorHoses' => ($VehicleStatusChecks[$vehicleStatus['radiatorHoses']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['radiatorHoses']],
            'driveBelts' => ($VehicleStatusChecks[$vehicleStatus['driveBelts']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['driveBelts']],
            'engineMount' => ($VehicleStatusChecks[$vehicleStatus['engineMount']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['engineMount']],
            'shockMounts' => ($VehicleStatusChecks[$vehicleStatus['shockMounts']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['shockMounts']],
            'silencerMounts' => ($VehicleStatusChecks[$vehicleStatus['silencerMounts']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['silencerMounts']],
            'axleBoots' => ($VehicleStatusChecks[$vehicleStatus['axleBoots']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['axleBoots']],
            'headLamps' => ($VehicleStatusChecks[$vehicleStatus['headLamps']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['headLamps']],
            'signalLamps' => ($VehicleStatusChecks[$vehicleStatus['signalLamps']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['signalLamps']],
            'parkingLamps' => ($VehicleStatusChecks[$vehicleStatus['parkingLamps']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['parkingLamps']],
            'fogLamps' => ($VehicleStatusChecks[$vehicleStatus['fogLamps']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['fogLamps']],
            'tailLamp' => ($VehicleStatusChecks[$vehicleStatus['tailLamp']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['tailLamp']],
            'roomLamp' => ($VehicleStatusChecks[$vehicleStatus['roomLamp']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['roomLamp']],
            'wScreenWasherFluid' => ($VehicleStatusChecks[$vehicleStatus['wScreenWasherFluid']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['wScreenWasherFluid']],
            'rackBoots' => ($VehicleStatusChecks[$vehicleStatus['rackBoots']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['rackBoots']],
            'brakePadsLiners' => ($VehicleStatusChecks[$vehicleStatus['brakePadsLiners']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['brakePadsLiners']],
            'tyreWareUneven' => ($VehicleStatusChecks[$vehicleStatus['tyreWareUneven']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['tyreWareUneven']],
            'wheelNutsStuds' => ($VehicleStatusChecks[$vehicleStatus['wheelNutsStuds']] == null) ? "" : $VehicleStatusChecks[$vehicleStatus['wheelNutsStuds']],
            'odoMeter' => ($vehicleStatus['odoMeter'] == undefined) ? "" : $vehicleStatus['odoMeter'],
            'brakePadsFrontfactor' => ($vehicleStatus['brakePadsFrontfactor'] == undefined) ? "" : $vehicleStatus['brakePadsFrontfactor'],
            'brakePadsRearfactor' => ($vehicleStatus['brakePadsRearfactor'] == undefined) ? "" : $vehicleStatus['brakePadsRearfactor'],
            'brakePadsChangeFluid' => ($vehicleStatus['brakePadsChangeFluid'] == undefined) ? "" : $vehicleStatus['brakePadsChangeFluid'],
            'entityID' => $entityID
        ];
        $saveData = new JobVehicleStatus();
        $saveData->exchangeArray($vehicleStatData);
        // var_dump($saveData).die();
        $result = $this->getModel('JobVehicleStatusTable')->saveJobVehicleStatus($saveData);
        
        if ($vehicleStatus['odoMeter'] != undefined) {
            $updateJobVehicle = [
                'jobVehicleKMs' => $vehicleStatus['odoMeter']
            ];
            
            $result = $this->getModel('JobVehicleTable')->updateJobVehicle($updateJobVehicle, $data['jobVehicleID']);

        }

        if (!$result) {
            return $this->returnError('ERR_SAVE_VEHICLE_STATUS');
        }

        return $this->returnSuccess(null, 'SUCC_SAVE_VEHICLE_STATUS');
    }

    public function saveVehicleRemark($data)
    {
        $vehicleRemarkData = [
            'jobVehicleID' => $data['jobVehicleID'],
            'remark' => $data['remark']
        ];

        $saveData = new JobVehicleRemark();
        $saveData->exchangeArray($vehicleRemarkData);
        $result = $this->getModel('JobVehicleRemarkTable')->saveJobVehicleRemark($saveData);
        
        if (!$result) {
            return $this->returnError('ERR_SAVE_VEHICLE_REMARK');
        }

        return $this->returnSuccess(null, 'SUCC_SAVE_VEHICLE_REMARK');   
    }

    public function saveTempJobMaterials($data)
    {
        $deleRes = $this->getModel('JobTempMaterialTable')->deleteMaterialByJobID($data['jobId']);

        foreach ($data['serviceMaterials'] as $value) {
            $jobMaterialData = [
                'jobID' => $data['jobId'],
                'locationProductID' => $value['LocationProductID'],
                'productID' => $value['productID'],
                'issueQty' => $value['issueQty'],
                'unitPrice' => $value['unitPrice'],
                'productUomID' => $value['selectedUomID'],
                'productDescription' => (!empty($value['itemDes'])) ? $value['itemDes'] : null,
            ];

            $saveData = new JobTempMaterial();
            $saveData->exchangeArray($jobMaterialData);
            $result = $this->getModel('JobTempMaterialTable')->save($saveData);

            if (!$result) {
                return $this->returnError('ERR_JOB_TRADE_MATE_SAVE');
            }
        }
        return $this->returnSuccess(null, 'SUCC_JOB_TRADE_MATE_SAVE');
    }

    public function getJobDetailsByJobId($data) {
        $jobServiceData = $this->getModel('JobTable')->getJobsWithServices(false,$data['jobId'], false);
            
        $jobServiceDetails = [];
        $jobCardIDs = [];
        foreach ($jobServiceData as $key => $row) {
            $tempG = array();
            $jobID = $row['jobId'];
            $tempG['jobId'] = $row['jobId'];
            $tempG['jobCode'] = $row['jobReferenceNumber'];
            $tempG['jobCustomer'] = $row['customerName'];
            $tempG['jobCardName'] = $row['taskCardName'];
            $tempG['vehicalModal'] = $row['vehicleModelName'];
            $tempG['vehicleType'] = $row['vehicleType'];
            $tempG['inDate'] = $this->getUserDateTime($row['createdTimeStamp']);
            $tempG['jobSupervisor'] = $row['userFirstName'].' '.$row['userLastName'];
            $tempG['jobStatus'] = $row['jobStatus'];
            $productUom = $this->getModel('ProductUomTable')->getProductUomListByProductID($row['productID']);

            $thisqty = $this->getProductQuantityViaDisplayUom($row['jobProductIssuedQty'], $productUom);
            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];
            
            $jobServices = (isset($jobServiceDetails[$row['jobId']]['jobServices'])) ? $jobServiceDetails[$row['jobId']]['jobServices'] : array();
            $jobTradeMaterials = (isset($jobServiceDetails[$row['jobId']]['jobTradeMaterials'])) ? $jobServiceDetails[$row['jobId']]['jobTradeMaterials'] : array();

            if ($row['jobProductID'] != null) {
                $jobTradeMaterials[$row['jobProductID']] = array('jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'jobProductID' => $row['jobProductID'],'locationProductID' => $row['locationProductID'], 'jobProductTaskID' => $row['jobProductTaskID'],'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductIssuedQty' => $thisqty['quantity'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'total' => floatval($row['jobProductIssuedQty']) * floatval($row['jobProductUnitPrice']));
            }

            if ($row['jobTaskID'] != NULL) {
                $serviceKey = $row['jobTaskID'];

                $serviceProducts = (isset($jobServices[$serviceKey]['serviceProducts'])) ? $jobServices[$serviceKey]['serviceProducts'] : array();
                $serviceSubTasks = (isset($jobServices[$serviceKey]['serviceSubTasks'])) ? $jobServices[$serviceKey]['serviceSubTasks'] : array();


                $jobServices[$serviceKey] = array('serviceCode' => $row['taskCode'], 'serviceName' => $row['taskName'],'jobServiceID' => $row['jobTaskID'],'serviceID' => $row['taskID'], 'serviceRate' => $serviceRate, 'jobTaskTaskCardID' => $row['jobTaskTaskCardID']);



                if (!empty($jobServices[$serviceKey]['jobTaskTaskCardID'])) {
                    $jobCardIDs[]=$jobServices[$serviceKey]['jobTaskTaskCardID'];
                }
                
                if ($row['jobProductID'] != NULL && $row['jobTaskID'] == $row['jobProductTaskID'] && $row['jobProductMaterialTypeID'] == 1) {
                    $serviceProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductIssuedQty' => $thisqty['quantity'],'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr']);
                }
                
                if ($row['jobSubTaskID'] != NULL && $row['jobTaskID'] == $row['jobTaskIdOfJobSubtask']) {
                    $serviceSubTasks[$row['jobSubTaskID']] = array('subTaskCode' => $row['subTaskCode'], 'subTaskName' => $row['subTaskName'],'subTaskID' => $row['subTaskId']);
                }
                
                $jobServices[$serviceKey]['serviceProducts'] = $serviceProducts;
                $jobServices[$serviceKey]['serviceSubTasks'] = $serviceSubTasks;
            }
            
            $tempG['jobServices'] = $jobServices;
            $tempG['jobTradeMaterials'] = $jobTradeMaterials;
            $jobServiceDetails[$row['jobId']] = $tempG;
        
        }


        $jobCardID = array_unique($jobCardIDs);
        if (sizeof($jobCardID) > 0 && sizeof($jobCardID) == 1) {
            $jobServiceDetails[$data['jobId']]['jobCardID'] = $jobCardID[0];
            $getJobCardServiceRate = $this->getModel('JobCardIncentiveRateTable')->getJobCardIncentiveByJobCardIdAndVehicleType($jobServiceDetails[$data['jobId']]['jobCardID'], $jobServiceDetails[$data['jobId']]['vehicleType']);
            $jobServiceDetails[$data['jobId']]['jobCardRateDetails'] = $getJobCardServiceRate;
            $jobServiceDetails[$data['jobId']]['useJobCardServices'] = true;
        }

        $materialData = $this->getModel('JobTempMaterialTable')->getJobRelatedMaterials($data['jobId']);

        $tempMaterials = [];
        foreach ($materialData as $value) {
            $tempMaterials[$value['jobTempMaterialID']] = $value;
        }

        $jobServiceDetails[$data['jobId']]['tempMaterials'] = $tempMaterials;

        $jobServiceDataFinal = $this->getServiceRateByServiceIdAndVehicleTypeId($jobServiceDetails[$jobID]);
        return $jobServiceDataFinal;
    }

    public function getServiceRateByServiceIdAndVehicleTypeId($serviceData)
    {   
        $jobData = $serviceData;
        $vehicleTypeID = $jobData['vehicleType'];
        $jobServices = [];
        foreach ($serviceData['jobServices'] as $key => $value) {
            $jobServices[$key] = $value;
            $res = $this->getModel('ServiceInceniveRateTable')->getIncentiveByServiceIdAndVehicleType($value['serviceID'], $vehicleTypeID);
            if (!empty($value['serviceSubTasks'])) {
                if ($res) {
                    $subTaskRate = $this->getModel('RateCardTaskSubTaskTable')->getRelatedServiceSubtasksRates($res['serviceInceniveRateId']);
                    if (count($subTaskRate) > 0) {
                        $serviceRateBySubTask = 0;
                        foreach ($subTaskRate as $sub) {
                            foreach ($value['serviceSubTasks'] as $val) {
                                if ($val['subTaskID'] == $sub['subTaskId']) {
                                    $serviceRateBySubTask += floatval($sub['rateCardTaskSubTaskRate']);
                                }
                                
                            }
                        }
                        
                        $jobServices[$key]['serviceRate'] = $serviceRateBySubTask;
                    } else {
                        $jobServices[$key]['serviceRate'] = (is_null($res['serviceRate']) ? 0 : floatval($res['serviceRate'])); 
                    }
                } else {
                    $jobServices[$key]['serviceRate'] = 0; 
                }
            } else {
                if ($res) {
                    $jobServices[$key]['serviceRate'] = (is_null($res['serviceRate']) ? 0 : floatval($res['serviceRate'])); 
                }
            }
        }
        $jobData['jobServices'] = $jobServices;
        
        return $jobData;
    }

    public function getJobEstimatedCostDetails($locationID, $data)
    {
        
        $result = $this->getJobDetailsByJobId($data);

        if (!$result) {
            return $this->returnError('ERR_JOB_TRADE_MATE_SAVE');
        }

        $nonJobCardServicesTotal = 0;
        $nonJobCardServices = [];
        //calculate total  cost
        foreach ($result['jobServices'] as $key => $value) {

            if (empty($value['jobTaskTaskCardID']))  { 
                    $nonJobCardServicesTotal = $nonJobCardServicesTotal + (!empty($value['serviceRate'])) ? $value['serviceRate'] : 0;
                    $nonJobCardServices[] = $value;
            }
        }


        $tradeMaterialTotal = 0;
        $tradeMaterials = [];
        //calculate total  cost
        foreach ($result['tempMaterials'] as $key => $val) {
            $tradeMaterialTotal = $tradeMaterialTotal + ((float) $val['unitPrice'] * (float) $val['issueQty']);
            $tradeMaterials[] = $val;
            
        }



        $jobCardRateTotal = (!empty($result['jobCardRateDetails'])) ? (float) $result['jobCardRateDetails']['serviceRate'] : 0;
        
        $data = [

            "dataSet" => $result,
            "jobCardRateTotal" => $jobCardRateTotal,
            "jobCardRateDetails" => (!empty($result['jobCardRateDetails'])) ? [$result['jobCardRateDetails']] : [],
            "nonJobCardServices" => $nonJobCardServices,
            "nonJobCardServicesTotal" => $nonJobCardServicesTotal,
            "tradeMaterials" => $tradeMaterials,
            "tradeMaterialTotal" => $tradeMaterialTotal,
        ];


        
        return $this->returnSuccess($data, 'SUCC_JOB_TRADE_MATE_SAVE');
    }

    public function getServiceSubTasks($locationID, $data)
    {
        $res = $this->getModel('jobSubTaskTable')->getSubTaskByJobTaskId($data['jobTaskId']);
        if (!$res) {
            return $this->returnError('ERR_SERVICE_SUB_TASK_RETRIVE');
        }

        $relatedServices = [];
        foreach ($res as $key => $value) {
            $relatedServices[] = $value;
        }


        return $this->returnSuccess($relatedServices, 'SUCC_SERVICE_SUB_TASK_RETRIVE');

       // var_dump($data).die();
    }

}