<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Jobs\Model\TaskCard;
use Jobs\Model\JobCardVehicleType;
use Jobs\Model\TaskCardTask;
use Jobs\Model\TaskCardTaskProduct;
use Jobs\Model\TaskCardTaskUnitPrice;
use Zend\View\Model\ViewModel;

class TaskCardService extends BaseService {
	/**
	 * This function is used save task card
	 * @param $postData
	 */
	public function saveTaskCard($data, $type = 'taskCard') {
		$taskCardCode = $data->taskCardCode;
		$taskCardName = $data->taskCardName;
		$taskCardTasks = $data->taskCardTasks;
		$taskCardProducts = $data->taskCardProducts;
		$taskCardVehicleTypes = (!empty($data->vehicalTypeIDs)) ? $data->vehicalTypeIDs : [];
		$productID = null;

		if ($type ==  'jobCard') {
			$productID = (!empty($data->productID)) ? $data->productID : null;
		}


		$taskCardTaskUnitPrice = $data->taskCardTaskPriceList;
		
		//check whether task card code alredy in database or not.
		$result = $this->getModel('TaskCardTable')->getTaskCardByTaskCardCode($taskCardCode);
		if (!($result->count() > 0)) {
			$data = array(
				'taskCardCode' => $taskCardCode,
				'taskCardName' => $taskCardName,
				'productID' => $productID

			);

			$taskCardData = new TaskCard();
			$taskCardData->exchangeArray($data);
			$taskCardID = $this->getModel('TaskCardTable')->saveTaskCard($taskCardData);
			if ($taskCardID) {

				foreach ($taskCardTasks as $key => $value) {
					$data = array(
						'taskCardID' => $taskCardID,
						'taskID' => $value['taskID'],
						'taskCardTaskRate1' => $value['defaultRate'],
						'taskCardTaskRate2' => $value['rate2'],
						'taskCardTaskRate3' => $value['rate3'],
						'taskCardTaskIncentiveValue' => null,
						'uomID' => $value['UomId'],
					);

					$taskCardTaskData = new TaskCardTask();
					$taskCardTaskData->exchangeArray($data);
					$taskCardTaskID = $this->getModel('TaskCardTaskTable')->saveTaskCardTask($taskCardTaskData);

					if (!$taskCardTaskID) {
						return $this->returnError('ERR_TSKCRD_TSK_SAVE', null);
					}
					// save task card rates
					$taskCardUnitPrice = new TaskCardTaskUnitPrice();
					foreach ($taskCardTaskUnitPrice[$value['taskID']] as $rateKeys => $rates) {
						$taskRates = [
							'taskCardTaskID' => $taskCardTaskID,
							'taskID' => $value['taskID'],
							'rates' => floatval($rates)
						];
						$taskCardUnitPrice->exchangeArray($taskRates);

						// save task card rates
						$saveRates = $this->getModel('TaskCardTaskUnitPriceTable')->saveTaskRates($taskCardUnitPrice);
						if (!$saveRates) {
							return $this->returnError('ERR_SAVE_TASK_RATES');
						}
					}

					if (!is_null($taskCardProducts[$key])) {
						foreach ($taskCardProducts[$key] as $ke => $val) {
							$data = array(
								'taskCardTaskID' => $taskCardTaskID,
								'locationProductID' => $val['locationPID'],
								'taskCardTaskProductName' => $val['pName'],
								'taskCardTaskProductQty' => $val['pQuantity'],
								'taskCardTaskProductUomID' => $val['pUOM'],
							);
							$taskCardTaskProductsData = new TaskCardTaskProduct();
							$taskCardTaskProductsData->exchangeArray($data);
							$taskCardTaskProductID = $this->getModel('TaskCardTaskProductTable')->saveTaskCardTaskProducts($taskCardTaskProductsData);

							if (!$taskCardTaskProductID) {
								return $this->returnError('ERR_TSKCRD_TSK__PRODUCT_SAVE', null);
							}
						}
					}
				}
				if ($type == 'jobCard' && !empty($taskCardVehicleTypes)) {
					foreach ($taskCardVehicleTypes as $key => $vehicleTypeID) {
						$data = [];
						$data = [
							'jobCardID' => $taskCardID,
							'vehicleTypeID' => $vehicleTypeID,
							'deleted' => 0
						];
							
						$jobCardVehicleTypeData = new JobCardVehicleType();
						$jobCardVehicleTypeData->exchangeArray($data);
						$jobCardVehicleTypeID = $this->getModel('JobCardVehicleTypeTable')->saveJobCardVehicleType($jobCardVehicleTypeData);

						if (!$jobCardVehicleTypeID) {
							return $this->returnError('ERR_JB_CRD_VHI_TPE_SAVE', null);
						}
					}
				}
			}
			return $this->returnSuccess(array('taskCardID' => $taskCardID), 'SUCC_TSKCRD_SAVE');

		} else {
			return $this->returnError('ERR_TSKCRD_SAVE', null);
		}
		
	}

	/**
	 * This function is used update task card
	 * @param $postData
	 */
	public function updateTaskCard($data, $taskCardTaskID, $table = NULL, $type = 'taskCard') {

		$deleteFlag = $this->getModel('TaskCardTable')->updateDeleteTaskCard($taskCardTaskID);
		$deleteTaskCardTaskFlag = $this->getModel('TaskCardTaskTable')->updateDeleteTaskCardTask($taskCardTaskID);

		if ($type = 'jobCard') {
			$updateDeleteJobCardVehicleType = $this->getModel('JobCardVehicleTypeTable')->updateDeleted($taskCardTaskID);
		}
		$taskCard = new TaskCard();
		$validateCode = $taskCard->getTaskCardCodeValidate($data->taskCardCode, $table);

		if (!$validateCode) {
			$this->returnError('ERR_TSK_CRD_CODE_ALREDY_EXIXT', null);
		}

		if ($deleteFlag && $deleteTaskCardTaskFlag) {
			$updatedFlag = $this->saveTaskCard($data, $type);
			if ($updatedFlag['status']) {
				return $this->returnSuccess($updatedFlag['data'], 'SUCC_TASKCARD_UPDATE');
			} else {
				return $this->returnError('ERR_TASKCARD_UPDATE', null);
			}
		} else {
			return $this->returnError('ERR_TASKCARD_UPDATE', null);
		}
	}

	/**
	 * This function is used to get task card by search key
	 */
	public function getTaskCardBySearchKey($data, $param, $type = 'taskCard') {
		$paginated = false;
		$taskSearchKey = $data['taskSearchKey'];

		if (!empty($taskSearchKey)) {
			$tbl = $this->getModel('TaskCardTable');
			$taskCardList = $tbl->getTaskCardforSearch($taskSearchKey, $type);
		} else {
			$taskCard = $this->getServiceLocator()->get("TaskCardController");
			$taskCardList = $taskCard->getPaginatedTaskCards(15, $param);
			$paginated = true;
		}

		$view = new ViewModel(array(
			'taskCards' => $taskCardList,
			'paginated' => $paginated,
		));

		$view->setTerminal(true);
		if ($type == 'taskCard') {
			$view->setTemplate('jobs/task-card/view.phtml');
		} else {
			$view->setTemplate('jobs/job-card-setup/list.phtml');
		}

		return $view;
	}

	/**
	 * This function is used to delete task card
	 **/
	public function deleteTaskCardByTaskCardID($data, $type = 'taskCard') {
		$taskCardID = $data['taskCardID'];
		$taskCode = $data['taskCardCode'];

		if ($type == 'jobCard') {
			$taskCardDetails = $this->getModel('jobTaskTable')->getTaskDetailsByTaskCardId($taskCardID);
			if ($taskCardDetails) {
				return $this->returnError('ERR_TSKCRD_DELETE_JOB_RELATE', null);
			}
			
		}
		if (!empty($taskCardID)) {
			$result = $this->getModel('TaskCardTable')->updateDeleteTaskCard($taskCardID);
			if ($result) {
				return $this->returnSuccess(array($taskCode), 'SUCC_TSKCRD_DELETE');
			} else {
				return $this->returnError('ERR_TSKCRD_DEL', null);
			}
		} else {
			return $this->returnError('ERR_TSKCRD_DELETE', null); 
		}
	}

	public function getTaskCardTaskProduct($data) {
		$results = $this->getModel('TaskCardTable')->getTaskCardByTaskCardID((int) $data['edittaskCardID']);
		if (!$results) {
			return $this->returnError('ERR_TSKPRD_LOAD', null);
		}

		foreach ($results as $key => $value) {
			$tasksList[$value['taskCardTaskID']] = array(
				'taskCardTaskID' => $value['taskCardTaskID'],
				'taskID' => $value['taskID'],
				'taskCode' => $value['taskCode'],
				'taskName' => $value['taskName'],
				'taskCardTaskRate1' => $value['taskCardTaskRate1'],
				'taskCardTaskRate2' => $value['taskCardTaskRate2'],
				'taskCardTaskRate3' => $value['taskCardTaskRate3'],
				'taskCardTaskIncentiveValue' => $value['taskCardTaskIncentiveValue'],
				'uomID' => $value['taskCardUomId'],
				'uomName' => $value['taskCardUomName'],
				'uomAbbr' => $value['taskCardUomAbbr'],
			);
			if ($value['taskCardTaskProductID'] != NULL) {
				$productList[$value['taskCardTaskID']][] = array(
					'taskCardTaskProductID' => $value['taskCardTaskProductID'],
					'taskCardTaskID' => $value['taskCardTaskID'],
					'locationProductID' => $value['locationProductID'],
					'taskCardTaskProductName' => $value['taskCardTaskProductName'],
					'taskCardTaskProductQty' => $value['taskCardTaskProductQty'],
					'productID' => $value['productID'],
					'productCode' => $value['productCode'],
					'taskCardTaskProductUomID' => $value['tcProductUomId'],
					'taskCardTaskProductUomName' => $value['tcProductUomName'],
					'taskCardTaskProductUomAbbr' => $value['tcProductUomAbbr'],
				);
			}
		}
		// get task rates by taskCardID
		$taskRates = $this->getModel('TaskCardTaskUnitPriceTable')->getTaskRatesByTaskCardID($data['edittaskCardID']);
		$taskRate = [];
		foreach ($taskRates as $key => $value) {
			$taskRate[$value['taskCardTaskID']][] = $value;
		} 

		foreach ($tasksList as $key => $value) {
			$tasksList[$key]['productList'] = $productList[$key];
			$tasksList[$key]['priceList'] = $taskRate[$key];
		}
			
		return $this->returnSuccess($tasksList, 'SUCC_TSKPRD_LOAD');
	}

	/**
	 * This function is used to search task card for dropdown
	 */
	public function searchTaskCardForDropdown($data) {
		$searchKey = $data['searchKey'];
		$documentType = $data['documentType'];

		$taskCard = $this->getModel('TaskCardTable')->getTaskCardforSearch($searchKey);
		$taskList = array();
		foreach ($taskCard as $dep) {
			$temp['value'] = $dep['taskCardID'];

			$temp['text'] = $dep['taskCardCode'].' - '.$dep['taskCardName'];
			$taskList[] = $temp;
		}
		return $taskList;
	}

	public function searchJobCardForDropdown($data) {
		$searchKey = $data['searchKey'];
		$documentType = $data['documentType'];
		$vehicleType = $data['vehicleType'];


		$taskCard = $this->getModel('TaskCardTable')->getJobCardforSearch($searchKey, $vehicleType);
		$taskList = array();
		foreach ($taskCard as $dep) {
		// var_dump($dep);
			$temp['value'] = $dep['taskCardID'];

			$temp['text'] = $dep['taskCardCode'].' - '.$dep['taskCardName'];
			$taskList[] = $temp;
		}
		// die();
		return $taskList;
	}

	public function searchJobCardForDropdownForDashboard($data) {
		$searchKey = $data['searchKey'];
		$documentType = $data['documentType'];
		$vehicleType = $data['vehicleType'];


		$taskCard = $this->getModel('TaskCardTable')->getJobCardforSearchForDashboard($searchKey, $vehicleType);
		$taskList = array();
		foreach ($taskCard as $dep) {
			$temp['value'] = $dep['taskCardID'];

			$temp['text'] = $dep['taskCardCode'].' - '.$dep['taskCardName'];
			$taskList[] = $temp;
		}
		return $taskList;
	}

	/***
		     * This function is used to get task card details by task card id
		     *
	*/
	public function getTaskCardDetailsByTaskCardIDs($data) {
		$taskCardIDs = $data['taskCardIDs'];

		if (!is_array($taskCardIDs)) {
			$taskCardIdArray = array($taskCardIDs);
		} else {
			$taskCardIdArray = $taskCardIDs;
		}

		$taskCardTaskArray = $this->getModel('TaskCardTaskTable')->getTaskCardTaskByTaskCardIds($taskCardIdArray);
		$locationProducts;

		$taskCardTasks = [];
		foreach ($taskCardTaskArray as $key => $value) {
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardID'] = $value['taskCardID'];
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardTaskID'] = $value['taskCardTaskID'];
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskID'] = $value['taskID'];
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskName'] = $value['taskName'];
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCode'] = $value['taskCode'];
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardTaskRate1'] = $value['taskCardTaskRate1'];
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardTaskRate2'] = $value['taskCardTaskRate2'];
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardTaskRate3'] = $value['taskCardTaskRate3'];
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['uomID'] = $value['taskCarduomID'];
			$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardUomAbbr'] = $value['taskCardUomAbbr'];

			if (!is_null($value['taskCardTaskProductID'])) {
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['taskCardTaskProductID'] = $value['taskCardTaskProductID'];
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['locationProductID'] = $value['locationProductID'];
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['productID'] = $value['productID'];
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['taskCardTaskProductQty'] = $value['taskCardTaskProductQty'];
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['productCode'] = $value['productCode'];
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['productName'] = $value['productName'];
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['tradingGoods'] = $value['productHandelingManufactureProduct'];
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['rawMaterials'] = $value['productHandelingPurchaseProduct'];
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['fixedAssets'] = $value['productHandelingFixedAssets'];
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'][$value['taskCardTaskProductID']]['finishedGoods'] = $value['productHandelingSalesProduct'];
				$locationProduct[$value['locationProductID']] = $this->getLocationProductDetails($value['locationID'], $value['productID']);
			} else {
				$temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']]['taskCardProducts'] = [];
			}

			$taskCardTasks[$value['taskCardID'] . '_' . $value['taskCardTaskID']] = $temp[$value['taskCardID'] . '_' . $value['taskCardTaskID']];
		}

		// get task rates
		$taskRates = $this->getModel('TaskCardTaskTable')->getTaskCardTaskWithRates($taskCardIdArray);
		$ratesArray = [];
		foreach ($taskRates as $key => $value) {
			$ratesArray[$value['taskCardTaskID']][] = $value['rates'];

		} 
			
		$data = array(
			'taskCardTask' => $taskCardTasks,
			'locationProducts' => $locationProduct,
			'ratesSet' => $ratesArray
		);
		return $data;
	}

	/**
     * This function is used to get job card related services
     */
    public function getRelatedServices($data)
    {
        $taskCardID = ($data['taskCardID'] == "")? null: $data['taskCardID'];
        $mode = ($data['mode'] == 'jobView') ? true: false;
        $relatedServices = $this->getModel('TaskCardTaskTable')->getRelatedServices($taskCardID, $mode);
        
        if ($relatedServices) {
        	$services = [];
        	$subTaskVerifyArray = [];

	        foreach ($relatedServices as $key => $value) {
	        	$result = $this->getModel('ServiceSubTaskTable')->getRelatedSubTasks($value['taskID']);


	            $services[$value['taskID']] = $value['taskCode'].'-'.$value['taskName'];
	            $subTaskVerifyArray[$value['taskID']] = ($result->count() > 0) ? true : false;
	        }

	        $data = [
	        	'services' => $services,
	        	'subTaskVerifyArray' => $subTaskVerifyArray 
	        ];
	        
            return $this->returnSuccess($data,null);
        } else {
            return $this->returnError('ERR_RETRIVE_JOB_CRD_RELATED_SER',null);
        }

    }

    /**
     * This function is used to get job card related services
     */
    public function getRelatedVehicleTypes($data)
    {
        $taskCardID = ($data['taskCardID'] == "")? null: $data['taskCardID'];
        // $mode = ($data['mode'] == 'jobView') ? true: false;
        $relatedVehicleTypes = $this->getModel('jobCardVehicleTypeTable')->getRelatedVehicleTypes($taskCardID);
        
        if ($relatedVehicleTypes) {
        	$vehicleTypes = [];
            foreach ($relatedVehicleTypes as $key => $value) {
                $vehicleTypes[] = $value['vehicleTypeID'];
            }

	        $data = [
	        	'vehicleTypes' => $vehicleTypes,
	        ];
	        
            return $this->returnSuccess($data,null);
        } else {
            return $this->returnError('ERR_RETRIVE_JOB_CRD_RELATED_SER',null);
        }

    }

    /**
     * This function is used to get related job card related services details
     **/
    public function viewRelatedServicesDetails($data)
    {
        $jobCardID = ($data['taskCardID'] == "") ? null: $data['taskCardID'];
        $jobCardServices = $this->getModel('TaskCardTaskTable')->getRelatedServices($jobCardID);

        $services = [];
        foreach ($jobCardServices as $key => $value) {
            $services[] = ['name' => $value['taskName'], 'code' => $value['taskCode'], 'status' => $value['taskCardTaskStatus'], 'taskCardTaskID' => $value['taskCardTaskID'], 'taskID' => $value['taskID']];
        }

        if ($jobCardServices) {
            return $this->returnSuccess($services,null);
        } else {
            return $this->returnError('ERR_RETRIVE_SUB_TSK_RELATED_SER',null);
        }
            
    }

    /**
     * This function is used to change the job card service status
     **/
    public function changeJobCardServiceStatus($data)
    {
        $jobCardServiceID = ($data['jobCardServiceId']) ? $data['jobCardServiceId']: null;
        $status = $data['taskCardTaskStatus'];
        
        $result = $this->getModel('TaskCardTaskTable')->changeJobCardServiceStatus($jobCardServiceID, $status);

        if ($result) {
            return $this->returnSuccess(null,'SUCC_JOB_CRD_SRVC_STATUS_CHANGE');
        } else {
            return $this->returnError('ERR_JOB_CRD_SRVC_STATUS_CHANGE',null);
        }
            
    }

    /**
     * This function is used to job card status
     **/
    public function changeStatusID($data)
    {
        $jobCardID = ($data['jobCardId']) ? $data['jobCardId']: null;
        $status = $data['status'];
        $result = $this->getModel('TaskCardTable')->changeStatusID($jobCardID, $status);

        if ($result) {
            return ($type == 'service') ? $this->returnSuccess(null,'SUCC_SER_STATUS_CHANGE') : $this->returnSuccess(null,'SUCC_JOB_CRD_STATUS_CHANGE');
        } else {
            return ($type == 'service') ? $this->returnError('ERR_SER_STATUS_CHANGE',null) : $this->returnError('ERR_JOB_CRD_STATUS_CHANGE',null);
        }
            
    }

    /**
     * This function is used to delete job card related services
     **/
    public function deleteJobCardService($data)
    {
        
        $taskCardTaskId = ($data['taskCardTaskID']) ? $data['taskCardTaskID']: null;
        $jobCardID = ($data['jobCardID']) ? $data['jobCardID']: null; 
        $taskID = ($data['taskID']) ? $data['taskID']: null;

        $taskCardDetails = $this->getModel('jobTaskTable')->getTaskDetailsByTaskCardId($jobCardID, $taskID);
		if ($taskCardDetails) {
			return $this->returnError('ERR_SRVC_DELETE_JOB_RELATE', null);
		}

        $result = $this->getModel('TaskCardTaskTable')->deleteJobCardServiceByTaskCardTaskID($taskCardTaskId);

        if ($result) {
            return $this->returnSuccess(null,'SUCC_JOB_CRD_SRVC_DELETE');
        } else {
            return $this->returnError('ERR_JOB_CRD_SRVC_DELETE',null);
        }
            
    }
}
