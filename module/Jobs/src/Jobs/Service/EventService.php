<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Jobs\Model\Job;
use Jobs\Model\JobEmployee;
use Jobs\Model\JobManager;
use Jobs\Model\JobProduct;
use Jobs\Model\JobSupervisor;
use Jobs\Model\JobTask;
use Jobs\Model\JobVehicle;
use Jobs\Model\JobSubTask;
use Jobs\Model\JobTaskProduct;
use Jobs\Model\JobContractor;
use Jobs\Model\JobTaskContractor;
use Zend\View\Model\ViewModel;
use Jobs\Model\ServiceVehicle;

class EventService extends BaseService {

	/**
	 * This function is used to save job
	 * @param JSON Data
	 */
	public function saveJob($data, $userLocation, $userId, $type = 'construction') 
	{
		$jobType = $data['jobType'];
		$jobCode = $data['jobCode'];
		$jobName = $data['jobName'];
		$jobEditflag = $data['jobEditflag'];
		$projectID = ($data['projectID'] == "") ? null : $data['projectID'];

		$startDate = $data['startDate'];
		$endDate = $data['endDate'];
		$jobEstimatedCost = $data['jobEstimatedCost'];
		$tournamentOfficials = $data['tournamentOfficials'];
		$jobResources = $data['jobResources'][$jobCode];

		//check whether job code alredy in database or not.
		$referenceService = $this->getService('ReferenceService');
        $refData = $referenceService->getReferenceNumber(41, $locationID);
        $rid = $refData['data']["referenceNo"];
        $locationReferenceID = $refData['data']["locationReferenceID"];

		$result = $this->getModel('JobTable')->getJobByCode($jobCode);
		if (count($result) > 0) {
			return $this->returnError('ERR_EVENT_CODE_EXIT', null);
		} 

		//get entityID from entity service
		$entityService = $this->getService('EntityService');
		$entityID = $entityService->createEntity($userId)['data']['entityID'];
		$statusName = 'Open';
		$statusID = $this->getStatusIDByStatusName($statusName);
		$jData = array(
			'jobName' => $jobName,
			'jobTypeId' => $jobType,
			'jobEstimatedCost' => $jobEstimatedCost,
			'projectId' => $projectID,
			'jobEstimatedEndDate' => $this->convertDateToStandardFormat($endDate),
			'jobStartDate' => $this->convertDateToStandardFormat($startDate),
			'jobReferenceNumber' => $jobCode,
			'locationID' => $userLocation,
			'entityID' => $entityID,
			'jobStatus' => $statusID
		);


		$jobData = new Job();
		$jobData->exchangeArray($jData);
		$jobID = $this->getModel('JobTable')->saveJob($jobData);

		if ($jobID) {
			//if the job supervisor is not empty, than save job supervisor details
			if (!is_null($tournamentOfficials)) {
				$saveJobPaticipants = $this->saveJobPaticipants($tournamentOfficials, $jobID);
				if (!$saveJobPaticipants) {
					return $this->returnError('ERR_SAVE_EVENT_PARTICIPANT', null);
				}
			}

			//If the resources is not empty, than save resources that are not related to tasks
			if (!is_null($jobResources)) {
				foreach ($jobResources as $key => $value) {
					$productKey = explode('_', $key);
					if (sizeof($productKey) == 2) {
						$saveProducts = $this->saveJobProducts($value, null, $jobID, $productKey[1], null);
						if (!$saveProducts) {
							return $this->returnError('ERR_SAVE_EVENT_EQUIPMENTS', null);
						}
					} else if (sizeof($productKey) == 4) {
						foreach ($jobTasks as $jobTaskKey => $jobtaskValue) {
							$taskProductKey = $productKey[1] . '_' . $productKey[2];
							if ($jobTaskKey == $taskProductKey) {
								$jobTaskDetails = $this->getModel('JobTaskTable')->getJobTaskDetails($jobID, $jobtaskValue['taskID'], $jobtaskValue['taskCardID'])->current();
								$jobTaskID = $jobTaskDetails['jobTaskID'];
								$saveTaskProducts = $this->saveJobProducts($value, $jobTaskID, $jobID, $productKey[3], $jobTaskDetails['jobTaskTaskCardID']);
								if (!$saveTaskProducts) {
									return $this->returnError('ERR_SAVE_JOB_TASK_PRODUCT', null);
								}
							}
						}
					}
				}
			}

			if ($locationReferenceID) {
				if ($jobCode == $rid) {
					$referenceService->updateReferenceNumber($locationReferenceID);
				}
			}

			if (!is_null($projectID)) {
				$result = $this->updateJobDetailsInProject($projectID);
				if (!$result) {
					return $this->returnError('ERR_UPDATE_TOURNAMENT_IN_EVENT', null);
				}
			}
			 $this->setLogMessage('Event '.$jobCode.' is created.');
			return $this->returnSuccess($jobCode, 'SUCC_SAVE_EVENT');
		} else {
			return $this->returnError('ERR_SAVE_EVENT', null);
		}
	}

	/**
	 * This function is used to update job details in project
	 * @param projectIDs
	 */
	public function updateJobDetailsInProject($projectID)
	{
		$jobDetails = $this->getModel('JobTable')->getJobDetailsByProjectID($projectID);
		$jobCount = count($jobDetails);


		$projectEstimatedCost = 0;
		foreach ($jobDetails as $key => $value) {
			$projectEstimatedCost += floatval($value['jobEstimatedCost']);
		}

		$data = array(
			'noOfJobs' => $jobCount,
			'eventEstimatedCost' => $projectEstimatedCost
		);

		$result = $this->getModel('ProjectTable')->updateProjectByArray($data, $projectID);
		return $result;
	}


	/**
	 * This function is used to save job supervisor
	 * @param $supervisorData
	 * @param $jobCode
	 */
	public function saveJobPaticipants($paricipantData, $jobID) {

		foreach ($paricipantData as $key => $value) {
			$data = array(
				'jobSupervisorID' => $value['participantID'],
				'participantTypeID' => $value['participantTypeID'],
				'participantSubTypeID' => $value['participantSubTypeID'],
				'teamID' => ($value['teamID'] == undefined) ? null : $value['teamID'],
				'jobID' => $jobID,
			);

			$jobSupervisorData = new JobSupervisor();
			$jobSupervisorData->exchangeArray($data);

			$saveSupervisor = $this->getModel('JobSupervisorTable')->saveSupervisor($jobSupervisorData);
			if (!$saveSupervisor) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job contarctor
	 * @param $contractorData
	 * @param $jobID
	 */
	public function saveJobContractor($contractorData, $jobID) {

		foreach ($contractorData as $key => $value) {
			$data = array(
				'contractorID' => $value,
				'jobID' => $jobID,
			);

			$jobContractorData = new JobContractor();
			$jobContractorData->exchangeArray($data);

			$saveContractor = $this->getModel('JobContractorTable')->saveContractor($jobContractorData);
			if (!$saveContractor) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job mangers
	 * @param $managerData
	 * @param $jobID
	 */
	public function saveJobManager($jobManagers, $jobID) {

		foreach ($jobManagers as $key => $value) {
			$data = array(
				'employeeID' => $value['jobManagerID'],
				'jobID' => $jobID,
			);

			$jobManagerData = new JobManager();
			$jobManagerData->exchangeArray($data);

			$saveManager = $this->getModel('JobManagerTable')->saveManager($jobManagerData);
			if (!$saveManager) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job tasks
	 * @param $jobTasks
	 * @param $jobID
	 */
	public function saveJobTasks($jobTasks, $jobID, $updateFlag = false) {
		$statusName = 'Open';
		$statusID = $this->getStatusIDByStatusName($statusName);
		foreach ($jobTasks as $key => $value) {
			if ($updateFlag) {
				$taskCardID = ($value['taskCardID'] == "") ? null : $value['taskCardID'];
				$checkForUpdate = $this->getModel('JobTaskTable')->getJobTaskDetails($jobID, $value['taskID'], $taskCardID)->current();
				if (!$checkForUpdate) {
					$result = $this->saveJobTaskData($value, $jobID, $statusID);
				} else {
					$result['status'] = true;
				} 
			} else {
				$result = $this->saveJobTaskData($value, $jobID, $statusID);
			}
			
			if (!$result['status']) {
				return ['status' => false, 'msg' => $result['msg']];
			}
		}

		return ['status' => true];
	}


	public function saveJobTaskData($value, $jobID, $statusID) {
		$taskRate = (empty($value['ratePerUnit1'])) ? $value['ratePerUnit2'] : $value['ratePerUnit1'];
		$data = array(
			'jobID' => $jobID,
			'taskID' => $value['taskID'],
			'uomID' => $value['UomId'],
			'jobTaskUnitCost' => $value['costPerUnit'],
			'jobTaskUnitRate' => $taskRate,
			'jobTaskEstQty' => $value['estQty'],
			'jobTaskStatus' => $statusID,
			'jobTaskTaskCardID' => $value['taskCardID'],
		);

		$jobTaskData = new JobTask();
		$jobTaskData->exchangeArray($data);

		$jobTaskID = $this->getModel('JobTaskTable')->saveJobTask($jobTaskData);

		if ($jobTaskID) {
			//If task employee is not empty, than save the task employees
			if (!is_null($value['employees'])) {
				$taskEmployeeResult = $this->saveTaskEmployees($value['employees'], $jobTaskID, $jobID);

				if (!$taskEmployeeResult) {
					return ['status' => false, 'msg' => "ERR_SAVE_JOB_TASK_EMPLOYEE"];
				}
			}

			//If task contractor is not empty, than save the task contractors
			if (!is_null($value['contactor'])) {
				$taskTaskContractorResult = $this->saveTaskContractors($value['contactor'], $jobTaskID, $jobID);

				if (!$taskTaskContractorResult) {
					return ['status' => false, 'msg' => "ERR_SAVE_JOB_TASK_CONTRACTOR"];
				}
			}

			if (!is_null($value['selectedSubTasks'])) {

				$jobSubTaskResult = $this->saveJobSubTasks($value['selectedSubTasks'], $jobTaskID);
				if (!$jobSubTaskResult) {
					return ['status' => false, 'msg' => "ERR_SAVE_JOB_SUB_TASK"];
				}
			}
		
			return ['status' => true];
		} else {
			return ['status' => false, 'msg' => 'ERR_SAVE_JOB_TASK'];
		}

	}
	/**
	 * This function is used to save job task employees
	 * @param $employeeData
	 * @param $jobID
	 * @param $jobTaskID
	 */
	public function saveTaskEmployees($employeeData, $jobTaskID, $jobID) {
		foreach ($employeeData as $key => $value) {
			$data = array(
				'jobID' => $jobID,
				'employeeDesignationID' => $value['employeeID'],
				'jobTaskID' => $jobTaskID,
			);

			$jobTaskEmployeeData = new JobEmployee();
			$jobTaskEmployeeData->exchangeArray($data);

			$jobTaskEmployeeID = $this->getModel('JobEmployeeTable')->saveJobTaskEmployee($jobTaskEmployeeData);

			if (!$jobTaskEmployeeID) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job task contractor
	 * @param $contractorData
	 * @param $jobID
	 * @param $jobTaskID
	 */
	public function saveTaskContractors($contractorData, $jobTaskID, $jobID) {
		foreach ($contractorData as $key => $value) {
			$data = array(
				'contractorID' => $value['contractorID'],
				'jobTaskID' => $jobTaskID,
			);

			$jobTaskContractorData = new JobTaskContractor();
			$jobTaskContractorData->exchangeArray($data);

			$jobTaskContractorID = $this->getModel('JobTaskContractorTable')->saveJobTaskContractor($jobTaskContractorData);

			if (!$jobTaskContractorID) {
				return false;
			}
		}
		return true;
	}

	/**
	 * This function is used to save job products
	 * @param $productData
	 * @param $jobID
	 * @param $jobTaskID
	 * @param $materialType
	 */
	public function saveJobProducts($productData, $jobTaskID = null, $jobID, $materialType, $taskCardID = null) {
		$materialTypeID = null;
		if ($materialType == "traG") {
			$materialTypeID = $this->getMaterialTypeID('Trading Items');
		} else if ($materialType == "fixA") {
			$materialTypeID = $this->getMaterialTypeID('Fixed Assets');
		} else if ($materialType == "raw") {
			$materialTypeID = $this->getMaterialTypeID('Raw Materials');
		}
		$pData = array(
			'jobID' => $jobID,
			'locationProductID' => $productData['jobMaterialTaskLocationProductID'],
			'jobProductUnitPrice' => $productData['jobMaterialProUnitPrice'],
			'uomID' => $productData['jobMaterialSelectedUomID'],
			'jobProductAllocatedQty' => $productData['jobMaterialProTotalQty'],
			'jobProductEstimatedQty' => $productData['jobMaterialProTotalQty'],
			'jobProductReOrderLevel' => $productData['jobMaterialReorderQty'],
			'jobProductMaterialTypeID' => $materialTypeID,
			'jobProductTaskCardID' => $taskCardID,
			'jobTaskID' => $jobTaskID,
		);

		$jobProductData = new JobProduct();
		$jobProductData->exchangeArray($pData);
		$jobProductID = $this->getModel('JobProductTable')->saveJobProduct($jobProductData);

		if (!$jobProductID) {
			return false;
		} else {
			return $jobProductID;
		}
	}

	/**
	 * This function is used to search projects for dropdown
	 */
	public function searchJobsForDropdown($data, $userLocation) {

		$searchKey = $data['searchKey'];
		$filterFlag = ($data['addFlag']) ? $data['addFlag']: null;
		$status = null;
		if ($filterFlag == "closed") {
			$status = 4;
			$filterFlag = null;
		} else {
			$filterFlag = $filterFlag;
		}

		$jobs = $this->getModel('JobTable')->jobSearchByKey($searchKey, $userLocation, null, null, $status, null, $filterFlag);
		$jobList = array();
		foreach ($jobs as $job) {
			$temp['value'] = $job['jobId'];
			$temp['text'] = $job['jobReferenceNumber'] . '-' . $job['jobName'];
			$jobList[] = $temp;
		}
		return $jobList;
	}

	/**
	 * This function is used to load jobs for dropdown
	 */
	public function loadJobsForDropdown($data) {



		$projectId = $data['projectId'];
		$progressFlag = ($data['progressFlag']) ? true: false;
		
		$jobs = $this->getModel('JobTable')->getJobByProjectId($projectId, $progressFlag);
		if ($jobs) {
			$jobList = array();
			foreach ($jobs as $job) {
				$temp['value'] = $job['jobId'];
				$temp['text'] = $job['jobReferenceNumber'] . '-' . $job['jobName'];
				$jobList[] = $temp;
			}
            return $this->returnSuccess(array('jobList' => $jobList), 'SUCC_LOAD_JOBS');
		} else {
			return $this->returnError('ERR_LOAD_JOBS', null);
		}
	}

	/**
	 * This function is used to get job by search key
	 */
	public function getJobBySearchKey($data, $param, $locationID) {
		$paginated = false;
		$jobSearchKey = ($data['jobSearchKey'] == "") ? null: $data['jobSearchKey'];
		$projectID = ($data['projectID'] == "") ? null : $data['projectID'];
		$jobSupervisorID = ($data['jobSupervisorID'] == "") ? null : $data['jobSupervisorID'];
		$jobStatusID = ($data['jobStatusID'] == "") ? null : $data['jobStatusID'];

		if ($jobSearchKey != null || $projectID != null || $jobSupervisorID != null || $jobStatusID != null) {
			$tbl = $this->getModel('JobTable');
			$jobList = $tbl->jobSearchByKey($jobSearchKey, null, $projectID, $jobSupervisorID, $jobStatusID);
		} else {
			$job = $this->getServiceLocator()->get("EventController");
			$jobList = $job->getPaginatedJob($locationID, $param);
			$paginated = true;
		}

		$view = new ViewModel(array(
			'jobList' => $jobList,
			'paginated' => $paginated,
            'status' => $this->getStatusesList(),
		));

		$view->setTerminal(true);
		$view->setTemplate('jobs/event/list-table.phtml');

		return $view;
	}

	/**
	 * This function is used to get job supervisor details by job ID
	 * @param $jobID
	 */
	public function getJobSupervisorsByJobID($data)
	{
		$jobSupervisors = $this->getModel('JobSupervisorTable')->getJobSupervisorsDetailsByJobID($data['jobID']);
		return $jobSupervisors;
	}

	/**
	 * This function is used to delete job by job ID
	 * @param $jobID
	 */
	public function deleteJob($data, $userId, $projectRelatedJobDeletion = false)
	{
		$jobID = $data['jobID'];
		$jobData = (object) $this->getModel('JobTable')->getJobDetails($jobID)->current();
        if ($jobData->jobStatus == 3) {
        	$entityService = $this->getService('EntityService');
			$updatedData = $entityService->updateDeleteInfoEntity($jobData->entityID,$userId);
            $statusName = 'Cancel';
            $statusID = $this->getStatusIDByStatusName($statusName);
            $updateJobData = $this->getModel('JobTable')->updateJobstatusID($jobID, $statusID);

            $projectID = ($projectRelatedJobDeletion) ? $data['projectID'] : $jobData->projectId;
            if ($updatedData && $updateJobData) {
            	if (!is_null($jobData->projectId)) {
					$result = $this->updateJobDetailsInProject($projectID);
					if (!$result) {
						return $this->returnError('ERR_UPDATE_TOURNAMENT_IN_EVENT', null);
					}
				}
				$this->setLogMessage('Event '.$jobData->jobReferenceNumber.' is deleted.');
                return $this->returnSuccess(null, 'SUCC_EVENT_DELETE');
            } else {
            	return $this->returnError('ERR_EVENT_DELETE', null);
            }
        } else {
        	return $this->returnError('ERR_EVENT_USED_DELETE', null);
        }
	}

	/**
	 * This function is used to get data for update job
	 * @param $jobID
	 */
	public function getJobDataForUpdate($data)
	{
		$jobData = $this->getModel('JobTable')->getJobByJobID($data['jobID']);
        $jobDetails = array();
        $jobID;
        foreach ($jobData as $row) {
            $tempG = array();
            $jobID = $row['jobId'];
            $tempG['jobId'] = $row['jobId'];
            $tempG['jobCode'] = $row['jobReferenceNumber'];
            $tempG['jobName'] = $row['jobName'];
            $tempG['projectId'] = $row['projectId'];
            $tempG['jobCustomer'] = $row['customerCode'].'-'.$row['customerName'];
            $tempG['jobCustomerID'] = $row['customerID'];
            $tempG['jobType'] = $row['jobTypeCode'].'-'.$row['jobTypeName'];
            $tempG['jobDueDate'] = $this->convertDateToUserFormat($row['jobDueDate']);
            $tempG['jobEstimatedEndDate'] = $this->convertDateToUserFormat($row['jobEstimatedEndDate']);
            $tempG['jobValue'] = $row['jobWeight'];
            $tempG['jobEstimatedCost'] = $row['jobEstimatedCost'];
            $jobTasks = (isset($jobDetails[$row['jobId']]['jobTasks'])) ? $jobDetails[$row['jobId']]['jobTasks'] : array();
            $jobProducts = (isset($jobDetails[$row['jobId']]['jobProducts'])) ? $jobDetails[$row['jobId']]['jobProducts'] : array();
            $jobSupervisors = (isset($jobDetails[$row['jobId']]['jobSupervisors'])) ? $jobDetails[$row['jobId']]['jobSupervisors'] : array();
            $jobManagers = (isset($jobDetails[$row['jobId']]['jobManagers'])) ? $jobDetails[$row['jobId']]['jobManagers'] : array();
            $jobContractors = (isset($jobDetails[$row['jobId']]['jobContractors'])) ? $jobDetails[$row['jobId']]['jobContractors'] : array();

            if (!is_null($row['jobManagerID'])) {
            	$jobManagers[$row['jobReferenceNumber']][$row['employeeIdOfJobManager']] = array('jobManagerID' => $row['employeeIdOfJobManager'], 'jobManagerCodeAndName' => $row['jobMangerFirstName'].' '.$row['jobMangerSecondName']);
            }

            if (!is_null($row['jobSupervisorTableID'])) {
            	if (is_null($row['teamID'])) {
            		$jobSupervisors[$row['participantTypeID'].'-'.$row['participantSubTypeID'].'-'.$row['jobSupervisorID']] = array('participantID' => $row['jobSupervisorID'],'participantTypeID' => $row['participantTypeID'],'participantSubTypeID' => $row['participantSubTypeID'], 'teamID' => $row['teamID']);
            	} else {
            		$jobSupervisors[$row['participantTypeID'].'-'.$row['participantSubTypeID'].'-'.$row['jobSupervisorID'].'-'.$row['teamID']] = array('participantID' => $row['jobSupervisorID'],'participantTypeID' => $row['participantTypeID'],'participantSubTypeID' => $row['participantSubTypeID'], 'teamID' => $row['teamID']);
            	}
            }

            if (!is_null($row['jobContractorID'])) {
            	$jobContractors[$row['contractorIdOfJob']] = array('contractorName' => $row['jobContractorFirstName'].' '.$row['jobContractorLastName']);
            }

            if (!is_null($row['jobProductID'])) {

            	$productUom = $this->getModel('ProductUomTable')->getProductUomListByProductID($row['productID']);

                $thisqty = $this->getProductQuantityViaDisplayUom($row['jobProductAllocatedQty'], $productUom);
                $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

            	$jobProducts[$row['jobProductID']] = array('locationProductID' => $row['locationProductID'], 'jobProductUnitPrice' => $row['jobProductUnitPrice'], 'uomID' => $row['uomID'], 'jobProductAllocatedQty' => $thisqty['quantity'], 'jobProductReOrderLevel' => $row['jobProductReOrderLevel'], 'jobProductMaterialTypeID' => $row['jobProductMaterialTypeID'],'productCode' => $row['productCode'], 'productName' => $row['productName'],'uomAbbr' => $row['uomAbbr'], 'productEstimatedCost' => floatval($row['jobProductAllocatedQty']) * floatval($row['jobProductUnitPrice']), 'jobProductTaskID' => $row['jobProductTaskID'],'jobProductTaskCardID' => $row['jobProductTaskCardID'],'productID' => $row['productID'],'jobProductStatus' => $row['jobProductStatus'],'jobProductID' => $row['jobProductID']);
            }

            if ($row['jobTaskID'] != NULL) {
                $taskKey = $row['jobTaskID'];

                $taskEmployees = (isset($jobTasks[$taskKey]['jobEmployees'])) ? $jobTasks[$taskKey]['jobEmployees'] : array();

                 $taskContractors = (isset($jobTasks[$taskKey]['taskContractors'])) ? $jobTasks[$taskKey]['taskContractors'] : array();

                $jobTasks[$taskKey] = array('taskCode' => $row['taskCode'], 'taskName' => $row['taskName'], 'jobTaskUnitCost' => $row['jobTaskUnitCost'],'jobTaskUnitRate' => $row['jobTaskUnitRate'], 'jobTaskEstQty' => $row['jobTaskEstQty'], 'taskUomID' => $row['taskUomID'], 'taskUomAbbr' => $row['taskUomAbbr'],'taskEstimatedCost' => floatval($row['jobTaskEstQty']) * floatval($row['jobTaskUnitCost']),'jobTaskID' => $row['jobTaskID'],'taskCardID' => $row['jobTaskTaskCardID'],'taskID' => $row['taskID'],'jobTaskStatus' => $row['jobTaskStatus']);
                
                if ($row['jobEmployeeID'] != NULL && $row['jobTaskID'] == $row['employeeJobTaskID']) {
                    $taskEmployees[$row['jobEmployeeID']] = array('employeeName' => $row['jobEmployeeFirstName'].' '.$row['jobEmployeeSecondName'].' ('.$row['designationName'].')', 'employeeJobTaskID' => $row['employeeJobTaskID'], 'employeeDesignationID' => $row['employeeDesignationID']);
                }
                
                if ($row['jobTaskContractorID'] != NULL && $row['jobTaskID'] == $row['contactorJobTaskID']) {
                    $taskContractors[$row['jobTaskContractorID']] = array('contractorName' => $row['jobTaskContractorFirstName'].' '.$row['jobTaskContractorLastName'], 'contactorJobTaskID' => $row['contactorJobTaskID'],'contactorIdOfJobTask' => $row['contactorIdOfJobTask']);
                }

                $jobTasks[$taskKey]['jobEmployees'] = $taskEmployees;
                $jobTasks[$taskKey]['taskContractors'] = $taskContractors;
            }
            
            $tempG['jobManagers'] = $jobManagers;
            $tempG['jobContractors'] = $jobContractors;
            $tempG['jobSupervisors'] = $jobSupervisors;
            $tempG['jobProducts'] = $jobProducts;
            $tempG['jobTasks'] = $jobTasks;
            $jobDetails[$row['jobId']] = $tempG;
        }
        
        return $jobDetails[$jobID];
	}
	
	/**
	 * This function is used to get job contractor details by job ID
	 * @param $jobID
	 */
	public function getJobContractorsByJobID($data)
	{
		$jobContractors = $this->getModel('JobContractorTable')->getJobContractorDetailsByJobID($data['jobID']);
		$jobContractorData = [];

		foreach ($jobContractors as $key => $value) {
			$jobContractorData[] = $value;
		}

		return $jobContractorData;
	}

	/**
	 * This function is used to update job
	 * @param $JSON Request
	 * @param $userLocation
	 * @param $userID
	 */
	public function updateJob($data, $userLocation, $userId) 
	{
		$jobType = $data['jobType'];
		$jobCode = $data['jobCode'];
		$jobName = $data['jobName'];
		$jobEditflag = $data['jobEditflag'];
		$projectID = ($data['projectID'] == "") ? null : $data['projectID'];
		$endDate = $data['endDate'];
		$jobEstimatedCost = $data['jobEstimatedCost'];
		$startDate = $data['startDate'];
		$tournamentOfficials = $data['tournamentOfficials'];
		$jobResources = $data['jobResources'][$jobCode];
		$editedJobID = $data['editedJobID'];
		$oldData = (object) $this->getModel('JobTable')->getJobDetails($editedJobID)->current();
		$jData = array(
			'jobName' => $jobName,
			'jobTypeId' => $jobType,
			'jobEstimatedCost' => $jobEstimatedCost,
			'projectId' => $projectID,
			'jobEstimatedEndDate' => $this->convertDateToStandardFormat($endDate),
			'jobStartDate' => $this->convertDateToStandardFormat($startDate),
			'locationID' => $userLocation
		);
		
		$jobUpdateStatus = $this->getModel('JobTable')->updateJobData($editedJobID, $jData);

		if ($jobUpdateStatus) {
			// update entity for job update			
			$jobData = $this->getModel('JobTable')->getJobsByJobID($editedJobID);
			$entityService = $this->getService('EntityService');
        	$entityID = $entityService->updateEntity($jobData->entityID, $userId);

			//update job supervisor details
			$updateEventParticipants = $this->updateEventParticipants($tournamentOfficials, $editedJobID);
			if (!$updateEventParticipants['status']) {
				return $this->returnError($updateEventParticipants['msg'], null);
			}
			
			//update job product details
			$updateEquipments = $this->updateEquipments($jobResources, $editedJobID);
			if (!$updateEquipments['status']) {
				return $this->returnError($updateEquipments['msg'], null);
			}
			
			if (!is_null($projectID)) {
				$result = $this->updateJobDetailsInProject($projectID);
				if (!$result) {
					return $this->returnError('ERR_UPDATE_TOURNAMENT_IN_EVENT', null);
				}
			}
			$changeArray = $this->getEventChageDetails($oldData, $data);
            //set log details
            $previousData = json_encode($changeArray['previousData']);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("Event ".$oldData->jobReferenceNumber." is updated.");
			return $this->returnSuccess($jobCode,'SUCC_UPDATE_EVENT');
		} else {
			return $this->returnError('ERR_UPDATE_EVENT',null);
		}
	}

	public function getEventChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Event code'] = $row->jobReferenceNumber;
        $previousData['Event name'] = $row->jobName;
        $previousData['Event estimated cost'] = $row->jobEstimatedCost;
        $previousData['Event start date'] = $this->convertDateToStandardFormat($row->jobStartDate);
        $previousData['Event end date'] = $this->convertDateToStandardFormat($row->jobEstimatedEndDate);

        $newData = [];
        $newData['Event code'] = $data['jobCode'];
        $newData['Event name'] = $data['jobName'];
        $newData['Event estimated cost'] = $data['jobEstimatedCost'];
        $newData['Event start date'] = $this->convertDateToStandardFormat($data['startDate']);
        $newData['Event end date'] = $this->convertDateToStandardFormat($data['endDate']);

        return array('previousData' => $previousData, 'newData' => $newData);
    }

	/**
	 * This function is used to update job supervisors
	 * @param $jobSupervisorData
	 * @param $jobID
	 * @return resultSet
	 */
	public function updateEventParticipants($tournamentOfficials, $jobID)
	{
		$deleteExistingJobSupervisors = $this->getModel('JobSupervisorTable')->deleteJobSupervisorByJobID($jobID);
		if ($deleteExistingJobSupervisors >= 0) {
			//if the job supervisor is not empty, than save job supervisor details
			if (!is_null($tournamentOfficials)) {
				$saveJobPaticipants = $this->saveJobPaticipants($tournamentOfficials, $jobID);
				if (!$saveJobPaticipants) {
					return $this->returnError('ERR_SAVE_EVENT_PARTICIPANT', null);
				}
				return $this->returnSuccess(null,'');
			}
			return $this->returnSuccess(null,'');
		}
	}

	/**
	 * This function is used to update job contractors
	 * @param $jobContractorData
	 * @param $jobID
	 * @return resultSet
	 */
	public function updateJobContractor($jobContractor, $jobID)
	{
		$deleteExistingJobContractor = $this->getModel('JobContractorTable')->deleteJobContractorByJobID($jobID);

		if ($deleteExistingJobContractor >= 0) {
			//if the job contractor is not empty, than save job contractor details
			if (!is_null($jobContractor)) {
				$saveJobContractor = $this->saveJobContractor($jobContractor, $jobID);
				if (!$saveJobContractor) {
					return $this->returnError('ERR_SAVE_JOB_CONTRACTOR', null);
				}
				return $this->returnSuccess(null,'');
			}
			return $this->returnSuccess(null,'');
		}
	}

	/**
	 * This function is used to update job mangaers
	 * @param $jobMangaerData
	 * @param $jobID
	 * @return resultSet
	 */
	public function updateJobManager($jobManagers, $jobID)
	{
		$deleteExistingJobManager = $this->getModel('JobManagerTable')->deleteJobManagerByJobID($jobID);

		if ($deleteExistingJobManager >= 0) {
			//if the job managers is not empty, than save job managers
			if (!is_null($jobManagers)) {
				$saveJobManager = $this->saveJobManager($jobManagers, $jobID);
				if (!$saveJobManager) {
					return $this->returnError('ERR_SAVE_JOB_MANAGER', null);
				}
				return $this->returnSuccess(null,'');
			}
			return $this->returnSuccess(null,'');
		}
	}

	/**
	 * This function is used to update job tasks
	 * @param $jobTaskData
	 * @param $jobID
	 * @return resultSet
	 */
	public function updateJobTask($jobTasks, $jobID)
	{
		$existingJobTasks = $this->getModel('JobTaskTable')->getJobTaskDetails($jobID, null, null, 3);
		
		foreach ($existingJobTasks as $key => $value) {
			$deleteExistingJobTaskEmployee = $this->getModel('JobEmployeeTable')->deleteJobTaskEmployeeByJobTaskID($value['jobTaskID']);
			$deleteExistingJobTaskContractor = $this->getModel('JobTaskContractorTable')->deleteJobTaskContractorByJobTaskID($value['jobTaskID']);

			$deleteExistingJobSubTasks = $this->getModel('JobSubTaskTable')->deleteJobSubTaskJobTaskID($value['jobTaskID']);
			$deleteExistingJobTask = $this->getModel('JobTaskTable')->deleteJobTaskByJobTaskID($value['jobTaskID']);
		}

		//If the job tasks is not empty, than save job tasks.
		if (!is_null($jobTasks)) {
			$saveJobTasks = $this->saveJobTasks($jobTasks, $jobID, true);
			if (!$saveJobTasks['status']) {
				return $this->returnError($saveJobTasks['msg'], null);
			}
		}
		return $this->returnSuccess(null,'');
	}

	/**
	 * This function is used to update job products
	 * @param $jobresourceData
	 * @param $jobID
	 * @param $jobtaskData
	 * @return resultSet
	 */
	public function updateEquipments($jobResources, $jobID) 
	{
		$existingJobProducts = $this->getModel('JobProductTable')->getJobProductByJobID($jobID);

		foreach ($existingJobProducts as $key => $value) {
			if (is_null($value->jobProductStatus)) {
				$deleteExistingJobProducts = $this->getModel('JobProductTable')->deleteJobProductByJobProductID($value->jobProductID);
			}
		}

		//If the resources is not empty, than save resources that are not related to tasks
		if (!is_null($jobResources)) {
			foreach ($jobResources as $key => $value) {
				$checkExistJobProduct = $this->getModel('JobProductTable')->getJobProductByJobProductID($value['jobMaterialEditedProductID']);
				$productKey = explode('_', $key);
				if (is_null($checkExistJobProduct)) {
					if (sizeof($productKey) == 2) {
						$saveProducts = $this->saveJobProducts($value, null, $jobID, $productKey[1], null);
						if (!$saveProducts) {
							return $this->returnError('ERR_SAVE_EVENT_EQUIPMENTS', null);
						}
					}
				}
			}
		}
		return $this->returnSuccess(null,'');
	}

    public function searchJobProjectForDropdown($data)
    {
    	$jobSearchKey = $data['searchKey'];

        $locationID = $this->user_session->userActiveLocation["locationID"];
        $jobs = $this->getModel('JobTable')->searchJobsAndProJectForDropDown($locationID, $jobSearchKey);
        
        $jobList = array();
        foreach ($jobs as $job) {
        	if ($job['jobReferenceNumber'] != null) {
	            $temp['value'] = $job['jobId'];
	            if ($job['projectCode'] != null) {
	            	$temp['text'] = $job['jobReferenceNumber'] . " - " . $job['jobName']." of project ". $job['projectCode']." - ".$job['projectName'] ;
	            	
	            } else {
	            	$temp['text'] = $job['jobReferenceNumber'] . " - " . $job['jobName'];
	            }
	            $jobList[] = $temp;
        		
        	}
        }
        
        return $jobList;
    }

    public function getJobTaskWithProductDetails($data)
    {
    	$status = [18]; // status 18 mean ended.
    	// get current location ID
    	$locationID = $this->user_session->userActiveLocation["locationID"];
    	// get completed job task with their products for given jobID
    	$jobTaskDetails = $this->getModel('JobTaskTable')->getJobTaskWithProductDetails($data['jobId'], $locationID, $status);

    	// set job task details and job task product details in to the two arrays
    	$jobTask = [];
    	$jobTaskProduct = [];
    	
    	foreach ($jobTaskDetails as $key => $jobTaskSingleRecord) {
    		
    		// check all qty invoiced or not
    		if ($jobTaskSingleRecord['jobTaskInvoicedQty'] < $jobTaskSingleRecord['jobTaskUsedQty']) {
    			// set data to the job task array
	    		$jobTask[$jobTaskSingleRecord['jobTaskID']] = [
	    			'jobTaskID' => $jobTaskSingleRecord['jobTaskID'],
	    			'taskID' => $jobTaskSingleRecord['taskID'],
	    			'jobTaskUnitCost' => floatval($jobTaskSingleRecord['jobTaskUnitCost']),
	    			'jobTaskUnitRate' => floatval($jobTaskSingleRecord['jobTaskUnitRate']),
	    			'jobTaskQty' => floatval($jobTaskSingleRecord['jobTaskUsedQty']),
	    			'taskCode' => $jobTaskSingleRecord['taskCode'],
	    			'taskName' => $jobTaskSingleRecord['taskName'],
	    			'uomName' => $jobTaskSingleRecord['taskUomAbbr']
	    		];
    			
    		}
    		
    		// set data to the job task product array
    		if ($jobTaskSingleRecord['jobProductID'] != null) {
	    		$jobTaskProduct[$jobTaskSingleRecord['jobTaskID']][$jobTaskSingleRecord['jobProductID']] = [
	    			'productCodeName' => $jobTaskSingleRecord['productCode'].' - '.$jobTaskSingleRecord['productName'],
	    			'uom' => $jobTaskSingleRecord['productUomAbbr'],
	    			'jobProductID' => $jobTaskSingleRecord['jobProductID'],
	    			'jobTaskID' => $jobTaskSingleRecord['jobTaskID'],
	    			'jobProductUnitPrice' => $jobTaskSingleRecord['jobProductUnitPrice'],
	    			'jobProductUsedQty' => $jobTaskSingleRecord['jobProductUsedQty'],
	    			'jobProductRemInvoicedQty' => floatval($jobTaskSingleRecord['jobProductUsedQty']) - floatval($jobTaskSingleRecord['jobProductInvoicedQty']),
	    			'locationProductID' => $jobTaskSingleRecord['locationProductID']
	    			
	    		];
    			
    		}
    	}

    	// get non tasked items
    	$jobProductDetails = $this->getModel('JobProductTable')->getNonTaskProductByJobIDAndLocationID($data['jobId'], $locationID);
    	$itemSetArray = [];
    	$addNonTaskProductFlag = false;
    	foreach ($jobProductDetails as $key => $jobProductSingle) {
    		
    		if ($jobProductSingle['jobProductInvoicedQty'] < $jobProductSingle['jobProductUsedQty']) {
    			$jobTaskProduct['default'][$jobProductSingle['jobProductID']] = [
    				'productCodeName' => $jobProductSingle['productCode'].' - '.$jobProductSingle['productName'],
    				'uom' => $jobProductSingle['uomAbbr'],
	    			'jobProductID' => $jobProductSingle['jobProductID'],
	    			'jobTaskID' => 'default',
	    			'jobProductUnitPrice' => $jobProductSingle['jobProductUnitPrice'],
	    			'jobProductUsedQty' => $jobProductSingle['jobProductUsedQty'],
	    			'jobProductRemInvoicedQty' => floatval($jobProductSingle['jobProductUsedQty']) - floatval($jobProductSingle['jobProductInvoicedQty']),
	    			'locationProductID' => $jobProductSingle['locationProductID']
    			];
    			$addNonTaskProductFlag = true;
    		}
    	}
    	if ($addNonTaskProductFlag) {
    		$jobTask['default'] = [
    			'jobTaskID' => null,
    			'taskID' => null,
    			'jobTaskUnitCost' => floatval(0),
    			'jobTaskUnitRate' => floatval(0),
    			'jobTaskQty' => floatval(1),
    			'taskCode' => 'def',
    			'taskName' => 'default',
    			'uomName' => '-'
    		];
    	}

    	return [
    		'jobTaskDetails' => $jobTask,
    		'jobTaskProductDetails' => $jobTaskProduct,
    	];
    	
    }

    /**
     * This function is used to get vehicle details
     */
    public function getVehicleDetails($data)
    {
        $vehicleRegNo = $data['vehicleRegNo'];
        $result = $this->getModel('ServiceVehicleTable')->getVehicleDetails($vehicleRegNo);
        
        
        if ($result) {
        	$respondData = $result->current();
            return $this->returnSuccess($respondData,null);
        } else {
            return $this->returnError('ERR_RETRIVE_JOB_CRD_RELATED_SER',null);
        }

    }

    /**
	 * This function is used process selected services as job tasks
	 * @param array $selectedService
	 * return array
	 */
	public function processJobServices($selectedService) {
		$jobTasks = [];
		foreach ($selectedService as $key => $value) {
			$temp = [];
			$temp['taskCardID'] = $value['jobCardID'];
			$temp['taskID'] = $key;
			$temp['selectedSubTasks'] = $value['selectedSubTasks'];
			$jobTasks[] = $temp;

		}

		return $jobTasks;
		
	}

	/**
	 * This function is used to save job sub task
	 * @param $subTaskData
	 * @param $jobTaskID
	 * return boolean
	 */
	public function saveJobSubTasks($subTaskData, $jobTaskID) {
		foreach ($subTaskData as $key => $value) {
			$data = array(
				'subTaskId' => $key,
				'jobTaskId' => $jobTaskID,
			);

			$jobSubTaskData = new JobSubTask();
			$jobSubTaskData->exchangeArray($data);

			$jobSubTaskID = $this->getModel('JobSubTaskTable')->saveJobSubTask($jobSubTaskData);

			if (!$jobSubTaskID) {
				return false;
			}
		}
		return true;
	}


	/**
	 * This function is used to save job vehicle data
	 * @param $vehicleId
	 * @param $jobID
	 * @param $kmCovered
	 */
	public function saveJobVehicleData($jobId, $kmCovered, $vehicleId) {

		$data = array(
			'vehicleID' => $vehicleId,
			'jobVehicleKMs' => $kmCovered,
			'jobId' => $jobId,
		);

		$jobVehicleData = new JobVehicle();
		$jobVehicleData->exchangeArray($data);
		$jobVehicleSave = $this->getModel('JobVehicleTable')->saveJobVehicle($jobVehicleData);
		
		if (!$jobVehicleSave) {
			return false;
		}

		return true;
	}

	/**
	 * This function is used to update job vehicle data
	 * @param $vehicleId
	 * @param $jobID
	 * @param $kmCovered
	 */
	public function updateJobVehicleData($jobId, $kmCovered, $vehicleId) {

		$getExsistingJobVehicle = $this->getModel('JobVehicleTable')->getJobVehicle($jobId, $vehicleId)->current();

		if ($getExsistingJobVehicle) {
			$deleteJobVehicle = $this->getModel('JobVehicleTable')->deleteJobVehicle($getExsistingJobVehicle['jobVehicleID']);

			if ($deleteJobVehicle) {
				$saveJobVehicle = $this->saveJobVehicleData($jobId, $kmCovered, $vehicleId);

				if (!$saveJobVehicle) {
					return false;
				}
			} else {
				return false;
			}
		} 
		return true;
	}

	/**
    * this function use to get job related services
    * @param array $data
    * return array
    **/
    public function getJobRelatedServicesByJobId($data, $jobDashboard = true)
    {
    	$jobId = $data['jobId'];
        $paginated = false;
            
        $serviceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($jobId);

        $list = [];
        foreach ($serviceList as $key => $value) {
        	$value['startedAt'] = ($value['startedAt'] != NULL) ? $this->getUserDateTime($value['startedAt']) : NULL;
        	$value['endedAt'] = ($value['endedAt'] != NULL) ? $this->getUserDateTime($value['endedAt']) : NULL;

        	$list [] = $value;
        }

        if ($serviceList) {
        	$view = new ViewModel(array(
	            'serviceList' => $list,
	            'status' => $this->getStatusesList(),
	        ));
	        $view->setTerminal(true);

	        if ($jobDashboard) {
	        	$view->setTemplate('jobs/service-job-dashboard/service-list.phtml');
	        } else {
	        	$view->setTemplate('jobs/service-jobs/view-services-modal.phtml');
	        }
	        
	        return [
	            'view' => $view,
	            'status' => true,
	            'data'=> $list
	        ];
        } else {
        	return $this->returnError('ERR_RETRIVE_JOB_RELATED_SER',null);
        }

    }

	/**
	 * This function is used to get job contractor details by job ID
	 * @param $jobID
	 */
	public function getServiceJobById($data, $locationID)
	{
		$jobID = $data['jobID'];
		$job = $this->getModel('JobTable')->getServiceJobById($jobID, $locationID);

		$relatedServices = $this->getModel('TaskCardTaskTable')->getRelatedServices($job['jobTaskTaskCardID']);
		$subTaskVerifyArr = [];
		$allServices = [];
		foreach ($relatedServices as $key => $value) {
			$allServices [$value['taskID']] = [
				'codeName' => $value['taskCode'].'-'.$value['taskName'],
				'jobCard' => $job['jobTaskTaskCardID']

			]; 
		}

		$selsectedServiceList = $this->getModel('JobTaskTable')->getJobRelatedServicesByJobId($job['jobId']);
		$selectedJobCardServices = [];
		$selectedotherService = [];
		$selectedServices = [];
		foreach ($selsectedServiceList as $key => $value) {
			$temp = [];
			$relatedSubTask = $this->getModel('ServiceSubTaskTable')->getRelatedSubTasks($value['taskID']);

			foreach ($relatedSubTask as $key => $val) {
				$isSelected = $this->getModel('JobSubTaskTable')->getSubTaskById($value['jobTaskID'], $val['subTaskId'])->current();

				$temp[$val['subTaskId']] = [
					'code' => $val['subTaskCode'],
					'selected' => ($isSelected) ? true : false
				]; 
			}
			$selectedServices [$value['taskID']] =  [
				'code' => $value['taskCode'],
				'jobCardID' => $value['jobTaskTaskCardID'],
				'selectedSubTasks' => $temp,
				'isStarted' => ($value['jobTaskStatus'] != 3) ? true : false
			];

			if ($value['jobTaskTaskCardID']) {
				$selectedJobCardServices [$value['taskID']] = $value['taskCode'].'-'.$value['taskName'];
			} else {
				$selectedotherService [$value['taskID']] = $value['taskCode'].'-'.$value['taskName'];
			}
		}


		foreach ($selectedotherService as $key => $value) {
			$allServices[$key] = $value;

			$allServices [$key] = [
				'codeName' => $value,
				'jobCard' => ''
			]; 
		}

		foreach ($allServices as $key => $value) {
			$result = $this->getModel('ServiceSubTaskTable')->getRelatedSubTasks($key);

			if ($result->count() > 0) {
				$subTaskVerifyArr[$key] = true;
			} else {
				$subTaskVerifyArr[$key] = false;
			}
		}

		$job['selectedJobCardServices'] = $selectedJobCardServices;
		$job['selectedotherService'] = $selectedotherService;
		$job['allServices'] = $allServices;
		$job['subTaskVerifyArr'] = $subTaskVerifyArr;
		$job['selectedServices'] = $selectedServices;
	
		if ($job) {
			return $this->returnSuccess($job,null);
		} else {
			return $this->returnError('ERR_RETRIVE_JOB_RELATED_SER',null);
		}

	}


    /**
	 * This function is used to save job vehicle data
	 * @param $vehicleId
	 * @param $jobID
	 * @param $kmCovered
	 */
	public function saveJobRelatedServiceProducts($jobTasks, $jobId) {

		foreach ($jobTasks as $key => $value) {

			if ($value['taskCardID']) {
				$products = $this->getModel('TaskCardTaskTable')->getTaskCardTaskProducts($value['taskID'], $value['taskCardID']);
				if (!$products) {
					return false;
				}

				$jobTaskDetails = $this->getModel('JobTaskTable')->getJobTaskDetails($jobId, $value['taskID'], $value['taskCardID'])->current();

				if (!$jobTaskDetails) {
					return false;
				}

				$jobTaskID = $jobTaskDetails['jobTaskID'];

				foreach ($products as $key => $val) {
					if ($val['locationProductID']) {
						$pData = array(
							'jobID' => $jobId,
							'locationProductID' => $val['locationProductID'],
							'jobProductUnitPrice' => $val['productDefaultSellingPrice'],
							'uomID' => $val['taskCardTaskProductUomID'],
							'jobProductAllocatedQty' => $val['taskCardTaskProductQty'],
							'jobProductRequestedQty' => $val['taskCardTaskProductQty'],
							'jobProductMaterialTypeID' => $val['taskCardTaskProductType'],
							'jobProductTaskCardID' => $value['taskCardID'],
							'jobTaskID' => $jobTaskID,
						);

						$jobProductData = new JobProduct();
						$jobProductData->exchangeArray($pData);
						$jobProductID = $this->getModel('JobProductTable')->saveJobProduct($jobProductData);
						
						if (!$jobProductID) {
							return false;
						}
					}
					
				}
				
			}
		}
		return true;
	}

	/**
	 * This function is used to save job vehicle data
	 * @param $vehicleId
	 * @param $jobID
	 * @param $kmCovered
	 */
	public function updateServiceJobProduct($jobTasks, $jobId) {

		$existingJobProducts = $this->getModel('JobProductTable')->getJobProductByJobID($jobId);
		foreach ($existingJobProducts as $key => $value) {	
			if (is_null($value->jobProductStatus)) {
				$deleteExistingJobProducts = $this->getModel('JobProductTable')->deleteJobProductByJobProductID($value->jobProductID);
			}
		}
		
		if (!is_null($jobTasks)) {
			$saveServiceProducts = $this->saveJobRelatedServiceProducts($jobTasks, $jobId);

			if (!$saveServiceProducts) {
				return false;
			}

		}
		return true;
	}

	/**
     * This function is used to get job by search key
     */
    public function getServiiceJobBySearchKey($data, $param, $locationID) {
        $paginated = false;
        $jobSearchKey = ($data['jobSearchKey'] == "") ? null: $data['jobSearchKey'];
        $jobCard = ($data['jobCard'] == "") ? null : $data['jobCard'];
        $serviceType = ($data['serviceType'] == "") ? null : $data['serviceType'];
        $toDate = ($data['toDate'] == "") ? null : $data['toDate'];
        $fromDate = ($data['fromDate'] == "") ? null : $data['fromDate'];

        if (!is_null($fromDate)) {
        	$fromDate = $this->getGMTDateTime('Y-m-d H:i:s', $fromDate)['data']['currentTime'];
        } 

        if (!is_null($toDate)) {
        	$toDate = $this->getGMTDateTime('Y-m-d H:i:s', $toDate)['data']['currentTime'];
        } 

        $status = $data['status'];
        $ServiceJobsController = $this->getServiceLocator()->get("ServiceJobsController");
        if ($jobSearchKey != null || $jobCard != null || $serviceType != null || $toDate != null) {
            $tbl = $this->getModel('JobTable');
            $jobList = $tbl->getJobBySearchKey($status, $jobSearchKey,$jobCard, $serviceType, $fromDate, $toDate);
        	$paginated = false;
        } else {
            $jobList = $ServiceJobsController->getPaginatedJob($this->locationID, $param, $status);
        	$paginated = true;
        }

        $view = new ViewModel(array(
            'jobList' => $jobList,
            'paginated' => $paginated,
            'status' => $this->getStatusesList(),
            'timeZone' => $this->timeZone
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/service-jobs/list-table.phtml');

        return $view;
    }

    /**
	 * this function use to save business type. this is a one time process.
	 * @param array $data
	 * return array
	 **/
	public function initiateBusinessType($data) {
		// save business type
		$displayData = [
			'jobBusinessType' => $data['id'],
			'jobWizardStatus' => 1,
		];
		$this->user_session->jobBusinessType = $data['id'];
		$saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);

		return $saveBusinessType;
	}

	public function getAllServiceVehicals() {

		$serviceVehicals = $this->getModel('ServiceVehicleTable')->fetchAll();
		$jobList = array();
		foreach ($serviceVehicals as $vehicle) {
			$vehicalList[] = $vehicle['serviceVehicleRegNo'];
		}
		return $vehicalList;
	}

	public function getParticipants($jobID) 
    {
    	$participants = $this->getModel('JobSupervisorTable')->getTournamentOfficialsByJobID($jobID);
    	$data = [];

    	foreach ($participants as $key => $value) {
            array_push($data, $value);
        }

        $view = new ViewModel(array(
            'employees' => $data,
            'paginated' => false,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/event/view-event-participants.phtml');
        
        return [
            'view' => $view,
            'status' => true,
        ];
    }
}
