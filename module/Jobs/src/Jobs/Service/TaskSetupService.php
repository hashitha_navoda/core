<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Jobs\Model\Task;
use Jobs\Model\ServiceSubTask;
use Jobs\Model\ServiceVehicleType;

class TaskSetupService extends BaseService
{
    /**
     * This function is used save and update task
     * @param $postData $userID
     */
	public function saveTask($data, $type = 'task')
    {
        $subTasks = $data['subTasks'];
        $vehicalTypeIDs = $data['vehicalTypeIDs'];
        $subTasksData = [];

        $taskCode = $data['taskCode'];
        $taskName = $data['taskName'];
        $departmentID = $data['departmentID'];
        $productID = $data['productID'];
        $editMode = $data['editMode'];
        //check wheter editMode is true or false. if it is true then update the task otherwise save the task
        if ($editMode == 'true') {
            $taskID = $data['taskID'];
            $data = array(
                'taskName' => $taskName,
                'departmentID' => $departmentID,
                'taskID' => $taskID,
            );

            $taskData = new Task();
            $taskData->exchangeArray($data);
            $result = $this->getModel('TaskTable')->updateTask($taskData);
            if ($result) {

                $updateOldSubTasks = $this->getModel('ServiceSubTaskTable')->updateOldServiceSubTasks($taskID);


                if (sizeof($subTasks) > 0) {
                    foreach ($subTasks as $key => $value) {
                        $subTasksData[] = ['name' => $value, 'code' => $key];
                    }

                    $result = $this->insertServiceSubTasks($subTasksData, $taskID);

                    if(!$result) {
                        return $this->returnError('ERR_SUB_TSK_SAVE', 'SUB_TASK_SAVE_FAILD');
                    }
                }

                if ($type == 'service') {
                    $updateServicVehic = $this->getModel('ServiceVehicleTypeTable')->updateDeleted($taskID); 

                    if(!$updateServicVehic) {
                        return $this->returnError('ERR_SERVICE_VEHICLE_TYPE_UPDATE', 'SUB_TASK_SAVE_FAILD');
                    }

                    if ($vehicalTypeIDs != null) {
                        foreach ($vehicalTypeIDs as $value) {
                            $serviceVehicleTypeData = [
                                'serviceID' => $taskID,
                                'vehicleTypeID' => $value,
                                'deleted' => 0
                            ];

                            $vehicleTypeSaveData = new ServiceVehicleType();
                            $vehicleTypeSaveData->exchangeArray($serviceVehicleTypeData);
                            $serviceVehicleTypeId = $this->getModel('ServiceVehicleTypeTable')->saveServiceVehicleTypeData($vehicleTypeSaveData); 

                            if(!$serviceVehicleTypeId) {
                                return $this->returnError('ERR_SERVICE_VEHICLE_TYPE_SAVE', 'SUB_TASK_SAVE_FAILD');
                            }                    
                        }
                    } 
                }
                
                return ($type == 'service') ? $this->returnSuccess(array('taskID' => $taskID), 'SUCC_SER_UPDATE') : $this->returnSuccess(array('taskID' => $taskID), 'SUCC_TSK_UPDATE');
                
            } else {
                return ($type == 'service') ? $this->returnError(null,'ERR_SER_UPDATE'):$this->returnError(null,'ERR_TSK_UPDATE');
            }
        } else {
            //check whether task code alredy in database or not.
            $result = $this->getModel('TaskTable')->getTaskByTaskCode($taskCode);
            if (!($result->count() > 0)) {
                $data = array(
                    'taskCode' => $taskCode,
                    'taskName' => $taskName,
                    'departmentID' => $departmentID,
                    'productID' => $productID
                );

                $taskData = new Task();
                $taskData->exchangeArray($data);
                $taskID = $this->getModel('TaskTable')->saveTask($taskData);
                if ($taskID) {
                    if (sizeof($subTasks) > 0) {
                        foreach ($subTasks as $key => $value) {
                            $subTasksData[] = ['name' => $value, 'code' => $key];
                        }

                        $result = $this->insertServiceSubTasks($subTasksData, $taskID);

                        if(!$result) {
                            return $this->returnError('ERR_SUB_TSK_SAVE', 'SUB_TASK_SAVE_FAILD');
                        }
                    }

                    if ($type == 'service') {
                        if ($vehicalTypeIDs != null) {
                            foreach ($vehicalTypeIDs as $value) {
                                $serviceVehicleTypeData = [
                                    'serviceID' => $taskID,
                                    'vehicleTypeID' => $value,
                                    'deleted' => 0
                                ];
                                $vehicleTypeSaveData = new ServiceVehicleType();
                                $vehicleTypeSaveData->exchangeArray($serviceVehicleTypeData);
                                $serviceVehicleTypeId = $this->getModel('ServiceVehicleTypeTable')->saveServiceVehicleTypeData($vehicleTypeSaveData); 

                                if(!$serviceVehicleTypeId) {
                                    return $this->returnError('ERR_SERVICE_VEHICLE_TYPE_SAVE', 'SUB_TASK_SAVE_FAILD');
                                }                    
                            }
                        }                   
                    }

                    return ($type == 'service') ? $this->returnSuccess(array('taskID' => $taskID),'SUCC_SER_SAVE'):$this->returnSuccess(array('taskID' => $taskID),'SUCC_TSK_SAVE');
                } else {
                    return ($type == 'service') ? $this->returnError('ERR_SER_SAVE', null) : $this->returnError('ERR_TSK_SAVE', null);
                }
            } else {
                return ($type == 'service') ? $this->returnError('ERR_SER_CODE_ALREDY_EXIXT','JTCERR') : $this->returnError('ERR_TSK_CODE_ALREDY_EXIXT','JTCERR');
            }
        }
    }

    /**
     * This function is used to get task by search key
     */
    public function getTaskBySearchKey($data, $param, $type = 'task')
    {
        $paginated = false;
        $taskSearchKey = $data['taskSearchKey'];

        if (!empty($taskSearchKey)) {
            $tbl = $this->getModel('TaskTable');
            $taskList = $tbl->getTaskforSearch($taskSearchKey, null, $type);
        } else {
            $taskSetup = $this->getServiceLocator()->get("TaskSetupController");
            $taskList = $taskSetup->getPaginatedJobTypes(6, $param);
            $paginated = true;
        }

        if ($type == 'task') {
            $view = new ViewModel(array(
                'taskList' => $taskList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('jobs/task-setup/list.phtml');
        } else {
            $view = new ViewModel(array(
                'ServiceList' => $taskList,
                'paginated' => $paginated
            ));

            $view->setTerminal(true);
            $view->setTemplate('jobs/service-setup/list.phtml');
        }

        return $view;
    }

    /**
     * This function is used to delete task
     **/
    public function deleteTaskByTaskID($data, $type = 'task')
    {
        $taskID = $data['taskID'];
        $taskCode = $data['taskCode'];
        $taskCardDetails = $this->getModel('TaskCardTaskTable')->getTaskCardDetailsByTaskID($taskID)->current();
        $jobTaskDetails = $this->getModel('JobTaskTable')->getTaskDetailsByTaskId($taskID);

        if (!$taskCardDetails) {
            if ($type == 'service') {
                if($jobTaskDetails) {
                    return ($type == 'service') ? $this->returnError('ERR_SER_DEL_USD_JOB',null) : $this->returnError('ERR_TSK_DEL_USD',null);
                }
            }
            if (!empty($taskID)) {
                $result = $this->getModel('TaskTable')->updateDeleteTask($taskID);
                if ($result) {
                    return ($type == 'service') ? $this->returnSuccess(array($taskCode),'SUCC_SER_DELETE') : $this->returnSuccess(array($taskCode),'SUCC_TSK_DELETE');
                } else {
                    return ($type == 'service') ? $this->returnError('ERR_SER_DEL',null) : $this->returnError('ERR_TSK_DEL',null);
                }
            } else {
                return ($type == 'service') ? $this->returnError('ERR_SER_DELETE',null) : $this->returnError('ERR_TSK_DELETE',null);
            }
        } else {
            return ($type == 'service') ? $this->returnError('ERR_SER_DEL_USD',null) : $this->returnError('ERR_TSK_DEL_USD',null);
        }
    }

    /**
     * This function is used to search tasks for dropdown
     */
    public function searchTasksForDropdown($data)
    {
        $searchKey = $data['searchKey'];
        $vehicalTypeID = ($data['vehicleType'] == "") ? null : $data['vehicleType'];
        $tasks = $this->getModel('TaskTable')->getTaskforSearch($searchKey, $vehicalTypeID);
        $taskList = array();
        foreach ($tasks as $dep) {
            $temp['value'] = $dep['taskID'];
            $temp['text'] = $dep['taskCode'] . '-' . $dep['taskName'];
            $taskList[] = $temp;
        }
        return $taskList;
    }

    /**
     * This function is used to save sub task relate with service
     **/
    public function insertServiceSubTasks($subTasksData, $taskID)
    {
        foreach ($subTasksData as $key => $val) {
            $subData = array(
                'subTaskCode' => $val['code'],
                'subTaskName' => $val['name'],
                'taskId' => $taskID,
            );

            $subTskData = new ServiceSubTask();
            $subTskData->exchangeArray($subData);

            $savedSubTaskResult = $this->getModel('ServiceSubTaskTable')->saveSubTask($subTskData);
            if (!$savedSubTaskResult) {
                return false; 
            }
        }
        return true;
            
    }

    /**
     * This function is used to get related sub tasks
     */
    public function getRelatedSubTasks($data)
    {
        $taskID = ($data['taskID'] == "")? null: $data['taskID'];
        $mode = ($data['mode'] == 'jobView') ? true: false; 
        $tasks = $this->getModel('ServiceSubTaskTable')->getRelatedSubTasks($taskID, $mode);
        
        $subTasks = [];
        foreach ($tasks as $key => $value) {
            $subTasks[$value['subTaskCode']] = $value['subTaskName'];
        }
        
        if (!$tasks) {
            return $this->returnError('ERR_RETRIVE_SUB_TSK_RELATED_SER',null);
        }

        if ($data['vehiclTypeFlag'] == true) {
            $vehiclRes = $this->getModel('ServiceVehicleTypeTable')->getRelatedVehicleTypes($taskID);
        
            $vehicleTypes = [];
            foreach ($vehiclRes as $key => $value) {
                $vehicleTypes[] = $value['vehicleTypeID'];
            }

            return $this->returnSuccess(['subTasks' => $subTasks, 'vehicleTypes' => $vehicleTypes],null);
        }

        return $this->returnSuccess($subTasks,null);
    }

    /**
     * This function is used to get related sub task details
     **/
    public function getSubTaskByTaskId($data)
    {
        $taskID = ($data['taskID'] == "") ? null: $data['taskID'];
        $mode = ($data['mode'] == 'jobView') ? true: false;
        $result = $this->getModel('ServiceSubTaskTable')->getRelatedSubTasks($taskID, $mode);

        $subTasks = [];
        foreach ($result as $key => $value) {
            $subTasks[] = ['name' => $value['subTaskName'], 'code' => $value['subTaskCode'], 'status' => $value['subTaskStatus'], 'subTaskID' => $value['subTaskId']];
        }
    
        if ($result) {
            return $this->returnSuccess($subTasks,null);
        } else {
            return $this->returnError('ERR_RETRIVE_SUB_TSK_RELATED_SER',null);
        }
            
    }

    /**
     * This function is used to change the sub task status
     **/
    public function changeSubTaskStatus($data)
    {
        $subTaskId = ($data['subTaskId']) ? $data['subTaskId']: null;
        $status = ($data['subTaskStatus']) ? $data['subTaskStatus']: null;

        $result = $this->getModel('ServiceSubTaskTable')->changeSubTaskStatus($subTaskId, $status);

        if ($result) {
            return $this->returnSuccess(null,'SUCC_SUB_TSK_STATUS_CHANGE');
        } else {
            return $this->returnError('ERR_SUB_TSK_STATUS_CHANGE',null);
        }
            
    }

    /**
     * This function is used to service status
     **/
    public function changeStatusID($data, $type = 'task')
    {
        $taskID = ($data['taskID']) ? $data['taskID']: null;
        $status = ($data['status'] != '') ? $data['status']: null;
        $result = $this->getModel('TaskTable')->changeStatusID($taskID, $status);

        if ($result) {
            return ($type == 'service') ? $this->returnSuccess(null,'SUCC_SER_STATUS_CHANGE') : $this->returnSuccess(null,'SUCC_TSK_STATUS_CHANGE');
        } else {
            return ($type == 'service') ? $this->returnError('ERR_SER_STATUS_CHANGE',null) : $this->returnError('ERR_TSK_STATUS_CHANGE',null);
        }
            
    }

     /**
     * This function is used to delete the sub task
     **/
    public function deleteSubTask($data)
    {
        $subTaskId = ($data['subTaskId']) ? $data['subTaskId']: null;
        $checkSubTaskUsedInJob = $this->getModel('JobSubTaskTable')->checkSubTaskUsedInJob($subTaskId);

        if (!$checkSubTaskUsedInJob) {
            return $this->returnError('ERR_SUB_TSK_ALREADY_USED',null);
        }

        $result = $this->getModel('ServiceSubTaskTable')->deleteSubTask($subTaskId);

        if ($result) {
            return $this->returnSuccess(null,'SUCC_SUB_TSK_DELETE');
        } else {
            return $this->returnError('ERR_SUB_TSK_DELETE',null);
        }
            
    }
}