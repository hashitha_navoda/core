<?php

namespace Jobs\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Jobs\Model\Team;
use Jobs\Model\EmployeeTeam;

class TeamService extends BaseService
{
	/**
    * this function use to save team.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function createTeam($data, $userId, $tournamentFlag = false)
    {

        $teamCode = $data['teamCode'];
        $teamName = $data['teamName'];
        $teamEmployees = $data['teamEmployees'];
        $teamID = null;

        $result = $this->getModel('teamTable')->getTeamByCode($teamCode);

        if ($result->count() > 0) { 
            return $this->returnError('ERR_TM_CODE_ALREDY_EXIST', 'TEAM_EXIST');
        }

        $data = array(
            'teamID' => $teamID,
            'teamCode' => $teamCode,
            'teamName' => $teamName,
        );

        $teamData = new Team();
        $teamData->exchangeArray($data);

        $savedResult = $this->getModel('TeamTable')->saveTeams($teamData);

        if (!$savedResult) {
            return $this->returnError('ERR_TM_SAVE', 'TEAM_SAVE_FAILD');
        } else {

            foreach ($teamEmployees as $key => $value) {
                $employeeTeamID = null;
                if ($tournamentFlag) {
                    $data = [
                        'employeeTeamID' => $employeeTeamID,
                        'employeeID' => $value['participantID'],
                        'participantTypeID' => $value['participantTypeID'],
                        'participantSubTypeID' => $value['participantSubTypeID'],
                        'teamID' => $savedResult
                    ];
                } else {
                    $data = [
                        'employeeTeamID' => $employeeTeamID,
                        'employeeID' => $value,
                        'teamID' => $savedResult
                    ];
                }

                $employeeTeamData = new EmployeeTeam();
                $employeeTeamData->exchangeArray($data);
                $saveTeamEmployees = $this->getModel('EmployeeTeamTable')->saveTeamEmployees($employeeTeamData);
                if (!$saveTeamEmployees) {
                    return $this->returnError('ERR_TM_SAVE', 'TEAM_SAVE_FAILD');
                }
            } 
            $this->setLogMessage('Team '.$teamCode.' is created.');
            return $this->returnSuccess('TEAM_SAVE_SUCESSFULLY', 'SUCC_TM_SAVE');
        }
    }

    /**
    * this function use to update employee.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function updateTeam($data, $userId, $tournamentFlag = false)
    {
        $checkCodeValid = $this->getModel('TeamTable')->checkTeamCodeValid($data['teamID'], $data['teamCode']);

        $teamEmployees = $data['teamEmployees'];
        if ($checkCodeValid > 0) {
            return $this->returnError('ERR_TM_CODE_ALREDY_EXIST', 'TEAM_CODE_EXIST');
        }

        $getInactiveTeamEmployees = $this->getModel('EmployeeTeamTable')->getInactiveTeamEmployees($data['teamID']);
        $teamNewData = $data;
        $teamData = new Team();
        $teamData->exchangeArray($data);

        $oldData = $this->getModel('TeamTable')->getTeamByID($data['teamID']);
        $result = $this->getModel('TeamTable')->updateTeamDelete($data['teamID']);

        if (!$result) {
            return $this->returnError('ERR_TM_UPDATE', 'TEAM_FAILD_UPDATE');
        }

        $savedResult = $this->getModel('TeamTable')->saveTeams($teamData);

        if (!$savedResult) {
            return $this->returnError('ERR_TM_UPDATE', 'TEAM_FAILD_UPDATE');
        } else {

            foreach ($getInactiveTeamEmployees as $key => $value) {
                    array_push($teamEmployees, $value['employeeID']);
            }

            foreach ($teamEmployees as $key => $value) {
                $employeeTeamID = null;
                if ($tournamentFlag) {
                    $data = [
                        'employeeTeamID' => $employeeTeamID,
                        'employeeID' => $value['participantID'],
                        'participantTypeID' => $value['participantTypeID'],
                        'participantSubTypeID' => $value['participantSubTypeID'],
                        'teamID' => $savedResult
                    ];
                } else {
                    $data = [
                        'employeeTeamID' => $employeeTeamID,
                        'employeeID' => $value,
                        'teamID' => $savedResult
                    ];
                }
                $employeeTeamData = new EmployeeTeam();
                $employeeTeamData->exchangeArray($data);
                $saveTeamEmployees = $this->getModel('EmployeeTeamTable')->saveTeamEmployees($employeeTeamData);
                if (!$saveTeamEmployees) {
                    return $this->returnError('ERR_TM_SAVE', 'TEAM_SAVE_FAILD');
                }
            } 
            $changeArray = $this->getTeamChageDetails($oldData, $teamNewData);
            //set log details
            $previousData = json_encode($changeArray['previousData']);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("Team ".$oldData->teamCode." is updated.");
            return $this->returnSuccess('TEAM_SUCESSFULLY_UPDATE', 'SUCC_TM_UPDATE');
        }
    }

    public function getTeamChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Team code'] = $row->teamCode;
        $previousData['Team name'] = $row->teamName;

        $newData = [];
        $newData['Team code'] = $data['teamCode'];
        $newData['Team name'] = $data['teamName'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }

    /**
    * this function use to update employee state.
    * @param array $data
    * return array
    **/
    public function updateTeamState($data)
    {
        $teamID = $data['teamID'];
        $oldData = $this->getModel('TeamTable')->getTeamByID($teamID);
        $status = $data['status'];
        $result = $this->getModel('TeamTable')->updateTeamState($teamID, $status);

        if ($result) {
            if ($status == 0) {
                $this->setLogMessage("Status of team ".$oldData->teamCode." is deactived");
            } else {
                $this->setLogMessage("Status of team ".$oldData->teamCode." is actived");
            }
            return $this->returnSuccess('TEAM_STATE_SUCESSFULLY_UPDATE', 'SUCC_TM_STATUS_UPDATE');
        } else {
            return $this->returnError('ERR_TM_STATE_UPDATE', 'TEAM_STATE_FAILD_UPDATE');
        }

    }

    /**
    * this function use to search employee by key, Department, Designation
    * @param array $data
    * @param int $param
    * return array
    **/
    public function teamSearchByKey($data, $param, $tournamentFlag = false)
    {
        $paginated = false;
        $key = $data['searchKey'];
        
        if (!empty($key)) {
            $teamList = $this->getModel('TeamTable')->teamSearchByKey($key);
            $status = true;
        } else {
            $status = true;
            $teamController = $this->getServiceLocator()->get("TeamController");
            $teamList = $teamController->getPaginatedTeams(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'teams' => $teamList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        if ($tournamentFlag) {
            $view->setTemplate('jobs/tournament-team/list.phtml');
        } else {
            $view->setTemplate('jobs/teams/list.phtml');
        }
        
        return [
            'view' => $view,
            'status' => $status
        ];

    }

    /**
    * this function use to get employees details according to the teamID
    * @param int $id
    * return array
    **/
    public function getTeamEmployees($id) {
        $teamEmployees = $this->getModel('EmployeeTeamTable')->getTeamEmployees($id);
        $data = [];

        foreach ($teamEmployees as $key => $value) {
            array_push($data, $value);
        }

        $view = new ViewModel(array(
            'employees' => $data,
            'paginated' => false,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/teams/team-employees-view.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data' => $data
        ];
    }

    public function getTeamPartcipants($id) {
        $teamEmployees = $this->getModel('EmployeeTeamTable')->getTeamEmployees($id);
        $data = [];

        foreach ($teamEmployees as $key => $value) {
            array_push($data, $value);
        }

        $view = new ViewModel(array(
            'employees' => $data,
            'paginated' => false,
        ));

        $view->setTerminal(true);
        $view->setTemplate('jobs/tournament-team/view-participants.phtml');
        
        return [
            'view' => $view,
            'status' => true,
            'data' => $data
        ];
    }

    /**
     * This function is used to delete team
     **/
    public function deleteTeamByTeamID($data, $userId)
    {
        $teamID = $data['teamID'];
        $oldData = $this->getModel('TeamTable')->getTeamByID($teamID);
        if (!empty($teamID)) {
            $result = $this->getModel('TeamTable')->updateTeamDelete($teamID);
            if ($result) {
                $this->setLogMessage('Team '.$oldData->employeeCode.' is deleted.');
                return $this->returnSuccess(array($taskCode),'SUCC_TM_DELETE');
            } else {
                return $this->returnError('ERR_TM_DEL',null);
            }
        } else {
            return $this->returnError('ERR_TM_DELETE',null);
        }
    }

    public function searchTeamsForDropdown($data) {
        $searchKey = $data['searchKey'];
        $teams = $this->getModel('TeamTable')->teamSearchByKeyForDropdown($searchKey);
        $teamList = array();
        foreach ($teams as $team) {
            $temp['value'] = $team['teamID'];
            $temp['text'] = $team['teamCode'] . ' ' . $team['teamName'];
            $teamList[] = $temp;
        }
        return $teamList;
    }
}