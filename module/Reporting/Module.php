<?php

namespace Reporting;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $this->getConfig();
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'SalesReportService' => 'Reporting\Service\SalesReportService',
                'StockInHandReportService' => 'Reporting\Service\StockInHandReportService',
                'FinanceReportService' => 'Reporting\Service\FinanceReportService',
                'BudgetReportService' => 'Reporting\Service\BudgetReportService',
                'DailySalesReportService' => 'Reporting\Service\DailySalesReportService',
                'InvoiceReportService' => 'Reporting\Service\InvoiceReportService',
                'IncomeExpenseReportService' => 'Reporting\Service\IncomeExpenseReportService'
            ),
            'shared' => array(
                'SalesReportService' => true,
                'StockInHandReportService' => true,
                'FinanceReportService' => true,
                'BudgetReportService' => true,
                'DailySalesReportService' => true,
                'InvoiceReportService' => true,
                'IncomeExpenseReportService' => true
            ),
            'aliases' => array(

            ),
            'factories' => array(
            ),
        );
    }

}