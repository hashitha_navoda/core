<?php

/**
 * Description of Configuration
 *
 * @author sandun <sandun@thinkcube.com>
 *
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Reporting\Controller\PosReport' => 'Reporting\Controller\PosReportController',
            'Reporting\Controller\API\PosReport' => 'Reporting\Controller\API\PosReportController',
            'Reporting\Controller\API\PurchaseReport' => 'Reporting\Controller\API\PurchaseReportController',
            'Reporting\Controller\PurchaseReport' => 'Reporting\Controller\PurchaseReportController',
            'Reporting\Controller\API\GrnReport' => 'Reporting\Controller\API\GrnReportController',
            'Reporting\Controller\GrnReport' => 'Reporting\Controller\GrnReportController',
            'Reporting\Controller\API\SupplierReport' => 'Reporting\Controller\API\SupplierReportController',
            'Reporting\Controller\SupplierReport' => 'Reporting\Controller\SupplierReportController',
            'Reporting\Controller\API\ItemTransferReport' => 'Reporting\Controller\API\ItemTransferReportController',
            'Reporting\Controller\ItemTransferReport' => 'Reporting\Controller\ItemTransferReportController',
            'Reporting\Controller\API\StockInHandReport' => 'Reporting\Controller\API\StockInHandReportController',
            'Reporting\Controller\StockInHandReport' => 'Reporting\Controller\StockInHandReportController',
            'Reporting\Controller\InventoryReport' => 'Reporting\Controller\InventoryReportController',
            'Reporting\Controller\InventoryReportAPI' => 'Reporting\Controller\InventoryReportAPIController',
            'Reporting\Controller\CustomerReport' => 'Reporting\Controller\CustomerReportController',
            'Reporting\Controller\API\CustomerReport' => 'Reporting\Controller\API\CustomerReportController',
            'Reporting\Controller\SalesReport' => 'Reporting\Controller\SalesReportController',
            'Reporting\Controller\API\SalesReport' => 'Reporting\Controller\API\SalesReportController',
            'Reporting\Controller\InvoiceReport' => 'Reporting\Controller\InvoiceReportController',
            'Reporting\Controller\API\InvoiceReport' => 'Reporting\Controller\Generate\InvoiceReportController',
            'Reporting\Controller\ListingReport' => 'Reporting\Controller\ListingReportController',
            'Reporting\Controller\ListingReportAPI' => 'Reporting\Controller\ListingReportAPIController',
            'Reporting\Controller\GraphReport' => 'Reporting\Controller\GraphReportController',
            'Reporting\Controller\GraphReportAPI' => 'Reporting\Controller\GraphReportAPIController',
            'Reporting\Controller\Reports' => 'Reporting\Controller\ReportsController',
            'Reporting\Controller\DailySalesReport' => 'Reporting\Controller\DailySalesReportController',
            'Reporting\Controller\PaymentReport' => 'Reporting\Controller\PaymentReportController',
            'Reporting\Controller\API\PaymentReport' => 'Reporting\Controller\Generate\PaymentReportController',
            'Reporting\Controller\DeliveryReport' => 'Reporting\Controller\DeliveryReportController',
            'Reporting\Controller\Generate\DeliveryReport' => 'Reporting\Controller\Generate\DeliveryReportController',
            'Reporting\Controller\Generate\PaymentVoucherReport' => 'Reporting\Controller\Generate\PaymentVoucherReportController',
            'Reporting\Controller\PaymentVoucherReport' => 'Reporting\Controller\PaymentVoucherReportController',
            'Reporting\Controller\JobCardReport' => 'Reporting\Controller\JobCardReportController',
            'Reporting\Controller\Generate\JobCardReport' => 'Reporting\Controller\Generate\JobCardReportController',
            'Reporting\Controller\ReconciliationReport' => 'Reporting\Controller\ReconciliationReportController',
            'Reporting\Controller\API\ReconciliationReport' => 'Reporting\Controller\API\ReconciliationReportController',
            'Reporting\Controller\ChequeReport' => 'Reporting\Controller\ChequeReportController',
            'Reporting\Controller\API\ChequeReport' => 'Reporting\Controller\API\ChequeReportController',
            'Reporting\Controller\PettyCashVoucherReport' => 'Reporting\Controller\PettyCashVoucherReportController',
            'Reporting\Controller\API\PettyCashVoucherReport' => 'Reporting\Controller\API\PettyCashVoucherReportController',
            'Reporting\Controller\ExpensePaymentVoucherReport' => 'Reporting\Controller\ExpensePaymentVoucherReportController',
            'Reporting\Controller\API\ExpensePaymentVoucherReport' => 'Reporting\Controller\API\ExpensePaymentVoucherReportController',
            'Reporting\Controller\SalesPaymentReport' => 'Reporting\Controller\SalesPaymentReportController',
            'Reporting\Controller\Generate\SalesPaymentReport' => 'Reporting\Controller\Generate\SalesPaymentReportController',
            'Reporting\Controller\AccountReport' => 'Reporting\Controller\AccountReportController',
            'Reporting\Controller\API\AccountReport' => 'Reporting\Controller\API\AccountReportController',
            'Reporting\Controller\QuotationReport' => 'Reporting\Controller\QuotationReportController',
            'Reporting\Controller\Generate\QuotationReport' => 'Reporting\Controller\Generate\QuotationReportController',
            'Reporting\Controller\SalesOrderReport' => 'Reporting\Controller\SalesOrderReportController',
            'Reporting\Controller\API\SalesOrderReport' => 'Reporting\Controller\API\SalesOrderReportController',
            'Reporting\Controller\FinanceReport' => 'Reporting\Controller\FinanceReportController',
            'Reporting\Controller\API\FinanceReport' => 'Reporting\Controller\API\FinanceReportController',
            'Reporting\Controller\BudgetReport' => 'Reporting\Controller\BudgetReportController',
            'Reporting\Controller\API\BudgetReport' => 'Reporting\Controller\API\BudgetReportController',
            'Reporting\Controller\LoyaltyCardReport' => 'Reporting\Controller\LoyaltyCardReportController',
            'Reporting\Controller\API\LoyaltyCardReport' => 'Reporting\Controller\API\LoyaltyCardReportController',
            'Reporting\Controller\JobReport' => 'Reporting\Controller\JobReportController',
            'Reporting\Controller\API\JobReport' => 'Reporting\Controller\API\JobReportController',
            'Reporting\Controller\IncomeExpenseReport' => 'Reporting\Controller\IncomeExpenseReportController',
            'Reporting\Controller\API\IncomeExpenseReport' => 'Reporting\Controller\API\IncomeExpenseReportController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'salesReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/sales-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\SalesReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'salesReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/sales-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\SalesReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'invoiceReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/invoice-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\InvoiceReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'invoiceReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/report/get/invoice[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\InvoiceReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'paymentReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/paymentReport[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\PaymentReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'deliveryNoteReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/get/delivery-note[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\Generate\DeliveryNoteReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'paymentReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/report/get/payment[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\PaymentReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'customerReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/customer-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\CustomerReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'customerReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/customer-report[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param3' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\CustomerReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'listingReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/listingReport[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\ListingReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'listingReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/listingReportAPI[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param3' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\ListingReportAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'graphReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/graphReport[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\GraphReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'graphReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/graphReportAPI[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param3' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\GraphReportAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'inventoryReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/inventoryReport[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\InventoryReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'inventoryReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/inventoryReportAPI[/:action][/:param1][/:param2][/:param3]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param3' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\InventoryReportAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'stockInHandReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/stock-in-hand-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\StockInHandReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'stockInHandReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/stock-in-hand-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\StockInHandReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'itemTransferReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/item-transfer-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\ItemTransferReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'itemTransferReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/item-transfer-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\ItemTransferReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'supplierReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/supplier-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\SupplierReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'supplierReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/supplier-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\SupplierReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'grnReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/grn-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\GrnReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'grnReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/grn-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\GrnReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'purchaseReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/purchase-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\PurchaseReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'purchaseReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/purchase-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\PurchaseReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'posReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/pos-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\PosReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'posReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/pos-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\PosReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'dailySalesReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/daily-sales-items-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\DailySalesReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'dailyDelivery' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/daily-delivery-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\DeliveryReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'dailyDeliveryGenerate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/get/daily-delivery[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\Generate\DeliveryReport',
                        'action' => 'dailyDeliveryValueView',
                    ),
                ),
            ),
            'paymentVoucherReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/payment-voucher-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\PaymentVoucherReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'paymentVoucherReportGenerate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/get/payment-voucher[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\Generate\PaymentVoucherReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'jobCardReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/job-card-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\JobCardReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'jobCardReportGenerate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/get/job-card[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\Generate\JobCardReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'jobReportGenerate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/api/job[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\JobReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'reconciliation-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/reconciliation-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\ReconciliationReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'salesPayment' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/sales-payment[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\SalesPaymentReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'pettyCashVoucherReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/petty-cash-voucher[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\PettyCashVoucherReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'reconciliation-report-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/reconciliation-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\ReconciliationReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'cheque-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/cheque-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\ChequeReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'cheque-report-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/cheque-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\ChequeReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'salesPaymentAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/get/sales-payment[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\Generate\SalesPaymentReport',
                        'action' => 'index',
                    ),
                ),
            ),

            'pettyCashVoucherReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/report/get/petty-cash-voucher[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\PettyCashVoucherReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'expensePaymentVoucherReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/expense-payment-voucher-report[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\ExpensePaymentVoucherReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'expensePaymentVoucherReportAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/get/expense-payment-voucher-report[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\ExpensePaymentVoucherReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'account-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/account-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\AccountReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'account-report-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/get/account-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\AccountReport',
                        'action' => 'index',
                    ),
                ),
            ),            
            'quotationReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/quotation-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\QuotationReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'quotation-report-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/api/quotation-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\Generate\QuotationReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'sales-order-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/sales-order-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\SalesOrderReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'sales-order-report-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/sales-order-report-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\SalesOrderReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'finance-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/finance-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\FinanceReport',
                        'action' => 'index',
                    ),
                ),
            ),

            'budget-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/budget-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\BudgetReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'income-expense-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/income-expense-report[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\IncomeExpenseReport',
                        'action' => 'index',
                    ),
                ),
            ),
            'income-expense-report-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/income-expense-report-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\IncomeExpenseReport',
                    ),
                )
            ),

            'loyalty-card-report' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/loyalty-card[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\LoyaltyCardReport',
                        'action' => 'index',
                    ),
                ),
            ),

            'jobReport' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/job[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\JobReport',
                        'action' => 'index',
                    ),
                ),
            ),

            'finance-report-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/finance-report-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\FinanceReport',
                    ),
                ),
            ),

            'budget-report-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/budget-report-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\BudgetReport',
                    ),
                ),
            ),

            'loyalty-card-report-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reports/loyalty-card-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Reporting\Controller\API\LoyaltyCardReport',
                        'action' => 'index',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'reporting' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
);
?>