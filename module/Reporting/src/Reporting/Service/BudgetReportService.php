<?php

namespace Reporting\Service;

use Core\Service\ReportService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;

class BudgetReportService extends ReportService
{
	public function getBudgetData($fiscalPeriodID, $accountIDs, $dimensionType, $dimensionValue, $financeAccountTypes)
	{
		$budgetBasicData = $this->getModel('BudgetTable')->getBudgetByFiscalPeriodID($fiscalPeriodID, $dimensionType, $dimensionValue)->current();
		$dimensionType = ($dimensionType == "") ? null : $dimensionType; 
		$dimensionValue = ($dimensionValue == "") ? null : $dimensionValue; 
		$fiscalPeriodData = $this->getModel('FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($fiscalPeriodID);
        $budgetData = [];
		if ($budgetBasicData) {
			$financeAccounts = $this->getModel('FinanceAccountsTable')->fetchAll(null, $accountIDs, true, $financeAccountTypes);
        	$temp = [];
	        foreach ($financeAccounts as $key => $value) {
	        	$fiscalPeriodColoumnData = [];
	            $accountID = $value['financeAccountsID'];
				if ($budgetBasicData['interval'] == "monthly") {
					$childFiscalPeriodData = $this->getModel('FiscalPeriodTable')->getFiscalPeriodChildDataByFiscalPeriodParentID($fiscalPeriodID);
					foreach ($childFiscalPeriodData as $chFiscData) {
						$checkBudgetValue = $this->getModel('BudgetDetailsTable')->getBudgetDetailsByFiscalPeriodIDAndFinanceAcID($chFiscData['fiscalPeriodID'], $accountID, $budgetBasicData['budgetId'])->current();
						$fiscalPeriodColoumnData[$chFiscData['fiscalPeriodID']] = $chFiscData['fiscalPeriodStartDate']." - ".$chFiscData['fiscalPeriodEndDate'];
						$currentAccValue = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRangeForBudget($accountID,$chFiscData['fiscalPeriodStartDate'],$chFiscData['fiscalPeriodEndDate'], false, $dimensionType, $dimensionValue)->current();
						if ($currentAccValue) {
							$currentDebit = $currentAccValue['totalDebit'];
							$currentCredit = $currentAccValue['totalCredit'];
						} else {
							$currentDebit = 0;
							$currentCredit = 0;
						}


						if ($checkBudgetValue) {
							$temp[$value['financeAccountClassName']][$accountID][$chFiscData['fiscalPeriodID']]['budgetValue'] = $checkBudgetValue['budgetValue'];
						} else {
							$temp[$value['financeAccountClassName']][$accountID][$chFiscData['fiscalPeriodID']]['budgetValue'] = 0;
						}
						$temp[$value['financeAccountClassName']][$accountID][$chFiscData['fiscalPeriodID']]['currentDebit'] = $currentDebit;
						$temp[$value['financeAccountClassName']][$accountID][$chFiscData['fiscalPeriodID']]['currentCredit'] = $currentCredit;
					}
				} else {
					$fiscalPeriodColoumnData[$fiscalPeriodData['fiscalPeriodID']] = $fiscalPeriodData['fiscalPeriodStartDate']." - ".$fiscalPeriodData['fiscalPeriodEndDate'];
					$checkBudgetValue = $this->getModel('BudgetDetailsTable')->getBudgetDetailsByFiscalPeriodIDAndFinanceAcID($fiscalPeriodData['fiscalPeriodID'], $accountID, $budgetBasicData['budgetId'])->current();

					$currentAccValue = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRangeForBudget($accountID,$fiscalPeriodData['fiscalPeriodStartDate'],$fiscalPeriodData['fiscalPeriodEndDate'], false, $dimensionType, $dimensionValue)->current();

					if ($currentAccValue) {
						$currentDebit = $currentAccValue['totalDebit'];
						$currentCredit = $currentAccValue['totalCredit'];
					} else {
						$currentDebit = 0;
						$currentCredit = 0;
					}


					if ($checkBudgetValue) {
						$temp[$value['financeAccountClassName']][$accountID][$fiscalPeriodData['fiscalPeriodID']]['budgetValue'] = $checkBudgetValue['budgetValue'];
					} else {
						$temp[$value['financeAccountClassName']][$accountID][$fiscalPeriodData['fiscalPeriodID']]['budgetValue'] = 0;
					}
					$temp[$value['financeAccountClassName']][$accountID][$fiscalPeriodData['fiscalPeriodID']]['currentDebit'] = $currentDebit;
					$temp[$value['financeAccountClassName']][$accountID][$fiscalPeriodData['fiscalPeriodID']]['currentCredit'] = $currentCredit;
				}
				// $temp[$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];
				$temp[$value['financeAccountClassName']][$accountID]['financeAccountsName'] = $value['financeAccountsCode']."-".$value['financeAccountsName'];
				// $temp[$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];

	        }
           	$budgetData['budgetData'] = $temp;
           	$budgetData['fiscalPeriodColoumnData'] = $fiscalPeriodColoumnData;
		} else {
			$budgetData = [];
		}
       	$budgetData['fiscalPeriodData'] = $fiscalPeriodData['fiscalPeriodStartDate']." - ".$fiscalPeriodData['fiscalPeriodEndDate'];

       	$processData = $this->processBudgetData($budgetData);

       	return $processData;
	}

	public function processBudgetData($budgetData)
	{
		$newBudgetData = [];
		foreach ($budgetData as $ind => $value) {
			foreach ($value as $key => $val) {
				foreach ($val as $acID => $acData) {
       				$res = $this->processBudgetAccountData($acData);
	       			if ($res) {
	       				unset($budgetData[$ind][$key][$acID]);
	       			}
				}
			}
		}

		return $budgetData;
	}

	public function processBudgetAccountData($data) {

		foreach ($data as $val) {
			if (is_array($val)) {
				if (floatval($val['budgetValue']) != 0 || floatval($val['currentDebit']) != 0 || floatval($val['currentCredit']) != 0) {
					return false;
				}
			}
		}
		return true;
	}
}