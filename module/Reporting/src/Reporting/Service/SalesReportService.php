<?php

namespace Reporting\Service;

use Core\Service\ReportService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;

class SalesReportService extends ReportService
{
    /**
     * Get sales representative wise item data
     */
    public function representativeWiseItemData($postData)
    {
        $dataList = [];

        extract($postData);

        $withDefaultSalesPerson = (in_array('other', $representativeIds)) ? true : false;

        if (filter_var($isAllItems, FILTER_VALIDATE_BOOLEAN)) {
            $items = $this->getModel('ProductTable')->fetchAll(false);
            $itemIds = [];
            foreach ($items as $item) {
                $itemIds[] = $item->productID;
            }
        }

        foreach ($representativeIds as $representativeId) {
            //get sales person details
            $salesPerson = $this->getModel('SalesPersonTable')->getSalesPersonDetailsBySalesPersonId($representativeId);

            if ($salesPerson == null && $withDefaultSalesPerson == true) {
                $salesPersonName = 'Others';
            } else {
                $salesPersonName = $salesPerson['salesPersonFirstName'] . ' ' . $salesPerson['salesPersonLastName'] . ' (' . $salesPerson['salesPersonSortName'] . ')';
            }
            $productArr = [];

            foreach ($itemIds as $itemId) {
                //get product details
                $product = $this->getModel('ProductTable')->getProductByProductID($itemId,$itemCategory);
                //get items according to sales person
                if ($representativeId == "other") {
                    $representativeId = null;
                }

                $itemList = $this->getModel('InvoiceProductTable')->getInvoiceProductBySalesPersonId($itemId, $representativeId, $fromDate, $toDate);

                if (!empty($itemList) && sizeof($product) > 1) {
                    $quantity = $amount = 0;
                    foreach ($itemList as $i) {
                        // $quantity += ($i['salesInvoiceProductQuantity'] - $i['cNPQuantity']);
                        // $amount += ($i['salesInvoiceProductTotal'] - $i['cNPTotal']);

                        $relatedSalesPersons = $this->getModel("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($i['salesInvoiceID']);

                        $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);
                        $quantity += (($i['salesInvoiceProductQuantity'] / $numOfSP) - ($i['cNPQuantity'] / $numOfSP));
                        $amount += ( ($i['salesInvoiceProductTotal'] / $numOfSP) - ($i['cNPTotal'] / $numOfSP));
                    }
                    if ($quantity > 0) {
                        $itemArr = array('quantity' => $quantity, 'amount' => $amount, 'proCode' => $product['productCode'], 'proCategory' => $product['categoryName']);
                        $productArr[$product['productName']] = $itemArr;
                    }
                }
            }
            if (!empty($productArr)) {
                $dataList[$salesPersonName] = $productArr;
            }
        }
        return $dataList;
    }

    public function representativeWiseItemPdf($postData)
    {
        try {
            $dataList = $this->representativeWiseItemData($postData);

            $translator = new Translator();
            $name = $translator->translate('Representative wise Item Report');
            $period = $postData['fromDate'] . ' - ' . $postData['toDate'];

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'currencySymbol' => $postData['companyCurrencySymbol'],
                'dataList' => $dataList,
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/daily-sales-report/generate-representative-wise-item');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('representative_wise_category_item', $htmlContent, true);

            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    public function representativeWiseItemCsv($postData)
    {
        try {
            $dataList = $this->representativeWiseItemData($postData);

            $cD = $this->getCompanyDetails();

            $reportName = 'Representative wise Item Report';

            if (!empty($dataList)) {
                $title = '';
                $in = array(
                    0 => "",
                    1 => 'REPRESENTATIVE WISE ITEM REPORT',
                    2 => ""
                );
                $title = implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Period :' . $postData['fromDate'] . '-' . $postData['toDate']
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("REPRESENTATIVE NAME", "PRODUCT NAME", "PRODUCT CODE", "PRODUCT CATEGORY", "TOTAL QUANTITY", "TOTAL PRICE");
                $header = implode(",", $arrs);
                $arr = '';

                $grandTotal = 0;
                foreach ($dataList as $repName => $itemList) {
                    $in = array(
                        0 => $repName
                    );
                    $arr.=implode(",", $in) . "\n";

                    $totalAmount = 0;
                    foreach ($itemList as $itemName => $item) {
                        $in = array(
                            0 => '',
                            1 => $itemName,
                            2 => $item['proCode'],
                            3 => $item['proCategory'],
                            4 => $item['quantity'],
                            5 => number_format($item['amount'], 2, '.', '')
                        );
                        $arr.=implode(",", $in) . "\n";
                        $totalAmount = $totalAmount + $item['amount'];
                    }
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => 'Total',
                        5 => number_format($totalAmount, 2, '.', '')
                    );
                    $arr.=implode(",", $in) . "\n";
                    $grandTotal = $grandTotal + $totalAmount;
                }
                $in = array(
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => '',
                    4 => 'Grand Total',
                    5 => number_format($grandTotal, 2, '.', '')
                );
                $arr.=implode(",", $in);
            } else {
                $in = array('no matching records found');
                $arr.=implode(",", $in) . "\n";
            }

            $name = "representative_wise_item_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $reportName], 'csv report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    private function representativeWiseItemWithInvoceData($postData)
    {
        $representativeIds = $postData['representativeIds'];
        $itemIds = $postData['itemIds'];

        $withDefaultSalesPerson = (in_array('other', $representativeIds)) ? true : false;
        $dataList = [];
        
        if (filter_var($postData['isAllItems'], FILTER_VALIDATE_BOOLEAN)) {
            $items = $this->getModel('ProductTable')->fetchAll(false);
            $itemIds = [];
            foreach ($items as $item) {
                $itemIds[] = $item->productID;
            }
        }

        //get all salesInvoice Product details with salesPerson
        $allDetails = $this->getModel('InvoiceProductTable')->getAllInvoiceProductBySalesPersonIdsAndProIDs($itemIds,$representativeIds,$postData['fromDate'], $postData['toDate'], $withDefaultSalesPerson);

        //get all item taxes with their product details
        $allDetailsWithTax = $this->getModel('InvoiceProductTable')->getAllInvoiceProductWithTaxeDetailsBySalesPersonIdsAndProIDs($itemIds,$representativeIds,$postData['fromDate'], $postData['toDate'], $withDefaultSalesPerson);

        //get all credit note product details with salesInvoice and sales persons.
        $creditNoteDetails = $this->getModel('InvoiceProductTable')->getAllInvoiceProductRelatedCreditNoteProductByproIDAndSalesPerson($itemIds,$representativeIds,$postData['fromDate'], $postData['toDate'], $withDefaultSalesPerson);

        //get creditNote tax values that given details
        $creditNoteTaxes = $this->getModel('CreditNoteTable')->getCreditNoteTaxesBySalesPersonIdsAndProIDs($itemIds,$representativeIds,$postData['fromDate'], $postData['toDate'], $withDefaultSalesPerson);

        //re arrange creditNote Tax details in to the array
        $creditNoteTaxDetails = [];
        foreach ($creditNoteTaxes as $key => $value) {
            $creditNoteTaxDetails[$value['salesInvoiceCode']] = $value['totalCreditNoteTax'];

        }

        //re arrange sales invoice product details with salesPersonID and product ID
        $finalDataArray = [];
        $invoiceIds = [];
        $invoiceTaxDataSet = [];
        foreach ($allDetailsWithTax as $key => $value) {
            $salesPersonID = (is_null($value['salesPersonID'])) ? 'other' : $value['salesPersonID'];
            if ($value['taxType'] == "c") {
                $invoiceTaxDataSet[$salesPersonID][$value['salesInvoiceProductID']]['compoundTax'] += $value['salesInvoiceProductTaxAmount'];
            } else {
                $invoiceTaxDataSet[$salesPersonID][$value['salesInvoiceProductID']]['normalTax'] += $value['salesInvoiceProductTaxAmount'];
            }
            $invoiceTaxDataSet[$salesPersonID][$value['salesInvoiceProductID']]['totalTax'] += $value['salesInvoiceProductTaxAmount'];
        }

        foreach ($allDetails as $key => $singleData) {
            $salesPersonID = (is_null($singleData['salesPersonID'])) ? 'other' : $singleData['salesPersonID']; 

            $relatedSalesPersons = $this->getModel("InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($singleData['salesInvoiceID']);

            $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);
            $invoiceDSet = array(
                'invoiceCode' => $singleData['salesInvoiceCode'],
                'invoiceDate' => $singleData['salesInvoiceIssuedDate'],
                'customerName' => $singleData['customerName']. " [". $singleData['customerCode'] . "]",
                'salesinvoiceAmount' => round(($singleData['salesinvoiceTotalAmount'] / $numOfSP), 2),
                'creditNoteAmount' => round(($singleData['creditNoteTotal'] / $numOfSP),2),
                'salesInvoicePaidAmount' => $singleData['salesInvoicePayedAmount'],
                'salesInvoiceRemainingAmount' => ($singleData['salesinvoiceTotalAmount'] / $numOfSP) - ($singleData['salesInvoicePayedAmount'] / $numOfSP),
                );

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['quantity'] += ($singleData['salesInvoiceProductQuantity'] != 0) ? round(($singleData['salesInvoiceProductQuantity'] / $numOfSP), 2) : 0;

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['invoiceData'][$singleData['salesInvoiceCode']] = $invoiceDSet;

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['invoiceDataForTax'][$singleData['salesInvoiceCode']]['taxWithOutItemTotal'] += ((floatval($singleData['salesInvoiceProductTotal']) / $numOfSP) - (floatval($invoiceTaxDataSet[$salesPersonID][$singleData['salesInvoiceProductID']]['totalTax'])) / $numOfSP);

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['invoiceDataForTax'][$singleData['salesInvoiceCode']]['normalTaxValueForItem'] += (floatval($invoiceTaxDataSet[$salesPersonID][$singleData['salesInvoiceProductID']]['normalTax'])) / $numOfSP;
            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['invoiceDataForTax'][$singleData['salesInvoiceCode']]['compoundTaxValueForItem'] += (floatval($invoiceTaxDataSet[$salesPersonID][$singleData['salesInvoiceProductID']]['compoundTax'])) / $numOfSP;

            $finalDataArray['taxAmounts'][$salesPersonID][$singleData['salesInvoiceCode']] += ($invoiceTaxDataSet[$salesPersonID][$singleData['salesInvoiceProductID']]['totalTax'] / $numOfSP);

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['amount'] += round(($singleData['salesInvoiceProductTotal'] / $numOfSP), 2);

            $finalDataArray[$salesPersonID]['name'] =  ($salesPersonID == "other") ? "Others": $singleData['salesPersonFirstName'] . ' ' . $singleData['salesPersonLastName'] . ' (' . $singleData['salesPersonSortName'] . ')';

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['proName'] = $singleData['productName'];

            $invoiceIds[] = $singleData['salesInvoiceID'];
        }


        //getCreditNote total by invoiceIDs
        $creditDetails = $this->getModel('CreditNoteTable')->getCreditNoteTotalByInvoiceIDArray(array_unique($invoiceIds));
        $creditNoteTotalWithSalesInvoice = [];
        foreach ($creditDetails as $key => $creditValue) {
            $creditNoteTotalWithSalesInvoice[$creditValue['salesInvoiceCode']] = $creditValue['creditNoteTot'];
        }

        //re-arrange creditNote productDetials
        foreach ($creditNoteDetails as $key => $singleCreditNote) {
            $salesPersonID = (is_null($singleCreditNote['salesPersonID'])) ? 'other' : $singleCreditNote['salesPersonID']; 
            $relatedSalesPersons = $this->getModel("InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($singleData['salesInvoiceID']);

            $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);
            $finalDataArray[$salesPersonID]['data'][$singleCreditNote['productID']]['quantity'] -=  round(($singleCreditNote['creditNoteProductQuantity'] / $numOfSP), 2);
            $finalDataArray[$salesPersonID]['data'][$singleCreditNote['productID']]['amount'] -= round(($singleCreditNote['creditNoteProductTotal'] / $numOfSP), 2);
            $finalDataArray[$salesPersonID]['data'][$singleCreditNote['productID']]['invoiceData'][$singleCreditNote['salesInvoiceCode']]['creditNoteAmount'] = ($creditNoteTotalWithSalesInvoice[$singleCreditNote['salesInvoiceCode']] / $numOfSP);
            $finalDataArray[$salesPersonID]['data'][$singleCreditNote['productID']]['invoiceData'][$singleCreditNote['salesInvoiceCode']]['creditNoteTaxAmount'] = $creditNoteTaxDetails[$singleCreditNote['salesInvoiceCode']] / $numOfSP;
        }
        return $finalDataArray;

    }

    public function representativeWiseItemWithInvocePdf($postData)
    {
        try {
            $dataList = $this->representativeWiseItemWithInvoceData($postData);

            $translator = new Translator();
            $name = $translator->translate('Representative Wise Item Report With Invoice Details');
            $period = $postData['fromDate'] . ' - ' . $postData['toDate'];

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'currencySymbol' => $postData['companyCurrencySymbol'],
                'dataList' => $dataList,
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/daily-sales-report/generate-representative-wise-item-with-invoice-details');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('representative_wise_item_with_invoice', $htmlContent, true);

            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    public function representativeWiseItemWithInvoceCsv($postData)
    {
        try {
            $dataList = $this->representativeWiseItemWithInvoceData($postData);

            $cD = $this->getCompanyDetails();

            $reportName = 'Representative Wise Item Report With Invoice Details';

            if (!empty($dataList)) {
                $title = '';
                $in = array(
                    0 => "",
                    1 => 'REPRESENTATIVE WISE ITEM REPORT WITH INVOICE DETAILS',
                    2 => ""
                );
                $title = implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Period :' . $postData['fromDate'] . '-' . $postData['toDate']
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("REPRESENTATIVE NAME", "PRODUCT NAME", "TOTAL QUANTITY", "INVOICE CODE","INVOICE DATE", "CUSTOMER NAME", "ITEM TOTAL WITHOUT TAX", "ITEM NORMAL TAX","ITEM COMPOUND TAX", "INVOICE TOTAL WITHOUT TAX", "INVOICE TOTAL TAX", "CREDIT NOTE AMOUNT WITHOUT TAX", "CREDIT NOTE TOTAL TAX", "PAID AMOUNT", "REMAINING AMOUNT");
                $header = implode(",", $arrs);
                $arr = '';

                $grandTotal = 0;
                $grandWithoutTaxTotal = 0.00;
                $grandNormalTaxTotal = 0.00;
                $grandCompoundTaxTotal = 0.00;
                $grandInvoiceTotalWithoutTax = 0.00;
                $grandInvoiceTotalTax = 0.00;
                $grandCreditNoteTotal = 0.00;
                $grandCreditNoteTaxTotal = 0.00;
                $grandRemainingTotal = 0.00;
                $grandPaidTotal = 0.00;
                $grandInvoiceSet = [];
                $taxDetails = $dataList['taxAmounts'];
                foreach ($dataList as $repName => $itemList) {
                    if($repName == 'taxAmounts'){
                                continue;
                    }
                    $repWithoutTaxTotal = 0.00;
                    $repNormalTaxTotal = 0.00;
                    $repCompoundTaxTotal = 0.00;
                    $repInvoiceTotalWithoutTax = 0.00;
                    $repInvoiceTotalTax = 0.00;
                    $repCreditNoteTotal = 0.00;
                    $repCreditNoteTaxTotal = 0.00;
                    $repRemainingTotal = 0.00;
                    $repPaidTotal = 0.00;
                    $repInvoiceSet = [];
                    $in = array(
                        0 => preg_replace("/(,|;)/", " ", $itemList['name']),
                    );
                    $arr.=implode(",", $in) . "\n";

                    $totalAmount = 0;
                    foreach ($itemList['data'] as $itemName => $item) {
                        $itemWithoutTaxTotal = 0.00;
                        $normalItemTaxTotal = 0.00;
                        $compoundItemTaxTotal = 0.00;
                        $itemInvoiceTotalWithoutTax = 0.00;
                        $itemInvoiceTotalTax = 0.00;
                        $itemCreditNoteTaxTotal = 0.00;
                        $itemCreditNoteTotal = 0.00;
                        $itemRemainingTotal = 0.00;
                        $itemPaidTotal = 0.00;
                        $in = array(
                            0 => '',
                            1 => preg_replace("/(,|;)/", " ", $item['proName']),
                            2 => $item['quantity']
                        );
                        $arr.=implode(",", $in) . "\n";
                        $totalAmount = $totalAmount + $item['amount'];
                        $itemTaxDetails = $item['invoiceDataForTax'];
                        foreach ($item['invoiceData'] as $invKey => $invoice) {
                            $remainnigAmount = floatval($invoice['salesinvoiceAmount']) - (floatval($invoice['creditNoteAmount']) + floatval($invoice['salesInvoicePaidAmount']));
                            $invoiceTotalWithoutTax = floatval($invoice['salesinvoiceAmount']) - floatval($taxDetails[$invoice['invoiceCode']]);
                            $creditNoteWithOutTax = floatval($invoice['creditNoteAmount']) - floatval($invoice['creditNoteTaxAmount']);
                            $in = array(
                                0 => '',
                                1 => '',
                                2 => '',
                                3 => $invoice['invoiceCode'],
                                4 => $invoice['invoiceDate'],
                                5 => preg_replace("/(,|;)/", " ", $invoice['customerName']),
                                6 => number_format($itemTaxDetails[$invoice['invoiceCode']]['taxWithOutItemTotal'], 2, '.', ''),
                                7 => number_format($itemTaxDetails[$invoice['invoiceCode']]['normalTaxValueForItem'], 2, '.', ''),
                                8 => number_format($itemTaxDetails[$invoice['invoiceCode']]['compoundTaxValueForItem'], 2, '.', ''),
                                9 => number_format($invoiceTotalWithoutTax, 2, '.', ''),
                                10 => number_format($taxDetails[$invoice['invoiceCode']], 2, '.', ''),
                                11 => number_format($creditNoteWithOutTax, 2, '.', ''),
                                12 => number_format($invoice['creditNoteTaxAmount'], 2, '.', ''),
                                13 => number_format($invoice['salesInvoicePaidAmount'], 2, '.', ''),
                                14 => number_format($remainnigAmount, 2, '.', ''),
                            );
                            $arr.=implode(",", $in) . "\n";

                            $itemWithoutTaxTotal += $itemTaxDetails[$invoice['invoiceCode']]['taxWithOutItemTotal'];
                            $normalItemTaxTotal += $itemTaxDetails[$invoice['invoiceCode']]['normalTaxValueForItem'];
                            $compoundItemTaxTotal += $itemTaxDetails[$invoice['invoiceCode']]['compoundTaxValueForItem'];
                            $itemInvoiceTotalWithoutTax += $invoiceTotalWithoutTax;
                            $itemInvoiceTotalTax += $taxDetails[$invoice['invoiceCode']];
                            $itemCreditNoteTotal += $creditNoteWithOutTax;
                            $itemCreditNoteTaxTotal += floatval($invoice['creditNoteTaxAmount']);
                            $itemPaidTotal += $invoice['salesInvoicePaidAmount'];
                            $itemRemainingTotal += $remainnigAmount;

                            $repWithoutTaxTotal += $itemTaxDetails[$invoice['invoiceCode']]['taxWithOutItemTotal'];
                            $repNormalTaxTotal += $itemTaxDetails[$invoice['invoiceCode']]['normalTaxValueForItem'];
                            $repCompoundTaxTotal += $itemTaxDetails[$invoice['invoiceCode']]['compoundTaxValueForItem'];
                            $ss = $invKey.' '.$repName;
                            if(!array_key_exists($ss, $repInvoiceSet)){
                                $repInvoiceTotalWithoutTax += $invoiceTotalWithoutTax;
                                $repInvoiceTotalTax += $taxDetails[$invoice['invoiceCode']];
                                $repCreditNoteTotal += $creditNoteWithOutTax;
                                $repCreditNoteTaxTotal += floatval($invoice['creditNoteTaxAmount']);
                                $repPaidTotal += $invoice['salesInvoicePaidAmount'];
                                $repRemainingTotal += $remainnigAmount;
                                $repInvoiceSet[$ss] = $ss;
                            }

                            $grandWithoutTaxTotal += $itemTaxDetails[$invoice['invoiceCode']]['taxWithOutItemTotal'];
                            $grandNormalTaxTotal += $itemTaxDetails[$invoice['invoiceCode']]['normalTaxValueForItem'];
                            $grandCompoundTaxTotal += $itemTaxDetails[$invoice['invoiceCode']]['compoundTaxValueForItem'];
                            if(!array_key_exists($ss, $grandInvoiceSet)){
                                $grandInvoiceTotalWithoutTax += $invoiceTotalWithoutTax;
                                $grandInvoiceTotalTax += $taxDetails[$invoice['invoiceCode']];
                                $grandCreditNoteTotal += $creditNoteWithOutTax;
                                $grandCreditNoteTaxTotal += floatval($invoice['creditNoteTaxAmount']);
                                $grandPaidTotal += $invoice['salesInvoicePaidAmount'];
                                $grandRemainingTotal += $remainnigAmount;
                                $grandInvoiceSet[$ss] = $ss;
                            }
                        }
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => 'Itemwise Total',
                            6 => number_format($itemWithoutTaxTotal, 2, '.', ''),
                            7 => number_format($normalItemTaxTotal, 2, '.', ''),
                            8 => number_format($compoundItemTaxTotal, 2, '.', ''),
                            9 => number_format($itemInvoiceTotalWithoutTax, 2, '.', ''),
                            10 => number_format($itemInvoiceTotalTax, 2, '.', ''),
                            11 => number_format($itemCreditNoteTotal, 2, '.', ''),
                            12 => number_format($itemCreditNoteTaxTotal, 2, '.', ''),
                            13 => number_format($itemPaidTotal, 2, '.', ''),
                            14 => number_format($itemRemainingTotal, 2, '.', '')
                            );
                        $arr.=implode(",", $in) . "\n";

                    }
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => 'Repwise Total',
                        6 => number_format($repWithoutTaxTotal, 2, '.', ''),
                        7 => number_format($repNormalTaxTotal, 2, '.', ''),
                        8 => number_format($repCompoundTaxTotal, 2, '.', ''),
                        9 => number_format($repInvoiceTotalWithoutTax, 2, '.', ''),
                        10 => number_format($repInvoiceTotalTax, 2, '.', ''),
                        11 => number_format($repCreditNoteTotal, 2, '.', ''),
                        12 => number_format($repCreditNoteTaxTotal, 2, '.', ''),
                        13 => number_format($repPaidTotal, 2, '.', ''),
                        14 => number_format($repRemainingTotal, 2, '.', '')
                        );
                    $arr.=implode(",", $in) . "\n";
                }
                $in = array(
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => '',
                    4 => '',
                    5 => 'Total',
                    6 => number_format($grandWithoutTaxTotal, 2, '.', ''),
                    7 => number_format($grandNormalTaxTotal, 2, '.', ''),
                    8 => number_format($grandCompoundTaxTotal, 2, '.', ''),
                    9 => number_format($grandInvoiceTotalWithoutTax, 2, '.', ''),
                    10 => number_format($grandInvoiceTotalTax, 2, '.', ''),
                    11 => number_format($grandCreditNoteTotal, 2, '.', ''),
                    12 => number_format($grandCreditNoteTaxTotal, 2, '.', ''),
                    13 => number_format($grandPaidTotal, 2, '.', ''),
                    14 => number_format($grandRemainingTotal, 2, '.', '')
                    );
                $arr.=implode(",", $in) . "\n";

            } else {
                $in = array('no matching records found');
                $arr.=implode(",", $in) . "\n";
            }

            $name = "representative_wise_item_report_with_invoice_details";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $reportName], 'csv report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    public function getRepresentativeWiseCategoryData($representativeIds, $categoryIds, $fromDate, $toDate)
    {
        $withDefaultSalesPerson = (in_array('other', $representativeIds)) ? true : false;
        $dataList = [];
        foreach ($representativeIds as $representativeId) {
            //get sales person details
            $salesPerson = $this->getModel("SalesPersonTable")->getSalesPersonDetailsBySalesPersonId($representativeId);
            if ($salesPerson == null && $withDefaultSalesPerson == true) {
                $salesPersonName = "Others";
            } else {
                $salesPersonName = $salesPerson['salesPersonFirstName'] . ' ' . $salesPerson['salesPersonLastName'] . ' (' . $salesPerson['salesPersonSortName'] . ')';
            }
            $categoryArr = array();
            foreach ($categoryIds as $categoryId) {
                //get category details
                $category = $this->getModel('CategoryTable')->getCategory($categoryId);
                //get items according to sales person
                if ($representativeId == "other") {
                    $representativeId = null;
                } else {
                    $representativeId = $representativeId;
                }
                $categoryList = $this->getModel("InvoiceProductTable")->getInvoiceProductByCategoryIdAndSalesPersonId($categoryId, $representativeId, $fromDate, $toDate, $withDefaultSalesPerson);

                if (!empty($categoryList)) {
                    $quantity = $amount = 0;
                    foreach ($categoryList as $i) {
                        $relatedSalesPersons = $this->getModel("InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($i['salesInvoiceID']);

                        $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);
                        $quantity += (($i['salesInvoiceProductQuantity'] / $numOfSP) - ($i['cNPQuantity'] / $numOfSP));
                        $amount += ( ($i['salesInvoiceProductTotal'] / $numOfSP) - ($i['cNPTotal'] / $numOfSP));
                    }
                    if ($quantity > 0) {
                        $categoryArr[$category->categoryName] = array('quantity' => $quantity, 'amount' => $amount);
                    }
                }
            }
            if (!empty($categoryArr)) {
                $dataList[$salesPersonName] = $categoryArr;
            }
        }
        return $dataList;
    }

    public function representativeWiseCategoryPdf($postData)
    {
        try{
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $isAllRepresentative = $postData['isAllRepresentative'];
            $representativeIds = $postData['representativeIds'];
            $isAllCategories = $postData['isAllCategories'];
            $categoryIds = $postData['categoryIds'];

            if ($isAllCategories) {
                $categories = $this->getModel('CategoryTable')->getActiveCategories();
                $categoryIds = array();
                foreach ($categories as $category) {
                    $categoryIds[] = $category['categoryID'];
                }
            }

            $translator = new Translator();
            $name = $translator->translate('Representative wise Category Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'currencySymbol' => $this->companyCurrencySymbol,
                'dataList' => $this->getRepresentativeWiseCategoryData($representativeIds, $categoryIds, $fromDate, $toDate),
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/daily-sales-report/generate-representative-wise-category');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('representative_wise_category_report', $htmlContent, true);


            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }


    public function representativeWiseCategorySheet($postData)
    {
        try{
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $isAllRepresentative = $postData['isAllRepresentative'];
            $representativeIds = $postData['representativeIds'];
            $isAllCategories = $postData['isAllCategories'];
            $categoryIds = $postData['categoryIds'];
            $cD = $this->getCompanyDetails();

            if ($isAllCategories) {
                $categories = $this->getModel('CategoryTable')->getActiveCategories();
                $categoryIds = array();
                foreach ($categories as $category) {
                    $categoryIds[] = $category['categoryID'];
                }
            }

            $dataList = $this->getRepresentativeWiseCategoryData($representativeIds, $categoryIds, $fromDate, $toDate);
            if (!empty($dataList)) {
                $title = '';
                $in = array(
                    0 => "",
                    1 => 'REPRESENTATIVE WISE CATEGORY REPORT',
                    2 => ""
                );
                $title = implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Period :' . $fromDate . '-' . $toDate
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("REPRESENTATIVE NAME", "CATEGORY NAME", "TOTAL QUANTITY", "TOTAL PRICE");
                $header = implode(",", $arrs);
                $arr = '';

                $grandTotal = 0;
                foreach ($dataList as $repName => $categoryList) {
                    $in = array(
                        0 => $repName
                    );
                    $arr.=implode(",", $in) . "\n";

                    $totalAmount = 0;
                    foreach ($categoryList as $categoryName => $category) {
                        $in = array(
                            0 => '',
                            1 => $categoryName,
                            2 => $category['quantity'],
                            3 => number_format($category['amount'], 2, '.', '')
                        );
                        $arr.=implode(",", $in) . "\n";
                        $totalAmount = $totalAmount + $category['amount'];
                    }
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => 'Total',
                        3 => number_format($totalAmount, 2, '.', '')
                    );
                    $arr.=implode(",", $in) . "\n";
                    $grandTotal = $grandTotal + $totalAmount;
                }
                $in = array(
                    0 => '',
                    1 => '',
                    2 => 'Grand Total',
                    3 => number_format($grandTotal, 2, '.', '')
                );
                $arr.=implode(",", $in);
            } else {
                $in = array('no matching records found');
                $arr.=implode(",", $in) . "\n";
            }

            $name = "representative_wise_category_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    public function _getGrossProfitData($locationList, $fromDate, $toDate, $cusCategories = NULL)
    {
        if (isset($locationList) && isset($fromDate) && isset($toDate)) {
            $getSalesInvoiceData = $this->getModel('ItemOutTable')->getSalesInvoiceDetails($fromDate, $toDate, $cusCategories);
            $getCreditNoteData = $this->getModel('ItemInTable')->getCreditNoteDetails($fromDate, $toDate, $cusCategories);
            $mergeData = array_merge_recursive($getCreditNoteData, $getSalesInvoiceData);
            $grossInvoiceCreditData = [];


            //Sales Invoice and Credit note related data put into one array
            //beacasue of easy to calculations
            foreach ($mergeData as $key => $d) {
                $date = $d['date'];
                $documentType = NULL;
                $documentNo = NULL;
                $documentValue = NULL;
                $documentCost = NULL;
                if (isset($d['itemOutDocumentType'])) {

                    $documentType = $d['itemOutDocumentType'];
                    $documentNo = $d['salesInvoiceCode'];
                    $documentCost = (floatval($d['totalInvoiceCost']) - (floatval($d['itemOutQty']) * floatval($d['itemInDiscount'])));
                    $documentValue = $d['itemOutQty']*($d['itemOutPrice']-$d['itemOutDiscount']);
                    //get invoice discount
                    $invoiceDetails = $this->getModel("InvoiceTable")->getInvoiceByID($d['itemOutDocumentID']);
                    // $nonInventoryItemTotal = $this->getModel("Invoice\Model\InvoiceTable")->getNonInventoryItemTotal($d['itemOutDocumentID']);
                    $nonInventoryItemTotal = 0;
                    //create array and add invoiceIds to it for optimizing quary
                    $invoiceIdsTempArray = [];
                    if(!array_key_exists($d['itemOutDocumentType'], $invoiceIdsTempArray)){
                        //get non inventory details
                        $nonInventoryDataSet = $this->getModel("InvoiceTable")->getNonAllInventoryItemTotalByInvoiceID($d['itemOutDocumentID']);
                        $nonInvetCalculatedTotal = 0;

                        foreach ($nonInventoryDataSet as $key => $nonSingleValue) {
                            if($nonSingleValue['salesInvoiceProductDiscount'] != null && $nonSingleValue['salesInvoiceProductDiscountType'] == 'precentage'){

                                $nonInvetCalculatedTotal += (floatval($nonSingleValue['salesInvoiceProductQuantity']) * floatval($nonSingleValue['salesInvoiceProductPrice'])) * (100- floatval($nonSingleValue['salesInvoiceProductDiscount']))/100;

                            } else if( $nonSingleValue['salesInvoiceProductDiscount'] != null && $nonSingleValue['salesInvoiceProductDiscountType'] == 'value') {

                                $nonInvetCalculatedTotal += (floatval($nonSingleValue['salesInvoiceProductQuantity']) * floatval($nonSingleValue['salesInvoiceProductPrice'])) - floatval($nonSingleValue['salesInvoiceProductDiscount']);
                            } else {

                                $nonInvetCalculatedTotal += (floatval($nonSingleValue['salesInvoiceProductQuantity']) * floatval($nonSingleValue['salesInvoiceProductPrice']));
                            }

                        }
                        $invoiceIdsTempArray[$d['itemOutDocumentType']] = $d['itemOutDocumentType'];
                        $nonInventoryItemTotal = $nonInvetCalculatedTotal;

                    }


                    $nonInventoryItemTotal = ($nonInventoryItemTotal) ? $nonInventoryItemTotal : 0;
                    $invoiceWiseTotalDiscount = ($invoiceDetails->salesInvoiceWiseTotalDiscount) ? $invoiceDetails->salesInvoiceWiseTotalDiscount : 0;

                    // $documentValue = $documentValue - $invoiceWiseTotalDiscount;
                    $invoiceWiseTotalDiscount = $invoiceDetails->salesInvoiceWiseTotalDiscount;

                    $grnID = $d['itemInDocumentID'];
                    $locationProductID = $d['itemInLocationProductID'];
                    $getProductTax = $this->getModel("GrnProductTaxTable")->getGrnProductTax($grnID, $locationProductID);
                    $totalTax = 0;
                    foreach ($getProductTax as $t) {
                        $totalTax += $t['grnTaxAmount'] / $t['grnProductTotalQty'];
                    }
                    $grossInvoiceCreditData['invoice'][$d['itemOutDocumentID']][] = [
                        'locationID' => $d['locationID'],
                        'locationName' => $d['locationName'],
                        'locationCode' => $d['locationCode'],
                        'date' => $date,
                        'documentNo' => $documentNo,
                        'documentType' => $documentType,
                        'documentValue' => $documentValue,
                        'nonInventoryItemTotal' => $nonInventoryItemTotal,
                        'documentCost' => $documentCost,
                        'totalTax' => $totalTax,
                        'qty' => $d['itemOutQty'],
                        'cusName' => $d['customerCode'].' - '.$d['customerName'],
                        'invoiceWiseTotalDiscount' => $invoiceWiseTotalDiscount,
                    ];

                } else if (isset($d['itemInDocumentType'])) {

                    $documentType = $d['itemInDocumentType'];
                    $documentNo = $d['creditNoteCode'];
                    $documentValue = $d['creditNoteTotal'];
                    $documentCost = $d['cNoteTotalCost'] - ($d['itemInQty'] * $d['itemInDiscount']);

                    $getCreditNoteTax = $this->getModel("CreditNoteProductTaxTable")->getCreditNoteProductTax($d['creditNoteID'], $d['itemInLocationProductID'], $d['locationID']);
                    $totalTax = 0;
                    foreach ($getCreditNoteTax as $t) {
                        $totalTax += $t['creditNoteTaxAmount'] / $t['creditNoteProductQuantity'];
                    }

                    $grossInvoiceCreditData['credit'][$d['creditNoteID']][] = [
                        'locationID' => $d['locationID'],
                        'locationName' => $d['locationName'],
                        'locationCode' => $d['locationCode'],
                        'date' => $date,
                        'documentNo' => $documentNo,
                        'documentType' => $documentType,
                        'documentValue' => $documentValue,
                        'nonInventoryItemTotal' => 0,
                        'documentCost' => $documentCost,
                        'totalTax' => $totalTax,
                        'qty' => $d['itemInQty'],
                        'cusName' => $d['customerCode'].' - '.$d['customerName']

                    ];
                }
            }

            $invoiceData = [];
            if ($grossInvoiceCreditData['invoice']) {
                foreach ($grossInvoiceCreditData['invoice'] as $iKey => $subData) {
                    $documentCost = 0;
                    $totalTax = 0;
                    $documentValue = 0;
                    $invoiceWiseTotalDiscount = 0;
                    foreach ($subData as $key => $value) {

                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $nonInventoryItemTotal = $value['nonInventoryItemTotal'];
                        $documentValue += $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];
                        $invoiceWiseTotalDiscount = $value['invoiceWiseTotalDiscount'];
                    }
                    $invoiceData[$iKey] = ['locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentCost' => $documentCost,
                        'documentNo' => $documentNo,
                        'documentType' => $documentType,
                        'documentValue' => floatval($documentValue) - floatval($invoiceWiseTotalDiscount),
                        // 'documentValue' => $documentInvValue,
                        'nonInventoryItemTotal' => $nonInventoryItemTotal,
                        'totalTax' => $totalTax,
                        'cusName' => $value['cusName']
                        ];
                }
            }


            $creditNoteData = [];
            if ($grossInvoiceCreditData['credit']) {
                foreach ($grossInvoiceCreditData['credit'] as $cKey => $subData) {
                    $documentCost = 0;
                    $totalTax = 0;
                    foreach ($subData as $key => $value) {
                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $documentValue = $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];

                    }
                    //calculate creditNote wise total taxes
                    $totalTax = $this->getModel('CreditNoteProductTaxTable')->getTotalCreditNoteProductTax($cKey)->current();

                    $creditNoteData[$cKey] = ['locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax['totalTax'],
                        'cusName' => $value['cusName']
                    ];
                }
            }

            // $grossProfitData = array_values($invoiceData + $creditNoteData);
            $grossProfitData = array_merge($invoiceData, $creditNoteData);


            //get jobcard related invoices
            $getJobCardInvoices = $this->getModel("InvoiceTable")->getJobCardInvoices($fromDate, $toDate, $cusCategories);
            foreach ($getJobCardInvoices as $key => $value) {
                if ($value['jobID'] != "0") {
                    
                    // get job product details by given jobCost id
                    $jobProductCostData = $this->getModel('JobProductCostTable')->getJobProductDeliveryNoteDataByInvoiceId($value['salesInvoiceID']);

                    $documentCost = 0;
                    foreach ($jobProductCostData as $val) {
                        if (!is_null($val['jobProductCostQty'])) {
                            $deliveryNoteProductCostData = $this->getModel('DeliveryNoteTable')->getDeliveryNoteProductCostByDeliveryNoteProductID($val['deliveryNoteProductID']);
                        $documentCost += floatval($deliveryNoteProductCostData['itemInPrice']) * floatval($val['jobProductCostQty']) - ($deliveryNoteProductCostData['itemInDiscount'] * $val['jobProductCostQty']);
                        }
                    }

                    $documentNo = $value['salesInvoiceCode'];
                    $documentValue = (floatval($value['salesinvoiceTotalAmount'])-floatval($value['saleInvoiceTotalTax']));
                    $date = $value['salesInvoiceIssuedDate'];
                  

                    $grossInvoiceCreditData['jobCard'][$value['salesInvoiceID']][] = array(
                        'locationID' => $value['locationID'],
                        'locationName' => $value['locationName'],
                        'locationCode' => $value['locationCode'],
                        'date' => $date,
                        'documentNo' => $documentNo,
                        'documentType' => "Sales Invoice",
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'cusName' => $value['customerCode'].' - '.$value['customerName']

                    );
                }
            }

            //get delivery note related invoices
            $getDeliveryNoteInvoices = $this->getModel("InvoiceTable")->getDeliveryNoteInvoices($fromDate, $toDate, $cusCategories);
            //for validate outvalues


            $outIds = [];
            foreach ($getDeliveryNoteInvoices as $key => $value) {
                $documentType = NULL;
                $documentNo = NULL;
                $documentValue = NULL;
                $documentCost = NULL;
                $jobCardID = '';
                $jobCardType = '';
                $date = '';
                $deliveryNoteID = ($value['salesInvoiceProductDocumentID'] != NULL) ? $value['salesInvoiceProductDocumentID'] : '';
                $deliveryNoteData = $this->getModel("ItemOutTable")->getInvoiceDeliveryNoteDetails($deliveryNoteID, $documentType = 'Delivery Note');
                if (!empty($deliveryNoteData)) {
                    foreach ($deliveryNoteData as $d) {
                            $outIds[] = $d['itemOutID'];
                            $checkProductInInvoice = $this->getModel("InvoiceProductTable")->gelSalesInvoiceProductDetailsByInvoiceIDAndLocationProductID($value['salesInvoiceID'], $d['itemOutLocationProductID'])->current();
                            if(isset($checkProductInInvoice['salesInvoiceProductID'])){ 
                                $documentType = $d['itemOutDocumentType'];
                                $documentNo = $value['salesInvoiceCode'];
                                $documentValue = (($d['itemOutPrice'] * $d['itemOutQty']) - (floatval($d['itemOutDiscount'])) * $d['itemOutQty']);
                                $documentCost = ($d['itemInPrice'] * $d['itemOutQty']) - ($d['itemInDiscount'] * $d['itemOutQty']);
                                $date = $value['salesInvoiceIssuedDate'];

                                $grnID = $d['itemInDocumentID'];
                                $locationProductID = $d['itemInLocationProductID'];
                                $getProductTax = $this->getModel("GrnProductTaxTable")->getGrnProductTax($grnID, $locationProductID);
                                $totalTax = 0;
                                foreach ($getProductTax as $t) {
                                    $totalTax += $t['grnTaxAmount'] / $t['grnProductTotalQty'];
                                }

                                $grossInvoiceCreditData['delivery'][$value['salesInvoiceID']][] = array(
                                    'locationID' => $value['locationID'],
                                    'locationName' => $value['locationName'],
                                    'locationCode' => $value['locationCode'],
                                    'date' => $date,
                                    'documentNo' => $documentNo,
                                    'documentType' => "Sales Invoice",
                                    'documentValue' => $documentValue,
                                    'documentCost' => $documentCost,
                                    'totalTax' => $totalTax,
                                    'qty' => $d['itemOutQty'],
                                    'cusName' => $value['customerCode'].' - '.$value['customerName']
                                );
                            }
                    }
                }
            }

            //get non inventory invoices
            $getNonInventoryInvoices = $this->getModel("InvoiceTable")->getNonInventoryInvoices($fromDate, $toDate, $cusCategories);

            foreach ($getNonInventoryInvoices as $key => $value) {
                $documentNo = $value['salesInvoiceCode'];
                $documentValue = (floatval($value['salesInvoiceProductGrandTotal'])-floatval($value['saleInvoiceTotalTax']));
                $date = $value['salesInvoiceIssuedDate'];

                $grossInvoiceCreditData['nonInventory'][$value['salesInvoiceID']][] = array(
                    'locationID' => $value['locationID'],
                    'locationName' => $value['locationName'],
                    'locationCode' => $value['locationCode'],
                    'date' => $date,
                    'documentNo' => $documentNo,
                    'documentType' => "Sales Invoice",
                    'documentValue' => $documentValue,
                    'documentCost' => 0,
                    'cusName' => $value['customerCode'].' - '.$value['customerName']
                );
            }

            //get direct credit note
            $getDirectCreditNote = $this->getModel("CreditNoteTable")->getDirectCreditNote($fromDate, $toDate, $cusCategories);

            foreach ($getDirectCreditNote as $key => $value) {
                $documentNo = $value['creditNoteCode'];
                $documentValue = (floatval($value['creditNoteTotal'])-floatval($value['creditNoteTotalTax']));
                $date = $value['creditNoteDate'];

                $grossInvoiceCreditData['directCreditNote'][$value['creditNoteID']][] = array(
                    'locationID' => $value['locationID'],
                    'locationName' => $value['locationName'],
                    'locationCode' => $value['locationCode'],
                    'date' => $date,
                    'documentNo' => $documentNo,
                    'documentType' => "Credit Note",
                    'documentValue' => $documentValue,
                    'documentCost' => 0,
                    'cusName' => $value['customerCode'].' - '.$value['customerName']
                );
            }

            //get credit note from DLN
            $dreditNoteFrmDLN = $this->getModel("CreditNoteTable")->getCreditNoteFromDLN($fromDate, $toDate, $cusCategories);

            foreach ($dreditNoteFrmDLN as $key => $value) {
                $deliveryNoteID = $value['salesInvoiceProductDocumentID'];
                $totalCostOfCreditNote = $this->getModel("ItemOutTable")->getCostForDelivaryNote($deliveryNoteID)['totalCostOfCreditNote'];
                $documentNo = $value['creditNoteCode'];
                $documentValue = (floatval($value['creditNoteTotal'])-floatval($value['creditNoteTotalTax']));
                $date = $value['creditNoteDate'];

                $grossInvoiceCreditData['creditNoteFromDLN'][$value['creditNoteID']][] = array(
                    'locationID' => $value['locationID'],
                    'locationName' => $value['locationName'],
                    'locationCode' => $value['locationCode'],
                    'date' => $date,
                    'documentNo' => $documentNo,
                    'documentType' => "Credit Note",
                    'documentValue' => $documentValue,
                    'documentCost' => $totalCostOfCreditNote,
                    'cusName' => $value['customerCode'].' - '.$value['customerName']
                );
            }

            $count = count($grossProfitData);
            if ($grossInvoiceCreditData['jobCard']) {
                foreach ($grossInvoiceCreditData['jobCard'] as $subData) {
                    $documentCost = 0;
                    $totalTax = 0;
                    foreach ($subData as $key => $value) {

                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $documentValue = $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];
                        $cusNamee = $value['cusName'];
                    }
                    $grossProfitData[++$count] = [
                        'locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax,
                        'cusName' => $cusNamee
                    ];
                }
            }

            if ($grossInvoiceCreditData['delivery']) {
                foreach ($grossInvoiceCreditData['delivery'] as $subData) {

                    $documentCost = 0;
                    $totalTax = 0;
                    $documentValue = 0;
                    foreach ($subData as $key => $value) {
                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $documentValue += $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];
                        $cusName = $value['cusName'];
                    }
                    $grossProfitData[++$count] = [
                        'locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax,
                        'cusName' => $cusName

                    ];
                }
            }

            if ($grossInvoiceCreditData['nonInventory']) {
                foreach ($grossInvoiceCreditData['nonInventory'] as $subData) {

                    $documentCost = 0;
                    $totalTax = 0;
                    $documentValue = 0;
                    foreach ($subData as $key => $value) {
                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $documentValue += $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += 0;
                        $cusName = $value['cusName'];
                    }
                    $grossProfitData[++$count] = [
                        'locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax,
                        'cusName' => $cusName

                    ];
                }
            }

            if ($grossInvoiceCreditData['directCreditNote']) {
                foreach ($grossInvoiceCreditData['directCreditNote'] as $subData) {

                    $documentCost = 0;
                    $totalTax = 0;
                    $documentValue = 0;
                    foreach ($subData as $key => $value) {
                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $documentValue += $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += 0;
                        $cusName = $value['cusName'];
                    }
                    $grossProfitData[++$count] = [
                        'locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax,
                        'cusName' => $cusName

                    ];
                }
            }

            if ($grossInvoiceCreditData['creditNoteFromDLN']) {
                foreach ($grossInvoiceCreditData['creditNoteFromDLN'] as $subData) {

                    $documentCost = 0;
                    $totalTax = 0;
                    $documentValue = 0;
                    foreach ($subData as $key => $value) {
                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $documentValue += $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += 0;
                        $cusName = $value['cusName'];
                    }
                    $grossProfitData[++$count] = [
                        'locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax,
                        'cusName' => $cusName

                    ];
                }
            }

            //sorting data on $grossProfitData array according to date element
            $sortData = [];
            foreach ($grossProfitData as $key => $r) {
                $sortData[$key] = ($r['date']);
            }
            //sort $grossProfitData array according to sorted $sortData array on 'date' element
            array_multisort($sortData, SORT_ASC, $grossProfitData);

            $arr = [];



            foreach ($grossProfitData as $element) {

                $documentValue = (isset($arr[$element['documentNo']])) ? $arr[$element['documentNo']]['documentValue'] + $element['documentValue'] : $element['documentValue'];

                $documentCost = (isset($arr[$element['documentNo']])) ? $arr[$element['documentNo']]['documentCost'] + $element['documentCost'] : $element['documentCost'];

                $arr[$element['documentNo']] = [
                    'locationID' => $element['locationID'],
                    'locationName' => $element['locationName'],
                    'locationCode' => $element['locationCode'],
                    'date' => $element['date'],
                    'documentValue' => $documentValue,
                    'documentCost' => $documentCost,
                    'documentType' => $element['documentType'],
                    'documentNo' => $element['documentNo'],
                    'totalTax' => $element['totalTax'],
                    'cusName' => $element['cusName'],
                ];
            }

            $locWiseGrossProfitData = array_fill_keys($locationList, '');
            foreach ($arr as $key => $v) {
                if (array_key_exists($v['locationID'], $locWiseGrossProfitData)) {
                    $locWiseGrossProfitData[$v['locationID']]['locGPD'][$key] = $v;
                    $locWiseGrossProfitData[$v['locationID']]['locationName'] = $v['locationName'];
                    $locWiseGrossProfitData[$v['locationID']]['locationCode'] = $v['locationCode'];
                }
            }

            return array_filter($locWiseGrossProfitData);
        }
    }

    public function grossProfitReportPdf($postData)
    {
        try{
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $locationList = $postData['locationList'];
            $cusCategories = $postData['cusCategories'];

            $grossProfitData = $this->_getGrossProfitData($locationList, $fromDate, date('Y-m-d H:i:s', strtotime($toDate)), $cusCategories);
            $name = 'Gross Profit Report';

            $headerView = $this->headerViewTemplate($name, $period = $fromDate . ' - ' . $toDate);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $grossProfitView = new ViewModel(array(
                'grossProfitData' => $grossProfitData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $grossProfitView->setTemplate('reporting/sales-report/generate-gross-profit-pdf');
            $grossProfitView->setTerminal(true);
            $htmlContent = $this->viewRendererHtmlContent($grossProfitView);
            $pdfPath = $this->downloadPDF('gross-profit-report', $htmlContent, true);

            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    public function grossProfitReportSheet($postData)
    {
        try{
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $locationList = $postData['locationList'];
            $cusCategories = $postData['cusCategories'];

            $grossProfitData = $this->_getGrossProfitData($locationList, $fromDate, date('Y-m-d H:i:s', strtotime($toDate)), $cusCategories);
            $cD = $this->getCompanyDetails();

            if (empty($grossProfitData)) {
                $arr = 'no matching records founds \n';
            } else {
                $title = '';
                $tit = 'GROSS PROFIT REPORT';
                $in = ["", $tit, "", ""];
                $title.=implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = ["Period:", $fromDate . '-' . $toDate, "", ""];
                $title.=implode(",", $in) . "\n";

                $in = [
                    $cD[0]->companyName,
                    $cD[0]->companyAddress,
                    'Tel: ' . $cD[0]->telephoneNumber
                ];
                $title.=implode(",", $in) . "\n";

                $arrs = ["NO", "LOCATION NAME - CODE", "DATE/TIME", "CUSTOMER NAME","DOCUMENT TYPE", "DOCUMENT CODE", "VALUE", "COST", "GROSS PROFIT", "GP PERCENTAGE"];
                $header = implode(",", $arrs);
                $arr = '';
                $totalValue = 0;
                $totalCost = 0;
                $totalGrossProfit = 0;

                foreach ($grossProfitData as $key => $d) {
                    if ($d['locGPD']) {
                        $i = 1;
                        $grossProfit = 0;
                        $in = ['', $d['locationName'] . ' - ' . $d['locationCode']];
                        $arr.= implode(",", $in) . "\n";

                        foreach ($d['locGPD'] as $v) {

                            $value = floatval($v['documentValue']) + floatval($v['nonInventoryItemTotal']) ? floatval($v['documentValue']) + floatval($v['nonInventoryItemTotal']) : 0;
                            $cost = $v['documentCost'] ? $v['documentCost'] : 0;
                            $totalTax = $v['totalTax'] ? $v['totalTax'] : 0;
                            if ($v['documentType'] == 'Sales Invoice') {
                                $grossProfit = $value - ($cost);
                                $totalValue+=$value;
                                $totalCost+=($cost);
                            } else if ($v['documentType'] == 'Credit Note') {
                                $value = $value - $totalTax;
                                $grossProfit = ($cost) - $value;
                                $totalValue-=$value;
                                $totalCost-=($cost);
                            }
                            $totalGrossProfit+=$grossProfit;

                            $gpPercentage = ($value == 0) ? '-' : round(floatval(($grossProfit/$value) * 100), 2).'%'; 

                            $in = [
                                $i, '', $v['date'], $v['cusName'], $v['documentType'], $v['documentNo'], $value, $cost , $grossProfit,$gpPercentage
                            ];
                            $arr.= implode(",", $in) . "\n";
                            $i++;
                        }
                    }
                }
                $in = ['', '', '', '', '', 'Sub Total', $totalValue, $totalCost, $totalGrossProfit, ''];
                $arr.= implode(",", $in) . "\n";
            }
            $name = "gross_profit_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }

    }

    public function _getGrossProfitDataForSalesDashboard($locationList, $fromDate, $toDate, $cusCategories = NULL)
    {
        if (isset($locationList) && isset($fromDate) && isset($toDate)) {
            $getSalesInvoiceData = $this->getModel('ItemOutTable')->getSalesInvoiceDetailsForDashboard($fromDate, $toDate, $cusCategories, $locationList[0]);
            $getCreditNoteData = $this->getModel('ItemInTable')->getCreditNoteDetailsForDashboard($fromDate, $toDate, $cusCategories, $locationList[0]);
            $mergeData = array_merge_recursive($getCreditNoteData, $getSalesInvoiceData);
            $grossInvoiceCreditData = [];

            //Sales Invoice and Credit note related data put into one array
            //beacasue of easy to calculations
            foreach ($mergeData as $key => $d) {
                $date = $d['date'];
                $documentType = NULL;
                $documentNo = NULL;
                $documentValue = NULL;
                $documentCost = NULL;
                if (isset($d['itemOutDocumentType'])) {

                    $documentType = $d['itemOutDocumentType'];
                    $documentNo = $d['salesInvoiceCode'];
                    $documentCost = (floatval($d['totalInvoiceCost']) - (floatval($d['itemOutQty']) * floatval($d['itemInDiscount'])));
                    $documentValue = $d['itemOutQty']*($d['itemOutPrice']-$d['itemOutDiscount']);
                    //get invoice discount
                    $invoiceDetails = $this->getModel("InvoiceTable")->getInvoiceByID($d['itemOutDocumentID']);
                    // $nonInventoryItemTotal = $this->getModel("Invoice\Model\InvoiceTable")->getNonInventoryItemTotal($d['itemOutDocumentID']);
                    $nonInventoryItemTotal = 0;
                    //create array and add invoiceIds to it for optimizing quary
                    $invoiceIdsTempArray = [];
                    if(!array_key_exists($d['itemOutDocumentType'], $invoiceIdsTempArray)){
                        //get non inventory details
                        $nonInventoryDataSet = $this->getModel("InvoiceTable")->getNonAllInventoryItemTotalByInvoiceID($d['itemOutDocumentID']);
                        $nonInvetCalculatedTotal = 0;

                        foreach ($nonInventoryDataSet as $key => $nonSingleValue) {
                            if($nonSingleValue['salesInvoiceProductDiscount'] != null && $nonSingleValue['salesInvoiceProductDiscountType'] == 'precentage'){

                                $nonInvetCalculatedTotal += (floatval($nonSingleValue['salesInvoiceProductQuantity']) * floatval($nonSingleValue['salesInvoiceProductPrice'])) * (100- floatval($nonSingleValue['salesInvoiceProductDiscount']))/100;

                            } else if( $nonSingleValue['salesInvoiceProductDiscount'] != null && $nonSingleValue['salesInvoiceProductDiscountType'] == 'value') {

                                $nonInvetCalculatedTotal += (floatval($nonSingleValue['salesInvoiceProductQuantity']) * floatval($nonSingleValue['salesInvoiceProductPrice'])) - floatval($nonSingleValue['salesInvoiceProductDiscount']);
                            } else {

                                $nonInvetCalculatedTotal += (floatval($nonSingleValue['salesInvoiceProductQuantity']) * floatval($nonSingleValue['salesInvoiceProductPrice']));
                            }

                        }
                        $invoiceIdsTempArray[$d['itemOutDocumentType']] = $d['itemOutDocumentType'];
                        $nonInventoryItemTotal = $nonInvetCalculatedTotal;

                    }


                    $nonInventoryItemTotal = ($nonInventoryItemTotal) ? $nonInventoryItemTotal : 0;
                    $invoiceWiseTotalDiscount = ($invoiceDetails->salesInvoiceWiseTotalDiscount) ? $invoiceDetails->salesInvoiceWiseTotalDiscount : 0;

                    // $documentValue = $documentValue - $invoiceWiseTotalDiscount;
                    $invoiceWiseTotalDiscount = $invoiceDetails->salesInvoiceWiseTotalDiscount;

                    $grnID = $d['itemInDocumentID'];
                    $locationProductID = $d['itemInLocationProductID'];
                    $getProductTax = $this->getModel("GrnProductTaxTable")->getGrnProductTax($grnID, $locationProductID);
                    $totalTax = 0;
                    foreach ($getProductTax as $t) {
                        $totalTax += $t['grnTaxAmount'] / $t['grnProductTotalQty'];
                    }
                    $grossInvoiceCreditData['invoice'][$d['itemOutDocumentID']][] = [
                        // 'locationID' => $d['locationID'],
                        // 'date' => $date,
                        'documentNo' => $documentNo,
                        'documentType' => $documentType,
                        'documentValue' => $documentValue,
                        'nonInventoryItemTotal' => $nonInventoryItemTotal,
                        'documentCost' => $documentCost,
                        'totalTax' => $totalTax,
                        'qty' => $d['itemOutQty'],
                        'invoiceWiseTotalDiscount' => $invoiceWiseTotalDiscount,
                    ];

                } else if (isset($d['itemInDocumentType'])) {
                    
                    $documentType = $d['itemInDocumentType'];
                    $documentNo = $d['creditNoteCode'];
                    $documentValue = $d['creditNoteTotal'];
                    $documentCost = $d['cNoteTotalCost'] - ($d['itemInQty'] * $d['itemInDiscount']);

                    $getCreditNoteTax = $this->getModel("CreditNoteProductTaxTable")->getCreditNoteProductTax($d['creditNoteID'], $d['itemInLocationProductID'], $d['locationID']);
                    $totalTax = 0;
                    foreach ($getCreditNoteTax as $t) {
                        $totalTax += $t['creditNoteTaxAmount'] / $t['creditNoteProductQuantity'];
                    }

                    $grossInvoiceCreditData['credit'][$d['creditNoteID']][] = [
                        // 'locationID' => $d['locationID'],
                        // 'date' => $date,
                        'documentNo' => $documentNo,
                        'documentType' => $documentType,
                        'documentValue' => $documentValue,
                        'nonInventoryItemTotal' => 0,
                        'documentCost' => $documentCost,
                        'totalTax' => $totalTax,
                        'qty' => $d['itemInQty'],

                    ];
                }
            }
            $invoiceData = [];
            if ($grossInvoiceCreditData['invoice']) {
                foreach ($grossInvoiceCreditData['invoice'] as $iKey => $subData) {
                    $documentCost = 0;
                    $totalTax = 0;
                    $documentValue = 0;
                    $invoiceWiseTotalDiscount = 0;
                    foreach ($subData as $key => $value) {

                        // $locationID = $value['locationID'];
                        // $date = $value['date'];
                        $nonInventoryItemTotal = $value['nonInventoryItemTotal'];
                        $documentValue += $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];
                        $invoiceWiseTotalDiscount = $value['invoiceWiseTotalDiscount'];
                    }
                    $invoiceData[$iKey] = [
                        // 'locationID' => $locationID,
                        // 'date' => $date,
                        'documentCost' => $documentCost,
                        'documentNo' => $documentNo,
                        'documentType' => $documentType,
                        'documentValue' => floatval($documentValue) - floatval($invoiceWiseTotalDiscount),
                        'nonInventoryItemTotal' => $nonInventoryItemTotal,
                        'totalTax' => $totalTax,
                        ];
                }
            }


            $creditNoteData = [];
            if ($grossInvoiceCreditData['credit']) {
                foreach ($grossInvoiceCreditData['credit'] as $cKey => $subData) {
                    $documentCost = 0;
                    $totalTax = 0;
                    foreach ($subData as $key => $value) {
                        // $locationID = $value['locationID'];
                        // $date = $value['date'];
                        $documentValue = $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];

                    }
                    //calculate creditNote wise total taxes
                    $totalTax = $this->getModel('CreditNoteProductTaxTable')->getTotalCreditNoteProductTax($cKey)->current();

                    $creditNoteData[$cKey] = [
                        // 'locationID' => $locationID,
                        // 'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax['totalTax'],
                    ];
                }
            }

            // $grossProfitData = array_values($invoiceData + $creditNoteData);
            $grossProfitData = array_merge($invoiceData, $creditNoteData);
            $count = count($grossProfitData);


            //get jobcard related invoices
            $getJobCardInvoices = $this->getModel("InvoiceTable")->getJobCardInvoicesForSalesDashboard($fromDate, $toDate, $cusCategories, $locationList[0]);
            foreach ($getJobCardInvoices as $key => $value) {
                if ($value['jobID'] != "0") {
                    
                    // get job product details by given jobCost id
                    $jobProductCostData = $this->getModel('JobProductCostTable')->getJobProductDeliveryNoteDataByInvoiceId($value['salesInvoiceID']);

                    $documentCost = 0;
                    foreach ($jobProductCostData as $val) {
                        if (!is_null($val['jobProductCostQty'])) {
                            $deliveryNoteProductCostData = $this->getModel('DeliveryNoteTable')->getDeliveryNoteProductCostByDeliveryNoteProductID($val['deliveryNoteProductID']);
                        $documentCost += floatval($deliveryNoteProductCostData['itemInPrice']) * floatval($val['jobProductCostQty']) - ($deliveryNoteProductCostData['itemInDiscount'] * $val['jobProductCostQty']);
                        }
                    }

                    $documentNo = $value['salesInvoiceCode'];
                    $documentValue = (floatval($value['salesinvoiceTotalAmount'])-floatval($value['saleInvoiceTotalTax']));
                    $date = $value['salesInvoiceIssuedDate'];
                  

                    $grossProfitData[++$count] = [
                        // 'locationID' => $value['locationID'],
                        // 'date' => $date,
                        'documentNo' => $documentNo,
                        'documentType' => "Sales Invoice",
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,

                    ];
                }
            }

            //get delivery note related invoices
            $getDeliveryNoteInvoices = $this->getModel("InvoiceTable")->getDeliveryNoteInvoicesForSalesDashboard($fromDate, $toDate, $cusCategories, $locationList[0]);
            //for validate outvalues
            // var_dump(count($getDeliveryNoteInvoices)).die();


            $outIds = [];
            foreach ($getDeliveryNoteInvoices as $key => $value) {
                $documentType = NULL;
                $documentNo = NULL;
                $documentValue = NULL;
                $documentCost = NULL;
                $jobCardID = '';
                $jobCardType = '';
                $date = '';
                $deliveryNoteID = ($value['salesInvoiceProductDocumentID'] != NULL) ? $value['salesInvoiceProductDocumentID'] : '';
                $deliveryNoteData = $this->getModel("ItemOutTable")->getInvoiceDeliveryNoteDetailsForDashboard($deliveryNoteID, $documentType = 'Delivery Note');
                if (!empty($deliveryNoteData)) {
                    foreach ($deliveryNoteData as $d) {
                            $outIds[] = $d['itemOutID'];
                            $checkProductInInvoice = $this->getModel("InvoiceProductTable")->gelSalesInvoiceProductDetailsByInvoiceIDAndLocationProductIDForDashboard($value['salesInvoiceID'], $d['itemOutLocationProductID'])->current();
                            if(isset($checkProductInInvoice['salesInvoiceProductID'])){
                                $documentType = $d['itemOutDocumentType'];
                                $documentNo = $value['salesInvoiceCode'];
                                // $documentValue = (floatval($value['salesInvoiceProductTotal'])-floatval($value['saleInvoiceTotalTax']));
                                // $documentCost = ($d['itemInPrice'] * $value['salesInvoiceProductQuantity']) - ($d['itemInDiscount'] * $d['itemOutQty']);
                                $documentValue = (($d['itemOutPrice'] * $d['itemOutQty']) - (floatval($d['itemOutDiscount'])) * $d['itemOutQty']);
                                $documentCost = ($d['itemInPrice'] * $d['itemOutQty']) - ($d['itemInDiscount'] * $d['itemOutQty']);

                                $date = $value['salesInvoiceIssuedDate'];

                                $grnID = $d['itemInDocumentID'];
                                $locationProductID = $d['itemInLocationProductID'];
                                $getProductTax = $this->getModel("GrnProductTaxTable")->getGrnProductTax($grnID, $locationProductID);
                                $totalTax = 0;
                                foreach ($getProductTax as $t) {
                                    $totalTax += $t['grnTaxAmount'] / $t['grnProductTotalQty'];
                                }

                                $grossProfitData[++$count] = [
                                    // 'locationID' => $value['locationID'],
                                    // 'date' => $date,
                                    'documentNo' => $documentNo,
                                    'documentType' => "Sales Invoice",
                                    'documentValue' => $documentValue,
                                    'documentCost' => $documentCost,
                                    'totalTax' => $totalTax,
                                    'qty' => $d['itemOutQty'],
                                ];
                            }
                    }
                }
            }
            

            // var_dump($grossInvoiceCreditData).die();

            //get non inventory invoices
            $getNonInventoryInvoices = $this->getModel("InvoiceTable")->getNonInventoryInvoicesForSalesDashboard($fromDate, $toDate, $cusCategories, $locationList[0]);

            foreach ($getNonInventoryInvoices as $key => $value) {
                $documentNo = $value['salesInvoiceCode'];
                $documentValue = (floatval($value['salesInvoiceProductGrandTotal'])-floatval($value['saleInvoiceTotalTax']));
                $date = $value['salesInvoiceIssuedDate'];

                $grossProfitData[++$count] = [
                    // 'locationID' => $value['locationID'],
                    // 'date' => $date,
                    'documentNo' => $documentNo,
                    'documentType' => "Sales Invoice",
                    'documentValue' => $documentValue,
                    'documentCost' => 0,
                ];
            }

            //get direct credit note
            $getDirectCreditNote = $this->getModel("CreditNoteTable")->getDirectCreditNoteForSalesDashboard($fromDate, $toDate, $cusCategories, $locationList[0]);

            foreach ($getDirectCreditNote as $key => $value) {

                $documentNo = $value['creditNoteCode'];
                $documentValue = (floatval($value['creditNoteTotal'])-floatval($value['creditNoteTotalTax']));
                $date = $value['creditNoteDate'];

                $grossProfitData[++$count] = [
                    // 'locationID' => $value['locationID'],
                    // 'date' => $date,
                    'documentNo' => $documentNo,
                    'documentType' => "Credit Note",
                    'documentValue' => $documentValue,
                    'documentCost' => 0,
                ];
            }
         
            //get credit note from DLN
            $dreditNoteFrmDLN = $this->getModel("CreditNoteTable")->getCreditNoteFromDLNForSalesDashboard($fromDate, $toDate, $cusCategories, $locationList[0]);

            foreach ($dreditNoteFrmDLN as $key => $value) {
                $deliveryNoteID = $value['salesInvoiceProductDocumentID'];
                $totalCostOfCreditNote = $this->getModel("ItemOutTable")->getCostForDelivaryNote($deliveryNoteID)['totalCostOfCreditNote'];
                $documentNo = $value['creditNoteCode'];
                $documentValue = (floatval($value['creditNoteTotal'])-floatval($value['creditNoteTotalTax']));
                $date = $value['creditNoteDate'];

                $grossProfitData[++$count] = [
                    // 'locationID' => $value['locationID'],
                    // 'date' => $date,
                    'documentNo' => $documentNo,
                    'documentType' => "Credit Note",
                    'documentValue' => $documentValue,
                    'documentCost' => $totalCostOfCreditNote,
                ];
            }

            // // $count = count($grossProfitData);
            // if ($grossInvoiceCreditData['jobCard']) {
            //     foreach ($grossInvoiceCreditData['jobCard'] as $subData) {
            //         $documentCost = 0;
            //         $totalTax = 0;
            //         foreach ($subData as $key => $value) {

            //             // $locationID = $value['locationID'];
            //             // $date = $value['date'];
            //             $documentValue = $value['documentValue'];
            //             $documentCost += $value['documentCost'];
            //             $documentType = $value['documentType'];
            //             $documentNo = $value['documentNo'];
            //             $totalTax += $value['totalTax'] * $value['qty'];
            //         }
            //         $grossProfitData[++$count] = [
            //             // 'locationID' => $locationID,
            //             // 'date' => $date,
            //             'documentValue' => $documentValue,
            //             'documentCost' => $documentCost,
            //             'documentType' => $documentType,
            //             'documentNo' => $documentNo,
            //             'totalTax' => $totalTax,
            //         ];
            //     }
            // }

            // if ($grossInvoiceCreditData['delivery']) {
            //     foreach ($grossInvoiceCreditData['delivery'] as $subData) {

            //         $documentCost = 0;
            //         $totalTax = 0;
            //         $documentValue = 0;
            //         foreach ($subData as $key => $value) {
            //             // $locationID = $value['locationID'];
            //             // $date = $value['date'];
            //             $documentValue += $value['documentValue'];
            //             $documentCost += $value['documentCost'];
            //             $documentType = $value['documentType'];
            //             $documentNo = $value['documentNo'];
            //             $totalTax += $value['totalTax'] * $value['qty'];
            //         }
            //         $grossProfitData[++$count] = [
            //             // 'locationID' => $locationID,
            //             // 'date' => $date,
            //             'documentValue' => $documentValue,
            //             'documentCost' => $documentCost,
            //             'documentType' => $documentType,
            //             'documentNo' => $documentNo,
            //             'totalTax' => $totalTax,
            //         ];
            //     }
            // }

            // if ($grossInvoiceCreditData['nonInventory']) {
            //     foreach ($grossInvoiceCreditData['nonInventory'] as $subData) {

            //         $documentCost = 0;
            //         $totalTax = 0;
            //         $documentValue = 0;
            //         foreach ($subData as $key => $value) {
            //             // $locationID = $value['locationID'];
            //             // $date = $value['date'];
            //             $documentValue += $value['documentValue'];
            //             $documentCost += $value['documentCost'];
            //             $documentType = $value['documentType'];
            //             $documentNo = $value['documentNo'];
            //             $totalTax += 0;
            //             // $cusName = $value['cusName'];
            //         }
            //         $grossProfitData[++$count] = [
            //             // 'locationID' => $locationID,
            //             // 'date' => $date,
            //             'documentValue' => $documentValue,
            //             'documentCost' => $documentCost,
            //             'documentType' => $documentType,
            //             'documentNo' => $documentNo,
            //             'totalTax' => $totalTax
            //         ];
            //     }
            // }

            // if ($grossInvoiceCreditData['directCreditNote']) {
            //     foreach ($grossInvoiceCreditData['directCreditNote'] as $subData) {

            //         $documentCost = 0;
            //         $totalTax = 0;
            //         $documentValue = 0;
            //         foreach ($subData as $key => $value) {
            //             // $locationID = $value['locationID'];
            //             // $date = $value['date'];
            //             $documentValue += $value['documentValue'];
            //             $documentCost += $value['documentCost'];
            //             $documentType = $value['documentType'];
            //             $documentNo = $value['documentNo'];
            //             $totalTax += 0;
            //         }
            //         $grossProfitData[++$count] = [
            //             // 'locationID' => $locationID,
            //             // 'date' => $date,
            //             'documentValue' => $documentValue,
            //             'documentCost' => $documentCost,
            //             'documentType' => $documentType,
            //             'documentNo' => $documentNo,
            //             'totalTax' => $totalTax,

            //         ];
            //     }
            // }

            // if ($grossInvoiceCreditData['creditNoteFromDLN']) {
            //     foreach ($grossInvoiceCreditData['creditNoteFromDLN'] as $subData) {

            //         $documentCost = 0;
            //         $totalTax = 0;
            //         $documentValue = 0;
            //         foreach ($subData as $key => $value) {
            //             // $locationID = $value['locationID'];
            //             // $date = $value['date'];
            //             $documentValue += $value['documentValue'];
            //             $documentCost += $value['documentCost'];
            //             $documentType = $value['documentType'];
            //             $documentNo = $value['documentNo'];
            //             $totalTax += 0;
            //         }
            //         $grossProfitData[++$count] = [
            //             // 'locationID' => $locationID,
            //             // 'date' => $date,
            //             'documentValue' => $documentValue,
            //             'documentCost' => $documentCost,
            //             'documentType' => $documentType,
            //             'documentNo' => $documentNo,
            //             'totalTax' => $totalTax,

            //         ];
            //     }
            // }


            //sorting data on $grossProfitData array according to date element
            // $sortData = [];
            // foreach ($grossProfitData as $key => $r) {
            //     $sortData[$key] = ($r['date']);
            // }
            // //sort $grossProfitData array according to sorted $sortData array on 'date' element
            // array_multisort($sortData, SORT_ASC, $grossProfitData);

            $arr = [];



            foreach ($grossProfitData as $element) {

                $documentValue = (isset($arr[$element['documentNo']])) ? $arr[$element['documentNo']]['documentValue'] + $element['documentValue'] : $element['documentValue'];

                $documentCost = (isset($arr[$element['documentNo']])) ? $arr[$element['documentNo']]['documentCost'] + $element['documentCost'] : $element['documentCost'];

                $arr[$element['documentNo']] = [
                    // 'locationID' => $element['locationID'],
                    // 'date' => $element['date'],
                    'documentValue' => $documentValue,
                    'documentCost' => $documentCost,
                    'documentType' => $element['documentType'],
                    'documentNo' => $element['documentNo'],
                    'totalTax' => $element['totalTax'],
                ];
            }
            // var_dump($arr).die();
            return $arr;

            // var_dump($arr).die();

            // $locWiseGrossProfitData = array_fill_keys($locationList, '');
            // foreach ($arr as $key => $v) {
            //     if (array_key_exists($v['locationID'], $locWiseGrossProfitData)) {
            //         $locWiseGrossProfitData[$v['locationID']]['locGPD'][$key] = $v;
            //         // $locWiseGrossProfitData[$v['locationID']]['locationName'] = $v['locationName'];
            //         // $locWiseGrossProfitData[$v['locationID']]['locationCode'] = $v['locationCode'];
            //     }
            // }

            // return array_filter($locWiseGrossProfitData);
        }
    }

}
