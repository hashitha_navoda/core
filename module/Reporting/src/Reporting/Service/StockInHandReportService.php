<?php

namespace Reporting\Service;

use Core\Service\ReportService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;

class StockInHandReportService extends ReportService
{

    public function getGlobalItemMovingData($proList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null, $documentDateFlag = false)
    {

        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $documentDateFlag = filter_var($documentDateFlag, FILTER_VALIDATE_BOOLEAN);
        //plus one day to $toDate
        //Because of mysql between function (I want to get data on $toDate)
        $plusToDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');

        if ($documentDateFlag) {
            $itemInDataOfSalesReturn = $this->getModel('ItemInTable')->getRangeItemInDetailsOfSalesReturnWithDocumentDate($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemInDataOfCreditNoteData = $this->getModel('ItemInTable')->getRangeItemInDetailsOfCreditNoteWithDocumentDate($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemInDataOfGrnData = $this->getModel('ItemInTable')->getRangeItemInDetailsOfGrnWithDocumentDate($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemInDataOfPostiveAdjData = $this->getModel('ItemInTable')->getRangeItemInDetailsOfPostiveAdjustmentWithDocumentDate($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemInDataOfPurchaseInvoiceData = $this->getModel('ItemInTable')->getRangeItemInDetailsOfPurchaseInvoiceWithDocumentDate($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemInDataOfExpensePaymentVoucherData = $this->getModel('ItemInTable')->getRangeItemInDetailsOfExpensePurchaseVoucherWithDocumentDate($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemInDataOfTransferData = $this->getModel('ItemInTable')->getRangeItemInDetailsOfTransferWithDocumentDate($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemInDataOfInvoiceData = $this->getModel('ItemInTable')->getRangeItemInDetailsOfSalesInvoiceWithDocumentDate($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);

            $itemInData = array_merge_recursive($itemInDataOfSalesReturn, $itemInDataOfCreditNoteData, $itemInDataOfGrnData, $itemInDataOfPostiveAdjData, $itemInDataOfPurchaseInvoiceData, $itemInDataOfExpensePaymentVoucherData, $itemInDataOfTransferData,$itemInDataOfInvoiceData);


            $itemOutDataOfSaleInvoice = $this->getModel('ItemOutTable')->getRangeItemOutSaleInvoiceDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemOutDataDLN = $this->getModel('ItemOutTable')->getRangeItemOutDeliveryNoteDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemOutDataGRN = $this->getModel('ItemOutTable')->getRangeItemOutGRNDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemOutDataPICancel = $this->getModel('ItemOutTable')->getRangeItemOutPurchaseInvoiceCancelDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemOutDataEXPVCancel = $this->getModel('ItemOutTable')->getRangeItemOutEXPVCancelDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemOutDataGoodIssue = $this->getModel('ItemOutTable')->getRangeItemOutGoodIssueDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemOutDataPurchaseReturn = $this->getModel('ItemOutTable')->getRangeItemOutPurchaseReturnDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemOutDataDebitNote = $this->getModel('ItemOutTable')->getRangeItemOutDebitNoteDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemOutDataActivity = $this->getModel('ItemOutTable')->getRangeItemOutActivityDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);

            $itemOutData = array_merge_recursive($itemOutDataOfSaleInvoice, $itemOutDataDLN, $itemOutDataGoodIssue, $itemOutDataPurchaseReturn, $itemOutDataDebitNote, $itemOutDataActivity,$itemOutDataGRN, $itemOutDataPICancel, $itemOutDataEXPVCancel);
        } else {
            $itemInData = $this->getModel('ItemInTable')->getRangeItemInDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $itemOutData = $this->getModel('ItemOutTable')->getRangeItemOutDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
        }

        $itemData = array_merge_recursive($itemInData, $itemOutData);
            
        //get transfer data that not in given location
        if($locationIds != ''){
            $transferData = $this->getModel('ItemInTable')->getTransferDetailsForMovements($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds, $documentDateFlag);
            $transData = [];
            foreach ($transferData as $key => $value) {
                if(!in_array($value['locationIDIn'], $locationIds) && in_array($value['locationIDOut'], $locationIds)){
                    $transData[] = $value;
                }
            }
            $itemData = array_merge($itemData, $transData);
        }

        $itemInOutData = [];
        $selectedProductIds = [];

        foreach ($itemData as $r) {
            $selectedProductIds[] = $r['productID'];
        }

        // $proList = $selectedProductIds;
        $proList = array_unique($selectedProductIds);
        if (count($proList) > 0) {
            //get all item in details
            $itemInDetails = $this->getModel('ItemInTable')->getAllItemInDetails();
            $itemInDataSet = [];
            foreach ($itemInDetails as $key => $value) {
                $itemInDataSet[$value['itemInID']] = $value;
            }

            // get all product uoms and set to array
            $allProductUoms = $this->getModel('ProductUomTable')->getProductUomListByProductIds();

            //create new element called itemDate
            //becasue I want to sort $itemData array according to Date value
            foreach ($itemData as $key => $v) {
                $unitPrice = ($v['itemInPrice']) ? $v['itemInPrice'] : 0;
                if ($v['itemOutItemInID']) {

                    // $itemInDetails = $this->getModel('Inventory\Model\ItemInTable')->getItemInDetails($v['itemOutItemInID']);
                    $itemInDetails = $itemInDataSet[$v['itemOutItemInID']];
                    $unitPrice = $itemInDetails['itemInPrice'];
                }

                $inOrOutDocumentType = $v['itemInDocumentType'] ? $v['itemInDocumentType'] : $v['itemOutDocumentType'];
                $inOutDocumentId = ($v['itemInDocumentID']) ? $v['itemInDocumentID'] : $v['itemOutDocumentID'];
                $inOrOutItemDetails = $this->getInOrOutItemCodex($inOrOutDocumentType, $inOutDocumentId);

                $deleteFlag = FALSE;
                if (isset($inOrOutItemDetails)) {
                    //get quantity and unit price according to dispaly uom
                    // $productUom = $this->getModel('Inventory\Model\ProductUomTable')->getProductUomListByProductID( $v['productID']);
                    $productUom = $allProductUoms[$v['productID']];

                    $thisTotalInQty = $this->getProductQuantityViaDisplayUom($v['totalInQty'], $productUom);
                    $totalInQty = ($thisTotalInQty['quantity'] == 0) ? 0 : $thisTotalInQty['quantity'];
                    $thisTotalOutQty = $this->getProductQuantityViaDisplayUom($v['totalOutQty'], $productUom);
                    $totalOutQty = ($thisTotalOutQty['quantity'] == 0) ? 0 : $thisTotalOutQty['quantity'];

                    if ($documentDateFlag) {
                        $date = $v['documentDate'] ? $v['documentDate'] : $v['documentOutDate'];
                    } else {
                        $date = $v['itemInDateAndTime'] ? $v['itemInDateAndTime'] : $v['itemOutDateAndTime'];
                    }

                    $itemInQty = $totalInQty;
                    $itemOutQty = $totalOutQty;

                    if ($v['goodsIssueStatus'] && ($inOrOutDocumentType == 'Delete Positive Adjustment')) {
                        $deleteFlag = ($v['goodsIssueStatus'] == 5) ? 'positive' : FALSE;
                    } else if ($v['goodsIssueStatus'] && ($inOrOutDocumentType == 'Delete Negative Adjustment')) {
                        $deleteFlag = ($v['goodsIssueStatus'] == 5) ? 'negative' : FALSE;
                    }
                    //If document type is transfer then get Location In Name and Out Name
                    $toLocation = NULL;
                    $fromLocation = NULL;
                    $isTransfer = FALSE;
                    if ($inOrOutDocumentType == 'Inventory Transfer') {
                        $fromLocation = $inOrOutItemDetails['locationOut'];
                        $toLocation = $inOrOutItemDetails['locationIn'];
                        $isTransfer = TRUE;
                    }

                    if ($inOrOutDocumentType == 'Payment Voucher') {
                        $inOrOutDocumentType = "Purchase Invoice";
                    }
                    if($inOrOutDocumentType == 'Inventory Transfer' && $locationIds != '' ){

                        if(in_array($inOrOutItemDetails['locationInID'], $locationIds)){

                            $itemInOutData[$key.'_1'] = array(
                                'pID' => $v['productID'],
                                'pName' => $v['productName'],
                                'pCD' => $v['productCode'],
                                'itemDate' => ($documentDateFlag) ? $this->getUserDateTime($date, 'Y-m-d') :$this->getUserDateTime($date),
                                'inOrOutItemCode' => $inOrOutItemDetails['itemCode'],
                                'itemInQty' => $itemInQty,
                                'itemOutQty' => $itemOutQty,
                                'itemInOrOutDocumentType' => $inOrOutDocumentType,
                                'unitPrice' => $unitPrice,
                                'fromLocation' => null,
                                'toLocation' => $toLocation,
                                'actLocation' => '(To) '.$toLocation,
                                'isTransfer' => false,
                                'deleteFlag' => $deleteFlag,
                                'uomAbbr' => $thisTotalOutQty['uomAbbr']
                            );
                        }
                         if(in_array($inOrOutItemDetails['locationOutID'], $locationIds)){

                            $itemInOutData[$key.'_2'] = array(
                                'pID' => $v['productID'],
                                'pName' => $v['productName'],
                                'pCD' => $v['productCode'],
                                'itemDate' => ($documentDateFlag) ? $this->getUserDateTime($date, 'Y-m-d') :$this->getUserDateTime($date),
                                'inOrOutItemCode' => $inOrOutItemDetails['itemCode'],
                                'itemInQty' => 0,
                                'itemOutQty' => $itemInQty,
                                'itemInOrOutDocumentType' => $inOrOutDocumentType,
                                'unitPrice' => $unitPrice,
                                'fromLocation' => $fromLocation,
                                'toLocation' => null,
                                'actLocation' => '(From) '.$fromLocation,
                                'isTransfer' => false,
                                'deleteFlag' => $deleteFlag,
                                'uomAbbr' => $thisTotalOutQty['uomAbbr']
                            );
                         }
                    } else {

                        $itemInOutData[$key] = array(
                            'pID' => $v['productID'],
                            'pName' => $v['productName'],
                            'pCD' => $v['productCode'],
                            'itemDate' => ($documentDateFlag) ? $this->getUserDateTime($date, 'Y-m-d') :$this->getUserDateTime($date),
                            'inOrOutItemCode' => $inOrOutItemDetails['itemCode'],
                            'itemInQty' => $itemInQty,
                            'itemOutQty' => $itemOutQty,
                            'itemInOrOutDocumentType' => $inOrOutDocumentType,
                            'unitPrice' => $unitPrice,
                            'fromLocation' => $fromLocation,
                            'toLocation' => $toLocation,
                            'actLocation' => $inOrOutItemDetails['locationName'],
                            'isTransfer' => $isTransfer,
                            'deleteFlag' => $deleteFlag,
                            'uomAbbr' => $thisTotalOutQty['uomAbbr']
                        );
                    }

                }
            }


            //sorting data on $itemData array according to itemDate element
            $sortData = Array();
            foreach ($itemInOutData as $key => $r) {
                $sortData[$key] = strtotime($r['itemDate']);
            }
            array_multisort($sortData, SORT_ASC, $itemInOutData);

            //array rearrange as product wise
            $productWiseData = array_fill_keys($proList, '');
            $i = 0;
            foreach (array_filter($itemInOutData) as $v) {
                $productWiseData[$v['pID']]['pD'][$i] = $v;
                $productWiseData[$v['pID']]['pName'] = $v['pName'];
                $productWiseData[$v['pID']]['pCD'] = $v['pCD'];
                $productWiseData[$v['pID']]['uomAbbr'] = $v['uomAbbr'];
                $productWiseData[$v['pID']]['unitPrice'] = $v['unitPrice'];
                $i++;
            }

            if ($documentDateFlag) {
                 // get item in details for open stock
                $itemInDataOfSalesReturnForBackDate = $this->getModel('ItemInTable')->getRangeItemInDetailsOfSalesReturnWithDocumentDate($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemInDataOfCreditNoteDataForBackDate = $this->getModel('ItemInTable')->getRangeItemInDetailsOfCreditNoteWithDocumentDate($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemInDataOfGrnDataForBackDate = $this->getModel('ItemInTable')->getRangeItemInDetailsOfGrnWithDocumentDate($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemInDataOfPostiveAdjDataForBackDate = $this->getModel('ItemInTable')->getRangeItemInDetailsOfPostiveAdjustmentWithDocumentDate($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemInDataOfPurchaseInvoiceDataForBackDate = $this->getModel('ItemInTable')->getRangeItemInDetailsOfPurchaseInvoiceWithDocumentDate($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemInDataOfExpensePaymentVoucherDataForBackDate = $this->getModel('ItemInTable')->getRangeItemInDetailsOfExpensePurchaseVoucherWithDocumentDate($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemInDataOfInvoiceDataForBackDate = $this->getModel('ItemInTable')->getRangeItemInDetailsOfSalesInvoiceWithDocumentDate($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemIntransferStockForOpenStock = $this->getModel('ItemInTable')->getRangeItemInDetailsOfTransferWithDocumentDate($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);


                $itemInStock = array_merge_recursive($itemInDataOfSalesReturnForBackDate, $itemInDataOfCreditNoteDataForBackDate, $itemInDataOfGrnDataForBackDate, $itemInDataOfPostiveAdjDataForBackDate, $itemInDataOfPurchaseInvoiceDataForBackDate, $itemInDataOfExpensePaymentVoucherDataForBackDate, $itemInDataOfInvoiceDataForBackDate, $itemIntransferStockForOpenStock);


                // get item out details for open stock
                $itemOutDataOfSaleInvoiceForBackDate = $this->getModel('ItemOutTable')->getRangeItemOutSaleInvoiceDetails($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemOutDataDLNForBackDate = $this->getModel('ItemOutTable')->getRangeItemOutDeliveryNoteDetails($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemOutDataGRNForBackDate = $this->getModel('ItemOutTable')->getRangeItemOutGRNDetails($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemOutDataPICancelForBackDate = $this->getModel('ItemOutTable')->getRangeItemOutPurchaseInvoiceCancelDetails($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemOutDataEXPVCancelForBackDate = $this->getModel('ItemOutTable')->getRangeItemOutEXPVCancelDetails($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemOutDataGoodIssueForBackDate = $this->getModel('ItemOutTable')->getRangeItemOutGoodIssueDetails($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemOutDataPurchaseReturnForBackDate = $this->getModel('ItemOutTable')->getRangeItemOutPurchaseReturnDetails($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemOutDataDebitNoteForBackDate = $this->getModel('ItemOutTable')->getRangeItemOutDebitNoteDetails($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds);
                $itemOutDataActivityForBackDate = $this->getModel('ItemOutTable')->getRangeItemOutActivityDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);


                $itemOutStock = array_merge_recursive($itemOutDataOfSaleInvoiceForBackDate, $itemOutDataDLNForBackDate, $itemOutDataGRNForBackDate, $itemOutDataPICancelForBackDate, $itemOutDataEXPVCancelForBackDate, $itemOutDataGoodIssueForBackDate, $itemOutDataPurchaseReturnForBackDate, $itemOutDataDebitNoteForBackDate, $itemOutDataActivityForBackDate);

                //get transfer data that not in given location
                if($locationIds != ''){
                    $transferData = $this->getModel('ItemInTable')->getTransferDetailsForMovements($proList, $fromDate, null, $isAllProducts, $isNonInventoryProducts, $locationIds, $documentDateFlag);
                    $transDataForOpenStock = [];
                    foreach ($transferData as $key => $value) {
                        if(!in_array($value['locationIDIn'], $locationIds) && in_array($value['locationIDOut'], $locationIds)){
                            $value['totalOutQty'] = $value['totalInQty'];
                            $transDataForOpenStock[] = $value;
                        }
                        if(in_array($value['locationIDIn'], $locationIds) && in_array($value['locationIDOut'], $locationIds)){
                            $value['totalOutQty'] = $value['totalInQty'];
                            $transDataForOpenStock[] = $value;
                        }
                    }
                    $itemOutStock = array_merge($itemOutStock, $transDataForOpenStock);
                }


            } else {
                $itemInStock = $this->getModel('ItemInTable')->getStockInRange($proList, $fromDate, $locationIds);
                $itemOutStock = $this->getModel('ItemOutTable')->getStockOutRange($proList, $fromDate, $locationIds);
                $itemIntransferStock = $this->getModel('ItemInTable')->getTransferStockInRange($proList, $fromDate, $locationIds);

            }
            
            $itemInArr = [];
            $transferQty = $transferValue = 0;
            
            //puts item wise open stock into $productWiseData Array
            foreach ($itemInStock as $value) {

                //get quantity and unit price according to dispaly uom
                $productUom = $this->getModel('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);
                $productUom =$allProductUoms[$value['productID']];
                $thisQty = ($documentDateFlag) ? $this->getProductQuantityViaDisplayUom($value['totalInQty'], $productUom) : $this->getProductQuantityViaDisplayUom($value['availableInQty'], $productUom);
                $thisUnitPrice = $this->getProductUnitPriceViaDisplayUom($value['itemInPrice'], $productUom);

                $availableQty = ($thisQty['quantity'] == 0) ? 0 : $thisQty['quantity'];
                $thisUnitPrice = ($thisUnitPrice == 0) ? 0 : $thisUnitPrice;
                $transferQty = ($value['itemInDocumentType'] == 'Inventory Transfer') ? $availableQty : 0;
                $transferValue = ($value['itemInDocumentType'] == 'Inventory Transfer') ? ($availableQty * $thisUnitPrice) : 0;

                if (!array_key_exists($value['productID'], $itemInArr)) {
                    $itemInArr[$value['productID']] = [
                        'itemInStock' => $availableQty,
                        'itemInStockValue' => $availableQty * $thisUnitPrice,
                        'transferInStock' => $transferQty,
                        'transferInStockValue' => $transferValue,
                    ];
                } else {
                    $itemInArr[$value['productID']]['itemInStock'] = $itemInArr[$value['productID']]['itemInStock'] + $availableQty;
                    $itemInArr[$value['productID']]['itemInStockValue'] = $itemInArr[$value['productID']]['itemInStockValue'] + ($availableQty * $thisUnitPrice);
                    $itemInArr[$value['productID']]['transferInStock'] = $itemInArr[$value['productID']]['transferInStock'] + $transferQty;
                    $itemInArr[$value['productID']]['transferInStockValue'] = $itemInArr[$value['productID']]['transferInStockValue'] + $transferValue;
                }

                $productWiseData[$value['productID']]['itemInStock'] = $itemInArr[$value['productID']]['itemInStock'];
                $productWiseData[$value['productID']]['itemInStockValue'] = $itemInArr[$value['productID']]['itemInStockValue'];
                $productWiseData[$value['productID']]['transferInStock'] = $itemInArr[$value['productID']]['transferInStock'];
                $productWiseData[$value['productID']]['transferInStockValue'] = $itemInArr[$value['productID']]['transferInStockValue'];
            }

            //get itemOut table stock item wise
            // $itemOutStock = $this->getModel('ItemOutTable')->getStockOutRange($proList, $fromDate, $locationIds);
            $itemOutArr = [];
            //puts item wise open stock into $productWiseData Array
            foreach ($itemOutStock as $value) {
                //get quantity and unit price according to dispaly uom
                // $productUom = $this->getModel('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);
                $productUom =$allProductUoms[$value['productID']];
                $thisQty = ($documentDateFlag) ? $this->getProductQuantityViaDisplayUom($value['totalOutQty'], $productUom) : $this->getProductQuantityViaDisplayUom($value['availableOutQty'], $productUom);
                $thisUnitPrice = $this->getProductUnitPriceViaDisplayUom($value['itemInPrice'], $productUom);

                $availableQty = ($thisQty['quantity'] == 0) ? 0 : $thisQty['quantity'];
                $thisUnitPrice = ($thisUnitPrice == 0) ? 0.00 : $thisUnitPrice;

                if (!array_key_exists($value['productID'], $itemOutArr)) {
                    $itemOutArr[$value['productID']] = [
                        'itemOutStock' => $availableQty,
                        'itemOutStockValue' => $availableQty * $thisUnitPrice
                    ];
                } else {
                    $itemOutArr[$value['productID']]['itemOutStock'] = $itemOutArr[$value['productID']]['itemOutStock'] + $availableQty;
                    $itemOutArr[$value['productID']]['itemOutStockValue'] = $itemOutArr[$value['productID']]['itemOutStockValue'] + ($availableQty * $thisUnitPrice);
                }

                $productWiseData[$value['productID']]['itemOutStock'] = $itemOutArr[$value['productID']]['itemOutStock'];
                $productWiseData[$value['productID']]['itemOutStockValue'] = $itemOutArr[$value['productID']]['itemOutStockValue'];
            }

            // $itemIntransferStock = $this->getModel('ItemInTable')->getTransferStockInRange($proList, $fromDate, $locationIds);
            foreach ($itemIntransferStock as $key => $value) {

                $productUom =$allProductUoms[$value['productID']];
                $thisQty = ($documentDateFlag) ? $this->getProductQuantityViaDisplayUom($value['totalInQty'], $productUom) : $this->getProductQuantityViaDisplayUom($value['transferedQuantity'], $productUom);
                $thisUnitPrice = $this->getProductUnitPriceViaDisplayUom($value['itemInPrice'], $productUom);

                $availableQty = ($thisQty['quantity'] == 0) ? 0 : $thisQty['quantity'];
                $thisUnitPrice = ($thisUnitPrice == 0) ? 0.00 : $thisUnitPrice;

                if (!array_key_exists($value['productID'], $itemOutArr)) {
                    $itemOutArr[$value['productID']] = [
                        'itemOutStock' => $availableQty,
                        'itemOutStockValue' => $availableQty * $thisUnitPrice
                    ];
                } else {
                    $itemOutArr[$value['productID']]['itemOutStock'] = $itemOutArr[$value['productID']]['itemOutStock'] + $availableQty;
                    $itemOutArr[$value['productID']]['itemOutStockValue'] = $itemOutArr[$value['productID']]['itemOutStockValue'] + ($availableQty * $thisUnitPrice);
                }

                $productWiseData[$value['productID']]['itemOutStock'] = $itemOutArr[$value['productID']]['itemOutStock'];
                $productWiseData[$value['productID']]['itemOutStockValue'] = $itemOutArr[$value['productID']]['itemOutStockValue'];


            }

            return $productWiseData;
        }
    }


    public function globalItemMovingPdf($postData)
    {
        try {

            $proList = $postData['productIds'];
            $isAllProducts = $postData['isAllProducts'];
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $locationIds = $postData['locationIds'];
            $documentDateFlag = $postData['documentDateFlag'];
            $globalItemMovingData = $this->getGlobalItemMovingData($proList, $fromDate, $toDate, $isAllProducts, null, $locationIds, $documentDateFlag);
            if ($documentDateFlag == "true") {
                $name = 'Item Moving Report With Document Date';
            } else {
                $name = 'Item Moving Report';
            }

            $headerView = $this->headerViewTemplate($name, $period = $fromDate . ' - ' . $toDate);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $globalItemMovingView = new ViewModel(array(
                'globalItemMovingData' => $globalItemMovingData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $globalItemMovingView->setTemplate('reporting/stock-in-hand-report/generate-global-item-moving-pdf');

            $globalItemMovingView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($globalItemMovingView);
            $pdfPath = $this->downloadPDF('global-item-moving-report', $htmlContent, true);

            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing pdf data.');
        }
    }


     public function globalItemMovingSheet($postData)
    {
        try {
            $proList = $postData['productIds'];
            $isAllProducts = $postData['isAllProducts'];
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $locationIds = $postData['locationIds'];
            $documentDateFlag = $postData['documentDateFlag'];
            $globalItemMovingData = $this->getGlobalItemMovingData($proList, $fromDate, $toDate, $isAllProducts, null, $locationIds, $documentDateFlag);
            $cD = $this->getCompanyDetails();

            if ($documentDateFlag == "true") {
                $reportName = 'Global Item moving report with document date';
            } else {
                $reportName = 'Global Item moving report';
            }

            if ($globalItemMovingData) {
                $title = '';
                $tit = 'ITEM MOVING REPORT';
                $in = ["", $tit, "", ""];
                $title.=implode(",", $in) . "\n";

                $in = ["Period:", $fromDate . '-' . $toDate, "", ""];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = array("NO", "PRODUCT NAME - PRODUCT CODE", "DATE/TIME", "TRANSACTION TYPE", "ACTION", "TRANSACTION CODE", "LOCATION(FROM - TO)", "QUANTITY", "IN", "OUT", "STOCK IN HAND");
                $header = implode(",", $arrs);
                $arr = '';
                $count = 0;
                foreach ($globalItemMovingData as $key => $d) {
                    if ($d['pD']) {
                        $i = 1;
                        $totalQty = 0;
                        $itemInStock = isset($d['itemInStock']) ? $d['itemInStock'] : 0;
                        $itemOutStock = isset($d['itemOutStock']) ? $d['itemOutStock'] : 0;
                        $negAdjstStock = isset($d['negAdjstStock']) ? $d['negAdjstStock'] : 0;
                        $posiAdjstStock = isset($d['posiAdjstStock']) ? $d['posiAdjstStock'] : 0;
                        $openStock = ($itemInStock + $negAdjstStock ) - ($itemOutStock + $posiAdjstStock);
                        $in = ['', $d['pName'] . ' - ' . $d['pCD'], 'Open Stock', '', '', '', '', '', '', '', $openStock . $d['uomAbbr']];
                        $arr.= implode(",", $in) . "\n";
                        $totalQty += $openStock;

                        foreach ($d['pD'] as $value) {
                            $qty = $value['itemInQty'] ? $value['itemInQty'] : $value['itemOutQty'];
                            $itemInQty = $value['itemInQty'];
                            $itemOutQty = $value['itemOutQty'];
                            $fromLocation = $value['fromLocation'] ? $value['fromLocation'] : '-';
                            $toLocation = $value['toLocation'] ? $value['toLocation'] : '-';
                            $actLocation = isset($value['actLocation']) ? $value['actLocation'] : '-';
                            $isTransfer = ($value['isTransfer'] == TRUE) ? TRUE : FALSE;
                            $action = '';
                            if ($itemInQty > 0 && $isTransfer == FALSE) {
                                if ($value['deleteFlag'] == 'negative') {
                                    $action = 'deleted';
                                } else {
                                    $action = '-';
                                }
                                $totalQty += $qty;
                            } else if ($itemOutQty > 0 && $isTransfer == FALSE) {
                                if ($value['deleteFlag'] == 'positive') {
                                    $action = 'deleted';
                                } else {
                                    $action = '-';
                                }
                                $totalQty -= $qty;
                            }
                            if ($isTransfer == TRUE) {
                                $in = [ $i, '', $value['itemDate'], $value['itemInOrOutDocumentType'],
                                    $action, $value['inOrOutItemCode'], $fromLocation, $qty . $d['uomAbbr'],
                                    '-', $itemOutQty . $d['uomAbbr'], $totalQty . $d['uomAbbr']];
                                $arr.= implode(",", $in) . "\n";

                                $in = [ $i, '', '', '', '', '', $toLocation, $qty . $d['uomAbbr'], $itemInQty . $d['uomAbbr'], '-', $totalQty . $d['uomAbbr']];
                                $arr.= implode(",", $in) . "\n";
                            } else {
                                $in = [$i, '', $value['itemDate'], $value['itemInOrOutDocumentType'],
                                    $action, $value['inOrOutItemCode'], $actLocation, $qty . $d['uomAbbr'],
                                    $itemInQty . $d['uomAbbr'], $itemOutQty . $d['uomAbbr'], $totalQty . $d['uomAbbr'],
                                ];
                                $arr.= implode(",", $in) . "\n";
                            }
                            $i++;
                            $count++;
                        }
                    }
                }
                if ($count == 0) {
                    $arr = "no matching records found\n";
                }
            }
            $name = "item-moving-report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $reportName], 'csv report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    public function getInOrOutItemCodex($inOrOutItemType, $documentId)
    {
        switch ($inOrOutItemType) {
            case 'Goods Received Note':
                $documentData = $this->getModel('GrnTable')->getGrnForItemMovingByGrnID($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['grnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Goods Received Note Cancel':
                $documentData = $this->getModel('GrnTable')->getGrnForItemMovingByGrnID($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['grnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Inventory Transfer':
                $documentData = $this->getModel('TransferTable')->getTransfersById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['transferCode'];
                $inOrOutItemTypeDetails['locationIn']   = $documentData['locationInName'];
                $inOrOutItemTypeDetails['locationOut']  = $documentData['locationOutName'];
                $inOrOutItemTypeDetails['locationInID']  = $documentData['locationInID'];
                $inOrOutItemTypeDetails['locationOutID']  = $documentData['locationOutID'];
                break;

            case 'Inventory Positive Adjustment':
            case 'Inventory Negative Adjustment':
            case 'Delete Positive Adjustment':
            case 'Delete Negative Adjustment':
                $documentData = $this->getModel('GoodsIssueTable')->getAdjutmentByAdjutmentId($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['goodsIssueCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Payment Voucher':
            case 'purchase Invoice cancel':
                $documentData = $this->getModel('PurchaseInvoiceTable')->getPIsByPIID($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['purchaseInvoiceCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Sales Returns':
                $documentData = $this->getModel('SalesReturnsTable')->getSalesReturnById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['salesReturnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;
            
            case 'Direct Return':
                $documentData = $this->getModel('SalesReturnsTable')->getSalesReturnById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['salesReturnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Credit Note':
                $documentData = $this->getModel('CreditNoteTable')->getCreditNoteById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['creditNoteCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;
            case 'Direct Credit Note':
                $documentData = $this->getModel('CreditNoteTable')->getCreditNoteById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['creditNoteCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;
            case 'Buy Back Credit Note':
                $documentData = $this->getModel('CreditNoteTable')->getCreditNoteById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['creditNoteCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Sales Invoice':
            case 'POS Invoice Cancel':
            case 'Invoice Cancel':
            case 'Invoice Edit':
                $documentData = $this->getModel('InvoiceTable')->getInvoiceDataByInvoiceId($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['salesInvoiceCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Delivery Note':
            case 'Delivery Note Edit':
                $documentData = $this->getModel('DeliveryNoteTable')->getDeliveryNoteByDeliveryNoteId($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['deliveryNoteCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Purchase Return':
                $documentData = $this->getModel('PurchaseReturnTable')->getPurchaseReturnById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['purchaseReturnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;
            
            case 'Direct Purchase Return':
                $documentData = $this->getModel('PurchaseReturnTable')->getPurchaseReturnDataById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['purchaseReturnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Debit Note':
                $documentData = $this->getModel('DebitNoteTable')->getDebitNoteById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['debitNoteCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Direct Debit Note':
                $documentData = $this->getModel('DebitNoteTable')->getDebitNoteById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['debitNoteCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Activity':
            case 'Delete Activity Item':
                $documentData = $this->getModel('ActivityTable')->getActivityDataById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['activityCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Expense Payment Voucher':
                $documentData = $this->getModel('PaymentVoucherTable')->getPaymentVoucherById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['paymentVoucherCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Payment Voucher Cancel':
                $documentData = $this->getModel('PaymentVoucherTable')->getPaymentVoucherById($documentId)->current();
                $inOrOutItemTypeDetails['itemCode']     = $documentData['paymentVoucherCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            default:
                $inOrOutItemTypeDetails = ['itemCode' => null, 'locationName' => null];
                break;
        }

        return $inOrOutItemTypeDetails;
    }

    /**
    * this function use to genarate data for globle stock value with GRN Costing
    * */
    public function getProductsWithGRNCosting($proIds = NULL, $locIds, $isAllProducts = false, $categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $proIds = empty($proIds) ? [] : $proIds;
        $categoryIds = empty($categoryIds) ? null : $categoryIds;
        if ((isset($proIds) || $isAllProducts ) && isset($locIds)) {
            $globalData['pD'] = array_fill_keys($locIds, '');
            $globalData = array_fill_keys($proIds, $globalData);

            //get available quantity by locationProduct table
            $productList = $this->getModel('LocationProductTable')->getProductQtyListByLocID($proIds, $locIds, $isAllProducts, $categoryIds,true);

            // get last grn value for given productID and locationIDs
            $docTypes = ['Goods Received Note','Payment Voucher'];
            $dataForCosting = $this->getModel('ItemInTable')->getLastInsertedItemValuesByDocumentTypeLocationIDAndProductID($docTypes, $locIds, $proIds, $isAllProducts);

            // set last grn price to the GRN data Array
            $grnCostArray = [];
            foreach ($dataForCosting as $key => $value) {
                if ($grnCostArray[$value['itemInLocationProductID']] == null) {
                    $grnCostArray[$value['itemInLocationProductID']]['itemPrice'] = floatval($value['itemInPrice']) - floatval($value['itemInDiscount']);
                    if ($value['itemInDocumentType'] == 'Goods Received Note') {
                        $grnCostArray[$value['itemInLocationProductID']]['docType'] = 'GRN';
                    } else {
                        $grnCostArray[$value['itemInLocationProductID']]['docType'] = 'Purchase Invoice';
                    }
                }
            }
                
            foreach ($productList as $p) {
                
                $globalData[$p['pID']]['pN'] = $p['pName'];
                $globalData[$p['pID']]['categoryName'] = $p['categoryName'];
                $globalData[$p['pID']]['pCD'] = $p['pCD'];
                $productUom = $this->getModel('ProductUomTable')->getProductUomListByProductID($p['pID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUom);
                $unitPrice = $this->getProductUnitPriceViaDisplayUom($grnCostArray[$p['locProID']]['itemPrice'], $productUom);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                $globalData[$p['pID']]['pD'][$p['locID']]['aQty'] = $quantity;
                $globalData[$p['pID']]['pD'][$p['locID']]['uomAbbr'] = $thisqty['uomAbbr'];
                $globalData[$p['pID']]['pD'][$p['locID']]['costPrice'] = $unitPrice;
                $globalData[$p['pID']]['pD'][$p['locID']]['TotalValue'] = $unitPrice*$quantity;
                $globalData[$p['pID']]['pD'][$p['locID']]['locName'] = $p['locName'];
                $globalData[$p['pID']]['pD'][$p['locID']]['docType'] = $grnCostArray[$p['locProID']]['docType'];
            }
            $proIds = array_keys($globalData);
            
            return $globalData;
        }
    }
    
    public function getProductsWithAvgCosting($proIds = NULL, $locIds, $isAllProducts = false, $categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $proIds = empty($proIds) ? [] : $proIds;
        $categoryIds = empty($categoryIds) ? null : $categoryIds;
        if ((isset($proIds) || $isAllProducts ) && isset($locIds)) {
            $globalData['pD'] = array_fill_keys($locIds, '');
            $globalData = array_fill_keys($proIds, $globalData);

            //get available quantity by locationProduct table
            $productList = $this->getModel('LocationProductTable')->getProductQtyListByLocID($proIds, $locIds, $isAllProducts, $categoryIds,true);
            foreach ($productList as $p) {
                $globalData[$p['pID']] = array(
                    'pN' => $p['pName'],
                    'categoryName' => $p['categoryName'],
                    'pCD' => $p['pCD']);
            }
            $proIds = array_keys($globalData);
            foreach ($productList as $p) {
                //get quantity and unit price according to dispaly uom
                $productUom = $this->getModel('ProductUomTable')->getProductUomListByProductID($p['pID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUom);
                $unitPrice = $this->getProductUnitPriceViaDisplayUom($p['avgCost'], $productUom);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                $globalData[$p['pID']]['pD'][$p['locID']]['aQty'] = $quantity;
                $globalData[$p['pID']]['pD'][$p['locID']]['uomAbbr'] = $thisqty['uomAbbr'];
                $globalData[$p['pID']]['pD'][$p['locID']]['costPrice'] = $unitPrice;
                $globalData[$p['pID']]['pD'][$p['locID']]['TotalValue'] = $unitPrice*$quantity;
                $globalData[$p['pID']]['pD'][$p['locID']]['locName'] = $p['locName'];
            }
            return $globalData;
        }
    }
    public function _getGlobalWiseStockValueData($proIds = NULL, $locIds, $isAllProducts = false, $categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $proIds = empty($proIds) ? [] : $proIds;
        $categoryIds = empty($categoryIds) ? null : $categoryIds;
        if ((isset($proIds) || $isAllProducts ) && isset($locIds)) {
            $globalData['pD'] = array_fill_keys($locIds, '');
            $globalData = array_fill_keys($proIds, $globalData);
            //get available quantity by locationProduct table
            $productList = $this->getModel('LocationProductTable')->getProductQtyListByLocID($proIds, $locIds, $isAllProducts, $categoryIds, true);
            foreach ($productList as $p) {
                $globalData[$p['pID']] = array(
                    'pN' => $p['pName'],
                    'categoryName' => $p['categoryName'],
                    'productDefaultSellingPrice' => $p['defaultSellingPrice'],
                    'pCD' => $p['pCD']);
            }
            $proIds = array_keys($globalData);
            //get all product uoms that related to the given product ids
            $allProductUoms = $this->getModel('ProductUomTable')->getProductUomListByProductIds($proIds);

            foreach ($productList as $p) {
                //get quantity and unit price according to dispaly uom
                // $productUom = $this->getModel('Inventory\Model\ProductUomTable')->getProductUomListByProductID($p['pID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $allProductUoms[$p['pID']]);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                $globalData[$p['pID']]['pD'][$p['locID']]['aQty'] = $quantity;
                $globalData[$p['pID']]['pD'][$p['locID']]['uomAbbr'] = $thisqty['uomAbbr'];
            }

            $itemData = $this->getModel('ItemInTable')->getProductDetails($proIds, $locIds);
            $productID = NULL;
            $totalQty = 0;
            $costValue = 0;
            $discountValue = 0;
            $count = 0;
            $locID = NULL;
            $dataFlag = false;

            foreach ($itemData as $i) {
                //get quantity and unit price according to dispaly uom

                $thisqty = $this->getProductQuantityViaDisplayUom($i['qtySubtraction'], $allProductUoms[$i['productID']]);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                $unitPrice = $this->getProductUnitPriceViaDisplayUom($i['itemInPrice'], $allProductUoms[$i['productID']]);
                $itemInDocumentType = $i['itemInDocumentType'];
                $locationProductID = $i['itemInLocationProductID'];
                $taxValue = 0;
                //if previous $productID is equal to new $i['productID']
                if ($productID == $i['productID'] && $locID == $i['locationID']) {
                    $previousStageTaxValue = $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['taxValue'];
                    $newTaxValue = $previousStageTaxValue + $taxValue;
                    $totalQty += $quantity;
                    $costValue += $quantity * $unitPrice;
                    $discountValue += $quantity * $i['itemInDiscount'];
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['totalQty'] = $totalQty;
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['costValue'] = $costValue;
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['discountValue'] = $discountValue;
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['taxValue'] = $newTaxValue;
                    $dataFlag = true;
                }
                //first product dataset put into $locWiseData array.because $flag is FALSE
                if ($dataFlag == false) {
                    $count++;
                    $totalQty = $quantity;
                    $costValue = $quantity * $unitPrice;
                    $discountValue = $quantity * $i['itemInDiscount'];
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count] = array(
                        'pID' => $i['productID'],
                        'locID' => $i['locationID'],
                        'costValue' => $costValue,
                        'totalQty' => $totalQty,
                        'discountValue' => $discountValue,
                        'taxValue' => $taxValue
                    );
                }
                //first comming productID and transferID, set to following variables
                $productID = $i['productID'];
                $locID = $i['locationID'];
                $dataFlag = false;
            }

            //calculation
            //product wise data are putting into one array,ProductID is as a key
            $gloProData = array();
            foreach ($globalData as $key => $gD) {
                if ($gD['pD']) {
                    foreach (($gD['pD']) as $locID => $data) {
                        $totalCostValue = 0;
                        $totalSubQty = 0;
                        $totalDiscount = 0;
                        $totalTax = 0;
                        if (!empty($data['itemD'])) {
                            foreach (($data['itemD']) as $d) {
                                $totalCostValue += $d['costValue'];
                                $totalSubQty +=$d['totalQty'];
                                $totalDiscount += $d['discountValue'];
                                $totalTax += $d['taxValue'];
                            }
                        }
                        if ($key && $gD['pCD'] && $gD['pN']) {
                            $gloProData[$key]['proD'][$locID] = array(
                                'availableQty' => $data['aQty'],
                                'pN' => $gD['pN'],
                                'pCD' => $gD['pCD'],
                                'categoryName' => $gD['categoryName'],
                                'productDefaultSellingPrice' => $gD['productDefaultSellingPrice'],
                                'pID' => $key,
                                'totalCostValue' => $totalCostValue,
                                'totalSubQty' => $totalSubQty,
                                'discountValue' => $totalDiscount,
                                'taxValue' => $totalTax,
                                'uomAbbr' => $data['uomAbbr'],
                            );
                        }
                    }
                }
            }

            return $gloProData;
        }
    }
    public function globalStockValuePdf($postData)
    {
        try{
            $proIds = $postData['productIds'];
            $isAllProducts = $postData['isAllProducts'];
            $categoryIds = $postData['categoryIds'];
            $locIds = $postData['locIds'];
            $grnCostingFlag = false;
            if ($postData['grnCosting'] == 'true') {
                $grnCostingFlag = true;
            }
            $avgCostingFlag = false;
            if($postData['avgCosting'] == 'true'){
                $avgCostingFlag = true;
            }
            if ($grnCostingFlag) {
                $location = $postData['locationIds'];
                $locId = $location;
                if(sizeof($location) == 1 && $location[0] == null){
                    $locId = $locIds;
                }
                $name = 'Global Stock Value With GRN Cost Report';
                $globalStockValueData = $this->getProductsWithGRNCosting($proIds, $locId, $isAllProducts, $categoryIds);

            } else if($avgCostingFlag){
                $location = $postData['locationIds'];
                $locId = $location;
                if(sizeof($location) == 1 && $location[0] == null){
                    $locId = $locIds;
                }
                $name = 'Global Stock Value With Avg Cost Report';
                $globalStockValueData = $this->getProductsWithAvgCosting($proIds, $locId, $isAllProducts, $categoryIds);
            } else {
                $name = 'Global Stock Value Report';
                $globalStockValueData = $this->_getGlobalWiseStockValueData($proIds, $locIds, $isAllProducts, $categoryIds);
            }
            $cD = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);
            $viewGlobalStockValue = new ViewModel(array(
                'gSVD' => $globalStockValueData,
                'cD' => $cD,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            if($avgCostingFlag || $grnCostingFlag){
                $viewGlobalStockValue->setTemplate('reporting/stock-in-hand-report/generate-globle-stock-with-avg-cost-value-pdf');
            } else {
                $viewGlobalStockValue->setTemplate('reporting/stock-in-hand-report/generate-global-stock-value-pdf');
            }
            $viewGlobalStockValue->setTerminal(true);
            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewGlobalStockValue);
            $pdfPath = $this->downloadPDF('global-stock-value-report', $htmlContent, true);

            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');
        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing pdf data.');
        }
    }
    
    public function globalStockValueSheet($postData)
    {
        try{
            $proIds = $postData['productIds'];
            $isAllProducts = $postData['isAllProducts'];
            $categoryIds = $postData['categoryIds'];
            $locIds = $postData['locIds'];
             $grnCostingFlag = false;
            if ($postData['grnCosting'] == 'true') {
                $grnCostingFlag = true;
            }
            $avgCostingFlag = false;
            if($postData['avgCosting'] == 'true'){
                $avgCostingFlag = true;
            }
            if ($grnCostingFlag) {
                $location = $postData['locationIds'];
                $locId = $location;
                if(sizeof($location) == 1 && $location[0] == null){
                    $locId = $locIds;
                }
                $name = 'Global Stock Value With GRN Cost Report';
                $globalStockValueData = $this->getProductsWithGRNCosting($proIds, $locId, $isAllProducts, $categoryIds);

            } else if($avgCostingFlag){
                $location = $postData['locationIds'];
                $locId = $location;
                if(sizeof($location) == 1 && $location[0] == null){
                    $locId = $locIds;
                }
                $globalStockValueData = $this->getProductsWithAvgCosting($proIds, $locId, $isAllProducts, $categoryIds);
            } else {
                $globalStockValueData = $this->_getGlobalWiseStockValueData($proIds, $locIds, $isAllProducts, $categoryIds);
            }
            $cD = $this->getCompanyDetails();
            if ($globalStockValueData && !$avgCostingFlag) {
                $title = '';
                $tit = 'GLOBAL STOCK VALUE REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                    3 => ""
                );
                $title.=implode(",", $in) . "\n";
                $in = array(0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $in) . "\n";
                $in = array(0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";
                $arrs = array("NO", "PRODUCT CODE", "PRODUCT NAME","PRODUCT CATEGORY", "AVAILABLE QUANTITY"," ", "TOTAL VALUE({$this->companyCurrencySymbol})");
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                $grandTotal = 0;
                foreach ($globalStockValueData as $key => $data) {
                    $availableQty = 0;
                    $totalCostValue = 0;
                    $discount = 0;
                    $tax = 0;
                    $availableQtyAmount = 0;
                    foreach ($data['proD'] as $key => $value) {
                        $costValue = $value['totalCostValue'] ? $value['totalCostValue'] : 0.00;
                        $aQty = $value['totalSubQty'] ? $value['totalSubQty'] : 0.00;
                        $availableQty += $aQty;
                        $uomAbbr = $value['uomAbbr'] ? $value['uomAbbr'] : '';
                        $discount = isset($value['discountValue']) ? $value['discountValue'] : 0;
                        $tax = isset($value['taxValue']) ? $value['taxValue'] : 0;
                        $totalCostValue += (($costValue + $tax) - $discount);
                    }
                    if ($availableQty != 0 && $availableQty != NULL) {
                        $uPrice = ($totalCostValue) / $availableQty;
                    }
                    $availableQtyAmount = $uPrice * $availableQty;
                    $grandTotal += $availableQtyAmount;
                    $in = array(
                        0 => $i,
                        1 => "{$value['pCD']}",
                        2 => str_replace(',',' ',$value['pN']),
                        3 => str_replace(',',' ',$value['categoryName']),
                        4 => $availableQty,
                        5 => $uomAbbr,
                        6 => $availableQtyAmount
                    );
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
                $in = array(
                    '',
                    '',
                    '',
                    '',
                    '',
                    'Grand Total Value ('.$this->companyCurrencySymbol.')',
                    number_format($grandTotal, 2, '.', '')
                );
                $arr.=implode(",", $in) . "\n";
            } else if($globalStockValueData && ($avgCostingFlag || $grnCostingFlag)) {
                $title = '';
                if ($grnCostingFlag) {
                    $tit = 'GLOBAL STOCK VALUE WITH GRN COSTING REPORT';
                } else {
                    $tit = 'GLOBAL STOCK VALUE WITH AVG COSTING REPORT';
                    
                }
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                    3 => ""
                );
                $title.=implode(",", $in) . "\n";
                $in = array(0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $in) . "\n";
                $in = array(0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";
                if ($grnCostingFlag) {
                    $arrs = array("NO", "PRODUCT CODE", "PRODUCT NAME", "PRODUCT CATEGORY", "LOCATION", "AVAILABLE QUANTITY IN LOCATION", "LAST DOC TYPE FOR COSTING","COSTING VALUE IN LOCATION({$this->companyCurrencySymbol})", "TOTAL VALUE({$this->companyCurrencySymbol})");
                } else {
                    $arrs = array("NO", "PRODUCT CODE", "PRODUCT NAME", "PRODUCT CATEGORY", "LOCATION", "AVAILABLE QUANTITY IN LOCATION", "COSTING VALUE IN LOCATION({$this->companyCurrencySymbol})", "TOTAL VALUE({$this->companyCurrencySymbol})");
                    
                }
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                $grandTotal = 0;
                foreach ($globalStockValueData as $key => $data) {
                    foreach ($data['pD'] as $key => $value) {
                        $grandTotal += $value['TotalValue'];
                        if ($grnCostingFlag) {
                            $in = array(
                                0 => $i,
                                1 => $data['pCD'],
                                2 => $data['pN'],
                                3 => $data['categoryName'],
                                4 => $value['locName'],
                                5 => $value['docType'],
                                6 => $value['aQty'].' '.$value['uomAbbr'],
                                7 => $value['costPrice'],
                                8 => $value['TotalValue'],
                            );
                        } else {
                            $in = array(
                                0 => $i,
                                1 => $data['pCD'],
                                2 => $data['pN'],
                                3 => $data['categoryName'],
                                4 => $value['locName'],
                                5 => $value['aQty'].' '.$value['uomAbbr'],
                                6 => $value['costPrice'],
                                7 => $value['TotalValue'],
                            );
                            
                        }
                        $arr.=implode(",", $in) . "\n";
                        $i++;
                    }
                }
                if ($grnCostingFlag) {
                    $in = array(
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        'Grand Total Value ('.$this->companyCurrencySymbol.')',
                        number_format($grandTotal, 2, '.', '')
                    );
                } else {
                    $in = array(
                        '',
                        '',
                        '',
                        '',
                        '',
                        '',
                        'Grand Total Value ('.$this->companyCurrencySymbol.')',
                        number_format($grandTotal, 2, '.', '')
                    );
                    
                }
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }
            $name = "global-stock-value";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);
            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }
    
    public function _getLocationWiseStockValueData($proIds = NULL, $locIds, $isAllProducts, $categoryIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        if ((isset($proIds) || $isAllProducts) && isset($locIds)) {
            $proKeys['pD'] = array_fill_keys($proIds, '');
            $locWiseData = array_fill_keys($locIds, $proKeys);
            //get available quantity by locationProduct table
            $productList = $this->getModel('LocationProductTable')->getProductQtyListByLocID($proIds, $locIds, $isAllProducts, $categoryIds);
            //get all product uom details
            $productUoms = $this->getModel('ProductUomTable')->getProductUomListByProductIds();
            $displayData = $this->getModel('DisplaySetupTable')->fetchAllDetails()->current();

            $selectedProductIDs = array();
            foreach ($productList as $p) {
                if (!in_array($p['pID'], $selectedProductIDs)) {
                    $selectedProductIDs[] = $p['pID'];
                }

                $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUoms[$p['pID']]);
                $qtySubtraction = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                $locWiseData[$p['locID']]['pD'][$p['pID']] = array(
                    'aQty' => $qtySubtraction,
                    'pN' => $p['pName'],
                    'pCD' => $p['pCD'],
                    'locationName' => $p['locName'],
                    'locationCode' => $p['locCD'],
                    'uomAbbr' => $thisqty['uomAbbr'],
                    'category' => $p['categoryName']
                    );
            }

            $proIds = $selectedProductIDs;
            $itemData = $this->getModel('ItemInTable')->getProductDetails($proIds, $locIds);
            $productID = NULL;
            $locID = NULL;
            $totalQty = 0;
            $costValue = 0;
            $discountValue = 0;
            $taxValue = 0;
            $dataFlag = false;
            $uomList = [];

            foreach ($itemData as $i) {
                $locPData = $this->getModel('LocationProductTable')->getLocationProductByProIdLocId($i['productID'], $i['locationID']);
                //get quantity and unit price according to dispaly uom
                if(!array_key_exists($i['productID'], $uomList)){
                    // $productUom = $this->getModel('Inventory\Model\ProductUomTable')->getProductUomListByProductID($i['productID']);
                    $uomList[$i['productID']] = $productUoms[$i['productID']];
                }

                $thisqty = $this->getProductQuantityViaDisplayUom($i['qtySubtraction'], $uomList[$i['productID']]);
                $qtySubtraction = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                if ($displayData['FIFOCostingFlag'] == 1) {
                    $itemInPrice = $this->getProductUnitPriceViaDisplayUom($i['itemInPrice'], $uomList[$i['productID']]);
                } else {
                    $itemInPrice = $this->getProductUnitPriceViaDisplayUom(floatval($locPData->locationProductAverageCostingPrice), $uomList[$i['productID']]);
                }

                //When $productID is previous productID
                //if previous $productID is equal to new $i['productID']
                if ($productID == $i['productID'] && $locID == $i['locationID']) {
                    $totalQty += $qtySubtraction;
                    $costValue += $qtySubtraction * $itemInPrice;
                    $discountValue += $qtySubtraction * $i['itemInDiscount'];
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'totalQty' => $totalQty));
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'costValue' => $costValue));
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'discountValue' => $discountValue));
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'taxValue' => $taxValue));
                    $dataFlag = true;
                }
                //first product data set put into $locWiseData array.because $flag is flase
                if ($dataFlag == false) {
                    $totalQty = $qtySubtraction;
                    $costValue = $qtySubtraction * $itemInPrice;
                    $discountValue = $qtySubtraction * $i['itemInDiscount'];
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'pID' => $i['productID'],
                        'costValue' => $costValue,
                        'totalQty' => $totalQty,
                        'discountValue' => $discountValue,
                        'taxValue' => $taxValue
                    ));
                }
                //first comming productID and transferID, set to following variables
                $productID = $i['productID'];
                $locID = $i['locationID'];
                $dataFlag = false;
            }

            return $locWiseData;
        }
    }
    
    public function locationWiseStockValuePdf($postData)
    {
        try{
            $isAllProducts = filter_var($postData['isAllProducts'], FILTER_VALIDATE_BOOLEAN);
            $proIds = $postData['productIds'];
            $categoryIds = $postData['categoryIds'];
            $locIds = $postData['locationIdList'];
            $name = 'Location Wise Stock Value Report';
            if ($isAllProducts) {

                $productData = $this->getModel('LocationProductTable')->searchLocationWiseInventoryProductsForDropdown($locIds, NULL, FALSE);
                foreach ($productData as $value) {
                    $proIds[$value['productID']] = $value['productID'];
                }
            }
            $locWiseStockValueData = $this->_getLocationWiseStockValueData($proIds, $locIds, $isAllProducts, $categoryIds);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);
            $locWiseStockValueView = new ViewModel(array(
                'locWSVD' => $locWiseStockValueData,
                'cD' => $companyDetails,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $locWiseStockValueView->setTemplate('reporting/stock-in-hand-report/generate-location-wise-stock-value-pdf');
            $locWiseStockValueView->setTerminal(true);
            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($locWiseStockValueView);
            $pdfPath = $this->downloadPDF('location-wise-stock-value-report', $htmlContent, true);
            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');
        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing pdf data.');
        }

    }
    
    public function locationWiseStockValueSheet($postData)
    {
        try{
            $proIds = $postData['productIds'];
            $isAllProducts = filter_var($postData['isAllProducts'], FILTER_VALIDATE_BOOLEAN);
            $locIds = $postData['locationIdList'];
            $categoryIds = $postData['categoryIds'];

            if ($isAllProducts) {
                $productData = $this->getModel('LocationProductTable')->searchLocationWiseInventoryProductsForDropdown($locIds, NULL, FALSE);
                foreach ($productData as $value) {
                    $proIds[$value['productID']] = $value['productID'];
                }
            }

            $locWiseStockValueData = $this->_getLocationWiseStockValueData($proIds, $locIds, $isAllProducts, $categoryIds);
            $cD = $this->getCompanyDetails();
            if ($locWiseStockValueData) {
                $title = '';
                $tit = 'LOCATION WISE STOCK VALUE REPORT';
                $in = array(
                    0 => "", 1
                    => $tit,
                    2 => "",
                    3 => ""
                );
                $title.=implode(",", $in) . "\n";
                $in = array(0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";
                $arrs = array("NO", "INDEX", "LOCATION NAME - CODE", "PRODUCT CATEGORY","PRODUCT CODE", "PRODUCT NAME", "AVAILABLE QUANTITY","UOM", "TOTAL VALUE({$this->companyCurrencySymbol})");
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                $grandTotal = 0;
                foreach ($locWiseStockValueData as $key => $data) {
                    $locationWiseTotal = 0;
                    $count = 1;
                    $uPrice = 0;
                    foreach ($data['pD'] as $key => $value) {
                        $uomAbbr = isset($value['uomAbbr']) ? $value['uomAbbr'] : '';
                        $availableQty = isset($value['aQty']) ? $value['aQty'] : 0.00;
                        $costValue = isset($value['costValue']) ? $value['costValue'] : 0.00;
                        $totalDiscount = isset($value['discountValue']) ? $value['discountValue'] : 0.00;
                        $taxValue = isset($value['taxValue']) ? $value['taxValue'] : 0.00;

                        if ($availableQty != 0 && $availableQty != NULL) {
                             $uPrice = ((floatval($costValue) + floatval($taxValue)) - floatval($totalDiscount)) / floatval($availableQty) ? ((floatval($costValue) + floatval($taxValue)) - floatval($totalDiscount)) / floatval($availableQty) : 0.00;
                        }

//                        $availableQty = $value['aQty'] ? $value['aQty'] : 0.00;
//                        $uPrice = $value['costValue'] / $availableQty ? $value['costValue'] / $availableQty : 0.00;
//                        $uomAbbr = $value['uomAbbr'] ? $value['uomAbbr'] : '';
//                        $totalDiscount = $value['totalDiscount'] ? $value['totalDiscount'] : 0;
                        if (!empty($value['locationName']) && !empty($value['locationCode'])) {
                            if ($count == 1) {
                                $in = array(
                                    0 => $i,
                                    1 => $count,
                                    2 => $value['locationName'] . '-' . $value['locationCode'],
                                    3 => $value['category'],
                                    4 => "{$value['pCD']}",
                                    5 => $value['pN'],
                                    6 => $availableQty,
                                    7 => $uomAbbr,
                                    8 => ($uPrice * $availableQty)
                                );
                                $arr.= implode(",", $in) . "\n";
                            } else {
                                $in = array(
                                    0 => '',
                                    1 => $count,
                                    2 => '',
                                    3 => $value['category'],
                                    4 => "{$value['pCD']}",
                                    5 => $value['pN'],
                                    6 => $availableQty,
                                    7 => $uomAbbr,
                                    8 => ($uPrice * $availableQty)
                                );
                                $arr.= implode(",", $in) . "\n";
                            }
                            $count++;
                        }
                        $availableQtyAmount = $uPrice * $availableQty;
                        $grandTotal += $availableQtyAmount;
                        $locationWiseTotal += $availableQtyAmount;
                    }
                    $i++;
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => '',
                        7 => "Location Wise Total",
                        8 => number_format($locationWiseTotal, 2, '.', '')
                    );
                    $arr.= implode(",", $in) . "\n";
                }
                $in = array(
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                    'Grand Total Value ('.$this->companyCurrencySymbol.')',
                    number_format($grandTotal, 2, '.', '')
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }
            $name = "location-wise-stock-value-report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);
            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }

    }

    /**
    * this function use to get serial movement details
    * @param array itemId(s)
    * @param array serialId(s)
    * return mix
    **/
    public function serialItemMovingData($proIDs, $isAllProducts, $serialIds, $isAllSerial ) 
    {
        // get item in and Item out details for the selected item and serial ids
        $allProductFlag = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $allSerialFlag = filter_var($isAllSerial, FILTER_VALIDATE_BOOLEAN);
        if ($allSerialFlag) {
            $serialIds = null;       
        } 
        if ($allProductFlag) {
            $proIDs = null;       
        }
        $itemInOutDetails = $this->getModel('ItemInTable')->getSerialItemDetails($proIDs,$serialIds);
        $returnValue = $this->setDocumentTypesAndCodes ($itemInOutDetails);
        
        return $returnValue;

    }

    /**
    * generate data set for serial movement reports
    **/ 
    public function setDocumentTypesAndCodes ($transactions)
    {
        $serialItemSet = [];
        foreach ($transactions as $key => $transData) {
            if ($transData['productSerialCode'] && $transData['productCode'] && $transData['productName']) {
                $serialItemSet[$transData['productID']]['productName'] = $transData['productName'];
                $serialItemSet[$transData['productID']]['productCode'] = $transData['productCode'];
                $dataList = array(
                    'productName' => $transData['productName']." - ".$transData['productCode'],
                    'productID' => $transData['productID'],
                    'serialCode' => $transData['productSerialCode'],
                    'serialID' => $transData['productSerialID'],
                    'locationName' => $transData['locationName']." - ".$transData['locationCode'],
                    'locationID' => $transData['locationID'],
                    'inDocType' => $transData['itemInDocumentType'],
                    'outDocType' => $transData['itemOutDocumentType'],
                    'soldQty' => $transData['itemInSoldQty']
                    );
                switch ($transData['itemInDocumentType']) {
                    case 'Goods Received Note':
                            $dataList['inDocCode'] = $transData['inGrnCode'];
                        break;
                    case 'Payment Voucher':
                            $dataList['inDocCode'] = $transData['inPICode'];
                        break;
                    case 'Expense Payment Voucher':
                            $dataList['inDocCode'] = $transData['inPVCode'];
                        break;
                    case 'Invoice Edit':
                    case 'Invoice Cancel':
                            $dataList['inDocCode'] = $transData['inInvCode'];
                        break;
                    case 'Delivery Note Edit':
                            $dataList['inDocCode'] = $transData['inDelCode'];
                        break;
                    case 'Sales Returns':
                            $dataList['inDocCode'] = $transData['salesReturnCode'];
                        break;
                    case 'Credit Note':
                            $dataList['inDocCode'] = $transData['inCredCode'];
                        break;
                    case 'Inventory Positive Adjustment':
                    case 'Delete Negative Adjustment':
                            $dataList['inDocCode'] = $transData['inAdjCode'];
                        break;
                    case 'Inventory Transfer':
                            $dataList['inDocCode'] = $transData['transferCode'];
                            $serialItemSet = $this->setTranferData($transData,$serialItemSet);
                        break;
                    default:
                            $dataList['inDocCode'] = '';
                        break;
                }

                switch ($transData['itemOutDocumentType']) {
                    case 'Sales Invoice':
                            $dataList['outDocCode'] = $transData['outInvCode'];
                        break;
                    case 'Delivery Note':
                            $dataList['outDocCode'] = $transData['outDelCode'];
                        break;
                    case 'Goods Received Note Cancel':
                            $dataList['outDocCode'] = $transData['outGrnCode'];
                        break;
                    case 'Inventory Negative Adjustment':
                    case 'Delete Positive Adjustment':
                            $dataList['outDocCode'] = $transData['outAdjCode'];
                        break;
                    case 'Payment Voucher Cancel':
                            $dataList['outDocCode'] = $transData['outPICode'];
                        break;
                    case 'Purchase Return':
                            $dataList['outDocCode'] = $transData['purchaseReturnCode'];
                        break;
                    default:
                            $dataList['outDocCode'] = '';
                        break;
                }
                $serialItemSet[$transData['productID']]['serialData'][$transData['productSerialCode']][] = $dataList;
            }
                
        }
        return $serialItemSet;
    }

     /**
    * add transfer details to the serial movement report
    **/
    public function setTranferData($transData,$serialItemSet)
    {
        
        foreach ($serialItemSet[$transData['productID']]['serialData'][$transData['productSerialCode']] as $key => $value) {
            if ($value['outDocType'] == '' && $value['soldQty'] == 1 && $value['locationID'] == $transData['locationIDOut']) {
                $serialItemSet[$transData['productID']]['serialData'][$transData['productSerialCode']][$key]['outDocType'] = 'Inventory Transfer';
                $serialItemSet[$transData['productID']]['serialData'][$transData['productSerialCode']][$key]['outDocCode'] = $transData['transferCode'];
            }
        }
        return $serialItemSet;
    }


    public function serialItemMovementPdf($postData)
    {
        try {
            $proIDs = $postData['productIds'];
            $isAllProducts = $postData['isAllProducts'];
            $serialIds = $postData['serialIds'];
            $isAllSerial = $postData['isAllSerial'];

            $name = 'Serial Item Movement Report';

            $companyDetails = $this->getCompanyDetails();
            $productLists = $this->serialItemMovingData($proIDs, $isAllProducts, $serialIds, $isAllSerial);

            $headerView = $this->headerViewTemplate($name, null);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $productListsView = new ViewModel(array(
                'lpL' => $productLists,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));
            $productListsView->setTemplate('reporting/stock-in-hand-report/generate-serial-item-movement-pdf');
            $productListsView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($productListsView);
            $pdfPath = $this->downloadPDF('Serial-movement', $htmlContent, true);

            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing pdf data.');
        }
    }

    public function serialItemMovementSheet($postData)
    {
        try {
            $proIDs = $postData['productIds'];
            $isAllProducts = $postData['isAllProducts'];
            $serialIds = $postData['serialIds'];
            $isAllSerial = $postData['isAllSerial'];
            $translator = new Translator();

            $reportName = "Serial Item Movement Report";
            $name = "serial_movement_report";
            $companyDetails = $this->getCompanyDetails();
            $productLists = $this->serialItemMovingData($proIDs, $isAllProducts, $serialIds, $isAllSerial);

            if ($productLists) {
                $title = '';
                $tit = 'SERIAL ITEM MOVEMENT REPORT';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";
                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = array("PRODUCT CODE/NAME", "SERIAL CODE", "IN/OUT", "DOCUMENT TYPE", "DOCUMENT CODE");

                $header = implode(",", $arrs);
                $arr = '';
                foreach ($productLists as $data) {
                    foreach ($data['serialData'] as $key => $serialDataSet) {
                        foreach ($serialDataSet as $serialData){
                            
                            $in = [
                                $serialData['productName'],
                                $serialData['serialCode'],
                                "In",
                                $serialData['inDocType'],
                                $serialData['inDocCode'],
                            ];
                            $arr.=implode(",", $in) . "\n";

                            $in = [
                                $serialData['productName'],
                                $serialData['serialCode'],
                                "Out",
                                $serialData['outDocType'],
                                $serialData['outDocCode']
                            ];
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                }
            } else {
                $arr = "no matching records found\n";
            }
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $reportName], 'csv report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

}