<?php

namespace Reporting\Service;

use Core\Service\ReportService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;

class DailySalesReportService extends ReportService
{
	/**
	 * This function is used to get data for view invoied daily item with cost
	 */
	public function getDataForViewInvoiceDailySalesItemWithCostReport($postData)
	{
		$fromDate = $postData->fromDate;
        $toDate = $postData->toDate;
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $translator = new Translator();
        $name = $translator->translate('Daily Invoice Sales Item With Cost Report');
        $period = $fromDate . ' - ' . $toDate;
        $categoryIds = $postData['categoryIds'];
        $isAllCategories = ($postData['isAllCategories'] == 0) ? false : true;
        $bypassCategory = false;

        if ($isAllCategories) {
            $bypassCategory = true;
        } else {
            if (empty($categoryIds)) {
                $bypassCategory = true;
            } 
        }

        $companyDetails = $this->getCompanyDetails();
        $dailySalesItemData = $this->getDailySalesItemsWithCostDetails($fromDate, $toDate, $categoryIds, $bypassCategory);
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        if ($isAllCategories || sizeof($categoryIds) > 0) {
            $dailySalesItemData = $this->arrangeDataSetAccordingToTheCategories($dailySalesItemData);
        }

        $dailySalesView = new ViewModel(array(
            'cD' => $companyDetails,
            'dailySalesItemData' => $dailySalesItemData,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'currencySymbol' => $this->companyCurrencySymbol,
            'headerTemplate' => $headerViewRender,
        ));
        $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-invoiced-items-with-cost');

        return $dailySalesView;
	}


    public function arrangeDataSetAccordingToTheCategories($dataSet) {
        $categoryWiseArr = [];

        foreach ($dataSet as $key => $value) {
            $categoryWiseArr[$value['categoryID']][$key] = $value;   
        }

        $recArr = [];
        foreach ($categoryWiseArr as $key1 => $value1) {
            $recArr = array_merge_recursive($recArr, $value1);
        }

        return $recArr;
    }

	/**
	 * This function is used to get data for pdf invoied daily item with cost
	 */
	public function getDataForViewInvoiceDailySalesItemWithCostPdfReport($postData)
	{
		$fromDate = $postData['fromDate'];
        $toDate = $postData['toDate'];
        $categoryIds = $postData['categoryIds'];
        $isAllCategories = ($postData['isAllCategories'] == 0) ? false : true;
        $translator = new Translator();
        $name = $translator->translate('Daily Invoice Sales Item With Cost Report');
        $period = $fromDate . ' - ' . $toDate;
        $companyDetails = $this->getCompanyDetails();
        $bypassCategory = false;

        if ($isAllCategories) {
            $bypassCategory = true;
        } else {
            if (empty($categoryIds)) {
                $bypassCategory = true;
            } 
        }
        $dailySalesItemData = $this->getDailySalesItemsWithCostDetails($fromDate, $toDate, $categoryIds, $bypassCategory);

        if ($isAllCategories || sizeof($categoryIds) > 0) {
            $dailySalesItemData = $this->arrangeDataSetAccordingToTheCategories($dailySalesItemData);
        }


        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $dailySalesView = new ViewModel(array(
            'cD' => $companyDetails,
            'dailySalesItemData' => $dailySalesItemData,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'currencySymbol' => $this->companyCurrencySymbol,
            'headerTemplate' => $headerViewRender,
        ));
        $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-invoiced-items-with-cost');

        // get rendered view into variable
        $htmlContent = $this->viewRendererHtmlContent($dailySalesView);

        return $htmlContent;
	}

	/**
	 * This function is used to get data for csv invoied daily item with cost
	 */
	public function getDataForViewInvoiceDailySalesItemWithCostCsvReport($postData)
	{
		$fromDate = $postData->fromDate;
        $toDate = $postData->toDate;
        $categoryIds = $postData['categoryIds'];
        $isAllCategories = ($postData['isAllCategories'] == 0) ? false : true;
        $cD = $this->getCompanyDetails();

        $bypassCategory = false;

        if ($isAllCategories) {
            $bypassCategory = true;
        } else {
            if (empty($categoryIds)) {
                $bypassCategory = true;
            } 
        }
        $dailySalesItemData = $this->getDailySalesItemsWithCostDetails($fromDate, $toDate, $categoryIds, $bypassCategory);

        if ($isAllCategories || sizeof($categoryIds) > 0) {
            $dailySalesItemData = $this->arrangeDataSetAccordingToTheCategories($dailySalesItemData);
        }




        if ($dailySalesItemData) {
            $title = '';
            $tit = 'DAILY INVOICE SALES ITEMS WITH COST REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("Item Name", "Item Code","Item Category", "Quantity","Total Cost Price","Total Sell Price","Profit", "Profit Percentage");
            $header = implode(",", $arrs);
            $arr = '';

            $netTotalCost = 0;
            $netTotalSales = 0;
            $netProfit = 0;
            $isDataExist = false;
            foreach ($dailySalesItemData as $data) {
                $totalCost = 0;
                $totalSales = 0;
                $profit = 0;
                $totalQty = 0;
                $uomAbbr = '';
                $itemCreditNoteQty = ($data['creditNoteProductQty']) ? $data['creditNoteProductQty'] : 0;
                $itemCreditNoteTotal = ($data['creditNoteProductTotal']) ? $data['creditNoteProductTotal'] : 0;
                $itemCreditNoteCost = ($data['creditNoteCost']) ? $data['creditNoteCost'] : 0;
                foreach ($data['data'] as $value) {
                    $qty = ($value['itemOutQty'] != NULL) ? $value['itemOutQty'] : 0;
                    $totalQty+= $qty;
                                
                    if ($data['costingMethod'] == "FIFO") {
                        //item in calculations
                        $itemInPrice = ($value['itemInPrice'] != NULL) ? $value['itemInPrice'] : 0;
                        $itemInDiscount = ($value['itemInDiscount'] != NULL) ? $value['itemInDiscount'] : 0;
                        $itemInTaxAmount = $value['itemInTaxAmount'];
                        $itemInActualPrice = ($itemInPrice + $itemInTaxAmount) - $itemInDiscount;

                        //item out calculations
                        $itemOutPrice = ($value['itemOutPrice'] != NULL) ? $value['itemOutPrice'] : 0;
                        $itemOutDiscount = ($value['itemOutDiscount'] != NULL) ? $value['itemOutDiscount'] : 0;
                        $invoiceDiscountValue = ($value['documentDiscountPercentage']) ? $value['documentDiscountPercentage'] : 0;
                        $itemOutTaxAmount = $value['itemOutTaxAmount'];
                        $itemOutActualPrice = $itemOutPrice - $itemOutDiscount;

                        if ($invoiceDiscountValue != 0) {
                            $itemOutActualPrice -= ($itemOutActualPrice * (float)$invoiceDiscountValue / (float)100);
                        }

                        $itemOutActualPrice = $itemOutActualPrice + $itemOutTaxAmount;

                    } else {
                        //item in calculations
                        $itemInActualPrice = ($value['itemInAverageCosting'] != NULL) ? $value['itemInAverageCosting'] : 0;

                        //item out calculations
                        $itemOutActualPrice = ($value['itemOutAvergeCosting'] != NULL) ? $value['itemOutAvergeCosting'] : 0;
                    }
                    
                    $totalCost+= $itemInActualPrice * $qty;
                    $netTotalCost += $itemInActualPrice * $qty;

                    $totalSales+= $itemOutActualPrice * $qty;
                    $netTotalSales += $itemOutActualPrice * $qty;
                    $uomAbbr = $value['uomAbbr'];
                }
                //item credit note calculations
                $totalQty = $totalQty - $itemCreditNoteQty;
                $totalSales = $totalSales - $itemCreditNoteTotal;
                $netTotalSales = $netTotalSales - $itemCreditNoteTotal;
                $totalCost = $totalCost - $itemCreditNoteCost;
                $netTotalCost = $netTotalCost - $itemCreditNoteCost;
                $profit = $totalSales - $totalCost;
                $netProfit += $profit;
                if ($totalQty != 0) {
                    $isDataExist = true;
                    $in = array(
                        0 => $data['productName'],
                        1 => $data['productCode'],
                        2 => str_replace(array(','), '-', $data['categoryName']),
                        3 => $totalQty . ' ' . $uomAbbr,
                        4 => str_replace(',', '', $totalCost),
                        5 => str_replace(',', '', $totalSales),
                        6 => str_replace(',', '', $profit),
                        7 => number_format((floatval(str_replace(',', '', $profit) / str_replace(',', '', $totalSales)) * 100), 2).'%',
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            }
            if ($isDataExist) {
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "Grand Total:",
                    4 => str_replace(',', '', $netTotalCost),
                    5 => str_replace(',', '', $netTotalSales),
                    6 => str_replace(',', '', $netProfit),
                    7 => ""
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found";
            }
        } else {
            $arr = "no matching records found";
        }

        $csvContent = $this->csvContent($title, $header, $arr);

        return $csvContent;
	}

	/**
	 * This function is used to get data for daily sales item with cost details
	 */
	public function getDailySalesItemsWithCostDetails($fromDate, $toDate, $categoryIds, $bypassCategory = false)
    {
        if (isset($fromDate) && isset($toDate)) {
            $dailySalesItemData = array();
            $getInvoiceItemDetails = $this->getModel('ItemOutTable')->getSalesInvoiceDetails($fromDate, $toDate);
            $getDlnWiseInvoiceItemDetails = $this->getModel('ItemOutTable')->getDlnWiseSalesInvoiceDetails($fromDate, $toDate);

            $getItemDetails = array_merge($getInvoiceItemDetails, $getDlnWiseInvoiceItemDetails);
            if (!empty($getItemDetails)) {
                foreach ($getItemDetails as $row) {
                    $getLocationProduct = $this->getModel("LocationProductTable")->getLocationProducts($row['itemOutLocationProductID'], $row['locationID']);

                    if ($bypassCategory) {
                        $productID = $getLocationProduct->productID;
                        $productName = $getLocationProduct->productName;
                        $categoryName = $getLocationProduct->categoryName;
                        $categoryID = $getLocationProduct->categoryID;
                        $productCode = $getLocationProduct->productCode;
                        $salesinvoiceData = [];

                        if ($productID) {
                            //set quantity and uomAbbr according to display UOM
                            $productUom = $this->getModel('ProductUomTable')->getProductUomListByProductID($productID);
                            $thisqty = $this->getProductQuantityViaDisplayUom($row['itemOutQty'], $productUom);
                            $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                            $itemInUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemInPrice'], $productUom);
                            $itemOutUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemOutPrice'], $productUom);

                            $itemOutAvergeCosting = $row['itemOutAverageCostingPrice']; 
                            $itemInAverageCosting = $row['itemInAverageCostingPrice'];

                            if ($row['itemOutDocumentType'] == "Sales Invoice") {
                                //get product credit note details
                                $creditNoteProductDetails = $this->getModel('InvoiceTable')->getInvoiceProductCreditNoteDetails($productID, $fromDate, $toDate);
                                $creditNoteCostDetails = $this->getModel('InvoiceTable')->getCreditNoteCostByCreditNoteIDAndProductId($productID, $fromDate, $toDate);
                                $creditNoteProductQty = $creditNoteProductDetails['creditNoteProductQty'];
                                $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal'];
                                $creditNoteCost = $creditNoteCostDetails['creditNoteCost'];

                                $salesinvoiceData = $this->getModel('InvoiceTable')->getInvoiceMetadataByInvoiceId($row['itemOutDocumentID']);

                            } else {
                                //if any return made to dln in this date range
                                $creditNoteProductDetails = $this->getModel('InvoiceTable')->getInvoiceProductSalesReturnDetails($productID, $fromDate, $toDate, $row['salesInvoiceID']);

                                if (!is_null($row['itemOutSerialID'])) {
                                    $creditNoteProductQty = sqrt($creditNoteProductDetails['creditNoteProductQty']);
                                    $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal']/$creditNoteProductQty;
                                    $creditNoteCost = ($creditNoteProductDetails['creditNoteCost']/$creditNoteProductQty) * $creditNoteProductQty;
                                } else {
                                    $creditNoteProductQty = $creditNoteProductDetails['creditNoteProductQty'];
                                    $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal'];
                                    $creditNoteCost = $creditNoteProductDetails['creditNoteCost'] * $creditNoteProductQty;
                                }
                            }
                               
                            if ($row['itemOutDocumentType'] == "Delivery Note") {
                                $deliveryNoteID = $row['itemOutDocumentID'];
                                $getDeliveryNoteProductTax = $this->getModel("DeliveryNoteProductTaxTable")->getProductTax($deliveryNoteID, $productID);
                                $itemOutTaxAmount = 0;
                                if (!empty($getDeliveryNoteProductTax)) {
                                    foreach ($getDeliveryNoteProductTax as $t) {
                                        $qty = ($t['deliveryNoteProductQuantity'] == NULL) ? 1 : $t['deliveryNoteProductQuantity'];
                                        $tax = $t['deliveryNoteTaxAmount'] / $qty;
                                        $itemOutTaxAmount += $tax;
                                    }
                                }
                            } else {
                                $invoiceID = $row['itemOutDocumentID'];
                                $getInvoiceProductTax = $this->getModel("InvoiceProductTaxTable")->getProductTax($invoiceID, $productID);
                                $itemOutTaxAmount = 0;
                                if (!empty($getInvoiceProductTax)) {
                                    foreach ($getInvoiceProductTax as $t) {
                                        $qty = ($t['salesInvoiceProductQuantity'] == NULL) ? 1 : $t['salesInvoiceProductQuantity'];
                                        $tax = $t['salesInvoiceProductTaxAmount'] / $qty;
                                        $itemOutTaxAmount += $tax;
                                    }
                                }
                            }

                            $itemInTaxAmount = 0;
                            if ($row['itemInDocumentType'] == "Goods Received Note") {
                                $grnID = $row['itemInDocumentID'];
                                $getGrnTax = $this->getModel("GrnProductTaxTable")->getGrnProductTax($grnID, $row['itemInLocationProductID']);
                                foreach ($getGrnTax as $g) {
                                    $grnQty = ($g['grnProductTotalQty'] == NULL) ? 1 : $g['grnProductTotalQty'];
                                    $itemInTaxAmount += $g['grnTaxAmount'] / $grnQty;
                                }
                            } else if ($row['itemInDocumentType'] == "Payment Voucher") {
                                $purchaseInvoiceID = $row['itemInDocumentID'];
                                $purchaseInvoiceTax = $this->getModel("PurchaseInvoiceProductTaxTable")->getPurchaseInvoiceProductTax($purchaseInvoiceID, $row['itemInLocationProductID']);
                                foreach ($purchaseInvoiceTax as $p) {
                                    $purchaseInvoiceQty = ($p['purchaseInvoiceProductTotalQty'] == NULL) ? 1 : $p['purchaseInvoiceProductTotalQty'];
                                    $itemInTaxAmount += $p['purchaseInvoiceTaxAmount'] / $purchaseInvoiceQty;
                                }
                            }

                            // check costing method
                            $setup = $this->getModel('DisplaySetupTable')->fetchAllDetails()->current();

            
                            if (!$setup['averageCostingFlag']) {
                                $dailySalesItemData[$productID]['costingMethod'] = 'FIFO';
                            } else {
                                $dailySalesItemData[$productID]['costingMethod'] = "Average";
                            }

                            $dailySalesItemData[$productID]['productName'] = $productName;
                            $dailySalesItemData[$productID]['categoryName'] = $categoryName;
                            $dailySalesItemData[$productID]['categoryID'] = $categoryID;
                            $dailySalesItemData[$productID]['productCode'] = $productCode;
                            $dailySalesItemData[$productID]['creditNoteProductTotal'] = $creditNoteProductTotal;
                            $dailySalesItemData[$productID]['creditNoteProductQty'] = $creditNoteProductQty;
                            $dailySalesItemData[$productID]['creditNoteCost'] = $creditNoteCost;
                            $dailySalesItemData[$productID]['data'][$row['itemOutID']] = array(
                                'itemOutQty' => $quantity,
                                'itemOutPrice' => $itemOutUnitPrice,
                                'itemOutAvergeCosting' => $itemOutAvergeCosting,
                                'itemOutDiscount' => $row['itemOutDiscount'],
                                'itemInPrice' => $itemInUnitPrice,
                                'itemInAverageCosting' => $itemInAverageCosting,
                                'itemInDiscount' => $row['itemInDiscount'],
                                'uomAbbr' => $thisqty['uomAbbr'],
                                'itemOutTaxAmount' => $itemOutTaxAmount,
                                'itemInTaxAmount' => $itemInTaxAmount
                            );

                            if (!empty($salesinvoiceData) && $salesinvoiceData['salesInvoiceTotalDiscountType'] == 'presentage') {
                                $dailySalesItemData[$productID]['data'][$row['itemOutID']]['documentDiscountPercentage'] = $salesinvoiceData['salesInvoiceDiscountRate'];
                            }

                        }
                    } else {

                        if (in_array($getLocationProduct->categoryID, $categoryIds)) {
                            $productID = $getLocationProduct->productID;
                            $productName = $getLocationProduct->productName;
                            $categoryName = $getLocationProduct->categoryName;
                            $categoryID = $getLocationProduct->categoryID;
                            $productCode = $getLocationProduct->productCode;
                            $salesinvoiceData = [];

                            if ($productID) {
                                //set quantity and uomAbbr according to display UOM
                                $productUom = $this->getModel('ProductUomTable')->getProductUomListByProductID($productID);
                                $thisqty = $this->getProductQuantityViaDisplayUom($row['itemOutQty'], $productUom);
                                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                                $itemInUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemInPrice'], $productUom);
                                $itemOutUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemOutPrice'], $productUom);

                                $itemOutAvergeCosting = $row['itemOutAverageCostingPrice']; 
                                $itemInAverageCosting = $row['itemInAverageCostingPrice'];

                                if ($row['itemOutDocumentType'] == "Sales Invoice") {
                                    //get product credit note details
                                    $creditNoteProductDetails = $this->getModel('InvoiceTable')->getInvoiceProductCreditNoteDetails($productID, $fromDate, $toDate);
                                    $creditNoteCostDetails = $this->getModel('InvoiceTable')->getCreditNoteCostByCreditNoteIDAndProductId($productID, $fromDate, $toDate);
                                    $creditNoteProductQty = $creditNoteProductDetails['creditNoteProductQty'];
                                    $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal'];
                                    $creditNoteCost = $creditNoteCostDetails['creditNoteCost'];

                                    $salesinvoiceData = $this->getModel('InvoiceTable')->getInvoiceMetadataByInvoiceId($row['itemOutDocumentID']);

                                } else {
                                    //if any return made to dln in this date range
                                    $creditNoteProductDetails = $this->getModel('InvoiceTable')->getInvoiceProductSalesReturnDetails($productID, $fromDate, $toDate, $row['salesInvoiceID']);

                                    if (!is_null($row['itemOutSerialID'])) {
                                        $creditNoteProductQty = sqrt($creditNoteProductDetails['creditNoteProductQty']);
                                        $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal']/$creditNoteProductQty;
                                        $creditNoteCost = ($creditNoteProductDetails['creditNoteCost']/$creditNoteProductQty) * $creditNoteProductQty;
                                    } else {
                                        $creditNoteProductQty = $creditNoteProductDetails['creditNoteProductQty'];
                                        $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal'];
                                        $creditNoteCost = $creditNoteProductDetails['creditNoteCost'] * $creditNoteProductQty;
                                    }
                                }
                                   
                                if ($row['itemOutDocumentType'] == "Delivery Note") {
                                    $deliveryNoteID = $row['itemOutDocumentID'];
                                    $getDeliveryNoteProductTax = $this->getModel("DeliveryNoteProductTaxTable")->getProductTax($deliveryNoteID, $productID);
                                    $itemOutTaxAmount = 0;
                                    if (!empty($getDeliveryNoteProductTax)) {
                                        foreach ($getDeliveryNoteProductTax as $t) {
                                            $qty = ($t['deliveryNoteProductQuantity'] == NULL) ? 1 : $t['deliveryNoteProductQuantity'];
                                            $tax = $t['deliveryNoteTaxAmount'] / $qty;
                                            $itemOutTaxAmount += $tax;
                                        }
                                    }
                                } else {
                                    $invoiceID = $row['itemOutDocumentID'];
                                    $getInvoiceProductTax = $this->getModel("InvoiceProductTaxTable")->getProductTax($invoiceID, $productID);
                                    $itemOutTaxAmount = 0;
                                    if (!empty($getInvoiceProductTax)) {
                                        foreach ($getInvoiceProductTax as $t) {
                                            $qty = ($t['salesInvoiceProductQuantity'] == NULL) ? 1 : $t['salesInvoiceProductQuantity'];
                                            $tax = $t['salesInvoiceProductTaxAmount'] / $qty;
                                            $itemOutTaxAmount += $tax;
                                        }
                                    }
                                }

                                $itemInTaxAmount = 0;
                                if ($row['itemInDocumentType'] == "Goods Received Note") {
                                    $grnID = $row['itemInDocumentID'];
                                    $getGrnTax = $this->getModel("GrnProductTaxTable")->getGrnProductTax($grnID, $row['itemInLocationProductID']);
                                    foreach ($getGrnTax as $g) {
                                        $grnQty = ($g['grnProductTotalQty'] == NULL) ? 1 : $g['grnProductTotalQty'];
                                        $itemInTaxAmount += $g['grnTaxAmount'] / $grnQty;
                                    }
                                } else if ($row['itemInDocumentType'] == "Payment Voucher") {
                                    $purchaseInvoiceID = $row['itemInDocumentID'];
                                    $purchaseInvoiceTax = $this->getModel("PurchaseInvoiceProductTaxTable")->getPurchaseInvoiceProductTax($purchaseInvoiceID, $row['itemInLocationProductID']);
                                    foreach ($purchaseInvoiceTax as $p) {
                                        $purchaseInvoiceQty = ($p['purchaseInvoiceProductTotalQty'] == NULL) ? 1 : $p['purchaseInvoiceProductTotalQty'];
                                        $itemInTaxAmount += $p['purchaseInvoiceTaxAmount'] / $purchaseInvoiceQty;
                                    }
                                }

                                // check costing method
                                $setup = $this->getModel('DisplaySetupTable')->fetchAllDetails()->current();

                
                                if (!$setup['averageCostingFlag']) {
                                    $dailySalesItemData[$productID]['costingMethod'] = 'FIFO';
                                } else {
                                    $dailySalesItemData[$productID]['costingMethod'] = "Average";
                                }

                                $dailySalesItemData[$productID]['productName'] = $productName;
                                $dailySalesItemData[$productID]['categoryName'] = $categoryName;
                                $dailySalesItemData[$productID]['categoryID'] = $categoryID;
                                $dailySalesItemData[$productID]['productCode'] = $productCode;
                                $dailySalesItemData[$productID]['creditNoteProductTotal'] = $creditNoteProductTotal;
                                $dailySalesItemData[$productID]['creditNoteProductQty'] = $creditNoteProductQty;
                                $dailySalesItemData[$productID]['creditNoteCost'] = $creditNoteCost;
                                $dailySalesItemData[$productID]['data'][$row['itemOutID']] = array(
                                    'itemOutQty' => $quantity,
                                    'itemOutPrice' => $itemOutUnitPrice,
                                    'itemOutAvergeCosting' => $itemOutAvergeCosting,
                                    'itemOutDiscount' => $row['itemOutDiscount'],
                                    'itemInPrice' => $itemInUnitPrice,
                                    'itemInAverageCosting' => $itemInAverageCosting,
                                    'itemInDiscount' => $row['itemInDiscount'],
                                    'uomAbbr' => $thisqty['uomAbbr'],
                                    'itemOutTaxAmount' => $itemOutTaxAmount,
                                    'itemInTaxAmount' => $itemInTaxAmount
                                );

                                if (!empty($salesinvoiceData) && $salesinvoiceData['salesInvoiceTotalDiscountType'] == 'presentage') {
                                    $dailySalesItemData[$productID]['data'][$row['itemOutID']]['documentDiscountPercentage'] = $salesinvoiceData['salesInvoiceDiscountRate'];
                                }

                            } 
                        }
                    }
                }
            }
            return $dailySalesItemData;
        }
    }

    public function generateInvoicedSubItemBasedBomProductReport ($postData) {
        $fromDate = $postData['fromDate'];
        $toDate = $postData['toDate'];
        $isAllLocation = $postData['isAllLocation'];
        $locations = $postData['locations'];
        $bomIds = (!empty($postData['bomIds'])) ? $postData['bomIds'] : [];
        $isAllBomItems = $postData['isAllItems'];

        if ($isAllBomItems) {
            $bomDetails =$this->getModel('BomTable')->getSubItemBasedAllBomIds();
            foreach ($bomDetails as $key => $value) {
                $bomIds[]= $value['bomID'];
            }
        }

        $translator = new Translator();
        $name = $translator->translate('Invoiced Sub Item Based Bom Products Report');
        $period = $fromDate . ' - ' . $toDate;
        $companyDetails = $this->getCompanyDetails();
        $subItemBasedBomData = $this->getSubItemBasedBomProductDetails($fromDate, $toDate, $bomIds, $isAllLocation, $locations);

        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $dailySalesView = new ViewModel(array(
            'cD' => $companyDetails,
            'subItemBasedBomData' => $subItemBasedBomData,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'currencySymbol' => $this->companyCurrencySymbol,
            'headerTemplate' => $headerViewRender,
        ));
        $dailySalesView->setTemplate('reporting/daily-sales-report/invoiced-sub-item-based-bom-product-details');

        return $dailySalesView;
    }


    public function generateInvoicedSubItemBasedBomProductPdfReport ($postData) {
        $fromDate = $postData['fromDate'];
        $toDate = $postData['toDate'];
        $isAllLocation = $postData['isAllLocation'];
        $locations = $postData['locations'];
        $bomIds = (!empty($postData['bomIds'])) ? $postData['bomIds'] : [];
        $isAllBomItems = $postData['isAllItems'];

        if ($isAllBomItems) {
            $bomDetails =$this->getModel('BomTable')->getSubItemBasedAllBomIds();
            foreach ($bomDetails as $key => $value) {
                $bomIds[]= $value['bomID'];
            }
        }

        $translator = new Translator();
        $name = $translator->translate('Invoiced Non Inventory Bom Products Report');
        $period = $fromDate . ' - ' . $toDate;
        $companyDetails = $this->getCompanyDetails();
        $subItemBasedBomData = $this->getSubItemBasedBomProductDetails($fromDate, $toDate, $bomIds, $isAllLocation, $locations);

        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $dailySalesView = new ViewModel(array(
            'cD' => $companyDetails,
            'subItemBasedBomData' => $subItemBasedBomData,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'currencySymbol' => $this->companyCurrencySymbol,
            'headerTemplate' => $headerViewRender,
        ));
        $dailySalesView->setTemplate('reporting/daily-sales-report/invoiced-sub-item-based-bom-product-details');
        $dailySalesView->setTerminal(true);

        return $dailySalesView;
    }

    public function generateInvoicedSubItemBasedBomProductCsvReport ($postData) {
        $fromDate = $postData['fromDate'];
        $toDate = $postData['toDate'];
        $isAllLocation = $postData['isAllLocation'];
        $locations = $postData['locations'];
        $bomIds = (!empty($postData['bomIds'])) ? $postData['bomIds'] : [];
        $isAllBomItems = $postData['isAllItems'];

        if ($isAllBomItems) {
            $bomDetails =$this->getModel('BomTable')->getSubItemBasedAllBomIds();
            foreach ($bomDetails as $key => $value) {
                $bomIds[]= $value['bomID'];
            }
        }

        $translator = new Translator();
        $name = $translator->translate('Invoiced Non Inventory Bom Products Report');
        $period = $fromDate . ' - ' . $toDate;
        $companyDetails = $this->getCompanyDetails();
        $subItemBasedBomData = $this->getSubItemBasedBomProductDetails($fromDate, $toDate, $bomIds, $isAllLocation, $locations);




        if (!empty($subItemBasedBomData)) {
            $title = '';
            $in = array(
                0 => "",
                1 => 'INVOICED NON INVENTORY BOM PRODUCT DETAILS REPORTS',
                2 => ""
            );
            $title = implode(",", $in) . "\n";
            $in = array(
                0 => $companyDetails[0]->companyName,
                1 => $companyDetails[0]->companyAddress,
                2 => 'Tel: ' . $companyDetails[0]->telephoneNumber
            );
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("LOCATION", "BOM PRODUCT", "SUB ITEM", "SUB ITEM TOTAL QTY", "SUB ITEM TOTAL AMOUNT");
            $header = implode(",", $arrs);
            $arr = '';

            foreach ($subItemBasedBomData as $key1 => $dataSet) {
                $in = array(
                    0 => $key1,
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => ""
                );
                $arr.=implode(",", $in) . "\n";

                foreach ($dataSet as $key => $data) {
                    $in = array(
                        0 => "",
                        1 => $key,
                        2 => "",
                        3 => "",
                        4 => ""
                    );
                    $arr.=implode(",", $in) . "\n";

                    foreach ($data['subProductData'] as $value) {
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => $value['subProduct'],
                            3 => $value['totalqty'],
                            4 => $value['totalAmount']
                        );
                        $arr.=implode(",", $in) . "\n";                        
                    }
                    $in = array(
                        0 => "",
                        1 => "Total",
                        2 => "",
                        3 => $data['totalQty'],
                        4 => $data['totalAmount']
                    );
                    $arr.=implode(",", $in) . "\n";
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => ""
                    );
                    $arr.=implode(",", $in) . "\n";

                }
            }
        } else {
            $in = array('no matching records found');
            $arr.=implode(",", $in) . "\n";
        }

        $csvContent = $this->csvContent($title, $header, $arr);
        return $csvContent;
    }

    public function getSubItemBasedBomProductDetails($fromDate, $toDate, $bomIds, $isAllLocation, $locations){

        $bomDetails =$this->getModel('InvoicedBomSubItemsTable')->getInvoicedBomSubItemsByBomItemIDAndDateRange($fromDate, $toDate, $bomIds, $isAllLocation, $locations);

        $bomData = [];

        foreach ($bomDetails as $key => $value) {
            $bomData[$value['locationName']][$value['bomCode'].'-'.$value['productName']]['totalQty'] += floatval($value['subProductTotalQty']);
            $bomData[$value['locationName']][$value['bomCode'].'-'.$value['productName']]['totalAmount'] += (floatval($value['subProductTotal']));

            $subProductData =$this->getModel('productTable')->getProduct($value['subProductID']);

            $bomData[$value['locationName']][$value['bomCode'].'-'.$value['productName']]['subProductData'][$value['subProductID']]['subProduct'] = $subProductData->productCode.'-'.$subProductData->productName;
            $bomData[$value['locationName']][$value['bomCode'].'-'.$value['productName']]['subProductData'][$value['subProductID']]['totalqty'] += floatval($value['subProductTotalQty']);
            $bomData[$value['locationName']][$value['bomCode'].'-'.$value['productName']]['subProductData'][$value['subProductID']]['totalAmount'] += floatval($value['subProductTotal']);

        }
            
        return $bomData;
    }

}