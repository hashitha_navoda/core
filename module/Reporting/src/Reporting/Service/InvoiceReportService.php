<?php

namespace Reporting\Service;

use Core\Service\ReportService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;

class InvoiceReportService extends ReportService
{

	/**
	 * This function is used to get invoice details with customer report view data
	 */
	public function getInvoiceDetailsWithCustomerReport($postData)
	{
		$invoiceData = $this->getInvoiceDetailsWithCustomerData($postData->fromDate, $postData->toDate, $postData->locationIds);

		$name = 'Invoice Details With Customer Details';

        $headerView = $this->headerViewTemplate($name, $period = $postData->fromDate . ' - ' . $postData->toDate);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $reportView = new ViewModel(array(
            'invoiceData' => $invoiceData,
            'cSymbol' => $this->companyCurrencySymbol,
            'headerTemplate' => $headerViewRender,
        ));
        $reportView->setTemplate('reporting/invoice-report/generate-invoice-details-with-customer-pdf');

        return $reportView;
	}

	/**
     * This function is used to get invoice details with customer report pdf data
     */
    public function getInvoiceDetailsWithCustomerReportPdf($postData)
    {

        $invoiceData = $this->getInvoiceDetailsWithCustomerData($postData->fromDate, $postData->toDate, $postData->locationIds);
        $companyDetails = $this->getCompanyDetails();
        $translator = new Translator();
        $name = $translator->translate('Invoice Details With Customer Details');
        $period = $postData->fromDate . ' - ' . $postData->toDate;
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $view = new ViewModel(array(
            'cD' => $companyDetails,
            'invoiceData' => $invoiceData,
            'fromDate' => $postData->fromDate,
            'toDate' => $postData->toDate,
            'headerTemplate' => $headerViewRender,
        ));
        $view->setTemplate('reporting/invoice-report/generate-invoice-details-with-customer-pdf');
        $view->setTerminal(true);

        return $view;
    }

    /**
     * This function is used to get invoice details with customer report csv data
     */
    public function getInvoiceDetailsWithCustomerReportSheet($postData)
    {
        $invoiceData = $this->getInvoiceDetailsWithCustomerData($postData->fromDate, $postData->toDate, $postData->locationIds);
        $companyDetails = $this->getCompanyDetails();
        if (!empty($invoiceData)) {
            $data = array(
                "Invoice Details With Customer Details"
            );
            $title.=implode(",", $data) . "\n";

            $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $data) . "\n";

            $data = array(
                $companyDetails[0]->companyName,
                $companyDetails[0]->companyAddress,
                "Tel: " . $companyDetails[0]->telephoneNumber
            );
            $title.=implode(",", $data) . "\n";

            $data = array(
                "Period :" . $postData->fromDate . " - " . $postData->toDate
            );
            $title.=implode(",", $data) . "\n";

            $tableHead = array('Invoice Number', 'Invoice Date', 'Customer Name','Customer Code','Customer Contact Number', 'Customer Address',  'Customer City', 'Invoice Amount', 'Invoice Comment');
            $header.=implode(",", $tableHead) . "\n";

            foreach ($invoiceData as $invData) {
                $data = array($invData['saleInvoiceCode'],$invData['saleInvoiceDate'] ,str_replace(',', "", $invData['customerName']),$invData['customerCode'], $invData['customerTelephoneNo'], str_replace(',', "", $invData['customerAddress']), str_replace(',', "", $invData['customerCity']),$invData['saleInvoiceTotal'], str_replace(',', "", $invData['saleInvoiceComment']));
                $arr.=implode(",", $data) . "\n";
            }
        } else {
            $arr = "no matching records found";
        }
        
        $csvContent = $this->csvContent($title, $header, $arr);
        
        return $csvContent;
    }

    /**
     * This function is used to get data for invoice details with customer report
     */
	public function getInvoiceDetailsWithCustomerData($fromDate, $endDate, $locationIds)
	{
		$invData = $this->getModel('InvoiceTable')->getSalesSummeryWithOutTax($fromDate, $endDate, $locationIds);

		$invCustomerData = [];
		foreach ($invData as $key => $value) {
			$temp['saleInvoiceCode'] = $value['salesInvoiceCode'];
			$temp['saleInvoiceDate'] = $value['salesInvoiceIssuedDate'];
			$temp['saleInvoiceTotal'] = $value['TotalAmount'];
			$temp['saleInvoiceComment'] = (is_null($value['salesInvoiceComment'])) ? '-' :$value['salesInvoiceComment'];
			$temp['customerName'] = $value['customerTitle'].$value['customerName'];
            $temp['customerCode'] = $value['customerCode'];
			$temp['customerTelephoneNo'] = (is_null($value['customerTelephoneNumber'])) ? '-' :$value['customerTelephoneNumber'];
			$temp['customerAddress'] = $value['customerProfileLocationNo']." ".$value['customerProfileLocationRoadName1']." ".$value['customerProfileLocationRoadName2']." ".$value['customerProfileLocationRoadName3'];
            $temp['customerCity'] = $value['customerProfileLocationTown'];
			$invCustomerData[] = $temp;
		}
		return $invCustomerData;
	}
}