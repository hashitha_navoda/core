<?php

namespace Reporting\Service;

use Core\Service\ReportService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;

class FinanceReportService extends ReportService

{

	public function getBalanceSheetData($startDate, $endDate, $dimensionType = null, $dimensionValue = null)
    {

        if(isset($startDate) && isset($endDate)){
            $financeAccounts = array();
            $fAccountsTypesData = $this->getModel('FinanceAccountTypesTable')->fetchAll();

            foreach ($fAccountsTypesData as $typeKey => $typeValue) {

                    $financeAccounts[$typeValue->financeAccountTypesID]['financeAccountTypesID'] = $typeValue->financeAccountTypesID;
                    $financeAccounts[$typeValue->financeAccountTypesID]['financeAccountTypesName'] = $typeValue->financeAccountTypesName;
                    $fAccountClassData =$this->getModel('FinanceAccountClassTable')->getAllActiveAccountClassesByAccountTypeID($typeValue->financeAccountTypesID);

                foreach ($fAccountClassData as $key => $value) {
                    $financeAccounts[$typeValue->financeAccountTypesID][$value['financeAccountClassID']]['financeAccountClassID'] = $value['financeAccountClassID'];
                    $financeAccounts[$typeValue->financeAccountTypesID][$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];
                    $fAccountsData = $this->getHierarchyByAccountClassIDs($value['financeAccountClassID'], $startDate, $endDate, true, $dimensionType, $dimensionValue);
                    $financeAccounts[$typeValue->financeAccountTypesID][$value['financeAccountClassID']]['financeAccounts'] = $fAccountsData ;
                }
            }

            return $financeAccounts;
        }
    }


    public function getBalanceSheetDataForBalanceSheetPdfAndCsv($startDate, $endDate, $dimensionType = null, $dimensionValue = null)
    {

        if(isset($startDate) && isset($endDate)){
            $financeAccounts = array();
            $fAccountsTypesData = $this->getModel('FinanceAccountTypesTable')->fetchAll();

            foreach ($fAccountsTypesData as $typeKey => $typeValue) {

                    $financeAccounts[$typeValue->financeAccountTypesID]['financeAccountTypesID'] = $typeValue->financeAccountTypesID;
                    $financeAccounts[$typeValue->financeAccountTypesID]['financeAccountTypesName'] = $typeValue->financeAccountTypesName;
                    $fAccountClassData =$this->getModel('FinanceAccountClassTable')->getAllActiveAccountClassesByAccountTypeID($typeValue->financeAccountTypesID);

                foreach ($fAccountClassData as $key => $value) {
                    $financeAccounts[$typeValue->financeAccountTypesID][$value['financeAccountClassID']]['financeAccountClassID'] = $value['financeAccountClassID'];
                    $financeAccounts[$typeValue->financeAccountTypesID][$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];
                    $fAccountsData = $this->getHierarchyByAccountClassIDsForBalanceSheet($value['financeAccountClassID'], $startDate, $endDate, true, $dimensionType, $dimensionValue);
                    $financeAccounts[$typeValue->financeAccountTypesID][$value['financeAccountClassID']]['financeAccounts'] = $fAccountsData ;
                }
            }

            return $financeAccounts;
        }
    }

    public function getBalanceSheetDataForViewReport($startDate, $endDate, $dimensionType = null, $dimensionValue = null, $locationIDs = null, $sepLocBal = false)
    {

        if(isset($startDate) && isset($endDate)){
            $financeAccounts = array();
            $fAccountsTypesData = $this->getModel('FinanceAccountTypesTable')->getFinanceAccountTypeWithTheirClasses();

            foreach ($fAccountsTypesData as $typeKey => $typeValue) {

                    $financeAccounts[$typeValue['financeAccountTypesID']]['financeAccountTypesID'] = $typeValue['financeAccountTypesID'];
                    $financeAccounts[$typeValue['financeAccountTypesID']]['financeAccountTypesName'] = $typeValue['financeAccountTypesName'];

                    $financeAccounts[$typeValue['financeAccountTypesID']][$typeValue['financeAccountClassID']]['financeAccountClassID'] = $typeValue['financeAccountClassID'];
                    $financeAccounts[$typeValue['financeAccountTypesID']][$typeValue['financeAccountClassID']]['financeAccountClassName'] = $typeValue['financeAccountClassName'];
                    $fAccountsData = $this->getHierarchyByAccountClassIDsForBalanceSheetView($typeValue['financeAccountClassID'], $startDate, $endDate, true, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);
                    $financeAccounts[$typeValue['financeAccountTypesID']][$typeValue['financeAccountClassID']]['financeAccounts'] = $fAccountsData ;
            }
            return $financeAccounts;
        }
    }


    public function balanceSheetCsv($postData)
    {
    	try {
    		$endDate = $postData['endDate'];
    		$dimensionType = $postData['dimensionType'];
	        $dimensionValue = $postData['dimensionValue'];
	        $locationIDs = $postData['locationID'];
	        $sepLocBal = ($postData['sepLocBal'] == 'true') ? true: false;
	        $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

	        $netProfitData = [];

	        $locationData = null;

	        if ($sepLocBal) {
	            foreach ($locationIDs as $key => $value) {
	                $location = $this->getModel('LocationTable')->getLocation($value);

	                $locationData[] = [
	                    'locationName' => $location->locationName,
	                    'locationCode' => $location->locationCode,
	                    'locationID' => $location->locationID

	                ];
	            }
	        }

	        if ($sepLocBal) {

                $balanceSheetData = $this->getBalanceSheetDataForViewReport('',$endDate, $dimensionType, $dimensionValue, $locationData, $sepLocBal);


                $translator = new Translator();
	            $name = $translator->translate('Balance Sheet');
	            $period = $endDate;

	            $profitAndLostData = [];
	            foreach ($balanceSheetData as $key => $value) {
	                if($value['financeAccountTypesID'] == 4){
	                    $profitAndLostData[0] = $value;
	                } else if( $value['financeAccountTypesID'] == 6){
	                    $profitAndLostData[1] = $value;
	                } else if( $value['financeAccountTypesID'] ==  7){
	                    $profitAndLostData[2] = $value;
	                } else if( $value['financeAccountTypesID'] ==  3){
	                    $profitAndLostData[3] = $value;
	                }
	            }
	            ksort($profitAndLostData);


	            foreach ($locationData as $key55 => $value55) {
	                    # code...
                    $netProfitData[$value55['locationCode'].'-'.$value55['locationID']] = $this->getNetProfitAmountLocationWise($profitAndLostData, $value55);
                }

		    	if ($balanceSheetData) {
	                $title = '';
	                $tit = 'BALANCE SHEET REPORT';
	                $in = ["", $tit];
	                $title = implode(",", $in) . "\n";

	                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];

	                $title.=implode(",", $in) . "\n";

	                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
	                $title.=implode(",", $in) . "\n";

	                $in = ["Period : " . $endDate];
	                $title.=implode(",", $in) . "\n";

	                $arr = '';

	                $libilityPlEquityCreditAmount = [];
	                $libilityPlEquityDebitAmount = [];

	                foreach ($balanceSheetData as $key => $typeData) {
	                    if($typeData['financeAccountTypesID'] == 1 || $typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
	                        $arr.= implode(",", []). "\n";
	                        $in1 = [$typeData['financeAccountTypesName']];
	                        $arr.= implode(",", $in1). "\n";

	                        $in = ["ACCOUNT CODE", "ACCOUNT NAME"];


	                        $headerLen = sizeof($in);
	                        foreach ($locationData as $key99 => $value99) {
	                        	$in[$headerLen] = strtoupper($value99['locationName'].' '.'Balance');
	                        	$headerLen++;
	                        }

	                       
	                        $arr.= implode(",", $in). "\n";

	                        $TotalDebitValue = [];
	                        $TotalCreditValue = [];
	                        $grandTotal = [];
	                        foreach ($typeData as $tbalance) {
	                            $accClassSubTotal = [];
	                            if(isset($tbalance['financeAccounts'][0]['child'])){
	                                $in = ["", preg_replace("/(,|;)/", " ", $tbalance['financeAccountClassName']), ""];
	                                $arr.= implode(",", $in). "\n";
	                                foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
	                                    	$emptyVal = 0;
	                                	foreach ($locationData as $key22 => $value22) {
											
											$locKey = $value22['locationCode'].'-'.$value22['locationID'];
											$amountData = [];
											if(isset($value['child'])){
												$amountData[$locKey] = $this->calculateAmountsLocWise($value['child'], $locKey);
												$TotalDebitValue[$locKey]+=$amountData[$locKey]['totalDebitAmount'];
												$TotalCreditValue[$locKey]+=$amountData[$locKey]['totalCreditAmount'];
												if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
													$libilityPlEquityCreditAmount[$locKey]+=$amountData[$locKey]['totalCreditAmount'];
													$libilityPlEquityDebitAmount[$locKey]+=$amountData[$locKey]['totalDebitAmount'];
												}
											}
											if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
												$libilityPlEquityCreditAmount[$locKey]+=$value['data']->totalCreditAmount[$locKey];
												$libilityPlEquityDebitAmount[$locKey]+=$value['data']->totalDebitAmount[$locKey];
											}
											$TotalDebitValue[$locKey]+=$value['data']->totalDebitAmount[$locKey];
											$TotalCreditValue[$locKey]+=$value['data']->totalCreditAmount[$locKey];



											$createdCredit[$locKey] = ($amountData[$locKey]['totalCreditAmount'] != 0)? floatval($amountData[$locKey]['totalCreditAmount'])+ floatval($value['data']->totalCreditAmount[$locKey]) : floatval($value['data']->totalCreditAmount[$locKey]);
											$createdDebit[$locKey] = ($amountData[$locKey]['totalDebitAmount'] != 0)? floatval($amountData[$locKey]['totalDebitAmount'])+ floatval($value['data']->totalDebitAmount[$locKey]) : floatval($value['data']->totalDebitAmount[$locKey]);
											if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
												$balance[$value['data']->financeAccountsID][$locKey] = floatval($createdCredit[$locKey]) - floatval($createdDebit[$locKey]);
											}
											else if($typeData['financeAccountTypesID'] == 1){
												$balance[$value['data']->financeAccountsID][$locKey] = floatval($createdDebit[$locKey]) - floatval($createdCredit[$locKey]);
											}
											$accClassSubTotal[$tbalance['financeAccountClassID']][$locKey] += floatval($balance[$value['data']->financeAccountsID][$locKey]);

											if($balance[$value['data']->financeAccountsID][$locKey] < 0){
												$viewBalnce[$locKey] = '('. floatval(($balance[$value['data']->financeAccountsID][$locKey] * -1)) .')';
											} else {
												$viewBalnce[$locKey] = floatval($balance[$value['data']->financeAccountsID][$locKey]);
											}


											if ($balance[$value['data']->financeAccountsID][$locKey] == 0) {
												$emptyVal++;
											}


										}


										if ($hideZeroVal) {
											if ($emptyVal != sizeof($locationData)) {
												$in = [$value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName)];


			                                    $tt = 2;
						                        foreach ($locationData as $key88 => $value88) {
						                        	$yy = $value88['locationCode'].'-'.$value88['locationID'];
						                        	$in[$tt] = $viewBalnce[$yy];
						                        	$tt++;
						                        }

			                                    $arr.=implode(",", $in) . "\n";
											}

										} else  {

		                                    $in = [$value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName)];


		                                    $tt = 2;
					                        foreach ($locationData as $key88 => $value88) {
					                        	$yy = $value88['locationCode'].'-'.$value88['locationID'];
					                        	$in[$tt] = $viewBalnce[$yy];
					                        	$tt++;
					                        }

		                                    $arr.=implode(",", $in) . "\n";
										}

	                                    if(isset($value['child'])){
	                                        $level = 1;
	                                        $arr.= $this->getChildAccountsLocationWise($value['child'],$tbalance,$level,"balanceSheet", $typeData['financeAccountTypesID'], $locationData, $hideZeroVal);
	                                    }
	                                }



	                                $in = ["sub Total balance", ""];
	                                $subSize = 2;

	                                foreach ($locationData as $key77 => $value77) {
			                        	$cc = $value77['locationCode'].'-'.$value77['locationID'];
		                               
			                        	$in[$subSize] = ($accClassSubTotal[$tbalance['financeAccountClassID']][$cc] < 0) ? '('.floatval(($accClassSubTotal[$tbalance['financeAccountClassID']][$cc] * -1)).')' : floatval($accClassSubTotal[$tbalance['financeAccountClassID']][$cc]);
			                        	$subSize++;
			                        }


	                                $arr.=implode(",", $in) . "\n";
	                                $in = ["", "", ""];
	                                    $arr.=implode(",", $in) . "\n";

	                            }
	                        }
	                        
	                        if($typeData['financeAccountTypesID'] == 5){
					   			foreach ($locationData as $key55 => $value55) {
					   				$ket = $value55['locationCode'].'-'.$value55['locationID'];
						   			if($netProfitData[$ket] < 0){
						   				$ntPro[$ket] = '('.floatval(($netProfitData[$ket] * -1)).')';
						   			} else {
						   				$ntPro[$ket] = floatval($netProfitData[$ket]);
						   			}
					   			}

					   			$in = ["Net profit",""];


					   			$netSize = 2;
		                        foreach ($locationData as $key54 => $value54) {
		                        	$ke = $value54['locationCode'].'-'.$value54['locationID'];

		                        	$in[$netSize] = $ntPro[$ke];
		                        	$netSize++;
		                        }

	                            $arr.=implode(",", $in)."\n";
					   		}

	                        $in = ["Total", ""];
					   		$totSize = 2;
					   		foreach ($locationData as $key00 => $value00) {
			   					$cc = $value00['locationCode'].'-'.$value00['locationID'];
				   			
				   				if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
				   					$grandTotal[$cc] = floatval($TotalCreditValue[$cc]) - floatval($TotalDebitValue[$cc]);
				   					
				   				} else {
				   					$grandTotal[$cc] = floatval($TotalDebitValue[$cc]) - floatval($TotalCreditValue[$cc]);
				   				}

				   				if($typeData['financeAccountTypesID'] == 5){

				   					$grandTotal[$cc] += $netProfitData[$cc];
				   				}

				   				if($grandTotal[$cc] < 0){
				   					$grandTotalView[$cc] = (!empty($grandTotal[$cc])) ? '('. floatval(($grandTotal[$cc]* -1)) . ')' : 0.00;
				   				} else {
				   					$grandTotalView[$cc] = (!empty($grandTotal[$cc])) ? floatval($grandTotal[$cc]) : 0.00;
				   				}


				   				$in[$totSize] = $grandTotalView[$cc];
				   				$totSize++;
				   			}

	                        $arr.=implode(",", $in) . "\n";
	                    }
	                }

	                $arr.= implode(",", []). "\n";
	                $finalTot = 2;
	                $in = ["Equity + Liability", ""];
	                foreach ($locationData as $key50 => $value50) {
						$pp = $value50['locationCode'].'-'.$value50['locationID'];
						
						$EqWithLib[$pp] = (floatval($netProfitData[$pp]) + floatval($libilityPlEquityCreditAmount[$pp])) - (floatval($libilityPlEquityDebitAmount[$pp]));
						if($EqWithLib[$pp] < 0){
						 	$eqWithLibView[$pp] = '(' . floatval(($EqWithLib[$pp] * -1)).')';
						} else {
						 	$eqWithLibView[$pp] = floatval($EqWithLib[$pp]);
						}

						$in[$finalTot] = $eqWithLibView[$pp];
						$finalTot ++;
					}

	               
	                $arr.= implode(",", $in). "\n";

	            } else {
	                $arr = "no matching records found\n";
	            }

	            $name = "Statement_of_financial_position.csv";
	            $csvContent = $this->csvContent($title, $header, $arr);
	            $csvPath = $this->generateCSVFile($name, $csvContent);

	            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');
            } else {

            	$balanceSheetData = $this->getBalanceSheetDataForViewReport('',$endDate, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);

            	$translator = new Translator();
	            $name = $translator->translate('Balance Sheet');
	            $period = $endDate;

	            $profitAndLostData = [];
	            foreach ($balanceSheetData as $key => $value) {
	                if($value['financeAccountTypesID'] == 4){
	                    $profitAndLostData[0] = $value;
	                } else if( $value['financeAccountTypesID'] == 6){
	                    $profitAndLostData[1] = $value;
	                } else if( $value['financeAccountTypesID'] ==  7){
	                    $profitAndLostData[2] = $value;
	                } else if( $value['financeAccountTypesID'] ==  3){
	                    $profitAndLostData[3] = $value;
	                }
	            }
	            ksort($profitAndLostData);

	                $netProfit = $this->getNetProfitAmount($profitAndLostData);

		    	if ($balanceSheetData) {
	                $title = '';
	                $tit = 'BALANCE SHEET REPORT';
	                $in = ["", $tit];
	                $title = implode(",", $in) . "\n";

	                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];

	                $title.=implode(",", $in) . "\n";

	                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
	                $title.=implode(",", $in) . "\n";

	                $in = ["Period : " . $endDate];
	                $title.=implode(",", $in) . "\n";

	                $arr = '';

	                $libilityPlEquityCreditAmount = 0;
	                $libilityPlEquityDebitAmount = 0;

	                foreach ($balanceSheetData as $key => $typeData) {
	                    if($typeData['financeAccountTypesID'] == 1 || $typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
	                        $arr.= implode(",", []). "\n";
	                        $in1 = [$typeData['financeAccountTypesName']];
	                        $arr.= implode(",", $in1). "\n";

	                        $in = ["ACCOUNT CODE", "ACCOUNT NAME", "BALANCE"];
	                        $arr.= implode(",", $in). "\n";

	                        $TotalDebitValue = 0;
	                        $TotalCreditValue = 0;
	                        foreach ($typeData as $tbalance) {
	                            $accClassSubTotal = 0;
	                            if(isset($tbalance['financeAccounts'][0]['child'])){
	                                $in = ["", preg_replace("/(,|;)/", " ", $tbalance['financeAccountClassName']), ""];
	                                $arr.= implode(",", $in). "\n";
	                                foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
	                                    $amountData=[];
	                                    if(isset($value['child'])){
	                                        $amountData = $this->calculateAmounts($value['child']);
	                                        $TotalDebitValue+=$amountData['totalDebitAmount'];
	                                        $TotalCreditValue+=$amountData['totalCreditAmount'];
	                                        if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
	                                            $libilityPlEquityCreditAmount+=$amountData['totalCreditAmount'];
	                                            $libilityPlEquityDebitAmount+=$amountData['totalDebitAmount'];
	                                        }
	                                    }
	                                    if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
	                                        $libilityPlEquityCreditAmount+=$value['data']->totalCreditAmount;
	                                        $libilityPlEquityDebitAmount+=$value['data']->totalDebitAmount;
	                                    }
	                                    $TotalDebitValue+=$value['data']->totalDebitAmount;
	                                    $TotalCreditValue+=$value['data']->totalCreditAmount;

	                                    $creditAmount = ($amountData['totalCreditAmount'] != 0)? (float) ($amountData['totalCreditAmount']+$value['data']->totalCreditAmount ): (float) $value['data']->totalCreditAmount;
	                                    $debitAmount = ($amountData['totalDebitAmount'] != 0)? (float) ($amountData['totalDebitAmount']+$value['data']->totalDebitAmount ): (float) $value['data']->totalDebitAmount;

	                                    if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
	                                        $viewCreditDebitAmount = floatval($creditAmount) - floatval($debitAmount);
	                                    } else {
	                                        $viewCreditDebitAmount = floatval($debitAmount) - floatval($creditAmount);
	                                    }
	                                    $accClassSubTotal += $viewCreditDebitAmount;
	                                    $actVal = $viewCreditDebitAmount;

	                                    if($viewCreditDebitAmount < 0){
	                                        $viewCreditDebitAmount = '(' . floatval($viewCreditDebitAmount*(-1)).')';
	                                    }

	                                    if ($hideZeroVal) {
	                                    	if ($actVal != 0) {
			                                    $in = [$value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName), $viewCreditDebitAmount];
			                                    $arr.=implode(",", $in) . "\n";
	                                    	}
	                                    } else {
	                                    	$in = [$value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName), $viewCreditDebitAmount];
			                                    $arr.=implode(",", $in) . "\n";
	                                    }


	                                    if(isset($value['child'])){
	                                        $level = 1;
	                                        $arr.= $this->getChildAccounts($value['child'],$tbalance,$level,"balanceSheet", $typeData['financeAccountTypesID'], $hideZeroVal);
	                                    }
	                                }
	                                if($accClassSubTotal< 0){
	                                    $accClassSubTotal = '('.$accClassSubTotal*(-1).')';
	                                }

	                                $in = ["sub Total balance", "", $accClassSubTotal];
	                                    $arr.=implode(",", $in) . "\n";
	                                $in = ["", "", ""];
	                                    $arr.=implode(",", $in) . "\n";

	                            }
	                        }
	                        $netProfView = 0;
	                        if($typeData['financeAccountTypesID'] == 5){
	                            $netProfView = $netProfit;
	                            if($netProfit < 0){
	                                $profit = '('. $netProfit*(-1) .')';
	                            } else {
	                                $profit = $netProfit;
	                            }
	                            $in = ["Net profit","", $profit];
	                            $arr.=implode(",", $in)."\n";
	                        }

	                        if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
	                            $totalCreditDebit = floatval($TotalCreditValue) -  floatval($TotalDebitValue);
	                        } else {
	                            $totalCreditDebit = floatval($TotalDebitValue) - floatval($TotalCreditValue);
	                        }
	                        $totalCreditDebit += $netProfView;
	                        if($totalCreditDebit < 0){
	                            $totalCreditDebit = '('.floatval($totalCreditDebit)*(-1) . ')';
	                        }
	                        $in = ["Total", "", $totalCreditDebit];
	                        $arr.=implode(",", $in) . "\n";
	                    }
	                }

	                $arr.= implode(",", []). "\n";
	                $libEqlDebCredit = floatval($netProfit) + floatval($libilityPlEquityCreditAmount) - floatval($libilityPlEquityDebitAmount);
	                if($libEqlDebCredit < 0){
	                    $libEqlDebCredit = '(' . floatval($libEqlDebCredit)*(-1).')';
	                }
	                $in = ["Equity + Liability", "", $libEqlDebCredit];
	                $arr.= implode(",", $in). "\n";

	            } else {
	                $arr = "no matching records found\n";
	            }

	            $name = "Statement_of_financial_position.csv";
	            $csvContent = $this->csvContent($title, $header, $arr);
	            $csvPath = $this->generateCSVFile($name, $csvContent);

	            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');
            }
	   	} catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }

    }

    public function balanceSheetPdf($postData)
    {
    	try {
	            $endDate = $postData['endDate'];
	            $dimensionType = $postData['dimensionType'];
        		$dimensionValue = $postData['dimensionValue'];
        		$locationIDs = $postData['locationID'];
		        $sepLocBal = ($postData['sepLocBal'] == 'true') ? true: false;
		        $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

		        $netProfitData = [];

		        $locationData = null;

		        if ($sepLocBal) {
		            foreach ($locationIDs as $key => $value) {
		                $location = $this->getModel('LocationTable')->getLocation($value);

		                $locationData[] = [
		                    'locationName' => $location->locationName,
		                    'locationCode' => $location->locationCode,
		                    'locationID' => $location->locationID

		                ];
		            }
		        }        


	            $translator = new Translator();
	            $name = $translator->translate('Balance Sheet');
	            $period = $endDate;

	            if ($sepLocBal) {
	                $balanceSheetData = $this->getBalanceSheetDataForViewReport('',$endDate, $dimensionType, $dimensionValue, $locationData, $sepLocBal);
	            } else {
	                $balanceSheetData = $this->getBalanceSheetDataForViewReport('',$endDate, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);

	            }

	            // $balanceSheetData = $this->getBalanceSheetDataForViewReport('',$endDate, $dimensionType, $dimensionValue);
	            $profitAndLostData = [];
	            foreach ($balanceSheetData as $key => $value) {
	                if($value['financeAccountTypesID'] == 4){
	                    $profitAndLostData[0] = $value;
	                } else if( $value['financeAccountTypesID'] == 6){
	                    $profitAndLostData[1] = $value;
	                } else if( $value['financeAccountTypesID'] ==  7){
	                    $profitAndLostData[2] = $value;
	                } else if( $value['financeAccountTypesID'] ==  3){
	                    $profitAndLostData[3] = $value;
	                }
	            }
	            ksort($profitAndLostData);
	            // $netProfit = $this->getNetProfitAmount($profitAndLostData);

	            if (!$sepLocBal) {
	                $netProfit = $this->getNetProfitAmount($profitAndLostData);
	            } else {

	                foreach ($locationData as $key55 => $value55) {
	                    # code...
	                    $netProfitData[$value55['locationCode'].'-'.$value55['locationID']] = $this->getNetProfitAmountLocationWise($profitAndLostData, $value55);
	                }

	            }

	            $companyDetails = $this->getCompanyDetails();

	            $headerView = $this->headerViewTemplate($name, $period);
	            $headerView->setTemplate('reporting/template/headerTemplate');
	            $headerViewRender = $this->htmlRender($headerView);

	            $balanceSheetView = new ViewModel(array(
	                'balanceSheetData' => $balanceSheetData,
	                'locationIDs' => (!empty($locationIDs)) ? $locationIDs : null,
	                'netProfitData' => $netProfitData,
	                'locationData' => $locationData,
                	'sepLocBal' => $sepLocBal,
                	'hideZeroVal' => $hideZeroVal,
	                'netProfit' => $netProfit,
	                'endDate' => $endDate,
	                'headerTemplate' => $headerViewRender,
	                'cD' => $companyDetails)
	            );

	            $balanceSheetView->setTemplate('reporting/finance-report/generate-balance-sheet-pdf');
	            $balanceSheetView->setTerminal(true);

	            // get rendered view into variable
	            $htmlContent = $this->viewRendererHtmlContent($balanceSheetView);
	            $pdfPath = $this->downloadPDF('Statement-of-financial-position', $htmlContent);

	            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

	    } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing pdf data.');
        }
    }

    public function getHierarchyByAccountClassIDs($accountClassID, $startDate = null, $endDate = null, $activeFlag = false, $dimensionType = null, $dimensionValue = null)
    {

        $res = $this->getModel('FinanceAccountsTable')->fetchAllHierarchicalByAccountClassID($accountClassID, $activeFlag);
        $all = array();
        $tempAll = array();
        foreach ($res as $loo) {
            $loo->totalCreditAmount = 0.00;
            $loo->totalDebitAmount = 0.00;

            $data = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($loo->financeAccountsID, $startDate, $endDate, $yearClosing = true, $dimensionType, $dimensionValue);

            if($data['journalEntryID'] != NULL){
                $loo->totalCreditAmount = $data['totalCreditAmount'];
                $loo->totalDebitAmount = $data['totalDebitAmount'];

            }
            //get journal Entry details
            $journalEntrys = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRange($loo->financeAccountsID, $startDate, $endDate, $dimensionType, $dimensionValue);
            //get duplicated credit and debit value for this account
            $jEntriesAndDuplicatedCreditDebitValue = $this->getDocumentCommentForRelatedJournalEntry($journalEntrys);

            if(sizeof($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']) > 0){
                $loo->totalCreditAmount = floatval($loo->totalCreditAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['credit']);
                $loo->totalDebitAmount = floatval($loo->totalDebitAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['debit']);
            }

            $tempAll[$loo->financeAccountsName] = $loo;
        }
    	ksort($tempAll);
        
    	foreach ($tempAll as $value) {
        	if ($value->totalCreditAmount != 0 || $value->totalDebitAmount != 0) {
            	$all[$value->financeAccountsID] = array("data" => $value);
        	}
    	}


        foreach ($all as $key => &$loo) {
            if (isset($loo['data'])) {
                $all[$loo['data']->financeAccountsParentID]['child'][$loo['data']->financeAccountsID] = $loo;
                unset($all[$key]);
            }
        }

        return $all;
    }


    public function getHierarchyByAccountClassIDsForTrialBalanceSheet($accountClassID, $startDate = null, $endDate = null, $activeFlag = false, $dimensionType = null, $dimensionValue = null, $locationIDs = null, $sepLocBal = false)
    {

        $res = $this->getModel('FinanceAccountsTable')->fetchAllHierarchicalByAccountClassID($accountClassID, $activeFlag);
        $all = array();

        foreach ($res as $loo) {
            $loo->totalCreditAmount = 0.00;
            $loo->totalDebitAmount = 0.00;

            $data = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountIDForTrialBalance($loo->financeAccountsID, $startDate, $endDate, $yearClosing = true, $dimensionType, $dimensionValue, $locationIDs);

            if($data['journalEntryID'] != NULL){
                $loo->totalCreditAmount = $data['totalCreditAmount'];
                $loo->totalDebitAmount = $data['totalDebitAmount'];

            }
            $all[$loo->financeAccountsID] = array("data" => $loo);
        }

        foreach ($all as $key => &$loo) {
            if (isset($loo['data'])) {
                $all[$loo['data']->financeAccountsParentID]['child'][$loo['data']->financeAccountsID] = $loo;
                unset($all[$key]);
            }
        }

        return $all;
    }


    public function getHierarchyByAccountClassIDsForBalanceSheet($accountClassID, $startDate = null, $endDate = null, $activeFlag = false, $dimensionType = null, $dimensionValue = null)
    {

        $res = $this->getModel('FinanceAccountsTable')->fetchAllHierarchicalByAccountClassID($accountClassID, $activeFlag);
        $all = array();
        $tempAll = array();

        foreach ($res as $loo) {
            $loo->totalCreditAmount = 0.00;
            $loo->totalDebitAmount = 0.00;

            $data = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountIDForBalanceSheetPdfAndCsv($loo->financeAccountsID, $startDate, $endDate, $yearClosing = true, $dimensionType, $dimensionValue);

            if($data['journalEntryID'] != NULL){
                $loo->totalCreditAmount = $data['totalCreditAmount'];
                $loo->totalDebitAmount = $data['totalDebitAmount'];

            }
            //get journal Entry details
            $journalEntrys = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRangeForBalanceSheetPdfAndCsv($loo->financeAccountsID, $startDate, $endDate, $dimensionType, $dimensionValue);
            //get duplicated credit and debit value for this account
            $jEntriesAndDuplicatedCreditDebitValue = $this->getDocumentCommentForRelatedJournalEntry($journalEntrys);

            if(sizeof($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']) > 0){
                $loo->totalCreditAmount = floatval($loo->totalCreditAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['credit']);
                $loo->totalDebitAmount = floatval($loo->totalDebitAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['debit']);
            }

        	$tempAll[$loo->financeAccountsName] = $loo;
        }
    	ksort($tempAll);
        
    	foreach ($tempAll as $value) {
    		$runningBalance = $value->totalCreditAmount - $value->totalDebitAmount;
        	if ($runningBalance != 0) {
            	$all[$value->financeAccountsID] = array("data" => $value);
        	}
    	}


        foreach ($all as $key => &$loo) {
            if (isset($loo['data'])) {
                $all[$loo['data']->financeAccountsParentID]['child'][$loo['data']->financeAccountsID] = $loo;
                unset($all[$key]);
            }
        }

        return $all;
    }



    public function getHierarchyByAccountClassIDsForBalanceSheetView($accountClassID, $startDate = null, $endDate = null, $activeFlag = false, $dimensionType, $dimensionValue, $locationIDs = null, $sepLocBal = false)
    {

        $res = $this->getModel('FinanceAccountsTable')->fetchAllHierarchicalByAccountClassID($accountClassID, $activeFlag);
        $all = array();
        $tempAll = array();
		
		foreach ($res as $loo) {

			if ($locationIDs != NULL && $sepLocBal) {
				foreach ($locationIDs as $key1 => $loc) {
					# code...
		            $loo->totalCreditAmount[$loc['locationCode'].'-'.$loc['locationID']] = 0.00;
		            $loo->totalDebitAmount[$loc['locationCode'].'-'.$loc['locationID']] = 0.00;

		            $data = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountIDForBalanceSheet($loo->financeAccountsID, $startDate, $endDate, $yearClosing = true, $dimensionType, $dimensionValue, $loc['locationID']);
		            
		            if($data['journalEntryID'] != NULL){
		                $loo->totalCreditAmount[$loc['locationCode'].'-'.$loc['locationID']] = $data['totalCreditAmount'];
		                $loo->totalDebitAmount[$loc['locationCode'].'-'.$loc['locationID']] = $data['totalDebitAmount'];

		            }
				}
			} else {
				$loo->totalCreditAmount = 0.00;
	            $loo->totalDebitAmount = 0.00;

	            $data = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountIDForBalanceSheet($loo->financeAccountsID, $startDate, $endDate, $yearClosing = true, $dimensionType, $dimensionValue, $locationIDs);
	            
	            if($data['journalEntryID'] != NULL){
	                $loo->totalCreditAmount = $data['totalCreditAmount'];
	                $loo->totalDebitAmount = $data['totalDebitAmount'];

	            }
			}




        	// $tempAll[$loo->financeAccountsName] = $loo;
        	$all[$loo->financeAccountsID] = array("data" => $loo);
        }
    	// ksort($tempAll);
        
    	// foreach ($tempAll as $value) {
    	// 	$runningBalance = $value->totalCreditAmount - $value->totalDebitAmount;
     //    	if ($runningBalance != 0) {
     //        	$all[$value->financeAccountsID] = array("data" => $value);
     //    	}
    	// }

        foreach ($all as $key => &$loo) {
            if (isset($loo['data'])) {
                $all[$loo['data']->financeAccountsParentID]['child'][$loo['data']->financeAccountsID] = $loo;
                unset($all[$key]);
            }
        }

        return $all;
    }


    public function getDocumentCommentForRelatedJournalEntry($jEntries)
    {
        $returnJEntity = [];
        $removedCreditDebit = [];
        foreach ($jEntries as $key => $singleValue) {
            $updated = true;
            switch ($singleValue['documentTypeID']){
                case '1' :
                    $invoiceDetails = $this->getModel('InvoiceTable')->getInvoiceByInvoiceIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$invoiceDetails['salesInvoiceID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        $singleValue['relatedDocComment'] = $invoiceDetails['salesInvoiceComment'];
                        $singleValue['documentType'] = 'Sales Invoice';
                        $singleValue['documentCode'] = $invoiceDetails['salesInvoiceCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $invoiceDetails['customerCode'].' - '.$invoiceDetails['customerName'];
                    }
                    break;
                case '4' :
                    $deliveryNoteDetails = $this->getModel('DeliveryNoteTable')->getDeliveryNoteByIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$deliveryNoteDetails['deliveryNoteID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {


                        $singleValue['relatedDocComment'] = $deliveryNoteDetails['deliveryNoteComment'];
                        $singleValue['documentType'] = 'Delivery Note';
                        $singleValue['documentCode'] = $deliveryNoteDetails['deliveryNoteCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $deliveryNoteDetails['customerCode'].' - '.$deliveryNoteDetails['customerName'];
                    }
                    break;
                case '7' :
                    $paymentDetails = $this->getModel('PaymentsTable')->getPaymentsByPaymentIDForJE($singleValue['journalEntryDocumentID']);
                    if(sizeof($paymentDetails) == 0){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        $paymentDet = $this->createPayDataSet($paymentDetails);

                        $singleValue['relatedDocComment'] = $paymentDet['docComment'];
                        $singleValue['documentType'] = 'Sales Payment';
                        $singleValue['documentCode'] = $paymentDet['docCode'];
                        $singleValue['childDoc'] = $paymentDet['childDoc'];
                        $singleValue['cusName'] = $paymentDet['cusName'];
                    }

                    break;
                case '5' :
                    $returnDetails = $this->getModel('SalesReturnsTable')->getReturnforSearchForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$returnDetails['salesReturnID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {

                        $singleValue['relatedDocComment'] = $returnDetails['salesReturnComment'];
                        $singleValue['documentType'] = 'Sales Return';
                        $singleValue['documentCode'] = $returnDetails['salesReturnCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $returnDetails['customerCode'].' - '.$returnDetails['customerName'];
                    }
                    break;
                case '6' :
                    $creditNoteDetails = $this->getModel('CreditNoteTable')->getCreditNoteByCreditNoteIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$creditNoteDetails['creditNoteID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {

                        $singleValue['relatedDocComment'] = $creditNoteDetails['creditNoteComment'];
                        $singleValue['documentType'] = 'Credit Note';
                        $singleValue['documentCode'] = $creditNoteDetails['creditNoteCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $creditNoteDetails['customerCode'].' - '.$creditNoteDetails['customerName'];
                    }
                    break;
                case '17' :
                    $creditNotePayment = $this->getModel('CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentIDForJE($singleValue['journalEntryDocumentID'], NULL)->current();
                    if(!$creditNotePayment['creditNotePaymentID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {

                        $singleValue['relatedDocComment'] = $creditNotePayment['creditNotePaymentMemo'];
                        $singleValue['documentType'] = 'Credit Note Payment';
                        $singleValue['documentCode'] = $creditNotePayment['creditNotePaymentCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $creditNotePayment['customerCode'].' - '.$creditNotePayment['customerName'];
                    }
                    break;
                case '12' :
                    $piDetails = $this->getModel('PurchaseInvoiceTable')->getPurchaseInvoiceByIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$piDetails['purchaseInvoiceID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {

                        $singleValue['relatedDocComment'] = $piDetails['purchaseInvoiceComment'];
                        $singleValue['documentType'] = 'Purchase Invoice';
                        $singleValue['documentCode'] = $piDetails['purchaseInvoiceCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $piDetails['supplierCode'].' - '.$piDetails['supplierName'];
                    }
                    break;
                case '14' :
                    $suppPaymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentsByPaymentIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$suppPaymentDetails['outgoingPaymentID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {

                        $singleValue['relatedDocComment'] = $suppPaymentDetails['outgoingPaymentMemo'];
                        $singleValue['documentType'] = 'Supplier Payment';
                        $singleValue['documentCode'] = $suppPaymentDetails['outgoingPaymentCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $suppPaymentDetails['supplierCode'].' - '.$suppPaymentDetails['supplierName'];
                    }
                    break;
                case '11' :
                    $prDetails = $this->getModel('PurchaseReturnTable')->getPurchaseReturnByIdForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$prDetails['purchaseReturnID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                    	if ($prDetails['directReturnFlag'] == "1") {
                    		$directPrDetails = $this->getModel('PurchaseReturnTable')->getDirectPurchaseReturnById($singleValue['journalEntryDocumentID'])->current();
                        	$singleValue['cusName'] = $directPrDetails['supplierCode'].' - '.$directPrDetails['supplierName'];
                    	} else {
                        	$singleValue['cusName'] = $prDetails['supplierCode'].' - '.$prDetails['supplierName'];
                    	}
                        $singleValue['relatedDocComment'] = $prDetails['purchaseReturnComment'];
                        $singleValue['documentType'] = 'Purchase Return';
                        $singleValue['documentCode'] = $prDetails['purchaseReturnCode'];
                        $singleValue['childDoc'] = '-';
                    }
                    break;
                case '13' :
                    $debitNoteDetails = $this->getModel('DebitNoteTable')->getDebitNoteByDebitNoteIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$debitNoteDetails['debitNoteID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {

                        $singleValue['relatedDocComment'] = $debitNoteDetails['debitNoteComment'];
                        $singleValue['documentType'] = 'Debit Note';
                        $singleValue['documentCode'] = $debitNoteDetails['debitNoteCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $debitNoteDetails['supplierCode'].' - '.$debitNoteDetails['supplierName'];
                    }
                    break;
                case '18' :
                    $debitNotePaymentDetails = $this->getModel('DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentIDForJE($singleValue['journalEntryDocumentID'], NULL)->current();
                    if(!$debitNotePaymentDetails['debitNotePaymentID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {

                        $singleValue['relatedDocComment'] = $debitNotePaymentDetails['debitNotePaymentMemo'];
                        $singleValue['documentType'] = 'Debit Note Payment';
                        $singleValue['documentCode'] = $debitNotePaymentDetails['debitNotePaymentCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $debitNotePaymentDetails['supplierCode'].' - '.$debitNotePaymentDetails['supplierName'];
                    }
                    break;
                case '10' :
                    $grnDetails = $this->getModel('GrnTable')->grnDetailsByGrnIdForJE($singleValue['journalEntryDocumentID']);
                    if(!$grnDetails['grnID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {

                        $singleValue['relatedDocComment'] = $grnDetails['grnComment'];
                        $singleValue['documentType'] = 'GRN';
                        $singleValue['documentCode'] = $grnDetails['grnCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $grnDetails['supplierCode'].' - '.$grnDetails['supplierName'];
                    }
                    break;
                case '16' :
                    $adjustmentDetails = $this->getModel('GoodsIssueTable')->getDataForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$adjustmentDetails['goodsIssueID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {

                        $singleValue['relatedDocComment'] = $adjustmentDetails['goodsIssueReason'];
                        $singleValue['documentCode'] = $adjustmentDetails['goodsIssueCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = '-';
                        if($adjustmentDetails['goodsIssueTypeID'] == '2'){
                            $singleValue['documentType'] = 'Positive Adjustment';
                        } else {
                            $singleValue['documentType'] = 'Negative Adjustment';
                        }
                    }
                    break;
                case '20' :
                    $pvDetails = $this->getModel('PaymentVoucherTable')->getPaymentVoucherByPaymentVoucherIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$pvDetails['paymentVoucherID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        $singleValue['relatedDocComment'] = $pvDetails['paymentVoucherComment'];
                        $singleValue['documentType'] = 'Payment Voucher';
                        $singleValue['documentCode'] = $pvDetails['paymentVoucherCode'];
                        $singleValue['childDoc'] = '-';
                        if($pvDetails['paymentVoucherSupplierID'] != ''){
                            $singleValue['cusName'] = $pvDetails['supplierCode'].' - '.$pvDetails['supplierName'];
                        } else {
                            $singleValue['cusName'] = $pvDetails['financeAccountsCode'].' - '.$pvDetails['financeAccountsName'];
                        }

                    }

                    break;
                case '32' :
                    $chequeData = $this->getModel('ChequeDepositTable')->getChequeDepositByChequeDeposiIdForJE($singleValue['journalEntryDocumentID']);
                    if($chequeData['chequeDepositId']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                       $updated = false;
                    }
                    break;
                default :
                    break;
            }
            if($updated){
                $returnJEntity[] = $singleValue;
            }
        }
        $retrunAllData = array(
            'returnJEntity' => $returnJEntity,
            'creditDebitRemovedValues' => $removedCreditDebit,
            );
        return $retrunAllData;
    }

    public function createPayDataSet($paymentDetails)
    {
        $docType = [];
        $returnData = [];
        foreach ($paymentDetails as $key => $value) {

            $returnData['cusName'] = $value['customerCode'].' - '.$value['customerName'];
            $returnData['docCode'] = $value['incomingPaymentCode'];
            $returnData['docComment'] = $value['incomingPaymentMemo'];
            if($value['incomingPaymentMethodChequeNumber'] != null){
                $docType[] = 'cheque No - '. $value['incomingPaymentMethodChequeNumber'];
            } else if($value['incomingPaymentMethodCreditReceiptNumber'] != null){
                $docType[] = 'credit Card ref - '. $value['incomingPaymentMethodCreditReceiptNumber'];
            }
        }
        $returnData['childDoc'] = implode("/", $docType);
        return $returnData;


    }

    public function getNetProfitAmount($profitAndLostData)
    {
        $netProfit = 0.00;
        foreach ($profitAndLostData as $typekey => $typeData) {
            $TotalDebitValue = 0;
            $TotalCreditValue = 0;

            foreach ($typeData as $tbalance) {
                if(isset($tbalance['financeAccounts'][0]['child'])){
                    foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
                        $creditAmount = 0;
                        $debitAmount = 0;
                        if(isset($value['child'])){
                            $amountData = $this->calculateAmountsForProfitAndLost($value['child']);
                            if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                                $TotalCreditValue+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                                $creditAmount = $amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                            } else {
                                $TotalDebitValue+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
                                $debitAmount = $amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
                            }
                        }

                        if($value['data']->totalCreditAmount - $value['data']->totalDebitAmount > 0){
                            $TotalCreditValue+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
                            $creditAmount+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
                        }else{
                            $TotalDebitValue+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
                            $debitAmount+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
                        }
                    }
                }
            }

            if($typekey == 0 || $typekey == 2){
                if($typekey == 0){
                    $revenueAmount = $TotalCreditValue - $TotalDebitValue;
                } else if($typekey == 2){
                    $otherRevenueAmount = $TotalCreditValue - $TotalDebitValue;
                }


            }else if($typekey == 1 || $typekey == 3){
                if($typekey == 1){
                    $costOfSalesAmount = $TotalDebitValue - $TotalCreditValue ;
                }else if($typekey == 3){
                    $expenceAmount = $TotalDebitValue - $TotalCreditValue ;
                }
            }

            if($typekey == 1){
                $grossProfit = $revenueAmount - $costOfSalesAmount;
            }else if($typekey == 3){
                $netProfit = ($revenueAmount - $costOfSalesAmount) +($otherRevenueAmount - $expenceAmount);
            }
        }
        return $netProfit;
    }


    public function calculateAmountsForProfitAndLost($dataArray)
    {
        $creditAmountTotal = 0;
        $debitAmountTotal = 0;
        foreach ($dataArray as $value) {
            if(isset($value['child'])){
                $amountData = $this->calculateAmountsForProfitAndLost($value['child']);
                if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                    $creditAmountTotal+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                }else{
                    $debitAmountTotal+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'] ;
                }
            }
            if($value['data']->totalCreditAmount - $value['data']->totalDebitAmount > 0){
                $creditAmountTotal+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
            }else{
                $debitAmountTotal+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
            }
        }
        return array('totalCreditAmount'=> $creditAmountTotal, 'totalDebitAmount' => $debitAmountTotal);

    }

    public function getTrialBalanceData($startDate, $endDate , $dimensionType = null, $dimensionValue = null, $locationIDs = null, $sepLocBal = false)
    {
    	if(isset($startDate) && isset($endDate)){
    		$fAccountClassData = $this->getModel('FinanceAccountClassTable')->getAllActiveAccountClasses();

            $financeAccounts = array();

            foreach ($fAccountClassData as $key => $value) {
                $financeAccounts[$value['financeAccountClassID']]['financeAccountClassID'] = $value['financeAccountClassID'];
                $financeAccounts[$value['financeAccountClassID']]['financeAccountTypesName'] = $value['financeAccountTypesName'];
                $financeAccounts[$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];
                $fAccountsData = $this->getHierarchyByAccountClassIDsForTrialBalanceSheet($value['financeAccountClassID'],$startDate, $endDate, true, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);
                $financeAccounts[$value['financeAccountClassID']]['financeAccounts'] = $fAccountsData ;
            }
            return $financeAccounts;
        }
    }

    public function trialBalanceSheet($postData)
    {
    	try{

    		$startDate = $postData['startDate'];
	        $endDate = $postData['endDate'];
	        $dimensionType = $postData['dimensionType'];
	        $dimensionValue = $postData['dimensionValue'];

	        $locationIDs = $postData['locationID'];
	        $sepLocBal = ($postData['sepLocBal'] == 'true') ? true: false;
	        $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

	        $netProfitData = [];

	        $locationData = null;


	        if ($sepLocBal) {
	            foreach ($locationIDs as $key => $value) {
	                $location = $this->getModel('LocationTable')->getLocation($value);

	                $locationData[] = [
	                    'locationName' => $location->locationName,
	                    'locationCode' => $location->locationCode,
	                    'locationID' => $location->locationID

	                ];
	            }
	        }     


	        $translator = new Translator();
	        $name = $translator->translate('Trial Balance');
	        $period = $startDate . ' - ' . $endDate;


	        if ($sepLocBal) {
	        	
	        	foreach ($locationData as $key => $value) {
                    
                    $trialBalanceData[$value['locationCode'].'-'.$value['locationID']] = $this->getTrialBalanceData($startDate, $endDate, $dimensionType, $dimensionValue, $value['locationID'], $sepLocBal);
                }

		        $companyDetails = $this->getCompanyDetails();

		    	if ($trialBalanceData) {
	                $title = '';
	                $tit = 'TRIAL BALANCE REPORT';
	                $in = ["", $tit];
	                $title = implode(",", $in) . "\n";

	                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];

	                $title.=implode(",", $in) . "\n";

	                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
	                $title.=implode(",", $in) . "\n";

	                $in = ["Period : " . $startDate . '-' . $endDate];
	                $title.=implode(",", $in) . "\n";

	                $arrs = ["LOCATION", "ACCOUNT NAME", "ACCOUNT CODE", "ACCOUNT CLASS", "ACCOUNT TYPE", "DEBIT", "CREDIT"];
	                $header = implode(",", $arrs);
	                $arr = '';

	                // $TotalCreditValue = 0;
	                // $TotalDebitValue = 0;



	               	foreach ($locationData as $key5 => $value5) {
			            $in = [
			                0 => $value5['locationName'],
			                1 => '',
			                2 => '',
			                3 => '',
			                4 => '',
			                5 => '',
			                6 => '',
		                ];

		                $arr.=implode(",", $in) . "\n";

						$TotalCreditValue = 0;
						$TotalDebitValue = 0;
						$locKey5 = $value5['locationCode'].'-'.$value5['locationID'];

	                    foreach ($trialBalanceData[$locKey5] as $key => $tbalance) {
	                        if(isset($tbalance['financeAccounts'][0]['child'])){
	                            foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
	                                $amountData = [];
	                                if(isset($value['child'])){
	                                    $amountData = $this->calculateAmounts($value['child']);
	                                }
	                                $viewDebit = 0;
	                                $viewCredit = 0;
	                                $creditAmount = ($amountData['totalCreditAmount'] != 0)? (float) ($amountData['totalCreditAmount']+$value['data']->totalCreditAmount ): (float) $value['data']->totalCreditAmount;
	                                $debitAmount = ($amountData['totalDebitAmount'] != 0)? (float) ($amountData['totalDebitAmount']+$value['data']->totalDebitAmount ): (float) $value['data']->totalDebitAmount;
	                                $debitCreditDef = floatval($debitAmount) - floatval($creditAmount);
	                                if($debitCreditDef < 0 ){
	                                    $viewCredit = $debitCreditDef*(-1);
	                                	$TotalCreditValue += floatval($debitCreditDef)*(-1);
	                                } else {
	                                	$viewDebit = $debitCreditDef;
	                            		$TotalDebitValue += floatval($debitCreditDef);
	                                }

	                                if ($hideZeroVal) {
	                                	if ($viewDebit != 0 || $viewCredit != 0) {
			                                $in = ["",preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName), $value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $tbalance['financeAccountClassName']), preg_replace("/(,|;)/", " ", $tbalance['financeAccountTypesName']), $viewDebit, $viewCredit];
			                                $arr.=implode(",", $in) . "\n";
		                                }
	                                } else {
	                                	$in = ["",preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName), $value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $tbalance['financeAccountClassName']), preg_replace("/(,|;)/", " ", $tbalance['financeAccountTypesName']), $viewDebit, $viewCredit];
			                            $arr.=implode(",", $in) . "\n";
	                                }

	                                if(isset($value['child'])){
	                                    $level = 1;
	                                   $arr.= $this->getChildAccountsForLocsForTrial($value['child'],$tbalance,$level,'trialBalance', null, $hideZeroVal);
	                                }

	                            }
	                        }
	                    }
	                    $in = ["","Total", "", "", "",floatval($TotalDebitValue),floatval($TotalCreditValue)];
	                    $arr.=implode(",", $in) . "\n";


	                    $in = ["","", "", "", "","",""];
	                    $arr.=implode(",", $in) . "\n";
	                }
	            } else {
	                $arr = "no matching records found\n";
	            }

	            $name = "Trial_Balance_Report.csv";
	            $csvContent = $this->csvContent($title, $header, $arr);
	            $csvPath = $this->generateCSVFile($name, $csvContent);

	            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');

	        } else {
	        	$trialBalanceData = $this->getTrialBalanceData($startDate, $endDate, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);
		        $companyDetails = $this->getCompanyDetails();

		    	if ($trialBalanceData) {
	                $title = '';
	                $tit = 'TRIAL BALANCE REPORT';
	                $in = ["", $tit];
	                $title = implode(",", $in) . "\n";

	                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];

	                $title.=implode(",", $in) . "\n";

	                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
	                $title.=implode(",", $in) . "\n";

	                $in = ["Period : " . $startDate . '-' . $endDate];
	                $title.=implode(",", $in) . "\n";

	                $arrs = ["ACCOUNT NAME", "ACCOUNT CODE", "ACCOUNT CLASS", "ACCOUNT TYPE", "DEBIT", "CREDIT"];
	                $header = implode(",", $arrs);
	                $arr = '';

	                $TotalCreditValue = 0;
	                $TotalDebitValue = 0;

                    foreach ($trialBalanceData as $key => $tbalance) {
                        if(isset($tbalance['financeAccounts'][0]['child'])){
                            foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
                                $amountData = [];
                                if(isset($value['child'])){
                                    $amountData = $this->calculateAmounts($value['child']);
                                }
                                $viewDebit = 0;
                                $viewCredit = 0;
                                $creditAmount = ($amountData['totalCreditAmount'] != 0)? (float) ($amountData['totalCreditAmount']+$value['data']->totalCreditAmount ): (float) $value['data']->totalCreditAmount;
                                $debitAmount = ($amountData['totalDebitAmount'] != 0)? (float) ($amountData['totalDebitAmount']+$value['data']->totalDebitAmount ): (float) $value['data']->totalDebitAmount;
                                $debitCreditDef = floatval($debitAmount) - floatval($creditAmount);
                                if($debitCreditDef < 0 ){
                                    $viewCredit = $debitCreditDef*(-1);
                                	$TotalCreditValue += floatval($debitCreditDef)*(-1);
                                } else {
                                	$viewDebit = $debitCreditDef;
                            		$TotalDebitValue += floatval($debitCreditDef);
                                }


                                if ($hideZeroVal) {
                                	if ($viewDebit != 0 || $viewCredit != 0) {
		                                $in = [preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName), $value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $tbalance['financeAccountClassName']), preg_replace("/(,|;)/", " ", $tbalance['financeAccountTypesName']), $viewDebit, $viewCredit];
		                                $arr.=implode(",", $in) . "\n";
	                                }
                                } else {
                                	$in = [preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName), $value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $tbalance['financeAccountClassName']), preg_replace("/(,|;)/", " ", $tbalance['financeAccountTypesName']), $viewDebit, $viewCredit];
		                            $arr.=implode(",", $in) . "\n";
                                }
                                

                                if(isset($value['child'])){
                                    $level = 1;
                                   $arr.= $this->getChildAccounts($value['child'],$tbalance,$level,'trialBalance', null,$hideZeroVal);
                                }

                            }
                        }
                    }
                    $in = ["Total", "", "", "",floatval($TotalDebitValue),floatval($TotalCreditValue)];
                    $arr.=implode(",", $in) . "\n";
	            } else {
	                $arr = "no matching records found\n";
	            }

	            $name = "Trial_Balance_Report.csv";
	            $csvContent = $this->csvContent($title, $header, $arr);
	            $csvPath = $this->generateCSVFile($name, $csvContent);

	            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');
		    }

	   	} catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    public function trialBalancePdf($postData)
    {
    	try{
	        $startDate = $postData['startDate'];
	        $endDate = $postData['endDate'];
	        $dimensionType = $postData['dimensionType'];
	        $dimensionValue = $postData['dimensionValue'];

	        $locationIDs = $postData['locationID'];
	        $sepLocBal = ($postData['sepLocBal'] == 'true') ? true: false;
	        $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

	        $netProfitData = [];

	        $locationData = null;


	        if ($sepLocBal) {
	            foreach ($locationIDs as $key => $value) {
	                $location = $this->getModel('LocationTable')->getLocation($value);

	                $locationData[] = [
	                    'locationName' => $location->locationName,
	                    'locationCode' => $location->locationCode,
	                    'locationID' => $location->locationID

	                ];
	            }
	        }     

	        $translator = new Translator();
	        $name = $translator->translate('Trial Balance');
	        $period = $startDate . ' - ' . $endDate;


	        if ($sepLocBal) {
                foreach ($locationData as $key => $value) {
                    
                    $trialBalanceData[$value['locationCode'].'-'.$value['locationID']] = $this->getTrialBalanceData($startDate, $endDate, $dimensionType, $dimensionValue, $value['locationID'], $sepLocBal);
                }

            } else {
                $trialBalanceData = $this->getTrialBalanceData($startDate, $endDate, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);

            }


	        $companyDetails = $this->getCompanyDetails();

	        $headerView = $this->headerViewTemplate($name, $period);
	        $headerView->setTemplate('reporting/template/headerTemplate');
	        $headerViewRender = $this->htmlRender($headerView);

	        $trialBalanceView = new ViewModel(array(
	            'trialBalanceData' => $trialBalanceData,
                'locationIDs' => $locationIDs,
                'sepLocBal' => $sepLocBal,
                'hideZeroVal' => $hideZeroVal,
                'locationData' => $locationData,
	            'startDate' => $startDate,
	            'endDate' => $endDate,
	            'headerTemplate' => $headerViewRender,
	            'cD' => $companyDetails)
	        );

	        $trialBalanceView->setTemplate('reporting/finance-report/generate-trial-balance-pdf');
	        $trialBalanceView->setTerminal(true);

	        // get rendered view into variable
	        $htmlContent = $this->viewRendererHtmlContent($trialBalanceView);
	        $pdfPath = $this->downloadPDF('trial-balance', $htmlContent);

	        return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

	    } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing pdf data.');
        }
    }

    public function profitAndLossSheet($postData)
    {
    	try{

    		$startDate = $postData['startDate'];
            $endDate = $postData['endDate'];
            $dimensionType = $postData['dimensionType'];
	        $dimensionValue = $postData['dimensionValue'];

	        $locationIDs = $postData['locationID'];
        	$sepLocBal = ($postData['sepLocBal'] == 'true') ? true: false;
        	$hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

        	$locationData = null;


	        if ($sepLocBal) {
	            foreach ($locationIDs as $key => $value) {
	                $location = $this->getModel('LocationTable')->getLocation($value);

	                $locationData[] = [
	                    'locationName' => $location->locationName,
	                    'locationCode' => $location->locationCode,
	                    'locationID' => $location->locationID

	                ];
	            }
	        } 


	        if ($sepLocBal) {

                $translator = new Translator();
	            $name = $translator->translate('Profit And Loss');
	            $period = $startDate . ' - ' . $endDate;

                $array = $this->getBalanceSheetDataForViewReport($startDate,$endDate, $dimensionType, $dimensionValue, $locationData, $sepLocBal);
	            $profitAndLostData = [];
	            foreach ($array as $key => $value) {
	                if($value['financeAccountTypesID'] == 4){
	                    $profitAndLostData[0] = $value;
	                } else if( $value['financeAccountTypesID'] == 6){
	                    $profitAndLostData[1] = $value;
	                } else if( $value['financeAccountTypesID'] ==  7){
	                    $profitAndLostData[2] = $value;
	                } else if( $value['financeAccountTypesID'] ==  3){
	                    $profitAndLostData[3] = $value;
	                }
	            }

	            ksort($profitAndLostData);
	            $cD = $this->getCompanyDetails();

		    	if ($profitAndLostData) {
		            $title = '';
		            $tit = 'PROFIT AND LOSS REPORT';
		            $in = ["", $tit];
		            $title = implode(",", $in) . "\n";

		            $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];

		            $title.=implode(",", $in) . "\n";

		            $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
		            $title.=implode(",", $in) . "\n";

		            $in = ["Period : " . $startDate . '-' . $endDate];
		            $title.=implode(",", $in) . "\n";

		            $arr = '';

		            foreach ($profitAndLostData as $typekey => $typeData) {
		                $TotalDebitValue = [];
		                $TotalCreditValue = [];

		                $arr.= implode(",", []). "\n";

		                $in = ["ACCOUNT CODE", "ACCOUNT NAME"];

		                $headerLen = sizeof($in);
                        foreach ($locationData as $key99 => $value99) {
                        	$in[$headerLen] = strtoupper($value99['locationName'].' '.'Balance');
                        	$headerLen++;
                        }


		                $arr.= implode(",", $in). "\n";

		                foreach ($typeData as $tbalance) {
		                    $accClassSubTotal = [];
		                    if(isset($tbalance['financeAccounts'][0]['child'])){
		                        $in = ["", preg_replace("/(,|;)/", "", $tbalance['financeAccountClassName']), ""];
		                        $arr.= implode(",", $in). "\n";


		                        foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
		                        	$emptyVal = 0;
	                                    
                                	foreach ($locationData as $key22 => $value22) {
										
										$locKey = $value22['locationCode'].'-'.$value22['locationID'];
										$amountData = [];
										if(isset($value['child'])){
											$amountData[$locKey] = $this->calculateAmountsLocWise($value['child'], $locKey);
											$TotalDebitValue[$locKey]+=$amountData[$locKey]['totalDebitAmount'];
											$TotalCreditValue[$locKey]+=$amountData[$locKey]['totalCreditAmount'];
											if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
												$libilityPlEquityCreditAmount[$locKey]+=$amountData[$locKey]['totalCreditAmount'];
												$libilityPlEquityDebitAmount[$locKey]+=$amountData[$locKey]['totalDebitAmount'];
											}
										}
										if($typeData['financeAccountTypesID'] == 5 || $typeData['financeAccountTypesID'] == 2){
											$libilityPlEquityCreditAmount[$locKey]+=$value['data']->totalCreditAmount[$locKey];
											$libilityPlEquityDebitAmount[$locKey]+=$value['data']->totalDebitAmount[$locKey];
										}
										$TotalDebitValue[$locKey]+=$value['data']->totalDebitAmount[$locKey];
										$TotalCreditValue[$locKey]+=$value['data']->totalCreditAmount[$locKey];



										$createdCredit[$locKey] = ($amountData[$locKey]['totalCreditAmount'] != 0)? floatval($amountData[$locKey]['totalCreditAmount'])+ floatval($value['data']->totalCreditAmount[$locKey]) : floatval($value['data']->totalCreditAmount[$locKey]);
										$createdDebit[$locKey] = ($amountData[$locKey]['totalDebitAmount'] != 0)? floatval($amountData[$locKey]['totalDebitAmount'])+ floatval($value['data']->totalDebitAmount[$locKey]) : floatval($value['data']->totalDebitAmount[$locKey]);
										if($typekey == 0 || $typekey == 2){
											$balance[$value['data']->financeAccountsID][$locKey] = floatval($createdCredit[$locKey]) - floatval($createdDebit[$locKey]);
										}
										else if($typekey == 1 || $typekey == 3){
											$balance[$value['data']->financeAccountsID][$locKey] = floatval($createdDebit[$locKey]) - floatval($createdCredit[$locKey]);
										}
										$accClassSubTotal[$tbalance['financeAccountClassID']][$locKey] += floatval($balance[$value['data']->financeAccountsID][$locKey]);

										if($balance[$value['data']->financeAccountsID][$locKey] < 0){
											$viewBalnce[$locKey] = '('. floatval(($balance[$value['data']->financeAccountsID][$locKey] * -1)) .')';
										} else {
											$viewBalnce[$locKey] = floatval($balance[$value['data']->financeAccountsID][$locKey]);
										}


										if ($balance[$value['data']->financeAccountsID][$locKey] == 0) {
											$emptyVal++;
										}


									}


									if ($hideZeroVal) {
										if ($emptyVal != sizeof($locationData)) {
											$in = [$value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName)];

		                                    $tt = 2;
					                        foreach ($locationData as $key88 => $value88) {
					                        	$yy = $value88['locationCode'].'-'.$value88['locationID'];
					                        	$in[$tt] = $viewBalnce[$yy];
					                        	$tt++;
					                        }

		                                    $arr.=implode(",", $in) . "\n";
										}

									} else {
										$in = [$value['data']->financeAccountsCode, preg_replace("/(,|;)/", " ", $value['data']->financeAccountsName)];

	                                    $tt = 2;
				                        foreach ($locationData as $key88 => $value88) {
				                        	$yy = $value88['locationCode'].'-'.$value88['locationID'];
				                        	$in[$tt] = $viewBalnce[$yy];
				                        	$tt++;
				                        }

	                                    $arr.=implode(",", $in) . "\n";
									}
                                    

                                    if(isset($value['child'])){
                                        $level = 1;
                                        $arr.= $this->getChildAccountsLocationWise($value['child'],$tbalance,$level,"balanceSheet", $typeData['financeAccountTypesID'], $locationData, $hideZeroVal);
                                    }
                                }


                                $in = ["sub Total", ""];
                                $subSize = 2;
                                
                                foreach ($locationData as $key77 => $value77) {
		                        	$cc = $value77['locationCode'].'-'.$value77['locationID'];
	                               
		                        	$in[$subSize] = ($accClassSubTotal[$tbalance['financeAccountClassID']][$cc] < 0) ? '('.floatval(($accClassSubTotal[$tbalance['financeAccountClassID']][$cc] * -1)).')' : floatval($accClassSubTotal[$tbalance['financeAccountClassID']][$cc]);
		                        	$subSize++;
		                        }


                                $arr.=implode(",", $in) . "\n";
                                $in = ["", "", ""];
                                    $arr.=implode(",", $in) . "\n";

		                    }
		                }
		                $arr.= implode(",", []). "\n";

		                if($typekey == 0 || $typekey == 2){

		                    $in = [preg_replace("/(,|;)/", "", $typeData['financeAccountTypesName']), ""];
		                    $typeCount = 2;
		                	foreach ($locationData as $key50a => $value50a) {
		                		$loc50a = $value50a['locationCode'].'-'.$value50a['locationID'];
			                    if($typekey == 0){
			                        $revenueAmount[$loc50a] = $TotalCreditValue[$loc50a] - $TotalDebitValue[$loc50a];
			                    } else if($typekey == 2){
			                        $otherRevenueAmount[$loc50a] = $TotalCreditValue[$loc50a] - $TotalDebitValue[$loc50a];
			                    }


			                    $value1[$loc50a] = (float)($TotalCreditValue[$loc50a] - $TotalDebitValue[$loc50a]);
			                    if($value1[$loc50a] < 0 ){
			                        $value1[$loc50a] = '('.$value1[$loc50a]*(-1).')';
			                    }
			                    $in[$typeCount] = $value1[$loc50a];
			                    $typeCount++;
		                	}

		                    $arr.=implode(",", $in) . "\n";

		                }else if($typekey == 1 || $typekey == 3){
		                    $in = [preg_replace("/(,|;)/", "", $typeData['financeAccountTypesName']), ""];

		                    $typeCount = 2;
		                    foreach ($locationData as $key50b => $value50b) {
		                		$loc50b = $value50b['locationCode'].'-'.$value50b['locationID'];

		                		if($typekey == 1){
			                        $costOfSalesAmount[$loc50b] = $TotalDebitValue[$loc50b] - $TotalCreditValue[$loc50b] ;
			                    }else if($typekey == 3){
			                        $expenceAmount[$loc50b] = $TotalDebitValue[$loc50b] - $TotalCreditValue[$loc50b] ;
			                    }

			                    $value2[$loc50b] = (float)($TotalDebitValue[$loc50b] - $TotalCreditValue[$loc50b]);
			                    if($value2[$loc50b] < 0 ){
			                        $value2[$loc50b] = '('.$value2[$loc50b]*(-1).')';
			                    }

			                    
			                    $in[$typeCount] = $value2[$loc50b];
			                    $typeCount++;
		                	}

		                    $arr.=implode(",", $in) . "\n";
		                }

		                if($typekey == 1){
		                    $arr.=implode(",", []) . "\n";

		                    $in = ['Gross Profit', ""];
		                    $grossCount = 2;

		                    foreach ($locationData as $key50c => $value50c) {
		                    	$loc50c = $value50c['locationCode'].'-'.$value50c['locationID'];
			                    $grossProfit[$loc50c] = $revenueAmount[$loc50c] - $costOfSalesAmount[$loc50c];
			                    if($grossProfit[$loc50c] < 0){
			                        $grossProfit[$loc50c] = '('. $grossProfit[$loc50c]*(-1).')';
			                    }

			                    $in[$grossCount] = $grossProfit[$loc50c];
			                    $grossCount++;
		                    }

		                    $arr.=implode(",", $in) . "\n";
		                    $arr.=implode(",", []) . "\n";
		                }else if($typekey == 3){
		                    $arr.=implode(",", []) . "\n";
		                    $in = ['Net Profit', ""];
		                    $netCount = 2;
		                    foreach ($locationData as $key50d => $value50d) {
		                    	$loc50d = $value50d['locationCode'].'-'.$value50d['locationID'];
		                    	$netProfit[$loc50d] = ($revenueAmount[$loc50d] - $costOfSalesAmount[$loc50d]) +($otherRevenueAmount[$loc50d] - $expenceAmount[$loc50d]);
			                    if($netProfit[$loc50d] < 0){
			                        $netProfit[$loc50d] = '('.$netProfit[$loc50d]*(-1).')';
			                    }

			                    $in[$netCount] = $netProfit[$loc50d];
			                    $netCount++;
		                    }

		                    $arr.=implode(",", $in) . "\n";
		                    $arr.=implode(",", []) . "\n";
		                }
		            }
		        } else {
		            $arr = "no matching records found\n";
		        }

		        $name = "Statement_of_income_and_expenditure.csv";
		        $csvContent = $this->csvContent($title, $header, $arr);
		        $csvPath = $this->generateCSVFile($name, $csvContent);

				return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');

            } else {


                $translator = new Translator();
	            $name = $translator->translate('Profit And Loss');
	            $period = $startDate . ' - ' . $endDate;

                $array = $this->getBalanceSheetDataForViewReport($startDate,$endDate, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);
	            $profitAndLostData = [];
	            foreach ($array as $key => $value) {
	                if($value['financeAccountTypesID'] == 4){
	                    $profitAndLostData[0] = $value;
	                } else if( $value['financeAccountTypesID'] == 6){
	                    $profitAndLostData[1] = $value;
	                } else if( $value['financeAccountTypesID'] ==  7){
	                    $profitAndLostData[2] = $value;
	                } else if( $value['financeAccountTypesID'] ==  3){
	                    $profitAndLostData[3] = $value;
	                }
	            }

	            ksort($profitAndLostData);
	            $cD = $this->getCompanyDetails();

		    	if ($profitAndLostData) {
		            $title = '';
		            $tit = 'PROFIT AND LOSS REPORT';
		            $in = ["", $tit];
		            $title = implode(",", $in) . "\n";

		            $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];

		            $title.=implode(",", $in) . "\n";

		            $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
		            $title.=implode(",", $in) . "\n";

		            $in = ["Period : " . $startDate . '-' . $endDate];
		            $title.=implode(",", $in) . "\n";

		            $arr = '';

		            foreach ($profitAndLostData as $typekey => $typeData) {
		                $TotalDebitValue = 0;
		                $TotalCreditValue = 0;

		                $arr.= implode(",", []). "\n";

		                $in = ["ACCOUNT CODE", "ACCOUNT NAME", "BALANCE"];
		                $arr.= implode(",", $in). "\n";

		                foreach ($typeData as $tbalance) {
		                    $subTotalForAcc = 0;
		                    if(isset($tbalance['financeAccounts'][0]['child'])){
		                        $in = ["", preg_replace("/(,|;)/", "", $tbalance['financeAccountClassName']), ""];
		                        $arr.= implode(",", $in). "\n";
		                        foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
		                            $creditAmount = 0;
		                            $debitAmount = 0;
		                            $subAccountCreditAmount = 0;
		                            $subAccountDebitAmount = 0;
		                            if(isset($value['child'])){
		                                $amountData = $this->calculateAmountsForProfitAndLost($value['child']);
		                                $subAccountCreditAmount += $amountData['totalCreditAmount'];
		                                $subAccountDebitAmount += $amountData['totalDebitAmount'];
		                                if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
		                                    $TotalCreditValue+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
		                                    $creditAmount = $amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
		                                } else {
		                                    $TotalDebitValue+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
		                                    $debitAmount = $amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
		                                }
		                            }

		                            if($value['data']->totalCreditAmount - $value['data']->totalDebitAmount > 0){
		                                $TotalCreditValue+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
		                                $creditAmount+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
		                            }else{
		                                $TotalDebitValue+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
		                                $debitAmount+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
		                            }

		                            $arrCreditAmount = ($creditAmount - $debitAmount > 0)? (float) ($creditAmount - $debitAmount) : (float) 0.00;
		                            $arrDebitAmount = ($debitAmount - $creditAmount > 0)? (float) ($debitAmount - $creditAmount)  : (float) 0.00;

		                            $totalCDvalue = 0;
		                            if($typekey == 0 || $typekey == 2){
		                                $subAccountTotal = $subAccountCreditAmount - $subAccountDebitAmount;
		                                $totalCDvalue = $value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
		                                $totalCDvalue += $subAccountTotal;
		                            } else if($typekey == 1 || $typekey == 3){
		                                $totalCDvalue = $value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
		                                $subAccountTotal = $subAccountDebitAmount - $subAccountCreditAmount;
		                                $totalCDvalue += $subAccountTotal;
		                            }
		                            $subTotalForAcc += $totalCDvalue;
		                            if($totalCDvalue < 0){
		                                $viewTotalCDValue = '('.number_format($totalCDvalue*(-1), 2, '.', '').')';
		                            } else {
		                                $viewTotalCDValue = number_format($totalCDvalue, 2, '.', '');
		                            }


		                            if ($hideZeroVal) {
		                            	if ($totalCDvalue != 0) {
		                            		$in = [$value['data']->financeAccountsCode, preg_replace("/(,|;)/", "", $value['data']->financeAccountsName), $viewTotalCDValue];
		                            		$arr.=implode(",", $in) . "\n";
		                            	}
		                            } else {
		                            	$in = [$value['data']->financeAccountsCode, preg_replace("/(,|;)/", "", $value['data']->financeAccountsName), $viewTotalCDValue];
		                            	$arr.=implode(",", $in) . "\n";
		                            }

		                           
		                            if(isset($value['child'])){
		                                $level = 1;
		                                $arr.= $this->getChildAccountsForProfitAndLost($value['child'],$tbalance,$level, $typekey, $hideZeroVal);
		                            }
		                        }
		                        $in = ["Sub Total", "", $subTotalForAcc];
		                        $arr.=implode(",", $in) . "\n";

		                        $in = ["", "", ""];
		                        $arr.=implode(",", $in) . "\n";

		                    }
		                }
		                $arr.= implode(",", []). "\n";

		                if($typekey == 0 || $typekey == 2){
		                    if($typekey == 0){
		                        $revenueAmount = $TotalCreditValue - $TotalDebitValue;
		                    } else if($typekey == 2){
		                        $otherRevenueAmount = $TotalCreditValue - $TotalDebitValue;
		                    }

		                    $value1 = (float)($TotalCreditValue - $TotalDebitValue);
		                    if($value1 < 0 ){
		                        $value1 = '('.$value1*(-1).')';
		                    }
		                    $in = [preg_replace("/(,|;)/", "", $typeData['financeAccountTypesName']), "", $value1];
		                    $arr.=implode(",", $in) . "\n";

		                }else if($typekey == 1 || $typekey == 3){
		                    if($typekey == 1){
		                        $costOfSalesAmount = $TotalDebitValue - $TotalCreditValue ;
		                    }else if($typekey == 3){
		                        $expenceAmount = $TotalDebitValue - $TotalCreditValue ;
		                    }

		                    $value2 = (float)($TotalDebitValue - $TotalCreditValue);
		                    if($value2 < 0 ){
		                        $value2 = '('.$value2*(-1).')';
		                    }
		                    $in = [preg_replace("/(,|;)/", "", $typeData['financeAccountTypesName']), "", $value2];
		                    $arr.=implode(",", $in) . "\n";
		                }

		                if($typekey == 1){
		                    $arr.=implode(",", []) . "\n";
		                    $grossProfit = $revenueAmount - $costOfSalesAmount;
		                    if($grossProfit < 0){
		                        $grossProfit = '('. $grossProfit*(-1).')';
		                    }
		                    $in = ['Gross Profit', "", $grossProfit];
		                    $arr.=implode(",", $in) . "\n";
		                    $arr.=implode(",", []) . "\n";
		                }else if($typekey == 3){
		                    $arr.=implode(",", []) . "\n";
		                    $netProfit = ($revenueAmount - $costOfSalesAmount) +($otherRevenueAmount - $expenceAmount);
		                    if($netProfit < 0){
		                        $netProfit = '('.$netProfit*(-1).')';
		                    }
		                    $in = ['Net Profit', "", $netProfit];
		                    $arr.=implode(",", $in) . "\n";
		                    $arr.=implode(",", []) . "\n";
		                }
		            }
		        } else {
		            $arr = "no matching records found\n";
		        }

		        $name = "Statement_of_income_and_expenditure.csv";
		        $csvContent = $this->csvContent($title, $header, $arr);
		        $csvPath = $this->generateCSVFile($name, $csvContent);

				return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');

            }

	   	} catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    public function profitAndLossPdf($postData)
    {
    	try{
            $startDate = $postData['startDate'];
            $endDate = $postData['endDate'];
            $dimensionType = $postData['dimensionType'];
	        $dimensionValue = $postData['dimensionValue'];

	        $locationIDs = $postData['locationID'];
        	$sepLocBal = ($postData['sepLocBal'] == 'true') ? true: false;
        	$hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

        	$locationData = null;


	        if ($sepLocBal) {
	            foreach ($locationIDs as $key => $value) {
	                $location = $this->getModel('LocationTable')->getLocation($value);

	                $locationData[] = [
	                    'locationName' => $location->locationName,
	                    'locationCode' => $location->locationCode,
	                    'locationID' => $location->locationID

	                ];
	            }
	        } 



            $translator = new Translator();
            $name = $translator->translate('Profit And Loss');
            $period = $startDate . ' - ' . $endDate;

            if ($sepLocBal) {
                $array = $this->getBalanceSheetDataForViewReport($startDate,$endDate, $dimensionType, $dimensionValue, $locationData, $sepLocBal);
            } else {
                $array = $this->getBalanceSheetDataForViewReport($startDate,$endDate, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);

            }

            $profitAndLostData = [];
            foreach ($array as $key => $value) {
                if($value['financeAccountTypesID'] == 4){
                    $profitAndLostData[0] = $value;
                } else if( $value['financeAccountTypesID'] == 6){
                    $profitAndLostData[1] = $value;
                } else if( $value['financeAccountTypesID'] ==  7){
                    $profitAndLostData[2] = $value;
                } else if( $value['financeAccountTypesID'] ==  3){
                    $profitAndLostData[3] = $value;
                }
            }

            ksort($profitAndLostData);
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $profitAndLostView = new ViewModel(array(
                'profitAndLostData' => $profitAndLostData,
                'locationIDs' => (!empty($locationIDs)) ? $locationIDs : null,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'locationData' => $locationData,
                'sepLocBal' => $sepLocBal,
                'hideZeroVal' => $hideZeroVal,
                'headerTemplate' => $headerViewRender,
                'cD' => $cD)
            );

            $profitAndLostView->setTemplate('reporting/finance-report/generate-profit-and-lost-pdf');
            $profitAndLostView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($profitAndLostView);
            $pdfPath = $this->downloadPDF('Statement-of-income-and-expenditure', $htmlContent);

            return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

	    } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing pdf data.');
        }

    }


    public function getGeneralLedgerData($startDate, $endDate, $accountIDs = null, $dimensionType = null, $dimensionValue = null, $isOrderByDate = false)
    {
        $financeAccounts = $this->getModel('FinanceAccountsTable')->fetchAll(null, $accountIDs, true);
        $generalLedgerData = [];
        foreach ($financeAccounts as $key => $value) {
            $accountID = $value['financeAccountsID'];
            $AccountData = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($accountID, '', $endDate, false, $dimensionType, $dimensionValue);
            $AccountData['financeAccountsCode'] = $value['financeAccountsCode'];
            $AccountData['financeAccountsName'] = $value['financeAccountsName'];
            $AccountData['financeAccountsTypeID'] = $value['financeAccountTypesID'];
            //calculate date
            $newDate   = date('Y-m-d', strtotime($startDate . ' -1 day'));

            $fAccounts = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($accountID, '', $newDate, false, $dimensionType, $dimensionValue);
            $journalEntrys = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRange($accountID, $startDate, $endDate, false, $dimensionType, $dimensionValue, $isOrderByDate);
            //get journal Entry details that out of selected date range
            $jEDataForOpening = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRange($accountID, '', $newDate, false, $dimensionType, $dimensionValue);
            $getopeningBalanceCreditDebit = $this->getDocumentCommentForRelatedJournalEntry($jEDataForOpening);

            $generalLedgerData[$accountID]['accountsData'] = $AccountData;
            $generalLedgerData[$accountID]['openingBalanceData'] = $fAccounts;
            $jEntriesAndDuplicatedCreditDebitValue = $this->getDocumentCommentForRelatedJournalEntry($journalEntrys);
            $generalLedgerData[$accountID]['journalEntries'] = $jEntriesAndDuplicatedCreditDebitValue['returnJEntity'];
            $generalLedgerData[$accountID]['removedCreditDebit'] = $jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues'];
            $generalLedgerData[$accountID]['removedCreditDebitForOpenning'] = $getopeningBalanceCreditDebit['creditDebitRemovedValues'];
        	$generalLedgerData[$accountID]['parentFinanceAccountsName'] = $value['parentFinanceAccountsName'];
            $generalLedgerData[$accountID]['sortingID'] = ($value['financeAccountsParentID'] == 0) ? $accountID : $value['financeAccountsParentID'];
        }

        $sortingID = array_column($generalLedgerData, 'sortingID');
		array_multisort($sortingID, SORT_ASC, $generalLedgerData);

        return array('generalLedgerData' => $generalLedgerData);
    }

    public function getAuditSheduleData($startDate, $endDate, $accountIDs = null, $dimensionType = null, $dimensionValue = null, $locationIDs)
    {

        $financeAccounts = $this->getModel('FinanceAccountsTable')->fetchAll(null, $accountIDs, true);
        $auditSheduleData = [];
        foreach ($financeAccounts as $key => $value) {
            $accountID = $value['financeAccountsID'];
            $AccountData = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountIDForAuditSheduleReport($accountID, '', $endDate, false, $dimensionType, $dimensionValue, $locationIDs);
            $AccountData['financeAccountsCode'] = $value['financeAccountsCode'];
            $AccountData['financeAccountsName'] = $value['financeAccountsName'];
            $AccountData['financeAccountsTypeID'] = $value['financeAccountTypesID'];
            //calculate date
            $newDate   = date('Y-m-d', strtotime($startDate . ' -1 day'));

            $fAccounts = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountIDForAuditSheduleReport($accountID, '', $newDate, false, $dimensionType, $dimensionValue, $locationIDs);

            $journalEntrys = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountIDForAuditSheduleReport($accountID, $startDate, $endDate, false, $dimensionType, $dimensionValue,$locationIDs);


            $auditSheduleData[$accountID]['accountsData'] = $AccountData;
            $auditSheduleData[$accountID]['openingBalanceData'] = $fAccounts;
            $auditSheduleData[$accountID]['movementCreditBalance'] = $journalEntrys['totalCreditAmount'];
            $auditSheduleData[$accountID]['movementDebitBalance'] = $journalEntrys['totalDebitAmount'];
        	$auditSheduleData[$accountID]['parentFinanceAccountsName'] = $value['parentFinanceAccountsName'];
            $auditSheduleData[$accountID]['sortingID'] = ($value['financeAccountsParentID'] == 0) ? $accountID : $value['financeAccountsParentID'];
        }
        return array('auditSheduleData' => $auditSheduleData);
    }




    public function getGeneralLedgerDataForViewReport($startDate, $endDate, $accountIDs = null, $dimensionType = null, $dimensionValue = null)
    {
        $financeAccounts = $this->getModel('FinanceAccountsTable')->fetchAll(null, $accountIDs, true);
        $generalLedgerData = [];
        foreach ($financeAccounts as $key => $value) {
            $accountID = $value['financeAccountsID'];
            $AccountData = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountIDForGeneralLedger($accountID, '', $endDate, false, $dimensionType, $dimensionValue);
            $AccountData['financeAccountsCode'] = $value['financeAccountsCode'];
            $AccountData['financeAccountsName'] = $value['financeAccountsName'];
            $AccountData['financeAccountsTypeID'] = $value['financeAccountTypesID'];
            //calculate date
            $newDate   = date('Y-m-d', strtotime($startDate . ' -1 day'));

            $fAccounts = $this->getModel('FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountIDForGeneralLedger($accountID, '', $newDate, false, $dimensionType, $dimensionValue);

            $generalLedgerData[$accountID]['accountsData'] = $AccountData;
            $generalLedgerData[$accountID]['openingBalanceData'] = $fAccounts;
        	$generalLedgerData[$accountID]['accountID'] = $accountID;
            $generalLedgerData[$accountID]['parentFinanceAccountsName'] = $value['parentFinanceAccountsName'];
            $generalLedgerData[$accountID]['sortingID'] = ($value['financeAccountsParentID'] == 0) ? $accountID : $value['financeAccountsParentID'];
        }

        $sortingID = array_column($generalLedgerData, 'sortingID');
		array_multisort($sortingID, SORT_ASC, $generalLedgerData);

        return array('generalLedgerData' => $generalLedgerData);
    }

    public function getGeneralLedgerJournalEntriesByAccountId($startDate, $endDate, $accountID, $dimensionType = null, $dimensionValue = null, $isOrderByDate = false)
    {
    	
        $financeAccounts = $this->getModel('FinanceAccountsTable')->fetchAll(null, array($accountID), true);
        $generalLedgerData = [];
        foreach ($financeAccounts as $key => $value) {
            $accountID = $value['financeAccountsID'];
            //calculate date
            $journalEntrys = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRangeForGeneralLedger($accountID, $startDate, $endDate, false, $dimensionType, $dimensionValue, $isOrderByDate);

            $jEntriesAndDuplicatedCreditDebitValue = $this->getDocumentCommentForRelatedJournalEntry($journalEntrys);
            $generalLedgerData[$accountID]['journalEntries'] = $jEntriesAndDuplicatedCreditDebitValue['returnJEntity'];
        }
        return array('generalLedgerJournalEntriesData' => $generalLedgerData);
    }

    public function generalLedgerSheet($postData)
    {
        try{
        	$startDate = $postData['startDate'];
            $endDate = $postData['endDate'];
            $accountIDs = $postData['accountID'];
            $dimensionType = $postData['dimensionType'];
        	$dimensionValue = $postData['dimensionValue'];
        	$isOrderByDate = ($postData['isOrderByDate'] == "true") ? true : false;
        	$isShowAccumilatedBalance = ($postData['balState'] == "true") ? true : false;
            $translator = new Translator();
            $name = $translator->translate('General Ledger');
            $period = $startDate . ' - ' . $endDate;

            $generalLedgerData = $this->getGeneralLedgerData($startDate, $endDate, $accountIDs, $dimensionType, $dimensionValue, $isOrderByDate)['generalLedgerData'];

            if ($generalLedgerData) {
                $title = '';
                $tit = 'GENERAL LEDGER REPORT';
                $in = ["", $tit];
                $title = implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];

                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $in = ["Period : " . $startDate . '-' . $endDate];
                $title.=implode(",", $in) . "\n";

                foreach ($generalLedgerData as $key => $Data) {
                	if (floatval($Data['accountsData']['totalDebitAmount']) != 0 || floatval($Data['accountsData']['totalCreditAmount']) != 0 || floatval($Data['openingBalanceData']['totalCreditAmount']) != 0 || floatval($Data['openingBalanceData']['totalDebitAmount']) != 0) {
	                    $totalBalanceAmount = 0.00;
	                    $openingBalance = 0.00;
	                    // reset accountDetails totalDebit and totalCredit
	                    $Data['accountsData']['totalCreditAmount'] = floatval($Data['accountsData']['totalCreditAmount']) - (floatval($Data['removedCreditDebit']['credit']) + floatval($Data['removedCreditDebitForOpenning']['credit']));
	                    $Data['accountsData']['totalDebitAmount'] = floatval($Data['accountsData']['totalDebitAmount']) - (floatval($Data['removedCreditDebit']['debit']) + floatval($Data['removedCreditDebitForOpenning']['debit']));

	                    if($Data['accountsData']['totalDebitAmount'] >= $Data['accountsData']['totalCreditAmount']){
	                        $totalBalanceAmount = $Data['accountsData']['totalDebitAmount'] - $Data['accountsData']['totalCreditAmount'];
	                    }else{
	                        $totalBalanceAmount = ($Data['accountsData']['totalCreditAmount'] - $Data['accountsData']['totalDebitAmount']) * -1;
	                    }

	                    if ($Data['accountsData']['financeAccountsTypeID'] == 1 || $Data['accountsData']['financeAccountsTypeID'] == 3 || $Data['accountsData']['financeAccountsTypeID'] == 6) {
	                    	if ($totalBalanceAmount == 0) {
		                    	$totalBalanceAmountText = 0.00;
		                    } elseif ($totalBalanceAmount > 0) {
		                    	$totalBalanceAmountText = $totalBalanceAmount;
		                    } elseif ($totalBalanceAmount < 0) {
		                    	$totalBalanceAmountText = '( '.($totalBalanceAmount * -1).' )';
		                    }

	                    } elseif ($Data['accountsData']['financeAccountsTypeID'] == 2 || $Data['accountsData']['financeAccountsTypeID'] == 4 || $Data['accountsData']['financeAccountsTypeID'] == 5  || $Data['accountsData']['financeAccountsTypeID'] == 7) { 
	                    	if ($totalBalanceAmount == 0) {
		                    	$totalBalanceAmountText = 0.00;
		                    } elseif ($totalBalanceAmount > 0) {
		                    	$totalBalanceAmountText = '( '.$totalBalanceAmount.' )';
		                    } elseif ($totalBalanceAmount < 0) {
		                    	$totalBalanceAmountText = ($totalBalanceAmount * -1);
		                    }
	                    }
	                    

	                    $in = ['ACCOUNT CODE AND NAME', '', '', '','','', '','','DEBIT', 'CREDIT','Balance'];
	                    $arr.=implode(",", $in) . "\n";

	                    if (is_null($Data['parentFinanceAccountsName'])) {
	                    	$accountCodeAndName = $Data['accountsData']['financeAccountsCode']." - ".$Data['accountsData']['financeAccountsName'];
	                    } else {
	                    	$accountCodeAndName = $Data['accountsData']['financeAccountsCode']." - ".$Data['accountsData']['financeAccountsName']." (Parent Ac- ".$Data['parentFinanceAccountsName'];
	                    }
	                    $debitAmount = ($Data['accountsData']['totalDebitAmount'] > 0)? $Data['accountsData']['totalDebitAmount'] : '0.00';
	                    $creditAmount = ($Data['accountsData']['totalCreditAmount'] > 0)? $Data['accountsData']['totalCreditAmount'] : '0.00';

	                    $in = [$accountCodeAndName, '', '', '','','','','', $debitAmount, $creditAmount, $totalBalanceAmountText];
	                    $arr.=implode(",", $in) . "\n";

	                    //reset acounts openning balances
	                    $Data['openingBalanceData']['totalCreditAmount'] = floatval($Data['openingBalanceData']['totalCreditAmount']) - floatval($Data['removedCreditDebitForOpenning']['credit']);
	                    $Data['openingBalanceData']['totalDebitAmount'] = floatval($Data['openingBalanceData']['totalDebitAmount']) - floatval($Data['removedCreditDebitForOpenning']['debit']);

	                    if( $Data['openingBalanceData']['totalCreditAmount'] >= $Data['openingBalanceData']['totalDebitAmount'] ){
	                        $openingBalance = ($Data['openingBalanceData']['totalCreditAmount'] - $Data['openingBalanceData']['totalDebitAmount']) * -1;
	                    }else{
	                        $openingBalance = $Data['openingBalanceData']['totalDebitAmount'] - $Data['openingBalanceData']['totalCreditAmount'];
	                    }

	                    // $openingBalance = ($openingBalance != 0)? $openingBalance : '0.00';

	                    if ($Data['accountsData']['financeAccountsTypeID'] == 1 || $Data['accountsData']['financeAccountsTypeID'] == 3 || $Data['accountsData']['financeAccountsTypeID'] == 6) {

	                    	if ($openingBalance == 0) {
		                    	$openingBalanceText = 0.00;
		                    } elseif ($openingBalance < 0) {
		                    	$openingBalanceText = '( '.($openingBalance * -1).' )';
		                    } elseif ($openingBalance > 0) {
		                    	$openingBalanceText = $openingBalance;
		                    }

	                    } elseif ($Data['accountsData']['financeAccountsTypeID'] == 2 || $Data['accountsData']['financeAccountsTypeID'] == 4 || $Data['accountsData']['financeAccountsTypeID'] == 5  || $Data['accountsData']['financeAccountsTypeID'] == 7) {

	                    	if ($openingBalance == 0) {
		                    	$openingBalanceText = 0.00;
		                    } elseif ($openingBalance < 0) {
		                    	$openingBalanceText = ($openingBalance * -1);
		                    } elseif ($openingBalance > 0) {
		                    	$openingBalanceText = '( '.$openingBalance.' )';
		                    }

	                    }

	                    $openingDebitAmount = ($Data['openingBalanceData']['totalDebitAmount'] > 0)? $Data['openingBalanceData']['totalDebitAmount']: '0.00';
	                    $openingCreditAmount = ($Data['openingBalanceData']['totalCreditAmount'] > 0)? $Data['openingBalanceData']['totalCreditAmount']: '0.00';

	                    $in = ['Opening Balance', '', '', '','','','','', $openingDebitAmount, $openingCreditAmount, $openingBalanceText];
	                    $arr.=implode(",", $in) . "\n";

	                    if(count($Data['journalEntries']) > 0){
	                        $in = ['Journal Entries', '', '', '', '', ''];
	                        $arr.=implode(",", $in) . "\n";

	                        $in = ['DATE', 'JOURNAL ENTRY CODE', 'JOURNAL ENTRY COMMENT', 'RELATED DOCUMENT TYPE', 'RELATED DOCUMENT CUSTOMER/SUPPLIER', 'RELATED DOCUMENT CODE','RELATED DOCUMENT COMMENT', 'RELATED DOCUMENT Details','DEBIT', 'CREDIT', 'BALANCE'];
	                        $arr.=implode(",", $in) . "\n";

	                        $accBal = $openingBalance;
	                        foreach ($Data['journalEntries'] as $JEkey => $JEvalue) {

	                            $jentryBalance = 0.00;

	                            if($JEvalue['journalEntryAccountsDebitAmount'] >= $JEvalue['journalEntryAccountsCreditAmount']){
	                                $jentryBalance = $JEvalue['journalEntryAccountsDebitAmount'] - $JEvalue['journalEntryAccountsCreditAmount'];
	                                $jeBal = $jentryBalance;
	                            }else{
	                                $jentryBalance = $JEvalue['journalEntryAccountsCreditAmount'] - $JEvalue['journalEntryAccountsDebitAmount'];
	                                $jeBal = $jentryBalance * -1;
	                            }

	                            $accBal += $jeBal;


	                            if ($Data['accountsData']['financeAccountsTypeID'] == 1 || $Data['accountsData']['financeAccountsTypeID'] == 3 || $Data['accountsData']['financeAccountsTypeID'] == 6) {

	                            	if ($isShowAccumilatedBalance) {
	                                	$jentryBalanceText = ($accBal < 0) ? '( '.($accBal * -1).' )' : $accBal;
		                            } else {
	                                	$jentryBalanceText = ($jeBal < 0) ? '( '.$jentryBalance. ' )' : $jentryBalance;
		                            }

	                            } elseif ($Data['accountsData']['financeAccountsTypeID'] == 2 || $Data['accountsData']['financeAccountsTypeID'] == 4 || $Data['accountsData']['financeAccountsTypeID'] == 5  || $Data['accountsData']['financeAccountsTypeID'] == 7) {
	                            	if ($isShowAccumilatedBalance) {
	                                	$jentryBalanceText = ($accBal < 0) ? ($accBal * -1) : '( '.$accBal.' )';
		                            } else {
	                                	$jentryBalanceText = ($jeBal < 0) ? $jentryBalance : '( '.$jentryBalance.' )';
		                            }

	                            }



	                            //reset document comment
	                            $in = [$JEvalue['journalEntryDate'],
	                                    $JEvalue['journalEntryCode'],
	                                    preg_replace("/(,|;)/", "", $JEvalue['journalEntryComment']),
	                                    $JEvalue['documentType'],
	                                    preg_replace("/(,|;)/", "", $JEvalue['cusName']),
	                                    $JEvalue['documentCode'],
	                                    preg_replace("/(,|;)/", " ", preg_replace('/\s+/', ' ',$JEvalue['relatedDocComment'])),
	                                    $JEvalue['childDoc'],
	                                    $JEvalue['journalEntryAccountsDebitAmount'],
	                                    $JEvalue['journalEntryAccountsCreditAmount'],
	                                    $jentryBalanceText
	                                    ];
	                            $arr.=implode(",", $in) . "\n";

	                        }

	                    }
	                    $arr.=implode(",", []) . "\n";
	                }
                }
            } else {
                $arr = "no matching records found\n";
            }

            $name = "General_Ledger_Report.csv";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');

	   	} catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    public function generalLedgerPdf($postData)
    {
    	try{
            $startDate = $postData['startDate'];
            $endDate = $postData['endDate'];
            $accountIDs = $postData['accountID'];
            $dimensionType = $postData['dimensionType'];
        	$dimensionValue = $postData['dimensionValue'];
        	$isOrderByDate = ($postData['isOrderByDate'] == "true") ? true : false;
        	$isShowAccumilatedBalance = ($postData['balState'] == "true") ? true : false;
            $translator = new Translator();
            $name = $translator->translate('General Ledger');
            $period = $startDate . ' - ' . $endDate;

            $generalLedgerData = $this->getGeneralLedgerData($startDate, $endDate, $accountIDs, $dimensionType, $dimensionValue, $isOrderByDate)['generalLedgerData'];
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $generalLedgerView = new ViewModel(array(
                'generalLedgerData' => $generalLedgerData,
                'startDate' => $startDate,
                'balState' => $isShowAccumilatedBalance,
                'endDate' => $endDate,
                'headerTemplate' => $headerViewRender,
                'cD' => $cD)
            );

            $generalLedgerView->setTemplate('reporting/finance-report/generate-general-ledger-pdf');
            $generalLedgerView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($generalLedgerView);
	        $pdfPath = $this->downloadPDF('general-ledger', $htmlContent);

	        return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

	    } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing pdf data.');
        }
    }

    public function calculateAmounts($dataArray)
    {
        $creditAmountTotal = 0;
        $debitAmountTotal = 0;
        foreach ($dataArray as $value) {
            if(isset($value['child'])){
                $amountData = $this->calculateAmounts($value['child']);
                $creditAmountTotal+=$amountData['totalCreditAmount'];
                $debitAmountTotal+=$amountData['totalDebitAmount'];
            }
            $creditAmountTotal+=$value['data']->totalCreditAmount;
            $debitAmountTotal+=$value['data']->totalDebitAmount;
        }
        return array('totalCreditAmount'=> $creditAmountTotal, 'totalDebitAmount' => $debitAmountTotal);
    }

    public function getChildAccounts($dataArray,$acCData,$level,$dtype, $docSubMode = null, $hideZeroVal = false)
    {
        $arr = '';
        foreach ($dataArray as $value) {
            if(isset($value['child'])){
                $amountData = $this->calculateAmounts($value['child']);
            }

            $creditAmount = ($amountData['totalCreditAmount'] != 0)? (float) ($amountData['totalCreditAmount']+$value['data']->totalCreditAmount ): (float) $value['data']->totalCreditAmount;
            $debitAmount = ($amountData['totalDebitAmount'] != 0)? (float) ($amountData['totalDebitAmount']+$value['data']->totalDebitAmount ): (float) $value['data']->totalDebitAmount;
            if($dtype == 'trialBalance'){
                $balance = floatval($debitAmount) - floatval($creditAmount);
                if($balance < 0){
                    $viewBalnce = '('. ($balance* (-1)) .')';
                } else {
                    $viewBalnce = $balance;
                }

                if ($hideZeroVal) {
                	if ($balance != 0) {
                		$in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsName, (string) $value['data']->financeAccountsCode, $acCData['financeAccountClassName'], $acCData['financeAccountTypesName'], $viewBalnce];
                		$arr.=implode(",", $in) . "\n";
                	}

                } else {
                	$in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsName, (string) $value['data']->financeAccountsCode, $acCData['financeAccountClassName'], $acCData['financeAccountTypesName'], $viewBalnce];
                	$arr.=implode(",", $in) . "\n";
                }
            }else if($dtype == 'balanceSheet'){
                if($docSubMode == 5 || $docSubMode == 2){
                    $balance = floatval($creditAmount) - floatval($debitAmount);
                }
                else if($docSubMode == 1){
                    $balance = floatval($debitAmount) - floatval($creditAmount);
                }

                if($balance < 0){
                    $viewBalnce = '('. ($balance* (-1)) .')';
                } else {
                    $viewBalnce = $balance;
                }

                if ($hideZeroVal) {
                	if ($balance != 0) {
                		$in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsCode, $value['data']->financeAccountsName, $viewBalnce];
                		$arr.=implode(",", $in) . "\n";
                	}
                } else {
                	$in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsCode, $value['data']->financeAccountsName, $viewBalnce];
                	$arr.=implode(",", $in) . "\n";
                }

                
            }

            if(isset($value['child'])){
                $arr.= $this->getChildAccounts($value['child'], $acCData, $level+1,$dtype, $docSubMode, $hideZeroVal);
            }
        }
        return $arr;
    }


    public function getChildAccountsForLocsForTrial($dataArray,$acCData,$level,$dtype, $docSubMode = null, $hideZeroVal = false)
    {
        $arr = '';
        foreach ($dataArray as $value) {
            if(isset($value['child'])){
                $amountData = $this->calculateAmounts($value['child']);
            }

            $creditAmount = ($amountData['totalCreditAmount'] != 0)? (float) ($amountData['totalCreditAmount']+$value['data']->totalCreditAmount ): (float) $value['data']->totalCreditAmount;
            $debitAmount = ($amountData['totalDebitAmount'] != 0)? (float) ($amountData['totalDebitAmount']+$value['data']->totalDebitAmount ): (float) $value['data']->totalDebitAmount;
            if($dtype == 'trialBalance'){
                $balance = floatval($debitAmount) - floatval($creditAmount);
                if($balance < 0){
                    $viewBalnce = '('. ($balance* (-1)) .')';
                } else {
                    $viewBalnce = $balance;
                }

                if ($hideZeroVal) {
                	if ($balance != 0) {
		                $in = ['',str_repeat(' ', 2*$level).$value['data']->financeAccountsName, (string) $value['data']->financeAccountsCode, $acCData['financeAccountClassName'], $acCData['financeAccountTypesName'], $viewBalnce];
		                $arr.=implode(",", $in) . "\n";
	                }
                } else {
                	$in = ['',str_repeat(' ', 2*$level).$value['data']->financeAccountsName, (string) $value['data']->financeAccountsCode, $acCData['financeAccountClassName'], $acCData['financeAccountTypesName'], $viewBalnce];
		            $arr.=implode(",", $in) . "\n";
                }

            }else if($dtype == 'balanceSheet'){
                if($docSubMode == 5 || $docSubMode == 2){
                    $balance = floatval($creditAmount) - floatval($debitAmount);
                }
                else if($docSubMode == 1){
                    $balance = floatval($debitAmount) - floatval($creditAmount);
                }

                if($balance < 0){
                    $viewBalnce = '('. ($balance* (-1)) .')';
                } else {
                    $viewBalnce = $balance;
                }

                $in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsCode, $value['data']->financeAccountsName, $viewBalnce];
                $arr.=implode(",", $in) . "\n";
            }

            if(isset($value['child'])){
                $arr.= $this->getChildAccountsForLocsForTrial($value['child'], $acCData, $level+1,$dtype, $docSubMode, $hideZeroVal);
            }
        }
        return $arr;
    }

    public function getCashFlowData($startDate, $endDate)
    {
	    $FinanceAccounts = $this->getModel('FinanceAccountsTable')->fetchAll();

	    $financeAccountsData = [];
	    foreach ($FinanceAccounts as $value) {
	    	$financeAccountsData[$value['financeAccountsID']] = $value;
	    }

    	$respon = $this->getModel('PaymentsTable')->getPaymentsDetailsForCashFlow($startDate, $endDate);
        $cashInFlowDta = $respon ? $respon : array();
    
    	$finalData = array();
        $cashInFlow = [];
        $cashOutFlow = [];
        foreach ($cashInFlowDta as $res) {
			$jERes = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($res['journalEntryID']);
   			
   			foreach ($jERes as $value) {
	    		if ($value['financeAccountsID'] == $res['cashAccountID']) {
					if (is_null($cashInFlow[$res['cashAccountID']])) {
						$cashInFlow[$res['cashAccountID']] = (floatval($value['journalEntryAccountsDebitAmount']) == 0) ? floatval($value['journalEntryAccountsCreditAmount']) : floatval($value['journalEntryAccountsDebitAmount']);	
					} else {
						$cashInFlow[$res['cashAccountID']] += (floatval($value['journalEntryAccountsDebitAmount']) == 0) ? floatval($value['journalEntryAccountsCreditAmount']) : floatval($value['journalEntryAccountsDebitAmount']);	
					}
	    			
	    		}

	    		if ($value['financeAccountsID'] == $res['incomingPaymentMethodChequeAccNo']) {
		        	if (is_null($cashInFlow[$res['incomingPaymentMethodChequeAccNo']])) {
						$cashInFlow[$res['incomingPaymentMethodChequeAccNo']] = (floatval($value['journalEntryAccountsDebitAmount']) == 0) ? floatval($value['journalEntryAccountsCreditAmount']) : floatval($value['journalEntryAccountsDebitAmount']);	
					} else {
						$cashInFlow[$res['incomingPaymentMethodChequeAccNo']] += (floatval($value['journalEntryAccountsDebitAmount']) == 0) ? floatval($value['journalEntryAccountsCreditAmount']) : floatval($value['journalEntryAccountsDebitAmount']);	
					}
	    		}

				if ($value['financeAccountsID'] == $res['cardAccountID']) {
		        	if (is_null($cashInFlow[$res['cardAccountID']])) {
						$cashInFlow[$res['cardAccountID']] = (floatval($value['journalEntryAccountsDebitAmount']) == 0) ? floatval($value['journalEntryAccountsCreditAmount']) : floatval($value['journalEntryAccountsDebitAmount']);	
					} else {
						$cashInFlow[$res['cardAccountID']] += (floatval($value['journalEntryAccountsDebitAmount']) == 0) ? floatval($value['journalEntryAccountsCreditAmount']) : floatval($value['journalEntryAccountsDebitAmount']);	
					}
	    		}
   			}
	    }

	    $cashOutFlowDta = $this->getModel('SupplierPaymentsTable')->getPaymentDetailsForCashFlow($startDate, $endDate);

	    foreach ($cashOutFlowDta as $value) {
	    	if ($value['outGoingPaymentMethodID'] == "1" || $value['outGoingPaymentMethodID'] == "2" || $value['outGoingPaymentMethodID'] == "3") {
				if (is_null($cashOutFlow[$value['outGoingPaymentMethodFinanceAccountID']])) {
					$cashOutFlow[$value['outGoingPaymentMethodFinanceAccountID']] = (floatval($value['debit']) == 0) ? floatval($value['credit']) : floatval($value['debit']);	
				} else {
					$cashOutFlow[$value['outGoingPaymentMethodFinanceAccountID']] += (floatval($value['debit']) == 0) ? floatval($value['credit']) : floatval($value['debit']);	
				}    		
	    	} 
	    }
 
	    $cashInFlowFinalData = [];
		foreach ($cashInFlow as $key => $value) {
			if (!empty($key)) {
		    	$cashInFlowFinalData[$key."_cashIn"]['accountID'] = $key;
		    	$cashInFlowFinalData[$key."_cashIn"]['type'] = "cashIn";
		    	$cashInFlowFinalData[$key."_cashIn"]['value'] = $value;
		    	$cashInFlowFinalData[$key."_cashIn"]['accountCode'] = $financeAccountsData[$key]['financeAccountsCode'];
		    	$cashInFlowFinalData[$key."_cashIn"]['accountName'] = $financeAccountsData[$key]['financeAccountsName'];
		    	$cashInFlowFinalData[$key."_cashIn"]['accountClassID'] = $financeAccountsData[$key]['financeAccountClassID'];
			}
	    }	    
    	
		$cashOutFlowFinalData = [];
		foreach ($cashOutFlow as $key => $value) {
			if (!empty($key)) {
		    	$cashOutFlowFinalData[$key."_cashOut"]['accountID'] = $key;
		    	$cashOutFlowFinalData[$key."_cashOut"]['type'] = "cashOut";
		    	$cashOutFlowFinalData[$key."_cashOut"]['value'] = $value;
		    	$cashOutFlowFinalData[$key."_cashOut"]['accountCode'] = $financeAccountsData[$key]['financeAccountsCode'];
		    	$cashOutFlowFinalData[$key."_cashOut"]['accountName'] = $financeAccountsData[$key]['financeAccountsName'];
		    	$cashOutFlowFinalData[$key."_cashOut"]['accountClassID'] = $financeAccountsData[$key]['financeAccountClassID'];
			}
	    }	    
    	
	    $finalData = array_merge($cashOutFlowFinalData, $cashInFlowFinalData);

	    $cashInOutFlowData = [];
	    $cashFlowTotal = 0;
	    foreach ($finalData as $key => $value) {
	    	if ($value['accountClassID'] == 5) {
	    		$cashInOutFlowData['Investing Activities'][] = $value;
	    	} else if ($value['accountClassID'] == 9) {
	    		$cashInOutFlowData['Financing Activities'][] = $value;
	    	} else {
	    		$cashInOutFlowData['Operating Activities'][] = $value;
	    	}

	    	if ($value['type'] == "cashOut") {
	    		$cashFlowTotal -= $value['value'];
	    	} else {
	    		$cashFlowTotal += $value['value'];
	    	}
	    }

	    return ['cashInOutFlowData' => $cashInOutFlowData, 'cashFlowTotal' => $cashFlowTotal];
    }


    public function cashFlowPdf($postData)
    {
    	try{
	        $startDate = $postData['startDate'];
	        $endDate = $postData['endDate'];

	        $translator = new Translator();
	        $name = $translator->translate('Statement of cash flows');
	        $period = $startDate . ' - ' . $endDate;

	        $cashFlowData = $this->getCashFlowData($startDate, $endDate);
	        $newDate   = date('Y-m-d', strtotime($startDate . ' -1 day'));
	        $lastYearCashFlowData = $this->getCashFlowData('', $newDate);

	        $companyDetails = $this->getCompanyDetails();

	        $headerView = $this->headerViewTemplate($name, $period);
	        $headerView->setTemplate('reporting/template/headerTemplate');
	        $headerViewRender = $this->htmlRender($headerView);

	        $cashFlowView = new ViewModel(array(
	            'cashFlowData' => $cashFlowData['cashInOutFlowData'],
	            'cashFlowTotal' => $lastYearCashFlowData['cashFlowTotal'],
	            'startDate' => $startDate,
	            'endDate' => $endDate,
	            'headerTemplate' => $headerViewRender,
	            'cD' => $cD)
	        );

	        $cashFlowView->setTemplate('reporting/finance-report/generate-cash-flow-pdf');
	        $cashFlowView->setTerminal(true);


	        // get rendered view into variable
	        $htmlContent = $this->viewRendererHtmlContent($cashFlowView);
	        $pdfPath = $this->downloadPDF('statement-of-cash-flows', $htmlContent);

	        return $this->returnSuccess(['fileName' => $pdfPath, 'fileType' => self::PDF_REPORT, 'reportName' => $name], 'pdf report has been generated.');

	    } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing pdf data.');
        }
    }

    public function cashFlowSheet($postData)
    {
    	try{

		 	$startDate = $postData['startDate'];
        	$endDate = $postData['endDate'];

	        $translator = new Translator();
	        $name = $translator->translate('Statement of cash flows');
	        $period = $startDate . ' - ' . $endDate;

	        $cashFlowData = $this->getCashFlowData($startDate, $endDate);
	        $newDate   = date('Y-m-d', strtotime($startDate . ' -1 day'));
	        $lastYearCashFlowData = $this->getCashFlowData('', $newDate);

	    	if ($cashFlowData['cashFlowTotal'] != 0 || $lastYearCashFlowData['cashFlowTotal'] != 0) {
	            $period = $startDate . ' - ' . $endDate;
	            $companyDetails = $this->getCompanyDetails();
	            $data = array(
	                "Statement of cash flows"
	            );
	            $title.=implode(",", $data) . "\n";

	            $data = array(
	                'Report Generated: ' . date('Y-M-d h:i:s a')
	            );
	            $title.=implode(",", $data) . "\n";

	            $data = array(
	                $companyDetails[0]->companyName,
	                $companyDetails[0]->companyAddress,
	                "Tel: " . $companyDetails[0]->telephoneNumber,
	                "Period : " . $period
	            );

	            $title.=implode(",", $data) . "\n";

	            $data = array(
	                "Period :" . $period
	            );
	            $title.=implode(",", $data) . "\n";


	            $tableHead = array('','Account Code & Name','Cash Value');
	            $header.=implode(",", $tableHead) . "\n";
	            $netCashTotal = 0;
	            foreach ($cashFlowData['cashInOutFlowData'] as $key => $value) { 
	                $data1 = array(strtoupper($key));
	                $arr.=implode(",", $data1) . "\n";     

	                $intervelWiseTotal = 0;
	                foreach ($value as $val) { 
	                    $cashValue = ($val['type'] == "cashOut") ? $val['value'] *-1 :$val['value'];
	                    $data2 = array('',$val['accountCode']." ".$val['accountName'],$cashValue); 
	                    $arr.=implode(",", $data2) . "\n";
	                    
	                    if ($val['type'] == "cashOut") {
	                        $intervelWiseTotal -= $val['value'];
	                        $netCashTotal -= $val['value'];
	                    } else {
	                        $intervelWiseTotal += $val['value'];
	                        $netCashTotal += $val['value'];
	                    }
	                }
	                $data3 = array('','Net cash provided by '.$key, $intervelWiseTotal);
	                $arr.=implode(",", $data3) . "\n";    
	            }

	            $data4 = array('','Net increase/decrease in cash', $netCashTotal);
	            $arr.=implode(",", $data4) . "\n";

	            $finalCash = $lastYearCashFlowData['cashFlowTotal'] + $netCashTotal;

	            $data5 = array('','Cash at the begining of the period', $lastYearCashFlowData['cashFlowTotal']);
	            $arr.=implode(",", $data5) . "\n";

	            $data6 = array('','Cash at the end of the period', $finalCash);
	            $arr.=implode(",", $data6) . "\n";
	        } else {
	            $arr = "no matching records found";
	        }

            $name = "statments_of_cashflow.csv";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            return $this->returnSuccess(['fileName' => $csvPath, 'fileType' => self::CSV_REPORT, 'reportName' => $name], 'csv report has been generated.');

	   	} catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnError('Error occoured while processing csv data.');
        }
    }

    private function getNetProfitAmountLocationWise($profitAndLostData, $locationData)
    {
        $kk  = $locationData['locationCode'].'-'.$locationData['locationID'];
        $netProfit = 0.00;
        foreach ($profitAndLostData as $typekey => $typeData) {
            $TotalDebitValue = 0;
            $TotalCreditValue = 0;

            foreach ($typeData as $tbalance) {
                if(isset($tbalance['financeAccounts'][0]['child'])){
                    foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
                        $creditAmount = 0;
                        $debitAmount = 0;
                        if(isset($value['child'])){
                            $amountData = $this->calculateAmountsForProfitAndLostLocationwise($value['child'], $locationData);
                            if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                                $TotalCreditValue+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                                $creditAmount = $amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                            } else {
                                $TotalDebitValue+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
                                $debitAmount = $amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
                            }
                        }

                        if($value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk] > 0){
                            $TotalCreditValue+=$value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk];
                            $creditAmount+=$value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk];
                        }else{
                            $TotalDebitValue+=$value['data']->totalDebitAmount[$kk] - $value['data']->totalCreditAmount[$kk];
                            $debitAmount+=$value['data']->totalDebitAmount[$kk] - $value['data']->totalCreditAmount[$kk];
                        }
                    }
                }
            }

            if($typekey == 0 || $typekey == 2){ 
                if($typekey == 0){
                    $revenueAmount = $TotalCreditValue - $TotalDebitValue; 
                } else if($typekey == 2){
                    $otherRevenueAmount = $TotalCreditValue - $TotalDebitValue;                             
                }

                
            }else if($typekey == 1 || $typekey == 3){
                if($typekey == 1){
                    $costOfSalesAmount = $TotalDebitValue - $TotalCreditValue ; 
                }else if($typekey == 3){
                    $expenceAmount = $TotalDebitValue - $TotalCreditValue ; 
                }
            }

            if($typekey == 1){
                $grossProfit = $revenueAmount - $costOfSalesAmount;
            }else if($typekey == 3){
                $netProfit = ($revenueAmount - $costOfSalesAmount) +($otherRevenueAmount - $expenceAmount);
            }
        }
        return $netProfit;
    }
    private function calculateAmountsForProfitAndLostLocationwise($dataArray, $locData)
    {
        $kk = $locData['locationCode'].'-'.$locData['locationID'];
        $creditAmountTotal = 0; 
        $debitAmountTotal = 0;
        foreach ($dataArray as $value) {
            if(isset($value['child'])){
                $amountData = $this->calculateAmountsForProfitAndLostLocationwise($value['child'], $locData);
                if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                    $creditAmountTotal+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                }else{
                    $debitAmountTotal+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'] ;
                }
            }
            if($value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk] > 0){
                $creditAmountTotal+=$value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk];
            }else{
                $debitAmountTotal+=$value['data']->totalDebitAmount[$kk] - $value['data']->totalCreditAmount[$kk];
            }
        }
        return array('totalCreditAmount'=> $creditAmountTotal, 'totalDebitAmount' => $debitAmountTotal);

    }

    private function calculateAmountsLocWise($dataArray, $kk){
		// $kk = $locData['locationCode'].'-'.$locData['locationID'];
		$creditAmountTotal = 0; 
		$debitAmountTotal = 0;
		foreach ($dataArray as $value) {
			if(isset($value['child'])){
				$amountData = $this->calculateAmountsLocWise($value['child'], $kk);
				$creditAmountTotal+=$amountData['totalCreditAmount'];
				$debitAmountTotal+=$amountData['totalDebitAmount'];
			}
			$creditAmountTotal+=$value['data']->totalCreditAmount[$kk];
			$debitAmountTotal+=$value['data']->totalDebitAmount[$kk];
		}
		return array('totalCreditAmount'=> $creditAmountTotal, 'totalDebitAmount' => $debitAmountTotal);
	}

	private function getChildAccountsLocationWise($dataArray,$acCData,$level,$dtype, $docSubMode = null, $locationData = null, $hideZeroVal = false)
    {
        $arr = '';
        foreach ($dataArray as $value) {

            if($dtype == 'trialBalance'){
	            if(isset($value['child'])){
	                $amountData = $this->calculateAmounts($value['child']);
	            }
	            $creditAmount = ($amountData['totalCreditAmount'] != 0)? (float) ($amountData['totalCreditAmount']+$value['data']->totalCreditAmount ): (float) $value['data']->totalCreditAmount;
	            $debitAmount = ($amountData['totalDebitAmount'] != 0)? (float) ($amountData['totalDebitAmount']+$value['data']->totalDebitAmount ): (float) $value['data']->totalDebitAmount;
                $balance = floatval($debitAmount) - floatval($creditAmount);
                if($balance < 0){
                    $viewBalnce = '('. ($balance* (-1)) .')';
                } else {
                    $viewBalnce = $balance;
                }
                $in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsName, (string) $value['data']->financeAccountsCode, $acCData['financeAccountClassName'], $acCData['financeAccountTypesName'], $viewBalnce];
                $arr.=implode(",", $in) . "\n";

            }else if($dtype == 'balanceSheet'){
                $in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsCode, $value['data']->financeAccountsName];
            	$chidSize = 2;
            	$emptyVal = 0;
            	foreach ($locationData as $key71 => $value71) {

					$locKey = $value71['locationCode'].'-'.$value71['locationID'];
					$amountData = [];
					if(isset($value['child'])){
						$amountData[$locKey] = $this->calculateAmountsLocWise($value['child'], $locKey);
					}

					$createdCredit[$locKey] = ($amountData[$locKey]['totalCreditAmount'] != 0)? floatval($amountData[$locKey]['totalCreditAmount'])+ floatval($value['data']->totalCreditAmount[$locKey]) : floatval($value['data']->totalCreditAmount[$locKey]);
					$createdDebit[$locKey] = ($amountData[$locKey]['totalDebitAmount'] != 0)? floatval($amountData[$locKey]['totalDebitAmount'])+ floatval($value['data']->totalDebitAmount[$locKey]) : floatval($value['data']->totalDebitAmount[$locKey]);
					if($docSubMode == 5 || $docSubMode == 2){
						$balance[$locKey] = floatval($createdCredit[$locKey]) - floatval($createdDebit[$locKey]);
					}
					else if($docSubMode == 1){
						$balance[$locKey] = floatval($createdDebit[$locKey]) - floatval($createdCredit[$locKey]);
					}
					// $accClassSubTotal[$bsData['financeAccountClassID']][$locKey] += floatval($balance[$value['data']->financeAccountsID][$locKey]);

					if($balance[$locKey] < 0){
						$viewBalnce[$locKey] = '('. floatval(($balance[$locKey]* (-1))) .')';
					} else {
						$viewBalnce[$locKey] = floatval($balance[$locKey]);
					}

					if($balance[$locKey] == 0) {
						$emptyVal++;
					} 

					$in[$chidSize] = $viewBalnce[$locKey];
					$chidSize++;

				}

				if ($hideZeroVal) {
					if ($emptyVal != sizeof($locationData)) {
                		$arr.=implode(",", $in) . "\n";
					}

				} else {
                	$arr.=implode(",", $in) . "\n";
				}

            }

            if(isset($value['child'])){
                $arr.= $this->getChildAccountsLocationWise($value['child'], $acCData, $level+1,$dtype, $docSubMode, $locationData, $hideZeroVal
            );
            }
        }
        return $arr;
    }

    private function getChildAccountsForProfitAndLost($dataArray,$acCData,$level, $typeK = null, $hideZeroVal)
    {
        $arr = '';
        foreach ($dataArray as $value) {
            $debitAmount = 0;
            $creditAmount = 0;
            $subAccountCreditAmount = 0;
            $subAccountDebitAmount = 0;
            if(isset($value['child'])){
                $amountData = $this->calculateAmountsForProfitAndLost($value['child']);
                $subAccountCreditAmount += $amountData['totalCreditAmount']; 
                $subAccountDebitAmount += $amountData['totalDebitAmount'];
                if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                    $creditAmount+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                }else{
                    $debitAmount+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'] ;
                }
            }
            if($value['data']->totalCreditAmount - $value['data']->totalDebitAmount > 0){
                $creditAmount+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
            }else{
                $debitAmount+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
            }
            $arrCreditAmount = ($creditAmount - $debitAmount > 0)? (float) ($creditAmount - $debitAmount) : (float) (0.00);
            $arrDebitAmount = ($debitAmount - $creditAmount > 0 )? (float) ($debitAmount - $creditAmount) : (float) (0.00);

            $totalCDvalue = 0;
            if($typeK == 0 || $typeK == 2){
                $subAccountTotal = $subAccountCreditAmount - $subAccountDebitAmount;
                $totalCDvalue = $value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
                $totalCDvalue += $subAccountTotal; 
            } else if($typeK == 1 || $typeK == 3){
                $totalCDvalue = $value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
                $subAccountTotal = $subAccountDebitAmount - $subAccountCreditAmount;
                $totalCDvalue += $subAccountTotal;
            }

            if($totalCDvalue < 0){
                $viewTotalCDValue = '('.($totalCDvalue*(-1)).')';
            } else {
                $viewTotalCDValue =  $totalCDvalue;
            }


            if ($hideZeroVal) {
            	if ($totalCDvalue != 0) {
            		$in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsCode, $value['data']->financeAccountsName, $viewTotalCDValue];
            		$arr.=implode(",", $in) . "\n";
            	}
            } else {
            	$in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsCode, $value['data']->financeAccountsName, $viewTotalCDValue];
            	$arr.=implode(",", $in) . "\n";
            }

            
            if(isset($value['child'])){
                $arr.=$this->getChildAccountsForProfitAndLost($value['child'],$acCData,$level+1, $typeK, $hideZeroVal);
            }
        }
        return $arr;
    }
}
?>