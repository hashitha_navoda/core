<?php

namespace Reporting\Service;

use Core\Service\ReportService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;

class IncomeExpenseReportService extends ReportService
{
	public function getIncomeExpenseData($fromDate, $toDate, $dimensionType, $dimensionValues, $summery, $isCsv = false)
	{
		$dimensionTypeData = $this->getModel('DimensionTable')->getDimensionByDimensionID($dimensionType)->current();

		if ($dimensionType == "job") {
        	$dimensionValueData = $this->getModel('jobTable')->getDimensionByDimensionValueIDArr($dimensionValues);
		} elseif ($dimensionType == "project") {
        	$dimensionValueData = $this->getModel('ProjectTable')->getDimensionByDimensionValueIDArr($dimensionValues);
		} else {
        	$dimensionValueData = $this->getModel('DimensionValueTable')->getDimensionByDimensionValueIDArr($dimensionValues);
		}


        $processDimension = $this->processDimensionValueList($dimensionType, $dimensionValueData, $isCsv);
        $processDimensionValueList = $processDimension['data'];

        $incomeGlAccounts = $this->getModel('FinanceAccountsTable')->getAllAccountsByAccountClassTypeID([4,7]);
        $expenseGlAccounts = $this->getModel('FinanceAccountsTable')->getAllAccountsByAccountClassTypeID([3]);

        
        $incomeDataSet = [];
        $tempData = [];
        foreach ($incomeGlAccounts as $key => $value) {
            if (!array_key_exists($value['financeAccountsID'].':'.$value['financeAccountsName'].'-'.$value['financeAccountsCode'], $tempData)) {
                $tempData[]=$value['financeAccountsID'].':'.$value['financeAccountsName'].'-'.$value['financeAccountsCode'];
            }
            foreach ($processDimensionValueList as $key1 => $val) {
                foreach ($val as $key2 => $v) {
                    $incomeDataSet[$key1][$value['financeAccountsID'].':'.$value['financeAccountsName'].'-'.$value['financeAccountsCode']][$v['dimensionValueID']] = 0;

                }
            }
        }


        $expenseDataSet = [];
        $tempData1 = [];
        foreach ($expenseGlAccounts as $key => $value1) {
            if (!array_key_exists($value1['financeAccountsID'].':'.$value1['financeAccountsName'].'-'.$value1['financeAccountsCode'], $tempData1)) {
                $tempData1[]=$value1['financeAccountsID'].':'.$value1['financeAccountsName'].'-'.$value1['financeAccountsCode'];
            }
            foreach ($processDimensionValueList as $key11 => $val2) {
                foreach ($val2 as $key2 => $v1) {
                    $expenseDataSet[$key11][$value1['financeAccountsID'].':'.$value1['financeAccountsName'].'-'.$value1['financeAccountsCode']][$v1['dimensionValueID']] = 0;

                }
            }
        }

        //process income data for table
        $processDimensionValueIncomeList = $this->processIncomeDataForTable($incomeDataSet, $fromDate, $toDate, $dimensionType);

        //process expense data for table
        $processDimensionValueExpenseList = $this->processExpenseDataForTable($expenseDataSet, $fromDate, $toDate, $dimensionType);


        foreach ($processDimension['finalNumTables'] as $key99 => $value99) {

        	$processDimension['finalNumTables'][$key99]['Income'] =  $processDimensionValueIncomeList[$key99];
        	$processDimension['finalNumTables'][$key99]['Expense'] =  $processDimensionValueExpenseList[$key99];
        }
        

        $finalData = [
        	"data" => $processDimension['finalNumTables'],
        	"headers" => $processDimension['data'],
        	"dimensionType" => $dimensionTypeData['dimensionName']

        ];


       	return $finalData;
	}

	public function processIncomeDataForTable($dataSet, $fromDate, $toDate, $dimensionType) {
		// var_dump($dataSet).die();
		foreach ($dataSet as $key3 => $value3) {
            foreach ($value3 as $key4 => $value4) {
                foreach ($value4 as $key5 => $value5) {
                    $arr = split(':', $key4);

                    $totalCredit = 0;
                    $totalDebit = 0;
                    $dataSet[$key3][$key4]['accName'] = $arr[1];
                    $dimensionData = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRangeForIncomeExpense($arr[0],$fromDate, $toDate, $dimensionType, $key5)->current();

                    if ($dimensionData) {
                    	$totalCredit = $dimensionData['totalCredit'];
                    	$totalDebit = $dimensionData['totalDebit'];
                    }

                    $netBal = floatval($totalCredit) - floatval($totalDebit);
                    $dataSet[$key3][$key4][$key5] = $netBal;
                }
            }
        }
        
        return $dataSet;
	}

	public function processExpenseDataForTable($dataSet, $fromDate, $toDate, $dimensionType) {
		foreach ($dataSet as $key3 => $value3) {
            foreach ($value3 as $key4 => $value4) {
                foreach ($value4 as $key5 => $value5) {
                    $arr = split(':', $key4);

                    $totalCredit = 0;
                    $totalDebit = 0;
                    $dataSet[$key3][$key4]['accName'] = $arr[1];
                    $dimensionData = $this->getModel('JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRangeForIncomeExpense($arr[0],$fromDate, $toDate, $dimensionType, $key5)->current();

                    if ($dimensionData) {
                    	$totalCredit = $dimensionData['totalCredit'];
                    	$totalDebit = $dimensionData['totalDebit'];
                    }

                    $netBal = floatval($totalDebit) - floatval($totalCredit);
                    $dataSet[$key3][$key4][$key5] = $netBal;
                }
            }
        }

       	return $dataSet;
	}

	public function processDimensionValueList($dimensionType,$dimensionValueData, $isCsv = false) {
		if ($dimensionType == "job") {
			if ($isCsv) {
	            $paginator = 1;
	            foreach ($dimensionValueData as $key => $value) {
	                $processedDimensionValSet['table_'.$paginator][] = [
	                    'dimensionValueID' => $value['jobId'],
	                    'dimensionValueName' => $value['jobReferenceNumber']
	                ];

	                if (!array_key_exists('table_'.$paginator, $finalNumTables)) {
	            		$finalNumTables['table_'.$paginator]['Income'] = [];
	            		$finalNumTables['table_'.$paginator]['Expense'] = [];
	            	}

	            }
	        } else {
	            $paginator = 1;
	            // $processedDimensionValSet[$paginator] = [];
	            $finalNumTables = [];
	            foreach ($dimensionValueData as $key => $value) {
	                if (sizeof($processedDimensionValSet['table_'.$paginator]) !== 10) {
	                   $processedDimensionValSet['table_'.$paginator][] = [
	                        'dimensionValueID' => $value['jobId'],
	                        'dimensionValueName' => $value['jobReferenceNumber']

	                   ]; 
	                } else {
	                    $paginator++;
	                    $processedDimensionValSet['table_'.$paginator][] = [
	                        'dimensionValueID' => $value['jobId'],
	                        'dimensionValueName' => $value['jobReferenceNumber']

	                   ];
	                }

	            	if (!array_key_exists('table_'.$paginator, $finalNumTables)) {
	            		$finalNumTables['table_'.$paginator]['Income'] = [];
	            		$finalNumTables['table_'.$paginator]['Expense'] = [];
	            	}
	            }
	        }
		} elseif ($dimensionType == "project") {
			if ($isCsv) {
	            $paginator = 1;
	            foreach ($dimensionValueData as $key => $value) {
	                $processedDimensionValSet['table_'.$paginator][] = [
	                    'dimensionValueID' => $value['projectId'],
	                    'dimensionValueName' => $value['projectCode']
	                ];

	                if (!array_key_exists('table_'.$paginator, $finalNumTables)) {
	            		$finalNumTables['table_'.$paginator]['Income'] = [];
	            		$finalNumTables['table_'.$paginator]['Expense'] = [];
	            	}

	            }
	        } else {

	            $paginator = 1;
	            // $processedDimensionValSet[$paginator] = [];
	            $finalNumTables = [];
	            foreach ($dimensionValueData as $key => $value) {
	                if (sizeof($processedDimensionValSet['table_'.$paginator]) !== 10) {
	                   $processedDimensionValSet['table_'.$paginator][] = [
	                        'dimensionValueID' => $value['projectId'],
	                        'dimensionValueName' => $value['projectCode']

	                   ]; 
	                } else {
	                    $paginator++;
	                    $processedDimensionValSet['table_'.$paginator][] = [
	                        'dimensionValueID' => $value['projectId'],
	                        'dimensionValueName' => $value['projectCode']

	                   ];
	                }

	            	if (!array_key_exists('table_'.$paginator, $finalNumTables)) {
	            		$finalNumTables['table_'.$paginator]['Income'] = [];
	            		$finalNumTables['table_'.$paginator]['Expense'] = [];
	            	}
	            }
	        }
		} else {
			if ($isCsv) {
	            $paginator = 1;
	            foreach ($dimensionValueData as $key => $value) {
	                $processedDimensionValSet['table_'.$paginator][] = [
	                    'dimensionValueID' => $value['dimensionValueId'],
	                    'dimensionValueName' => $value['dimensionValue']
	                ];

	                if (!array_key_exists('table_'.$paginator, $finalNumTables)) {
	            		$finalNumTables['table_'.$paginator]['Income'] = [];
	            		$finalNumTables['table_'.$paginator]['Expense'] = [];
	            	}

	            }
	        } else {

	            $paginator = 1;
	            // $processedDimensionValSet[$paginator] = [];
	            $finalNumTables = [];
	            foreach ($dimensionValueData as $key => $value) {
	                if (sizeof($processedDimensionValSet['table_'.$paginator]) !== 10) {
	                   $processedDimensionValSet['table_'.$paginator][] = [
	                        'dimensionValueID' => $value['dimensionValueId'],
	                        'dimensionValueName' => $value['dimensionValue']

	                   ]; 
	                } else {
	                    $paginator++;
	                    $processedDimensionValSet['table_'.$paginator][] = [
	                        'dimensionValueID' => $value['dimensionValueId'],
	                        'dimensionValueName' => $value['dimensionValue']

	                   ];
	                }

	            	if (!array_key_exists('table_'.$paginator, $finalNumTables)) {
	            		$finalNumTables['table_'.$paginator]['Income'] = [];
	            		$finalNumTables['table_'.$paginator]['Expense'] = [];
	            	}
	            }
	        }
		}

        $dataSet = [
        	'data' => $processedDimensionValSet,
        	'finalNumTables' => $finalNumTables
        ];


        return $dataSet;
    }
}