<?php

namespace Reporting\Controller\Generate;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

/**
 * @author Peushani Jayasekara <peushani@thinkcube.com>
 *
 */
class DeliveryReportController extends CoreController
{

    public function dailyDeliveryValueViewAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $cusCategory = $request->getPost('cusCategories');
            $result = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDailyDeliveryValuesByDate($fromDate, $toDate, $cusCategory);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Daily Delivery Value Report');
            $period = $fromDate . ' - ' . $toDate;

            $i = 0;
            $dailyDeliveryData = array();
            $grandTotal = 0;

            foreach ($result as $row) {
                if ($row['deliveryNoteID']) {
                    $dailyDeliveryData[$i]['deliveryNoteDeliveryDate'] = $row['deliveryNoteDeliveryDate'];
                    $dailyDeliveryData[$i]['deliveryNoteCode'] = $row['deliveryNoteCode'];
                    $dailyDeliveryData[$i]['deliveryNotePriceTotal'] = number_format($row['deliveryNotePriceTotal'], 2);
                    $grandTotal += $row['deliveryNotePriceTotal'];
                    $i++;
                }
            }


            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);


            $dailydeliveryView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryData' => $dailyDeliveryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($grandTotal, 2),
                'headerTemplate' => $headerViewRender,
            ));
            $dailydeliveryView->setTemplate('reporting/delivery-report/daily-delivery-table');
            $this->html = $dailydeliveryView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateDailyDeliveryValuePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cusCategories = $request->getPost('cusCategories');
            $result = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDailyDeliveryValuesByDate($fromDate, $toDate, $cusCategories);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Daily Delivery Value Report');
            $period = $fromDate . ' - ' . $toDate;

            $i = 0;
            $dailyDeliveryData = array();
            $grandTotal = 0;
            foreach ($result as $row) {
                if ($row['deliveryNoteID']) {
                    $dailyDeliveryData[$i]['deliveryNoteDeliveryDate'] = $row['deliveryNoteDeliveryDate'];
                    $dailyDeliveryData[$i]['deliveryNoteCode'] = $row['deliveryNoteCode'];
                    $dailyDeliveryData[$i]['deliveryNotePriceTotal'] = number_format($row['deliveryNotePriceTotal'], 2);
                    $grandTotal += $row['deliveryNotePriceTotal'];
                    $i++;
                }
            }

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailydeliveryView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryData' => $dailyDeliveryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($grandTotal, 2),
                'headerTemplate' => $headerViewRender,
            ));
            $dailydeliveryView->setTemplate('reporting/delivery-report/daily-delivery-table');
            $dailydeliveryView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailydeliveryView);
            $pdfPath = $this->downloadPDF('delivery-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateDailyDeliveryValueSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cusCategories = $request->getPost('cusCategories');

            $result = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDailyDeliveryValuesByDate($fromDate, $toDate, $cusCategories);
            $cD = $this->getCompanyDetails();

            $i = 0;
            $dailyDeliveryData = array();
            $grandTotal = 0;
            foreach ($result as $row) {
                if ($row['deliveryNoteID']) {
                    $dailyDeliveryData[$i]['deliveryNoteDeliveryDate'] = $row['deliveryNoteDeliveryDate'];
                    $dailyDeliveryData[$i]['deliveryNoteCode'] = $row['deliveryNoteCode'];
                    $dailyDeliveryData[$i]['deliveryNotePriceTotal'] = $row['deliveryNotePriceTotal'];
                    $grandTotal += $row['deliveryNotePriceTotal'];
                    $i++;
                }
            }
            $title = '';
            $tit = 'Daily Delivery Values Report';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("Date", "Delivery Note No", "Total Amount");
            $header = implode(",", $arrs);
            $arr = '';
            if ($dailyDeliveryData) {
                foreach ($dailyDeliveryData as $row) {
                    $in = array(
                        0 => $row['deliveryNoteDeliveryDate'],
                        1 => $row['deliveryNoteCode'],
                        2 => $row['deliveryNotePriceTotal'],
                    );
                    $arr.=implode(",", $in) . "\n";
                }
                $in = array(
                    0 => "",
                    1 => "Grand Total:",
                    2 => $grandTotal,
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Daily_Delivery_Values_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function _getDailyDeliveryItemData($fromDate, $toDate, $cusCategories = NULL)
    {
        if (isset($fromDate) && isset($toDate)) {

            $result = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDailyDeliveryItemByDate($fromDate, $toDate, $cusCategories);

            $dailyDeliveryItemData = [];
            foreach ($result as $value) {
                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($value['deliveryProductQuantity'], $productUom);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                $dailyDeliveryItemData[$value['productID']] = array_merge($value, array(
                    'quantity' => $quantity,
                    'uomAbbr' => $thisqty['uomAbbr']
                        )
                );
            }
            return $dailyDeliveryItemData;
        }
    }

    public function dailyDeliveryItemViewAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cusCategories = $request->getPost('cusCategories');
            $dailyDeliveryItemData = $this->_getDailyDeliveryItemData($fromDate, $toDate, $cusCategories);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Daily Delivery Item Quantity Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyDeliveryItemView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryItemData' => $dailyDeliveryItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $dailyDeliveryItemView->setTemplate('reporting/delivery-report/daily-delivery-item-table');
            $this->html = $dailyDeliveryItemView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateDailyDeliveryItemPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cusCategories = $request->getPost('cusCategories');
            $dailyDeliveryItemData = $this->_getDailyDeliveryItemData($fromDate, $toDate, $cusCategories);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('Daily Delivery Item Quantity Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyDeliveryItemView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryItemData' => $dailyDeliveryItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $dailyDeliveryItemView->setTemplate('reporting/delivery-report/daily-delivery-item-table');
            $dailyDeliveryItemView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailyDeliveryItemView);
            $pdfPath = $this->downloadPDF('delivery-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateDailyDeliveryItemSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cusCategories = $request->getPost('cusCategories');

            $dailyDeliveryItemData = $this->_getDailyDeliveryItemData($fromDate, $toDate, $cusCategories);
            $cD = $this->getCompanyDetails();

            $title = '';
            $tit = 'Daily Delivery Item Quantity Report';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("Item Code", "Item Name", "Item Quantity");
            $header = implode(",", $arrs);
            $arr = '';
            if ($dailyDeliveryItemData) {
                foreach ($dailyDeliveryItemData as $row) {

                    $in = array(
                        0 => $row['productCode'],
                        1 => $row['productName'],
                        2 => $row['quantity'] . ' ' . $row['uomAbbr'],
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Daily_Delivery_Item_Quantity_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function _getNotInvoiceDeliveryItemData($fromDate, $toDate)
    {
        if (isset($fromDate) && isset($toDate)) {
            $result = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getNotInvoiceDeliveryItemByDate($fromDate, $toDate);

            $dailyDeliveryItemData = [];
            foreach ($result as $value) {
                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($value['deliveryProductQuantity'], $productUom);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                $dailyDeliveryItemData[$value['productID']] = array_merge($value, array(
                    'quantity' => $quantity,
                    'uomAbbr' => $thisqty['uomAbbr']
                        )
                );
            }
            return $dailyDeliveryItemData;
        }
    }

    public function notInvoiceDeliveryItemViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $dailyDeliveryItemData = $this->_getNotInvoiceDeliveryItemData($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('Item quantity report for not Invoiced Delivery Notes');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyDeliveryItemView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryItemData' => $dailyDeliveryItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $dailyDeliveryItemView->setTemplate('reporting/delivery-report/daily-delivery-item-table');
            $this->html = $dailyDeliveryItemView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateNotInvoiceDeliveryItemPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $dailyDeliveryItemData = $this->_getNotInvoiceDeliveryItemData($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('Item quantity report for not Invoiced Delivery Notes');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyDeliveryItemView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryItemData' => $dailyDeliveryItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $dailyDeliveryItemView->setTemplate('reporting/delivery-report/daily-delivery-item-table');
            $dailyDeliveryItemView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailyDeliveryItemView);
            $pdfPath = $this->downloadPDF('delivery-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateNotInvoiceDeliveryItemSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $dailyDeliveryItemData = $this->_getNotInvoiceDeliveryItemData($fromDate, $toDate);
            $cD = $this->getCompanyDetails();

            $title = '';
            $tit = 'Item Quantity Report for not Invoiced Delivery Notes';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("Item Code", "Item Name", "Item Quantity");
            $header = implode(",", $arrs);
            $arr = '';
            if ($dailyDeliveryItemData) {
                foreach ($dailyDeliveryItemData as $row) {
                    $in = array(
                        0 => $row['productCode'],
                        1 => $row['productName'],
                        2 => $row['quantity'] . ' ' . $row['uomAbbr'],
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Item_Quantity_Report_for_not_Invoiced_Delivery_Notes";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function invoicedDeliveryNoteValuesViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Value report for invoiced Delivery Notes');
            $period = $fromDate . ' - ' . $toDate;

            $result = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getInvoicedDeliveryNoteVAluesData($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();
            $grandTotal = 0;
            if (isset($result)) {
                foreach ($result as $total) {
                    $grandTotal += $total['totalAmount'];
                }
            }

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyDeliveryNoteView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryNoteValueData' => $result,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($grandTotal, 2),
                'headerTemplate' => $headerViewRender,
            ));
            $dailyDeliveryNoteView->setTemplate('reporting/delivery-report/deliverynote-values-table');
            $this->html = $dailyDeliveryNoteView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Yashora <yashora@thinkcube.com>
     * generate a pdf for total values of invoiced delivery notes
     * @return JSrespondHtml
     */
    public function invoicedDeliveryNoteValuesPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Value report for invoiced Delivery Notes');
            $period = $fromDate . ' - ' . $toDate;

            $dailyDeliveryNoteValueData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getInvoicedDeliveryNoteVAluesData($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();
            $grandTotal = 0;
            foreach ($dailyDeliveryNoteValueData as $total) {
                $grandTotal += $total['totalAmount'];
            }

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyDeliveryNoteView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryNoteValueData' => $dailyDeliveryNoteValueData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($grandTotal, 2),
                'headerTemplate' => $headerViewRender,
            ));

            $dailyDeliveryNoteView->setTemplate('reporting/delivery-report/deliverynote-values-table');
            $dailyDeliveryNoteView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailyDeliveryNoteView);
            $pdfPath = $this->downloadPDF('delivery-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Yashora <yashora@thinkcube.com>
     * generate a SpreadSheet for total values of invoiced delivery notes
     * @return JSrespondHtml
     */
    public function invoicedDeliveryNoteValuesSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $result = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getInvoicedDeliveryNoteVAluesData($fromDate, $toDate);
            $cD = $this->getCompanyDetails();
            $grandTotal = 0;
            foreach ($result as $total) {
                $grandTotal += $total['totalAmount'];
            }

            $dailyDeliveryNoteValueData = $result;
            $title = '';
            $tit = 'Value Report for Invoiced Delivery Notes';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("Date", "Delivery Note Number", "Total Amount");
            $header = implode(",", $arrs);
            $arr = '';
            if ($dailyDeliveryNoteValueData) {
                foreach ($dailyDeliveryNoteValueData as $row) {
                    $in = array(
                        0 => $row['deliveryDate'],
                        1 => $row['deliveryNoteNumber'],
                        2 => $row['totalAmount'],
                    );
                    $arr.=implode(",", $in) . "\n";
                }

                $in = array(
                    0 => "",
                    1 => "Grand Total:",
                    2 => $grandTotal,
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "No related data found\n";
            }

            $name = "Value_Report_for_Invoiced_Delivery_Notes";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function _getInvoicedDeliveryNoteItemsData($fromDate, $toDate)
    {
        if (isset($fromDate) && isset($toDate)) {
            $result = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getInvoicedDeliveryNoteItemsData($fromDate, $toDate);
           
            $dailyDeliveryNoteItemData = [];

            if (!empty($result)) {
                foreach ($result as $value) {
                    //set quantity and uomAbbr according to display UOM
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);
                    $thisqty = $this->getProductQuantityViaDisplayUom($value['quantity'], $productUom);
                    $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                    $dailyDeliveryNoteItemData[$value['productID']] = array_merge($value, array(
                        'quantity' => $quantity,
                        'uomAbbr' => $thisqty['uomAbbr'])
                    );
                }
            }

            return $dailyDeliveryNoteItemData;
        }
    }

    public function invoicedDeliveryNoteItemsViewAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Item quantity report for invoiced Delivery Notes');
            $period = $fromDate . ' - ' . $toDate;

            $dailyDeliveryNoteItemData = $this->_getInvoicedDeliveryNoteItemsData($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyDeliveryNoteView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryNoteItemData' => $dailyDeliveryNoteItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $dailyDeliveryNoteView->setTemplate('reporting/delivery-report/deliverynote-items-table');

            $this->html = $dailyDeliveryNoteView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function invoicedDeliveryNoteItemsPdfAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Item quantity report for invoiced Delivery Notes');
            $period = $fromDate . ' - ' . $toDate;

            $dailyDeliveryNoteItemData = $this->_getInvoicedDeliveryNoteItemsData($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();


            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyDeliveryNoteView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryNoteItemData' => $dailyDeliveryNoteItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $dailyDeliveryNoteView->setTemplate('reporting/delivery-report/deliverynote-items-table');
            $dailyDeliveryNoteView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailyDeliveryNoteView);
            $pdfPath = $this->downloadPDF('delivery-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function invoicedDeliveryNoteItemsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $dailyDeliveryNoteItemData = $this->_getInvoicedDeliveryNoteItemsData($fromDate, $toDate);
            $cD = $this->getCompanyDetails();

            $title = '';
            $tit = 'Item Quantity Report For Invoiced Delivery Notes';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("Item Code", "Item Name", "Total Quantity");
            $header = implode(",", $arrs);
            $arr = '';
            if ($dailyDeliveryNoteItemData) {
                foreach ($dailyDeliveryNoteItemData as $row) {
                    $row['quantity'] = $row['quantity'] ? $row['quantity'] : 0;
                    $in = array(
                        0 => $row['itemCode'],
                        1 => $row['itemName'],
                        2 => $row['quantity'] . ' ' . $row['uomAbbr'],
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = "No related data found\n";
            }

            $name = "Item_Quantity_Report_For_Invoiced_Delivery_Notes";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function notInvoicedDeliveryNoteValuesViewAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $period = $fromDate . ' - ' . $toDate;
            $translator = new Translator();
            $name = $translator->translate('Value report for not Invoiced Delivery Notes');
//            $name = 'Value report for not Invoiced Delivery Notes';


            $result = $this->getNotInvoicedDeliveryNoteValuesData($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();
            $grandTotal = 0;
            foreach ($result as $total) {
                $grandTotal += $total['totalAmount'];
            }


            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);


            $dailyDeliveryNoteView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryNoteValueData' => $result,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($grandTotal, 2),
                'headerTemplate' => $headerViewRender,
            ));
            $dailyDeliveryNoteView->setTemplate('reporting/delivery-report/deliverynote-values-table');

            $this->html = $dailyDeliveryNoteView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function notInvoicedDeliveryNoteValuesPdfAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Value report for not Invoiced Delivery Notes');
//            $name = 'Value report for not Invoiced Delivery Notes';
            $period = $fromDate . ' - ' . $toDate;

            $dailyDeliveryNoteValueData = $this->getNotInvoicedDeliveryNoteValuesData($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();
            $grandTotal = 0;
            foreach ($dailyDeliveryNoteValueData as $total) {
                $grandTotal += $total['totalAmount'];
            }


            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);


            $dailyDeliveryNoteView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyDeliveryNoteValueData' => $dailyDeliveryNoteValueData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($grandTotal, 2),
                'headerTemplate' => $headerViewRender,
            ));

            $dailyDeliveryNoteView->setTemplate('reporting/delivery-report/deliverynote-values-table');
            $dailyDeliveryNoteView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailyDeliveryNoteView);
            $pdfPath = $this->downloadPDF('delivery-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function notInvoicedDeliveryNoteValuesSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');


            $result = $this->getNotInvoicedDeliveryNoteValuesData($fromDate, $toDate);
            $cD = $this->getCompanyDetails();
            $grandTotal = 0;
            foreach ($result as $total) {
                $grandTotal += $total['totalAmount'];
            }
            $dailyDeliveryNoteValueData = $result;

            $title = '';
            $tit = 'Value Report for not Invoiced Delivery Notes';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("Date", "Customer Name", "Delivery Note Number", "Total Amount");
            $header = implode(",", $arrs);
            $arr = '';
            if ($dailyDeliveryNoteValueData) {
                foreach ($dailyDeliveryNoteValueData as $row) {
                    $in = array(
                        0 => $row['deliveryDate'],
                        1 => $row['customerCode'].' - '. $row['customerName'],
                        2 => $row['deliveryNoteNumber'],
                        3 => $row['totalAmount'],
                    );
                    $arr.=implode(",", $in) . "\n";
                }
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "Grand Total:",
                    3 => $grandTotal,
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "No related data found\n";
            }

            $name = "Value_Report_For_NotInvoiced_Delivery_Notes";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    ////////////////////////
    private function getDeliveryNoteItemsDetails($postData) 
    {        
        $postData['isAllCustomers'] = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
        $postData['isAllItems'] = filter_var($postData['isAllItems'], FILTER_VALIDATE_BOOLEAN);
        $postData['isAllDeliveryNotes'] = filter_var($postData['isAllDeliveryNotes'], FILTER_VALIDATE_BOOLEAN);
        $postData['isTaxEnabled'] = filter_var($postData['isTaxEnabled'], FILTER_VALIDATE_BOOLEAN);
        $postData['isSortByCustomer'] = filter_var($postData['isSortByCustomer'], FILTER_VALIDATE_BOOLEAN);
        $postData['customerIds'] = ($postData['isAllCustomers']) ? [] : $postData['customerIds'];
        $postData['itemIds'] = ($postData['isAllItems']) ? [] : $postData['itemIds'];
        $postData['deliveryNoteIds'] = ($postData['isAllDeliveryNotes']) ? [] : $postData['deliveryNoteIds'];
        $postData['locationIds'] = ($postData['locationIds']) ? $postData['locationIds'] : [];
        $costingType = 'Avarage costing';
        $dataList = $this->CommonTable('Invoice\Model\DeliveryNoteTable')
                ->getDeliverNoteItemDetails($postData['fromDate'], $postData['toDate'], $postData['locationIds'], $postData['deliveryNoteIds'], $postData['customerIds'], $postData['itemIds'], $postData['isSortByCustomer']);

        //get all delivery note related itemOut details
        $documentType = 'Delivery Note';
        $deliveryNotItemOutDateSet = [];
        // check costing method
        $setup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        
        if (!$setup['averageCostingFlag']) {
            $costingType = 'FIFO costing';
            // get item in related item cost for fifo customers
            $itemInCostDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInQtyAndCostByItemOutDocumentType($documentType);
            $inItemCostValue = [];
            foreach ($itemInCostDetails as $inCost) {
                $inItemCostValue[$inCost['itemOutDocumentID']][$inCost['itemOutLocationProductID']] = floatval($inCost['totalValue'])/floatval($inCost['totalQt']);
            }
            
        }
            
        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDocumentType($documentType);
        foreach ($itemOutDetails as $key => $itemOutSingleValue) {
            if (!$setup['averageCostingFlag']) {
                $createdData = array(
                    'costing' => $inItemCostValue[$itemOutSingleValue['itemOutDocumentID']][$itemOutSingleValue['itemOutLocationProductID']],
                    'totalCost' => floatval($inItemCostValue[$itemOutSingleValue['itemOutDocumentID']][$itemOutSingleValue['itemOutLocationProductID']]) *(floatval($itemOutSingleValue['totalSoldQtty'])- floatval($itemOutSingleValue['totalReturn'])),
                );
            } else {
                $createdData = array(
                    'costing' => $itemOutSingleValue['itemOutAverageCostingPrice'],
                    'totalCost' => floatval($itemOutSingleValue['itemOutAverageCostingPrice']) *(floatval($itemOutSingleValue['totalSoldQtty'])- floatval($itemOutSingleValue['totalReturn'])),
                    );
                
            }
            $deliveryNotItemOutDateSet[$itemOutSingleValue['itemOutDocumentID']][$itemOutSingleValue['productID']] = $createdData;
        }
                

        $deliveryNoteDetails = [];
        foreach ($dataList as $data) {
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($data['productID']);
            $issuedDetails = $this->getProductQuantityViaDisplayUom($data['deliveryNoteProductQuantity'], $productUom);
            $returnedDetails = $this->getProductQuantityViaDisplayUom($data['deliveryNoteItemReturnQty'], $productUom);
            $unitPrice = $this->getProductUnitPriceViaDisplayUom($data['deliveryNoteProductPrice'], $productUom);
            $avgCostPrice = $this->getProductUnitPriceViaDisplayUom($deliveryNotItemOutDateSet[$data['deliveryNoteID']][$data['productID']]['costing'], $productUom);
            $productTaxDetails = $this->CommonTable('Invoice\Model\DeliveryNoteProductTaxTable')->getDeliveryNoteTotalProductTax($data['deliveryNoteProductID']);
            $unitTax = $this->getProductUnitPriceViaDisplayUom($productTaxDetails['deliveryNoteProductTaxAmount'], $productUom) / $issuedDetails['quantity'];            
            $totalProductTax = $unitTax * $issuedDetails['quantity'];
            $totalReturnedTax = $unitTax * $returnedDetails['quantity'];
            
            if (!array_key_exists($data['deliveryNoteID'], $deliveryNoteDetails)) {
                $deliveryNoteDetails[$data['deliveryNoteID']] = [
                    'code' => $data['deliveryNoteCode'],
                    'date' => $data['deliveryNoteDeliveryDate'],
                    'customer' => $data['customerName'] . ' (' . $data['customerCode'] . ')',
                    'customerProfile' => $data['customerProfileName'],
                    'amount' => $data['deliveryNotePriceTotal'],
                    'totalTax' => 0
                ];
            }
            //for deduct delivery note product tax
            $itemRemainingTaxAmount = 0;
            if (!$postData['isTaxEnabled']) {
                $deliveryNoteDetails[$data['deliveryNoteID']]['totalTax'] += $totalProductTax;
                $itemRemainingTaxAmount = $totalProductTax - $totalReturnedTax;
            }            
            $deliveryNoteDetails[$data['deliveryNoteID']]['itemData'][] = [
                'itemName' => $data['productName'] . ' (' . $data['productCode'] . ')',
                'itemUOM' => $issuedDetails['uomAbbr'],
                'itemTotal' => $data['deliveryNoteProductTotal'],
                'itemIssuedQty' => $issuedDetails['quantity'],
                'itemReturnedQty' => $returnedDetails['quantity'],
                'itemPrice' => $unitPrice,
                'itemReturnedAmount' => $data['deliveryNoteItemReturnAmount'],
                'itemRemainingTaxAmount' => $itemRemainingTaxAmount,
                'unitCost' => $avgCostPrice,
                'totalCost' => $deliveryNotItemOutDateSet[$data['deliveryNoteID']][$data['productID']]['totalCost'],
            ];
        }
        $returnDataSet['deliveryDetails'] = $deliveryNoteDetails;
        $returnDataSet['deliveryType'] = $costingType;
        
        return $returnDataSet;
    }

    public function deliveryNoteItemsDetailReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $postData = $request->getPost()->toArray();            
            $dataList = $this->getDeliveryNoteItemsDetails($postData);
            $translator = new Translator();
            $name = $translator->translate('Delivery Note Item Details Report ('.$dataList['deliveryType'].')');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList['deliveryDetails'],
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/delivery-report/delivery-item-detail-report');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }
    
    public function deliveryNoteItemsDetailPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {            
            $postData = $request->getPost()->toArray();            
            $dataList = $this->getDeliveryNoteItemsDetails($postData);
            
            $translator = new Translator();
            $name = $translator->translate('Delivery Note Item Details Report ('.$dataList['deliveryType'].')');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList['deliveryDetails'],
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/delivery-report/delivery-item-detail-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('delivery-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }
    
    public function deliveryNoteItemsDetailCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {            
            $postData = $request->getPost()->toArray();            
            $dataList = $this->getDeliveryNoteItemsDetails($postData);
            $cD = $this->getCompanyDetails();
            
            $translator = new Translator();
            $period = $postData['fromDate'] ." - ". $postData['toDate'];
            
            $title = '';
            $tit = 'DELIVERY NOTE ITEM DETAIL REPORT ('.$dataList['deliveryType'].')';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $period
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array(
                "Delivery Note Code", 
                "Date", 
                "Customer",
                "Customer Profile",
                "Amount",
                "Item Name",
                "Issued Price",
                "Unit Cost",
                "UOM",
                "Issued Quantity",
                "Returned Quantity",
                "Total Cost",
                "Remaining Item Amount"
            );
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($dataList['deliveryDetails'])) {
                
                $totalDeliveryNoteAmount = $totalDeliveryNoteItemAmount = 0;
                $totalDNoteIssuesQty  = 0;
                $totalDNoteIreturnQty = 0;
                $totalDNoteTotalCost = 0;
                foreach ($dataList['deliveryDetails'] as $data) {
                    $totalDeliveryNoteAmount += $data['amount'];
                    $totalDeliveryNoteAmount -= $data['totalTax'];
                    
                    $in = array(
                        0 => $data['code'],
                        1 => $data['date'],
                        2 => $data['customer'],
                        3 => $data['customerProfile'],
                        4 => number_format(($data['amount'] - $data['totalTax']), 2, '.', ''),
                        5 => '',
                        6 => '',
                        7 => '',
                        8 => '',
                        9 => '',
                        10 => '',
                        11 => '',
                        12 => ''
                    );
                    $arr.=implode(",", $in) . "\n";
                    
                    $totalItemAmount = 0;
                    $dNoteIssuesQty  = 0;
                    $dNoteIreturnQty = 0;
                    $dNotetotalCost = 0;
                    foreach ($data['itemData'] as $itemData) {
                        $itemValue = ($itemData['itemTotal'] - $itemData['itemReturnedAmount'] - $itemData['itemRemainingTaxAmount']);
                        $totalItemAmount += $itemValue;
                        $totalDeliveryNoteItemAmount += $itemValue;
                        $dNoteIssuesQty += $itemData['itemIssuedQty'];
                        $dNoteIreturnQty += $itemData['itemReturnedQty'];
                        $totalDNoteIssuesQty += $itemData['itemIssuedQty'];
                        $totalDNoteIreturnQty += $itemData['itemReturnedQty'];
                        $dNotetotalCost += $itemData['totalCost'];
                        $totalDNoteTotalCost +=$itemData['totalCost'];
                        
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => $itemData['itemName'],
                            6 => number_format($itemData['itemPrice'], 2, '.', ''),
                            7 => number_format($itemData['unitCost'],2,'.', ''),
                            8 => $itemData['itemUOM'],
                            9 => number_format($itemData['itemIssuedQty'], 2, '.', ''),
                            10 => number_format($itemData['itemReturnedQty'], 2, '.', ''),
                            11 => number_format($itemData['totalCost'],2,'.', ''),
                            12 => number_format($itemValue, 2, '.', '')
                        );
                        $arr.=implode(",", $in) . "\n";                        
                    }
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => '',
                        7 => '',
                        8 => 'Delivery Note Item',
                        9 => number_format($dNoteIssuesQty, 2, '.', ''),
                        10 => number_format($dNoteIreturnQty, 2, '.', ''),
                        11 => number_format($dNotetotalCost,2, '.', ''),
                        12 => number_format($totalItemAmount, 2, '.', '')
                    );
                    $arr.=implode(",", $in) . "\n";
                }
                $in = array(
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => 'Total Delivery Note Amount',
                    4 => number_format($totalDeliveryNoteAmount, 2, '.', ''),
                    5 => '',
                    6 => '',
                    7 => '',
                    8 => 'Total Delivery Note Item Amount',
                    9 => number_format($totalDNoteIssuesQty, 2, '.', ','),
                    10 => number_format($totalDNoteIreturnQty, 2, '.', ','),
                    11 => number_format($totalDNoteTotalCost, 2, '.', ''),
                    12 => number_format($totalDeliveryNoteItemAmount, 2, '.', '')
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "No related data found";
            }            
            $name = "Delivery_Note_Item_Detail_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }
    ////////////////////////

    private function getDeliveryNoteItemsSummaryDetails($postData) 
    {        
        $postData['isAllCustomers'] = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
        $postData['isAllItems'] = filter_var($postData['isAllItems'], FILTER_VALIDATE_BOOLEAN);
        $postData['isAllDeliveryNotes'] = filter_var($postData['isAllDeliveryNotes'], FILTER_VALIDATE_BOOLEAN);
        $postData['isTaxEnabled'] = filter_var($postData['isTaxEnabled'], FILTER_VALIDATE_BOOLEAN);
        $postData['isSortByCustomer'] = filter_var($postData['isSortByCustomer'], FILTER_VALIDATE_BOOLEAN);
        $postData['customerIds'] = ($postData['isAllCustomers']) ? [] : $postData['customerIds'];
        $postData['itemIds'] = ($postData['isAllItems']) ? [] : $postData['itemIds'];
        $postData['deliveryNoteIds'] = ($postData['isAllDeliveryNotes']) ? [] : $postData['deliveryNoteIds'];
        $postData['locationIds'] = ($postData['locationIds']) ? $postData['locationIds'] : [];

        $dataList = $this->CommonTable('Invoice\Model\DeliveryNoteTable')
                ->getDeliverNoteItemDetails($postData['fromDate'], $postData['toDate'], $postData['locationIds'], $postData['deliveryNoteIds'], $postData['customerIds'], $postData['itemIds'], $postData['isSortByCustomer']);
        $deliveryData = [];
        $deliveryNoteDetails = [];
        foreach ($dataList as $data) {
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($data['productID']);
            $issuedDetails = $this->getProductQuantityViaDisplayUom($data['deliveryNoteProductQuantity'], $productUom);
            $returnedDetails = $this->getProductQuantityViaDisplayUom($data['deliveryNoteItemReturnQty'], $productUom);
            $unitPrice = $this->getProductUnitPriceViaDisplayUom($data['deliveryNoteProductPrice'], $productUom);
            $productTaxDetails = $this->CommonTable('Invoice\Model\DeliveryNoteProductTaxTable')->getDeliveryNoteTotalProductTax($data['deliveryNoteProductID']);
            $unitTax = $this->getProductUnitPriceViaDisplayUom($productTaxDetails['deliveryNoteProductTaxAmount'], $productUom) / $issuedDetails['quantity'];            
            $totalProductTax = $unitTax * $issuedDetails['quantity'];
            $totalReturnedTax = $unitTax * $returnedDetails['quantity'];

            $itemRemainingTaxAmount = 0;
            if ($postData['isTaxEnabled']) {
                $deliveryNoteDetails[$data['deliveryNoteID']]['totalTax'] += $totalProductTax;
                $itemRemainingTaxAmount = $totalProductTax - $totalReturnedTax;
            }            
            $deliveryData[$data['customerID']]['details'] = array(
                'name' => $data['customerName'],
                'code' => $data['customerCode']
            );
          
            $uPrice = (($issuedDetails['quantity']*$unitPrice) 
                    + ($deliveryData[$data['customerID']]['dataSet'][$data['productID']]['specialUPrice'] * $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['IssuedAllQty']))/($deliveryData[$data['customerID']]['dataSet'][$data['productID']]['IssuedAllQty'] 
                    + $issuedDetails['quantity']);
            
            $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['itemName'] = $data['productName'] . ' (' . $data['productCode'] . ')';
            $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['itemUOM'] = $issuedDetails['uomAbbr'];
            $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['specialUPrice'] = $uPrice;
            $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['IssuedAllQty'] += $issuedDetails['quantity']; 
            $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['returnedAllQty'] +=  $returnedDetails['quantity'];
            $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['remainingAmount'] = (
                $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['IssuedAllQty'] - $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['returnedAllQty']) * $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['specialUPrice'];
           
            $deliveryData[$data['customerID']]['dataSet'][$data['productID']]['remainingTaxAmount'] += 
                    $itemRemainingTaxAmount; 
            $deliveryData[$data['customerID']]['deliveryNotes'][] = $data['deliveryNoteCode']; 

        }

        return $deliveryData;
    }

    public function deliveryNoteItemsDetailSummaryReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $postData = $request->getPost()->toArray();            
            $dataList = $this->getDeliveryNoteItemsSummaryDetails($postData);
            
            $translator = new Translator();
            $name = $translator->translate('Delivery Note Item Details Summary Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/delivery-report/delivery-item-detail-summary-report');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }
    
    public function deliveryNoteItemsDetailSummaryPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {            
            $postData = $request->getPost()->toArray();            
            $dataList = $this->getDeliveryNoteItemsSummaryDetails($postData);
            
            $translator = new Translator();
            $name = $translator->translate('Delivery Note Item Details Summary Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/delivery-report/delivery-item-detail-summary-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('delivery-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }
    
    public function deliveryNoteItemsSummaryDetailCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {            
            $postData = $request->getPost()->toArray();            
            $dataList = $this->getDeliveryNoteItemsSummaryDetails($postData);
            $cD = $this->getCompanyDetails();
            
            $translator = new Translator();
            $period = $postData['fromDate'] ." - ". $postData['toDate'];
            
            $title = '';
            $tit = 'DELIVERY NOTE ITEM SUMMARY DETAIL REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $period
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array(
                "Customer",
                "Product Code-Name",
                "Unit Price", 
                "UOM", 
                "Issued Quantity",
                "Returned Quantity",
                "Remaining Item Amount"
                
            );
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($dataList)) {
                
                $totalDeliveryNoteAmount  = 0;
                foreach ($dataList as $data) {
                    $deliveryNoteCode = implode(" ", array_unique($data['deliveryNotes']));
                                        
                    $in = array(
                        0 => $data['details']['name'].'-'.$data['details']['code'],
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => '',
                        7 => ''
                     
                    );
                    $arr.=implode(",", $in) . "\n";
                    
                    $totalItemAmount = 0;
                    $totalItemTaxAmount = 0;
                    foreach ($data['dataSet'] as $itemData) {
                        $totalItemAmount += $itemData['remainingAmount'];
                        $totalItemTaxAmount += $itemData['remainingTaxAmount'];
                        $totalValue = $itemData['remainingAmount'] + $itemData['remainingTaxAmount'];
                        $cusWiseTotal = $totalItemAmount + $totalItemTaxAmount;
                        
                        
                        $in = array(
                            0 => '',
                            1 => $itemData['itemName'],
                            2 => number_format($itemData['specialUPrice'], 2, '.', ''),
                            3 => $itemData['itemUOM'],
                            4 => number_format($itemData['IssuedAllQty'], 2, '.', ''),
                            5 => number_format($itemData['returnedAllQty'], 2, '.', ''),
                            6 => number_format($totalValue, 2, '.', '')
                            
                        );
                        $arr.=implode(",", $in) . "\n";                        
                    }
                    $in = array(
                        0 => 'Delivery Note Codes',
                        1 => $deliveryNoteCode,
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => 'Delivery Note customer wise Total',
                        6 => number_format($cusWiseTotal, 2, '.', ''),
                    );
                    $arr.=implode(",", $in) . "\n";
                    $totalDeliveryNoteAmount += $cusWiseTotal;
                }
                $in = array(
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => '',
                    4 => '',
                    5 => 'Total Delivery Note Item Amount',
                    6 => number_format($totalDeliveryNoteAmount, 2, '.', '')
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "No related data found";
            }            
            $name = "Delivery_Note_Item_Detail_Summary_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getDeliveryNoteWIseInvoiceDetails($postData)
    {
        $postData['isAllDeliveryNotes'] = filter_var($postData['isAllDeliveryNotes'], FILTER_VALIDATE_BOOLEAN);
        $postData['deliveryNoteIds'] = ($postData['isAllDeliveryNotes']) ? [] : $postData['deliveryNoteIds'];

        $dnInvoices = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteWiseInvoices($postData['fromDate'], $postData['toDate'], $postData['deliveryNoteIds']);

        $dataList = [];
        foreach ($dnInvoices as $invoice) {
            if (!array_key_exists($invoice['deliveryNoteID'], $dataList)) {
                $dataList[$invoice['deliveryNoteID']] = [
                    'deliveryNoteCode' => $invoice['deliveryNoteCode'],
                    'deliveryNoteDeliveryDate' => $invoice['deliveryNoteDeliveryDate'],
                    'deliveryNotePriceTotal' => $invoice['deliveryNotePriceTotal'],
                    'deliveryNoteStatus' => $invoice['dnStatus']
                ];
            }
            $dataList[$invoice['deliveryNoteID']]['invoiceData'][] = [
                'salesInvoiceCode' => $invoice['salesInvoiceCode'],
                'salesInvoiceIssuedDate' => $invoice['salesInvoiceIssuedDate'],
                'salesinvoiceTotalAmount' => $invoice['salesinvoiceTotalAmount'],
                'salesinvoiceCopiedAmount' => $invoice['copiedAmount'],
                'salesinvoiceStatus' => $invoice['siStatus'],
            ];
        }
        return $dataList;
    }

    public function deliveryNoteWiseInvoiceReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getDeliveryNoteWIseInvoiceDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Delivery Note Wise Invoice Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/delivery-report/delivery-note-wise-invoice-report');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function deliveryNoteWiseInvoicePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getDeliveryNoteWIseInvoiceDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Delivery Note Wise Invoice Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/delivery-report/delivery-note-wise-invoice-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('delivery-note-wise-invoice-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function deliveryNoteWiseInvoiceCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getDeliveryNoteWIseInvoiceDetails($postData);
            $companyDetails = $this->getCompanyDetails();

            if ($dataList) {
                $data = ['Delivery Note Wise Invoice Report'];
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = [
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                ];
                $title.=implode(",", $data) . "\n";

                $data = ["Period :" . $postData['fromDate'] . " - " . $postData['toDate']];
                $title.=implode(",", $data) . "\n";

                $tableHead = [
                    'Delivery Note Code','Delivery Note Date','Delivery Note Amount',
                    'Delivery Note Status','Invoice Code','Invoice Date','Invoice Status',
                    'Total Invoice Amount','Copied Invoice Amount'
                ];
                $header.=implode(",", $tableHead) . "\n";

                foreach ($dataList as $dnData) {
                    $data = [
                        $dnData['deliveryNoteCode'],
                        $dnData['deliveryNoteDeliveryDate'],
                        number_format($dnData['deliveryNotePriceTotal'], 2, '.', ''),
                        $dnData['deliveryNoteStatus']
                    ];
                    $arr.=implode(",", $data) . "\n";
                    $totalCopiedAmount = 0;
                    foreach ($dnData['invoiceData'] as $invoiceData) {                        
                        $data = [
                            '', '', '', '',
                            $invoiceData['salesInvoiceCode'],
                            $invoiceData['salesInvoiceIssuedDate'],
                            $invoiceData['salesinvoiceStatus'],
                            number_format($invoiceData['salesinvoiceTotalAmount'], 2, '.', ''),
                            number_format($invoiceData['salesinvoiceCopiedAmount'], 2, '.', ''),
                        ];
                        $arr.=implode(",", $data) . "\n";
                        $totalCopiedAmount += $invoiceData['salesinvoiceCopiedAmount'];
                    }
                    $data = [
                        '', '', '', '', '', '', '', 'Total Copied Amount :',
                        number_format($totalCopiedAmount, 2, '.', '')
                    ];
                    $arr.=implode(",", $data) . "\n";
                }

            } else {
                $arr = "no matching records found";
            }

            $name = "Delivery-Note-Wise-Invoice-Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getNotInvoicedDeliveryNoteValuesData($fromDate, $toDate)
    {
        $result = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getNotInvoicedDeliveryNoteValuesData($fromDate, $toDate);

        $partialInvoicedData = [];

        foreach ($result as $val) {
            $partialInvoicedData['deliveryDate'] = $val['deliveryDate'];
            $partialInvoicedData['deliveryNoteNumber'] = $val['deliveryNoteNumber'];
            $partialInvoicedData['totalAmount'] = $val['totalAmount'] - $val['deliveryNoteCopiedTotal'];
            $partialInvoicedData['documentTypeID'] = $val['documentTypeID'];
            $partialInvoicedData['salesInvoiceProductTotal'] = $val['salesInvoiceProductTotal'];
            $partialInvoicedData['customerCode'] = $val['customerCode'];
            $partialInvoicedData['customerName'] = $val['customerName'];
        
            $resultArray[] = $partialInvoicedData;
        }
        
        return $resultArray;
    }

    public function editedDeliveryNoteReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $users = $request->getPost('users');
            $translator = new Translator();
            $name = $translator->translate('Edited Delivery Note Report');
            $period = $fromDate . ' - ' . $toDate;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $editedDeliveryNoteData = $this->_getEditedDeliveryNoteData($fromDate, $toDate, $users, $customerIds);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $editedDeliveryNoteView = new ViewModel(array(
                'cD' => $companyDetails,
                'editedDeliveryNoteData' => $editedDeliveryNoteData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $editedDeliveryNoteView->setTemplate('reporting/delivery-report/generate-edited-deliveryNote-pdf');

            $this->html = $editedDeliveryNoteView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function editedDeliveryNotePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $users = $request->getPost('users');
            $translator = new Translator();
            $name = $translator->translate('Edited Delivery Note Report');
            $period = $fromDate . ' - ' . $toDate;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $editedDeliveryNoteData = $this->_getEditedDeliveryNoteData($fromDate, $toDate, $users, $customerIds);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $editedDeliveryNoteView = new ViewModel(array(
                'cD' => $companyDetails,
                'editedDeliveryNoteData' => $editedDeliveryNoteData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $editedDeliveryNoteView->setTemplate('reporting/delivery-report/generate-edited-deliveryNote-pdf');
            $editedDeliveryNoteView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($editedDeliveryNoteView);
            $pdfPath = $this->downloadPDF('edited-delivery-note-pdf', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function editedDeliveryNoteSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $users = $request->getPost('users');
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;
            $editedDeliveryNoteData = $this->_getEditedDeliveryNoteData($fromDate, $toDate, $users, $customerIds);
            $cD = $this->getCompanyDetails();

            if (count($editedDeliveryNoteData) > 0) {
                $header = '';
                $reportTitle = [ "", 'EDITED DELIVERY NOTE REPORT', "",];
                $header = implode(',', $reportTitle) . "\n";

                $header .= $cD[0]->companyName . "\n";
                $header .= $cD[0]->companyAddress . "\n";
                $header .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $peroidRow = ["Period : " . $fromDate . '-' . $toDate];
                $header .= implode(",", $peroidRow) . "\n";
                $header .= implode(",", [""]) . "\n";

                $columnTitlesArray = ['Delivery Note Code',
                    'Customer Name',
                    'Edited Date',
                    'Edited User',
                    'Changed Category',
                    'Changed Details'];
                $columnTitles = implode(",", $columnTitlesArray);

                $csvRow = '';
                foreach ($editedDeliveryNoteData as $data) {
                    $tableRow = [
                        $data['deliveryNoteCode'],
                        str_replace(',', "", $data['deliveryNoteCustomerName']),
                        $data['dateAndTime'],
                        $data['createdUser'],
                    ];
                    $csvRow .= implode(",", $tableRow) . "\n";
                    foreach ($data['data'] as $value) {
                        $tableRow = [
                            "", "", "", "",
                            $value['category'],
                            $value['changed'],
                        ];
                        $csvRow .= implode(",", $tableRow) . "\n";
                    }
                }
            } else {
                $csvRow = "No matching records found \n";
            }

            $csvContent = $this->csvContent($header, $columnTitles, $csvRow);
            $csvPath = $this->generateCSVFile('Edited_dln_Report_csv', $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function _getEditedDeliveryNoteData($fromDate = NULL, $toDate = NULL, $users = [], $customerIds)
    {
        if (isset($fromDate) && isset($toDate)) {
            $plusToDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
            $editedDeliveryNotes = $this->CommonTable('Invoice/Model/DeliveryNoteEditLogDetailsTable')->getEditedDeliveryNoteDetails($fromDate, $plusToDate, $users, $customerIds);

            $editedDeliveryNoteData = [];
            foreach ($editedDeliveryNotes as $eDln) {
                if (isset($eDln['deliveryNoteEditLogID'])) {
                    $editedDeliveryNoteData[$eDln['deliveryNoteEditLogID']]['deliveryNoteCode'] = $eDln['deliveryNoteCode'];
                    $editedDeliveryNoteData[$eDln['deliveryNoteEditLogID']]['deliveryNoteCustomerName'] = $eDln['customerName'];
                    $editedDeliveryNoteData[$eDln['deliveryNoteEditLogID']]['dateAndTime'] = $this->getUserDateTime($eDln['dateAndTime'], 'Y-m-d H:i:s');
                    $editedDeliveryNoteData[$eDln['deliveryNoteEditLogID']]['createdUser'] = $eDln['createdUser'];
                    $editedDeliveryNoteData[$eDln['deliveryNoteEditLogID']]['data'][$eDln['deliveryNoteEditLogDetailsID']] = [
                        'changed' => $eDln['deliveryNoteEditLogDetailsNewState'],
                        'category' => $eDln['deliveryNoteEditLogDetailsCategory']
                    ];
                }
            }

            return $editedDeliveryNoteData;
        }
    }

    public function deliveryNoteWiseSalesReturnReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getDeliveryNoteWIseSalesReturnDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Delivery Note Wise Sales Return Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/delivery-report/delivery-note-wise-sales-return-report');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    private function getDeliveryNoteWIseSalesReturnDetails($postData)
    {
        $postData['isAllDeliveryNotes'] = filter_var($postData['isAllDeliveryNotes'], FILTER_VALIDATE_BOOLEAN);
        $postData['deliveryNoteIds'] = ($postData['isAllDeliveryNotes']) ? [] : $postData['deliveryNoteIds'];

        $dnInvoices = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteWiseSalesReturn($postData['fromDate'], $postData['toDate'], $postData['deliveryNoteIds']);

        $dataList = [];
        foreach ($dnInvoices as $invoice) {
            if (!array_key_exists($invoice['deliveryNoteID'], $dataList)) {
                $dataList[$invoice['deliveryNoteID']] = [
                    'deliveryNoteCode' => $invoice['deliveryNoteCode'],
                    'deliveryNoteDeliveryDate' => $invoice['deliveryNoteDeliveryDate'],
                    'deliveryNotePriceTotal' => $invoice['deliveryNotePriceTotal'],
                    'deliveryNoteStatus' => $invoice['dnStatus']
                ];
            }
            $dataList[$invoice['deliveryNoteID']]['returnData'][] = [
                'salesReturnCode' => $invoice['salesReturnCode'],
                'salesReturnDate' => $invoice['salesReturnDate'],
                'salesReturnTotal' => $invoice['salesReturnTotal'],
                'salesReturnCopiedAmount' => $invoice['copiedAmount'],
                'salesReturnStatus' => $invoice['siStatus'],
            ];
        }
        return $dataList;
    }

    public function deliveryNoteWiseSalesReturnPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getDeliveryNoteWIseSalesReturnDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Delivery Note Wise Sales Return Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/delivery-report/delivery-note-wise-sales-return-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('delivery-note-wise-sales-return-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function deliveryNoteWiseSalesReturnCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getDeliveryNoteWIseSalesReturnDetails($postData);
            $companyDetails = $this->getCompanyDetails();

            if ($dataList) {
                $data = ['Delivery Note Wise Sales Return Report'];
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = [
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                ];
                $title.=implode(",", $data) . "\n";

                $data = ["Period :" . $postData['fromDate'] . " - " . $postData['toDate']];
                $title.=implode(",", $data) . "\n";

                $tableHead = [
                    'Delivery Note Code','Delivery Note Date','Delivery Note Amount',
                    'Delivery Note Status','Sales Return Code','Sales Return Date','Sales Return Status',
                    'Total Sales Return Amount','Copied Sales Return Amount'
                ];
                $header.=implode(",", $tableHead) . "\n";

                foreach ($dataList as $dnData) {
                    $data = [
                        $dnData['deliveryNoteCode'],
                        $dnData['deliveryNoteDeliveryDate'],
                        number_format($dnData['deliveryNotePriceTotal'], 2, '.', ''),
                        $dnData['deliveryNoteStatus']
                    ];
                    $arr.=implode(",", $data) . "\n";
                    $totalCopiedAmount = 0;
                    foreach ($dnData['returnData'] as $returnData) {                        
                        $data = [
                            '', '', '', '',
                            $returnData['salesReturnCode'],
                            $returnData['salesReturnDate'],
                            $returnData['salesReturnStatus'],
                            number_format($returnData['salesReturnTotal'], 2, '.', ''),
                            number_format($returnData['salesReturnCopiedAmount'], 2, '.', ''),
                        ];
                        $arr.=implode(",", $data) . "\n";
                        $totalCopiedAmount += $returnData['salesReturnCopiedAmount'];
                    }
                    $data = [
                        '', '', '', '', '', '', '', 'Total Copied Amount :',
                        number_format($totalCopiedAmount, 2, '.', '')
                    ];
                    $arr.=implode(",", $data) . "\n";
                }

            } else {
                $arr = "no matching records found";
            }

            $name = "Delivery-Note-Wise-Sales-return-Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

}
