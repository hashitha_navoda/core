<?php

namespace Reporting\Controller\Generate;

use Core\Controller\API\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;

class PaymentReportController extends CoreController
{

    protected $userID;
    protected $salesByInvoiceTable;
    protected $productTable;
    protected $paymentsTable;
    protected $loggerTable;
    protected $userTable;
    protected $taxAmountTable;

    public function monthlyPaymentViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');
            $period = $fromMonth . ' - ' . $toMonth;
            $translator = new Translator();
            $name = $translator->translate('Monthly Payment Report');

            $respondAmount = $this->getMonthlyPaymentDetails($fromMonth, $toMonth, $sort, $paymentNumber, $locationNumber);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/payment-report/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $monthlyPaymentView = new ViewModel(array(
                'monthlyPaymentData' => $respondAmount,
                'fromMonth' => $fromMonth,
                'toMonth' => $toMonth,
                'headerTemplate' => $headerViewRender,
                'cD' => $companyDetails));


            $monthlyPaymentView->setTemplate('reporting/payment-report/generate-monthly-payment-pdf');

            $this->setLogMessage("Retrive monthly supplier payment details for monthly supplier payments view report.");
            $this->html = $monthlyPaymentView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getMonthlyPaymentDetails($fromMonth = null, $toMonth = null, $sort = null, $paymentNumber = null, $locationNumber = null)
    {
        if (isset($fromMonth) && isset($toMonth)) {
            // $respondAmount = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getMonthlyPaymentData($fromMonth, $toMonth, $paymentNumber, $locationNumber);

            $respondAmount = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getMonthlyPaymentDataForNewReport($fromMonth, $toMonth, $paymentNumber, $locationNumber);
            $respondAmountData = $respondAmount ? $respondAmount : array();

            $processedRespondAmountData = [];


            foreach ($respondAmountData as $key => $value) {

                $paymentID = $value['outgoingPaymentID'];
                $paymentMethods = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPaymentMethodDetailsByPaymentIdAndPaymentMethodId($paymentID, $paymentNumber);
                $dataOfPaymentMethod = [];
                $totalPaid = 0;
                foreach ($paymentMethods as $paymethods) {
                    $paymethods = (object) $paymethods;
                    $paymentMethodName = '';
                    switch ($paymethods->outGoingPaymentMethodID) {
                        case '0':
                            $paymentMethodName = 'Credit';                        
                            break;
                        case '1':
                            $paymentMethodName = 'Cash';                        
                            break;
                        case '2':
                            $paymentMethodName = 'Cheque';
                            break;
                        case '3':
                            $paymentMethodName = 'Card';
                            break;
                        case '4':
                            $paymentMethodName = 'Loyalty Card';
                            break;
                        case '5':
                            $paymentMethodName = 'Bank Transfer';
                            break;
                        case '6':
                            $paymentMethodName = 'Gift Card';
                            break;
                        case '7':
                            $paymentMethodName = 'LC';
                            break;
                        case '8':
                            $paymentMethodName = 'TT';
                            break;
                        default:
                            break;
                    }

                    $totalPaid += floatval($paymethods->outGoingPaymentMethodPaidAmount);
                    $dataOfPaymentMethod[] = [
                        'paymentMethodID' => $paymethods->outGoingPaymentMethodID,
                        'paymentMethodName' => $paymentMethodName,
                        'paidAmount' => $paymethods->outGoingPaymentMethodPaidAmount
                    ];
                }


                if (floatval($value['outgoingPaymentCreditAmount']) > 0) {

                    $totalPaid += $value['outgoingPaymentCreditAmount'];
                    $dataOfPaymentMethod[] = [
                        'paymentMethodID' => '0',
                        'paymentMethodName' => 'Credit',
                        'paidAmount' => $value['outgoingPaymentCreditAmount']
                    ];
                }


                if ($value['outgoingPaymentType'] == 'invoice') {

                    $paymentInvoice = $this->CommonTable('SupplierInvoicePaymentsTable')->getPIDataByPaymentID($paymentID);


                    if (sizeof($paymentInvoice) > 0) {

                        $relatedPVs = '';

                        foreach ($paymentInvoice as $key55 => $value55) {

                            if ((sizeof($paymentInvoice)-1) == $key55) {
                                $relatedPVs .= $value55['purchaseInvoiceCode'];
                            } else {
                                $relatedPVs .= $value55['purchaseInvoiceCode'].'/';
                            }
                        }
                    } else {
                        $relatedPVs = '-';
                    }
                } else {
                    $relatedPVs = '-';
                }
                
                $respondAmountData[$key]['relatedPVs'] = $relatedPVs;
                $respondAmountData[$key]['payData'] = $dataOfPaymentMethod;
                $respondAmountData[$key]['actTotal'] = $totalPaid;

            }


            foreach ($respondAmountData as $key99 => $value99) {
                if (sizeof($value99['payData']) > 0) {

                    $processedRespondAmountData[] = $value99;
                }
            }

            $months = $this->getSalesByInvoiceTable()->getMonthsYearBetweenTwoDates($fromMonth, $toMonth);
            $monthlyPaymentDetails = array();
            $respondCreditAmount = array();

            foreach ($months as $month) {
                $temp = explode(' ', $month);
                $monthlyPaymentDetails[$temp[1]][$temp[0]][0] = array('Month' => $temp[0], 'MonthWithYear' => $month, 'Year' => $temp[1]);
            }
            $i = 0;

          
            foreach ($processedRespondAmountData as $res) {
                $res['TotalAmount'] = ($res['outgoingPaymentType'] == 'invoice') ? $res['actTotal'] : $res['actTotal'];
                // $res['purchaseInvoiceCode'] = ($res['purchaseInvoiceCode']) ? $res['purchaseInvoiceCode'] : '-';
                $res['supplier'] = ($res['supplierTitle']) ? $res['supplierTitle']. ' '.$res['supplierName']. ' ['.$res['supplierCode'].']' : $res['supplierName']. ' ['.$res['supplierCode'].']';
                $monthlyPaymentDetails[$res['Year']][$res['Month']][$i] = $res;
                $i++;
            }
            return $monthlyPaymentDetails;
        }
    }

    public function monthlyPaymentPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');
            $translator = new Translator();
            $name = $translator->translate('Monthly Payment Report');
            $period = $fromMonth . ' -' . $toMonth;

            $monthlypaymentDetails = $this->getMonthlyPaymentDetails($fromMonth, $toMonth, $sort, $paymentNumber, $locationNumber);
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/payment-report/headerTemplate');

            $headerViewRender = $this->htmlRender($headerView);


            $viewMonthlyPayment = new ViewModel(array(
                'cD' => $cD,
                'monthlyPaymentData' => $monthlypaymentDetails,
                'fromMonth' => $fromMonth,
                'toMonth' => $toMonth,
                'headerTemplate' => $headerViewRender,
                'checkTaxStatus' => $checkTaxStatus
            ));

            $viewMonthlyPayment->setTemplate('reporting/payment-report/generate-monthly-payment-pdf');
            $viewMonthlyPayment->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($viewMonthlyPayment);
            $pdfPath = $this->downloadPDF('monthly-payment-report', $htmlContent, true);

            $this->setLogMessage("Retrive monthly supplier payment details for monthly supplier payments PDF report.");
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function monthlyPaymentCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');

            $monthlyPaymentDetails = $this->getMonthlyPaymentDetails($fromMonth, $toMonth, $sort, $paymentNumber, $locationNumber);
            $cD = $this->getCompanyDetails();

            if ($monthlyPaymentDetails) {
                $title = '';
                $tit = 'MONTHLY OUTGOING PAYMENT REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                    3 => ""
                );
                $title = implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "Period : " . $fromMonth . '-' . $toMonth
                );
                $title.=implode(",", $in) . "\n";


                $arrs = array("YEAR", "MONTH", "PAYMENT ID", "PAYMENT VOUCHER ID", "SUPPLIER NAME", "ISSUED DATE", "LOCATION", "PAYMENT TYPE", "AMOUNT");
                $header = implode(",", $arrs);
                $arr = '';

                $net_amount = 0.0;
                foreach ($monthlyPaymentDetails as $key => $value) {
                    $in = array(
                        0 => $key,
                        1 => "",
                        2 => " ",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "",
                        7 => "",
                        8 => ""
                    );
                    $arr.=implode(",", $in) . "\n";

                    foreach ($value as $key1 => $value1) {
                        $in = array(
                            0 => "",
                            1 => $key1,
                            2 => " ",
                            3 => "",
                            4 => "",
                            5 => "",
                            6 => "",
                            7 => "",
                            8 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        $sub_amount = 0.00;
                        foreach ($value1 as $key2 => $value2) {

                            $sub_amount+= $value2['TotalAmount'];
                            $net_amount +=$value2['TotalAmount'];
                            if ($value2['outgoingPaymentType'] == 'advance') {
                                $paymentCode = $value2['outgoingPaymentCode']." (Advance)";
                            } else {
                                $paymentCode = $value2['outgoingPaymentCode'];
                            }
                            $in = array(
                                0 => "",
                                1 => "",
                                2 => $paymentCode,
                                3 => $value2['relatedPVs'],
                                4 => $value2['supplier'],
                                5 => $value2['IssuedDate'],
                                6 => $value2['locationName'],
                                7 => "",
                                8 => "",
                            );
                            $arr.=implode(",", $in) . "\n";


                            foreach ($value2['payData'] as $key77 => $value77) {
                                $in = array(
                                    0 => "",
                                    1 => "",
                                    2 => "",
                                    3 => "",
                                    4 => "",
                                    5 => "",
                                    6 => "",
                                    7 => $value77['paymentMethodName'],
                                    8 => $value77['paidAmount'],
                                );
                                $arr.=implode(",", $in) . "\n";

                            }
                        }
                        $in = array(
                            1 => " ",
                            2 => " ",
                            3 => "",
                            4 => " ",
                            5 => " ",
                            6 => " ",
                            7 => " ",
                            8 => "Monthly Sales",
                            9 => $sub_amount
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                }
                $in = array(
                    1 => " ",
                    2 => " ",
                    3 => "",
                    4 => " ",
                    5 => " ",
                    6 => "",
                    7 => "",
                    8 => "",
                    9 => ""
                );
                $arr.=implode(",", $in) . "\n";
                $in = array(
                    1 => " ",
                    2 => " ",
                    3 => "",
                    4 => " ",
                    5 => " ",
                    6 => " ",
                    7 => " ",
                    8 => "Total Sales",
                    9 => $net_amount
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }


            $name = "Monthly_Payment_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->setLogMessage("Retrive monthly supplier payment details for monthly supplier payments CSV report.");
            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function dailyPaymentViewAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');
            $translator = new Translator();
            $name = $translator->translate('Daily Payment Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/payment-report/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyPaymentData = $this->getDailyPaymentDetails($fromDate, $toDate, $sort, $paymentNumber, $locationNumber);
            $cD = $this->getCompanyDetails();

            $dailyPaymentView = new ViewModel(array(
                'cD' => $cD,
                'dailyPaymentData' => $dailyPaymentData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $dailyPaymentView->setTemplate('reporting/payment-report/generate-daily-payment-pdf');

            $this->setLogMessage("Retrive daily supplier payment details for daily supplier payments view report.");
            $this->html = $dailyPaymentView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getDailyPaymentDetails($fromDate = null, $toDate = null, $sort = null, $paymentNumber = null, $locationNumber = null)
    {
        if (isset($fromDate) && isset($toDate)) {
            // $respond = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getDailyPaymentData($fromDate, $toDate, $paymentNumber, $locationNumber);
            $respond = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getDailyPaymentDataForNewReport($fromDate, $toDate, $paymentNumber, $locationNumber);
            $respondDays = $this->getSalesByInvoiceTable()->getDailySalesDataDays($fromDate, $toDate);
            $days = $respondDays;
            $dailyPaymentData = array();
            $respondCreditAmount = array();
            
            $processedRespondAmountData = [];


            foreach ($respond as $key => $value) {

                $paymentID = $value['outgoingPaymentID'];
                $paymentMethods = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPaymentMethodDetailsByPaymentIdAndPaymentMethodId($paymentID, $paymentNumber);
                $dataOfPaymentMethod = [];
                $totalPaid = 0;
                foreach ($paymentMethods as $paymethods) {
                    $paymethods = (object) $paymethods;
                    $paymentMethodName = '';
                    switch ($paymethods->outGoingPaymentMethodID) {
                        case '0':
                            $paymentMethodName = 'Credit';                        
                            break;
                        case '1':
                            $paymentMethodName = 'Cash';                        
                            break;
                        case '2':
                            $paymentMethodName = 'Cheque';
                            break;
                        case '3':
                            $paymentMethodName = 'Card';
                            break;
                        case '4':
                            $paymentMethodName = 'Loyalty Card';
                            break;
                        case '5':
                            $paymentMethodName = 'Bank Transfer';
                            break;
                        case '6':
                            $paymentMethodName = 'Gift Card';
                            break;
                        case '7':
                            $paymentMethodName = 'LC';
                            break;
                        case '8':
                            $paymentMethodName = 'TT';
                            break;
                        default:
                            break;
                    }

                    $totalPaid += floatval($paymethods->outGoingPaymentMethodPaidAmount);
                    $dataOfPaymentMethod[] = [
                        'paymentMethodID' => $paymethods->outGoingPaymentMethodID,
                        'paymentMethodName' => $paymentMethodName,
                        'paidAmount' => $paymethods->outGoingPaymentMethodPaidAmount
                    ];
                }


                if (floatval($value['outgoingPaymentCreditAmount']) > 0) {

                    $totalPaid += $value['outgoingPaymentCreditAmount'];
                    $dataOfPaymentMethod[] = [
                        'paymentMethodID' => '0',
                        'paymentMethodName' => 'Credit',
                        'paidAmount' => $value['outgoingPaymentCreditAmount']
                    ];
                }


                if ($value['outgoingPaymentType'] == 'invoice') {

                    $paymentInvoice = $this->CommonTable('SupplierInvoicePaymentsTable')->getPIDataByPaymentID($paymentID);


                    if (sizeof($paymentInvoice) > 0) {

                        $relatedPVs = '';

                        foreach ($paymentInvoice as $key55 => $value55) {

                            if ((sizeof($paymentInvoice)-1) == $key55) {
                                $relatedPVs .= $value55['purchaseInvoiceCode'];
                            } else {
                                $relatedPVs .= $value55['purchaseInvoiceCode'].'/';
                            }
                        }
                    } else {
                        $relatedPVs = '-';
                    }
                } else {
                    $relatedPVs = '-';
                }
                
                $respond[$key]['relatedPVs'] = $relatedPVs;
                $respond[$key]['payData'] = $dataOfPaymentMethod;
                $respond[$key]['actTotal'] = $totalPaid;

            }


            foreach ($respond as $key99 => $value99) {
                if (sizeof($value99['payData']) > 0) {

                    $processedRespondAmountData[] = $value99;
                }
            }


            foreach ($days as $day) {
                $dailyPaymentData[$day] = array();
            }
            $processedRespondAmountData = (sizeof($processedRespondAmountData) > 0) ? $processedRespondAmountData : array();
            // $respond = array_merge($respond, $respondCreditAmount);
            if ($processedRespondAmountData) {
                $i = 0;
                foreach ($processedRespondAmountData as $res) {
                    
                    $res['TotalAmount'] = ($res['outgoingPaymentType'] == 'invoice') ? $res['actTotal'] : $res['actTotal'];
                    // $res['purchaseInvoiceCode'] = ($res['purchaseInvoiceCode']) ? $res['purchaseInvoiceCode'] : '-';
                    $res['supplier'] = ($res['supplierTitle']) ? $res['supplierTitle']. ' '.$res['supplierName']. ' ['.$res['supplierCode'].']' : $res['supplierName']. ' ['.$res['supplierCode'].']';
                    $dailyPaymentData[$res['Day']][$i] = $res;
                    $i++;
                }
            }
            return $dailyPaymentData;
        }
    }

    public function dailyPaymentPdfAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');
            $translator = new Translator();
            $name = $translator->translate('Daily Payment Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/payment-report/headerTemplate');

            $headerViewRender = $this->htmlRender($headerView);

            $dailyPaymentData = $this->getDailyPaymentDetails($fromDate, $toDate, $sort, $paymentNumber, $locationNumber);
            $cD = $this->getCompanyDetails();

            $viewDailyPayment = new ViewModel(array(
                'cD' => $cD,
                'dailyPaymentData' => $dailyPaymentData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $viewDailyPayment->setTemplate('reporting/payment-report/generate-daily-payment-pdf');
            $viewDailyPayment->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewDailyPayment);
            $pdfPath = $this->downloadPDF('daily-payment-report', $htmlContent, true);

            $this->setLogMessage("Retrive daily supplier payment details for daily supplier payments PDF report.");
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function dailyPaymentCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');

            $dailyPaymentData = $this->getDailyPaymentDetails($fromDate, $toDate, $sort, $paymentNumber, $locationNumber);
            $cD = $this->getCompanyDetails();

            if ($dailyPaymentData) {
                $netTax = 0;
                $netAmount = 0;

                $title = '';
                $tit = 'DAILY OUTGOING PAYENT REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                    3 => "",
                    4 => ""
                );
                $title = implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "Period : " . $fromDate . '-' . $toDate
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("DATE", "PAYMENT ID", "PAYMENT VOUCHER ID", "SUPPLIER NAME", "ISSUED DATE", "LOCATION", "PAYMENT TYPE", "AMOUNT");
                $header = implode(",", $arrs);
                $arr = '';

                foreach ($dailyPaymentData as $invID => $value1) {
                    $in = array(
                        0 => $invID,
                        1 => "",
                        2 => "",
                        3 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                    $sub_amount = 0.00;
                    foreach ($value1 as $key1 => $value2) {
                        $amount = $value2['TotalAmount'];
                        $amount = $amount ? $amount : 0.00;
                        $netAmount += $amount;
                        $sub_amount += $amount;
                        if ($value2['outgoingPaymentType'] == 'advance') {
                            $paymentCode = $value2['outgoingPaymentCode']." (Advance)";
                        } else {
                            $paymentCode = $value2['outgoingPaymentCode'];
                        }
                        $in = array(
                            0 => "",
                            1 => $paymentCode,
                            2 => $value2['relatedPVs'],
                            3 => $value2['supplier'],
                            4 => $value2['IssuedDate'],
                            5 => $value2['locationName'],
                            6 => "",
                            7 => ""
                        );
                        $arr.=implode(",", $in) . "\n";

                        foreach ($value2['payData'] as $key77 => $value77) {
                            $in = array(
                                0 => "",
                                1 => "",
                                2 => "",
                                3 => "",
                                4 => "",
                                5 => "",
                                6 => $value77['paymentMethodName'],
                                7 => $value77['paidAmount']
                            );
                            $arr.=implode(",", $in) . "\n";

                        }
                    }
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "Total Sales",
                        7 => $sub_amount
                    );
                    $arr.=implode(",", $in) . "\n";
                }
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => "GRAND TOTAL",
                    7 => $netAmount
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Daily_Payment_Report";

            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->setLogMessage("Retrive daily supplier payment details for daily supplier payments CSV report.");
            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function annualPaymentViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $year = $request->getPost('year');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');
            $translator = new Translator();
            $name = $translator->translate('Annual Payment Report');
            
            $annualPaymentData = $this->getAnnualPaymentDetails($year, $sort, $paymentNumber, $locationNumber);
            $companyDetails = $this->getCompanyDetails();
            
            $headerView = $this->headerViewTemplate($name, $year);
            $headerView->setTemplate('reporting/payment-report/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);
            
            $annualPaymentView = new ViewModel(array(
                'annualPaymentData' => $annualPaymentData,
                'headerView' => $headerViewRender,
            ));
            $annualPaymentView->setTemplate('reporting/payment-report/generate-annual-payment-pdf');

            $this->setLogMessage("Retrive annual supplier payment details for annual supplier payments view report.");
            $this->html = $annualPaymentView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getAnnualPaymentDetails($year = null, $sort = null, $paymentNumber = null, $locationNumber = null)
    {
        if (isset($year)) {
            // $respondAmount = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getAnnualPaymentData($year, $paymentNumber, $locationNumber);
            $respondAmount = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getAnnualPaymentDataForNewReport($year, $paymentNumber, $locationNumber);

            $A = $year . '-01-01';
            $B = $year . '-12-31';
            $months = $this->getSalesByInvoiceTable()->getMonthsYearBetweenTwoDates($A, $B);
            $respondAmountdata = $respondAmount ? $respondAmount : array();
            $annualPaymentDetails = array();
            $respondCreditAmount = array();
           

            $processedRespondAmountData = [];

            foreach ($respondAmountdata as $key => $value) {

                $paymentID = $value['outgoingPaymentID'];
                $paymentMethods = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPaymentMethodDetailsByPaymentIdAndPaymentMethodId($paymentID, $paymentNumber);
                $dataOfPaymentMethod = [];
                $totalPaid = 0;
                foreach ($paymentMethods as $paymethods) {
                    $paymethods = (object) $paymethods;
                    $paymentMethodName = '';
                    switch ($paymethods->outGoingPaymentMethodID) {
                        case '0':
                            $paymentMethodName = 'Credit';                        
                            break;
                        case '1':
                            $paymentMethodName = 'Cash';                        
                            break;
                        case '2':
                            $paymentMethodName = 'Cheque';
                            break;
                        case '3':
                            $paymentMethodName = 'Card';
                            break;
                        case '4':
                            $paymentMethodName = 'Loyalty Card';
                            break;
                        case '5':
                            $paymentMethodName = 'Bank Transfer';
                            break;
                        case '6':
                            $paymentMethodName = 'Gift Card';
                            break;
                        case '7':
                            $paymentMethodName = 'LC';
                            break;
                        case '8':
                            $paymentMethodName = 'TT';
                            break;
                        default:
                            break;
                    }

                    $totalPaid += floatval($paymethods->outGoingPaymentMethodPaidAmount);
                    $dataOfPaymentMethod[] = [
                        'paymentMethodID' => $paymethods->outGoingPaymentMethodID,
                        'paymentMethodName' => $paymentMethodName,
                        'paidAmount' => $paymethods->outGoingPaymentMethodPaidAmount
                    ];
                }


                if (floatval($value['outgoingPaymentCreditAmount']) > 0) {

                    $totalPaid += $value['outgoingPaymentCreditAmount'];
                    $dataOfPaymentMethod[] = [
                        'paymentMethodID' => '0',
                        'paymentMethodName' => 'Credit',
                        'paidAmount' => $value['outgoingPaymentCreditAmount']
                    ];
                }


                if ($value['outgoingPaymentType'] == 'invoice') {

                    $paymentInvoice = $this->CommonTable('SupplierInvoicePaymentsTable')->getPIDataByPaymentID($paymentID);


                    if (sizeof($paymentInvoice) > 0) {

                        $relatedPVs = '';

                        foreach ($paymentInvoice as $key55 => $value55) {

                            if ((sizeof($paymentInvoice)-1) == $key55) {
                                $relatedPVs .= $value55['purchaseInvoiceCode'];
                            } else {
                                $relatedPVs .= $value55['purchaseInvoiceCode'].'/';
                            }
                        }
                    } else {
                        $relatedPVs = '-';
                    }
                } else {
                    $relatedPVs = '-';
                }
                $respondAmountdata[$key]['relatedPVs'] = $relatedPVs;
                $respondAmountdata[$key]['payData'] = $dataOfPaymentMethod;
                $respondAmountdata[$key]['actTotal'] = $totalPaid;

            }


            foreach ($respondAmountdata as $key99 => $value99) {
                if (sizeof($value99['payData']) > 0) {

                    $processedRespondAmountData[] = $value99;
                }
            }



            foreach ($months as $month) {
                $temp = explode(' ', $month);
                if ($temp[0] == 'January' || $temp[0] == 'February' || $temp[0] == 'March') {
                    $annualPaymentDetails['1st Quarter'][0] = array();
                } elseif ($temp[0] == 'April' || $temp[0] == 'May' || $temp[0] == 'June') {
                    $annualPaymentDetails['2nd Quarter'][0] = array();
                } elseif ($temp[0] == 'July' || $temp[0] == 'August' || $temp[0] == 'September') {
                    $annualPaymentDetails['3rd Quarter'][0] = array();
                } elseif ($temp[0] == 'October' || $temp[0] == 'November' || $temp[0] == 'December') {
                    $annualPaymentDetails['4th Quarter'][0] = array();
                }
            }

            $i = 0;
            foreach ($processedRespondAmountData as $res) {
                    
                

                $res['TotalAmount'] = ($res['outgoingPaymentType'] == 'invoice') ? $res['actTotal'] : $res['actTotal'];
                    // $res['purchaseInvoiceCode'] = ($res['purchaseInvoiceCode']) ? $res['purchaseInvoiceCode'] : '-';
                $res['supplier'] = ($res['supplierTitle']) ? $res['supplierTitle']. ' '.$res['supplierName']. ' ['.$res['supplierCode'].']' : $res['supplierName']. ' ['.$res['supplierCode'].']';

                
                if ($res['Month'] == 'January' || $res['Month'] == 'February' || $res['Month'] == 'March') {
                    $annualPaymentDetails['1st Quarter'][$i] = $res;
                } else if ($res['Month'] == 'April' || $res['Month'] == 'May' || $res['Month'] == 'June') {
                    $annualPaymentDetails['2nd Quarter'][$i] = $res;
                } else if ($res['Month'] == 'July' || $res['Month'] == 'August' || $res['Month'] == 'September') {
                    $annualPaymentDetails['3rd Quarter'][$i] = $res;
                } else if ($res['Month'] == 'October' || $res['Month'] == 'November' || $res['Month'] == 'December') {
                    $annualPaymentDetails['4th Quarter'][$i] = $res;
                }
                $i++;
            }
            return $annualPaymentDetails;
        }
    }

    public function annualPaymentPdfAction()
    {
        $requset = $this->getRequest();
        if ($requset->isPost()) {
            $year = $requset->getPost('year');
            $paymentNumber = $requset->getPost('paymentNumber');
            $locationNumber = $requset->getPost('locationNumber');
            $translator = new Translator();
            $name = $translator->translate('Annual Payment Report');
            $period = $year;

            $annualPaymentData = $this->getAnnualPaymentDetails($year, $sort, $paymentNumber, $locationNumber);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/payment-report/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);
            
            $viewAnnualPayment = new ViewModel(array(
                'cD' => $companyDetails,
                'annualPaymentData' => $annualPaymentData,
                'year' => $year,
                'headerView' => $headerViewRender,
            ));

            $viewAnnualPayment->setTemplate('reporting/payment-report/generate-annual-payment-pdf');
            $viewAnnualPayment->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewAnnualPayment);
            $pdfPath = $this->downloadPDF('Annual_Payment_Report', $htmlContent, true);

            $this->setLogMessage("Retrive annual supplier payment details for annual supplier payments PDF report.");
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function annualPaymentCsvAction()
    {
        $request = $this->getRequest();
        if ($request) {
            $year = $request->getPost('year');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');

            $annualPaymentData = $this->getAnnualPaymentDetails($year, $sort, $paymentNumber, $locationNumber);
            $cD = $this->getCompanyDetails();

            if ($annualPaymentData) {

                $netTax = 0.00;
                $netAmount = 0.00;

                $title = '';
                $tit = 'ANNUAL OUTGOING PAYMENT REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                    3 => "",
                    4 => ""
                );
                $title = implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "Period : " . $year
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("QUARTER", "PAYMENT ID", "PAYMENT VOUCHER ID", "SUPPLIER NAME", "ISSUED DATE", "LOCATION", "PAYMENT TYPE", "AMOUNT");
                $header = implode(",", $arrs);
                $arr = '';

                $net_amount = 0.00;
                foreach ($annualPaymentData as $invID => $value1) {
                    $in = array(
                        0 => $invID,
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "",
                        7 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                    $sub_amount = 0.00;
                    foreach ($value1 as $key1 => $value2) {
                        $amount = $value2['TotalAmount'];
                        $amount = $amount ? $amount : 0.00;
                        $net_amount += $amount;
                        $sub_amount += $amount;
                        if ($value2['outgoingPaymentType'] == 'advance') {
                            $paymentCode = $value2['outgoingPaymentCode']." (Advance)";
                        } else {
                            $paymentCode = $value2['outgoingPaymentCode'];
                        }
                        $in = array(
                            0 => "",
                            1 => $paymentCode,
                            2 => $value2['relatedPVs'],
                            3 => $value2['supplier'],
                            4 => $value2['IssuedDate'],
                            5 => $value2['locationName'],
                            6 => "",
                            7 => ""
                        );
                        $arr.=implode(",", $in) . "\n";

                        foreach ($value2['payData'] as $key77 => $value77) {
                            $in = array(
                                0 => "",
                                1 => "",
                                2 => "",
                                3 => "",
                                4 => "",
                                5 => "",
                                6 => $value77['paymentMethodName'],
                                7 => $value77['paidAmount']
                            );
                            $arr.=implode(",", $in) . "\n";

                        }
                    }
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "",
                        7 => ""
                    );
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "Sales of " . $invID,
                        7 => $sub_amount
                    );
                    $arr.=implode(",", $in) . "\n";
                }
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "",
                );
                $arr.=implode(",", $in) . "\n";
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => "Grand Total Sales",
                    7 => $net_amount
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Annual_Payment_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->setLogMessage("Retrive annual supplier payment details for annual supplier payments CSV report.");
            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getPaymentSummeryDetails($fromDate = null, $toDate = null, $sort = null, $paymentNumber = null, $locationNumber = null)
    {
        if (isset($fromDate) && isset($toDate)) {
            // $paymentSummery = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPaymentSummeryData($fromDate, $toDate, $paymentNumber, $locationNumber);

            $paymentSummery = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPaymentSummeryDataForNewReport($fromDate, $toDate, $paymentNumber, $locationNumber);
            $paymentSummeryData = $paymentSummery ? $paymentSummery : array();
            $outgoingPaymentSummeryData = array();

            $processedRespondAmountData = [];

            foreach ($paymentSummeryData as $key => $value) {

                $paymentID = $value['outgoingPaymentID'];
                $paymentMethods = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPaymentMethodDetailsByPaymentIdAndPaymentMethodId($paymentID, $paymentNumber);
                $dataOfPaymentMethod = [];
                $totalPaid = 0;
                foreach ($paymentMethods as $paymethods) {
                    $paymethods = (object) $paymethods;
                    $paymentMethodName = '';
                    switch ($paymethods->outGoingPaymentMethodID) {
                        case '0':
                            $paymentMethodName = 'Credit';                        
                            break;
                        case '1':
                            $paymentMethodName = 'Cash';                        
                            break;
                        case '2':
                            $paymentMethodName = 'Cheque';
                            break;
                        case '3':
                            $paymentMethodName = 'Card';
                            break;
                        case '4':
                            $paymentMethodName = 'Loyalty Card';
                            break;
                        case '5':
                            $paymentMethodName = 'Bank Transfer';
                            break;
                        case '6':
                            $paymentMethodName = 'Gift Card';
                            break;
                        case '7':
                            $paymentMethodName = 'LC';
                            break;
                        case '8':
                            $paymentMethodName = 'TT';
                            break;
                        default:
                            break;
                    }

                    $totalPaid += floatval($paymethods->outGoingPaymentMethodPaidAmount);
                    $dataOfPaymentMethod[] = [
                        'paymentMethodID' => $paymethods->outGoingPaymentMethodID,
                        'paymentMethodName' => $paymentMethodName,
                        'paidAmount' => $paymethods->outGoingPaymentMethodPaidAmount
                    ];
                }


                if (floatval($value['outgoingPaymentCreditAmount']) > 0) {

                    $totalPaid += $value['outgoingPaymentCreditAmount'];
                    $dataOfPaymentMethod[] = [
                        'paymentMethodID' => '0',
                        'paymentMethodName' => 'Credit',
                        'paidAmount' => $value['outgoingPaymentCreditAmount']
                    ];
                }


                if ($value['outgoingPaymentType'] == 'invoice') {

                    $paymentInvoice = $this->CommonTable('SupplierInvoicePaymentsTable')->getInvoiceByPaymentID($paymentID);


                    if (sizeof($paymentInvoice) > 0) {

                        $relatedPVs = '';

                        foreach ($paymentInvoice as $key55 => $value55) {


                            if ($value55['invoiceID'] != 0) {
                                if ((sizeof($paymentInvoice)-1) == $key55) {
                                    $relatedPVs .= $value55['purchaseInvoiceCode'];
                                } else {
                                    $relatedPVs .= $value55['purchaseInvoiceCode'].'/';
                                }
                            } else {
                                if ((sizeof($paymentInvoice)-1) == $key55) {
                                    $relatedPVs .= $value55['paymentVoucherCode'];
                                } else {
                                    $relatedPVs .= $value55['paymentVoucherCode'].'/';
                                }
                            }

                        }
                    } else {
                        $relatedPVs = '-';
                    }
                } else {
                    $relatedPVs = '-';
                }
                $paymentSummeryData[$key]['relatedPVs'] = $relatedPVs;
                $paymentSummeryData[$key]['payData'] = $dataOfPaymentMethod;
                $paymentSummeryData[$key]['actTotal'] = $totalPaid;

            }


            foreach ($paymentSummeryData as $key99 => $value99) {
                if (sizeof($value99['payData']) > 0) {

                    $processedRespondAmountData[] = $value99;
                }
            }



            $i = 0;            
            foreach ($processedRespondAmountData as $res) {
                
                $res['TotalAmount'] = ($res['outgoingPaymentType'] == 'invoice') ? $res['actTotal'] : $res['actTotal'];
                // $res['purchaseInvoiceCode'] = ($res['purchaseInvoiceCode']) ? $res['purchaseInvoiceCode'] : $res['paymentVoucherCode'];
                $res['supplier'] = ($res['supplierTitle']) ? $res['supplierTitle']. ' '.$res['supplierName']. ' ['.$res['supplierCode'].']' : $res['supplierName']. ' ['.$res['supplierCode'].']';
                $outgoingPaymentSummeryData[$res['supplierID']][$i] = $res;
                $i++;
            }
            return $outgoingPaymentSummeryData;
        }
    }

    public function summeryPaymentViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');
            $translator = new Translator();
            $name = $translator->translate('Payment Summary Report');
            $period = $fromDate . ' - ' . $toDate;
            $paymentSummeryData = $this->getPaymentSummeryDetails($fromDate, $toDate, $sort, $paymentNumber, $locationNumber);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/payment-report/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $paymentSummeryView = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentSummeryData' => $paymentSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $paymentSummeryView->setTemplate('reporting/payment-report/generate-payment-summery-pdf');

            $this->setLogMessage("Retrive supplier payment summary details for supplier payments summary view report.");
            $this->html = $paymentSummeryView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function summeryPaymentPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');
            $translator = new Translator();
            $name = $translator->translate('Payment Summary Report');
            $period = $fromDate . ' - ' . $toDate;

            $paymentSummeryData = $this->getPaymentSummeryDetails($fromDate, $toDate, $sort, $paymentNumber, $locationNumber);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/payment-report/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewPaymentSummery = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentSummeryData' => $paymentSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $viewPaymentSummery->setTemplate('reporting/payment-report/generate-payment-summery-pdf');
            $viewPaymentSummery->setTerminal(true);
            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewPaymentSummery);
            $pdfPath = $this->downloadPDF('Payment_Summary_Report', $htmlContent, true);

            $this->setLogMessage("Retrive supplier payment summary details for supplier payments summary PDF report.");
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function summeryPaymentCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');

            $paymentSummeryData = $this->getPaymentSummeryDetails($fromDate, $toDate, $sort, $paymentNumber, $locationNumber);
            $cD = $this->getCompanyDetails();

            $title = '';
            $tit = 'PAYMENT SUMMARY REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => ""
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => $cD[0]->companyName,
                1 => $cD[0]->companyAddress,
                2 => 'Tel: ' . $cD[0]->telephoneNumber
            );
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("SUPPLIER NAME", "PAYMENT ID", "PAYMENT VOUCHER ID", "ISSUED DATE", "LOCATION", "PAYMENT TYPE", "AMOUNT");
            $header = implode(",", $arrs);
            $arr = '';
            $netAmount = 0.00;

            if (empty($paymentSummeryData)) {
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => ""
                );
                $arr.=implode(",", $in) . "\n";
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "NO DATA",
                    5 => "",
                    6 => ""
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                foreach ($paymentSummeryData as $key => $data) {
                    if ($key != "") {
                        $subAmount = 0.00;
                        foreach ($data as $key1 => $data1) {

                            $subAmount+=$data1['TotalAmount'];
                            $netAmount+=$data1['TotalAmount'];
                            if ($data1['outgoingPaymentType'] == 'advance') {
                                $paymentCode = $data1['outgoingoPaymentCode']." (Advance)";
                            } else {
                                $paymentCode = $data1['outgoingoPaymentCode'];
                            }
                            $in = array(
                                0 => $data1['title'] . ' ' . $data1['name'],
                                1 => $paymentCode,
                                2 => $data1['relatedPVs'],
                                3 => $data1['IssuedDate'],
                                4 => $data1['locationName'],
                                5 => "",
                                6 => ""
                            );
                            $arr.=implode(",", $in) . "\n";

                            foreach ($data1['payData'] as $key77 => $value77) {
                                $in = array(
                                    0 => "",
                                    1 => "",
                                    2 => "",
                                    3 => "",
                                    4 => "",
                                    5 => $value77['paymentMethodName'],
                                    6 => $value77['paidAmount']
                                );
                                $arr.=implode(",", $in) . "\n";

                            }
                        }

                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => " ",
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => "sub Total of " . $data1['name'],
                            6 => $subAmount
                        );
                        $arr.=implode(",", $in) . "\n";
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => " "
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                }
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => ""
                );
                $arr.=implode(",", $in) . "\n";
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "Net Total",
                    5 => $netAmount
                );
                $arr.=implode(",", $in) . "\n";
            }

            $name = "payment-summary-report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->setLogMessage("Retrive supplier payment summary details for supplier payments summary CSV report.");
            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getProductTable()
    {
        if (!$this->productTable) {
            $sm = $this->getServiceLocator();
            $this->productTable = $sm->get('Invoice\Model\ProductTable');
        }
        return $this->productTable;
    }

    public function getPayementsTable()
    {
        if (!$this->paymentsTable) {
            $sm = $this->getServiceLocator();
            $this->paymentsTable = $sm->get('Invoice\Model\PaymentsTable');
        }
        return $this->paymentsTable;
    }

    public function getSalesByInvoiceTable()
    {
        if (!$this->salesByInvoiceTable) {
            $sm = $this->getServiceLocator();
            $this->salesByInvoiceTable = $sm->get('Invoice\Model\InvoiceTable');
        }
        return $this->salesByInvoiceTable;
    }

    public function getLoggerTable()
    {
        if (!$this->loggerTable) {
            $sm = $this->getServiceLocator();
            $this->loggerTable = $sm->get('User\Model\LoggerTable');
        }
        return $this->loggerTable;
    }

    public function getUserTable()
    {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }

}
