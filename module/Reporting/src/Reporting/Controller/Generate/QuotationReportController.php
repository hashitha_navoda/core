<?php

namespace Reporting\Controller\Generate;

use Core\Controller\API\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;

class QuotationReportController extends CoreController
{

    public function quotationDataViewAction()
    {

        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $dataSet = $request->getPost();
        $translator = new Translator();
        $name = $translator->translate('Quotation Report');
        $period = $dataSet['fromDate'] . ' - ' . $dataSet['toDate'];
        $quotationDetails = $this->getQuotationDetails($dataSet);
        $companyDetails = $this->getCompanyDetails();
        $systemCurrency = $this->CommonTable('Settings\Model\DisplaySetupTable')->getSystemCurrency()->current();
        $cusCategoryFlag = ($dataSet['cusCategories']) ? 1 : 0;
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);
        $quotationReportView = new ViewModel(array(
            'quotationDetails' => $quotationDetails,
            'fromMonth' => $dataSet['fromDate'],
            'toMonth' => $dataSet['toDate'],
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
            'currencySymbol' => $systemCurrency['currencySymbol'],
            'cusCatgoryFlag' => $cusCategoryFlag,
        ));
        $quotationReportView->setTemplate('reporting/quotation-report/generate-quotation-pdf');

        $this->html = $quotationReportView;
        $this->status = true;
        return $this->JSONRespondHtml();
    }

    public function quotationDataPdfAction()
    {

        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        
        $dataSet = $request->getPost();
        $translator = new Translator();
        $name = $translator->translate('Quotation Report');
        $period = $dataSet['fromDate'] . ' - ' . $dataSet['toDate'];

        $quotationDetails = $this->getQuotationDetails($dataSet);
        $companyDetails = $this->getCompanyDetails();
        $systemCurrency = $this->CommonTable('Settings\Model\DisplaySetupTable')->getSystemCurrency()->current();

        $cusCategoryFlag = ($dataSet['cusCategories']) ? 1 : 0;
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);
        $quotationReportView = new ViewModel(array(
            'quotationDetails' => $quotationDetails,
            'checkTaxStatus' => $checkTaxStatus,
            'fromMonth' => $dataSet['fromDate'],
            'toMonth' => $dataSet['toDate'],
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
            'currencySymbol' => $systemCurrency['currencySymbol'],
            'cusCatgoryFlag' => $cusCategoryFlag,
        ));    
        
        $quotationReportView->setTemplate('reporting/quotation-report/generate-quotation-pdf');
        $quotationReportView->setTerminal(true);
         // get rendered view into variable
        $htmlContent = $this->viewRendererHtmlContent($quotationReportView);
        $pdfPath = $this->downloadPDF('quotation-report', $htmlContent, true);

        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespond();
    }

     public function quotationCsvAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        
        $dataSet = $request->getPost();
        $translator = new Translator();
        $name = $translator->translate('Quotation Report');
        $period = $dataSet['fromDate'] . ' - ' . $dataSet['toDate'];
        $cusCategoryFlag = ($dataSet['cusCategories']) ? 1 : 0;
        $quotationDetails = $this->getQuotationDetails($dataSet);
        $companyDetails = $this->getCompanyDetails();
        $systemCurrency = $this->CommonTable('Settings\Model\DisplaySetupTable')->getSystemCurrency()->current();

        if ($quotationDetails) {
            $title = '';
            $tit = 'QUOTATION REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
                3 => ""
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => $companyDetails[0]->companyName,
                1 => $companyDetails[0]->companyAddress,
                2 => 'Tel: ' . $companyDetails[0]->telephoneNumber
            );
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => "Period : " . $period
            );
            $title.=implode(",", $in) . "\n";

                
            $arrs = array("DATE", "LOCATION", "CUSTOMER NAME/CODE/CUSTOMER CATEGORY", "QUOTATION CODE", "QUOTATION STATUS", "QUOTATION TOTAL AMOUNT WITHOUT TAX","NORMAL TAX","COMPOUND TAX","QUOTATION TOTAL AMOUNT WITH TAX", "SALES PERSON", "PAY TYPE");
            $header = implode(",", $arrs);
            $arr = '';

            $net_amount = 0.0;
            
            foreach ($quotationDetails as $key => $value) {
                $in = array(
                    0 => $key,
                    1 => "",
                    2 => " ",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => ""
                );
                $arr.=implode(",", $in) . "\n";

                foreach ($value as $key1 => $value1) {
                    $in = array(
                        0 => "",
                        1 => $key1,
                        2 => " ",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "",
                        7 => ""
                    );
                $arr.=implode(",", $in) . "\n";
                            
                        foreach ($value1 as $key2 => $value2) {
                            if($cusCategoryFlag){
                                $cusName = $value2['quotationCustomerName']." - ( ".$value2['customerCategoryName']. " )";
                            } else {
                                $cusName = $value2['quotationCustomerName'];
                            }
                            $net_amount +=$value2['quotationTotalAmount'];
                            $sub_amount_without_tax +=floatval(($value2['quotationTotalAmount']) - floatval($value2['normalTax']) - floatval($value2['compoundTaxTax']));
                            $sub_amount_of_normal_tax +=$value2['normalTax'];
                            $sub_amount_of_compound_tax +=$value2['compoundTaxTax'];
                            $in = array(
                                0 => "",
                                1 => "",
                                2 => $cusName,
                                3 => $value2['quotationCode'],
                                4 => $value2['statusName'],
                                5 => floatval(($value2['quotationTotalAmount']) - floatval($value2['normalTax']) - floatval($value2['compoundTaxTax'])),
                                6 => (floatval($value2['normalTax'])),
                                7 => (floatval($value2['compoundTaxTax'])),
                                8 => (floatval($value2['quotationTotalAmount'])),
                                9 => $value2['salesPersonSortName'],
                                10 => $value2['paymentTermName']
                            );
                            $arr.=implode(",", $in) . "\n";
                            }
                            
                        }
                    }
                    $in = array(
                        0 => " ",
                        1 => " ",
                        2 => " ",
                        3 => "",
                        4 => " ",
                        5 => " ",
                        6 => "",
                        7 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                    $in = array(
                        0 => " ",
                        1 => " ",
                        2 => " ",
                        3 => "",
                        4 => "Quotation Total",
                        5 => $systemCurrency['currencySymbol']." ".$sub_amount_without_tax,
                        6 => $systemCurrency['currencySymbol']." ".$sub_amount_of_normal_tax,
                        7 => $systemCurrency['currencySymbol']." ".$sub_amount_of_compound_tax,
                        8 => $systemCurrency['currencySymbol']." ".$net_amount,
                        9 => " ",
                        10 => " "
                    );
                    $arr.=implode(",", $in) . "\n";
                
        } else {
            $arr = "no matching records found\n";
        }

        $name = "Quotation_Report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
        
    }


    public function getQuotationDetails($dataSet)
    {
        $quotData = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionsForReport(
                $dataSet['fromDate'], $dataSet['toDate'], $dataSet['customer'], $dataSet['locations'], $dataSet['quotationStatus'], $dataSet['paymentTermList'], $dataSet['salesPersons'],$dataSet['cusCategories']);
        $quotDataSet = [];

        foreach ($quotData as $key => $value) {
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationID'] = $value['quotationID'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationCode'] = $value['quotationCode'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationCustomerName'] = $value['quotationCustomerName'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['customerID'] = $value['customerID'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationIssuedDate'] = $value['quotationIssuedDate'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationExpireDate'] = $value['quotationExpireDate'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationPayementTerm'] = $value['quotationPayementTerm'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationTaxAmount'] = $value['quotationTaxAmount'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationTotalDiscount'] = $value['quotationTotalDiscount'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationTotalAmount'] = $value['quotationTotalAmount'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationComment'] = $value['quotationComment'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationBranch'] = $value['quotationBranch'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['salesPersonID'] = $value['salesPersonID'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['quotationStatus'] = $value['quotationStatus'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['customerName'] = $value['customerName'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['currencyName'] = $value['currencyName'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['currencySymbol'] = $value['currencySymbol'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['currencyRate'] = $value['currencyRate'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['locationName'] = $value['locationName'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['locationCode'] = $value['locationCode'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['statusName'] = $value['statusName'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['salesPersonSortName'] = $value['salesPersonSortName'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['paymentTermID'] = $value['paymentTermID'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['paymentTermName'] = $value['paymentTermName'];
            $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['paymentTermDescription'] = $value['paymentTermDescription'];

            if ($value['taxType'] == "v") {
                $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['normalTax'] += floatval($value['quotTaxAmount']);  
            }  
            
            if ($value['taxType'] == "c") {
                $quotDataSet[$value['quotationIssuedDate']][$value['locationName']][$value['quotationID']]['compoundTaxTax'] += floatval($value['quotTaxAmount']);  
            }  
        }
        return $quotDataSet;
    }
    

}
