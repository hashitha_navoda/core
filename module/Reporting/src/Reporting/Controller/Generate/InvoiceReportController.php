<?php

namespace Reporting\Controller\Generate;

use Core\Controller\API\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;

class InvoiceReportController extends CoreController
{

    protected $userID;
    protected $salesByInvoiceTable;
    protected $productTable;
    protected $paymentsTable;
    protected $loggerTable;
    protected $userTable;
    protected $taxAmountTable;

    public function monthlyInvoiceViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $paymentTerm = $request->getPost('paymentTerm');
            $locationID = $request->getPost('locationID');

            $translator = new Translator();
            $name = $translator->translate('Monthly Invoice Report');
            $period = $fromMonth . ' - ' . $toMonth;

            $respondAmount = $this->getMonthlyInvoiceDetails($fromMonth, $toMonth, $paymentTerm, $locationID);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $monthlyInvoiceView = new ViewModel(array(
                'monthlyInvoiceData' => $respondAmount,
                'checkTaxStatus' => $checkTaxStatus,
                'fromMonth' => $fromMonth,
                'toMonth' => $toMonth,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));
            $monthlyInvoiceView->setTemplate('reporting/invoice-report/generate-monthly-invoice-pdf');

            $this->html = $monthlyInvoiceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getMonthlyInvoiceDetails($fromMonth = NULL, $toMonth = NULL, $paymentTerm = NULL, $locationID = NULL)
    {
        if (isset($fromMonth) && isset($toMonth)) {
            $respondAmount = $this->CommonTable('Invoice\Model\InvoiceTable')->getMonthlyInvoiceData($fromMonth, $toMonth, $paymentTerm, $locationID);
            $respondAmountData = $respondAmount ? $respondAmount : [];

            $respondYears = $this->CommonTable('Invoice\Model\InvoiceTable')->getMonthlySalesDataYears($fromMonth, $toMonth, $locationID);
            $months = $this->CommonTable('Invoice\Model\InvoiceTable')->getMonthsYearBetweenTwoDates($fromMonth, $toMonth);
            $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteDetailsByInvoiceDate($fromMonth, $toMonth, $locationID);
            //set credit note data to the array
            foreach ($creditNoteDetails as $val) {
                if($creditNoteID != $val['creditNoteID']){
                    $creditNoteDataArray[$val['invoiceID']]['TotalCreditAmount'] += floatval($val['TotalCreditAmount']);
                }
                $creditNoteDataArray[$val['invoiceID']]['creditNoteID'] = $val['creditNoteID'];
                $creditNoteDataArray[$val['invoiceID']]['creditNoteTaxAmount'] += floatval($val['creditNoteTaxAmount']);
                $creditNoteID = $val['creditNoteID'];
            }

            $monthlyInvoiceDetails = [];
            $years = $respondYears;

            foreach ($months as $month) {
                $temp = split(' ', $month);
                $monthlyInvoiceDetails[$temp[1]][$temp[0]][0] = array('Month' => $temp[0], 'MonthWithYear' => $month, 'Year' => $temp[1]);
            }
            foreach ($respondAmountData as $res) {
                //check that given invoice has any credit notes.
                if(count($creditNoteDataArray[$res['invoiceID']]) > 0){
                    //check that all items are return or not that given invoice
                    if($res['TotalAmount'] != $creditNoteDataArray[$res['invoiceID']]['TotalCreditAmount']){
                        $res['TotalAmount'] = $res['TotalAmount'] - $creditNoteDataArray[$res['invoiceID']]['TotalCreditAmount'];
                        $res['TotalTax'] = $res['TotalTax'] - $creditNoteDataArray[$res['invoiceID']]['creditNoteTaxAmount'];

                        $monthlyInvoiceDetails[$res['Year']][$res['Month']][$res['InvoiceNumber']] = $res;
                    }
                }else{
                    $monthlyInvoiceDetails[$res['Year']][$res['Month']][$res['InvoiceNumber']] = $res;
                }
            }

            return $monthlyInvoiceDetails;
        }
    }

    public function monthlyInvoicePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $paymentTerm = $request->getPost('paymentTerm');
            $locationID = $request->getPost('locationID');

            $translator = new Translator();
            $name = $translator->translate('Monthly Invoice Report');
            $period = $fromMonth . ' - ' . $toMonth;

            $monthlyInvoiceDetails = $this->getMonthlyInvoiceDetails($fromMonth, $toMonth, $paymentTerm, $locationID);
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewMonthlyInvoice = new ViewModel(array(
                'cD' => $cD,
                'monthlyInvoiceData' => $monthlyInvoiceDetails,
                'fromMonth' => $fromMonth,
                'toMonth' => $toMonth,
                'checkTaxStatus' => $checkTaxStatus,
                'headerTemplate' => $headerViewRender,
            ));

            $viewMonthlyInvoice->setTemplate('reporting/invoice-report/generate-monthly-invoice-pdf');
            $viewMonthlyInvoice->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewMonthlyInvoice);
            $pdfPath = $this->downloadPDF('monthly-invoice-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function monthlyInvoiceCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $paymentTerm = $request->getPost('paymentTerm');
            $locationID = $request->getPost('locationID');

            $monthlyInvoiceDetails = $this->getMonthlyInvoiceDetails($fromMonth, $toMonth, $paymentTerm, $locationID);
            $cD = $this->getCompanyDetails();

            if ($monthlyInvoiceDetails) {
                $title = '';
                $tit = 'MONTHLY INVOICE REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                    3 => ""
                );
                $title = implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "Period : " . $fromMonth . '-' . $toMonth
                );
                $title.=implode(",", $in) . "\n";

                if ($checkTaxStatus == 'true') {
                    $arrs = array("YEAR", "MONTH", "", "NAME", "INVOICE NUMBER", "ISSUED DATE", "DUE DATE", "TOTAL AMOUNT", "PAID AMOUNT", "TAX");
                    $header = implode(",", $arrs);
                    $arr = '';
                    $netTax = 0.0;
                    $netAmount = 0.0;

                    foreach ($monthlyInvoiceDetails as $key => $value) {
                        $in = array(
                            0 => $key,
                            1 => "",
                            2 => " ",
                            3 => "",
                            4 => "",
                            5 => "",
                            6 => "",
                            7 => "",
                            8 => "",
                            9 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        foreach ($value as $key1 => $value1) {


                            $in = array(
                                0 => "",
                                1 => $key1,
                                2 => " ",
                                3 => "",
                                4 => "",
                                5 => "",
                                6 => "",
                                7 => "",
                                8 => "",
                                9 => ""
                            );
                            $arr.=implode(",", $in) . "\n";
                            $subAmount = 0.00;
                            $subTax = 0.00;
                            foreach ($value1 as $key2 => $value2) {

                                $amount = $value2['TotalAmount'];
                                $amount = $amount ? $amount : 0.00;
                                $subAmount += $amount;
                                $netAmount += $amount;
                                $tax = $value2['TotalTax'];
                                $tax = $tax ? $tax : 0.00;
                                $subTax += $tax;
                                $netTax += $tax;

                                $in = array(
                                    0 => "",
                                    1 => "",
                                    2 => ($value2['customerID'] == 0) ? '' : $value2['CusTitle'],
                                    3 => $value2['CusName'],
                                    4 => $value2['InvoiceNumber'],
                                    5 => $value2['IssuedDate'],
                                    6 => $value2['DueDate'],
                                    7 => $value2['TotalAmount'],
                                    8 => $value2['PayedAmount'],
                                    9 => $value2['TotalTax']
                                );
                                $arr.=implode(",", $in) . "\n";
                            }
                            $in = array(
                                0 => "",
                                1 => "",
                                2 => "",
                                3 => "",
                                4 => "",
                                5 => "",
                                6 => "",
                                7 => "",
                                8 => ""
                            );
                            $arr.=implode(",", $in) . "\n";
                            $in = array(
                                0 => "",
                                1 => "",
                                2 => "",
                                3 => "",
                                4 => "",
                                5 => "",
                                6 => "monthly Sales",
                                7 => $subAmount,
                                8 => ""
                            );
                            $arr.=implode(",", $in) . "\n";
                            $in = array(
                                0 => "",
                                1 => "",
                                2 => "",
                                3 => "",
                                4 => "",
                                5 => "",
                                6 => "monthly Tax",
                                7 => $subTax,
                                8 => ""
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "Total Sales",
                        7 => ($netAmount),
                        8 => ""
                    );
                    $arr.=implode(",", $in) . "\n";

                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "Total Tax",
                        7 => ($netTax),
                        8 => ""
                    );
                    $arr.=implode(",", $in) . "\n";

                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "Sales Excluding Tax",
                        7 => ($netAmount - $netTax),
                        8 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $arrs = array("YEAR", "MONTH", "NAME", "INVOICE NUMBER", "ISSUED DATE", "DUE DATE", "TOTAL AMOUNT", "PAID AMOUNT");
                    $header = implode(",", $arrs);
                    $arr = '';

                    $net_amount = 0.0;
                    foreach ($monthlyInvoiceDetails as $key => $value) {
                        $in = array(
                            0 => $key,
                            1 => "",
                            2 => " ",
                            3 => "",
                            4 => "",
                            5 => "",
                            6 => "",
                            7 => ""
                        );
                        $arr.=implode(",", $in) . "\n";

                        foreach ($value as $key1 => $value1) {
                            $in = array(
                                0 => "",
                                1 => $key1,
                                2 => " ",
                                3 => "",
                                4 => "",
                                5 => "",
                                6 => "",
                                7 => ""
                            );
                            $arr.=implode(",", $in) . "\n";
                            $sub_amount = 0.00;
                            foreach ($value1 as $key2 => $value2) {

                                $sub_amount+= $value2['TotalAmount'];
                                $net_amount +=$value2['TotalAmount'];
                                $in = array(
                                    0 => "",
                                    1 => "",
                                    2 => ($value2['customerID'] == 0) ? $value2['CusName'] : $value2['CusTitle'] . ' ' . $value2['CusName'],
                                    3 => $value2['InvoiceNumber'],
                                    4 => $value2['IssuedDate'],
                                    5 => $value2['DueDate'],
                                    6 => $value2['TotalAmount'],
                                    7 => $value2['PayedAmount']
                                );
                                $arr.=implode(",", $in) . "\n";
                            }
                            $in = array(
                                1 => " ",
                                2 => " ",
                                3 => "",
                                4 => " ",
                                5 => " ",
                                6 => "Monthly Sales",
                                7 => $sub_amount
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                    $in = array(
                        1 => " ",
                        2 => " ",
                        3 => "",
                        4 => " ",
                        5 => " ",
                        6 => "",
                        7 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                    $in = array(
                        1 => " ",
                        2 => " ",
                        3 => "",
                        4 => " ",
                        5 => " ",
                        6 => "Total Sales",
                        7 => $net_amount
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Monthly_Invoice_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function dailyInvoiceViewAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $paymentTerm = $request->getPost('paymentTerm');
            $locationID = $request->getPost('locationID');

            $translator = new Translator();
            $name = $translator->translate('Daily Invoice Report');
            $period = $fromDate . ' - ' . $toDate;

            $dailyInvoiceData = $this->getDailyInvoiceDetails($fromDate, $toDate, $paymentTerm, $locationID);
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailyInvoiceView = new ViewModel(array(
                'cD' => $cD,
                'dailyInvoiceData' => $dailyInvoiceData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'checkTaxStatus' => $checkTaxStatus,
                'headerTemplate' => $headerViewRender,
            ));
            $dailyInvoiceView->setTemplate('reporting/invoice-report/generate-daily-invoice-pdf');

            $this->html = $dailyInvoiceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getDailyInvoiceDetails($fromDate = NULL, $toDate = NULL, $paymentTerm = NULL, $locationID = NULL)
    {
        if (isset($fromDate) && isset($toDate)) {
            $result = $this->CommonTable('Invoice\Model\InvoiceTable')->getDailyInvoiceData($fromDate, $toDate, $paymentTerm, $locationID);
            $respondDays = $this->CommonTable('Invoice\Model\InvoiceTable')->getDailySalesDataDays($fromDate, $toDate);
            $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteDetailsByInvoiceDate($fromDate, $toDate, $locationID);
            //set credit note data to the array
            foreach ($creditNoteDetails as $val) {
                if($creditNoteID != $val['creditNoteID']){
                    $creditNoteDataArray[$val['invoiceID']]['TotalCreditAmount'] += floatval($val['TotalCreditAmount']);
                }
                $creditNoteDataArray[$val['invoiceID']]['creditNoteID'] = $val['creditNoteID'];
                $creditNoteDataArray[$val['invoiceID']]['creditNoteTaxAmount'] += floatval($val['creditNoteTaxAmount']);
                $creditNoteID = $val['creditNoteID'];
            }

            $respond = [];
            foreach ($result as $val) {
                $respond[] = $val;
            }

            $respondData = $respond ? $respond : [];
            $days = $respondDays;
            $dailyInvoiceData = [];

            foreach ($days as $day) {
                $dailyInvoiceData[$day] = [];
            }

            foreach ($respondData as $res) {
                //check that given invoice has any credit notes.
                if(count($creditNoteDataArray[$res['invoiceID']]) > 0){
                    //check that all items are return or not that given invoice
                    if($res['TotalAmount'] != $creditNoteDataArray[$res['invoiceID']]['TotalCreditAmount']){
                        $res['TotalAmount'] = $res['TotalAmount'] - $creditNoteDataArray[$res['invoiceID']]['TotalCreditAmount'];
                        $res['TotalTax'] = $res['TotalTax'] - $creditNoteDataArray[$res['invoiceID']]['creditNoteTaxAmount'];

                        $dailyInvoiceData[$res['Day']][$res['InvoiceNumber']] = $res;
                    }
                }else{
                    $dailyInvoiceData[$res['Day']][$res['InvoiceNumber']] = $res;
                }

            }

            return $dailyInvoiceData;
        }
    }

    public function dailyInvoicePdfAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $paymentTerm = $request->getPost('paymentTerm');
            $locationID = $request->getPost('locationID');

            $name = 'Daily Invoice Report';
            $period = $fromDate . ' - ' . $toDate;

            $dailyInvoiceData = $this->getDailyInvoiceDetails($fromDate, $toDate, $paymentTerm, $locationID);
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewDailyInvoice = new ViewModel(array(
                'cD' => $cD,
                'dailyInvoiceData' => $dailyInvoiceData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'checkTaxStatus' => $checkTaxStatus,
                'headerTemplate' => $headerViewRender,
            ));

            $viewDailyInvoice->setTemplate('reporting/invoice-report/generate-daily-invoice-pdf');
            $viewDailyInvoice->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewDailyInvoice);
            $pdfPath = $this->downloadPDF('daily-invoice-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function dailyInvoiceCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $paymentTerm = $request->getPost('paymentTerm');
            $locationID = $request->getPost('locationID');

            $dailyInvoiceData = $this->getDailyInvoiceDetails($fromDate, $toDate, $paymentTerm, $locationID);
            $cD = $this->getCompanyDetails();

            if ($dailyInvoiceData) {
                $netTax = 0;
                $netAmount = 0;
                if ($checkTaxStatus == 'true') {
                    $title = '';
                    $tit = 'DAILY SALES REPORT';
                    $in = array(
                        0 => "",
                        1 => $tit,
                        2 => "",
                        3 => "",
                        4 => ""
                    );
                    $title = implode(",", $in) . "\n";

                    $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                    $title.=implode(",", $in) . "\n";

                    $in = array(
                        0 => $cD[0]->companyName,
                        1 => $cD[0]->companyAddress,
                        2 => 'Tel: ' . $cD[0]->telephoneNumber
                    );
                    $title.=implode(",", $in) . "\n";

                    $in = array(
                        0 => "Period : " . $fromDate . '-' . $toDate
                    );
                    $title.=implode(",", $in) . "\n";

                    $arrs = array("DATE", "NAME", "INVOICE NUMBER", "ISSUED DATE", "DUE DATE", "TOTAL AMOUNT", "PAID AMOUNT", "TAX");
                    $header = implode(",", $arrs);
                    $arr = '';

                    foreach ($dailyInvoiceData as $invID => $value1) {
                        $subAmount = 0.00;
                        $subTax = 0.00;
                        $in = array(
                            0 => $invID,
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => "",
                            6 => "",
                            7 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        foreach ($value1 as $key1 => $value2) {
                            $taxAmount = $value2['TotalTax'];
                            $taxAmount = $taxAmount ? $taxAmount : 0.00;
                            $amount = $value2['TotalAmount'];
                            $amount = $amount ? $amount : 0.00;
                            $netTax += $taxAmount;
                            $netAmount += $amount;
                            $subTax += $taxAmount;
                            $subAmount += $amount;
                            $name = ($value2['customerID'] == 0) ? $value2['CusName'] : $value2['CusTitle']  . ' ' .  $value2['CusName'];
                            $in = array(
                                0 => "",
                                1 => str_replace(',', " ",$name) ,
                                2 => $value2['InvoiceNumber'],
                                3 => $value2['IssuedDate'],
                                4 => $value2['DueDate'],
                                5 => $value2['TotalAmount'],
                                6 => $value2['PayedAmount'],
                                7 => $value2['TotalTax']
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => "",
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "Total Tax  of $invID",
                            5 => $subTax,
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "Total Sales  of $invID",
                            5 => $subAmount,
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "Total Sales",
                        5 => $netAmount,
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";

                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "Total Tax",
                        5 => $netTax,
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";

                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "Sales Excluding Tax",
                        5 => $netAmount - $netTax,
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $title = '';
                    $tit = 'DAILY INVOICE REPORT';
                    $in = array(
                        0 => "",
                        1 => $tit,
                        2 => "",
                        3 => "",
                        4 => ""
                    );
                    $title = implode(",", $in) . "\n";

                    $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                    $title.=implode(",", $in) . "\n";

                    $in = array(
                        0 => "Period : " . $fromDate . '-' . $toDate
                    );
                    $title.=implode(",", $in) . "\n";

                    $arrs = array("DATE", "NAME", "INVOICE NUMBER", "ISSUED DATE", "DUE DATE", "TOTAL AMOUNT", "PAID AMOUNT");
                    $header = implode(",", $arrs);
                    $arr = '';

                    foreach ($dailyInvoiceData as $invID => $value1) {

                        $in = array(
                            0 => $invID,
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => "",
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        $sub_amount = 0.00;
                        foreach ($value1 as $key1 => $value2) {
                            $amount = $value2['TotalAmount'];
                            $amount = $amount ? $amount : 0.00;
                            $netAmount += $amount;
                            $sub_amount += $amount;
                            $name = ($value2['customerID'] == 0) ? $value2['CusName'] : $value2['CusTitle']  . ' ' .  $value2['CusName'];
                            $in = array(
                                0 => "",
                                1 => str_replace(',', " ", $name),
                                2 => $value2['InvoiceNumber'],
                                3 => $value2['IssuedDate'],
                                4 => $value2['DueDate'],
                                5 => $value2['TotalAmount'],
                                6 => $value2['PayedAmount']
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => "Total Sales",
                            6 => $sub_amount
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "GRAND TOTAL",
                        6 => $netAmount
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Daily_Sales_Report";

            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function annualInvoiceViewAction()
    {

        $request = $this->getRequest();

        if ($request->isPost()) {
            $year = $request->getPost('year');
            $paymentTerm = $request->getPost('paymentTerm');
            $locationID = $request->getPost('locationID');
            $checkTaxStatus = $request->getPost('checkTaxStatus');

            $translator = new Translator();
            $name = $translator->translate('Annual Invoice Report');
            $period = $year;

            $annualInvoiceData = $this->getAnnualInvoiceDetails($year, $paymentTerm, $locationID);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $annualInvoiceView = new ViewModel(array(
                'cD' => $companyDetails,
                'annualInvoiceData' => $annualInvoiceData,
                'year' => $year,
                'checkTaxStatus' => $checkTaxStatus,
                'headerTemplate' => $headerViewRender,
            ));

            $annualInvoiceView->setTemplate('reporting/invoice-report/generate-annual-invoice-pdf');

            $this->html = $annualInvoiceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getAnnualInvoiceDetails($selectedYear = NULL, $paymentTerm = NULL, $locationID = NULL)
    {
        if (isset($selectedYear)) {
            $respondAmount = $this->CommonTable('Invoice\Model\InvoiceTable')->getAnnualInvoiceData($selectedYear, $paymentTerm, $locationID);
            $respondAmountData = $respondAmount ? $respondAmount : [];
            $beginYear = $selectedYear . '-01-01';
            $endYear = $selectedYear . '-12-31';
            $months = $this->CommonTable('Invoice\Model\InvoiceTable')->getMonthsYearBetweenTwoDates($beginYear, $endYear);
            $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteDetailsByInvoiceDate($beginYear, $endYear, $locationID);
            //set credit note data to the array
            foreach ($creditNoteDetails as $val) {
                if($creditNoteID != $val['creditNoteID']){
                    $creditNoteDataArray[$val['invoiceID']]['TotalCreditAmount'] += floatval($val['TotalCreditAmount']);
                }
                $creditNoteDataArray[$val['invoiceID']]['creditNoteID'] = $val['creditNoteID'];
                $creditNoteDataArray[$val['invoiceID']]['creditNoteTaxAmount'] += floatval($val['creditNoteTaxAmount']);
                $creditNoteID = $val['creditNoteID'];
            }


            $annualInvoiceDetails = array();
            $years = $respondYears;
            foreach ($months as $month) {
                $temp = split(' ', $month);
                if ($temp[0] == 'January' || $temp[0] == 'February' || $temp[0] == 'March') {
                    $annualInvoiceDetails['1st Quarter'][0] = array();
                } elseif ($temp[0] == 'April' || $temp[0] == 'May' || $temp[0] == 'June') {
                    $annualInvoiceDetails['2nd Quarter'][0] = array();
                } elseif ($temp[0] == 'July' || $temp[0] == 'August' || $temp[0] == 'September') {
                    $annualInvoiceDetails['3rd Quarter'][0] = array();
                } elseif ($temp[0] == 'October' || $temp[0] == 'November' || $temp[0] == 'December') {
                    $annualInvoiceDetails['4th Quarter'][0] = array();
                }
            }

            foreach ($respondAmountData as $res) {
                //check that given invoice has any credit notes.
                if(count($creditNoteDataArray[$res['invoiceID']]) > 0){
                    //check that all items are return or not that given invoice
                    if($res['TotalAmount'] != $creditNoteDataArray[$res['invoiceID']]['TotalCreditAmount']){
                        $res['TotalAmount'] = $res['TotalAmount'] - $creditNoteDataArray[$res['invoiceID']]['TotalCreditAmount'];
                        $res['TotalTax'] = $res['TotalTax'] - $creditNoteDataArray[$res['invoiceID']]['creditNoteTaxAmount'];

                    }else {
                        continue;
                    }
                }

                if ($res['Month'] == 'January' || $res['Month'] == 'February' || $res['Month'] == 'March') {
                    $annualInvoiceDetails['1st Quarter'][$res['InvoiceNumber']] = $res;
                } elseif ($res['Month'] == 'April' || $res['Month'] == 'May' || $res['Month'] == 'June') {
                    $annualInvoiceDetails['2nd Quarter'][$res['InvoiceNumber']] = $res;
                } elseif ($res['Month'] == 'July' || $res['Month'] == 'August' || $res['Month'] == 'September') {
                    $annualInvoiceDetails['3rd Quarter'][$res['InvoiceNumber']] = $res;
                } elseif ($res['Month'] == 'October' || $res['Month'] == 'November' || $res['Month'] == 'December') {
                    $annualInvoiceDetails['4th Quarter'][$res['InvoiceNumber']] = $res;
                }
            }

            return $annualInvoiceDetails;
        }
    }

    public function annualInvoicePdfAction()
    {
        $requset = $this->getRequest();
        if ($requset->isPost()) {
            $year = $requset->getPost('year');
            $checkTaxStatus = $requset->getPost('checkTaxStatus');
            $paymentTerm = $requset->getPost('paymentTerm');
            $locationID = $requset->getPost('locationID');

            $translator = new Translator();
            $name = $translator->translate('Annual Invoice Report');
            $period = $year;

            $annualInvoiceData = $this->getAnnualInvoiceDetails($year, $paymentTerm, $locationID);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewAnnualInvoice = new ViewModel(array(
                'cD' => $companyDetails,
                'annualInvoiceData' => $annualInvoiceData,
                'checkTaxStatus' => $checkTaxStatus,
                'year' => $year,
                'headerTemplate' => $headerViewRender,
            ));

            $viewAnnualInvoice->setTemplate('reporting/invoice-report/generate-annual-invoice-pdf');
            $viewAnnualInvoice->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewAnnualInvoice);
            $pdfPath = $this->downloadPDF('Annual_Invoice_Report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function annualInvoiceCsvAction()
    {
        $request = $this->getRequest();
        if ($request) {
            $year = $request->getPost('year');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $paymentTerm = $request->getPost('paymentTerm');
            $locationID = $request->getPost('locationID');

            $annualInvoiceData = $this->getAnnualInvoiceDetails($year, $paymentTerm, $locationID);
            $cD = $this->getCompanyDetails();

            if ($annualInvoiceData) {

                $netTax = 0.00;
                $netAmount = 0.00;
                if ($checkTaxStatus == 'true') {
                    $title = '';
                    $tit = 'ANNUAL INVOICE REPORT';
                    $in = array(
                        0 => "",
                        1 => $tit,
                        2 => "",
                        3 => "",
                        4 => ""
                    );
                    $title = implode(",", $in) . "\n";

                    $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                    $title.=implode(",", $in) . "\n";

                    $in = array(
                        0 => $cD[0]->companyName,
                        1 => $cD[0]->companyAddress,
                        2 => 'Tel: ' . $cD[0]->telephoneNumber
                    );
                    $title.=implode(",", $in) . "\n";

                    $in = array(
                        0 => "Period : " . $year
                    );
                    $title.=implode(",", $in) . "\n";

                    $arrs = array("QUARTER", "NAME", "INVOICE NUMBER", "ISSUED DATE", "DUE DATE", "TOTAL AMOUNT", "PAID AMOUNT", "TAX");
                    $header = implode(",", $arrs);
                    $arr = '';

                    foreach ($annualInvoiceData as $invID => $value1) {

                        $subTax = 0.00;
                        $subAmount = 0.00;
                        $in = array(
                            0 => $invID,
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => "",
                            6 => "",
                            7 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        foreach ($value1 as $key1 => $value2) {

                            $taxAmount = $value2['TotalTax'];
                            $taxAmount = $taxAmount ? $taxAmount : 0.00;
                            $amount = $value2['TotalAmount'];
                            $amount = $amount ? $amount : 0.00;
                            $netTax += $taxAmount;
                            $netAmount += $amount;
                            $subTax += $taxAmount;
                            $subAmount += $amount;

                            $in = array(
                                0 => "",
                                1 => ($value2['customerID'] == 0) ? $value2['CusName'] : $value2['CusTitle'] . '' . $value2['CusName'],
                                2 => $value2['InvoiceNumber'],
                                3 => $value2['IssuedDate'],
                                4 => $value2['DueDate'],
                                5 => $value2['TotalAmount'],
                                6 => $value2['PayedAmount'],
                                7 => $value2['TotalTax']
                            );
                            $arr.=implode(",", $in) . "\n";
                        }

                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "Sales of " . $invID,
                            5 => $subAmount,
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "Tax of " . $invID,
                            5 => $subTax,
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "Total Sales",
                        5 => $netAmount,
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";

                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "Total Tax",
                        5 => $netTax,
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";

                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "Sales Excluding Tax",
                        5 => $netAmount - $netTax,
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $title = '';
                    $tit = 'ANNUAL INVOICE REPORT';
                    $in = array(
                        0 => "",
                        1 => $tit,
                        2 => "",
                        3 => "",
                        4 => ""
                    );
                    $title = implode(",", $in) . "\n";

                    $in = array(
                        0 => "Period : " . $year
                    );
                    $title.=implode(",", $in) . "\n";

                    $arrs = array("QUARTER", "NAME", "INVOICE NUMBER", "ISSUED DATE", "DUE DATE", "TOTAL AMOUNT", "PAID AMOUNT");
                    $header = implode(",", $arrs);
                    $arr = '';

                    $net_amount = 0.00;
                    foreach ($annualInvoiceData as $invID => $value1) {
                        $in = array(
                            0 => $invID,
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => "",
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        $sub_amount = 0.00;
                        foreach ($value1 as $key1 => $value2) {
                            $amount = $value2['TotalAmount'];
                            $amount = $amount ? $amount : 0.00;
                            $net_amount += $amount;
                            $sub_amount += $amount;
                            $in = array(
                                0 => "",
                                1 => ($value2['customerID'] == 0) ? $value2['CusName'] : $value2['CusTitle'] . '' . $value2['CusName'],
                                2 => $value2['InvoiceNumber'],
                                3 => $value2['IssuedDate'],
                                4 => $value2['DueDate'],
                                5 => $value2['TotalAmount'],
                                6 => $value2['PayedAmount']
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => "",
                            6 => ""
                        );
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "Sales of " . $invID,
                            5 => $sub_amount,
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "Grand Total Sales",
                        5 => $net_amount,
                        6 => ""
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = "no matching records found\n";
            }


            $name = "Annual_Invoice_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getInvoiceSummeryDetails($fromDate, $toDate)
    {
        if (isset($fromDate) && isset($toDate)) {
            $salesSummeryWithOutTax = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceSummeryWithOutTax($fromDate, $toDate);
            $salesSummeryWithOutTaxData = $salesSummeryWithOutTax ? $salesSummeryWithOutTax : array();
            $salesSummeryWithTax = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesSummeryWithTax($fromDate, $toDate);
            $salesSummeryWithTaxData = $salesSummeryWithTax ? $salesSummeryWithTax : array();
            $result = array_replace_recursive($salesSummeryWithOutTaxData, $salesSummeryWithTaxData);

            $invoiceSummeryData = array();
            foreach ($result as $res) {
                $invoiceSummeryData[$res['customerID']][$res['InvoiceNumber']] = $res;
            }

            return $invoiceSummeryData;
        }
    }

    public function summeryInvoiceViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Invoice Summary Report');
//            $name = 'Invoice Summary Report';
            $period = $fromDate . ' - ' . $toDate;

            $invoiceSummeryData = $this->getInvoiceSummeryDetails($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $invoiceSummeryView = new ViewModel(array(
                'cD' => $companyDetails,
                'invoiceSummeryData' => $invoiceSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $invoiceSummeryView->setTemplate('reporting/invoice-report/generate-invoice-summery-pdf');

            $this->html = $invoiceSummeryView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function summeryInvoicePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Invoice Summary Report');
//            $name = 'Invoice Summary Report';
            $period = $fromDate . ' - ' . $toDate;

            $invoiceSummeryData = $this->getInvoiceSummeryDetails($fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewInvoiceSummery = new ViewModel(array(
                'cD' => $companyDetails,
                'invoiceSummeryData' => $invoiceSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $viewInvoiceSummery->setTemplate('reporting/invoice-report/generate-invoice-summery-pdf');
            $viewInvoiceSummery->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewInvoiceSummery);
            $pdfPath = $this->downloadPDF('Invoice_Summary_Report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function summeryInvoiceCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $invoiceSummeryData = $this->getInvoiceSummeryDetails($fromDate, $toDate);
            $cD = $this->getCompanyDetails();

            $netTax = 0;

            $title = '';
            $tit = 'INVOICE SUMMARY REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => ""
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => $cD[0]->companyName,
                1 => $cD[0]->companyAddress,
                2 => 'Tel: ' . $cD[0]->telephoneNumber
            );
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("CUSTOMER NAME", "SHORT NAME", "INVOICE NUMBER", "ISSUED DATE", "DUE DATE", "TOTAL AMOUNT", "PAID AMOUNT");
            $header = implode(",", $arrs);
            $arr = '';
            $netAmount = 0.00;
            if (empty($invoiceSummeryData)) {
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => " "
                );
                $arr.=implode(",", $in) . "\n";
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "NO DATA",
                    6 => " "
                );
                $arr.=implode(",", $in) . "\n";
            } else {


                foreach ($invoiceSummeryData as $key => $data) {
                    if ($key != "") {
                        $subAmount = 0.00;
                        foreach ($data as $key1 => $data1) {

                            $subAmount+=$data1['TotalAmount'];
                            $netAmount+=$data1['TotalAmount'];
                            $in = array(
                                0 => $data1['customerTitle'] . '' . $data1['customerName'],
                                1 => $data1['customerShortName'],
                                2 => $data1['InvoiceNumber'],
                                3 => $data1['IssuedDate'],
                                4 => $data1['DueDate'],
                                5 => $data1['TotalAmount'],
                                6 => $data1['PayedAmount']
                            );
                            $arr.=implode(",", $in) . "\n";
                        }


                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => " ",
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "sub Total of " . $data1['customerName'],
                            5 => $subAmount,
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => "",
                            4 => "",
                            5 => " ",
                            6 => ""
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                }
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => ""
                );
                $arr.=implode(",", $in) . "\n";
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "Net Total",
                    5 => $netAmount,
                    6 => ""
                );
                $arr.=implode(",", $in) . "\n";
            }

            $name = "invoice-summary-report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getOpenInvoiceWithCreditNoteDetails($taxType, $customerIds, $isAllCustomers, $salesPersonIds)
    {
        $dataList = [];
        if($isAllCustomers) {
            $customerIds = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomers(array('customer.customerName ASC'));
        }
        $withDefaultSalesPerson = (in_array('other', $salesPersonIds)) ? true : false;

        foreach ($customerIds as $customerId) {
            //get customer details
            $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerId);
            //customer invoices
            $invoices = $this->CommonTable("Invoice\Model\InvoiceTable")->getOpenInvoicesByCustomerIdAndSalesPersonIds($customerId, $taxType, $salesPersonIds, $withDefaultSalesPerson);

            $invoicesArr = $columnsArr = [];
            $invoiceAmount = $paidAmount = $remainingAmount = $creditNotesAmount = 0;

            foreach ($invoices as $invoice) {
                $customerInvoice = $invoice;

                $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceID($invoice['salesInvoiceID']);

                $creditNoteAmountArr = array_map(function($creditNote) {
                    return ($creditNote['creditNoteTotal'] / $creditNote['creditNoteCustomCurrencyRate']);
                }, iterator_to_array($creditNotes));
                $creditNoteAmount = array_sum($creditNoteAmountArr);

                $today = new \DateTime("now");
                $dueDate = new \DateTime($invoice['salesInvoiceOverDueDate']);
                $interval = $today->diff($dueDate);

                $customerInvoice['days'] = $interval->format('%R%a days');
                $customerInvoice['salesinvoiceCreditNoteAmount'] = $creditNoteAmount;
                $customerInvoice['remainingAmount'] = $invoice['salesinvoiceTotalAmount'] - $creditNoteAmount - $invoice['salesInvoicePayedAmount'];
                $invoiceAmount = $invoiceAmount + $invoice['salesinvoiceTotalAmount'];
                $creditNotesAmount = $creditNotesAmount + $creditNoteAmount;
                $paidAmount = $paidAmount + $invoice['salesInvoicePayedAmount'];
                $remainingAmount = $remainingAmount + $customerInvoice['remainingAmount'];
                $columnsArr[] = $customerInvoice;
            }
            $invoicesArr['columns'] = $columnsArr;
            $invoicesArr['amount'] = array(
                'invoiceAmount' => $invoiceAmount,
                'paidAmount' => $paidAmount,
                'remainingAmount' => $remainingAmount,
                'creditNotesAmount' => $creditNotesAmount
                    );
            if (!empty($columnsArr)) {
                $dataList[$customer->customerName.'-'.$customer->customerCode] = $invoicesArr;
            }
        }
        return $dataList;
    }

    public function openInvoiceWithCreditNoteReportAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespond();
        }

        $postData = $request->getPost()->toArray();
        $taxType = ($postData['taxType']) ? $postData['taxType'] : [];
        $customerIds = ($postData['customerIds']) ? $postData['customerIds'] : [];
        $isAllCustomers = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
        $salesPersonIds = ($postData['salesPersonIds']) ? $postData['salesPersonIds'] : [];

        $dataList = $this->getOpenInvoiceWithCreditNoteDetails($taxType, $customerIds, $isAllCustomers, $salesPersonIds);

        $translator = new Translator();
        $name = $translator->translate('Customer wise open invoice with credit notes');
        $period = 'All time';

        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $openInvoiceView = new ViewModel(array(
            'cD' => $this->getCompanyDetails(),
            'invoiceData' => $dataList,
            'headerTemplate' => $headerViewRender,
        ));

        $openInvoiceView->setTemplate('reporting/invoice-report/generate-open-invoice-with-credit-notes-pdf');

        $this->html = $openInvoiceView;
        $this->status = true;
        return $this->JSONRespondHtml();
    }

    public function openInvoiceWithCreditNotePdfAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespond();
        }

        $postData = $request->getPost()->toArray();
        $taxType = ($postData['taxType']) ? $postData['taxType'] : [];
        $customerIds = ($postData['customerIds']) ? $postData['customerIds'] : [];
        $isAllCustomers = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
        $salesPersonIds = ($postData['salesPersonIds']) ? $postData['salesPersonIds'] : [];

        $dataList = $this->getOpenInvoiceWithCreditNoteDetails($taxType, $customerIds, $isAllCustomers, $salesPersonIds);

        $translator = new Translator();
        $name = $translator->translate('Customer wise open invoice with credit notes');
        $period = 'All time';

        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $openInvoiceView = new ViewModel(array(
            'cD' => $this->getCompanyDetails(),
            'invoiceData' => $dataList,
            'headerTemplate' => $headerViewRender,
        ));

        $openInvoiceView->setTemplate('reporting/invoice-report/generate-open-invoice-with-credit-notes-pdf');
        $openInvoiceView->setTerminal(true);

        $htmlContent = $this->viewRendererHtmlContent($openInvoiceView);
        $pdfPath = $this->downloadPDF('customer_wise_open_invoice_with_credit_note', $htmlContent, true);

        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function openInvoiceWithCreditNoteCsvAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespond();
        }

        $postData = $request->getPost()->toArray();
        $taxType = ($postData['taxType']) ? $postData['taxType'] : [];
        $customerIds = ($postData['customerIds']) ? $postData['customerIds'] : [];
        $isAllCustomers = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
        $salesPersonIds = ($postData['salesPersonIds']) ? $postData['salesPersonIds'] : [];

        $dataList = $this->getOpenInvoiceWithCreditNoteDetails($taxType, $customerIds, $isAllCustomers, $salesPersonIds);

        $cD = $this->getCompanyDetails();

        $title = ' CUSTOMER WISE OPEN INVOICE REPORT WITH CREDIT NOTES';
        $in = array(
            0 => "",
            1 => $title,
            2 => ""
        );
        $title = implode(",", $in) . "\n";

        $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
        $title.=implode(",", $in) . "\n";

        $in = array(
            0 => $cD[0]->companyName,
            1 => $cD[0]->companyAddress,
            2 => 'Tel: ' . $cD[0]->telephoneNumber
        );
        $title.=implode(",", $in) . "\n";

        $in = array(
            0 => "Period : All time"
        );
        $title.=implode(",", $in) . "\n";

        $arrs = array("CUSTOMER NAME", "INVOICE NUMBER", "INVOICE DATE", "COMMENT", "DUE DATE", "DAYS", "TOTAL AMOUNT", "CREDIT NOTE AMOUNT", "PAID AMOUNT", "REMAINING AMOUNT");
        $header = implode(",", $arrs);
        $arr = '';

        $grandInvoiceAmount = 0;
        $grandPaidAmount = 0;
        $grandRemainingAmount = 0;
        $grandCreditNoteAmount = 0;

        if (!empty($dataList)) {
            foreach ($dataList as $customer => $customerInvoices) {
                
                $in = array(
                    0 => str_replace(',', "", $customer),
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => " ",
                    7 => "",
                    8 => " ",
                    9 => " "
                );
                $arr.=implode(",", $in) . "\n";

                foreach ($customerInvoices['columns'] as $invoice) {
                    $in = array(
                        0 => "",
                        1 => $invoice['salesInvoiceCode'],
                        2 => $invoice['salesInvoiceIssuedDate'],
                        3 => $invoice['salesInvoiceComment'],
                        4 => $invoice['salesInvoiceOverDueDate'],
                        5 => $invoice['days'],
                        6 => number_format($invoice['salesinvoiceTotalAmount'], 2, '.', ''),
                        7 => number_format($invoice['salesinvoiceCreditNoteAmount'], 2, '.', ''),
                        8 => number_format($invoice['salesInvoicePayedAmount'], 2, '.', ''),
                        9 => number_format($invoice['remainingAmount'], 2, '.', '')
                    );
                    $arr.=implode(",", $in) . "\n";
                }

                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "TOTLAL",
                    6 => number_format($customerInvoices['amount']['invoiceAmount'], 2, '.', ''),
                    7 => number_format($customerInvoices['amount']['creditNotesAmount'], 2, '.', ''),
                    8 => number_format($customerInvoices['amount']['paidAmount'], 2, '.', ''),
                    9 => number_format($customerInvoices['amount']['remainingAmount'], 2, '.', '')
                );
                $arr.=implode(",", $in) . "\n";

                $grandInvoiceAmount = $grandInvoiceAmount + $customerInvoices['amount']['invoiceAmount'];
                $grandCreditNoteAmount = $grandCreditNoteAmount + $customerInvoices['amount']['creditNotesAmount'];
                $grandPaidAmount = $grandPaidAmount + $customerInvoices['amount']['paidAmount'];
                $grandRemainingAmount = $grandRemainingAmount + $customerInvoices['amount']['remainingAmount'];
            }
            $in = array(
                0 => "",
                1 => "",
                2 => "",
                3 => "",
                4 => "",
                5 => "GRAND TOTLAL",
                6 => number_format($grandInvoiceAmount, 2, '.', ''),
                7 => number_format($grandCreditNoteAmount, 2, '.', ''),
                8 => number_format($grandPaidAmount, 2, '.', ''),
                9 => number_format($grandRemainingAmount, 2, '.', '')
            );
            $arr.=implode(",", $in) . "\n";
        } else {
            $in = array('no matching records found');
            $arr.=implode(",", $in) . "\n";
        }

        $name = "customer_wise_open_invoice_with_credit_notes";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    private function getCustomerWiseOpenInvoices($customerIds, $isAllCustomers, $taxType, $locationIDs)
    {
        if($isAllCustomers) {
            $customerIds = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomers(array('customer.customerName ASC'));
        }

        $invoices = $this->CommonTable("Invoice\Model\InvoiceTable")->getOpenInvoicesByCustomerId($customerIds, $taxType, $locationIDs);

        $invoicesArr = $columnsArr = [];
        $invoiceAmount = $paidAmount = $remainingAmount = [];

        foreach ($invoices as $invoice) {
            if (!isset($invoiceAmount[$invoice['customerID']])) {
                $invoiceAmount[$invoice['customerID']] = 0;
            }
            
            if (!isset($invoiceAmount[$invoice['customerID']])) {
                $invoiceAmount[$invoice['customerID']] = 0;
            }
            
            if (!isset($invoiceAmount[$invoice['customerID']])) {
                $invoiceAmount[$invoice['customerID']] = 0;
            }
            
            $customerInvoice = $invoice;

            $today = new \DateTime("now");
            $dueDate = new \DateTime($invoice['salesInvoiceOverDueDate']);
            $interval = $today->diff($dueDate);

            $customerInvoice['days'] = $interval->format('%R%a days');
            $customerInvoice['remainingAmount'] = $invoice['salesinvoiceTotalAmount'] - $invoice['salesInvoicePayedAmount'];
            $invoiceAmount[$invoice['customerID']] = $invoiceAmount[$invoice['customerID']] + $invoice['salesinvoiceTotalAmount'];
            $paidAmount[$invoice['customerID']] = $paidAmount[$invoice['customerID']] + $invoice['salesInvoicePayedAmount'];
            $remainingAmount[$invoice['customerID']] = $remainingAmount[$invoice['customerID']] + $customerInvoice['remainingAmount'];
            $columnsArr[$invoice['customerID']][] = $customerInvoice;
        }

        foreach ($customerIds as $value) {
            $invoicesArr['columns'] = $columnsArr[$value];
            $invoicesArr['amount'] = array('invoiceAmount' => $invoiceAmount[$value], 'paidAmount' => $paidAmount[$value], 'remainingAmount' => $remainingAmount[$value]);
            if (!empty($columnsArr[$value])) {
                $dataList[$invoicesArr['columns'][0]['customerID']] = $invoicesArr;
            }
        }

        return $dataList;
    }

    public function openInvoiceReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $taxType = ($postData['taxType']) ? $postData['taxType'] : [];
            $customerIds = ($postData['customerIds']) ? $postData['customerIds'] : [];
            $locationIDs = ($postData['locationIDs']) ? $postData['locationIDs'] : [];
            $isAllCustomers = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);

            $companyDetails = $this->getCompanyDetails();
            $dataList = $this->getCustomerWiseOpenInvoices($customerIds, $isAllCustomers, $taxType, $locationIDs);

            $translator = new Translator();
            $name = $translator->translate('Customer wise open invoice');
            $period = 'All time';

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $openInvoiceView = new ViewModel(array(
                'cD' => $companyDetails,
                'invoiceData' => $dataList,
                'headerTemplate' => $headerViewRender,
            ));

            $openInvoiceView->setTemplate('reporting/invoice-report/generate-open-invoice-pdf');

            $this->html = $openInvoiceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function openInvoicePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $taxType = ($postData['taxType']) ? $postData['taxType'] : [];
            $customerIds = ($postData['customerIds']) ? $postData['customerIds'] : [];
            $isAllCustomers = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
            $locationIDs = ($postData['locationIDs']) ? $postData['locationIDs'] : [];

            $companyDetails = $this->getCompanyDetails();
            $dataList = $this->getCustomerWiseOpenInvoices($customerIds, $isAllCustomers, $taxType, $locationIDs);

            $translator = new Translator();
            $name = $translator->translate('Customer wise open invoice');
            $period = 'All time';

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $openInvoiceView = new ViewModel(array(
                'cD' => $companyDetails,
                'invoiceData' => $dataList,
                'headerTemplate' => $headerViewRender,
            ));

            $openInvoiceView->setTemplate('reporting/invoice-report/generate-open-invoice-pdf');
            $openInvoiceView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($openInvoiceView);
            $pdfPath = $this->downloadPDF('customer_wise_open_invoice', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function openInvoiceCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $taxType = ($postData['taxType']) ? $postData['taxType'] : [];
            $customerIds = ($postData['customerIds']) ? $postData['customerIds'] : [];
            $isAllCustomers = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
            $locationIDs = ($postData['locationIDs']) ? $postData['locationIDs'] : [];

            $companyDetails = $this->getCompanyDetails();
            $dataList = $this->getCustomerWiseOpenInvoices($customerIds, $isAllCustomers, $taxType, $locationIDs);

            $title = '';
            $tit = ' CUSTOMER WISE OPEN INVOICE REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => ""
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => $companyDetails[0]->companyName,
                1 => $companyDetails[0]->companyAddress,
                2 => 'Tel: ' . $companyDetails[0]->telephoneNumber
            );
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => "Period : All time"
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("CUSTOMER NAME", "CUSTOMER CODE", "CUSTOMER TEL","INVOICE NUMBER", "INVOICE DATE", "COMMENT", "DUE DATE", "DAYS", "TOTAL AMOUNT", "PAID AMOUNT", "REMAINING AMOUNT");
            $header = implode(",", $arrs);
            $arr = '';

            $grandInvoiceAmount = 0;
            $grandPaidAmount = 0;
            $grandRemainingAmount = 0;

            if (!empty($dataList)) {
                foreach ($dataList as $customer => $customerInvoices) {
                    $in = array(
                        0 => str_replace(',', "", $customerInvoices['columns'][0]['customerName']),
                        1 => str_replace(',', "", $customerInvoices['columns'][0]['customerCode']),
                        2 => str_replace(',', "", $customerInvoices['columns'][0]['customerTelephoneNumber']),
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "",
                        7 => "",
                        8 => "",
                        9 => "",
                        10 => "",
                    );
                    $arr.=implode(",", $in) . "\n";

                    foreach ($customerInvoices['columns'] as $invoice) {
                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => $invoice['salesInvoiceCode'],
                            4 => $invoice['salesInvoiceIssuedDate'],
                            5 => $invoice['salesInvoiceComment'],
                            6 => $invoice['salesInvoiceOverDueDate'],
                            7 => $invoice['days'],
                            8 => number_format($invoice['salesinvoiceTotalAmount'], 2, '.', ''),
                            9 => number_format($invoice['salesInvoicePayedAmount'], 2, '.', ''),
                            10 => number_format($invoice['remainingAmount'], 2, '.', '')
                        );
                        $arr.=implode(",", $in) . "\n";
                    }

                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => "",
                        7 => "TOTLAL",
                        8 => number_format($customerInvoices['amount']['invoiceAmount'], 2, '.', ''),
                        9 => number_format($customerInvoices['amount']['paidAmount'], 2, '.', ''),
                        10 => number_format($customerInvoices['amount']['remainingAmount'], 2, '.', '')
                    );
                    $arr.=implode(",", $in) . "\n";

                    $grandInvoiceAmount = $grandInvoiceAmount + $customerInvoices['amount']['invoiceAmount'];
                    $grandPaidAmount = $grandPaidAmount + $customerInvoices['amount']['paidAmount'];
                    $grandRemainingAmount = $grandRemainingAmount + $customerInvoices['amount']['remainingAmount'];
                }
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "GRAND TOTLAL",
                    8 => number_format($grandInvoiceAmount, 2, '.', ''),
                    9 => number_format($grandPaidAmount, 2, '.', ''),
                    10 => number_format($grandRemainingAmount, 2, '.', '')
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $in = array('no matching records found');
                $arr.=implode(",", $in) . "\n";
            }

            $name = "customer_wise_open_invoice";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * view of invoice outstanding balance
     * @return JSONRespondHtml
     */
    public function viewOutstandingBalanceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $showTax = $postData['showTax'];
            $salesPersonIds = ($postData['salesPersonIds']) ? $postData['salesPersonIds'] : [];
            $customerIds = ($postData['customerIds']) ? $postData['customerIds'] : [];
            $isAllCustomers = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
            $itemIds = ($postData['itemIds']) ? $postData['itemIds'] : [];
            $cusCategory = ($postData['casCategory']) ? $postData['casCategory'] : null;

            $translator = new Translator();
            $name = $translator->translate('Sales Person Wise Outstanding Balance Report');
            $period = $fromDate . ' - ' . $toDate;
            $invoiceSalesPersonData = $this->_getOutstandingBalanceDetails($fromDate, $toDate, $salesPersonIds, $customerIds, $isAllCustomers, $itemIds, $cusCategory);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $salesPersonWiseOutstandingBalanceView = new ViewModel(array(
                'cD' => $companyDetails,
                'invoiceData' => $invoiceSalesPersonData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => $cusCategory,
                'showTax' => $showTax == 'true' ? true : false
            ));

            $salesPersonWiseOutstandingBalanceView->setTemplate('reporting/invoice-report/generate-sales-person-wise-outstanding-balance-pdf');

            $this->html = $salesPersonWiseOutstandingBalanceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * out put a pdf file related to given figures
     * @return JSONRespondHtml
     */
    public function generateOutstandingBalancePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $showTax = $postData['showTax'];
            $salesPersonIds = ($postData['salesPersonIds']) ? $postData['salesPersonIds'] : [];
            $customerIds = ($postData['customerIds']) ? $postData['customerIds'] : [];
            $isAllCustomers = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
            $itemIds = ($postData['itemIds']) ? $postData['itemIds'] : [];
            $cusCategory = ($postData['casCategory']) ? $postData['casCategory'] : null;

            $translator = new Translator();
            $name = $translator->translate('Sales Person Wise Outstanding Balance Report');
            $period = $fromDate . ' - ' . $toDate;

            $invoiceSalesPersonData = $this->_getOutstandingBalanceDetails($fromDate, $toDate, $salesPersonIds, $customerIds, $isAllCustomers, $itemIds, $cusCategory);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $salesPersonWiseOutstandingBalanceView = new ViewModel(array(
                'cD' => $companyDetails,
                'invoiceData' => $invoiceSalesPersonData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => $cusCategory,
                'showTax' => $showTax == 'true' ? true : false
            ));

            $salesPersonWiseOutstandingBalanceView->setTemplate('reporting/invoice-report/generate-sales-person-wise-outstanding-balance-pdf');
            $salesPersonWiseOutstandingBalanceView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($salesPersonWiseOutstandingBalanceView);
            $pdfPath = $this->downloadPDF('outstanding_balance_wise_sales_person', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * out put a pdf file related to given figures
     * @return JSONRespondHtml
     */
    public function generateOutstandingBalanceSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $fromDate = $postData['fromDate'];
            $showTax = $postData['showTax'] == 'true' ? true : false;
            $toDate = $postData['toDate'];
            $salesPersonIds = ($postData['salesPersonIds']) ? $postData['salesPersonIds'] : [];
            $customerIds = ($postData['customerIds']) ? $postData['customerIds'] : [];
            $isAllCustomers = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
            $itemIds = ($postData['itemIds']) ? $postData['itemIds'] : [];
            $cusCategory = ($postData['casCategory']) ? $postData['casCategory'] : null;

            $invoiceSalesPersonData = $this->_getOutstandingBalanceDetails($fromDate, $toDate, $salesPersonIds, $customerIds, $isAllCustomers, $itemIds, $cusCategory);
            
            $companyDetails = $this->getCompanyDetails();
            if($showTax){
                if (!empty($invoiceSalesPersonData)) {
                    $title = '';
                    $tit = 'Sales Person Wise Outstanding Balance Report';
                    $in = array(
                        0 => "",
                        1 => $tit,
                        2 => "",
                    );
                    $title = implode(",", $in) . "\n";

                    $title .= $companyDetails[0]->companyName . "\n";
                    $title .= $companyDetails[0]->companyAddress . "\n";
                    $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

                    $in = array(
                        0 => "Period : " . $fromDate . '-' . $toDate
                    );
                    $title.=implode(",", $in) . "\n";

                    if($cusCategory){
                        $arrs = array("SALES PERSON", "CUSTOMER NAME", "CUSTOMER CATEGORY", "INVOICE CODE", "INVOICE COMMENT","ISSUED DATE", "DUE DATE","SELECTED ITEM TOTAL", "TOTAL AMOUNT", "TAX","TOTAL TAX AMOUNT", "TOTAL AMOUNT WITHOUT TAX","PAID AMOUNT", "CREDIT NOTE AMOUNT","OUTSTANDING BALANCE");
                    } else {
                        $arrs = array("SALES PERSON", "CUSTOMER NAME", "INVOICE CODE", "INVOICE COMMENT","ISSUED DATE", "DUE DATE", "SELECTED ITEM TOTAL", "TOTAL AMOUNT", "TAX", "TOTAL TAX AMOUNT", "TOTAL AMOUNT WITHOUT TAX", "PAID AMOUNT", "CREDIT NOTE AMOUNT","OUTSTANDING BALANCE");

                    }
                    $header = implode(",", $arrs);
                    $arr = '';

                    $gTotalAmount = $gPaidAmount = $gTaxAmount = $gOutstandingAmount = $gCreditNoteAmount = $gWithoutTax = $gTotalSelectedItemTotal = 0;
                    $itemRealTotal = $invoiceSalesPersonData['onebyoneinvoicevalue'];
                                        
                    foreach ($invoiceSalesPersonData as $key => $data) {
                        $salesPerson = (($key === 'other')) ? 'Other' : $data['title'] . ' ' . $data['name'];

                        $in = array(
                            0 => $salesPerson,
                        );
                        $arr.=implode(",", $in) . "\n";

                        $count = 0;
                        if (!empty($data['data'])) {
                            $totalAmount = $paidAmount = $taxAmount = $outstandingAmount = $creditNoteAmount = $withoutTax = $totalSelectedItemTotal = 0;
                            foreach ($data['data'] as $customerID => $customerData) {
                                $cTotalAmount = $cPaidAmount = $cTaxAmount = $cOutstandingAmount = $cCreditNoteAmount = $cWithoutTax = $cusSelectedItemTotal = 0;

                                foreach ($customerData as $d) {
                                    $outstandingBalance = $d['salesinvoiceTotalAmount'] - $d['creditNoteTotal'] - $d['salesInvoicePayedAmount'];
                                    if ($outstandingBalance > 0) {
                                        $totalAmount += $d['salesinvoiceTotalAmount'];
                                        $paidAmount += $d['salesInvoicePayedAmount'];
                                        //$taxAmount += $d['salesInvoiceTaxAmount'];
                                        $creditNoteAmount += $d['creditNoteTotal'];
                                        $outstandingAmount += $outstandingBalance;
                                        
                                        $count++;
                                        $customerName = ($d['customerID'] == 0) ? $d['customerName'] : $d['customerTitle'] . ' ' . $d['customerName'];
                                        $taxArr = array();
                                        $totTax = 0;
                                        foreach ($data['taxData'][$d['customerID']][$d['salesInvoiceID']] as $key => $value) {

                                                        if($value['taxName'] != null && $value['taxAmount'] != 0){
                                                            $totTax += $value['taxAmount'];
                                                            $cTaxAmount += $value['taxAmount'];
                                                            $taxAmount += $value['taxAmount'];
                                                        $taxArr[] = $value['taxName'].' - '.number_format($value['taxAmount'], 2, '.','');
                                                    }
                                                    }
                                                    $cWithoutTax += $d['salesinvoiceTotalAmount'] - $totTax;

                                        //reset invoice comment
                                        $com = preg_replace('/\s+/', ' ',$d['salesInvoiceComment']);

                                        if($cusCategory){
                                            $in = array(
                                                0 => '',
                                                1 => str_replace(',', '', $customerName),
                                                2 => $d['customerCategoryName'],
                                                3 => $d['salesInvoiceCode'],
                                                4 => str_replace(',', '', $com),
                                                5 => $d['salesInvoiceIssuedDate'],
                                                6 => $d['salesInvoiceOverDueDate'],
                                                7 => $itemRealTotal[$d['salesInvoiceID']],
                                                8 => $d['salesinvoiceTotalAmount'],
                                                9 => implode(" / ",$taxArr),
                                                10 => $totTax,
                                                11 => $d['salesinvoiceTotalAmount'] - $totTax,
                                                12 => $d['salesInvoicePayedAmount'],
                                                13 => (is_null($d['creditNoteTotal'])) ? 0 : $d['creditNoteTotal'],
                                                14 => $d['salesinvoiceTotalAmount'] - $d['salesInvoicePayedAmount'],
                                            );
                                        } else {
                                            $in = array(
                                                0 => '',
                                                1 => str_replace(',', '', $customerName),
                                                2 => $d['salesInvoiceCode'],
                                                3 => str_replace(',', '', $com),
                                                4 => $d['salesInvoiceIssuedDate'],
                                                5 => $d['salesInvoiceOverDueDate'],
                                                6 => $itemRealTotal[$d['salesInvoiceID']],
                                                7 => $d['salesinvoiceTotalAmount'],
                                                8 => implode(" / ",$taxArr),
                                                9 => $totTax,
                                                10 => $d['salesinvoiceTotalAmount'] - $totTax,
                                                11 => $d['salesInvoicePayedAmount'],
                                                12 => (is_null($d['creditNoteTotal'])) ? 0 : $d['creditNoteTotal'],
                                                13 => $d['salesinvoiceTotalAmount'] - $d['salesInvoicePayedAmount'],
                                            );
                                        }
                                        $arr.=implode(",", $in) . "\n";
                                    }
                                    $cTotalAmount += $d['salesinvoiceTotalAmount'];
                                    $cPaidAmount += $d['salesInvoicePayedAmount'];
                                    //$cTaxAmount += $d['salesInvoiceTaxAmount'];

                                    $cCreditNoteAmount += $d['creditNoteTotal'];
                                    $cusSelectedItemTotal += $itemRealTotal[$d['salesInvoiceID']];
                                    $totalSelectedItemTotal += $itemRealTotal[$d['salesInvoiceID']];
                                }
                                if($cusCategory){
                                    $in = array(
                                        0 => '',
                                        1 => '',
                                        2 => '',
                                        3 => '',
                                        4 => '',
                                        5 => '',
                                        6 => 'Total',
                                        7 => number_format($cusSelectedItemTotal, 2, '.', ''),
                                        8 => number_format($cTotalAmount, 2, '.', ''),
                                        9 => '',
                                        10 => number_format($cTaxAmount, 2, '.', ''),
                                        11 => number_format($cWithoutTax, 2, '.', ''),
                                        12 => number_format($cPaidAmount, 2, '.', ''),
                                        13 => number_format($cCreditNoteAmount, 2, '.', ''),
                                        14 => number_format($cOutstandingAmount, 2, '.', '')
                                    );
                                } else {

                                    $in = array(
                                        0 => '',
                                        1 => '',
                                        2 => '',
                                        3 => '',
                                        4 => '',
                                        5 => 'Total',
                                        6 => number_format($cusSelectedItemTotal, 2, '.', ''),
                                        7 => number_format($cTotalAmount, 2, '.', ''),
                                        8 => '',
                                        9 => number_format($cTaxAmount, 2, '.', ''),
                                        10 => number_format($cWithoutTax, 2, '.', ''),
                                        11 => number_format($cPaidAmount, 2, '.', ''),
                                        12 => number_format($cCreditNoteAmount, 2, '.', ''),
                                        13 => number_format($cOutstandingAmount, 2, '.', '')
                                    );
                                }
                                $withoutTax += $cWithoutTax;
                                $cWithoutTax = 0;

                                $arr.=implode(",", $in) . "\n";
                            }

                            $gTotalAmount += $totalAmount;
                            $gPaidAmount += $paidAmount;
                            $gTaxAmount += $taxAmount;
                            $gCreditNoteAmount += $creditNoteAmount;
                            $gOutstandingAmount += $outstandingAmount;
                            $gTotalSelectedItemTotal += $totalSelectedItemTotal;

                            if($cusCategory){
                                $in = array(
                                    0 => '',
                                    1 => '',
                                    2 => '',
                                    3 => '',
                                    4 => '',
                                    5 => '',
                                    6 => 'Sales Person Total',
                                    7 => number_format($totalSelectedItemTotal, 2, '.', ''),
                                    8 => number_format($totalAmount, 2, '.', ''),
                                    9 => '',
                                    10 => number_format($taxAmount, 2, '.', ''),
                                    11 => number_format($withoutTax, 2, '.', ''),
                                    12 => number_format($paidAmount, 2, '.', ''),
                                    13 => number_format($creditNoteAmount, 2, '.', ''),
                                    14 => number_format($outstandingAmount, 2, '.', '')
                                );
                            } else {
                                $in = array(
                                    0 => '',
                                    1 => '',
                                    2 => '',
                                    3 => '',
                                    4 => '',
                                    5 => 'Sales Person Total',
                                    6 => number_format($totalSelectedItemTotal, 2, '.', ''),
                                    7 => number_format($totalAmount, 2, '.', ''),
                                    8 => '',
                                    9 => number_format($taxAmount, 2, '.', ''),
                                    10 => number_format($withoutTax, 2, '.', ''),
                                    11 => number_format($paidAmount, 2, '.', ''),
                                    12 => number_format($creditNoteAmount, 2, '.', ''),
                                    13=> number_format($outstandingAmount, 2, '.', '')
                                );
                            }
                            $gWithoutTax += $withoutTax;
                            $withoutTax = 0;
                            $arr.=implode(",", $in) . "\n";
                        }

                        if ($count == 0) {
                            $in = array(
                                0 => "",
                                1 => "No related data found",
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                    if($cusCategory){
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => 'Grand Total',
                            7 => number_format($gTotalSelectedItemTotal, 2, '.', ''),
                            8 => number_format($gTotalAmount, 2, '.', ''),
                            9 => '',
                            10 => number_format($gTaxAmount, 2, '.', ''),
                            11 => number_format($gWithoutTax, 2, '.', ''),
                            12 => number_format($gPaidAmount, 2, '.', ''),
                            13 => number_format($gCreditNoteAmount, 2, '.', ''),
                            14 => number_format($gOutstandingAmount, 2, '.', '')
                        );
                    } else {
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => 'Grand Total',
                            6 => number_format($gTotalSelectedItemTotal, 2, '.', ''),
                            7 => number_format($gTotalAmount, 2, '.', ''),
                            8 => '',
                            9 => number_format($gTaxAmount, 2, '.', ''),
                            10 => number_format($gWithoutTax, 2, '.', ''),
                            11 => number_format($gPaidAmount, 2, '.', ''),
                            12 => number_format($gCreditNoteAmount, 2, '.', ''),
                            13 => number_format($gOutstandingAmount, 2, '.', '')
                        );
                    }
                    $arr.=implode(",", $in) . "\n\n";

                } else {
                    $arr = "No related data found\n";
                }
            } else {
                if (!empty($invoiceSalesPersonData)) {
                    $title = '';
                    $tit = 'Sales Person Wise Outstanding Balance Report';
                    $in = array(
                        0 => "",
                        1 => $tit,
                        2 => "",
                    );
                    $title = implode(",", $in) . "\n";

                    $title .= $companyDetails[0]->companyName . "\n";
                    $title .= $companyDetails[0]->companyAddress . "\n";
                    $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

                    $in = array(
                        0 => "Period : " . $fromDate . '-' . $toDate
                    );
                    $title.=implode(",", $in) . "\n";

                    if($cusCategory){
                        $arrs = array("SALES PERSON", "CUSTOMER NAME", "CUSTOMER CATEGORY", "INVOICE CODE", "INVOICE COMMENT","ISSUED DATE", "DUE DATE", "SELECTED ITEM TOTAL","TOTAL AMOUNT", "PAID AMOUNT", "CREDIT NOTE AMOUNT","OUTSTANDING BALANCE");
                    } else {
                        $arrs = array("SALES PERSON", "CUSTOMER NAME", "INVOICE CODE", "INVOICE COMMENT","ISSUED DATE", "DUE DATE", "SELECTED ITEM TOTAL","TOTAL AMOUNT", "PAID AMOUNT", "CREDIT NOTE AMOUNT","OUTSTANDING BALANCE");

                    }
                    $header = implode(",", $arrs);
                    $arr = '';

                    $gTotalAmount = $gPaidAmount = $gTaxAmount = $gOutstandingAmount = $gCreditNoteAmount = $gTotalSelectedItemTotal = 0;
                    
                    $itemRealTotal = $invoiceSalesPersonData['onebyoneinvoicevalue'];
                    foreach ($invoiceSalesPersonData as $key => $data) {
                        $salesPerson = (($key === 'other')) ? 'Other' : $data['title'] . ' ' . $data['name'];

                        $in = array(
                            0 => $salesPerson,
                        );
                        $arr.=implode(",", $in) . "\n";

                        $count = 0;
                        if (!empty($data['data'])) {
                            $totalAmount = $paidAmount = $taxAmount = $outstandingAmount = $creditNoteAmount = $totalSelectedItemTotal = 0;
                            foreach ($data['data'] as $customerID => $customerData) {
                                $cTotalAmount = $cPaidAmount = $cTaxAmount = $cOutstandingAmount = $cCreditNoteAmount = $cusSelectedItemTotal = 0;
                                foreach ($customerData as $d) {
                                    $outstandingBalance = $d['salesinvoiceTotalAmount'] - $d['creditNoteTotal'] - $d['salesInvoicePayedAmount'];
                                    if ($outstandingBalance > 0) {
                                        $totalAmount += $d['salesinvoiceTotalAmount'];
                                        $paidAmount += $d['salesInvoicePayedAmount'];
                                        $taxAmount += $d['salesInvoiceTaxAmount'];
                                        $creditNoteAmount += $d['creditNoteTotal'];
                                        $outstandingAmount += $outstandingBalance;
                                        $totalSelectedItemTotal += $itemRealTotal[$d['salesInvoiceID']];
                                        $count++;
                                        $customerName = ($d['customerID'] == 0) ? $d['customerName'] : $d['customerTitle'] . ' ' . $d['customerName'];

                                        //reset invoice comment
                                        $com = preg_replace('/\s+/', ' ',$d['salesInvoiceComment']);

                                        if($cusCategory){
                                            $in = array(
                                                0 => '',
                                                1 => str_replace(',', '', $customerName),
                                                2 => $d['customerCategoryName'],
                                                3 => $d['salesInvoiceCode'],
                                                4 => str_replace(',', '', $com),
                                                5 => $d['salesInvoiceIssuedDate'],
                                                6 => $d['salesInvoiceOverDueDate'],
                                                7 => $itemRealTotal[$d['salesInvoiceID']],
                                                8 => $d['salesinvoiceTotalAmount'],
                                                9 => $d['salesInvoicePayedAmount'],
                                                10 => (is_null($d['creditNoteTotal'])) ? 0 : $d['creditNoteTotal'],
                                                11 => $d['salesinvoiceTotalAmount'] - $d['salesInvoicePayedAmount'],
                                            );
                                        } else {
                                            $in = array(
                                                0 => '',
                                                1 => str_replace(',', '', $customerName),
                                                2 => $d['salesInvoiceCode'],
                                                3 => str_replace(',', '', $com),
                                                4 => $d['salesInvoiceIssuedDate'],
                                                5 => $d['salesInvoiceOverDueDate'],
                                                6 => $itemRealTotal[$d['salesInvoiceID']],
                                                7 => $d['salesinvoiceTotalAmount'],
                                                8 => $d['salesInvoicePayedAmount'],
                                                9 => (is_null($d['creditNoteTotal'])) ? 0 : $d['creditNoteTotal'],
                                                10 => $d['salesinvoiceTotalAmount'] - $d['salesInvoicePayedAmount'],
                                            );
                                        }
                                        $arr.=implode(",", $in) . "\n";
                                    }
                                    $cTotalAmount += $d['salesinvoiceTotalAmount'];
                                    $cPaidAmount += $d['salesInvoicePayedAmount'];
                                    $cTaxAmount += $d['salesInvoiceTaxAmount'];
                                    $cCreditNoteAmount += $d['creditNoteTotal'];
                                    $cOutstandingAmount += $outstandingBalance;
                                    $cusSelectedItemTotal += $itemRealTotal[$d['salesInvoiceID']];
                                }
                                if($cusCategory){
                                    $in = array(
                                        0 => '',
                                        1 => '',
                                        2 => '',
                                        3 => '',
                                        4 => '',
                                        5 => '',
                                        6 => 'Total',
                                        7 => number_format($cusSelectedItemTotal, 2, '.', ''),
                                        8 => number_format($cTotalAmount, 2, '.', ''),
                                        9 => number_format($cPaidAmount, 2, '.', ''),
                                        10 => number_format($cCreditNoteAmount, 2, '.', ''),
                                        11 => number_format($cOutstandingAmount, 2, '.', '')
                                    );
                                } else {

                                    $in = array(
                                        0 => '',
                                        1 => '',
                                        2 => '',
                                        3 => '',
                                        4 => '',
                                        5 => 'Total',
                                        6 => number_format($cusSelectedItemTotal, 2, '.', ''),
                                        7 => number_format($cTotalAmount, 2, '.', ''),
                                        8 => number_format($cPaidAmount, 2, '.', ''),
                                        9 => number_format($cCreditNoteAmount, 2, '.', ''),
                                        10 => number_format($cOutstandingAmount, 2, '.', '')
                                    );
                                }
                                $arr.=implode(",", $in) . "\n";
                            }
                            $gTotalAmount += $totalAmount;
                            $gPaidAmount += $paidAmount;
                            $gTaxAmount += $taxAmount;
                            $gCreditNoteAmount += $creditNoteAmount;
                            $gOutstandingAmount += $outstandingAmount;
                            $gTotalSelectedItemTotal += $totalSelectedItemTotal;

                            if($cusCategory){
                                $in = array(
                                    0 => '',
                                    1 => '',
                                    2 => '',
                                    3 => '',
                                    4 => '',
                                    5 => '',
                                    6 => 'Sales Person Total',
                                    7 => number_format($totalSelectedItemTotal, 2, '.', ''),
                                    8 => number_format($totalAmount, 2, '.', ''),
                                    9 => number_format($paidAmount, 2, '.', ''),
                                    10 => number_format($creditNoteAmount, 2, '.', ''),
                                    11 => number_format($outstandingAmount, 2, '.', '')
                                );
                            } else {
                                $in = array(
                                    0 => '',
                                    1 => '',
                                    2 => '',
                                    3 => '',
                                    4 => '',
                                    5 => 'Sales Person Total',
                                    6 => number_format($totalSelectedItemTotal, 2, '.', ''),
                                    7 => number_format($totalAmount, 2, '.', ''),
                                    8 => number_format($paidAmount, 2, '.', ''),
                                    9 => number_format($creditNoteAmount, 2, '.', ''),
                                    10 => number_format($outstandingAmount, 2, '.', '')
                                );
                            }
                            $arr.=implode(",", $in) . "\n";
                        }

                        if ($count == 0) {
                            $in = array(
                                0 => "",
                                1 => "No related data found",
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                    if($cusCategory){
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => 'Grand Total',
                            7 => number_format($gTotalSelectedItemTotal, 2, '.', ''),
                            8 => number_format($gTotalAmount, 2, '.', ''),
                            9 => number_format($gPaidAmount, 2, '.', ''),
                            10 => number_format($gCreditNoteAmount, 2, '.', ''),
                            11 => number_format($gOutstandingAmount, 2, '.', '')
                        );
                    } else {
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => 'Grand Total',
                            6 => number_format($gTotalSelectedItemTotal, 2, '.', ''),
                            7 => number_format($gTotalAmount, 2, '.', ''),
                            8 => number_format($gPaidAmount, 2, '.', ''),
                            9 => number_format($gCreditNoteAmount, 2, '.', ''),
                            10 => number_format($gOutstandingAmount, 2, '.', '')
                        );
                    }
                    $arr.=implode(",", $in) . "\n\n";

                } else {
                    $arr = "No related data found\n";
                }
            }
            $name = "sales_person_wise_outstanding_balance";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function _getOutstandingBalanceDetails($fromDate, $toDate, $salesPersonIds, $customerIds, $isAllCustomers, $itemIds, $cusCategory = null)
    {
        $salesPersonData = array_fill_keys($salesPersonIds, '');
        if ($isAllCustomers) {
            $customerIds = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomers(array('customer.customerName ASC'));
        }

        $withDefaultSalesPerson = (in_array('other', $salesPersonIds)) ? true : false;

        $getSalesPersonData = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductDetailsBySalesPersonIdsAndCustomerIds($fromDate, $toDate, $itemIds, $customerIds, $salesPersonIds, $withDefaultSalesPerson, $cusCategory);
        $getSalesPerson = $this->CommonTable("User\Model\SalesPersonTable")->getSalesPersonByIds($salesPersonIds);


        foreach ($getSalesPerson as $s) {
            $salesPersonData[$s['salesPersonID']]['name'] = $s['salesPersonSortName'];
            $salesPersonData[$s['salesPersonID']]['title'] = $s['salesPersonTitle'];
        }
     
        $itemWiseTotal = [];
        $itemWiseGrandTotal = [];
        foreach ($getSalesPersonData as $value) {

            $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($value['salesInvoiceID']);

            $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);

            $value['customerName'] = $value['customerName'].'-'.$value['customerCode'];

            $value['salesinvoiceTotalAmount'] = floatval($value['salesinvoiceTotalAmount']) / $numOfSP;
            
            if(empty($itemWiseTotal[$value['salesInvoiceID']][$value['productID']][$value['salesInvoiceProductID']])){
                
                $itemWiseGrandTotal[$value['salesInvoiceID']] += $value['salesInvoiceProductTotal'] / $numOfSP;
            }

            $itemWiseTotal[$value['salesInvoiceID']][$value['productID']][$value['salesInvoiceProductID']] = $value['salesInvoiceProductTotal'] / $numOfSP;
            
            $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceCreditNoteTotalByInvoiceId($value['salesInvoiceID']);
            $value['creditNoteTotal'] = $creditNotes['total'] / $numOfSP;
            if ($value['salesPersonID'] != NULL) {
                $salesPersonData[$value['salesPersonID']]['name'] = $value['salesPersonSortName'];
                $salesPersonData[$value['salesPersonID']]['title'] = $value['salesPersonTitle'];
                $salesPersonData[$value['salesPersonID']]['data'][$value['customerID']][$value['salesInvoiceID']] = $value;
                $salesPersonData[$value['salesPersonID']]['taxData'][$value['customerID']][$value['salesInvoiceID']][$value['taxID']] = array(
                        "taxName" => $value['taxName'],
                        "taxAmount" => $value['salesInvoiceProductTaxAmount'] / $numOfSP
                    );
            } else {
                $salesPersonData['other']['data'][$value['customerID']][$value['salesInvoiceID']] = $value;
                $salesPersonData['other']['taxData'][$value['customerID']][$value['salesInvoiceID']][$value['taxID']] = array(
                        "taxName" => $value['taxName'],
                        "taxAmount" => $value['salesInvoiceProductTaxAmount'] / $numOfSP
                    );

            }
        }

        $salesPersonData['onebyoneinvoicevalue'] = $itemWiseGrandTotal;

        return $salesPersonData;
    }

    /**
     * View Cancelled Invoices
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function viewCancelledInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cancelledUsers = $request->getPost('users');
            $locations = $request->getPost('locations');
            $translator = new Translator();
            $name = $translator->translate('Cancelled Invoice Report');
            $period = $fromDate . ' - ' . $toDate;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $cancelledInvoiceArray = $this->getCancelledInvoiceData($locations, $fromDate, $toDate, $cancelledUsers, $customerIds);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $cancelledInvoiceView = new ViewModel(array(
                'cD' => $companyDetails,
                'cancelledInvoices' => $cancelledInvoiceArray,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $cancelledInvoiceView->setTemplate('reporting/invoice-report/generate-cancelled-invoice-pdf');

            $this->html = $cancelledInvoiceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sharmilan <sahrmilan@thinkcube.com>
     * Get cancelled invoice pdf
     * @return type
     */
    public function getCancelledInvoicePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cancelledUsers = $request->getPost('users');
            $locations = $request->getPost('locations');
            $translator = new Translator();
            $name = $translator->translate('Cancelled Invoice Report');
            $period = $fromDate . ' - ' . $toDate;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $cancelledInvoiceArray = $this->getCancelledInvoiceData($locations, $fromDate, $toDate, $cancelledUsers, $customerIds);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $cancelledInvoiceView = new ViewModel(array(
                'cD' => $companyDetails,
                'cancelledInvoices' => $cancelledInvoiceArray,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $cancelledInvoiceView->setTemplate('reporting/invoice-report/generate-cancelled-invoice-pdf');
            $cancelledInvoiceView->setTerminal(true);
            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($cancelledInvoiceView);
            $pdfPath = $this->downloadPDF('cancelled_invoice_report' . md5($this->getGMTDateTime()), $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getCancelledInvoiceSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cancelledUsers = $request->getPost('users');
            $locations = $request->getPost('locations');
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;
            $cancelledInvoiceArray = $this->getCancelledInvoiceData($locations, $fromDate, $toDate, $cancelledUsers, $customerIds);
            $cD = $this->getCompanyDetails();

            if (count($cancelledInvoiceArray) > 0) {
                $header = '';
                $reportTitle = [ "", 'CANCELLED INVOICE REPORT', "",];
                $header = implode(',', $reportTitle) . "\n";

                $header .= $cD[0]->companyName . "\n";
                $header .= $cD[0]->companyAddress . "\n";
                $header .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $peroidRow = ["Period : " . $fromDate . '-' . $toDate];
                $header .= implode(",", $peroidRow) . "\n";
                $header .= implode(",", [""]) . "\n";

                $columnTitlesArray = ['Invoice No',
                    'Customer Name',
                    'Invoiced Date',
                    'Cancelled Date',
                    'Created User',
                    'Deleted User',
                    'Invoice Amount'];
                $columnTitles = implode(",", $columnTitlesArray);

                $csvRow = '';
                $grandTotal = 0.00;
                foreach ($cancelledInvoiceArray as $cancelledInvoice) {
                    $tableRow = [
                        $cancelledInvoice['salesInvoiceCode'],
                        str_replace(',',"",$cancelledInvoice['salesInvoiceCustomerName']),
                        $cancelledInvoice['createdTimeStamp'],
                        $cancelledInvoice['deletedTimeStamp'],
                        $cancelledInvoice['createdUser'],
                        $cancelledInvoice['deletedUser'],
                        $cancelledInvoice['salesinvoiceTotalAmount'],
                    ];
                    $grandTotal += $cancelledInvoice['salesinvoiceTotalAmount'];
                    $csvRow .= implode(",", $tableRow) . "\n";
                }
                $grandTotalRow = ['', '', '','', '', 'Grand Total:', $grandTotal];
                $csvRow .= implode(',', $grandTotalRow) . "\n";
            } else {
                $csvRow = "No matching records found \n";
            }

            $name = 'Cancelled_Report_Report_csv' . md5($this->getGMTDateTime());
            $csvContent = $this->csvContent($header, $columnTitles, $csvRow);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    protected function getCancelledInvoiceData($locations, $fromDate = FALSE, $toDate = false, $cancelledUsers = false, $customerIds)
    {
        $cancelledInvoices = $this->CommonTable('Invoice/Model/InvoiceTable')->getCancelledInvocieDetails($locations, date('Y-m-d H:i:s', strtotime($fromDate)), date('Y-m-d H:i:s', strtotime($toDate. ' +1 day')), $cancelledUsers, $customerIds);
        $cancelledInvoiceArray = [];
        foreach ($cancelledInvoices as $cI) {
            $cancelledInvoiceArray[$cI['salesInvoiceID']]['salesInvoiceCode'] = $cI['salesInvoiceCode'];
            $cancelledInvoiceArray[$cI['salesInvoiceID']]['salesInvoiceCustomerName'] = $cI['customerName'].'-'.$cI['customerCode'];
            $cancelledInvoiceArray[$cI['salesInvoiceID']]['createdTimeStamp'] = $cI['salesInvoiceIssuedDate'];
            $cancelledInvoiceArray[$cI['salesInvoiceID']]['deletedTimeStamp'] = $this->getUserDateTime($cI['deletedTimeStamp'], 'Y-m-d');
            $cancelledInvoiceArray[$cI['salesInvoiceID']]['createdUser'] = $cI['createdUser'];
            $cancelledInvoiceArray[$cI['salesInvoiceID']]['deletedUser'] = $cI['deletedUser'];
            $cancelledInvoiceArray[$cI['salesInvoiceID']]['salesinvoiceTotalAmount'] = $cI['salesinvoiceTotalAmount'];
        }
        return $cancelledInvoiceArray;
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     * @return view report
     */
    public function viewEditedInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $users = $request->getPost('users');
            $translator = new Translator();
            $name = $translator->translate('Edited Invoice Report');
            $period = $fromDate . ' - ' . $toDate;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $editedInvoiceData = $this->_getEditedInvoiceData($fromDate, $toDate, $users, $customerIds);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $cancelledInvoiceView = new ViewModel(array(
                'cD' => $companyDetails,
                'editedInvoiceData' => $editedInvoiceData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $cancelledInvoiceView->setTemplate('reporting/invoice-report/generate-edited-invoice-pdf');

            $this->html = $cancelledInvoiceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     * @return generate pdf report
     */
    public function generateEditedInvoicePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $users = $request->getPost('users');
            $translator = new Translator();
            $name = $translator->translate('Edited Invoice Report');
            $period = $fromDate . ' - ' . $toDate;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $editedInvoiceData = $this->_getEditedInvoiceData($fromDate, $toDate, $users, $customerIds);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $editedInvoiceView = new ViewModel(array(
                'cD' => $companyDetails,
                'editedInvoiceData' => $editedInvoiceData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $editedInvoiceView->setTemplate('reporting/invoice-report/generate-edited-invoice-pdf');
            $editedInvoiceView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($editedInvoiceView);
            $pdfPath = $this->downloadPDF('edited-invoice-pdf', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return pdf
     */
    public function generateEditedInvoiceSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $users = $request->getPost('users');
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;
            $editedInvoiceData = $this->_getEditedInvoiceData($fromDate, $toDate, $users, $customerIds);
            $cD = $this->getCompanyDetails();

            if (count($editedInvoiceData) > 0) {
                $header = '';
                $reportTitle = [ "", 'EDITED INVOICE REPORT', "",];
                $header = implode(',', $reportTitle) . "\n";

                $header .= $cD[0]->companyName . "\n";
                $header .= $cD[0]->companyAddress . "\n";
                $header .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $peroidRow = ["Period : " . $fromDate . '-' . $toDate];
                $header .= implode(",", $peroidRow) . "\n";
                $header .= implode(",", [""]) . "\n";

                $columnTitlesArray = ['Invoice Code',
                    'Customer Name',
                    'Edited Date',
                    'Edited User',
                    'Changed Category',
                    'Changed Details'];
                $columnTitles = implode(",", $columnTitlesArray);

                $csvRow = '';
                foreach ($editedInvoiceData as $data) {
                    $tableRow = [
                        $data['salesInvoiceCode'],
                        str_replace(',', "", $data['salesInvoiceCustomerName']),
                        $data['dateAndTime'],
                        $data['createdUser'],
                    ];
                    $csvRow .= implode(",", $tableRow) . "\n";
                    foreach ($data['data'] as $value) {
                        $tableRow = [
                            "", "", "", "",
                            $value['category'],
                            $value['changed'],
                        ];
                        $csvRow .= implode(",", $tableRow) . "\n";
                    }
                }
            } else {
                $csvRow = "No matching records found \n";
            }

            $csvContent = $this->csvContent($header, $columnTitles, $csvRow);
            $csvPath = $this->generateCSVFile('Edited_Report_Report_csv', $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function viewIssuedInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locIDs = $request->getPost('locationIdList');
            $isAllCategories = $request->getPost('isAllCategories');
            $categoryIds = $request->getPost('categoryIds');
            $isAllCategories = filter_var($isAllCategories, FILTER_VALIDATE_BOOLEAN);
            $categoryIds = ($isAllCategories) ? null : $categoryIds;
            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $translator = new Translator();
            $name = $translator->translate('Issued Invoice item Report');
            $period = $fromDate . ' - ' . $toDate;

            $issuedItemData = $this->issuedInvoiceItemDetails($locIDs, $fromDate, $toDate, $categoryIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dnView = new ViewModel(array(
                'issuedItemData' => $issuedItemData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));
            $dnView->setTemplate('reporting/invoice-report/generate-issued-invoice-item-pdf');

            $this->html = $dnView;
            $this->msg = $this->getMessage('INFO_STOCKHAND_DELINOTE');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateIssuedInvoiceItemPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locIDs = $request->getPost('locationIdList');
            $isAllCategories = $request->getPost('isAllCategories');
            $categoryIds = $request->getPost('categoryIds');
            $isAllCategories = filter_var($isAllCategories, FILTER_VALIDATE_BOOLEAN);
            $categoryIds = ($isAllCategories) ? null : $categoryIds;
            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $translator = new Translator();
            $name = $translator->translate('Issued Invoice item Report');
            $period = $fromDate . ' - ' . $toDate;
            $issuedItemData = $this->issuedInvoiceItemDetails($locIDs, $fromDate, $toDate, $categoryIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewIssuedInvoiceItem = new ViewModel(array(
                'issuedItemData' => $issuedItemData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));

            $viewIssuedInvoiceItem->setTemplate('reporting/invoice-report/generate-issued-invoice-item-pdf');
            $viewIssuedInvoiceItem->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewIssuedInvoiceItem);
            $pdfPath = $this->downloadPDF('issued-invoice-item-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateIssuedInvoiceItemSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locIDs = $request->getPost('locationIdList');
            $isAllCategories = $request->getPost('isAllCategories');
            $categoryIds = $request->getPost('categoryIds');
            $isAllCategories = filter_var($isAllCategories, FILTER_VALIDATE_BOOLEAN);
            $categoryIds = ($isAllCategories) ? null : $categoryIds;
            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $issuedItemData = $this->issuedInvoiceItemDetails($locIDs, $fromDate, $toDate, $categoryIds);
            $cD = $this->getCompanyDetails();

            if ($issuedItemData) {
                $title = '';
                $tit = 'ISSUED INVOICE ITEM REPORT';
                $in = ["", "", "", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";
                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["LOCATION NAME", "DOCUMENT TYPE", "DOCUMENT CODE", "DOCUMENT DATE", "DOCUMENT STATUS", "PRODUCT NAME-CODE","PRODUCT CATEGORY", "QUANTITY", "COST VALUE","SUB DOCUMENT CODE","SUB DOCUMENT DATE","SUB DOCUMENT STATUS","SUB DOCUMENT PRODUCT NAME-CODE","SUB DOCUMENT PRODUCT QUANTITY"];
                $header = implode(",", $arrs);
                $arr = '';

                foreach ($issuedItemData as $key => $data) {
                    $locName = $key;
                    $in = [$locName];
                    $arr.=implode(",", $in) . "\n";
                    foreach ($data as $dnID => $dnData) {
                        $in = ['', $dnData['documentType'],$dnData['documentCode'],$dnData['documentDate'],$dnData['documentStatus']];
                        $arr.=implode(",", $in) . "\n";
                        foreach ($dnData['productDetails'] as $key => $value) {
                            $in = ['','','','','', $value['productName'],$value['categoryName'],$value['quantity'],$value['cost']];
                            $arr.=implode(",", $in) . "\n";
                        }
                        foreach ($dnData['subDocumentDetails'] as $ke => $val) {
                            foreach ($val as $k => $subVal) {
                                $in = ['','','','','','','','', '',$subVal['subDocCode'],$subVal['subDocDate'],$subVal['subDocStatus']];
                                $arr.=implode(",", $in) . "\n";
                                foreach ($subVal['subDocProductDetails'] as $subPro) {
                                    foreach ($subPro as $subProDta) {
                                        $in = ['','','','','','','','','','','', '',$subProDta['subProductName'],$subProDta['subQuantity']];
                                        $arr.=implode(",", $in) . "\n";
                                    }
                                }
                            }
                        }
                    }
                    
                }
            } else {
                $arr = "no matching records found\n";
            }
        }

        $name = "issued_invoice_item_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }


    public function issuedInvoiceItemDetails($locIDs = null, $fromDate = null, $toDate = null, $categoryIds)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        if (isset($fromDate) && isset($toDate) && isset($locIDs)) {
            $salesAndDeliveryData = [];
            
            //get invoiced item details
            $salesInvoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesInvoiceDetails($fromDate, $toDate,FALSE, $locIDs, $categoryIds);

            $invoicedItemData = [];
            $tempArray = [];
            $deliveryNoteIDs = [];
            foreach ($salesInvoiceDetails as $key => $value) {
                if ($value['documentTypeID'] == "4" && $value['salesInvoiceProductDocumentID'] != null) {
                    $deliNtData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteProductByDeliveryNoteId($value['salesInvoiceProductDocumentID'], $categoryIds);
                    foreach ($deliNtData as $ke => $val) {
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['documentType'] = "Delivery Note";
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['documentCode'] = $val['deliveryNoteCode'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['documentID'] = $val['deliveryNoteID'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['documentDate'] = $val['deliveryNoteDeliveryDate'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['documentStatus'] = $val['statusName'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['productDetails'][$val['deliveryNoteProductID']]['productName'] = $val['pName'].'_'.$val['pCD'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['productDetails'][$val['deliveryNoteProductID']]['categoryName'] = $val['categoryName'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['productDetails'][$val['deliveryNoteProductID']]['quantity'] = $val['deliveryNoteProductQuantity'];
                        $itemInCost = $this->calculateItemInCost($val['deliveryNoteID'],$val['locationProductID'],'Delivery Note');
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['productDetails'][$val['deliveryNoteProductID']]['cost'] = $itemInCost * floatval($val['deliveryNoteProductQuantity']);
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocCode'] = $value['salesInvoiceCode'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocDate'] = $value['salesInvoiceIssuedDate'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocStatus'] = $value['statusName'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocProductDetails'][$value['salesInvoiceProductDocumentID']][$value['salesInvoiceProductID']]['subProductName'] = $value['pName'].'_'.$value['pCD'];
                        $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocProductDetails'][$value['salesInvoiceProductDocumentID']][$value['salesInvoiceProductID']]['subQuantity'] = $value['salesInvoiceProductQuantity'];


                    }
                    $invoicedItemData[$val['deliveryNoteID'].'_DLN_'.$val['deliveryNoteLocationID']] = $tempArray[$val['deliveryNoteLocationID'].'_'.$val['deliveryNoteID']];
                } else {
                    $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']]['documentType'] = "Sales Invoice";
                    $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']]['documentCode'] = $value['salesInvoiceCode'];
                    $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']]['documentDate'] = $value['salesInvoiceIssuedDate'];
                    $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']]['documentID'] = $value['salesInvoiceID'];
                    $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']]['documentStatus'] = $value['statusName'];
                    $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']]['productDetails'][$value['salesInvoiceProductID']]['productName'] = $value['pName'].'_'.$value['pCD'];
                    $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']]['productDetails'][$value['salesInvoiceProductID']]['categoryName'] = $value['categoryName'];
                    $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']]['productDetails'][$value['salesInvoiceProductID']]['quantity'] = $value['salesInvoiceProductQuantity'];
                    $itemInCost = $this->calculateItemInCost($value['salesInvoiceID'],$value['locationProductID'],'Sales Invoice');
                    $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']]['productDetails'][$value['salesInvoiceProductID']]['cost'] = $itemInCost * floatval($value['salesInvoiceProductQuantity']);

                    $invoicedItemData[$value['salesInvoiceID'].'_INV_'.$value['locationID']] = $tempArray[$value['locationID'].'_'.$value['salesInvoiceID']];
                }
            }
            // getting DeliveryNote details
            $deliveryNotData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getIssuedDeliveryNotesItemDetails($fromDate, $toDate, $locIDs, $categoryIds);
            foreach ($deliveryNotData as $key => $value) {
                $itemInCost = $this->calculateItemInCost($value['deliveryNoteID'],$value['dlnLocationProductID'],'Delivery Note');
                if ($value['documentTypeID'] == null || $value['documentTypeID'] == "4") {
                    if (is_null($value['salesInvoiceProductID'])) {
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentType'] = "Delivery Note";
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentCode'] = $value['deliveryNoteCode'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentDate'] = $value['deliveryNoteDeliveryDate'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentID'] = $value['deliveryNoteID'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentStatus'] = $value['dnStatus'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['productDetails'][$value['deliveryNoteProductID']]['productName'] = $value['pName'].'_'.$value['pCD'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['productDetails'][$value['deliveryNoteProductID']]['categoryName'] = $value['categoryName'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['productDetails'][$value['deliveryNoteProductID']]['quantity'] = $value['qty'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['productDetails'][$value['deliveryNoteProductID']]['cost'] = $itemInCost * floatval($value['qty']);

                        $invoicedItemData[$value['deliveryNoteID'].'_DLN_'.$value['locID']] = $tempArray[$value['locID'].'_'.$value['deliveryNoteID']];
                    } else {
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentType'] = "Delivery Note";
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentCode'] = $value['deliveryNoteCode'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentID'] = $value['deliveryNoteID'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentDate'] = $value['deliveryNoteDeliveryDate'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['documentStatus'] = $value['dnStatus'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['productDetails'][$value['deliveryNoteProductID']]['productName'] = $value['pName'].'_'.$value['pCD'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['productDetails'][$value['deliveryNoteProductID']]['categoryName'] = $value['categoryName'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['productDetails'][$value['deliveryNoteProductID']]['quantity'] = $value['qty'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['productDetails'][$value['deliveryNoteProductID']]['cost'] = $itemInCost * floatval($value['qty']);
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocCode'] = $value['salesInvoiceCode'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocDate'] = $value['salesInvoiceIssuedDate'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocStatus'] = $value['siStatus'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocProductDetails'][$value['salesInvoiceProductDocumentID']][$value['salesInvoiceProductID']]['subProductName'] = $value['psName'].'_'.$value['psCD'];
                        $tempArray[$value['locID'].'_'.$value['deliveryNoteID']]['subDocumentDetails'][$value['locationID']][$value['salesInvoiceID']]['subDocProductDetails'][$value['salesInvoiceProductDocumentID']][$value['salesInvoiceProductID']]['subQuantity'] = $value['salesInvoiceProductQuantity'];

                        $invoicedItemData[$value['deliveryNoteID'].'_DLN_'.$value['locID']] = $tempArray[$value['locID'].'_'.$value['deliveryNoteID']];
                    }
                }
            }

            $finalDataArray = [];
            foreach ($invoicedItemData as $key => $value) {
                $locationID = explode('_', $key)[2];
                foreach ($locIDs as $ke => $val) {
                    if (floatval($val) == floatval($locationID)) {
                        $locationName = $this->CommonTable('Settings\Model\LocationTable')->getLocationByID($locationID);
                        $finalDataArray[$locationName->locationName][] = $value;
                    }
                }
            }
            return $finalDataArray;
        }
    }

    /**
     * calulate item in cost
     * @param documentID,locationProductID,documentType
     * @return int  
     */
    public function calculateItemInCost($documentID, $locationProductID, $documentType)
    {
        $itemInData = $this->CommonTable('Inventory\Model\ItemOutTable')->getInvoiceDeliveryNoteDetails($documentID,$documentType,$locationProductID);
        $itemInCost = 0;
        foreach ($itemInData as $key => $value) {
            $itemInCost = floatval($value['itemInPrice']) - floatval($value['itemInDiscount']);
        }
        return $itemInCost;
    }

    private function _getTotalItemInCost($locationProductData, $itemInData)
    {
                 
        $itemInCost = 0;
        $totalDelitNtInQty = 0;
        foreach ($itemInData as $v) {

            $inQty = isset($v['itemOutQty']) ? $v['itemOutQty'] : 0;
            $totalDelitNtInQty += $inQty;
            if ($v['itemOutBatchID'] != NULL && $v['itemOutSerialID'] == NULL) {
                $totalDelitNtInQty = 1;
            } else if ($v['itemOutBatchID'] == NULL && $v['itemOutSerialID'] == NULL) {
                $totalDelitNtInQty = 1;
            }
            $inPrice = isset($v['itemInPrice']) ? $v['itemInPrice'] : 0;
            //GRN and Purchase Invoice has only percentage value(%) for discount
            //ItemIn table has discount value(not percentage value)
            $inDiscountValue = isset($v['itemInDiscount']) ? $v['itemInDiscount'] : 0;
            $totalTaxAmount = 0;

            if ($v['itemInDocumentType'] == "Goods Received Note") {
                $itemInTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($v['itemInDocumentID'], $locationProductData['locationProductID']);

                if (!empty($itemInTax)) {
                    foreach ($itemInTax as $t) {
                        $grnProductTotalQty = ($t['grnProductTotalQty'] == NULL) ? 1 : $t['grnProductTotalQty'];
                        $totalTaxAmount += $t['grnTaxAmount'] / $grnProductTotalQty;
                    }
                }
            } else if ($v['itemInDocumentType'] == "Payment Voucher") {
                $itemInTax = $this->CommonTable("Inventory\Model\PurchaseInvoiceProductTaxTable")->getPurchaseInvoiceProductTax($v['itemInDocumentID'], $locationProductData['locationProductID']);

                // $totalTaxAmount = 0;
                if (!empty($itemInTax)) {
                    foreach ($itemInTax as $t) {
                        $purchaseInvoiceProductTotalQty = ($t['purchaseInvoiceProductTotalQty'] == NULL) ? 1 : $t['purchaseInvoiceProductTotalQty'];
                        $totalTaxAmount += $t['purchaseInvoiceTaxAmount'] / $purchaseInvoiceProductTotalQty;
                    }
                }
            }
            $itemInCost += ($inPrice - ($inDiscountValue)) * $v['itemOutQty'];
        }

        return $itemInCost;
        
    }
    /**
     * @author sandun <sandun@thinkcube.com>
     * @param date $fromDate
     * @param date $toDate
     * @param array $users
     * @return array $editedInvoiceData
     */
    private function _getEditedInvoiceData($fromDate = NULL, $toDate = NULL, $users = [], $customerIds)
    {
        if (isset($fromDate) && isset($toDate)) {
            $plusToDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
            $editedInvoices = $this->CommonTable('Invoice/Model/InvoiceEditLogDetailsTable')->getEditedInvocieDetails($fromDate, $plusToDate, $users, $customerIds);

            $editedInvoiceData = [];
            foreach ($editedInvoices as $eI) {
                if (isset($eI['invoiceEditLogID'])) {
                    $editedInvoiceData[$eI['invoiceEditLogID']]['salesInvoiceCode'] = $eI['salesInvoiceCode'];
                    $editedInvoiceData[$eI['invoiceEditLogID']]['salesInvoiceCustomerName'] = $eI['customerName'].'-'.$eI['customerCode'];
                    $editedInvoiceData[$eI['invoiceEditLogID']]['dateAndTime'] = $this->getUserDateTime($eI['dateAndTime'], 'Y-m-d H:i:s');
                    $editedInvoiceData[$eI['invoiceEditLogID']]['createdUser'] = $eI['createdUser'];
                    $editedInvoiceData[$eI['invoiceEditLogID']]['data'][$eI['invoiceEditLogDetailsID']] = [
                        'changed' => $eI['invoiceEditLogDetailsNewState'],
                        'category' => $eI['invoiceEditLogDetailsCategory']
                    ];
                }
            }

            return $editedInvoiceData;
        }
    }

    public function invoicePaymentDetailsReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $invoiceIds = $request->getPost('invoiceIds');
            $customerIds = $request->getPost('customerIds');
            $locationIds = $request->getPost('locationIds');
            $isAllInvoices = $request->getPost('isAllInvoices');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $invoiceIds = (filter_var($isAllInvoices, FILTER_VALIDATE_BOOLEAN)) ? [] : $invoiceIds;
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;
            $paymentDetails = $this->getInvoicePaymentReportDetails($invoiceIds, $locationIds, $fromDate, $toDate, $customerIds);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Invoice & Payment Details Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentData' => $paymentDetails['dataList'],
                'summaryDetails' => $paymentDetails['summeryData'],
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/invoice-report/generate-invoice-payments-details-report');

            $this->html = $view;
            $this->status = true;
        }
        return $this->JSONRespondHtml();
    }

    public function invoicePaymentDetailsReportPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $invoiceIds = $request->getPost('invoiceIds');
            $customerIds = $request->getPost('customerIds');
            $locationIds = $request->getPost('locationIds');
            $isAllInvoices = $request->getPost('isAllInvoices');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $invoiceIds = (filter_var($isAllInvoices, FILTER_VALIDATE_BOOLEAN)) ? [] : $invoiceIds;
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;
            $paymentDetails = $this->getInvoicePaymentReportDetails($invoiceIds, $locationIds, $fromDate, $toDate, $customerIds);

            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Invoice & Payment Details Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentData' => $paymentDetails['dataList'],
                'summaryDetails' => $paymentDetails['summeryData'],
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/invoice-report/generate-invoice-payments-details-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('payment-details-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
        }
        return $this->JSONRespond();
    }

    public function invoicePaymentDetailsReportCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $invoiceIds = $request->getPost('invoiceIds');
            $locationIds = $request->getPost('locationIds');
            $isAllInvoices = $request->getPost('isAllInvoices');
            $invoiceIds = (filter_var($isAllInvoices, FILTER_VALIDATE_BOOLEAN)) ? [] : $invoiceIds;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $companyDetails = $this->getCompanyDetails();
            $paymentDetails = $this->getInvoicePaymentReportDetails($invoiceIds, $locationIds, $fromDate, $toDate, $customerIds);
            if ($paymentDetails['dataList']) {
                $data = array(
                    "Invoice & Payment Details Report"
                );
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = array(
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    "Period :" . $fromDate . " - " . $toDate
                );
                $title.=implode(",", $data) . "\n";

                $tableHead = array('Invoice Number', 'Customer Name', 'Invoice Date', 'Invoice Due Date', 'Invoice Amount', 'Paid Amount', 'Remaining Amount', 'Payment Number', 'Payment Date', 'Payment Amount', 'Payment Method', 'Reference');
                $header.=implode(",", $tableHead) . "\n";

                foreach ($paymentDetails['dataList'] as $paymentData) {
                    $data = array($paymentData['invoiceNumber'], str_replace(',', "", $paymentData['customerName']), $paymentData['invoiceDate'], $paymentData['invoiceDueDate'], $paymentData['invoiceAmount'], $paymentData['payedAmount'], $paymentData['remainingAmount']);
                    $arr.=implode(",", $data) . "\n";
                    foreach ($paymentData['payments'] as $payment) {
                        $data = array('', '', '', '', '', '', '', $payment['paymentNumber'], $payment['paymentDate'], $payment['paymentAmount'], $payment['paymentMethod'], $payment['reference']);
                        $arr.=implode(",", $data) . "\n";
                    }
                }
                $data = array('', '', '', '', $paymentDetails['summeryData']['totalInvoiceAmount'], $paymentDetails['summeryData']['totalPaidAmount'], $paymentDetails['summeryData']['totalRemainingAmount']);
                    $arr.=implode(",", $data) . "\n";
            } else {
                $arr = "no matching records found";
            }
            $name = "Invoice & Payment details report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);
            $this->data = $csvPath;
            $this->status = true;
        }
        return $this->JSONRespond();
    }

    /**
     * Get payment method details
     * @param array $paymentData
     * @param array $paymentMethodData
     * @return string
     */
    private function addPaymentMethodAndReferance($paymentData, $paymentMethodData)
    {
        if (!empty(intval($paymentData['incomingPaymentMethodCashId']))) {
            $paymentMethodData['paymentMethod'] = 'Cash';
            $paymentMethodData['reference'] = '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodChequeId']))) {
            $paymentMethodData['paymentMethod'] = 'Cheque';
            $paymentMethodData['reference'] = (!empty($paymentData['incomingPaymentMethodChequeNumber'])) ? $paymentData['incomingPaymentMethodChequeNumber'] : '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodCreditCardId']))) {
            $paymentMethodData['paymentMethod'] = 'Credit Card';
            $paymentMethodData['reference'] = $paymentData['incomingPaymentMethodCreditReceiptNumber'];
        } else if (!empty(intval($paymentData['incomingPaymentMethodBankTransferId']))) {
            $paymentMethodData['paymentMethod'] = 'Bank Transfer';
            $paymentMethodData['reference'] = (!empty(intval($paymentData['incomingPaymentMethodBankTransferCustomerAccountNumber']))) ? $paymentData['incomingPaymentMethodBankTransferCustomerAccountNumber'] : '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodGiftCardId']))) {
            $paymentMethodData['paymentMethod'] = 'Gift Card';
            $paymentMethodData['reference'] = $paymentData['giftCardId'];
        } else if (!empty(intval($paymentData['incomingPaymentMethodLCId']))) {
            $paymentMethodData['paymentMethod'] = 'LC';
            $paymentMethodData['reference'] = '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodTTId']))) {
            $paymentMethodData['paymentMethod'] = 'TT';
            $paymentMethodData['reference'] = '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodLoyaltyCardId']))) {
            $paymentMethodData['paymentMethod'] = 'Loyalty Card';
            $paymentMethodData['reference'] = '-';
        }
        return $paymentMethodData;
    }

    /**
     * Get sales invoice details
     * @param array $invoiceIds
     * @param array $locationIds
     * @param data $fromDate
     * @param date $toDate
     * @return array
     */
    private function getInvoicePaymentReportDetails($invoiceIds, $locationIds, $fromDate, $toDate, $customerIds)
    {
        $salesData = [];
        $summaryDetails = [
            'totalInvoiceAmount' => 0,
            'totalPaidAmount' => 0,
            'totalRemainingAmount' => 0
        ];
        $currencySymbol = '';
        $paymentDetails = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoicePaymentDetails($invoiceIds, [], $locationIds, $fromDate, $toDate, $customerIds);
        if ($paymentDetails) {
            $paymentDataArr = [];
            foreach ($paymentDetails as $payment) {
                if (!array_key_exists($payment['salesInvoiceCode'], $salesData)) {
                    $paymentDataArr = [];
                    $paidAmount = $payment['salesInvoicePayedAmount'];
                    $invoiceAmount = $payment['salesinvoiceTotalAmount'];
                    $creditNoteDetails = $this->CommonTable("Invoice\Model\CreditNoteTable")->getInvoiceCreditNoteTotalByInvoiceId($payment['salesInvoiceID']);
                    $creditNoteAmount = ($creditNoteDetails['total']) ? $creditNoteDetails['total'] : 0;

                    if ($payment['salesInvoiceCustomCurrencyRate'] > 0) {
                        $paidAmount = $payment['salesInvoicePayedAmount'] / $payment['salesInvoiceCustomCurrencyRate'];
                        $invoiceAmount = $payment['salesinvoiceTotalAmount'] / $payment['salesInvoiceCustomCurrencyRate'];
                        $creditNoteAmount = $creditNoteAmount / $payment['salesInvoiceCustomCurrencyRate'];
                    }

                    $salesData[$payment['salesInvoiceCode']] = array(
                        'invoiceNumber' => $payment['salesInvoiceCode'],
                        'invoiceDate' => $payment['salesInvoiceIssuedDate'],
                        'invoiceDueDate' => $payment['salesInvoiceOverDueDate'],
                        'invoiceAmount' => $payment['currencySymbol'] . ' ' . number_format(($invoiceAmount - $creditNoteAmount), 2, '.', ''),
                        'payedAmount' => $payment['currencySymbol'] . ' ' . number_format($paidAmount, 2, '.', ''),
                        'remainingAmount' => $payment['currencySymbol'] . ' ' . number_format((($invoiceAmount - $paidAmount - $creditNoteAmount)), 2, '.', ''),
                        'customerName' => $payment['customerName'].'-'.$payment['customerCode'],
                        'payments' => []
                    );

                    $summaryDetails['totalInvoiceAmount'] += ($invoiceAmount - $creditNoteAmount) ;
                    $summaryDetails['totalPaidAmount'] += ($paidAmount) ;
                    $summaryDetails['totalRemainingAmount'] += ($invoiceAmount - $creditNoteAmount - $paidAmount) ;
                    $currencySymbol = $payment['currencySymbol'];

                }
                $paymentMethodAmount = ($payment['salesInvoiceCustomCurrencyRate'] > 0) ? ($payment['incomingPaymentMethodAmount'] / $payment['salesInvoiceCustomCurrencyRate']) : $payment['incomingPaymentMethodAmount'];
                //to keep payment method details
                $paymentData = array(
                    'paymentNumber' => $payment['incomingPaymentCode'],
                    'paymentDate' => $payment['incomingPaymentDate'],
                    'paymentAmount' => $payment['currencySymbol'] . ' ' . number_format($paymentMethodAmount, 2, '.', '')
                );

                //add payment mothod and reference to payment details
                if ($paidAmount > 0) {
                    $paymentDataArr[] = $this->addPaymentMethodAndReferance($payment, $paymentData);
                    $salesData[$payment['salesInvoiceCode']]['payments'] = $paymentDataArr;
                } else {
                    $salesData[$payment['salesInvoiceCode']]['payments'] = array(array(
                            'paymentNumber' => '-',
                            'paymentDate' => ' ',
                            'paymentAmount' => ' ',
                            'paymentMethod' => ' ',
                            'reference' => ' '
                    ));
                }
            }
        }

        $summaryDetails['totalInvoiceAmount'] = $currencySymbol ." ". number_format($summaryDetails['totalInvoiceAmount'], 2, '.', '');
        $summaryDetails['totalPaidAmount'] = $currencySymbol ." ". number_format($summaryDetails['totalPaidAmount'], 2, '.', '');
        $summaryDetails['totalRemainingAmount'] = $currencySymbol ." ". number_format($summaryDetails['totalRemainingAmount'], 2, '.', '');


        return ['dataList' => $salesData, 'summeryData' => $summaryDetails];
    }

    private function getInvoiceDetailsReportData($invoiceIds, $locationIds, $fromDate, $toDate, $customerIds)
    {
        $dataList = [];
        //get given invoices
        $salesInvoices = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoicesByDateRange($fromDate, $toDate, $locationIds, $invoiceIds, $customerIds);
        $summaryDetails = [
            'totalInvoiceAmount' => 0,
            'totalPaidAmount' => 0,
            'totalRemainingAmount' => 0
        ];
        foreach ($salesInvoices as $salesInvoice) {
            $invoiceIds = [];
            $invoiceAmount = $salesInvoice['salesinvoiceTotalAmount'] / $salesInvoice['salesInvoiceCustomCurrencyRate'];
            $paidAmount = $salesInvoice['salesInvoicePayedAmount'] / $salesInvoice['salesInvoiceCustomCurrencyRate'];
            $creditNoteAmount = $salesInvoice['totalCreditNote'];
            $currencySymbol = ($salesInvoice['currencySymbol']) ? $salesInvoice['currencySymbol'] : $this->companyCurrencySymbol;
            if (!array_key_exists($salesInvoice['salesInvoiceID'], $dataList)) {
                $dataList[$salesInvoice['salesInvoiceID']] = [
                    'invoiceNumber'  => $salesInvoice['salesInvoiceCode'],
                    'customerName'   => $salesInvoice['customerName'].'-'.$salesInvoice['customerCode'],
                    'invoiceDate'    => $salesInvoice['salesInvoiceIssuedDate'],
                    'invoiceDueDate' => $salesInvoice['salesInvoiceOverDueDate'],
                    'invoiceStatus'  => $salesInvoice['statusName'],
                    'invoiceAmount'  => $currencySymbol ." ". number_format(($invoiceAmount - $creditNoteAmount), 2, '.', ''),
                    'paidAmount'     => $currencySymbol ." ". number_format($paidAmount, 2, '.', ''),
                    'remainingAmount'=> $currencySymbol ." ". number_format(($invoiceAmount - $creditNoteAmount - $paidAmount), 2, '.', ''),
                    'payments'       => []
                ];
            }
            $invoiceIds[] = $salesInvoice['salesInvoiceID'];

            if ($salesInvoice['statusName'] != 'Cancel') {
                $summaryDetails['totalInvoiceAmount'] += ($invoiceAmount - $creditNoteAmount) ;
                $summaryDetails['totalPaidAmount'] += ($paidAmount) ;
                $summaryDetails['totalRemainingAmount'] += ($invoiceAmount - $creditNoteAmount - $paidAmount) ;
            }

            //get payment details
            $invoicePaymentDetails = $this->CommonTable("Invoice\Model\InvoicePaymentsTable")->getIncomingInvoicePaymentDetails($invoiceIds);
            $payments = [];

            foreach ($invoicePaymentDetails as $invoicePayment) {
                $incomingPaymentMethodAmount = $invoicePayment['incomingPaymentMethodAmount'] / $invoicePayment['incomingPaymentCustomCurrencyRate'];
                $paymentMethod = [
                    'paymentNumber' => $invoicePayment['incomingPaymentCode'],
                    'paymentDate'   => $invoicePayment['incomingPaymentDate'],
                    'paymentAmount' => $invoicePayment['currencySymbol'] . ' ' . number_format($incomingPaymentMethodAmount, 2, '.', '')
                ];
                $payments[] = $this->addPaymentMethodAndReferance($invoicePayment, $paymentMethod);
                $dataList[$salesInvoice['salesInvoiceID']]['payments'] = $payments;
            }
        }

        $summaryDetails['totalInvoiceAmount'] = $currencySymbol ." ". number_format($summaryDetails['totalInvoiceAmount'], 2, '.', '');
        $summaryDetails['totalPaidAmount'] = $currencySymbol ." ". number_format($summaryDetails['totalPaidAmount'], 2, '.', '');
        $summaryDetails['totalRemainingAmount'] = $currencySymbol ." ". number_format($summaryDetails['totalRemainingAmount'], 2, '.', '');


        return ['dataList' => $dataList, 'summeryData' => $summaryDetails];
    }

    public function invoiceDetailsReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $invoiceIds = $request->getPost('invoiceIds');
            $locationIds = $request->getPost('locationIds');
            $isAllInvoices = $request->getPost('isAllInvoices');
            $invoiceIds = (filter_var($isAllInvoices, FILTER_VALIDATE_BOOLEAN)) ? [] : $invoiceIds;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $invoiceDetails = $this->getInvoiceDetailsReportData($invoiceIds, $locationIds, $fromDate, $toDate, $customerIds);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('All Invoice Details & Payments Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentData' => $invoiceDetails['dataList'],
                'summaryDetails' => $invoiceDetails['summeryData'],
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/invoice-report/generate-invoice-details-report');
            $this->html = $view;
            $this->status = true;
        }
        return $this->JSONRespondHtml();
    }

    public function invoiceDetailsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $invoiceIds = $request->getPost('invoiceIds');
            $locationIds = $request->getPost('locationIds');
            $isAllInvoices = $request->getPost('isAllInvoices');
            $invoiceIds = (filter_var($isAllInvoices, FILTER_VALIDATE_BOOLEAN)) ? [] : $invoiceIds;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $invoiceDetails = $this->getInvoiceDetailsReportData($invoiceIds, $locationIds, $fromDate, $toDate, $customerIds);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('All Invoice Details & Payments Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentData' => $invoiceDetails['dataList'],
                'summaryDetails' => $invoiceDetails['summeryData'],
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/invoice-report/generate-invoice-details-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('invoice-details-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
        }
        return $this->JSONRespondHtml();
    }

    public function invoicePaymentDetailsCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $invoiceIds = $request->getPost('invoiceIds');
            $locationIds = $request->getPost('locationIds');
            $isAllInvoices = $request->getPost('isAllInvoices');
            $invoiceIds = (filter_var($isAllInvoices, FILTER_VALIDATE_BOOLEAN)) ? [] : $invoiceIds;
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $invoiceDetails = $this->getInvoiceDetailsReportData($invoiceIds, $locationIds, $fromDate, $toDate, $customerIds);
            $companyDetails = $this->getCompanyDetails();
            if ($invoiceDetails['dataList']) {
                $data = array(
                    "All Invoice Details & Payments Report"
                );
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = array(
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    "Period :" . $fromDate . " - " . $toDate
                );
                $title.=implode(",", $data) . "\n";

                $tableHead = array('Invoice Number', 'Customer Name', 'Invoice Date', 'Invoice Due Date', 'Invoice Status', 'Invoice Amount', 'Paid Amount', 'Remaining Amount', 'Payment Number', 'Payment Date', 'Payment Amount', 'Payment Method', 'Reference');
                $header.=implode(",", $tableHead) . "\n";

                foreach ($invoiceDetails['dataList'] as $paymentData) {
                    $data = array($paymentData['invoiceNumber'], str_replace(',', "", $paymentData['customerName']), $paymentData['invoiceDate'], $paymentData['invoiceDueDate'], $paymentData['invoiceStatus'], $paymentData['invoiceAmount'], $paymentData['paidAmount'], $paymentData['remainingAmount']);
                    $arr.=implode(",", $data) . "\n";
                    foreach ($paymentData['payments'] as $payment) {
                        $data = array('', '', '', '', '', '', '', '', $payment['paymentNumber'], $payment['paymentDate'], $payment['paymentAmount'], $payment['paymentMethod'], $payment['reference']);
                        $arr.=implode(",", $data) . "\n";
                    }
                }
                $data = array('', '', '', '', '', $invoiceDetails['summeryData']['totalInvoiceAmount'], $invoiceDetails['summeryData']['totalPaidAmount'], $invoiceDetails['summeryData']['totalRemainingAmount']);
                    $arr.=implode(",", $data) . "\n";

            } else {
                $arr = "no matching records found";
            }
            $name = "Invoice Details Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);
            $this->data = $csvPath;
            $this->status = true;
        }
        return $this->JSONRespond();
    }

    /**
     * This function is used to generate invoice due date report
     * @param JSON Request
     * @return JSON Respond
     */
    public function invoiceDueDateReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationIds = $request->getPost('locationIds');
            $statusIds = $request->getPost('status');
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $invoiceDetails = $this->getInvoiceDueDateReportData($locationIds, $fromDate, $toDate, $customerIds, $statusIds);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('Invoice Due Date Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentData' => $invoiceDetails,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/invoice-report/generate-invoice-due-date-report');
            $this->html = $view;
            $this->status = true;
        }
        return $this->JSONRespondHtml();
    }

    /**
     * This function is used to generate invoice due date report pdf
     * @param JSON Request
     * @return JSON Respond
     */
    public function invoiceDueDateReportPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationIds = $request->getPost('locationIds');
            $statusIds = $request->getPost('status');
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $invoiceDetails = $this->getInvoiceDueDateReportData($locationIds, $fromDate, $toDate, $customerIds, $statusIds);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Invoice Due Date Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentData' => $invoiceDetails,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/invoice-report//generate-invoice-due-date-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('invoice-due-date-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
        }
        return $this->JSONRespondHtml();
    }

    /**
     * This function is used to generate invoice due date report csv
     * @param JSON Request
     * @return JSON Respond
     */
    public function invoiceDueDateReportCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationIds = $request->getPost('locationIds');
            $statusIds = $request->getPost('status');
            $customerIds = $request->getPost('customerIds');
            $isAllCustomers = $request->getPost('isAllCustomers');
            $customerIds = (filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN)) ? [] : $customerIds;

            $invoiceDetails = $this->getInvoiceDueDateReportData($locationIds, $fromDate, $toDate, $customerIds, $statusIds);
            $companyDetails = $this->getCompanyDetails();
            if ($invoiceDetails) {
                $data = array(
                    "Invocie Due Date Report"
                );
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = array(
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    "Period :" . $fromDate . " - " . $toDate
                );
                $title.=implode(",", $data) . "\n";

                $tableHead = array('Invoice Number', 'Customer Name', 'Location Name','Invoice Date', 'Invoice Due Date', 'Invoice Status', 'Invoice Amount', 'Paid Amount');
                $header.=implode(",", $tableHead) . "\n";

                foreach ($invoiceDetails as $invData) {
                    $data = array($invData['invoiceNumber'], str_replace(',', "", $invData['customerName'].'_'.$invData['customerCode']), $invData['locationName'],$invData['invoiceDate'], $invData['invoiceDueDate'], $invData['invoiceStatus'], $invData['invoiceAmount'], $invData['invoicePaidAmount']);
                    $arr.=implode(",", $data) . "\n";
                }
            } else {
                $arr = "no matching records found";
            }
            $name = "Invoice Due Date Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);
            $this->data = $csvPath;
            $this->status = true;
        }
        return $this->JSONRespond();
    }

    /**
     * This function is used to get invoice data by due date range
     * @param $locationIds, $fromdate, $todate, $customerIds
     * @return data set 
     */
    public function getInvoiceDueDateReportData($locationIds, $fromDate, $toDate, $customerIds, $statusIds)
    {
        $statusIds = ($statusIds == "") ? NULL :$statusIds;
        $dataList = [];
        //get given invoices
        $salesInvoices = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoicesByDueDateRange($fromDate, $toDate, $locationIds,$customerIds, $statusIds);
        foreach ($salesInvoices as $salesInvoice) {
            if (!array_key_exists($salesInvoice['salesInvoiceID'], $dataList)) {
                $dataList[$salesInvoice['salesInvoiceID']] = [
                    'invoiceNumber'  => $salesInvoice['salesInvoiceCode'],
                    'customerName'   => $salesInvoice['customerName'].'_'.$salesInvoice['customerCode'],
                    'invoiceDate'    => $salesInvoice['salesInvoiceIssuedDate'],
                    'invoiceDueDate' => $salesInvoice['salesInvoiceOverDueDate'],
                    'invoiceStatus'  => $salesInvoice['statusName'],
                    'invoiceAmount'  => $salesInvoice['salesinvoiceTotalAmount'],
                    'invoicePaidAmount'  => $salesInvoice['salesInvoicePayedAmount'],
                    'locationName'  => $salesInvoice['locationName']
                ];
            }
        }
        return $dataList;
    }

    /**
     * This function is used to generate invoice details with customer report
     * @param JSON Request
     * @return JSON Respond
     */
    public function invoiceDetailsWithCustomerReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $reportView = $this->getService('InvoiceReportService')->getInvoiceDetailsWithCustomerReport($request->getPost());

            $this->html = $reportView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * This function is used to generate invoice details with customer report pdf
     * @param JSON Request
     * @return JSON Respond
     */
    public function invoiceDetailsWithCustomerReportPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $view = $this->getService('InvoiceReportService')->getInvoiceDetailsWithCustomerReportPdf($request->getPost());

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('invoice-details-with-customer-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * This function is used to generate invoice details with customer report csv
     * @param JSON Request
     * @return JSON Respond
     */
    public function invoiceDetailsWithCustomerReportCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $csvContent = $this->getService('InvoiceReportService')->getInvoiceDetailsWithCustomerReportSheet($request->getPost());

            $name = "Invoice Details With Customer Details";
            $csvPath = $this->generateCSVFile($name, $csvContent);
            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }
}
