<?php

namespace Reporting\Controller\Generate;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;

/**
 * @author Yashora <yashora@thinkcube.com>
 * This file contains action methods for the payment voucher report controller
 */
class PaymentVoucherReportController extends CoreController
{

    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'inve_report_upper_menu';

    /**
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * @return location Id
     * get location Ids , selected by an user
     */
    public function getlocationID()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationIds = $request->getPost('locationIds');
        }
        return $locationIds;
    }

    /**
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * get data from the database and put them into an array according to the locations
     * @return array
     */
    public function getPaymentVoucherDetails()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationIds = $this->getlocationID();

            $locationWiseData = array_fill_keys($locationIds, '');

            $result = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPaymentVoucherWithTaxDetails($fromDate, $toDate, $locationIds);
            
            $debitNoteDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getDebitNoteWithTaxDetails($fromDate, $toDate, $locationIds);

            $taxDetails = $this->CommonTable('Settings\Model\TaxCompoundTable')->fetchAll();
            $compoundTaxes = [];

            foreach ($taxDetails as $key => $taxValue) {
                $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;
                
            }

            foreach ($debitNoteDetails as $row) {
                $locationWiseData[$row['locationID']]['lc'][$row['ID']]['debitNoteTotal'] = $row['debitNoteTotal'];
                if ($row['taxType'] == 'c') {
                    $locationWiseData[$row['locationID']]['lc'][$row['ID']]['debitNoteCompoundTaxAmount'] += $row['debitNoteTax'];
                }else{
                    $locationWiseData[$row['locationID']]['lc'][$row['ID']]['debitNoteNormalTaxAmount'] += $row['debitNoteTax'];
                }
            }
            
            foreach ($result as $row) {
                $locationWiseData[$row['locationID']]['locName'] = $row['location'];
                $locationWiseData[$row['locationID']]['lc'][$row['ID']]['PVID'] = $row['PVID'];
                $locationWiseData[$row['locationID']]['lc'][$row['ID']]['Date'] = $row['Date'];
                $locationWiseData[$row['locationID']]['lc'][$row['ID']]['IssueDate'] = $row['IssueDate'];
                $locationWiseData[$row['locationID']]['lc'][$row['ID']]['Amount'] = $row['Amount'];
                $locationWiseData[$row['locationID']]['lc'][$row['ID']]['SupplierName'] = $row['SupplierName'];
                $locationWiseData[$row['locationID']]['lc'][$row['ID']]['Status'] = $row['statusName'];
                
                if(array_key_exists($row['purchaseInvoiceTaxID'], $compoundTaxes)){
                    $taxAmount = 0;
                    if (!is_null($row['productSerialID'])) {
                        $taxAmount = floatval($row['purchaseInvoiceTaxAmount']) / $row['purchaseInvoiceProductTotalQty'];
                    } else {
                        $taxAmount = floatval($row['purchaseInvoiceTaxAmount']);
                    }
                    $locationWiseData[$row['locationID']]['lc'][$row['ID']]['compoundTax'] += $taxAmount;
                } else {
                    $taxAmount = 0;
                    if (!is_null($row['productSerialID'])) {
                        $taxAmount = floatval($row['purchaseInvoiceTaxAmount']) / $row['purchaseInvoiceProductTotalQty'];
                    } else {
                        $taxAmount = floatval($row['purchaseInvoiceTaxAmount']);
                    }

                    $locationWiseData[$row['locationID']]['lc'][$row['ID']]['normalTax'] += $taxAmount;
                }
            }
            
            $pvList = array_filter($locationWiseData);

            return $pvList;
        }
    }

    /**
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * @return JSONRespondHtml
     * action to view payment voucher report details
     */
    public function viewPaymentVoucherDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $dailyPayVoucherData = $this->getPaymentVoucherDetails();
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Daily Purchase Invoice Report');
//            $name = 'Daily Payment Voucher Report';
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');

            $headerViewRender = $this->htmlRender($headerView);


            $dailyPayVoucherView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyPayVoucherData' => $dailyPayVoucherData, //$result,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $dailyPayVoucherView->setTemplate('reporting/payment-voucher-report/payment-voucher-table');
            
            $this->setLogMessage("Retrive daily purchase invoice details for daily purchase invoice view report.");
            $this->html = $dailyPayVoucherView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * @return JSONRespondHtml
     * action to generate payment voucher report detail PDF
     */
    public function generatePaymentVoucherDetailsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $dailyPayVoucherData = $this->getPaymentVoucherDetails();
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Daily Purchase Invoice Report');
//            $name = 'Daily Payment Voucher Report';
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');

            $headerViewRender = $this->htmlRender($headerView);


            $dailyPayVoucherView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailyPayVoucherData' => $dailyPayVoucherData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $dailyPayVoucherView->setTemplate('reporting/payment-voucher-report/payment-voucher-table');
            $dailyPayVoucherView->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($dailyPayVoucherView);
            $pdfPath = $this->downloadPDF('daily-payment-voucher-report', $htmlContent, true);

            $this->setLogMessage("Retrive daily purchase invoice details for daily purchase invoice PDF report.");
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * @return JSONRespondHtml
     * action to generate payment voucher report detail CSV
     */
    public function generatePaymentVoucherDetailsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationIds = $this->getlocationID();

            $result = $dailyPayVoucherData = $this->getPaymentVoucherDetails();
            $cD = $this->getCompanyDetails();

            // if ($result) {
            $title = '';
            $tit = 'DAILY PURCHASE INVOICE REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $cD[0]->companyName . "\n";
            $title .= $cD[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

            $in = array(
                0 => "Period : " . $fromDate . '-' . $toDate
            );
            $title.=implode(",", $in) . "\n";
            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("Ref NO", "Location", "Issue Date", "Supplier Name", "PVID", "Due Date", "Status", "Compond Tax(VAT)", "Normal Tax(NBT)","NET PURCHASE","Amount Without Tax");
            $header = implode(",", $arrs);
            $arr = '';
            $i = 1;
            $grandTotal = 0;
            if ($result) {
                foreach ($result as $locID => $data) {
                    if (!empty($data['lc'])) {
                        $in = array(
                            0 => $i,
                            1 => $data['locName']
                        );
                        $arr.=implode(",", $in) . "\n";
                        foreach ($data['lc'] as $d) {
                            $netPurchase = floatval($d['Amount']) - floatval($d['debitNoteTotal']);
                            $debitNoteTotalAmountWithoutTax = 0;
                            $debitNoteTotalAmountWithoutTax = floatval($d['debitNoteTotal']) - (floatval($d['debitNoteNormalTaxAmount']) + floatval($d['debitNoteCompoundTaxAmount']));
                            $totalWithoutTax = floatval($d['Amount']) - (floatval($d['normalTax']) + floatval($d['compoundTax'])) - $debitNoteTotalAmountWithoutTax;
                            $nbt = 0;
                            $nbt = $d['normalTax'] - $d['debitNoteNormalTaxAmount'];
                            $vat = 0;
                            $vat = $d['compoundTax'] - $d['debitNoteCompoundTaxAmount'];

                            $in = array(
                                0 => '',
                                1 => '',
                                2 => $d['IssueDate'],
                                3 => $d['SupplierName'],
                                4 => $d['PVID'],
                                5 => $d['Date'],
                                6 => $d['Status'],
                                7 => $vat,
                                8 => $nbt,
                                9 => $netPurchase,
                                10 => $totalWithoutTax
                            );
                            $arr.=implode(",", $in) . "\n";

                            $grandTotal += $totalWithoutTax;
                        }
                    }
                    $i++;
                }

                $in = array(
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => '',
                    4 => '',
                    5 => '',
                    6 => '',
                    7 => '',
                    8 => '',
                    9 => 'Total Purchase',
                    10 => $grandTotal
                );
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "No related data found\n";
            }

            $name = "Daily_Purchase_Invoice_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->setLogMessage("Retrive daily purchase invoice details for daily purchase invoice CSV report.");
            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function piWiseDebitNoteReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getPiWiseDebitNoteDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Purchase Invoice Wise Debit Note Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/payment-voucher-report/pi-wise-debit-note-report');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function piWiseDebitNotePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getPiWiseDebitNoteDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Purchase Invoice Wise Debit Note Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/payment-voucher-report/pi-wise-debit-note-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('purchase-invoice-wise-debit-note-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function piWiseDebitNoteCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getPiWiseDebitNoteDetails($postData);
            $companyDetails = $this->getCompanyDetails();

            if ($dataList) {
                $data = ['Purchase Invoice Wise Debit Note Report'];
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = [
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                ];
                $title.=implode(",", $data) . "\n";

                $data = ["Period :" . $postData['fromDate'] . " - " . $postData['toDate']];
                $title.=implode(",", $data) . "\n";

                $tableHead = [
                    'Purchase Invoice Code','Purchase Invoice Date','Purchase Invoice Amount',
                    'Purchase Invoice Status','Debit Note Code','Debit Note Date','Debit Note Status',
                    'Total Debit Note Amount','Copied Debit Note Amount'
                ];
                $header.=implode(",", $tableHead) . "\n";

                foreach ($dataList as $piData) {
                    $data = [
                        $piData['purchaseInvoiceCode'],
                        $piData['purchaseInvoiceIssueDate'],
                        number_format($piData['purchaseInvoiceTotal'], 2, '.', ''),
                        $piData['piStatus']
                    ];
                    $arr.=implode(",", $data) . "\n";
                    $totalCopiedAmount = 0;
                    foreach ($piData['debitNoteData'] as $debitNoteData) {                        
                        $data = [
                            '', '', '', '',
                            $debitNoteData['debitNoteCode'],
                            $debitNoteData['debitNoteDate'],
                            $debitNoteData['dNStatus'],
                            number_format($debitNoteData['debitNoteTotal'], 2, '.', ''),
                            number_format($debitNoteData['debitNoteCopiedAmount'], 2, '.', ''),
                        ];
                        $arr.=implode(",", $data) . "\n";
                        $totalCopiedAmount += $debitNoteData['debitNoteCopiedAmount'];
                    }
                    $data = [
                        '', '', '', '', '', '', '', 'Total Copied Amount :',
                        number_format($totalCopiedAmount, 2, '.', '')
                    ];
                    $arr.=implode(",", $data) . "\n";
                }

            } else {
                $arr = "no matching records found";
            }

            $name = "purchase-invoice-Wise-debit-note-Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getPiWiseDebitNoteDetails($postData)
    {
        $postData['isAllPIs'] = filter_var($postData['isAllPIs'], FILTER_VALIDATE_BOOLEAN);
        $postData['piIds'] = ($postData['isAllPIs']) ? [] : $postData['piIds'];

        $piDebitNotes = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPiWiseDebitNote($postData['fromDate'], $postData['toDate'], $postData['piIds']);

        $dataList = [];
        foreach ($piDebitNotes as $invoice) {
            if (!array_key_exists($invoice['purchaseInvoiceID'], $dataList)) {
                $dataList[$invoice['purchaseInvoiceID']] = [
                    'purchaseInvoiceCode' => $invoice['purchaseInvoiceCode'],
                    'purchaseInvoiceIssueDate' => $invoice['purchaseInvoiceIssueDate'],
                    'purchaseInvoiceTotal' => $invoice['purchaseInvoiceTotal'],
                    'piStatus' => $invoice['piStatus']
                ];
            }
            $dataList[$invoice['purchaseInvoiceID']]['debitNoteData'][] = [
                'debitNoteCode' => $invoice['debitNoteCode'],
                'debitNoteDate' => $invoice['debitNoteDate'],
                'debitNoteTotal' => $invoice['debitNoteTotal'],
                'debitNoteCopiedAmount' => $invoice['copiedAmount'],
                'dNStatus' => $invoice['dNStatus'],
            ];
        }
        return $dataList;
    }
}
