<?php

namespace Reporting\Controller\Generate;

use Core\Controller\CoreController;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;

/**
 * Description of SalesPaymentReportController
 *
 * @author shermilan
 */
class SalesPaymentReportController extends CoreController
{

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function viewPaymentsSummeryAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentTypes = $request->getPost('paymentTypes');
            $locations = $request->getPost('locations');
            $accountId = $request->getPost('accountId');
            $catPayModOpt = $request->getPost('categorizedPayModOpt') == 'true' ? true : false;

            $paymentSummeryData = $this->getPaymentsSummeryDetails($fromDate, $toDate, $paymentTypes, $locations,false, $accountId);

            if ($catPayModOpt) {
                $paymentSummeryData = $this->modifyDataForCategorizedPaymentsSummeryDetails($paymentSummeryData);
            }
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Payment Summary Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $paymentSummeryView = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentSummeryData' => $paymentSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            if ($catPayModOpt) {
                $paymentSummeryView->setTemplate('reporting/sales-payment-report/generate-categorized-payments-summery-pdf');
            } else {
                $paymentSummeryView->setTemplate('reporting/sales-payment-report/generate-payments-summery-pdf');
            }

            $this->html = $paymentSummeryView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generatePaymentsSummeryPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentTypes = $request->getPost('paymentTypes');
            $locations = $request->getPost('locations');
            $accountId = $request->getPost('accountId');
            $catPayModOpt = $request->getPost('categorizedPayModOpt') == 'true' ? true : false;

            $paymentSummeryData = $this->getPaymentsSummeryDetails($fromDate, $toDate, $paymentTypes, $locations,false, $accountId);

            if ($catPayModOpt) {
                $paymentSummeryData = $this->modifyDataForCategorizedPaymentsSummeryDetails($paymentSummeryData);
            }

            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Payment Summary Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewPaymentSummery = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentSummeryData' => $paymentSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            if ($catPayModOpt) {
                $viewPaymentSummery->setTemplate('reporting/sales-payment-report/generate-categorized-payments-summery-pdf');
            } else {
                $viewPaymentSummery->setTemplate('reporting/sales-payment-report/generate-payments-summery-pdf');
            }
            $viewPaymentSummery->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($viewPaymentSummery);
            $pdfPath = $this->downloadPDF('payments-summary-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;

            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * Generate Payment Summery CSV Sheet
     * @return CSV Sheet
     */
    public function generatePaymentsSummerySheetAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentTypes = $request->getPost('paymentTypes');
            $locations = $request->getPost('locations');
            $accountId = $request->getPost('accountId');
            $catPayModOpt = $request->getPost('categorizedPayModOpt') == 'true' ? true : false;

            $paymentsSummeryData = $this->getPaymentsSummeryDetails($fromDate, $toDate, $paymentTypes, $locations, false, $accountId);

            if ($catPayModOpt) {
                $paymentsSummeryData = $this->modifyDataForCategorizedPaymentsSummeryDetails($paymentsSummeryData);
            }

            $cD = $this->getCompanyDetails();

            if ($paymentsSummeryData) {
                $title = '';
                $tit = 'PAYMENT SUMMARY REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => $tit,
                    4 => "",
                    5 => "",
                    6 => "",
                );
                $title = implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "Period : " . $fromDate . '-' . $toDate
                );
                $title.=implode(",", $in) . "\n";

                if ($catPayModOpt) {
                    $arrs = array("PAYMENT NO", "DATE", "CUSTOMER", "LOCATION", "AMOUNT", "CHEQUE NO","POST DATED CHEQUE DATE", "AMOUNT", "REMARKS");
                    $header = implode(",", $arrs);
                    $arr = '';
                    foreach ($paymentsSummeryData as $method => $value) {
                    $in = array($method . ' payments');
                    $arr.=implode(",", $in) . "\n";
                        foreach ($value as $paymentsSummeryData1) {
                            $rowNo = 0;
                            foreach ($paymentsSummeryData1 as $data) {
                                $rowNo++;
                                if ($rowNo != 0) {
                                    $data['incomingPaymentCode'] = '';
                                }

                                if ($data['paymentMethodName'] == "Credit") {
                                    $payAmount = $data['incomingPaymentCreditAmount'];
                                } else {
                                    $payAmount = $data['incomingPaymentMethodAmount'];
                                }
                                if ($data['locationName'] != NULL) {
                                    if ($rowNo == count($paymentsSummeryData1)) {
                                        $incomingPaymentMethodWiseAmount = $paymentsSummeryData1['incomingPaymentMethodWiseAmount'];
                                    } else if ($rowNo == (count($paymentsSummeryData1)-1)) {
                                        $incomingPaymentMethodWiseAmount = $paymentsSummeryData1['incomingPaymentMethodWiseAmount'];
                                    } else {
                                        $incomingPaymentMethodWiseAmount = "";
                                    }
                                    $in = array(
                                        0 => $data['incomingPaymentCode'],
                                        1 => $data['incomingPaymentDate'],
                                        2 => $data['customerTitle'] . ' ' . $data['customerName'],
                                        3 => $data['locationName'],
                                        4 => $payAmount,
                                        5 => $data['paymentMethodReferenceNumber'],
                                        6 => $data['postdatedChequeDate'],
                                        7 => $incomingPaymentMethodWiseAmount,
                                        8 => $data['incomingPaymentMemo']
                                    );
                                    $arr.=implode(",", $in) . "\n";
                                }
                            }
                        }
                    }
                } else {
                    $arrs = array("PAYMENT NO", "DATE", "CUSTOMER", "LOCATION", "PAYMENT METHOD AND AMOUNT", "CHEQUE NO","POST DATED CHEQUE DATE", "AMOUNT", "REMARKS");
                    $header = implode(",", $arrs);
                    $arr = '';
                    foreach ($paymentsSummeryData as $paymentsSummeryData1) {
                        $rowNo = 0;
                        foreach ($paymentsSummeryData1 as $data) {
                            if ($rowNo != 0) {
                                $data['incomingPaymentCode'] = '';
                            }
                            if ($rowNo != count($paymentsSummeryData1) - 1) {
                                $data['incomingPaymentAmount'] = '';
                            }
                            if ($data['paymentMethodName'] == "Credit") {
                                $payAmount = '(' . $data['paymentMethodName'] . ') ' . $data['incomingPaymentCreditAmount'];
                            } else {
                                $payAmount = '(' . $data['paymentMethodName'] . ') ' . $data['incomingPaymentMethodAmount'];
                            }
                            $in = array(
                                0 => $data['incomingPaymentCode'],
                                1 => $data['incomingPaymentDate'],
                                2 => $data['customerTitle'] . ' ' . $data['customerName'],
                                3 => $data['locationName'],
                                4 => $payAmount,
                                5 => $data['paymentMethodReferenceNumber'],
                                6 => $data['postdatedChequeDate'],
                                7 => $data['incomingPaymentAmount'],
                                8 => $data['incomingPaymentMemo']
                            );
                            $arr.=implode(",", $in) . "\n";
                            $rowNo++;
                        }
                    }
                }
            } else {
                $arr = 'no matching records founds \n';
            }

            $name = "payment_summary_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  Dissanayake<sandun@thinkcube.com>
     * @return array $respond
     */
    public function getPaymentsSummeryDetails($fromDate = null, $toDate = null, $paymentNumber = null, $locationNumber = null, $cancelled = false, $accountId)
    {
        if (isset($fromDate) && isset($toDate)) {
            $accountId = (empty($accountId)) ? null : $accountId;
            if (!is_null($accountId)) {
                $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
                $accountId = $account['financeAccountID'];
            }
            $respon = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsSummeryDataForPaymentReport($fromDate, $toDate, false, $paymentNumber, $locationNumber, $cancelled, $accountId);
            $respond = $respon ? $respon : array();
            $finalData = array();
            $finalCrdeitData = array();
            $i = 0;
            $j = 0;

            foreach ($paymentNumber as $value) {
                if ($value == "0") {
                    $respondd = $this->CommonTable('Invoice\Model\PaymentsTable')->getCreditPaymentsSummeryDataForPaymentReport($fromDate, $toDate, false, $paymentNumber, $locationNumber, $cancelled, $accountId);
                    $creditRespond = $respondd ? $respondd : array();
                }
            }
            if (!$respond) {
                if ($creditRespond) {
                    foreach ($creditRespond as $key => $value) {
                        if (!is_null($value['incomingPaymentCreditAmount'])) {
                            $value['paymentMethodName'] = 'Credit';
                            $finalData[$value['incomingPaymentCode']][$value['incomingPaymentID']] = $value;
                        }
                    }
                }
            }

            if (empty($respond) && empty($finalData)) {
                return null;
            }

            foreach ($respond as $res) {
                $incomingPaymentCustomCurrencyRate = ($res['incomingPaymentCustomCurrencyRate'] == 0) ? 1 : $res['incomingPaymentCustomCurrencyRate'];
                //this condition use to map currency rate with payed amount and item wise payed amount
                if ($res['incomingPaymentCustomCurrencyRate']) {
                    $res['incomingPaymentMethodAmount'] = $res['incomingPaymentMethodAmount'] / $incomingPaymentCustomCurrencyRate;
                    $res['incomingPaymentAmount'] = $res['incomingPaymentAmount'] / $incomingPaymentCustomCurrencyRate;
                }

                if (in_array('1', $paymentNumber) && isset($res['incomingPaymentMethodCashId'])) {
                    $res['paymentMethodName'] = 'Cash';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('2', $paymentNumber) && isset($res['incomingPaymentMethodChequeId'])) {
                    $res['paymentMethodName'] = 'Cheque';
                    $res['paymentMethodReferenceNumber'] = $res['incomingPaymentMethodChequeNumber'];
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('3', $paymentNumber) && isset($res['incomingPaymentMethodCreditCardId'])) {
                    $res['paymentMethodName'] = 'Credit Card';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('4', $paymentNumber) && isset($res['incomingPaymentMethodLoyaltyCardId']) && is_null($res['incomingPaymentMethodCashId']) && is_null($res['incomingPaymentMethodChequeId']) && is_null($res['incomingPaymentMethodCreditCardId']) && is_null($res['incomingPaymentMethodBankTransferId']) && is_null($res['incomingPaymentMethodGiftCardId'])) {
                    $res['paymentMethodName'] = 'Loyalty Card';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('5', $paymentNumber) && isset($res['incomingPaymentMethodBankTransferId'])) {
                    $res['paymentMethodName'] = 'Bank Transfer';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('6', $paymentNumber) && isset($res['incomingPaymentMethodGiftCardId'])) {
                    $res['paymentMethodName'] = 'Gift Card';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('7', $paymentNumber) && isset($res['incomingPaymentMethodLCId']) && $res['incomingPaymentMethodLCId'] != "0") {
                    $res['paymentMethodName'] = 'LC';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('8', $paymentNumber) && isset($res['incomingPaymentMethodTTId']) && $res['incomingPaymentMethodTTId'] != "0") {
                    $res['paymentMethodName'] = 'TT';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                }

                foreach ($creditRespond as $key => $value) {
                    if (!is_null($value['incomingPaymentCreditAmount'])) {
                        $value['paymentMethodName'] = 'Credit';
                        $finalData[$value['incomingPaymentCode']][$value['incomingPaymentID']] = $value;
                    }
                    $j++;
                }
                $i++;
            }

            ksort($finalData);
            return $finalData;
                
        }
    }

    public function viewCancelledPaymentsSummeryAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentTypes = $request->getPost('paymentTypes');
            $locations = $request->getPost('locations');
            $cancelled = true;
            $paymentSummeryData = $this->getPaymentsSummeryDetails($fromDate, $toDate, $paymentTypes, $locations, $cancelled);

            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Cancelled Payment Summary Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $paymentSummeryView = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentSummeryData' => $paymentSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $paymentSummeryView->setTemplate('reporting/sales-payment-report/generate-payments-summery-pdf');

            $this->html = $paymentSummeryView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateCanclledPaymentsSummeryPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentTypes = $request->getPost('paymentTypes');
            $locations = $request->getPost('locations');
            $cancelled = true;
            $paymentSummeryData = $this->getPaymentsSummeryDetails($fromDate, $toDate, $paymentTypes, $locations, $cancelled);

            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Cancelled Payment Summary Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewPaymentSummery = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentSummeryData' => $paymentSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $viewPaymentSummery->setTemplate('reporting/sales-payment-report/generate-payments-summery-pdf');
            $viewPaymentSummery->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($viewPaymentSummery);
            $pdfPath = $this->downloadPDF('cancelled-payments-summary-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;

            return $this->JSONRespond();
        }
    }

    public function generateCancelledPaymentsSummerySheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentTypes = $request->getPost('paymentTypes');
            $locations = $request->getPost('locations');
            $cancelled = true;
            $paymentsSummeryData = $this->getPaymentsSummeryDetails($fromDate, $toDate, $paymentTypes, $locations, $cancelled);
            $cD = $this->getCompanyDetails();

            if ($paymentsSummeryData) {
                $title = '';
                $tit = 'Cancelled PAYMENT SUMMARY REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => $tit,
                    4 => "",
                    5 => "",
                    6 => "",
                );
                $title = implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "Period : " . $fromDate . '-' . $toDate
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("PAYMENT NO", "DATE", "CUSTOMER", "LOCATION", "PAYMENT METHOD AND AMOUNT", "CHEQUE NO", "AMOUNT", "REMARKS");
                $header = implode(",", $arrs);
                $arr = '';
                foreach ($paymentsSummeryData as $paymentsSummeryData1) {
                    $rowNo = 0;
                    foreach ($paymentsSummeryData1 as $data) {
                        if ($rowNo != 0) {
                            $data['incomingPaymentCode'] = '';
                        }
                        if ($rowNo != count($paymentsSummeryData1) - 1) {
                            $data['incomingPaymentAmount'] = '';
                        }
                        if ($data['paymentMethodName'] == "Credit") {
                            $payAmount = '(' . $data['paymentMethodName'] . ') ' . $data['incomingPaymentCreditAmount'];
                        } else {
                            $payAmount = '(' . $data['paymentMethodName'] . ') ' . $data['incomingPaymentMethodAmount'];
                        }
                        $in = array(
                            0 => $data['incomingPaymentCode'],
                            1 => $data['incomingPaymentDate'],
                            2 => $data['customerTitle'] . ' ' . $data['customerName'],
                            3 => $data['locationName'],
                            4 => $payAmount,
                            5 => $data['paymentMethodReferenceNumber'],
                            6 => $data['incomingPaymentAmount'],
                            7 => $data['incomingPaymentMemo']
                        );
                        $arr.=implode(",", $in) . "\n";
                        $rowNo++;
                    }
                }
            } else {
                $arr = 'no matching records founds \n';
            }

            $name = "cancelled_payment_summary_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function salesDetailsReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentIds = $request->getPost('paymentIds');
            $locationIds = $request->getPost('locationIds');
            $isAllPayments = $request->getPost('isAllPayments');            
            $paymentIds = (filter_var($isAllPayments, FILTER_VALIDATE_BOOLEAN)) ? [] : $paymentIds;
            $accountId = $request->getPost('accountId');
            $salesPersons = $request->getPost('salesPersons');

            $paymentDetails = $this->getSalesDetailsReportDetails($paymentIds, $locationIds, $fromDate, $toDate, $accountId, $salesPersons);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Payment Details Report');
            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);
            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentData' => $paymentDetails,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            if (empty($salesPersons)) {
                $view->setTemplate('reporting/sales-payment-report/generate-payments-details-report');
            } else {
                $view->setTemplate('reporting/sales-payment-report/generate-sales-person-wise-payments-details-report');
            }

            $this->html = $view;
            $this->status = true;
        }
        return $this->JSONRespondHtml();
    }

    public function salesDetailsReportPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentIds = $request->getPost('paymentIds');
            $locationIds = $request->getPost('locationIds');
            $isAllPayments = $request->getPost('isAllPayments');            
            $paymentIds = (filter_var($isAllPayments, FILTER_VALIDATE_BOOLEAN)) ? [] : $paymentIds;
            $accountId = $request->getPost('accountId');
            $salesPersons = $request->getPost('salesPersons');
            $paymentDetails = $this->getSalesDetailsReportDetails($paymentIds, $locationIds, $fromDate, $toDate, $accountId, $salesPersons);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Payment Details Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentData' => $paymentDetails,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));


            if (empty($salesPersons)) {
                $view->setTemplate('reporting/sales-payment-report/generate-payments-details-report');
            } else {
                $view->setTemplate('reporting/sales-payment-report/generate-sales-person-wise-payments-details-report');
            }

            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('payment-details-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
        }
        return $this->JSONRespond();
    }

    public function salesDetailsReportCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentIds = $request->getPost('paymentIds');
            $locationIds = $request->getPost('locationIds');
            $isAllPayments = $request->getPost('isAllPayments');            
            $paymentIds = (filter_var($isAllPayments, FILTER_VALIDATE_BOOLEAN)) ? [] : $paymentIds;
            $accountId = $request->getPost('accountId');
            $salesPersons = $request->getPost('salesPersons');

            $paymentDetails = $this->getSalesDetailsReportDetails($paymentIds, $locationIds, $fromDate, $toDate, $accountId, $salesPersons);
            if ($paymentDetails) {
                $data = array(
                    "Payment Details Report"
                );
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = array(
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    "Period :" . $fromDate . " - " . $toDate
                );
                $title.=implode(",", $data) . "\n";


                if (empty($salesPersons)) {
                    $tableHead = array('Invoice Number', 'Customer Name', 'Invoice Date', 'Invoice DueDate', 'Currency','Invoice Amount', 'Credit Amount', 'All Paid Amount', 'Remaining Amount', 'Paid Amount', 'Payment Number', 'Payment Date', 'Payment Amount', 'Payment Method', 'Reference');
                    $header.=implode(",", $tableHead) . "\n";

                    $totalInvoiceAmount = 0.00;
                    $totalPayedAmount = 0.00;
                    $totalRemainingAmount = 0.00;
                    $totalPaymentAmount = 0.00;
                    $totalPaidAmountByPeriod = 0.00;

                    foreach ($paymentDetails as $paymentData) {
                        $totalInvoiceAmount += floatval($paymentData['realInvAmo']);
                        $totalCreditAmount += floatval($data['realCrditAmo']);
                        $totalPayedAmount += floatval($paymentData['realpayAmo']);
                        $totalRemainingAmount += floatval($paymentData['realremAmo']);
                        $totalPaidAmountByPeriod += floatval($paymentData['paidAmountByPeriod']);
                        
                        $data = array($paymentData['invoiceNumber'], $paymentData['customerName'], $paymentData['invoiceDate'], $paymentData['invoiceDueDate'],$paymentData['currencySymbol'], $paymentData['realInvAmo'], $paymentData['realCrditAmo'], $paymentData['realpayAmo'], $paymentData['realremAmo'],number_format(($paymentData['paidAmountByPeriod']), 2, '.', ''));
                        $arr.=implode(",", $data) . "\n";
                        foreach ($paymentData['payments'] as $payment) {
                            $totalPaymentAmount += floatval(explode(" ",$payment['paymentAmount'])[1]);
                            if ($payment['multipleInvoiceForPayment']) {
                                $paymentCode = $payment['paymentNumber'].' *';
                            } else {
                                $paymentCode = $payment['paymentNumber'];
                            }
                            $data = array('', '', '','' ,'', '', '', '', '', '',$paymentCode, $payment['paymentDate'], $payment['paymentRealAmount'], $payment['paymentMethod'], $payment['reference']);
                            $arr.=implode(",", $data) . "\n";
                        }
                    }

                    $totalData = array('', '', '','','TOTAL', $totalInvoiceAmount, $totalCreditAmount, $totalPayedAmount, $totalRemainingAmount,$totalPaidAmountByPeriod, '', '', '', '', '');
                    $arr.=implode(",", $totalData) . "\n";
                } else {
                    $tableHead = array('Sales Person','Invoice Number', 'Customer Name', 'Invoice Date', 'Invoice DueDate', 'Currency','Invoice Amount', 'Credit Amount', 'All Paid Amount', 'Remaining Amount', 'Paid Amount', 'Payment Number', 'Payment Date', 'Payment Amount', 'Payment Method', 'Reference');
                    $header.=implode(",", $tableHead) . "\n";

                    $totalInvoiceAmount = 0.00;
                    $totalPayedAmount = 0.00;
                    $totalRemainingAmount = 0.00;
                    $totalPaymentAmount = 0.00;
                    $totalPaidAmountByPeriod = 0.00;

                    foreach ($paymentDetails as $pData) {
                        $data = array($pData['salesPersonName']);
                        $arr.=implode(",", $data) . "\n";

                        $salesPersonTotalInvoiceAmount = 0.00;
                        $salesPersonTotalPayedAmount = 0.00;
                        $salesPersonTotalRemainingAmount = 0.00;
                        $salesPersonTotalPaymentAmount = 0.00;
                        $salesPersonTotalPaidAmountByPeriod = 0.00;

                        if ($pData['salesPersonName']) {
                            foreach ($pData['data'] as $paymentData) {
                                $totalInvoiceAmount += floatval($paymentData['realInvAmo']);
                                $totalCreditAmount += floatval($data['realCrditAmo']);
                                $totalPayedAmount += floatval($paymentData['realpayAmo']);
                                $totalRemainingAmount += floatval($paymentData['realremAmo']);
                                $totalPaidAmountByPeriod += floatval($paymentData['paidAmountByPeriod']);

                                $salesPersonTotalInvoiceAmount += floatval($paymentData['realInvAmo']);
                                $salesPersonTotalCreditAmount += floatval($data['realCrditAmo']);
                                $salesPersonTotalPayedAmount += floatval($paymentData['realpayAmo']);
                                $salesPersonTotalRemainingAmount += floatval($paymentData['realremAmo']);
                                $salesPersonTotalPaidAmountByPeriod += floatval($paymentData['paidAmountByPeriod']);
                                
                                $data = array('',$paymentData['invoiceNumber'], $paymentData['customerName'], $paymentData['invoiceDate'], $paymentData['invoiceDueDate'], $paymentData['currencySymbol'], $paymentData['realInvAmo'], $paymentData['realCrditAmo'], $paymentData['realpayAmo'], $paymentData['realremAmo'],number_format(($paymentData['paidAmountByPeriod']), 2, '.', ''));
                                $arr.=implode(",", $data) . "\n";
                                foreach ($paymentData['payments'] as $payment) {
                                    $totalPaymentAmount += floatval(explode(" ",$payment['paymentAmount'])[1]);
                                    $salesPersonTotalPaymentAmount += floatval(explode(" ",$payment['paymentAmount'])[1]);
                                    if ($payment['multipleInvoiceForPayment']) {
                                        $paymentCode = $payment['paymentNumber'].' *';
                                    } else {
                                        $paymentCode = $payment['paymentNumber'];
                                    }
                                    $data = array('', '', '', '', '', '','', '', '','','', $paymentCode, $payment['paymentDate'], $payment['paymentRealAmount'], $payment['paymentMethod'], $payment['reference']);
                                    $arr.=implode(",", $data) . "\n";
                                }
                            }
                            $totalData = array('', '', '','','', 'Total of '.$pData['salesPersonName'], $salesPersonTotalInvoiceAmount, $salesPersonTotalCreditAmount, $salesPersonTotalPayedAmount, $salesPersonTotalRemainingAmount, $salesPersonTotalPaidAmountByPeriod, '', '', '', '', '');
                            $arr.=implode(",", $totalData) . "\n";
                        }

                    }
                    $totalData = array('', '', '','', '','TOTAL', $totalInvoiceAmount, $totalCreditAmount, $totalPayedAmount, $totalRemainingAmount, $totalPaidAmountByPeriod, '', '', '', '', '');
                    $arr.=implode(",", $totalData) . "\n";
                }
            } else {
                $arr = "no matching records found";
            }
            $name = "payment-details-report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);
            $this->data = $csvPath;
            $this->status = true;
        }
        return $this->JSONRespond();
    }

    /**
     * Get sales invoice detail report
     * @param array $invoiceIds
     * @param data $fromDate
     * @param date $toDate
     * @return array
     */
    private function getSalesDetailsReportDetails($paymentIds, $locationIds, $fromDate, $toDate, $accountId, $salesPersons)
    {
        $salesData = [];
        $accountId = (empty($accountId)) ? null : $accountId;
        if (!is_null($accountId)) {
            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
            $accountId = $account['financeAccountID'];
        }

        if (!empty($salesPersons)) {
            $salesDetails = $this->getSalesDetailsReportSalesPersonWiseDetails($paymentIds, $locationIds, $fromDate, $toDate, $accountId, $salesPersons);
            return $salesDetails;
        }

        $paymentDetails = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoicePaymentDetails([], $paymentIds, $locationIds, $fromDate, $toDate, [], $accountId);
        if ($paymentDetails) {
            $paymentDataArr = [];
            $paidAmountByPeriodArray = [];
            
            foreach ($paymentDetails as $payment) {
                if (!array_key_exists($payment['salesInvoiceCode'], $salesData)) {
                    $paymentDataArr = [];
                    $paidAmount = $payment['salesInvoicePayedAmount'];
                    $invoiceAmount = $payment['salesinvoiceTotalAmount'];
                    $creditNoteDetails = $this->CommonTable("Invoice\Model\CreditNoteTable")->getInvoiceCreditNoteTotalByInvoiceId($payment['salesInvoiceID']);
                    $creditNoteAmount = ($creditNoteDetails['total']) ? $creditNoteDetails['total'] : 0;
                    
                    if ($payment['incomingPaymentCustomCurrencyRate'] > 0) {
                        $paidAmount = $payment['salesInvoicePayedAmount'] / $payment['incomingPaymentCustomCurrencyRate'];
                        $invoiceAmount = $payment['salesinvoiceTotalAmount'] / $payment['incomingPaymentCustomCurrencyRate'];
                        $creditNoteAmount = $creditNoteAmount / $payment['incomingPaymentCustomCurrencyRate'];
                    }

                    $salesData[$payment['salesInvoiceCode']] = array(
                        'invoiceNumber' => $payment['salesInvoiceCode'],
                        'invoiceDate' => $payment['salesInvoiceIssuedDate'],
                        'invoiceDueDate' => $payment['salesInvoiceOverDueDate'],
                        'invoiceAmount' => $payment['currencySymbol'] . ' ' . number_format(($invoiceAmount), 2, '.', ''),
                        'creditAmount' => $payment['currencySymbol'] . ' ' . number_format(($creditNoteAmount), 2, '.', ''),
                        'payedAmount' => $payment['currencySymbol'] . ' ' . number_format($paidAmount, 2, '.', ''),
                        'remainingAmount' => $payment['currencySymbol'] . ' ' . number_format((($invoiceAmount - $paidAmount - $creditNoteAmount)), 2, '.', ''),
                        'realInvAmo' => floatval($invoiceAmount),
                        'realpayAmo' => floatval($paidAmount),
                        'realremAmo' => floatval($invoiceAmount)- floatval($paidAmount) - floatval($creditNoteAmount),
                        'realCrditAmo' => floatval($creditNoteAmount),
                        'customerName' => $payment['customerName'],
                        'payments' => [],
                        'paidAmountByPeriod' => 0,
                        'currencySymbol' => $payment['currencySymbol']
                    );
                }
                $paymentMethodAmount = ($payment['incomingPaymentCustomCurrencyRate'] > 0) ? ($payment['incomingPaymentMethodAmount'] / $payment['incomingPaymentCustomCurrencyRate']) : $payment['incomingPaymentMethodAmount'];
                
                $paymentMethodInvoiceAmount = ($payment['incomingPaymentCustomCurrencyRate'] > 0) ? ($payment['incomingInvoicePaymentAmount'] / $payment['incomingPaymentCustomCurrencyRate']) : $payment['incomingInvoicePaymentAmount'];

                $paymentInvoiceCount = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getInvoicePaymentByPaymentId($payment['incomingPaymentID']);
                $multipleInvoiceForPayment = false;
                if (count($paymentInvoiceCount) > 1) {
                    $multipleInvoiceForPayment = true;
                }
                //to keep payment method details
                $paymentData = array(
                    'paymentNumber' => $payment['incomingPaymentCode'],
                    'multipleInvoiceForPayment' => $multipleInvoiceForPayment,
                    'paymentDate' => $payment['incomingPaymentDate'],
                    'paymentAmount' => $payment['currencySymbol'] . ' ' . number_format($paymentMethodAmount, 2, '.', ''),
                    'paymentRealAmount' => number_format($paymentMethodAmount, 2, '.', '')
                );
                if (empty($salesData[$payment['salesInvoiceCode']]['payments']) && $payment['incomingPaymentCreditAmount'] != '') {

                    $creditValue = ($payment['incomingPaymentCustomCurrencyRate'] > 0) ? $payment['incomingPaymentCreditAmount'] / $payment['incomingPaymentCustomCurrencyRate']: $payment['incomingPaymentCreditAmount'];
                    $creditPayments = array(
                        'paymentNumber' => $payment['incomingPaymentCode'],
                        'multipleInvoiceForPayment' => $multipleInvoiceForPayment,
                        'paymentDate' => $payment['incomingPaymentDate'],
                        'paymentAmount' => $payment['currencySymbol'] . ' ' . number_format($creditValue, 2, '.', ''),
                        'paymentRealAmount' => number_format($creditValue, 2, '.', ''),
                        'paymentMethod' => 'credit payments',
                        'reference' => '-'
                        );
                    $salesData[$payment['salesInvoiceCode']]['payments'][] = $creditPayments;
                }

                //add payment mothod and reference to payment details
                if ($paidAmount > 0) {
                    $paymentDataArr = $this->addPaymentMethodAndReferance($payment, $paymentData);

                    if (!array_key_exists($payment['incomingPaymentCode'].$payment['salesInvoiceCode'], $paidAmountByPeriodArray)) {
                        $salesData[$payment['salesInvoiceCode']]['paidAmountByPeriod'] += $paymentMethodInvoiceAmount;
                        
                        $paidAmountByPeriodArray[$payment['incomingPaymentCode'].$payment['salesInvoiceCode']] = $paymentMethodInvoiceAmount; 
                    }
                    $salesData[$payment['salesInvoiceCode']]['payments'][] = $paymentDataArr;
                } else {
                    $salesData[$payment['salesInvoiceCode']]['payments'][] = array(
                            'paymentNumber' => '-',
                            'paymentDate' => ' ',
                            'paymentAmount' => ' ',
                            'paymentMethod' => ' ',
                            'reference' => ' '
                    );
                }
                
            }
        }
        return $salesData;
    }

    public function getSalesDetailsReportSalesPersonWiseDetails($paymentIds, $locationIds, $fromDate, $toDate, $accountId, $salesPersons) 
    {
        $salesData = [];
        $withDefaultSalesPerson = (in_array('other', $salesPersons)) ? true : false;
        
        $paymentDetails = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoicePaymentDetailsForSalesPersonWise([], $paymentIds, $locationIds, $fromDate, $toDate, [], $accountId, $salesPersons, $withDefaultSalesPerson);

        if ($paymentDetails) {
            $paymentDataArr = [];
            $paidAmountByPeriodArray = [];
            foreach ($paymentDetails as $payment) { 
                $salesPersonID = (is_null($payment['salesPersonID'])) ? "other" : $payment['salesPersonID'];
                $salesPersonName = ($salesPersonID == "other") ? "Other" :$payment['salesPersonSortName'];

                $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($payment['salesInvoiceID']);
                $numOfSP = (is_null($payment['salesPersonID'])) ? 1 : sizeof($relatedSalesPersons);


                if (!array_key_exists($payment['salesInvoiceCode'], $salesData[$salesPersonID]['data'])) {
                    $paymentDataArr = [];
                    $paidAmount = $payment['salesInvoicePayedAmount'];
                    $invoiceAmount = $payment['salesinvoiceTotalAmount'];
                    $creditNoteDetails = $this->CommonTable("Invoice\Model\CreditNoteTable")->getInvoiceCreditNoteTotalByInvoiceId($payment['salesInvoiceID']);
                    $creditNoteAmount = ($creditNoteDetails['total']) ? $creditNoteDetails['total'] : 0;
                    
                    if ($payment['incomingPaymentCustomCurrencyRate'] > 0) {
                        $paidAmount = ($payment['salesInvoicePayedAmount'] / $payment['incomingPaymentCustomCurrencyRate']) / $numOfSP;
                        $invoiceAmount = ($payment['salesinvoiceTotalAmount'] / $payment['incomingPaymentCustomCurrencyRate']) / $numOfSP;
                        $creditNoteAmount = ($creditNoteAmount / $payment['incomingPaymentCustomCurrencyRate']) / $numOfSP;
                    }

                    $salesData[$salesPersonID]['data'][$payment['salesInvoiceCode']] = array(
                        'invoiceNumber' => $payment['salesInvoiceCode'],
                        'invoiceDate' => $payment['salesInvoiceIssuedDate'],
                        'invoiceDueDate' => $payment['salesInvoiceOverDueDate'],
                        'invoiceAmount' => $payment['currencySymbol'] . ' ' . number_format(($invoiceAmount), 2, '.', ''),
                        'creditAmount' => $payment['currencySymbol'] . ' ' . number_format(($creditNoteAmount), 2, '.', ''),
                        'payedAmount' => $payment['currencySymbol'] . ' ' . number_format($paidAmount, 2, '.', ''),
                        'remainingAmount' => $payment['currencySymbol'] . ' ' . number_format((($invoiceAmount - $paidAmount - $creditNoteAmount)), 2, '.', ''),
                        'realInvAmo' => floatval($invoiceAmount),
                        'realpayAmo' => floatval($paidAmount),
                        'realremAmo' => floatval($invoiceAmount)- floatval($paidAmount) - floatval($creditNoteAmount),
                        'realCrditAmo' => floatval($creditNoteAmount),
                        'customerName' => $payment['customerName'],
                        'salesPerson' => ($salesPersonID == "other") ? "Other" :$payment['salesPersonSortName'],
                        'payments' => [],
                        'paidAmountByPeriod' => 0,
                        'currencySymbol' => $payment['currencySymbol']
                    );
                }

                $salesData[$salesPersonID]['salesPersonName'] = $salesPersonName;
                
                $paymentMethodAmount = ($payment['incomingPaymentCustomCurrencyRate'] > 0) ? (($payment['incomingPaymentMethodAmount'] / $payment['incomingPaymentCustomCurrencyRate'])) / $numOfSP : $payment['incomingPaymentMethodAmount'] / $numOfSP;

                $paymentMethodInvoiceAmount = ($payment['incomingPaymentCustomCurrencyRate'] > 0) ? (($payment['incomingInvoicePaymentAmount'] / $payment['incomingPaymentCustomCurrencyRate'])) / $numOfSP : $payment['incomingInvoicePaymentAmount'] / $numOfSP;

                $paymentInvoiceCount = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getInvoicePaymentByPaymentId($payment['incomingPaymentID']);
                $multipleInvoiceForPayment = false;
                if (count($paymentInvoiceCount) > 1) {
                    $multipleInvoiceForPayment = true;
                }
                //to keep payment method details
                $paymentData = array(
                    'paymentNumber' => $payment['incomingPaymentCode'],
                    'multipleInvoiceForPayment' => $multipleInvoiceForPayment,
                    'paymentDate' => $payment['incomingPaymentDate'],
                    'paymentAmount' => $payment['currencySymbol'] . ' ' . number_format($paymentMethodAmount, 2, '.', ''),
                    'paymentRealAmount' => number_format($paymentMethodAmount, 2, '.', '')
                );
                if (empty($salesData[$salesPersonID]['data'][$payment['salesInvoiceCode']]['payments']) && $payment['incomingPaymentCreditAmount'] != '') {

                    $creditValue = ($payment['incomingPaymentCustomCurrencyRate'] > 0) ? ($payment['incomingPaymentCreditAmount'] / $payment['incomingPaymentCustomCurrencyRate']) / $numOfSP: $payment['incomingPaymentCreditAmount'] / $numOfSP;
                    $creditPayments = array(
                        'paymentNumber' => $payment['incomingPaymentCode'],
                        'multipleInvoiceForPayment' => $multipleInvoiceForPayment,
                        'paymentDate' => $payment['incomingPaymentDate'],
                        'paymentAmount' => $payment['currencySymbol'] . ' ' . number_format($creditValue, 2, '.', ''),
                        'paymentRealAmount' => number_format($creditValue, 2, '.', ''),
                        'paymentMethod' => 'credit payments',
                        'reference' => '-'
                        );
                    $salesData[$salesPersonID]['data'][$payment['salesInvoiceCode']]['payments'][] = $creditPayments;
                }

                //add payment mothod and reference to payment details
                if ($paidAmount > 0) {
                    $paymentDataArr = $this->addPaymentMethodAndReferance($payment, $paymentData);
                    if (!array_key_exists($salesPersonID.$payment['incomingPaymentCode'].$payment['salesInvoiceCode'], $paidAmountByPeriodArray)) {
                        $salesData[$salesPersonID]['data'][$payment['salesInvoiceCode']]['paidAmountByPeriod'] += $paymentMethodInvoiceAmount;
                        
                        $paidAmountByPeriodArray[$salesPersonID.$payment['incomingPaymentCode'].$payment['salesInvoiceCode']] = $paymentMethodInvoiceAmount; 
                    }

                    $salesData[$salesPersonID]['data'][$payment['salesInvoiceCode']]['payments'][] = $paymentDataArr;
                } else {
                    $salesData[$salesPersonID]['data'][$payment['salesInvoiceCode']]['payments'][] = array(
                            'paymentNumber' => '-',
                            'paymentDate' => ' ',
                            'paymentAmount' => ' ',
                            'paymentMethod' => ' ',
                            'reference' => ' '
                    );
                }
            }
        }
        return $salesData;
    }


    /**
     * Get payment method details
     * @param array $paymentData
     * @param array $paymentMethodData
     * @return string
     */
    private function addPaymentMethodAndReferance($paymentData, $paymentMethodData)
    {
        if (!empty(intval($paymentData['incomingPaymentMethodCashId']))) {
            $paymentMethodData['paymentMethod'] = 'Cash';
            $paymentMethodData['reference'] = '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodChequeId']))) {
            $paymentMethodData['paymentMethod'] = 'Cheque';
            $paymentMethodData['reference'] = (!empty($paymentData['incomingPaymentMethodChequeNumber'])) ? $paymentData['incomingPaymentMethodChequeNumber'] : '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodCreditCardId']))) {
            $paymentMethodData['paymentMethod'] = 'Credit Card';
            $paymentMethodData['reference'] = $paymentData['incomingPaymentMethodCreditReceiptNumber'];
        } else if (!empty(intval($paymentData['incomingPaymentMethodBankTransferId']))) {
            $paymentMethodData['paymentMethod'] = 'Bank Transfer';
            $paymentMethodData['reference'] = (!empty(intval($paymentData['incomingPaymentMethodBankTransferCustomerAccountNumber']))) ? $paymentData['incomingPaymentMethodBankTransferCustomerAccountNumber'] : '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodGiftCardId']))) {
            $paymentMethodData['paymentMethod'] = 'Gift Card';
            $paymentMethodData['reference'] = $paymentData['giftCardId'];
        } else if (!empty(intval($paymentData['incomingPaymentMethodLCId']))) {
            $paymentMethodData['paymentMethod'] = 'LC';
            $paymentMethodData['reference'] = '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodTTId']))) {
            $paymentMethodData['paymentMethod'] = 'TT';
            $paymentMethodData['reference'] = '-';
        } else if (!empty(intval($paymentData['incomingPaymentMethodLoyaltyCardId']))) {
            if (is_null($paymentData['incomingPaymentMethodCashId']) && is_null($paymentData['incomingPaymentMethodChequeId']) && is_null($paymentData['incomingPaymentMethodCreditCardId']) && is_null($paymentData['incomingPaymentMethodGiftCardId']) && is_null($paymentData['incomingPaymentMethodLCId'])) {
                $paymentMethodData['paymentMethod'] = 'Loyalty Card';
                $paymentMethodData['reference'] = '-';
            }
        }
        return $paymentMethodData;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return return HTML content
     */
    public function cardTypePaymentReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cardTypeIds = $request->getPost('cardTypeIds');

            $cardTypePaymentData = $this->_getCardTypePayments($fromDate, $toDate, $cardTypeIds);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Card Type Payment Details Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $cardTypePaymentView = new ViewModel(array(
                'cD' => $companyDetails,
                'cardTypePaymentData' => $cardTypePaymentData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $cardTypePaymentView->setTemplate('reporting/sales-payment-report/generate-card-type-payments-details-report');

            $this->html = $cardTypePaymentView;
            $this->status = true;
        }

        return $this->JSONRespondHtml();
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return Given PDF path which created
     */
    public function cardTypePaymentReportPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cardTypeIds = $request->getPost('cardTypeIds');

            $cardTypePaymentData = $this->_getCardTypePayments($fromDate, $toDate, $cardTypeIds);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Card Type Payment Details Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $cardTypePaymentView = new ViewModel(array(
                'cD' => $companyDetails,
                'cardTypePaymentData' => $cardTypePaymentData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $cardTypePaymentView->setTemplate('reporting/sales-payment-report/generate-card-type-payments-details-report');
            $cardTypePaymentView->setTerminal(TRUE);

            $htmlContent = $this->viewRendererHtmlContent($cardTypePaymentView);
            $pdfPath = $this->downloadPDF('card-type-payment-details-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
        }

        return $this->JSONRespond();
    }

    public function cardTypePaymentReportCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cardTypeIds = $request->getPost('cardTypeIds');

            $cardTypePaymentData = $this->_getCardTypePayments($fromDate, $toDate, $cardTypeIds);
            $companyDetails = $this->getCompanyDetails();

            if ($cardTypePaymentData) {
                $data = array(
                    "Card Type Payment Details Report"
                );
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = array(
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    "Period :" . $fromDate . " - " . $toDate
                );
                $title.=implode(",", $data) . "\n";

                $tableHead = array('Payment Number', 'Payment Date', 'Card Type Name', 'Payed Amount');
                $header.=implode(",", $tableHead) . "\n";

                foreach ($cardTypePaymentData as $payment) {
                    $data = [
                        $payment['paymentCode'],
                        $payment['paidDate'],
                        $payment['cardTypeName'],
                        $payment['currencySymbol'] . $payment['paidAmount']
                    ];
                    $arr.=implode(",", $data) . "\n";
                }
            } else {
                $arr = "no matching records found";
            }
            $name = "card-type-payment-details-report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);
            $this->data = $csvPath;
            $this->status = true;
        }
        return $this->JSONRespond();
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @param array $cardTypeIds
     * @return array card type payment data
     */
    private function _getCardTypePayments($fromDate, $toDate, $cardTypeIds)
    {
        $cardTypePayment = $this->CommonTable("Invoice\Model\InvoicePaymentsTable")->getCardTypePaymentDetails($fromDate, $toDate, $cardTypeIds);
        $cardTypePaymentData = [];
        foreach ($cardTypePayment as $c) {
            $cardTypePaymentData[$c['incomingInvoicePaymentID']] = [
                'paymentCode' => $c['incomingPaymentCode'],
                'cardTypeName' => $c['cardTypeName'],
                'paidDate' => $c['incomingPaymentDate'],
                'paidAmount' => $c['incomingPaymentAmount'],
                'currencySymbol' => $c['currencySymbol']
            ];
        }

        return $cardTypePaymentData;
    }

    private function modifyDataForCategorizedPaymentsSummeryDetails($data)
    {
        $respond = [];
        foreach ($data as $key => $value) {
            foreach ($value as $k => $v) {
                $respond[$v['paymentMethodName']][$key][$k] = $v;

                if ($v['paymentMethodName'] == "Credit") {
                    $respond[$v['paymentMethodName']][$key]['incomingPaymentMethodWiseAmount'] += $v['incomingPaymentCreditAmount'];
                } else {
                    $respond[$v['paymentMethodName']][$key]['incomingPaymentMethodWiseAmount'] += $v['incomingPaymentMethodAmount'];
                }
            }
        }
        return $respond;
    }

}
