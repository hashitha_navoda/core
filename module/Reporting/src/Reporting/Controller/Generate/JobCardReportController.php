<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains action methods for the payment voucher report controller
 */

namespace Reporting\Controller\Generate;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;

class JobCardReportController extends CoreController
{

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get jobcard related invoice details
     * @param type $activityId
     * @param type $isAllSelect
     * @return array $invoiceValueData
     */
    private function _getInvoiceValueDetails($activityId = NULL, $jobId = NULL, $projectId = NULL, $isAllSelect, $typeId)
    {
        $isAllSelect = filter_var($isAllSelect, FILTER_VALIDATE_BOOLEAN);

        if ((isset($activityId) || isset($jobId) || isset($projectId)) || $isAllSelect) {
            $invoiceValueData = array();

            if ($activityId != NULL) {
                $invoiceValueData = array_fill_keys($activityId, '');
            } else if ($jobId != NULL) {
                $invoiceValueData = array_fill_keys($jobId, '');
            } else if ($projectId != NULL) {
                $invoiceValueData = array_fill_keys($projectId, '');
            }

            $jobCardInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByJobCardRelatedId($activityId, $jobId, $projectId, $typeId);
            if ($jobCardInvoiceData != NULL) {

                foreach ($jobCardInvoiceData as $j) {

                    $relatedJobCardCode = $this->_getRelatedJobCardCode($j)['jobCardCode'];
                    $relatedJobCardId = $this->_getRelatedJobCardId($j);

                    $invoiceValueData[$relatedJobCardId]['jD'][$j['salesInvoiceID']] = array(
                        'referenceCode' => $relatedJobCardCode,
                        'invCode' => $j['salesInvoiceCode'],
                        'invTotalValue' => $j['invValueTotal']);
                }
            }
            return array_filter($invoiceValueData);
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get job card related reference number(it may be activity,job or project code)
     * @param array $data
     * @return $jobCardCode
     */
    private function _getRelatedJobCardCode($data = NULL, $type = NULL)
    {
        $jobCardCode = array('jobCardCode', 'jobCardType');

        if (isset($data['activityCode']) && $type == 'activityNo') {
            $jobCardCode = array('jobCardCode' => $data['activityCode'], 'jobCardType' => 'activity');
        } else if (isset($data['jobReferenceNumber']) && $type == 'jobNo') {
            $jobCardCode = array('jobCardCode' => $data['jobReferenceNumber'], 'jobCardType' => 'job');
        } else if (isset($data['projectCode']) && $type == 'projectNo') {
            $jobCardCode = array('jobCardCode' => $data['projectCode'], 'jobCardType' => 'project');
        }

        return $jobCardCode;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get job card related reference number(it may be activity,job or project code)
     * @param array $data
     * @return $jobCardCode
     */
    private function _getRelatedJobCardId($data = NULL, $type = NULL)
    {
        $jobCardId = NULL;

        if (isset($data['activityID']) && $type == 'activityNo') {
            $jobCardId = $data['activityID'];
        } else if (isset($data['jobID']) && $type == 'jobNo') {
            $jobCardId = $data['jobID'];
        } else if (isset($data['projectID']) && $type == 'projectNo') {
            $jobCardId = $data['projectID'];
        }

        return $jobCardId;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * view invoice value report related to job card
     * @return JSONRespondHtml
     */
    public function viewJobCardInvoiceValueAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $translator = new Translator();
            $name = $translator->translate('Project/ Job/ Activity and Invoice Report');

            $invoiceRelatedData = $this->_getInvoiceValueDetails($activityId, $jobId, $projectId, $isAllSelect, $typeId);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $invValueView = new ViewModel(array(
                'cD' => $companyDetails,
                'invData' => $invoiceRelatedData,
                'headerTemplate' => $headerViewRender,
            ));
            $invValueView->setTemplate('reporting/job-card-report/generate-invoice-value-pdf');

            $this->html = $invValueView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * getting pdf from invoice data related to job card action
     * @return JSONRespond
     */
    public function generateJobCardInvoiceValuePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $translator = new Translator();
            $name = $translator->translate('Project/ Job/ Activity and Invoice Report');

            $invoiceRelatedData = $this->_getInvoiceValueDetails($activityId, $jobId, $projectId, $isAllSelect, $typeId);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $invValueView = new ViewModel(array(
                'cD' => $companyDetails,
                'invData' => $invoiceRelatedData,
                'headerTemplate' => $headerViewRender,
            ));
            $invValueView->setTemplate('reporting/job-card-report/generate-invoice-value-pdf');
            $invValueView->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($invValueView);
            $pdfPath = $this->downloadPDF('invoice-value', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * return CSV path
     * @return JSONRespond
     */
    public function generateJobCardInvoiceValueSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $invoiceRelatedData = $this->_getInvoiceValueDetails($activityId, $jobId, $projectId, $isAllSelect, $typeId);
            $companyDetails = $this->getCompanyDetails();

            if (!empty($invoiceRelatedData)) {
                $title = '';
                $tit = 'PROJECT/ JOB/ ACTIVITY AND INVOICE REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                );
                $title = implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $title .= $companyDetails[0]->companyName . "\n";
                $title .= $companyDetails[0]->companyAddress . "\n";
                $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

                $in = array(
                    0 => ""
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("Activity/ Job/ Project ID", "Invoice Code", "Total Invoice Value");
                $header = implode(",", $arrs);
                $arr = '';
                if (!empty($invoiceRelatedData)) {
                    foreach ($invoiceRelatedData as $key => $value) {
                        foreach ($value['jD'] as $v) {
                            $in = array(
                                0 => $v['referenceCode'],
                                1 => $v['invCode'],
                                2 => $v['invTotalValue'],
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                } else {
                    $arr = "No related data found\n";
                }
            }
            $name = "job_card_invoice_value";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $userId
     * @param array $customerId
     * @param array $dutyManagerId
     * @param Date $fromDate
     * @param Date $toDate
     * @return array $inquiryLogData
     */
    private function _getInquiryLogDetails($userId = NULL, $customerId = NULL, $dutyManagerId = NULL, $fromDate = NULL, $toDate = NULL)
    {
        $getInquiryLog = $this->CommonTable("JobCard\Model\InquiryLogTable")->getInquiryLogDetails($inquiryLogID = NULL, $userId, $customerId, $dutyManagerId, $fromDate, $toDate);

        $inquiryLogData = array();
        foreach ($getInquiryLog as $value) {
            $value['time'] = $this->getUserDateTime($value['inquiryLogDateAndTime']);
            $inquiryLogData[$value['inquiryLogID']]['data']['inquiry'] = $value;
            $inquiryLogData[$value['inquiryLogID']]['data']['dM'][$value['inquiryLogDutyManagerID']] = array(
                'employeeFirstName' => $value['employeeFirstName'],
                'employeeSecondName' => $value['employeeSecondName'],
            );
        }

        return $inquiryLogData;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * view inquiry log data according to passing data of user
     * @return JSONRespondHtml
     */
    public function viewInquiryLogAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $userId = $request->getPost('userId');
            $customerId = $request->getPost('customerId');
            $dutyManagerId = $request->getPost('dutyManagerId');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $translator = new Translator();
            $name = $translator->translate('Inquiry Log Report');

            $inquiryLogData = $this->_getInquiryLogDetails($userId, $customerId, $dutyManagerId, $fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $inquiryLogView = new ViewModel(array(
                'cD' => $companyDetails,
                'iLogData' => $inquiryLogData,
                'headerTemplate' => $headerViewRender,
            ));
            $inquiryLogView->setTemplate('reporting/job-card-report/generate-inquiry-log-pdf');

            $this->html = $inquiryLogView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * generate inquiry log data pdf according to passing data of user
     * @return JSONRespondHtml
     */
    public function generateInquiryLogPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $userId = $request->getPost('userId');
            $customerId = $request->getPost('customerId');
            $dutyManagerId = $request->getPost('dutyManagerId');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $translator = new Translator();
            $name = $translator->translate('Inquiry Log Report');

            $inquiryLogData = $this->_getInquiryLogDetails($userId, $customerId, $dutyManagerId, $fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $inquiryLogView = new ViewModel(array(
                'cD' => $companyDetails,
                'iLogData' => $inquiryLogData,
                'headerTemplate' => $headerViewRender,
            ));
            $inquiryLogView->setTemplate('reporting/job-card-report/generate-inquiry-log-pdf');
            $inquiryLogView->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($inquiryLogView);
            $pdfPath = $this->downloadPDF('inquiry-log', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * return CSV sheet path
     * @return JSONRespond
     */
    public function generateInquiryLogSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $userId = $request->getPost('userId');
            $customerId = $request->getPost('customerId');
            $dutyManagerId = $request->getPost('dutyManagerId');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $inquiryLogData = $this->_getInquiryLogDetails($userId, $customerId, $dutyManagerId, $fromDate, $toDate);
            $companyDetails = $this->getCompanyDetails();

            $title = '';
            $tit = 'INQUIRY LOG REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("INQUIRY ID", "DATE / TIME", "CUSTOMER NAME", "CREATED USER", "DUTY MANAGER");
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($inquiryLogData)) {
                foreach ($inquiryLogData as $value['data']) {
                    foreach ($value['data'] as $v) {
                        $in = array(
                            0 => $v['inquiry']['inquiryLogReference'],
                            1 => $v['inquiry']['time'],
                            2 => $v['inquiry']['customerName'],
                            3 => $v['inquiry']['userUsername'],
                        );

                        if (!empty($v['dM'])) {
                            $i = 1;
                            foreach ($v['dM'] as $value) {
                                if ($i == 1) {
                                    array_push($in, $value['employeeFirstName'] . '-' . $value['employeeSecondName']);
                                    $arr.=implode(",", $in) . "\n";
                                } else {
                                    $in = array(
                                        0 => '',
                                        1 => '',
                                        2 => '',
                                        3 => '',
                                        4 => $value['employeeFirstName'] . '-' . $value['employeeSecondName'],
                                    );
                                    $arr.=implode(",", $in) . "\n";
                                }
                                $i++;
                            }//dM end foreach
                        }//if dM
                    }
                }//data foreach
            } else {
                $arr = "No related data found\n";
            }

            $name = "job_card_inquiry_log";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get data of weightage from job and activity table according to passing variable
     * @param array $activityId
     * @param array $jobId
     * @param array $projectId
     * @param array $typeId
     * @param array $fromDate
     * @param array $toDate
     * @param boolean $isAllSelect
     * @return array $weightageData
     */
    protected function _getWeightageDetails($data)
    {
        $isAllSelect = filter_var($data['isAllSelect'], FILTER_VALIDATE_BOOLEAN);

        if ((isset($data['activityId']) || isset($data['jobId']) || isset($data['projectId'])) || $isAllSelect) {
            $getWeightageData = $this->CommonTable('JobCard\Model\ProjectTable')->getWeightData($data);

            $weightageProjectData = [];
            $weightageActivityData = [];
            foreach ($getWeightageData as $w) {

                if (isset($w['projectId'])) {

                    $weightageProjectData[$w['projectId']][$w['activityId']] = array_merge($w, array('time' => $this->getUserDateTime($w['createdTimeStamp'])));
                } else {

                    $weightageActivityData[$w['activityId']] = array_merge($w, array('time' => $this->getUserDateTime($w['createdTimeStamp'])));
                }
            }

            $weightageData = $weightageProjectData + $weightageActivityData;

            return $weightageData;
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * view weightage
     * @return JSONRespondHtml
     */
    public function viewWeightageAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId,
                'fromDate' => $fromDate,
                'toDate' => $toDate
            );

            $weightageData = $this->_getWeightageDetails($data);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('Activity/ Job Weightage Report');

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $invValueView = new ViewModel(array(
                'cD' => $companyDetails,
                'weightData' => $weightageData,
                'headerTemplate' => $headerViewRender,
            ));
            $invValueView->setTemplate('reporting/job-card-report/generate-activity-job-weightage-pdf');

            $this->html = $invValueView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * return PDF path
     * @return JSONRespond
     */
    public function generateWeightagePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId,
                'fromDate' => $fromDate,
                'toDate' => $toDate
            );

            $weightageData = $this->_getWeightageDetails($data);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('Activity/ Job Weightage Report');

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $invValueView = new ViewModel(array(
                'cD' => $companyDetails,
                'weightData' => $weightageData,
                'headerTemplate' => $headerViewRender,
            ));
            $invValueView->setTemplate('reporting/job-card-report/generate-activity-job-weightage-pdf');
            $invValueView->setTerminal(TRUE);

            $htmlContent = $this->viewRendererHtmlContent($invValueView);
            $pdfPath = $this->downloadPDF('activity-job-weightage', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateWeightageSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId,
                'fromDate' => $fromDate,
                'toDate' => $toDate
            );

            $weightData = $this->_getWeightageDetails($data);
            $companyDetails = $this->getCompanyDetails();

            if (!empty($weightData)) {
                $title = '';
                $tit = 'ACTIVITY / JOB WEIGHTAGE REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                );
                $title = implode(",", $in) . "\n";

                $title .= $companyDetails[0]->companyName . "\n";
                $title .= $companyDetails[0]->companyAddress . "\n";
                $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

                $in = array(
                    0 => ""
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("DATE / TIME", "CUSTOMER NAME", "PROJECT ID", "JOB ID & WEIGHT", "ACTIVITY ID & WEIGHT");
                $header = implode(",", $arrs);
                $arr = '';
                if (!empty($weightData)) {
                    foreach ($weightData as $value) {
                        $activityWeight1 = '';
                        $activityCode1 = (isset($value['activityCode'])) ? $value['activityCode'] : NULL;
                        if (isset($activityCode1) && isset($value['activityWeight'])) {
                            $activityWeight1 = number_format($value['activityWeight'], 2, '.', '');
                        } else if (isset($activityCode1) && $value['activityWeight'] == 0) {
                            $activityWeight1 = number_format(0, 2, '.', '');
                        } else if (!isset($activityCode1) && $value['activityWeight'] == 0) {
                            $activityWeight1 = '';
                        }

                        $customerName1 = (isset($value['customerName'])) ? $value['customerName'] : '-';
                        $jobCode1 = (isset($value['jobReferenceNumber'])) ? $value['jobReferenceNumber'] : NULL;
                        $jobWeight1 = '';
                        if (isset($jobCode1) && isset($value['jobWeight'])) {
                            $jobWeight1 = number_format($value['jobWeight'], 2, '.', '');
                        } else if (isset($jobCode1) && $value['jobWeight'] == 0) {
                            $jobWeight1 = number_format(0, 2, '.', '');
                        } else if (!isset($jobCode1) && $value['jobWeight'] == 0) {
                            $jobWeight1 = '';
                        }
                        foreach ($value as $v) {
                            if (isset($v['projectId'])) {
                                $customerName = (isset($v['customerName'])) ? $v['customerName'] : '-';
                                $activityCode = (isset($v['activityCode'])) ? $v['activityCode'] : NULL;
                                $activityWeight = '';
                                if (isset($activityCode) && isset($v['activityWeight'])) {
                                    $activityWeight = number_format($v['activityWeight'], 2, '.', '');
                                } else if (isset($activityCode) && $v['activityWeight'] == 0) {
                                    $activityWeight = number_format(0, 2, '.', '');
                                } else if (!isset($activityCode) && $v['activityWeight'] == 0) {
                                    $activityWeight = '';
                                }

                                $jobCode = (isset($v['jobReferenceNumber'])) ? $v['jobReferenceNumber'] : NULL;
                                $jobWeight = '';
                                if (isset($jobCode) && isset($v['jobWeight'])) {
                                    $jobWeight = number_format($v['jobWeight'], 2, '.', '');
                                } else if (isset($jobCode) && $v['jobWeight'] == 0) {
                                    $jobWeight = number_format(0, 2, '.', '');
                                } else if (!isset($jobCode) && $v['jobWeight'] == 0) {
                                    $jobWeight = '';
                                }
                                $in = array(
                                    0 => $v['time'],
                                    1 => $v['customerTitle'] . ' ' . $customerName,
                                    2 => (isset($v['projectCode'])) ? $v['projectCode'] : '-',
                                    3 => $jobCode . ' - ' . $jobWeight,
                                    4 => $v['activityCode'] . '-' . number_format($activityWeight, 2, '.', ''),
                                );
                                $arr.=implode(",", $in) . "\n";
                            }
                        }
                        if (!isset($v['projectId'])) {
                            $in = array(
                                0 => $value['time'],
                                1 => $value['customerTitle'] . ' ' . $customerName1,
                                2 => '-',
                                3 => $jobCode1 . ' - ' . $jobWeight1,
                                4 => $value['activityCode'] . '-' . number_format($activityWeight1, 2, '.', ''),
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                } else {
                    $arr = "No related data found\n";
                }

                $name = "activity_job_weightage";
                $csvContent = $this->csvContent($title, $header, $arr);
                $csvPath = $this->generateCSVFile($name, $csvContent);

                $this->data = $csvPath;
                $this->status = true;
                return $this->JSONRespond();
            }
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * return progress related data according to passing vaiables
     * @param array $data
     * @return array $progressResultset
     */
    protected function _getJobCardProgressDetails($data)
    {
        $isAllSelect = filter_var($data['isAllSelect'], FILTER_VALIDATE_BOOLEAN);

        if ((isset($data['activityId']) || isset($data['jobId']) || isset($data['projectId']) || isset($data['customerId'])) || $isAllSelect) {
            $getJobCardProgress = $this->CommonTable('JobCard\Model\ProjectTable')->getProgressData($data);

            $progressResultset = [];
            if (!empty($data['customerId'])) {

                foreach ($getJobCardProgress as $v) {
                    $progressResultset[] = $v;
                }
            } else {

                foreach ($getJobCardProgress as $value) {
                    $progressResultset[] = $value;
                }
            }

            return $progressResultset;
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * return pdf with progress related data according to passing vaiables
     * @return JSONRespondHtml
     */
    public function viewJobCardProgressAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $customerId = $request->getPost('customerId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Activity/ Job/ Project Progress Report');

            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'customerId' => $customerId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId,
            );

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $progressData = $this->_getJobCardProgressDetails($data);

            if (!empty($customerId)) {
                $progressView = new ViewModel(array(
                    'cD' => $companyDetails,
                    'progressData' => $progressData,
                    'headerTemplate' => $headerViewRender,
                ));
                $progressView->setTemplate('reporting/job-card-report/generate-customer-wise-progress-pdf');
            } else {
                $progressView = new ViewModel(array(
                    'cD' => $companyDetails,
                    'progressData' => $progressData,
                    'headerTemplate' => $headerViewRender,
                ));
                $progressView->setTemplate('reporting/job-card-report/generate-progress-pdf');
            }

            $this->html = $progressView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get PDF file related to Jobcard progress
     * @return JSONRespond with PDF file url
     */
    public function generateJobCardProgressPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $customerId = $request->getPost('customerId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Activity/ Job/ Project Progress Report');

            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'customerId' => $customerId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId,
            );

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $progressData = $this->_getJobCardProgressDetails($data);

            if (!empty($customerId)) {
                $progressView = new ViewModel(array(
                    'cD' => $companyDetails,
                    'progressData' => $progressData,
                    'headerTemplate' => $headerViewRender,
                ));
                $progressView->setTemplate('reporting/job-card-report/generate-customer-wise-progress-pdf');
            } else {
                $progressView = new ViewModel(array(
                    'cD' => $companyDetails,
                    'progressData' => $progressData,
                    'headerTemplate' => $headerViewRender,
                ));
                $progressView->setTemplate('reporting/job-card-report/generate-progress-pdf');
            }
            $progressView->setTerminal(TRUE);

            $htmlContent = $this->viewRendererHtmlContent($progressView);
            $pdfPath = $this->downloadPDF('activity-job-weightage', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get CSV file related to Jobcard progress
     * @return JSONRespond with CSV file url
     */
    public function generateJobCardProgressSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $customerId = $request->getPost('customerId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'customerId' => $customerId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId,
            );

            $progressData = $this->_getJobCardProgressDetails($data);
            $companyDetails = $this->getCompanyDetails();

            if (!empty($customerId)) {
                if (!empty($progressData)) {
                    $title = '';
                    $tit = 'ACTIVITY / JOB / PROJECT PROGRESS REPORT';
                    $in = array(
                        0 => "",
                        1 => $tit,
                        2 => "",
                    );
                    $title = implode(",", $in) . "\n";

                    $title .= $companyDetails[0]->companyName . "\n";
                    $title .= $companyDetails[0]->companyAddress . "\n";
                    $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

                    $in = array(
                        0 => ""
                    );
                    $title.=implode(",", $in) . "\n";

                    $arrs = array("DATE / TIME", "CUSTOMER NAME", "PROJECT CODE - PROGRESS", "JOB CODE - PROGRESS", "ACTIVITY - PROGRESS");
                    $header = implode(",", $arrs);
                    $arr = '';
                    foreach ($progressData as $p) {
                        $in = array(
                            0 => $p['createdTimeStamp'],
                            1 => $p['customerName'],
                            2 => $p['projectCode'] . '-' . $p['projectProgress'],
                            3 => $p['jobReferenceNumber'] . '-' . $p['jobProgress'],
                            4 => $p['activityCode'] . '-' . $p['activityProgress'],
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                } else {
                    $arr = "No related data found\n";
                }
            } else {
                if (!empty($progressData)) {
                    $title = '';
                    $tit = 'ACTIVITY / JOB / PROJECT PROGRESS REPORT';
                    $in = array(
                        0 => "",
                        1 => $tit,
                        2 => "",
                    );
                    $title = implode(",", $in) . "\n";

                    $title .= $companyDetails[0]->companyName . "\n";
                    $title .= $companyDetails[0]->companyAddress . "\n";
                    $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

                    $in = array(
                        0 => ""
                    );
                    $title.=implode(",", $in) . "\n";

                    $arrs = array("DATE / TIME", "CUSTOMER NAME", "REF. NO", "PROGRESS");
                    $header = implode(",", $arrs);
                    $arr = '';
                    foreach ($progressData as $p) {
                        $in = array(
                            0 => $p['createdTimeStamp'],
                            1 => ($p['customerID']) ? $p['customerName'] : '-',
                            2 => $p['refCode'],
                            3 => ($p['progress']) ? $p['progress'] : '0.00',
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                } else {
                    $arr = "No related data found\n";
                }
            }

            $name = "jobcard_progress";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get job card related reference name(it may be activity,job or project code)
     * @param array $data
     * @return $jobCardCode
     */
    private function _getRelatedJobCardRefName($data = NULL, $type = NULL)
    {

        $jobCardRefName = NULL;

        if (isset($data['activityName']) && $type == 'activityNo') {
            $jobCardRefName = $data['activityName'];
        } else if (isset($data['jobName']) && $type == 'jobNo') {
            $jobCardRefName = $data['jobName'];
        } else if (isset($data['projectName']) && $type == 'projectNo') {
            $jobCardRefName = $data['projectName'];
        }

        return $jobCardRefName;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get job card costing details
     * @param array $activityId
     * @param array $jobId
     * @param array $projectId
     * @param boolean $isAllSelect
     * @param string $typeId
     * @return array $costingData
     */
    private function _getJobCardCosting($activityId = NULL, $jobId = NULL, $projectId = NULL, $isAllSelect, $typeId)
    {
        $isAllSelect = filter_var($isAllSelect, FILTER_VALIDATE_BOOLEAN);

        if ((isset($activityId) || isset($jobId) || isset($projectId)) || $isAllSelect) {
            $costingData = array();

            if ($activityId != NULL) {
                $costingData = array_fill_keys($activityId, '');
            } else if ($jobId != NULL) {
                $costingData = array_fill_keys($jobId, '');
            } else if ($projectId != NULL) {
                $costingData = array_fill_keys($projectId, '');
            }
            //cost type data
            //That is mean none inventory data
            //I have used following method to get none inventory item data which used on activity
            //because of none inventory data does not save in itemIn and itemOut table
            $nonInventroyData = $this->_getNonInventroyProducts($activityId, $jobId, $projectId, $typeId);

            $jobCardCostingData = $this->CommonTable('Invoice\Model\InvoiceTable')->getCostingDataByJobCardRefId($activityId, $jobId, $projectId, $typeId);
            //if their have inventory items
            if ($jobCardCostingData != NULL) {

                foreach ($jobCardCostingData as $j) {

                    $relatedJobCardCode = $this->_getRelatedJobCardCode($j, $typeId);
                    $relatedJobCardId = $this->_getRelatedJobCardId($j, $typeId);
                    $relatedJobCardRefName = $this->_getRelatedJobCardRefName($j, $typeId);

                    $costingData[$relatedJobCardId]['referenceCode'] = $relatedJobCardCode['jobCardCode'];
                    $costingData[$relatedJobCardId]['referenceName'] = $relatedJobCardRefName;

                    //set quantity and uomAbbr according to display UOM
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($j['productID']);
                    $thisqty = $this->getProductQuantityViaDisplayUom($j['itemOutQty'], $productUom);
                    $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                    $itemInPrice = $this->getProductUnitPriceViaDisplayUom($j['itemInPrice'], $productUom);
                    $itemOutPrice = $this->getProductUnitPriceViaDisplayUom($j['itemOutPrice'], $productUom);

                    $costingData[$relatedJobCardId]['jD'][$j['itemOutLocationProductID']] = array(
                        'referenceCode' => $relatedJobCardCode['jobCardCode'],
                        'referenceName' => $relatedJobCardRefName,
                        'productName' => $j['productName'],
                        'productCode' => $j['productCode'],
                        'invCode' => $j['salesInvoiceCode'],
                        'invTotalValue' => $j['invValueTotal'],
                        'itemInPrice' => $itemInPrice,
                        'itemOutPrice' => $itemOutPrice,
                        'itemOutQty' => $j['itemOutQty'],
                        'productType' => 1
                    );
                }
                //if there have none inventory items
                //those items put into $costingData
                //If it is same jobCard reference ID(projectID,JobID,activityID)
                //then adds into array as new element using $nonInvetoryData locationProductID
                if ($nonInventroyData != NULL) {

                    foreach ($costingData as $invRefID => $invJobCardData) {
                        foreach ($invJobCardData['jD'] as $invProID => $invData) {

                            foreach ($nonInventroyData as $nonInvRefID => $nonInvJobData) {
                                foreach ($nonInvJobData['jD'] as $nonInvProID => $value) {

                                    if ($invRefID == $nonInvRefID) {
                                        $costingData[$invRefID]['jD'][$nonInvProID] = $value;
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            } else {
                //if inventory data have not in regarded activity
                //so we have only none inventory data
                $costingData = $nonInventroyData;
            }

            return array_filter($costingData);
        }
    }

    protected function _getNonInventroyProducts($activityId = NULL, $jobId = NULL, $projectId = NULL, $jobCardType = NULL)
    {
        $nonInventoryData = [];
        if (isset($jobCardType)) {
            $nonInventroyProductData = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoiceDetailsByInvoiceID($activityId, $jobId, $projectId, $jobCardType);
            foreach ($nonInventroyProductData as $n) {
                $itemInPrice = 0;
                $estimateCost = isset($n['estimateCost']) ? $n['estimateCost'] : 0;
                $actualCost = isset($n['actualCost']) ? $n['actualCost'] : 0;

                if ($actualCost > 0) {
                    $itemInPrice = $actualCost;
                } else if ($estimateCost > 0) {
                    $itemInPrice = $estimateCost;
                } else {
                    $itemInPrice = 0;
                }

                $relatedJobCardId = $this->_getRelatedJobCardId($n, $jobCardType);
                $nonInventoryData[$relatedJobCardId]['referenceCode'] = $n['referenceCode'];
                $nonInventoryData[$relatedJobCardId]['referenceName'] = $n['referenceName'];
                $nonInventoryData[$relatedJobCardId]['jD'][$n['locationProductID']] = array(
                    'referenceCode' => $n['referenceCode'],
                    'referenceName' => $n['referenceName'],
                    'productName' => $n['productName'],
                    'productCode' => $n['productCode'],
                    'invCode' => $n['salesInvoiceCode'],
                    'invTotalValue' => $n['salesInvoiceProductTotal'],
                    'itemInPrice' => $itemInPrice,
                    'itemOutPrice' => $n['salesInvoiceProductPrice'],
                    'itemOutQty' => 1,
                    'productType' => 2
                );
            }
        }

        return $nonInventoryData;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * view job card costing report with invoice data
     * @return JSONRespondHtml
     */
    public function viewJobCardCostingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $translator = new Translator();
            $name = $translator->translate('Project/ Job/ Activity Costing Report');

            $costingData = $this->_getJobCardCosting($activityId, $jobId, $projectId, $isAllSelect, $typeId);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $jobCardCostingView = new ViewModel(array(
                'cD' => $companyDetails,
                'costingData' => $costingData,
                'headerTemplate' => $headerViewRender,
            ));
            $jobCardCostingView->setTemplate('reporting/job-card-report/generate-job-card-costing-pdf');

            $this->html = $jobCardCostingView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * return pdf path
     * @return JSONRespond
     */
    public function generateJobCardCostingPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $translator = new Translator();
            $name = $translator->translate('Project/ Job/ Activity Costing Report');

            $costingData = $this->_getJobCardCosting($activityId, $jobId, $projectId, $isAllSelect, $typeId);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $jobCardCostingView = new ViewModel(array(
                'cD' => $companyDetails,
                'costingData' => $costingData,
                'headerTemplate' => $headerViewRender,
            ));
            $jobCardCostingView->setTemplate('reporting/job-card-report/generate-job-card-costing-pdf');
            $jobCardCostingView->setTerminal(TRUE);

            $htmlContent = $this->viewRendererHtmlContent($jobCardCostingView);
            $pdfPath = $this->downloadPDF('jobcard-costing', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * return CSV sheet path
     * @return JSONRespond
     */
    public function generateJobCardCostingSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $costingData = $this->_getJobCardCosting($activityId, $jobId, $projectId, $isAllSelect, $typeId);
            $companyDetails = $this->getCompanyDetails();

            $title = '';
            $tit = 'PROJECT/ JOB/ ACTIVITY COSTING REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("", "PROJECT/JOB/ACTIVITY ID", "NAME", "TOTAL COST", "TOTAL INVOICE VALUE", "GROSS PROFIT");
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($costingData)) {
                $i = 1;
                foreach ($costingData as $value) {
                    $totalCostValue = 0;
                    $totalInvoiceValue = 0;
                    foreach ($value['jD'] as $c) {
                        $itemInPrice = $c['itemInPrice'] ? $c['itemInPrice'] : 0;
                        $itemOutPrice = $c['itemOutPrice'] ? $c['itemOutPrice'] : 0;
                        $itemOutQty = $c['itemOutQty'] ? $c['itemOutQty'] : 0;
                        $invTotalValue = $c['invTotalValue'] ? $c['invTotalValue'] : 0;
                        $costValue = $itemOutQty * $itemInPrice;
                        $totalCostValue += $costValue;
                        $invoiceValue = $itemOutQty * $itemOutPrice;
                        $totalInvoiceValue += $invoiceValue;
                    }
                    $in = array(
                        0 => $i,
                        1 => $c['referenceCode'],
                        2 => $c['referenceName'],
                        3 => $totalCostValue,
                        4 => $totalInvoiceValue,
                        5 => $totalInvoiceValue - $totalCostValue,
                    );
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
            } else {
                $arr = "No related data found\n";
            }

            $name = "job_card_costing";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * view details of sales invoice done by jobcard
     * @return JSONRespondHtml
     */
    public function viewJobCardDetailCostingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $translator = new Translator();
            $name = $translator->translate('Project/ Job/ Activity Detail Costing Report');

            $costingData = $this->_getJobCardCosting($activityId, $jobId, $projectId, $isAllSelect, $typeId);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $jobCardCostingView = new ViewModel(array(
                'cD' => $companyDetails,
                'costingData' => $costingData,
                'headerTemplate' => $headerViewRender,
            ));
            $jobCardCostingView->setTemplate('reporting/job-card-report/generate-job-card-detail-costing-pdf');

            $this->html = $jobCardCostingView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * return pdf path
     * @return JSONRespond
     */
    public function generateJobCardDetailCostingPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $translator = new Translator();
            $name = $translator->translate('Project/ Job/ Activity Detail Costing Report');

            $costingData = $this->_getJobCardCosting($activityId, $jobId, $projectId, $isAllSelect, $typeId);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $jobCardCostingView = new ViewModel(array(
                'cD' => $companyDetails,
                'costingData' => $costingData,
                'headerTemplate' => $headerViewRender,
            ));
            $jobCardCostingView->setTemplate('reporting/job-card-report/generate-job-card-detail-costing-pdf');
            $jobCardCostingView->setTerminal(TRUE);

            $htmlContent = $this->viewRendererHtmlContent($jobCardCostingView);
            $pdfPath = $this->downloadPDF('jobcard-costing', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * return CSV sheet path
     * @return JSONRespond
     */
    public function generateJobCardDetailCostingSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');

            $costingData = $this->_getJobCardCosting($activityId, $jobId, $projectId, $isAllSelect, $typeId);
            $companyDetails = $this->getCompanyDetails();

            $title = '';
            $tit = 'PROJECT/ JOB/ ACTIVITY DETAIL COSTING REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("", "PROJECT/JOB/ACTIVITY ID", "NAME", "PRODUCT NAME - CODE", "QUANTITY", "ITEM IN PRICE", "TOTAL COST", "ITEM OUT PRICE", "TOTAL INVOICE VALUE", "GROSS PROFIT");
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($costingData)) {
                $i = 1;
                foreach ($costingData as $value) {

                    $totalCostValue = 0;
                    $totalInvoiceValue = 0;
                    $in = array(
                        0 => $i,
                        1 => $value['referenceCode'],
                        2 => $value['referenceName']
                    );
                    $arr.=implode(",", $in) . "\n";
                    foreach ($value['jD'] as $c) {

                        $itemInPrice = $c['itemInPrice'] ? $c['itemInPrice'] : 0;
                        $itemOutPrice = $c['itemOutPrice'] ? $c['itemOutPrice'] : 0;
                        $itemOutQty = $c['itemOutQty'] ? $c['itemOutQty'] : 0;
                        $invTotalValue = $c['invTotalValue'] ? $c['invTotalValue'] : 0;
                        $costValue = $itemOutQty * $itemInPrice;
                        $totalCostValue += $costValue;
                        $invoiceValue = $itemOutQty * $itemOutPrice;
                        $totalInvoiceValue += $invoiceValue;
                        $qty = ($c['productType'] == 1) ? $itemOutQty : 0;

                        $in = array(
                            0 => "",
                            1 => "",
                            2 => "",
                            3 => $c['productName'] . "-" . $c['productCode'],
                            4 => $qty,
                            5 => $itemInPrice,
                            6 => $costValue,
                            7 => $itemOutPrice,
                            8 => $invoiceValue,
                            9 => $invoiceValue - $costValue,
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "",
                        3 => "",
                        4 => "",
                        5 => "",
                        6 => $totalCostValue,
                        7 => "",
                        8 => $totalInvoiceValue,
                        9 => $totalInvoiceValue - $totalCostValue,
                    );
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
            } else {
                $arr = "No related data found\n";
            }

            $name = "job_card_detail_costing";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $tempProductId
     * @param boolean $isAllSelect
     * @param string $type
     * @return \Zend\View\Model\ViewModel
     */
    private function _getTemporaryProductDetails($tempProductId, $isAllSelect, $type)
    {

        $isAllSelect = filter_var($isAllSelect, FILTER_VALIDATE_BOOLEAN);
        if (isset($tempProductId) || $isAllSelect) {
            $getTempProductData = $this->CommonTable('JobCard\Model\TemporaryProductTable')->getTemporaryProductWithActivity($tempProductId, $isAllSelect);

            foreach ($getTempProductData as $value) {
                $tempProductData[$value['temporaryProductID']] = $value;
            }
            //if $type is not a csv
            //data fill into pdf file
            if ($type != 'csv') {

                $translator = new Translator();
                $name = $translator->translate('Temporary Item Report');
                $companyDetails = $this->getCompanyDetails();

                $headerView = $this->headerViewTemplate($name, $period = NULL);
                $headerView->setTemplate('reporting/template/headerTemplateNormal');
                $headerViewRender = $this->htmlRender($headerView);

                $tempProductView = new ViewModel(array(
                    'cD' => $companyDetails,
                    'tempProductData' => $tempProductData,
                    'headerTemplate' => $headerViewRender,
                ));
                $tempProductView->setTemplate('reporting/job-card-report/generate-temporary-item-pdf');

                if ($type == 'pdf') {
                    $tempProductView->setTerminal(TRUE);
                    $htmlContent = $this->viewRendererHtmlContent($tempProductView);
                    $pdfPath = $this->downloadPDF('temporary-item-details', $htmlContent, true);

                    return $pdfPath;
                }

                return $tempProductView;
            } else {
                return $tempProductData;
            }
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * given HTML contontent which contain temporary product details
     * @return JSONRespondHtml
     */
    public function viewTemporaryProductAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $isAllSelect = $request->getPost('isAllSelect');
            $tempProductId = $request->getPost('tempProductId');

            $tempProductView = $this->_getTemporaryProductDetails($tempProductId, $isAllSelect, $type = 'view');

            $this->html = $tempProductView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * given pdf path which contain temporary product details pdf
     * @return JSONRespond
     */
    public function generateTemporaryProductPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $isAllSelect = $request->getPost('isAllSelect');
            $tempProductId = $request->getPost('tempProductId');

            $pdfPath = $this->_getTemporaryProductDetails($tempProductId, $isAllSelect, $type = 'pdf');

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * given csv path which contain temporary product details csv
     * @return JSONRespond
     */
    public function generateTemporaryProductSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $isAllSelect = $request->getPost('isAllSelect');
            $tempProductId = $request->getPost('tempProductId');

            $tempProductData = $this->_getTemporaryProductDetails($tempProductId, $isAllSelect, $type = 'csv');
            $companyDetails = $this->getCompanyDetails();

            $title = '';
            $tit = 'TEMPORARY ITEM REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("", "ITEM NAME - CODE", "ACTIVITY CODE", "QUANTITY");
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($tempProductData)) {
                $i = 0;
                foreach ($tempProductData as $t) {

                    $in = array(
                        0 => ++$i,
                        1 => $t['temporaryProductName'] . '-' . $t['temporaryProductCode'],
                        2 => $t['activityCode'],
                        3 => $t['temporaryProductQuantity'] . ' ' . $t['uomAbbr']
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = "No related data found\n";
            }

            $name = "temporary_item";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $activityId
     * @param boolean $isAllSelect
     * @param string $type
     * @return \Zend\View\Model\ViewModel
     */
    private function _getActivityWiseTemporaryProductDetails($activityId, $isAllSelect)
    {
        $isAllSelect = filter_var($isAllSelect, FILTER_VALIDATE_BOOLEAN);
        if (isset($activityId) || $isAllSelect) {

            //if isAllSelect variable true then getting temporary product item id's from db
            if ($isAllSelect) {

                $activityIds = $this->CommonTable('JobCard\Model\ActivityTable')->fechAll(NULL, FALSE);
                foreach ($activityIds as $a) {
                    $activityId[$a['activityId']] = $a['activityId'];
                }
            }

            $activityData = array_fill_keys($activityId, '');

            $getActivityRelatedTempProductData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityTempProdcutDetailsByActivityID($activityId);
            foreach ($getActivityRelatedTempProductData as $g) {

                $activityData[$g['activityId']]['activityCode'] = $g['activityCode'];
                $activityData[$g['activityId']]['tProData'][$g['tempProID']] = $g;
            }

            return array_filter($activityData);
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $activityData
     * render html content with related data
     * @return \Zend\View\Model\ViewModel
     */
    private function _activityWiseTemporaryPrductReportPrepare($activityData)
    {

        $translator = new Translator();
        $name = $translator->translate('Activity wise Temporary Product Report');
        $companyDetails = $this->getCompanyDetails();

        $headerView = $this->headerViewTemplate($name, $period = NULL);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $activityWiseTempProductView = new ViewModel(array(
            'cD' => $companyDetails,
            'activityProductData' => $activityData,
            'headerTemplate' => $headerViewRender,
        ));
        $activityWiseTempProductView->setTemplate('reporting/job-card-report/generate-activity-wise-temporary-item-pdf');

        return $activityWiseTempProductView;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * given HTML content which contain pdf of activity wise temporay product
     * @return JSONRespondHtml
     */
    public function viewActivityWiseTemporaryProductAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $isAllSelect = $request->getPost('isAllSelect');
            $activityId = $request->getPost('activityId');

            $activityWiseTempProductData = $this->_getActivityWiseTemporaryProductDetails($activityId, $isAllSelect);
            $activityWiseTempProductView = $this->_activityWiseTemporaryPrductReportPrepare($activityWiseTempProductData);

            $this->html = $activityWiseTempProductView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * given pdf path which contain pdf of temporary product details
     * @return JSONRespond
     */
    public function generateActivityWiseTemporaryProductPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $isAllSelect = $request->getPost('isAllSelect');
            $activityId = $request->getPost('activityId');

            $activityWiseTempProductData = $this->_getActivityWiseTemporaryProductDetails($activityId, $isAllSelect);
            $activityWiseTempProductView = $this->_activityWiseTemporaryPrductReportPrepare($activityWiseTempProductData);

            $activityWiseTempProductView->setTerminal(TRUE);
            $htmlContent = $this->viewRendererHtmlContent($activityWiseTempProductView);
            $pdfPath = $this->downloadPDF('activity-wise-temporary-item-details', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * given csv path which contain csv of temporary product details
     * @return JSONRespond
     */
    public function generateActivityWiseTemporaryProductSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $isAllSelect = $request->getPost('isAllSelect');
            $activityId = $request->getPost('activityId');

            $activityWiseTempProductData = $this->_getActivityWiseTemporaryProductDetails($activityId, $isAllSelect, $type = 'csv');
            $companyDetails = $this->getCompanyDetails();

            $title = '';
            $tit = 'ACTIVITY WISE TEMPORARY ITEM REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("", "ITEM NAME - CODE", "ACTIVITY CODE", "QUANTITY");
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($activityWiseTempProductData)) {
                $i = 0;
                foreach ($activityWiseTempProductData as $value) {

                    $in = array(
                        0 => ++$i,
                        1 => $value['activityCode'],
                    );
                    $arr.=implode(",", $in) . "\n";
                    foreach ($value['tProData'] as $a) {

                        $in = array(
                            0 => '',
                            1 => $a['temporaryProductName'] . '-' . $a['temporaryProductCode'],
                            2 => $a['temporaryProductQuantity'] . ' ' . $a['uomAbbr'],
                            3 => $a['customerTitle'] . '' . $a['customerName'],
                            4 => $a['createdTimeStamp'],
                            5 => $a['jobReferenceNumber'],
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                }
            } else {
                $arr = "No related data found\n";
            }

            $name = "activity_wise_temporary_item";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $data
     * get jobcard related quotation data
     * @return $quotationData
     */
    private function _getJobCardQuotationDetails($data)
    {
        $isAllSelect = filter_var($data['isAllSelect'], FILTER_VALIDATE_BOOLEAN);

        if ((isset($data['activityId']) || isset($data['jobId']) || isset($data['projectId'])) || $isAllSelect) {
            $quotationData = array();

            if ($data['activityId'] != NULL) {
                $quotationData = array_fill_keys($data['activityId'], '');
            } else if ($data['jobId'] != NULL) {
                $quotationData = array_fill_keys($data['jobId'], '');
            } else if ($data['projectId'] != NULL) {
                $quotationData = array_fill_keys($data['projectId'], '');
            }

            $getWeightageData = $this->CommonTable('JobCard\Model\ProjectTable')->getJobCardQuotationData($data);
            if ($getWeightageData != NULL) {
                foreach ($getWeightageData as $q) {

                    $relatedJobCardCode = $this->_getRelatedJobCardCode($q, $data['typeId'])['jobCardCode'];
                    $relatedJobCardId = $this->_getRelatedJobCardId($q, $data['typeId']);
                    $q['refJobCardCode'] = $relatedJobCardCode;

                    if ($relatedJobCardId != NULL) {
                        $quotationData[$q['quotationID']] = $q;
                    }
                }
            }

            return array_filter($quotationData);
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return HTML content related to jobcard quotation
     */
    public function viewJobCardQuotationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId,
                'fromDate' => $fromDate,
                'toDate' => $toDate
            );

            $jobCardQuotationData = $this->_getJobCardQuotationDetails($data);
            $translator = new Translator();
            $reportContentData['name'] = $translator->translate('JobCard Quotation Report');
            $reportContentData['period'] = $fromDate . '-' . $toDate;
            $reportContentData['reportData'] = $jobCardQuotationData;
            $reportContentData['htmlFilePath'] = 'reporting/job-card-report/generate-job-card-quotation-pdf';
            $jobCardQuotationView = $this->reportHtmlContentPrepare($reportContentData);

            $this->html = $jobCardQuotationView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return PDF related to jobcard quotation
     */
    public function generateJobCardQuotationPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId,
                'fromDate' => $fromDate,
                'toDate' => $toDate
            );

            $jobCardQuotationData = $this->_getJobCardQuotationDetails($data);
            $translator = new Translator();
            $reportContentData['name'] = $translator->translate('JobCard Quotation Report');
            $reportContentData['period'] = $fromDate . '-' . $toDate;
            $reportContentData['reportData'] = $jobCardQuotationData;
            $reportContentData['htmlFilePath'] = 'reporting/job-card-report/generate-job-card-quotation-pdf';
            $jobCardQuotationView = $this->reportHtmlContentPrepare($reportContentData);

            $jobCardQuotationView->setTerminal(TRUE);
            $htmlContent = $this->viewRendererHtmlContent($jobCardQuotationView);
            $pdfPath = $this->downloadPDF('job-card-quotation', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return CSV related to jobcard quotation
     */
    public function generateJobCardQuotationCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId,
                'fromDate' => $fromDate,
                'toDate' => $toDate
            );

            $jobCardQuotationData = $this->_getJobCardQuotationDetails($data);
            $title = '';
            $tit = 'JOBCARD QUOTATION REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("", "Activity/Job/Project Reference", "Customer Name",
                "Quotation No",
                "Quotation Expired Date",
                "Activity/Job/Project Created Date",
                "Job No",
                "Project No"
            );
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($jobCardQuotationData)) {
                $i = 0;
                foreach ($jobCardQuotationData as $q) {
                    $jobCode = isset($q['jobCode']) ? $q['jobCode'] : '';
                    $projectCD = isset($q['projectCD']) ? $q['projectCD'] : '';

                    $in = array(
                        0 => ++$i,
                        1 => $q['refJobCardCode'],
                        2 => $q['customerTitle'] . ' ' . $q['customerName'],
                        3 => $q['quotationCode'],
                        4 => $q['quotationExpireDate'],
                        5 => $jobCode,
                        6 => $projectCD,
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = "No related data found\n";
            }

            $name = "job_card_quotation";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Generate vehicles allocated report for jobs/ activities.
     * @return JsonModel
     */
    public function vehicleAllocationReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $vehicleIds = $request->getPost('vehicleIds');
            $isAllVehicles = $request->getPost('isAllVehicles');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            if ($isAllVehicles) {
                $vehicleIds = array();
                $vehicles = $this->CommonTable('JobCard\Model\VehicleTable')->fetchAll();
                foreach ($vehicles as $vehicle) {
                    $vehicleIds[] = $vehicle['vehicleId'];
                }
            }

            $dataList = array();
            foreach ($vehicleIds as $vehicleId) {
                $activityList = $this->CommonTable('JobCard/Model/ActivityVehicleTable')->getActivityVehiclesByVehicleId($vehicleId, $fromDate, $toDate);
                foreach ($activityList as $activity) {
                    $key = $activity['vehicleRegistrationNumber'] . ' (' . $activity['vehicleMake'] . '-' . $activity['vehicleModel'] . ')';
                    $dataList[$key][] = array(
                        'job' => $activity['jobName'] . ' (' . $activity['jobReferenceNumber'] . ')',
                        'activity' => $activity['activityName'] . ' (' . $activity['activityCode'] . ')',
                        'startMileage' => $activity['activityVehicleStartMileage'],
                        'endMileage' => $activity['activityVehicleEndMileage'],
                        'distance' => ($activity['activityVehicleEndMileage'] - $activity['activityVehicleStartMileage']),
                        'cost' => $activity['activityVehicleCost']
                    );
                }
            }
            $translator = new Translator();
            $reportContentData['name'] = $translator->translate('Vehicle Allocation Report');
            $reportContentData['period'] = $fromDate . ' - ' . $toDate;
            $reportContentData['reportData'] = $dataList;
            $reportContentData['htmlFilePath'] = 'reporting/job-card-report/generate-vehicle-allocation';
            $view = $this->reportHtmlContentPrepare($reportContentData);

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * Generate vehicles allocated pdf for jobs/ activities.
     * @return JsonModel
     */
    public function vehicleAllocationPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $vehicleIds = $request->getPost('vehicleIds');
            $isAllVehicles = $request->getPost('isAllVehicles');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            if ($isAllVehicles) {
                $vehicleIds = array();
                $vehicles = $this->CommonTable('JobCard\Model\VehicleTable')->fetchAll();
                foreach ($vehicles as $vehicle) {
                    $vehicleIds[] = $vehicle['vehicleId'];
                }
            }

            $dataList = array();
            foreach ($vehicleIds as $vehicleId) {
                $activityList = $this->CommonTable('JobCard/Model/ActivityVehicleTable')->getActivityVehiclesByVehicleId($vehicleId, $fromDate, $toDate);
                foreach ($activityList as $activity) {
                    $key = $activity['vehicleRegistrationNumber'] . ' (' . $activity['vehicleMake'] . '-' . $activity['vehicleModel'] . ')';
                    $dataList[$key][] = array(
                        'job' => $activity['jobName'] . ' (' . $activity['jobReferenceNumber'] . ')',
                        'activity' => $activity['activityName'] . ' (' . $activity['activityCode'] . ')',
                        'startMileage' => $activity['activityVehicleStartMileage'],
                        'endMileage' => $activity['activityVehicleEndMileage'],
                        'distance' => ($activity['activityVehicleEndMileage'] - $activity['activityVehicleStartMileage']),
                        'cost' => $activity['activityVehicleCost']
                    );
                }
            }

            $translator = new Translator();
            $reportContentData['name'] = $translator->translate('Vehicle Allocation Report');
            $reportContentData['period'] = $fromDate . '-' . $toDate;
            $reportContentData['reportData'] = $dataList;
            $reportContentData['htmlFilePath'] = 'reporting/job-card-report/generate-vehicle-allocation';
            $view = $this->reportHtmlContentPrepare($reportContentData);

            $view->setTerminal(TRUE);
            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('vehicle-allocation', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Generate vehicles allocated csv for jobs/ activities.
     * @return JsonModel
     */
    public function vehicleAllocationCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $vehicleIds = $request->getPost('vehicleIds');
            $isAllVehicles = $request->getPost('isAllVehicles');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $companyDetails = $this->getCompanyDetails();

            if ($isAllVehicles) {
                $vehicleIds = array();
                $vehicles = $this->CommonTable('JobCard\Model\VehicleTable')->fetchAll();
                foreach ($vehicles as $vehicle) {
                    $vehicleIds[] = $vehicle['vehicleId'];
                }
            }

            $dataList = array();
            foreach ($vehicleIds as $vehicleId) {
                $activityList = $this->CommonTable('JobCard/Model/ActivityVehicleTable')->getActivityVehiclesByVehicleId($vehicleId, $fromDate, $toDate);
                foreach ($activityList as $activity) {
                    $key = $activity['vehicleRegistrationNumber'] . ' (' . $activity['vehicleMake'] . '-' . $activity['vehicleModel'] . ')';
                    $dataList[$key][] = array(
                        'job' => $activity['jobName'] . ' (' . $activity['jobReferenceNumber'] . ')',
                        'activity' => $activity['activityName'] . ' (' . $activity['activityCode'] . ')',
                        'startMileage' => $activity['activityVehicleStartMileage'],
                        'endMileage' => $activity['activityVehicleEndMileage'],
                        'distance' => ($activity['activityVehicleEndMileage'] - $activity['activityVehicleStartMileage']),
                        'cost' => $activity['activityVehicleCost']
                    );
                }
            }

            $title = '';
            $tit = 'VEHICLE ALLOCATION REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";
            $title .= 'Period : ' . $fromDate . ' - ' . $toDate . "\n";

            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array(
                "Vehicle",
                "Job Name",
                "Activity Name",
                "Start Mileage",
                "End Mileage",
                "Distance",
                "Cost"
            );
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($dataList)) {
                foreach ($dataList as $key => $data) {

                    $in = array(
                        0 => $key
                    );
                    $arr .= implode(",", $in) . "\n";

                    foreach ($data as $activity) {
                        $in = array(
                            0 => '',
                            1 => $activity['job'],
                            2 => $activity['activity'],
                            3 => number_format($activity['startMileage'], 2, '.', ''),
                            4 => number_format($activity['endMileage'], 2, '.', ''),
                            5 => number_format($activity['distance'], 2, '.', ''),
                            6 => number_format($activity['cost'], 2, '.', '')
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                }
            } else {
                $arr = "No related data found\n";
            }

            $name = "vehicle-allocation";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Generate employee wise activity report.
     * @return JsonModel
     */
    public function employeeWiseActivityReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $employeeIds = $request->getPost('employeeIds');
            $employeeTypes = $request->getPost('employeeTypes');
            $isAllEmployees = $request->getPost('isAllEmployees');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            if ($isAllEmployees) {
                $employeeIds = array();
                $employees = $this->CommonTable('JobCard\Model\EmployeeTable')->fetchAll();
                foreach ($employees as $employee) {
                    $employeeIds[] = $employee['employeeID'];
                }
            }

            $dataList = array();
            foreach ($employeeIds as $employeeId) {
                if (in_array('activity-owner', $employeeTypes) && in_array('activity-supervisor', $employeeTypes)) {
                    $owners = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->getActivityOwnerDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($owners as $owner) {
                        $dataList[$owner['employeeFirstName'] . ' ' . $owner['employeeSecondName']][] = array(
                            'jobName' => $owner['jobName'] . ' (' . $owner['jobReferenceNumber'] . ')',
                            'activityName' => $owner['activityName'] . ' (' . $owner['activityCode'] . ')',
                            'type' => 'Owner'
                        );
                    }
                    $supervisors = $this->CommonTable('JobCard\Model\ActivitySupervisorTable')->getActivitySupervisorDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($supervisors as $supervisor) {
                        $dataList[$supervisor['employeeFirstName'] . ' ' . $supervisor['employeeSecondName']][] = array(
                            'jobName' => $supervisor['jobName'] . ' (' . $supervisor['jobReferenceNumber'] . ')',
                            'activityName' => $supervisor['activityName'] . ' (' . $supervisor['activityCode'] . ')',
                            'type' => 'Supervisor'
                        );
                    }
                } else if (in_array('activity-owner', $employeeTypes)) {
                    $owners = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->getActivityOwnerDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($owners as $owner) {
                        $dataList[$owner['employeeFirstName'] . ' ' . $owner['employeeSecondName']][] = array(
                            'jobName' => $owner['jobName'] . ' (' . $owner['jobReferenceNumber'] . ')',
                            'activityName' => $owner['activityName'] . ' (' . $owner['activityCode'] . ')',
                            'type' => 'Owner'
                        );
                    }
                } elseif (in_array('activity-supervisor', $employeeTypes)) {
                    $supervisors = $this->CommonTable('JobCard\Model\ActivitySupervisorTable')->getActivitySupervisorDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($supervisors as $supervisor) {
                        $dataList[$supervisor['employeeFirstName'] . ' ' . $supervisor['employeeSecondName']][] = array(
                            'jobName' => $supervisor['jobName'] . ' (' . $supervisor['jobReferenceNumber'] . ')',
                            'activityName' => $supervisor['activityName'] . ' (' . $supervisor['activityCode'] . ')',
                            'type' => 'Supervisor'
                        );
                    }
                }
            }

            $translator = new Translator();
            $reportContentData['name'] = $translator->translate('Employee Wise Activity Report');
            $reportContentData['period'] = $fromDate . ' - ' . $toDate;
            $reportContentData['reportData'] = $dataList;
            $reportContentData['htmlFilePath'] = 'reporting/job-card-report/generate-employee-wise-activity';
            $view = $this->reportHtmlContentPrepare($reportContentData);

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * Generate employee wise activity pdf.
     * @return JsonModel
     */
    public function employeeWiseActivityPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $employeeIds = $request->getPost('employeeIds');
            $employeeTypes = $request->getPost('employeeTypes');
            $isAllEmployees = $request->getPost('isAllEmployees');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            if ($isAllEmployees) {
                $employeeIds = array();
                $employees = $this->CommonTable('JobCard\Model\EmployeeTable')->fetchAll();
                foreach ($employees as $employee) {
                    $employeeIds[] = $employee['employeeID'];
                }
            }

            $dataList = array();
            foreach ($employeeIds as $employeeId) {
                if (in_array('activity-owner', $employeeTypes) && in_array('activity-supervisor', $employeeTypes)) {
                    $owners = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->getActivityOwnerDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($owners as $owner) {
                        $dataList[$owner['employeeFirstName'] . ' ' . $owner['employeeSecondName']][] = array(
                            'jobName' => $owner['jobName'] . ' (' . $owner['jobReferenceNumber'] . ')',
                            'activityName' => $owner['activityName'] . ' (' . $owner['activityCode'] . ')',
                            'type' => 'Owner'
                        );
                    }
                    $supervisors = $this->CommonTable('JobCard\Model\ActivitySupervisorTable')->getActivitySupervisorDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($supervisors as $supervisor) {
                        $dataList[$supervisor['employeeFirstName'] . ' ' . $supervisor['employeeSecondName']][] = array(
                            'jobName' => $supervisor['jobName'] . ' (' . $supervisor['jobReferenceNumber'] . ')',
                            'activityName' => $supervisor['activityName'] . ' (' . $supervisor['activityCode'] . ')',
                            'type' => 'Supervisor'
                        );
                    }
                } else if (in_array('activity-owner', $employeeTypes)) {
                    $owners = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->getActivityOwnerDetailsByEmployeeId($employeeI, $fromDate, $toDated);
                    foreach ($owners as $owner) {
                        $dataList[$owner['employeeFirstName'] . ' ' . $owner['employeeSecondName']][] = array(
                            'jobName' => $owner['jobName'] . ' (' . $owner['jobReferenceNumber'] . ')',
                            'activityName' => $owner['activityName'] . ' (' . $owner['activityCode'] . ')',
                            'type' => 'Owner'
                        );
                    }
                } else if (in_array('activity-supervisor', $employeeTypes)) {
                    $supervisors = $this->CommonTable('JobCard\Model\ActivitySupervisorTable')->getActivitySupervisorDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($supervisors as $supervisor) {
                        $dataList[$supervisor['employeeFirstName'] . ' ' . $supervisor['employeeSecondName']][] = array(
                            'jobName' => $supervisor['jobName'] . ' (' . $supervisor['jobReferenceNumber'] . ')',
                            'activityName' => $supervisor['activityName'] . ' (' . $supervisor['activityCode'] . ')',
                            'type' => 'Supervisor'
                        );
                    }
                }
            }
            $translator = new Translator();
            $reportContentData['name'] = $translator->translate('Employee Wise Activity Report');
            $reportContentData['period'] = $fromDate . ' - ' . $toDate;
            $reportContentData['reportData'] = $dataList;
            $reportContentData['htmlFilePath'] = 'reporting/job-card-report/generate-employee-wise-activity';
            $view = $this->reportHtmlContentPrepare($reportContentData);

            $view->setTerminal(TRUE);
            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('employee-wise-activity', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Generate employee wise activity csv.
     * @return JsonModel
     */
    public function employeeWiseActivityCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $employeeIds = $request->getPost('employeeIds');
            $employeeTypes = $request->getPost('employeeTypes');
            $isAllEmployees = $request->getPost('isAllEmployees');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $companyDetails = $this->getCompanyDetails();

            if ($isAllEmployees) {
                $employeeIds = array();
                $employees = $this->CommonTable('JobCard\Model\EmployeeTable')->fetchAll();
                foreach ($employees as $employee) {
                    $employeeIds[] = $employee['employeeID'];
                }
            }

            $dataList = array();
            foreach ($employeeIds as $employeeId) {
                if (in_array('activity-owner', $employeeTypes) && in_array('activity-supervisor', $employeeTypes)) {
                    $owners = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->getActivityOwnerDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($owners as $owner) {
                        $dataList[$owner['employeeFirstName'] . ' ' . $owner['employeeSecondName']][] = array(
                            'jobName' => $owner['jobName'] . ' (' . $owner['jobReferenceNumber'] . ')',
                            'activityName' => $owner['activityName'] . ' (' . $owner['activityCode'] . ')',
                            'type' => 'Owner'
                        );
                    }
                    $supervisors = $this->CommonTable('JobCard\Model\ActivitySupervisorTable')->getActivitySupervisorDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($supervisors as $supervisor) {
                        $dataList[$supervisor['employeeFirstName'] . ' ' . $supervisor['employeeSecondName']][] = array(
                            'jobName' => $supervisor['jobName'] . ' (' . $supervisor['jobReferenceNumber'] . ')',
                            'activityName' => $supervisor['activityName'] . ' (' . $supervisor['activityCode'] . ')',
                            'type' => 'Supervisor'
                        );
                    }
                } else if (in_array('activity-owner', $employeeTypes)) {
                    $owners = $this->CommonTable('JobCard\Model\ActivityOwnerTable')->getActivityOwnerDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($owners as $owner) {
                        $dataList[$owner['employeeFirstName'] . ' ' . $owner['employeeSecondName']][] = array(
                            'jobName' => $owner['jobName'] . ' (' . $owner['jobReferenceNumber'] . ')',
                            'activityName' => $owner['activityName'] . ' (' . $owner['activityCode'] . ')',
                            'type' => 'Owner'
                        );
                    }
                } else if (in_array('activity-supervisor', $employeeTypes)) {
                    $supervisors = $this->CommonTable('JobCard\Model\ActivitySupervisorTable')->getActivitySupervisorDetailsByEmployeeId($employeeId, $fromDate, $toDate);
                    foreach ($supervisors as $supervisor) {
                        $dataList[$supervisor['employeeFirstName'] . ' ' . $supervisor['employeeSecondName']][] = array(
                            'jobName' => $supervisor['jobName'] . ' (' . $supervisor['jobReferenceNumber'] . ')',
                            'activityName' => $supervisor['activityName'] . ' (' . $supervisor['activityCode'] . ')',
                            'type' => 'Supervisor'
                        );
                    }
                }
            }

            $title = '';
            $tit = 'EMPLOYEE WISE ACTIVITY REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";
            $title .= 'Period : ' . $fromDate . ' - ' . $toDate . "\n";

            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array(
                "Employee Name",
                "Job Name",
                "Activity Name",
                "Role Type"
            );
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($dataList)) {
                foreach ($dataList as $key => $data) {

                    $in = array(
                        0 => $key
                    );
                    $arr .= implode(",", $in) . "\n";

                    foreach ($data as $activity) {
                        $in = array(
                            0 => '',
                            1 => $activity['jobName'],
                            2 => $activity['activityName'],
                            3 => $activity['type']
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                }
            } else {
                $arr = "No related data found\n";
            }

            $name = "employee-wise-activity";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Get view Job / Activity Repeat report
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function viewRepeatReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $type = $request->getPost('type');
            $locations = $request->getPost('locations');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $repeatList = [];
            if ($type == 'activityNo') {
                $repeatActivities = $this->CommonTable('JobCard\Model\ActivityTable')->getRepeatActivityList($locations, $fromDate, $toDate);
                $repeatActivityList = [];
                foreach ($repeatActivities as $repeatActivity) {
                    $repeatList[$repeatActivity['activityId']]['activityCode'] = $repeatActivity['activityCode'];
                    $repeatList[$repeatActivity['activityId']]['activityName'] = $repeatActivity['activityName'];
                    $repeatList[$repeatActivity['activityId']]['jobReferenceNumber'] = $repeatActivity['jobReferenceNumber'];
                    $repeatList[$repeatActivity['activityId']]['projectCode'] = $repeatActivity['projectCode'];
                    $repeatList[$repeatActivity['activityId']]['repeatComment'] = $repeatActivity['activityRepeatComment'];
                    $repeatList[$repeatActivity['activityId']]['createdTimeStamp'] = $this->getUserDateTime($repeatActivity['createdTimeStamp'], 'Y-m-d');
                }

                $isActivityColumn = TRUE;
                $name = $translator->translate('Repeat Activity Report');
            } else if ($type == 'jobNo') {
                $repeatJobs = $this->CommonTable('JobCard\Model\JobTable')->getRepeatJobs($locations, $fromDate, $toDate);
                foreach ($repeatJobs as $repeatJob) {
                    $repeatList[$repeatJob['jobId']]['jobReferenceNumber'] = $repeatJob['jobReferenceNumber'];
                    $repeatList[$repeatJob['jobId']]['projectCode'] = $repeatJob['projectCode'];
                    $repeatList[$repeatJob['jobId']]['repeatComment'] = $repeatJob['jobRepeatComment'];
                    $repeatList[$repeatJob['jobId']]['createdTimeStamp'] = $this->getUserDateTime($repeatJob['createdTimeStamp'], 'Y-m-d');
                }
                $name = $translator->translate('Repeat Job Report');
                $isActivityColumn = FALSE;
            }

            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'repeatList' => $repeatList,
                'isActivityColumn' => $isActivityColumn,
                'headerTemplate' => $headerViewRender,
            ));


            $view->setTerminal(true);
            $view->setTemplate('reporting/job-card-report/generate-repeat-activity-pdf');
            $this->status = true;
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * Generate Repeat Activity/Job pdf
     * @author sharmilan <sharmilan@thinkube.com>
     * @return type
     */
    public function generateRepeatPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $type = $request->getPost('type');
            $locations = $request->getPost('locations');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $repeatList = [];
            if ($type == 'activityNo') {
                $repeatActivities = $this->CommonTable('JobCard\Model\ActivityTable')->getRepeatActivityList($locations, $fromDate, $toDate);
                $repeatActivityList = [];
                foreach ($repeatActivities as $repeatActivity) {
                    $repeatList[$repeatActivity['activityId']]['activityCode'] = $repeatActivity['activityCode'];
                    $repeatList[$repeatActivity['activityId']]['activityName'] = $repeatActivity['activityName'];
                    $repeatList[$repeatActivity['activityId']]['jobReferenceNumber'] = $repeatActivity['jobReferenceNumber'];
                    $repeatList[$repeatActivity['activityId']]['projectCode'] = $repeatActivity['projectCode'];
                    $repeatList[$repeatActivity['activityId']]['repeatComment'] = $repeatActivity['activityRepeatComment'];
                    $repeatList[$repeatActivity['activityId']]['createdTimeStamp'] = $this->getUserDateTime($repeatActivity['createdTimeStamp'], 'Y-m-d');
                }

                $isActivityColumn = TRUE;
                $name = $translator->translate('Repeat Activity Report');
            } else if ($type == 'jobNo') {
                $repeatJobs = $this->CommonTable('JobCard\Model\JobTable')->getRepeatJobs($locations, $fromDate, $toDate);
                foreach ($repeatJobs as $repeatJob) {
                    $repeatList[$repeatJob['jobId']]['jobReferenceNumber'] = $repeatJob['jobReferenceNumber'];
                    $repeatList[$repeatJob['jobId']]['projectCode'] = $repeatJob['projectCode'];
                    $repeatList[$repeatJob['jobId']]['repeatComment'] = $repeatJob['jobRepeatComment'];
                    $repeatList[$repeatJob['jobId']]['createdTimeStamp'] = $this->getUserDateTime($repeatJob['createdTimeStamp'], 'Y-m-d');
                }
                $name = $translator->translate('Repeat Job Report');
                $isActivityColumn = FALSE;
            }

            $period = $fromDate . ' - ' . $toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'repeatList' => $repeatList,
                'isActivityColumn' => $isActivityColumn,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTerminal(true);
            $view->setTemplate('reporting/job-card-report/generate-repeat-activity-pdf');
            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('Repeat_Activity_or_Job_Report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Generate Repeat Activity/job csv
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function generateRepeatCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $type = $request->getPost('type');
            $locations = $request->getPost('locations');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cD = $this->getCompanyDetails();

            if ($type == 'activityNo') {
                $repeatActivities = $this->CommonTable('JobCard\Model\ActivityTable')->getRepeatActivityList($locations, $fromDate, $toDate);
                $reportTitle = [ "", 'REPEAT ACTIVITY REPORT', "",];
                $header = '';

                $header = implode(',', $reportTitle) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $header .= $cD[0]->companyName . "\n";
                $header .= $cD[0]->companyAddress . "\n";
                $header .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $peroidRow = ["Period : " . $fromDate . '-' . $toDate];
                $header .= implode(",", $peroidRow) . "\n";
                $header .= implode(",", [""]) . "\n";

                $columnTitlesArray = ['Activity',
                    'Job No',
                    'Project No',
                    'Comment',
                    'Created Date'];
                $columnTitles = implode(",", $columnTitlesArray);

                $csvRow = '';

                foreach ($repeatActivities as $repeatActivity) {
                    $tableRow = [
                        $repeatActivity['activityCode'] . ' - ' . $repeatActivity['activityName'],
                        $repeatActivity['jobReferenceNumber'],
                        $repeatActivity['projectCode'],
                        $repeatActivity['activityRepeatComment'],
                        $this->getUserDateTime($repeatActivity['createdTimeStamp'], 'Y-m-d'),
                    ];
                    $csvRow .= implode(",", $tableRow) . "\n";
                }
                if (count($repeatActivities) <= 0) {
                    $csvRow = "No matching records found \n";
                }
            } else if ($type == 'jobNo') {
                $repeatJobs = $this->CommonTable('JobCard\Model\JobTable')->getRepeatJobs($locations, $fromDate, $toDate);
                $header = '';
                $reportTitle = [ "", 'REPEAT JOB REPORT', "",];
                $header = implode(',', $reportTitle) . "\n";

                $header .= $cD[0]->companyName . "\n";
                $header .= $cD[0]->companyAddress . "\n";
                $header .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $peroidRow = ["Period : " . $fromDate . '-' . $toDate];
                $header .= implode(",", $peroidRow) . "\n";
                $header .= implode(",", [""]) . "\n";

                $columnTitlesArray = ['Job No',
                    'Project No',
                    'Comment',
                    'Created Date'];
                $columnTitles = implode(",", $columnTitlesArray);

                $csvRow = '';
                foreach ($repeatJobs as $repeatJob) {
                    $tableRow = [
                        $repeatJob['jobReferenceNumber'],
                        $repeatJob['projectCode'],
                        $repeatJob['jobRepeatComment'],
                        $this->getUserDateTime($repeatJob['createdTimeStamp'], 'Y-m-d'),
                    ];
                    $csvRow .= implode(",", $tableRow) . "\n";
                }
                if (count($repeatJobs) <= 0) {
                    $csvRow = "No matching records found \n";
                }
            }

            $name = 'Repeat_Activity_or_Job_Report_csv' . md5($this->getGMTDateTime());
            $csvContent = $this->csvContent($header, $columnTitles, $csvRow);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function viewOpenJobCardDetailCostingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');
            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId
            );
            $translator = new Translator();
            $name = $translator->translate('Open Project/ Job/ Activity Detail Costing Report');

            $openCostingData = $this->_getOpenJobCardCosting($data);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $openJobCardCostingView = new ViewModel(array(
                'cD' => $companyDetails,
                'openCostingData' => $openCostingData,
                'headerTemplate' => $headerViewRender,
            ));
            $openJobCardCostingView->setTemplate('reporting/job-card-report/generate-open-job-card-detail-costing-pdf');

            $this->html = $openJobCardCostingView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return Pdf path
     */
    public function generateOpenJobCardDetailCostingPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');
            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId
            );
            $translator = new Translator();
            $name = $translator->translate('Open Project/ Job/ Activity Detail Costing Report');

            $openCostingData = $this->_getOpenJobCardCosting($data);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $openJobCardCostingView = new ViewModel(array(
                'cD' => $companyDetails,
                'openCostingData' => $openCostingData,
                'headerTemplate' => $headerViewRender,
            ));
            $openJobCardCostingView->setTemplate('reporting/job-card-report/generate-open-job-card-detail-costing-pdf');
            $openJobCardCostingView->setTerminal(TRUE);

            $htmlContent = $this->viewRendererHtmlContent($openJobCardCostingView);
            $pdfPath = $this->downloadPDF('job-card-quotation', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return csv path
     */
    public function generateOpenJobCardDetailCostingCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $activityId = $request->getPost('activityId');
            $jobId = $request->getPost('jobId');
            $projectId = $request->getPost('projectId');
            $isAllSelect = $request->getPost('isAllSelect');
            $typeId = $request->getPost('typeId');
            $data = array(
                'activityId' => $activityId,
                'jobId' => $jobId,
                'projectId' => $projectId,
                'isAllSelect' => $isAllSelect,
                'typeId' => $typeId
            );

            $openCostingData = $this->_getOpenJobCardCosting($data);
            $companyDetails = $this->getCompanyDetails();

            $title = '';
            $tit = 'Open Project/ Job/ Activity Detail Costing Report';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
            );
            $title = implode(",", $in) . "\n";

            $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

            $in = array(
                0 => ""
            );
            $title.=implode(",", $in) . "\n";

            $arrs = array("Activity/ Job/ Project ID", "Name", "Type", "Reference Name - Code", "Amount", "Total");
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($openCostingData)) {
                $i = 0;
                foreach ($openCostingData as $value) {

                    $in = array(
                        0 => $value['referenceCode'],
                        1 => $value['referenceName'],
                    );
                    $arr.=implode(",", $in) . "\n";

                    if (isset($value['rMD']) || isset($value['cTD']) || isset($value['vD']) || isset($value['eD'])) {
                        $netTotal = 0;
                        if (isset($value['rMD'])) {
                            $totalCost = 0;
                            foreach ($value['rMD'] as $items) {
                                foreach ($items as $c) {
                                    $quantity = isset($c['quantity']) ? $c['quantity'] : 0;
                                    $uomAbbr = isset($c['uomAbbr']) ? $c['uomAbbr'] : '';
                                    $costValue = isset($c['costValue']) ? $c['costValue'] : 0;
                                    $totalCost = $quantity * $costValue;
                                    $netTotal += $totalCost;

                                    $in = ['', '', 'Raw Material', $c['refCode'] . '-' . $c['refName'], $quantity . '' . $uomAbbr, $totalCost];
                                    $arr.=implode(",", $in) . "\n";
                                }
                            }
                        }

                        if (isset($value['cTD'])) {
                            $totalCost = 0;
                            foreach ($value['cTD'] as $items) {
                                foreach ($items as $c) {
                                    $actualCost = isset($c['actualCost']) ? $c['actualCost'] : 0;
                                    $netTotal+= $actualCost;
                                    $in = array(
                                        0 => '',
                                        1 => '',
                                        2 => 'Cost Type',
                                        3 => $c['costTypeName'],
                                        4 => $actualCost,
                                        5 => $actualCost,
                                    );
                                    $arr.=implode(",", $in) . "\n";
                                }
                            }
                        }

                        if (isset($value['vD'])) {
                            $totalCost = 0;
                            foreach ($value['vD'] as $items) {
                                foreach ($items as $c) {
                                    $vehicleCost = isset($c['activityVehicleCost']) ? $c['activityVehicleCost'] : 0;
                                    $netTotal+= $vehicleCost;
                                    $in = array(
                                        0 => '',
                                        1 => '',
                                        2 => 'Vehicle',
                                        3 => $c['vehicleMake'] . '-' . $c['vehicleModel'] . '-' . $c['vehicleRegistrationNumber'],
                                        4 => $vehicleCost,
                                        5 => $vehicleCost,
                                    );
                                    $arr.=implode(",", $in) . "\n";
                                }
                            }
                        }

                        if (isset($value['eD'])) {
                            $totalCost = 0;
                            foreach ($value['eD'] as $items) {
                                foreach ($items as $c) {
                                    $employeeCost = isset($c['employeeCost']) ? $c['employeeCost'] : 0;
                                    $netTotal+= $employeeCost;
                                    $hours = explode(".", $c['hours']);

                                    $in = ['', '', 'Activity Owner', $c['employeeFirstName'] . '-' . $c['employeeSecondName'], $hours[0] . ' hours and ' . $hours[1] . ' min', $employeeCost];
                                    $arr.=implode(",", $in) . "\n";
                                }
                            }
                        }

                        $in = ['', '', '', '', "Net Total", $netTotal];
                        $arr.=implode(",", $in) . "\n";
                    } else {

                        $in = array(
                            0 => $value['referenceCode'],
                            1 => $value['referenceName'],
                            2 => 'No related data found'
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                }
            } else {
                $arr = "No related data found\n";
            }

            $name = "open_jobcard_costing";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $data
     * @return $openJobCardCostingData
     */
    private function _getOpenJobCardCosting($data = NULL)
    {
        $isAllSelect = filter_var($data['isAllSelect'], FILTER_VALIDATE_BOOLEAN);

        if ((isset($data['activityId']) || isset($data['jobId']) || isset($data['projectId'])) || $isAllSelect) {
            $openJobCardCostingData = array();

            if ($data['activityId'] != NULL) {
                $openJobCardCostingData = array_fill_keys($data['activityId'], '');
            } else if ($data['jobId'] != NULL) {
                $openJobCardCostingData = array_fill_keys($data['jobId'], '');
            } else if ($data['projectId'] != NULL) {
                $openJobCardCostingData = array_fill_keys($data['projectId'], '');
            }

            $getOpenJobCardCostingData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityCostingDetails($data);

            if ($getOpenJobCardCostingData != NULL) {
                foreach ($getOpenJobCardCostingData as $c) {
                    $relatedJobCardCode = $this->_getRelatedJobCardCode($c, $data['typeId']);
                    $relatedJobCardId = $this->_getRelatedJobCardId($c, $data['typeId']);
                    $relatedJobCardRefName = $this->_getRelatedJobCardRefName($c, $data['typeId']);

                    $openJobCardCostingData[$relatedJobCardId]['referenceCode'] = $relatedJobCardCode['jobCardCode'];
                    $openJobCardCostingData[$relatedJobCardId]['referenceName'] = $relatedJobCardRefName;

                    if ($c['activityRawMaterialId'] != NULL) {
                        //proID element using because of there has two coulmn called productID
                        //when join query where is costType table and product table
                        //so I need only product table's productID and
                        //I am getting it called proID
                        //set quantity and uomAbbr according to display UOM
                        $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($c['proID']);
                        $thisqty = $this->getProductQuantityViaDisplayUom($c['activityRawMaterialQuantity'], $productUom);
                        $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                        $uomAbbr = ($thisqty['uomAbbr'] == '') ? '' : $thisqty['uomAbbr'];

                        $openJobCardCostingData[$relatedJobCardId]['rMD'][$c['activityID']][$c['locationProductId']] = array(
                            'refCode' => $c['productCode'],
                            'refName' => $c['productName'],
                            'quantity' => $quantity,
                            'uomAbbr' => $uomAbbr,
                            'costValue' => $c['activityRawMaterialCost']
                        );
                    }
                    if ($c['costTypeId'] != NULL) {
                        $openJobCardCostingData[$relatedJobCardId]['cTD'][$c['activityID']][$c['costTypeId']] = array(
                            'costTypeName' => $c['costTypeName'],
                            'actualCost' => $c['activityCostTypeActualCost'],
                            'estimateValue' => $c['activityCostTypeEstimatedCost']
                        );
                    }

                    if ($c['vehicleId'] != NULL) {
                        $openJobCardCostingData[$relatedJobCardId]['vD'][$c['activityID']][$c['costTypeId']] = array(
                            'vehicleMake' => $c['vehicleMake'],
                            'vehicleModel' => $c['vehicleModel'],
                            'vehicleRegistrationNumber' => $c['vehicleRegistrationNumber'],
                            'vehicleCost' => $c['vehicleCost'],
                            'estimateValue' => $c['activityCostTypeEstimatedCost'],
                            'activityVehicleStartMileage' => $c['activityVehicleStartMileage'],
                            'activityVehicleEndMileage' => $c['activityVehicleEndMileage'],
                            'activityVehicleCost' => $c['activityVehicleCost'],
                        );
                    }

                    if ($c['hours'] > 0 && $c['employeeID'] != NULL) {
                        $openJobCardCostingData[$relatedJobCardId]['eD'][$c['activityID']][$c['employeeID']] = array(
                            'employeeFirstName' => $c['employeeFirstName'],
                            'employeeSecondName' => $c['employeeSecondName'],
                            'employeeCost' => $c['employeeHourlyCost'] * $c['hours'],
                            'hours' => number_format($c['hours'], 2)
                        );
                    }
                }
            }

            return array_filter($openJobCardCostingData);
        }
    }

}
