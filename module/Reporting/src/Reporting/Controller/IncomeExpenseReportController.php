<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class IncomeExpenseReportController extends CoreController
{
    protected $sideMenus  = 'accounting_side_menu';
    protected $upperMenus = 'expenses_reports_upper_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Income & Expenses', 'Accounting');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/income-expense-report.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');
        
        $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllUnlockedFiscalPeriods(false);

        $fiscalPeriodArray = array();
        foreach ($fiscalPeriodData as $value) {
                $fiscalPeriodArray[$value['fiscalPeriodID']] = $value['fiscalPeriodStartDate']." - ".$value['fiscalPeriodEndDate'];
        }

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";
        

        $budgetReportView = new ViewModel(array(
            'fiscalPeriods' => $fiscalPeriodArray,
            'dimensions' => $dimensions,
        )); 
        
        return $budgetReportView;
    }

}
