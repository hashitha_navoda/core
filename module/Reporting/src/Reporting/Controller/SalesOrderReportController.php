<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class SalesOrderReportController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_report_upper_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Sales Order', 'SALES');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/sales-order-report.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');
        
        $companyDetails = $this->getCompanyDetails();
        $dateFormat = $this->getUserDateFormat();
        
        $salesPersonList = [];
        $salesPersons = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($salesPersons as $salesPerson) {
            $salesPersonList[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }
        
        //get CustomerCategory list
        $customerCategory = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();

        return new ViewModel([
            'locationList' => $this->allLocations,
            'currentLocation' => '',
            'salesPersonList' => $salesPersonList,
            'cusCategory' => $customerCategory
        ]);
    }

}
