<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * job card report related view functions
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class JobCardReportController extends CoreController
{

    protected $sideMenus = 'job_card_side_menu';
    protected $upperMenus = 'job_card_reports_upper_menu';

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Sales Report form created in this method
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Job Card', 'JobCard');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/job-card-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/accounting.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'getAllProjectWithProgress', 'jobReference'
        ]);
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $activityList = array();
        $jobList = array();
        $projectList = array();

        $activitys = $this->CommonTable('JobCard\Model\ActivityTable')->fechAll(FALSE);
        foreach ($activitys as $a) {
            $activityList[$a['activityId']] = $a['activityCode'];
        }

        $jobs = $this->CommonTable('JobCard\Model\JobTable')->fetchAll(FALSE);
        foreach ($jobs as $j) {
            $jobList[$j['jobId']] = $j['jobReferenceNumber'];
        }

        $projects = $this->CommonTable('JobCard\Model\ProjectTable')->fetchAll();
        foreach ($projects as $p) {
            $projectList[$p['projectId']] = $p['projectCode'];
        }

        $users = $this->CommonTable('User\Model\UserTable')->fetchAll();
        foreach ($users as $u) {
            $userList[$u['userID']] = $u;
        }

        return new ViewModel(array(
            'activityList' => $activityList,
            'jobList' => $jobList,
            'projectList' => $projectList,
            'userList' => $userList,
            'locationList' => $this->allLocations,
        ));
    }

}
