<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class InvoiceReportController extends CoreController
{

    protected $productCategoryTable;
    protected $paymentsTable;
    protected $salesByInvoiceTable;
    protected $productTable;
    protected $loggerTable;
    protected $userTable;
    protected $companyTable;
    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_report_upper_menu';

    function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
            $this->upperMenus = 'invoice_report_upper_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'SALES', 'SALES');
        $this->getSideAndUpperMenus('Reports', 'Invoice');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/invoice-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/accounting.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $companyDetails = $this->getCompanyDetails();

        //Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll(false, true);
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        $activeUsers = $this->CommonTable('User\Model\UserTable')->getActiveUsers();
        $users = array();
        foreach ($activeUsers as $activeUser) {
            $users[$activeUser['userID']] = $activeUser['userUsername'];
        }
        
        //get payment terms
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();

        //get customer category
        $customerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAll();
        $customerCategory = [];
        foreach ($customerCategories as $key => $value) {
            $customerCategory[$value['customerCategoryID']] = $value['customerCategoryName'];
            
        }

        return new ViewModel(array(
            'companyDetails' => $companyDetails,
            'salesPersons' => $salesPersons,
            'users' => $users,
            'locationList' => $this->allLocations,
            'paymentTerms' => $paymentTerms,
            'currentLocation' => $this->user_session->userActiveLocation['locationID'],
            'cusCategory' => $customerCategory,
        ));
    }

    public function getProductCategoryTable()
    {
        if (!$this->productCategoryTable) {
            $sm = $this->getServiceLocator();
            $this->productCategoryTable = $sm->get('Invoice\Model\ProductCategoryTable');
        }
        return $this->productCategoryTable;
    }

    public function getProductTable()
    {
        if (!$this->productTable) {
            $sm = $this->getServiceLocator();
            $this->productTable = $sm->get('Invoice\Model\ProductTable');
        }
        return $this->productTable;
    }

    public function getPaymentsTable()
    {
        if (!$this->PaymentsTable) {
            $sm = $this->getServiceLocator();
            $this->PaymentsTable = $sm->get('Invoice\Model\PaymentsTable');
        }
        return $this->PaymentsTable;
    }

    public function getSalesByInvoiceTable()
    {
        if (!$this->salesByInvoiceTable) {
            $sm = $this->getServiceLocator();
            $this->salesByInvoiceTable = $sm->get('Invoice\Model\InvoiceTable');
        }

        return new ViewModel(array(
            'companyDetails' => $companyDetails,
            'salesPersons' => $salesPersons
        ));
    }

}
