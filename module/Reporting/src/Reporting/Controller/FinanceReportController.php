<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Finance Report related actions
 */
class FinanceReportController extends CoreController
{
    protected $sideMenus  = 'accounting_side_menu';
    protected $upperMenus = 'expenses_reports_upper_menu';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->upperMenus = 'expenses_reports_upper_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Finance', 'Accounting');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/finance-report.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');
        
        $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllUnlockedFiscalPeriods(false);

        $fiscalPeriodArray = array();
        foreach ($fiscalPeriodData as $value) {
                $fiscalPeriodArray[$value['fiscalPeriodID']] = $value['fiscalPeriodStartDate']."_".$value['fiscalPeriodEndDate'];
        }

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";
        

        $financeReportView = new ViewModel(array(
            'locationList' => $this->allLocations,
            'fiscalPeriods' => $fiscalPeriodArray,
            'dimensions' => $dimensions,
        )); 
        
        return $financeReportView;
    }

}
