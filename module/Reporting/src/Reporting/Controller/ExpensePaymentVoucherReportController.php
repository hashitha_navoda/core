<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * Description of ExpensePaymentVoucherController
 *
 * @author shermilan
 */
class ExpensePaymentVoucherReportController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'expenses_reports_upper_menu';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->upperMenus = 'expenses_reports_upper_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Payment Voucher', 'Accounting');

//        get expense Type list
        $expenseTypes = $this->CommonTable('Expenses\Model\ExpenseTypeTable')->getExpeneTypes();
        $expensetypeList = [];
        foreach ($expenseTypes as $expenseType) {
            $expensetypeList[$expenseType['expenseTypeId']] = $expenseType['expenseTypeName'];
        }

        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/expense-payment-voucher-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        return new ViewModel([
            'expenseTypes' => $expensetypeList,
            'dimensions' => $dimensions,
        ]);
    }

}
