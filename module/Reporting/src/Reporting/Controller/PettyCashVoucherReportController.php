<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

/**
 * Description of PettyCashVoucherReportController
 *
 * @author shermilan <sharmilan@thinkcube.com>
 */
class PettyCashVoucherReportController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'expenses_reports_upper_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Petty Cash', 'Accounting');

        // get eaxpense type list for dropdown
        $pettyCashExpenseTypes = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getActiveExpenseTypes();
        foreach ($pettyCashExpenseTypes as $pettyCashExpenseType) {
            $range = '';
            if ($pettyCashExpenseType->pettyCashExpenseTypeMinAmount) {
                $range .= '(' . number_format($pettyCashExpenseType->pettyCashExpenseTypeMinAmount, 0) . ' < amount ';
            }
            if ($pettyCashExpenseType->pettyCashExpenseTypeMaxAmount) {
                $range.= '< ' . number_format($pettyCashExpenseType->pettyCashExpenseTypeMaxAmount);
            }
            $range.= ($pettyCashExpenseType->pettyCashExpenseTypeMinAmount || $pettyCashExpenseType->pettyCashExpenseTypeMaxAmount) ? ')' : '';
            $activePettyCashExpenseTypes[$pettyCashExpenseType->pettyCashExpenseTypeID] = $pettyCashExpenseType->pettyCashExpenseTypeName . ' ' . $range;
        }

        // get petty cash types for drop down
        $pettyCashVoucherTypes = $this->getServiceLocator()->get('config')['pettyCashVoucherTypes'];

        $view = new ViewModel([
            'pettyCashVoucherTypes' => $pettyCashVoucherTypes,
            'pettyCashExpenseTypes' => $activePettyCashExpenseTypes,
        ]);
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/petty-cash-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        return $view;
    }

}
