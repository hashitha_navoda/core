<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * Description of SalesPaymentReportController
 *
 * @author shermilan
 */
class SalesPaymentReportController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_report_upper_menu';

    function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
            $this->upperMenus = 'invoice_report_upper_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Payments', 'SALES');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/sales-payment.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/accounting.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $paymentType = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPaymentTypeData();
        $paymentTypes = [];
        foreach ($paymentType as $pt) {
            $paymentTypes[$pt['paymentMethodID']] = $pt;
        }

        //fetch card types and set them
        $cardType = $this->CommonTable('Expenses\Model\CardTypeTable')->fetchAll();
        $cardTypes = [];
        foreach ($cardType as $c) {
            $cardTypes[$c['cardTypeID']] = $c['cardTypeName'];
        }

        //Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        $bankList = $this->CommonTable('Expenses\Model\BankTable')->getBankListForDropDown();

        return new ViewModel([
            'paymentTypes' => $paymentTypes,
            'locations' => $this->allLocations,
            'currentLocation' => $this->user_session->userActiveLocation['locationID'],
            'cardTypes' => $cardTypes,
            'banks'=> $bankList,
            'salesPersons' => $salesPersons
        ]);
    }

}
