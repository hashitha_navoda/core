<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains ReconciliationReport related actions
 */
class ReconciliationReportController extends CoreController
{
    protected $sideMenus  = 'accounting_side_menu';
    protected $upperMenus = 'expenses_reports_upper_menu';
    
    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->upperMenus = 'expenses_reports_upper_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Reconciliation', 'Accounting');
        
        $company = $this->getCompanyDetails();
        
        $bankList = $this->CommonTable('Expenses\Model\BankTable')->getBankListForDropDown();
        
        return new ViewModel(array('company'=>$company,'banks'=>$bankList));
    }

}
