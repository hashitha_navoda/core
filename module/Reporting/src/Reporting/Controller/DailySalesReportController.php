<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;
use Core\BackgroundJobs\ReportJob;
use Zend\Session\Container;

/**
 * Description of Daily Sales Item Report
 *
 * @author sharmilan <sharmilan@thinkcube.com>
 */
class DailySalesReportController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_report_upper_menu';

    function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
            $this->upperMenus = 'invoice_report_upper_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Items', 'SALES');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/daily-sales.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $companyDetails = $this->getCompanyDetails();

        $locations = [];
        $allLocations = $this->allLocations;
        foreach ($allLocations as $key => $value) {
            $locations[$key] = $value['locationName'] . '-' . $value['locationCode'];
        }

        //Get sales person list
        $salesPersons = [];
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'userLocations',
        ]);

        return new ViewModel(array(
            'companyDetails' => $companyDetails,
            'locationList' => $locations,
            'salesPersons' => $salesPersons
        ));
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function viewInvoicedDailySalesItemAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $batchSerial = $request->getPost('batchSerial');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $translator = new Translator();
            $name = $translator->translate('Daily Invoice Sales Item Report');
            $period = $fromDate . ' - ' . $toDate;

            $companyDetails = $this->getCompanyDetails();
            $dailySalesItemData = $this->_getDailySalesItemsDetails($fromDate, $toDate, $batchSerial);

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailySalesView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailySalesItemData' => $dailySalesItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'batchSerial' => $batchSerial,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-invoiced-items');
            $this->html = $dailySalesView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @return array $dailySalesItemData
     */
    private function _getDailySalesItemsDetails($fromDate, $toDate, $batchSerial)
    {
        if (isset($fromDate) && isset($toDate)) {
            $dailySalesItemData = array();
            //because of mysql BETWEEN function
//            $toDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));

            $getInvoiceItemDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSalesInvoiceDetails($fromDate, $toDate);
            $getDlnWiseInvoiceItemDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getDlnWiseSalesInvoiceDetails($fromDate, $toDate);

            $getItemDetails = array_merge($getInvoiceItemDetails, $getDlnWiseInvoiceItemDetails);
            if (!empty($getItemDetails)) {
                foreach ($getItemDetails as $row) {
                    $getLocationProduct = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProducts($row['itemOutLocationProductID'], $row['locationID']);

                    $productID = $getLocationProduct->productID;
                    $productName = $getLocationProduct->productName;
                    $productCode = $getLocationProduct->productCode;

                    if (!is_null($row['itemOutBatchID'])) {
                        $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($row['itemOutBatchID']);
                    } else  {
                        $batchDetails = null;
                    }

                    if (!is_null($row['itemOutSerialID'])) {
                        $serialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($row['itemOutSerialID']);
                    } else {
                        $serialData = null;
                    }

                    if ($productID) {
                        //set quantity and uomAbbr according to display UOM
                        $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productID);
                        $thisqty = $this->getProductQuantityViaDisplayUom($row['itemOutQty'], $productUom);
                        $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                        $itemInUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemInPrice'], $productUom);
                        $itemOutUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemOutPrice'], $productUom);
                        //get product credit note details
                        $creditNoteProductDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceProductCreditNoteDetails($productID, $fromDate, $toDate);

                        if ($row['itemOutDocumentType'] == "Delivery Note") {
                            $deliveryNoteID = $row['itemOutDocumentID'];
                            $getDeliveryNoteProductTax = $this->CommonTable("Invoice\Model\DeliveryNoteProductTaxTable")->getProductTax($deliveryNoteID, $productID);
                            $itemOutTaxAmount = 0;
                            if (!empty($getDeliveryNoteProductTax)) {
                                foreach ($getDeliveryNoteProductTax as $t) {
                                    $qty = ($t['deliveryNoteProductQuantity'] == NULL) ? 1 : $t['deliveryNoteProductQuantity'];
                                    $tax = $t['deliveryNoteTaxAmount'] / $qty;
                                    $itemOutTaxAmount += $tax;
                                }
                            }
                        } else {
                            $invoiceID = $row['itemOutDocumentID'];
                            $getInvoiceProductTax = $this->CommonTable("Invoice\Model\InvoiceProductTaxTable")->getProductTax($invoiceID, $productID);
                            $itemOutTaxAmount = 0;
                            if (!empty($getInvoiceProductTax)) {
                                foreach ($getInvoiceProductTax as $t) {
                                    $qty = ($t['salesInvoiceProductQuantity'] == NULL) ? 1 : $t['salesInvoiceProductQuantity'];
                                    $tax = $t['salesInvoiceProductTaxAmount'] / $qty;
                                    $itemOutTaxAmount += $tax;
                                }
                            }
                        }

                        $itemInTaxAmount = 0;
                        if ($row['itemInDocumentType'] == "Goods Received Note") {
                            $grnID = $row['itemInDocumentID'];
                            $getGrnTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($grnID, $row['itemInLocationProductID']);
                            foreach ($getGrnTax as $g) {
                                $grnQty = ($g['grnProductTotalQty'] == NULL) ? 1 : $g['grnProductTotalQty'];
                                $itemInTaxAmount += $g['grnTaxAmount'] / $grnQty;
                            }
                        }

                        $dailySalesItemData[$productID]['productName'] = $productName;
                        $dailySalesItemData[$productID]['productCode'] = $productCode;
                        $dailySalesItemData[$productID]['creditNoteProductTotal'] = $creditNoteProductDetails['creditNoteProductTotal'];
                        $dailySalesItemData[$productID]['creditNoteProductQty'] = $creditNoteProductDetails['creditNoteProductQty'];
                        $dailySalesItemData[$productID]['creditNoteCost'] = $creditNoteProductDetails['creditNoteCost'];
                        $dailySalesItemData[$productID]['data'][$row['itemOutID']] = array(
                            'itemOutQty' => $quantity,
                            'itemOutPrice' => $itemOutUnitPrice,
                            'itemOutBatchCode' => (is_null($batchDetails)) ? null : $batchDetails->productBatchCode,
                            'itemOutSerialCode' => (is_null($serialData)) ? null : $serialData->productSerialCode,
                            'itemOutDiscount' => $row['itemOutDiscount'],
                            'itemInPrice' => $itemInUnitPrice,
                            'itemInDiscount' => $row['itemInDiscount'],
                            'uomAbbr' => $thisqty['uomAbbr'],
                            'itemOutTaxAmount' => $itemOutTaxAmount,
                            'itemInTaxAmount' => $itemInTaxAmount
                        );
                        if ((!is_null($batchDetails) || !is_null($serialData)) && $batchSerial == "true") {
                            $dailySalesItemData[$productID]['subProductData'][$row['itemOutID']] = array(
                                'itemOutQty' => $quantity,
                                'itemOutBatchCode' => (is_null($batchDetails)) ? null : $batchDetails->productBatchCode,
                                'itemOutSerialCode' => (is_null($serialData)) ? null : $serialData->productSerialCode,
                            );
                        }
                    }
                }
            }
            return $dailySalesItemData;
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * create invoice pdf
     */
    public function generateInvoicedDailySalesItemsPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $batchSerial = $request['batchSerial'];
            $translator = new Translator();
            $name = $translator->translate('Daily Invoice Sales Item Report');
            $period = $fromDate . ' - ' . $toDate;
            $companyDetails = $this->getCompanyDetails();
            $dailySalesItemData = $this->_getDailySalesItemsDetails($fromDate, $toDate, $batchSerial);

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailySalesView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailySalesItemData' => $dailySalesItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'batchSerial' => $batchSerial,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-invoiced-items');

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailySalesView);
            $this->downloadPDF('daily-sales-report', $htmlContent);

            exit;
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function generateInvoicedDailySalesItemsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $batchSerial = $request->getPost('batchSerial');
            $cD = $this->getCompanyDetails();
            $dailySalesItemData = $this->_getDailySalesItemsDetails($fromDate, $toDate, $batchSerial);

            //if data is not null
            //then generate csv
            if ($dailySalesItemData) {
                $title = '';
                $tit = 'DAILY INVOICE SALES ITEMS REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                );
                $title = implode(",", $in) . "\n";

                $title .= $cD[0]->companyName . "\n";
                $title .= $cD[0]->companyAddress . "\n";
                $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $in = array(
                    0 => "Period : " . $fromDate . '-' . $toDate
                );
                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => ""
                );
                $title.=implode(",", $in) . "\n";

                if ($batchSerial == "true") {
                    $arrs = array("Item Name", "Item Code", "Quantity", "Total Sell Price ({$this->companyCurrencySymbol})","Batch/Serial Code", "Batch/Serial Qty");
                } else {
                    $arrs = array("Item Name", "Item Code", "Quantity", "Total Sell Price ({$this->companyCurrencySymbol})");
                }

                $header = implode(",", $arrs);
                $arr = '';

                $netTotalCost = 0;
                $netTotalSales = 0;
                $isDataExist = false;
                foreach ($dailySalesItemData as $data) {
                    $totalCost = 0;
                    $totalSales = 0;
                    $totalQty = 0;
                    $uomAbbr = '';
                    $itemCreditNoteQty = ($data['creditNoteProductQty']) ? $data['creditNoteProductQty'] : 0;
                    $itemCreditNoteTotal = ($data['creditNoteProductTotal']) ? $data['creditNoteProductTotal'] : 0;
                    $itemCreditNoteCost = ($data['creditNoteCost']) ? $data['creditNoteCost'] : 0;
                    foreach ($data['data'] as $value) {
                        $qty = ($value['itemOutQty'] != NULL) ? $value['itemOutQty'] : 0;
                        $totalQty+= $qty;
                        //item in calculations
                        $itemInPrice = ($value['itemInPrice'] != NULL) ? $value['itemInPrice'] : 0;
                        $itemInDiscount = ($value['itemInDiscount'] != NULL) ? $value['itemInDiscount'] : 0;
                        $itemInTaxAmount = $value['itemInTaxAmount'];
                        $itemInActualPrice = ($itemInPrice + $itemInTaxAmount) - $itemInDiscount;
                        $totalCost+= $itemInActualPrice * $qty;
                        $netTotalCost += $itemInActualPrice * $qty;
                        //item out calculations
                        $itemOutPrice = ($value['itemOutPrice'] != NULL) ? $value['itemOutPrice'] : 0;
                        $itemOutDiscount = ($value['itemOutDiscount'] != NULL) ? $value['itemOutDiscount'] : 0;
                        $itemOutTaxAmount = $value['itemOutTaxAmount'];
                        $itemOutActualPrice = ($itemOutPrice + $itemOutTaxAmount) - $itemOutDiscount;
                        $totalSales+= $itemOutActualPrice * $qty;
                        $netTotalSales += $itemOutActualPrice * $qty;
                        $uomAbbr = $value['uomAbbr'];
                    }
                    //item credit note calculations
                    $totalQty = $totalQty - $itemCreditNoteQty;
                    $totalSales = $totalSales - $itemCreditNoteTotal;
                    $netTotalSales = $netTotalSales - $itemCreditNoteTotal;
                    $totalCost = $totalCost - $itemCreditNoteCost;
                    $netTotalCost = $netTotalCost - $itemCreditNoteCost;
                    if ($totalQty != 0) {
                        $isDataExist = true;
                        $in = array(
                            0 => $data['productName'],
                            1 => $data['productCode'],
                            2 => $totalQty . ' ' . $uomAbbr,
                            3 => str_replace(',', '', $totalSales),
                        );
                        $arr.=implode(",", $in) . "\n";
                        foreach ($data['subProductData'] as $val) {
                            $in = array(
                                0 => '',
                                1 => '',
                                2 => '',
                                3 => '',
                                4 => (is_null($val['itemOutBatchCode'])) ? $val['itemOutSerialCode'] : $val['itemOutBatchCode'],
                                5 => $val['itemOutQty'] . ' ' . $uomAbbr,
                            );
                            $arr.=implode(",", $in) . "\n";

                        }
                    }
                }
                if ($isDataExist) {
                    $in = array(
                        0 => "",
                        1 => "",
                        2 => "Grand Total:",
                        3 => str_replace(',', '', $netTotalSales),
                    );
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $arr = "no matching records found";
                }
            } else {
                $arr = "no matching records found";
            }

            $name = "Daily_Invoice_Sales_Items_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     * @param date $fromDate
     * @param date $toDate
     */
    private function _getDailySalesNoneInventoryItemsDetails($fromDate = NULL, $toDate = NULL)
    {
        if (isset($fromDate) && isset($toDate)) {
            $getNoneInventoryData = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoicedDailySalesItems($fromDate, $toDate, $locationID = \NULL, TRUE);
            $getNoneInventoryCreditNoteData = $this->CommonTable("Invoice\Model\CreditNoteTable")->getCreditNoteDailySalesNonInventoryItems($fromDate, $toDate);
            $noneInventoryData = [];
            foreach ($getNoneInventoryData as $key => $v) {
                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($v['productID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($v['salesInvoiceProductQuantity'], $productUom);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                $productPrice = ($v['salesInvoiceProductPrice'] == NULL) ? 0 : $v['salesInvoiceProductPrice'];
                $disType = ($v['salesInvoiceProductDiscountType'] == NULL) ? 0 : $v['salesInvoiceProductDiscountType'];
                $disValue = ($v['salesInvoiceProductDiscount'] == NULL) ? 0 : $v['salesInvoiceProductDiscount'];
                if($disType == 'precentage') {
                    $disPrice = ($productPrice * (100 - $disValue)) / 100;
                }else if($disType == 'value') {
                    $disPrice = $productPrice - $disValue;
                }else {
                    $disPrice = 0;
                }

                $noneInventoryData[$v['productID']]['data'][$v['salesInvoiceID']] = [
                    'qty' => $quantity,
                    'productPrice' => $productPrice,
                    'disPrice' => $disPrice
                ];
                $noneInventoryData[$v['productID']]['productName'] = $v['productName'];
                $noneInventoryData[$v['productID']]['productCode'] = $v['productCode'];
                $noneInventoryData[$v['productID']]['uomAbbr'] = $thisqty['uomAbbr'];

                foreach ($getNoneInventoryCreditNoteData as $value) {
                    
                    $thisCQty = $this->getProductQuantityViaDisplayUom($value['creditNoteProductQuantity'], $productUom);
                    $cQuantity = ($thisCQty['quantity'] == 0) ? 0 : $thisCQty['quantity'];
                    $cProductPrice = ($value['creditNoteProductPrice'] == NULL) ? 0 : $value['creditNoteProductPrice'];
                    $cDisType = ($value['creditNoteProductDiscountType'] == NULL) ? 0 : $value['creditNoteProductDiscountType'];
                    $cDisValue = ($value['creditNoteProductDiscount'] == NULL) ? 0 : $value['creditNoteProductDiscount'];
                    if($cDisType == 'precentage') {
                        $cDisPrice = ($cProductPrice * (100 - $cDisValue)) / 100;
                    }else if($cDisType == 'value') {
                        $cDisPrice = $cProductPrice - $cDisValue;
                    }else {
                        $cDisPrice = 0;
                    }

                    if ($v['productID'] == $value['productID']) {
                        $noneInventoryData[$value['productID']]['cData'][$value['creditNoteID']] = [
                            'cQty' => $cQuantity,
                            'cProductPrice' => $cProductPrice,
                            'cDisPrice' => $cDisPrice
                        ];
                    }
                }
            }
            return $noneInventoryData;
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     */
    public function viewInvoicedDailySalesNoneInventoryItemAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Invoiced Daily Sales None Inventory Item Report');
            $period = $fromDate . ' - ' . $toDate;

            $companyDetails = $this->getCompanyDetails();
            $dailySalesNoneInventoryItemData = $this->_getDailySalesNoneInventoryItemsDetails($fromDate, $toDate);

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);
            $dailySalesView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailySalesNoneInventoryItemData' => $dailySalesNoneInventoryItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-invoiced-none-inventory-items');
            $this->html = $dailySalesView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     */
    public function generateInvoicedDailySalesNoneInventoryItemPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $translator = new Translator();
            $name = $translator->translate('Invoiced Daily Sales None Inventory Item Report');
            $period = $fromDate . ' - ' . $toDate;

            $companyDetails = $this->getCompanyDetails();
            $dailySalesNoneInventroyItemData = $this->_getDailySalesNoneInventoryItemsDetails($fromDate, $toDate);

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailySalesView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailySalesNoneInventoryItemData' => $dailySalesNoneInventroyItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-invoiced-none-inventory-items');
            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailySalesView);
            $this->downloadPDF('daily-sales-report', $htmlContent);

            exit;
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     * @return return sheet
     */
    public function generateInvoicedDailySalesNoneInventoryItemSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $cD = $this->getCompanyDetails();
            $dailySalesNoneInventroyItemData = $this->_getDailySalesNoneInventoryItemsDetails($fromDate, $toDate);

            //if data is not null
            //then generate csv
            if ($dailySalesNoneInventroyItemData) {
                $title = '';
                $tit = 'Invoiced Daily Sales None Inventory Item Report';
                $in = ["", $tit, ""];
                $title = implode(",", $in) . "\n";

                $title .= $cD[0]->companyName . "\n";
                $title .= $cD[0]->companyAddress . "\n";
                $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $in = ["Period : " . $fromDate . '-' . $toDate];
                $title.=implode(",", $in) . "\n";

                $arrs = array("Item Name - Code", "Quantity", "Total Sell Price Without Discount ({$this->companyCurrencySymbol})", "Total Sell Price With Discount ({$this->companyCurrencySymbol})");
                $header = implode(",", $arrs);
                $arr = '';

                $netTotalSells = 0;
                foreach ($dailySalesNoneInventroyItemData as $data) {
                    $uomAbbr = $data['uomAbbr'];
                    $totalQty = 0;
                    $cTotalQty = 0;
                    $totalSells = 0;
                    $totalDisSells = 0;
                    $cTotalSells = 0;
                    $cTotalDisSells = 0;
                    foreach ($data['data'] as $value) {
                        $qty = ($value['qty'] != NULL) ? $value['qty'] : 0;
                        $totalQty+= $qty;
                        $productPrice = ($value['productPrice'] != NULL) ? $value['productPrice'] : 0;
                        $disPrice = ($value['disPrice'] != NULL) ? $value['disPrice'] : 0;
                        if ($qty == 0) {
                            $totalSells += $productPrice;
                            $totalDisSells += $disPrice;
                        } else {
                            $totalSells += $productPrice * $qty;
                            $totalDisSells += $disPrice * $qty;
                        }
                    }
                    $netTotalSells += $totalSells;
                    $netTotalDisSells += $totalDisSells;
                    foreach ($data['cData'] as $val) {
                        $cQty = ($val['cQty'] != NULL) ? $val['cQty'] : 0;
                        $cTotalQty+= $cQty;
                        $cProductPrice = ($val['cProductPrice'] != NULL) ? $val['cProductPrice'] : 0;
                        $cDisPrice = ($val['cDisPrice'] != NULL) ? $val['cDisPrice'] : 0;
                        if ($cQty == 0) {
                            $cTotalSells += $cProductPrice;
                            $cTotalDisSells += $cDisPrice;
                        } else {
                            $cTotalSells += $cProductPrice * $cQty;
                            $cTotalDisSells += $cDisPrice * $cQty;
                        }
                    }
                    $totalQty -= $cTotalQty;
                    $totalSells -= $cTotalSells;
                    $totalDisSells -= $cTotalDisSells;
                    $netTotalSells -= $cTotalSells;
                    $netTotalDisSells -= $cTotalDisSells;
                    if ($totalQty > 0) {
                        $totalQty = $totalQty;
                    } else {
                        $totalQty = "-";
                    }
                    $in = [$data['productName'] . '-' . $data['productCode'],
                        $totalQty . ' ' . $uomAbbr,
                        str_replace(',', '', $totalSells),
                        str_replace(',', '', $totalDisSells),
                    ];
                    $arr.=implode(",", $in) . "\n";
                }
                $in = ['', "Grand Total:",
                    str_replace(',', '', $netTotalSells),
                    str_replace(',', '', $netTotalDisSells),
                ];
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Daily_Sales_Invoiced_None_Inventory_Items_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param date $fromDate
     * @param date $toDate
     * @param int $locationID
     * @return array $data
     */
    private function _getPosDailySalesItems($fromDate, $toDate, $locationID)
    {
        if (isset($fromDate) && isset($toDate) && isset($locationID)) {
            $result = $this->CommonTable('Invoice\Model\InvoiceTable')->getPosDailySalesItems($fromDate, $toDate, $locationID);

            $i = 0;
            //adding uom and set variables
            $dailySalesItemData = array();
            $grandTotal = 0;
            foreach ($result as $row) {
                if ($row['productID']) {
                    //set quantity and uomAbbr according to display UOM
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);
                    $thisqty = $this->getProductQuantityViaDisplayUom($row['salesInvoiceProductQuantity'], $productUom);
                    $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                    $dailySalesItemData[$i]['productName'] = $row['productName'];
                    $dailySalesItemData[$i]['productCode'] = $row['productCode'];
                    $dailySalesItemData[$i]['quantity'] = $quantity;
                    $dailySalesItemData[$i]['uomAbbr'] = $thisqty['uomAbbr'];
                    $dailySalesItemData[$i]['salesInvoiceProductTotal'] = $row['salesInvoiceProductTotal'];
                    $grandTotal += $row['salesInvoiceProductTotal'];
                    $i++;
                }
            }
            $resultSet = array(
                'dailySalesItemData' => $dailySalesItemData,
                'grandTotal' => $grandTotal
            );

            return $resultSet;
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function viewPosDailySalesItemsAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $translator = new Translator();
            $name = $translator->translate('Daily POS Sales Item Report');
            $period = $fromDate . ' - ' . $toDate;

            $dailySalesItemData = $this->_getPosDailySalesItems($fromDate, $toDate, $locationID);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailySalesView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailySalesItemData' => $dailySalesItemData['dailySalesItemData'],
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($dailySalesItemData['grandTotal'], 2),
                'headerTemplate' => $headerViewRender,
            ));
            $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-items-table');

            $this->html = $dailySalesView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function generatePosDailySalesItemsPdfAction()
    {

        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $translator = new Translator();
            $name = $translator->translate('Daily POS Sales Item Report');
            $period = $fromDate . ' - ' . $toDate;
            $locationID = $this->user_session->userActiveLocation['locationID'];

            $dailySalesItemData = $this->_getPosDailySalesItems($fromDate, $toDate, $locationID);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailySalesView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailySalesItemData' => $dailySalesItemData['dailySalesItemData'],
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($dailySalesItemData['grandTotal'], 2),
                'headerTemplate' => $headerViewRender,
            ));
            $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-items-table');

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailySalesView);
            $this->downloadPDF('daily-sales-report', $htmlContent);

            exit;
        }
    }

    public function generatePosDailySalesItemsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $this->user_session->userActiveLocation['locationID'];

            $cD = $this->getCompanyDetails();
            $dailySalesItemData = $this->_getPosDailySalesItems($fromDate, $toDate, $locationID);

            //csv
            if ($dailySalesItemData['dailySalesItemData']) {
                $title = '';
                $tit = 'DAILY POS SALES ITEMS REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                );
                $title = implode(",", $in) . "\n";

                $title .= $cD[0]->companyName . "\n";
                $title .= $cD[0]->companyAddress . "\n";
                $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $in = array(
                    0 => "Period : " . $fromDate . '-' . $toDate
                );
                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => ""
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("Item Name", "Item Code", "Quantity", "Total Price");
                $header = implode(",", $arrs);
                $arr = '';
                foreach ($dailySalesItemData['dailySalesItemData'] as $row) {
                    $in = [$row['productName'],
                        $row['productCode'],
                        $row['quantity'] . ' ' . $row['uomAbbr'],
                        str_replace(',', '', $row['salesInvoiceProductTotal']),
                    ];
                    $arr.=implode(",", $in) . "\n";
                }
                $in = ["", "", "Grand Total:", $dailySalesItemData['grandTotal'],
                ];
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Daily_POS_Sales_Items_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sharmilan
     * @return type
     */
    public function viewPosAndInvoicedDailySalesItemsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $translator = new Translator();
            $name = $translator->translate('Daily POS and Invoice Sales Item Report');
            $period = $fromDate . ' - ' . $toDate;

            $result = $this->CommonTable('Invoice\Model\InvoiceTable')->getPosAndinvoiceDailySalesItems($fromDate, $toDate, $locationID);
            $creditNoteResult = $this->CommonTable('Invoice\Model\CreditNoteTable')->getPosAndcreditNoteDailyCreditNoteItems($fromDate, $toDate, $locationID);
            $companyDetails = $this->getCompanyDetails();

            $i = 0;
            //adding uom and set variables
            $dailySalesItemData = array();
            $grandTotal = 0;
            foreach ($result as $row) {
                if ($row['productID']) {
                    //set quantity and uomAbbr according to display UOM
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);
                    $thisqty = $this->getProductQuantityViaDisplayUom($row['salesInvoiceProductQuantity'], $productUom);
                    $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                    $dailySalesItemData[$i]['productName'] = $row['productName'];
                    $dailySalesItemData[$i]['productCode'] = $row['productCode'];
                    $dailySalesItemData[$i]['quantity'] = $quantity;
                    $dailySalesItemData[$i]['uomAbbr'] = $thisqty['uomAbbr'];
                    $dailySalesItemData[$i]['salesInvoiceProductTotal'] = $row['salesInvoiceProductTotal'];
                    foreach ($creditNoteResult as $value) {
                            if ($dailySalesItemData[$i]['productCode'] == $value['productCode']) {
                                $dailySalesItemData[$i]['quantity'] =  $dailySalesItemData[$i]['quantity'] - floatval($value['creditNoteProductQuantity']);
                                $dailySalesItemData[$i]['salesInvoiceProductTotal'] =  floatval($dailySalesItemData[$i]['salesInvoiceProductTotal']) - floatval($value['creditNoteProductTotal']);
                            }
                    }
                    $grandTotal += $dailySalesItemData[$i]['salesInvoiceProductTotal'];
                    $i++;
                }
            }

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailySalesView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailySalesItemData' => $dailySalesItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($grandTotal, 2),
                'headerTemplate' => $headerViewRender,
            ));
            $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-items-table');
            $this->html = $dailySalesView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function generatePosAndInvoiceDailySalesItemsPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $translator = new Translator();
            $name = $translator->translate('Daily POS and Invoice Sales Item Report');
            $period = $fromDate . ' - ' . $toDate;

            $locationID = $this->user_session->userActiveLocation['locationID'];

            $result = $this->CommonTable('Invoice\Model\InvoiceTable')->getPosAndinvoiceDailySalesItems($fromDate, $toDate, $locationID);
            $creditNoteResult = $this->CommonTable('Invoice\Model\CreditNoteTable')->getPosAndcreditNoteDailyCreditNoteItems($fromDate, $toDate, $locationID);
            $companyDetails = $this->getCompanyDetails();

            $i = 0;
            //adding uom and set variables
            $dailySalesItemData = array();
            $grandTotal = 0;
            foreach ($result as $row) {
                if ($row['productID']) {
                    //set quantity and uomAbbr according to display UOM
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);
                    $thisqty = $this->getProductQuantityViaDisplayUom($row['salesInvoiceProductQuantity'], $productUom);
                    $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                    $dailySalesItemData[$i]['productName'] = $row['productName'];
                    $dailySalesItemData[$i]['productCode'] = $row['productCode'];
                    $dailySalesItemData[$i]['quantity'] = $quantity;
                    $dailySalesItemData[$i]['uomAbbr'] = $thisqty['uomAbbr'];
                    $dailySalesItemData[$i]['salesInvoiceProductTotal'] = $row['salesInvoiceProductTotal'];
                    foreach ($creditNoteResult as $value) {
                            if ($dailySalesItemData[$i]['productCode'] == $value['productCode']) {
                                $dailySalesItemData[$i]['quantity'] =  $dailySalesItemData[$i]['quantity'] - floatval($value['creditNoteProductQuantity']);
                                $dailySalesItemData[$i]['salesInvoiceProductTotal'] =  floatval($dailySalesItemData[$i]['salesInvoiceProductTotal']) - floatval($value['creditNoteProductTotal']);
                            }
                    }
                    $grandTotal += $dailySalesItemData[$i]['salesInvoiceProductTotal'];
                    $i++;
                }
            }

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $dailySalesView = new ViewModel(array(
                'cD' => $companyDetails,
                'dailySalesItemData' => $dailySalesItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($grandTotal, 2),
                'headerTemplate' => $headerViewRender,
            ));
            $dailySalesView->setTemplate('reporting/daily-sales-report/daily-sales-items-table');

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($dailySalesView);
            $this->downloadPDF('daily-sales-report', $htmlContent);

            exit;
        }
    }

    public function generatePosAndInvoicedDailySalesItemsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $this->user_session->userActiveLocation['locationID'];

            $result = $this->CommonTable('Invoice\Model\InvoiceTable')->getPosAndinvoiceDailySalesItems($fromDate, $toDate, $locationID);
            $creditNoteResult = $this->CommonTable('Invoice\Model\CreditNoteTable')->getPosAndcreditNoteDailyCreditNoteItems($fromDate, $toDate, $locationID);
            $cD = $this->getCompanyDetails();

            $i = 0;
            //adding uom and set variables
            $dailySalesItemData = array();
            $grandTotal = 0;
            foreach ($result as $row) {
                if ($row['productID']) {
                    //set quantity and uomAbbr according to display UOM
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);
                    $thisqty = $this->getProductQuantityViaDisplayUom($row['salesInvoiceProductQuantity'], $productUom);
                    $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                    $dailySalesItemData[$i]['productName'] = $row['productName'];
                    $dailySalesItemData[$i]['productCode'] = $row['productCode'];
                    $dailySalesItemData[$i]['quantity'] = $quantity;
                    $dailySalesItemData[$i]['uomAbbr'] = $thisqty['uomAbbr'];
                    $dailySalesItemData[$i]['salesInvoiceProductTotal'] = $row['salesInvoiceProductTotal'];
                    foreach ($creditNoteResult as $value) {
                            if ($dailySalesItemData[$i]['productCode'] == $value['productCode']) {
                                $dailySalesItemData[$i]['quantity'] =  $dailySalesItemData[$i]['quantity'] - floatval($value['creditNoteProductQuantity']);
                                $dailySalesItemData[$i]['salesInvoiceProductTotal'] =  floatval($dailySalesItemData[$i]['salesInvoiceProductTotal']) - floatval($value['creditNoteProductTotal']);
                            }
                    }
                    $grandTotal += $dailySalesItemData[$i]['salesInvoiceProductTotal'];
                    $i++;
                }
            }
            //csv
            if ($dailySalesItemData) {
                $title = '';
                $tit = 'DAILY POS AND INVOICED SALES ITEMS REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                );
                $title = implode(",", $in) . "\n";

                $title .= $cD[0]->companyName . "\n";
                $title .= $cD[0]->companyAddress . "\n";
                $title .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $in = array(
                    0 => "Period : " . $fromDate . '-' . $toDate
                );
                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => ""
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("Item Name", "Item Code", "Quantity", "Total Price");
                $header = implode(",", $arrs);
                $arr = '';
                foreach ($dailySalesItemData as $row) {
                    $in = [
                        $row['productName'],
                        $row['productCode'],
                        $row['quantity'] . ' ' . $row['uomAbbr'],
                        str_replace(',', '', $row['salesInvoiceProductTotal']),
                    ];
                    $arr.=implode(",", $in) . "\n";
                }
                $in = ["", "", "Grand Total:", $grandTotal];
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Daily_POS_and_Invoiced_Sales_Items_Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateSerialItemMovementReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $invoiceIds = ($postData['invoiceIds']) ? $postData['invoiceIds'] : null;
            $productIds = ($postData['productIds']) ? $postData['productIds'] : null;
            $serialIds = ($postData['serialIds']) ? $postData['serialIds'] : null;
            $locationIds = $this->getActiveAllLocationsIds();
            
            $tempList = array();
            $dataList = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductsToMovementReport($serialIds, $invoiceIds, $productIds, $locationIds);
            foreach ($dataList as $serialProduct) {
                if (!is_null($serialProduct['deliveryNoteCode'])) {
                    $salesInvoiceCode = $serialProduct['salesInvoiceCode'].' ('.$serialProduct['deliveryNoteCode'].')';
                } else {
                    $salesInvoiceCode = $serialProduct['salesInvoiceCode'];
                }
                $temp = array();
                $temp['productName'] = $serialProduct['productName'];
                $temp['productSerialCode'] = $serialProduct['productSerialCode'];
                $temp['salesInvoiceCode'] = $salesInvoiceCode;
                $temp['salesInvoiceIssuedDate'] = $serialProduct['salesInvoiceIssuedDate'];

                if (is_null($serialProduct['productSerialWarrantyPeriod'])) {
                    $temp['productSerialWarrantyPeriod'] = '-';
                    $temp['expiryDate'] = '-';
                } else {
                    $temp['productSerialWarrantyPeriod'] = $serialProduct['productSerialWarrantyPeriod'] . ' days';
                    $exDate = new \DateTime($serialProduct['salesInvoiceIssuedDate']);
                    $exDate->add(new \DateInterval('P' . $serialProduct['productSerialWarrantyPeriod'] . 'D'));
                    $temp['expiryDate'] = $exDate->format('Y-m-d');
                }

                switch ($serialProduct['itemInDocumentType']) {
                    case 'Payment Voucher':
                        $pv = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($serialProduct['itemInDocumentID']);
                        $temp['typeId'] = $pv->current()['purchaseInvoiceCode'];
                        break;
                    case 'Goods Received Note':
                        $grn = $this->CommonTable('Inventory\Model\GrnTable')->grnDetailsByGrnId($serialProduct['itemInDocumentID']);
                        $temp['typeId'] = $grn['grnCode'];
                        break;
                    default :
                        error_log('invalid option');
                }
                array_push($tempList, $temp);
            }

            $translator = new Translator();
            $name = $translator->translate('Serial Item Movement Report');
            $period = 'All time';

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $tempList,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/daily-sales-report/serial-item-movement');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateSerialItemMovementPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $invoiceIds = ($postData['invoiceIds']) ? $postData['invoiceIds'] : null;
            $productIds = ($postData['productIds']) ? $postData['productIds'] : null;
            $serialIds = ($postData['serialIds']) ? $postData['serialIds'] : null;
            $locationIds = $this->getActiveAllLocationsIds();
            
            $tempList = array();
            $dataList = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductsToMovementReport($serialIds, $invoiceIds, $productIds, $locationIds);
            foreach ($dataList as $serialProduct) {
                if (!is_null($serialProduct['deliveryNoteCode'])) {
                    $salesInvoiceCode = $serialProduct['salesInvoiceCode'].' ('.$serialProduct['deliveryNoteCode'].')';
                } else {
                    $salesInvoiceCode = $serialProduct['salesInvoiceCode'];
                }
                $temp = array();
                $temp['productName'] = $serialProduct['productName'];
                $temp['productSerialCode'] = $serialProduct['productSerialCode'];
                $temp['salesInvoiceCode'] = $salesInvoiceCode;
                $temp['salesInvoiceIssuedDate'] = $serialProduct['salesInvoiceIssuedDate'];

                if (is_null($serialProduct['productSerialWarrantyPeriod'])) {
                    $temp['productSerialWarrantyPeriod'] = '-';
                    $temp['expiryDate'] = '-';
                } else {
                    $temp['productSerialWarrantyPeriod'] = $serialProduct['productSerialWarrantyPeriod'] . ' days';
                    $exDate = new \DateTime($serialProduct['salesInvoiceIssuedDate']);
                    $exDate->add(new \DateInterval('P' . $serialProduct['productSerialWarrantyPeriod'] . 'D'));
                    $temp['expiryDate'] = $exDate->format('Y-m-d');
                }

                switch ($serialProduct['itemInDocumentType']) {
                    case 'Payment Voucher':
                        $pv = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($serialProduct['itemInDocumentID']);
                        $temp['typeId'] = $pv->current()['purchaseInvoiceCode'];
                        break;
                    case 'Goods Received Note':
                        $grn = $this->CommonTable('Inventory\Model\GrnTable')->grnDetailsByGrnId($serialProduct['itemInDocumentID']);
                        $temp['typeId'] = $grn['grnCode'];
                        break;
                    default :
                        error_log('invalid option');
                }
                array_push($tempList, $temp);
            }

            $translator = new Translator();
            $name = $translator->translate('Serial Item Movement Report');
            $period = 'All time';

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $tempList,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/daily-sales-report/serial-item-movement');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('serial_item_movement_report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateSerialItemMovementCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $postData = $request->getPost()->toArray();

            $invoiceIds = ($postData['invoiceIds']) ? $postData['invoiceIds'] : null;
            $productIds = ($postData['productIds']) ? $postData['productIds'] : null;
            $serialIds = ($postData['serialIds']) ? $postData['serialIds'] : null;
            $locationIds = $this->getActiveAllLocationsIds();
            $cD = $this->getCompanyDetails();
            
            $tempList = array();
            $dataList = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductsToMovementReport($serialIds, $invoiceIds, $productIds, $locationIds);
            foreach ($dataList as $serialProduct) {
                if (!is_null($serialProduct['deliveryNoteCode'])) {
                    $salesInvoiceCode = $serialProduct['salesInvoiceCode'].' ('.$serialProduct['deliveryNoteCode'].')';
                } else {
                    $salesInvoiceCode = $serialProduct['salesInvoiceCode'];
                }
                $temp = array();
                $temp['productName'] = $serialProduct['productName'];
                $temp['productSerialCode'] = $serialProduct['productSerialCode'];
                $temp['salesInvoiceCode'] = $salesInvoiceCode;
                $temp['salesInvoiceIssuedDate'] = $serialProduct['salesInvoiceIssuedDate'];

                if (is_null($serialProduct['productSerialWarrantyPeriod'])) {
                    $temp['productSerialWarrantyPeriod'] = '-';
                    $temp['expiryDate'] = '-';
                } else {
                    $temp['productSerialWarrantyPeriod'] = $serialProduct['productSerialWarrantyPeriod'] . ' days';
                    $exDate = new \DateTime($serialProduct['salesInvoiceIssuedDate']);
                    $exDate->add(new \DateInterval('P' . $serialProduct['productSerialWarrantyPeriod'] . 'D'));
                    $temp['expiryDate'] = $exDate->format('Y-m-d');
                }

                switch ($serialProduct['itemInDocumentType']) {
                    case 'Payment Voucher':
                        $pv = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($serialProduct['itemInDocumentID']);
                        $temp['typeId'] = $pv->current()['purchaseInvoiceCode'];
                        break;
                    case 'Goods Received Note':
                        $grn = $this->CommonTable('Inventory\Model\GrnTable')->grnDetailsByGrnId($serialProduct['itemInDocumentID']);
                        $temp['typeId'] = $grn['grnCode'];
                        break;
                    default :
                        error_log('invalid option');
                }
                array_push($tempList, $temp);
            }

            if (!empty($tempList)) {
                $title = '';
                $in = array(
                    0 => "",
                    1 => 'SERIAL ITEM MOVEMENT REPORT',
                    2 => ""
                );
                $title = implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "Period : All time"
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("PRODUCT NAME", "PRODUCT SERIAL", "INVOICE NUMBER", "INOVICE DATE", "WARRANTY PERIOD", "WARRANTY EXPIRY DATE", "PV / GRN");
                $header = implode(",", $arrs);
                $arr = '';

                foreach ($tempList as $data) {

                    $in = array(
                        0 => $data['productName'],
                        1 => $data['productSerialCode'],
                        2 => $data['salesInvoiceCode'],
                        3 => $data['salesInvoiceIssuedDate'],
                        4 => $data['productSerialWarrantyPeriod'],
                        5 => $data['expiryDate'],
                        6 => $data['typeId'],
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $in = array('no matching records found');
                $arr.=implode(",", $in) . "\n";
            }

            $name = "serial_item_movement_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function representativeWiseItemData($postData){
        $dataList = [];
        $x = extract($postData);

        if (filter_var($isAllRepresentative, FILTER_VALIDATE_BOOLEAN)) {
            $salesPersons = $this->CommonTable("User/Model/SalesPersonTable")->fetchAll(false);
            $representativeIds = array();
            foreach ($salesPersons as $salesPerson) {
                $representativeIds[] = $salesPerson['salesPersonID'];
            }
        }
        if (filter_var($isAllItems, FILTER_VALIDATE_BOOLEAN)) {
            $items = $this->CommonTable('Inventory\Model\ProductTable')->fetchAll(false);
            $itemIds = array();
            foreach ($items as $item) {
                $itemIds[] = $item->productID;
            }
        }
        foreach ($representativeIds as $representativeId) {
            //get sales person details
            $salesPerson = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonDetailsBySalesPersonId($representativeId);
            $salesPersonName = $salesPerson['salesPersonFirstName'] . ' ' . $salesPerson['salesPersonLastName'] . ' (' . $salesPerson['salesPersonSortName'] . ')';
            $productArr = array();


            foreach ($itemIds as $itemId) {
                //get product details
                $product = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($itemId,$itemCategory);
                //get items according to sales person
                $itemList = $this->CommonTable("Invoice\Model\InvoiceProductTable")->getInvoiceProductBySalesPersonId($itemId, $representativeId, $fromDate, $toDate);

                if (!empty($itemList) && sizeof($product) > 1) {

                    $quantity = $amount = 0;
                    foreach ($itemList as $i) {
                        $quantity += ($i['salesInvoiceProductQuantity'] - $i['cNPQuantity']);
                        $amount += ($i['salesInvoiceProductTotal'] - $i['cNPTotal']);
                    }
                    if ($quantity > 0) {

                        $itemArr = array('quantity' => $quantity, 'amount' => $amount, 'proCode' => $product['productCode'], 'proCategory' => $product['categoryName']);
                        $productArr[$product['productName']] = $itemArr;
                    }
                }

            }
            if (!empty($productArr)) {
                $dataList[$salesPersonName] = $productArr;
            }
        }
        return $dataList;
    }

    public function generateRepresentativeWiseItemReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $dataList = $this->getService('SalesReportService')->representativeWiseItemData($postData);

            $translator = new Translator();
            $name = $translator->translate('Representative wise Item Report');
            $period = $postData['fromDate'] . ' - ' . $postData['toDate'];

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'currencySymbol' => $this->companyCurrencySymbol,
                'dataList' => $dataList,
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/daily-sales-report/generate-representative-wise-item');
            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateRepresentativeWiseItemPdfxAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getService('SalesReportService')->representativeWiseItemData($postData);

            $translator = new Translator();
            $name = $translator->translate('Representative wise Item Report');
            $period = $postData['fromDate'] . ' - ' . $postData['toDate'];

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'currencySymbol' => $this->companyCurrencySymbol,
                'dataList' => $dataList,
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/daily-sales-report/generate-representative-wise-item');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('representative_wise_category_item', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateRepresentativeWiseItemPdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['representative-wise-item'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    public function generateRepresentativeWiseItemCsvAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['representative-wise-item'], 'csv');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    public function generateRepresentaetiveWiseItemCsvxAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getService('SalesReportService')->representativeWiseItemData($postData);
            $cD = $this->getCompanyDetails();

            if (!empty($dataList)) {
                $title = '';
                $in = array(
                    0 => "",
                    1 => 'REPRESENTATIVE WISE ITEM REPORT',
                    2 => ""
                );
                $title = implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Period :' . $postData['fromDate'] . '-' . $postData['toDate']
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("REPRESENTATIVE NAME", "PRODUCT NAME", "PRODUCT CODE", "PRODUCT CATEGORY", "TOTAL QUANTITY", "TOTAL PRICE");
                $header = implode(",", $arrs);
                $arr = '';

                $grandTotal = 0;
                foreach ($dataList as $repName => $itemList) {
                    $in = array(
                        0 => $repName
                    );
                    $arr.=implode(",", $in) . "\n";

                    $totalAmount = 0;
                    foreach ($itemList as $itemName => $item) {
                        $in = array(
                            0 => '',
                            1 => $itemName,
                            2 => $item['proCode'],
                            3 => $item['proCategory'],
                            4 => $item['quantity'],
                            5 => number_format($item['amount'], 2, '.', '')
                        );
                        $arr.=implode(",", $in) . "\n";
                        $totalAmount = $totalAmount + $item['amount'];
                    }
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => 'Total',
                        5 => number_format($totalAmount, 2, '.', '')
                    );
                    $arr.=implode(",", $in) . "\n";
                    $grandTotal = $grandTotal + $totalAmount;
                }
                $in = array(
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => '',
                    4 => 'Grand Total',
                    5 => number_format($grandTotal, 2, '.', '')
                );
                $arr.=implode(",", $in);
            } else {
                $in = array('no matching records found');
                $arr.=implode(",", $in) . "\n";
            }

            $name = "representative_wise_item_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getRepresentativeWiseCategoryData($representativeIds, $categoryIds, $fromDate, $toDate)
    {
        $dataList = [];
        foreach ($representativeIds as $representativeId) {
            //get sales person details
            $salesPerson = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonDetailsBySalesPersonId($representativeId);
            $salesPersonName = $salesPerson['salesPersonFirstName'] . ' ' . $salesPerson['salesPersonLastName'] . ' (' . $salesPerson['salesPersonSortName'] . ')';
            $categoryArr = array();
            foreach ($categoryIds as $categoryId) {
                //get category details
                $category = $this->CommonTable('Inventory/Model/CategoryTable')->getCategory($categoryId);
                //get items according to sales person
                $categoryList = $this->CommonTable("Invoice\Model\InvoiceProductTable")->getInvoiceProductByCategoryIdAndSalesPersonId($categoryId, $representativeId, $fromDate, $toDate);
                if (!empty($categoryList)) {
                    $quantity = $amount = 0;
                    foreach ($categoryList as $i) {
                        $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($i['salesInvoiceID']);

                        $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);
                        $quantity += (($i['salesInvoiceProductQuantity'] / $numOfSP) - ($i['cNPQuantity'] / $numOfSP));
                        $amount += ( ($i['salesInvoiceProductTotal'] / $numOfSP) - ($i['cNPTotal'] / $numOfSP));
                    }
                    if ($quantity > 0) {
                        $categoryArr[$category->categoryName] = array('quantity' => $quantity, 'amount' => $amount);
                    }
                }
            }
            if (!empty($categoryArr)) {
                $dataList[$salesPersonName] = $categoryArr;
            }
        }
        return $dataList;
    }

    public function generateRepresentativeWiseCategoryReportAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $isAllRepresentative = $request->getPost('isAllRepresentative');
            $representativeIds = $request->getPost('representativeIds');
            $isAllCategories = $request->getPost('isAllCategories');
            $categoryIds = $request->getPost('categoryIds');

            if ($isAllCategories) {
                $categories = $this->CommonTable('Inventory/Model/CategoryTable')->getActiveCategories();
                $categoryIds = array();
                foreach ($categories as $category) {
                    $categoryIds[] = $category['categoryID'];
                }
            }

            $translator = new Translator();
            $name = $translator->translate('Representative wise Category Report');
            $period = $fromDate . ' - ' . $toDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'currencySymbol' => $this->companyCurrencySymbol,
                'dataList' => $this->getService('SalesReportService')->getRepresentativeWiseCategoryData($representativeIds, $categoryIds, $fromDate, $toDate),
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/daily-sales-report/generate-representative-wise-category');
            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateRepresentativeWiseCategoryPdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['representative-wise-category'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    public function generateRepresentativeWiseCategoryCsvAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['representative-wise-category'], 'csv');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     * get free issue items
     * @param date $fromDate
     * @param date $toDate
     */
    private function _getFreeIssueItems($fromDate = NULL, $toDate = NULL)
    {
        if (isset($fromDate) && isset($toDate)) {
            $getFreeIssueItems = $this->CommonTable("Invoice\Model\InvoiceTable")->getSalesInvoiceDetails($fromDate, $toDate, $freeIssueFlag = TRUE, $locIDs = []);

            $freeIssueData = [];
            foreach ($getFreeIssueItems as $v) {
                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($v['pID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($v['salesInvoiceProductQuantity'], $productUom);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                $freeIssueData[$v['pID']]['data'][$v['salesInvoiceProductID']] = [
                    'qty' => $quantity,
                    'uomAbbr' => $thisqty['uomAbbr'],
                    'invoicedDate' => $v['salesInvoiceIssuedDate'],
                    'createdUser' => $v['createdUser'],
                    'invoiceCode' => $v['salesInvoiceCode']
                ];
                $freeIssueData[$v['pID']]['productName'] = $v['pName'];
                $freeIssueData[$v['pID']]['productCode'] = $v['pCD'];
            }

            return $freeIssueData;
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * view invoiced free issued item report
     * @return JSONRespondHtml
     */
    public function viewFreeIssuedItemAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Free Issued Item Report');
            $period = $fromDate . ' - ' . $toDate;

            $companyDetails = $this->getCompanyDetails();
            $freeIssueItemData = $this->_getFreeIssueItems($fromDate, $toDate);

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $freeIssueItemView = new ViewModel(array(
                'cD' => $companyDetails,
                'freeIssueItemData' => $freeIssueItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $freeIssueItemView->setTemplate('reporting/daily-sales-report/free-issue-invoiced-items');
            $this->html = $freeIssueItemView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     * create free invoiced item pdf
     */
    public function generateFreeIssuedItemPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $translator = new Translator();
            $name = $translator->translate('Free Issued Item Report');
            $period = $fromDate . ' - ' . $toDate;
            $companyDetails = $this->getCompanyDetails();
            $freeIssueItemData = $this->_getFreeIssueItems($fromDate, $toDate);

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $freeIssueItemView = new ViewModel(array(
                'cD' => $companyDetails,
                'freeIssueItemData' => $freeIssueItemData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $freeIssueItemView->setTemplate('reporting/daily-sales-report/free-issue-invoiced-items');

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($freeIssueItemView);
            $this->downloadPDF('daily-sales-report', $htmlContent);

            exit;
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     */
    public function generateFreeIssuedItemSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $cD = $this->getCompanyDetails();
            $freeIssueItemData = $this->_getFreeIssueItems($fromDate, $toDate);

            //if data is not null
            //then generate csv
            if ($freeIssueItemData) {
                $title = '';
                $tit = 'FREE ISSUE ITEM REPORT';
                $in = ["", $tit, ""];
                $title = implode(",", $in) . "\n";

                $title .= $cD[0]->companyName . "\n";
                $title .= $cD[0]->companyAddress . "\n";
                $title .= 'Tel: ' . $cD [0]->telephoneNumber . "\n";

                $in = ["Period : " . $fromDate . '-' . $toDate];
                $title.=implode(",", $in) . "\n";

                $arrs = array("Item Name - Code", "Created User", "Invoice Code","Invoiced Date", "Quantity");
                $header = implode(",", $arrs);
                $arr = '';

                foreach ($freeIssueItemData as $data) {
                    $in = [
                        $data['productName'] . ' ' . $data['productCode'],
                    ];
                    $arr.=implode(",", $in) . "\n";

                    $totalQty = 0;
                    $uomAbbr = '';
                    foreach ($data['data'] as $value) {
                        $qty = ($value['qty'] != NULL) ? $value['qty'] : 0;
                        $totalQty+= $qty;
                        $uomAbbr = $value['uomAbbr'];
                        $in = [
                            "", $value['createdUser'], $value['invoiceCode'],
                            $value['invoicedDate'], $qty . ' ' . $uomAbbr
                        ];
                        $arr.=implode(",", $in) . "\n";
                    }
                }
            } else {
                $arr = "no matching records found\n";
            }

            $name = "Free_issued_item_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Get serial item report details
     * @param array $postData
     * @param array $locationIds
     * @return array
     */
    public function getSerialItemReportDetails($postData, $locationIds)
    {
        $productIds = empty($postData['productIds']) ? array() : $postData['productIds'];
        $serialIds = empty($postData['serialIds']) ? array() : $postData['serialIds'];

        if ($postData['isAllProducts']) {
            $products = $this->CommonTable('Inventory\Model\ProductTable')->getSerialProductList();
            foreach ($products as $product) {
                $productIds[] = $product['productID'];
            }
        }

        if ($postData['isAllSerials']) {
            $serials = $this->CommonTable('Inventory\Model\ProductSerialTable')->searchProductSreialsForDropdown(null);
            foreach ($serials as $serial) {
                $serialIds[] = $serial['productSerialID'];
            }
        }

        $tempList = array();
        $dataList = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductsDetails($serialIds, $productIds, $locationIds);
        foreach ($dataList as $serialProduct) {
            $temp = array();
            $temp['productName'] = $serialProduct['productName'];
            $temp['productSerialCode'] = $serialProduct['productSerialCode'];

            if (is_null($serialProduct['productSerialWarrantyPeriod'])) {
                $temp['productSerialWarrantyPeriod'] = '-';
                $temp['expiryDate'] = '-';
            } else {
                $temp['productSerialWarrantyPeriod'] = $serialProduct['productSerialWarrantyPeriod'] . ' days';
                $exDate = new \DateTime($serialProduct['salesInvoiceIssuedDate']);
                $exDate->add(new \DateInterval('P' . $serialProduct['productSerialWarrantyPeriod'] . 'D'));
                $temp['expiryDate'] = $exDate->format('Y-m-d');
            }

            switch ($serialProduct['itemInDocumentType']) {
                case 'Payment Voucher':
                    $pv = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($serialProduct['itemInDocumentID']);
                    $temp['typeId'] = $pv->current()['purchaseInvoiceCode'];
                    break;
                case 'Goods Received Note':
                    $grn = $this->CommonTable('Inventory\Model\GrnTable')->grnDetailsByGrnId($serialProduct['itemInDocumentID']);
                    $temp['typeId'] = $grn['grnCode'];
                    break;
                case 'Inventory Positive Adjustment':
                    $adj = (array) $this->CommonTable('Inventory\Model\GoodsIssueTable')->getGoodsIssue($serialProduct['itemInDocumentID']);
                    $temp['typeId'] = $adj['goodsIssueCode'];
                    break;
                case 'Inventory Transfer':
                    $transfer = (array) $this->CommonTable('Inventory\Model\TransferTable')->getTransferByID($serialProduct['itemInDocumentID']);
                    $temp['typeId'] = $transfer['transferCode'];
                    break;
                case 'Credit Note':
                    $transfer = (array) $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($serialProduct['itemInDocumentID'])->current();
                    $temp['typeId'] = $transfer['creditNoteCode'];
                    break;
                default :
                    error_log('invalid option');
            }
            array_push($tempList, $temp);
        }
        return $tempList;
    }

    /**
     * for generate serial item details report
     */
    public function generateSerialItemDetailReportAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        ;
        $this->data = null;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            //get active locations
            $locationIds = $this->getActiveAllLocationsIds();
            //get serial item details
            $serialItemList = $this->getSerialItemReportDetails($postData, $locationIds);

            $translator = new Translator();
            $name = $translator->translate('Serial Item Details Report');
            $period = 'All time';

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $serialItemList,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/daily-sales-report/serial-item-details');
            $this->html = $view;
            $this->status = true;
        }
        return $this->JSONRespondHtml();
    }

    /**
     * for generate serial item details pdf report
     */
    public function generateSerialItemDetailPdfAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        ;
        $this->data = null;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            //get active locations
            $locationIds = $this->getActiveAllLocationsIds();
            //get serial item details
            $serialItemList = $this->getSerialItemReportDetails($postData, $locationIds);

            $translator = new Translator();
            $name = $translator->translate('Serial Item Details Report');
            $period = 'All time';

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $serialItemList,
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/daily-sales-report/serial-item-details');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('serial_item_movement_report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
        return $this->JSONRespondHtml();
    }

    /**
     * for generate serial item details csv report
     */
    public function generateSerialItemDetailCsvAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        ;
        $this->data = null;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            //company details
            $cD = $this->getCompanyDetails();
            //get active locations
            $locationIds = $this->getActiveAllLocationsIds();
            //get serial item details
            $serialItemList = $this->getSerialItemReportDetails($postData, $locationIds);

            if (!empty($serialItemList)) {
                $title = '';
                $in = array(
                    0 => "",
                    1 => 'SERIAL ITEM DETAILS REPORT',
                    2 => ""
                );
                $title = implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "Period : All time"
                );
                $title.=implode(",", $in) . "\n";

                $headerArr = array("PRODUCT NAME", "PRODUCT SERIAL", "WARRANTY PERIOD", "WARRANTY EXPIRY DATE", "PV / GRN / Adjustment");
                $header = implode(",", $headerArr);
                $dataArr = '';

                foreach ($serialItemList as $data) {
                    $in = array(
                        0 => $data['productName'],
                        1 => $data['productSerialCode'],
                        4 => $data['productSerialWarrantyPeriod'],
                        5 => $data['expiryDate'],
                        6 => $data['typeId'],
                    );
                    $dataArr.=implode(",", $in) . "\n";
                }
            } else {
                $in = array('no matching records found');
                $dataArr.=implode(",", $in) . "\n";
            }

            $name = "serial_item_details_report";
            $csvContent = $this->csvContent($title, $header, $dataArr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
        }
        return $this->JSONRespond();
    }


    private function getDemandForecastDetails($postData)
    {
        $isAllProducts = filter_var($postData['isAllProducts'], FILTER_VALIDATE_BOOLEAN);
        $isAllCategories = filter_var($postData['isAllCategories'], FILTER_VALIDATE_BOOLEAN);
        $productIds = ($isAllProducts) ? [] : $postData['productIds'];
        $categoryIds = ($isAllCategories) ? null : $postData['categoryIds'];
        $locationIds = $postData['locationIds'];

        //get products uom list
        $uomList = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductIds($productIds);

        //get supplier list
        $suppliers = $this->CommonTable('Inventory\Model\ProductSupplierTable')->getProductSuppliersByProductIds($productIds);

        $salesProductArr = [];
        //get sales details
        $salesDetails = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProductSalesDetails($postData['date'], $productIds, $locationIds) ;
        foreach ($salesDetails as $sales) {
            if (!array_key_exists($sales['locationProductID'], $salesProductArr)) {
                $salesProductArr[$sales['locationProductID']] = [
                    'recent1' => $sales['recent1'],
                    'recent2' => $sales['recent2'],
                    'recent3' => $sales['recent3']
                ];
            } else {
                $salesProductArr[$sales['locationProductID']]['recent1'] += $sales['recent1'];
                $salesProductArr[$sales['locationProductID']]['recent2'] += $sales['recent2'];
                $salesProductArr[$sales['locationProductID']]['recent3'] += $sales['recent3'];
            }
        }
        $locationProductArr = [];
        //get location products
        $locationProducts = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProductsDetails($productIds, $locationIds, $categoryIds);
        foreach ($locationProducts as $locationProduct) {
            if (!array_key_exists($locationProduct['productID'], $locationProductArr)) {
                $locationProductArr[$locationProduct['productID']] = [
                    'productName' => $locationProduct['productName'],
                    'productCode' => $locationProduct['productCode'],
                    'suppliers'   => ($suppliers[$locationProduct['productID']]) ? implode(',', $suppliers[$locationProduct['productID']]) : '-',
                    'categoryName' => $locationProduct['categoryName'],
                    'location'    => []
                ];
            }

            $locationProductQty = $locationProduct['locationProductQuantity'];
            $avgSales = 0;
            $recentMonthSales = 0;
            $assumedQty = 0;
            $assumedDays = '0 days';
            $reorderLevel = 0;
            $location = $locationProduct['locationName'];

            if (array_key_exists($locationProduct['locationProductID'], $salesProductArr)) {
                $avgSales     = ($salesProductArr[$locationProduct['locationProductID']]['recent1']
                        + $salesProductArr[$locationProduct['locationProductID']]['recent2']
                        + $salesProductArr[$locationProduct['locationProductID']]['recent3']) / 3;
                $recentMonthSales = $salesProductArr[$locationProduct['locationProductID']]['recent1'];
                $assumedQty   = ($avgSales + $recentMonthSales) / 2;
                $assumedDays  = ($assumedQty > 0) ? round($locationProductQty / ( $assumedQty / 30 )) : 0;
                $reorderLevel = (($assumedQty - $locationProductQty) > 0) ? ($assumedQty - $locationProductQty) : 0;
            }
            $excessQty    = (($locationProductQty - $assumedQty) > 0.0) ? ($locationProductQty - $assumedQty) : 0;
            $dataSet = [
                'location'    => $location,
                'lastMonths'  => $this->getProductDisplayQuantity($avgSales, $uomList[$locationProduct['productID']]),
                'lastMonth'   => $this->getProductDisplayQuantity($recentMonthSales, $uomList[$locationProduct['productID']]),
                'assumedQty'  => $this->getProductDisplayQuantity($assumedQty, $uomList[$locationProduct['productID']]),
                'currentQty'  => $this->getProductDisplayQuantity($locationProduct['locationProductQuantity'], $uomList[$locationProduct['productID']]),
                'assumedDays' => $assumedDays .' days',
                'orderQty'    => $this->getProductDisplayQuantity($reorderLevel, $uomList[$locationProduct['productID']]),
                'costValue'   => $this->getProductReorderCost($reorderLevel, $locationProduct['defaultPurchasePrice'], $uomList[$locationProduct['productID']]),
                'excessQty'   => $this->getProductDisplayQuantity($excessQty, $uomList[$locationProduct['productID']]),
            ];
            $locationProductArr[$locationProduct['productID']]['location'][$locationProduct['locationProductID']] = $dataSet;
        }

        return $locationProductArr;
    }

    private function getLocationProductSales($locationId, $locationProductId, $fromDate, $toDate)
    {
        $invoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getAllSalesInvoicesByLocationIdAndDateRange( $locationId, $fromDate, $toDate);
        $invoiceIds = array_map(function($invoice){
            return $invoice['salesInvoiceID'];
        }, iterator_to_array($invoices));

        if(count($invoices) > 0 && $locationProductId) {
            $outProduct = $this->CommonTable("Inventory\Model\ItemOutTable")->getLocationProductItemOutQuantityDetails( 'Sales Invoice', $invoiceIds, $locationProductId);
            if($outProduct) {
                return ($outProduct['itemOutQty'] - $outProduct['itemReturnQty']);
            }
        }
        return 0;
    }

    private function getProductDisplayQuantity($quantity, $productUomList)
    {
        $displayUom = $this->getProductQuantityViaDisplayUom($quantity, $productUomList);
        return number_format( $displayUom['quantity'], $displayUom['uomDecimalPlace'], '.', '').' '.$displayUom['uomAbbr'];
    }

    private function getProductReorderCost($quantity, $purchasePrice, $productUomList)
    {
        $displayUom = $this->getProductQuantityViaDisplayUom($quantity, $productUomList);
        return number_format(($displayUom['quantity'] * $purchasePrice), 2, '.', '');
    }

    public function generateDemandForecastReportAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if (!$this->getRequest()->isPost()) {
            return $this->JSONRespondHtml();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $reportData = $this->getDemandForecastDetails($postData);

        $translator = new Translator();
        $name = $translator->translate('Demand Forecast Report');
        $period = $postData['date'] ." - ". date("Y-m-d",strtotime('+1 months', strtotime($postData['date'])));

        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $view = new ViewModel(array(
            'cD' => $this->getCompanyDetails(),
            'reportData' => $reportData,
            'headerTemplate' => $headerViewRender,
        ));
        $view->setTemplate('reporting/daily-sales-report/demand-forecast');
        $this->html = $view;
        $this->status = true;

        return $this->JSONRespondHtml();
    }

    public function generateDemandForecastPdfAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if (!$this->getRequest()->isPost()) {
            return $this->JSONRespondHtml();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $reportData = $this->getDemandForecastDetails($postData);

        $translator = new Translator();
        $name = $translator->translate('Demand Forecast Report');
        $period = $postData['date'] ." - ". date("Y-m-d",strtotime('+1 months', strtotime($postData['date'])));


        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $view = new ViewModel(array(
            'cD' => $this->getCompanyDetails(),
            'reportData' => $reportData,
            'headerTemplate' => $headerViewRender,
        ));

        $view->setTemplate('reporting/daily-sales-report/demand-forecast');
        $view->setTerminal(true);

        $htmlContent = $this->viewRendererHtmlContent($view);
        $pdfPath = $this->downloadPDF('demand_forecast_report', $htmlContent, true);

        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespondHtml();
    }

    public function generateDemandForecastCsvAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if (!$this->getRequest()->isPost()) {
            return $this->JSONRespondHtml();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $reportData = $this->getDemandForecastDetails($postData);
        $cD = $this->getCompanyDetails();

        $period = $postData['date'] ." - ". date("Y-m-d",strtotime('+1 months', strtotime($postData['date'])));

        if (!empty($reportData)) {
            $title = '';
            $in = array(
                0 => "",
                1 => 'DEMAND FORECAST REPORT',
                2 => ""
            );
            $title = implode(",", $in) . "\n";
            $in = array(
                0 => $cD[0]->companyName,
                1 => $cD[0]->companyAddress,
                2 => 'Tel: ' . $cD[0]->telephoneNumber
            );
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => "Period : ".$period
            );
            $title.=implode(",", $in) . "\n";

            $arrs = ["PRODUCT NAME", "SUPPLIERS","PRODUCT CATEGORY", "LOCATION", "AVG: SALES OF RECENT 3 MONTHS", "RECENT MONTH", "ASSUMED QUANTITY", "STOCK IN HAND", "ASSUMED DAYS", "REORDER QUANTITY", "REORDER COST","EXCESS QUANTITY"];
            $header = implode(",", $arrs);

            $arr = '';
            foreach ($reportData as $data) {
                $in = array(
                    0 => $data['productName'],
                    1 => $data['suppliers'],
                    2 => $data['categoryName']
                );
                $arr.=implode(",", $in) . "\n";

                foreach ($data['location'] as $value) {
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => $value['location'],
                        4 => $value['lastMonths'],
                        5 => $value['lastMonth'],
                        6 => $value['assumedQty'],
                        7 => $value['currentQty'],
                        8 => $value['assumedDays'],
                        9 => $value['orderQty'],
                        10 => $value['costValue'],
                        11 => $value['excessQty'],
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            }
        } else {
            $in = array('no matching records found');
            $arr.=implode(",", $in) . "\n";
        }

        $name = "demand_forecast_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespondHtml();
    }

    public function viewItemWiseSalesReportAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if (!$this->getRequest()->isPost()) {
            return $this->JSONRespondHtml();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $products = $postData['products'];
        $isAllProducts = $postData['isAllProducts'];
        $locations = $postData['locations'];
        $isAllLocation = $postData['isAllLocation'];
        $fromDate = $postData['fromDate'];
        $endDate = $postData['endDate'];
        $reportData = $this->getItemWiseSalesReportData($products, $isAllProducts, $locations, $isAllLocation, $fromDate, $endDate);

        $translator = new Translator();
        $name = $translator->translate('Item Wise Sales Report');

        $headerView = $this->headerViewTemplate($name, $period = null);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $view = new ViewModel(array(
            'cD' => $this->getCompanyDetails(),
            'reportData' => $reportData,
            'headerTemplate' => $headerViewRender,
        ));
        $view->setTemplate('reporting/daily-sales-report/generate-item-wise-sales-report');
        $this->html = $view;
        $this->status = true;

        return $this->JSONRespondHtml();
    }

    //genarate item wise sales report pdf
    public function generateItemWiseSalesReportPdfAction(){
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if (!$this->getRequest()->isPost()) {
            return $this->JSONRespondHtml();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $products = $postData['products'];
        $isAllProducts = $postData['isAllProducts'];
        $locations = $postData['locations'];
        $isAllLocation = $postData['isAllLocation'];
        $fromDate = $postData['fromDate'];
        $endDate = $postData['endDate'];

        $reportData = $this->getItemWiseSalesReportData($products, $isAllProducts, $locations, $isAllLocation, $fromDate, $endDate);

        $translator = new Translator();
        $name = $translator->translate('Item Wise Sales Report');

        $headerView = $this->headerViewTemplate($name, $period = null);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $view = new ViewModel(array(
            'cD' => $this->getCompanyDetails(),
            'reportData' => $reportData,
            'headerTemplate' => $headerViewRender,
        ));
        $view->setTemplate('reporting/daily-sales-report/generate-item-wise-sales-report');
        $view->setTerminal(true);

        $htmlContent = $this->viewRendererHtmlContent($view);
        $pdfPath = $this->downloadPDF('item_wise_sales_report', $htmlContent, true);

        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespondHtml();
    }

     public function generateItemWiseSalesReportCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $products = $request->getPost('products');
            $isAllProducts = $request->getPost("isAllProducts");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $itemSalesData = $this->getItemWiseSalesReportData($products, $isAllProducts, $locations, $isAllLocation, $fromDate, $endDate);
            $cD = $this->getCompanyDetails();

            if ($itemSalesData) {
                $title = '';
                $tit = 'ITEM WISE SALES REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => $tit,
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => "",
                    9 => "",
                    10 => "",
                    11 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("ITEM CODE", "ITEM NAME", "INVOICE NO", "INVOICE COMMENT", "INVOICE DATE", "CUSTOMER NAME", "ITEM QUANTITY", "ITEM PRICE", "ITEM DISCOUNT", "DISCOUNTED ITEM PRICE", "TOTAL AMOUNT", "CUMILATIVE AMOUNT");
                $header = implode(",", $arrs);
                $arr = '';

                if (count($itemSalesData) > 0) {
                    $totalQty = 0;
                    $totalAmt = 0;
                    $cumulativeBalance = 0;
                    foreach ($itemSalesData as $c) {
                        $itemWiseQty = 0;
                        $itemWiseAmt = 0;
                        $itemWiseCumilative = 0;
                        $in = array(
                            0 => $c['productCode'],
                            1 => $c['productName'],
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => '',
                            8 => '',
                            9 => '',
                            10 => '',
                            11 => '',
                        );
                        $arr.=implode(",", $in) . "\n";
                        foreach ($c['invoice'] as $inv) {
                            $cumulativeBalance += $inv['productTotal'];
                            $totalQty += $inv['productQuantity'];
                            $totalAmt += $inv['productTotal'];
                            $itemWiseQty += $inv['productQuantity'];
                            $itemWiseAmt += $inv['productTotal'];
                            $itemWiseCumilative += $inv['productTotal'];
                            $in = array(
                                0 => '',
                                1 => '',
                                2 => $inv['invoiceNO'],
                                3 => $inv['invoiceComment'],
                                4 => $inv['invoiceDate'],
                                5 => $inv['customerName'],
                                6 => $inv['productQuantity'],
                                7 => $inv['productPrice'],
                                8 => $inv['productDiscount'],
                                9 => $inv['productDiscountedPrice'],
                                10 => $inv['productTotal'],
                                11 => $itemWiseCumilative,
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => $itemWiseQty,
                            7 => '',
                            8 => '',
                            9 => '',
                            10 => $itemWiseAmt,
                            11 => $itemWiseCumilative,
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => $totalQty,
                        7 => '',
                        8 => '',
                        9 => '',
                        10 => $totalAmt,
                        11 => $cumulativeBalance,
                        );
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $arr = 'No Related Data for Customer Details';
                }

                $name = "item_wise_sales_report";
                $csvContent = $this->csvContent($title, $header, $arr);
                $csvPath = $this->generateCSVFile($name, $csvContent);

                $this->data = $csvPath;
                $this->status = true;
                return $this->JSONRespond();
            }
        }
    }

    //get customer wise sales report data
    public function getItemWiseSalesReportData($products = null, $isAllProducts = false, $locations = null, $isAllLocation = false, $formDate = null, $endDate = null )
    {
        if (isset($products)) {
            $itemData = $this->CommonTable('Inventory\Model\ProductTable')->getItemWiseSalesReport($products, $isAllProducts, $locations, $isAllLocation, $formDate, $endDate );
            $itemDetails = array();
            $invoice = array();
            foreach ($itemData as $c) {
                $creditNoteProduct = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductByInvoiceProductID($c['salesInvoiceProductID']);
                $creditNoteProductQty = 0;
                $creditNoteProductTotal = 0;
                if (count($creditNoteProduct) > 0) {
                    foreach ($creditNoteProduct as $key => $value) {
                        $creditNoteProductQty += $value['creditNoteProductQuantity'];
                        $creditNoteProductTotal += $value['creditNoteProductTotal'];
                    }
                }

                $discountedAmount = 0;
                if($c['salesInvoiceProductDiscountType'] == 'precentage'){
                    $discountedAmount = $c['salesInvoiceProductPrice']*(100 - $c['salesInvoiceProductDiscount'])/100;
                } else if($c['salesInvoiceProductDiscountType'] == 'value') {
                    $discountedAmount = $c['salesInvoiceProductPrice'] - $c['salesInvoiceProductDiscount'];
                }

                $invoice[$c['productID']][$c['salesInvoiceProductID']]['invoiceNO'] =  $c['salesInvoiceCode'];
                $invoice[$c['productID']][$c['salesInvoiceProductID']]['invoiceComment'] =  $c['salesInvoiceComment'];
                $invoice[$c['productID']][$c['salesInvoiceProductID']]['invoiceDate'] =  $c['salesInvoiceIssuedDate'];
                $invoice[$c['productID']][$c['salesInvoiceProductID']]['customerName'] =  $c['customerName'];
                $invoice[$c['productID']][$c['salesInvoiceProductID']]['productPrice'] =  $c['salesInvoiceProductPrice'];
                $invoice[$c['productID']][$c['salesInvoiceProductID']]['productDiscountedPrice'] = $discountedAmount;
                $invoice[$c['productID']][$c['salesInvoiceProductID']]['productDiscount'] =  $c['salesInvoiceProductDiscount'];
                $invoice[$c['productID']][$c['salesInvoiceProductID']]['productDiscountType'] =  $c['salesInvoiceProductDiscountType'];
                $invoice[$c['productID']][$c['salesInvoiceProductID']]['productQuantity'] +=  $c['salesInvoiceProductQuantity'] -  $creditNoteProductQty;
                $invoice[$c['productID']][$c['salesInvoiceProductID']]['productTotal'] += $c['salesInvoiceProductTotal'] - $creditNoteProductTotal;
                $itemDetails[$c['productID']]['productName'] = $c['productName'];
                $itemDetails[$c['productID']]['productCode'] = $c['productCode'];
                $itemDetails[$c['productID']]['invoice'] = $invoice[$c['productID']];
            }
            return $itemDetails;
        }
    }

    private function getRepresentativeWiseItemWithInvoiceDetails($data)
    {
        $representativeIds = $data['representativeIds'];
        $itemIds = $data['itemIds'];

        $dataList = [];
        $withDefaultSalesPerson = (in_array('other', $representativeIds)) ? true : false;
        if (filter_var($data['isAllItems'], FILTER_VALIDATE_BOOLEAN)) {
            $items = $this->CommonTable('Inventory\Model\ProductTable')->fetchAll(false);
            $itemIds = [];
            foreach ($items as $item) {
                $itemIds[] = $item->productID;
            }
        }

        //get all salesInvoice Product details with salesPerson
        $allDetails = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getAllInvoiceProductBySalesPersonIdsAndProIDs($itemIds,$representativeIds,$data['fromDate'], $data['toDate'], $withDefaultSalesPerson);

        //get all item taxes with their product details
        $allDetailsWithTax = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getAllInvoiceProductWithTaxeDetailsBySalesPersonIdsAndProIDs($itemIds,$representativeIds,$data['fromDate'], $data['toDate'], $withDefaultSalesPerson);

        //get all credit note product details with salesInvoice and sales persons.
        $creditNoteDetails = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getAllInvoiceProductRelatedCreditNoteProductByproIDAndSalesPerson($itemIds,$representativeIds,$data['fromDate'], $data['toDate'], $withDefaultSalesPerson);

        //get creditNote tax values that given details
        $creditNoteTaxes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTaxesBySalesPersonIdsAndProIDs($itemIds,$representativeIds,$data['fromDate'], $data['toDate'], $withDefaultSalesPerson);

        //re arrange creditNote Tax details in to the array
        $creditNoteTaxDetails = [];
        foreach ($creditNoteTaxes as $key => $value) {
            $creditNoteTaxDetails[$value['salesInvoiceCode']] = $value['totalCreditNoteTax'];

        }

        //re arrange sales invoice product details with salesPersonID and product ID
        $finalDataArray = [];
        $invoiceIds = [];
        $invoiceTaxDataSet = [];
        foreach ($allDetailsWithTax as $key => $value) {
            $salesPersonID = (is_null($value['salesPersonID'])) ? 'other' : $value['salesPersonID'];
            if ($value['taxType'] == "c") {
                $invoiceTaxDataSet[$salesPersonID][$value['salesInvoiceProductID']]['compoundTax'] += $value['salesInvoiceProductTaxAmount'];
            } else {
                $invoiceTaxDataSet[$salesPersonID][$value['salesInvoiceProductID']]['normalTax'] += $value['salesInvoiceProductTaxAmount'];
            }
            $invoiceTaxDataSet[$salesPersonID][$value['salesInvoiceProductID']]['totalTax'] += $value['salesInvoiceProductTaxAmount'];
        }

        foreach ($allDetails as $key => $singleData) {
            $salesPersonID = (is_null($singleData['salesPersonID'])) ? 'other' : $singleData['salesPersonID']; 

            $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($singleData['salesInvoiceID']);

            $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);
            $invoiceDSet = array(
                'invoiceCode' => $singleData['salesInvoiceCode'],
                'invoiceDate' => $singleData['salesInvoiceIssuedDate'],
                'customerName' => $singleData['customerName']. " [". $singleData['customerCode'] . "]",
                'salesinvoiceAmount' => round(($singleData['salesinvoiceTotalAmount'] / $numOfSP), 2),
                'creditNoteAmount' => round(($singleData['creditNoteTotal'] / $numOfSP),2),
                'salesInvoicePaidAmount' => $singleData['salesInvoicePayedAmount'],
                'salesInvoiceRemainingAmount' => ($singleData['salesinvoiceTotalAmount'] / $numOfSP) - ($singleData['salesInvoicePayedAmount'] / $numOfSP),
                );

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['quantity'] += ($singleData['salesInvoiceProductQuantity'] != 0) ? round(($singleData['salesInvoiceProductQuantity'] / $numOfSP), 2) : 0;

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['invoiceData'][$singleData['salesInvoiceCode']] = $invoiceDSet;

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['invoiceDataForTax'][$singleData['salesInvoiceCode']]['taxWithOutItemTotal'] += ((floatval($singleData['salesInvoiceProductTotal']) / $numOfSP) - (floatval($invoiceTaxDataSet[$salesPersonID][$singleData['salesInvoiceProductID']]['totalTax'])) / $numOfSP);

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['invoiceDataForTax'][$singleData['salesInvoiceCode']]['normalTaxValueForItem'] += (floatval($invoiceTaxDataSet[$salesPersonID][$singleData['salesInvoiceProductID']]['normalTax'])) / $numOfSP;
            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['invoiceDataForTax'][$singleData['salesInvoiceCode']]['compoundTaxValueForItem'] += (floatval($invoiceTaxDataSet[$salesPersonID][$singleData['salesInvoiceProductID']]['compoundTax'])) / $numOfSP;

            $finalDataArray['taxAmounts'][$salesPersonID][$singleData['salesInvoiceCode']] += ($invoiceTaxDataSet[$salesPersonID][$singleData['salesInvoiceProductID']]['totalTax'] / $numOfSP);

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['amount'] += round(($singleData['salesInvoiceProductTotal'] / $numOfSP), 2);

            $finalDataArray[$salesPersonID]['name'] =  ($salesPersonID == "other") ? "Others": $singleData['salesPersonFirstName'] . ' ' . $singleData['salesPersonLastName'] . ' (' . $singleData['salesPersonSortName'] . ')';

            $finalDataArray[$salesPersonID]['data'][$singleData['productID']]['proName'] = $singleData['productName'];

            $invoiceIds[] = $singleData['salesInvoiceID'];
        }


        //getCreditNote total by invoiceIDs
        $creditDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceIDArray(array_unique($invoiceIds));
        $creditNoteTotalWithSalesInvoice = [];
        foreach ($creditDetails as $key => $creditValue) {
            $creditNoteTotalWithSalesInvoice[$creditValue['salesInvoiceCode']] = $creditValue['creditNoteTot'];
        }

        //re-arrange creditNote productDetials
        foreach ($creditNoteDetails as $key => $singleCreditNote) {
            $salesPersonID = (is_null($singleCreditNote['salesPersonID'])) ? 'other' : $singleCreditNote['salesPersonID']; 
            $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($singleData['salesInvoiceID']);

            $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);
            $finalDataArray[$salesPersonID]['data'][$singleCreditNote['productID']]['quantity'] -=  round(($singleCreditNote['creditNoteProductQuantity'] / $numOfSP), 2);
            $finalDataArray[$salesPersonID]['data'][$singleCreditNote['productID']]['amount'] -= round(($singleCreditNote['creditNoteProductTotal'] / $numOfSP), 2);
            $finalDataArray[$salesPersonID]['data'][$singleCreditNote['productID']]['invoiceData'][$singleCreditNote['salesInvoiceCode']]['creditNoteAmount'] = ($creditNoteTotalWithSalesInvoice[$singleCreditNote['salesInvoiceCode']] / $numOfSP);
            $finalDataArray[$salesPersonID]['data'][$singleCreditNote['productID']]['invoiceData'][$singleCreditNote['salesInvoiceCode']]['creditNoteTaxAmount'] = $creditNoteTaxDetails[$singleCreditNote['salesInvoiceCode']] / $numOfSP;
        }

        return $finalDataArray;

    }

    public function generateRepresentativeWiseItemWithInvoiceReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()){
            $postData = $request->getPost()->toArray();
            $dataList = $this->getRepresentativeWiseItemWithInvoiceDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Representative Wise Item Report With Invoice Details');
            $period = $postData['fromDate'] . ' - ' . $postData['toDate'];

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'currencySymbol' => $this->companyCurrencySymbol,
                'dataList' => $dataList,
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/daily-sales-report/generate-representative-wise-item-with-invoice-details');
            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateRepresentativeWiseItemWithInvoicePdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }
            $postData = $request->getPost()->toArray(); // get report request data
            $config = $this->getServiceLocator()->get('config');
            $meta = $this->getReportJobCallbackData($config['report']['representative-wise-item-with-invoice'], 'pdf');
            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array
            $result = ReportJob::add($jobDetails); // add to queue
            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }
            return $this->returnJsonSuccess($result, 'Report has been added to queue.');
        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    public function generateRepresentativeWiseItemWithInvoiceCsvAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }
            $postData = $request->getPost()->toArray(); // get report request data
            $config = $this->getServiceLocator()->get('config');
            $meta = $this->getReportJobCallbackData($config['report']['representative-wise-item-with-invoice'], 'csv');
            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array
            $result = ReportJob::add($jobDetails); // add to queue
            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
                            }
            return $this->returnJsonSuccess($result, 'Report has been added to queue.');
        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }


    public function generateInvoicedBatchSerialItemDetailsReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $invoiceIds = ($postData['invoiceIds']) ? $postData['invoiceIds'] : null;
            $productIds = ($postData['productIds']) ? $postData['productIds'] : null;
            $locationIds = $this->getActiveAllLocationsIds();
            $dataList = $this->getInvoicedBatchAndSerialItemData($invoiceIds, $productIds, $locationIds);

            $translator = new Translator();
            $name = $translator->translate('Invoiced Batch And Serial Item Details Report');
            $period = 'All time';
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $dataList,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/daily-sales-report/invoiced-batch-serial-item-details');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

     public function generateInvoicedBatchSerialItemDetailsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $invoiceIds = ($postData['invoiceIds']) ? $postData['invoiceIds'] : null;
            $productIds = ($postData['productIds']) ? $postData['productIds'] : null;
            $locationIds = $this->getActiveAllLocationsIds();
            $dataList = $this->getInvoicedBatchAndSerialItemData($invoiceIds, $productIds, $locationIds);
            
            $translator = new Translator();
            $name = $translator->translate('Invoiced Batch And Serial Item Details Report');
            $period = 'All time';

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $dataList,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/daily-sales-report/invoiced-batch-serial-item-details');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('invoiced_batch_serial_item_details_report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateInvoicedBatchSerialItemDetailsCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $postData = $request->getPost()->toArray();

            $invoiceIds = ($postData['invoiceIds']) ? $postData['invoiceIds'] : null;
            $productIds = ($postData['productIds']) ? $postData['productIds'] : null;
            $locationIds = $this->getActiveAllLocationsIds();
            $cD = $this->getCompanyDetails();
            $dataList = $this->getInvoicedBatchAndSerialItemData($invoiceIds, $productIds, $locationIds);

            if (!empty($dataList)) {
                $title = '';
                $in = array(
                    0 => "",
                    1 => 'INVOICED BATCH AND SERIAL ITEM DETAILS REPORT',
                    2 => ""
                );
                $title = implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "Period : All time"
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("PRODUCT NAME", "PRODUCT SERIAL CODE/ PRODUCT BATCH CODE", "INVOICE NUMBER", "INOVICE DATE", "WARRANTY PERIOD", "WARRANTY EXPIRY DATE", "PV / GRN");
                $header = implode(",", $arrs);
                $arr = '';

                foreach ($dataList as $data) {

                    $in = array(
                        0 => $data['productName'],
                        1 => $data['productBatchSerialCode'],
                        2 => $data['salesInvoiceCode'],
                        3 => $data['salesInvoiceIssuedDate'],
                        4 => $data['productWarrantyPeriod'],
                        5 => $data['expiryDate'],
                        6 => $data['typeId'],
                    );
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $in = array('no matching records found');
                $arr.=implode(",", $in) . "\n";
            }

            $name = "serial_item_movement_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    //get data for invoiced batch and serial item detail report
    public function getInvoicedBatchAndSerialItemData($invoiceIds, $productIds, $locationIds)
    {
        $dataList = $this->CommonTable('Invoice\Model\InvoiceTable')->getBatchAndSerialProductsToInvoicedBatchSerialItemDetailsReport($invoiceIds, $productIds, $locationIds);
        
        $tempList = array();
        foreach ($dataList as $batchSerialProduct) {
            if (!is_null($batchSerialProduct['deliveryNoteCode'])) {
                $salesInvoiceCode = $batchSerialProduct['salesInvoiceCode'].' ('.$batchSerialProduct['deliveryNoteCode'].')';
            } else {
                $salesInvoiceCode = $batchSerialProduct['salesInvoiceCode'];
            }
            $temp = array();
            $temp['productName'] = $batchSerialProduct['productName'];
            $temp['productBatchSerialCode'] = (is_null($batchSerialProduct['productSerialCode'])) ? $batchSerialProduct['productBatchCode'] : $batchSerialProduct['productSerialCode'];
            $temp['salesInvoiceCode'] = $salesInvoiceCode;
            $temp['salesInvoiceIssuedDate'] = $batchSerialProduct['salesInvoiceIssuedDate'];

            if (is_null($batchSerialProduct['salesInvoiceSubProductWarranty'])) {
                $temp['productWarrantyPeriod'] = '-';
                $temp['expiryDate'] = '-';
            } else {           
                $wTypeStr='';
                if($batchSerialProduct['salesInvoiceSubProductWarrantyType']== 1){
                    $wTypeStr= 'days';
                    $exDate = new \DateTime($batchSerialProduct['salesInvoiceIssuedDate']);
                    $exDate->add(new \DateInterval('P' . $batchSerialProduct['salesInvoiceSubProductWarranty'] . 'D'));   
                }else if($batchSerialProduct['salesInvoiceSubProductWarrantyType'] == 2){
                    $wTypeStr= 'weeks';
                    $exDate = new \DateTime($batchSerialProduct['salesInvoiceIssuedDate']);
                    $exDate->add(new \DateInterval('P' . $batchSerialProduct['salesInvoiceSubProductWarranty'] . 'W'));
                }else if($batchSerialProduct['salesInvoiceSubProductWarrantyType'] == 3){
                    $wTypeStr= 'months';
                    $exDate = new \DateTime($batchSerialProduct['salesInvoiceIssuedDate']);
                    $exDate->add(new \DateInterval('P' . $batchSerialProduct['salesInvoiceSubProductWarranty'] . 'M'));
                }else if($batchSerialProduct['salesInvoiceSubProductWarrantyType'] == 4){
                    $wTypeStr= 'years';
                    $exDate = new \DateTime($batchSerialProduct['salesInvoiceIssuedDate']);
                    $exDate->add(new \DateInterval('P' . $batchSerialProduct['salesInvoiceSubProductWarranty'] . 'Y'));
                };

                $temp['productWarrantyPeriod'] = $batchSerialProduct['salesInvoiceSubProductWarranty'].$wTypeStr ;
                $temp['expiryDate'] = $exDate->format('Y-m-d');
            }

            if (!is_null($batchSerialProduct['serialItemInDocumentType'])) {
                switch ($batchSerialProduct['serialItemInDocumentType']) {
                    case 'Payment Voucher':
                        $pv = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($batchSerialProduct['serialItemInDocumentID']);
                        $temp['typeId'] = $pv->current()['purchaseInvoiceCode'];
                        break;
                    case 'Goods Received Note':
                        $grn = $this->CommonTable('Inventory\Model\GrnTable')->grnDetailsByGrnId($batchSerialProduct['serialItemInDocumentID']);
                        $temp['typeId'] = $grn['grnCode'];
                        break;
                    case 'Expense Payment Voucher':
                        $paymentVoucher = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherByPaymentVoucherID($batchSerialProduct['serialItemInDocumentID']);
                        $temp['typeId'] = $paymentVoucher->paymentVoucherCode;
                        break;
                    default :
                        error_log('invalid option');
                }
            }

            if (!is_null($batchSerialProduct['batchItemInDocumentType'])) {
                switch ($batchSerialProduct['batchItemInDocumentType']) {
                    case 'Payment Voucher':
                        $pv = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($batchSerialProduct['batchItemInDocumentID']);
                        $temp['typeId'] = $pv->current()['purchaseInvoiceCode'];
                        break;
                    case 'Goods Received Note':
                        $grn = $this->CommonTable('Inventory\Model\GrnTable')->grnDetailsByGrnId($batchSerialProduct['batchItemInDocumentID']);
                        $temp['typeId'] = $grn['grnCode'];
                        break;
                    case 'Expense Payment Voucher':
                        $paymentVoucher = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherByPaymentVoucherID($batchSerialProduct['batchItemInDocumentID']);
                        $temp['typeId'] = $paymentVoucher->paymentVoucherCode;
                        break;
                    default :
                        error_log('invalid option');
                }
            }
            array_push($tempList, $temp);
        }

        return $tempList;
    }

    /**
     * This function is used to generate invoiced daily sales item with cost view report
     * @param JSON Request
     * @return JSON Respond
     */
    public function viewInvoicedDailySalesItemWithCostAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $dailySalesView = $this->getService('DailySalesReportService')->getDataForViewInvoiceDailySalesItemWithCostReport($request->getPost());
            $this->html = $dailySalesView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * This function is used to generate invoiced daily sales item with cost pdf report
     * @param JSON Request
     * @return JSON Respond
     */
    public function generateInvoicedDailySalesItemsWithCostPdfAction()
    {
        $request = $this->getRequest();
        if ($request->getPost()) {
            $htmlContent = $this->getService('DailySalesReportService')->getDataForViewInvoiceDailySalesItemWithCostPdfReport($request->getPost());
            
            $pdfPath = $this->downloadPDF('daily-sales-item-with-cost-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond(); 
        }
    }

    /**
     * This function is used generate invoiced daily sales item with cost csv report
     * @param JSON Requset
     * @return JSON Respond
     */
    public function generateInvoicedDailySalesItemsWithCostSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $csvContent = $this->getService('DailySalesReportService')->getDataForViewInvoiceDailySalesItemWithCostCsvReport($request->getPost());
            $name = "Daily_Invoice_Sales_Items_With_Cost_Report";
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * This function is used generate Invoiced Sub Item Based Bom Product Report View
     * @param JSON Requset
     * @return JSON Respond
     */
    public function generateInvoicedSubItemBasedBomProductReportAction()
    {
       $request = $this->getRequest();
        if ($request->isPost()) {
            $dailySalesView = $this->getService('DailySalesReportService')->generateInvoicedSubItemBasedBomProductReport($request->getPost());
            $this->html = $dailySalesView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }


    /**
     * This function is used generate Invoiced Sub Item Based Bom Product Report Pdf report
     * @param JSON Requset
     * @return JSON Respond
     */
    public function generateInvoicedSubItemBasedBomProductReportPdfAction()
    {
        $request = $this->getRequest();
        if ($request->getPost()) {
            $view = $this->getService('DailySalesReportService')->generateInvoicedSubItemBasedBomProductPdfReport($request->getPost());

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('invoiced-non-inventory-bom-product-details-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond(); 
        }
    }

    /**
     * This function is used generate Invoiced Sub Item Based Bom Product Report csv report
     * @param JSON Requset
     * @return JSON Respond
     */
    public function generateInvoicedSubItemBasedBomProductDetilsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->getPost()) {
            $csvContent = $this->getService('DailySalesReportService')->generateInvoicedSubItemBasedBomProductCsvReport($request->getPost());
            $name = "Invoiced_Non_Inventory_Bom_Product_Details_Report";
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

            



}
