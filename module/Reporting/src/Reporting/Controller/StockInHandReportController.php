<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains Stock Report related controller functions
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class StockInHandReportController extends CoreController
{

    protected $userRoleID;
    protected $allLocations;
    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'inve_report_upper_menu';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'inventory_side_menu'.$this->packageID;
            $this->upperMenus = 'inve_report_upper_menu'.$this->packageID;
        }
    }


    /**
     * @author SANDUN <sandun@thinkcube.com>
     * StockInHand Report main tab
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Stock In Hand', 'INVENTORY');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/stock-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/accounting.js');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['location']);
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $catgry = array();
        $allLocations = $this->allLocations;

        foreach ($allLocations as $key => $value) {
            $locIDs[] = $key;
            $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
        }

        // if admin
        $catgrys = $this->CommonTable('Inventory/Model/CategoryTable')->actionFetchAll();
        foreach ($catgrys as $c) {
            $catgry[$c['categoryID']] = $c['categoryName'];
        }

        return new ViewModel(array(
            'catgryList' => $catgry,
            'locNames' => $locNames
                )
        );
    }

}
