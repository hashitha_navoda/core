<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains supplier Report related controller functions
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class PurchaseReportController extends CoreController
{

    protected $userRoleID;
    protected $allLocations;
    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'purchasing_report_upper_menu';

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Supplier Report main tab
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Purchase', 'PURCHASING');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/purchase-report.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $this->setLogMessage("PO report create page accessed.");
        return new ViewModel();
    }

}
