<?php

/**
 * @author Yashora <yashora@thinkcube.com>
 * This is the controller file for payment voucher reports
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;

class PaymentVoucherReportController extends CoreController
{

    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'purchasing_report_upper_menu';

    function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->userRoleID = $this->user_session->userActiveLocation['roleID'];
        $this->allLocations = $this->user_session->userAllLocations;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'purchasing_side_menu'.$this->packageID;
            $this->upperMenus = 'purchasing_report_upper_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'PURCHASING', 'PURCHASING');
        $this->getSideAndUpperMenus('Reports', 'Purchase Invoice');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/payment-voucher-report.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $location = $this->CommonTable('Settings\Model\LocationTable')->getLocationName();

        foreach ($location as $row) {
            $locationWise[$row['locationName']] = $row;
        }
        $companyDetails = $this->getCompanyDetails();

        $this->setLogMessage("Purchase invoice report create page accessed.");
        return new ViewModel(array(
            'locList' => $locationWise,
            'com_data' => $companyDetails,
            'packageID' => $this->packageID
        ));
    }

}
