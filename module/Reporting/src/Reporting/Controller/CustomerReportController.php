<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains customer transaction Report PDF related controller functions
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use DOMPDFModule\View\Model\PdfModel;
use Zend\Session\Container;

/**
 * @author SANDUN <sandun@thinkcube.com>
 * customerRport controller class create
 */
class CustomerReportController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_report_upper_menu';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
            $this->upperMenus = 'invoice_report_upper_menu'.$this->packageID;
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * customer transaction report creted in this method
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Customer', 'SALES');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/customer-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/accounting.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'userLocations',
        ]);
        
        $allLocations = $this->allLocations;
        foreach ($allLocations as $key => $value) {
            $locNames[] = $value['locationName'] . '-' . $value['locationCode'];
        }

        //get CustomerCategory list
        $customerCategory = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();

        //Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        return new ViewModel(
            array(
                'locNames' => $locNames,
                'cusCategory' => $customerCategory,
                'salesPersons'=> $salesPersons,
                ));
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return \DOMPDFModule\View\Model\PdfModel
     */
    public function generateDailyCusTransactionPdfAction()
    {
        $this->setLogMessage("Daily Customer Transaction PDF report generated ");

        $request = $this->getRequest()->getQuery();
        $date = $request['from_date'];
        $customer_name = $request['cus_name'];
        $customer_shortname = $request['cus_shortname'];

        if ($date != null && $customer_name == null) {

            $resinvdata = $this->CommonTable('Invoice\Model\InvoiceTable')->getDailyInvoiceByDate($date);

            $rescrndata = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDailyCreditNoteByDate($date);

            $respaymentdata = $this->CommonTable('Invoice\Model\PaymentsTable')->getDailyPaymentsDetailsByOnlyDate($date);
        } else if ($date != null && $customer_name != null) {

            $resinvdata = $this->CommonTable('Invoice\Model\InvoiceTable')->getDailyInvoiceByDateCusName($customer_name, $date);

            $rescrndata = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDailyCreditNoteByDateCusName($customer_name, $date);

            $getCustomerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByShortname($customer_shortname);
            $customerId = $getCustomerDetails->customerID;

            $respaymentdata = $this->CommonTable('Invoice\Model\PaymentsTable')->getDailyPaymentsDetailsByDate($customerId, $date);
        }

        $companyDetails = $this->CommonTable('Invoice\Model\CompanyTable')->fetchAll();

        while ($row = $companyDetails->current()) {
            $a[] = $row;
        }

        $pdf = new PdfModel();
        $pdf->setOption('filename', 'DailyCustomerTransaction-report'); // Triggers PDF download, automatically appends ".pdf"
        $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
        $pdf->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"

        $pdf->setVariables(array(
            'from_date' => $date,
            'invoiceTbl_data' => $resinvdata,
            'creditNoteTbl_data' => $rescrndata,
            'paymentTbl_data' => $respaymentdata,
            'cus_name' => $customer_name,
            'company_data' => $a
        ));
        return $pdf;
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return \DOMPDFModule\View\Model\PdfModel
     */
    public function generateCusTransactionPdfAction()
    {

        $request = $this->getRequest()->getQuery();
        $from_date = $request['from_date'];
        $to_date = $request['to_date'];
        $cus_name = $request['cus_name'];

        if ($from_date != null && $to_date != null && $cus_name != null) {

            $resinvdata = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByCustomerName($cus_name, $from_date, $to_date);
            $rescrndata = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCustomerNameReport($cus_name, $from_date, $to_date);

            $customer_shortname = split('- ', $cus_name);

            $getCustomerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByShortname($customer_shortname[1]);
            $customerId = $getCustomerDetails->customerID;


            $respaymentdata = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsDetailsByCusId($customerId, $from_date, $to_date);

            $companyDetails = $this->CommonTable('Invoice\Model\CompanyTable')->fetchAll();

            while ($row = $companyDetails->current()) {
                $a[] = $row;
            }

            $pdf = new PdfModel();
            $pdf->setOption('filename', 'CustomerTransaction-report');
            $pdf->setOption('paperSize', 'a4');
            $pdf->setOption('paperOrientation', 'landscape');


            $pdf->setVariables(array(
                'from_date' => $from_date,
                'to_date' => $to_date,
                'invoiceTbl_data' => $resinvdata,
                'creditNoteTbl_data' => $rescrndata,
                'paymentTbl_data' => $respaymentdata,
                'cus_name' => $cus_name,
                'company_data' => $a
            ));
            return $pdf;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return \DOMPDFModule\View\Model\PdfModel
     */
    public function generateCusBalancesPdfAction()
    {
        $this->setLogMessage("Customer Balance PDF Report generated");

        $request = $this->getRequest()->getQuery();
        $from_date = $request['from_date'];
        $to_date = $request['to_date'];
        $customer_name = $request['customer_name'];
        $customer_shortname = $request['customer_shortname'];

        $companyDetails = $this->CommonTable('Invoice\Model\CompanyTable')->fetchAll();

        while ($row = $companyDetails->current()) {
            $a[] = $row;
        }

        $array_inv = array();
        $array_crd = array();
        $balancesData = array();

        $customersList = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomers();
        $customers = $customersList;

        if ($customer_name == '') {
            $resinvdata = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBalances($from_date, $to_date);

            $rescrndata = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteBalances($from_date, $to_date);

            $getCustomerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByShortname($customer_shortname);
            $customerId = $getCustomerDetails->customerID;

            $respaymentdata = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsBalancesData($customerId, $from_date, $to_date);

            if ($resinvdata != null || $rescrndata != null) {
                foreach ($customers as $customer) {
                    $array_inv[$customer] = array();
                }

                foreach ($resinvdata as $res) {
                    array_push($array_inv[$res['CusID']], $res);
                }

                foreach ($customers as $customer) {
                    $array_crd[$customer] = array();
                }

                foreach ($rescrndata as $res) {
                    array_push($array_crd[$res['CusID']], $res);
                }

                $balancesData[0] = 'nodata';
            } else {
                $array_inv[0] = 'nodata';
                $array_crd[0] = 'nodata';
            }
        } else {
            $resinvdata = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBalancesByName($customer_name, $from_date, $to_date);
            $rescrndata = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteBalancesByName($customer_name, $from_date, $to_date);
            if ($resinvdata != null || $rescrndata != null) {
                $balanceData = array(
                    0 => $resinvdata[0]['totAmountInv'],
                    1 => $resinvdata[0]['totSetledInv'],
                    2 => $rescrndata[0]['crdOrPayAmount'],
                    3 => $rescrndata[0]['setledCrdt'],
                );
                $array_inv[0] = 'nodata';
                $array_crd[0] = 'nodata';
            } else {
                $balanceData[0] = 'nodata';
            }
        }

        $pdf = new PdfModel();
        $pdf->setOption('filename', 'CustomerBalances-report');
        $pdf->setOption('paperSize', 'a4');
        $pdf->setOption('paperOrientation', 'landscape');

        ini_set('max_execution_time', -1);
        set_time_limit(0);
        $pdf->setVariables(array(
            'from_date' => $from_date,
            'to_date' => $to_date,
            'array_inv' => $array_inv,
            'array_crd' => $array_crd,
            'balanceData' => $balanceData,
            'company_data' => $a,
            'Cus_name' => $customer_name,
            'customersList' => $customersList
        ));

        return $pdf;
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return \DOMPDFModule\View\Model\PdfModel
     */
    public function generateAgedCusSummeryPdfAction()
    {
        $this->setLogMessage("Aged Customer Summery PDF Report generated");

        $request = $this->getRequest()->getQuery();
        $end_date = $request['end_date'];
        $cus_short_name = $request['cus_short_name'];
        $cus_name = $request['cus_name'];

        if ($end_date != null && $cus_short_name != null) {

            $getCustomerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByShortname($cus_short_name);
            $customerId = $getCustomerDetails->customerID;

            $agedCustomerDeatailsByInvTable = $this->CommonTable('Invoice\Model\InvoiceTable')->getAgedCusInvoiceData($customerId, $end_date);
            $agedCustomerDeatailsByCrdtNoteTable = $this->CommonTable('Invoice\Model\CreditNoteTable')->getAgedCusCrdtNoteData($customerId, $end_date);

            $Current_inv = 0.00;
            $Within7_inv = 0.00;
            $Within14_inv = 0.00;
            $Within30_inv = 0.00;
            $Over30_inv = 0.00;
            $Total_inv = 0.00;

            $Current_crdt = 0.00;
            $Within7_crdt = 0.00;
            $Within14_crdt = 0.00;
            $Within30_crdt = 0.00;
            $Over30_crdt = 0.00;
            $Total_crdt = 0.00;

            foreach ($agedCustomerDeatailsByInvTable as $data) {
                $current = $data['Current'];
                $Current_inv += $current;
                $withIn7 = $data['WithIn7Days'];
                $Within7_inv += $withIn7;
                $withIn14 = $data['WithIn14Days'];
                $Within14_inv += $withIn14;
                $withIn30 = $data['WithIn30Days'];
                $Within30_inv += $withIn30;
                $over30 = $data['Over30'];
                $Over30_inv += $over30;

                $Total_i = $current + $withIn7 + $withIn14 + $withIn30 + $over30;
                $Total_inv += $Total_i;
            }

            foreach ($agedCustomerDeatailsByCrdtNoteTable as $data) {
                $current = $data['Current'];
                $Current_crdt += $current;
                $withIn7 = $data['WithIn7Days'];
                $Within7_crdt += $withIn7;
                $withIn14 = $data['WithIn14Days'];
                $Within14_crdt += $withIn14;
                $withIn30 = $data['WithIn30Days'];
                $Within30_crdt += $withIn30;
                $over30 = $data['Over30'];
                $Over30_crdt += $over30;

                $Total_c = $current + $withIn7 + $withIn14 + $withIn30 + $over30;
                $Total_crdt += $Total_c;
            }

            $Current_net = $Current_inv - $Current_crdt;
            $WithIn7_net = $Within7_inv - $Within7_crdt;
            $WithIn14_net = $Within14_inv - $Within14_crdt;
            $WithIn30_net = $Within30_inv - $Within30_crdt;
            $Over30_net = $Over30_inv - $Over30_crdt;
            $Total_net = $Total_inv - $Total_crdt;

            $pdf = new PdfModel();
            $pdf->setOption('filename', 'agedCustomerSummery-report'); // Triggers PDF download, automatically appends ".pdf"
            $pdf->setOption('paperSize', 'a4'); // Defaults to "8x11"
            $pdf->setOption('paperOrientation', 'landscape'); // Defaults to "portrait"

            $companyDetails = $this->CommonTable('Invoice\Model\CompanyTable')->fetchAll();

            while ($row = $companyDetails->current()) {
                $a[] = $row;
            }

            $pdf->setVariables(array(
                'end_date' => $end_date,
                'cus_name' => $cus_name,
                'current_net' => $Current_net,
                'within7_net' => $WithIn7_net,
                'within14_net' => $WithIn14_net,
                'within30_net' => $WithIn30_net,
                'over30_net' => $Over30_net,
                'total_net' => $Total_net,
                'company_data' => $a
            ));

            return $pdf;
        }
    }

    public function generateCusDetailsPdfAction()
    {
        $this->setLogMessage("Customer Details PDF Report generated");

        $request = $this->getRequest()->getQuery();
        $customer_name = $request['customer_name'];
        $customer_name_split = split('- ', $customer_name);

        $companyDetails = $this->CommonTable('Invoice\Model\CompanyTable')->fetchAll();

        while ($row = $companyDetails->current()) {
            $a[] = $row;
        }

        if ($customer_name == '' || $customer_name == NULL) {

            $rescusdata = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();

            if ($rescusdata != null) {

                while ($row = $rescusdata->current()) {
                    $cusdata[] = $row;
                }

                foreach ($cusdata as $key) {
                    array_push($cusdata[$key[$key]], $key);
                }
            }
        } else {
            $rescusdata = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByName($customer_name_split[0]);

            if ($rescusdata != null) {
                $cusdata[] = $rescusdata;
            }
        }

        $pdf = new PdfModel();
        $pdf->setOption('filename', 'CustomerDetails-report');
        $pdf->setOption('paperSize', 'a4');
        $pdf->setOption('paperOrientation', 'landscape');
        $pdf->setVariables(array(
            'cus_data' => $cusdata,
            'company_data' => $a
        ));

        return $pdf;
    }

}
