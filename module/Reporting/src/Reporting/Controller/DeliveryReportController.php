<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

/**
 * @author Peushani Jayasekara <peushani@thinkcube.com>
 *
 */
class DeliveryReportController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_report_upper_menu';

    function __construct()
    {
        parent::__construct();
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Delivery Notes', 'SALES');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/delivery-report.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $companyDetails = $this->getCompanyDetails();

        //get CustomerCategory list
        $customerCategory = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();

        $activeUsers = $this->CommonTable('User\Model\UserTable')->getActiveUsers();
        $users = array();
        foreach ($activeUsers as $activeUser) {
            $users[$activeUser['userID']] = $activeUser['userUsername'];
        }

        return new ViewModel(array(
            'companyDetails' => $companyDetails,
            'locationList' => $this->allLocations,
            'users' => $users,
            'currentLocation' => $this->user_session->userActiveLocation['locationID'],
            'cusCategory' => $customerCategory
        ));
    }

}
