<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains Report PDF related controller functions
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class SalesReportController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_report_upper_menu';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
            $this->upperMenus = 'invoice_report_upper_menu'.$this->packageID;
        }
    }


    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Sales Report form created in this method
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'SALES', 'SALES');
        $this->getSideAndUpperMenus('Reports', 'Sales');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/sales-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/accounting.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');
        $companyDetails = $this->getCompanyDetails();
        $dateFormat = $this->getUserDateFormat();

        $paymentList = [];
        $paymentType = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPaymentTypeData();
        foreach ($paymentType as $pt) {
            $paymentList[$pt['paymentMethodID']] = $pt;
        }

        //Get sales person list
        $salesPersons = [];
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        //get CustomerCategory list
        $customerCategory = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        

        return new ViewModel(array(
            'locationList' => $this->allLocations,
            'paymentList' => $paymentList,
            'companyDetails' => $companyDetails,
            'dateFormat' => $dateFormat,
            'salesPersons' => $salesPersons,
            'cusCategory' => $customerCategory
        ));
    }

}
