<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class PosReportController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_report_upper_menu';

    function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
            $this->upperMenus = 'invoice_report_upper_menu'.$this->packageID;
        }
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Sales Report form created in this method
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'SALES', 'SALES');
        $this->getSideAndUpperMenus('Reports', 'POS');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/pos-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/accounting.js');
        $this->getViewHelper('HeadScript')->prependFile('/assets/bootstrap/js/bootstrap-select.min.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');
        $this->getViewHelper('headLink')->prependStylesheet('/assets/bootstrap/css/bootstrap-select.min.css');

        $activeLocations = $this->user_session->userAllLocations;

        $user = array();
        // if super admin
        if ($this->user_session->roleID == 1) {
            $users = $this->CommonTable('User\Model\UserTable')->fetchAll();
            foreach ($users as $u) {
                $user[$u['userID']] = $u;
            }
        } else {
            $users = $this->CommonTable('User\Model\UserTable')->fetchAll();
            foreach ($users as $u) {
                if ($u->roleID != 1) {
                    $user[$u['userID']] = $u;
                }
            }
        }

        $paymentMethod = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentMethod as $p) {
            $p = (object) $p;
            $paymentMethodSet[$p->paymentMethodID] = $p;
        }

        return new ViewModel(array(
            'aL' => $activeLocations,
            'uL' => $user,
            'pML' => $paymentMethodSet
                )
        );
    }

}
