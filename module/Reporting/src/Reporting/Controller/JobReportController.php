<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * job card report related view functions
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class JobReportController extends CoreController
{

    protected $sideMenus = 'jobs_side_menu_of_service';
    protected $upperMenus = 'job_reports_upper_menu';
    protected $downMenus = 'job_setup_down_menu_of_service';


    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Sales Report form created in this method
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Employee', 'Jobs');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/job-report.js');


        $allLocations = $this->allLocations;

        foreach ($allLocations as $key => $value) {
            $locIDs[] = $key;
            $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
        }

        return new ViewModel(array(
            'locNames' => $locNames,
        ));
    }

}
