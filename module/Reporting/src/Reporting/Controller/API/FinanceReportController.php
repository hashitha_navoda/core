<?php

/**
 * @author Ashan <ashan@thinkcube.com>
 * This file contains finance report api  related controller functions
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;
use Core\BackgroundJobs\ReportJob;

class FinanceReportController extends CoreController
{

    protected $userID;


    /**
     * @author Ashan  <ashan@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function trialBalanceAction()
    {

    	if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
           
        $startDate = $postData['startDate'];
        $endDate = $postData['endDate'];
        $reportModel = $postData['reportModel'];
        $dimensionType = $postData['dimensionType'];
        $dimensionValue = $postData['dimensionValue'];
        $locationIDs = $postData['locationID'];
        $sepLocBal = ($postData['sepLocBal'] == 'true') ? true: false;
        $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

        $netProfitData = [];

        $locationData = null;


        if ($sepLocBal) {
            foreach ($locationIDs as $key => $value) {
                $location = $this->CommonTable('Core\Model\LocationTable')->getLocation($value);

                $locationData[] = [
                    'locationName' => $location->locationName,
                    'locationCode' => $location->locationCode,
                    'locationID' => $location->locationID

                ];
            }
        }     

        
        if($reportModel == 'View'){

            if ($sepLocBal) {

                foreach ($locationData as $key => $value) {
                    
                    $trialBalanceData[$value['locationCode'].'-'.$value['locationID']] = $this->getService('FinanceReportService')->getTrialBalanceData($startDate, $endDate, $dimensionType, $dimensionValue, $value['locationID'], $sepLocBal);
                }


            } else {
                $trialBalanceData = $this->getService('FinanceReportService')->getTrialBalanceData($startDate, $endDate, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);

            }

            $cD = $this->getCompanyDetails();
            
            $translator = new Translator();
            $name = $translator->translate('Trial Balance');
            $period = $startDate . ' - ' . $endDate;
            
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $trialBalanceView = new ViewModel(array(
                'trialBalanceData' => $trialBalanceData,
                'locationIDs' => $locationIDs,
                'sepLocBal' => $sepLocBal,
                'hideZeroVal' => $hideZeroVal,
                'locationData' => $locationData,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'headerTemplate' => $headerViewRender,
                'cD' => $companyDetails)
            );
            
            $trialBalanceView->setTemplate('reporting/finance-report/generate-trial-balance-view');

            $this->html = $trialBalanceView;
            $this->status = true;
            return $this->JSONRespondHtml();

        }else if($reportModel == 'Csv'){

            try {
                $config = $this->getServiceLocator()->get('config');

                $meta = $this->getReportJobCallbackData($config['report']['trial-balance'], 'csv');
            
                $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

                $result = ReportJob::add($jobDetails); // add to queue

                if (!$result) { // if erorr occures
                    return $this->returnJsonError('An error occurred while job adding to queue.');
                }

                return $this->returnJsonSuccess($result, 'Report has been added to queue.');

            } catch (\Exception $ex) {
                error_log($ex->getMessage());
                return $this->returnJsonError('An error occurred while adding to queue.');
            }
            
        }

    }

    /**
     * @author Ashan  <ashan@thinkcube.com>
     * @return $pdf
     */
    public function generateTrialBalancePdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data
            
            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['trial-balance'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    /**
     * @author Ashan  <ashan@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function balanceSheetAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();

        $endDate = $postData['endDate'];
        $dimensionType = $postData['dimensionType'];
        $dimensionValue = $postData['dimensionValue'];
        $reportModel = $postData['reportModel'];
        $locationIDs = $postData['locationID'];
        $sepLocBal = ($postData['sepLocBal'] == 'true') ? true: false;
        $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

        $netProfitData = [];

        $locationData = null;


        if ($sepLocBal) {
            foreach ($locationIDs as $key => $value) {
                $location = $this->CommonTable('Core\Model\LocationTable')->getLocation($value);

                $locationData[] = [
                    'locationName' => $location->locationName,
                    'locationCode' => $location->locationCode,
                    'locationID' => $location->locationID

                ];
            }
        }        

        if($reportModel == 'View'){

            if ($sepLocBal) {
                $balanceSheetData = $this->getService('FinanceReportService')->getBalanceSheetDataForViewReport('',$endDate, $dimensionType, $dimensionValue, $locationData, $sepLocBal);
            } else {
                $balanceSheetData = $this->getService('FinanceReportService')->getBalanceSheetDataForViewReport('',$endDate, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);

            }

            $profitAndLostData = [];
            foreach ($balanceSheetData as $key => $value) {
                if($value['financeAccountTypesID'] == 4){
                    $profitAndLostData[0] = $value;
                } else if( $value['financeAccountTypesID'] == 6){
                    $profitAndLostData[1] = $value;
                } else if( $value['financeAccountTypesID'] ==  7){
                    $profitAndLostData[2] = $value;
                } else if( $value['financeAccountTypesID'] ==  3){
                    $profitAndLostData[3] = $value;
                }
            }
            ksort($profitAndLostData);

            if (!$sepLocBal) {
                $netProfit = $this->getNetProfitAmount($profitAndLostData);
            } else {

                foreach ($locationData as $key55 => $value55) {
                    # code...
                    $netProfitData[$value55['locationCode'].'-'.$value55['locationID']] = $this->getNetProfitAmountLocationWise($profitAndLostData, $value55);
                }

            }

            $cD = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('Balance Sheet');
            $period = $endDate;
            
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);
            
            $balanceSheetView = new ViewModel(array(
                'balanceSheetData' => $balanceSheetData,
                'locationIDs' => (!empty($locationIDs)) ? $locationIDs : null,
                'netProfit' => $netProfit,
                'netProfitData' => $netProfitData,
                'endDate' => $endDate,
                'headerTemplate' => $headerViewRender,
                'locationData' => $locationData,
                'sepLocBal' => $sepLocBal,
                'hideZeroVal' => $hideZeroVal,
                'cD' => $companyDetails)
            );
            
            $balanceSheetView->setTemplate('reporting/finance-report/generate-balance-sheet-view');

            $this->html = $balanceSheetView;
            $this->status = true;
            return $this->JSONRespondHtml();

        } else if($reportModel == 'Csv'){
            
            try {
                $config = $this->getServiceLocator()->get('config');

                $meta = $this->getReportJobCallbackData($config['report']['balance-sheet'], 'csv');
            
                $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

                $result = ReportJob::add($jobDetails); // add to queue

                if (!$result) { // if erorr occures
                    return $this->returnJsonError('An error occurred while job adding to queue.');
                }

                return $this->returnJsonSuccess($result, 'Report has been added to queue.');

            } catch (\Exception $ex) {
                error_log($ex->getMessage());
                return $this->returnJsonError('An error occurred while adding to queue.');
            }
        }
    }

    /**
     * @author Ashan  <ashan@thinkcube.com>
     * @return $pdf
     */
    public function generateBalanceSheetPdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }
            $postData = $request->getPost()->toArray();
            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['balance-sheet'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }

    }

    public function profitAndLostAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
           
        $startDate = $postData['startDate'];
        $endDate = $postData['endDate'];
        $reportModel = $postData['reportModel'];
        $dimensionType = $postData['dimensionType'];
        $dimensionValue = $postData['dimensionValue'];
        $locationIDs = $postData['locationID'];
        $sepLocBal = ($postData['sepLocBal'] == 'true') ? true: false;
        $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;



        $locationData = null;


        if ($sepLocBal) {
            foreach ($locationIDs as $key => $value) {
                $location = $this->CommonTable('Core\Model\LocationTable')->getLocation($value);

                $locationData[] = [
                    'locationName' => $location->locationName,
                    'locationCode' => $location->locationCode,
                    'locationID' => $location->locationID

                ];
            }
        } 


        if($reportModel == 'View'){
                        
            if ($sepLocBal) {
                $array = $this->getService('FinanceReportService')->getBalanceSheetDataForViewReport($startDate,$endDate, $dimensionType, $dimensionValue, $locationData, $sepLocBal);
            } else {
                $array = $this->getService('FinanceReportService')->getBalanceSheetDataForViewReport($startDate,$endDate, $dimensionType, $dimensionValue, $locationIDs, $sepLocBal);

            }
            
            $profitAndLostData = [];
            foreach ($array as $key => $value) {
                if($value['financeAccountTypesID'] == 4){
                    $profitAndLostData[0] = $value;
                } else if( $value['financeAccountTypesID'] == 6){
                    $profitAndLostData[1] = $value;
                } else if( $value['financeAccountTypesID'] ==  7){
                    $profitAndLostData[2] = $value;
                } else if( $value['financeAccountTypesID'] ==  3){
                    $profitAndLostData[3] = $value;
                }
            }
            
            ksort($profitAndLostData);
            $cD = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('Profit And Loss');
            $period = $startDate . ' - ' . $endDate;
            
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $profitAndLostView = new ViewModel(array(
                'profitAndLostData' => $profitAndLostData,
                'locationIDs' => (!empty($locationIDs)) ? $locationIDs : null,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'headerTemplate' => $headerViewRender,
                'locationData' => $locationData,
                'sepLocBal' => $sepLocBal,
                'hideZeroVal' => $hideZeroVal,
                'cD' => $cD)
            );
            
            $profitAndLostView->setTemplate('reporting/finance-report/generate-profit-and-lost-view');

            $this->html = $profitAndLostView;
            $this->status = true;
            return $this->JSONRespondHtml();

        } else if( $reportModel == 'Csv' ){

            try {
                $config = $this->getServiceLocator()->get('config');

                $meta = $this->getReportJobCallbackData($config['report']['profit-and-loss'], 'csv');
            
                $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

                $result = ReportJob::add($jobDetails); // add to queue

                if (!$result) { // if erorr occures
                    return $this->returnJsonError('An error occurred while job adding to queue.');
                }

                return $this->returnJsonSuccess($result, 'Report has been added to queue.');

            } catch (\Exception $ex) {
                error_log($ex->getMessage());
                return $this->returnJsonError('An error occurred while adding to queue.');
            }
        }
    }

     /**
     * @author Ashan  <ashan@thinkcube.com>
     * @return $pdf
     */
    public function generateProfitAndLostPdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data
            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['profit-and-loss'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    public function generalLedgerAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
           
        $startDate = $postData['startDate'];
        $endDate = $postData['endDate'];
        $reportModel = $postData['reportModel'];
        $accountIDs = $postData['accountID'];
        $dimensionType = $postData['dimensionType'];
        $dimensionValue = $postData['dimensionValue'];
              
        if($reportModel == 'View'){
 
            $generalLedgerData = $this->getService('FinanceReportService')->getGeneralLedgerDataForViewReport($startDate, $endDate, $accountIDs, $dimensionType, $dimensionValue)['generalLedgerData'];
            $cD = $this->getCompanyDetails();
 
            $translator = new Translator();
            $name = $translator->translate('General Ledger');
            $period = $startDate . ' - ' . $endDate;
            
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $generalLedgerView = new ViewModel(array(
                'generalLedgerData' => $generalLedgerData,
                'startDate' => $startDate,
                'endDate' => $endDate,
                'headerTemplate' => $headerViewRender,
                'cD' => $cD)
            );
            
            $generalLedgerView->setTemplate('reporting/finance-report/generate-general-ledger-view');

            $this->html = $generalLedgerView;
            $this->status = true;
            return $this->JSONRespondHtml();
        } else if($reportModel == 'Csv'){
            
            try {
                $config = $this->getServiceLocator()->get('config');

                $meta = $this->getReportJobCallbackData($config['report']['general-ledger'], 'csv');
            
                $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

                $result = ReportJob::add($jobDetails); // add to queue

                if (!$result) { // if erorr occures
                    return $this->returnJsonError('An error occurred while job adding to queue.');
                }

                return $this->returnJsonSuccess($result, 'Report has been added to queue.');

            } catch (\Exception $ex) {
                error_log($ex->getMessage());
                return $this->returnJsonError('An error occurred while adding to queue.');
            }
        }

    }

    public function getGeneralLedgerJournalEntriesByAccountIdAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();

        $startDate = $postData['startDate'];
        $endDate = $postData['endDate'];
        $accountID = $postData['accountID'];
        $dimensionType = $postData['dimensionType'];
        $dimensionValue = $postData['dimensionValue'];
        $isOrderByDate = ($postData['isOrderByDate'] == "true") ? true : false;

        $generalLedgerData = $this->getService('FinanceReportService')->getGeneralLedgerJournalEntriesByAccountId($startDate, $endDate, $accountID, $dimensionType, $dimensionValue, $isOrderByDate)['generalLedgerJournalEntriesData'];

        $this->status = true;
        $this->data = $generalLedgerData;
        return $this->JSONRespond();
    }

     /**
     * @author Ashan  <ashan@thinkcube.com>
     * @return $pdf
     */
    public function generateGeneralLedgerPdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data
            
            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['general-ledger'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    private function calculateAmounts($dataArray)
    {
        $creditAmountTotal = 0; 
        $debitAmountTotal = 0;
        foreach ($dataArray as $value) {
            if(isset($value['child'])){
                $amountData = $this->calculateAmounts($value['child']);
                $creditAmountTotal+=$amountData['totalCreditAmount'];
                $debitAmountTotal+=$amountData['totalDebitAmount'];
            }
            $creditAmountTotal+=$value['data']->totalCreditAmount;
            $debitAmountTotal+=$value['data']->totalDebitAmount;
        }
        return array('totalCreditAmount'=> $creditAmountTotal, 'totalDebitAmount' => $debitAmountTotal);
    }

    private function getChildAccounts($dataArray,$acCData,$level,$dtype, $docSubMode = null)
    {   
        $arr = '';
        foreach ($dataArray as $value) {
            if(isset($value['child'])){
                $amountData = $this->calculateAmounts($value['child']);
            }

            $creditAmount = ($amountData['totalCreditAmount'] != 0)? (float) ($amountData['totalCreditAmount']+$value['data']->totalCreditAmount ): (float) $value['data']->totalCreditAmount;
            $debitAmount = ($amountData['totalDebitAmount'] != 0)? (float) ($amountData['totalDebitAmount']+$value['data']->totalDebitAmount ): (float) $value['data']->totalDebitAmount;
            if($dtype == 'trialBalance'){
                $balance = floatval($debitAmount) - floatval($creditAmount);
                if($balance < 0){
                    $viewBalnce = '('. ($balance* (-1)) .')';
                } else {
                    $viewBalnce = $balance;
                }
                $in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsName, (string) $value['data']->financeAccountsCode, $acCData['financeAccountClassName'], $acCData['financeAccountTypesName'], $viewBalnce];
                $arr.=implode(",", $in) . "\n";

            }else if($dtype == 'balanceSheet'){
                if($docSubMode == 5 || $docSubMode == 2){
                    $balance = floatval($creditAmount) - floatval($debitAmount);
                }
                else if($docSubMode == 1){
                    $balance = floatval($debitAmount) - floatval($creditAmount);
                }
                
                if($balance < 0){
                    $viewBalnce = '('. ($balance* (-1)) .')';
                } else {
                    $viewBalnce = $balance;
                }

                $in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsCode, $value['data']->financeAccountsName, $viewBalnce];
                $arr.=implode(",", $in) . "\n";
            }

            if(isset($value['child'])){
                $arr.= $this->getChildAccounts($value['child'], $acCData, $level+1,$dtype, $docSubMode);
            }
        }
        return $arr;
    }


    private function getTrialBalanceData($startDate, $endDate)
    {
    	if(isset($startDate) && isset($endDate)){
    		$fAccountClassData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClasses();

            $financeAccounts = array();

            foreach ($fAccountClassData as $key => $value) {
                $financeAccounts[$value['financeAccountClassID']]['financeAccountClassID'] = $value['financeAccountClassID'];
                $financeAccounts[$value['financeAccountClassID']]['financeAccountTypesName'] = $value['financeAccountTypesName'];
                $financeAccounts[$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];
                $fAccountsData = $this->getHierarchyByAccountClassIDs($value['financeAccountClassID'],$startDate, $endDate, true);
                $financeAccounts[$value['financeAccountClassID']]['financeAccounts'] = $fAccountsData ;
            }
            return $financeAccounts; 
        }
    }

    private function getBalanceSheetData($startDate, $endDate)
    {
        if(isset($startDate) && isset($endDate)){
            $financeAccounts = array();
            $fAccountsTypesData = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->fetchAll();
            
            foreach ($fAccountsTypesData as $typeKey => $typeValue) {
                
                    $financeAccounts[$typeValue->financeAccountTypesID]['financeAccountTypesID'] = $typeValue->financeAccountTypesID;
                    $financeAccounts[$typeValue->financeAccountTypesID]['financeAccountTypesName'] = $typeValue->financeAccountTypesName;
                    $fAccountClassData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClassesByAccountTypeID($typeValue->financeAccountTypesID);

                foreach ($fAccountClassData as $key => $value) {
                    $financeAccounts[$typeValue->financeAccountTypesID][$value['financeAccountClassID']]['financeAccountClassID'] = $value['financeAccountClassID'];
                    $financeAccounts[$typeValue->financeAccountTypesID][$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];
                    $fAccountsData = $this->getHierarchyByAccountClassIDs($value['financeAccountClassID'], $startDate, $endDate, true);
                    $financeAccounts[$typeValue->financeAccountTypesID][$value['financeAccountClassID']]['financeAccounts'] = $fAccountsData ;
                }
            }
            
            return $financeAccounts; 
        }
    }

    private function calculateAmountsForProfitAndLost($dataArray)
    {
        $creditAmountTotal = 0; 
        $debitAmountTotal = 0;
        foreach ($dataArray as $value) {
            if(isset($value['child'])){
                $amountData = $this->calculateAmountsForProfitAndLost($value['child']);
                if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                    $creditAmountTotal+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                }else{
                    $debitAmountTotal+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'] ;
                }
            }
            if($value['data']->totalCreditAmount - $value['data']->totalDebitAmount > 0){
                $creditAmountTotal+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
            }else{
                $debitAmountTotal+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
            }
        }
        return array('totalCreditAmount'=> $creditAmountTotal, 'totalDebitAmount' => $debitAmountTotal);

    }

    private function calculateAmountsForProfitAndLostLocationwise($dataArray, $locData)
    {
        $kk = $locData['locationCode'].'-'.$locData['locationID'];
        $creditAmountTotal = 0; 
        $debitAmountTotal = 0;
        foreach ($dataArray as $value) {
            if(isset($value['child'])){
                $amountData = $this->calculateAmountsForProfitAndLostLocationwise($value['child'], $locData);
                if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                    $creditAmountTotal+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                }else{
                    $debitAmountTotal+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'] ;
                }
            }
            if($value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk] > 0){
                $creditAmountTotal+=$value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk];
            }else{
                $debitAmountTotal+=$value['data']->totalDebitAmount[$kk] - $value['data']->totalCreditAmount[$kk];
            }
        }
        return array('totalCreditAmount'=> $creditAmountTotal, 'totalDebitAmount' => $debitAmountTotal);

    }

    private function getChildAccountsForProfitAndLost($dataArray,$acCData,$level, $typeK = null)
    {
        $arr = '';
        foreach ($dataArray as $value) {
            $debitAmount = 0;
            $creditAmount = 0;
            $subAccountCreditAmount = 0;
            $subAccountDebitAmount = 0;
            if(isset($value['child'])){
                $amountData = $this->calculateAmountsForProfitAndLost($value['child']);
                $subAccountCreditAmount += $amountData['totalCreditAmount']; 
                $subAccountDebitAmount += $amountData['totalDebitAmount'];
                if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                    $creditAmount+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                }else{
                    $debitAmount+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'] ;
                }
            }
            if($value['data']->totalCreditAmount - $value['data']->totalDebitAmount > 0){
                $creditAmount+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
            }else{
                $debitAmount+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
            }
            $arrCreditAmount = ($creditAmount - $debitAmount > 0)? (float) ($creditAmount - $debitAmount) : (float) (0.00);
            $arrDebitAmount = ($debitAmount - $creditAmount > 0 )? (float) ($debitAmount - $creditAmount) : (float) (0.00);

            $totalCDvalue = 0;
            if($typeK == 0 || $typeK == 2){
                $subAccountTotal = $subAccountCreditAmount - $subAccountDebitAmount;
                $totalCDvalue = $value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
                $totalCDvalue += $subAccountTotal; 
            } else if($typeK == 1 || $typeK == 3){
                $totalCDvalue = $value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
                $subAccountTotal = $subAccountDebitAmount - $subAccountCreditAmount;
                $totalCDvalue += $subAccountTotal;
            }

            if($totalCDvalue < 0){
                $viewTotalCDValue = '('.($totalCDvalue*(-1)).')';
            } else {
                $viewTotalCDValue =  $totalCDvalue;
            }

            $in = [str_repeat(' ', 2*$level).$value['data']->financeAccountsCode, $value['data']->financeAccountsName, $viewTotalCDValue];
            $arr.=implode(",", $in) . "\n";

            if(isset($value['child'])){
                $arr.=$this->getChildAccountsForProfitAndLost($value['child'],$acCData,$level+1, $typeK);
            }
        }
        return $arr;
    }

    private function getGeneralLedgerData($startDate, $endDate, $accountIDs = null)
    {
        $financeAccounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAll(null, $accountIDs, true);
        $generalLedgerData = [];
        foreach ($financeAccounts as $key => $value) {
            $accountID = $value['financeAccountsID'];
            $AccountData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($accountID, '', $endDate);
            $AccountData['financeAccountsCode'] = $value['financeAccountsCode'];
            $AccountData['financeAccountsName'] = $value['financeAccountsName'];
            //calculate date
            $newDate   = date('Y-m-d', strtotime($startDate . ' -1 day'));
           
            $fAccounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($accountID, '', $newDate);
            $journalEntrys = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRange($accountID, $startDate, $endDate);
            //get journal Entry details that out of selected date range
            $jEDataForOpening = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRange($accountID, '', $newDate);
            $getopeningBalanceCreditDebit = $this->getDocumentCommentForRelatedJournalEntry($jEDataForOpening);
           
            $generalLedgerData[$accountID]['accountsData'] = $AccountData;
            $generalLedgerData[$accountID]['openingBalanceData'] = $fAccounts;
            $jEntriesAndDuplicatedCreditDebitValue = $this->getDocumentCommentForRelatedJournalEntry($journalEntrys);
            $generalLedgerData[$accountID]['journalEntries'] = $jEntriesAndDuplicatedCreditDebitValue['returnJEntity'];
            $generalLedgerData[$accountID]['removedCreditDebit'] = $jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues'];
            $generalLedgerData[$accountID]['removedCreditDebitForOpenning'] = $getopeningBalanceCreditDebit['creditDebitRemovedValues'];
        }

        return array('generalLedgerData' => $generalLedgerData); 
    }

    /**
    * Generate accounts array nested under each parent
    * @return array
    */
    public function getHierarchyByAccountClassIDs($accountClassID, $startDate = null, $endDate = null, $activeFlag = false)
    {

        $res = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAllHierarchicalByAccountClassID($accountClassID, $activeFlag);
        $all = array();

        foreach ($res as $loo) {
            $loo->totalCreditAmount = 0.00;
            $loo->totalDebitAmount = 0.00;
            
            $data = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($loo->financeAccountsID, $startDate, $endDate);
            if($data['journalEntryID'] != NULL){
                $loo->totalCreditAmount = $data['totalCreditAmount'];
                $loo->totalDebitAmount = $data['totalDebitAmount'];

            }
            //get journal Entry details
            $journalEntrys = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRange($loo->financeAccountsID, $startDate, $endDate);
            //get duplicated credit and debit value for this account
            $jEntriesAndDuplicatedCreditDebitValue = $this->getDocumentCommentForRelatedJournalEntry($journalEntrys);
            if(sizeof($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']) > 0){
                $loo->totalCreditAmount = floatval($loo->totalCreditAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['credit']);
                $loo->totalDebitAmount = floatval($loo->totalDebitAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['debit']);                
            }

            $all[$loo->financeAccountsID] = array("data" => $loo);
        }

        foreach ($all as $key => &$loo) {
            if (isset($loo['data'])) {
                $all[$loo['data']->financeAccountsParentID]['child'][$loo['data']->financeAccountsID] = $loo;
                unset($all[$key]);
            }
        }

        return $all;
    }

    
    private function getNetProfitAmount($profitAndLostData)
    {
        $netProfit = 0.00;
        foreach ($profitAndLostData as $typekey => $typeData) {
            $TotalDebitValue = 0;
            $TotalCreditValue = 0;

            foreach ($typeData as $tbalance) {
                if(isset($tbalance['financeAccounts'][0]['child'])){
                    foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
                        $creditAmount = 0;
                        $debitAmount = 0;
                        if(isset($value['child'])){
                            $amountData = $this->calculateAmountsForProfitAndLost($value['child']);
                            if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                                $TotalCreditValue+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                                $creditAmount = $amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                            } else {
                                $TotalDebitValue+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
                                $debitAmount = $amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
                            }
                        }

                        if($value['data']->totalCreditAmount - $value['data']->totalDebitAmount > 0){
                            $TotalCreditValue+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
                            $creditAmount+=$value['data']->totalCreditAmount - $value['data']->totalDebitAmount;
                        }else{
                            $TotalDebitValue+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
                            $debitAmount+=$value['data']->totalDebitAmount - $value['data']->totalCreditAmount;
                        }
                    }
                }
            }

            if($typekey == 0 || $typekey == 2){ 
                if($typekey == 0){
                    $revenueAmount = $TotalCreditValue - $TotalDebitValue; 
                } else if($typekey == 2){
                    $otherRevenueAmount = $TotalCreditValue - $TotalDebitValue;                             
                }

                
            }else if($typekey == 1 || $typekey == 3){
                if($typekey == 1){
                    $costOfSalesAmount = $TotalDebitValue - $TotalCreditValue ; 
                }else if($typekey == 3){
                    $expenceAmount = $TotalDebitValue - $TotalCreditValue ; 
                }
            }

            if($typekey == 1){
                $grossProfit = $revenueAmount - $costOfSalesAmount;
            }else if($typekey == 3){
                $netProfit = ($revenueAmount - $costOfSalesAmount) +($otherRevenueAmount - $expenceAmount);
            }
        }
        return $netProfit;
    }

    private function getNetProfitAmountLocationWise($profitAndLostData, $locationData)
    {
        $kk  = $locationData['locationCode'].'-'.$locationData['locationID'];
        $netProfit = 0.00;
        foreach ($profitAndLostData as $typekey => $typeData) {
            $TotalDebitValue = 0;
            $TotalCreditValue = 0;

            foreach ($typeData as $tbalance) {
                if(isset($tbalance['financeAccounts'][0]['child'])){
                    foreach ($tbalance['financeAccounts'][0]['child'] as $key => $value) {
                        $creditAmount = 0;
                        $debitAmount = 0;
                        if(isset($value['child'])){
                            $amountData = $this->calculateAmountsForProfitAndLostLocationwise($value['child'], $locationData);
                            if($amountData['totalCreditAmount'] - $amountData['totalDebitAmount'] > 0){
                                $TotalCreditValue+=$amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                                $creditAmount = $amountData['totalCreditAmount'] - $amountData['totalDebitAmount'];
                            } else {
                                $TotalDebitValue+=$amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
                                $debitAmount = $amountData['totalDebitAmount'] - $amountData['totalCreditAmount'];
                            }
                        }

                        if($value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk] > 0){
                            $TotalCreditValue+=$value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk];
                            $creditAmount+=$value['data']->totalCreditAmount[$kk] - $value['data']->totalDebitAmount[$kk];
                        }else{
                            $TotalDebitValue+=$value['data']->totalDebitAmount[$kk] - $value['data']->totalCreditAmount[$kk];
                            $debitAmount+=$value['data']->totalDebitAmount[$kk] - $value['data']->totalCreditAmount[$kk];
                        }
                    }
                }
            }

            if($typekey == 0 || $typekey == 2){ 
                if($typekey == 0){
                    $revenueAmount = $TotalCreditValue - $TotalDebitValue; 
                } else if($typekey == 2){
                    $otherRevenueAmount = $TotalCreditValue - $TotalDebitValue;                             
                }

                
            }else if($typekey == 1 || $typekey == 3){
                if($typekey == 1){
                    $costOfSalesAmount = $TotalDebitValue - $TotalCreditValue ; 
                }else if($typekey == 3){
                    $expenceAmount = $TotalDebitValue - $TotalCreditValue ; 
                }
            }

            if($typekey == 1){
                $grossProfit = $revenueAmount - $costOfSalesAmount;
            }else if($typekey == 3){
                $netProfit = ($revenueAmount - $costOfSalesAmount) +($otherRevenueAmount - $expenceAmount);
            }
        }
        return $netProfit;
    }


    /** 
    * use to get related document comments for given journal entries.
    *
    **/
    private function getDocumentCommentForRelatedJournalEntry($jEntries)
    {
        $returnJEntity = [];
        $removedCreditDebit = [];
        foreach ($jEntries as $key => $singleValue) {
            $updated = true;
            switch ($singleValue['documentTypeID']){
                case '1' :
                    $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByInvoiceIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$invoiceDetails['salesInvoiceID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        $singleValue['relatedDocComment'] = $invoiceDetails['salesInvoiceComment'];
                        $singleValue['documentType'] = 'Sales Invoice';
                        $singleValue['documentCode'] = $invoiceDetails['salesInvoiceCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $invoiceDetails['customerCode'].' - '.$invoiceDetails['customerName'];
                    }
                    break;
                case '4' :
                    $deliveryNoteDetails = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteByIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$deliveryNoteDetails['deliveryNoteID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        
                        $singleValue['relatedDocComment'] = $deliveryNoteDetails['deliveryNoteComment'];
                        $singleValue['documentType'] = 'Delivery Note';
                        $singleValue['documentCode'] = $deliveryNoteDetails['deliveryNoteCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $deliveryNoteDetails['customerCode'].' - '.$deliveryNoteDetails['customerName'];
                    }
                    break;
                case '7' :
                    $paymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByPaymentIDForJE($singleValue['journalEntryDocumentID']);
                    if(sizeof($paymentDetails) == 0){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        $paymentDet = $this->createPayDataSet($paymentDetails);
                        
                        $singleValue['relatedDocComment'] = $paymentDet['docComment'];
                        $singleValue['documentType'] = 'Sales Payment';
                        $singleValue['documentCode'] = $paymentDet['docCode'];
                        $singleValue['childDoc'] = $paymentDet['childDoc'];
                        $singleValue['cusName'] = $paymentDet['cusName'];
                    }
                         
                    break;
                case '5' :
                    $returnDetails = $this->CommonTable('SalesReturnsTable')->getReturnforSearchForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$returnDetails['salesReturnID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $returnDetails['salesReturnComment'];
                        $singleValue['documentType'] = 'Sales Return';
                        $singleValue['documentCode'] = $returnDetails['salesReturnCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $returnDetails['customerCode'].' - '.$returnDetails['customerName'];
                    }
                    break;
                case '6' :
                    $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$creditNoteDetails['creditNoteID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $creditNoteDetails['creditNoteComment'];
                        $singleValue['documentType'] = 'Credit Note';
                        $singleValue['documentCode'] = $creditNoteDetails['creditNoteCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $creditNoteDetails['customerCode'].' - '.$creditNoteDetails['customerName'];
                    }
                    break;
                case '17' :
                    $creditNotePayment = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentIDForJE($singleValue['journalEntryDocumentID'], NULL)->current();
                    if(!$creditNotePayment['creditNotePaymentID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $creditNotePayment['creditNotePaymentMemo'];
                        $singleValue['documentType'] = 'Credit Note Payment';
                        $singleValue['documentCode'] = $creditNotePayment['creditNotePaymentCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $creditNotePayment['customerCode'].' - '.$creditNotePayment['customerName'];
                    }
                    break;
                case '12' :
                    $piDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$piDetails['purchaseInvoiceID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $piDetails['purchaseInvoiceComment'];
                        $singleValue['documentType'] = 'Purchase Invoice';
                        $singleValue['documentCode'] = $piDetails['purchaseInvoiceCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $piDetails['supplierCode'].' - '.$piDetails['supplierName'];
                    }
                    break;
                case '14' :
                    $suppPaymentDetails = $this->CommonTable('SupplierPaymentsTable')->getPaymentsByPaymentIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$suppPaymentDetails['outgoingPaymentID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $suppPaymentDetails['outgoingPaymentMemo'];
                        $singleValue['documentType'] = 'Supplier Payment';
                        $singleValue['documentCode'] = $suppPaymentDetails['outgoingPaymentCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $suppPaymentDetails['supplierCode'].' - '.$suppPaymentDetails['supplierName'];
                    }
                    break;
                case '11' :
                    $prDetails = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnByIdForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$prDetails['purchaseReturnID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $prDetails['purchaseReturnComment'];
                        $singleValue['documentType'] = 'Purchase Return';
                        $singleValue['documentCode'] = $prDetails['purchaseReturnCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $prDetails['supplierCode'].' - '.$prDetails['supplierName'];
                    }
                    break;
                case '13' :
                    $debitNoteDetails = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$debitNoteDetails['debitNoteID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $debitNoteDetails['debitNoteComment'];
                        $singleValue['documentType'] = 'Debit Note';
                        $singleValue['documentCode'] = $debitNoteDetails['debitNoteCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $debitNoteDetails['supplierCode'].' - '.$debitNoteDetails['supplierName'];
                    }
                    break;
                case '18' :
                    $debitNotePaymentDetails = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentIDForJE($singleValue['journalEntryDocumentID'], NULL)->current();
                    if(!$debitNotePaymentDetails['debitNotePaymentID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $debitNotePaymentDetails['debitNotePaymentMemo'];
                        $singleValue['documentType'] = 'Debit Note Payment';
                        $singleValue['documentCode'] = $debitNotePaymentDetails['debitNotePaymentCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $debitNotePaymentDetails['supplierCode'].' - '.$debitNotePaymentDetails['supplierName'];
                    }
                    break;
                case '10' :
                    $grnDetails = $this->CommonTable('Inventory\Model\GrnTable')->grnDetailsByGrnIdForJE($singleValue['journalEntryDocumentID']);
                    if(!$grnDetails['grnID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $grnDetails['grnComment'];
                        $singleValue['documentType'] = 'GRN';
                        $singleValue['documentCode'] = $grnDetails['grnCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $grnDetails['supplierCode'].' - '.$grnDetails['supplierName'];
                    }
                    break;
                case '16' :
                    $adjustmentDetails = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getDataForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$adjustmentDetails['goodsIssueID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $adjustmentDetails['goodsIssueReason'];
                        $singleValue['documentCode'] = $adjustmentDetails['goodsIssueCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = '-';
                        if($adjustmentDetails['goodsIssueTypeID'] == '2'){
                            $singleValue['documentType'] = 'Positive Adjustment';
                        } else {
                            $singleValue['documentType'] = 'Negative Adjustment';
                        }
                    }
                    break;
                case '20' :
                    $pvDetails = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherByPaymentVoucherIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$pvDetails['paymentVoucherID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        $singleValue['relatedDocComment'] = $pvDetails['paymentVoucherComment'];
                        $singleValue['documentType'] = 'Payment Voucher';
                        $singleValue['documentCode'] = $pvDetails['paymentVoucherCode'];
                        $singleValue['childDoc'] = '-';
                        if($pvDetails['paymentVoucherSupplierID'] != ''){
                            $singleValue['cusName'] = $pvDetails['supplierCode'].' - '.$pvDetails['supplierName'];
                        } else {
                            $singleValue['cusName'] = $pvDetails['financeAccountsCode'].' - '.$pvDetails['financeAccountsName'];
                        }
                        
                    }

                    break;
                case '32' :
                    $chequeData = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getChequeDepositByChequeDeposiIdForJE($singleValue['journalEntryDocumentID']);
                    if($chequeData['chequeDepositId']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                       $updated = false; 
                    }
                    break;
                default :
                    break;
            }
            if($updated){
                $returnJEntity[] = $singleValue;
            }
        }
        $retrunAllData = array(
            'returnJEntity' => $returnJEntity,
            'creditDebitRemovedValues' => $removedCreditDebit,
            );
        return $retrunAllData;
    }

    public function createPayDataSet($paymentDetails)
    {
        $docType = [];
        $returnData = [];
        foreach ($paymentDetails as $key => $value) {
            
            $returnData['cusName'] = $value['customerCode'].' - '.$value['customerName'];
            $returnData['docCode'] = $value['incomingPaymentCode'];
            $returnData['docComment'] = $value['incomingPaymentMemo'];
            if($value['incomingPaymentMethodChequeNumber'] != null){
                $docType[] = 'cheque No - '. $value['incomingPaymentMethodChequeNumber'];
            } else if($value['incomingPaymentMethodCreditReceiptNumber'] != null){
                $docType[] = 'credit Card ref - '. $value['incomingPaymentMethodCreditReceiptNumber'];
            }
        }
        $returnData['childDoc'] = implode("/", $docType);
        return $returnData;
            
                
    }

    /**
    * This function is used to generate cashflow view report
    **/
    public function cashFlowReportAction()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');

        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
           
        $startDate = $postData['startDate'];
        $endDate = $postData['endDate'];
      
        $cashFlowData = $this->getService('FinanceReportService')->getCashFlowData($startDate, $endDate);

        $newDate   = date('Y-m-d', strtotime($startDate . ' -1 day'));

        $lastYearCashFlowData = $this->getService('FinanceReportService')->getCashFlowData('', $newDate);

        $cD = $this->getCompanyDetails();
        $translator = new Translator();
        $name = $translator->translate('Statement of cash flows');
        $period = $startDate . ' - ' . $endDate;
        
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $cashFlowView = new ViewModel(array(
            'cashFlowData' => $cashFlowData['cashInOutFlowData'],
            'cashFlowTotal' => $lastYearCashFlowData['cashFlowTotal'],
            'startDate' => $startDate,
            'endDate' => $endDate,
            'headerTemplate' => $headerViewRender,
            'cD' => $cD)
        );
        
        $cashFlowView->setTemplate('reporting/finance-report/generate-cash-flow-view');

        $this->html = $cashFlowView;
        $this->status = true;
        return $this->JSONRespondHtml();
    } 

    public function cashFlowReportPdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data
            
            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['cash-flow'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }


    public function cashFlowReportCsvAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        
        try {
            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['cash-flow'], 'csv');
        
            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    public function auditSheduleAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
           
        $startDate = $postData['startDate'];
        $endDate = $postData['endDate'];
        $reportModel = $postData['reportModel'];
        $accountIDs = $postData['accountID'];
        $locationIDs = $postData['locationID'];
        $dimensionType = $postData['dimensionType'];
        $dimensionValue = $postData['dimensionValue'];
        $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

        $auditSheduleData = $this->getService('FinanceReportService')->getAuditSheduleData($startDate, $endDate, $accountIDs, $dimensionType, $dimensionValue, $locationIDs)['auditSheduleData'];

        $cD = $this->getCompanyDetails();
    
        $translator = new Translator();
        $name = $translator->translate('Audit Shedule Report');
        $period = $startDate . ' - ' . $endDate;
        
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $auditSheduleDataView = new ViewModel(array(
            'auditSheduleData' => $auditSheduleData,
            'headerTemplate' => $headerViewRender,
            'hideZeroVal' => $hideZeroVal,
            'cD' => $companyDetails)
        );
        
        $auditSheduleDataView->setTemplate('reporting/finance-report/generate-audit-shedule-view');

        $this->html = $auditSheduleDataView;
        $this->status = true;
        return $this->JSONRespondHtml();
    }


    public function generateAuditShedulePdfAction()
    {

        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->returnJsonError('Invalid request');
        }

        $postData = $this->getRequest()->getPost()->toArray();
       
        $startDate = $postData['startDate'];
        $endDate = $postData['endDate'];
        $reportModel = $postData['reportModel'];
        $accountIDs = $postData['accountID'];
        $locationIDs = $postData['locationID'];
        $dimensionType = $postData['dimensionType'];
        $dimensionValue = $postData['dimensionValue'];
        $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

        $auditSheduleData = $this->getService('FinanceReportService')->getAuditSheduleData($startDate, $endDate, $accountIDs, $dimensionType, $dimensionValue, $locationIDs)['auditSheduleData'];

        $cD = $this->getCompanyDetails();
    
        $translator = new Translator();
        $name = $translator->translate('Audit Shedule Report');
        $period = $startDate . ' - ' . $endDate;
        
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);


        $auditSheduleDataView = new ViewModel(array(
            'auditSheduleData' => $auditSheduleData,
            'headerTemplate' => $headerViewRender,
            'hideZeroVal' => $hideZeroVal,
            'cD' => $companyDetails)
        );

        $auditSheduleDataView->setTemplate('reporting/finance-report/generate-audit-shedule-pdf');
        $auditSheduleDataView->setTerminal(true);

        // get rendered view into variable
        $htmlContent = $this->viewRendererHtmlContent($auditSheduleDataView);
        $pdfPath = $this->downloadPDF('finance-report', $htmlContent, true, true);
        
        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    
    public function auditSheduleSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $postData = $this->getRequest()->getPost()->toArray();
       
            $startDate = $postData['startDate'];
            $endDate = $postData['endDate'];
            $reportModel = $postData['reportModel'];
            $accountIDs = $postData['accountID'];
            $locationIDs = $postData['locationID'];
            $dimensionType = $postData['dimensionType'];
            $dimensionValue = $postData['dimensionValue'];
            $hideZeroVal = ($postData['hideZeroVal'] == 'true') ? true: false;

            $auditSheduleData = $this->getService('FinanceReportService')->getAuditSheduleData($startDate, $endDate, $accountIDs, $dimensionType, $dimensionValue, $locationIDs)['auditSheduleData'];

            $companyDetails = $this->getCompanyDetails();

            if ($auditSheduleData) {
                $title = '';
                $tit = 'Audit Shedule Report';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";
                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";
                $in = [$companyDetails[0]->companyName, $companyDetails[0]->companyAddress, 'Tel: ' . $companyDetails[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";
                $title.=implode(",", []) . "\n";
                $arrs = ["Account Code And Name", "Opening Balance", "Moving Balance", "Closing Balance"];
                

                $header = implode(",", $arrs);

                $arr = "";
                foreach ($auditSheduleData as $key => $Data) {

                    $openingBalanaceAmount = 0;
                    $movingBalanaceAmount = 0;
                    $currentBalanaceAmount = 0;
                    
                    if ($Data['accountsData']['financeAccountsTypeID'] == 1 || $Data['accountsData']['financeAccountsTypeID'] == 3 || $Data['accountsData']['financeAccountsTypeID'] == 6) {

                            

                        $openingBalanaceAmount =  $Data['openingBalanceData']['totalDebitAmount'] -  $Data['openingBalanceData']['totalCreditAmount'];

                        if (empty($Data['movementCreditBalance'])) {
                            $Data['movementCreditBalance'] = 0;
                        }

                        if (empty($Data['movementDebitBalance'])) {
                            $Data['movementDebitBalance'] = 0;
                        }


                        $movingBalanaceAmount = $Data['movementDebitBalance'] - $Data['movementCreditBalance'];


                        $currentBalanaceAmount = ($Data['openingBalanceData']['totalDebitAmount'] + $Data['movementDebitBalance']) - ($Data['openingBalanceData']['totalCreditAmount'] + $Data['movementCreditBalance']);


                    } elseif ($Data['accountsData']['financeAccountsTypeID'] == 2 || $Data['accountsData']['financeAccountsTypeID'] == 4 || $Data['accountsData']['financeAccountsTypeID'] == 5  || $Data['accountsData']['financeAccountsTypeID'] == 7) {


                        $openingBalanaceAmount =  $Data['openingBalanceData']['totalCreditAmount'] - $Data['openingBalanceData']['totalDebitAmount'];

                        if (empty($Data['movementCreditBalance'])) {
                            $Data['movementCreditBalance'] = 0;
                        }

                        if (empty($Data['movementDebitBalance'])) {
                            $Data['movementDebitBalance'] = 0;
                        }

                        $movingBalanaceAmount = $Data['movementCreditBalance'] - $Data['movementDebitBalance'];


                        $currentBalanaceAmount = ($Data['openingBalanceData']['totalCreditAmount'] + $Data['movementCreditBalance']) - ($Data['openingBalanceData']['totalDebitAmount'] + $Data['movementDebitBalance']);

                    }

                    if ($hideZeroVal) {
                        if ($openingBalanaceAmount != 0 || $movingBalanaceAmount != 0 || $currentBalanaceAmount != 0) {
                            $test = [];

                            $accountName = $Data['accountsData']['financeAccountsCode']." - ".$Data['accountsData']['financeAccountsName'];

                            array_push($test, $accountName);
                            array_push($test, ($openingBalanaceAmount < 0) ? '('.($openingBalanaceAmount * -1).')' : $openingBalanaceAmount);
                            array_push($test, ($movingBalanaceAmount < 0) ? '('.($movingBalanaceAmount * -1).')' : $movingBalanaceAmount);
                            array_push($test, ($currentBalanaceAmount < 0) ? '('.($currentBalanaceAmount * -1).')' : $currentBalanaceAmount);

                            $arr .= implode(",", $test). "\n";

                        }
                    } else {
                        $test = [];

                        $accountName = $Data['accountsData']['financeAccountsCode']." - ".$Data['accountsData']['financeAccountsName'];

                        array_push($test, $accountName);
                        array_push($test, ($openingBalanaceAmount < 0) ? '('.($openingBalanaceAmount * -1).')' : $openingBalanaceAmount);
                        array_push($test, ($movingBalanaceAmount < 0) ? '('.($movingBalanaceAmount * -1).')' : $movingBalanaceAmount);
                        array_push($test, ($currentBalanaceAmount < 0) ? '('.($currentBalanaceAmount * -1).')' : $currentBalanaceAmount);

                        $arr .= implode(",", $test). "\n";
                    }

                    
                }

            } else {
                $arr = "no matching records found\n";
            }
            $name = "audit_shedule_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
        
    }
}
