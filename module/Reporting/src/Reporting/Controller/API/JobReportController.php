<?php

/**
 * @author Sandun <sandun@thinkcube.com>
 * This file contains action methods for the payment voucher report controller
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;

class JobReportController extends CoreController
{

    /**
     * @author Sandun <sandun@thinkcube.com>
     * view invoice value report related to job card
     * @return JSONRespondHtml
     */
    public function viewEmployeeIncentiveReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $empIDs = $request->getPost('employeeIds');
            $zeroService = ($request->getPost('zeroService') == "true") ? true : false;
            $isAllEmployees = $request->getPost('isAllEmployees');
            $serviceIds = $request->getPost('serviceIds');
            $locIDs = $this->getActiveAllLocationsIds();
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $isQtyBase = $request->getPost('isQtyBase');
            $isAmountBase = $request->getPost('isAmountBase');
            $translator = new Translator();
            $name = $translator->translate('Job Employee Incentive Report');
            $reportType = null;
            $period = $fromDate . ' - ' . $toDate;
            $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));

            if ($isQtyBase == "true" && $isAmountBase == "false") {
                $reportType = "qty";
            } elseif ($isQtyBase == "false" && $isAmountBase == "true") {
                $reportType = "amount";
            }


            $dataLists = $this->getJobEmployeeIncentiveDetails($empIDs, $locIDs, $isAllEmployees, $serviceIds, $fromDate, $toDate, $reportType,$zeroService);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);


            $viewEmpIncentive = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataLists['dataSet'],
                'headers' => $dataLists['headers'],
                'headerTemplate' => $headerViewRender,
                'reportType' => $reportType

            ));


            $viewEmpIncentive->setTemplate('reporting/job-report/generate-job-employee-incentive-pdf');

            $this->html = $viewEmpIncentive;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
        
    }

    public function getJobEmployeeIncentiveDetails($empIDs, $locIDs, $isAllEmployees, $serviceIds, $fromDate, $toDate, $reportType, $zeroService)
    {

        if (!empty($empIDs)) {
            $employees = $this->CommonTable('Jobs\Model\EmployeeTable')->getEmployeeByIds($empIDs);
        } else {
            $employees = $this->CommonTable('Jobs\Model\EmployeeTable')->fetchActiveEmployees();
        }

        $processEmpList = $this->processEmployees($employees);

        if (!empty($serviceIds)) {
            $services = $this->CommonTable('Jobs\Model\TaskTable')->getTaskByTaskIDs($serviceIds);
        } else {
            $services = $this->CommonTable('Jobs\Model\TaskTable')->fetchAll();
        }
        $dataSet = [];
        $tempData = [];
        foreach ($services as $key => $value) {
            if (!array_key_exists($value['taskID'].':'.$value['taskName'], $tempData)) {
                $tempData[]=$value['taskID'].':'.$value['taskName'];
            }
            foreach ($processEmpList as $key1 => $val) {
                foreach ($val as $key2 => $v) {
                    $dataSet[$key1][$value['taskID'].':'.$value['taskName']][$v['employeeID']] = 0;

                }
            }
        }


        foreach ($dataSet as $key3 => $value3) {
            foreach ($value3 as $key4 => $value4) {
                foreach ($value4 as $key5 => $value5) {
                    $arr = split(':', $key4);
                    $taskEmployee = $this->CommonTable('Jobs\Model\JobTaskEmployeeTable')->getJobTaskEmployeeByTaskIDEmpID($arr[0],$key5, $fromDate, $toDate, $locIDs);
                    $dataSet[$key3][$key4]['taskName'] = $arr[1];
                    if ($reportType == 'qty') {
                        $dataSet[$key3][$key4][$key5] = count($taskEmployee);
                    } elseif ($reportType == "amount") {
                        $amount = 0;
                        foreach ($taskEmployee as $key2 => $v) {
                            $amount += floatval($v['incentiveValue']);
                        }
                        $dataSet[$key3][$key4][$key5] = number_format($amount, 2, '.', ''); 
                    }   
                }
            }
        }


        if ($zeroService) {
            $finalDataSet = [];
            foreach ($dataSet as $key55 => $value55) {
                foreach ($value55 as $key56 => $value56) {
                    $zeroCount = 0;
                    foreach ($value56 as $key57 => $value57) {
                        if ($key57 != 'taskName') {
                            if (floatval($value57) == 0) {
                                $zeroCount += 1;
                            }
                        }
                    }

                    if ($zeroCount != (sizeof($value56)-1)) {
                        $finalDataSet[$key55][$key56] = $value56;
                    }
                }
            }

            $returnData = [
                'dataSet' => $finalDataSet,
                'headers' => $processEmpList
            ];
        } else {
            $returnData = [
                'dataSet' => $dataSet,
                'headers' => $processEmpList
            ];
        }

        return $returnData;

    }

    public function getJobEmployeeIncentiveDetailsForCsv($empIDs, $locIDs, $isAllEmployees, $serviceIds, $fromDate, $toDate, $reportType, $zeroService)
    {

        if (!empty($empIDs)) {
            $employees = $this->CommonTable('Jobs\Model\EmployeeTable')->getEmployeeByIds($empIDs);
        } else {
            $employees = $this->CommonTable('Jobs\Model\EmployeeTable')->fetchActiveEmployees();
        }

        $processEmpList = $this->processEmployees($employees, true);


        if (!empty($serviceIds)) {
            $services = $this->CommonTable('Jobs\Model\TaskTable')->getTaskByTaskIDs($serviceIds);
        } else {
            $services = $this->CommonTable('Jobs\Model\TaskTable')->fetchAll();
        }
        $dataSet = [];
        $tempData = [];
        foreach ($services as $key => $value) {
            if (!array_key_exists($value['taskID'].':'.$value['taskName'], $tempData)) {
                $tempData[]=$value['taskID'].':'.$value['taskName'];
            }
            foreach ($processEmpList as $key1 => $val) {
                foreach ($val as $key2 => $v) {
                    $dataSet[$key1][$value['taskID'].':'.$value['taskName']][$v['employeeID']] = 0;

                }
            }
        }


        foreach ($dataSet as $key3 => $value3) {
            foreach ($value3 as $key4 => $value4) {
                foreach ($value4 as $key5 => $value5) {
                    $arr = split(':', $key4);
                    $taskEmployee = $this->CommonTable('Jobs\Model\JobTaskEmployeeTable')->getJobTaskEmployeeByTaskIDEmpID($arr[0],$key5, $fromDate, $toDate, $locIDs);
                    $dataSet[$key3][$key4]['taskName'] = $arr[1];
                    if ($reportType == 'qty') {
                        $dataSet[$key3][$key4][$key5] = count($taskEmployee);
                    } elseif ($reportType == "amount") {
                        $amount = 0;
                        foreach ($taskEmployee as $key2 => $v) {
                            $amount += floatval($v['incentiveValue']);
                        }
                        $dataSet[$key3][$key4][$key5] = number_format($amount, 2, '.', ''); 
                    }   
                }
            }
        }

    
        if ($zeroService) {
            $finalDataSet = [];
            foreach ($dataSet as $key55 => $value55) {
                foreach ($value55 as $key56 => $value56) {
                    $zeroCount = 0;
                    foreach ($value56 as $key57 => $value57) {
                        if ($key57 != 'taskName') {
                            if (floatval($value57) == 0) {
                                $zeroCount += 1;
                            }
                        }
                    }

                    if ($zeroCount != (sizeof($value56)-1)) {
                        $finalDataSet[$key55][$key56] = $value56;
                    }
                }
            }

            $returnData = [
                'dataSet' => $finalDataSet,
                'headers' => $processEmpList
            ];
        } else {
            $returnData = [
                'dataSet' => $dataSet,
                'headers' => $processEmpList
            ];
        }


        return $returnData;

    }

    public function processEmployees($employees, $forCsv = false) {

        if ($forCsv) {
            $paginator = 0;
            foreach ($employees as $key => $value) {
                $processedEmpSet[$paginator][] = [
                    'employeeID' => $value['employeeID'],
                    'employeeFirstName' => $value['employeeFirstName']

                ];

            }
        } else {

            $paginator = 0;
            $processedEmpSet[$paginator] = [];
            foreach ($employees as $key => $value) {
                if (sizeof($processedEmpSet[$paginator]) !== 10) {
                   $processedEmpSet[$paginator][] = [
                        'employeeID' => $value['employeeID'],
                        'employeeFirstName' => $value['employeeFirstName']

                   ]; 
                } else {
                    $paginator++;
                    $processedEmpSet[$paginator][] = [
                        'employeeID' => $value['employeeID'],
                        'employeeFirstName' => $value['employeeFirstName']

                   ];
                }

            }
        }
        return $processedEmpSet;
    }


    /**
     * @author Sandun <sandun@thinkcube.com>
     * view invoice value report related to job card
     * @return JSONRespondHtml
     */
    public function employeeIncentiveReportPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $empIDs = $request->getPost('employeeIds');
            $zeroService = ($request->getPost('zeroService') == "true") ? true : false;
            $isAllEmployees = $request->getPost('isAllEmployees');
            $serviceIds = $request->getPost('serviceIds');
            $locIDs = $this->getActiveAllLocationsIds();
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $isQtyBase = $request->getPost('isQtyBase');
            $isAmountBase = $request->getPost('isAmountBase');
            $translator = new Translator();
            $name = $translator->translate('Job Employee Incentive Report');
            $reportType = null;
            $period = $fromDate . ' - ' . $toDate;
            $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));

            if ($isQtyBase == "true" && $isAmountBase == "false") {
                $reportType = "qty";
            } elseif ($isQtyBase == "false" && $isAmountBase == "true") {
                $reportType = "amount";
            }


            $dataLists = $this->getJobEmployeeIncentiveDetails($empIDs, $locIDs, $isAllEmployees, $serviceIds, $fromDate, $toDate, $reportType, $zeroService);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewEmpIncentive = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataLists['dataSet'],
                'headers' => $dataLists['headers'],
                'headerTemplate' => $headerViewRender,
                'reportType' => $reportType

            ));
            $viewEmpIncentive->setTemplate('reporting/job-report/generate-job-employee-incentive-pdf');

            $viewEmpIncentive->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewEmpIncentive);
            $pdfPath = $this->downloadPDF('job-employee-incentive', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
        
    }


    /**
     * @author Sandun <sandun@thinkcube.com>
     * view invoice value report related to job card
     * @return JSONRespondHtml
     */
    public function generateEmployeeIncentiveSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $empIDs = $request->getPost('employeeIds');
            $zeroService = ($request->getPost('zeroService') == "true") ? true : false;
            $isAllEmployees = $request->getPost('isAllEmployees');
            $serviceIds = $request->getPost('serviceIds');
            $locIDs = $this->getActiveAllLocationsIds();
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $isQtyBase = $request->getPost('isQtyBase');
            $isAmountBase = $request->getPost('isAmountBase');
            $translator = new Translator();
            $name = $translator->translate('Job Employee Incentive Report');
            $reportType = null;
            $period = $fromDate . ' - ' . $toDate;
            $toDate = date('Y-m-d', strtotime($toDate . ' +1 day'));

            if ($isQtyBase == "true" && $isAmountBase == "false") {
                $reportType = "qty";
            } elseif ($isQtyBase == "false" && $isAmountBase == "true") {
                $reportType = "amount";
            }


            $dataLists = $this->getJobEmployeeIncentiveDetailsForCsv($empIDs, $locIDs, $isAllEmployees, $serviceIds, $fromDate, $toDate, $reportType, $zeroService);
            $companyDetails = $this->getCompanyDetails();

            if ($dataLists) {
                $title = '';
                $tit = 'Job Employee Incentive';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";
                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";
                $in = [$companyDetails[0]->companyName, $companyDetails[0]->companyAddress, 'Tel: ' . $companyDetails[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";
                $title.=implode(",", []) . "\n";
                $arrs = ["Services"];
                $arr = "";
                foreach ($dataLists['dataSet'] as $key => $value) {
                    
                    foreach ($dataLists['headers'][$key] as $key1 => $value1) {
                        array_push($arrs, $value1['employeeFirstName']);
                    }
                    
                    # code...
                }

                $header = implode(",", $arrs);

                $Services = [];
                foreach ($dataLists['dataSet'][0] as $key => $value) {
                    
                    $Services[$key] = [];
                    
                    # code...
                }

                $processdData = [];
                foreach ($Services as $key => $value) {
                    foreach ($dataLists['dataSet'] as $key1 => $value1) {
                        foreach ($value1 as $key2 => $value2) {
                            $tskNme= $value2['taskName'];
                            unset($value2['taskName']);
                            if ($key == $key2) {
                                foreach ($value2 as $key3 => $value3) {
                                    # code...
                                    $processdData[$tskNme][] = $value3;  
                                }
                            }
                        }
                           # code...
                    }
                }

                $arr = "";
                foreach ($processdData as $key5 => $value5) {
                    $dd = [];
                    array_push($dd, $key5);
                    $test = array_merge($dd,$value5);
                    $arr .= implode(",", $test). "\n";
                }

            } else {
                $arr = "no matching records found\n";
            }
            $name = "item_wise_stock_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();

        }
        
    }


}
