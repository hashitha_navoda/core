<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains item transfer Report related controller functions
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class ItemTransferReportController extends CoreController
{

    protected $userRoleID;
    protected $allLocations;
    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'inve_report_upper_menu';

    public function viewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationFromIDs = $request->getPost('locationFromIds');
            $locationToIDs = $request->getPost('locationToIds');
            $fromDate = $request->getPost('dateFrom') ? $request->getPost('dateFrom') : null;
            $toDate = $request->getPost('dateTo') ? $request->getPost('dateTo') : null;
            $translator = new Translator();
            $name = $translator->translate('Item Transfer Report');

            $companyDetails = $this->getCompanyDetails();
            $locationsInTransferLists = $this->itemInTransferDetails($locationFromIDs, $locationToIDs, $fromDate, $toDate);

            $period = $fromDate && $toDate ? $fromDate . ' - ' . $toDate : null;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);
            $time = $this->getUserDateTime();

            $locListView = new ViewModel(array(
                'iL' => $locationsInTransferLists,
                'cD' => $companyDetails,
                'time' => $time,
                'headerTemplate' => $headerViewRender
            ));
            $locListView->setTemplate('reporting/item-transfer-report/generate-item-transfer-pdf');

            $this->html = $locListView;
            $this->msg = $this->getMessage('INFO_ITEMTRANS_REPORT_VIEW');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function itemWiseViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationFromIDs = $request->getPost('locationFromIds');
            $locationToIDs = $request->getPost('locationToIds');
            $fromDate = $request->getPost('dateFrom') ? $request->getPost('dateFrom') : null;
            $toDate = $request->getPost('dateTo') ? $request->getPost('dateTo') : null;
            $allProducts = $request->getPost('allProducts');
            $cusProducts = $request->getPost('cusProducts') ? $request->getPost('cusProducts') : null;
            $translator = new Translator();
            $name = $translator->translate('Item Wise Transfer Report');

            $companyDetails = $this->getCompanyDetails();

            if ($allProducts === '1' || $allProducts === 1) {
                $locationsInTransferLists = $this->itemWiseTransferDetails($locationFromIDs, $locationToIDs, $fromDate, $toDate);
            } else {
                $locationsInTransferLists = $this->itemWiseTransferDetails($locationFromIDs, $locationToIDs, $fromDate, $toDate, $cusProducts);
            }

            $period = $fromDate && $toDate ? $fromDate . ' - ' . $toDate : null;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);
            $time = $this->getUserDateTime();

            $locListView = new ViewModel(array(
                'iL' => $locationsInTransferLists,
                'cD' => $companyDetails,
                'time' => $time,
                'headerTemplate' => $headerViewRender
            ));
            $locListView->setTemplate('reporting/item-transfer-report/generate-item-wise-transfer-pdf');

            $this->html = $locListView;
            $this->msg = $this->getMessage('INFO_ITEMTRANS_REPORT_VIEW');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $locationFromIds
     * @param type $locationToIds
     * @return $locationsTransferLists
     */
    private function itemOutTransferDetails($locationFromIds = null, $locationToIds = null)
    {
        if (isset($locationFromIds) && isset($locationToIds)) {
            $locationsFromArr = array('locationTo' => array_fill_keys($locationToIds, '')); //create 'locations' element and push locationID's to it
            $locationsTransferLists = array_fill_keys($locationFromIds, $locationsFromArr);

            foreach ($locationsTransferLists as $key => $value) {
                foreach ($value['locationTo'] as $index => $subvalue) {
                    if (array_key_exists($key, $locationsTransferLists)) {
                        unset($locationsTransferLists[$key]['locationTo'][$key]);
                        break;
                    }
                }
            }

            $resTransfer = $this->CommonTable('Inventory\Model\TransferTable')->getTransferByLocIds($locationFromIds, $locationToIds);

            foreach ($resTransfer as $row) {
                $locationsTransferLists[$row['locFromID']]['locFromName'] = $row['locFromName'];
                $locationsTransferLists[$row['locFromID']]['locationTo'][$row['locToID']][$row['tID']][$row['pID']] = $row;

                //get quantity and unit price according to dispaly uom
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['pID']);
                $thisQty = $this->getProductQuantityViaDisplayUom($row['qty'], $productUom);
                $qty = ($thisQty['quantity'] == 0) ? 0 : $thisQty['quantity'];

                $locationsTransferLists[$row['locFromID']]['locationTo'][$row['locToID']][$row['tID']][$row['pID']]['uomAbbr'] = $qty;
                $locationsTransferLists[$row['locFromID']]['locationTo'][$row['locToID']][$row['tID']][$row['pID']]['qty'] = $thisQty['uomAbbr'];
            }

            return $locationsTransferLists;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $locationFromIds
     * @param type $locationToIds
     * @param type $fromDate
     * @param type $toDate
     * @return $locationsInTransferLists
     */
    private function itemInTransferDetails($locationFromIds = null, $locationToIds = null, $fromDate = null, $toDate = null)
    {
        if (isset($locationFromIds) && isset($locationToIds)) {
            $locationsInArr = array('locationIn' => array_fill_keys($locationFromIds, '')); //create 'locations' element and push locationID's to it
            $locationsInTransferLists = array_fill_keys($locationToIds, $locationsInArr);

            foreach ($locationsInTransferLists as $key => $value) {
                foreach ($value['locationIn'] as $index => $subvalue) {
                    if (array_key_exists($key, $locationsInTransferLists)) {
                        unset($locationsInTransferLists[$key]['locationTo'][$key]);
                        break;
                    }
                }
            }

            $resInTransfer = $this->CommonTable('Inventory\Model\TransferTable')->getTransferByLocIds($locationToIds, $locationFromIds, $fromDate, $toDate);
            foreach ($resInTransfer as $row) {
                //get quantity and unit price according to dispaly uom
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['pID']);
                $thisQty = $this->getProductQuantityViaDisplayUom($row['qty'], $productUom);
                $qty = ($thisQty['quantity'] == 0) ? 0 : $thisQty['quantity'];

                $locationsInTransferLists[$row['locToID']]['locToName'] = $row['locToName'];
                $inDetails = array_intersect_key($row, array_flip(array('tID', 'transferDate', 'tCD', 'pID', 'pName', 'pCD', 'qty', 'locFromName')));
                $locationsInTransferLists[$row['locToID']]['locationIn'][$row['locFromID']][$row['tID']][$row['pID']] = $inDetails;
                $locationsInTransferLists[$row['locToID']]['locationIn'][$row['locFromID']][$row['tID']][$row['pID']]['qty'] = $qty;
                $locationsInTransferLists[$row['locToID']]['locationIn'][$row['locFromID']][$row['tID']][$row['pID']]['uomAbbr'] = $thisQty['uomAbbr'];
            }

            return $locationsInTransferLists;
        }
    }

    /**
     * @author hashan <hashan@thinkcube.com>
     * @param type $locationFromIds
     * @param type $locationToIds
     * @param type $fromDate
     * @param type $toDate
     * @param type $productIds
     * @return $locationsInTransferLists
     */
    private function itemWiseTransferDetails($locationFromIds = null, $locationToIds = null, $fromDate = null, $toDate = null, $productIds = null)
    {
        if (isset($locationFromIds) && isset($locationToIds)) {
            $resInTransfer = $this->CommonTable('Inventory\Model\TransferTable')->getTransferByLocIds($locationToIds, $locationFromIds, $fromDate, $toDate, $productIds);

            $allItemWiseTransfer = [];
            foreach ($resInTransfer as $row) {
                $allItemWiseTransfer[$row['pID']]['pID'] = $row['pID'];
                $allItemWiseTransfer[$row['pID']]['pCD'] = $row['pCD'];
                $allItemWiseTransfer[$row['pID']]['pName'] = $row['pName'];

                //get quantity and unit price according to dispaly uom
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['pID']);
                $thisQty = $this->getProductQuantityViaDisplayUom($row['qty'], $productUom);
                $qty = ($thisQty['quantity'] == 0) ? 0 : $thisQty['quantity'];

                if (array_key_exists('totalQty', $allItemWiseTransfer[$row['pID']])) {
                    $allItemWiseTransfer[$row['pID']]['totalQty'] += $qty;
                } else {
                    $allItemWiseTransfer[$row['pID']]['totalQty'] = $qty;
                }

                $allItemWiseTransfer[$row['pID']]['transfersDate'][$row['tID']] = array(
                    'tID' => $row['tID'],
                    'tCD' => $row['tCD'],
                    'transferDate' => $row['transferDate'],
                    'qty' => $qty,
                    'locFromID' => $row['locFromID'],
                    'locFromName' => $row['locFromName'],
                    'locToID' => $row['locToID'],
                    'locToName' => $row['locToName'],
                    'uomAbbr' => $productUom[0]['uomAbbr'],
                );
            }
            return $allItemWiseTransfer;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateItemTransferPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationFromIDs = $request->getPost('locationFromIds');
            $locationToIDs = $request->getPost('locationToIds');
            $fromDate = $request->getPost('dateFrom') ? $request->getPost('dateFrom') : null;
            $toDate = $request->getPost('dateTo') ? $request->getPost('dateTo') : null;
            $translator = new Translator();
            $name = $translator->translate('Item Transfer Report');

            $companyDetails = $this->getCompanyDetails();
            $locationsInTransferLists = $this->itemInTransferDetails($locationFromIDs, $locationToIDs, $fromDate, $toDate);

            $period = $fromDate && $toDate ? $fromDate . ' - ' . $toDate : null;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewItemTransfer = new ViewModel(array(
                'iL' => $locationsInTransferLists,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));

            $viewItemTransfer->setTemplate('reporting/item-transfer-report/generate-item-transfer-pdf');
            $viewItemTransfer->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewItemTransfer);
            $pdfPath = $this->downloadPDF('item-transfer-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author hashan  <hashan@thinkcube.com>
     * @return $pdf
     */
    public function generateItemWiseTransferPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationFromIDs = $request->getPost('locationFromIds');
            $locationToIDs = $request->getPost('locationToIds');
            $fromDate = $request->getPost('dateFrom') ? $request->getPost('dateFrom') : null;
            $toDate = $request->getPost('dateTo') ? $request->getPost('dateTo') : null;
            $allProducts = $request->getPost('allProducts');
            $cusProducts = $request->getPost('cusProducts') ? $request->getPost('cusProducts') : null;
            $translator = new Translator();
            $name = $translator->translate('Item Wise Transfer Report');

            $companyDetails = $this->getCompanyDetails();

            if ($allProducts === '1' || $allProducts === 1) {
                $locationsInTransferLists = $this->itemWiseTransferDetails($locationFromIDs, $locationToIDs, $fromDate, $toDate);
            } else {
                $locationsInTransferLists = $this->itemWiseTransferDetails($locationFromIDs, $locationToIDs, $fromDate, $toDate, $cusProducts);
            }

            $period = $fromDate && $toDate ? $fromDate . ' - ' . $toDate : null;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewItemTransfer = new ViewModel(array(
                'iL' => $locationsInTransferLists,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));

            $viewItemTransfer->setTemplate('reporting/item-transfer-report/generate-item-wise-transfer-pdf');
            $viewItemTransfer->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewItemTransfer);
            $pdfPath = $this->downloadPDF('item-transfer-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateItemTransferSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationFromIDs = $request->getPost('locationFromIds');
            $locationToIDs = $request->getPost('locationToIds');
            $fromDate = $request->getPost('dateFrom') ? $request->getPost('dateFrom') : null;
            $toDate = $request->getPost('dateTo') ? $request->getPost('dateTo') : null;

            $cD = $this->getCompanyDetails();
            $iL = $this->itemInTransferDetails($locationFromIDs, $locationToIDs, $fromDate, $toDate);

            if ($iL) {
                $title = '';
                $tit = 'ITEM TRANSFER REPORT';
                $in = ["", "", "", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                if ($fromDate != null && $toDate != null) {
                    $in = ['Period: ' . $fromDate . ' - ' . $toDate];
                    $title.=implode(",", $in) . "\n";
                }

                $arrs = ["NO", "BASE LOCATION", "TRANSFER LOCATION", "TRANSFER DATE", "TRANSFER CODE", "ITEM CODE", "ITEM NAME", "QUANTITY"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;

                foreach ($iL as $locID => $locData) {
                    if (array_filter($locData['locationIn'])) {
                        $in = [$i, $locData['locToName']];
                        $arr.=implode(",", $in) . "\n";
                        foreach ($locData['locationIn'] as $locTransID => $locTransferData) {
                            if (is_array($locTransferData)) {
                                foreach ($locTransferData as $transData) {
                                    foreach ($transData as $transID => $transferData) {
                                        $qty = $transferData['qty'] ? $transferData['qty'] : 0.00;
                                        $uomAbbr = $transferData['uomAbbr'] ? $transferData['uomAbbr'] : '';
                                        $tCD = $transferData['tCD'] ? $transferData['tCD'] : '-';
                                        $in = ['', '', $transferData['locFromName'], $transferData['transferDate'],
                                            $tCD,
                                            $transferData['pCD'],
                                            $transferData['pName'],
                                            $qty . ' ' . $uomAbbr
                                        ];
                                        $arr.=implode(",", $in) . "\n";
                                    }
                                }
                                $i++;
                            }
                        }
                    }
                }
            } else {
                $arr = "no matching records found\n";
            }
            $name = "item_transfer_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateItemWiseTransferSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationFromIDs = $request->getPost('locationFromIds');
            $locationToIDs = $request->getPost('locationToIds');
            $fromDate = $request->getPost('dateFrom') ? $request->getPost('dateFrom') : null;
            $toDate = $request->getPost('dateTo') ? $request->getPost('dateTo') : null;
            $allProducts = $request->getPost('allProducts');
            $cusProducts = $request->getPost('cusProducts') ? $request->getPost('cusProducts') : null;
            $translator = new Translator();
            $name = $translator->translate('Item Wise Transfer Report');

            $companyDetails = $this->getCompanyDetails();

            if ($allProducts === '1' || $allProducts === 1) {
                $locationsInTransferLists = $this->itemWiseTransferDetails($locationFromIDs, $locationToIDs, $fromDate, $toDate);
            } else {
                $locationsInTransferLists = $this->itemWiseTransferDetails($locationFromIDs, $locationToIDs, $fromDate, $toDate, $cusProducts);
            }

            if ($locationsInTransferLists) {
                $title = '';
                $in = ["", "", "", $name];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$companyDetails[0]->companyName, $companyDetails[0]->companyAddress, 'Tel: ' . $companyDetails[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                if ($fromDate != null && $toDate != null) {
                    $in = ['Period: ' . $fromDate . ' - ' . $toDate];
                    $title.=implode(",", $in) . "\n";
                }

                $arrs = ["NO", "ITEM CODE", "ITEM NAME", "FROM LOCATION", "TO LOCATION", "TRANSFER DATE", "TRANSFER CODE", "QUANTITY"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;

                foreach ($locationsInTransferLists as $productValue) {
                    $in = [$i, $productValue['pCD'], $productValue['pName']];
                    $arr.=implode(",", $in) . "\n";
                    if (count($productValue['transfersDate']) > 0) {
                        foreach ($productValue['transfersDate'] as $key => $value) {
                            $qty = $value['qty'] ? $value['qty'] : 0.00;
                            $uomAbbr = $value['uomAbbr'] ? $value['uomAbbr'] : '';
                            $tCD = $value['tCD'] ? $value['tCD'] : '-';
                            $in = ['', '', '',
                                $value['locFromName'],
                                $value['locToName'],
                                $value['transferDate'],
                                $tCD,
                                number_format($qty, 2) . ' ' . $uomAbbr
                            ];
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                    $in = ['', '', '', '', '', '', 'Total Quantity', number_format($productValue['totalQty'], 2) . ' ' . $uomAbbr];
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }
            $name = "item_wise_transfer_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

}
