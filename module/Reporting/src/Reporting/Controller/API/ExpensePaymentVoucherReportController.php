<?php

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

/**
 * Description of ExpensePaymentVoucherReportController
 *
 * @author shermilan
 */
class ExpensePaymentVoucherReportController extends CoreController
{

    public function viewReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $translator = new Translator();
            $name = $translator->translate('Payment Voucher Report');
            $period = $request->getPost('fromDate') . ' - ' . $request->getPost('toDate');
            $companyDetails = $this->getCompanyDetails();
            $dimensionType = $request->getPost('dimensionType');
            $dimensionValue = $request->getPost('dimensionValue');

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $data = $this->getReportData($request->getPost('expenseType'), $request->getPost('fromDate'), $request->getPost('toDate'), $dimensionType, $dimensionValue);
            $view = new ViewModel([
                'cD' => $companyDetails,
                'paymentVoucherData' => $data['reportData'],
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($data['grandTotal'], 2),
                'headerTemplate' => $headerViewRender,
            ]);
            $view->setTerminal(true);
            $view->setTemplate('/reporting/expense-payment-voucher-report/expense-payment-voucher-table');
            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generetePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $translator = new Translator();
            $name = $translator->translate('Payment Voucher Report');
            $period = $request->getPost('fromDate') . ' - ' . $request->getPost('toDate');
            $companyDetails = $this->getCompanyDetails();
            $dimensionType = $request->getPost('dimensionType');
            $dimensionValue = $request->getPost('dimensionValue');

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $data = $this->getReportData($request->getPost('expenseType'), $request->getPost('fromDate'), $request->getPost('toDate'), $dimensionType, $dimensionValue);
            $view = new ViewModel([
                'cD' => $companyDetails,
                'paymentVoucherData' => $data['reportData'],
                'currencySymbol' => $this->companyCurrencySymbol,
                'grandTotal' => number_format($data['grandTotal'], 2),
                'headerTemplate' => $headerViewRender,
            ]);
            $view->setTerminal(true);
            $view->setTemplate('/reporting/expense-payment-voucher-report/expense-payment-voucher-table');

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('payment-voucher-report' . md5($this->getGMTDateTime()), $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = TRUE;
            return $this->JSONRespond();
        }
    }

    public function generateCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $dimensionType = $request->getPost('dimensionType');
            $dimensionValue = $request->getPost('dimensionValue');

            $data = $this->getReportData($request->getPost('expenseType'), $fromDate, $request->getPost('toDate'), $dimensionType, $dimensionValue);

            $cD = $this->getCompanyDetails();

            if ($data['reportData']) {
                $header = '';
                $reportTitle = [ "", 'PAYMENT VOUCHER REPORT', "",];
                $header = implode(',', $reportTitle) . "\n";

                $date = [
                    'Report Generated: ' . date('Y-M-d h:i:s a')
                ];
                $title.=implode(",", $date) . "\n";

                $header .= $cD[0]->companyName . "\n";
                $header .= $cD[0]->companyAddress . "\n";
                $header .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $peroidRow = ["Period : " . $fromDate . '-' . $toDate];
                $header .= implode(",", $peroidRow) . "\n";
                $header .= implode(",", [""]) . "\n";

                $columnTitlesArray = ['No',
                    'Due Date',
                    'Issued Date',
                    'Expense Type',
                    'Commment',
                    'Amount (' . $this->companyCurrencySymbol . ')'];
                $columnTitles = implode(",", $columnTitlesArray);

                $csvRow = '';
                foreach ($data['reportData'] as $paymentVoucher) {
                    $tableRow = [
                        $paymentVoucher['paymentVoucherCode'],
                        $paymentVoucher['paymentVoucherIssuedDate'],
                        $paymentVoucher['paymentVoucherDueDate'],
                        $paymentVoucher['expenseTypeName'],
                        $paymentVoucher['paymentVoucherComment'],
                        $paymentVoucher['paymentVoucherTotal'],
                    ];
                    $csvRow.=implode(",", $tableRow) . "\n";
                }
                $grandTotalRow = ['', '', '', '', 'Grand Total:', $data['grandTotal']];
                $csvRow .= implode(',', $grandTotalRow) . "\n";
            } else {
                $csvRow = "No matching records found \n";
            }

            $name = 'Payment_Voucher_Report_csv' . md5($this->getGMTDateTime());
            $csvContent = $this->csvContent($header, $columnTitles, $csvRow);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getReportData($expenseTypes, $fromDate, $toDate,  $dimensionType = NULL, $dimensionValue=NULL)
    {
        $result = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchersForReport($expenseTypes, $fromDate, $toDate, $dimensionType, $dimensionValue);
        $paymentVoucherData = [];
        $grandTotal = 0;
        if (count($result) > 0) {
            if (!empty($dimensionType) && !empty($dimensionValue)) {
                foreach ($result as $row) {
                    $addTotheList = false;
                    $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(20,$row['paymentVoucherID']);
                    foreach ($jEDimensionData as $value) {

                        if (!$addTotheList) {
                            if ($dimensionType === $value['dimensionType'] &&  $dimensionValue === $value['dimensionValueID']) {
                                $paymentVoucherData[] = [
                                    'paymentVoucherCode' => $row['paymentVoucherCode'],
                                    'paymentVoucherDueDate' => $row['paymentVoucherDueDate'],
                                    'paymentVoucherIssuedDate' => $row['paymentVoucherIssuedDate'],
                                    'paymentVoucherComment' => $row['paymentVoucherComment'],
                                    'expenseTypeName' => $row['expenseTypeName'],
                                    'paymentVoucherTotal' => $row['paymentVoucherTotal'],
                                ];
                                $grandTotal += $row['paymentVoucherTotal'];
                                $addTotheList = true;
                            }
                        }
                    }
                }

            } else {
                foreach ($result as $row) {
                    $paymentVoucherData[] = [
                        'paymentVoucherCode' => $row['paymentVoucherCode'],
                        'paymentVoucherDueDate' => $row['paymentVoucherDueDate'],
                        'paymentVoucherIssuedDate' => $row['paymentVoucherIssuedDate'],
                        'paymentVoucherComment' => $row['paymentVoucherComment'],
                        'expenseTypeName' => $row['expenseTypeName'],
                        'paymentVoucherTotal' => $row['paymentVoucherTotal'],
                    ];
                    $grandTotal += $row['paymentVoucherTotal'];
                }
            }
        }
        return ['reportData' => $paymentVoucherData, 'grandTotal' => $grandTotal];
    }

}
