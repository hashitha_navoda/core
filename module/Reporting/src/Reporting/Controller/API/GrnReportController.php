<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains GRN Report related controller functions
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class GrnReportController extends CoreController
{

    protected $userRoleID;
    protected $allLocations;
    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'inve_report_upper_menu';

    private function _getGrnStatusDetails($grnIDs = null, $isAllGRNs = false, $fromDate = false, $toDate = false, $locations = false)
    {
        $isAllGRNs = filter_var($isAllGRNs, FILTER_VALIDATE_BOOLEAN);
        if (isset($grnIDs) || $isAllGRNs) {
            $grnStatusData = $this->CommonTable('Inventory\Model\GrnTable')->grnStatusDetails($grnIDs, $isAllGRNs, $fromDate, $toDate, $locations);
            foreach ($grnStatusData as $row) {
                $grnListsByCode[$row['gID']] = $row;
            }

            return $grnListsByCode;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return type
     */
    public function viewGrnStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $grnIDs = $request->getPost('grnIds');
            $isAllGRNs = $request->getPost('isAllGRNs');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locations = $request->getPost('locations');

            if (empty($grnIDs)) {
                $grnData = $this->CommonTable('Inventory\Model\GrnTable')->fetchDescAll();
                foreach ($grnData as $value) {
                    $grnIDs[$value['grnID']] = $value['grnID'];
                }
            }

            $translator = new Translator();
            $name = $translator->translate('GRN Status Report');

            $grnListsByCode = $this->_getGrnStatusDetails($grnIDs, $isAllGRNs, $fromDate, $toDate, $locations);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = $fromDate . ' - ' . $toDate);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $grnListsView = new ViewModel(array(
                'cD' => $companyDetails,
                'gL' => $grnListsByCode,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $grnListsView->setTemplate('reporting/grn-report/generate-grn-status-pdf');

            $this->html = $grnListsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return $pdfPath
     */
    public function generateGrnStatusPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $grnIDs = $request->getPost('grnIds');
            $isAllGRNs = $request->getPost('isAllGRNs');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locations = $request->getPost('locations');

            if (empty($grnIDs)) {
                $grnData = $this->CommonTable('Inventory\Model\GrnTable')->fetchDescAll();
                foreach ($grnData as $value) {
                    $grnIDs[$value['grnID']] = $value['grnID'];
                }
            }

            $translator = new Translator();
            $name = $translator->translate('GRN Status Report');
            $period = $fromDate . ' - ' . $toDate;

            $grnListsByCode = $this->_getGrnStatusDetails($grnIDs, $isAllGRNs, $fromDate, $toDate, $locations);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewGrnStatus = new ViewModel(array(
                'gL' => $grnListsByCode,
                'cD' => $companyDetails,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $viewGrnStatus->setTemplate('reporting/grn-report/generate-grn-status-pdf');
            $viewGrnStatus->setTerminal(true);
            $htmlContent = $this->viewRendererHtmlContent($viewGrnStatus);
            $pdfPath = $this->downloadPDF('grn-status-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;

            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return csv Grn Status csv file
     */
    public function generateGrnStatusSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $grnIDs = $request->getPost('grnIds');
            $isAllGRNs = $request->getPost('isAllGRNs');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locations = $request->getPost('locations');

            if (empty($grnIDs)) {
                $grnData = $this->CommonTable('Inventory\Model\GrnTable')->fetchDescAll();
                foreach ($grnData as $value) {
                    $grnIDs[$value['grnID']] = $value['grnID'];
                }
            }

            $grnListsByCode = $this->_getGrnStatusDetails($grnIDs, $isAllGRNs, $fromDate, $toDate, $locations);
            $cD = $this->getCompanyDetails();

            if ($grnListsByCode) {
                $title = '';
                $tit = 'GRN STATUS REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => $tit,
                    3 => "",
                    4 => "",
                    5 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => ''
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("NO", "GRN CODE", "GRN STATUS", "SUPPLIER CODE", "SUPPLIER NAME", "GRN DATE", "TOTAL VALUE({$this->companyCurrencySymbol})");
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                foreach ($grnListsByCode as $key => $value) {
                    $sCD = isset($value['sCD']) ? $value['sCD'] : '-';
                    $sName = isset($value['sName']) ? $value['sName'] : '-';
                    $gTot = isset($value['gTot']) ? $value['gTot'] : 0.00;
                    $gDate = isset($value['gDate']) ? $value['gDate'] : '-';
                    $gCD = isset($value['gCD']) ? $value['gCD'] : '-';
                    $gStatus = isset($value['statusName']) ? $value['statusName'] : '-';

                    $in = array(
                        0 => $i,
                        1 => $gCD,
                        2 => $gStatus,
                        3 => $sCD,
                        4 => $sName,
                        5 => $gDate,
                        6 => $gTot
                    );
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }
        }

        $name = "grn_status_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }
    
    public function grnItemDetailsReportAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $translator = new Translator();
            $name = $translator->translate('GRN Item Details Report');
            $period = $postData['fromDate'] . " - " . $postData['toDate'];

            $dataList = $this->getGrnItemDetails($postData);
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $grnListsView = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $grnListsView->setTemplate('reporting/grn-report/grn-item-details-report');

            $this->html = $grnListsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function grnItemDetailsPdfAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $translator = new Translator();
            $name = $translator->translate('GRN Item Details Report');
            $period = $postData['fromDate'] . " - " . $postData['toDate'];

            $dataList = $this->getGrnItemDetails($postData);
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $grnListsView = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $grnListsView->setTemplate('reporting/grn-report/grn-item-details-report');
            $grnListsView->setTerminal(true);
            $htmlContent = $this->viewRendererHtmlContent($grnListsView);
            $pdfPath = $this->downloadPDF('grn-item-details-pdf', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;

            return $this->JSONRespond();
        }
    }
    
    public function grnItemDetailsCsvAction() 
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $translator = new Translator();
            $name = $translator->translate('GRN Item Details Report');
            $period = $postData['fromDate'] . " - " . $postData['toDate'];

            $dataList = $this->getGrnItemDetails($postData);
            $cD = $this->getCompanyDetails();

            if ($dataList) {
                $title = '';
                $tit = 'GRN ITEM DETAILS REPORT';
                $in = array(
                    0 => "",
                    1 => "$tit"
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => ''
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("GRN NUMBER", "SUPPLIER", "SUPPLIER REFERENCE", "GRN DATE", "ITEM CODE", "ITEM NAME", "UOM", "ISSUED DATE", "GRN QUANTITY", "RETURNED QUANTITY", "TAX NAME", "GRN TAX AMOUNT", "GRN AMOUNT WITHOUT TAX", "GRN AMOUNT");
                $header = implode(",", $arrs);
                $arr = '';

                if (!empty($dataList)) {
                    $totalGrnAmount = $totalGrnTaxAmount = 0;
                    $totalGrnQty = $totalReturnQty = 0;
                    foreach ($dataList as $data) {
                        $totalGrnAmount += $data['grnTotal'];
                        $totalGrnTaxAmount += $data['grnTax'];
                        $in = array(
                            0 => $data['grnNumber'],
                            1 => $data['supplier'],
                            2 => $data['supplierRef'],
                            3 => $data['grnDate'],
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => '',
                            8 => '',
                            9 => '',
                            10 => '',
                            11 => number_format($data['grnTax'], 2, '.', ''),
                            12 => number_format(($data['grnTotal'] - $data['grnTax']), 2, '.', ''),
                            13 => number_format($data['grnTax'], 2, '.', ''),
                        );
                        $arr.=implode(",", $in) . "\n";
                        
                        foreach ($data['productList'] as $itemData) {
                            $totalGrnQty += $itemData['itemQty'];
                            $totalReturnQty += $itemData['itemReturnQty'];
                            $in = array(
                                0 => '',
                                1 => '',
                                2 => '',
                                3 => '',
                                4 => $itemData['itemCode'],
                                5 => $itemData['itemName'],
                                6 => $itemData['itemUOM'],
                                7 => number_format($itemData['issuedPrice'], 2, '.', ''),
                                8 => number_format($itemData['itemQty'], 2, '.', ''),
                                9 => number_format($itemData['itemReturnQty'], 2, '.', ''),
                                10 => '',
                                11 => '',
                                12 => '',
                                13 => '',
                            );
                            foreach ($itemData['itemTaxDetails'] as $index => $itemTax) {
                                if ($index != 0) {
                                    $in = array(
                                        0 => '',
                                        1 => '',
                                        2 => '',
                                        3 => '',
                                        4 => '',
                                        5 => '',
                                        6 => '',
                                        7 => '',
                                        8 => '',
                                        9 => '',
                                        10 => $itemTax['taxName'],
                                        11 => number_format($itemTax['taxAmount'], 2, '.', ''),
                                        12 => '',
                                        13 => '',
                                    );
                                } else {
                                    $in['10'] = $itemTax['taxName'];
                                    $in['11'] = number_format($itemTax['taxAmount'], 2, '.', '');
                                }
                                $arr.=implode(",", $in) . "\n";
                            }
                            if (empty($itemData['itemTaxDetails'])) {
                                $arr.=implode(",", $in) . "\n";
                            }
                        }
                    }
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => '',
                        7 => 'TOTAL',
                        8 => number_format($totalGrnQty, 2, '.', ''),
                        9 => number_format($totalReturnQty, 2, '.', ''),
                        10 => '',
                        11 => number_format($totalGrnTaxAmount, 2, '.', ''),
                        12 => number_format(($totalGrnAmount - $totalGrnTaxAmount), 2, '.', ''),
                        13 => number_format($totalGrnAmount, 2, '.', '')
                    );
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $arr = "No related data found";
                }
                $name = "GRN_Item_Detail_Report";
                $csvContent = $this->csvContent($title, $header, $arr);
                $csvPath = $this->generateCSVFile($name, $csvContent);

                $this->data = $csvPath;
                $this->status = true;
                return $this->JSONRespond();
            }
        }
    }

    private function getGrnItemDetails($postData) {
        $grnIds = (filter_var($postData['isAllGRNs'], FILTER_VALIDATE_BOOLEAN)) ? [] : $postData['grnIds'];
        $itemIds = (filter_var($postData['isAllItems'], FILTER_VALIDATE_BOOLEAN)) ? [] : $postData['itemIds'];
        $supplierIds = (filter_var($postData['isAllSuppliers'], FILTER_VALIDATE_BOOLEAN)) ? [] : $postData['supplierIds'];
        $locationIds = $postData['locationIds'];
        $fromDate = $postData['fromDate'];
        $toDate = $postData['toDate'];

        $dataList = [];
        //grn product details
        $grnDetails = $this->CommonTable('Inventory\Model\GrnTable')->getGrnDetails($grnIds, $itemIds, $supplierIds, $locationIds, $fromDate, $toDate);
        foreach ($grnDetails as $grn) {
            $dataList[$grn['grnID']] = [
                'grnNumber'   => $grn['grnCode'],
                'supplierRef' => ($grn['grnSupplierReference']) ? $grn['grnSupplierReference'] : '-',
                'grnDate'     => $grn['grnDate'],
                'supplier'    => $grn['supplierName'] . ' [' . $grn['supplierCode'] . ']',
                'grnTotal'    => $grn['grnDeliveryCharge'],
                'grnTax'      => 0
            ];
            if ($itemIds) {
                $grnLocationproductDetails = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductDetailsByGrnId($grn['grnID'], $itemIds);
            } else {
                $grnLocationproductDetails = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnLocationProductDetailsByGrnId($grn['grnID']);
            }

            foreach ($grnLocationproductDetails as $key => $value) {
                $grnItemDetails = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductDetailsByGrnIdAndLocationProductId($grn['grnID'], $value['locationProductID']);
                       
                foreach ($grnItemDetails as $grnItem) {
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($grnItem['productID']);
                    $issuedDetails = $this->getProductQuantityViaDisplayUom($grnItem['grnProductTotalQty'], $productUom);
                    //return details
                    $returnedQty = $returnedTotal = 0;
                    $productReturnDetails = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getGrnProductReturnDetailsByGrnIdAndLocationProductId($grnItem['grnID'], $grnItem['locationProductID']);
                    foreach ($productReturnDetails as $productReturn) {
                        $returnedDetails = $this->getProductQuantityViaDisplayUom($productReturn['purchaseReturnProductReturnedQty'], $productUom);
                        $returnedQty += $returnedDetails['quantity'];
                        $returnedTotal += $productReturn['purchaseReturnProductTotal'];
                    }
                    //tax details
                    $grnTaxDetails = $this->CommonTable('Inventory\Model\GrnProductTaxTable')->getGrnProductTaxDetails($grn['grnID'], $grnItem['locationProductID']);
                    $productTax = 0;
                    $productTaxArr = [];
                    foreach ($grnTaxDetails as $grnProductTax) {
                        if ($grnItem['grnProductID'] == $grnProductTax['grnProductID']) {
                            $productTax += $grnProductTax['grnTaxAmount'];
                            $productTaxArr[] = [
                                'taxName' => $grnProductTax['taxName'],
                                'taxAmount' => (($grnProductTax['grnTaxAmount'] / $issuedDetails['quantity']) * ($issuedDetails['quantity'] - $returnedQty)),
                            ];
                        }
                    }
                    $dataList[$grn['grnID']]['grnTax'] += $productTax;
                    //return product tax
                    $productReturnTaxDetails = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getGrnProductTaxByGrnIdAndLocationProductId($grnItem['grnID'], $grnItem['locationProductID']);
                    $returnedTax = array_sum(array_map(function($productReturnTax) {
                        return $productReturnTax['purchaseReturnTaxAmount'];
                    }, iterator_to_array($productReturnTaxDetails)));
                    
                    $dataList[$grnItem['grnID']]['productList'][] = [
                        'itemCode' => $grnItem['productCode'],
                        'itemName' => $grnItem['productName'],
                        'itemUOM' => $issuedDetails['uomAbbr'],
                        'itemQty' => $issuedDetails['quantity'],
                        'itemReturnQty' => $returnedQty,
                        'issuedPrice' => $grnItem['grnProductPrice'],
                        'itemTaxDetails' => $productTaxArr
                    ];

                    $dataList[$grnItem['grnID']]['grnTotal'] += $grnItem['grnProductTotal'];
                    $dataList[$grnItem['grnID']]['grnTax'] -= $returnedTax;
                    $dataList[$grnItem['grnID']]['grnTotal'] -= $returnedTotal;
                }
            }     
        }
        // die();      
        return $dataList;
    }

    public function grnWisePurchaseReturnReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getGrnWisePurchaseReturnDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Grn Wise Purchase Return Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/grn-report/grn-wise-purchase-return-report');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function grnWisePurchaseReturnPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getGrnWisePurchaseReturnDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Grn Wise Purchase Return Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/grn-report/grn-wise-purchase-return-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('Grn-wise-purchase-return-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function grnWisePurchaseReturnCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getGrnWisePurchaseReturnDetails($postData);
            $companyDetails = $this->getCompanyDetails();

            if ($dataList) {
                $data = ['Grn Wise Purchase Return Report'];
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = [
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                ];
                $title.=implode(",", $data) . "\n";

                $data = ["Period :" . $postData['fromDate'] . " - " . $postData['toDate']];
                $title.=implode(",", $data) . "\n";

                $tableHead = [
                    'Grn Code','Grn Date','Grn Amount',
                    'Grn Status','Purchase Return Code','Purchase Return Date','Purchase Return Status',
                    'Total Purchase Return Amount','Copied Purchase Return Amount'
                ];
                $header.=implode(",", $tableHead) . "\n";

                foreach ($dataList as $grnData) {
                    $data = [
                        $grnData['grnCode'],
                        $grnData['grnDate'],
                        number_format($grnData['grnTotal'], 2, '.', ''),
                        $grnData['grnStatus']
                    ];
                    $arr.=implode(",", $data) . "\n";
                    $totalCopiedAmount = 0;
                    foreach ($grnData['returnData'] as $returnData) {                        
                        $data = [
                            '', '', '', '',
                            $returnData['purchaseReturnCode'],
                            $returnData['purchaseReturnDate'],
                            $returnData['pRStatus'],
                            number_format($returnData['purchaseReturnTotal'], 2, '.', ''),
                            number_format($returnData['purchaseReturnCopiedAmount'], 2, '.', ''),
                        ];
                        $arr.=implode(",", $data) . "\n";
                        $totalCopiedAmount += $returnData['purchaseReturnCopiedAmount'];
                    }
                    $data = [
                        '', '', '', '', '', '', '', 'Total Copied Amount :',
                        number_format($totalCopiedAmount, 2, '.', '')
                    ];
                    $arr.=implode(",", $data) . "\n";
                }

            } else {
                $arr = "no matching records found";
            }

            $name = "Grn-Wise-purchase-return-Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getGrnWisePurchaseReturnDetails($postData)
    {
        $postData['isAllGRNs'] = filter_var($postData['isAllGRNs'], FILTER_VALIDATE_BOOLEAN);
        $postData['grnIds'] = ($postData['isAllGRNs']) ? [] : $postData['grnIds'];

        $grnReturns = $this->CommonTable('Inventory\Model\GrnTable')->getGrnWisePurchaseReturn($postData['fromDate'], $postData['toDate'], $postData['grnIds']);

        $dataList = [];
        foreach ($grnReturns as $returns) {
            if (!array_key_exists($returns['grnID'], $dataList)) {
                $dataList[$returns['grnID']] = [
                    'grnCode' => $returns['grnCode'],
                    'grnDate' => $returns['grnDate'],
                    'grnTotal' => $returns['grnTotal'],
                    'grnStatus' => $returns['grnStatus']
                ];
            }
            $dataList[$returns['grnID']]['returnData'][] = [
                'purchaseReturnCode' => $returns['purchaseReturnCode'],
                'purchaseReturnDate' => $returns['purchaseReturnDate'],
                'purchaseReturnTotal' => $returns['purchaseReturnTotal'],
                'purchaseReturnCopiedAmount' => $returns['copiedAmount'],
                'pRStatus' => $returns['pRStatus'],
            ];
        }
        return $dataList;
    }

    public function grnWisePurchaseInvoiceReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getGrnWisePurchaseInvoiceDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Grn Wise Purchase Invoice Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/grn-report/grn-wise-purchase-invoice-report');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function grnWisePurchaseInvoicePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getGrnWisePurchaseInvoiceDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Grn Wise Purchase Invoice Report');
            $period = $postData['fromDate'] ." - ". $postData['toDate'];

            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'dataList' => $dataList,
                'currencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/grn-report/grn-wise-purchase-invoice-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('Grn-wise-purchase-invoice-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function grnWisePurchaseInvoiceCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getGrnWisePurchaseInvoiceDetails($postData);
            $companyDetails = $this->getCompanyDetails();

            if ($dataList) {
                $data = ['Grn Wise Purchase Invoice Report'];
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = [
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                ];
                $title.=implode(",", $data) . "\n";

                $data = ["Period :" . $postData['fromDate'] . " - " . $postData['toDate']];
                $title.=implode(",", $data) . "\n";

                $tableHead = [
                    'Grn Code','Grn Date','Grn Amount',
                    'Grn Status','Purchase Invoice Code','Purchase Invoice Date','Purchase Invoice Status',
                    'Total Purchase Invoice Amount','Copied Purchase Invoice Amount'
                ];
                $header.=implode(",", $tableHead) . "\n";

                foreach ($dataList as $grnData) {
                    $data = [
                        $grnData['grnCode'],
                        $grnData['grnDate'],
                        number_format($grnData['grnTotal'], 2, '.', ''),
                        $grnData['grnStatus']
                    ];
                    $arr.=implode(",", $data) . "\n";
                    $totalCopiedAmount = 0;
                    foreach ($grnData['piData'] as $piData) {                        
                        $data = [
                            '', '', '', '',
                            $piData['purchaseInvoiceCode'],
                            $piData['purchaseInvoiceIssueDate'],
                            $piData['pIStatus'],
                            number_format($piData['purchaseInvoiceTotal'], 2, '.', ''),
                            number_format($piData['purchaseInvoiceCopiedAmount'], 2, '.', ''),
                        ];
                        $arr.=implode(",", $data) . "\n";
                        $totalCopiedAmount += $piData['purchaseInvoiceCopiedAmount'];
                    }
                    $data = [
                        '', '', '', '', '', '', '', 'Total Copied Amount :',
                        number_format($totalCopiedAmount, 2, '.', '')
                    ];
                    $arr.=implode(",", $data) . "\n";
                }

            } else {
                $arr = "no matching records found";
            }

            $name = "Grn-Wise-purchase-invoice-Report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getGrnWisePurchaseInvoiceDetails($postData)
    {
        $postData['isAllGRNs'] = filter_var($postData['isAllGRNs'], FILTER_VALIDATE_BOOLEAN);
        $postData['grnIds'] = ($postData['isAllGRNs']) ? [] : $postData['grnIds'];

        $grnInvoices = $this->CommonTable('Inventory\Model\GrnTable')->getGrnWisePurchaseInvoice($postData['fromDate'], $postData['toDate'], $postData['grnIds']);

        $dataList = [];
        foreach ($grnInvoices as $invoice) {
            if (!array_key_exists($invoice['grnID'], $dataList)) {
                $dataList[$invoice['grnID']] = [
                    'grnCode' => $invoice['grnCode'],
                    'grnDate' => $invoice['grnDate'],
                    'grnTotal' => $invoice['grnTotal'],
                    'grnStatus' => $invoice['grnStatus']
                ];
            }
            $dataList[$invoice['grnID']]['piData'][] = [
                'purchaseInvoiceCode' => $invoice['purchaseInvoiceCode'],
                'purchaseInvoiceIssueDate' => $invoice['purchaseInvoiceIssueDate'],
                'purchaseInvoiceTotal' => $invoice['purchaseInvoiceTotal'],
                'purchaseInvoiceCopiedAmount' => $invoice['copiedAmount'],
                'pIStatus' => $invoice['pIStatus'],
            ];
        }
        return $dataList;
    }

    public function editedGrnReportAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Edited GRN Report');
            $period = $fromDate . ' - ' . $toDate;
            $supplierIds = $request->getPost('supplierIds');
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $supplierIds = (filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN)) ? [] : $supplierIds;

            $editedGrnData = $this->getEditedGrnData($fromDate, $toDate, $supplierIds);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $editedDeliveryNoteView = new ViewModel(array(
                'cD' => $companyDetails,
                'editedGrnData' => $editedGrnData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $editedDeliveryNoteView->setTemplate('reporting/grn-report/generate-edited-grn-report-pdf');

            $this->html = $editedDeliveryNoteView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function editedGrnPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Edited GRN Report');
            $period = $fromDate . ' - ' . $toDate;
            $supplierIds = $request->getPost('supplierIds');
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $supplierIds = (filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN)) ? [] : $supplierIds;

            $editedGrnData = $this->getEditedGrnData($fromDate, $toDate, $supplierIds);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $editedGrnView = new ViewModel(array(
                'cD' => $companyDetails,
                'editedGrnData' => $editedGrnData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $editedGrnView->setTemplate('reporting/grn-report/generate-edited-grn-report-pdf');
            $editedGrnView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($editedGrnView);
            $pdfPath = $this->downloadPDF('edited-grn-pdf', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function editedGrnSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $supplierIds = $request->getPost('supplierIds');
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $supplierIds = (filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN)) ? [] : $supplierIds;
            $editedGrnData = $this->getEditedGrnData($fromDate, $toDate, $supplierIds);
            $cD = $this->getCompanyDetails();

            if (count($editedGrnData) > 0) {
                $header = '';
                $reportTitle = [ "", 'EDITED GRN REPORT', "",];
                $header = implode(',', $reportTitle) . "\n";

                $header .= $cD[0]->companyName . "\n";
                $header .= $cD[0]->companyAddress . "\n";
                $header .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $peroidRow = ["Period : " . $fromDate . '-' . $toDate];
                $header .= implode(",", $peroidRow) . "\n";
                $header .= implode(",", [""]) . "\n";

                $columnTitlesArray = ['GRN Code',
                    'New Products / Old Products',
                    'Product Name',
                    'Quantity',
                    'Unit Price',
                    'Discount',
                    'Tax Name',
                    'Tax Percentage',
                    'Tax Amount'];
                $columnTitles = implode(",", $columnTitlesArray);

                $csvRow = '';
                foreach ($editedGrnData as $key => $data) {
                    $tableRow = [
                        $key,
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                        "",
                    ];
                    $csvRow .= implode(",", $tableRow) . "\n";
                    foreach ($data as $ke => $val) {
                        $tableRow = [
                            "",
                            $ke,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                        ];
                        $csvRow .= implode(",", $tableRow) . "\n";
                        foreach ($val['gProducts'] as $value) {

                            $tableRow = [
                                "",
                                "",
                                $value['gPN'],
                                $value['gPQ'],
                                round($value['gPP'],2),
                                $value['gPD'],
                                "",
                                "",
                                "",
                            ];
                            $csvRow .= implode(",", $tableRow) . "\n";
                            foreach ($value['pT'] as $tax) {
                                $tableRow = [
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    "",
                                    $tax['pTN'],
                                    $tax['pTP'],
                                    round($tax['pTA'],2),
                                ];
                                $csvRow .= implode(",", $tableRow) . "\n";
                            }
                        }
                    }
                }
            } else {
                $csvRow = "No matching records found \n";
            }

            $csvContent = $this->csvContent($header, $columnTitles, $csvRow);
            $csvPath = $this->generateCSVFile('Edited_grn_Report_csv', $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getEditedGrnData($fromDate = NULL, $toDate = NULL, $supplierIds)
    {
        if (isset($fromDate) && isset($toDate)) {
            $plusToDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
            $editGrnIds = $this->CommonTable('Inventory/Model/GrnTable')->getEditedGrnDetails($fromDate, $plusToDate, $supplierIds);

            $editGrnData = [];
            $editedGrnData = [];
            foreach ($editGrnIds as $key => $value) {
                $editedGrnDetails = $this->CommonTable('Inventory/Model/GrnTable')->getGrnByGrnCodeForEdiedGrn($value['grnCode']);
                    $editGrnData[$value['grnCode']] = $value['grnID'];
                foreach ($editedGrnDetails as $ke => $val) {
                    $editedGrnData[$value['grnCode']] = $val['grnID'];
                }
            }
            $grnIds = array_merge_recursive($editGrnData, $editedGrnData);
            
            $oldProductData = [];
            $newProductData = [];
            foreach ($grnIds as $key => $value) {
                $getOldGrnData = $this->CommonTable("Inventory\Model\GrnTable")->getGrnDetailsByGrnId($value[0]);
                $getNewGrnData = $this->CommonTable("Inventory\Model\GrnTable")->getGrnDetailsByGrnId($value[1]);

                $getOldGrnProductData = $this->CommonTable("Inventory\Model\GrnTable")->getGrnByGrnID($value[0], false, false, true);
                $oldProducts = $this->getGrnProductSet($getOldGrnProductData);

                $getNewGrnProductData = $this->CommonTable("Inventory\Model\GrnTable")->getGrnByGrnID($value[1], false, false, true);
                $newProducts = $this->getGrnProductSet($getNewGrnProductData);

                $editedGrnProductData[$oldProducts['gCd']]['oldProducts'] = $oldProducts;
                $editedGrnProductData[$newProducts['gCd']]['newProducts'] = $newProducts;
            }

            return $editedGrnProductData;
        }
    }

    private function getGrnProductSet($grnData)
    {
        $rowCount = 0;
        $tempProduct = [];
        $btchData = [];
        $serialData = [];
        $tempG = array();
        $taxDetails = [];

        foreach ($grnData as $key => $row) {

                $grnID = $row['grnID'];
                $tempG['gID'] = $row['grnID'];
                $tempG['gCd'] = $row['grnCode'];
                $tempG['gSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempG['gSID'] = $row['grnSupplierID'];
                $tempG['gSR'] = $row['grnSupplierReference'];
                $tempG['gRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempG['retriveLocation'] = $row['grnRetrieveLocation'];
                $tempG['gD'] = $this->convertDateToUserFormat($row['grnDate']);
                $tempG['gDC'] = $row['grnDeliveryCharge'];
                $tempG['gT'] = $row['grnTotal'];
                $tempG['gC'] = $row['grnComment'];
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);
                $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['grnProductPrice'], $productUom);
                $productQtyDetails = $this->getProductQuantityViaDisplayUom($row['grnProductTotalQty'], $productUom);

                if($row['grnUploadFlag']){
                    if(isset($tempProduct[$rowCount]['bP'][$row['productBatchID']]) && $row['productSerialID']){

                    } else if(!$row['productBatchID'] && $row['productSerialID'] && $tempProduct[$rowCount]['lPID'] == $row['locationProductID']){

                    } else {
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    }
                } else {
                    if($tempProduct[$rowCount]['lPID'] != $row['locationProductID']){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    } else if($tempProduct[$rowCount]['gPP'] != $unitPrice){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    }

                }
                $discSymbol = null;
                if($row['grnProductDiscountType'] == 'value'){
                    $discSymbol = $this->companyCurrencySymbol;
                } else if($row['grnProductDiscountType'] == 'percentage') {
                    $discSymbol = '%';
                }

                $tempProduct[$rowCount] = array(
                    'gPC' => $row['productCode'],
                    'gPN' => $row['productName'],
                    'lPID' => $row['locationProductID'],
                    'gPP' => $unitPrice,
                    'gPD' => $row['grnProductDiscount'],
                    'gPQ' => $productQtyDetails['quantity'],
                    'gPT' => $row['grnProductTotal'],
                    'gPUom' => $productQtyDetails['uomAbbr'],
                    'gPUomDecimal' => $row['uomDecimalPlace'],
                    'gPDT' => $discSymbol,
                    'productID' => $row['productID'],
                    'productUomID' => $row['uomID'],
                    'productPoID' => $row['grnProductDocumentId'],
                    'productType' => $row['productTypeID'],
                    'grnProductID' => $row['grnProductID'],
                    );

                if ($row['grnTaxID'] != NULL) {

                    $taxDetails[$row['grnTaxID']] = array(
                        'pTN' => $row['taxName'],
                        'pTP' => $row['grnTaxPrecentage'],
                        'pTA' => $row['grnTaxAmount'],
                        'TXID' => $row['grnTaxID'],

                        );
                    $tempProduct[$rowCount]['pT'] = $taxDetails;
                }

                if($row['productBatchID']){

                    $btchData[$row['productBatchID']] = array(
                        'bC' => $row['productBatchCode'],
                        'bED' => $row['productBatchExpiryDate'],
                        'bW' => $row['productBatchWarrantyPeriod'],
                        'bMD' => $row['productBatchManufactureDate'],
                        'bQ' => $row['grnProductQuantity'],
                        'bPrice' => $row['productBatchPrice'],
                        );
                    $tempProduct[$rowCount]['bP'] = $btchData;

                    if($row['productSerialID']){
                        $serialData[$row['productSerialID']] = array(
                            'sC' => $row['productSerialCode'],
                            'sW' => $row['productSerialWarrantyPeriod'],
                            'sED' => $row['productSerialExpireDate'],
                            'sBC' => $row['productBatchCode']
                            );
                        $tempProduct[$rowCount]['sP'] = $serialData;

                    }

                } else if($row['productSerialID']){
                    $serialData[$row['productSerialID']] = array(
                        'sC' => $row['productSerialCode'],
                        'sW' => $row['productSerialWarrantyPeriod'],
                        'sED' => $row['productSerialExpireDate'],
                        'sBC' => $row['productBatchCode']
                        );
                    $tempProduct[$rowCount]['sP'] = $serialData;
                }
                $tempG['gProducts'] = $tempProduct;
        }
        return $tempG;
    }
}
