<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains customer transaction Report PDF related controller functions
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class CustomerReportController extends CoreController
{

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return $customerDetails
     */
    public function getAgedCustomerDetails($cusIds = null, $endDate = null, $isAllCustomers = false, $cusCategory = null, $salesPersons = [])
    {
        if (isset($cusIds) && isset($endDate)) {
            $endDate = $this->getGMTDateTime($endDate);
            $customerDetails = array();
            $salesPersonFlag = false;
            if(!empty($salesPersons)){
                $salesPersonFlag = true;
                $withDefaultSalesPerson = (in_array('other', $salesPersons)) ? true : false;
            }

            $agedCustomerDeatailsByInvTable = $this->CommonTable('Invoice\Model\InvoiceTable')->getAgedCusInvoiceData($cusIds, $endDate, $groupBy = false, $isAllCustomers, $cusCategory, $salesPersons, "",$withDefaultSalesPerson);

            $processedInvData = $this->processInvToAges($agedCustomerDeatailsByInvTable, $endDate);

            foreach ($processedInvData as $c) {

                $c['salesInvoiceIssuedDate'] = $this->getUserDateTime($c['salesInvoiceIssuedDate']);
                $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceCreditNoteTotalByInvoiceIdForAge($c['salesInvoiceID'], $endDate);
                if($creditNotes){
                    $current = is_null($c['Current']) ? 0 : $c['Current'];
                    if (is_null($c['Current']) && is_null($c['WithIn7Days'])) {
                        $withIn7Days = 0;
                    } else if (!is_null($c['Current']) && is_null($c['WithIn7Days'])) {
                        $withIn7Days = floatval($current) - $creditNotes['total'];
                    } else {
                        $withIn7Days = $c['WithIn7Days'] - $creditNotes['total'] + floatval($current);
                    }
                    // $withIn7Days = is_null($c['WithIn7Days']) ? 0 : $c['WithIn7Days'] - $creditNotes['total'];
                    $withIn14Days = is_null($c['WithIn14Days']) ? 0 : $c['WithIn14Days'] - $creditNotes['total'];
                    $withIn30Days = is_null($c['WithIn30Days']) ? 0 : $c['WithIn30Days'] - $creditNotes['total'];
                    $over30 = is_null($c['Over30']) ? 0 : $c['Over30'] - $creditNotes['total'] ;
                }
                if($salesPersonFlag){
                    //if first time of customerID coming from $customerDetails array
                    $salesPersonID = (is_null($c['salesPersonID'])) ? "other" : $c['salesPersonID'];
                    if (empty($customerDetails[$salesPersonID][$c['customerID']])) {
                        $withIn7DaysTotal = 0;
                        $withIn14DaysTotal = 0;
                        $withIn30DaysTotal = 0;
                        $over30Total = 0;
                    } else {
                        //if already have customerID related data on $customerDetails array
                        //get previous value from $customerDetails array to calculations

                        $withIn7DaysTotal = $customerDetails[$salesPersonID][$c['customerID']]['withIn7Days'];
                        $withIn14DaysTotal = $customerDetails[$salesPersonID][$c['customerID']]['withIn14Days'];
                        $withIn30DaysTotal = $customerDetails[$salesPersonID][$c['customerID']]['withIn30Days'];
                        $over30Total = $customerDetails[$salesPersonID][$c['customerID']]['over30'];
                    }
                } else {
                    if (empty($customerDetails[$c['customerID']])) {
                        $withIn7DaysTotal = 0;
                        $withIn14DaysTotal = 0;
                        $withIn30DaysTotal = 0;
                        $over30Total = 0;
                    } else {
                        //if already have customerID related data on $customerDetails array
                        //get previous value from $customerDetails array to calculations

                        $withIn7DaysTotal = $customerDetails[$c['customerID']]['withIn7Days'];
                        $withIn14DaysTotal = $customerDetails[$c['customerID']]['withIn14Days'];
                        $withIn30DaysTotal = $customerDetails[$c['customerID']]['withIn30Days'];
                        $over30Total = $customerDetails[$c['customerID']]['over30'];
                    }
                    
                } 

                $withIn7DaysTotal += ($withIn7Days > 0) ? $withIn7Days : 0;
                $withIn14DaysTotal += ($withIn14Days > 0) ? $withIn14Days : 0;
                $withIn30DaysTotal += ($withIn30Days > 0) ? $withIn30Days : 0;
                $over30Total += ($over30 > 0) ? $over30 : 0;
                if($salesPersonFlag){

                    $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($c['salesInvoiceID']);

                    $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);

                    $customerDetails[$salesPersonID][$c['customerID']]['cT'] = $c['customerTitle'];
                    $customerDetails[$salesPersonID][$c['customerID']]['cN'] = $c['customerName'];
                    $customerDetails[$salesPersonID][$c['customerID']]['cusCD'] = $c['customerCode'];
                    $customerDetails[$salesPersonID][$c['customerID']]['withIn7Days'] = $withIn7DaysTotal / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['withIn14Days'] = $withIn14DaysTotal / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['withIn30Days'] = $withIn30DaysTotal / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['over30'] = $over30Total / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['customerCategoryName'] = $c['customerCategoryName'];
                    $customerDetails[$salesPersonID][$c['customerID']]['salesPerson'] = ($salesPersonID == "other") ? "Other" :$c['salesPersonSortName'];
                    
                } else {
                    $customerDetails[$c['customerID']]['cT'] = $c['customerTitle'];
                    $customerDetails[$c['customerID']]['cN'] = $c['customerName'];
                    $customerDetails[$c['customerID']]['cusCD'] = $c['customerCode'];
                    $customerDetails[$c['customerID']]['withIn7Days'] = $withIn7DaysTotal;
                    $customerDetails[$c['customerID']]['withIn14Days'] = $withIn14DaysTotal;
                    $customerDetails[$c['customerID']]['withIn30Days'] = $withIn30DaysTotal;
                    $customerDetails[$c['customerID']]['over30'] = $over30Total;
                    $customerDetails[$c['customerID']]['customerCategoryName'] = $c['customerCategoryName'];
                }
            }

            return $customerDetails;
        }
    }

    public function processInvToAges($dataSet, $endDate) {
        $data = [];
        foreach ($dataSet as $key => $value) {

            $allow = true;

            if ($value['statusID'] == 5) {
                $dteArr = explode(' ', $value['deletedTimeStamp']);

                if (($dteArr[0] == $endDate || $dteArr[0] < $endDate)) {
                    $allow = false;
                } 
               
            }

            if ($allow) {
                $invID = $value['salesInvoiceID'];
                $payaments = $this->CommonTable('Invoice\Model\PaymentsTable')->getInvoicePaymentsDataByInvoiceId($invID, $endDate)->current();


                $paidAmount = (!empty($payaments['paidTotalAmount'])) ? $payaments['paidTotalAmount'] : 0;

                $end = strtotime($endDate);
                $invDate = strtotime($value['salesInvoiceIssuedDate']);

                $diff = $end - $invDate;

                $days = round($diff / (60 * 60 * 24));

                if (floatval($paidAmount) < floatval($value['salesinvoiceTotalAmount'])) {

                    $value['Current'] = NULL;
                    $value['WithIn7Days'] = NULL;
                    $value['WithIn14Days'] = NULL;
                    $value['WithIn30Days'] = NULL;
                    $value['Over30'] = NULL;

                    if ($days == 0) {
                        $value['Current'] =   floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);                   

                    } elseif ($days > 0 && $days <= 7) {
                        $value['WithIn7Days'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);
                    } elseif ($days > 7 && $days <= 14) {
                        $value['WithIn14Days'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);
                    } elseif ($days > 14 && $days <= 30) {
                        $value['WithIn30Days'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);
                    } else {
                        $value['Over30'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);

                    }

                    $data[] = $value;
                }

            }

        }
            
        return $data;

    }

    public function processInvAdvToAges($dataSet, $endDate) {
        $data = [];
        foreach ($dataSet as $key => $value) {


            $allow = true;

            if ($value['statusID'] == 5) {
                $dteArr = explode(' ', $value['deletedTimeStamp']);

                if (($dteArr[0] == $endDate || $dteArr[0] < $endDate)) {
                    $allow = false;
                } 
               
            }

            if ($allow) {
                $invID = $value['salesInvoiceID'];
                $payaments = $this->CommonTable('Invoice\Model\PaymentsTable')->getInvoicePaymentsDataByInvoiceId($invID, $endDate)->current();

                $paidAmount = (!empty($payaments['paidTotalAmount'])) ? $payaments['paidTotalAmount'] : 0;

                $end = strtotime($endDate);
                $invDate = strtotime($value['salesInvoiceIssuedDate']);

                $diff = $end - $invDate;

                $days = round($diff / (60 * 60 * 24));

                if (floatval($paidAmount) < floatval($value['salesinvoiceTotalAmount'])) {

                    $value['Current'] = NULL;
                    $value['WithIn30Days'] = NULL;
                    $value['WithIn60Days'] = NULL;
                    $value['WithIn90Days'] = NULL;
                    $value['WithIn180Days'] = NULL;
                    $value['WithIn270Days'] = NULL;
                    $value['WithIn365Days'] = NULL;
                    $value['Over365'] = NULL;

                    if ($days == 0) {
                        $value['Current'] =   floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);                   
                    } elseif ($days > 0 && $days <= 30) {
                        $value['WithIn30Days'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);
                    } elseif ($days > 30 && $days <= 60) {
                        $value['WithIn60Days'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);
                    } elseif ($days > 60 && $days <= 90) {
                        $value['WithIn90Days'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);
                    } elseif ($days > 90 && $days <= 180) {
                        $value['WithIn180Days'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);
                    } elseif ($days > 180 && $days <= 270) {
                        $value['WithIn270Days'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);
                    } elseif ($days > 270 && $days <= 365) {
                        $value['WithIn365Days'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);
                    } else {
                        $value['Over365'] = floatval($value['salesinvoiceTotalAmount']) - floatval($paidAmount);

                    }

                    $data[] = $value;
                }
            }

        }

           
        return $data;

    }


    public function getAdvanceAgedCustomerDetails($cusIds = null, $endDate = null, $isAllCustomers = false, $cusCategory = null,$salesPersons = [])
    {
        
        if (isset($cusIds) && isset($endDate)) {
            $endDate = $this->getGMTDateTime($endDate);
            $salesPersonFlag = false;
            if(!empty($salesPersons)){
                $salesPersonFlag = true;
                $withDefaultSalesPerson = (in_array('other', $salesPersons)) ? true : false;
            }
            $customerDetails = array();
            $agedCustomerDeatailsByInvTable = $this->CommonTable('Invoice\Model\InvoiceTable')->getAgedCusInvoiceAdvanceData($cusIds, $endDate, $groupBy = false, $isAllCustomers, $cusCategory, $salesPersons, "", $withDefaultSalesPerson);

            $processedInvAdvData = $this->processInvAdvToAges($agedCustomerDeatailsByInvTable, $endDate);
            foreach ($processedInvAdvData as $c) {
                $c['salesInvoiceIssuedDate'] = $this->getUserDateTime($c['salesInvoiceIssuedDate'], 'Y-m-d');
                $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceCreditNoteTotalByInvoiceIdForAge($c['salesInvoiceID'], $endDate);
                
                if($creditNotes){
                    $current = is_null($c['Current']) ? 0 : $c['Current'] - $creditNotes['total'];
                    $withIn30Days = is_null($c['WithIn30Days']) ? 0 : ($c['WithIn30Days'] + $current) - $creditNotes['total'];
                    $withIn60Days = is_null($c['WithIn60Days']) ? 0 : $c['WithIn60Days'] - $creditNotes['total'];
                    $withIn90Days = is_null($c['WithIn90Days']) ? 0 : $c['WithIn90Days'] - $creditNotes['total'];
                    $withIn180Days = is_null($c['WithIn180Days']) ? 0 : $c['WithIn180Days'] - $creditNotes['total'];
                    $withIn270Days = is_null($c['WithIn270Days']) ? 0 : $c['WithIn270Days'] - $creditNotes['total'];
                    $withIn365Days = is_null($c['WithIn365Days']) ? 0 : $c['WithIn365Days'] - $creditNotes['total'];
                    $over365 = is_null($c['Over365']) ? 0 : $c['Over365'] - $creditNotes['total'] ;
                }

                if($salesPersonFlag){
                    //if first time of customerID coming from $customerDetails array
                    $salesPersonID = (is_null($c['salesPersonID'])) ? "other" : $c['salesPersonID'];
                    if (empty($customerDetails[$salesPersonID][$c['customerID']])) {
                        $current = 0;
                        $withIn30DaysTotal = 0;
                        $withIn60DaysTotal = 0;
                        $withIn90DaysTotal = 0;
                        $withIn180DaysTotal = 0;
                        $withIn270DaysTotal = 0;
                        $withIn365DaysTotal = 0;
                        $over365Total = 0;
                    } else {
                        //if already have customerID related data on $customerDetails array
                        //get previous value from $customerDetails array to calculations
                        $current = $customerDetails[$salesPersonID][$c['customerID']]['current'];
                        $withIn30DaysTotal = $customerDetails[$salesPersonID][$c['customerID']]['withIn30Days'];
                        $withIn60DaysTotal = $customerDetails[$salesPersonID][$c['customerID']]['withIn60Days'];
                        $withIn90DaysTotal = $customerDetails[$salesPersonID][$c['customerID']]['withIn90Days'];
                        $withIn180DaysTotal = $customerDetails[$salesPersonID][$c['customerID']]['withIn180Days'];
                        $withIn270DaysTotal = $customerDetails[$salesPersonID][$c['customerID']]['withIn270Days'];
                        $withIn365DaysTotal = $customerDetails[$salesPersonID][$c['customerID']]['withIn365Days'];
                        $over365Total = $customerDetails[$salesPersonID][$c['customerID']]['over365'];
                    }
                } else {
                    //if first time of customerID coming from $customerDetails array
                    if (empty($customerDetails[$c['customerID']])) {
                        $currentTotal = 0;
                        $withIn30DaysTotal = 0;
                        $withIn60DaysTotal = 0;
                        $withIn90DaysTotal = 0;
                        $withIn180DaysTotal = 0;
                        $withIn270DaysTotal = 0;
                        $withIn365DaysTotal = 0;
                        $over365Total = 0;
                    } else {
                        //if already have customerID related data on $customerDetails array
                        //get previous value from $customerDetails array to calculations
                        $currentTotal = $customerDetails[$c['customerID']]['current'];
                        $withIn30DaysTotal = $customerDetails[$c['customerID']]['withIn30Days'];
                        $withIn60DaysTotal = $customerDetails[$c['customerID']]['withIn60Days'];
                        $withIn90DaysTotal = $customerDetails[$c['customerID']]['withIn90Days'];
                        $withIn180DaysTotal = $customerDetails[$c['customerID']]['withIn180Days'];
                        $withIn270DaysTotal = $customerDetails[$c['customerID']]['withIn270Days'];
                        $withIn365DaysTotal = $customerDetails[$c['customerID']]['withIn365Days'];
                        $over365Total = $customerDetails[$c['customerID']]['over365'];
                    }
                    
                }


                $currentTotal += $current;
                $withIn30DaysTotal += ($withIn30Days > 0) ? $withIn30Days : 0;
                $withIn60DaysTotal += ($withIn60Days > 0) ? $withIn60Days : 0;
                $withIn90DaysTotal += ($withIn90Days > 0) ? $withIn90Days : 0;
                $withIn180DaysTotal += ($withIn180Days > 0) ? $withIn180Days : 0;
                $withIn270DaysTotal += ($withIn270Days > 0) ? $withIn270Days : 0;
                $withIn365DaysTotal += ($withIn365Days > 0) ? $withIn365Days : 0;
                $over365Total += ($over365 > 0) ? $over365 : 0 ;

                if($salesPersonFlag){

                    $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($c['salesInvoiceID']);

                    $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);

                    $customerDetails[$salesPersonID][$c['customerID']]['cT'] = $c['customerTitle'];
                    $customerDetails[$salesPersonID][$c['customerID']]['cN'] = $c['customerName'];
                    $customerDetails[$salesPersonID][$c['customerID']]['cusCD'] = $c['customerCode'];
                    $customerDetails[$salesPersonID][$c['customerID']]['current'] = $currentTotal;
                    $customerDetails[$salesPersonID][$c['customerID']]['withIn30Days'] = $withIn30DaysTotal / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['withIn60Days'] = $withIn60DaysTotal / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['withIn90Days'] = $withIn90DaysTotal / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['withIn180Days'] = $withIn180DaysTotal / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['withIn270Days'] = $withIn270DaysTotal / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['withIn365Days'] = $withIn365DaysTotal / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['over365'] = $over365Total / $numOfSP;
                    $customerDetails[$salesPersonID][$c['customerID']]['customerCategoryName'] = $c['customerCategoryName'];
                    $customerDetails[$salesPersonID][$c['customerID']]['salesPerson'] = ($salesPersonID == "other") ? "Other" : $c['salesPersonSortName'];
                    
                } else {
                    $customerDetails[$c['customerID']]['cT'] = $c['customerTitle'];
                    $customerDetails[$c['customerID']]['cN'] = $c['customerName'];
                    $customerDetails[$c['customerID']]['cusCD'] = $c['customerCode'];
                    $customerDetails[$c['customerID']]['current'] = $currentTotal;
                    $customerDetails[$c['customerID']]['withIn30Days'] = $withIn30DaysTotal;
                    $customerDetails[$c['customerID']]['withIn60Days'] = $withIn60DaysTotal;
                    $customerDetails[$c['customerID']]['withIn90Days'] = $withIn90DaysTotal;
                    $customerDetails[$c['customerID']]['withIn180Days'] = $withIn180DaysTotal;
                    $customerDetails[$c['customerID']]['withIn270Days'] = $withIn270DaysTotal;
                    $customerDetails[$c['customerID']]['withIn365Days'] = $withIn365DaysTotal;
                    $customerDetails[$c['customerID']]['over365'] = $over365Total;
                    $customerDetails[$c['customerID']]['customerCategoryName'] = $c['customerCategoryName'];
                    
                }

            }

            return $customerDetails;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function viewAgedCustomerDataAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $cusIds = $request->getPost("cusIds");
            $endDate = $request->getPost("endDate");
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $advanceRow = $request->getPost('advanceDataFlag');
            $salesPersons = $request->getPost('salesPersons');
            $translator = new Translator();
            $name = $translator->translate('Customer Aged Analysis Report');
            $period = $endDate;
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }
            if($advanceDetails){
                $invoiceAgedCusData = $this->getAdvanceAgedCustomerDetails($cusIds, $endDate, $isAllCustomers, $cusCategory, $salesPersons);
            } else {
                $invoiceAgedCusData = $this->getAgedCustomerDetails($cusIds, $endDate, $isAllCustomers, $cusCategory, $salesPersons);
            }
                        
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateEndDate');
            $headerViewRender = $this->htmlRender($headerView);

            $agedCustomerView = new ViewModel(array(
                'aCD' => $invoiceAgedCusData,
                'endDate' => $endDate,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
                'advanceDetails' => $advanceDetails,
                'salesPersonFlag' => (empty($salesPersons))? 0 : 1,
            ));
            $agedCustomerView->setTemplate('reporting/customer-report/generate-aged-cus-pdf');

            $this->html = $agedCustomerView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateAgedCusPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $endDate = $request->getPost('endDate');
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $advanceRow = $request->getPost('advanceData');
            $salesPersons = $request->getPost('salesPersons');
            $translator = new Translator();
            $name = $translator->translate('Customer Aged Analysis Report');
            $period = $endDate;
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }

            if($advanceDetails){
                $invoiceAgedCusData = $this->getAdvanceAgedCustomerDetails($cusIds, $endDate, $isAllCustomers, $cusCategory, $salesPersons);
            } else {
                $invoiceAgedCusData = $this->getAgedCustomerDetails($cusIds, $endDate, $isAllCustomers, $cusCategory, $salesPersons);
            }

            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateEndDate');
            $headerViewRender = $this->htmlRender($headerView);

            $agedCustomerView = new ViewModel(array(
                'aCD' => $invoiceAgedCusData,
                'endDate' => $endDate,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
                'advanceDetails' => $advanceDetails,
                'salesPersonFlag' => (empty($salesPersons))? 0 : 1,
            ));

            $agedCustomerView->setTemplate('reporting/customer-report/generate-aged-cus-pdf');
            $agedCustomerView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($agedCustomerView);
            $pdfPath = $this->downloadPDF('customer-aged-analysis-report', $htmlContent, true, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * Generate Aged Customer Analysis sheet
     * @return csv
     */
    public function generateAgedCusSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $endDate = $request->getPost('endDate');
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $advanceRow = $request->getPost('advanceData');
            $salesPersons = $request->getPost('salesPersons');
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }

            if($advanceDetails){
                $invoiceAgedCusData = $this->getAdvanceAgedCustomerDetails($cusIds, $endDate, $isAllCustomers, $cusCategory,$salesPersons);
            } else {
                $invoiceAgedCusData = $this->getAgedCustomerDetails($cusIds, $endDate, $isAllCustomers, $cusCategory, $salesPersons);
            }

            $cD = $this->getCompanyDetails();
            if ($invoiceAgedCusData) {
                $title = '';
                $tit = 'CUSTOMER AGED ANALYSIS REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => $tit,
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "End Date",
                    4 => $endDate,
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-m-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                if(!empty($salesPersons)){
                    if($advanceDetails){
                        $arrs = array("SALES PERSON","CUSTOMER NAME", "CUSTOMER CODE", "0 - 1 DAYS", "1 - 30 DAYS", "31 - 60 DAYS","61 - 90 DAYS", "91 - 180 DAYS","181 - 270 DAYS", "271 - 365 DAYS", "> 365", "TOTAL");
                    } else {
                        $arrs = array("SALES PERSON", "CUSTOMER NAME", "CUSTOMER CODE", "WITHIN 7 DAYS", "WITHIN 14 DAYS", "WITHIN 30 DAYS", "OVER 30 DAYS", "TOTAL");
                        
                    }
                } else {
                    if($advanceDetails){
                        $arrs = array("CUSTOMER NAME", "CUSTOMER CODE", "0 - 1 DAYS", "1 - 30 DAYS", "31 - 60 DAYS","61 - 90 DAYS", "91 - 180 DAYS","181 - 270 DAYS", "271 - 365 DAYS", "> 365", "TOTAL");
                    } else {
                        $arrs = array("CUSTOMER NAME", "CUSTOMER CODE", "WITHIN 7 DAYS", "WITHIN 14 DAYS", "WITHIN 30 DAYS", "OVER 30 DAYS", "TOTAL");
                        
                    }
                    
                }

                $header = implode(",", $arrs);
                $arr = '';
                $str = array();

                if (sizeof($invoiceAgedCusData) == 0) {
                    $in = "no matching records found in Invoice";
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $currentTotal = 0.00;
                    $withIn7Total = 0.00;
                    $withIn14Total = 0.00;
                    $withIn30Total = 0.00;
                    $withIn60Total = 0.00;
                    $withIn90Total = 0.00;
                    $withIn180Total = 0.00;
                    $withIn270Total = 0.00;
                    $withIn365Total = 0.00;
                    $over365Total = 0.00;
                    $over30Total = 0.00;
                    $invTotal = 0.00;
                    foreach ($invoiceAgedCusData as $a) {
                        if (!empty($a)) {
                            if(!empty($salesPersons)){
                                foreach ($a as $key => $value) {
                                    $refNo = $value['salesInvoiceCode'];
                                    $date = $value['salesInvoiceIssuedDate'];
                                    $withIn7 = $value['withIn7Days'];
                                    $withIn7 = $withIn7 ? $withIn7 : 0;
                                    $withIn7Total += $withIn7;
                                    $withIn14 = $value['withIn14Days'];
                                    $withIn14 = $withIn14 ? $withIn14 : 0;
                                    $withIn14Total += $withIn14;
                                    $withIn30 = $value['withIn30Days'];
                                    $withIn30 = $withIn30 ? $withIn30 : 0;
                                    $withIn30Total += $withIn30;
                                    if($advanceDetails){
                                        $current = $value['current'];
                                        $current = $current ? $current : 0;
                                        $currentTotal += $current;

                                        $withIn60 = $value['withIn60Days'];
                                        $withIn60 = $withIn60 ? $withIn60 : 0;
                                        $withIn60Total += $withIn60;

                                        $withIn90 = $value['withIn90Days'];
                                        $withIn90 = $withIn90 ? $withIn90 : 0;
                                        $withIn90Total += $withIn90;

                                        $withIn180 = $value['withIn180Days'];
                                        $withIn180 = $withIn180 ? $withIn180 : 0;
                                        $withIn180Total += $withIn180;

                                        $withIn270 = $value['withIn270Days'];
                                        $withIn270 = $withIn270 ? $withIn270 : 0;
                                        $withIn270Total += $withIn270;

                                        $withIn365 = $value['withIn365Days'];
                                        $withIn365 = $withIn365 ? $withIn365 : 0;
                                        $withIn365Total += $withIn365;

                                        $over365 = $value['over365'];
                                        $over365 = $over365 ? $over365 : 0;
                                        $over365Total += $over365;
                                        $Total = $current + $withIn30 + $withIn60 + $withIn90 + $withIn180 + $withIn270 + $withIn365 + $over365;
                                    } else {
                                        $over30 = $value['over30'];
                                        $over30 = $over30 ? $over30 : 0;
                                        $over30Total += $over30;
                                        $Total = $withIn7 + $withIn14 + $withIn30 + $over30;
                                        
                                    }

                                    $invTotal += $Total;
                                    if ($Total > 1) {
                                        if($advanceDetails){
                                            $in = array(
                                            0 => $value['salesPerson'],
                                            1 => str_replace(",", " ",$value['cT'] . ' ' . $value['cN']),
                                            2 => $value['cusCD'],
                                            3 => $current,
                                            4 => $withIn30,
                                            5 => $withIn60,
                                            6 => $withIn90,
                                            7 => $withIn180,
                                            8 => $withIn270,
                                            9 => $withIn365,
                                            10 => $over365,
                                            11 => $Total
                                        );    
                                        } else {
                                            $in = array(
                                                0 => $value['salesPerson'],
                                                1 => str_replace(",", " ", $value['cT'] . ' ' . $value['cN']),
                                                2 => $value['cusCD'],
                                                3 => $withIn7,
                                                4 => $withIn14,
                                                5 => $withIn30,
                                                6 => $over30,
                                                7 => $Total
                                            );
                                            
                                        }
                                        $arr.=implode(",", $in) . "\n";
                                    }
                                }

                            } else {
                                $refNo = $a['salesInvoiceCode'];
                                $date = $a['salesInvoiceIssuedDate'];
                                $withIn7 = $a['withIn7Days'];
                                $withIn7 = $withIn7 ? $withIn7 : 0;
                                $withIn7Total += $withIn7;
                                $withIn14 = $a['withIn14Days'];
                                $withIn14 = $withIn14 ? $withIn14 : 0;
                                $withIn14Total += $withIn14;
                                $withIn30 = $a['withIn30Days'];
                                $withIn30 = $withIn30 ? $withIn30 : 0;
                                $withIn30Total += $withIn30;
                                if($advanceDetails){
                                    $current = $a['current'];
                                    $current = $current ? $current : 0;
                                    $currentTotal += $current;

                                    $withIn60 = $a['withIn60Days'];
                                    $withIn60 = $withIn60 ? $withIn60 : 0;
                                    $withIn60Total += $withIn60;

                                    $withIn90 = $a['withIn90Days'];
                                    $withIn90 = $withIn90 ? $withIn90 : 0;
                                    $withIn90Total += $withIn90;

                                    $withIn180 = $a['withIn180Days'];
                                    $withIn180 = $withIn180 ? $withIn180 : 0;
                                    $withIn180Total += $withIn180;

                                    $withIn270 = $a['withIn270Days'];
                                    $withIn270 = $withIn270 ? $withIn270 : 0;
                                    $withIn270Total += $withIn270;

                                    $withIn365 = $a['withIn365Days'];
                                    $withIn365 = $withIn365 ? $withIn365 : 0;
                                    $withIn365Total += $withIn365;

                                    $over365 = $a['over365'];
                                    $over365 = $over365 ? $over365 : 0;
                                    $over365Total += $over365;
                                    $Total = $current + $withIn30 + $withIn60 + $withIn90 + $withIn180 + $withIn270 + $withIn365 + $over365;
                                } else {
                                    $over30 = $a['over30'];
                                    $over30 = $over30 ? $over30 : 0;
                                    $over30Total += $over30;
                                    $Total = $withIn7 + $withIn14 + $withIn30 + $over30;
                                    
                                }

                                $invTotal += $Total;
                                if ($Total > 1) {
                                    if($advanceDetails){
                                        $in = array(
                                        0 => str_replace(",", " ", $a['cT'] . ' ' . $a['cN']),
                                        1 => $a['cusCD'],
                                        2 => $current,
                                        3 => $withIn30,
                                        4 => $withIn60,
                                        5 => $withIn90,
                                        6 => $withIn180,
                                        7 => $withIn270,
                                        8 => $withIn365,
                                        9 => $over365,
                                        10 => $Total
                                    );    
                                    } else {
                                        $in = array(
                                            0 => str_replace(",", " ", $a['cT'] . ' ' . $a['cN']),
                                            1 => $a['cusCD'],
                                            2 => $withIn7,
                                            3 => $withIn14,
                                            4 => $withIn30,
                                            5 => $over30,
                                            6 => $Total
                                        );
                                        
                                    }
                                    $arr.=implode(",", $in) . "\n";
                                }
                                
                            }
                        }
                    }
                    if(!empty($salesPersons)){
                        if($advanceDetails){
                            $in = array(
                            0 => "",
                            1 => "",
                            2 => "Total Sales Invoice",
                            3 => $currentTotal,
                            4 => $withIn30Total,
                            5 => $withIn60Total,
                            6 => $withIn90Total,
                            7 => $withIn180Total,
                            8 => $withIn270Total,
                            9 => $withIn365Total,
                            10 => $over365Total,
                            11 => $invTotal
                        );    
                        } else {
                            $in = array(
                                0 => "",
                                1 => "",
                                2 => "Total Sales Invoice",
                                3 => $withIn7Total,
                                4 => $withIn14Total,
                                5 => $withIn30Total,
                                6 => $over30Total,
                                7 => $invTotal
                            );
                            
                        }
                    } else {
                        if($advanceDetails){
                            $in = array(
                            0 => "",
                            1 => "Total Sales Invoice",
                            2 => $currentTotal,
                            3 => $withIn30Total,
                            4 => $withIn60Total,
                            5 => $withIn90Total,
                            6 => $withIn180Total,
                            7 => $withIn270Total,
                            8 => $withIn365Total,
                            9 => $over365Total,
                            10 => $invTotal
                        );    
                        } else {
                            $in = array(
                                0 => "",
                                1 => "Total Sales Invoice",
                                2 => $withIn7Total,
                                3 => $withIn14Total,
                                4 => $withIn30Total,
                                5 => $over30Total,
                                6 => $invTotal
                            );
                            
                        }
                        
                    }
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = 'no matching records found\n';
            }
        }
        $name = "customer-aged-analysis";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $endDate
     * @return $customerDetails
     */
    public function getAgedAnalysisDetails($endDate = null, $cusIds = null, $groupBy = false, $isAllCustomers = false, $cusCategory = null, $advanceDetails = false, $salesPersons = [], $sortBy = null)
    {
        if (isset($endDate)) {
            $endDate = $this->getGMTDateTime($endDate);
            $customerDetails = array();
            $withDefaultSalesPerson = (in_array('other', $salesPersons)) ? true : false;

            if($advanceDetails){
                $agedCustomerDeatailsByInvTable = $this->CommonTable('Invoice\Model\InvoiceTable')->getAgedCusInvoiceAdvanceData($cusIds, $endDate, $groupBy = false, $isAllCustomers, $cusCategory, $salesPersons, $sortBy, $withDefaultSalesPerson);

                $processedInvData = $this->processInvAdvToAges($agedCustomerDeatailsByInvTable, $endDate);
                foreach ($processedInvData as $key => $c) {

                    $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($c['salesInvoiceID']);

                    $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);

                    $c['salesInvoiceIssuedDate'] = $this->getUserDateTime($c['salesInvoiceIssuedDate'], 'Y-m-d');
                    $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceCreditNoteTotalByInvoiceIdForAge($c['salesInvoiceID'], $endDate);
                    if($creditNotes){
                        $c['Current'] = is_null($c['Current']) ? 0 : ($c['Current'] / $numOfSP) - ($creditNotes['total'] / $numOfSP);
                        $c['WithIn30Days'] = is_null($c['WithIn30Days']) ? 0 : ($c['WithIn30Days'] / $numOfSP) - ($creditNotes['total'] / $numOfSP);
                        $c['WithIn60Days'] = is_null($c['WithIn60Days']) ? 0 : ($c['WithIn60Days'] /$numOfSP) - ($creditNotes['total'] / $numOfSP);
                        $c['WithIn90Days'] = is_null($c['WithIn90Days']) ? 0 : ($c['WithIn90Days'] / $numOfSP) - ($creditNotes['total'] / $numOfSP);
                        $c['WithIn180Days'] = is_null($c['WithIn180Days']) ? 0 : ($c['WithIn180Days'] / $numOfSP) - ($creditNotes['total']/ $numOfSP);
                        $c['WithIn270Days'] = is_null($c['WithIn270Days']) ? 0 : ($c['WithIn270Days'] / $numOfSP)  - ($creditNotes['total'] / $numOfSP);
                        $c['WithIn365Days'] = is_null($c['WithIn365Days']) ? 0 : ($c['WithIn365Days'] / $numOfSP) - ($creditNotes['total'] / $numOfSP);
                        $c['Over365'] = is_null($c['Over365']) ? 0 : ($c['Over365'] / $numOfSP) - ($creditNotes['total']/ $numOfSP) ;
                    }
                    $customerDetails[$c['customerID']][$key][$c['salesInvoiceID']] = $c;
                }

            } else {

                $agedCustomerDeatailsByInvTable = $this->CommonTable('Invoice\Model\InvoiceTable')->getAgedCusInvoiceData($cusIds, $endDate, $groupBy,$isAllCustomers, $cusCategory, $salesPersons, $sortBy, $withDefaultSalesPerson);

                $processedInvData = $this->processInvToAges($agedCustomerDeatailsByInvTable, $endDate);
                foreach ($processedInvData as $key => $c) {
                    $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($c['salesInvoiceID']);

                    $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);
                    $c['salesInvoiceIssuedDate'] = $this->getUserDateTime($c['salesInvoiceIssuedDate'], 'Y-m-d');
                    $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceCreditNoteTotalByInvoiceIdForAge($c['salesInvoiceID'], $endDate);
                    if($creditNotes){
                        $current = is_null($c['Current']) ? 0 : $c['Current'];
                        if (is_null($c['Current']) && is_null($c['WithIn7Days'])) {
                            $c['WithIn7Days'] = 0;
                        } else if (!is_null($c['Current']) && is_null($c['WithIn7Days'])) {
                            $c['WithIn7Days'] = (floatval($current) / $numOfSP) - ($creditNotes['total'] / $numOfSP);
                        } else {
                            $c['WithIn7Days'] = ($c['WithIn7Days'] / $numOfSP) - ($creditNotes['total'] / $numOfSP) + (floatval($current) / $numOfSP);
                        }
                        $c['WithIn14Days'] = is_null($c['WithIn14Days']) ? 0 : ($c['WithIn14Days'] / $numOfSP) - ($creditNotes['total'] / $numOfSP);
                        $c['WithIn30Days'] = is_null($c['WithIn30Days']) ? 0 : ($c['WithIn30Days'] / $numOfSP) - ($creditNotes['total'] / $numOfSP);
                        $c['Over30'] = is_null($c['Over30']) ? 0 : ($c['Over30'] / $numOfSP) - ($creditNotes['total'] /$numOfSP) ;
                    }
                    $customerDetails[$c['customerID']][$key][$c['salesInvoiceID']] = $c;
                }
                
            }
            return $customerDetails;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function viewAgedAnalysisAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $endDate = $request->getPost('endDate');
            $cusIds = $request->getPost("cusIds");
            $groupBy = false;
            $endDate = $request->getPost("endDate");
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $advanceRow = $request->getPost('advanceDataFlag');
            $salesPersons = $request->getPost('salesPersons');
            $sortBy = $request->getPost('sortBy');
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }
            
            $agedAnalysisData = $this->getAgedAnalysisDetails($endDate,$cusIds, $groupBy, $isAllCustomers, $cusCategory, $advanceDetails, $salesPersons, $sortBy);
            
            $translator = new Translator();
            $name = $translator->translate('Invoice wise Aged Analysis Report');
            $period = $endDate;
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateEndDate');
            $headerViewRender = $this->htmlRender($headerView);

            $agedAnalysisView = new ViewModel(array(
                'aAD' => $agedAnalysisData,
                'endDate' => $endDate,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'advanceDetailsFlag' => $advanceDetails,
                'salesPersonFlag' => (empty($salesPersons))? 0 : 1,
            ));
            $agedAnalysisView->setTemplate('reporting/customer-report/generate-invoice-wise-aged-analysis-pdf');

            $this->html = $agedAnalysisView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return $agedAnalysisPdf
     */
    public function generateAgedAnalysisPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $endDate = $request->getPost('endDate');
            $cusIds = $request->getPost("cusIds");
            $groupBy = false;
            $endDate = $request->getPost("endDate");
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $advanceRow = $request->getPost('advanceDataFlag');
            $salesPersons = $request->getPost('salesPersons');
            $sortBy = $request->getPost('sortBy');
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }
                        
            $agedAnalysisData = $this->getAgedAnalysisDetails($endDate,$cusIds, $groupBy, $isAllCustomers, $cusCategory, $advanceDetails, $salesPersons, $sortBy);

            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Invoice wise Aged Analysis Report');
            $period = $endDate;

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateEndDate');
            $headerViewRender = $this->htmlRender($headerView);

            $invoiceWiseAgedCusView = new ViewModel(array(
                'aAD' => $agedAnalysisData,
                'endDate' => $endDate,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'advanceDetailsFlag' => $advanceDetails,
                'salesPersonFlag' => (empty($salesPersons))? 0 : 1,
            ));

            $invoiceWiseAgedCusView->setTemplate('reporting/customer-report/generate-invoice-wise-aged-analysis-pdf');
            $invoiceWiseAgedCusView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($invoiceWiseAgedCusView);
            $pdfPath = $this->downloadPDF('invoice-wise-aged-analysis-report', $htmlContent, true, true);

            $this->data = $pdfPath;
            $this->status = true;

            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return csv
     */
    public function generateAgedAnalysisSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $endDate = $request->getPost('endDate');
            $cusIds = $request->getPost("cusIds");
            $groupBy = false;
            $endDate = $request->getPost("endDate");
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $advanceRow = $request->getPost('advanceDataFlag');
            $salesPersons = $request->getPost('salesPersons');
            $sortBy = $request->getPost('sortBy');
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }
            
                        
            $agedAnalysisData = $this->getAgedAnalysisDetails($endDate,$cusIds, $groupBy, $isAllCustomers, $cusCategory, $advanceDetails, $salesPersons, $sortBy);

            $cD = $this->getCompanyDetails();

            if ($agedAnalysisData) {
                $title = '';
                $tit = 'INVOICE WISE AGED ANALYSIS REPORT';
                $in = array(
                    0 => $tit,
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "End Date",
                    1 => "$endDate",
                    2 => "",
                    3 => "",
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => ""
                );
                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => 'Report Generated: ' . date('Y-m-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n\n";
                if(!empty($salesPersons)){
                    if($advanceDetails){
                        $arrs = array("REF. NO", "CUSTOMER NAME","CUSTOMER CODE", "SALES PERSON","INVOICE DATE","TOTAL INVOICE AMOUNT", "0 - 1 DAYS", "1 - 30 DAYS", "31 - 60 DAYS", "61 - 90 DAYS", "91 - 180 DAYS", "181 - 270 DAYS", "271 - 365 DAYS", "> 365 DAYS");
                    } else {
                        $arrs = array("REF. NO", "CUSTOMER NAME","CUSTOMER CODE", "SALES PERSON","INVOICE DATE","TOTAL INVOICE AMOUNT", "WITHIN 7 DAYS", "WITHIN 14 DAYS", "WITHIN 30 DAYS", "OVER 30 DAYS");
                    }
                } else {
                    if($advanceDetails){
                        $arrs = array("REF. NO", "CUSTOMER NAME","CUSTOMER CODE", "INVOICE DATE","TOTAL INVOICE AMOUNT", "0 - 1 DAYS", "1 - 30 DAYS", "31 - 60 DAYS", "61 - 90 DAYS", "91 - 180 DAYS", "181 - 270 DAYS", "271 - 365 DAYS", "> 365 DAYS");
                    } else {
                        $arrs = array("REF. NO", "CUSTOMER NAME","CUSTOMER CODE", "INVOICE DATE","TOTAL INVOICE AMOUNT", "WITHIN 7 DAYS", "WITHIN 14 DAYS", "WITHIN 30 DAYS", "OVER 30 DAYS");
                    }
                    
                }
                $header = implode(",", $arrs);
                $arr = '';
                $str = array();

                if (sizeof($agedAnalysisData) == 0) {
                    $in = "no matching records found in Invoice";
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $currentTotal = 0.00;
                    $withIn7Total = 0.00;
                    $withIn14Total = 0.00;
                    $withIn30Total = 0.00;
                    $over30Total = 0.00;
                    $withIn60Total = 0.00;
                    $withIn90Total = 0.00;
                    $withIn180Total = 0.00;
                    $withIn270Total = 0.00;
                    $withIn365Total = 0.00;
                    $over365Total = 0.00;
                    $invTotal = 0.00;
                    foreach ($agedAnalysisData as $key => $cusValue) {
                        $customerWiseTotal = 0.00;
                        $customerCurrentTotal = 0.00;
                        $customerWiseWithIn7Total = 0.00;
                        $customerWiseWithIn14Total = 0.00;
                        $customerWiseWithIn30Total = 0.00;
                        $customerWiseOver30Total = 0.00;
                        $customerWiseWithIn60Total = 0.00;
                        $customerWiseWithIn90Total = 0.00;
                        $customerWiseWithIn180Total = 0.00;
                        $customerWiseWithIn270Total = 0.00;
                        $customerWiseWithIn365Total = 0.00;
                        $customerWiseOver365Total = 0.00;
                        foreach ($cusValue as $data) {
                            if (!empty($data)) {
                                foreach ($data as $a) {
                                    $refNo = $a['salesInvoiceCode'];
                                    $date = $a['salesInvoiceIssuedDate'];
                                    $withIn7 = $a['WithIn7Days'];
                                    $withIn7 = $withIn7 ? $withIn7 : 0;
                                    $withIn7Total += $withIn7;
                                    $withIn14 = $a['WithIn14Days'];
                                    $withIn14 = $withIn14 ? $withIn14 : 0;
                                    $withIn14Total += $withIn14;
                                    $withIn30 = $a['WithIn30Days'];
                                    $withIn30 = $withIn30 ? $withIn30 : 0;
                                    $withIn30Total += $withIn30;
                                    $current = $a['Current'];
                                    $current = $current ? $current : 0;
                                    $currentTotal += $current;
                                    $over30 = $a['Over30'];
                                    $over30 = $over30 ? $over30 : 0;
                                    $over30Total += $over30;

                                    $withIn60 = $a['WithIn60Days'];
                                    $withIn60 = $withIn60 ? $withIn60 : 0;
                                    $withIn60Total += $withIn60;
                                    $withIn90 = $a['WithIn90Days'];
                                    $withIn90 = $withIn90 ? $withIn90 : 0;
                                    $withIn90Total += $withIn90;
                                    $withIn180 = $a['WithIn180Days'];
                                    $withIn180 = $withIn180 ? $withIn180 : 0;
                                    $withIn180Total += $withIn180;
                                    $withIn270 = $a['WithIn270Days'];
                                    $withIn270 = $withIn270 ? $withIn270 : 0;
                                    $withIn270Total += $withIn270;
                                    $withIn365 = $a['WithIn365Days'];
                                    $withIn365 = $withIn365 ? $withIn365 : 0;
                                    $withIn365Total += $withIn365;
                                    $over365 = $a['Over365'];
                                    $over365 = $over365 ? $over365 : 0;
                                    $over365Total += $over365;

                                    $customerWiseWithIn30Total += $withIn30;

                                    if($advanceDetails){
                                        $Total = $current + $withIn30 + $withIn60 + $withIn90 + $withIn180 + $withIn270 + $withIn365 + $over365;
                                        $invTotal += $Total;
                                        $customerWiseTotal += $Total;
                                        $customerCurrentTotal += $current;
                                        $customerWiseWithIn60Total += $withIn60;
                                        $customerWiseWithIn90Total += $withIn90;
                                        $customerWiseWithIn180Total += $withIn180;
                                        $customerWiseWithIn270Total += $withIn270;
                                        $customerWiseWithIn365Total += $withIn365;
                                        $customerWiseOver365Total += $over365;

                                        if ($Total > 1) {
                                            if(!empty($salesPersons)){
                                                $in = [$refNo, str_replace(",", " ", $a['customerTitle'] . ' ' . $a['customerName']) ,$a['customerCode'],(is_null($a['salesPersonSortName'])) ? "Other" : $a['salesPersonSortName'], $a['salesInvoiceIssuedDate'],$Total,$current, $withIn30, $withIn60, $withIn90, $withIn180, $withIn270, $withIn365, $over365];
                                            } else {
                                                $in = [$refNo, str_replace(",", " ", $a['customerTitle'] . ' ' . $a['customerName']) ,$a['customerCode'], $a['salesInvoiceIssuedDate'],$Total, $current,$withIn30, $withIn60, $withIn90, $withIn180, $withIn270, $withIn365, $over365];
                                            }
                                            $arr.=implode(",", $in) . "\n";
                                        }
                                    } else {
                                        $Total = $withIn7 + $withIn14 + $withIn30 + $over30;
                                        $invTotal += $Total;
                                        $customerWiseTotal += $Total;

                                        $customerWiseWithIn7Total += $withIn7;
                                        $customerWiseWithIn14Total += $withIn14;
                                        $customerWiseOver30Total += $over30;
                                        if ($Total > 1) {
                                            if(!empty($salesPersons)){
                                                $in = [$refNo, str_replace(",", " ", $a['customerTitle'] . ' ' . $a['customerName']) ,$a['customerCode'], (is_null($a['salesPersonSortName'])) ? "Other" : $a['salesPersonSortName'],$a['salesInvoiceIssuedDate'],$Total, $withIn7, $withIn14, $withIn30, $over30];
                                            } else {
                                                $in = [$refNo, str_replace(",", " ", $a['customerTitle'] . ' ' . $a['customerName']) ,$a['customerCode'], $a['salesInvoiceIssuedDate'],$Total, $withIn7, $withIn14, $withIn30, $over30];
                                                
                                            }
                                            $arr.=implode(",", $in) . "\n";
                                        }
                                        
                                    }

                                }
                            }
                        }
                        if(!empty($salesPersons)){
                            if($advanceDetails){
                                $in = ["","","","", "Customer wise Total Sales Invoice", $customerWiseTotal, $customerCurrentTotal, $customerWiseWithIn30Total, $customerWiseWithIn60Total, $customerWiseWithIn90Total, $customerWiseWithIn180Total, $customerWiseWithIn270Total, $customerWiseWithIn365Total, $customerWiseOver365Total];
                            } else {
                                $in = ["", "","","","Customer wise Total Sales Invoice", $customerWiseTotal, $customerWiseWithIn7Total, $customerWiseWithIn14Total, $customerWiseWithIn30Total, $customerWiseOver30Total];
                            
                            }
                        } else {
                            if($advanceDetails){
                                $in = ["","","", "Customer wise Total Sales Invoice", $customerWiseTotal,$customerCurrentTotal, $customerWiseWithIn30Total, $customerWiseWithIn60Total, $customerWiseWithIn90Total, $customerWiseWithIn180Total, $customerWiseWithIn270Total, $customerWiseWithIn365Total, $customerWiseOver365Total];
                            } else {
                                $in = ["", "","","Customer wise Total Sales Invoice", $customerWiseTotal, $customerWiseWithIn7Total, $customerWiseWithIn14Total, $customerWiseWithIn30Total, $customerWiseOver30Total];
                            
                            }
                            
                        }
                        $arr.=implode(",", $in) . "\n";

                        
                    }
                    if(!empty($salesPersons)){
                        if($advanceDetails){
                            $in = ["", "","","","Total Sales Invoice", $invTotal, $currentTotal,$withIn30Total, $withIn60Total, $withIn90Total, $withIn180Total, $withIn270Total, $withIn365Total, $over365Total];
                        } else {
                            $in = ["", "","","","Total Sales Invoice", $invTotal, $withIn7Total, $withIn14Total, $withIn30Total, $over30Total];
                            
                        }
                    } else {
                        if($advanceDetails){
                            $in = ["", "","","Total Sales Invoice", $invTotal, $currentTotal,$withIn30Total, $withIn60Total, $withIn90Total, $withIn180Total, $withIn270Total, $withIn365Total, $over365Total];
                        } else {
                            $in = ["", "","","Total Sales Invoice", $invTotal, $withIn7Total, $withIn14Total, $withIn30Total, $over30Total];
                            
                        }
                        
                    }
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = 'no matching records found\n';
            }
        }
        $name = "invoice-wise-aged-analysis-report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $cusIds
     * @return $cusDetails
     */
    public function getCustomerDetails($cusIds = null, $isAllCustomers = false, $cusCategory = null)
    {
        if (isset($cusIds)) {
            $cusData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerAndPrimaryProfileDetailsByIds($cusIds, $isAllCustomers,$cusCategory);
            $cusDetails = array();
            foreach ($cusData as $c) {
                $c['customerAddress'] = (($c['customerProfileLocationNo']) ? $c['customerProfileLocationNo'] . ', ' : '') .
                        (($c['customerProfileLocationRoadName1']) ? $c['customerProfileLocationRoadName1'] . ', ' : '' ) .
                        (($c['customerProfileLocationRoadName2']) ? $c['customerProfileLocationRoadName2'] . ', ' : '' ) .
                        (($c['customerProfileLocationRoadName3']) ? $c['customerProfileLocationRoadName3'] . ', ' : '' ) .
                        (($c['customerProfileLocationSubTown']) ? $c['customerProfileLocationSubTown'] . ', ' : '') .
                        (($c['customerProfileLocationTown']) ? $c['customerProfileLocationTown'] . ', ' : '') .
                        (($c['customerProfileLocationPostalCode']) ? $c['customerProfileLocationPostalCode'] . ', ' : '' ) .
                        $c['customerProfileLocationCountry'] . '.';
                $c['customerEmail'] = $c['customerProfileEmail'];
                $cusDetails[$c['customerID']] = $c;
            }
            return $cusDetails;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function viewCustomerDetailsAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $translator = new Translator();
            $name = $translator->translate('Customer Details Report');
            $cusData = $this->getCustomerDetails($cusIds, $isAllCustomers, $cusCategory);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $cusDetailsView = new ViewModel(array(
                'cSD' => $cusData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
            ));
            $cusDetailsView->setTemplate('reporting/customer-report/generate-customer-details-pdf');

            $this->html = $cusDetailsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @return $customerDetailsPdf
     */
    public function generateCustomerDetailsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $translator = new Translator();
            $name = $translator->translate('Customer Details Report');

            $customerData = $this->getCustomerDetails($cusIds, $isAllCustomers, $cusCategory);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $customerDetailsView = new ViewModel(array(
                'cSD' => $customerData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
            ));

            $customerDetailsView->setTemplate('reporting/customer-report/generate-customer-details-pdf');
            $customerDetailsView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($customerDetailsView);
            $pdfPath = $this->downloadPDF('customer-details-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * return csv
     */
    public function generateCustomerDetailsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $customerData = $this->getCustomerDetails($cusIds, $isAllCustomers, $cusCategory);
            $cD = $this->getCompanyDetails();

            if ($customerData) {
                $title = '';
                $tit = 'CUSTOMER DETAILS REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "",
                    4 => $tit,
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => "",
                    9 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("NAME", "SHORT NAME", "ADDRESS", "DATE OF BIRTH","TELEPHONE", "EMAIL", "STATUS", "CREDIT LIMIT", "OUTSTANDING BALANCE", "CREDIT BALANCE");
                $header = implode(",", $arrs);
                $arr = '';

                if (count($customerData) > 0) {
                    foreach ($customerData as $c) {
                        $in = array(
                            0 => str_replace(',', "", $c['customerTitle'] . ' ' . $c['customerName']),
                            1 => str_replace(',', "", $c['customerCode']),
                            2 => str_replace([',', '"'], "", $c['customerAddress']),
                            3 => str_replace(',', "", $c['customerDateOfBirth']),
                            4 => str_replace(',', "", $c['customerTelephoneNumber']),
                            5 => str_replace(',', "", $c['customerEmail']),
                            6 => $c['customerStatus'] == 1 ? 'Active' : 'Inactive',
                            7 => $c['customerCreditLimit'],
                            8 => $c['customerCurrentBalance'],
                            9 => $c['customerCurrentCredit']
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                } else {
                    $arr = 'No Related Data for Customer Details';
                }

                $name = "customer_details_report";
                $csvContent = $this->csvContent($title, $header, $arr);
                $csvPath = $this->generateCSVFile($name, $csvContent);

                $this->data = $csvPath;
                $this->status = true;
                return $this->JSONRespond();
            }
        }
    }

    //get customer wist item details report
     public function viewCustomerWiseSalesItemDetailsAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $categoryIds = $request->getPost('categoryIds');
            $isAllCategory = $request->getPost('isAllCategory');
            $isAllItem = $request->getPost('isAllItem');
            $itemIDs = $request->getPost('itemIDs');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusCategory = $request->getPost('cusCategory');
            $translator = new Translator();
            $name = $translator->translate('Customer Wise Sales Item Details Report');
            $cusData = $this->getCustomerWiseSalesItemDetails($cusIds, $isAllCustomers, $categoryIds, $isAllCategory, $fromDate, $endDate, $isAllItem, $itemIDs, $cusCategory);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $cusDetailsView = new ViewModel(array(
                'cSD' => $cusData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
            ));
            $cusDetailsView->setTemplate('reporting/customer-report/generate-customer-wise-sales-item-details-pdf');

            $this->html = $cusDetailsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

     public function viewCustomerWiseSalesItemDetailsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $categoryIds = $request->getPost('categoryIds');
            $isAllCategory = $request->getPost('isAllCategory');
            $isAllItem = $request->getPost('isAllItem');
            $itemIDs = $request->getPost('itemIDs');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusCategory = $request->getPost('cusCategory');
            
            $translator = new Translator();
            $name = $translator->translate('Customer Wise Sales Item Details Report');
            $cusData = $this->getCustomerWiseSalesItemDetails($cusIds, $isAllCustomers, $categoryIds, $isAllCategory, $fromDate, $endDate, $isAllItem, $itemIDs, $cusCategory);
            
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $cusDetailsView = new ViewModel(array(
                'cSD' => $cusData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
            ));
            $cusDetailsView->setTemplate('reporting/customer-report/generate-customer-wise-sales-item-details-pdf');
            $cusDetailsView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($cusDetailsView);
            $pdfPath = $this->downloadPDF('customer-wise-sales-item-details-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateCustomerWiseSalesItemDetailsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $categoryIds = $request->getPost('categoryIds');
            $isAllCategory = $request->getPost('isAllCategory');
            $isAllItem = $request->getPost('isAllItem');
            $itemIDs = $request->getPost('itemIDs');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusCategory = $request->getPost('cusCategory');
            $cusData = $this->getCustomerWiseSalesItemDetails($cusIds, $isAllCustomers, $categoryIds, $isAllCategory, $fromDate, $endDate, $isAllItem, $itemIDs, $cusCategory);
            $cD = $this->getCompanyDetails();

            if ($cusData) {
                $title = '';
                $tit = 'CUSTOMER WISE SALES ITEM DETAILS REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => $tit,
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("CUSTOMER NAME","CUSTOMER CODE", "PARENT CATEGORY NAME","CATEGORY NAME", "ITEM CODE", "ITEM NAME", "ITEM QUANTITY", "ITEM TOTAL");
                $header = implode(",", $arrs);
                $arr = '';

                if (count($cusData) > 0) {
                    $totalQty = 0;
                    $totalAmt = 0;
                    foreach ($cusData as $c) {
                        $in = array(
                            0 => str_replace(',', ' ', $c['customerName']),
                            1 => $c['customerCode'],
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => '',
                        );
                        $arr.=implode(",", $in) . "\n";
                        foreach ($c['categorys'] as $cat) {
                            $catTotalQty = 0;
                            $catTotalAmt = 0;
                            $in = array(
                                0 => '',
                                1 => '',
                                2 => $cat['parentCategoryName'],
                                3 => $cat['categoryName'],
                                4 => '',
                                5 => '',
                                6 => '',
                                7 => '',
                            );
                            $arr.=implode(",", $in) . "\n";
                            foreach ($cat['products'] as $pro) {
                                $catTotalQty +=$pro['productQuantity'];
                                $totalQty +=$pro['productQuantity'];
                                $catTotalAmt +=$pro['productTotal'];
                                $totalAmt +=$pro['productTotal'];
                                $in = array(
                                    0 => str_replace(',', ' ', $c['customerName']),
                                    1 => $c['customerCode'],
                                    2 => $cat['parentCategoryName'],
                                    3 => $cat['categoryName'],
                                    4 => $pro['productCode'],
                                    5 => $pro['productName'],
                                    6 => $pro['productQuantity'],
                                    7 => $pro['productTotal'],
                                );
                                $arr.=implode(",", $in) . "\n";
                            }
                            $in = array(
                                0 => '',
                                1 => '',
                                2 => '',
                                3 => '',
                                4 => '',
                                5 => '',
                                6 => 'Category Wise Item Quantity',
                                7 => 'Category Wise Item Total',
                            );
                            $arr.=implode(",", $in) . "\n";
                            $in = array(
                                0 => str_replace(',', ' ', $c['customerName']),
                                1 => $c['customerCode'],
                                2 => $cat['parentCategoryName'],
                                3 => $cat['categoryName'],
                                4 => '',
                                5 => '',
                                6 => $catTotalQty,
                                7 => $catTotalAmt,
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => 'Total Quantity',
                        7 => 'Total Amount',
                    );
                    $arr.=implode(",", $in) . "\n";

                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => $totalQty,
                        7 => $totalAmt,
                    );
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $arr = 'No Related Data for Customer Details';
                }

                $name = "customer_wise_sales_item_details_report";
                $csvContent = $this->csvContent($title, $header, $arr);
                $csvPath = $this->generateCSVFile($name, $csvContent);

                $this->data = $csvPath;
                $this->status = true;
                return $this->JSONRespond();
            }
        }
    }

    public function getCustomerWiseSalesItemDetails($cusIds = null, $isAllCustomers = false, $categoryIds = null, $isAllCategory = false, $formDate = null, $endDate = null, $isAllItem = false, $itemIDs = null ,$cusCategory = null )
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        $catogaryIdSets = $this->CommonTable('Invoice\Model\CustomerTable')->getChildCateogryIds($categoryIds);

        if (sizeof($catogaryIdSets) > 0) {
            $childCategoryIds = explode(',', $catogaryIdSets['categoryIds']);
            $categoryIds = array_merge($childCategoryIds,$categoryIds);  
        }
        $cusData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerWiseSalesItemsDetails($cusIds, $isAllCustomers, $categoryIds, $isAllCategory, $formDate, $endDate, $isAllItem , $itemIDs, $cusCategory);
        $cusDetails = array();
        $category = array();
        $product = array();
        //get all credit note details
        $allCreditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getAllCreditNoteProductsForReport();
        //create creditNoteProduct data array
        $creditNoteDataArray = [];
        foreach ($allCreditNoteDetails as $creditNotekey => $creditNoteValue) {
            $creditNoteDataArray[$creditNoteValue['invoiceProductID']][] = $creditNoteValue;
        }
        
        foreach ($cusData as $c) {
            $creditNoteProductQty = 0;
            $creditNoteProductTotal = 0;
            if (count($creditNoteDataArray[$c['salesInvoiceProductID']]) > 0) {
                foreach ($creditNoteDataArray[$c['salesInvoiceProductID']] as $key => $value) {
                    
                    $creditNoteProductQty += $value['creditNoteProductQuantity'];
                    $creditNoteProductTotal += $value['creditNoteProductTotal'];
                }
            }

            $product[$c['customerID']][$c['categoryID']][$c['productID']]['productName'] =  $c['productName'];
            $product[$c['customerID']][$c['categoryID']][$c['productID']]['productCode'] =  $c['productCode'];
            $product[$c['customerID']][$c['categoryID']][$c['productID']]['productQuantity'] +=  $c['salesInvoiceProductQuantity'] - $creditNoteProductQty;
            $product[$c['customerID']][$c['categoryID']][$c['productID']]['productTotal'] +=  $c['salesInvoiceProductTotal'] - $creditNoteProductTotal;
            $category[$c['customerID']][$c['categoryID']]['categoryName'] =  $c['c1.catname'];
            $category[$c['customerID']][$c['categoryID']]['parentCategoryName'] =  $c['c2.catname'];
            $category[$c['customerID']][$c['categoryID']]['products'] = $product[$c['customerID']][$c['categoryID']];
            $cusDetails[$c['customerID']]['customerName'] = $c['customerName'];
            $cusDetails[$c['customerID']]['customerCode'] = $c['customerCode'];
            $cusDetails[$c['customerID']]['customerCategory']  = $c['customerCategoryName'];
            $cusDetails[$c['customerID']]['categorys'] = $category[$c['customerID']];
        }
        
        return $cusDetails;
    
    }

        //get customer wist item details report
     public function viewCustomerWiseSalesReportAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusCategory = $request->getPost('cusCategory');
            $translator = new Translator();
            $name = $translator->translate('Customer Wise Sales Report');
            $cusSalesData = $this->getCustomerWiseSalesReportData($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $cusSalesDetailsView = new ViewModel(array(
                'cSD' => $cusSalesData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
            ));
            $cusSalesDetailsView->setTemplate('reporting/customer-report/generate-customer-wise-sales-report-pdf');

            $this->html = $cusSalesDetailsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function viewCustomerWiseSalesReportPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusCategory = $request->getPost('cusCategory');
            $translator = new Translator();
            $name = $translator->translate('Customer Wise Sales Report');
            $cusSalesData = $this->getCustomerWiseSalesReportData($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $cusSalesDetailsView = new ViewModel(array(
                'cSD' => $cusSalesData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
            ));
            $cusSalesDetailsView->setTemplate('reporting/customer-report/generate-customer-wise-sales-report-pdf');
            $cusSalesDetailsView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($cusSalesDetailsView);
            $pdfPath = $this->downloadPDF('customer-wise-sales-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateCustomerWiseSalesReportSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusSalesData = $this->getCustomerWiseSalesReportData($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate);
            $cD = $this->getCompanyDetails();

            if ($cusSalesData) {
                $title = '';
                $tit = 'CUSTOMER WISE SALES REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => $tit,
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("CUSTOMER NAME", "CUSTOMER CODE","INV/CRN NO", "INV/CRN COMMENT", "INV/CRN DATE", "INV/CRN DISCOUNT", "INV/CRN TOTAL","ITEM NAME", "ITEM CODE", "ITEM QUANTITY", "ITEM PRICE", "ITEM DISCOUNT", "DISCOUNTED ITEM PRICE", "TOTAL CUMILATIVE AMOUNT");
                $header = implode(",", $arrs);
                $arr = '';

                if (count($cusSalesData) > 0) {
                    $totalQty = 0;
                    $totalAmt = 0;
                    $cumulativeBalance = 0;
                    foreach ($cusSalesData as $c) {
                        $custWiseQty = 0;
                        $custWiseAmt = 0;
                        $custWiseCumilative = 0;
                        $in = array(
                            0 => str_replace(',', ' ', $c['customerName']),
                            1 => $c['customerCode'],
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => '',
                            8 => '',
                            9 => '',
                            10 => '',
                            11 => '',
                            12 => '',
                            13 => '',
                        );
                        $arr.=implode(",", $in) . "\n";
                        foreach ($c['invoice'] as $inv) {
                            if ($inv['type']=="invoice") {
                                $totalInvValue += $inv['totalInvAmount'];
                                $custWiseCumilative += $inv['totalInvAmount'];
                                $cumulativeBalance += $inv['totalInvAmount'];
                            } else {
                                $totalInvValue -= $inv['totalInvAmount'];
                                $custWiseCumilative -= $inv['totalInvAmount'];
                                $cumulativeBalance -= $inv['totalInvAmount'];
                            }
                            $in = array(
                                0 => '',
                                1 => '',
                                2 => $inv['invoiceNO'],
                                3 => $inv['invoiceComment'],
                                4 => $inv['invoiceDate'],
                                5 => $inv['totalInvDisc'],
                                6 => $inv['totalInvAmount'],
                                7 => '',
                                8 => '',
                                9 => '',
                                10 => '',
                                11 => '',
                                12 => '',
                                13 => '',
                            );
                            $arr.=implode(",", $in) . "\n";
                            foreach ($inv['products'] as $pro) {
                                if ($pro['type']=="invoice") {
                                    $totalQty += $pro['productQuantity'];
                                    $totalAmt += $pro['productTotal'];
                                    $custWiseQty += $pro['productQuantity'];
                                    $custWiseAmt += $pro['productTotal'];
                                } else {
                                    $totalQty -= $pro['productQuantity'];
                                    $totalAmt -= $pro['productTotal'];
                                    $custWiseQty -= $pro['productQuantity'];
                                    $custWiseAmt -= $pro['productTotal'];
                                }
                                if ($pro['productDiscountType'] == "precentage" && $pro['productDiscount'] != "") {
                                    $discount = $pro['productDiscount']." (%)";
                                } else if ($pro['productDiscountType'] == "value" && $pro['productDiscount'] != ""){
                                    $discount = $pro['productDiscount']." (Rs)";
                                } else {
                                    $discount = $pro['productDiscount'];
                                }
                                $in = array(
                                    0 => '',
                                    1 => '',
                                    2 => '',
                                    3 => '',
                                    4 => '',
                                    5 => '',
                                    6 => '',
                                    7 => $pro['productName'],
                                    8 => $pro['productCode'],
                                    9 => $pro['productQuantity'],
                                    10 => $pro['productPrice'],
                                    11 => $discount,
                                    12 => $pro['productDiscountedPrice'],
                                    13 => '',
                                );
                                $arr.=implode(",", $in) . "\n";
                            }
                            $in = array(
                                    0 => '',
                                    1 => '',
                                    2 => '',
                                    3 => '',
                                    4 => '',
                                    5 => '',
                                    6 => '',
                                    7 => '',
                                    8 => '',
                                    9 => '',
                                    10 => '',
                                    11 => '',
                                    12 => '',
                                    13 => $totalInvValue,
                                );
                                $arr.=implode(",", $in) . "\n";

                        }
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => '',
                            8 => 'Customer Wise Qty',
                            9 => $custWiseQty,
                            10 => '',
                            11 => '',
                            12 => 'Customer Wise Cumulative Value',
                            13 => $custWiseCumilative,
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                    $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => '',
                        7 => '',
                        8 => 'Total Qty',
                        9 => $totalQty,
                        10 => '',
                        11 => '',
                        12 => 'Total Cumulative Value',
                        13 => $cumulativeBalance,
                        );
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $arr = 'No Related Data for Customer Details';
                }

                $name = "customer_wise_sales_item_details_report";
                $csvContent = $this->csvContent($title, $header, $arr);
                $csvPath = $this->generateCSVFile($name, $csvContent);

                $this->data = $csvPath;
                $this->status = true;
                return $this->JSONRespond();
            }
        }
    }

    //get customer wise sales report data
    public function getCustomerWiseSalesReportData($cusIds = null, $isAllCustomers = false, $locations = null, $isAllLocation = false, $formDate = null, $endDate = null, $cusCategory = null)
    {

        ini_set('memory_limit','-1');
        ini_set('max_execution_time','0');
        if (isset($cusIds)) {
            $cusData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerWiseSalesReport($cusIds, $isAllCustomers, $locations, $isAllLocation, $formDate, $endDate, $cusCategory);
            $cusCreditNoteData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerWiseCreditNoteData($cusIds, $isAllCustomers, $locations, $isAllLocation, $formDate, $endDate, $cusCategory);
            $cusDetails = array();
            $invoice = array();
            $product = array();
            $tmpCusData = [];
            $tmpInvProIds = [];

            foreach ($cusData as $c) {
                $tmpCusData[] = $c;
            }

            foreach ($cusCreditNoteData as $c) {
                $tmpCusData[] = $c;
            }

            $creditNoteTotal = [];
            $creditNoteTotalDisc = [];
            $creditNoteValidation =[];
            $invoiceValidation = [];
            $invoiceTotalDisc = [];
            foreach ($tmpCusData as $c) {
                if ($c['salesInvoiceID']) {
                    //calculate invoice wise total discount
                    if(!$invoiceValidation[$c['salesInvoiceID']]){
                        if($c['salesInvoiceWiseTotalDiscount'] != null){
                            $invoiceTotalDisc[$c['salesInvoiceID']] = $c['salesInvoiceWiseTotalDiscount'];
                        
                        } else {
                            $invoiceTotalDisc[$c['salesInvoiceID']] = 0;
                        }
                        $invoiceValidation[$c['salesInvoiceID']] = true;
                    }


                    if($c['salesInvoiceProductDiscountType'] == 'precentage'){
                        $discountedAmount = $c['salesInvoiceProductPrice']*(100 - $c['salesInvoiceProductDiscount'])/100;
                    } else if($c['salesInvoiceProductDiscountType'] == 'value') {
                        $discountedAmount = $c['salesInvoiceProductPrice'] - $c['salesInvoiceProductDiscount'];
                    } else {
                        $discountedAmount = 0;
                    }

                    $product[$c['customerID']][$c['salesInvoiceID']][$c['salesInvoiceProductID']]['productName'] =  $c['productName'];
                    $product[$c['customerID']][$c['salesInvoiceID']][$c['salesInvoiceProductID']]['productCode'] =  $c['productCode'];
                    $product[$c['customerID']][$c['salesInvoiceID']][$c['salesInvoiceProductID']]['productPrice'] =  $c['salesInvoiceProductPrice'];
                    $product[$c['customerID']][$c['salesInvoiceID']][$c['salesInvoiceProductID']]['productDiscountedPrice'] =  $discountedAmount;
                    $product[$c['customerID']][$c['salesInvoiceID']][$c['salesInvoiceProductID']]['productDiscount'] =  $c['salesInvoiceProductDiscount'];
                    $product[$c['customerID']][$c['salesInvoiceID']][$c['salesInvoiceProductID']]['productDiscountType'] =  $c['salesInvoiceProductDiscountType'];
                    $product[$c['customerID']][$c['salesInvoiceID']][$c['salesInvoiceProductID']]['productQuantity'] +=  $c['salesInvoiceProductQuantity'];
                    $product[$c['customerID']][$c['salesInvoiceID']][$c['salesInvoiceProductID']]['productTotal'] +=  $c['salesInvoiceProductTotal'];
                    $product[$c['customerID']][$c['salesInvoiceID']][$c['salesInvoiceProductID']]['type'] = "invoice";
                    $invoice[$c['customerID']][$c['salesInvoiceID']]['invoiceNO'] =  $c['salesInvoiceCode'];
                    $invoice[$c['customerID']][$c['salesInvoiceID']]['invoiceComment'] =  $c['salesInvoiceComment'];
                    $invoice[$c['customerID']][$c['salesInvoiceID']]['invoiceDate'] =  $c['salesInvoiceIssuedDate'];
                    $invoice[$c['customerID']][$c['salesInvoiceID']]['invoiceDate'] =  $c['salesInvoiceIssuedDate'];
                    $invoice[$c['customerID']][$c['salesInvoiceID']]['totalInvAmount'] =  floatval($c['salesinvoiceTotalAmount']);
                    $invoice[$c['customerID']][$c['salesInvoiceID']]['totalInvDisc'] =  floatval($invoiceTotalDisc[$c['salesInvoiceID']]);
                    $invoice[$c['customerID']][$c['salesInvoiceID']]['type'] =  "invoice";

                    $invoice[$c['customerID']][$c['salesInvoiceID']]['products'] = $product[$c['customerID']][$c['salesInvoiceID']];
                    $cusDetails[$c['customerID']]['customerName'] = $c['customerName'];
                    $cusDetails[$c['customerID']]['customerCode'] = $c['customerCode'];
                    $cusDetails[$c['customerID']]['cusCategory'] = $c['customerCategoryName'];
                    $cusDetails[$c['customerID']]['invoice'] = $invoice[$c['customerID']];
                }
                
                if ($c['creditNoteID']) {
                    //calculate invoice wise total discount
                    if(!$creditNoteValidation[$c['creditNoteID']]){
                        if($c['creditNoteInvoiceDiscountAmount'] != null){
                            $creditNoteTotalDisc[$c['creditNoteID']] = $c['creditNoteInvoiceDiscountAmount'];
                        
                        } else {
                            $creditNoteTotalDisc[$c['creditNoteID']] = 0;
                        }
                        $creditNoteValidation[$c['creditNoteID']] = true;
                    }


                    if($c['creditNoteProductDiscountType'] == 'precentage'){
                        $discountedAmount = $c['creditNoteProductPrice']*(100 - $c['creditNoteProductDiscount'])/100;
                    } else if($c['creditNoteProductDiscountType'] == 'value') {
                        $discountedAmount = $c['creditNoteProductPrice'] - $c['creditNoteProductDiscount'];
                    } else {
                        $discountedAmount = 0;
                    }

                    $product[$c['customerID']][$c['creditNoteID']][$c['creditNoteProductID']]['productName'] =  $c['productName'];
                    $product[$c['customerID']][$c['creditNoteID']][$c['creditNoteProductID']]['productCode'] =  $c['productCode'];
                    $product[$c['customerID']][$c['creditNoteID']][$c['creditNoteProductID']]['productPrice'] =  $c['creditNoteProductPrice'];
                    $product[$c['customerID']][$c['creditNoteID']][$c['creditNoteProductID']]['productDiscountedPrice'] =  $discountedAmount;
                    $product[$c['customerID']][$c['creditNoteID']][$c['creditNoteProductID']]['productDiscount'] =  $c['creditNoteProductDiscount'];
                    $product[$c['customerID']][$c['creditNoteID']][$c['creditNoteProductID']]['productDiscountType'] =  $c['creditNoteProductDiscountType'];
                    $product[$c['customerID']][$c['creditNoteID']][$c['creditNoteProductID']]['productQuantity'] +=  $c['creditNoteProductQuantity'];
                    $product[$c['customerID']][$c['creditNoteID']][$c['creditNoteProductID']]['productTotal'] +=  $c['creditNoteProductTotal'];
                    $product[$c['customerID']][$c['creditNoteID']][$c['creditNoteProductID']]['type'] = "creditNote";
                    $invoice[$c['customerID']][$c['creditNoteID']]['invoiceNO'] =  $c['creditNoteCode'];
                    $invoice[$c['customerID']][$c['creditNoteID']]['invoiceComment'] =  $c['creditNoteComment'];
                    $invoice[$c['customerID']][$c['creditNoteID']]['invoiceDate'] =  $c['creditNoteDate'];
                    $invoice[$c['customerID']][$c['creditNoteID']]['invoiceDate'] =  $c['creditNoteDate'];
                    $invoice[$c['customerID']][$c['creditNoteID']]['totalInvAmount'] =  floatval($c['creditNoteTotal']);
                    $invoice[$c['customerID']][$c['creditNoteID']]['totalInvDisc'] =  floatval($invoiceTotalDisc[$c['creditNoteID']]);
                    $invoice[$c['customerID']][$c['creditNoteID']]['type'] =  "creditNote";

                    $invoice[$c['customerID']][$c['creditNoteID']]['products'] = $product[$c['customerID']][$c['creditNoteID']];
                    $cusDetails[$c['customerID']]['customerName'] = $c['customerName'];
                    $cusDetails[$c['customerID']]['customerCode'] = $c['customerCode'];
                    $cusDetails[$c['customerID']]['cusCategory'] = $c['customerCategoryName'];
                    $cusDetails[$c['customerID']]['invoice'] = $invoice[$c['customerID']];
                    
                }
            }
            return $cusDetails;
        }
    }

    public function viewCustomerAllTransactionsAction()
    {

         $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusCategory = $request->getPost('cusCategory');
            $summaryData = $request->getPost('summaryData');
            $translator = new Translator();
            $name = $translator->translate('Customer All Transactions Report');
            $cusTransactionData = $this->getCustomerAllTransactionDetails($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $cusSalesDetailsView = new ViewModel(array(
                'cusTransactionData' => $cusTransactionData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
                'summaryOnly' => $summaryData
            ));
            $cusSalesDetailsView->setTemplate('reporting/customer-report/generate-customer-all-transaction-details-pdf');

            $this->html = $cusSalesDetailsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateCustomerAllTransactionsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusCategory = $request->getPost('cusCategory');
            $summaryData = $request->getPost('summaryData');
            $translator = new Translator();
            $name = $translator->translate('Customer All Transactions Report');
            $cusTransactionData = $this->getCustomerAllTransactionDetails($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $cusSalesDetailsView = new ViewModel(array(
                'cusTransactionData' => $cusTransactionData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
                'summaryOnly' => $summaryData
            ));
            $cusSalesDetailsView->setTemplate('reporting/customer-report/generate-customer-all-transaction-details-pdf');
            $cusSalesDetailsView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($cusSalesDetailsView);
            $pdfPath = $this->downloadPDF('customer-all-transaction-details-pdf', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateCustomerAllTransactionsSheetAction()
    {


        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusCategory = $request->getPost('cusCategory');
            $summaryData = $request->getPost('summaryData');
            $cusTransactionData = $this->getCustomerAllTransactionDetails($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory);
            $cD = $this->getCompanyDetails();

            if ($cusTransactionData) {
                $title = '';
                $tit = 'CUSTOMER ALL TRANSACTIONS REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => $tit,
                    3 => "",
                    4 => "",
                    5 => "",

                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";
                if($summaryData == 'true'){
                    $arrs = array("CUSTOMER NAME", "CUSTOMER CODE", "OPENNING BALANCE", "CUMULATIVE CUSTOMER BALANCE");
                } else {
                    $arrs = array("CUSTOMER NAME", "CUSTOMER CODE", "TRANSACTION TYPE", "TRANSACTION CODE", "DATE", "STATUS", "PARENT DOC","AMOUNT", "CUMULATIVE CUSTOMER BALANCE");
                }

                $header = implode(",", $arrs);
                $arr = '';

                if (count($cusTransactionData) > 0) {
                    $cumulativeBalance = 0;
                    foreach ($cusTransactionData as $c) {
                        $cumulativeBalance = $c['customerCumulativeBalance'];
                        if($summaryData != 'true'){
                            $in = array(
                                0 => str_replace(',', ' ', $c['customerName']),
                                1 => $c['customerCode'],
                                2 => '',
                                3 => '',
                                4 => '',
                                5 => '',
                                6 => '',
                                7 => 'Opening Balance',
                                8 => round($c['customerCumulativeBalance'],2)//<0?"(".round(abs($c['customerCumulativeBalance']),2).")":round(abs($c['customerCumulativeBalance']), 2)
                            );
                            $arr.=implode(",", $in) . "\n";
                            
                        }
                        foreach ($c['customerTransactions'] as $tran) {

                            $cumulativeBalance += ($tran['amount'] - $tran['creditAmount']);

                            $parentDocString = "";

                            if($tran['docType'] == 'Sales Invoice'){

                                if($tran['quotationCode'] != null){
                                    $parentDocString = 'QT / '.implode(' - ',$tran['parentDoc']);
                                } else if($tran['deliveryNoteCode'] != null){
                                    $parentDocString = 'DN / '.implode(' - ',array_unique($tran['parentDoc']));
                                } else if($tran['soCode'] != null){
                                    $parentDocString = 'SO / '.implode(' - ',$tran['parentDoc']);
                                } else if($tran['activityCode'] != null){
                                    $parentDocString = 'ACT / '.implode(' - ',$tran['parentDoc']);
                                } else if($tran['jobReferenceNumber'] != null){
                                    $parentDocString = 'JOB / '.implode(' - ',$tran['parentDoc']);
                                } else if($tran['projectCode'] != null){
                                    $parentDocString = 'PROJ / '.implode(' - ',$tran['parentDoc']);
                                }


                            } else if($tran['docType'] == 'Payment'){
                                $parentDocString = 'INV / '.implode(' - ', $tran['parentDoc']);
                            } else if($tran['docType'] == 'Credit Note'){
                                $parentDocString = 'INV / '.implode(' - ', $tran['parentDoc']);
                            } else if($tran['docType'] == 'Credit Note Payment'){
                                $parentDocString = 'CR / '.implode(' - ', $tran['parentDoc']);
                            } else if($tran['docType'] == 'Income'){
                                $parentDocString = 'INC / '.implode(' , ', $tran['parentDoc']);
                            } else if($tran['docType'] == 'incomePayment'){
                                $parentDocString = 'INC / '.implode(' , ', $tran['parentDoc']);
                            }

                            if($summaryData != 'true'){
                                $in = array(
                                    0 => str_replace(',', ' ', $c['customerName']),
                                    1 => $c['customerCode'],
                                    2 => $tran['docType'],
                                    3 => $tran['code'],
                                    4 => $tran['issueDate'],
                                    5 => $tran['status'],
                                    6 => $parentDocString,
                                    7 => round($tran['amount'],2),//<0?"(".round(abs($tran['amount']),2).")":round(abs($tran['amount']), 2),
                                    8 => round($cumulativeBalance,2)//<0?"(".round(abs($cumulativeBalance),2).")":round(abs($cumulativeBalance), 2)
                                );
                                $arr.=implode(",", $in) . "\n";
                            }

                        }
                        if($summaryData != 'true'){
                            $in = array(
                                0 => '',
                                1 => '',
                                2 => '',
                                3 => '',
                                4 => '',
                                5 => '',
                                6 => '',
                                7 => 'Cumulative Total',
                                8 => round($cumulativeBalance,2)//<0?"(".round(abs($cumulativeBalance),2).")":round(abs($cumulativeBalance), 2)

                            );
                            $arr.=implode(",", $in) . "\n";
                            
                        } else {
                            $in = array(
                                0 => str_replace(',', ' ', $c['customerName']),
                                1 => $c['customerCode'],
                                2 => round($c['customerCumulativeBalance'],2),
                                3 => round($cumulativeBalance,2),
                                
                            );
                            $arr.=implode(",", $in) . "\n";
                        }
                        $grandTotal += $cumulativeBalance;
                    }
                    if($summaryData != 'true'){
                        $in = array(
                                0 => '',
                                1 => '',
                                2 => '',
                                3 => '',
                                4 => '',
                                5 => '',
                                6 => '',
                                7 => 'Grand Total',
                                8 => round($grandTotal,2)//<0?"(".round(abs($grandTotal),2).")":round(abs($grandTotal), 2)

                        );
                        $arr.=implode(",", $in) . "\n";
                        
                    } else {
                        $in = array(
                                0 => '',
                                1 => '',
                                2 => 'Grand Total',
                                3 => round($grandTotal,2)//<0?"(".round(abs($grandTotal),2).")":round(abs($grandTotal), 2)

                        );
                        $arr.=implode(",", $in) . "\n";
                    }

                } else {
                    $arr = 'No Related Data for Customer Details';
                }


                $name = "customer_all_transaction_details";
                $csvContent = $this->csvContent($title, $header, $arr);

                $csvPath = $this->generateCSVFile($name, $csvContent);

                $this->data = $csvPath;
                $this->status = true;
                return $this->JSONRespond();
            }
        }
    }

    public function getCustomerAllTransactionDetails($cusIds = null, $isAllCustomers = false, $locations = null, $isAllLocation = false, $fromDate = null, $endDate = null, $cusCategory = null)
    {
        $status = ['3','4','6'];

        $cumulativeInvoiceTotal = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerCumulativeInvoiceBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeInvoiceTotal = $cumulativeInvoiceTotal != null?$cumulativeInvoiceTotal:array();


        $cumulativeIncomeTotal = $this->CommonTable('Accounting\Model\IncomeTable')->getCustomerCumulativeIncomeBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeIncomeTotal = ($cumulativeIncomeTotal != null) ? $cumulativeIncomeTotal : array();

        $cumulativeIncomePaymentTotal = $this->CommonTable('Invoice\Model\PaymentsTable')->getCustomerCumulativeIncomePaymentBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeIncomePaymentTotal = ($cumulativeIncomePaymentTotal != null) ? $cumulativeIncomePaymentTotal : array();


        $cumulativePaymentTotal = $this->CommonTable('Invoice\Model\PaymentsTable')->getCustomerCumulativePaymentBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativePaymentTotal = $cumulativePaymentTotal != null?$cumulativePaymentTotal:array();


        $cumulativeAdvancePaymentTotal = $this->CommonTable('Invoice\Model\PaymentsTable')->getCustomerCumulativeAdvancePaymentBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

         $cumulativeAdvancePaymentTotal = $cumulativeAdvancePaymentTotal != null?$cumulativeAdvancePaymentTotal:array();


        $cumulativeCreditNoteTotal = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCustomerCumulativeCreditNoteBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

         $cumulativeCreditNoteTotal = $cumulativeCreditNoteTotal != null?$cumulativeCreditNoteTotal:array();


        $cumulativeDirectCreditNoteTotal = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCustomerDirectCumulativeCreditNoteBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeDirectCreditNoteTotal = $cumulativeDirectCreditNoteTotal != null?$cumulativeDirectCreditNoteTotal:array();

        $cumulativeLinkedDirectCreditNoteTotal = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCustomerLinkedDirectCumulativeCreditNoteBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeLinkedDirectCreditNoteTotal = $cumulativeLinkedDirectCreditNoteTotal != null?$cumulativeLinkedDirectCreditNoteTotal:array();


        $cumulativeCreditNotePaymentTotal = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCustomerCumulativeCreditNotePaymentBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeCreditNotePaymentTotal = $cumulativeCreditNotePaymentTotal != null?$cumulativeCreditNotePaymentTotal:array();


        //get all invoices
        $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        // get all payment details
        $paymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);


        //get all incomes
        $incomeDetails = $this->CommonTable('Accounting\Model\IncomeTable')->getIncomerByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);


        // get income payment details
        $incPaymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getIncomePaymentByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        // get all credit note details
        $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

         // get all direct credit note details
        $directCreditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDirectCreditNoteByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        $linkedDirectCreditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getLinkedDirectCreditNoteByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        // get all credit note payments details
        $creditNotePaymentDetails = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        //get tax details
        $taxDetails = $this->CommonTable('Settings\Model\TaxTable')->getAllTax();
        $suspendedTax = [];
        foreach ($taxDetails as $key => $taxValue) {
            if($taxValue['taxSuspendable'] == 1){
                $suspendedTax[$taxValue['id']] = $taxValue['id'];
            }
            
        }

        $siDataSet = [];
        $paymentDataSet = [];
        $advancePaymentDataSet = [];
        $creditNoteDataSet = [];
        $directCreditNoteDataSet = [];
        $creditNotePaymentDataSet = [];  
        $incomePaymentDataSet = [];   
        $incDataSet = [];   

        foreach ($invoiceDetails as $key => $siValue) {
            $invoiceAmo = floatval($siValue['salesinvoiceTotalAmount']);
            // if($siValue['salesInvoiceSuspendedTax'] == 1 && sizeof($suspendedTax) > 0){
                                        
            //     $getTaxDetails = $this->CommonTable('Invoice\Model\InvoiceProductTaxTable')->getProductTaxByInvoiceAndTaxID($siValue['salesInvoiceID'], $suspendedTax)->current();
            //     $invoiceAmo += floatval($getTaxDetails['tax_total']);
            
            // }
            $siDataSet[$siValue['salesInvoiceID']]['docType'] = 'Sales Invoice';
            $siDataSet[$siValue['salesInvoiceID']]['code'] = $siValue['salesInvoiceCode'];
            $siDataSet[$siValue['salesInvoiceID']]['issueDate'] = $siValue['salesInvoiceIssuedDate'];
            $siDataSet[$siValue['salesInvoiceID']]['status'] = $siValue['statusName'];
            $siDataSet[$siValue['salesInvoiceID']]['amount'] = floatval($invoiceAmo);
            $siDataSet[$siValue['salesInvoiceID']]['customerID'] = $siValue['customerID'];
            $siDataSet[$siValue['salesInvoiceID']]['quotationCode'] = $siValue['quotationCode'];
            $siDataSet[$siValue['salesInvoiceID']]['deliveryNoteCode'] = $siValue['deliveryNoteCode'];
            $siDataSet[$siValue['salesInvoiceID']]['soCode'] = $siValue['soCode'];
            $siDataSet[$siValue['salesInvoiceID']]['activityCode'] = $siValue['activityCode'];
            $siDataSet[$siValue['salesInvoiceID']]['jobReferenceNumber'] = $siValue['jobReferenceNumber'];
            $siDataSet[$siValue['salesInvoiceID']]['projectCode'] = $siValue['projectCode'];

            if($siValue['quotationCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['quotationCode'];
            } else if($siValue['deliveryNoteCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['deliveryNoteCode'];
            } else if($siValue['soCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['soCode'];
            } else if($siValue['activityCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['activityCode'];
            } else if($siValue['jobReferenceNumber'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['jobReferenceNumber'];
            } else if($siValue['projectCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['projectCode'];
            }


        }

        foreach ($incomeDetails as $key => $inValue) {
            $invoiceAmo = floatval($inValue['incomeTotal']);
            
            $incDataSet[$inValue['incomeID']]['docType'] = 'Income';
            $incDataSet[$inValue['incomeID']]['code'] = $inValue['incomeCode'];
            $incDataSet[$inValue['incomeID']]['issueDate'] = $inValue['createdTimeStamp'];
            $incDataSet[$inValue['incomeID']]['status'] = $inValue['statusName'];
            $incDataSet[$inValue['incomeID']]['amount'] = floatval($invoiceAmo);
            $incDataSet[$inValue['incomeID']]['customerID'] = $inValue['customerID'];
            $incDataSet[$inValue['incomeID']]['parentDoc'][] = $inValue['incomeCode'];
        }

        foreach ($paymentDetails as $key => $paymentValue) {

            if($paymentValue['incomingPaymentType'] == 'invoice'){
                $paymentDataSet[$paymentValue['incomingPaymentID']]['docType'] = 'Payment';
                $paymentDataSet[$paymentValue['incomingPaymentID']]['code'] = $paymentValue['incomingPaymentCode'];
                $paymentDataSet[$paymentValue['incomingPaymentID']]['issueDate'] = $paymentValue['incomingPaymentDate'];
                $paymentDataSet[$paymentValue['incomingPaymentID']]['status'] = $paymentValue['statusName'];
                $paymentDataSet[$paymentValue['incomingPaymentID']]['parentDoc'][] = $paymentValue['salesInvoiceCode'];
                $paymentDataSet[$paymentValue['incomingPaymentID']]['amount'] = floatval($paymentValue['incomingPaymentAmount'])*-1;
                $paymentDataSet[$paymentValue['incomingPaymentID']]['creditAmount'] = floatval($paymentValue['incomingPaymentCreditAmount'])*-1;
                $paymentDataSet[$paymentValue['incomingPaymentID']]['customerID'] = $paymentValue['customerID'];
            }else if($paymentValue['incomingPaymentType'] == 'advance'){
                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['docType'] = 'Advance Payment';

                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['code'] = $paymentValue['incomingPaymentCode'];
                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['issueDate'] = $paymentValue['incomingPaymentDate'];
                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['amount'] = floatval($paymentValue['incomingPaymentAmount'])*-1;
                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['customerID'] = $paymentValue['customerID'];
            }

        }

        foreach ($incPaymentDetails as $key => $paymentValue) {
            if($paymentValue['incomingPaymentType'] == 'income'){
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['docType'] = 'incomePayment';
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['code'] = $paymentValue['incomingPaymentCode'];
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['issueDate'] = $paymentValue['createdTimeStamp'];
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['status'] = $paymentValue['statusName'];
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['parentDoc'][] = $paymentValue['incomeCode'];
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['amount'] = floatval($paymentValue['incomingPaymentAmount']) * -1;
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['creditAmount'] = floatval($paymentValue['incomingPaymentCreditAmount']) * -1;
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['balance'] = floatval($paymentValue['incomingPaymentBalance']);
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['paidAmount'] = floatval($paymentValue['incomingPaymentPaidAmount']);
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['customerID'] = $paymentValue['customerID'];
            }

        }


        foreach ($creditNoteDetails as $key => $creditNoteValue) {

            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['docType'] = 'Credit Note';
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['code'] = $creditNoteValue['creditNoteCode'];
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['issueDate'] = $creditNoteValue['creditNoteDate'];
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['status'] = $creditNoteValue['statusName'];
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['parentDoc'][] = $creditNoteValue['salesInvoiceCode'];
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['amount'] = floatval($creditNoteValue['creditNoteTotal'])*-1;
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['customerID'] = $creditNoteValue['customerID'];

        }


        foreach ($linkedDirectCreditNoteDetails as $key => $directLinkedCreditNoteValue) {

            $creditNoteDataSet[$directLinkedCreditNoteValue['creditNoteID']]['docType'] = 'Linked Direct Credit Note';
            $creditNoteDataSet[$directLinkedCreditNoteValue['creditNoteID']]['code'] = $directLinkedCreditNoteValue['creditNoteCode'];
            $creditNoteDataSet[$directLinkedCreditNoteValue['creditNoteID']]['issueDate'] = $directLinkedCreditNoteValue['creditNoteDate'];
            $creditNoteDataSet[$directLinkedCreditNoteValue['creditNoteID']]['status'] = $directLinkedCreditNoteValue['statusName'];
            $creditNoteDataSet[$directLinkedCreditNoteValue['creditNoteID']]['parentDoc'][] = $directLinkedCreditNoteValue['salesInvoiceCode'];
            $creditNoteDataSet[$directLinkedCreditNoteValue['creditNoteID']]['amount'] = floatval($directLinkedCreditNoteValue['creditNoteTotal'])*-1;
            $creditNoteDataSet[$directLinkedCreditNoteValue['creditNoteID']]['customerID'] = $directLinkedCreditNoteValue['customerID'];

        }


        foreach ($directCreditNoteDetails as $key => $directCreditNoteValue) {

            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['docType'] = 'Direct Credit Note';
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['code'] = $directCreditNoteValue['creditNoteCode'];
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['issueDate'] = $directCreditNoteValue['creditNoteDate'];
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['status'] = $directCreditNoteValue['statusName'];
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['parentDoc'][] = $directCreditNoteValue['salesInvoiceCode'];
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['amount'] = floatval($directCreditNoteValue['creditNoteTotal'])*-1;
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['customerID'] = $directCreditNoteValue['customerID'];

        }

        foreach ($creditNotePaymentDetails as $key => $creditNotePaymentValue) {

            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['docType'] = 'Credit Note Payment';
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['code'] = $creditNotePaymentValue['creditNotePaymentCode'];
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['issueDate'] = $creditNotePaymentValue['creditNotePaymentDate'];
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['status'] = $creditNotePaymentValue['statusName'];
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['parentDoc'][] = $creditNotePaymentValue['creditNoteCode'];
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['amount'] = floatval($creditNotePaymentValue['creditNotePaymentAmount']);
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['customerID'] = $creditNotePaymentValue['customerID'];

        }


        $arr = array_merge($siDataSet, $paymentDataSet, $advancePaymentDataSet, $creditNoteDataSet,  $creditNotePaymentDataSet, $incomePaymentDataSet, $incDataSet, $directCreditNoteDataSet);

        $sortData = Array();
        foreach ($arr as $key => $r) {
            if($r['docType'] == 'Payment'){
                $sortData[$key] = strtotime($r['issueDate'])+1;
            }else if($r['docType'] == 'Advance Payment'){
                $sortData[$key] = strtotime($r['issueDate'])+2;
            }else if($r['docType'] == 'Credit Note'){
                $sortData[$key] = strtotime($r['issueDate'])+3;
            }else if($r['docType'] == 'Credit Note Payment'){
                $sortData[$key] = strtotime($r['issueDate'])+4;
            }else if($r['docType'] == 'Income'){
                $sortData[$key] = strtotime($r['issueDate'])+5;
            }else if($r['docType'] == 'incomePayment'){
                $sortData[$key] = strtotime($r['issueDate'])+6;
            }else{
                $sortData[$key] = strtotime($r['issueDate']);
            }

        }

        array_multisort($sortData, SORT_ASC, $arr);

        $cutomerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCustomerIDs($cusIds, $isAllCustomers, $cusCategory);


        $customerData = [];
        foreach ($cutomerDetails as $key1 => $customerValue) {

            $cumulativeInvoiceTotalVal=0;
            $cumulativeIncomeTotalVal=0;
            $cumulativePaymentTotalVal=0;
            $cumulativeIncomePaymentTotalVal=0;
            $cumulativeAdvancePaymentTotalVal=0;
            $cumulativeCreditNoteTotalVal=0;
            $cumulativeDirectCreditNoteTotalVal=0;
            $cumulativeLinkedDirectCreditNoteTotalVal=0;
            $cumulativeCreditNotePaymentTotalVal=0;

            $customerData[$customerValue['customerID']]['customerName'] = $customerValue['customerName'];
            $customerData[$customerValue['customerID']]['customerCode'] = $customerValue['customerCode'];
            $customerData[$customerValue['customerID']]['customerTransactions'] = array();
            foreach ($arr as $key2 => $value) {

                if($value['customerID'] == $customerValue['customerID']){
                    array_push($customerData[$customerValue['customerID']]['customerTransactions'], $value);

                }



            }


            foreach ($cumulativeInvoiceTotal as $value2) {
                if($value2['customerID'] == $customerValue['customerID']){
                    $cumulativeInvoiceTotalVal += $value2['cumulative_total'];
                }
            }

            foreach ($cumulativeIncomeTotal as $value3) {
                if($value3['customerID'] == $customerValue['customerID']){
                    $cumulativeIncomeTotalVal += $value3['cumulative_total'];
                }
            }

            foreach ($cumulativeIncomePaymentTotal as $value4) {
                if($value4['customerID'] == $customerValue['customerID']){
                    $cumulativeIncomePaymentTotalVal += $value4['cumulative_total'];
                }
            }

            foreach ($cumulativePaymentTotal as $key => $value) {
                if($value['customerID'] == $customerValue['customerID']){
                    $cumulativePaymentTotalVal += $value['cumulative_total'];
                }
            }

            foreach ($cumulativeAdvancePaymentTotal as $key => $value) {
                if($value['customerID'] == $customerValue['customerID']){
                    $cumulativeAdvancePaymentTotalVal += $value['cumulative_total'];
                }
            }

            foreach ($cumulativeCreditNoteTotal as $key => $value) {
                if($value['customerID'] == $customerValue['customerID']){
                    $cumulativeCreditNoteTotalVal += $value['cumulative_total'];
                }
            }

            foreach ($cumulativeDirectCreditNoteTotal as $key => $value) {
                if($value['customerID'] == $customerValue['customerID']){
                    $cumulativeDirectCreditNoteTotalVal += $value['cumulative_total'];
                }
            }


            foreach ($cumulativeLinkedDirectCreditNoteTotal as $key => $value) {
                if($value['customerID'] == $customerValue['customerID']){
                    $cumulativeLinkedDirectCreditNoteTotalVal += $value['cumulative_total'];
                }
            }

            foreach ($cumulativeCreditNotePaymentTotal as $key => $value) {
                if($value['customerID'] == $customerValue['customerID']){
                    $cumulativeCreditNotePaymentTotalVal += $value['cumulative_total'];
                }
            }

            $customerData[$customerValue['customerID']]['customerCumulativeBalance'] = ($cumulativeInvoiceTotalVal + $cumulativeCreditNotePaymentTotalVal+ $cumulativeIncomeTotalVal) - ($cumulativePaymentTotalVal + $cumulativeAdvancePaymentTotalVal + $cumulativeCreditNoteTotalVal + $cumulativeIncomePaymentTotalVal + $cumulativeDirectCreditNoteTotalVal + $cumulativeLinkedDirectCreditNoteTotalVal);

            //testing code
            $customerData[$customerValue['customerID']]['customerOutstanding'] = $customerValue['customerCurrentBalance'] - $customerValue['customerCurrentCredit'];
        }

        return $customerData;
    }

    public function viewCustomerBalanceAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $plusToDate = date('Y-m-d H:i:s', strtotime($endDate . ' + 1 day'));
            $cusCategory = $request->getPost('cusCategory');
            $translator = new Translator();
            $name = $translator->translate('Customer Credit and Outstanding Balance Report');
            $cusTransactionData = $this->getCustomerBalanceDetails($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $plusToDate, $cusCategory);
            $companyDetails = $this->getCompanyDetails();
            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $cusSalesDetailsView = new ViewModel(array(
                'cusTransactionData' => $cusTransactionData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
                'summaryOnly' => $summaryData
            ));
            $cusSalesDetailsView->setTemplate('reporting/customer-report/generate-customer-balance-pdf');

            $this->html = $cusSalesDetailsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateCustomerBalanceDetailsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $plusToDate = date('Y-m-d H:i:s', strtotime($endDate . ' + 1 day'));
            $cusCategory = $request->getPost('cusCategory');
            $translator = new Translator();
            $name = $translator->translate('Customer Credit and Outstanding Balance Report');
            $cusTransactionData = $this->getCustomerBalanceDetails($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $plusToDate, $cusCategory);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $cusSalesDetailsView = new ViewModel(array(
                'cusTransactionData' => $cusTransactionData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
                'summaryOnly' => $summaryData
            ));
            $cusSalesDetailsView->setTemplate('reporting/customer-report/generate-customer-balance-pdf');
            $cusSalesDetailsView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($cusSalesDetailsView);
            $pdfPath = $this->downloadPDF('customer-balance-details-pdf', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateCustomerBalanceDetailsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $locations = $request->getPost('locations');
            $isAllLocation = $request->getPost('isAllLocation');
            $fromDate = $request->getPost('fromDate');
            $endDate = $request->getPost('endDate');
            $cusCategory = $request->getPost('cusCategory');
            $plusToDate = date('Y-m-d H:i:s', strtotime($endDate . ' + 1 day'));
            $cusTransactionData = $this->getCustomerBalanceDetails($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $plusToDate, $cusCategory);
            $cD = $this->getCompanyDetails();

            if ($cusTransactionData) {
                $title = '';
                $tit = 'CUSTOMER CREDIT AND OUTSTANDING BALANCE REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => $tit,
                    3 => "",
                    4 => "",
                    5 => "",

                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";
                $arrs = array("CUSTOMER NAME", "CUSTOMER CODE", "TRANSACTION TYPE", "TRANSACTION CODE", "CREATED DATE", "STATUS", "PARENT DOC","AMOUNT", "CUSTOMER OUTSTANDING", "CUSTOMER CREDIT");

                $header = implode(",", $arrs);
                $arr = '';

                if (count($cusTransactionData) > 0) {
                    $cumulativeBalance = 0;
                    $tempCusCredit = 0;
                    foreach ($cusTransactionData as $c) {
                        $tempCusOutstanding = $c['customerOpeningOutstanding'];
                        $tempCusCredit = $c['customerOpeningCredit'];
                        $in = array(
                            0 => str_replace(',', ' ', $c['customerName']),
                            1 => $c['customerCode'],
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => 'Opening Balance',
                            8 => round($c['customerOpeningOutstanding'],2),
                            9 => round($c['customerOpeningCredit'],2)
                        );
                        $arr.=implode(",", $in) . "\n";
                            
                        foreach ($c['customerTransactions'] as $tran) {

                            $parentDocString = "";

                            if($tran['docType'] == 'Sales Invoice'){

                                if($tran['quotationCode'] != null){
                                    $parentDocString = 'QT / '.implode(' - ',$tran['parentDoc']);
                                } else if($tran['deliveryNoteCode'] != null){
                                    $parentDocString = 'DN / '.implode(' - ',array_unique($tran['parentDoc']));
                                } else if($tran['soCode'] != null){
                                    $parentDocString = 'SO / '.implode(' - ',$tran['parentDoc']);
                                } else if($tran['activityCode'] != null){
                                    $parentDocString = 'ACT / '.implode(' - ',$tran['parentDoc']);
                                } else if($tran['jobReferenceNumber'] != null){
                                    $parentDocString = 'JOB / '.implode(' - ',$tran['parentDoc']);
                                } else if($tran['projectCode'] != null){
                                    $parentDocString = 'PROJ / '.implode(' - ',$tran['parentDoc']);
                                }


                            } else if($tran['docType'] == 'Payment'){
                                $parentDocString = 'INV / '.implode(' - ', $tran['parentDoc']);
                            } else if($tran['docType'] == 'Credit Note'){
                                $parentDocString = 'INV / '.implode(' - ', $tran['parentDoc']);
                            } else if($tran['docType'] == 'Credit Note Payment'){
                                $parentDocString = 'CR / '.implode(' - ', $tran['parentDoc']);
                            } else if($tran['docType'] == 'Income'){
                                $parentDocString = 'INC / '.implode(' , ', $tran['parentDoc']);
                            } else if($tran['docType'] == 'incomePayment'){
                                $parentDocString = 'INC / '.implode(' , ', $tran['parentDoc']);
                            }

                            if($tran['docType'] == 'Sales Invoice'){
                                $docAmount = $tran['amount'];
                                $tempCusOutstanding += $tran['amount'];
                            } else if($tran['docType'] == 'Payment'){
                                $tempCusCredit += $tran['balance'];
                                $tempCusCredit -= $tran['creditAmount'];
                                if ($tran['paidAmount'] != 0) {
                                    if ($tempCusOutstanding >= $tran['amount']) {
                                        $tempCusOutstanding -= $tran['amount'];
                                    } else {
                                        $creditDiffForInvPayment = $tran['amount'] - $tempCusOutstanding;
                                        $tempCusOutstanding = 0;
                                        $tempCusCredit += $creditDiffForInvPayment;
                                    }
                                } else {
                                    $tempCusOutstanding -= $tran['amount'];
                                }

                                if ($tran['creditAmount'] != 0) {
                                    $docAmount = $tran['paidAmount'] + $tran['creditAmount'];
                                } else {
                                    $docAmount = $tran['paidAmount'];
                                }
                            } else if($tran['docType'] == 'Advance Payment'){
                                $docAmount = $tran['amount'];
                                $tempCusCredit += $tran['amount'];
                            } else if($tran['docType'] == 'Credit Note'){
                                $docAmount = $tran['amount'];
                                 if ($tran['invoiceBalanceAmount'] >= $tran['amount']) {
                                    if ($tempCusOutstanding >= $tran['amount']) {
                                        $tempCusOutstanding -= $tran['amount'];
                                    } else {
                                        $creditDiffForCreditNote = $tran['amount'] - $tempCusOutstanding;
                                        $tempCusOutstanding = 0;
                                        $tempCusCredit += $creditDiffForCreditNote;
                                    }
                                } else {
                                    $tempCusOutstanding -= $tran['invoiceBalanceAmount'];
                                    $creditDiffForCreditNote = $tran['amount'] - $tran['invoiceBalanceAmount'];
                                    $tempCusCredit += $creditDiffForCreditNote; 
                                }
                            }else if($tran['docType'] == 'Income'){
                                $docAmount = $tran['amount'];
                                $tempCusOutstanding += $tran['amount'];
                            }else if($tran['docType'] == 'incomePayment'){
                                $tempCusCredit += $tran['balance'];
                                $tempCusCredit -= $tran['creditAmount'];
                                if ($tran['paidAmount'] != 0) {
                                    if ($tempCusOutstanding >= $tran['amount']) {
                                        $tempCusOutstanding -= $tran['amount'];
                                    } else {
                                        $creditDiffForInvPayment = $tran['amount'] - $tempCusOutstanding;
                                        $tempCusOutstanding = 0;
                                        $tempCusCredit += $creditDiffForInvPayment;
                                    }
                                } else {
                                    $tempCusOutstanding -= $tran['amount'];
                                }

                                if ($tran['creditAmount'] != 0) {
                                    $docAmount = $tran['paidAmount'] + $tran['creditAmount'];
                                } else {
                                    $docAmount = $tran['paidAmount'];
                                }
                            }else if($tran['docType'] == 'Linked Direct Credit Note'){
                                $docAmount = $tran['amount'];
                                 if ($tran['invoiceBalanceAmount'] >= $tran['amount']) {
                                    if ($tempCusOutstanding >= $tran['amount']) {
                                        $tempCusOutstanding -= $tran['amount'];
                                    } else {
                                        $creditDiffForCreditNote = $tran['amount'] - $tempCusOutstanding;
                                        $tempCusOutstanding = 0;
                                        $tempCusCredit += $creditDiffForCreditNote;
                                    }
                                } else {
                                    $tempCusOutstanding -= $tran['invoiceBalanceAmount'];
                                    $creditDiffForCreditNote = $tran['amount'] - $tran['invoiceBalanceAmount'];
                                    $tempCusCredit += $creditDiffForCreditNote; 
                                }
                            } else if($tran['docType'] == 'Direct Credit Note'){
                                $docAmount = $tran['amount'];
                                $tempCusCredit += $tran['amount'];
                            } else if($tran['docType'] == 'Credit Note Payment'){
                                $docAmount = $tran['amount'];
                                if ($tempCusCredit >= $tran['amount']) {
                                    $tempCusCredit -= $tran['amount'];
                                } else {
                                    $tempCusCredit = 0;
                                }
                            }

                            $in = array(
                                0 => str_replace(',', ' ', $c['customerName']),
                                1 => $c['customerCode'],
                                2 => $tran['docType'],
                                3 => $tran['code'],
                                4 => explode(" ",$tran['issueDate'])[0],
                                5 => $tran['status'],
                                6 => $parentDocString,
                                7 => round($docAmount,2),
                                8 => round($tempCusOutstanding,2),
                                9 => round($tempCusCredit,2)
                            );
                            $arr.=implode(",", $in) . "\n";

                        }
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => 'Cumulative Total',
                            8 => round($tempCusOutstanding,2),
                            9 => round($tempCusCredit,2)

                        );
                        $arr.=implode(",", $in) . "\n";
                            
                        $grandOutstandingTotal += $tempCusOutstanding;
                        $grandCreditTotal += $tempCusCredit;
                    }
                    $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => 'Grand Total',
                            8 => round($grandOutstandingTotal,2),
                            9 => round($grandCreditTotal,2)

                    );
                    $arr.=implode(",", $in) . "\n";

                } else {
                    $arr = 'No Related Data for Customer Details';
                }


                $name = "customer_credit_outstanding_balance_details_report";
                $csvContent = $this->csvContent($title, $header, $arr);

                $csvPath = $this->generateCSVFile($name, $csvContent);

                $this->data = $csvPath;
                $this->status = true;
                return $this->JSONRespond();
            }
        }
    }


    public function getCustomerBalanceDetails($cusIds = null, $isAllCustomers = false, $locations = null, $isAllLocation = false, $fromDate = null, $endDate = null, $cusCategory = null)
    {
        $status = ['3','4','6'];

        $cumulativeInvoiceTotal = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerCumulativeInvoiceBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeInvoiceTotal = ($cumulativeInvoiceTotal != null) ? $cumulativeInvoiceTotal : array();


        $cumulativeIncomeTotal = $this->CommonTable('Accounting\Model\IncomeTable')->getCustomerCumulativeIncomeBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeIncomeTotal = ($cumulativeIncomeTotal != null) ? $cumulativeIncomeTotal : array();

        $cumulativePaymentTotal = $this->CommonTable('Invoice\Model\PaymentsTable')->getCustomerCumulativePaymentBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativePaymentTotal = ($cumulativePaymentTotal != null) ? $cumulativePaymentTotal : array();

        $cumulativeIncomePaymentTotal = $this->CommonTable('Invoice\Model\PaymentsTable')->getCustomerCumulativeIncomePaymentBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeIncomePaymentTotal = ($cumulativeIncomePaymentTotal != null) ? $cumulativeIncomePaymentTotal : array();

        $cumulativeAdvancePaymentTotal = $this->CommonTable('Invoice\Model\PaymentsTable')->getCustomerCumulativeAdvancePaymentBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeAdvancePaymentTotal = ($cumulativeAdvancePaymentTotal != null) ? $cumulativeAdvancePaymentTotal : array();


        $cumulativeCreditNoteTotal = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCustomerCumulativeCreditNoteBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeCreditNoteTotal = ($cumulativeCreditNoteTotal != null) ? $cumulativeCreditNoteTotal : array();


        $cumulativeDirectCreditNoteTotal = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCustomerCumulativeDirectCreditNoteBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeDirectCreditNoteTotal = ($cumulativeDirectCreditNoteTotal != null) ? $cumulativeDirectCreditNoteTotal : array();

        $cumulativeLinkedDirectCreditNoteTotal = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCustomerCumulativeLinkedDirectCreditNoteBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeLinkedDirectCreditNoteTotal = ($cumulativeLinkedDirectCreditNoteTotal != null) ? $cumulativeLinkedDirectCreditNoteTotal : array();


        $cumulativeCreditNotePaymentTotal = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCustomerCumulativeCreditNotePaymentBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $cusCategory, $status);

        $cumulativeCreditNotePaymentTotal = ($cumulativeCreditNotePaymentTotal != null) ? $cumulativeCreditNotePaymentTotal : array();
        
        //get all invoices
        $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        // get all payment details
        $paymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);


        //get all incomes
        $incomeDetails = $this->CommonTable('Accounting\Model\IncomeTable')->getIncomerByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);


        // get income payment details
        $incPaymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getIncomePaymentByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        // get all credit note details
        $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        $directCreditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDirectCreditNoteByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        $linkedDirectCreditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDirectCreditNoteLinkedWithINVByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        // get all credit note payments details
        $creditNotePaymentDetails = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation,$fromDate, $endDate, $cusCategory, $status);

        //get tax details
        $taxDetails = $this->CommonTable('Settings\Model\TaxTable')->getAllTax();
        $suspendedTax = [];
        foreach ($taxDetails as $key => $taxValue) {
            if($taxValue['taxSuspendable'] == 1){
                $suspendedTax[$taxValue['id']] = $taxValue['id'];
            }
            
        }

        // $dddd = $this->getGMTDateTime('Y-m-d H:i:s','2018-05-22 10:36');
        
        $siDataSet = [];
        $paymentDataSet = [];
        $advancePaymentDataSet = [];
        $creditNoteDataSet = [];
        $directCreditNoteDataSet = [];
        $linkedDirectCreditNoteDataSet = [];
        $creditNotePaymentDataSet = [];    
        $incDataSet = [];
        $incomePaymentDataSet = [];


        foreach ($invoiceDetails as $key => $siValue) {
            $invoiceAmo = floatval($siValue['salesinvoiceTotalAmount']);
            // if($siValue['salesInvoiceSuspendedTax'] == 1 && sizeof($suspendedTax) > 0){
                                        
            //     $getTaxDetails = $this->CommonTable('Invoice\Model\InvoiceProductTaxTable')->getProductTaxByInvoiceAndTaxID($siValue['salesInvoiceID'], $suspendedTax)->current();
            //     $invoiceAmo += floatval($getTaxDetails['tax_total']);
            
            // }
            $siDataSet[$siValue['salesInvoiceID']]['docType'] = 'Sales Invoice';
            $siDataSet[$siValue['salesInvoiceID']]['code'] = $siValue['salesInvoiceCode'];
            $siDataSet[$siValue['salesInvoiceID']]['issueDate'] = $siValue['createdTimeStamp'];
            $siDataSet[$siValue['salesInvoiceID']]['status'] = $siValue['statusName'];
            $siDataSet[$siValue['salesInvoiceID']]['amount'] = floatval($invoiceAmo);
            $siDataSet[$siValue['salesInvoiceID']]['customerID'] = $siValue['customerID'];
            $siDataSet[$siValue['salesInvoiceID']]['quotationCode'] = $siValue['quotationCode'];
            $siDataSet[$siValue['salesInvoiceID']]['deliveryNoteCode'] = $siValue['deliveryNoteCode'];
            $siDataSet[$siValue['salesInvoiceID']]['soCode'] = $siValue['soCode'];
            $siDataSet[$siValue['salesInvoiceID']]['activityCode'] = $siValue['activityCode'];
            $siDataSet[$siValue['salesInvoiceID']]['jobReferenceNumber'] = $siValue['jobReferenceNumber'];
            $siDataSet[$siValue['salesInvoiceID']]['projectCode'] = $siValue['projectCode'];

            if($siValue['quotationCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['quotationCode'];
            } else if($siValue['deliveryNoteCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['deliveryNoteCode'];
            } else if($siValue['soCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['soCode'];
            } else if($siValue['activityCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['activityCode'];
            } else if($siValue['jobReferenceNumber'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['jobReferenceNumber'];
            } else if($siValue['projectCode'] != null){
                $siDataSet[$siValue['salesInvoiceID']]['parentDoc'][] = $siValue['projectCode'];
            }


        }

        foreach ($incomeDetails as $key => $inValue) {
            $invoiceAmo = floatval($inValue['incomeTotal']);
            
            $incDataSet[$inValue['incomeID']]['docType'] = 'Income';
            $incDataSet[$inValue['incomeID']]['code'] = $inValue['incomeCode'];
            $incDataSet[$inValue['incomeID']]['issueDate'] = $inValue['createdTimeStamp'];
            $incDataSet[$inValue['incomeID']]['status'] = $inValue['statusName'];
            $incDataSet[$inValue['incomeID']]['amount'] = floatval($invoiceAmo);
            $incDataSet[$inValue['incomeID']]['customerID'] = $inValue['customerID'];
            $incDataSet[$inValue['incomeID']]['parentDoc'][] = $inValue['incomeCode'];
        }


        foreach ($paymentDetails as $key => $paymentValue) {
            if($paymentValue['incomingPaymentType'] == 'invoice'){
                $paymentDataSet[$paymentValue['incomingPaymentID']]['docType'] = 'Payment';
                $paymentDataSet[$paymentValue['incomingPaymentID']]['code'] = $paymentValue['incomingPaymentCode'];
                $paymentDataSet[$paymentValue['incomingPaymentID']]['issueDate'] = $paymentValue['createdTimeStamp'];
                $paymentDataSet[$paymentValue['incomingPaymentID']]['status'] = $paymentValue['statusName'];
                $paymentDataSet[$paymentValue['incomingPaymentID']]['parentDoc'][] = $paymentValue['salesInvoiceCode'];
                $paymentDataSet[$paymentValue['incomingPaymentID']]['amount'] = floatval($paymentValue['incomingPaymentAmount']);
                $paymentDataSet[$paymentValue['incomingPaymentID']]['creditAmount'] = floatval($paymentValue['incomingPaymentCreditAmount']);
                $paymentDataSet[$paymentValue['incomingPaymentID']]['balance'] = floatval($paymentValue['incomingPaymentBalance']);
                $paymentDataSet[$paymentValue['incomingPaymentID']]['paidAmount'] = floatval($paymentValue['incomingPaymentPaidAmount']);
                $paymentDataSet[$paymentValue['incomingPaymentID']]['customerID'] = $paymentValue['customerID'];
            }else if($paymentValue['incomingPaymentType'] == 'advance'){
                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['docType'] = 'Advance Payment';

                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['code'] = $paymentValue['incomingPaymentCode'];
                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['issueDate'] = $paymentValue['createdTimeStamp'];
                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['amount'] = floatval($paymentValue['incomingPaymentPaidAmount']);
                $advancePaymentDataSet[$paymentValue['incomingPaymentID']]['customerID'] = $paymentValue['customerID'];
            }

        }


        foreach ($incPaymentDetails as $key => $paymentValue) {
            if($paymentValue['incomingPaymentType'] == 'income'){
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['docType'] = 'incomePayment';
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['code'] = $paymentValue['incomingPaymentCode'];
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['issueDate'] = $paymentValue['createdTimeStamp'];
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['status'] = $paymentValue['statusName'];
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['parentDoc'][] = $paymentValue['incomeCode'];
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['amount'] = floatval($paymentValue['incomingPaymentAmount']);
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['creditAmount'] = floatval($paymentValue['incomingPaymentCreditAmount']);
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['balance'] = floatval($paymentValue['incomingPaymentBalance']);
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['paidAmount'] = floatval($paymentValue['incomingPaymentPaidAmount']);
                $incomePaymentDataSet[$paymentValue['incomingPaymentID']]['customerID'] = $paymentValue['customerID'];
            }

        }

        foreach ($creditNoteDetails as $key => $creditNoteValue) {

            $invoiceBalanceAfterPayment = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBalanceAmountAfterPayment($creditNoteValue['invoiceID'], $status);
            $invoiceBalanceAmount = $invoiceBalanceAfterPayment['remainingInvoiceAmount'];
            $invoiceBalanceAfterCreditNote = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceRelatedCreditNotesForInvoiceRemainingAmount($creditNoteValue['invoiceID'], $status, $creditNoteValue['creditNoteID']);
            if (!is_null($invoiceBalanceAfterCreditNote)) {
                $invoiceBalanceAmount = $invoiceBalanceAmount + $invoiceBalanceAfterCreditNote['TotalCreditAmountAfterInvoice'];
            }

            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['docType'] = 'Credit Note';
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['code'] = $creditNoteValue['creditNoteCode'];
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['issueDate'] = $creditNoteValue['createdTimeStamp'];
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['status'] = $creditNoteValue['statusName'];
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['parentDoc'][] = $creditNoteValue['salesInvoiceCode'];
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['amount'] = floatval($creditNoteValue['creditNoteTotal']);
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['invoiceBalanceAmount'] = floatval($invoiceBalanceAmount);
            $creditNoteDataSet[$creditNoteValue['creditNoteID']]['customerID'] = $creditNoteValue['customerID'];

        }


        foreach ($linkedDirectCreditNoteDetails as $key => $linkedDirectcreditNoteValue) {

            // $invoiceBalanceAfterPayment = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBalanceAmountAfterPayment($linkedDirectcreditNoteValue['invoiceID'], $status);
            // $invoiceBalanceAmount = $invoiceBalanceAfterPayment['remainingInvoiceAmount'];
            // $invoiceBalanceAfterCreditNote = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceRelatedCreditNotesForInvoiceRemainingAmount($linkedDirectcreditNoteValue['invoiceID'], $status, $linkedDirectcreditNoteValue['creditNoteID']);
            // if (!is_null($invoiceBalanceAfterCreditNote)) {
            //     $invoiceBalanceAmount = $invoiceBalanceAmount + $invoiceBalanceAfterCreditNote['TotalCreditAmountAfterInvoice'];
            // }

            $linkedDirectCreditNoteDataSet[$linkedDirectcreditNoteValue['creditNoteID']]['docType'] = 'Linked Direct Credit Note';
            $linkedDirectCreditNoteDataSet[$linkedDirectcreditNoteValue['creditNoteID']]['code'] = $linkedDirectcreditNoteValue['creditNoteCode'];
            $linkedDirectCreditNoteDataSet[$linkedDirectcreditNoteValue['creditNoteID']]['issueDate'] = $linkedDirectcreditNoteValue['createdTimeStamp'];
            $linkedDirectCreditNoteDataSet[$linkedDirectcreditNoteValue['creditNoteID']]['status'] = $linkedDirectcreditNoteValue['statusName'];
            $linkedDirectCreditNoteDataSet[$linkedDirectcreditNoteValue['creditNoteID']]['parentDoc'][] = $linkedDirectcreditNoteValue['salesInvoiceCode'];
            $linkedDirectCreditNoteDataSet[$linkedDirectcreditNoteValue['creditNoteID']]['amount'] = floatval($linkedDirectcreditNoteValue['creditNoteTotal']);
            $linkedDirectCreditNoteDataSet[$linkedDirectcreditNoteValue['creditNoteID']]['invoiceBalanceAmount'] = floatval($linkedDirectcreditNoteValue['creditNoteTotal']);
            $linkedDirectCreditNoteDataSet[$linkedDirectcreditNoteValue['creditNoteID']]['customerID'] = $linkedDirectcreditNoteValue['customerID'];

        }

        foreach ($directCreditNoteDetails as $key => $directCreditNoteValue) {

            // $invoiceBalanceAfterPayment = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBalanceAmountAfterPayment($creditNoteValue['invoiceID'], $status);
            // $invoiceBalanceAmount = $invoiceBalanceAfterPayment['remainingInvoiceAmount'];
            // $invoiceBalanceAfterCreditNote = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceRelatedCreditNotesForInvoiceRemainingAmount($creditNoteValue['invoiceID'], $status, $creditNoteValue['creditNoteID']);
            // if (!is_null($invoiceBalanceAfterCreditNote)) {
            //     $invoiceBalanceAmount = $invoiceBalanceAmount + $invoiceBalanceAfterCreditNote['TotalCreditAmountAfterInvoice'];
            // }

            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['docType'] = 'Direct Credit Note';
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['code'] = $directCreditNoteValue['creditNoteCode'];
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['issueDate'] = $directCreditNoteValue['createdTimeStamp'];
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['status'] = $directCreditNoteValue['statusName'];
            // $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['parentDoc'][] = null;
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['amount'] = floatval($directCreditNoteValue['creditNoteTotal']);
            // $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['invoiceBalanceAmount'] = floatval($invoiceBalanceAmount);
            $directCreditNoteDataSet[$directCreditNoteValue['creditNoteID']]['customerID'] = $directCreditNoteValue['customerID'];

        }

        foreach ($creditNotePaymentDetails as $key => $creditNotePaymentValue) {

            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['docType'] = 'Credit Note Payment';
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['code'] = $creditNotePaymentValue['creditNotePaymentCode'];
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['issueDate'] = $creditNotePaymentValue['createdTimeStamp'];
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['status'] = $creditNotePaymentValue['statusName'];
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['parentDoc'][] = $creditNotePaymentValue['creditNoteCode'];
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['amount'] = floatval($creditNotePaymentValue['creditNotePaymentAmount']);
            $creditNotePaymentDataSet[$creditNotePaymentValue['creditNotePaymentID']]['customerID'] = $creditNotePaymentValue['customerID'];

        }


        $arr = array_merge($siDataSet, $paymentDataSet, $advancePaymentDataSet, $creditNoteDataSet,  $creditNotePaymentDataSet, $incomePaymentDataSet, $incDataSet, $directCreditNoteDataSet, $linkedDirectCreditNoteDataSet);

        $sortData = Array();
        foreach ($arr as $key => $r) {
            if($r['docType'] == 'Payment'){
                $sortData[$key] = strtotime($r['issueDate'])+1;
            }else if($r['docType'] == 'Advance Payment'){
                $sortData[$key] = strtotime($r['issueDate'])+2;
            }else if($r['docType'] == 'Credit Note'){
                $sortData[$key] = strtotime($r['issueDate'])+3;
            }else if($r['docType'] == 'Credit Note Payment'){
                $sortData[$key] = strtotime($r['issueDate'])+4;
            }else if($r['docType'] == 'Income'){
                $sortData[$key] = strtotime($r['issueDate'])+5;
            }else if($r['docType'] == 'incomePayment'){
                $sortData[$key] = strtotime($r['issueDate'])+6;
            }else if($r['docType'] == 'Direct Credit Note'){
                $sortData[$key] = strtotime($r['issueDate'])+7;
            }else if($r['docType'] == 'Linked Direct Credit Note'){
                $sortData[$key] = strtotime($r['issueDate'])+8;
            }else{
                $sortData[$key] = strtotime($r['issueDate']);
            }

        }

        array_multisort($sortData, SORT_ASC, $arr);

        $arrd = array_merge($cumulativeInvoiceTotal, $cumulativePaymentTotal, $cumulativeAdvancePaymentTotal, $cumulativeCreditNoteTotal,  $cumulativeCreditNotePaymentTotal, $cumulativeIncomePaymentTotal, $cumulativeIncomeTotal, $cumulativeDirectCreditNoteTotal, $cumulativeLinkedDirectCreditNoteTotal);

        $cumulativeTotalSortData = Array();
        foreach ($arrd as $key => $value) {
            if($value['incomingPaymentType'] == 'invoice'){
                $cumulativeTotalSortData[$key] = strtotime($value['createdTimeStamp'])+1;
            }else if($value['incomingPaymentType'] == 'advance'){
                $cumulativeTotalSortData[$key] = strtotime($value['createdTimeStamp'])+2;
            }else if($key == 'creditNoteID'){
                $cumulativeTotalSortData[$key] = strtotime($value['createdTimeStamp'])+3;
            }else if($key == 'creditNotePaymentID'){
                $cumulativeTotalSortData[$key] = strtotime($value['createdTimeStamp'])+4;
            }else if($value['incomingPaymentType'] == 'income'){
                $cumulativeTotalSortData[$key] = strtotime($value['createdTimeStamp'])+5;
            }else if($key == 'incomeID'){
                $cumulativeTotalSortData[$key] = strtotime($value['createdTimeStamp'])+6;
            }else{
                $cumulativeTotalSortData[$key] = strtotime($value['createdTimeStamp']);
            }
        }

        array_multisort($cumulativeTotalSortData, SORT_ASC, $arrd);
        $cutomerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCustomerIDs($cusIds, $isAllCustomers, $cusCategory);

        $customerData = [];
        foreach ($cutomerDetails as $key1 => $customerValue) {

            $openingCredit=0;
            $openingOutstanding=0;

            $customerData[$customerValue['customerID']]['customerName'] = $customerValue['customerName'];
            $customerData[$customerValue['customerID']]['customerCode'] = $customerValue['customerCode'];
            $customerData[$customerValue['customerID']]['customerTransactions'] = array();
            foreach ($arr as $key2 => $value) {
                if($value['customerID'] == $customerValue['customerID']){
                    array_push($customerData[$customerValue['customerID']]['customerTransactions'], $value);
                }
            }

            foreach ($arrd as $key => $value) {
                if($value['customerID'] == $customerValue['customerID']){
                    foreach ($value as $ke => $val) {
                        if ($ke == "salesInvoiceID") {
                            $openingOutstanding += $value['salesinvoiceTotalAmount'];
                        } else if ($ke == "creditNoteID") {
                            if ($value['invoiceID'] == 0) {
                                $openingCredit += $value['creditNoteTotal'];
                            } else {
                                $invoiceBalanceAfterPayment = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBalanceAmountAfterPayment($value['invoiceID'], $status);
                                $invoiceBalanceAmount = (is_null($invoiceBalanceAfterPayment['remainingInvoiceAmount'])) ? 0 :$invoiceBalanceAfterPayment['remainingInvoiceAmount'];
                                if ($value['invoiceID']!=0) {
                                    $invoiceBalanceAfterCreditNote = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceRelatedCreditNotesForInvoiceRemainingAmount($value['invoiceID'], $status, $value['creditNoteID']);
                                }
                                if (!is_null($invoiceBalanceAfterCreditNote)) {
                                    $invoiceBalanceAmount = $invoiceBalanceAmount + $invoiceBalanceAfterCreditNote['TotalCreditAmountAfterInvoice'];
                                }
                                
                                if ($invoiceBalanceAmount >= $value['creditNoteTotal']) {
                                    if ($openingOutstanding >= $value['creditNoteTotal']) {
                                        $openingOutstanding -= $value['creditNoteTotal'];
                                    } else {
                                        $creditDiffForCreditNote = $value['creditNoteTotal'] - $openingOutstanding;
                                        $openingOutstanding = 0;
                                        $openingCredit += $creditDiffForCreditNote;
                                    }
                                } else {
                                    $openingOutstanding -= $invoiceBalanceAmount;
                                    $creditDiffForCreditNote = $value['creditNoteTotal'] - $invoiceBalanceAmount;
                                    $openingCredit += $creditDiffForCreditNote; 
                                }
                            }
                        } else if ($ke == "creditNotePaymentID") {
                            if ($openingCredit >= $value['creditNotePaymentAmount']) {
                                $openingCredit -= $value['creditNotePaymentAmount'];
                            } else {
                                $openingCredit = 0;
                            }
                        } else if ($val == "invoice") {
                            $openingCredit += $value['incomingPaymentBalance'];
                            if (!is_null($value['incomingPaymentCreditAmount'])) {
                                $openingCredit -= $value['incomingPaymentCreditAmount'];
                                $openingOutstanding -= $value['incomingPaymentCreditAmount'];
                            }
                            if ($value['incomingPaymentPaidAmount'] != 0) {
                                if ($openingOutstanding >= $value['incomingPaymentAmount']) {
                                    $openingOutstanding -= $value['incomingPaymentAmount'];
                                } else {
                                    $creditDiffForInvPayment = $value['incomingPaymentAmount'] - $openingOutstanding;
                                    $openingOutstanding = 0;
                                    $openingCredit += $creditDiffForInvPayment;
                                }
                            }
                        } else if ($val == "advance") {
                            $openingCredit += $value['incomingPaymentAmount'];
                        } else if ($val == "advance") {
                            $openingCredit += $value['incomingPaymentAmount'];
                        } else if ($ke == "incomeID") {
                            $openingOutstanding += $value['incomeTotal'];
                        } else if ($val == "income") {
                            $openingCredit += $value['incomingPaymentBalance'];
                            if (!is_null($value['incomingPaymentCreditAmount'])) {
                                $openingCredit -= $value['incomingPaymentCreditAmount'];
                                $openingOutstanding -= $value['incomingPaymentCreditAmount'];
                            }
                            if ($value['incomingPaymentPaidAmount'] != 0) {
                                if ($openingOutstanding >= $value['incomingPaymentAmount']) {
                                    $openingOutstanding -= $value['incomingPaymentAmount'];
                                } else {
                                    $creditDiffForInvPayment = $value['incomingPaymentAmount'] - $openingOutstanding;
                                    $openingOutstanding = 0;
                                    $openingCredit += $creditDiffForInvPayment;
                                }
                            }
                        }
                    }
                }
            }
            $customerData[$customerValue['customerID']]['customerOpeningOutstanding'] = $openingOutstanding;
            $customerData[$customerValue['customerID']]['customerOpeningCredit'] = $openingCredit;

        }

        return $customerData;
    }

    /**
     * This function is used generate customer aged analysis report for done payments
     * @param JSON Request
     * @return JSONRespondHtml
     */
    public function generateCustomerAgedAnalysisForDonePaymentsReportAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $cusIds = $request->getPost("cusIds");
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $advanceRow = $request->getPost('advanceDataFlag');
            $translator = new Translator();
            $name = $translator->translate('Customer Aged Analysis For done payments Report');

            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }
            if($advanceDetails){
                $agedCusData = $this->getAdvanceAgedCustomerDetailsForDonePayments($cusIds, $isAllCustomers, $cusCategory);
            } else {
                $agedCusData = $this->getAgedCustomerDataAnalysisForDonePayments($cusIds, $isAllCustomers, $cusCategory);
            }
                        
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $agedCustomerView = new ViewModel(array(
                'aCD' => $agedCusData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
                'advanceDetails' => $advanceDetails,
            ));
            $agedCustomerView->setTemplate('reporting/customer-report/generate-aged-cus-for-done-payment-pdf');

            $this->html = $agedCustomerView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @param JSON Request
     * @return $pdf
     */
    public function generateAgedCusForDonePaymentPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $advanceRow = $request->getPost('advanceData');
            $translator = new Translator();
            $name = $translator->translate('Customer Aged Analysis For Done Payment Report');
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }

            if($advanceDetails){
                $agedCusData = $this->getAdvanceAgedCustomerDetailsForDonePayments($cusIds, $isAllCustomers, $cusCategory);
            } else {
                $agedCusData = $this->getAgedCustomerDataAnalysisForDonePayments($cusIds, $isAllCustomers, $cusCategory);
            }

            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $agedCustomerView = new ViewModel(array(
                'aCD' => $agedCusData,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'cusCategory' => ($cusCategory) ? 1 : 0,
                'advanceDetails' => $advanceDetails,
            ));

            $agedCustomerView->setTemplate('reporting/customer-report/generate-aged-cus-for-done-payment-pdf');
            $agedCustomerView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($agedCustomerView);
            $pdfPath = $this->downloadPDF('customer-aged-for-done-payment-analysis-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * Generate Aged Customer Analysis sheet
     * @return csv
     */
    public function generateAgedCusForDonePaymentSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cusIds = $request->getPost('cusIds');
            $isAllCustomers = $request->getPost("isAllCustomers");
            $cusCategory = $request->getPost('cusCategory');
            $advanceRow = $request->getPost('advanceData');
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }

            if($advanceDetails){
                $agedCusData = $this->getAdvanceAgedCustomerDetailsForDonePayments($cusIds, $isAllCustomers, $cusCategory);
            } else {
                $agedCusData = $this->getAgedCustomerDataAnalysisForDonePayments($cusIds, $isAllCustomers, $cusCategory);
            }
            $cD = $this->getCompanyDetails();
            if ($agedCusData) {
                $title = '';
                $tit = 'CUSTOMER AGED ANALYSIS FOR DONE PAYMENTS REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => $tit,
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-m-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                if($advanceDetails){
                    $arrs = array("CUSTOMER NAME", "CUSTOMER CODE", "1 - 30 DAYS", "31 - 60 DAYS","61 - 90 DAYS", "91 - 180 DAYS","181 - 270 DAYS", "271 - 365 DAYS", "> 365", "TOTAL");
                } else {
                    $arrs = array("CUSTOMER NAME", "CUSTOMER CODE", "WITHIN 7 DAYS", "WITHIN 14 DAYS", "WITHIN 30 DAYS", "OVER 30 DAYS", "TOTAL");
                    
                }

                $header = implode(",", $arrs);
                $arr = '';
                $str = array();

                if (sizeof($agedCusData) == 0) {
                    $in = "no matching records found";
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $withIn7Total = 0.00;
                    $withIn14Total = 0.00;
                    $withIn30Total = 0.00;
                    $withIn60Total = 0.00;
                    $withIn90Total = 0.00;
                    $withIn180Total = 0.00;
                    $withIn270Total = 0.00;
                    $withIn365Total = 0.00;
                    $over365Total = 0.00;
                    $over30Total = 0.00;
                    $payTotal = 0.00;
                    foreach ($agedCusData as $a) {
                        if (!empty($a)) {
                            $refNo = $a['incomingPaymentCode'];
                            $date = $a['incomingPaymentDate'];
                            $withIn7 = $a['withIn7Days'];
                            $withIn7 = $withIn7 ? $withIn7 : 0;
                            $withIn7Total += $withIn7;
                            $withIn14 = $a['withIn14Days'];
                            $withIn14 = $withIn14 ? $withIn14 : 0;
                            $withIn14Total += $withIn14;
                            $withIn30 = $a['withIn30Days'];
                            $withIn30 = $withIn30 ? $withIn30 : 0;
                            $withIn30Total += $withIn30;
                            if($advanceDetails){
                                $withIn60 = $a['withIn60Days'];
                                $withIn60 = $withIn60 ? $withIn60 : 0;
                                $withIn60Total += $withIn60;

                                $withIn90 = $a['withIn90Days'];
                                $withIn90 = $withIn90 ? $withIn90 : 0;
                                $withIn90Total += $withIn90;

                                $withIn180 = $a['withIn180Days'];
                                $withIn180 = $withIn180 ? $withIn180 : 0;
                                $withIn180Total += $withIn180;

                                $withIn270 = $a['withIn270Days'];
                                $withIn270 = $withIn270 ? $withIn270 : 0;
                                $withIn270Total += $withIn270;

                                $withIn365 = $a['withIn365Days'];
                                $withIn365 = $withIn365 ? $withIn365 : 0;
                                $withIn365Total += $withIn365;

                                $over365 = $a['over365'];
                                $over365 = $over365 ? $over365 : 0;
                                $over365Total += $over365;
                                $Total = $withIn30 + $withIn60 + $withIn90 + $withIn180 + $withIn270 + $withIn365 + $over365;
                            } else {
                                $over30 = $a['over30'];
                                $over30 = $over30 ? $over30 : 0;
                                $over30Total += $over30;
                                $Total = $withIn7 + $withIn14 + $withIn30 + $over30;
                                
                            }

                            $payTotal += $Total;
                            if ($Total > 1) {
                                if($advanceDetails){
                                    $in = array(
                                    0 => str_replace(",", " ", $a['cT'] . ' ' . $a['cN']),
                                    1 => $a['cusCD'],
                                    2 => $withIn30,
                                    3 => $withIn60,
                                    4 => $withIn90,
                                    5 => $withIn180,
                                    6 => $withIn270,
                                    7 => $withIn365,
                                    8 => $over365,
                                    9 => $Total
                                );    
                                } else {
                                    $in = array(
                                        0 => str_replace(",", " ", $a['cT'] . ' ' . $a['cN']),
                                        1 => $a['cusCD'],
                                        2 => $withIn7,
                                        3 => $withIn14,
                                        4 => $withIn30,
                                        5 => $over30,
                                        6 => $Total
                                    );
                                    
                                }
                                $arr.=implode(",", $in) . "\n";
                            }
                            
                        }
                    }
                    
                    if($advanceDetails){
                        $in = array(
                        0 => "",
                        1 => "Total Payments",
                        2 => $withIn30Total,
                        3 => $withIn60Total,
                        4 => $withIn90Total,
                        5 => $withIn180Total,
                        6 => $withIn270Total,
                        7 => $withIn365Total,
                        8 => $over365Total,
                        9 => $payTotal
                    );    
                    } else {
                        $in = array(
                            0 => "",
                            1 => "Total Payments",
                            2 => $withIn7Total,
                            3 => $withIn14Total,
                            4 => $withIn30Total,
                            5 => $over30Total,
                            6 => $payTotal
                        );
                        
                    }
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = 'no matching records found\n';
            }
        }
        $name = "customer-aged-analysis-for-done-payments";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }


    /**
     * @param customer IDs and Customer category
     * @return $agedData
     */
    public function getAgedCustomerDataAnalysisForDonePayments($cusIds = null, $isAllCustomers = false, $cusCategory = null)
    {
        if (isset($cusIds)) {
            $customerDetails = array();
            $endDate = $this->convertDateToStandardFormat(date("Y-m-d"));

            $agedCustomerDeatails = $this->CommonTable('Invoice\Model\PaymentsTable')->getAgedCustomerForDonePayments($cusIds, $endDate, $isAllCustomers, $cusCategory);

            foreach ($agedCustomerDeatails as $c) {
                if ($c['WithIn7Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['WithIn7Days'] += $c['incomingPaymentCreditAmount'];
                } else if ($c['WithIn14Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['WithIn14Days'] += $c['incomingPaymentCreditAmount'];
                } else if ($c['WithIn30Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['WithIn30Days'] += $c['incomingPaymentCreditAmount'];
                } else if ($c['Over30'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['Over30'] += $c['incomingPaymentCreditAmount'];
                }

                $withIn7Days = is_null($c['WithIn7Days']) ? 0 : $c['WithIn7Days'];
                $withIn14Days = is_null($c['WithIn14Days']) ? 0 : $c['WithIn14Days'];
                $withIn30Days = is_null($c['WithIn30Days']) ? 0 : $c['WithIn30Days'];
                $over30 = is_null($c['Over30']) ? 0 : $c['Over30'] ;
                
                if (empty($customerDetails[$c['customerID']])) {
                    $withIn7DaysTotal = 0;
                    $withIn14DaysTotal = 0;
                    $withIn30DaysTotal = 0;
                    $over30Total = 0;
                } else {
                    //if already have customerID related data on $customerDetails array
                    //get previous value from $customerDetails array to calculations

                    $withIn7DaysTotal = $customerDetails[$c['customerID']]['withIn7Days'];
                    $withIn14DaysTotal = $customerDetails[$c['customerID']]['withIn14Days'];
                    $withIn30DaysTotal = $customerDetails[$c['customerID']]['withIn30Days'];
                    $over30Total = $customerDetails[$c['customerID']]['over30'];
                }


                $withIn7DaysTotal += $withIn7Days;
                $withIn14DaysTotal += $withIn14Days;
                $withIn30DaysTotal += $withIn30Days;
                $over30Total += $over30;
                $customerDetails[$c['customerID']]['cT'] = $c['customerTitle'];
                $customerDetails[$c['customerID']]['cN'] = $c['customerName'];
                $customerDetails[$c['customerID']]['cusCD'] = $c['customerCode'];
                $customerDetails[$c['customerID']]['withIn7Days'] = $withIn7DaysTotal;
                $customerDetails[$c['customerID']]['withIn14Days'] = $withIn14DaysTotal;
                $customerDetails[$c['customerID']]['withIn30Days'] = $withIn30DaysTotal;
                $customerDetails[$c['customerID']]['over30'] = $over30Total;
                $customerDetails[$c['customerID']]['customerCategoryName'] = $c['customerCategoryName'];
            }

            return $customerDetails;
        }
    }

    public function getAdvanceAgedCustomerDetailsForDonePayments($cusIds = null, $isAllCustomers = false, $cusCategory = null)
    {
        
        if (isset($cusIds)) {
            $customerDetails = array();
            $endDate = $this->convertDateToStandardFormat(date("Y-m-d"));
            $agedCustomerDeatails = $this->CommonTable('Invoice\Model\PaymentsTable')->getAgedCusForDonePaymentAdvanceData($cusIds, $endDate, $isAllCustomers, $cusCategory);
            
            foreach ($agedCustomerDeatails as $c) {
                if ($c['WithIn30Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['WithIn30Days'] += $c['incomingPaymentCreditAmount'];
                } else if ($c['WithIn60Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['WithIn60Days'] += $c['incomingPaymentCreditAmount'];
                } else if ($c['WithIn90Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['WithIn90Days'] += $c['incomingPaymentCreditAmount'];
                } else if ($c['WithIn180Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['WithIn180Days'] += $c['incomingPaymentCreditAmount'];
                } else if ($c['WithIn270Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['WithIn270Days'] += $c['incomingPaymentCreditAmount'];
                } else if ($c['WithIn365Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['WithIn365Days'] += $c['incomingPaymentCreditAmount'];
                } else if ($c['Over365'] != null && $c['incomingPaymentCreditAmount'] != null) {
                    $c['Over365'] += $c['incomingPaymentCreditAmount'];
                }

                $withIn30Days = is_null($c['WithIn30Days']) ? 0 : $c['WithIn30Days'];
                $withIn60Days = is_null($c['WithIn60Days']) ? 0 : $c['WithIn60Days'];
                $withIn90Days = is_null($c['WithIn90Days']) ? 0 : $c['WithIn90Days'];
                $withIn180Days = is_null($c['WithIn180Days']) ? 0 : $c['WithIn180Days'];
                $withIn270Days = is_null($c['WithIn270Days']) ? 0 : $c['WithIn270Days'];
                $withIn365Days = is_null($c['WithIn365Days']) ? 0 : $c['WithIn365Days'];
                $over365 = is_null($c['Over365']) ? 0 : $c['Over365'];

                //if first time of customerID coming from $customerDetails array
                if (empty($customerDetails[$c['customerID']])) {
                    $withIn30DaysTotal = 0;
                    $withIn60DaysTotal = 0;
                    $withIn90DaysTotal = 0;
                    $withIn180DaysTotal = 0;
                    $withIn270DaysTotal = 0;
                    $withIn365DaysTotal = 0;
                    $over365Total = 0;
                } else {
                    //if already have customerID related data on $customerDetails array
                    //get previous value from $customerDetails array to calculations
                    $withIn30DaysTotal = $customerDetails[$c['customerID']]['withIn30Days'];
                    $withIn60DaysTotal = $customerDetails[$c['customerID']]['withIn60Days'];
                    $withIn90DaysTotal = $customerDetails[$c['customerID']]['withIn90Days'];
                    $withIn180DaysTotal = $customerDetails[$c['customerID']]['withIn180Days'];
                    $withIn270DaysTotal = $customerDetails[$c['customerID']]['withIn270Days'];
                    $withIn365DaysTotal = $customerDetails[$c['customerID']]['withIn365Days'];
                    $over365Total = $customerDetails[$c['customerID']]['over365'];
                }
                    

                $withIn30DaysTotal += $withIn30Days;
                $withIn60DaysTotal += $withIn60Days;
                $withIn90DaysTotal += $withIn90Days;
                $withIn180DaysTotal += $withIn180Days;
                $withIn270DaysTotal += $withIn270Days;
                $withIn365DaysTotal += $withIn365Days;
                $over365Total += $over365;

                $customerDetails[$c['customerID']]['cT'] = $c['customerTitle'];
                $customerDetails[$c['customerID']]['cN'] = $c['customerName'];
                $customerDetails[$c['customerID']]['cusCD'] = $c['customerCode'];
                $customerDetails[$c['customerID']]['withIn30Days'] = $withIn30DaysTotal;
                $customerDetails[$c['customerID']]['withIn60Days'] = $withIn60DaysTotal;
                $customerDetails[$c['customerID']]['withIn90Days'] = $withIn90DaysTotal;
                $customerDetails[$c['customerID']]['withIn180Days'] = $withIn180DaysTotal;
                $customerDetails[$c['customerID']]['withIn270Days'] = $withIn270DaysTotal;
                $customerDetails[$c['customerID']]['withIn365Days'] = $withIn365DaysTotal;
                $customerDetails[$c['customerID']]['over365'] = $over365Total;
                $customerDetails[$c['customerID']]['customerCategoryName'] = $c['customerCategoryName'];
                    
            }

            return $customerDetails;
        }
    }

}
