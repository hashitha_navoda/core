<?php

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Account Report Api related actions
 */
class AccountReportController extends CoreController
{

    public function generateAccountTransactionReportAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();

            $accountId = $postData['accountId'];
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $transactionType = $postData['transactionType'];
            $period = ($postData['fromDate'] && $postData['toDate']) ? $postData['fromDate'] . ' - ' . $postData['toDate'] : 'All time';
            $dataList = [];

            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
            $acountDetails = $account['accountName'] . ' (' . $account['accountNumber'] . ' )';

            $result = $this->getTransactions($accountId, $account['financeAccountID'], $transactionType, $toDate, $fromDate);
            $dataList[$acountDetails] = $result;

            $headerView = $this->headerViewTemplate("Account Transaction Report", $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'transcationData' => $dataList,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/account-report/generate-account-transaction-report');

            $this->status = true;
            $this->msg = 'Account Transaction report generated';
            $this->html = $view;
        }

        return $this->JSONRespondHtml();
    }

    public function generateAccountTransactionPdfAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();

            $accountId = $postData['accountId'];
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $transactionType = $postData['transactionType'];
            $period = ($postData['fromDate'] && $postData['toDate']) ? $postData['fromDate'] . ' - ' . $postData['toDate'] : 'All time';
            $dataList = [];

            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
            $acountDetails = $account['accountName'] . ' (' . $account['accountNumber'] . ' )';

            $result = $this->getTransactions($accountId, $account['financeAccountID'], $financeAccountId, $transactionType, $toDate, $fromDate);
            $dataList[$acountDetails] = $result;

            $headerView = $this->headerViewTemplate("Account Transaction Report", $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'transcationData' => $dataList,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/account-report/generate-account-transaction-report');
            $view->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($view);

            $pdfPath = $this->downloadPDF('account_transaction_report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;

            return $this->JSONRespond();
        }
    }

    public function generateAccountTransactionCsvAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();

            $accountId = $postData['accountId'];
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $transactionType = $postData['transactionType'];
            $period = ($postData['fromDate'] && $postData['toDate']) ? $postData['fromDate'] . ' - ' . $postData['toDate'] : 'All time';
            $companyDetails = $this->getCompanyDetails();
            $dataList = [];

            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
            $acountDetails = $account['accountName'] . ' (' . $account['accountNumber'] . ' )';

            $result = $this->getTransactions($accountId, $account['financeAccountID'], $transactionType, $toDate, $fromDate);
            $dataList[$acountDetails] = $result;

            $data = array(
                "Account Transaction Report"
            );
            $title.=implode(",", $data) . "\n";

            $data = array(
                'Report Generated: ' . date('Y-M-d h:i:s a')
            );
            $title.=implode(",", $data) . "\n";

            $data = array(
                $companyDetails[0]->companyName,
                $companyDetails[0]->companyAddress,
                "Tel: " . $companyDetails[0]->telephoneNumber,
                "Period : " . $period
            );

            $title.=implode(",", $data) . "\n";

            $data = array(
                "Period :" . $postData['fromDate'] . " - " . $postData['toDate']
            );
            $title.=implode(",", $data) . "\n";

            $tableHead = array('Account', 'Cheque Reference', 'Customer/Supplier Account Number', 'Bank Name', 'Amount', 'Date', 'Transaction Type');
            $header.=implode(",", $tableHead) . "\n";

            if (!empty($dataList)) {
                foreach ($dataList as $account => $transactions) {
                    $data = array($account);
                    $arr.=implode(",", $data) . "\n";
                    foreach ($transactions as $transaction) {
                        $data = array(
                            '',
                            $transaction['refNum'],
                            $transaction['account'],
                            $transaction['bank'],
                            number_format($transaction['amount'], 2, '.', ''),
                            $transaction['date'],
                            $transaction['type']
                        );
                        $arr.=implode(",", $data) . "\n";
                    }
                }
            } else {
                $arr = "no matching records found";
            }

            $name = "account_transaction.csv";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getTransactions($accountId, $financeAccountId, $transactionType, $toDate = null, $fromDate = null)
    {
        $transactions = $depositedCheques = $issuedCheques = $adPayissuedCheques = $bankOutgoingTransfer = $adPaybankOutgoingTransfer = $bankIncomingTransfer = $cash = $issuedChequesByFinanceAccount = $adPayissuedChequesByGlAccountId = $bankOutgoingTransferByGlAccount = $adPaybankOutgoingTransferByGlAccount = $outgoingAdCashByGlAccount = $OutgoingCash = $withdrawalsByGlAccountId = $depositsByGlAccountId = array();
        //get deposited cheques
        if (empty($transactionType) || $transactionType == 1) {
            $depositedCheques = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getDepositedChequeByAccountId($accountId, null, $fromDate, $toDate);
            $depositedCheques = array_map(function($transaction) {
                $transaction['refNum'] = $transaction['incomingPaymentMethodChequeNumber'];
                $transaction['account'] = '-';
                $transaction['bank'] = $transaction['incomingPaymentMethodChequeBankName'];
                $transaction['date'] = $transaction['chequeDepositDate'];
                $transaction['type'] = 'Received Cheque';
                $transaction['amount'] = $transaction['incomingPaymentMethodAmount']/$transaction['chequeDepositAccountCurrencyRate'];
                return $transaction;
            }, iterator_to_array($depositedCheques));

            //get invoice payment issued cheques
            $issuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId($accountId, null, $fromDate, $toDate);
            $issuedCheques = array_map(function($transaction) {
                $transaction['refNum'] = $transaction['outGoingPaymentMethodReferenceNumber'];
                $transaction['account'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Issued Cheque';
                $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                return $transaction;
            }, iterator_to_array($issuedCheques));

            //get invoice payment issued cheques by finacinal account id
            $issuedChequesByFinanceAccount = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByfinancialAccountId($financeAccountId, null, $fromDate, $toDate);
            $issuedChequesByFinanceAccount = array_map(function($transaction) {
                $transaction['refNum'] = $transaction['outGoingPaymentMethodReferenceNumber'];
                $transaction['account'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Issued Cheque';
                $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                return $transaction;
            }, iterator_to_array($issuedChequesByFinanceAccount));

            //get advanced payment issued cheques
            $adPayissuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId($accountId, null, $fromDate, $toDate);
            $adPayissuedCheques = array_map(function($transaction) {
                $transaction['refNum'] = $transaction['outGoingPaymentMethodReferenceNumber'];
                $transaction['account'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Issued Cheque';
                $transaction['amount'] = $transaction['outgoingPaymentAmount'];
                return $transaction;
            }, iterator_to_array($adPayissuedCheques));

            //get advanced payment issued cheques by gl account id
            $adPayissuedChequesByGlAccountId = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByGlAccountId($financeAccountId, null, $fromDate, $toDate);
            $adPayissuedChequesByGlAccountId = array_map(function($transaction) {
                $transaction['refNum'] = $transaction['outGoingPaymentMethodReferenceNumber'];
                $transaction['account'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Issued Cheque';
                $transaction['amount'] = $transaction['outgoingPaymentAmount'];
                return $transaction;
            }, iterator_to_array($adPayissuedChequesByGlAccountId));
        }

        //get issued cheques
        if (empty($transactionType) || $transactionType == 2) {
            //get invoice outgoing bank transfer
            $bankOutgoingTransfer = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getOutgoingBankTransfersByAccountId($accountId, null, $fromDate, $toDate);
            $bankOutgoingTransfer = array_map(function($transaction) {
                $transaction['account'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber'];
                $transaction['refNum'] = '-';
                $transaction['bank'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierBankName']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierBankName'];
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Outgoing Transfer';
                $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                return $transaction;
            }, iterator_to_array($bankOutgoingTransfer));
            
            //get invoice outgoing bank transfer by gl account
            $bankOutgoingTransferByGlAccount = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getOutgoingBankTransfersByGlAccountId($financeAccountId, null, $fromDate, $toDate);
            $bankOutgoingTransferByGlAccount = array_map(function($transaction) {
                $transaction['account'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber'];
                $transaction['refNum'] = '-';
                $transaction['bank'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierBankName']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierBankName'];
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Outgoing Transfer';
                $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                return $transaction;
            }, iterator_to_array($bankOutgoingTransferByGlAccount));

            //get advanced payment bank transfer
            $adPaybankOutgoingTransfer = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentOutgoingBankTransfersByAccountId($accountId, null, $fromDate, $toDate);
            $adPaybankOutgoingTransfer = array_map(function($transaction) {
                $transaction['account'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber'];
                $transaction['refNum'] = '-';
                $transaction['bank'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierBankName']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierBankName'];
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Outgoing Transfer';
                $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                return $transaction;
            }, iterator_to_array($adPaybankOutgoingTransfer));

            //get advanced payment bank transfer by gl account
            $adPaybankOutgoingTransferByGlAccount = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentOutgoingBankTransfersByGlAccountId($financeAccountId, null, $fromDate, $toDate);
            $adPaybankOutgoingTransferByGlAccount = array_map(function($transaction) {
                $transaction['account'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber'];
                $transaction['refNum'] = '-';
                $transaction['bank'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierBankName']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierBankName'];
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Outgoing Transfer';
                $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                return $transaction;
            }, iterator_to_array($adPaybankOutgoingTransferByGlAccount));

            //get incoming bank transfer
            $bankIncomingTransfer = $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->getBankTransfersByAccountId($accountId, null, $fromDate, $toDate);
            $bankIncomingTransfer = array_map(function($transaction) {
                $transaction['account'] = empty($transaction['incomingPaymentMethodBankTransferCustomerAccountNumber']) ? '-' : $transaction['incomingPaymentMethodBankTransferCustomerAccountNumber'];
                $transaction['refNum'] = '-';
                $transaction['bank'] = empty($transaction['incomingPaymentMethodBankTransferCustomerBankName']) ? '-' : $transaction['incomingPaymentMethodBankTransferCustomerBankName'];
                $transaction['date'] = $transaction['incomingPaymentDate'];
                $transaction['type'] = 'Incoming Transfer';
                $transaction['amount'] = $transaction['incomingPaymentMethodAmount'];
                return $transaction;
            }, iterator_to_array($bankIncomingTransfer));
        }

        //cash
        if (empty($transactionType) || $transactionType == 3) {
            $cash = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getPettyCashFloatByCashType(2, $accountId, $fromDate, $toDate);
            $cash = array_map(function($transaction) {
                $transaction['account'] = '-';
                $transaction['refNum'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['pettyCashFloatDate'];
                $transaction['type'] = 'Cash';
                $transaction['amount'] = $transaction['pettyCashFloatAmount'];
                return $transaction;
            }, iterator_to_array($cash));

            //outgoing advanced payment by gl account id
            $outgoingAdCashByGlAccount = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentCashByGlAccountId($financeAccountId, null, $fromDate, $toDate);
            $outgoingAdCashByGlAccount = array_map(function($transaction) {
                $transaction['account'] = empty($transaction['incomingPaymentMethodBankTransferCustomerAccountNumber']) ? '-' : $transaction['incomingPaymentMethodBankTransferCustomerAccountNumber'];
                $transaction['refNum'] = '-';
                $transaction['bank'] = empty($transaction['incomingPaymentMethodBankTransferCustomerBankName']) ? '-' : $transaction['incomingPaymentMethodBankTransferCustomerBankName'];
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Cash';
                $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                return $transaction;
            }, iterator_to_array($outgoingAdCashByGlAccount));

            //get invoice outgoing bank transfer
            $OutgoingCash = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getOutgoingCashByGlAccountId($financeAccountId, null, $fromDate, $toDate);
            $OutgoingCash = array_map(function($transaction) {
                $transaction['account'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierAccountNumber'];
                $transaction['refNum'] = '-';
                $transaction['bank'] = empty($transaction['outGoingPaymentMethodBankTransferSupplierBankName']) ? '-' : $transaction['outGoingPaymentMethodBankTransferSupplierBankName'];
                $transaction['date'] = $transaction['outgoingPaymentDate'];
                $transaction['type'] = 'Cash';
                $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                return $transaction;
            }, iterator_to_array($OutgoingCash));
        }

        //withdrawals
        $withdrawals = array();
        if (empty($transactionType) || $transactionType == 4) {
            $withdrawals = $this->CommonTable('Expenses\Model\AccountWithdrawalTable')->getWithdrawalsByAccountId($accountId, $fromDate, $toDate);
            $withdrawals = array_map(function($transaction) {
                $transaction['account'] = '-';
                $transaction['refNum'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['accountWithdrawalDate'];
                $transaction['type'] = 'Withdrawal';
                $transaction['amount'] = $transaction['accountWithdrawalAmount'];
                return $transaction;
            }, iterator_to_array($withdrawals));

            //withdrawal by gl account id
            $withdrawalsByGlAccountId = $this->CommonTable('Expenses\Model\AccountWithdrawalTable')->getWithdrawalsByGlAccountId($financeAccountId, $fromDate, $toDate);
            $withdrawalsByGlAccountId = array_map(function($transaction) {
                $transaction['account'] = '-';
                $transaction['refNum'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['accountWithdrawalDate'];
                $transaction['type'] = 'Withdrawal';
                $transaction['amount'] = $transaction['accountWithdrawalAmount'];
                return $transaction;
            }, iterator_to_array($withdrawalsByGlAccountId));
        }

        //deposits
        $deposits = array();
        if (empty($transactionType) || $transactionType == 5) {
            $deposits = $this->CommonTable('Expenses\Model\AccountDepositTable')->getDepositsByAccountId($accountId, $fromDate, $toDate);
            $deposits = array_map(function($transaction) {
                $transaction['account'] = '-';
                $transaction['refNum'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['accountDepositDate'];
                $transaction['type'] = 'Deposit';
                $transaction['amount'] = $transaction['accountDepositAmount'];
                return $transaction;
            }, iterator_to_array($deposits));

            $depositsByGlAccountId = $this->CommonTable('Expenses\Model\AccountDepositTable')->getDepositsByGlAccountId($financeAccountId, $fromDate, $toDate);
            $depositsByGlAccountId = array_map(function($transaction) {
                $transaction['account'] = '-';
                $transaction['refNum'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['accountDepositDate'];
                $transaction['type'] = 'Deposit';
                $transaction['amount'] = $transaction['accountDepositAmount'];
                return $transaction;
            }, iterator_to_array($depositsByGlAccountId));
        }

        //transfer
        $inTransfer = $outTransfer = array();
        if (empty($transactionType) || $transactionType == 6) {
            $inTransfer = $this->CommonTable('Expenses\Model\AccountTransferTable')->getIncomingBankTransfersByAccountId($financeAccountId, $fromDate, $toDate);
            $inTransfer = array_map(function($transaction) {
                $transaction['account'] = '-';
                $transaction['refNum'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['accountTransferDate'];
                $transaction['type'] = 'Incoming Account Transfer';
                $transaction['amount'] = $transaction['accountTransferAmount'];
                return $transaction;
            }, iterator_to_array($inTransfer));

            $outTransfer = $this->CommonTable('Expenses\Model\AccountTransferTable')->getOutgoingBankTransfersByAccountId($financeAccountId, $fromDate, $toDate);
            $outTransfer = array_map(function($transaction) {
                $transaction['account'] = '-';
                $transaction['refNum'] = '-';
                $transaction['bank'] = '-';
                $transaction['date'] = $transaction['accountTransferDate'];
                $transaction['type'] = 'Outgoing Account Transfer';
                $transaction['amount'] = $transaction['accountTransferAmount'];
                return $transaction;
            }, iterator_to_array($outTransfer));
        }

        return array_merge(
                $transactions, $depositedCheques, $issuedCheques, $issuedChequesByFinanceAccount, $adPayissuedCheques, $adPayissuedChequesByGlAccountId, $bankOutgoingTransfer, $bankOutgoingTransferByGlAccount, $adPaybankOutgoingTransfer, $adPaybankOutgoingTransferByGlAccount, $bankIncomingTransfer, $cash, $outgoingAdCashByGlAccount, $OutgoingCash, $withdrawals, $withdrawalsByGlAccountId, $deposits, $depositsByGlAccountId, $inTransfer, $outTransfer
        );
    }

}
