<?php
namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;

class IncomeExpenseReportController extends CoreController
{

    public function incomeExpenseReportViewAction()
    {

    	if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $dimensionType = $postData['dimensionType'];
        $dimensionValues = $postData['dimensionValues'];
        $fromDate = $postData['fromDate'];
        $toDate = $postData['toDate'];
        $summery = ($postData['isSummery'] == "true") ? true :false;

        $incomeExpenseData = $this->getService('IncomeExpenseReportService')->getIncomeExpenseData($fromDate, $toDate, $dimensionType, $dimensionValues, $summery);
        $cD = $this->getCompanyDetails();
        
        $translator = new Translator();
        $name = $translator->translate('Dimension Wise Income & Expense Summery Report');
        $period = $fromDate . ' - ' . $toDate;
        
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $incomeExpenseDataView = new ViewModel(array(
            'dataSet' => $incomeExpenseData['data'],
            'headers' => $incomeExpenseData['headers'],
            'dimensionType' => $incomeExpenseData['dimensionType'],
            'isSummery' => $summery,
            'headerTemplate' => $headerViewRender,
            'cD' => $companyDetails)
        );
        
        $incomeExpenseDataView->setTemplate('reporting/income-expense-report/generate-income-expense-view');

        $this->html = $incomeExpenseDataView;
        $this->status = true;
        return $this->JSONRespondHtml();
    }

    public function incomeExpenseReportPdfAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
           
        $postData = $this->getRequest()->getPost()->toArray();
        $dimensionType = $postData['dimensionType'];
        $dimensionValues = $postData['dimensionValues'];
        $fromDate = $postData['fromDate'];
        $toDate = $postData['toDate'];
        $summery = ($postData['isSummery'] == "true") ? true :false;

        $incomeExpenseData = $this->getService('IncomeExpenseReportService')->getIncomeExpenseData($fromDate, $toDate, $dimensionType, $dimensionValues, $summery);
        $cD = $this->getCompanyDetails();

        $translator = new Translator();
        $name = $translator->translate('Dimension Wise Income & Expense Summery Report');
        $period = $fromDate . ' - ' . $toDate;

        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $incomeExpenseDataView = new ViewModel(array(
            'dataSet' => $incomeExpenseData['data'],
            'headers' => $incomeExpenseData['headers'],
            'dimensionType' => $incomeExpenseData['dimensionType'],
            'isSummery' => $summery,
            'headerTemplate' => $headerViewRender,
            'cD' => $companyDetails)
        );

        $incomeExpenseDataView->setTemplate('reporting/income-expense-report/generate-income-expense-view');
        $incomeExpenseDataView->setTerminal(true);

        // get rendered view into variable
        $htmlContent = $this->viewRendererHtmlContent($incomeExpenseDataView);
        $pdfPath = $this->downloadPDF('income-expense-report', $htmlContent, true, true);
        
        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function incomeExpenseReportSheetAction() 
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
           
            $postData = $this->getRequest()->getPost()->toArray();
            $dimensionType = $postData['dimensionType'];
            $dimensionValues = $postData['dimensionValues'];
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $summery = ($postData['isSummery'] == "true") ? true :false;

            $incomeExpenseData = $this->getService('IncomeExpenseReportService')->getIncomeExpenseData($fromDate, $toDate, $dimensionType, $dimensionValues, $summery, true);


            if ($incomeExpenseData['data']) {
                $period = $fromDate . ' - ' . $toDate;
                $companyDetails = $this->getCompanyDetails();
                $data = array(
                    "Dimension Wise Income & Expense Summery Report"
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber,
                    "Period : " . $period
                );

                $title.=implode(",", $data) . "\n";

                $data = array(
                    "Period :" . $period
                );
                $title.=implode(",", $data) . "\n";
                $arrs = ["Dimension Type","GL Account Type","GL Account Name"];

                foreach ($incomeExpenseData['data'] as $key => $value) {
                    
                    foreach ($incomeExpenseData['headers'][$key] as $key1 => $value1) {
                        array_push($arrs, $value1['dimensionValueName']);
                    }
                }
                array_push($arrs, "Total");

                $header = implode(",", $arrs);
                $arr = '';

                $temp1=[
                    0 => $incomeExpenseData['dimensionType'],
                    1 => '',
                    2 => ''
                ];

                foreach ($incomeExpenseData['headers']['table_1'] as $key11 => $value11) {
                    array_push($temp1, '');
                }
                array_push($temp1, '');

                $arr .= implode(",", $temp1). "\n";


                $temp2=[
                    0 => '',
                    1 => 'Income GL Accounts',
                    2 => ''
                ];

                foreach ($incomeExpenseData['headers']['table_1'] as $key12 => $value12) {
                    array_push($temp2, '');
                }
                array_push($temp2, '');

                $arr .= implode(",", $temp2). "\n";

                if (!$summery) {
                    $incomColTotal = [];
                    foreach ($incomeExpenseData['data']['table_1']['Income'] as $key13 => $value13) {
                        $incRowTotal = 0; 
                        $temp3=[
                            0 => '',
                            1 => ''
                        ];
                        $accNme= $value13['accName'];
                        unset($value13['accName']);
                        array_push($temp3, $accNme);

                        foreach ($incomeExpenseData['headers']['table_1'] as $key15 => $val15) {
                            $incRowTotal += $value13[$val15['dimensionValueID']];
                            $incomColTotal[$val15['dimensionValueID']] = $incomColTotal[$val15['dimensionValueID']] + $value13[$val15['dimensionValueID']];
                            array_push($temp3, $value13[$val15['dimensionValueID']]);
                        }
                        $incomColTotal['tot'] = $incomColTotal['tot'] + $incRowTotal;
                        array_push($temp3, $incRowTotal);
                        $arr .= implode(",", $temp3). "\n";
                    }
                } else {
                    $incomColTotal = [];
                    foreach ($incomeExpenseData['data']['table_1']['Income'] as $rowKey => $val) {
                        $incRowTotal = 0;
                        foreach ($incomeExpenseData['headers']['table_1'] as $key5 => $val2) {
                            $incRowTotal += $val[$val2['dimensionValueID']];
                            $incomColTotal[$val2['dimensionValueID']] = $incomColTotal[$val2['dimensionValueID']] + $val[$val2['dimensionValueID']];

                        }
                        $incomColTotal['tot'] = $incomColTotal['tot'] + $incRowTotal;
                    }
                }


                $temp4=[
                    0 => '',
                    1 => '',
                    2 => 'Total'
                ];

                foreach ($incomeExpenseData['headers']['table_1'] as $key16 => $value16) {
                    array_push($temp4, $incomColTotal[$value16['dimensionValueID']]);
                }
                array_push($temp4, $incomColTotal['tot']);

                $arr .= implode(",", $temp4). "\n";


                $temp5=[
                    0 => '',
                    1 => '',
                    2 => ''
                ];

                foreach ($incomeExpenseData['headers']['table_1'] as $key17 => $value17) {
                    array_push($temp5, '');
                }
                array_push($temp5, '');

                $arr .= implode(",", $temp5). "\n";

                $temp6=[
                    0 => '',
                    1 => 'Expese GL Accounts',
                    2 => ''
                ];

                foreach ($incomeExpenseData['headers']['table_1'] as $key18 => $value18) {
                    array_push($temp6, '');
                }
                array_push($temp6, '');

                $arr .= implode(",", $temp6). "\n";


                if (!$summery) {
                    $expenseColTotal = [];
                    foreach ($incomeExpenseData['data']['table_1']['Expense'] as $key19 => $value19) {
                        $expRowTotal = 0; 
                        $temp3=[
                            0 => '',
                            1 => ''
                        ];
                        $accNme= $value19['accName'];
                        unset($value19['accName']);
                        array_push($temp3, $accNme);

                        foreach ($incomeExpenseData['headers']['table_1'] as $key15 => $val15) {
                            $expRowTotal += $value19[$val15['dimensionValueID']];
                            $expenseColTotal[$val15['dimensionValueID']] = $expenseColTotal[$val15['dimensionValueID']] + $value19[$val15['dimensionValueID']];
                            array_push($temp3, $value19[$val15['dimensionValueID']]);
                        }
                        $expenseColTotal['tot'] = $expenseColTotal['tot'] + $expRowTotal;
                        array_push($temp3, $expRowTotal);
                        $arr .= implode(",", $temp3). "\n";
                    }
                } else {
                    $expenseColTotal = [];
                    foreach ($incomeExpenseData['data']['table_1']['Expense'] as $rowKey1 => $val55) {
                        $expRowTotal = 0;

                        foreach ($incomeExpenseData['headers']['table_1'] as $key56 => $val56) {
                            $expRowTotal += $val55[$val56['dimensionValueID']];
                            $expenseColTotal[$val56['dimensionValueID']] = $expenseColTotal[$val56['dimensionValueID']] + $val55[$val56['dimensionValueID']];
                        }
                        $expenseColTotal['tot'] = $expenseColTotal['tot'] + $expRowTotal;
                    }
                }

                $temp4=[
                    0 => '',
                    1 => '',
                    2 => 'Total'
                ];

                foreach ($incomeExpenseData['headers']['table_1'] as $key16 => $value16) {
                    array_push($temp4, $expenseColTotal[$value16['dimensionValueID']]);
                }
                array_push($temp4, $expenseColTotal['tot']);

                $arr .= implode(",", $temp4). "\n";

                $temp5=[
                    0 => '',
                    1 => '',
                    2 => ''
                ];

                foreach ($incomeExpenseData['headers']['table_1'] as $key17 => $value17) {
                    array_push($temp5, '');
                }
                array_push($temp5, '');

                $arr .= implode(",", $temp5). "\n";


                $temp7=[
                    0 => 'Net Total',
                    1 => '',
                    2 => ''
                ];

                foreach ($incomeExpenseData['headers']['table_1'] as $key19 => $value19) {
                    $netTot = $incomColTotal[$value19['dimensionValueID']] - $expenseColTotal[$value19['dimensionValueID']];
                    array_push($temp7, $netTot);
                }
                array_push($temp7, $incomColTotal['tot'] - $expenseColTotal['tot']);

                $arr .= implode(",", $temp7). "\n";
                
            } else {
                $arr = "no matching records found";
            }
            $name = "dimension_wise_income_expense_summery_report.csv";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }
}
