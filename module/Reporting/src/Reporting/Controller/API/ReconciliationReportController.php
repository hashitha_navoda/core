<?php

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains ReconciliationReport related actions
 */
class ReconciliationReportController extends CoreController
{

    protected $sideMenus = 'expenses_side_menu';
    protected $upperMenus = 'expenses_reports_upper_menu';

    public function generateAccountReconciliationAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $reconciliationData = array();

            $accountList = array();
            if (!empty($postData['accountId'])) {
                array_push($accountList, $postData['accountId']);
            } else {
                $reconciliations = $this->CommonTable('Expenses\Model\ReconciliationTable')
                        ->getReconciliationsByAccountId(null, 0, $postData['fromDate'], $postData['toDate'], false, array('reconciliation.reconciliationId ASC'));

                $accountList = array_map(function($reconciliations) {
                    $accountList = $reconciliations['accountId'];
                    return $accountList;
                }, iterator_to_array($reconciliations));
                $accountList = array_unique($accountList);
            }
            foreach ($accountList as $accountId) {
                $accReconciliation = array();
                $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
                $accReconciliation['accountName'] = $account['accountName'];
                $accReconciliation['accountNumber'] = $account['accountNumber'];

                $bank = $this->CommonTable('Expenses\Model\BankTable')->getBankByBankId($account['bankId']);
                $accReconciliation['bankName'] = $bank['bankName'];
                $rec = $this->CommonTable('Expenses\Model\ReconciliationTable')->getReconciliationsByAccountId($accountId, 0, $postData['fromDate'], $postData['toDate'], false, array('reconciliation.reconciliationId ASC'));
                $accReconciliation['reconciliations'] = iterator_to_array($rec);
                array_push($reconciliationData, $accReconciliation);
            }

            $period = $postData['fromDate'] . ' - ' . $postData['toDate'];
            $headerView = $this->headerViewTemplate('Account Reconciliation Report', $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'reconciliationData' => $reconciliationData,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/reconciliation-report/generate-account-reconciliation');

            $this->status = true;
            $this->msg = 'Account reconciliation report generated';
            $this->html = $view;
        }

        return $this->JSONRespondHtml();
    }

    public function generateAccountReconciliationPdfAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $reconciliationData = array();

            $accountList = array();
            if (!empty($postData['accountId'])) {
                array_push($accountList, $postData['accountId']);
            } else {
                $reconciliations = $this->CommonTable('Expenses\Model\ReconciliationTable')
                        ->getReconciliationsByAccountId(null, 0, $postData['fromDate'], $postData['toDate'], false, array('reconciliation.reconciliationId ASC'));

                $accountList = array_map(function($reconciliations) {
                    $accountList = $reconciliations['accountId'];
                    return $accountList;
                }, iterator_to_array($reconciliations));
                $accountList = array_unique($accountList);
            }
            foreach ($accountList as $accountId) {
                $accReconciliation = array();
                $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
                $accReconciliation['accountName'] = $account['accountName'];
                $accReconciliation['accountNumber'] = $account['accountNumber'];

                $bank = $this->CommonTable('Expenses\Model\BankTable')->getBankByBankId($account['bankId']);
                $accReconciliation['bankName'] = $bank['bankName'];
                $rec = $this->CommonTable('Expenses\Model\ReconciliationTable')->getReconciliationsByAccountId($accountId, 0, $postData['fromDate'], $postData['toDate'], false, array('reconciliation.reconciliationId ASC'));
                $accReconciliation['reconciliations'] = iterator_to_array($rec);
                array_push($reconciliationData, $accReconciliation);
            }

            $period = $postData['fromDate'] . ' - ' . $postData['toDate'];
            $headerView = $this->headerViewTemplate('Account Reconciliation Report', $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'reconciliationData' => $reconciliationData,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/reconciliation-report/generate-account-reconciliation');
            $view->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($view);

            $pdfPath = $this->downloadPDF('account_reconciliation_report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;

            return $this->JSONRespond();
        }
    }

    public function generateAccountReconciliationCsvAction()
    {

        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
            $reconciliationData = array();

            $companyDetails = $this->getCompanyDetails();

            $accountList = array();
            if (!empty($postData['accountId'])) {
                array_push($accountList, $postData['accountId']);
            } else {
                $reconciliations = $this->CommonTable('Expenses\Model\ReconciliationTable')
                        ->getReconciliationsByAccountId(null, 0, $postData['fromDate'], $postData['toDate'], false, array('reconciliation.reconciliationId ASC'));

                $accountList = array_map(function($reconciliations) {
                    $accountList = $reconciliations['accountId'];
                    return $accountList;
                }, iterator_to_array($reconciliations));
                $accountList = array_unique($accountList);
            }
            foreach ($accountList as $accountId) {
                $accReconciliation = array();
                $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
                $accReconciliation['accountName'] = $account['accountName'];
                $accReconciliation['accountNumber'] = $account['accountNumber'];

                $bank = $this->CommonTable('Expenses\Model\BankTable')->getBankByBankId($account['bankId']);
                $accReconciliation['bankName'] = $bank['bankName'];
                $rec = $this->CommonTable('Expenses\Model\ReconciliationTable')->getReconciliationsByAccountId($accountId, 0, $postData['fromDate'], $postData['toDate'], false, array('reconciliation.reconciliationId ASC'));
                $accReconciliation['reconciliations'] = iterator_to_array($rec);
                array_push($reconciliationData, $accReconciliation);
            }

            if ($reconciliationData) {

                $data = array(
                    "Account Reconciliation Report"
                );
                $title.=implode(",", $data) . "\n";

                $data = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $data) . "\n";

                $data = array(
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    "Period :" . $postData['fromDate'] . " - " . $postData['toDate']
                );
                $title.=implode(",", $data) . "\n";

                $tableHead = array('Bank', 'Account Name', 'Account Number', 'Reconciliation Comment', 'Reconciliation Date', 'Reconciliation Ammount');
                $header.=implode(",", $tableHead) . "\n";

                foreach ($reconciliationData as $accounts) {

                    $data = array($accounts['bankName'], $accounts['accountName'], $accounts['accountNumber']);
                    $arr.=implode(",", $data) . "\n";
                    foreach ($accounts as $key => $account) {

                        if ($key == 'reconciliations') {

                            foreach ($account as $reconciliation) {
                                $data = array('', '', '', $reconciliation['reconciliationComment'], $reconciliation['reconciliationDate'], $reconciliation['reconciliationAmount']);
                                $arr.=implode(",", $data) . '\n';
                            }
                        }
                    }
                }
            } else {
                $arr = "no matching records found";
            }
            $name = "account_reconciliation_report.csv";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

}
