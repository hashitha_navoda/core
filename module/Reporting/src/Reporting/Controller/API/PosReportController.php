<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class PosReportController extends CoreController
{

    protected $user_session;
    protected $locationID;
    protected $allLocations;

    private function _getPosInvoiceData($fromDate = null, $toDate = null, $userIds = NULL, $locIds = NULL, $discount = FALSE)
    {
        $invData = $this->CommonTable('Invoice\Model\InvoiceTable')->getPosInvoiceData($fromDate, $toDate, $userIds, $locIds, $discount);
        $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getPosCreditNoteData($fromDate, $toDate, $userIds, $locIds, $discount);
        $invDataSet = array();

        foreach ($invData as $i) {
            $invDataSet[$i['salesInvoiceID']] = $i;
        }
        foreach ($creditNoteData as $i) {
            $invDataSet[$i['creditNoteID']] = $i;
        }
        return $invDataSet;
    }

    public function posInvoiceViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $userIds = $request->getPost('userIds');
            $locIds = $request->getPost('locIds');
            $invData = $this->_getPosInvoiceData($fromDate, $toDate, $userIds, $locIds);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('POS Invoice Report');
//            $name = 'POS Invoice Report';
            $period = $fromDate." - ".$toDate;


            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $posInvoice = new ViewModel(array(
                'cD' => $companyDetails,
                'iD' => $invData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $posInvoice->setTemplate('reporting/pos-report/generate-pos-invoice-pdf');

            $this->html = $posInvoice;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generatePosInvoicePdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $uIds = $request['userIds'];
            $translator = new Translator();
            $name = $translator->translate('POS Invoice Report');
//            $name = 'POS Invoice Report';
            $period = $fromDate." - ".$toDate;
            $userIds = explode(",", $uIds);
            if ($userIds[0] == 'null') {
                $userIds = array();
            }
            $lIds = $request['locIds'];
            $locIds = explode(",", $lIds);
            if ($locIds[0] == 'null') {
                $locIds = array();
            }
            $invData = $this->_getPosInvoiceData($fromDate, $toDate, $userIds, $locIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $posInvoiceView = new ViewModel(array(
                'cD' => $companyDetails,
                'iD' => $invData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $posInvoiceView->setTemplate('reporting/pos-report/generate-pos-invoice-pdf');
            $posInvoiceView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($posInvoiceView);

            $this->downloadPDF('pos-invoice-report', $htmlContent);

            exit;
        }
    }

    public function generatePosInvoiceSheetAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $uIds = $request['userIds'];
            $userIds = explode(",", $uIds);
            if ($userIds[0] == 'null') {
                $userIds = array();
            }
            $lIds = $request['locIds'];
            $locIds = explode(",", $lIds);
            if ($locIds[0] == 'null') {
                $locIds = array();
            }
            $invData = $this->_getPosInvoiceData($fromDate, $toDate, $userIds, $locIds);
            $cD = $this->getCompanyDetails();

            if ($invData) {
                $title = '';
                $tit = 'POS INVOICE REPORT';
                $in = ["", $tit];

                $title.=implode(",", $in) . "\n";
                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["NO", "DATE", "USER", "INVOICE/CREDITNOTE ID", "INVOICE/CREDITNOTE AMOUNT"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                $netInvTotal = 0;
                foreach ($invData as $invID => $value) {
                    $iCD = $value['salesInvoiceCode'] ? $value['salesInvoiceCode'] :  $value['creditNoteCode'].' ('.$value['sCode'].')';
                    $date =  $value['salesInvoiceIssuedDate'] ? $value['salesInvoiceIssuedDate'] : $value['creditNoteDate'];
                    if ($value['salesinvoiceTotalAmount']) {
                        $invTotal = $value['salesinvoiceTotalAmount'];
                        $netInvTotal += $invTotal;  
                    } else if ($value['creditNoteTotal']) {
                        $invTotal = $value['creditNoteTotal'];
                        $netInvTotal -= $invTotal;  
                    } else {
                        $invTotal = 0.00;
                        $netInvTotal += $invTotal;
                    }

                    $in = [$i,
                        $date,
                        $value['userFirstName'] . '-' . $value['userUsername'],
                        $iCD,
                        $invTotal
                    ];
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
                $in = ['', '', '', 'Grand Total', $netInvTotal];
                $arr.=implode(",", $in) . "\n";
            }
        } else {
            $arr = "no matching records found\n";
        }

        $name = "pos-invoice.csv";
        $size = "4GB";
        $size_f = strlen($header . "\n" . $arr);
        $this->csvHeader($name, $size, $size_f, $title, $header, $arr);
    }

    private function _getUserBalanceSummaryData($fromDate = NULL, $toDate = NULL, $userIds = NULL, $locIds = NULL)
    {
        if (!empty($userIds)) {
            $userData = array_fill_keys($userIds, '');

            $fromDate = $fromDate.' 00:00:00';
            $toDate = $toDate.' 23:59:00';

            $posData = $this->CommonTable('Pos\Model\PosTable')->getPosBalanceData($fromDate, $toDate, $userIds, $locIds);
            foreach ($posData as $p) {
                $userData[$p['userID']]['bD'][$p['posID']] = $p;
                //GMT login and logout time convert into current time zone date and time
                $userData[$p['userID']]['bD'][$p['posID']]['logInDateTime'] = $this->getUserDateTime($p['logInDateTime']);
                $userData[$p['userID']]['bD'][$p['posID']]['logOutDateTime'] = $this->getUserDateTime($p['logOutDateTime']);
                $userData[$p['userID']]['uD'] = array('fName' => $p['userFirstName'], 'lName' => $p['userLastName']);
            }

            return $userData;
        }
    }

    public function userWiseBalanceSummaryViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('User wise opening balances and closing balances');
//            $name = 'User wise opening balances and closing balances';
            $period = $fromDate.' - '.$toDate;
            $userIds = $request->getPost('userIds');
            if (empty($userIds)) {
                $users = $this->CommonTable('User\Model\UserTable')->fetchAll();
                foreach ($users as $u) {
                    $userIds[$u['userID']] = $u['userID'];
                }
            }
            $locIds = $request->getPost('locIds');

            $userBalanceSummaryDataSet = $this->_getUserBalanceSummaryData($fromDate, $toDate, $userIds, $locIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $posBalanceSummary = new ViewModel(array(
                'cD' => $companyDetails,
                'pBSD' => $userBalanceSummaryDataSet,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $posBalanceSummary->setTemplate('reporting/pos-report/generate-pos-balance-summary-pdf');

            $this->html = $posBalanceSummary;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generatePosBalanceSummaryPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $uIds = $request['userIds'];
            $translator = new Translator();
            $name = $translator->translate('User wise opening balances and closing balances');
//            $name = 'User wise opening balances and closing balances';
            $period = $fromDate.' - '.$toDate;
            $userIds = explode(",", $uIds);
            if ($userIds[0] == 'null') {
                $users = $this->CommonTable('User\Model\UserTable')->fetchAll();
                foreach ($users as $u) {
                    $userIds[$u['userID']] = $u['userID'];
                }
            }
            $lIds = $request['locIds'];
            $locIds = explode(",", $lIds);
            if ($locIds[0] == 'null') {
                $locIds = array();
            }
            $userBalanceSummaryDataSet = $this->_getUserBalanceSummaryData($fromDate, $toDate, $userIds, $locIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $posBalanceSummary = new ViewModel(array(
                'cD' => $companyDetails,
                'pBSD' => $userBalanceSummaryDataSet,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $posBalanceSummary->setTemplate('reporting/pos-report/generate-pos-balance-summary-pdf');
            $posBalanceSummary->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($posBalanceSummary);

            $this->downloadPDF('pos-balance-summary-report', $htmlContent);

            exit;
        }
    }

    public function generatePosBalanceSummarySheetAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $uIds = $request['userIds'];
            $userIds = explode(",", $uIds);
            if ($userIds[0] == 'null') {
                $users = $this->CommonTable('User\Model\UserTable')->fetchAll();
                foreach ($users as $u) {
                    $userIds[$u['userID']] = $u['userID'];
                }
            }
            $lIds = $request['locIds'];
            $locIds = explode(",", $lIds);
            if ($locIds[0] == 'null') {
                $locIds = array();
            }
            $userBalanceSummaryDataSet = $this->_getUserBalanceSummaryData($fromDate, $toDate, $userIds, $locIds);
            $cD = $this->getCompanyDetails();

            if ($userBalanceSummaryDataSet) {
                $title = '';
                $tit = 'USER WISE OPENING BALANCES AND CLOSING BALANCES REPORT';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["NO", "USER FIRST NAME", "USER LAST NAME", "TILL OPEN TIME", "TILL OPEN BALANCE({$this->companyCurrencySymbol})", "TILL CLOSE TIME", "TILL CLOSE BALANCE({$this->companyCurrencySymbol})", "TOTAL COLLECTION({$this->companyCurrencySymbol})"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                $netTotalOpenBalance = 0;
                $netTotalCloseBalance = 0;
                $netTotalCollection = 0;
                foreach ($userBalanceSummaryDataSet as $userID => $data) {
                    if (!empty($data)) {
                        $in = [$i, $data['uD']['fName'], $data['uD']['lName']];
                        $arr.=implode(",", $in) . "\n";

                        $netOpenBalance = 0;
                        $netColseBalance = 0;
                        $netCollection = 0;
                        foreach ($data['bD'] as $d) {
                            $openBalance = $d['openingBalance'] ? $d['openingBalance'] : 0.00;
                            $closeBalance = $d['closingBalance'] ? $d['closingBalance'] : 0.00;
                            if ($closeBalance != 0) {
                                $collection = $closeBalance - $openBalance;
                            } else {
                                $collection = 0;
                            }
                            $netOpenBalance += $openBalance;
                            $netColseBalance += $closeBalance;
                            $netCollection += $collection;
                            $netTotalOpenBalance += $openBalance;
                            $netTotalCloseBalance += $closeBalance;
                            $netTotalCollection += $collection;

                            $in = [ '', '', '',
                                $d['logInDateTime'],
                                ($openBalance),
                                $d['logOutDateTime'],
                                ($closeBalance),
                                ($collection)
                            ];
                            $arr.=implode(",", $in) . "\n";
                        }
                        $in = ['', '', '', 'Total', ($netOpenBalance), '',
                            ($netColseBalance), ($netCollection)];
                        $arr.=implode(",", $in) . "\n";
                        $i++;
                    }
                }
                $in = ['', '', '', 'Net Total', ($netTotalOpenBalance), '',
                    ($netTotalCloseBalance),
                    ($netTotalCollection)
                ];
                $arr.=implode(",", $in) . "\n";
            }
        } else {
            $arr = "no matching records found\n";
        }

        $name = "pos-balance-summary.csv";
        $size = "4GB";
        $size_f = strlen($header . "\n" . $arr);
        $this->csvHeader($name, $size, $size_f, $title, $header, $arr);
    }

    /**
     * get payments regarding to pos invoice
     * @author Sandun <sandun@thinkcube.com>
     * @param date $date
     * @param array $paymentMethodsIds
     * @param array $locIds
     * @return array $posPaymentData
     */
    private function _getPosPaymentData($fromDate = NULL, $toDate = null, $paymentMethodsIds = NULL, $locIds = NULL, $userIds = null)
    {
        $userIds = ($userIds == "") ? NULL : $userIds;
        $getPosPaymentData = $this->CommonTable('Invoice\Model\InvoiceTable')->getPosPaymentData($fromDate, $toDate, $locIds, $userIds);
        $getPosCreditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getPosCreditNotePaymentData($fromDate, $toDate, $locIds, $userIds);

        $invoiceData = [];        
        $paymentMethodsIds = empty(!$paymentMethodsIds) ? $paymentMethodsIds : [];
        foreach ($getPosPaymentData as $res) {
            $posPaymentData = [];
            
            if (in_array('1', $paymentMethodsIds) && isset($res['incomingPaymentMethodCashId'])) {
                $posPaymentData = [
                    'paymentType' => 'Cash',
                    'paymentTypeID' => 'Cash',
                    'invoicePaidAmount' => $res['incomingPaymentMethodAmount']
                ];
            } else if (in_array('2', $paymentMethodsIds) && isset($res['incomingPaymentMethodChequeId'])) {
                $posPaymentData = [
                    'paymentType' => 'Cheque',
                    'paymentTypeID' => 'Cheque',
                    'invoicePaidAmount' => $res['incomingPaymentMethodAmount']
                ];
            } else if (in_array('3', $paymentMethodsIds) && isset($res['incomingPaymentMethodCreditCardId'])) {
                $posPaymentData = [
                    'paymentType' => "Credit Card [{$res['cardTypeName']}]",
                    'paymentTypeID' => "Card",
                    'invoicePaidAmount' => $res['incomingPaymentMethodAmount']
                ];
            } else if (in_array('4', $paymentMethodsIds) && isset($res['incomingPaymentMethodLoyaltyCardId'])) {
                $posPaymentData = [
                    'paymentType' => 'Loyalty Card',
                    'paymentTypeID' => 'Loyalty Card',
                    'invoicePaidAmount' => $res['incomingPaymentMethodAmount']
                ];
            } else if (in_array('5', $paymentMethodsIds) && isset($res['incomingPaymentMethodBankTransferId'])) {
                $posPaymentData = [
                    'paymentType' => 'Bank Transfer',
                    'paymentTypeID' => 'Bank Transfer',
                    'invoicePaidAmount' => $res['incomingPaymentMethodAmount']
                ];
            } else if (in_array('6', $paymentMethodsIds) && isset($res['incomingPaymentMethodGiftCardId'])) {
                $posPaymentData = [
                    'paymentType' => 'Gift Card',
                    'paymentTypeID' => 'Gift Card',
                    'invoicePaidAmount' => $res['incomingPaymentMethodAmount']
                ];
            }
            if (!empty($posPaymentData)) {
                if (!isset($invoiceData[$res['salesInvoiceID']])) {
                    $invoiceData[$res['salesInvoiceID']] = [
                        'invoiceCode' => $res['salesInvoiceCode'],
                        'invoiceIssuedDate' => $res['salesInvoiceIssuedDate'],
                        'invoiceTotalAmount' => $res['salesinvoiceTotalAmount'],
                        'invoicePaidAmount' => $res['salesInvoicePayedAmount'],
                        'createdUser' => $res['createdUser'],
                        'paymentData' => []
                    ];
                }
                $invoiceData[$res['salesInvoiceID']]['paymentData'][] = $posPaymentData;
            }            
        }

        foreach ($getPosCreditNotePaymentData as $value) {
            if ($value['creditNoteSettledAmount'] != 0) {
                $posPaymentData = [
                        'paymentType' => 'Cash',
                        'paymentTypeID' => 'Cash',
                        'invoicePaidAmount' => $value['creditNoteSettledAmount']
                    ];

                $invoiceData[$value['creditNoteID']] = [
                    'invoiceCode' => $value['creditNoteCode'].' ('.$value['sCode'].')',
                    'invoiceIssuedDate' => $value['creditNoteDate'],
                    'invoiceTotalAmount' => $value['creditNoteTotal'],
                    'invoicePaidAmount' => $value['creditNoteSettledAmount'],
                    'createdUser' => $value['createdUser'],
                    'paymentData' => []
                ];

                $invoiceData[$value['creditNoteID']]['paymentData'][] = $posPaymentData;
            }
        }
        return $invoiceData;
    }

    public function posPaymentViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $paymentMethodsIds = $request->getPost('paymentMethodsIds');
            $locIds = $request->getPost('locIds');
            $userIds = $request->getPost('userIds');
            $translator = new Translator();
            $name = $translator->translate('POS Invoices by payment type Report');
            $period = $fromDate.' - '.$toDate;

            $posPaymentData = $this->_getPosPaymentData($fromDate, $toDate, $paymentMethodsIds, $locIds, $userIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $posPayment = new ViewModel(array(
                'cD' => $companyDetails,
                'pPD' => $posPaymentData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $posPayment->setTemplate('reporting/pos-report/generate-pos-payment-pdf');

            $this->html = $posPayment;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generatePosPaymentPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $pIds = $request['paymentMethodsIds'];
            $uIds = $request['userIds'];
            $userIds = explode(",", $uIds);
            if ($userIds[0] == 'null') {
                $userIds = array();
            }
            $paymentMethodsIds = explode(",", $pIds);

            if ($paymentMethodsIds[0] == 'null') {
                $paymentMethodsIds = array();
            }
            $lIds = $request['locIds'];
            $locIds = explode(",", $lIds);
            if ($locIds[0] == 'null') {
                $locIds = array();
            }

            $posPaymentData = $this->_getPosPaymentData($fromDate, $toDate, $paymentMethodsIds, $locIds, $userIds);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('POS Invoices by payment type Report');
            $period = $fromDate.' - '.$toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);


            $posPaymentView = new ViewModel(array(
                'cD' => $companyDetails,
                'pPD' => $posPaymentData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $posPaymentView->setTemplate('reporting/pos-report/generate-pos-payment-pdf');
            $posPaymentView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($posPaymentView);
            $this->downloadPDF('pos-invoice-report', $htmlContent);
            exit;
        }
    }

    public function generatePosPaymentSheetAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $pIds = $request['paymentMethodsIds'];
            $uIds = $request['userIds'];
            $paymentMethodsIds = explode(",", $pIds);
            if ($paymentMethodsIds[0] == 'null') {
                $paymentMethodsIds = array();
            }
            $userIds = explode(",", $uIds);
            if ($userIds[0] == 'null') {
                $userIds = array();
            }
            $lIds = $request['locIds'];
            $locIds = explode(",", $lIds);
            if ($locIds[0] == 'null') {
                $locIds = array();
            }

            $posPaymentData = $this->_getPosPaymentData($fromDate, $toDate, $paymentMethodsIds, $locIds, $userIds);
            $cD = $this->getCompanyDetails();

            if ($posPaymentData) {
                $title = '';
                $tit = 'POS INVOICE BY PAYMENT TYPE REPORT';
                $in = ["", $tit, "", ""];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = array("REF:NO", "INVOICE/CREDITNOTE ID","CREATED BY", "DATE", "INVOICE/CREDITNOTE AMOUNT({$this->companyCurrencySymbol})", "PAID AMOUNT({$this->companyCurrencySymbol})", "CASH({$this->companyCurrencySymbol})", "CARD({$this->companyCurrencySymbol})", "OTHER PAYMENT TYPE({$this->companyCurrencySymbol})");
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                $netInvTotal = 0;
                $netInvPaidTotal = 0;
                $netInvPayemntTotal = 0;
                $netCashAmount = 0;
                $netCardAmount = 0;
                $netOtherAmount = 0;
                foreach ($posPaymentData as $invID => $value) {
                    $netInvTotal += $value['invoiceTotalAmount'];
                    $netInvPaidTotal += $value['invoicePaidAmount'];
                    $in = [
                        $i, 
                        $value['invoiceCode'],
                        $value['createdUser'],
                        $value['invoiceIssuedDate'],
                        number_format($value['invoiceTotalAmount'], 2, '.', ''),
                        number_format($value['invoiceTotalAmount'], 2, '.', ''),
                        '',
                        ''
                    ];
                    $arr.=implode(",", $in) . "\n";
                    foreach ($value['paymentData'] as $paymentData) {
                        $netInvPayemntTotal += $paymentData['invoicePaidAmount'];
                        $cashAmount = ($paymentData['paymentTypeID'] == 'Cash') ? number_format($paymentData['invoicePaidAmount'], 2, '.', '') : "";
                        $netCashAmount += ($paymentData['paymentTypeID'] == 'Cash') ? $paymentData['invoicePaidAmount'] : 0;

                        $cardAmount = ($paymentData['paymentTypeID'] == 'Card') ? number_format($paymentData['invoicePaidAmount'], 2, '.', '') : "";
                        $netCardAmount += ($paymentData['paymentTypeID'] == 'Card') ? $paymentData['invoicePaidAmount'] : 0;

                        $OtherAmount = ($paymentData['paymentTypeID'] != 'Cash' && $paymentData['paymentTypeID'] != 'Card') ? number_format($paymentData['invoicePaidAmount'], 2, '.', '') : "";
                        $netOtherAmount += ($paymentData['paymentTypeID'] != 'Cash' && $paymentData['paymentTypeID'] != 'Card') ? $paymentData['invoicePaidAmount'] : 0;
                        $in = [
                            '',
                            '',
                            '',
                            '',
                            '',
                            '',
                            $cashAmount,
                            $cardAmount,
                            $OtherAmount
                        ];
                        $arr.=implode(",", $in) . "\n";
                    }                    
                    $i++;
                }
                $in = [
                    '',
                    '',
                    '',
                    'Net Total',
                    $netInvTotal,
                    $netInvPaidTotal,
                    $netCashAmount,
                    $netCardAmount,
                    $netOtherAmount
                ];
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }

            $name = "pos-payment-report.csv";
            $size = "4GB";
            $size_f = strlen($header . "\n" . $arr);
            $this->csvHeader($name, $size, $size_f, $title, $header, $arr);
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     * view pos item wise dicount report
     */
    public function posItemDiscountViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $userIds = $request->getPost('userIds');
            $locIds = $request->getPost('locIds');

            $posDiscountInvoice = $this->_getPosInvoiceData($fromDate, $toDate, $userIds, $locIds, $discount = TRUE);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('POS Discount Invoices Report');
            $period = $fromDate.' - '.$toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $posPayment = new ViewModel(array(
                'cD' => $companyDetails,
                'iD' => $posDiscountInvoice,
                'type' => 'discount',
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $posPayment->setTemplate('reporting/pos-report/generate-pos-invoice-pdf');

            $this->html = $posPayment;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generatePosItemDiscountPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $uIds = $request['userIds'];
            $userIds = explode(",", $uIds);
            if ($userIds[0] == 'null') {
                $userIds = [];
            }
            $lIds = $request['locIds'];
            $locIds = explode(",", $lIds);
            if ($locIds[0] == 'null') {
                $locIds = [];
            }

            $posDiscountInvoice = $this->_getPosInvoiceData($fromDate, $toDate, $userIds, $locIds, $discount = TRUE);
            $companyDetails = $this->getCompanyDetails();

            $translator = new Translator();
            $name = $translator->translate('POS Discount Invoices Report');
            $period = $fromDate.' - '.$toDate;
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $posInvoiceDiscountView = new ViewModel(array(
                'cD' => $companyDetails,
                'iD' => $posDiscountInvoice,
                'type' => 'discount',
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $posInvoiceDiscountView->setTemplate('reporting/pos-report/generate-pos-invoice-pdf');
            $posInvoiceDiscountView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($posInvoiceDiscountView);
            $this->downloadPDF('pos-invoice-discount-report', $htmlContent);
            exit;
        }
    }

    public function generatePosItemDiscountCsvAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $uIds = $request['userIds'];
            $userIds = explode(",", $uIds);
            if ($userIds[0] == 'null') {
                $userIds = [];
            }
            $lIds = $request['locIds'];
            $locIds = explode(",", $lIds);
            if ($locIds[0] == 'null') {
                $locIds = [];
            }

            $posDisountInvoice = $this->_getPosInvoiceData($fromDate, $toDate, $userIds, $locIds, $discount = TRUE);
            $cD = $this->getCompanyDetails();

            if (!empty($posDisountInvoice)) {
                $title = '';
                $tit = 'POS INVOICE DISCOUNT REPORT';
                $in = ["", $tit, "", ""];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["NO", "DATE", "USER", "INVOICE ID", "DISCOUNT AMOUNT({$this->companyCurrencySymbol})", "INVOICE AMOUNT({$this->companyCurrencySymbol})"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                $netInvTotal = 0;
                $netInvDiscountTotal = 0;
                foreach ($posDisountInvoice as $invID => $value) {
                    $invTotal = $value['salesinvoiceTotalAmount'] ? $value['salesinvoiceTotalAmount'] : 0.00;
                    $discount = $value['salesInvoiceWiseTotalDiscount'];
                    $netInvDiscountTotal += $discount;
                    $iCD = $value['salesInvoiceCode'] ? $value['salesInvoiceCode'] : '-';
                    $netInvTotal += $invTotal;

                    $in = [
                        $i, $value['salesInvoiceIssuedDate'],
                        $value['userFirstName'] . '-' . $value['userUsername'],
                        $iCD, $discount, $invTotal
                    ];
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
                $in = ['', '', '', 'Net Total',
                    $netInvDiscountTotal,
                    $netInvTotal
                ];
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }

            $name = "pos-invoice-discount-report.csv";
            $size = "4GB";
            $size_f = strlen($header . "\n" . $arr);
            $this->csvHeader($name, $size, $size_f, $title, $header, $arr);
        }
    }
    
    private function getPosSalesSummeryDetails($postData) 
    {
        $salesData = [];        
        //get sales details        
        $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getPosInvoiceSummeryDetails($postData['fromDate'], $postData['toDate'], $postData['userIds'], $postData['locationIds'], [3, 4, 5, 6]);
        $invicesArr = $canceledInvicesArr =[];
        $totalSalesAmount = $totalCanceledSalesAmount = 0;
        foreach ($invoiceDetails as $invoice) {            
            if ($invoice['statusID'] == 5) {
                $totalCanceledSalesAmount += $invoice['salesinvoiceTotalAmount'] * $invoice['salesInvoiceCustomCurrencyRate'];
                $canceledInvicesArr[] = $invoice['salesInvoiceID'];
            }
            $invicesArr[] = $invoice['salesInvoiceID'];
            $totalSalesAmount += $invoice['salesinvoiceTotalAmount'] * $invoice['salesInvoiceCustomCurrencyRate'];
        }
        
        //pos payment details
        $paymentDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getPosPaymentSummeryDetails($postData['fromDate'], $postData['toDate'], $postData['userIds'], $postData['locationIds'], [3, 4, 5, 6]);
        $paymentArr = $canceledPaymentArr = [];
        $totalPaidAmount = 0;
        foreach ($paymentDetails as $payment) {
            if ($payment['incomingPaymentStatus'] == 5) {
                $canceledPaymentArr[] = $payment['incomingPaymentID'];
            }
            $paymentArr[] = $payment['incomingPaymentID'];
            $totalPaidAmount += $payment['incomingPaymentPaidAmount'] * $payment['salesInvoiceCustomCurrencyRate'];
        }
        
        //cancelled payment details
        $paymentCancellationDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getPosPaymentSummeryDetails($postData['fromDate'], $postData['toDate'], $postData['userIds'], $postData['locationIds'], [3, 4, 5, 6], [5]);
        $totalCancellationAmount = 0;
        foreach ($paymentCancellationDetails as $payment) {
            $totalCancellationAmount += $payment['incomingPaymentAmount'] * $payment['salesInvoiceCustomCurrencyRate'];
        }
        
        //credit not details
        $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteSummeryDetails($postData['fromDate'], $postData['toDate'], $postData['userIds'], $postData['locationIds'], [3, 4, 5, 6]);
        $creditNoteTotalAmount = $creditNotePaidAmount = 0;
        foreach ($creditNoteDetails as $creditNote) {
            $creditNoteTotalAmount += $creditNote['creditNotePaymentAmount'] * $creditNote['creditNoteCustomCurrencyRate'];
            $creditNotePaidAmount += $creditNote['creditNoteSettledAmount'] * $creditNote['creditNoteCustomCurrencyRate'];
        }
        
        //cash payments
        $cashPayments = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getPosCashPayments($postData['fromDate'], $postData['toDate'], $postData['userIds'], $postData['locationIds'], [3, 4, 5, 6]);
        $totalCashPayments = 0;
        foreach ($cashPayments as $cashPayment) {
            $totalCashPayments += $cashPayment['incomingPaymentMethodAmount'] * $cashPayment['salesInvoiceCustomCurrencyRate'];
        }
        
        //card payments
        $cardPayments = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getPosCardPayments($postData['fromDate'], $postData['toDate'], $postData['userIds'], $postData['locationIds'], [3, 4, 5, 6]);
        $totalCardPayments = 0;
        foreach ($cardPayments as $cardPayment) {
            $totalCardPayments += $cardPayment['incomingPaymentMethodAmount'] * $cardPayment['salesInvoiceCustomCurrencyRate'];
        }
        
        //other payments
        $otherPayments = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getOtherPosPayments($postData['fromDate'], $postData['toDate'], $postData['userIds'], $postData['locationIds'], [3, 4, 5, 6]);
        $totalOtherPayments = 0;
        foreach ($otherPayments as $otherPayment) {
            $totalOtherPayments += $otherPayment['incomingPaymentMethodAmount'] * $otherPayment['salesInvoiceCustomCurrencyRate'];
        }
        $netSalesTotal = $totalSalesAmount - $creditNoteTotalAmount - $totalCanceledSalesAmount;
        $netPaymentAmount = $totalPaidAmount - $creditNotePaidAmount - $totalCancellationAmount;
        $totalPay = $totalCashPayments + $totalCardPayments + $totalOtherPayments;
        $salesData['totalPaymentAmount'] = $this->companyCurrencySymbol.' '.number_format($totalPay, 2);
        $salesData['numberOfPayments'] = count($paymentArr);
        $salesData['numberOfCanceledPayments'] = count($canceledPaymentArr);
        $salesData['totalPaymentCancellationAmount'] = $this->companyCurrencySymbol.' '.number_format($totalCancellationAmount, 2);
        $salesData['netSalesTotal'] = $this->companyCurrencySymbol.' '.number_format($netSalesTotal, 2);
        $salesData['numberOfInvoices'] = count($invicesArr);
        $salesData['numberOfCanceledInvoices'] = count($canceledInvicesArr);
        $salesData['totalSalesAmount'] = $this->companyCurrencySymbol.' '.number_format($totalSalesAmount, 2);
        $salesData['totalSalesCancellationAmount'] = $this->companyCurrencySymbol.' '.number_format($totalCanceledSalesAmount, 2);
        $salesData['totalCashPayments'] = $this->companyCurrencySymbol.' '.number_format($totalCashPayments, 2);
        $salesData['totalCardPayments'] = $this->companyCurrencySymbol.' '.number_format($totalCardPayments, 2);
        $salesData['totalOtherPayments'] = $this->companyCurrencySymbol.' '.number_format($totalOtherPayments, 2);
        $salesData['totalCreditNoteAmount'] = $this->companyCurrencySymbol.' '.number_format($creditNoteTotalAmount, 2);
        $salesData['totalPaidCreditNoteAmount'] = $this->companyCurrencySymbol.' '.number_format($creditNotePaidAmount, 2);
        $salesData['netPaymentAmount'] = $this->companyCurrencySymbol.' '.number_format($netPaymentAmount, 2);
        
        return $salesData;
    }

    public function posSalesSummeryReportAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->html = '';
        
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespondHtml();
        }
        
        $postData = $request->getPost()->toArray();
        $companyDetails = $this->getCompanyDetails();
        $salesData = $this->getPosSalesSummeryDetails($postData);
        
        $translator = new Translator();
        $name = $translator->translate('POS Sales Summery Report');
        $period = $postData['fromDate'] .' - '. $postData['toDate'];
        
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $salesSummery = new ViewModel(array(
            'companyDetails' => $companyDetails,
            'salesData' => $salesData,
            'companySymbol' => $this->companyCurrencySymbol,
            'headerTemplate' => $headerViewRender,
            'printView' => $postData['printView']
        ));
        $salesSummery->setTemplate('reporting/pos-report/pos-sales-summery-report');

        $this->status = true;
        $this->html = $salesSummery;
        $this->msg = 'success';
        
        return $this->JSONRespondHtml();
    }
    
    public function posSalesSummeryPdfAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->html = '';
        
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespondHtml();
        }
        
        $postData = $request->getPost()->toArray();        
        $companyDetails = $this->getCompanyDetails();
        $salesData = $this->getPosSalesSummeryDetails($postData);
        
        $translator = new Translator();
        $name = $translator->translate('POS Sales Summery Report');
        $period = $postData['fromDate'] .' - '. $postData['toDate'];
        
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $salesSummery = new ViewModel(array(
            'companyDetails' => $companyDetails,
            'salesData' => $salesData,
            'companySymbol' => $this->companyCurrencySymbol,
            'headerTemplate' => $headerViewRender,
            'printView' => $postData['printView']
        ));
        $salesSummery->setTemplate('reporting/pos-report/pos-sales-summery-report');
        $salesSummery->setTerminal(true);

        $htmlContent = $this->viewRendererHtmlContent($salesSummery);
        $pdfPath = $this->downloadPDF('pos-sales-summery-report', $htmlContent, true);
        $this->data = $pdfPath;
        $this->status = true;

        return $this->JSONRespond();
    }
    
    public function posSalesSummeryCsvAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->html = '';
        
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->JSONRespondHtml();
        }
        
        $postData = $request->getPost()->toArray();        
        $companyDetails = $this->getCompanyDetails();
        $salesData = $this->getPosSalesSummeryDetails($postData);        
        $period = $postData['fromDate'] .' - '. $postData['toDate'];
        
        if (!empty($salesData)) {
            $title = '';
            $in = array(
                0 => "",
                1 => 'POS SALES SUMMERY REPORT',
                2 => ""
            );
            $title = implode(",", $in) . "\n";
            $in = array(
                0 => $companyDetails[0]->companyName,
                1 => $companyDetails[0]->companyAddress,
                2 => 'Tel: ' . $companyDetails[0]->telephoneNumber
            );
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => "Period : ".$period
            );
            $title.=implode(",", $in) . "\n";
            
            $dataArr = [
                "TOTAL SALES" => str_replace(',', ' ', $salesData['totalSalesAmount']), 
                "TOTAL SALES RETURNS" => str_replace(',', ' ', $salesData['totalCreditNoteAmount']),
                "TOTAL SALES CANCELLATIONS" => str_replace(',', ' ', $salesData['totalSalesCancellationAmount']),
                "NET SALES TOTAL" => str_replace(',', ' ', $salesData['netSalesTotal']),
                "INVOICE RECORDS" => $salesData['numberOfInvoices'],
                "CANCELED INVOICE RECORDS" => $salesData['numberOfCanceledInvoices'],
                "PAYMENT RECORDS" => $salesData['numberOfPayments'],
                "CANCELED PAYMENT RECORDS" => $salesData['numberOfCanceledPayments'],
                "TOTAL PAYMENT AMOUNT" => str_replace(',', ' ', $salesData['totalPaymentAmount']),
                "CASH PAYMENTS" => str_replace(',', ' ', $salesData['totalCashPayments']),
                "CARD PAYMENTS" => str_replace(',', ' ', $salesData['totalCardPayments']),
                "OTHER PAYMENTS" => str_replace(',', ' ', $salesData['totalOtherPayments']),
                "CANCELED PAYMENT" => str_replace(',', ' ', $salesData['totalPaymentCancellationAmount']),
                "CREDIT NOTE PAYMENT TOTAL" => str_replace(',', ' ', $salesData['totalPaidCreditNoteAmount']),
                // "NET PAYMENT TOTAL" => str_replace(',', ' ', $salesData['netPaymentAmount']),
                ];            
                foreach ($dataArr as $key => $value) {
                    $in = array(
                        0 => $key,
                        1 => $value
                    );
                $arr.=implode(",", $in) . "\n";
                }            
        } else {
            $in = array('no matching records found');
            $arr.=implode(",", $in) . "\n";
        }
        
        $name = "pos_sales_summery_report";
        $csvContent = $this->csvContent($title, '', $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;        
        return $this->JSONRespondHtml();
    }

}
