<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains regarding of stock reports api fucntions
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;
use Core\BackgroundJobs\ReportJob;

class StockInHandReportController extends CoreController
{

    protected $locationID;

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param type $proIDs
     * @param type $locIDs
     * @return $proLists
     */
    public function getItemWiseStockDetails($proIDs = null, $locIDs = null, $isAllProducts, $categoryIds)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $categoryIds = (empty($categoryIds)) ? null : $categoryIds;
        if (isset($proIDs) || $isAllProducts) {

            $proLists = array();
            $productList = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyList($proIDs, $locIDs, $isAllProducts, $categoryIds);

            foreach ($productList as $p) {

                $product = array(
                    'pCD' => $p['pCD'],
                    'pName' => $p['pName'],
                    'categoryName' => $p['categoryName'],
                    'pS' => $p['pS'],
                    'pD' => $p['pD'],
                );
                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($p['pID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUom);
                $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                $product = array_merge($product, array(
                    'qty' => $thisqty['quantity'],
                    'uomAbbr' => $thisqty['uomAbbr'])
                );

                $proLists[$p['pID']] = $product;
            }

            return $proLists;
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function indexAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $translator = new Translator();
            $name = $translator->translate('Item wise Stock Report');

            $locIDs = $this->getActiveAllLocationsIds();
            $proLists = $this->getItemWiseStockDetails($proIDs, $locIDs, $isAllProducts, $categoryIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $proListView = new ViewModel(array(
                'cD' => $companyDetails,
                'pL' => $proLists,
                'headerTemplate' => $headerViewRender,
            ));
            $proListView->setTemplate('reporting/stock-in-hand-report/generate-item-wise-stock-pdf');

            $this->html = $proListView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return PDF path
     */
    public function generateItemWiseStockPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $locIDs = $this->getActiveAllLocationsIds();
            $translator = new Translator();
            $name = $translator->translate('Item wise Stock Report');

            $proLists = $this->getItemWiseStockDetails($proIDs, $locIDs, $isAllProducts, $categoryIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);


            $viewItemWiseStock = new ViewModel(array(
                'cD' => $companyDetails,
                'pL' => $proLists,
                'headerTemplate' => $headerViewRender,
            ));

            $viewItemWiseStock->setTemplate('reporting/stock-in-hand-report/generate-item-wise-stock-pdf');
            $viewItemWiseStock->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewItemWiseStock);
            $pdfPath = $this->downloadPDF('item-wise-stock', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return CSV Item Wise Stock
     */
    public function generateItemWiseStockSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $locIDs = $this->getActiveAllLocationsIds();

            $proLists = $this->getItemWiseStockDetails($proIDs, $locIDs, $isAllProducts, $categoryIds);
            $cD = $this->getCompanyDetails();

            // TODO - use fputcsv php function
            if ($proLists) {
                $title = '';
                $tit = 'ITEM WISE STOCK REPORT';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";
                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";
                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["NO", "PRODUCT CODE", "PRODUCT NAME", "PRODUCT CATEGORY", "QUANTITY", "UOM"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                foreach ($proLists as $key => $value) {
                    $qty = $value['qty'] ? $value['qty'] : 0.00;
                    $uomAbbr = $value['uomAbbr'] ? $value['uomAbbr'] : '';

                    $in = [$i, "{$value['pCD']}", $value['pName'], $value['categoryName'], $qty, $uomAbbr];
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }
            $name = "item_wise_stock_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function globalItemDetailsViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $proLists = $this->getItemWiseStockDetails($proIDs, null, $isAllProducts, $categoryIds);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Global Item Details Report');

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $proListView = new ViewModel(array(
                'cD' => $companyDetails,
                'pL' => $proLists,
                'headerTemplate' => $headerViewRender,
            ));
            $proListView->setTemplate('reporting/stock-in-hand-report/generate-global-item-details-pdf');

            $this->html = $proListView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return path of PDF
     */
    public function generateGlobalItemDetailsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $locIDs = $this->getActiveAllLocationsIds();
            $translator = new Translator();
            $name = $translator->translate('Global Item Details Report');

            $proLists = $this->getItemWiseStockDetails($proIDs, $locIDs, $isAllProducts, $categoryIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewGlobalItemDetails = new ViewModel(array(
                'cD' => $companyDetails,
                'pL' => $proLists,
                'headerTemplate' => $headerViewRender,
            ));

            $viewGlobalItemDetails->setTemplate('reporting/stock-in-hand-report/generate-global-item-details-pdf');
            $viewGlobalItemDetails->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewGlobalItemDetails);
            $pdfPath = $this->downloadPDF('global-item-details', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return path of CSV
     */
    public function generateGlobalItemDetailsSheetAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $locIDs = $this->getActiveAllLocationsIds();

            $proLists = $this->getItemWiseStockDetails($proIDs, $locIDs, $isAllProducts, $categoryIds);
            $cD = $this->getCompanyDetails();

            // TODO - use fputcsv php function
            if ($proLists) {
                $title = '';
                $tit = 'GLOBAL ITEM DETAILS REPORT';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";
                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["NO", "PRODUCT CODE", "PRODUCT NAME", "PRODUCT CATEGORY", "PRODUCT DESCRIPTION", "ACTIVE/INACTIVE", "QUANTITY"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                foreach ($proLists as $key => $value) {
                    $qty = $value['qty'] ? $value['qty'] : 0.00;
                    $uomAbbr = $value['uomAbbr'] ? $value['uomAbbr'] : '';
                    $status = ($value['pS'] == 1) ? 'Active' : 'Inactive';
                    $proDescription = (is_null($value['pD']) || $value['pD'] == 'NULL') ? '-' : $value['pD'];

                    $in = [$i, $value['pCD'], $value['pName'], $value['categoryName'],
                        $proDescription, $status, $qty . ' ' . $uomAbbr];
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }
            $name = "global_item_details_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $categoryIDs
     * @param type $locIDs
     * @return $categoryLists
     */
    public function getCategoryWiseDetails($categoryIDs = null, $locIDs = null)
    {
        if (isset($categoryIDs) && isset($locIDs)) {
            $categoryLists = array_fill_keys($categoryIDs, '');
            $categoryQtyList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategoryQtyList($categoryIDs, $locIDs);
            foreach ($categoryQtyList as $value) {
                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['pID']);
                $quantity = $this->getProductQuantityViaDisplayUom($value['qty'], $productUom);
                $quantity['quantity'] = ($quantity['quantity'] == 0) ? 0 : $quantity['quantity'];

                $categoryLists[$value['cID']]['cName'] = $value['cName'];
                $categoryLists[$value['cID']]['pD'][$value['pID']] = $value;

                $categoryLists[$value['cID']]['pD'][$value['pID']] = array_merge(
                        $categoryLists[$value['cID']]['pD'][$value['pID']], array(
                    'uomAbbr' => $quantity['uomAbbr'],
                    'qty' => $quantity['quantity']
                        )
                );
            }
            return $categoryLists;
        }
    }

    /**
     * Get category wise stock details
     * @param array $categoryIds
     * @param array $locationIds
     * @return array
     */
    private function getCategoryWiseStockDetails($categoryIds, $locationIds)
    {
        $stockArr = array();
        //get currency symbol
        $currencySymbol = $this->user_session->companyCurrencySymbol;
        foreach ($locationIds as $locationId) {
            //get location
            $location = $this->CommonTable('Core\Model\LocationTable')->getLocationById($locationId);
            $productCategoryList = [];
            foreach ($categoryIds as $categoryId) {
                //get category
                $category = $this->CommonTable('Inventory\Model\CategoryTable')->getCategory($categoryId);
                //get product list

                //get icategory wise item details
                $proData = $this->getCategoryWiseItemDetails($categoryId,$locationId);
                if ($proData) {
                    $productCategoryList[$category->categoryName]['item'] = $proData;
                } 
                //process sub categories
                $subDataArray = [];
                $subDataArray = $this->getSubCategories($categoryId,$locationId, $subDataArray, $category->categoryName);
                foreach ($subDataArray as $key => $value) {
                    $productCategoryList[$category->categoryName][] = $value;
                }
                
            }
            if ($productCategoryList) {
                $stockArr[$location->locationName] = $productCategoryList;
            }
        }
        
        return $stockArr;
    }
    
    /**
    * this function use to get selected category wise item details
    * @param int $categoryId
    * @param int $locationId
    * return array
    **/
    private function getCategoryWiseItemDetails($categoryId,$locationId)
    {
        $productList = $this->CommonTable('Inventory\Model\ProductTable')->getProductListByCategoryId($categoryId);
        $productList = iterator_to_array($productList);
        $productListArr = [];
        foreach ($productList as $product) {
            $locationProduct = $this->CommonTable('Inventory\Model\ProductTable')->getLocationWiseProductDetails($product['productID'], $locationId);
            if ($locationProduct) {
                $productUomList = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($locationProduct['productID']);
                $quantity = $this->getProductQuantityViaDisplayUom($locationProduct['locationProductQuantity'], $productUomList);
                $productQuantity = (is_null($quantity['quantity'])) ? 0 : $quantity['quantity'];
                $productPurchasePrice = $this->getProductUnitPriceViaDisplayUom($locationProduct['defaultPurchasePrice'], $productUomList);
                $productSellingPrice = $this->getProductUnitPriceViaDisplayUom($locationProduct['defaultSellingPrice'], $productUomList);
                $productArr = array(
                    'productCode' => $locationProduct['productCode'],
                    'productName' => $locationProduct['productName'],
                    'productQty' => number_format($productQuantity, 2) . ' ' . $quantity['uomAbbr'],
                    'unitPurchasePrice' => $currencySymbol . ' ' . number_format($productPurchasePrice, 2, '.', ' '),
                    'unitSellingPrice' => $currencySymbol . ' ' . number_format($productSellingPrice, 2, '.', ' '),
                    'reorderQty' => number_format($locationProduct['locationProductReOrderLevel'], 2) . ' ' . $quantity['uomAbbr'],
                    'productQtyWithoutUom' => $productQuantity,
                    'uom' => $quantity['uomAbbr'],
                    'reorderQtyWithoutUom' => $locationProduct['locationProductReOrderLevel']
                );
                $productListArr[] = $productArr;
            }
        }
        return $productListArr;
    }

    
    /**
    * this function use to get sub categories by given perent category. this is a recursive function
    * @param int $categoryId
    * @param int $locationId
    * @param array $subDataArray
    * @param string $parent
    * return array
    **/
    public function getSubCategories($categoryId,$locationId, $subDataArray = [], $parent)
    {
        $returnDataSet = $subDataArray;
        $itemArray = [];
        //get sub categories
        $subCategories = $this->CommonTable('Inventory\Model\CategoryTable')->getCategoryByParentID($categoryId);
        if (empty($subCategories)) {
            return $returnDataSet;
        } else {
            foreach ($subCategories as $key => $value) {
                //get sub wise item Details
                $catName = $parent.' > '.$value['categoryName'];
                $itemArray[$catName] = $this->getCategoryWiseItemDetails($value['categoryID'],$locationId);
                // $itemArray[$value['categoryName']] =  $value['categoryName'];
                $returnDataSet[$parent] = $itemArray;
                $returnDataSet = self::getSubCategories($value['categoryID'],$locationId, $returnDataSet, $catName);

            }
            
        } 
        return $returnDataSet;

    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return HTML content
     */
    public function viewCategoryWiseDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $categoryIds = $request->getPost('categoryIds');
            $locationIds = $request->getPost('locationIds');
            $locNames = $this->getActiveAllLocationsNames();

            //get category wise stock details
            $categoryList = $this->getCategoryWiseStockDetails($categoryIds, $locationIds);

            $translator = new Translator();
            $name = $translator->translate('Category wise Stock Report');
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $categoryListsView = new ViewModel(array(
                'cD' => $companyDetails,
                'lN' => $locNames,
                'cL' => $categoryList,
                'headerTemplate' => $headerViewRender,
            ));
            $categoryListsView->setTemplate('reporting/stock-in-hand-report/generate-category-wise-stock-pdf');

            $this->html = $categoryListsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return path of PDF
     */
    public function generateCategoryWiseStockPdfAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $categoryIds = $request->getPost('categoryIds');
            $locationIds = $request->getPost('locationIds');
            $locNames = $this->getActiveAllLocationsNames();

            //get category wise stock details
            $categoryList = $this->getCategoryWiseStockDetails($categoryIds, $locationIds);

            $translator = new Translator();
            $name = $translator->translate('Category wise Stock Report');

            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewCatgoryWiseDetails = new ViewModel(array(
                'cD' => $companyDetails,
                'lN' => $locNames,
                'cL' => $categoryList,
                'headerTemplate' => $headerViewRender,
            ));

            $viewCatgoryWiseDetails->setTemplate('reporting/stock-in-hand-report/generate-category-wise-stock-pdf');
            $viewCatgoryWiseDetails->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewCatgoryWiseDetails);
            $pdfPath = $this->downloadPDF('category-item-details', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @return path of CSV
     */
    public function generateCategoryWiseStockSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $categoryIds = $request->getPost('categoryIds');
            $locationIds = $request->getPost('locationIds');
            $locNames = $this->getActiveAllLocationsNames();

            //get category wise stock details
            $categoryList = $this->getCategoryWiseStockDetails($categoryIds, $locationIds);

            $cD = $this->getCompanyDetails();

            if (array_filter($categoryList)) {
                $title = '';
                $tit = 'CATEGORY WISE STOCK REPORT';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [ $cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["BRANCH NAME", "CATEGORY NAME", "PRODUCT CODE", "PRODUCT NAME", "PRODUCT QUANTITY", "UOM", "REORDER QUANTITY", "UOM", "PURCHASE PRICE", "SELLING PRICE"];
                $header = implode(",", $arrs);
                $arr = '';

                foreach ($categoryList as $key => $locationProducts) {
                    if (!empty($locationProducts)) {
                        $locationArr = [$key];
                        $arr.=implode(",", $locationArr) . "\n";
                        foreach ($locationProducts as $category => $products) {

                            foreach ($products as $dataKey => $dataValue) {
                                if ($dataKey === 'item') {
                                    
                                    $categoryArr = ['', $category];
                                    $arr.=implode(",", $categoryArr) . "\n";
                                    
                                    foreach ($dataValue as $product){
                                        if ($product) {
                                            $productArr = ['', ''];
                                            $productArr[] = $product['productCode'];
                                            $productArr[] = $product['productName'];
                                            $productArr[] = $product['productQtyWithoutUom'];
                                            $productArr[] = $product['uom'];
                                            $productArr[] = $product['reorderQtyWithoutUom'];
                                            $productArr[] = $product['uom'];
                                            $productArr[] = $product['unitPurchasePrice'];
                                            $productArr[] = $product['unitSellingPrice'];
                                            $arr.=implode(",", $productArr) . "\n";
                                        }
                                    }
                                } else {
                                    foreach ($dataValue as $subValKey => $subValue) {
                                        $productArr = [''];
                                        $productArr[] = $subValKey;
                                        $arr.=implode(",", $productArr) . "\n";

                                        foreach ($subValue as $superSubKey => $superSubValue) {
                                            $productArr = ['', ''];
                                            $productArr[] = $superSubValue['productCode'];
                                            $productArr[] = $superSubValue['productName'];
                                            $productArr[] = $superSubValue['productQtyWithoutUom'];
                                            $productArr[] = $superSubValue['uom'];
                                            $productArr[] = $superSubValue['reorderQtyWithoutUom'];
                                            $productArr[] = $superSubValue['uom'];
                                            $productArr[] = $superSubValue['unitPurchasePrice'];
                                            $productArr[] = $superSubValue['unitSellingPrice'];
                                            $arr.=implode(",", $productArr) . "\n";   
                                        }
                                    }
                                }
                            }
                            $productArr = [''];
                            $arr.=implode(",", $productArr) . "\n";
                        }
                    }
                }
            } else {
                $arr = "no matching records found\n";
            }
        }

        $name = "category_wise_stock_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param type $proIDs
     * @param type $locIDs
     * @return $productLists
     */
    public function getLocationWiseStockDetails($proIDs = null, $locIDs = null, $isAllProducts = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $proIDs = (empty($proIDs)) ? [] : $proIDs;
        if ((isset($proIDs) || $isAllProducts) && isset($locIDs)) {
            // create empty array for products and nest empty locations inside each product
            $locationsEmptyArr = array('locations' => array_fill_keys($locIDs, '')); //create 'locations' element and push locationID's to it
            $productLists = array_fill_keys($proIDs, $locationsEmptyArr);

            $locProductQtyList = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyListByLocID($proIDs, $locIDs, $isAllProducts);

            foreach ($locProductQtyList as $p) {
                $productLists[$p['pID']]['pCD'] = $p['pCD']; //create 'pCD' element and push data to it
                $productLists[$p['pID']]['pID'] = $p['pID'];
                $productLists[$p['pID']]['pName'] = $p['pName'];

                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($p['pID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUom);
                $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                $p['qty'] = $thisqty['quantity'];
                $productLists[$p['pID']]['uomAbbr'] = $thisqty['uomAbbr'];

                $locProduct = array_intersect_key($p, array_flip(array('locID', 'locName', 'qty')));

                $productLists[$p['pID']]['locations'][$p['locID']] = $locProduct;
            }
            //unset locations array when it is empty, from $productLists
            foreach ($productLists as $key => $proData) {
                foreach ($proData['locations'] as $data) {
                    //filter location array,if value is 0 remove that array from $productLists
                    $filertLocations = array_filter($proData['locations']);
                    if (count($filertLocations) == 0) {
                        unset($productLists[$key]);
                    }
                }
            }
            //  add zero values for empty locations
            foreach ($locIDs as $locID) {
                foreach ($productLists as $productKey => $proData) {
                    if (!array_key_exists($locID, $proData['locations'])) {
                        $productLists[$productKey]['locations'][$locID] = array('qty' => '0');
                    }
                }
            }

            return $productLists;
        }
    }

    public function viewLocationWiseStockDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $locIDs = $request->getPost('locationIdList');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $batchSerialFlag = filter_var($request->getPost('batchSerialType'), FILTER_VALIDATE_BOOLEAN);
            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $allLocations = $this->allLocations;
            $locations = array_intersect_key($allLocations, array_flip($locIDs));

            $locNames = array();
            foreach ($locations as $key => $value) {
                $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
            }

            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();

            if($batchSerialFlag){
                $name = $translator->translate('Location wise Batch/Serial Stock Details');
                $productLists = $this->getLocationWiseStockWithBatchDetaials($proIDs, $locIDs, $isAllProducts, $categoryIds);
            } else {
                $name = $translator->translate('Location wise Stock Report');
                $productLists = $this->getLocationWiseStockDetails($proIDs, $locIDs, $isAllProducts);


            }
            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);
                $productListsView = new ViewModel(array(
                    'lL' => $locNames,
                    'lpL' => $productLists,
                    'cD' => $companyDetails,
                    'headerTemplate' => $headerViewRender,
                ));
            if($batchSerialFlag){
                $productListsView->setTemplate('reporting/stock-in-hand-report/generate-location-wise-batch-serial-details-pdf');
            } else {
                $productListsView->setTemplate('reporting/stock-in-hand-report/generate-location-wise-stock-pdf');

            }




            $this->html = $productListsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateLocationWiseStockPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $locIDs = $request->getPost('locationIdList');
            $categoryIds = $request->getPost('categoryIds');
            $batchSerialFlag = filter_var($request->getPost('batchSerialType'), FILTER_VALIDATE_BOOLEAN);
            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $allLocations = $this->allLocations;
            $locations = array_intersect_key($allLocations, array_flip($locIDs));
            $locNames = array();
            foreach ($locations as $key => $value) {
                $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
            }

            $translator = new Translator();
            $cD = $this->getCompanyDetails();

            if($batchSerialFlag){
                $name = $translator->translate('Location wise Batch/Serial Stock Details');
                $productLists = $this->getLocationWiseStockWithBatchDetaials($proIDs, $locIDs, $isAllProducts, $categoryIds);


            } else {
                $name = $translator->translate('Location wise Stock Report');
                $productLists = $this->getLocationWiseStockDetails($proIDs, $locIDs, $isAllProducts);


            }
            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $locationWiseStock = new ViewModel(array(
                'lL' => $locNames,
                'lpL' => $productLists,
                'cD' => $cD,
                'headerTemplate' => $headerViewRender,
            ));

             if($batchSerialFlag){
                $locationWiseStock->setTemplate('reporting/stock-in-hand-report/generate-location-wise-batch-serial-details-pdf');
            } else {
                $locationWiseStock->setTemplate('reporting/stock-in-hand-report/generate-location-wise-stock-pdf');

            }
            $locationWiseStock->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($locationWiseStock);
            $pdfPath = $this->downloadPDF('location-wise-stock', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateLocationWiseStockSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $locIDs = $request->getPost('locationIdList');
            $categoryIds = $request->getPost('categoryIds');
            $batchSerialFlag = filter_var($request->getPost('batchSerialType'), FILTER_VALIDATE_BOOLEAN);

            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $allLocations = $this->allLocations;
            $locations = array_intersect_key($allLocations, array_flip($locIDs));
            $locNames = array();
            foreach ($locations as $key => $value) {
                $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
            }
            $cD = $this->getCompanyDetails();

            if($batchSerialFlag){
                $tit = 'LOCATION WISE BATCH/SERILA STOCK REPORT';
                $productLists = $this->getLocationWiseStockWithBatchDetaials($proIDs, $locIDs, $isAllProducts, $categoryIds);

            } else {
                $tit = 'LOCATION WISE STOCK REPORT';
                $productLists = $this->getLocationWiseStockDetails($proIDs, $locIDs, $isAllProducts);

            }
            if ($productLists) {
                $title = '';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";
                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";
                $arr = '';

                if($batchSerialFlag){
                    $arrs = array("LOCATION","PRODUCT CODE","PRODUCT NAME","PRODUCT CATEGORY","BATCH/SERIAL TYPE","BATCH/SERIAL CODE","QTY","","UNIT PRICE", "TOTAL");
                    $header = implode(",", $arrs);
                    foreach ($locNames as $locID => $locName) {
                        $locationTotal = 0;
                        $serialIDs = [];
                        foreach ($productLists as $key => $proData) {
                            $uomAbbr = $proData['uomAbbr'] ? $proData['uomAbbr'] : '';
                            $productSerialID = (is_null($proData['productSerialID'])) ? '-' : $proData['productSerialID'];
                            $productBatchID = (is_null($proData['productBatchID'])) ? '-' : $proData['productBatchID'];
                            if($proData['locationID'] == $locID && !$serialIDs[$productSerialID."_".$productBatchID]){
                                $serialIDs[$productSerialID."_".$productBatchID] = true;
                                if($proData['productBatchCode'] != '' && $proData['productBatchQuantity'] != ''){
                                    $qty = $proData['productBatchQuantity'];
                                    $type = 'BATCH';
                                    $typeCode = $proData['productBatchCode'];
                                } else {
                                    $qty = 1;
                                    $type = 'SERIAL';
                                    $typeCode = $proData['productSerialCode'];
                                }
                                $totalCost = floatval($proData['itemInPrice'] - $proData['itemInDiscount']) * floatval($qty);
                                $grandTotal += $totalCost;
                                $locationTotal += $totalCost;

                                $in = array(
                                    0 => $locName,
                                    1 => str_replace(',', "", $proData['productCode']),
                                    2 => str_replace(',', "", $proData['productName']),
                                    3 => str_replace(',', "", $proData['categoryName']),
                                    4 => $type,
                                    5 => $typeCode,
                                    6 => $qty,
                                    7 => $uomAbbr,
                                    8 => $proData['itemInPrice'] - $proData['itemInDiscount'],
                                    9 => $totalCost
                                    );
                                $arr.=implode(",", $in) . "\n";
                            }
                        }
                        $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => '',
                            8 => 'Location Wise Total Cost',
                            9 => $locationTotal
                            );
                        $arr.=implode(",", $in) . "\n";
                    }
                    $in = array(
                            0 => '',
                            1 => '',
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => '',
                            6 => '',
                            7 => '',
                            8 => 'Total Cost',
                            9 => $grandTotal
                            );
                    $arr.=implode(",", $in) . "\n";
                } else {

                    $arrs = array("NO", "PRODUCT CODE", "PRODUCT NAME", "UOM");
                    ksort($locNames);
                    $arrsLoc = array_merge($arrs, $locNames);

                    $header = implode(",", $arrsLoc);
                    $i = 1;
                    foreach ($productLists as $proID => $proData) {
                        $uomAbbr = $proData['uomAbbr'] ? $proData['uomAbbr'] : '';
                        $in = [$i, $proData['pCD'], str_replace(',', "", $proData['pName']), $uomAbbr];

                        $j = 0;
                        $allData = [];
                        ksort($proData['locations']);
                        foreach ($proData['locations'] as $locID => $locData) {
                            $qty = $locData['qty'] ? $locData['qty'] : 0.00;

                            $pro = [
                                $j => $qty
                            ];

                            $j++;
                            $allData = array_merge($allData, $pro);
                        }

                        $i++;
                        $in = array_merge($in, $allData);
                        $arr.=implode(",", $in) . "\n";
                    }
                }

            } else {
                $arr = "no matching records found\n";
            }
        }

        $name = "location_wise_stock_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get issued invoice and delivery note details in given date range
     * @param Array $locIDs
     * @param Date $fromDate
     * @param Date $toDate
     * @return Array $salesAndDeliveryData
     */
    public function issuedInvoiceItemDetails($locIDs = null, $fromDate = null, $toDate = null)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');
        if (isset($fromDate) && isset($toDate) && isset($locIDs)) {
            $salesAndDeliveryData = [];

            //getting DeliveryNote Ids and put into $deliNtIds
            $deliveryNotIds = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getIssuedDeliveryNotesIds($fromDate, $toDate, $locIDs);
            foreach ($deliveryNotIds as $id) {
                $deliNtIds[$id] = array();
            }

            //getting delivery note details which regarding to only for deliveryNote
            //what there has no any deliveryNote convert to Invoice data
            $deliNtData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getIssuedDeliveryNotesDetails($fromDate, $toDate, $locIDs);

            //get all uom details
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductIds();

            //set basic data set

            //get all location product details
            $locationProductData = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProductsDetails();

            //set location product details to the array
            $locProAllDetails = [];
            $locProIDs = [];
            foreach ($locationProductData as $key => $value) {

                $locProIDs[] = $value['locationProductID'];
                $locProAllDetails[$value['productID']][$value['locationID']] = $value;

            }

            //get all itemOut details
            $itemInData = $this->CommonTable("Inventory\Model\ItemOutTable")->getInvoiceDeliveryNoteDetails($locProIDs);

            //set itemout data to the array
            $allOutDetails = [];
            foreach ($itemInData as $key => $value) {
                $allOutDetails[$value['itemOutDocumentType']][$value['itemOutDocumentID']][$value['itemOutLocationProductID']][] = $value;
            }


            //delivery note details put into $deliNtIds array according to deliveryNoteID
            foreach ($deliNtData as $d) {
                $locProDetails = $locProAllDetails[$d['pID']][$d['locID']];

                $deliveryNoteInCost = $this->_getTotalItemInCost($locProDetails, $allOutDetails['Delivery Note'][$d['deliveryNoteID']][$locProDetails['locationProductID']]);

                $deliNtIds[$d['deliveryNoteID']]['dnCD'] = $d['deliveryNoteCode'];
                $deliNtIds[$d['deliveryNoteID']]['dnDate'] = $d['deliveryNoteDeliveryDate'];

                $thisqty = $this->getProductQuantityViaDisplayUom($d['qty'], $productUom[$d['pID']]);
                $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                $deliNtIds[$d['deliveryNoteID']]['data'][$d['pID']] = array(
                    'pCD' => $d['pCD'],
                    'pName' => $d['pName'],
                    'qty' => $thisqty['quantity'],
                    'uomAbbr' => $thisqty['uomAbbr'],
                    'deliveryNoteInCost' => $deliveryNoteInCost
                );

                $salesAndDeliveryData[$d['locID']]['locName'] = $d['locationName'];
                $salesAndDeliveryData[$d['locID']]['locCode'] = $d['locationCode'];
                $salesAndDeliveryData[$d['locID']]['dnData'] = $deliNtIds;
            }



            $invoiceIds = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesInvoiceIds($fromDate, $toDate, $cancelInvoice = 5, $editedInvoice = 10, $locIDs);
            foreach ($invoiceIds as $s) {
                $salesInvoiceIds[$s] = array();
            }

            //getting item data from salesInvoice table
            //this data has contain deliveryNote and invoice Details
            $salesInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesInvoiceDetails($fromDate, $toDate, FALSE, $locIDs);

            foreach ($salesInvoiceData as $i) {

                $locProDetails = $locProAllDetails[$i['pID']][$i['locID']];

                //get itemIn cost related to delivery note product
                $invoiceInCost = $this->_getTotalItemInCost($locProDetails, $allOutDetails['Sales Invoice'][$i['salesInvoiceID']][$locProDetails['locationProductID']]);

                // $invoiceInCost = $this->_getItemInCost($i['pID'], $i['locID'], $i['salesInvoiceID'], $documentType = "Sales Invoice");

                $salesInvoiceIds[$i['salesInvoiceID']]['sInCD'] = $i['salesInvoiceCode'];
                $salesInvoiceIds[$i['salesInvoiceID']]['dnCD'] = $i['deliveryNoteCode'];
                $salesInvoiceIds[$i['salesInvoiceID']]['sInDate'] = $i['salesInvoiceIssuedDate'];
                $salesInvoiceIds[$i['salesInvoiceID']]['data'][$i['pID']] = array(
                    'pCD' => $i['pCD'],
                    'pName' => $i['pName'],
                    'qty' => $i['salesInvoiceProductQuantity'],
                    'invoiceInCost' => $invoiceInCost
                );

                $salesInvoiceIds[$i['salesInvoiceID']]['data'][$i['pID']]['uomAbbr'] = '';
                // $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomByProductID($i['pID']);
                foreach ($productUom[$i['pID']] as $p) {
                    if ($p['productUomConversion'] == 1) {
                        $salesInvoiceIds[$i['salesInvoiceID']]['data'][$i['pID']]['uomAbbr'] = $p['uomAbbr'];
                        break;
                    }
                }
                $salesAndDeliveryData[$i['locID']]['locName'] = $i['locationName'];
                $salesAndDeliveryData[$i['locID']]['locCode'] = $i['locationCode'];
                $salesAndDeliveryData[$i['locID']]['invData'] = $salesInvoiceIds;
            }

            return $salesAndDeliveryData;
        }
    }
    /**
     * given cost value of delivery note product
     * @param array $locationProductData
     * @param array $itemInData
     * @return float $debtNtInCost
    **/

    private function _getTotalItemInCost($locationProductData, $itemInData)
    {

        $itemInCost = 0;
        $totalDelitNtInQty = 0;
        foreach ($itemInData as $v) {

            $inQty = isset($v['itemOutQty']) ? $v['itemOutQty'] : 0;
            $totalDelitNtInQty += $inQty;
            if ($v['itemOutBatchID'] != NULL && $v['itemOutSerialID'] == NULL) {
                $totalDelitNtInQty = 1;
            } else if ($v['itemOutBatchID'] == NULL && $v['itemOutSerialID'] == NULL) {
                $totalDelitNtInQty = 1;
            }
            $inPrice = isset($v['itemInPrice']) ? $v['itemInPrice'] : 0;
            //GRN and Purchase Invoice has only percentage value(%) for discount
            //ItemIn table has discount value(not percentage value)
            $inDiscountValue = isset($v['itemInDiscount']) ? $v['itemInDiscount'] : 0;
            $totalTaxAmount = 0;

            if ($v['itemInDocumentType'] == "Goods Received Note") {
                $itemInTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($v['itemInDocumentID'], $locationProductData['locationProductID']);

                if (!empty($itemInTax)) {
                    foreach ($itemInTax as $t) {
                        $grnProductTotalQty = ($t['grnProductTotalQty'] == NULL) ? 1 : $t['grnProductTotalQty'];
                        $totalTaxAmount += $t['grnTaxAmount'] / $grnProductTotalQty;
                    }
                }
            } else if ($v['itemInDocumentType'] == "Payment Voucher") {
                $itemInTax = $this->CommonTable("Inventory\Model\PurchaseInvoiceProductTaxTable")->getPurchaseInvoiceProductTax($v['itemInDocumentID'], $locationProductData['locationProductID']);

                // $totalTaxAmount = 0;
                if (!empty($itemInTax)) {
                    foreach ($itemInTax as $t) {
                        $purchaseInvoiceProductTotalQty = ($t['purchaseInvoiceProductTotalQty'] == NULL) ? 1 : $t['purchaseInvoiceProductTotalQty'];
                        $totalTaxAmount += $t['purchaseInvoiceTaxAmount'] / $purchaseInvoiceProductTotalQty;
                    }
                }
            }
            $itemInCost += ($inPrice - ($inDiscountValue)) * $v['itemOutQty'];
        }

        return $itemInCost;

    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * given cost value of delivery note product
     * @param int $productID
     * @param int $locationID
     * @param int $documentID
     * @param string $documentType
     * @return float $debtNtInCost
     */
    private function _getItemInCost($productID, $locationID, $documentID, $documentType)
    {
        if (isset($productID) && isset($locationID) && isset($documentID)) {
            $locationProductData = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProductByProIdLocId($productID, $locationID);
            $itemInData = $this->CommonTable("Inventory\Model\ItemOutTable")->getInvoiceDeliveryNoteDetails($documentID, $documentType, $locationProductData->locationProductID);

            $itemInCost = 0;
            $totalDelitNtInQty = 0;
            foreach ($itemInData as $v) {

                $inQty = isset($v['itemOutQty']) ? $v['itemOutQty'] : 0;
                $totalDelitNtInQty += $inQty;
                if ($v['itemOutBatchID'] != NULL && $v['itemOutSerialID'] == NULL) {
                    $totalDelitNtInQty = 1;
                } else if ($v['itemOutBatchID'] == NULL && $v['itemOutSerialID'] == NULL) {
                    $totalDelitNtInQty = 1;
                }
                $inPrice = isset($v['itemInPrice']) ? $v['itemInPrice'] : 0;
                //GRN and Purchase Invoice has only percentage value(%) for discount
                //ItemIn table has discount value(not percentage value)
                $inDiscountValue = isset($v['itemInDiscount']) ? $v['itemInDiscount'] : 0;

                if ($v['itemInDocumentType'] == "Goods Received Note") {
                    $itemInTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($v['itemInDocumentID'], $locationProductData->locationProductID);

                    $totalTaxAmount = 0;
                    if (!empty($itemInTax)) {
                        foreach ($itemInTax as $t) {
                            $grnProductTotalQty = ($t['grnProductTotalQty'] == NULL) ? 1 : $t['grnProductTotalQty'];
                            $totalTaxAmount += $t['grnTaxAmount'] / $grnProductTotalQty;
                        }
                    }
                } else if ($v['itemInDocumentType'] == "Payment Voucher") {
                    $itemInTax = $this->CommonTable("Inventory\Model\PurchaseInvoiceProductTaxTable")->getPurchaseInvoiceProductTax($v['itemInDocumentID'], $locationProductData->locationProductID);

                    $totalTaxAmount = 0;
                    if (!empty($itemInTax)) {
                        foreach ($itemInTax as $t) {
                            $purchaseInvoiceProductTotalQty = ($t['purchaseInvoiceProductTotalQty'] == NULL) ? 1 : $t['purchaseInvoiceProductTotalQty'];
                            $totalTaxAmount += $t['purchaseInvoiceTaxAmount'] / $purchaseInvoiceProductTotalQty;
                        }
                    }
                }
                $itemInCost += ($inPrice - ($inDiscountValue + $totalTaxAmount)) * $v['itemOutQty'];
            }

            return $itemInCost;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param type $sbProIDs
     * @param type $fromDate
     * @param type $toDate
     * @return $products
     */
    public function getItemWiseExpireDetails($sbProIDs = null, $fromDate = null, $toDate = null, $isAllProducts = false)
    {
        // get batch related details
        $resProBatch = $this->CommonTable('Inventory\Model\ProductTable')->getExpiredBatchData($sbProIDs, $fromDate, $toDate, $isAllProducts);
        // get serial related details
        $serialDetails = $this->CommonTable('Inventory\Model\ProductTable')->getExpiredSerialData($sbProIDs, $fromDate, $toDate);
        
        $allDetails = array_merge($resProBatch,$serialDetails);

        $productBatchs = array();
        if ($allDetails != null) {
            foreach ($allDetails as $bValue) {
                
                if ($bValue['productSerialSold'] == 0) {
                    if ((isset($bValue['sExDate']) && isset($bValue['bExDate']))) {
                        $expireDate = $bValue['sExDate'];
                    } else if ((isset($bValue['sExDate']) && !isset($bValue['bExDate']))) {
                        $expireDate = $bValue['sExDate'];
                    } else {
                        $expireDate = $bValue['bExDate'];
                    }
                    //set quantity and uomAbbr according to display UOM
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($bValue['pID']);
                    $thisqty = $this->getProductQuantityViaDisplayUom($bValue['qty'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                    $qty = isset($bValue['productSerialID']) ? 1 : $thisqty['quantity'];

                    $bValue['bExDate'] = $expireDate;
                    $bValue['qty'] = $qty;
                    $productBatchs[$bValue['pID']]['pName'] = $bValue['pName'];
                    $productBatchs[$bValue['pID']]['pCD'] = $bValue['pCD'];
                    //data puts as serial and batch wise

                    if (isset($bValue['productSerialID']) && !isset($bValue['productBatchID'])) {
                        $productBatchs[$bValue['pID']]['sData'][0]['data'][$bValue['productSerialID']] = $bValue;
                        $productBatchs[$bValue['pID']]['sData'][0]['bCD'] = $bValue['bCD'];
                    } else if (isset($bValue['productSerialID']) && isset($bValue['productBatchID'])) {
                        $productBatchs[$bValue['pID']]['sData'][$bValue['productBatchID']]['data'][$bValue['productSerialID']] = $bValue;
                        $productBatchs[$bValue['pID']]['sData'][$bValue['productBatchID']]['bCD'] = $bValue['bCD'];
                    } else {
                        $productBatchs[$bValue['pID']]['bData'][$bValue['productBatchID']] = $bValue;
                    }

                    $productBatchs[$bValue['pID']]['uomAbbr'] = $thisqty['uomAbbr'];
                }
            }
            
        }
        
        return $productBatchs;
    }

    public function viewItemWiseExpireAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $sbProIDs = $request->getPost('sbProductIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Item wise expire stock Report');
            $period = $fromDate . ' - ' . $toDate;

            $products = $this->getItemWiseExpireDetails($sbProIDs, $fromDate, $toDate, $isAllProducts);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $bSExpiredView = new ViewModel(array(
                'bS' => $products,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));
            $bSExpiredView->setTemplate('reporting/stock-in-hand-report/generate-item-wise-expire-pdf');

            $this->html = $bSExpiredView;
            $this->msg = $this->getMessage('INFO_STOCKHAND_EXPIRED_ITEM');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateItemWiseExpirePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $sbProIDs = $request->getPost('sbProductIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $translator = new Translator();
            $name = $translator->translate('Item wise expire stock Report');
            $period = $fromDate . ' - ' . $toDate;
            $products = $this->getItemWiseExpireDetails($sbProIDs, $fromDate, $toDate, $isAllProducts);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewItemWiseExpireList = new ViewModel(array(
                'bS' => $products, 'cD' => $companyDetails, 'headerTemplate' => $headerViewRender,
            ));

            $viewItemWiseExpireList->setTemplate('reporting/stock-in-hand-report/generate-item-wise-expire-pdf');
            $viewItemWiseExpireList->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewItemWiseExpireList);
            $pdfPath = $this->downloadPDF('item-wise-expired-dates-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateItemWiseExpireSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $sbProIDs = $request->getPost('sbProductIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $productsData = $this->getItemWiseExpireDetails($sbProIDs, $fromDate, $toDate, $isAllProducts);
            $cD = $this->getCompanyDetails();

            if ($productsData) {
                $title = '';
                $tit = 'ITEM WISE EXPIRE STOCK REPORT';
                $in = ["", "", "", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["NO", "PRO. NAME - CODE", "BATCH CODE", "SERIAL CODE", "EXP. DATE", "QUANTITY"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;

                foreach ($productsData as $proID => $proData) {
                    $in = [$i, $proData['pName'] . '-' . $proData['pCD']];
                    $arr.=implode(",", $in) . "\n";

                    foreach ($proData['sData'] as $lpLID => $data) {
                        $uomAbbr = $proData['uomAbbr'] ? $proData['uomAbbr'] : '';
                        $totalQty = 0;
                        $count = 0;
                        foreach ($data['data'] as $sData) {
                            $sCD = $sData['sCD'] ? $sData['sCD'] : '-';
                            $qty = $sData['qty'] ? $sData['qty'] : 0.00;
                            $totalQty += $qty;
                            if ($count == 0 && $data['bCD']) {
                                $in = ['', '', $data['bCD'], $sCD,
                                    $sData['exDate'],
                                    $qty . '' . $uomAbbr
                                ];
                                $arr.=implode(",", $in) . "\n";
                                $count++;
                            } else {
                                $in = ['', '', '',
                                    $sCD,
                                    $sData['exDate'],
                                    $qty . '' . $uomAbbr
                                ];
                                $arr.=implode(",", $in) . "\n";
                            }
                        }
                        $in = ['', '', '', '', 'Total Quantity for '.$proData['pName'], $totalQty . '' . $uomAbbr];
                        $arr.=implode(",", $in) . "\n";
                    }
                    foreach ($proData['bData'] as $lpLID => $bData) {
                        $uomAbbr = $proData['uomAbbr'] ? $proData['uomAbbr'] : '';
                        $qty = $bData['qty'] ? $bData['qty'] : 0.00;
                        $in = [ '', '', $bData['bCD'], '-', $bData['bExDate'], $qty . '' . $uomAbbr];
                        $arr.=implode(",", $in) . "\n";
                    }
                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }
        }

        $name = "item_wise_expired_stock";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return Html out put
     */
    public function viewItemWiseMinimumInventoryLevelAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $locIDs = $request->getPost('locationIdList');
            $isMILType = ($request->getPost('isMILType') == 'true') ? true : false;
            $isReach = ($request->getPost('isReach') == 'true') ? true : false;

            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }

            if ($isMILType) {
                $proLists = $this->getItemWiseMinimumInventoryLevelDetails($proIDs, $isAllProducts, $locIDs, $isReach);
            } else {

                $proLists = $this->getItemWiseReorderLevelDetails($proIDs, $isAllProducts, $locIDs, $isReach);
            }

            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            // $name = $translator->translate('Item wise Minimum Inventory Level Report');
            if ($isMILType == true) {
                $name = $translator->translate('Item wise Minimum Inventory Level Report');
            } else {
                $name = $translator->translate('Item wise Reorder Level Report');
            }

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $itemWMInventoryView = new ViewModel(array(
                'pL' => $proLists,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'isMILType' => $isMILType
            ));
            $itemWMInventoryView->setTemplate('reporting/stock-in-hand-report/generate-item-wise-minimum-inventory-level-pdf');

            $this->html = $itemWMInventoryView;
            $this->msg = $this->getMessage('INFO_STOCKHAND_MINI_INVENLEVEL_ITEM');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateItemWiseMinimumInventoryLevelPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $locIDs = $request->getPost('locationIdList');
            $isMILType = ($request->getPost('isMILType') == 'true') ? true : false;
            $isReach = ($request->getPost('isReach') == 'true') ? true : false;
            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $translator = new Translator();


            if ($isMILType == true) {
                $name = $translator->translate('Item wise Minimum Inventory Level Report');
            } else {
                $name = $translator->translate('Item wise Reorder Level Report');
            }
            if ($isMILType) {
                $proLists = $this->getItemWiseMinimumInventoryLevelDetails($proIDs, $isAllProducts, $locIDs, $isReach);
            } else {

                $proLists = $this->getItemWiseReorderLevelDetails($proIDs, $isAllProducts, $locIDs, $isReach);
            }

            // $proLists = $this->getItemWiseMinimumInventoryLevelDetails($proIDs, $isAllProducts, $locIDs);
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewMinimumInventoryLevel = new ViewModel(array(
                'pL' => $proLists, 'cD' => $cD, 'headerTemplate' => $headerViewRender, "isMILType" => $isMILType
            ));

            $viewMinimumInventoryLevel->setTemplate('reporting/stock-in-hand-report/generate-item-wise-minimum-inventory-level-pdf');
            $viewMinimumInventoryLevel->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewMinimumInventoryLevel);
            $pdfPath = $this->downloadPDF('item-wise-minimum-inventory-level-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @return csv file
     */
    public function generateItemWiseMinimumInventoryLevelSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $locIDs = $request->getPost('locationIdList');
            $isMILType = ($request->getPost('isMILType') == 'true') ? true : false;
            $isReach = ($request->getPost('isReach') == 'true') ? true : false;
            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            if ($isMILType) {
                $proLists = $this->getItemWiseMinimumInventoryLevelDetails($proIDs, $isAllProducts, $locIDs, $isReach);
            } else {

                $proLists = $this->getItemWiseReorderLevelDetails($proIDs, $isAllProducts, $locIDs, $isReach);
            }

            $cD = $this->getCompanyDetails();

            if ($proLists) {
                $title = '';
                

                if ($isMILType == true) {
                    $tit = 'ITEM WISE MINIMUM INVENTORY LEVEL REPORT';
                    $arrs = ["NO", "LOCATION NAME - CODE", "PRO. NAME", "PRO. CODE", "MI LEVEL","", "AVIALABLE QUANTITY",""];
                } else {
                    $tit = 'ITEM WISE REODER LEVEL REPORT';
                    $arrs = ["NO", "LOCATION NAME - CODE", "PRO. NAME", "PRO. CODE", "ROL LEVEL","", "AVIALABLE QUANTITY",""];
                }
                $in = ['', '', $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;

                foreach ($proLists as $lpLID => $data) {
                    if (!empty($data['pD'])) {
                        $in = [ $i, $data['locNameCD']];
                        $arr.=implode(",", $in) . "\n";
                        foreach ($data['pD'] as $pData) {
                            $qty = $pData['qty'] ? $pData['qty'] : 0.00;
                            if ($isMILType == true) {
                                $miL = $pData['miL'] ? $pData['miL'] : 0.00;
                            } else {

                                $miL = $pData['roL'] ? $pData['roL'] : 0.00;
                            }

                            $uomAbbr = $pData['uomAbbr'] ? $pData['uomAbbr'] : '';
                            $in = [ '', '', $pData['pName'],
                                $pData['pCD'],
                                $miL,
                                $uomAbbr,
                                $qty,
                                $uomAbbr
                            ];
                            $arr.=implode(",", $in) . "\n";
                        }
                    }
                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }
            $name = "item_wise_minimum_inventory_level";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @param type $proIDs
     * @return $proLists
     */
    public function getItemWiseMinimumInventoryLevelDetails($proIDs = null, $isAllProducts = false ,$locationIDs, $isReach = true)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        if (isset($proIDs) || $isAllProducts) {
            $allLocations = $this->allLocations;
            foreach ($allLocations as $key => $value) {
                $locIDs[] = $key;
            }
            $locationWiseData = array_fill_keys($locIDs, '');

            $resPro = $this->CommonTable('Inventory\Model\LocationProductTable')->getItemWiseMinimumInvenLevelDetails($proIDs, $locationIDs, $isAllProducts, $isReach);
            foreach ($resPro as $row) {
                $locationWiseData[$row['locID']]['locNameCD'] = $row['locName'] . '-' . $row['locationCode'];
                $locationWiseData[$row['locID']]['pD'][$row['pID']]['uomAbbr'] = '';
                $locationWiseData[$row['locID']]['pD'][$row['pID']]['pCD'] = $row['pCD'];
                $locationWiseData[$row['locID']]['pD'][$row['pID']]['pName'] = $row['pName'];

                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['pID']);
                $quantity = $this->getProductQuantityViaDisplayUom($row['qty'], $productUom);
                $quantity['quantity'] = ($quantity['quantity'] == 0) ? 0 : $quantity['quantity'];

                $miLQty = $this->getProductQuantityViaDisplayUom($row['miL'], $productUom);
                $miLQty['quantity'] = ($miLQty['quantity'] == 0) ? 0 : $miLQty['quantity'];

                $locationWiseData[$row['locID']]['pD'][$row['pID']]['qty'] = $quantity['quantity'];
                $locationWiseData[$row['locID']]['pD'][$row['pID']]['miL'] = $miLQty['quantity'];
                $locationWiseData[$row['locID']]['pD'][$row['pID']] ['uomAbbr'] = $quantity['uomAbbr'];
            }

            $pLists = array_filter($locationWiseData);
            return $pLists;
        }
    }

    public function getItemWiseReorderLevelDetails($proIDs = null, $isAllProducts = false ,$locationIDs, $isReach)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        if (isset($proIDs) || $isAllProducts) {
            $allLocations = $this->allLocations;
            foreach ($allLocations as $key => $value) {
                $locIDs[] = $key;
            }
            $locationWiseData = array_fill_keys($locIDs, '');

            $resPro = $this->CommonTable('Inventory\Model\LocationProductTable')->getItemWiseReorderLevelDetails($proIDs, $locationIDs, $isAllProducts, $isReach);
            foreach ($resPro as $row) {
                $locationWiseData[$row['locID']]['locNameCD'] = $row['locName'] . '-' . $row['locationCode'];
                $locationWiseData[$row['locID']]['pD'][$row['pID']]['uomAbbr'] = '';
                $locationWiseData[$row['locID']]['pD'][$row['pID']]['pCD'] = $row['pCD'];
                $locationWiseData[$row['locID']]['pD'][$row['pID']]['pName'] = $row['pName'];

                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['pID']);
                $quantity = $this->getProductQuantityViaDisplayUom($row['qty'], $productUom);
                $quantity['quantity'] = ($quantity['quantity'] == 0) ? 0 : $quantity['quantity'];

                $roLQty = $this->getProductQuantityViaDisplayUom($row['roL'], $productUom);
                $roLQty['quantity'] = ($roLQty['quantity'] == 0) ? 0 : $roLQty['quantity'];

                $locationWiseData[$row['locID']]['pD'][$row['pID']]['qty'] = $quantity['quantity'];
                $locationWiseData[$row['locID']]['pD'][$row['pID']]['roL'] = $roLQty['quantity'];
                $locationWiseData[$row['locID']]['pD'][$row['pID']] ['uomAbbr'] = $quantity['uomAbbr'];
            }

            $pLists = array_filter($locationWiseData);
            return $pLists;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function viewCategoryWiseMinimumInventoryLevelAction()
    {
        $request = $this->getRequest();
        $translator = new Translator();

        if ($request->isPost()) {
            $categoryIDs = json_decode($request->getPost('categoryIds'));
            $isMILType = ($request->getPost('isMILType') == 'true') ? true : false;
            $isReach = ($request->getPost('isReach') == 'true') ? true : false;

            if ($isMILType) {
                $categoryLists = $this->getCategoryWiseMinimumInventoryLevelDetails($categoryIDs, $isReach);
                $name = $translator->translate('Location wise Category Minimum Inventory Level Report');

            } else {
                $categoryLists = $this->getCategoryWiseReorderLevelDetails($categoryIDs, $isReach);
                $name = $translator->translate('Location wise Category Reorder Level Report');
            }


            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $categoryWMInventoryView = new ViewModel(array(
                'cL' => $categoryLists,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'isMILType' => $isMILType
            ));
            $categoryWMInventoryView->setTemplate('reporting/stock-in-hand-report/generate-category-wise-minimum-inventory-level-pdf');

            $this->html = $categoryWMInventoryView;
            $this->msg = $this->getMessage('INFO_STOCKHAND_MINI_INVENLEVEL_CATG');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateCategoryWiseMinimumInventoryLevelPdfAction()
    {
        $request = $this->getRequest();
        $translator = new Translator();
        if ($request->isPost()) {

            $categoryIDs = json_decode($request->getPost('categoryIds'));
            $isMILType = ($request->getPost('isMILType') == 'true') ? true : false;
            $isReach = ($request->getPost('isReach') == 'true') ? true : false;

            if ($isMILType) {
                $categoryLists = $this->getCategoryWiseMinimumInventoryLevelDetails($categoryIDs, $isReach);
                $name = $translator->translate('Location wise Category Minimum Inventory Level Report');

            } else {
                $categoryLists = $this->getCategoryWiseReorderLevelDetails($categoryIDs, $isReach);
                $name = $translator->translate('Location wise Category Reorder Level Report');
            }
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);


            $viewCateWiseMinimumInventoryLevel = new ViewModel(array(
                'cD' => $companyDetails,
                'cL' => $categoryLists,
                'headerTemplate' => $headerViewRender,
                'isMILType' => $isMILType
            ));

            $viewCateWiseMinimumInventoryLevel->setTemplate('reporting/stock-in-hand-report/generate-category-wise-minimum-inventory-level-pdf');
            $viewCateWiseMinimumInventoryLevel->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewCateWiseMinimumInventoryLevel);
            $pdfPath = $this->downloadPDF('category-wise-minimum-inventory-level-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun<sandun@thinkcube.com>
     * @return csv file
     */
    public function generateCategoryWiseMinimumInventoryLevelSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $categoryIDs = json_decode($request->getPost('categoryIds'));
            $isMILType = ($request->getPost('isMILType') == 'true') ? true : false;
            $isReach = ($request->getPost('isReach') == 'true') ? true : false;

            if ($isMILType) {
                $categoryLists = $this->getCategoryWiseMinimumInventoryLevelDetails($categoryIDs, $isReach);

            } else {
                $categoryLists = $this->getCategoryWiseReorderLevelDetails($categoryIDs, $isReach);
            }

            $cD = $this->getCompanyDetails();

            if ($categoryLists) {
                $title = '';
                if ($isMILType) {
                    $tit = 'CATEGORY WISE MINIMUM INVENTORY LEVEL REPORT';
                    $arrs = ["NO", "LOCATION NAME - CODE", "CATEGORY NAME", "PRODUCT CODE", "PRODUCT NAME", "MI LEVEL"," ","AVIALABLE QUANTITY", " ", "LOCATION WISE SALE PRICE"];
                } else {
                    $arrs = ["NO", "LOCATION NAME - CODE", "CATEGORY NAME", "PRODUCT CODE", "PRODUCT NAME", "ROL LEVEL"," ","AVIALABLE QUANTITY", " ", "LOCATION WISE SALE PRICE"];
                    $tit = 'CATEGORY WISE REODER LEVEL REPORT';
                }
                $in = ['', '', $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;

                foreach ($categoryLists as $cId => $c) {
                    if (!empty($c['cD'])) {
                        $in = [$i, $c['locNameCD']];
                        $arr.=implode(",", $in) . "\n";
                        foreach ($c['cD'] as $cData) {
                            foreach ($cData as $pId => $p) {
                                $qty = $p['qty'] ? $p['qty'] : 0.00;
                                if ($isMILType) {
                                    $miL = $p['miL'] ? $p['miL'] : 0.00;
                                } else {
                                    $miL = $p['roL'] ? $p['roL'] : 0.00;
                                }

                                $uomAbbr = $p['uomAbbr'] ? $p['uomAbbr'] : '';
                                $unitPrice = $p['defPrice'] ? $p['systemCurrency'] . ' ' . $p['defPrice'] : '';
                                $in = ['', '', $p['cName'], $p['pCD'],
                                    $p['pName'],
                                    $miL,
                                    $uomAbbr,
                                    $qty,
                                    $uomAbbr,
                                    ($unitPrice)];
                                $arr.=implode(",", $in) . "\n";
                            }
                        }
                    }

                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }
            $name = "category_wise_minimum_inventory_level";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param type $categoryIds
     * @return $cList
     */
    public function getCategoryWiseMinimumInventoryLevelDetails($categoryIds = null, $isReach)
    {
        if (isset($categoryIds)) {
            $locIDs = $this->getActiveAllLocationsIds();
            $categoryListWiseLocation = array_fill_keys($locIDs, '');
            $currency = $this->user_session->companyCurrencySymbol;
            $res = $this->CommonTable('Inventory/Model/CategoryTable')->getCategoryWiseMinimumInvenLevelDetails($categoryIds, $locIDs, $isReach);
            foreach ($res as $row) {
                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['pID']);
                $quantity = $this->getProductQuantityViaDisplayUom($row['qty'], $productUom);
                $quantity['quantity'] = ($quantity['quantity'] == 0) ? 0 : $quantity['quantity'];

                $miLQty = $this->getProductQuantityViaDisplayUom($row['miL'], $productUom);
                $miLQty['quantity'] = ($miLQty['quantity'] == 0) ? 0 : $miLQty['quantity'];

                $defaultUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['locDefaultPrice'], $productUom);

                $categoryListWiseLocation[$row['locID']]['locNameCD'] = $row['locName'] . '-' . $row['locCD'];
                $categoryListWiseLocation[$row['locID']]['cD'][$row['categoryID']][$row['pID']] = array(
                    'cName' => $row['categoryName'],
                    'pName' => $row['pName'],
                    'pCD' => $row['pCD'],
                    'qty' => $quantity['quantity'],
                    'miL' => $miLQty['quantity'],
                    'uomAbbr' => $quantity['uomAbbr'],
                    'defPrice' => $defaultUnitPrice,
                    'systemCurrency' => $currency
                );
            }

            $cList = array_filter($categoryListWiseLocation);
            return $cList;
        }
    }

    public function getCategoryWiseReorderLevelDetails($categoryIds = null, $isReach)
    {
        if (isset($categoryIds)) {
            $locIDs = $this->getActiveAllLocationsIds();
            $categoryListWiseLocation = array_fill_keys($locIDs, '');
            $currency = $this->user_session->companyCurrencySymbol;
            $res = $this->CommonTable('Inventory/Model/CategoryTable')->getCategoryWiseReorderLevelDetails($categoryIds, $locIDs, $isReach);
            foreach ($res as $row) {
                //set quantity and uomAbbr according to display UOM
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['pID']);
                $quantity = $this->getProductQuantityViaDisplayUom($row['qty'], $productUom);
                $quantity['quantity'] = ($quantity['quantity'] == 0) ? 0 : $quantity['quantity'];

                $roLQty = $this->getProductQuantityViaDisplayUom($row['roL'], $productUom);
                $roLQty['quantity'] = ($roLQty['quantity'] == 0) ? 0 : $roLQty['quantity'];

                $defaultUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['locDefaultPrice'], $productUom);

                $categoryListWiseLocation[$row['locID']]['locNameCD'] = $row['locName'] . '-' . $row['locCD'];
                $categoryListWiseLocation[$row['locID']]['cD'][$row['categoryID']][$row['pID']] = array(
                    'cName' => $row['categoryName'],
                    'pName' => $row['pName'],
                    'pCD' => $row['pCD'],
                    'qty' => $quantity['quantity'],
                    'roL' => $roLQty['quantity'],
                    'uomAbbr' => $quantity['uomAbbr'],
                    'defPrice' => $defaultUnitPrice,
                    'systemCurrency' => $currency
                );
            }

            $cList = array_filter($categoryListWiseLocation);
            return $cList;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function itemDetailsViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIds = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $locIds = $request->getPost('locationIdList');

            $translator = new Translator();
            $name = $translator->translate('Location wise Item Details Report');

            $proDetails = $this->_getLocWiseItemDetails($proIds, $isAllProducts, $categoryIds, $locIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $locWiseItemDetailsView = new ViewModel(array('pD' => $proDetails,
                'cD' => $companyDetails,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender));
            $locWiseItemDetailsView->setTemplate('reporting/stock-in-hand-report/generate-location-wise-item-details-pdf');

            $this->html = $locWiseItemDetailsView;
            $this->msg = $this->getMessage('INFO_STOCKHAND_LOCATION_ITEM');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param type $proIds
     * @param type $locIds
     * @return $proDetails
     */
    private function _getLocWiseItemDetails($proIds = null, $isAllProducts = false, $categoryIds, $locIds = NULL)
    {
        if (empty($locIds)) {
            $locIds = $this->getActiveAllLocationsIds();
            $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        }

        $categoryIds = (empty($categoryIds)) ? null : $categoryIds;
        if (isset($proIds) || $isAllProducts) {
            $proDetails = array_fill_keys($locIds, '');

            //get all product uoms
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductIds();

            //get all product tax details
            $productTaxData = $this->CommonTable("Inventory\Model\ProductTaxTable")->getProTaxDetailsByProductID();
            $productTaxDataSet = [];
            // set product taxdetails to the array
            foreach ($productTaxData as $key => $value) {
                $productTaxDataSet[$value['productID']][] = $value;
            }

            $resLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyListByLocID($proIds, $locIds, $isAllProducts, $categoryIds);

            foreach ($resLocPro as $row) {
                //set taxes to the productDetails Array
                if(array_key_exists($row['pID'],$productTaxDataSet)){
                    $row['tD'] = $productTaxDataSet[$row['pID']];
                }

                $proDetails[$row['locID']]['data'] [$row['pID']] = $row;
                $proDetails[$row['locID']]['locName'] = $row['locName'];
                $proDetails[$row['locID']]['locCD'] = $row['locCD'];

                // $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['pID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($row['qty'], $productUom[$row['pID']]);
                //set quantity and uomAbbr as display UOM
                $proDetails[$row['locID']]['data'][$row['pID']]['qty'] = $thisqty['quantity'];
                $proDetails[$row['locID']]['data'][$row['pID']]['uomAbbr'] = $thisqty['uomAbbr'];

            }

            return $proDetails;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateLocationWiseItemDetailsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIds = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $translator = new Translator();
            $locIds = $request->getPost('locationIdList');
            $name = $translator->translate('Location wise Item Details Report');
            $proDetails = $this->_getLocWiseItemDetails($proIds, $isAllProducts, $categoryIds, $locIds);
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewLocWiseItemDetails = new ViewModel(array(
                'cD' => $cD,
                'pD' => $proDetails,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $viewLocWiseItemDetails->setTemplate('reporting/stock-in-hand-report/generate-location-wise-item-details-pdf');
            $viewLocWiseItemDetails->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewLocWiseItemDetails);
            $pdfPath = $this->downloadPDF('location-wise-item-details-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * return csv file
     */
    public function generateLocationWiseItemDetailsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIds = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $locIds = $request->getPost('locationIdList');
            $proDetails = $this->_getLocWiseItemDetails($proIds, $isAllProducts, $categoryIds, $locIds);
            $cD = $this->getCompanyDetails();

            if ($proDetails) {
                $title = '';
                $tit = 'LOCATION WISE ITEM DETAILS REPORT';
                $in = ['', '', $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["NO", "LOCATION NAME - CODE", "PRO. NAME", "PRO. CODE", "ITEM DESCRIPTION", "ITEM CATEGORY", "SELLING PRICE({$this->companyCurrencySymbol})", "PURCHASING PRICE({$this->companyCurrencySymbol})", "REODER LEVEL", "MINIMUM INV. LEVEL","MAXIMUM INV. LEVEL", "DIS COUNT %", "DIS. VALUE", "TAX VALUE", "QUANTITY"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                foreach ($proDetails as $locID => $locData) {
                    if (!empty($locData['data'])) {
                        $in = [$i, $locData['locName'] . '-' . $locData['locCD']];
                        $arr.=implode(',', $in) . "\n";
                        foreach ($locData['data'] as $proIds => $proData) {
                            $uomAbbr = $proData['uomAbbr'] ? $proData['uomAbbr'] : '';
                            $qty = $proData['qty'] ? $proData['qty'] : 0.00;
                            $pD = !empty($proData['pD']) ? $proData['pD'] : '-';
                            $taxData = '';
                            foreach ($proData['tD'] as $value) {
                                $taxData .= $value['taxName'] . '-' . $value['taxPrecentage'] . '% ';
                            }

                            $in = ['', '',
                                str_replace(',', "", $proData['pName']),
                                $proData['pCD'],
                                str_replace(',', "", $pD),
                                $proData['categoryName'],
                                $proData['defaultSellingPrice'],
                                $proData['defaultPurchasePrice'],
                                $proData['locROL'] . '' . $uomAbbr,
                                $proData['locMIL'] . '' . $uomAbbr,
                                $proData['locMaxIL'] . '' . $uomAbbr,
                                $proData['locDP'],
                                $proData['locDV'],
                                $taxData,
                                $qty . '' . $uomAbbr];
                            $arr.=implode(',', $in) . "\n";
                        }
                        $i++;
                    }
                }
            } else {
                $arr = "no matching records found\n";
            }
            $name = "location_wise_item_details";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function viewGlobalStockValueAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locIds = $this->getActiveAllLocationsIds();
            $isAllProducts = $request->getPost('isAllProducts');
            $proIds = $request->getPost('productIds');
            $categoryIds = $request->getPost('categoryIds');
            $grnCostingFlag = false;
            if ($request->getPost('grnCosting') == 'true') {
                $grnCostingFlag = true;
            }
            $avgCostingFlag = false;
            if($request->getPost('avgCosting') == 'true'){
                $avgCostingFlag = true;
            }
            
            //if user has selected isAllProduct button then puts all active product ids
            //into $proIds array
            if (empty($proIds)) {
                $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown($locIds, $productSearchKey = NULL, $isSearch = FALSE);

                foreach ($productData as $value) {
                    $proIds[$value['productID']] = $value['productID'];
                }
            }
            

            //use to get avg cost
            if ($grnCostingFlag) {
                $location = $request->getPost('locationIds');
                $locId = $location;
                if(sizeof($location) == 1 && $location[0] == null){
                    $locId = $locIds;
                }
                
                $name = 'Global Stock Value With GRN Cost Report';
                $globalStockValueData = $this->getService('StockInHandReportService')->getProductsWithGRNCosting($proIds, $locId, $isAllProducts, $categoryIds);
            } else if($avgCostingFlag){
                $location = $request->getPost('locationIds');
                $locId = $location;
                if(sizeof($location) == 1 && $location[0] == null){
                    $locId = $locIds;
                }
                $name = 'Global Stock Value With Avg Cost Report';
                $globalStockValueData = $this->getService('StockInHandReportService')->getProductsWithAvgCosting($proIds, $locId, $isAllProducts, $categoryIds);

            } else {
                $name = 'Global Stock Value Report';
                $globalStockValueData = $this->getService('StockInHandReportService')->_getGlobalWiseStockValueData($proIds, $locIds, $isAllProducts, $categoryIds);

            }
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $globalStockValueView = new ViewModel(array(
                'gSVD' => $globalStockValueData,
                'cD' => $companyDetails,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
                'grnCost' => $grnCostingFlag
            ));
            if($avgCostingFlag || $grnCostingFlag){
                $globalStockValueView->setTemplate('reporting/stock-in-hand-report/generate-globle-stock-with-avg-cost-value-pdf');
            } else {
                $globalStockValueView->setTemplate('reporting/stock-in-hand-report/generate-global-stock-value-pdf');
            }

            $this->html = $globalStockValueView;
            $this->msg = $this->getMessage('INFO_STOCKHAND_GLOBAL_STOCKVAL');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateGlobalStockValuePdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $locIds = $this->getActiveAllLocationsIds();
            $postData = $request->getPost()->toArray(); // get report request data
            $postData['locIds'] = $locIds;

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['global-stock-value'], 'pdf');
            
            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    public function generateGlobalStockValueSheetAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $locIds = $this->getActiveAllLocationsIds();

            $postData = $request->getPost()->toArray(); // get report request data
            $postData['locIds'] = $locIds;

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['global-stock-value'], 'csv');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    public function locationWiseStockValueViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $isAllProducts = $request->getPost('isAllProducts');
            $locIds = $request->getPost('locationIdList');
            $proIds = $request->getPost('productIds');
            $categoryIds = $request->getPost('categoryIds');

            //if user has selected isAllProduct button then all active product ids put
            //into $proIds array
            if (empty($proIds)) {
                $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown($locIds, $productSearchKey = NULL, $isSearch = FALSE);

                foreach ($productData as $value) {
                    $proIds[$value['productID']] = $value['productID'];
                }
            }
            $name = 'Location Wise Stock Value Report';

            $locWiseStockValueData = $this->getService('StockInHandReportService')->_getLocationWiseStockValueData($proIds, $locIds, $isAllProducts, $categoryIds);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $locWiseStockValueView = new ViewModel(array(
                'locWSVD' => $locWiseStockValueData,
                'cD' => $companyDetails,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $locWiseStockValueView->setTemplate('reporting/stock-in-hand-report/generate-location-wise-stock-value-pdf');

            $this->html = $locWiseStockValueView;
            $this->msg = $this->getMessage('INFO_STOCKHAND_LOCATION_STOCKVAL');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateLocationWiseStockValuePdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['location-wise-stock-value'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    public function generateLocationWiseStockValueSheetAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['location-wise-stock-value'], 'csv');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * get data for location wise stock value report
     * @param array $proIds
     * @param array $locIds
     * @return $locProData
     */
    private function _getLocationWiseStockValueData($proIds = NULL, $locIds, $isAllProducts, $categoryIds = null)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        if ((isset($proIds) || $isAllProducts) && isset($locIds)) {
            $proKeys['pD'] = array_fill_keys($proIds, '');
            $locWiseData = array_fill_keys($locIds, $proKeys);

            //get available quantity by locationProduct table
            $productList = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyListByLocID($proIds, $locIds, $isAllProducts, $categoryIds);
            //get all product uom details
            $productUoms = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductIds();

            $selectedProductIDs = array();
            foreach ($productList as $p) {
                if (!in_array($p['pID'], $selectedProductIDs)) {
                    $selectedProductIDs[] = $p['pID'];
                }

                $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUoms[$p['pID']]);
                $qtySubtraction = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                $locWiseData[$p['locID']]['pD'][$p['pID']] = array(
                    'aQty' => $qtySubtraction,
                    'pN' => $p['pName'],
                    'pCD' => $p['pCD'],
                    'locationName' => $p['locName'],
                    'locationCode' => $p['locCD'],
                    'uomAbbr' => $thisqty['uomAbbr'],
                    'category' => $p['categoryName']
                    );
            }


            $proIds = $selectedProductIDs;

            $itemData = $this->CommonTable('Inventory\Model\ItemInTable')->getProductDetails($proIds, $locIds);
            $productID = NULL;
            $locID = NULL;
            $totalQty = 0;
            $costValue = 0;
            $discountValue = 0;
            $taxValue = 0;

            $dataFlag = false;
            $uomList = [];

            foreach ($itemData as $i) {
                //get quantity and unit price according to dispaly uom
                if(!array_key_exists($i['productID'], $uomList)){
                    // $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($i['productID']);
                    $uomList[$i['productID']] = $productUoms[$i['productID']];
                }

                $thisqty = $this->getProductQuantityViaDisplayUom($i['qtySubtraction'], $uomList[$i['productID']]);
                $qtySubtraction = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                $itemInPrice = $this->getProductUnitPriceViaDisplayUom($i['itemInPrice'], $uomList[$i['productID']]);

                //When $productID is previous productID
                //if previous $productID is equal to new $i['productID']
                if ($productID == $i['productID'] && $locID == $i['locationID']) {
                    $totalQty += $qtySubtraction;
                    $costValue += $qtySubtraction * $itemInPrice;
                    $discountValue += $qtySubtraction * $i['itemInDiscount'];
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'totalQty' => $totalQty));
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'costValue' => $costValue));
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'discountValue' => $discountValue));
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'taxValue' => $taxValue));
                    $dataFlag = true;
                }

                //first product data set put into $locWiseData array.because $flag is flase
                if ($dataFlag == false) {
                    $totalQty = $qtySubtraction;
                    $costValue = $qtySubtraction * $itemInPrice;
                    $discountValue = $qtySubtraction * $i['itemInDiscount'];
                    $locWiseData[$i['locationID']]['pD'][$i['productID']] = array_merge(
                            $locWiseData[$i['locationID']]['pD'][$i['productID']], array(
                        'pID' => $i['productID'],
                        'costValue' => $costValue,
                        'totalQty' => $totalQty,
                        'discountValue' => $discountValue,
                        'taxValue' => $taxValue
                    ));
                }
                //first comming productID and transferID, set to following variables
                $productID = $i['productID'];
                $locID = $i['locationID'];
                $dataFlag = false;
            }

            return $locWiseData;
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * get data for global stock value report
     * @param type $proIds
     * @return $gloProData
     */
    private function _getGlobalWiseStockValueData($proIds = NULL, $locIds, $isAllProducts = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $proIds = empty($proIds) ? [] : $proIds;
        if ((isset($proIds) || $isAllProducts ) && isset($locIds)) {
            $globalData['pD'] = array_fill_keys($locIds, '');
            $globalData = array_fill_keys($proIds, $globalData);

            //get available quantity by locationProduct table
            $productList = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyListByLocID($proIds, $locIds, $isAllProducts, null, true);

            foreach ($productList as $p) {
                $globalData[$p['pID']] = array(
                    'pN' => $p['pName'],
                    'pCD' => $p['pCD']);
            }
            $proIds = array_keys($globalData);
            //get all product uoms that related to the given product ids
            $allProductUoms = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductIds($proIds);

            foreach ($productList as $p) {
                //get quantity and unit price according to dispaly uom
                // $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($p['pID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $allProductUoms[$p['pID']]);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                $globalData[$p['pID']]['pD'][$p['locID']]['aQty'] = $quantity;
                $globalData[$p['pID']]['pD'][$p['locID']]['uomAbbr'] = $thisqty['uomAbbr'];
            }

            $itemData = $this->CommonTable('Inventory\Model\ItemInTable')->getProductDetails($proIds, $locIds);
            $productID = NULL;
            $totalQty = 0;
            $costValue = 0;
            $discountValue = 0;
            $count = 0;
            $locID = NULL;
            $dataFlag = false;

            foreach ($itemData as $i) {
                //get quantity and unit price according to dispaly uom

                $thisqty = $this->getProductQuantityViaDisplayUom($i['qtySubtraction'], $allProductUoms[$i['productID']]);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                $unitPrice = $this->getProductUnitPriceViaDisplayUom($i['itemInPrice'], $allProductUoms[$i['productID']]);

                $itemInDocumentType = $i['itemInDocumentType'];
                $locationProductID = $i['itemInLocationProductID'];
                $taxValue = 0;
                //if previous $productID is equal to new $i['productID']
                if ($productID == $i['productID'] && $locID == $i['locationID']) {
                    $previousStageTaxValue = $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['taxValue'];
                    $newTaxValue = $previousStageTaxValue + $taxValue;

                    $totalQty += $quantity;
                    $costValue += $quantity * $unitPrice;
                    $discountValue += $quantity * $i['itemInDiscount'];
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['totalQty'] = $totalQty;
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['costValue'] = $costValue;
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['discountValue'] = $discountValue;
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count]['taxValue'] = $newTaxValue;
                    $dataFlag = true;
                }
                //first product dataset put into $locWiseData array.because $flag is FALSE
                if ($dataFlag == false) {

                    $count++;
                    $totalQty = $quantity;
                    $costValue = $quantity * $unitPrice;
                    $discountValue = $quantity * $i['itemInDiscount'];
                    $globalData[$i['productID']]['pD'][$i['locationID']]['itemD'][$count] = array(
                        'pID' => $i['productID'],
                        'locID' => $i['locationID'],
                        'costValue' => $costValue,
                        'totalQty' => $totalQty,
                        'discountValue' => $discountValue,
                        'taxValue' => $taxValue
                    );
                }
                //first comming productID and transferID, set to following variables
                $productID = $i['productID'];
                $locID = $i['locationID'];
                $dataFlag = false;
            }

            //calculation
            //product wise data are putting into one array,ProductID is as a key
            $gloProData = array();
            foreach ($globalData as $key => $gD) {
                if ($gD['pD']) {
                    foreach (($gD['pD']) as $locID => $data) {

                        $totalCostValue = 0;
                        $totalSubQty = 0;
                        $totalDiscount = 0;
                        $totalTax = 0;
                        if (!empty($data['itemD'])) {
                            foreach (($data['itemD']) as $d) {
                                $totalCostValue += $d['costValue'];
                                $totalSubQty +=$d['totalQty'];
                                $totalDiscount += $d['discountValue'];
                                $totalTax += $d['taxValue'];
                            }
                        }

                        if ($key && $gD['pCD'] && $gD['pN']) {
                            $gloProData[$key]['proD'][$locID] = array(
                                'availableQty' => $data['aQty'],
                                'pN' => $gD['pN'],
                                'pCD' => $gD['pCD'],
                                'pID' => $key,
                                'totalCostValue' => $totalCostValue,
                                'totalSubQty' => $totalSubQty,
                                'discountValue' => $totalDiscount,
                                'taxValue' => $totalTax,
                                'uomAbbr' => $data['uomAbbr'],
                            );
                        }
                    }
                }
            }

            return $gloProData;
        }
    }

    public function viewGlobalItemMovingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proList = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationIds = $request->getPost('locationIds');
            $documentDateFlag = $request->getPost('documentDateFlag');

            $globalItemMovingData = $this->getService('StockInHandReportService')->getGlobalItemMovingData($proList, $fromDate, $toDate, $isAllProducts, null,$locationIds, $documentDateFlag);

            if ($documentDateFlag == "true") {
                $name = 'Item Moving Report With Document Date';
            } else {
                $name = 'Item Moving Report';
            }

            $headerView = $this->headerViewTemplate($name, $period = $fromDate . ' - ' . $toDate);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $globalItemMovingView = new ViewModel(array(
                'globalItemMovingData' => $globalItemMovingData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $globalItemMovingView->setTemplate('reporting/stock-in-hand-report/generate-global-item-moving-pdf');

            $this->html = $globalItemMovingView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }


    public function generateGlobalItemMovingPdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['global-item-moving'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @return path of CSV
     */
    public function generateGlobalItemMovingSheetAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }


            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['global-item-moving'], 'csv');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param Array $proList
     * @param Date $fromDate
     * @param Date $toDate
     * @param Array $productWiseData Product wise Item moving data
     */
    private function _getGlobalItemMovingData($proList, $fromDate, $toDate, $isAllProducts = false, $isNonInventoryProducts = false, $locationIds = null)
    {

        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        //plus one day to $toDate
        //Because of mysql between function (I want to get data on $toDate)
        $plusToDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '0');

        $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getRangeItemInDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
        $itemOutData = $this->CommonTable('Inventory\Model\ItemOutTable')->getRangeItemOutDetails($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);


        $itemData = array_merge_recursive($itemInData, $itemOutData);

        //get transfer data that not in given location
        if($locationIds != ''){
            $transferData = $this->CommonTable('Inventory\Model\ItemInTable')->getTransferDetailsForMovements($proList, $fromDate, $plusToDate, $isAllProducts, $isNonInventoryProducts, $locationIds);
            $transData = [];
            foreach ($transferData as $key => $value) {
                if(!in_array($value['locationIDIn'], $locationIds) && in_array($value['locationIDOut'], $locationIds)){
                    $transData[] = $value;
                }
            }
            $itemData = array_merge($itemData, $transData);
        }
        $itemInOutData = [];
        $selectedProductIds = [];

        foreach ($itemData as $r) {
            $selectedProductIds[] = $r['productID'];
        }

        // $proList = $selectedProductIds;
        $proList = array_unique($selectedProductIds);
        if (count($proList) > 0) {
            //get all item in details
            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getAllItemInDetails();
            $itemInDataSet = [];
            foreach ($itemInDetails as $key => $value) {
                $itemInDataSet[$value['itemInID']] = $value;
            }

            // get all product uoms and set to array
            $allProductUoms = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductIds();

            //create new element called itemDate
            //becasue I want to sort $itemData array according to Date value
            foreach ($itemData as $key => $v) {
                $unitPrice = ($v['itemInPrice']) ? $v['itemInPrice'] : 0;
                $discountx = ($v['itemInDiscount']) ? $v['itemInDiscount'] : 0;
                if ($v['itemOutItemInID']) {

                    // $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetails($v['itemOutItemInID']);
                    $itemInDetails = $itemInDataSet[$v['itemOutItemInID']];
                    $unitPrice = $itemInDetails['itemInPrice'];
                    $discountx = $itemInDetails['itemInDiscount'];
                }

                $inOrOutDocumentType = $v['itemInDocumentType'] ? $v['itemInDocumentType'] : $v['itemOutDocumentType'];
                $inOutDocumentId = ($v['itemInDocumentID']) ? $v['itemInDocumentID'] : $v['itemOutDocumentID'];
                $inOrOutItemDetails = $this->_getInOrOutItemCodex($inOrOutDocumentType, $inOutDocumentId);

                $deleteFlag = FALSE;
                if (isset($inOrOutItemDetails)) {
                    //get quantity and unit price according to dispaly uom
                    // $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID( $v['productID']);
                    $productUom = $allProductUoms[$v['productID']];

                    $thisTotalInQty = $this->getProductQuantityViaDisplayUom($v['totalInQty'], $productUom);
                    $totalInQty = ($thisTotalInQty['quantity'] == 0) ? 0 : $thisTotalInQty['quantity'];
                    $thisTotalOutQty = $this->getProductQuantityViaDisplayUom($v['totalOutQty'], $productUom);
                    $totalOutQty = ($thisTotalOutQty['quantity'] == 0) ? 0 : $thisTotalOutQty['quantity'];

                    $date = $v['itemInDateAndTime'] ? $v['itemInDateAndTime'] : $v['itemOutDateAndTime'];
                    $itemInQty = $totalInQty;
                    $itemOutQty = $totalOutQty;

                    if ($v['goodsIssueStatus'] && ($inOrOutDocumentType == 'Delete Positive Adjustment')) {
                        $deleteFlag = ($v['goodsIssueStatus'] == 5) ? 'positive' : FALSE;
                    } else if ($v['goodsIssueStatus'] && ($inOrOutDocumentType == 'Delete Negative Adjustment')) {
                        $deleteFlag = ($v['goodsIssueStatus'] == 5) ? 'negative' : FALSE;
                    }
                    //If document type is transfer then get Location In Name and Out Name
                    $toLocation = NULL;
                    $fromLocation = NULL;
                    $isTransfer = FALSE;
                    if ($inOrOutDocumentType == 'Inventory Transfer') {
                        $fromLocation = $inOrOutItemDetails['locationOut'];
                        $toLocation = $inOrOutItemDetails['locationIn'];
                        $isTransfer = TRUE;
                    }

                    if ($inOrOutDocumentType == 'Payment Voucher') {
                        $inOrOutDocumentType = "Purchase Invoice";
                    }
                    if($inOrOutDocumentType == 'Inventory Transfer' && $locationIds != '' ){

                        if(in_array($inOrOutItemDetails['locationInID'], $locationIds)){

                            $itemInOutData[$key.'_1'] = array(
                                'pID' => $v['productID'],
                                'pName' => $v['productName'],
                                'pCD' => $v['productCode'],
                                'itemDate' => $this->getUserDateTime($date),
                                'inOrOutItemCode' => $inOrOutItemDetails['itemCode'],
                                'itemInQty' => $itemInQty,
                                'itemOutQty' => $itemOutQty,
                                'itemInDiscount' => $discountx,
                                'itemInOrOutDocumentType' => $inOrOutDocumentType,
                                'unitPrice' => $unitPrice,
                                'fromLocation' => null,
                                'toLocation' => $toLocation,
                                'actLocation' => '(To) '.$toLocation,
                                'isTransfer' => false,
                                'deleteFlag' => $deleteFlag,
                                'uomAbbr' => $thisTotalOutQty['uomAbbr']
                            );
                        }
                         if(in_array($inOrOutItemDetails['locationOutID'], $locationIds)){

                            $itemInOutData[$key.'_2'] = array(
                                'pID' => $v['productID'],
                                'pName' => $v['productName'],
                                'pCD' => $v['productCode'],
                                'itemDate' => $this->getUserDateTime($date),
                                'inOrOutItemCode' => $inOrOutItemDetails['itemCode'],
                                'itemInQty' => 0,
                                'itemOutQty' => $itemInQty,
                                'itemInDiscount' => $discountx,
                                'itemInOrOutDocumentType' => $inOrOutDocumentType,
                                'unitPrice' => $unitPrice,
                                'fromLocation' => $fromLocation,
                                'toLocation' => null,
                                'actLocation' => '(From) '.$fromLocation,
                                'isTransfer' => false,
                                'deleteFlag' => $deleteFlag,
                                'uomAbbr' => $thisTotalOutQty['uomAbbr']
                            );
                         }
                    } else {

                        $itemInOutData[$key] = array(
                            'pID' => $v['productID'],
                            'pName' => $v['productName'],
                            'pCD' => $v['productCode'],
                            'itemDate' => $this->getUserDateTime($date),
                            'inOrOutItemCode' => $inOrOutItemDetails['itemCode'],
                            'itemInQty' => $itemInQty,
                            'itemOutQty' => $itemOutQty,
                            'itemInDiscount' => $discountx,
                            'itemInOrOutDocumentType' => $inOrOutDocumentType,
                            'unitPrice' => $unitPrice,
                            'fromLocation' => $fromLocation,
                            'toLocation' => $toLocation,
                            'actLocation' => $inOrOutItemDetails['locationName'],
                            'isTransfer' => $isTransfer,
                            'deleteFlag' => $deleteFlag,
                            'uomAbbr' => $thisTotalOutQty['uomAbbr']
                        );
                    }

                }
            }


            //sorting data on $itemData array according to itemDate element
            $sortData = Array();
            foreach ($itemInOutData as $key => $r) {
                $sortData[$key] = strtotime($r['itemDate']);
            }
            array_multisort($sortData, SORT_ASC, $itemInOutData);

            //array rearrange as product wise
            $productWiseData = array_fill_keys($proList, '');
            $i = 0;
            foreach (array_filter($itemInOutData) as $v) {
                $productWiseData[$v['pID']]['pD'][$i] = $v;
                $productWiseData[$v['pID']]['pName'] = $v['pName'];
                $productWiseData[$v['pID']]['pCD'] = $v['pCD'];
                $productWiseData[$v['pID']]['uomAbbr'] = $v['uomAbbr'];
                $productWiseData[$v['pID']]['unitPrice'] = $v['unitPrice'];
                $i++;
            }

            //get itemIn table stock item wise
            $itemInStock = $this->CommonTable('Inventory\Model\ItemInTable')->getStockInRange($proList, $fromDate, $locationIds,$isNonInventoryProducts);
            $itemInArr = [];
            $transferQty = $transferValue = 0;

            $itemTransferdIds = $this->CommonTable('Inventory\Model\ItemInTable')->getTransferedlocationIds($locationIds);

            //get transfered products
            $transferedLocationIds = [];
            for ($i=0; $i < sizeof($itemTransferdIds) ; $i++) {
                $transferedLocationIds = array($i => $itemTransferdIds[$i]['trnasferedLocationId']);
            }

            if (!empty($transferedLocationIds)) {
                $transferedItemDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getTransferedStockInRange($proList, $fromDate, $transferedLocationIds);
            }

            $transferedQuantity = 0;
            foreach ($transferedItemDetails as $value) {
                $transferedQuantity  += $value['transferedQuantity'];
            }

            //puts item wise open stock into $productWiseData Array
            foreach ($itemInStock as $value) {
                //get quantity and unit price according to dispaly uom
                // $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);
                $productUom =$allProductUoms[$value['productID']];
                $thisQty = $this->getProductQuantityViaDisplayUom($value['availableInQty'], $productUom);
                $thisUnitPrice = $this->getProductUnitPriceViaDisplayUom($value['itemInPrice'], $productUom);

                $availableQty = ($thisQty['quantity'] == 0) ? 0 : $thisQty['quantity'];
                $thisUnitPrice = ($thisUnitPrice == 0) ? 0 : $thisUnitPrice;
                $transferQty = ($value['itemInDocumentType'] == 'Inventory Transfer') ? $availableQty : 0;
                $transferValue = ($value['itemInDocumentType'] == 'Inventory Transfer') ? ($availableQty * $thisUnitPrice) : 0;

                if (!array_key_exists($value['productID'], $itemInArr)) {
                    $itemInArr[$value['productID']] = [
                        'itemInStock' => $availableQty,
                        'itemInStockValue' => $availableQty * $thisUnitPrice,
                        'transferInStock' => $transferedQuantity,
                        'transferInStockValue' => $transferValue,
                    ];
                } else {
                    $itemInArr[$value['productID']]['itemInStock'] = $itemInArr[$value['productID']]['itemInStock'] + $availableQty;
                    $itemInArr[$value['productID']]['itemInStockValue'] = $itemInArr[$value['productID']]['itemInStockValue'] + ($availableQty * $thisUnitPrice);
                    $itemInArr[$value['productID']]['transferInStock'] = $itemInArr[$value['productID']]['transferInStock'] + $transferQty;
                    $itemInArr[$value['productID']]['transferInStockValue'] = $itemInArr[$value['productID']]['transferInStockValue'] + $transferValue;
                }

                $productWiseData[$value['productID']]['itemInStock'] = $itemInArr[$value['productID']]['itemInStock'];
                $productWiseData[$value['productID']]['itemInStockValue'] = $itemInArr[$value['productID']]['itemInStockValue'];
                $productWiseData[$value['productID']]['transferInStock'] = $itemInArr[$value['productID']]['transferInStock'];
                $productWiseData[$value['productID']]['transferInStockValue'] = $itemInArr[$value['productID']]['transferInStockValue'];
            }

            //get itemOut table stock item wise
            $itemOutStock = $this->CommonTable('Inventory\Model\ItemOutTable')->getStockOutRange($proList, $fromDate, $locationIds);
            $itemOutArr = [];
            //puts item wise open stock into $productWiseData Array
            foreach ($itemOutStock as $value) {
                //get quantity and unit price according to dispaly uom
                // $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);
                $productUom =$allProductUoms[$value['productID']];
                $thisQty = $this->getProductQuantityViaDisplayUom($value['availableOutQty'], $productUom);
                $thisUnitPrice = $this->getProductUnitPriceViaDisplayUom($value['itemInPrice'], $productUom);

                $availableQty = ($thisQty['quantity'] == 0) ? 0 : $thisQty['quantity'];
                $thisUnitPrice = ($thisUnitPrice == 0) ? 0.00 : $thisUnitPrice;

                if (!array_key_exists($value['productID'], $itemOutArr)) {
                    $itemOutArr[$value['productID']] = [
                        'itemOutStock' => $availableQty,
                        'itemOutStockValue' => $availableQty * $thisUnitPrice
                    ];
                } else {
                    $itemOutArr[$value['productID']]['itemOutStock'] = $itemOutArr[$value['productID']]['itemOutStock'] + $availableQty;
                    $itemOutArr[$value['productID']]['itemOutStockValue'] = $itemOutArr[$value['productID']]['itemOutStockValue'] + ($availableQty * $thisUnitPrice);
                }

                $productWiseData[$value['productID']]['itemOutStock'] = $itemOutArr[$value['productID']]['itemOutStock'];
                $productWiseData[$value['productID']]['itemOutStockValue'] = $itemOutArr[$value['productID']]['itemOutStockValue'];
            }
            
            $itemIntransferStock = $this->CommonTable('Inventory\Model\ItemInTable')->getTransferStockInRange($proList, $fromDate, $locationIds);
            // foreach ($itemIntransferStock as $key => $value) {
            foreach ($itemIntransferStock as $key => $value) {

                $productUom =$allProductUoms[$value['productID']];
                $thisQty = $this->getProductQuantityViaDisplayUom($value['transferedQuantity'], $productUom);
                $thisUnitPrice = $this->getProductUnitPriceViaDisplayUom($value['itemInPrice'], $productUom);

                $availableQty = ($thisQty['quantity'] == 0) ? 0 : $thisQty['quantity'];
                $thisUnitPrice = ($thisUnitPrice == 0) ? 0.00 : $thisUnitPrice;

                if (!array_key_exists($value['productID'], $itemOutArr)) {
                    $itemOutArr[$value['productID']] = [
                        'itemOutStock' => $availableQty,
                        'itemOutStockValue' => $availableQty * $thisUnitPrice
                    ];
                } else {
                    $itemOutArr[$value['productID']]['itemOutStock'] = $itemOutArr[$value['productID']]['itemOutStock'] + $availableQty;
                    $itemOutArr[$value['productID']]['itemOutStockValue'] = $itemOutArr[$value['productID']]['itemOutStockValue'] + ($availableQty * $thisUnitPrice);
                }

                $productWiseData[$value['productID']]['itemOutStock'] = $itemOutArr[$value['productID']]['itemOutStock'];
                $productWiseData[$value['productID']]['itemOutStockValue'] = $itemOutArr[$value['productID']]['itemOutStockValue'];


            }

            return $productWiseData;
        }
    }

    private function _getInOrOutItemCodex($inOrOutItemType, $documentId)
    {
        switch ($inOrOutItemType) {
            case 'Goods Received Note':
                $documentData = $this->_getDocumentDetails($documentId, "Inventory\Model\GrnTable", getGrnByGrnID);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['grnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Inventory Transfer':
                $documentData = $this->_getDocumentDetails($documentId, "Inventory\Model\TransferTable", getTransfersById);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['transferCode'];
                $inOrOutItemTypeDetails['locationIn']   = $documentData['locationInName'];
                $inOrOutItemTypeDetails['locationOut']  = $documentData['locationOutName'];
                $inOrOutItemTypeDetails['locationInID']  = $documentData['locationInID'];
                $inOrOutItemTypeDetails['locationOutID']  = $documentData['locationOutID'];
                break;

            case 'Inventory Positive Adjustment':
            case 'Inventory Negative Adjustment':
            case 'Delete Positive Adjustment':
            case 'Delete Negative Adjustment':
                $documentData = $this->_getDocumentDetails($documentId, "Inventory\Model\GoodsIssueTable", getAdjutmentByAdjutmentId);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['goodsIssueCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Payment Voucher':
                $documentData = $this->_getDocumentDetails($documentId, "Inventory\Model\PurchaseInvoiceTable", getPIsByPIID);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['purchaseInvoiceCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Payment Voucher Cancel':
                $documentData = $this->_getDocumentDetails($documentId, "Expenses\Model\PaymentVoucherTable", getPaymentVoucherDetailsByPVID);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['paymentVoucherCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Sales Returns':
                $documentData = $this->_getDocumentDetails($documentId, "SalesReturnsTable", getSalesReturnById);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['salesReturnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;
            case 'Direct Return':
                $documentData = $this->_getDocumentDetails($documentId, "SalesReturnsTable", getSalesReturnById);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['salesReturnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Credit Note':
                $documentData = $this->_getDocumentDetails($documentId, "Invoice\Model\CreditNoteTable", getCreditNoteById);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['creditNoteCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Sales Invoice':
            case 'POS Invoice Cancel':
            case 'Invoice Cancel':
            case 'Invoice Edit':
                $documentData = $this->_getDocumentDetails($documentId, "Invoice\Model\InvoiceTable", getInvoiceDataByInvoiceId);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['salesInvoiceCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Delivery Note':
            case 'Delivery Note Edit':
                $documentData = $this->_getDocumentDetails($documentId, "Invoice\Model\DeliveryNoteTable", getDeliveryNoteByDeliveryNoteId);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['deliveryNoteCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'purchase Invoice cancel':
                $documentData = $this->_getDocumentDetails($documentId, "Inventory\Model\PurchaseInvoiceTable", getPIsByPIID);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['purchaseInvoiceCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Purchase Return':
                $documentData = $this->_getDocumentDetails($documentId, "Inventory\Model\PurchaseReturnTable", getPurchaseReturnById);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['purchaseReturnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Debit Note':
                $documentData = $this->_getDocumentDetails($documentId, "Inventory\Model\DebitNoteTable", getDebitNoteById);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['debitNoteCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Activity':
            case 'Delete Activity Item':
                $documentData = $this->_getDocumentDetails($documentId, "JobCard\Model\ActivityTable", getActivityDataById);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['activityCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Expense Payment Voucher':
                $documentData = $this->_getDocumentDetails($documentId, "Expenses\Model\PaymentVoucherTable", getPaymentVoucherById);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['paymentVoucherCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            case 'Goods Received Note Cancel':
                $documentData = $this->_getDocumentDetails($documentId, "Inventory\Model\GrnTable", getGrnByGrnID);
                $inOrOutItemTypeDetails['itemCode']     = $documentData['grnCode'];
                $inOrOutItemTypeDetails['locationName'] = $documentData['locationName'];
                break;

            default:
                $inOrOutItemTypeDetails = ['itemCode' => null, 'locationName' => null];
                break;
        }

        return $inOrOutItemTypeDetails;
    }

    /**
     * @author Sandun Dissanayake<sandun@thinkcube.com>
     * get itemIn or itemOut document code
     * @param String $inOrOutItemType
     * @param array $data
     * @return String $inOrOutItemDetails
     */
    private function _getInOrOutItemCode($inOrOutItemType, $data)
    {
        $inOrOutItemTypeDetails = array('itemCode', 'locationName');
        if (isset($inOrOutItemType)) {

            $inOrOutItemTypeDetails = NULL;
            if ($inOrOutItemType == 'Goods Received Note') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['grnCode'] != NULL) ? $data['grnCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Inventory\Model\GrnTable", getGrnByGrnID);
                }
            } else if ($inOrOutItemType == 'Inventory Transfer') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['transferCode'] != NULL) ? $data['transferCode'] : NULL;
            } else if ($inOrOutItemType == 'Inventory Positive Adjustment') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['goodsIssueCode'] != NULL) ? $data['goodsIssueCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Inventory\Model\GoodsIssueTable", getAdjutmentByCode);
                }
            } else if ($inOrOutItemType == 'Payment Voucher') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['purchaseInvoiceCode'] != NULL) ? $data['purchaseInvoiceCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Inventory\Model\PurchaseInvoiceTable", getPIsByPICode);
                }
            } else if ($inOrOutItemType == 'Sales Returns') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['salesReturnCode'] != NULL) ? $data['salesReturnCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "SalesReturnsTable", getSalesReturnByCode);
                }
            } else if ($inOrOutItemType == 'Credit Note') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['creditNoteCode'] != NULL) ? $data['creditNoteCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Invoice\Model\CreditNoteTable", getCreditNoteByCode);
                }
            } else if ($inOrOutItemType == 'Sales Invoice') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['salesInvoiceCode'] != NULL) ? $data['salesInvoiceCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Invoice\Model\InvoiceTable", getInvoiceByCode);
                }
            } else if ($inOrOutItemType == 'POS Invoice Cancel') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['salesInvoiceCode'] != NULL) ? $data['salesInvoiceCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Invoice\Model\InvoiceTable", getInvoiceByCode);
                }
            } else if ($inOrOutItemType == 'Invoice Cancel') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['salesInvoiceCode'] != NULL) ? $data['salesInvoiceCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Invoice\Model\InvoiceTable", getInvoiceByCode);
                }
            } else if ($inOrOutItemType == 'Delivery Note') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['deliveryNoteCode'] != NULL) ? $data['deliveryNoteCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Invoice\Model\DeliveryNoteTable", getDeliveryNoteByCode);
                }
            } else if ($inOrOutItemType == 'Inventory Negative Adjustment') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['goodsIssueCode'] != NULL) ? $data['goodsIssueCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Inventory\Model\GoodsIssueTable", getAdjutmentByCode);
                }
            } else if ($inOrOutItemType == 'Purchase Return') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['purchaseReturnCode'] != NULL) ? $data['purchaseReturnCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Inventory\Model\PurchaseReturnTable", getPurchaseReturnByCode);
                }
            } else if ($inOrOutItemType == 'Debit Note') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['debitNoteCode'] != NULL) ? $data['debitNoteCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Inventory\Model\DebitNoteTable", getDebitNoteByCode);
                }
            } else if ($inOrOutItemType == 'Activity') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['activityCode'] != NULL) ? $data['activityCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "JobCard\Model\ActivityTable", getActivityByCode);
                }
            } else if ($inOrOutItemType == 'Delete Positive Adjustment') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['goodsIssueCode'] != NULL) ? $data['goodsIssueCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Inventory\Model\GoodsIssueTable", getAdjutmentByCode);
                }
            } else if ($inOrOutItemType == 'Delete Negative Adjustment') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['goodsIssueCode'] != NULL) ? $data['goodsIssueCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "Inventory\Model\GoodsIssueTable", getAdjutmentByCode);
                }
            } else if ($inOrOutItemType == 'Delete Activity Item') {
                $inOrOutItemTypeDetails['itemCode'] = ($data['activityCode'] != NULL) ? $data['activityCode'] : NULL;
                if (isset($inOrOutItemTypeDetails['itemCode'])) {
                    $inOrOutItemTypeDetails['locationName'] = $this->_getLocationName($inOrOutItemTypeDetails['itemCode'], "JobCard\Model\ActivityTable", getActivityByCode);
                }
            }

            return $inOrOutItemTypeDetails;
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param string $actionCode
     * @param string $module
     * @param string $function
     * @return string $locationName
     */
    protected function _getLocationName($actionCode, $module, $function)
    {
        if (isset($actionCode)) {
            $resultData = $this->CommonTable($module)->$function($actionCode);
            $locationName = $resultData->current()['locationName'];
        }
        return $locationName;
    }

    /**
     * Get Document details by document id
     * @param string $documentId
     * @param string $module
     * @param string $function
     * @return mixed
     */
    protected function _getDocumentDetails($documentId, $module, $function)
    {
        $resultData = $this->CommonTable($module)->$function($documentId,FALSE,FALSE,TRUE);
        return $resultData->current();
    }

    /**
     * Get View of non inventory item moving report
     * @return type
     */
    public function viewInvoicedNonInventoryItemAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proList = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $this->user_session->userActiveLocation['locationID'];

            $invoicedNonInventoryItems = $this->CommonTable('Invoice\ModelInvoiceTable')->getInvoicedNonInventoryItems($locationID, $proList, $isAllProducts, $fromDate, $toDate);
            $itemList = [];
            foreach ($invoicedNonInventoryItems as $item) {
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceCode'] = $item['salesInvoiceCode'];
                $itemList[$item['salesInvoiceProductID']]['productName'] = $item['productName'] . ' - ' . $item['productCode'];
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceProductQuantity'] = ($item['salesInvoiceProductQuantity'] == 0) ? '1' : $item['salesInvoiceProductQuantity'];
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceProductPrice'] = $item['salesInvoiceProductPrice'];
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceIssuedDate'] = $item['salesInvoiceIssuedDate'];
                $itemList[$item['salesInvoiceProductID']]['salesinvoiceTotalAmount'] = $item['salesinvoiceTotalAmount'];
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceProductTotal'] = $item['salesInvoiceProductTotal'];
            }

            $name = 'Invoiced Non Inventory Items';

            $headerView = $this->headerViewTemplate($name, $period = $fromDate . ' - ' . $toDate);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $globalItemMovingView = new ViewModel(array(
                'itemList' => $itemList,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $globalItemMovingView->setTemplate('reporting/stock-in-hand-report/generate-invoiced-non-inventory-product-pdf');

            $this->html = $globalItemMovingView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * Generate Non Inventory Item pdf
     * @return type
     */
    public function generateInvoicedNonInventoryItemPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proList = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $invoicedNonInventoryItems = $this->CommonTable('Invoice\ModelInvoiceTable')->getInvoicedNonInventoryItems($locationID, $proList, $isAllProducts, $fromDate, $toDate);
            $itemList = [];
            foreach ($invoicedNonInventoryItems as $item) {
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceCode'] = $item['salesInvoiceCode'];
                $itemList[$item['salesInvoiceProductID']]['productName'] = $item['productName'] . ' - ' . $item['productCode'];
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceProductQuantity'] = ($item['salesInvoiceProductQuantity'] == 0) ? '1' : $item['salesInvoiceProductQuantity'];
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceProductPrice'] = $item['salesInvoiceProductPrice'];
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceIssuedDate'] = $item['salesInvoiceIssuedDate'];
                $itemList[$item['salesInvoiceProductID']]['salesinvoiceTotalAmount'] = $item['salesinvoiceTotalAmount'];
                $itemList[$item['salesInvoiceProductID']]['salesInvoiceProductTotal'] = $item['salesInvoiceProductTotal'];
            }

            $name = 'Invoiced Non Inventory Items';

            $headerView = $this->headerViewTemplate($name, $period = $fromDate . ' - ' . $toDate);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);
            $globalItemMovingView = new ViewModel(array(
                'itemList' => $itemList,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $globalItemMovingView->setTemplate('reporting/stock-in-hand-report/generate-invoiced-non-inventory-product-pdf');

            $globalItemMovingView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($globalItemMovingView);
            $pdfPath = $this->downloadPDF('invoiced-non-inventory-item-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * Get Non inventory Item Moving sheet
     * @return type
     */
    public function generateNonInventoryItemMovingSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proList = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $invoicedNonInventoryItems = $this->CommonTable('Invoice\ModelInvoiceTable')->getInvoicedNonInventoryItems($locationID, $proList, $isAllProducts, $fromDate, $toDate);

            $cD = $this->getCompanyDetails();
            $header = '';
            $reportTitle = [ "", 'INVOICED NON INVENTORY ITEM REPORT', "",];
            $header = implode(',', $reportTitle) . "\n";
            $dateTime = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
            $header .= implode(",", $dateTime) . "\n";
            $header .= $cD[0]->companyName . "\n";
            $header .= $cD[0]->companyAddress . "\n";
            $header .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";
            $peroidRow = ["Period : " . $fromDate . '-' . $toDate];
            $header .= implode(",", $peroidRow) . "\n";
            $header .= implode(",", [""]) . "\n";

            $columnTitlesArray = ['Invoiced Date',
                'Invoice Code',
                'Product Name',
                'Quantity',
                'Unit Price',
                'Total Amount'];
            $columnTitles = implode(",", $columnTitlesArray);
            $csvRow = '';
            if (count($invoicedNonInventoryItems) > 0) {
                foreach ($invoicedNonInventoryItems as $item) {
                    $qty = ($item['salesInvoiceProductQuantity'] == 0) ? 1 : $item['salesInvoiceProductQuantity'];
                    $tableRow = [
                        $item['salesInvoiceIssuedDate'],
                        $item['salesInvoiceCode'],
                        $item['productName'] . ' - ' . $item['productCode'],
                        $qty,
                        $item['salesInvoiceProductPrice'],
                        $item['salesInvoiceProductTotal']
                    ];
                    $grandTotal += $item['salesInvoiceProductTotal'];
                    $csvRow .= implode(",", $tableRow) . "\n";
                }
            } else {
                $csvRow = "no matching records found\n";
            }

            $grandTotalRow = ['', '', '', '', 'Grand Total:', $grandTotal];
            $csvRow .= implode(',', $grandTotalRow) . "\n";

            $name = "invoiced-non-inventory-item-report";
            $csvContent = $this->csvContent($header, $columnTitles, $csvRow);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function itemMovingWithValueAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $globalItemMovingData = $this->_getGlobalItemMovingData($postData['productIds'], $postData['fromDate'], $postData['toDate'], $postData['isAllProducts'],null, $postData['locationIds']);

            $headerView = $this->headerViewTemplate('Item Moving With Value Report', $period = $postData['fromDate']. '-' .$postData['toDate']);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $globalItemMovingView = new ViewModel(array(
                'globalItemMovingData' => $globalItemMovingData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $globalItemMovingView->setTemplate('reporting/stock-in-hand-report/generate-item-moving-with-value');

            $this->html = $globalItemMovingView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function itemMovingWithValuePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $globalItemMovingData = $this->_getGlobalItemMovingData($postData['productIds'], $postData['fromDate'], $postData['toDate'], $postData['isAllProducts'], null, $postData['locationIds']);

            $headerView = $this->headerViewTemplate('Item Moving With Value Report', $period = $postData['fromDate']. '-' .$postData['toDate']);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $globalItemMovingView = new ViewModel(array(
                'globalItemMovingData' => $globalItemMovingData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $globalItemMovingView->setTemplate('reporting/stock-in-hand-report/generate-item-moving-with-value');
            $globalItemMovingView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($globalItemMovingView);
            $pdfPath = $this->downloadPDF('global-item-moving-with-value-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function itemMovingWithValueCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();

            $globalItemMovingData = $this->_getGlobalItemMovingData($postData['productIds'], $postData['fromDate'], $postData['toDate'], $postData['isAllProducts'], null, $postData['locationIds']);
            $cD = $this->getCompanyDetails();

            if ($globalItemMovingData) {
                $title = '';
                $tit = 'ITEM MOVING WITH VALUE REPORT';
                $in = ["", $tit, "", ""];
                $title.=implode(",", $in) . "\n";

                $in = ["Period:", $postData['fromDate'] . '-' . $postData['toDate'], "", ""];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = array("NO", "TRANSACTION CODE", "TRANSACTION TYPE", "DATE/TIME", "LOCATION(FROM - TO)", "UNIT PRICE", "IN QUANTITY", "OUT QUANTITY", "STOCK IN HAND", "TRANSACTION VALUE", "STOCK VALUE");
                $header = implode(",", $arrs);
                $arr = '';
                $count = 0;
                foreach ($globalItemMovingData as $key => $d) {
                    if ($d['pD']) {
                        $i = 1;
                        $totalQty = 0;
                        $itemInStock = isset($d['itemInStock']) ? $d['itemInStock'] : 0;
                        $itemOutStock = isset($d['itemOutStock']) ? $d['itemOutStock'] : 0;
                        $negAdjstStock = isset($d['negAdjstStock']) ? $d['negAdjstStock'] : 0;
                        $posiAdjstStock = isset($d['posiAdjstStock']) ? $d['posiAdjstStock'] : 0;
                        $openStock = ($itemInStock + $negAdjstStock) - ($itemOutStock + $posiAdjstStock);
                        $stockValue = $d['itemInStockValue'] - $d['itemOutStockValue'];

                        $in = ['', $d['pName'] . ' [ ' . $d['pCD'] . ']'];
                        $arr.= implode(",", $in) . "\n";

                        foreach ($d['pD'] as $value) {
                            $qty = $value['itemInQty'] ? $value['itemInQty'] : $value['itemOutQty'];
                            $itemInQty = $value['itemInQty'];
                            $itemOutQty = $value['itemOutQty'];
                            $inOutPrice = ($value['unitPrice']) ? $value['unitPrice'] : 0;
                            $discount = $value['itemInDiscount'] ? $value['itemInDiscount'] : 0;
                            $totalDiscountAmount = $discount * $qty;
                            $fromLocation = isset($value['fromLocation']) ? $value['fromLocation'] : '-';
                            $toLocation = isset($value['toLocation']) ? $value['toLocation'] : '-';
                            $actLocation = isset($value['actLocation']) ? $value['actLocation'] : '-';
                            $isTransfer = ($value['isTransfer'] == TRUE) ? TRUE : FALSE;
                            $transactionValue = ($inOutPrice * $qty) - $totalDiscountAmount;
                            $action = '-';

                            if ($itemInQty > 0 && $isTransfer == FALSE) {
                                if ($value['deleteFlag'] == 'negative') {
                                    $action = 'deleted';
                                } else {
                                    $action = '-';
                                }
                                $totalQty += $qty;
                            } else if ($itemOutQty > 0 && $isTransfer == FALSE) {
                                if ($value['deleteFlag'] == 'positive') {
                                    $action = 'deleted';
                                } else {
                                    $action = '-';
                                }
                                $totalQty -= $qty;
                            }

                            if ($value['isTransfer'] == FALSE) {
                                $stockValue = $value['itemInQty'] ? ($stockValue + $transactionValue) : ($stockValue - $transactionValue);
                            }

                            if ($isTransfer == TRUE) {
                                $in = [
                                    $i,
                                    $value['inOrOutItemCode'],
                                    $value['itemInOrOutDocumentType'],
                                    $value['itemDate'],
                                    $fromLocation,
                                    number_format($inOutPrice, 2, '.', ''),
                                    '-',
                                    number_format($qty, 2, '.', '') . " ". $d['uomAbbr'],
                                    number_format($transactionValue, 2, '.', ''),
                                    number_format($totalQty, 2, '.', '') . " ". $d['uomAbbr'],
                                    number_format($stockValue, 2, '.', '')
                                ];
                                $arr.= implode(",", $in) . "\n";

                                $in = [
                                    '',
                                    '',
                                    '',
                                    '',
                                    $toLocation,
                                    '',
                                    number_format($qty, 2, '.', '')  . " ". $d['uomAbbr'],
                                    '-',
                                    '',
                                    '',
                                    ''
                                ];
                                $arr.= implode(",", $in) . "\n";
                            } else {
                                $in = [
                                    $i,
                                    $value['inOrOutItemCode'],
                                    $value['itemInOrOutDocumentType'],
                                    $value['itemDate'],
                                    $actLocation,
                                    number_format($inOutPrice, 2, '.', ''),
                                    number_format($itemInQty, 2, '.', ''),
                                    number_format($itemOutQty, 2, '.', '') . " ". $d['uomAbbr'],
                                    number_format($transactionValue, 2, '.', ''),
                                    number_format($totalQty, 2, '.', '') . " ". $d['uomAbbr'],
                                    number_format($stockValue, 2, '.', '')
                                ];
                                $arr.= implode(",", $in) . "\n";
                            }
                            $i++;
                            $count++;
                        }
                    }
                }
                if ($count == 0) {
                    $arr = "no matching records found\n";
                }
            }
            $name = "item-moving-with-value-report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function stockAgingAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $postData = $request->getPost()->toArray();
            $dataList = $this->getStockAgingDetails($postData);

            $headerView = $this->headerViewTemplate('Stock Aging Report', $period = '-');
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $stockAgingView = new ViewModel(array(
                'dataList' => $dataList,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $stockAgingView->setTemplate('reporting/stock-in-hand-report/generate-stock-aging-report');

            $this->html = $stockAgingView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function stockAgingPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $postData = $request->getPost()->toArray();
            $dataList = $this->getStockAgingDetails($postData);

            $headerView = $this->headerViewTemplate('Stock Aging Report', $period = '-');
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $stockAgingView = new ViewModel(array(
                'dataList' => $dataList,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $stockAgingView->setTemplate('reporting/stock-in-hand-report/generate-stock-aging-report');
            $stockAgingView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($stockAgingView);
            $pdfPath = $this->downloadPDF('generate-stock-aging-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function stockAgingCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $postData = $request->getPost()->toArray();

            $dataList = $this->getStockAgingDetails($postData);
            $cD = $this->getCompanyDetails();

            if ($dataList) {
                $title = '';
                $tit = 'STOCK AGING REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => ""
                );

                $title.=implode(",", $in) . "\n";

                $in = array(0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array(
                    "ITEM NAME",
                    "ITEM CODE",
                    "ITEM CATEGORY",
                    "270+ DAYS QUANTITY",
                    "270+ DAYS STOCK VALUE",
                    "270 - 180 DAYS QUANTITY",
                    "270 - 180 DAYS STOCK VALUE",
                    "180 - 90 DAYS QUANTITY",
                    "180 - 90 DAYS STOCK VALUE",
                    "90 - 30 DAYS QUANTITY",
                    "90 - 30 STOCK VALUE",
                    "30 - 0 QUANTITY",
                    "30 - 0 DAYS STOCK VALUE",
                );
                $header = implode(",", $arrs);
                $arr = '';


                $over270StockValue = 0;
                $withIn270StockValue = 0;
                $withIn180StockValue = 0;
                $withIn90StockValue = 0;
                $withIn30StockValue = 0;
                $tottalStockValue = 0;

                foreach ($dataList as $row) {
                    $over270StockValue += $row['Over270StockValue'];
                    $withIn270StockValue += $row['WithIn270StockValue'];
                    $withIn180StockValue += $row['WithIn180StockValue'];
                    $withIn90StockValue += $row['WithIn90StockValue'];
                    $withIn30StockValue += $row['WithIn30StockValue'];

                    $tottalValue = $row['WithIn270StockValue'] + $row['WithIn270StockValue'] + $row['WithIn180StockValue'] + $row['WithIn90StockValue'] + $row['WithIn30StockValue'];
                    $tottalQty = $row['WithIn270Qty'] + $row['WithIn270Qty'] + $row['WithIn180Qty'] + $row['WithIn90Qty'] + $row['WithIn30Qty'];

                    $tottalStockValue += $tottalValue;

                    $in = [
                        $row['productName'],
                        $row['productCode'],
                        $row['categoryName'],
                        number_format($row['Over270Qty'], 2, '.', ''),
                        number_format($row['Over270StockValue'], 2, '.', ''),
                        number_format($row['WithIn270Qty'], 2, '.', ''),
                        number_format($row['WithIn270StockValue'], 2, '.', ''),
                        number_format($row['WithIn180Qty'], 2, '.', ''),
                        number_format($row['WithIn180StockValue'], 2, '.', ''),
                        number_format($row['WithIn90Qty'], 2, '.', ''),
                        number_format($row['WithIn90StockValue'], 2, '.', ''),
                        number_format($row['WithIn30Qty'], 2, '.', ''),
                        number_format($row['WithIn30StockValue'], 2, '.', ''),
                        number_format($tottalQty, 2, '.', ''),
                        number_format($tottalValue, 2, '.', '')
                    ];
                    $arr.= implode(",", $in) . "\n";
                }

                $in = [
                    '',
                    '',
                    'TOTAL ( ' . $this->companyCurrencySymbol . ' ) :',
                    '',
                    number_format($over270StockValue, 2, '.', ''),
                    '',
                    number_format($withIn270StockValue, 2, '.', ''),
                    '',
                    number_format($withIn180StockValue, 2, '.', ''),
                    '',
                    number_format($withIn90StockValue, 2, '.', ''),
                    '',
                    number_format($withIn30StockValue, 2, '.', ''),
                    '',
                    number_format($tottalStockValue, 2, '.', ''),
                    '',
                ];
                $arr.= implode(",", $in) . "\n";
            } else {
                $arr = "no matching records found\n";
            }
            $name = "stock-aging-report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getStockAgingDetails($postData)
    {
        $dataList = [];

        $isAllProducts = filter_var($postData['isAllProducts'], FILTER_VALIDATE_BOOLEAN);
        $productIds = ($isAllProducts) ? [] : $postData['productIds'];
        $categoryIds = (empty($postData['categoryIds'])) ? null : $postData['categoryIds'];

        $displayData = $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();

        if ($displayData['FIFOCostingFlag'] == 1) {
            $results = $this->CommonTable('Inventory\Model\ItemInTable')->getStockAgeDetails(date("Y-m-d"), $productIds, $categoryIds);
        } else {
            $results = $this->CommonTable('Inventory\Model\ItemInTable')->getStockAgeDetailsForAvg(date("Y-m-d"), $productIds, $categoryIds);
            
        }
        foreach ($results as $result) {
            if (!array_key_exists($result['productID'], $dataList)) {
                $dataList[$result['productID']] = [
                    'productName' => $result['productName'],
                    'productCode' => $result['productCode'],
                    'categoryName' => $result['categoryName'],
                    'Over270StockValue' => $result['Over270StockValue'],
                    'Over270Qty' => $result['Over270Qty'],
                    'WithIn270StockValue' => $result['Over270Qty'],
                    'WithIn270Qty' => $result['WithIn270Qty'],
                    'WithIn180StockValue' => $result['WithIn180StockValue'],
                    'WithIn180Qty' => $result['WithIn180Qty'],
                    'WithIn90StockValue' => $result['WithIn90StockValue'],
                    'WithIn90Qty' => $result['WithIn90Qty'],
                    'WithIn30StockValue' => $result['WithIn30StockValue'],
                    'WithIn30Qty' => $result['WithIn30Qty']
                ];
            } else {
                $dataList[$result['productID']]['Over270StockValue'] += $result['Over270StockValue'];
                $dataList[$result['productID']]['Over270Qty'] += $result['Over270Qty'];
                $dataList[$result['productID']]['WithIn270StockValue'] += $result['WithIn270StockValue'];
                $dataList[$result['productID']]['WithIn270Qty'] += $result['WithIn270Qty'];
                $dataList[$result['productID']]['WithIn180StockValue'] += $result['WithIn180StockValue'];
                $dataList[$result['productID']]['WithIn180Qty'] += $result['WithIn180Qty'];
                $dataList[$result['productID']]['WithIn90StockValue'] += $result['WithIn90StockValue'];
                $dataList[$result['productID']]['WithIn90Qty'] += $result['WithIn90Qty'];
                $dataList[$result['productID']]['WithIn30StockValue'] += $result['WithIn30StockValue'];
                $dataList[$result['productID']]['WithIn30Qty'] = $dataList[$result['productID']]['WithIn30Qty'] + $result['WithIn30Qty'];
            }
        }
        return $dataList;
    }

    private function getProductsWithAvgCosting($proIds = NULL, $locIds, $isAllProducts = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $proIds = empty($proIds) ? [] : $proIds;
        if ((isset($proIds) || $isAllProducts ) && isset($locIds)) {
            $globalData['pD'] = array_fill_keys($locIds, '');
            $globalData = array_fill_keys($proIds, $globalData);

            //get available quantity by locationProduct table
            $productList = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyListByLocID($proIds, $locIds, $isAllProducts, null,true);

            foreach ($productList as $p) {
                $globalData[$p['pID']] = array(
                    'pN' => $p['pName'],
                    'pCD' => $p['pCD']);
            }
            $proIds = array_keys($globalData);

            foreach ($productList as $p) {
                //get quantity and unit price according to dispaly uom
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($p['pID']);
                $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUom);
                $unitPrice = $this->getProductUnitPriceViaDisplayUom($p['avgCost'], $productUom);
                $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                $globalData[$p['pID']]['pD'][$p['locID']]['aQty'] = $quantity;
                $globalData[$p['pID']]['pD'][$p['locID']]['uomAbbr'] = $thisqty['uomAbbr'];
                $globalData[$p['pID']]['pD'][$p['locID']]['costPrice'] = $unitPrice;
                $globalData[$p['pID']]['pD'][$p['locID']]['TotalValue'] = $unitPrice*$quantity;
                $globalData[$p['pID']]['pD'][$p['locID']]['locName'] = $p['locName'];

            }
            return $globalData;
        }
    }

    public function getLocationAndDateWiseStockDetails($proIDs = null, $locIDs = null, $isAllProducts = false, $toDate = null, $categoryIds, $isDocDateWise = false)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $proIDs = (empty($proIDs)) ? [] : $proIDs;
        $categoryIds = (empty($categoryIds)) ? null : $categoryIds;
        if ((isset($proIDs) || $isAllProducts) && isset($locIDs) && isset($toDate)) {
            $productLists = array();
            $products = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductsByLocIDAndProID($proIDs, $locIDs, $isAllProducts, true, $categoryIds);
            $newDate   = date('Y-m-d', strtotime($toDate . ' +1 day'));

            if ($isDocDateWise) {

                //Item In Data
                $itemInDataOfSalesReturn = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForSalesReturnWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);
                $itemInDataOfCreditNoteData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForCreditNoteWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);
                $itemInDataOfGrnData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForGrnWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);
                $itemInDataOfPostiveAdjData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForPostiveAdjustmentWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);
                $itemInDataOfPurchaseInvoiceData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForPurchaseInvoiceWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);
                $itemInDataOfExpensePaymentVoucherData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForExpensePurchaseVoucherWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);
                $itemInDataOfInvoiceData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForSalesInvoiceWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);


                $locItemInList = array_merge_recursive($itemInDataOfSalesReturn, $itemInDataOfCreditNoteData, $itemInDataOfGrnData, $itemInDataOfPostiveAdjData, $itemInDataOfPurchaseInvoiceData, $itemInDataOfExpensePaymentVoucherData, $itemInDataOfInvoiceData);



                //Item Out Data
                $itemOutDataOfSaleInvoice = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfSaleInvoiceDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);
                $itemOutDataDLN = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfDeliveryNoteDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);
                $itemOutDataGRN = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfGRNDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);
                $itemOutDataPICancel = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfPurchaseInvoiceCancelDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);
                $itemOutDataEXPVCancel = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfEXPVCancelDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);
                $itemOutDataGoodIssue = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfGoodIssueDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);
                $itemOutDataPurchaseReturn = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfPurchaseReturnDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);
                $itemOutDataDebitNote = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfDebitNoteDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);
                // $itemOutDataActivity = $this->CommonTable('Inventory\Model\LocationProductTable')->getRangeItemOutActivityDetails($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);

                $locItemOutList = array_merge_recursive($itemOutDataOfSaleInvoice, $itemOutDataDLN, $itemOutDataGoodIssue, $itemOutDataPurchaseReturn, $itemOutDataDebitNote,$itemOutDataGRN, $itemOutDataPICancel, $itemOutDataEXPVCancel);

            } else {


                //Item In Data
                $itemInDataOfSalesReturn = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForSalesReturnWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, false, true);
                $itemInDataOfCreditNoteData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForCreditNoteWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, false, true);
                $itemInDataOfGrnData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForGrnWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, false, true);
                $itemInDataOfPostiveAdjData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForPostiveAdjustmentWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, false, true);
                $itemInDataOfPurchaseInvoiceData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForPurchaseInvoiceWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, false, true);
                $itemInDataOfExpensePaymentVoucherData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForExpensePurchaseVoucherWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, false, true);
                $itemInDataOfInvoiceData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForSalesInvoiceWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, false, true);


                $locItemInList = array_merge_recursive($itemInDataOfSalesReturn, $itemInDataOfCreditNoteData, $itemInDataOfGrnData, $itemInDataOfPostiveAdjData, $itemInDataOfPurchaseInvoiceData, $itemInDataOfExpensePaymentVoucherData, $itemInDataOfInvoiceData);


                //Item Out Data
                $itemOutDataOfSaleInvoice = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfSaleInvoiceDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds,  true);
                $itemOutDataDLN = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfDeliveryNoteDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds,  true);
                $itemOutDataGRN = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfGRNDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds,  true);
                $itemOutDataPICancel = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfPurchaseInvoiceCancelDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);
                $itemOutDataEXPVCancel = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfEXPVCancelDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds,  true);
                $itemOutDataGoodIssue = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfGoodIssueDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);
                $itemOutDataPurchaseReturn = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfPurchaseReturnDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds,  true);
                $itemOutDataDebitNote = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListOfDebitNoteDetailsByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);

                $itemOutDataCancelCreditNote = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForCreditNoteCancelWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);
                
                // var_dump($itemInDataOfGrnData).die();
                // $itemOutDataActivity = $this->CommonTable('Inventory\Model\LocationProductTable')->getRangeItemOutActivityDetails($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);

                $locItemOutList = array_merge_recursive($itemOutDataOfSaleInvoice, $itemOutDataDLN, $itemOutDataGoodIssue, $itemOutDataPurchaseReturn, $itemOutDataDebitNote,$itemOutDataGRN, $itemOutDataPICancel, $itemOutDataEXPVCancel, $itemOutDataCancelCreditNote);


                // $locItemInList = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);


                // $locItemOutList = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyOutListByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds);
            }


            if ($isDocDateWise) {
                $locTransferItemInList = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsOfTransferWithDocumentDateAndLocID($proIDs, $locIDs, $isAllProducts, '',$toDate, true, $categoryIds);
            } else {

                $locTransferItemInList = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsOfTransferWithDocumentDateAndLocID($proIDs, $locIDs, $isAllProducts, '',$toDate, true, $categoryIds);
                // $locTransferItemInList = $this->CommonTable('Inventory\Model\transferProductTable')->getProductTransferQtyInListByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$toDate, true, $categoryIds);
            }


            // if categoryIDs null then get all category ids
            if ($categoryIds == null) {
                $allCategories = $this->CommonTable('Inventory\Model\CategoryTable')->actionFetchAll();
                foreach ($allCategories as $key => $cateValue) {
                    $categoryIds[] = $cateValue['categoryID'];
                }
            }
            $categories = implode(",", $categoryIds);
            
            // if product ids null then get all product ids 
            if (sizeof($proIDs) == 0) {
                $allProIDs = $this->CommonTable('Core\Model\ProductTable')->getAllActiveProducts();
                foreach ($allProIDs as $key => $productValue) {
                    $proIDs[] = $productValue['productID'];
                }
            }
            $productsList = implode(",", $proIDs);
            $locationsList = implode(",", $locIDs);        

            $locTransferItemOutList = $this->CommonTable('Inventory\Model\transferProductTable')->gettransferProductSetByEndDate($productsList, $locationsList , '',$toDate, $categories, $isDocDateWise);
            
            $locationItemInData = [];
            $locationItemOutData = [];
            $transferItemInData = [];
            // $transferItemOutData = [];

            // set item in data to the array
            foreach ($locItemInList as $pIn) {
                $locationItemInData[$pIn['pID']]['pQtyIn'] += $pIn['qtyIn'];
                $locationItemInData[$pIn['pID']]['pQtyInCost'] +=  $pIn['costIn'];
            }

            // set itemOut data to the array
            foreach ($locItemOutList as $pOut) {
                $locationItemOutData[$pOut['pID']]['pQtyOut'] += $pOut['qtyOut'];
                $locationItemOutData[$pOut['pID']]['pQtyOutCost'] += $pOut['qtyOutCost'];
            }

            // set transfer in data to the array
            foreach ($locTransferItemInList as $pTranIn) {
                $transferItemInData[$pTranIn['pID']]['pQtyTransferIn'] = $pTranIn['qtyTransferIn'];
                $transferItemInData[$pTranIn['pID']]['pQtyTransferInCost'] = $pTranIn['inCost'];
            }


            if ($isDocDateWise) {
                // set transfer out data to the array
                $transferProductArray = [];
                foreach ($locTransferItemOutList as $pTranOutIn) {
                    if ($pTranOutIn['transferDate'] < $newDate) {
                        if (!$transferProductArray[$pTranOutIn['transferProductID']]) {
                            if ($transferItemOutData[$pTranOutIn['pID']]) {
                                $transferItemOutData[$pTranOutIn['pID']]['pQtyTransferOut'] += $pTranOutIn['transferProductQuantity'];
                                $transferItemOutData[$pTranOutIn['pID']]['pQtyTransferOutCost'] += floatval($pTranOutIn['itemInAverageCostingPrice'])*floatval($pTranOutIn['transferProductQuantity']);
                            } else {
                                $transferItemOutData[$pTranOutIn['pID']]['pQtyTransferOut'] = $pTranOutIn['transferProductQuantity'];
                                $transferItemOutData[$pTranOutIn['pID']]['pQtyTransferOutCost'] = floatval($pTranOutIn['itemInAverageCostingPrice'])*floatval($pTranOutIn['transferProductQuantity']);
                                
                            }
                            $transferProductArray[$pTranOutIn['transferProductID']] = true;
                        }
                    }
                }
            } else {
                $transferProductArray = [];
                foreach ($locTransferItemOutList as $pTranOutIn) {
                    if ($pTranOutIn['itemInDateAndTime'] < $newDate) {
                        if (!$transferProductArray[$pTranOutIn['transferProductID']]) {
                            if ($transferItemOutData[$pTranOutIn['pID']]) {
                                $transferItemOutData[$pTranOutIn['pID']]['pQtyTransferOut'] += $pTranOutIn['transferProductQuantity'];
                                $transferItemOutData[$pTranOutIn['pID']]['pQtyTransferOutCost'] += floatval($pTranOutIn['itemInAverageCostingPrice'])*floatval($pTranOutIn['transferProductQuantity']);
                            } else {
                                $transferItemOutData[$pTranOutIn['pID']]['pQtyTransferOut'] = $pTranOutIn['transferProductQuantity'];
                                $transferItemOutData[$pTranOutIn['pID']]['pQtyTransferOutCost'] = floatval($pTranOutIn['itemInAverageCostingPrice'])*floatval($pTranOutIn['transferProductQuantity']);
                                
                            }
                            $transferProductArray[$pTranOutIn['transferProductID']] = true;
                        }
                    }
                }
            }


            if (!$isDocDateWise) {
                // get itemIn future details that given date
                // $inDate = $this->CommonTable('Inventory\Model\ItemInTable')->getAllItemDetailsAfterSelectedDate($newDate, $locIDs, $proIDs);
                // $itemCost = [];
                // foreach ($inDate as $key => $value) {
                //     if (!$itemCost[$value['productID']]) {
                //         $itemCost[$value['productID']] = floatval($value['itemInAverageCostingPrice']);
                //     }
                    
                // }

                //Item In Data
                $ungroupeditemInDataOfSalesReturn = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForSalesReturnWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true, true);
                $ungroupeditemInDataOfCreditNoteData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForCreditNoteWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true, true);
                $ungroupeditemInDataOfGrnData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForGrnWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true, true);
                $ungroupeditemInDataOfPostiveAdjData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForPostiveAdjustmentWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true, true);
                $ungroupeditemInDataOfPurchaseInvoiceData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForPurchaseInvoiceWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true, true);
                $ungroupeditemInDataOfExpensePaymentVoucherData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForExpensePurchaseVoucherWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true, true);
                $ungroupeditemInDataOfInvoiceData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForSalesInvoiceWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true);


                $ungroupedlocItemInList = array_merge_recursive($ungroupeditemInDataOfSalesReturn, $ungroupeditemInDataOfCreditNoteData, $ungroupeditemInDataOfGrnData, $ungroupeditemInDataOfPostiveAdjData, $ungroupeditemInDataOfPurchaseInvoiceData, $ungroupeditemInDataOfExpensePaymentVoucherData, $ungroupeditemInDataOfInvoiceData);


                array_multisort(array_column($ungroupedlocItemInList, 'itemInID'), SORT_DESC, $ungroupedlocItemInList);

                $itemCost = [];
                foreach ($ungroupedlocItemInList as $key => $value) {
                    if (!$itemCost[$value['pID']]) {
                        $itemCost[$value['pID']] = floatval($value['itemInAverageCostingPrice']);
                    }
                    
                }

            } else {

                //Item In Data
                $ungroupeditemInDataOfSalesReturn = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForSalesReturnWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true);
                $ungroupeditemInDataOfCreditNoteData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForCreditNoteWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true);
                $ungroupeditemInDataOfGrnData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForGrnWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true);
                $ungroupeditemInDataOfPostiveAdjData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForPostiveAdjustmentWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true);
                $ungroupeditemInDataOfPurchaseInvoiceData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForPurchaseInvoiceWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true);
                $ungroupeditemInDataOfExpensePaymentVoucherData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForExpensePurchaseVoucherWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true, true);
                $ungroupeditemInDataOfInvoiceData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductQtyInListForSalesInvoiceWithDocumentDateByLocIDAndDate($proIDs, $locIDs, $isAllProducts, '',$newDate, true, $categoryIds, true);


                $ungroupedlocItemInList = array_merge_recursive($ungroupeditemInDataOfSalesReturn, $ungroupeditemInDataOfCreditNoteData, $ungroupeditemInDataOfGrnData, $ungroupeditemInDataOfPostiveAdjData, $ungroupeditemInDataOfPurchaseInvoiceData, $ungroupeditemInDataOfExpensePaymentVoucherData, $ungroupeditemInDataOfInvoiceData);


                array_multisort(array_column($ungroupedlocItemInList, 'documentDate'), SORT_DESC, $ungroupedlocItemInList);

                $itemCost = [];
                foreach ($ungroupedlocItemInList as $key => $value) {
                    if (!$itemCost[$value['pID']]) {
                        $itemCost[$value['pID']] = floatval($value['itemInAverageCostingPrice']);
                    }
                    
                }
            }

            if ($isDocDateWise) {
                foreach ($products as $p) {
                    $productLists[$p['pID']]['pCD'] = $p['pCD'];
                    $productLists[$p['pID']]['pID'] = $p['pID'];
                    $productLists[$p['pID']]['pName'] = $p['pName'];
                    $productLists[$p['pID']]['categoryName'] = $p['categoryName'];
                    $productLists[$p['pID']]['pQtyIn'] = 0;
                    $productLists[$p['pID']]['pQtyOut'] = 0;
                    $productLists[$p['pID']]['pQtyIn'] = floatval($locationItemInData[$p['pID']]['pQtyIn']) + floatval($transferItemInData[$p['pID']]['pQtyTransferIn']);
                    $productLists[$p['pID']]['pQtyInCost'] = floatval($locationItemInData[$p['pID']]['pQtyInCost']);
                    $productLists[$p['pID']]['pQtyOut'] = floatval($locationItemOutData[$p['pID']]['pQtyOut']) + floatval($transferItemOutData[$p['pID']]['pQtyTransferOut']);
                    $productLists[$p['pID']]['pQtyOutCost'] = floatval($locationItemOutData[$p['pID']]['pQtyOutCost']) + floatval($transferItemOutData[$p['pID']]['pQtyTransferOutCost']);
                    $productLists[$p['pID']]['pTotQty'] = $productLists[$p['pID']]['pQtyIn'] -  $productLists[$p['pID']]['pQtyOut'];
                    $productLists[$p['pID']]['pRemCost'] = floatval($productLists[$p['pID']]['pTotQty']) * floatval($itemCost[$p['pID']]);
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($p['pID']);
                            $thisqty = $this->getProductQuantityViaDisplayUom($productLists[$p['pID']]['pTotQty'], $productUom);
                            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                            $productLists[$p['pID']]['pTotQty'] = $thisqty['quantity'];
                            $productLists[$p['pID']]['uomAbbr'] = $thisqty['uomAbbr'];

                }
            } else {
                foreach ($products as $p) {
                    $productLists[$p['pID']]['pCD'] = $p['pCD'];
                    $productLists[$p['pID']]['pID'] = $p['pID'];
                    $productLists[$p['pID']]['pName'] = $p['pName'];
                    $productLists[$p['pID']]['categoryName'] = $p['categoryName'];
                    $productLists[$p['pID']]['pQtyIn'] = 0;
                    $productLists[$p['pID']]['pQtyOut'] = 0;
                    $productLists[$p['pID']]['pQtyIn'] = floatval($locationItemInData[$p['pID']]['pQtyIn']) + floatval($transferItemInData[$p['pID']]['pQtyTransferIn']);
                    $productLists[$p['pID']]['pQtyInCost'] = floatval($locationItemInData[$p['pID']]['pQtyInCost']);
                    $productLists[$p['pID']]['pQtyOut'] = floatval($locationItemOutData[$p['pID']]['pQtyOut']) + floatval($transferItemOutData[$p['pID']]['pQtyTransferOut']);
                    $productLists[$p['pID']]['pQtyOutCost'] = floatval($locationItemOutData[$p['pID']]['pQtyOutCost']) + floatval($transferItemOutData[$p['pID']]['pQtyTransferOutCost']);
                    $productLists[$p['pID']]['pTotQty'] = $productLists[$p['pID']]['pQtyIn'] -  $productLists[$p['pID']]['pQtyOut'];
                    $productLists[$p['pID']]['pRemCost'] = floatval($productLists[$p['pID']]['pTotQty']) * floatval($itemCost[$p['pID']]);
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($p['pID']);
                            $thisqty = $this->getProductQuantityViaDisplayUom($productLists[$p['pID']]['pTotQty'], $productUom);
                            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                            $productLists[$p['pID']]['pTotQty'] = $thisqty['quantity'];
                            $productLists[$p['pID']]['uomAbbr'] = $thisqty['uomAbbr'];

                }   
            }


            
            
            return $productLists;
        }

    }

    public function viewLocationAndDateWiseStockDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $locIDs = $request->getPost('locationIdList');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $isDocDateWise = ($request->getPost('isDocDateWise') == "true") ? true : false;
            $toDate = $request->getPost('toDate');

            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $allLocations = $this->allLocations;
            $locations = array_intersect_key($allLocations, array_flip($locIDs));
            $locNames = array();
            foreach ($locations as $key => $value) {
                $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
            }

            $translator = new Translator();

            if ($isDocDateWise) {
                $name = $translator->translate('Location wise Stock Report - Date wise (Document date wise)');
            } else {
                $name = $translator->translate('Location wise Stock Report - Date wise (Actual date wise)');
            }

            $companyDetails = $this->getCompanyDetails();
            $productLists = $this->getLocationAndDateWiseStockDetails($proIDs, $locIDs, $isAllProducts, $toDate, $categoryIds, $isDocDateWise);

            $headerView = $this->headerViewTemplate($name, $period = $toDate);
            $headerView->setTemplate('reporting/template/headerTemplateEndDate');
            $headerViewRender = $this->htmlRender($headerView);

            $productListsView = new ViewModel(array(
                'lL' => $locNames,
                'lpL' => $productLists,
                'cD' => $companyDetails,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $productListsView->setTemplate('reporting/stock-in-hand-report/generate-location-and-date-wise-stock-pdf');

            $this->html = $productListsView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateLocationAndDateWiseStockPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $locIDs = $request->getPost('locationIdList');
            $isDocDateWise = ($request->getPost('isDocDateWise') == "true") ? true : false;
            $toDate = $request->getPost('toDate');
            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $allLocations = $this->allLocations;
            $locations = array_intersect_key($allLocations, array_flip($locIDs));
            $locNames = array();
            foreach ($locations as $key => $value) {
                $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
            }

            $translator = new Translator();
            if ($isDocDateWise) {
                $name = $translator->translate('Location wise Stock Report - Date wise (Document date wise)');
            } else {
                $name = $translator->translate('Location wise Stock Report - Date wise (Actual date wise)');
            }

            $cD = $this->getCompanyDetails();
            $productLists = $this->getLocationAndDateWiseStockDetails($proIDs, $locIDs, $isAllProducts, $toDate, $categoryIds, $isDocDateWise);

            $headerView = $this->headerViewTemplate($name, $period = $toDate);
            $headerView->setTemplate('reporting/template/headerTemplateEndDate');
            $headerViewRender = $this->htmlRender($headerView);

            $locationWiseStock = new ViewModel(array(
                'lL' => $locNames,
                'lpL' => $productLists,
                'cD' => $cD,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $locationWiseStock->setTemplate('reporting/stock-in-hand-report/generate-location-and-date-wise-stock-pdf');
            $locationWiseStock->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($locationWiseStock);
            $pdfPath = $this->downloadPDF('location-wise-stock', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateLocationAndDateWiseStockSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $proIDs = $request->getPost('productIds');
            $isAllProducts = $request->getPost('isAllProducts');
            $categoryIds = $request->getPost('categoryIds');
            $locIDs = $request->getPost('locationIdList');
            $toDate = $request->getPost('toDate');
            $isDocDateWise = ($request->getPost('isDocDateWise') == "true") ? true : false;

            if (empty($locIDs)) {
                $locIDs = $this->getActiveAllLocationsIds();
            }
            $allLocations = $this->allLocations;
            $locations = array_intersect_key($allLocations, array_flip($locIDs));
            $locNames = array();
            foreach ($locations as $key => $value) {
                $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
            }

            $cD = $this->getCompanyDetails();
            $productLists = $this->getLocationAndDateWiseStockDetails($proIDs, $locIDs, $isAllProducts, $toDate, $categoryIds, $isDocDateWise);

            if ($productLists) {
                $title = '';

                if ($isDocDateWise) {
                    $tit = 'LOCATION WISE STOCK REPORT - DATE WISE (Document date wise)';
                } else {
                    $tit = 'LOCATION WISE STOCK REPORT - DATE WISE (Actual date wise)';
                }
                // $tit = 'LOCATION WISE STOCK REPORT - DATE WISE';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";
                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $in = ['End Date: ' . $toDate];
                $title.=implode(",", $in) . "\n";

                $in = ['Showing results of locations'];
                ksort($locNames);
                $arrsLoc = array_merge($in, $locNames);

                $title.=implode(",", $arrsLoc) . "\n";

                $arrs = array("NO", "PRODUCT NAME"," PRODUCT CODE", "PRODUCT CATEGORY", "TOTAL IN QUANTITY","", "TOTAL OUT QUANTITY","", "STOCK IN HAND","","", "STOCK IN HAND COST");


                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                $currencySym = $this->companyCurrencySymbol;
                $pRemCost = 0;
                foreach ($productLists as $proID => $proData) {
                    $qtyIn = $proData['pQtyIn'] ? round($proData['pQtyIn'],2) : 0.00;
                    $qtyOut = $proData['pQtyOut'] ? round($proData['pQtyOut'],2) : 0.00;
                    $qty = $proData['pTotQty'] ? round($proData['pTotQty'],2) : 0.00;
                    $uomAbbr = $proData['uomAbbr'] ? $proData['uomAbbr'] : '';
                    $cost = $proData['pRemCost'] ? round($proData['pRemCost'],2) : 0.00;

                    $pRemCost = ($proData['pRemCost']) ? $pRemCost + $proData['pRemCost'] : $pRemCost + 0;

                    $in = [
                        $i,
                        $proData['pName'], 
                        $proData['pCD'],
                        $proData['categoryName'],
                        $qtyIn,  
                        $uomAbbr,
                        $qtyOut,
                        $uomAbbr,
                        $qty, 
                        $uomAbbr,
                        $currencySym, 
                        $cost
                    ];

                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }

            $in = [
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                ''
            ];

            $arr.=implode(",", $in) . "\n";
            $in = [
                '',
                'Total',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                $currencySym,
                round($pRemCost,2)
            ];
            $arr.=implode(",", $in) . "\n";
        }

        $name = "location_and_date_wise_stock_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }
    /**
    * get location wise item qty with batch details
    * @param array $productIDs
    * @param array $locationIDs
    * @param boolean $isAllProducts
    * @return array
    **/
    public function getLocationWiseStockWithBatchDetaials($productIDs, $locationIDs, $isAllProducts, $categoryIds)
    {
        $allProductFlag = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $categoryIds = (empty($categoryIds)) ? null : $categoryIds;
        $batchProductDetails = [];
        $serialProductDetails = [];
        //get all active product with their batch details
        $batchProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getBatchDetailsByLocationIDsAndProductIDs($locationIDs, $productIDs, $allProductFlag, $categoryIds);
        //get all active products with their serial details
        $serialProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getSerialDetailsByLocationIDsAndProductIDs($locationIDs, $productIDs, $allProductFlag, $categoryIds);

        if (!empty($batchProductDetails) && !empty($serialProductDetails)) {
            $allDataSet = array_merge($batchProductDetails,$serialProductDetails);

        } else if (!empty($serialProductDetails)) {
            $allDataSet = $serialProductDetails;

        } else if (!empty($batchProductDetails)) {
            $allDataSet = $batchProductDetails;
        }

        foreach ($allDataSet as $key => $value) {
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($value['productID']);
            $qty = ($value['productBatchQuantity']) ? $value['productBatchQuantity'] : 1;

            $thisqty = $this->getProductQuantityViaDisplayUom($value['productBatchQuantity'], $productUom);
            $thisPrice = $unitPrice = $this->getProductUnitPriceViaDisplayUom($value['itemInPrice'], $productUom);

            $allDataSet[$key]['uomAbbr'] = $thisqty['uomAbbr'];
            $allDataSet[$key]['productBatchQuantity'] = $thisqty['quantity'];
            $allDataSet[$key]['itemInPrice'] = $thisPrice;
        }

        return $allDataSet;
    }

    public function viewSerialMovementDetailsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespondHtml();    
        } 
        $proIDs = $request->getPost('productIds');
        $isAllProducts = $request->getPost('isAllProducts');
        $serialIds = $request->getPost('serialIds');
        $isAllSerial = $request->getPost('isAllSerial');
        
        $translator = new Translator();
        $name = $translator->translate('Serial Item Movement Report');

        $companyDetails = $this->getCompanyDetails();
        $productLists = $this->getService('StockInHandReportService')->serialItemMovingData($proIDs, $isAllProducts, $serialIds, $isAllSerial);

        $headerView = $this->headerViewTemplate($name, null);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $productListsView = new ViewModel(array(
            'lpL' => $productLists,
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
        ));
        $productListsView->setTemplate('reporting/stock-in-hand-report/generate-serial-item-movement-pdf');

        $this->html = $productListsView;
        $this->status = true;
        return $this->JSONRespondHtml();
    }

    public function viewSerialMovementDetailsPdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['serial-item-moving'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
        
    }


    public function viewSerialMovementDetailsSheetAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }


            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['serial-item-moving'], 'csv');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }
    
    /**
    * this function use to get serial movement details
    * @param array itemId(s)
    * @param array serialId(s)
    * return mix
    **/
    public function serialItemMovingData($proIDs, $isAllProducts, $serialIds, $isAllSerial ) 
    {
        // get item in and Item out details for the selected item and serial ids
        $allProductFlag = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $allSerialFlag = filter_var($isAllSerial, FILTER_VALIDATE_BOOLEAN);
        if ($allSerialFlag) {
            $serialIds = null;       
        } 
        if ($allProductFlag) {
            $proIDs = null;       
        }
        $itemInOutDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getSerialItemDetails($proIDs,$serialIds);
        $returnValue = $this->setDocumentTypesAndCodes ($itemInOutDetails);
        return $returnValue;

    }
    
    /**
    * generate data set for serial movement reports
    **/ 
    public function setDocumentTypesAndCodes ($transactions)
    {
        $serialItemSet = [];
        foreach ($transactions as $key => $transData) {
            if ($transData['productSerialCode'] && $transData['productCode'] && $transData['productName']) {

                $dataList = array(
                    'productName' => $transData['productName']." - ".$transData['productCode'],
                    'productID' => $transData['productID'],
                    'serialCode' => $transData['productSerialCode'],
                    'serialID' => $transData['productSerialID'],
                    'locationName' => $transData['locationName']." - ".$transData['locationCode'],
                    'locationID' => $transData['locationID'],
                    'inDocType' => $transData['itemInDocumentType'],
                    'outDocType' => $transData['itemOutDocumentType'],
                    'soldQty' => $transData['itemInSoldQty']
                    );
                switch ($transData['itemInDocumentType']) {
                    case 'Goods Received Note':
                            $dataList['inDocCode'] = $transData['inGrnCode'];
                        break;
                    case 'Payment Voucher':
                            $dataList['inDocCode'] = $transData['inPICode'];
                        break;
                    case 'Expense Payment Voucher':
                            $dataList['inDocCode'] = $transData['inPVCode'];
                        break;
                    case 'Invoice Edit':
                    case 'Invoice Cancel':
                            $dataList['inDocCode'] = $transData['inInvCode'];
                        break;
                    case 'Delivery Note Edit':
                            $dataList['inDocCode'] = $transData['inDelCode'];
                        break;
                    case 'Sales Returns':
                            $dataList['inDocCode'] = $transData['salesReturnCode'];
                        break;
                    case 'Credit Note':
                            $dataList['inDocCode'] = $transData['inCredCode'];
                        break;
                    case 'Inventory Positive Adjustment':
                    case 'Delete Negative Adjustment':
                            $dataList['inDocCode'] = $transData['inAdjCode'];
                        break;
                    case 'Inventory Transfer':
                            $dataList['inDocCode'] = $transData['transferCode'];
                            $serialItemSet = $this->setTranferData($transData,$serialItemSet);
                        break;
                    default:
                            $dataList['inDocCode'] = '';
                        break;
                }

                switch ($transData['itemOutDocumentType']) {
                    case 'Sales Invoice':
                            $dataList['outDocCode'] = $transData['outInvCode'];
                        break;
                    case 'Delivery Note':
                            $dataList['outDocCode'] = $transData['outDelCode'];
                        break;
                    case 'Goods Received Note Cancel':
                            $dataList['outDocCode'] = $transData['outGrnCode'];
                        break;
                    case 'Inventory Negative Adjustment':
                    case 'Delete Positive Adjustment':
                            $dataList['outDocCode'] = $transData['outAdjCode'];
                        break;
                    case 'Payment Voucher Cancel':
                            $dataList['outDocCode'] = $transData['outPICode'];
                        break;
                    case 'Purchase Return':
                            $dataList['outDocCode'] = $transData['purchaseReturnCode'];
                        break;
                    default:
                            $dataList['outDocCode'] = '';
                        break;
                }
                $serialItemSet[$transData['productID']][$transData['productSerialCode']][] = $dataList;
            }
                
        }
        return $serialItemSet;
    }
   
    /**
    * add transfer details to the serial movement report
    **/
    public function setTranferData($transData,$serialItemSet)
    {
        
        foreach ($serialItemSet[$transData['productID']][$transData['productSerialCode']] as $key => $value) {
            if ($value['outDocType'] == '' && $value['soldQty'] == 1 && $value['locationID'] == $transData['locationIDOut']) {
                $serialItemSet[$transData['productID']][$transData['productSerialCode']][$key]['outDocType'] = 'Inventory Transfer';
                $serialItemSet[$transData['productID']][$transData['productSerialCode']][$key]['outDocCode'] = $transData['transferCode'];
            }
        }
        return $serialItemSet;
    }

}
