<?php

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains ChequeReport related actions
 */
class ChequeReportController extends CoreController
{

    protected $sideMenus = 'expenses_side_menu';
    protected $upperMenus = 'expenses_reports_upper_menu';

    public function generateChequeReportAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();

            $accountId = $postData['accountId'];
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $chequeType = $postData['chequeType'];
            $period = ($postData['fromDate'] && $postData['toDate']) ? $postData['fromDate'] . ' - ' . $postData['toDate'] : 'All time';
            $reconcileStatus = (strlen($postData['reconcile']) > 0 ) ? $postData['reconcile'] : null;

            $cheques = $depositedCheques = $issuedCheques = $adPayissuedCheques = array();
            //get deposited cheques
            if (empty($chequeType) || $chequeType == 1) {
                $depositedCheques = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getDepositedChequeByAccountId($accountId, $reconcileStatus, $fromDate, $toDate);
                $depositedCheques = array_map(function($transaction) {
                    $transaction['refNum'] = $transaction['incomingPaymentMethodChequeNumber'];
                    $transaction['bank'] = $transaction['incomingPaymentMethodChequeBankName'];
                    $transaction['date'] = $transaction['chequeDepositDate'];
                    $transaction['type'] = 'Received';
                    $transaction['amount'] = $transaction['incomingPaymentMethodAmount'];
                    $transaction['recStatus'] = $transaction['incomingPaymentMethodChequeReconciliationStatus'];
                    return $transaction;
                }, iterator_to_array($depositedCheques));
            }

            //get issued cheques
            if (empty($chequeType) || $chequeType == 2) {
                //get invoice payment issued cheques
                $issuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId($accountId, $reconcileStatus, $fromDate, $toDate);
                $issuedCheques = array_map(function($transaction) {
                    $transaction['refNum'] = $transaction['outGoingPaymentMethodReferenceNumber'];
                    $transaction['bank'] = '-';
                    $transaction['date'] = $transaction['outgoingPaymentDate'];
                    $transaction['type'] = 'Issued';
                    $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                    $transaction['recStatus'] = $transaction['outGoingPaymentMethodReconciliationStatus'];
                    return $transaction;
                }, iterator_to_array($issuedCheques));

                //get advanced payment issued cheques
                $adPayissuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId($accountId, $reconcileStatus, $fromDate, $toDate);
                $adPayissuedCheques = array_map(function($transaction) {
                    $transaction['refNum'] = $transaction['outGoingPaymentMethodReferenceNumber'];
                    $transaction['bank'] = '-';
                    $transaction['date'] = $transaction['outgoingPaymentDate'];
                    $transaction['type'] = 'Issued';
                    $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                    $transaction['recStatus'] = $transaction['outGoingPaymentMethodReconciliationStatus'];
                    return $transaction;
                }, iterator_to_array($adPayissuedCheques));
            }
            $result = array_merge($cheques, $depositedCheques, $issuedCheques, $adPayissuedCheques);

            $headerView = $this->headerViewTemplate("Cheque Report", $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'chequeData' => $result,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/cheque-report/generate-cheque-report');

            $this->status = true;
            $this->msg = 'Cheque report generated';
            $this->html = $view;
        }

        return $this->JSONRespondHtml();
    }

    public function generateChequeReportPdfAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();

            $accountId = $postData['accountId'];
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $chequeType = $postData['chequeType'];
            $period = ($postData['fromDate'] && $postData['toDate']) ? $postData['fromDate'] . ' - ' . $postData['toDate'] : 'All time';
            $reconcileStatus = (empty($postData['reconcile'])) ? null : $postData['reconcile'];

            $cheques = $depositedCheques = $issuedCheques = array();
            //get deposited cheques
            if (empty($chequeType) || $chequeType == 1) {
                $depositedCheques = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getDepositedChequeByAccountId($accountId, $reconcileStatus, $fromDate, $toDate);
                $depositedCheques = array_map(function($transaction) {
                    $transaction['refNum'] = $transaction['incomingPaymentMethodChequeNumber'];
                    $transaction['bank'] = $transaction['incomingPaymentMethodChequeBankName'];
                    $transaction['date'] = $transaction['chequeDepositDate'];
                    $transaction['type'] = 'Received';
                    $transaction['amount'] = $transaction['incomingPaymentMethodAmount'];
                    $transaction['recStatus'] = $transaction['incomingPaymentMethodChequeReconciliationStatus'];
                    return $transaction;
                }, iterator_to_array($depositedCheques));
            }

            //get issued cheques
            if (empty($chequeType) || $chequeType == 2) {
                $issuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId($accountId, $reconcileStatus, $fromDate, $toDate);
                $issuedCheques = array_map(function($transaction) {
                    $transaction['refNum'] = $transaction['outGoingPaymentMethodReferenceNumber'];
                    $transaction['bank'] = '-';
                    $transaction['date'] = $transaction['outgoingPaymentDate'];
                    $transaction['type'] = 'Issued';
                    $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                    $transaction['recStatus'] = $transaction['outGoingPaymentMethodReconciliationStatus'];
                    return $transaction;
                }, iterator_to_array($issuedCheques));
            }
            $result = array_merge($cheques, $depositedCheques, $issuedCheques);

            $headerView = $this->headerViewTemplate("Cheque Report", $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'chequeData' => $result,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/cheque-report/generate-cheque-report');
            $view->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($view);

            $pdfPath = $this->downloadPDF('cheque_report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;

            return $this->JSONRespond();
        }
    }

    public function generateChequeReportCsvAction()
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();

            $accountId = $postData['accountId'];
            $fromDate = $postData['fromDate'];
            $toDate = $postData['toDate'];
            $chequeType = $postData['chequeType'];
            $period = ($postData['fromDate'] && $postData['toDate']) ? $postData['fromDate'] . ' - ' . $postData['toDate'] : 'All time';
            $reconcileStatus = (empty($postData['reconcile'])) ? null : $postData['reconcile'];

            $cheques = $depositedCheques = $issuedCheques = array();
            //get deposited cheques
            if (empty($chequeType) || $chequeType == 1) {
                $depositedCheques = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getDepositedChequeByAccountId($accountId, $reconcileStatus, $fromDate, $toDate);
                $depositedCheques = array_map(function($transaction) {
                    $transaction['refNum'] = $transaction['incomingPaymentMethodChequeNumber'];
                    $transaction['bank'] = $transaction['incomingPaymentMethodChequeBankName'];
                    $transaction['date'] = $transaction['chequeDepositDate'];
                    $transaction['type'] = 'Received';
                    $transaction['amount'] = $transaction['incomingPaymentMethodAmount'];
                    $transaction['recStatus'] = $transaction['incomingPaymentMethodChequeReconciliationStatus'];
                    return $transaction;
                }, iterator_to_array($depositedCheques));
            }

            //get issued cheques
            if (empty($chequeType) || $chequeType == 2) {
                $issuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId($accountId, $reconcileStatus, $fromDate, $toDate);
                $issuedCheques = array_map(function($transaction) {
                    $transaction['refNum'] = $transaction['outGoingPaymentMethodReferenceNumber'];
                    $transaction['bank'] = '-';
                    $transaction['date'] = $transaction['outgoingPaymentDate'];
                    $transaction['type'] = 'Issued';
                    $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                    $transaction['recStatus'] = $transaction['outGoingPaymentMethodReconciliationStatus'];
                    return $transaction;
                }, iterator_to_array($issuedCheques));
            }
            $result = array_merge($cheques, $depositedCheques, $issuedCheques);

            if ($result) {

                $companyDetails = $this->getCompanyDetails();

                $data = array(
                    "Cheque Report"
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber,
                    "Period : " . $period
                );

                $title.=implode(",", $data) . "\n";

                $data = array(
                    "Period :" . $postData['fromDate'] . " - " . $postData['toDate']
                );
                $title.=implode(",", $data) . "\n";

                $tableHead = array('Cheque Reference', 'Bank Name', 'Cheque Amount', 'Date', 'Cheque Type');
                $header.=implode(",", $tableHead) . "\n";

                foreach ($result as $cheque) {
                    $data = array($cheque['refNum'], $cheque['bank'], $cheque['amount'], $cheque['date'], $cheque['type']);
                    $arr.=implode(",", $data) . "\n";
                }
            } else {
                $arr = "no matching records found";
            }
            $name = "cheque_report.csv";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

}
