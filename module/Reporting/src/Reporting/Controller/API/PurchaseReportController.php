<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains Purchase Report related controller functions
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class PurchaseReportController extends CoreController
{

    protected $userRoleID;

    public function getPurchaseOrderData($poIDs = null, $isAllPO = false)
    {
        $isAllPO = filter_var($isAllPO, FILTER_VALIDATE_BOOLEAN);
        if (isset($poIDs) || $isAllPO) {
            if (!$isAllPO) {
                $poListsByCode = array_fill_keys($poIDs, '');
            }

            $resPOStatus = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->poStatusDetails($poIDs, $isAllPO);

            foreach ($resPOStatus as $p) {
                $poListsByCode[$p['poID']] = $p;
            }

            return $poListsByCode;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return type
     */
    public function viewPurchaseOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $poIDs = $request->getPost('poIds');
            $isAllPO = $request->getPost('isAllPO');
            $translator = new Translator();
            $name = $translator->translate('Purchase Order Status Report');

            $poDetails = $this->getPurchaseOrderData($poIDs, $isAllPO);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewPurchaseOrder = new ViewModel(array(
                'poL' => $poDetails,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));


            $viewPurchaseOrder->setTemplate('reporting/purchase-report/generate-po-status-pdf');

            $this->setLogMessage("Retrive purchase order details for purchase order view report.");
            $this->html = $viewPurchaseOrder;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return pdf
     */
    public function generatePoStatusPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $poIds = $request->getPost('poIds');
            $isAllPO = $request->getPost('isAllPO');
            $translator = new Translator();
            $name = $translator->translate('Purchase Order Status Report');

            //getting PO data
            $poListsByCode = $this->getPurchaseOrderData($poIds, $isAllPO);
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewPurchaseOrder = new ViewModel(array(
                'cD' => $cD,
                'poL' => $poListsByCode,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));

            $viewPurchaseOrder->setTemplate('reporting/purchase-report/generate-po-status-pdf');
            $viewPurchaseOrder->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewPurchaseOrder);
            $pdfPath = $this->downloadPDF('po-status-report', $htmlContent, true);

            $this->setLogMessage("Retrive purchase order details for purchase order PDF report.");
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return csv PO Status csv file
     */
    public function generatePoStatusSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $poIds = $request->getPost('poIds');
            $isAllPO = $request->getPost('isAllPO');

            $poListsByCode = $this->getPurchaseOrderData($poIds, $isAllPO);
            $cD = $this->getCompanyDetails();
            if ($poListsByCode) {
                $title = '';
                $tit = 'PURCHASE ORDER STATUS REPORT';
                $in = ["", "", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["NO", "PO CODE", "PO STATUS", "SUPPLIER CODE", "SUPPLIER NAME", "EX. DE. DATE", "PO TOTAL($this->companyCurrencySymbol)"];
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                foreach ($poListsByCode as $key => $value) {
                    $sCD = $value['sCD'] ? $value['sCD'] : '-';
                    $sTitle = $value['sTitle'] ? $value['sTitle'] : '-';
                    $sName = $value['sName'] ? $value['sName'] : '-';
                    $poTot = $value['poTot'] ? $value['poTot'] : 0.00;
                    $poDate = $value['poDate'] ? $value['poDate'] : '-';

                    $in = [$i, $value['poCD'], $value['statusName'],$sCD,
                        $sTitle . ' ' . $sName,
                        $poDate,
                        $poTot];
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }
        }

        $name = "po_status_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->setLogMessage("Retrive purchase order details for purchase order CSV report.");
        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function viewPurchaseOrderWithGrnAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $dataSet = $request->getPost();
        $translator = new Translator();
        $name = $translator->translate('Purchase Orders With GRNs Report');

        $dataList = $this->getPurchaseOrdersWithGrnData($dataSet);
        $companyDetails = $this->getCompanyDetails();

        $headerView = $this->headerViewTemplate($name, $period = NULL);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $view = new ViewModel(array(
            'dataList' => $dataList,
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
        ));
        $view->setTemplate('reporting/purchase-report/generate-po-wise-grn-report');

        $this->setLogMessage("Retrive purchase order with GRN details for purchase order with GRN view report.");
        $this->html = $view;
        $this->status = true;

        return $this->JSONRespondHtml();
    }

    public function viewPurchaseOrderWithGrnPdfAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $dataSet = $request->getPost();
        $translator = new Translator();
        $name = $translator->translate('Purchase Orders With GRNs Report');

        $dataList = $this->getPurchaseOrdersWithGrnData($dataSet);
        $companyDetails = $this->getCompanyDetails();

        $headerView = $this->headerViewTemplate($name, $period = NULL);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $view = new ViewModel(array(
            'dataList' => $dataList,
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
        ));
        $view->setTemplate('reporting/purchase-report/generate-po-wise-grn-report');
        $view->setTerminal(true);

        $htmlContent = $this->viewRendererHtmlContent($view);
        $pdfPath = $this->downloadPDF('generate-po-wise-grn-report', $htmlContent, true);

        $this->setLogMessage("Retrive purchase order with GRN details for purchase order with GRN PDF report.");
        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function viewPurchaseOrderWithGrnCsvAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $dataSet = $request->getPost();
        $translator = new Translator();
        $name = $translator->translate('Purchase Orders With GRNs Report');

        $dataList = $this->getPurchaseOrdersWithGrnData($dataSet);
        $cD = $this->getCompanyDetails();

        if ($dataList) {
            $title = '';
            $tit = 'PURCHASE ORDER WITH GRNS REPORT';
            $in = ["", "", $tit];
            $title.=implode(",", $in) . "\n";

            $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
            $title.=implode(",", $in) . "\n";

            $arrs = ["PO CODE", "PO DATE", "DELIVERY DATE", "GRN NUMBER", "GRN DATE", "GRN DELAY", "COMPLETION"];
            $header = implode(",", $arrs);
            $arr = '';

            foreach ($dataList as $po) {
                $tottalCompletion = 0;

                $in = [
                    $po['poCode'],
                    $po['poDate'],
                    $po['poDeliveryDate'],
                ];
                $arr.= implode(",", $in) . "\n";

                foreach ($po['grnDetails'] as $grn) {
                    $tottalCompletion += $grn['grnCompletion'];

                    $in = [
                        '',
                        '',
                        '',
                        $grn['grnCode'],
                        $grn['grnDate'],
                        $grn['grnDelay'],
                        number_format($grn['grnCompletion'], 2, '.', '') .' %',
                    ];
                    $arr.= implode(",", $in) . "\n";
                }

                $in = [
                    '',
                    '',
                    '',
                    '',
                    '',
                    'TOTAL :',
                    number_format($tottalCompletion, 2, '.', '') . ' %',
                ];
                $arr.= implode(",", $in) . "\n";
            }
        } else {
            $arr = "no matching records found\n";
        }
        $name = "generate-po-wise-grn-report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->setLogMessage("Retrive purchase order with GRN details for purchase order with GRN CSV report.");
        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    private function getPurchaseOrdersWithGrnData($dataSet)
    {
        $isAllPo  = filter_var($dataSet['isAllPO'], FILTER_VALIDATE_BOOLEAN);
        $poIds    = ($isAllPo) ? [] : $dataSet['poIds'];
        $fromDate = $dataSet['fromDate'];
        $toDate   = date('Y-m-d', strtotime($dataSet['toDate'] . ' +1 day'));

        $dataList = [];
        $purchaseOrders = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrderWiseProducts($poIds, $fromDate, $toDate);
        //get grn wise return qty
        $grnReturnDetails = $this->CommonTable('Inventory\Model\PurchaseReturnProductTable')->getGrnWiseProductReturnQuantityDetails();

        foreach ($purchaseOrders as $purchaseOrder)
        {
            $grnData = [];
            $grns = $this->CommonTable('Inventory\Model\GrnProductTable')->getCopiedGrnItemsByPoId($purchaseOrder['purchaseOrderID']);
            foreach ($grns as $grn)
            {
                $date1 = new \DateTime($purchaseOrder['deliveryDate']);
                $date2 = new \DateTime($grn['grnDate']);
                $interval = $date1->diff($date2);
                $grnReturnQty = array_key_exists($grn['grnID'], $grnReturnDetails) ? $grnReturnDetails[$grn['grnID']]: 0;

                $grnData[] = [
                    'grnCode' => $grn['grnCode'],
                    'grnDate' => $grn['grnDate'],
                    'grnDelay' => $interval->format('%R%a days'),
                    'grnCompletion' => ($grn['copiedQty'] - $grnReturnQty) / $purchaseOrder['poTotalQty'] * 100
                ];
            }
            $dataList[$purchaseOrder['purchaseOrderID']] = [
                'poCode' => $purchaseOrder['purchaseOrderCode'],
                'poDate' => $purchaseOrder['poDate'],
                'poDeliveryDate' => $purchaseOrder['deliveryDate'],
                'poTotalQty' => $purchaseOrder['poTotalQty'],
                'grnDetails' => $grnData
            ];
        }

        $orgData = [];
        foreach ($dataList as $key => $value) {
            if (!empty($value['grnDetails'])) {
               array_push($orgData, $value); 
            }
        }
        return $orgData;
    }

}
