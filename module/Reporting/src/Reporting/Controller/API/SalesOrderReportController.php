<?php

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class SalesOrderReportController extends CoreController {

    public function generateSalesOrderWiseInvoiceReportAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getSalesOrderWiseInvoiceData($postData);
            $headerView = $this->headerViewTemplate("Sales Order Wise Invoice Details Report", $postData['fromDate'] . '-' . $postData['toDate']);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);
            $cusCategory = ($postData['cusCategories']) ? 1 : 0;

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $dataList,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
                'cusCategory' => $cusCategory,
            ));
            $view->setTemplate('reporting/sales-order-report/sales-order-wise-invoice-report');

            $this->status = true;
            $this->msg = 'Sales Order wise invoice details report generated';
            $this->html = $view;

            return $this->JSONRespondHtml();
        }
    }

    public function generateSalesOrderWiseInvoicePdfAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getSalesOrderWiseInvoiceData($postData);
            $headerView = $this->headerViewTemplate("Sales Order Wise Invoice Details Report", $postData['fromDate'] . '-' . $postData['toDate']);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);
            $cusCategory = ($postData['cusCategories']) ? 1 : 0;

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $dataList,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
                'cusCategory' => $cusCategory,
            ));
            $view->setTemplate('reporting/sales-order-report/sales-order-wise-invoice-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('sales-order-wise-invoice-details-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            $this->msg = 'Sales Order wise invoice details pdf generated';

            return $this->JSONRespond();
        }
    }

    public function generateSalesOrderWiseInvoiceCsvAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getSalesOrderWiseInvoiceData($postData);
            $cD = $this->getCompanyDetails();
            $cusCategory = ($postData['cusCategories']) ? 1 : 0;

            if (!empty($dataList)) {
                $title = '';
                $in = [
                    "",
                    "SALES ORDER WISE SALES INVOICE REPORT"
                ];
                $title = implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                ];
                $title.=implode(",", $in) . "\n";

                $in = [
                    0 => "Period : " . $postData['fromDate'] . '-' . $postData['toDate']
                ];
                $title.=implode(",", $in) . "\n";

                $arrs = [
                    'SALES PERSON',
                    'SALES ORDER CODE',
                    'SALES ORDER DATE',
                    'CUSTOMER/CUSTOMER CATEGORY',
                    'SALES ORDER AMOUNT',
                    'INVOICE CODE',
                    'INVOICE DATE',
                    'INVOICE AMOUNT'
                ];
                $header = implode(",", $arrs);
                $arr = '';

                $gSoAmount = $gSoInvoiceAmount = 0;
                $isDataExist = false;
                foreach ($dataList as $salesPerson => $data) {
                    if (!empty($data)) {
                        $isDataExist = true;
                        $arrs = [
                            $salesPerson
                        ];
                        $arr.=implode(",", $arrs) . "\n";

                        foreach ($data as $salesOrder) {
                            if($cusCategory){
                                $arrs = [
                                    '',
                                    $salesOrder['code'],
                                    $salesOrder['date'],
                                    $salesOrder['customer']."/".$salesOrder['cusCategory'],
                                    number_format($salesOrder['amount'], 2, '.', '')
                                ];
                            } else {
                                $arrs = [
                                    '',
                                    $salesOrder['code'],
                                    $salesOrder['date'],
                                    $salesOrder['customer'],
                                    number_format($salesOrder['amount'], 2, '.', '')
                                ];
                            }
                            $arr.=implode(",", $arrs) . "\n";
                            $soInvoiceAmount = 0;
                            foreach ($salesOrder['invoiceData'] as $invoiceData) {
                                $soInvoiceAmount += $invoiceData['invoiceAmount'];
                                $arrs = [
                                    '',
                                    '',
                                    '',
                                    '',
                                    '',
                                    $invoiceData['invoiceCode'],
                                    $invoiceData['invoiceDate'],
                                    number_format($invoiceData['invoiceAmount'], 2, '.', '')
                                ];
                                $arr.=implode(",", $arrs) . "\n";
                            }
                            $gSoInvoiceAmount += $soInvoiceAmount;
                            $gSoAmount += $salesOrder['amount'];
                            $arrs = [
                                '',
                                '',
                                '',
                                '',
                                '',
                                '',
                                'Sales Order Invoice Total',
                                number_format($soInvoiceAmount, 2, '.', '')
                            ];
                            $arr.=implode(",", $arrs) . "\n";
                        }
                    }
                }
                if ($isDataExist) {
                    $arrs = [
                        '',
                        '',
                        '',
                        'Sales Order Grand Total',
                        number_format($gSoAmount, 2, '.', ''),
                        '',
                        'Sales Order Invoice Grand Total',
                        number_format($gSoInvoiceAmount, 2, '.', '')
                    ];
                    $arr.=implode(",", $arrs) . "\n";
                } else {
                    $arr = 'no matching records founds';
                }
            } else {
                $arr = 'no matching records founds';
            }

            $name = "sales_order_wise_invoice_detail_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getSalesOrderWiseInvoiceData($data) {
        $isAllCustomers = filter_var($data['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
        $isAllRepresentative = filter_var($data['isAllRepresentative'], FILTER_VALIDATE_BOOLEAN);
        $representativeIds = empty($data['salesPersonIds']) ? [] : $data['salesPersonIds'];

        if ($isAllRepresentative) {
            $salesPersons = $this->CommonTable("User/Model/SalesPersonTable")->fetchAll(false);
            $representativeIds = array_map(function($salesPerson) {
                return $salesPerson['salesPersonID'];
            }, iterator_to_array($salesPersons));
            array_push($representativeIds, '0');
        }

        $dataArr = [];
        foreach ($representativeIds as $representativeId) {
            if($representativeId != '0'){
                $salesPerson = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonDetailsBySalesPersonId($representativeId);
                $salesPersonName = $salesPerson['salesPersonFirstName'] . ' ' . $salesPerson['salesPersonLastName'] . '(' . $salesPerson['salesPersonSortName'] . ')';
            } else {
                $salesPersonName = 'Other';
            }            

            $soData = [];
            $soDetails = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesPersonWiseSalesOrderDetails($representativeId, $data['toDate'], $data['fromDate'], $data['locationIds'], $data['customerIds'], $isAllCustomers, $data['cusCategories']);
            foreach ($soDetails as $so) {
                $invoiceData = [];
                $invoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicesBySalesOrderId($so['soID']);
                foreach ($invoices as $invoice) {
                    $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getInvoiceCreditNoteTotalByInvoiceId($invoice['salesInvoiceID']);
                    $invoiceData[] = [
                        'invoiceCode' => $invoice['salesInvoiceCode'],
                        'invoiceDate' => $invoice['salesInvoiceIssuedDate'],
                        'invoiceAmount' => $invoice['salesinvoiceTotalAmount'] - $creditNotes['total']
                    ];
                }
                $soArr = [
                    'code' => $so['soCode'],
                    'date' => $so['issuedDate'],
                    'customer' => $so['customerName'] . ' (' . $so['customerCode'] . ')',
                    'amount' => $so['totalAmount'],
                    'invoiceData' => $invoiceData,
                    'cusCategory' => $so['customerCategoryName'],
                ];
                array_push($soData, $soArr);
            }
            $dataArr[$salesPersonName] = $soData;
        }
        return $dataArr;
    }


    public function generateSalesOrderWiseDeliveryNoteReportAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $dataSet = $request->getPost();
        $translator = new Translator();
        $name = $translator->translate('Sales Order Wise Delivery Note Details Report');

        $dataList = $this->getSalesOrderWiseDeliveryNoteData($dataSet);
        $companyDetails = $this->getCompanyDetails();

        $headerView = $this->headerViewTemplate($name, $period = NULL);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $view = new ViewModel(array(
            'dataList' => $dataList,
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
        ));
        $view->setTemplate('reporting/sales-order-report/sales-order-wise-delivery-note-report');
        $this->html = $view;
        $this->status = true;

        return $this->JSONRespondHtml();
    }

     public function generateSalesOrderWiseDeliveryNotePdfAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $dataSet = $request->getPost();
        $translator = new Translator();
        $name = $translator->translate('Sales Order Wise Delivery Note Details Report');

        $dataList = $this->getSalesOrderWiseDeliveryNoteData($dataSet);
        $companyDetails = $this->getCompanyDetails();

        $headerView = $this->headerViewTemplate($name, $period = NULL);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $view = new ViewModel(array(
            'dataList' => $dataList,
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
        ));
        $view->setTemplate('reporting/sales-order-report/sales-order-wise-delivery-note-report');
        $view->setTerminal(true);

        $htmlContent = $this->viewRendererHtmlContent($view);
        $pdfPath = $this->downloadPDF('generate-so-wise-delivery-note-report', $htmlContent, true);

        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function generateSalesOrderWiseDeliveryNoteCsvAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }
        $dataSet = $request->getPost();
        $translator = new Translator();
        $name = $translator->translate('Sales Order Wise Delivery Note Details Report');

        $dataList = $this->getSalesOrderWiseDeliveryNoteData($dataSet);
        $cD = $this->getCompanyDetails();

        if ($dataList) {
            $title = '';
            $tit = 'SALES ORDER WISE DELIVERY NOTE REPORT';
            $in = ["", "", $tit];
            $title.=implode(",", $in) . "\n";

            $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
            $title.=implode(",", $in) . "\n";

            $arrs = ["SO CODE", "SO DATE", "DELIVERY DATE", "DELIVEY NOTE CODE", "DELIVEY NOTE DATE", "DELIVEY NOTE DELAY", "COMPLETION"];
            $header = implode(",", $arrs);
            $arr = '';

            foreach ($dataList as $so) {
                $tottalCompletion = 0;

                $in = [
                    $so['soCode'],
                    $so['soDate'],
                    $so['soDeliveryDate'],
                ];
                $arr.= implode(",", $in) . "\n";

                foreach ($so['deliveryNoteDetails'] as $deliveryNote) {
                    $tottalCompletion += $deliveryNote['deliveryNoteCompletion'];

                    $in = [
                        '',
                        '',
                        '',
                        $deliveryNote['deliveryNoteCode'],
                        $deliveryNote['deliveryNoteDate'],
                        $deliveryNote['deliveryNoteDelay'],
                        number_format($deliveryNote['deliveryNoteCompletion'], 2, '.', '') .' %',
                    ];
                    $arr.= implode(",", $in) . "\n";
                }

                $in = [
                    '',
                    '',
                    '',
                    '',
                    '',
                    'TOTAL :',
                    number_format($tottalCompletion, 2, '.', '') . ' %',
                ];
                $arr.= implode(",", $in) . "\n";
            }
        } else {
            $arr = "no matching records found\n";
        }
        $name = "generate-so-wise-delivery-note-report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    private function getSalesOrderWiseDeliveryNoteData($dataSet)
    {
        $isAllSalesOrders  = filter_var($dataSet['isAllSalesOrders'], FILTER_VALIDATE_BOOLEAN);
        $soIds    = ($isAllSalesOrders) ? [] : $dataSet['salesOrderIds'];
        $fromDate = $dataSet['fromDate'];
        $toDate   = $dataSet['toDate'];

        $dataList = [];
        $salesOrders = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderWiseProducts($soIds, $fromDate, $toDate);

        foreach ($salesOrders as $salesOrder)
        {
            $deliveryNoteData = [];
            $deliveryNotes = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getCopiedDeliveryNoteItemsBySoId($salesOrder['soID']);
            foreach ($deliveryNotes as $deliveryNote)
            {
                $date1 = new \DateTime($salesOrder['issuedDate']);
                $date2 = new \DateTime($deliveryNote['deliveryNoteDeliveryDate']);
                $interval = $date1->diff($date2);

                $deliveryNoteData[] = [
                    'deliveryNoteCode' => $deliveryNote['deliveryNoteCode'],
                    'deliveryNoteDate' => $deliveryNote['deliveryNoteDeliveryDate'],
                    'deliveryNoteDelay' => $interval->format('%R%a days'),
                    'deliveryNoteCompletion' => $deliveryNote['copiedQty'] / $salesOrder['soTotalQty'] * 100
                ];
            }

            $dataList[$salesOrder['soID']] = [
                'soCode' => $salesOrder['soCode'],
                'soDate' => $salesOrder['issuedDate'],
                'soDeliveryDate' => $salesOrder['expireDate'],
                'soTotalQty' => $salesOrder['soTotalQty'],
                'deliveryNoteDetails' => $deliveryNoteData
            ];
        }
        return $dataList;
    }

    /*
    *  This fuction is used to generate sales order due date report
    *  @param json request
    *   return json respond
    */ 
    public function generateSalesOrderDueDateReportAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getSalesOrderDueDateData($postData);
            $headerView = $this->headerViewTemplate("Sales Order Due Date Report", $postData['fromDate'] . '-' . $postData['toDate']);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $dataList,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/sales-order-report/sales-order-due-date-report');

            $this->status = true;
            $this->msg = 'Sales Order due date report generated';
            $this->html = $view;

            return $this->JSONRespondHtml();
        }
    }

    public function generateSalesOrderDueDatePdfAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getSalesOrderDueDateData($postData);
            $headerView = $this->headerViewTemplate("Sales Order Due Date Report", $postData['fromDate'] . '-' . $postData['toDate']);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $this->getCompanyDetails(),
                'dataList' => $dataList,
                'fromDate' => $postData['fromDate'],
                'toDate' => $postData['toDate'],
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/sales-order-report/sales-order-due-date-report');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('sales-order-due-date-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;
            $this->msg = 'Sales Order due date report pdf generated';

            return $this->JSONRespond();
        }
    }

    public function generateSalesOrderDueDateCsvAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $postData = $request->getPost()->toArray();
            $dataList = $this->getSalesOrderDueDateData($postData);
            $cD = $this->getCompanyDetails();

            if (!empty($dataList)) {
                $title = '';
                $in = [
                    "",
                    "SALES ORDER DUE DATE REPORT"
                ];
                $title = implode(",", $in) . "\n";

                $in = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                ];
                $title.=implode(",", $in) . "\n";

                $in = [
                    0 => "Period : " . $postData['fromDate'] . '-' . $postData['toDate']
                ];
                $title.=implode(",", $in) . "\n";

                $arrs = [
                    'SALES ORDER CODE',
                    'SALES ORDER ISSUE DATE',
                    'SALES ORDER DUE DATE',
                    'SALES ORDER TOTAL AMOUNT',
                    'SALES ORDER STATUS',
                    'INVOICE TOTAL AMOUNT',
                    'INVOICE TOTAL PAID AMOUNT'
                ];
                $header = implode(",", $arrs);
                $arr = '';

                foreach ($dataList as $key => $data) {
                    $arrs = [
                            $data['soCode'],
                            $data['issuedDate'],
                            $data['expireDate'],
                            number_format($data['totalAmount'],2, '.', ''),
                            $data['stName'],
                            number_format($data['salesInvoiceTotalAmount'],2, '.', ''),
                            number_format($data['salesInvoiceTotalPaidAmount'], 2, '.', '')
                        ];
                        $arr.=implode(",", $arrs) . "\n";   
                }
            } else {
                $arr = 'no matching records founds';
            }

            $name = "sales_order_due_date_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getSalesOrderDueDateData($data) {
        $fromDate = $data['fromDate'];
        $toDate   = $data['toDate'];
        $status   = $data['status'];

        $dataList = [];
        $salesOrders = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderDataByDueDateRange($fromDate, $toDate,$status);

        $finalData = [];
        foreach ($salesOrders as $key => $value) {
            $finalData[] = $value;
        }
        
        return $finalData;
    }
}
