<?php

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class LoyaltyCardReportController extends CoreController
{
    public function loyaltyCardHolderViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost())
        {
            $postData = $request->getPost()->toArray();            
            $companyDetails = $this->getCompanyDetails();
            $loyalityCardDetails = $this->getLoyalCardDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Loyalty Card Holders Report');

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'cardDetails' => $loyalityCardDetails,
                'headerTemplate' => $headerViewRender,
            ));
            $view->setTemplate('reporting/loyalty-card-report/loyalty-card-holder');

            $this->html = $view;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function loyaltyCardHolderPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost())
        {
            $postData = $request->getPost()->toArray();
            $companyDetails = $this->getCompanyDetails();
            $loyalityCardDetails = $this->getLoyalCardDetails($postData);

            $translator = new Translator();
            $name = $translator->translate('Loyalty Card Holders Report');

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $view = new ViewModel(array(
                'cD' => $companyDetails,
                'cardDetails' => $loyalityCardDetails,
                'headerTemplate' => $headerViewRender,
            ));

            $view->setTemplate('reporting/loyalty-card-report/loyalty-card-holder');
            $view->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($view);
            $pdfPath = $this->downloadPDF('loyalty-card-holders', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function loyaltyCardHolderCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost())
        {
            $postData = $request->getPost()->toArray();
            $cD = $this->getCompanyDetails();
            $loyalityCardDetails = $this->getLoyalCardDetails($postData);

            if ($loyalityCardDetails) {
                $title = '';
                $tit = 'LOYALTY CARD HOLDERS REPORT';
                $in = ["", $tit];
                $title.=implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";
                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $arrs = ["CUSTOMER NAME", "CUSTOMER CODE", "CUSTOMER PHONE NUMBER", "LOYALTY CARD TYPE", "LOYALTY CARD NUMBER", "AVAILABLE POINTS"];
                $header = implode(",", $arrs);
                $arr = '';
                foreach ($loyalityCardDetails as $value) {
                    
                }
            } else {
                $arr = "no matching records found\n";
            }

            $name = "loyalty_card_holders_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getLoyalCardDetails($postData)
    {
        $dataList = [];

        $isAllCustomers   = filter_var($postData['isAllCustomers'], FILTER_VALIDATE_BOOLEAN);
        $isAllCards       = filter_var($postData['isAllCards'], FILTER_VALIDATE_BOOLEAN);
        $isAllCardNumbers = filter_var($postData['isAllCardNumbers'], FILTER_VALIDATE_BOOLEAN);
        $customerIds      = ($isAllCustomers) ? [] : $postData['customerIds'];
        $cardIds          = ($isAllCards) ? [] : $postData['cardIds'];
        $cardNumbers    = ($isAllCardNumbers) ? [] : $postData['cardNumbers'];

        $cardDetails = $this->CommonTable('LoyaltyTable')->getLoyaltyCardHoldersDetails($customerIds, $cardIds, $cardNumbers);
        foreach ($cardDetails as $cL) {
            $dataList[] = $cL;
        }

        return $dataList;
    }

}
