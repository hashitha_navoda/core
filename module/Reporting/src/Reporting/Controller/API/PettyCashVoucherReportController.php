<?php

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

/**
 * Description of pettyCashVoucherReportController
 *
 * @author shermilan
 */
class PettyCashVoucherReportController extends CoreController
{

    /**
     * get petty cash voucher for diplay the report
     */
    public function viewReportAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $pVoucherType = $request->getPost('pvoucherType');
            $pExpenseType = $request->getPost('pExpenseType');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $this->html = $this->getPettyCashVoucherData($pVoucherType, $pExpenseType, $fromDate, $toDate);
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generatePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pVoucherType = $request->getPost('pvoucherType');
            $pExpenseType = $request->getPost('pExpenseType');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $pettyCashVoucherView = $this->getPettyCashVoucherData($pVoucherType, $pExpenseType, $fromDate, $toDate);
            $pettyCashVoucherView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($pettyCashVoucherView);
            $pdfPath = $this->downloadPDF('petty-cash-voucher-report' . md5($this->getGMTDateTime()), $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = TRUE;
            return $this->JSONRespond();
        }
    }

    public function generateCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pVoucherType = $request->getPost('pvoucherType');
            $pExpenseType = $request->getPost('pExpenseType');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $result = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getDataForReport($locationID, $pVoucherType, $pExpenseType, $fromDate, $toDate);
            $cD = $this->getCompanyDetails();

//          get petty cash types for drop down
            $pettyCashVoucherTypes = $this->getServiceLocator()->get('config')['pettyCashVoucherTypes'];

            $i = 0;
            $pettyCashVoucherData = [];
            $grandTotal = 0;
            if (count($result) > 0) {
                foreach ($result as $row) {
                    $pettyCashVoucherData[$i]['pettyCashVoucherNo'] = $row['pettyCashVoucherNo'];
                    $pettyCashVoucherData[$i]['pettyCashVoucherAmount'] = $row['pettyCashVoucherAmount'];
                    $pettyCashVoucherData[$i]['pettyCashVoucherDate'] = ($row['pettyCashVoucherDate']) ? $row['pettyCashVoucherDate'] : '-';
                    $pettyCashVoucherData[$i]['pettyCashExpenseTypeName'] = ($row['pettyCashExpenseTypeName']) ? $row['pettyCashExpenseTypeName'] : '-';
                    $pettyCashVoucherData[$i]['userUsername'] = ($row['userUsername']) ? $row['userUsername'] : '-';
                    $pettyCashVoucherData[$i]['userFirstName'] = ($row['userFirstName']) ? $row['userFirstName'] : '-';
                    $pettyCashVoucherData[$i]['userLastName'] = ($row['userLastName']) ? $row['userLastName'] : '-';
                     $pettyCashVoucherData[$i]['pettyCashComment'] = ($row['pettyCashVoucherComment']) ? $row['pettyCashVoucherComment'] : '-';
                    $pettyCashVoucherData[$i]['pettyCashVoucherType'] = $pettyCashVoucherTypes[$row['pettyCashVoucherType']];
                    $grandTotal += $row['pettyCashVoucherAmount'];
                    $i++;
                }
            }
            if ($pettyCashVoucherData) {
                $header = '';
                $reportTitle = [ "", 'PETTY CASH VOUCHER REPORT', "",];
                $header = implode(',', $reportTitle) . "\n";

                $dateTime = [ 'Report Generated: ' . date('Y-M-d h:i:s a')];
                $header.=implode(",", $dateTime) . "\n";

                $header .= $cD[0]->companyName . "\n";
                $header .= $cD[0]->companyAddress . "\n";
                $header .= 'Tel: ' . $cD[0]->telephoneNumber . "\n";

                $peroidRow = ["Period : " . $fromDate . '-' . $toDate];
                $header .= implode(",", $peroidRow) . "\n";
                $header .= implode(",", [""]) . "\n";

                $columnTitlesArray = ['Petty Cash Voucher No',
                    'Petty Cash Voucher Type',
                    'Petty Cash Voucher Date',
                    'Petty Cash Comment',
                    'Petty Cash Expense Type',
                    'Petty Cash User Name',
                    'Petty Cash Voucher Amount'];
                $columnTitles = implode(",", $columnTitlesArray);

                $csvRow = '';
                foreach ($pettyCashVoucherData as $pettyCashVoucher) {
                    $tableRow = [
                        $pettyCashVoucher['pettyCashVoucherNo'],
                        $pettyCashVoucher['pettyCashVoucherType'],
                        $pettyCashVoucher['pettyCashVoucherDate'],
                        $pettyCashVoucher['pettyCashComment'],
                        $pettyCashVoucher['pettyCashExpenseTypeName'],
                        $pettyCashVoucher['userUsername'],
                        $pettyCashVoucher['pettyCashVoucherAmount'],
                    ];
                    $csvRow.=implode(",", $tableRow) . "\n";
                }
                $grandTotalRow = ['', '', '', '', '', 'Grand Total:', $grandTotal];
                $csvRow .= implode(',', $grandTotalRow) . "\n";
            } else {
                $csvRow = "No matching records found \n";
            }

            $name = 'Petty_Cash_Voucher_Report_csv' . md5($this->getGMTDateTime());
            $csvContent = $this->csvContent($header, $columnTitles, $csvRow);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function getPettyCashVoucherData($pVoucherType = FALSE, $pExpenseType = false, $fromDate = false, $toDate = false)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $translator = new Translator();
        $name = $translator->translate('Petty Cash Voucher Report');
        $period = $fromDate . ' - ' . $toDate;

        $result = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getDataForReport($locationID, $pVoucherType, $pExpenseType, $fromDate, $toDate);
        $companyDetails = $this->getCompanyDetails();
//          get petty cash types for drop down
        $pettyCashVoucherTypes = $this->getServiceLocator()->get('config')['pettyCashVoucherTypes'];
        $i = 0;
        $pettyCashVoucherData = [];
        $grandTotal = 0;
        if (count($result) > 0) {
            foreach ($result as $row) {
                
                $pettyCashVoucherData[$i]['pettyCashVoucherNo'] = $row['pettyCashVoucherNo'];
                $pettyCashVoucherData[$i]['pettyCashVoucherAmount'] = number_format($row['pettyCashVoucherAmount'], 2);
                $pettyCashVoucherData[$i]['pettyCashVoucherDate'] = ($row['pettyCashVoucherDate']) ? $row['pettyCashVoucherDate'] : '-';
                $pettyCashVoucherData[$i]['pettyCashExpenseTypeName'] = ($row['pettyCashExpenseTypeName']) ? $row['pettyCashExpenseTypeName'] : '-';
                $pettyCashVoucherData[$i]['userUsername'] = ($row['userUsername']) ? $row['userUsername'] : '-';
                $pettyCashVoucherData[$i]['userFirstName'] = ($row['userFirstName']) ? $row['userFirstName'] : '-';
                $pettyCashVoucherData[$i]['userLastName'] = ($row['userLastName']) ? $row['userLastName'] : '-';
                $pettyCashVoucherData[$i]['pettyCashComment'] = ($row['pettyCashVoucherComment']) ? $row['pettyCashVoucherComment'] : '-';
                $pettyCashVoucherData[$i]['pettyCashVoucherType'] = $pettyCashVoucherTypes[$row['pettyCashVoucherType']];
                $grandTotal += $row['pettyCashVoucherAmount'];
                $i++;
            }
        }
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $pettyCashVoucherView = new ViewModel(array(
            'cD' => $companyDetails,
            'pettyCashVoucherData' => $pettyCashVoucherData,
            'fromDate' => $fromDate,
            'toDate' => $toDate,
            'currencySymbol' => $this->companyCurrencySymbol,
            'grandTotal' => number_format($grandTotal, 2),
            'headerTemplate' => $headerViewRender,
        ));
        return $pettyCashVoucherView->setTemplate('reporting/petty-cash-voucher-report/petty-cash-voucher-table');
    }

}
