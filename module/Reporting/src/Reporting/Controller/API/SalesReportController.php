<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains ReportApi PDF related controller functions
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;
use Core\BackgroundJobs\ReportJob;

class SalesReportController extends CoreController
{

    protected $userID;

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function viewMonthlySalesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $locationID = $request->getPost('locationID');
            $customerCategories = $request->getPost('cusCategories');
            $salesPersonIds = $request->getPost('salesPersonIds');
            
            $translator = new Translator();
            $name = $translator->translate('Monthly Sales Report');
            $period = $fromMonth . ' - ' . $toMonth;
            
            if (empty($salesPersonIds)) {
                $mothlySalesDetails = $this->
                    getMonthlySalesDetails($fromMonth, $toMonth, $locationID, $customerCategories);

            } else {
                $mothlySalesDetails = $this->
                        getMonthlySalesDetailsBySalesPerson($fromMonth, $toMonth, $locationID, $customerCategories, $salesPersonIds);
            }
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');

            $headerViewRender = $this->htmlRender($headerView);

            $monthlySalesView = new ViewModel(array(
                'monthlySalesData' => $mothlySalesDetails,
                'checkTaxStatus' => $checkTaxStatus,
                'fromMonth' => $fromMonth,
                'toMonth' => $toMonth,
                'headerTemplate' => $headerViewRender,
                'cD' => $companyDetails));
            if (empty($salesPersonIds)) {
                $monthlySalesView->setTemplate('reporting/sales-report/generate-monthly-sales-pdf');
            } else {
                $monthlySalesView->setTemplate('reporting/sales-report/generate-monthly-sales-by-sales-person-pdf');
            }
            $this->html = $monthlySalesView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateMonthlySalesPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromMonth = $request['fromMonth'];
            $toMonth = $request['toMonth'];
            $checkTaxStatus = $request['checkTaxStatus'];
            $locationID = ($request['locationID']) ? $request['locationID'] : null;
            $cusCategories = ($request['cusCategories']) ? $request['cusCategories'] : null;
            $salesPersonIds = $request['salesPersonIds'];
            $translator = new Translator();
            $name = $translator->translate('Monthly Sales Report');
            $period = $fromMonth . ' - ' . $toMonth;

            if (empty($salesPersonIds)) {
                $monthlySalesDetails = $this->getMonthlySalesDetails($fromMonth, $toMonth, $locationID, $cusCategories);
                
            } else {
                $monthlySalesDetails = $this->getMonthlySalesDetailsBySalesPerson($fromMonth, $toMonth, $locationID, $cusCategories, $salesPersonIds);
            }
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewMonthlySales = new ViewModel(array(
                'cD' => $cD,
                'monthlySalesData' => $monthlySalesDetails,
                'fromMonth' => $fromMonth,
                'toMonth' => $toMonth,
                'headerTemplate' => $headerViewRender,
                'checkTaxStatus' => $checkTaxStatus
            ));

            if (empty($salesPersonIds)) {
                $viewMonthlySales->setTemplate('reporting/sales-report/generate-monthly-sales-pdf');
            } else {
                $viewMonthlySales->setTemplate('reporting/sales-report/generate-monthly-sales-by-sales-person-pdf');
            }
            $viewMonthlySales->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewMonthlySales);
            $this->downloadPDF('monthly-sales-report', $htmlContent);
            exit;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return csv Monthly sales
     */
    public function generateMonthlySalesSheetAction()
    {        
        $request = $this->getRequest();        
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $checkTaxStatus = filter_var($request->getPost('checkTaxStatus'), FILTER_VALIDATE_BOOLEAN);
            $locationID = $request->getPost('locationID') ? $request->getPost('locationID') : null;
            $cusCategories = $request->getPost('cusCategories') ? $request->getPost('cusCategories') : null;
            $salesPersonIds = $request->getPost('salesPersonIds');
            
            if (empty($salesPersonIds)) {
                $monthlySalesDetails = $this->getMonthlySalesDetails($fromMonth, $toMonth, $locationID, $cusCategories);
            } else {
                $monthlySalesDetails = $this->getMonthlySalesDetailsBySalesPerson($fromMonth, $toMonth, $locationID, $cusCategories, $salesPersonIds);
            }
            $cD = $this->getCompanyDetails();
            
            if ($monthlySalesDetails) {  
                if (empty($salesPersonIds)) {
                    $title = '';
                    $tit = 'MONTHLY SALES REPORT';
                    $in = ["", $tit];
                    $title = implode(",", $in) . "\n";

                    $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];

                    $title.=implode(",", $in) . "\n";

                    $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                    $title.=implode(",", $in) . "\n";

                    $in = ["Period : " . $fromMonth . '-' . $toMonth];
                    $title.=implode(",", $in) . "\n";

                    if ($checkTaxStatus == 'true') {
                        $arrs = ["YEAR", "MONTH", "TOTAL CREDIT NOTE","NORMAL TAX","COMPOUND TAX","SUSPENDED TAX", "TOTAL AMOUNT"];
                        $header = implode(",", $arrs);
                        $arr = '';
                        $netTax = 0.0;
                        $netSuspendedTax = 0.0;
                        $netAmount = 0.0;
                        $netCreditNote = 0.0;

                        foreach ($monthlySalesDetails as $key => $value) {
                            $subAmount = 0.00;
                            $subNormalTax = 0.00;
                            $subCompoundTax = 0.00;
                            $subSuspendedTax = 0.00;
                            $subCreditNote = 0.00;
                            $in = [$key];
                            $arr.=implode(",", $in) . "\n";

                            foreach ($value as $key1 => $value1) {
                                $amount = $value1['TotalAmount'];
                                $amount = $amount ? $amount : 0.00;
                                
                                $creditNoteAmount = $value1['creditNoteProductTotal'];
                                $directCreditNoteAmount = $value1['directCreditNoteProductTotal'];
                                $buybackCreditNoteAmount = $value1['buybackCreditNoteProductTotal'];
                                $netCreditNote+= $creditNoteAmount;
                                $subCreditNote+= $creditNoteAmount;
                                
                                $grossTotal = ($amount - $creditNoteAmount);
                                $grossTotal = $grossTotal + $directCreditNoteAmount + $buybackCreditNoteAmount;

                                $subAmount += $grossTotal;
                                $netAmount += $grossTotal;

                                $normaltax = $value1['normalTax'] - $value1['cnNormalTax'];
                                $compoundTax = $value1['compoundTax'] - $value1['cnCompoundTax'];
                                $suspendedTax = $value1['suspendedTax'] - $value1['cnSuspendedTax'];
                                $normaltax = $normaltax ? $normaltax : 0.00;
                                $compoundTax = $compoundTax ? $compoundTax : 0.00;
                                $suspendedTax = $suspendedTax ? $suspendedTax : 0.00;
                                $subNormalTax += $normaltax;
                                $subCompoundTax += $compoundTax;
                                $subSuspendedTax += $suspendedTax;
                                $netTax += $normaltax;
                                $netTax += $compoundTax;
                                $netSuspendedTax += $suspendedTax;
                                $in = ["", $value1['Month'], $creditNoteAmount, $normaltax, $compoundTax, $suspendedTax, $grossTotal];
                                $arr.=implode(",", $in) . "\n";
                            }
                            $in = ["Total Sales $key", "", $subCreditNote, $subNormalTax, $subCompoundTax, $subSuspendedTax, $subAmount];
                            $arr.=implode(",", $in) . "\n";
                        }

                        $arr.=implode(",", []) . "\n";
                        $in = ["", "", "", "", "", "Total Sales", ($netAmount)];
                        $arr.=implode(",", $in) . "\n";
                        
                        $in = ["", "", "", "", "", "Total CreditNote", ($netCreditNote)];
                        $arr.=implode(",", $in) . "\n";

                        $in = ["", "", "", "", "", "Total Tax", ($netTax)];
                        $arr.=implode(",", $in) . "\n";

                        $in = ["", "", "", "", "", "Total Suspended Tax", ($netSuspendedTax)];
                        $arr.=implode(",", $in) . "\n";

                        $in = ["", "", "", "", "", "Sales Excluding Tax", ($netAmount - $netTax)];
                        $arr.=implode(",", $in) . "\n";
                    } else {
                        $arrs = ["YEAR", "MONTH", "TOTAL CREDIT NOTE", "TOTAL AMOUNT"];
                        $header = implode(",", $arrs);
                        $arr = '';

                        $netAmount = 0.0;
                        foreach ($monthlySalesDetails as $key => $value) {
                            $subAmount = 0.00;
                            $subCreditNote = 0.00;
                            
                            $in = [$key];
                            $arr.=implode(",", $in) . "\n";

                            foreach ($value as $key1 => $value1) {
                                $amount = $value1['TotalAmount'];
                                $amount = $amount ? $amount : 0.00;                            
                                $creditNoteAmount = $value1['creditNoteProductTotal'];
                                $directCreditNoteAmount = $value1['directCreditNoteProductTotal'];
                                $buybackCreditNoteAmount = $value1['buybackCreditNoteProductTotal'];
                                $netCreditNote+= $creditNoteAmount;
                                $subCreditNote+= $creditNoteAmount;
                                                  
                                $grossTotal = ($amount - $creditNoteAmount);
                                $grossTotal = $grossTotal + $directCreditNoteAmount + $buybackCreditNoteAmount;
                                $subAmount += $grossTotal;
                                $netAmount += $grossTotal;

                                $in = ["", $value1['Month'], $creditNoteAmount, $grossTotal];
                                $arr.=implode(",", $in) . "\n";
                            }
                            $in = ["Total Sales $key", " ", $subCreditNote, $subAmount];
                            $arr.=implode(",", $in) . "\n";
                        }
                        $in = ["Total Sales", " ", " ",$netAmount];
                        $arr.=implode(",", $in) . "\n";
                        
                        $in = ["Total CreditNote", "", "",($netCreditNote)];
                        $arr.=implode(",", $in) . "\n";
                    }
                } else {
                    $title = '';
                    $tit = 'MONTHLY SALES REPORT';
                    $in = ["", $tit];
                    $title = implode(",", $in) . "\n";

                    $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];

                    $title.=implode(",", $in) . "\n";

                    $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                    $title.=implode(",", $in) . "\n";

                    $in = ["Period : " . $fromMonth . '-' . $toMonth];
                    $title.=implode(",", $in) . "\n";

                    if ($checkTaxStatus == 'true') {
                        $arrs = ["SALES PERSON","YEAR", "MONTH", "TOTAL CREDIT NOTE","NORMAL TAX","COMPOUND TAX", "TOTAL AMOUNT"];
                        $header = implode(",", $arrs);
                        $arr = '';
                        $netTax = 0.0;
                        $netAmount = 0.0;
                        $netCreditNote = 0.0;

                        foreach ($monthlySalesDetails as $key => $value) {
                            $subAmount = 0.00;
                            $subNormalTax = 0.00;
                            $subCompoundTax = 0.00;
                            $subCreditNote = 0.00;
                            $in = [$key];
                            $arr.=implode(",", $in) . "\n";
                            foreach ($value as $key1 => $value1) {
                                $in = ["",$key1];
                                $arr.=implode(",", $in) . "\n";
                                foreach ($value1 as $key2 => $value2) {
                                    $amount = $value2['TotalAmount'];
                                    $amount = $amount ? $amount : 0.00;
                                    
                                    $creditNoteAmount = $value2['creditNoteProductTotal'] ? $value2['creditNoteProductTotal'] :0.00;
                                    $netCreditNote+= $creditNoteAmount;
                                    $subCreditNote+= $creditNoteAmount;
                                    
                                    $grossTotal = ($amount - $creditNoteAmount);
                                    $subAmount += $grossTotal;
                                    $netAmount += $grossTotal;

                                    $normaltax = $value2['normalTax'] - $value2['cnNormalTax'];
                                    $compoundTax = $value2['compoundTax'] - $value2['cnCompoundTax'];
                                    $normaltax = $normaltax ? $normaltax : 0.00;
                                    $compoundTax = $compoundTax ? $compoundTax : 0.00;
                                    $subNormalTax += $normaltax;
                                    $subCompoundTax += $compoundTax;
                                    $netTax += $normaltax;
                                    $netTax += $compoundTax;

                                    $in = ["","", $value2['Month'], $creditNoteAmount, $normaltax,$compoundTax, $grossTotal];
                                    $arr.=implode(",", $in) . "\n";
                                }
                            }
                            $in = ["Total Sales $key", "","", $subCreditNote, $subNormalTax,$subCompoundTax, $subAmount];
                            $arr.=implode(",", $in) . "\n";
                        }

                        $arr.=implode(",", []) . "\n";
                        $in = ["","","Total Sales", "", "", "",($netAmount)];
                        $arr.=implode(",", $in) . "\n";
                        
                        $in = ["","","Total CreditNote", "", "", "",($netCreditNote)];
                        $arr.=implode(",", $in) . "\n";

                        $in = ["","","Total Tax", "", "", "",($netTax)];
                        $arr.=implode(",", $in) . "\n";

                        $in = ["","","Sales Excluding Tax", "", "", "",($netAmount - $netTax)];
                        $arr.=implode(",", $in) . "\n";
                    } else {
                        $arrs = ["SALES PERSON","YEAR", "MONTH", "TOTAL CREDIT NOTE", "TOTAL AMOUNT"];
                        $header = implode(",", $arrs);
                        $arr = '';

                        $netAmount = 0.0;
                        foreach ($monthlySalesDetails as $key => $value) {
                            $subAmount = 0.00;
                            $subCreditNote = 0.00;
                            
                            $in = [$key];
                            $arr.=implode(",", $in) . "\n";
                            foreach ($value as $key1 => $value1) {
                                $in = ["",$key1];
                                $arr.=implode(",", $in) . "\n";
                                foreach ($value1 as $key2 => $value2) {
                                    $amount = $value2['TotalAmount'];
                                    $amount = $amount ? $amount : 0.00;                            
                                    $creditNoteAmount = $value2['creditNoteProductTotal'] ? $value2['creditNoteProductTotal'] : 0.00;
                                    $netCreditNote+= $creditNoteAmount;
                                    $subCreditNote+= $creditNoteAmount;
                                                                
                                    $grossTotal = ($amount - $creditNoteAmount);
                                    $subAmount += $grossTotal;
                                    $netAmount += $grossTotal;

                                    $in = ["","", $value2['Month'], $creditNoteAmount, $grossTotal];
                                    $arr.=implode(",", $in) . "\n";
                                }
                            }
                            $in = ["Total Sales $key", " ","", $subCreditNote, $subAmount];
                            $arr.=implode(",", $in) . "\n";
                        }
                        $in = ["Total Sales", " ","", " ",$netAmount];
                        $arr.=implode(",", $in) . "\n";
                        
                        $in = ["Total CreditNote", "","", "",($netCreditNote)];
                        $arr.=implode(",", $in) . "\n";
                    }
                }              
            } else {
                $arr = "no matching records found\n";
            }
            
            $name = "Monthly_Sales_Report.csv";            
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;            
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param Date $fromMonth
     * @param Date $toMonth
     * @return $monthlySalesDetails
     */
    public function getMonthlySalesDetails($fromMonth = NULL, $toMonth = NULL, $locationID = NULL, $customerCategory = NULL)
    {
        if (isset($fromMonth) && isset($toMonth)) {
            $respondAmount = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesData($fromMonth, $toMonth, $locationID, $customerCategory);
            $respondTax = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesTaxDetails($fromMonth, $toMonth, $locationID, $customerCategory);

            //get all suspended taxes
            $simpleTaxDetails = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
            $suspendedTaxes = [];
            foreach ($simpleTaxDetails as $key => $value) {
                if ( $value->taxSuspendable == 1 ) {
                    $suspendedTaxes[$value->id] = $value->id;
                }
            }

            $taxData = [];
            $tempArray = []; 
            foreach ($respondTax as $key => $value) {
                $tempArray[$value['MonthWithYear']]['Month'] = $value['Month'];   
                $tempArray[$value['MonthWithYear']]['MonthWithYear'] = $value['MonthWithYear'];   
                $tempArray[$value['MonthWithYear']]['Year'] = $value['Year'];   
                $tempArray[$value['MonthWithYear']]['salesInvoiceProductID'] = $value['salesInvoiceProductID'];
                if ($value['salesInvoiceSuspendedTax'] == '1' && array_key_exists($value['taxID'], $suspendedTaxes)) {
                    $tempArray[$value['MonthWithYear']]['suspendedTax'] += floatval($value['salesInvoiceProductTaxAmount']);
                } else {
                    if ($value['taxType'] == "v") {
                        $tempArray[$value['MonthWithYear']]['normalTax'] += floatval($value['salesInvoiceProductTaxAmount']);
                    } else if ($value['taxType'] == "c"){
                        $tempArray[$value['MonthWithYear']]['compoundTax'] += floatval($value['salesInvoiceProductTaxAmount']);
                    }
                }

                $tempArray[$value['MonthWithYear']]['deleted'] = $value['deleted'];   
            }

            foreach ($tempArray as $key => $value) {
                $taxData[] = $value;
            }

            //get credit note total values without tax
            $respondCreditNote = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesCreditNoteData($fromMonth, $toMonth, $locationID, $customerCategory);
           
            //get credit note tax details
            $respondCreditNoteTax = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesCreditNoteTaxData($fromMonth, $toMonth, $locationID, $customerCategory);

            //get direct credit note total values without tax
            $respondDirectCreditNote = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesDirectCreditNoteData($fromMonth, $toMonth, $locationID, $customerCategory);

            //get direct credit note tax details
            $respondDirectCreditNoteTax = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesDirectCreditNoteTaxData($fromMonth, $toMonth, $locationID, $customerCategory);


            //get buy back credit note total values without tax
            $respondBuyBackCreditNote = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesBuyBackCreditNoteData($fromMonth, $toMonth, $locationID, $customerCategory);
           
            //get buy back credit note tax details
            $respondBuyBackCreditNoteTax = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesBuyBackCreditNoteTaxData($fromMonth, $toMonth, $locationID, $customerCategory);

            $cnTaxData = [];
            $cnTempArray = [];
            foreach ($respondCreditNoteTax as $key => $value) {
                $cnTempArray[$value['MonthWithYear']]['Month'] = $value['Month'];   
                $cnTempArray[$value['MonthWithYear']]['MonthWithYear'] = $value['MonthWithYear'];   
                $cnTempArray[$value['MonthWithYear']]['Year'] = $value['Year'];   
                if ($value['salesInvoiceSuspendedTax'] == '1' && array_key_exists($value['taxID'], $suspendedTaxes)) {
                    $cnTempArray[$value['MonthWithYear']]['cnSuspendedTax'] += floatval($value['creditNoteTaxAmount']);
                } else {
                    if ($value['taxType'] == "v") {
                        $cnTempArray[$value['MonthWithYear']]['cnNormalTax'] += floatval($value['creditNoteTaxAmount']);
                    } else if ($value['taxType'] == "c"){
                        $cnTempArray[$value['MonthWithYear']]['cnCompoundTax'] += floatval($value['creditNoteTaxAmount']);
                    }
                }
                $cnTempArray[$value['MonthWithYear']]['deleted'] = $value['deleted'];   
            }

            foreach ($cnTempArray as $key => $value) {
                $cnTaxData[] = $value;
            }

            $dcnTaxData = [];
            $dcnTempArray = []; 
            foreach ($respondDirectCreditNoteTax as $key => $val) {
                $dcnTempArray[$val['MonthWithYear']]['Month'] = $val['Month'];   
                $dcnTempArray[$val['MonthWithYear']]['MonthWithYear'] = $val['MonthWithYear'];   
                $dcnTempArray[$val['MonthWithYear']]['Year'] = $val['Year'];   
                if ($value['taxType'] == "v") {
                    $dcnTempArray[$val['MonthWithYear']]['dcnNormalTax'] += floatval($val['creditNoteTaxAmount']);   
                } else if ($val['taxType'] == "c"){
                    $dcnTempArray[$val['MonthWithYear']]['dcnCompoundTax'] += floatval($val['creditNoteTaxAmount']);   
                }   
                $cnTempArray[$val['MonthWithYear']]['deleted'] = $val['deleted'];   
            }

            foreach ($dcnTempArray as $key => $val) {
                $dcnTaxData[] = $val;
            }



            $bbcnTaxData = [];
            $bbnTempArray = []; 
            foreach ($respondBuyBackCreditNoteTax as $key => $val) {
                $bbcnTempArray[$val['MonthWithYear']]['Month'] = $val['Month'];   
                $bbcnTempArray[$val['MonthWithYear']]['MonthWithYear'] = $val['MonthWithYear'];   
                $bbcnTempArray[$val['MonthWithYear']]['Year'] = $val['Year'];   
                if ($value['taxType'] == "v") {
                    $bbcnTempArray[$val['MonthWithYear']]['dcnNormalTax'] += floatval($val['creditNoteTaxAmount']);   
                } else if ($val['taxType'] == "c"){
                    $bbcnTempArray[$val['MonthWithYear']]['dcnCompoundTax'] += floatval($val['creditNoteTaxAmount']);   
                }   
                $bbTempArray[$val['MonthWithYear']]['deleted'] = $val['deleted'];   
            }

            foreach ($bbcnTempArray as $key => $val) {
                $bbcnTaxData[] = $val;
            }

            $respond = array_replace_recursive($respondAmount, $taxData, $respondCreditNote, $cnTaxData, $respondDirectCreditNote, $dcnTaxData, $respondBuyBackCreditNote, $bbcnTaxData);
            $respondYears = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesDataYears($fromMonth, $toMonth, $locationID, $customerCategory);            
            $months = $this->CommonTable('Invoice\Model\InvoiceTable')->getMonthsYearBetweenTwoDates($fromMonth, $toMonth);
            
            $monthlySalesDetails = array();
            $years = $respondYears;

            foreach ($years as $year) {
                $monthlySalesDetails[$year] = array();
            }

            foreach ($months as $month) {
                $temp = explode(' ', $month);
                $monthlySalesDetails[$temp[1]][$temp[0]] = array('Month' => $temp[0], 'MonthWithYear' => $month, 'Year' => $temp[1], 'TotalTax' => 0, 'TotalAmount' => 0);
            }

            foreach ($respond as $res) {
                $monthlySalesDetails[$res['Year']][$res['Month']] = $res;
            }
            return $monthlySalesDetails;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * Generatte JSON Object from given data
     * @return \Zend\View\Model\JsonModel
     */
    public function getMonthlySalesGraphDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $locationID = $request->getPost('locationID');
            $cusCategories = $request->getPost('cusCategories');

            $mothlySalesDetails = $this->getMonthlySalesDetails($fromMonth, $toMonth, $locationID, $cusCategories);
            $monthlySalaDataSet = array('mothlySalesDetails' => $mothlySalesDetails, 'cSymbol' => $this->companyCurrencySymbol);

            $this->data = $monthlySalaDataSet;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @param array $salesPersonIds
     * @return array $dailySalesData
     */
    public function _getDailySalesDetails($fromDate = NULL, $toDate = NULL, $salesPersonIds = NULL, $locationID = NULL, $cusCategories = NULL)
    {
        if (isset($fromDate) && isset($toDate)) {
            $normalTaxListArray = [];
            $compoundTaxListArray = [];
            $normalTaxString = "";
            $compoundTaxString = "";
            $withDefaultSalesPerson = (in_array('other', $salesPersonIds)) ? true : false;

            //get date range between which passed from date and to date
            $salesData = $this->CommonTable('Invoice\Model\InvoiceTable')->
                getDailySalesData($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategories, $withDefaultSalesPerson);
            $creditNoteData = $this->CommonTable('Invoice\Model\InvoiceTable')->getDailySalesCreditNoteData($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategories, $withDefaultSalesPerson);
            $salesDataTaxResult = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceRelatedTaxDetails($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategories, $withDefaultSalesPerson);
            
            $creditNoteTaxResult = $this->CommonTable('Invoice\Model\InvoiceTable')->getDailySalesCreditNoteTaxData($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategories, $withDefaultSalesPerson);

            //get all tax details
            $taxDetails = $this->CommonTable('Settings\Model\TaxCompoundTable')->fetchAll();
            $simpleTaxDetails = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
            $compoundTaxes = [];
            $suspendedTaxes = [];

            foreach ($taxDetails as $key => $taxValue) {

                $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;
            }

            // find vat type taxes and bnt type taxes
            $nbtTaxes = [];
            $vatTaxes = [];
            foreach ($simpleTaxDetails as $key => $value) {
                if ( $value->taxSuspendable == 1 ) {
                    $suspendedTaxes[$value->id] = $value->id;
                }
                if (strpos($value->taxName, "NBT") !== false) {
                    $nbtTaxes[$value->id] = $value->taxName;
                } else if (strpos($value->taxName, "VAT") !== false) {
                    $vatTaxes[$value->id] = $value->taxName;
                }
                
            }


            //add tax values to the related salesInvoiceID
            $salesInvTaxDetails = [];
            foreach ($salesDataTaxResult as $key => $salesTaxVal) {
                if ($salesTaxVal['salesInvoiceSuspendedTax'] == 1 && array_key_exists($salesTaxVal['taxID'], $suspendedTaxes)) {
                    $salesInvTaxDetails[$salesTaxVal['salesInvoiceID']]['suspendedTax'] += floatval($salesTaxVal['salesInvoiceProductTaxAmount']);
                } else {
                    if(array_key_exists($salesTaxVal['taxID'], $vatTaxes)){
                        $salesInvTaxDetails[$salesTaxVal['salesInvoiceID']]['compoundTax'] += floatval($salesTaxVal['salesInvoiceProductTaxAmount']);
                        // if (!array_key_exists($salesTaxVal['taxName'], $compoundTaxListArray)) {
                        //     $compoundTaxListArray[$salesTaxVal['taxName']] = $salesTaxVal['taxName'];
                        // }
                    } else if (array_key_exists($salesTaxVal['taxID'], $nbtTaxes)){
                        $salesInvTaxDetails[$salesTaxVal['salesInvoiceID']]['normalTax'] += floatval($salesTaxVal['salesInvoiceProductTaxAmount']);
                        // if (!array_key_exists($salesTaxVal['taxName'], $normalTaxListArray)) {
                        //     $normalTaxListArray[$salesTaxVal['taxName']] = $salesTaxVal['taxName'];
                        // }
                    }
                }
            }

            // foreach ($normalTaxListArray as $value) {
            //     $normalTaxString = ($normalTaxString == "") ? $value : $normalTaxString.", ".$value;
            // }

            // foreach ($compoundTaxListArray as $value) {
            //     $compoundTaxString = ($compoundTaxString == "") ? $value : $compoundTaxString.", ".$value;
            // }

            $normalTaxString = "NBT";
            $compoundTaxString = "VAT";
            
            $creditNoteTaxDetails = [];
            foreach ($creditNoteTaxResult as $key => $creditNoteTaxValue) {
                if ($creditNoteTaxValue['salesInvoiceSuspendedTax'] == 1 && array_key_exists($creditNoteTaxValue['taxID'], $suspendedTaxes)) {
                    $creditNoteTaxDetails[$creditNoteTaxValue['creditNoteID']]['suspendedTax'] += floatval($creditNoteTaxValue['creditNoteTaxAmount']);
                } else {
                    if(array_key_exists($creditNoteTaxValue['taxID'], $vatTaxes)){
                        $creditNoteTaxDetails[$creditNoteTaxValue['creditNoteID']]['compoundTax'] += floatval($creditNoteTaxValue['creditNoteTaxAmount']);
                    } else if (array_key_exists($creditNoteTaxValue['taxID'], $nbtTaxes)) {
                        $creditNoteTaxDetails[$creditNoteTaxValue['creditNoteID']]['normalTax'] += floatval($creditNoteTaxValue['creditNoteTaxAmount']);
                    }
                }
            }

            $salesDataFinal = [];
            foreach ($salesData as $val) {
                if ($salesPersonIds != NULL) {

                    $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($val['InvNo']);

                    if (sizeof($relatedSalesPersons) > 0) {
                        $numOfSP = (sizeof($relatedSalesPersons) == 0) ? 1 : sizeof($relatedSalesPersons);
                        foreach ($relatedSalesPersons as $key => $value) {

                            if (in_array($value['salesPersonID'], $salesPersonIds)) {
                                $singleDataArray = array(
                                    'Day' => $val['Day'],
                                    'InvID' => $val['InvID'],
                                    'Amount' => round(($val['Amount'] / $numOfSP), 2),
                                    'salesPersonID' => $value['salesPersonID'],
                                    'salesInvoiceID' => $val['salesInvoiceID'],
                                    'CusName' => $val['CusName'],
                                    'CusTitle' => $val['CusTitle'],
                                    'salesInvoiceProductID' => $val['salesInvoiceProductID'],
                                    'Tax' => round(($val['Tax'] / $numOfSP), 2),
                                    'deleted' => $val['deleted'],
                                    'salesPersonSortName' => $value['salesPersonSortName'],
                                    'salesPersonLastName' => $value['salesPersonLastName'],
                                    'normalTax' => round(($salesInvTaxDetails[$val['InvNo']]['normalTax'] / $numOfSP), 2),
                                    'compoundTax' => round(($salesInvTaxDetails[$val['InvNo']]['compoundTax'] / $numOfSP), 2),
                                    'suspendedTax' => round(($salesInvTaxDetails[$val['InvNo']]['suspendedTax'] / $numOfSP), 2),
                                    'customerCategoryName' => $val['customerCategoryName']  
                                );
                                $salesDataFinal[] = $singleDataArray;
                            }
                        }  
                    } else {
                        if (in_array('other', $salesPersonIds)) { 
                            $singleDataArray = array(
                                'Day' => $val['Day'],
                                'InvID' => $val['InvID'],
                                'Amount' => $val['Amount'],
                                'salesPersonID' => null,
                                'salesInvoiceID' => $val['salesInvoiceID'],
                                'CusName' => $val['CusName'],
                                'CusTitle' => $val['CusTitle'],
                                'salesInvoiceProductID' => $val['salesInvoiceProductID'],
                                'Tax' => $val['Tax'],
                                'deleted' => $val['deleted'],
                                'salesPersonSortName' => 'Other',
                                'salesPersonLastName' => $val['salesPersonLastName'],
                                'normalTax' => $salesInvTaxDetails[$val['InvNo']]['normalTax'],
                                'compoundTax' => $salesInvTaxDetails[$val['InvNo']]['compoundTax'],
                                'suspendedTax' => $salesInvTaxDetails[$val['InvNo']]['suspendedTax'],
                                'customerCategoryName' => $val['customerCategoryName']  
                            );
                            $salesDataFinal[] = $singleDataArray;
                        }
                    }
                 } else {
                    $singleDataArray = array(
                        'Day' => $val['Day'],
                        'InvID' => $val['InvID'],
                        'Amount' => $val['Amount'],
                        'salesPersonID' => $val['salesPersonID'],
                        'salesInvoiceID' => $val['salesInvoiceID'],
                        'CusName' => $val['CusName'],
                        'CusTitle' => $val['CusTitle'],
                        'salesInvoiceProductID' => $val['salesInvoiceProductID'],
                        'Tax' => $val['Tax'],
                        'deleted' => $val['deleted'],
                        'salesPersonSortName' => $val['salesPersonSortName'],
                        'salesPersonLastName' => $val['salesPersonLastName'],
                        'normalTax' => $salesInvTaxDetails[$val['InvNo']]['normalTax'],
                        'compoundTax' => $salesInvTaxDetails[$val['InvNo']]['compoundTax'],
                        'suspendedTax' => $salesInvTaxDetails[$val['InvNo']]['suspendedTax'],
                        'customerCategoryName' => $val['customerCategoryName']  
                    );
                    $salesDataFinal[] = $singleDataArray;
                 }

            }

            $creditNoteDataFinal = [];
            foreach ($creditNoteData as $val) {
                if ($salesPersonIds != NULL) {

                    $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($val['salesinvoiceID']);

                    if (sizeof($relatedSalesPersons) > 0) {
                        $numOfSP = (sizeof($relatedSalesPersons) == 0) ? 1 : sizeof($relatedSalesPersons);
                        foreach ($relatedSalesPersons as $key => $value) {

                            if (in_array($value['salesPersonID'], $salesPersonIds)) {
                                $singleDataArray = array(
                                    'Day' => $val['Day'],
                                    'creditNoteID' => $val['creditNoteID'],
                                    'creditNoteCode' => $val['creditNoteCode'],
                                    'creditNoteProductTotal' => round(($val['creditNoteProductTotal'] / $numOfSP), 2),
                                    'deleted' => $val['deleted'],
                                    'creditNoteProductID' => $val['creditNoteProductID'],
                                    'creditNoteTaxTotal' => round(($val['creditNoteTaxTotal'] / $numOfSP),2),
                                    'CusTitle' => $val['CusTitle'],
                                    'CusName' => $val['CusName'],
                                    'salesPersonSortName' => $value['salesPersonSortName'],
                                    'salesPersonLastName' => $value['salesPersonLastName'],
                                    'CnormalTax' => round(($creditNoteTaxDetails[$val['creditNoteID']]['normalTax'] / $numOfSP), 2),
                                    'CcompoundTax' => round(($creditNoteTaxDetails[$val['creditNoteID']]['compoundTax'] / $numOfSP), 2),
                                    'CsuspendedTax' => round(($creditNoteTaxDetails[$val['creditNoteID']]['suspendedTax'] /$numOfSP), 2),
                                    'salesPersonID' => $value['salesPersonID'],
                                    );
                                $creditNoteDataFinal[] = $singleDataArray;
                            }
                        }  
                    } else {
                        if (in_array('other', $salesPersonIds)) { 
                            $singleDataArray = array(
                                'Day' => $val['Day'],
                                'creditNoteID' => $val['creditNoteID'],
                                'creditNoteCode' => $val['creditNoteCode'],
                                'creditNoteProductTotal' => $val['creditNoteProductTotal'],
                                'deleted' => $val['deleted'],
                                'creditNoteProductID' => $val['creditNoteProductID'],
                                'creditNoteTaxTotal' => $val['creditNoteTaxTotal'],
                                'CusTitle' => $val['CusTitle'],
                                'CusName' => $val['CusName'],
                                'salesPersonSortName' => 'Other',
                                'salesPersonLastName' => $val['salesPersonLastName'],
                                'CnormalTax' => $creditNoteTaxDetails[$val['creditNoteID']]['normalTax'],
                                'CcompoundTax' => $creditNoteTaxDetails[$val['creditNoteID']]['compoundTax'],
                                'CsuspendedTax' => $creditNoteTaxDetails[$val['creditNoteID']]['suspendedTax'],
                                'salesPersonID' => $val['salesPersonID'],
                                );
                            $creditNoteDataFinal[] = $singleDataArray;
                        }
                    }
                 } else {
                    $singleDataArray = array(
                        'Day' => $val['Day'],
                        'creditNoteID' => $val['creditNoteID'],
                        'creditNoteCode' => $val['creditNoteCode'],
                        'creditNoteProductTotal' => $val['creditNoteProductTotal'],
                        'deleted' => $val['deleted'],
                        'creditNoteProductID' => $val['creditNoteProductID'],
                        'creditNoteTaxTotal' => $val['creditNoteTaxTotal'],
                        'CusTitle' => $val['CusTitle'],
                        'CusName' => $val['CusName'],
                        'salesPersonSortName' => $val['salesPersonSortName'],
                        'salesPersonLastName' => $val['salesPersonLastName'],
                        'CnormalTax' => $creditNoteTaxDetails[$val['creditNoteID']]['normalTax'],
                        'CcompoundTax' => $creditNoteTaxDetails[$val['creditNoteID']]['compoundTax'],
                        'CsuspendedTax' => $creditNoteTaxDetails[$val['creditNoteID']]['suspendedTax'],
                        'salesPersonID' => $val['salesPersonID'],
                        );
                    $creditNoteDataFinal[] = $singleDataArray;
                 }
            }
                 
            $respond = $salesDataFinal;
            
            $respondDays = $this->CommonTable('Invoice\Model\InvoiceTable')->getDailySalesDataDays($fromDate, $toDate);

            $days = $respondDays;
            $dailySalesData = array();
            //if salesPersonsIds are not null
            if ($salesPersonIds != NULL) {
                //filling sales person ids as key on $dailySalesData array
                $dailySalesData = array_fill_keys($salesPersonIds, '');
                //filling date as key for sales person array
                foreach ($dailySalesData as $key => $value) {
                    $dailySalesData[$key]['data'] = array_fill_keys($days, '');
                }
                //get sales person data and puts wanted data into $dailySalesData array
                $getSalesPerson = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByIds($salesPersonIds);
                foreach ($getSalesPerson as $sPerson) {
                    $dailySalesData[$sPerson['salesPersonID']]['salesPersonTitle'] = $sPerson['salesPersonTitle'];
                    $dailySalesData[$sPerson['salesPersonID']]['salesPersonSortName'] = $sPerson['salesPersonSortName'];
                }

                if ($withDefaultSalesPerson) {
                    $dailySalesData['other']['salesPersonTitle'] = '';
                    $dailySalesData['other']['salesPersonSortName'] = 'Others';
                }

                foreach ($respond as $k => $data) {
                    if (!is_null($data['salesPersonID'])) {
                        $dailySalesData[$data['salesPersonID']]['salesPersonLastName'] = $data['salesPersonLastName'];
                        $dailySalesData[$data['salesPersonID']]['data'][$data['Day']][] = $data;
                    } else {
                        $dailySalesData['other']['salesPersonLastName'] = '-';
                        $dailySalesData['other']['data'][$data['Day']][] = $data;
                    }
                }
                
                foreach ($creditNoteDataFinal as $k => $data) {
                    $dailySalesData[$data['salesPersonID']]['salesPersonLastName'] = $data['salesPersonLastName'];
                    $dailySalesData[$data['salesPersonID']]['data'][$data['Day']][] = $data;
                    if ($withDefaultSalesPerson && $data['salesPersonID'] == null) {
                        $dailySalesData['other']['salesPersonLastName'] = '';
                        $dailySalesData['other']['salesPersonSortName'] = 'Others';
                        $dailySalesData['other']['data'][$data['Day']][] = $data;

                    }

                }
            } else {
                foreach ($days as $day) {
                    $dailySalesData[$day] = array();
                }

                foreach ($respond as $res) {
                    array_push($dailySalesData[$res['Day']], $res);
                }

                foreach ($creditNoteDataFinal as $res) {
                    array_push($dailySalesData[$res['Day']], $res);
                }
            }
            
            $taxString['normalTaxString'] = $normalTaxString;
            $taxString['compoundTaxString'] = $compoundTaxString;

            return ['dailySalesData' => $dailySalesData,'taxString' => $taxString];
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * Generatte JSON Object from given data
     * @return \Zend\View\Model\JsonModel
     */
    public function viewDailySalesAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $salesPersonIds = $request->getPost('salesPersonIds');
            $locationID = $request->getPost('locationID');
            $cusCategories = ($request->getPost('cusCategories')) ? $request->getPost('cusCategories') : NULL;
           
            $translator = new Translator();
            $name = $translator->translate('Daily Sales Report');
            $period = $fromDate . ' - ' . $toDate;

            $dailySalesData = $this->_getDailySalesDetails($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategories);
            $cD = $this->getCompanyDetails();
            
            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);
            $dailySalesView = new ViewModel(array(
                'cD' => $cD,
                'dailySalesData' => $dailySalesData['dailySalesData'],
                'taxString' => $dailySalesData['taxString'],
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
                'checkTaxStatus' => $checkTaxStatus,
                'cusCategories' => $cusCategories
            ));

            if ($salesPersonIds != NULL) {
                $dailySalesView->setTemplate('reporting/sales-report/generate-daily-sales-by-sales-person-pdf');
            } else {
                $dailySalesView->setTemplate('reporting/sales-report/generate-daily-sales-pdf');
            }
            $this->html = $dailySalesView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $PDF
     */
    public function generateDailySalesPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $checkTaxStatus = $request['checkTaxStatus'];                        
            $salesPersonIds = ($request['salesPersonIds']) ? $request['salesPersonIds'] : null;
            $locationID = ($request['locationID']) ? $request['locationID'] : null;
            $cusCategories = ($request['cusCategories']) ? $request['cusCategories'] : null;
                        
            $translator = new Translator();
            $name = $translator->translate('Daily Sales Report');
            $period = $fromDate . ' - ' . $toDate;
            $dailySalesData = $this->_getDailySalesDetails($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategories);
            $cD = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewDailySales = new ViewModel(array(
                'cD' => $cD,
                'dailySalesData' => $dailySalesData['dailySalesData'],
                'taxString' => $dailySalesData['taxString'],
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
                'checkTaxStatus' => $checkTaxStatus,
                'cusCategories' => $cusCategories
            ));

            if ($salesPersonIds != NULL) {
                $viewDailySales->setTemplate('reporting/sales-report/generate-daily-sales-by-sales-person-pdf');
            } else {
                $viewDailySales->setTemplate('reporting/sales-report/generate-daily-sales-pdf');
            }
            $viewDailySales->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewDailySales);
            $this->downloadPDF('daily-sales-report', $htmlContent);

            exit;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return csv Generate Daily Sales csv file
     */
    public function generateDailySalesSheetAction()
    {
        $request = $this->getRequest();        
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $checkTaxStatus = filter_var($request->getPost('checkTaxStatus'), FILTER_VALIDATE_BOOLEAN);
            $salesPersonIds = ($request->getPost('salesPersonIds')) ? $request->getPost('salesPersonIds') : null;
            $locationID = ($request->getPost('locationID')) ? $request->getPost('locationID') : null;
            $cusCategories = ($request->getPost('cusCategories')) ? $request->getPost('cusCategories') : null;
            
            $dailySalesData = $this->
                _getDailySalesDetails($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategories);
            $cD = $this->getCompanyDetails();

            if ($dailySalesData) {
                $salesData = array();
                if ($salesPersonIds != NULL) {
                    $salesData = $this->_getDailySaleDataBySalesPerson($dailySalesData, $checkTaxStatus, $cD, $fromDate, $toDate);
                } else {
                    $salesData = $this->_getDailySaleData($dailySalesData, $checkTaxStatus, $cD, $fromDate, $toDate, $cusCategories);
                }
            } else {
                $salesData = "no matching records found\n";
            }

            $name = "Daily_Sales_Report.csv";            
            $csvContent = $this->csvContent($salesData['title'], $salesData['header'], $salesData['salesData']);
            $csvPath = $this->generateCSVFile($name, $csvContent);
            $this->data = $csvPath;
            $this->status = true;            
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $dailySalesData
     * @param string $checkTaxStatus
     * @return array $result
     */
    private function _getDailySaleDataBySalesPerson($dailySalesData, $checkTaxStatus, $cD, $fromDate, $toDate)
    {
        $netTax = 0;
        $netAmount = 0;
        $netNormalTax = 0.00;
        $netSusTax = 0.00;
        $netNormalAmount = 0.00;
        if ($checkTaxStatus == 'true') {
            $title = '';
            $tit = 'DAILY SALES REPORT';
            $in = ["", $tit];
            $title = implode(",", $in) . "\n";

            $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = [$cD[0]->companyName,
                $cD[0]->companyAddress,
                'Tel: ' . $cD[0]->telephoneNumber
            ];
            $title.=implode(",", $in) . "\n";

            $in = [ "Period : " . $fromDate . '-' . $toDate];
            $title.=implode(",", $in) . "\n";

            $arrs = ["SALES PERSON", "DATE", "ID", "CUSTOMER NAME", "TOTAL AMOUNT","NBT TAX AMOUNT","VAT TAX AMOUNT","Suspended TAX AMOUNT(SVAT)", "TOTAL AMOUNT WITHOUT TAX"];
            $header = implode(",", $arrs);
            $salesData = '';

            foreach ($dailySalesData['dailySalesData'] as $sID => $data) {
                $in = [$data['salesPersonSortName']];
                $salesData.=implode(",", $in) . "\n";

                if ($data['salesPersonSortName']) {

                    foreach ($data['data'] as $date => $invoiceData) {
                        $subAmount = 0.00;
                        $subNormalTax = 0.00;
                        $subComTax = 0.00;
                        $subSusTax = 0.00;
                        $subNormalAmount = 0.00;
                        $in = ["", $date];
                        $salesData.=implode(",", $in) . "\n";
                        
                        if (is_array($invoiceData)) {
                            foreach ($invoiceData as $d) {
                                $amount = 0.00;
                                $nrmTax = 0.00;
                                $comTax = 0.00;
                                $susTax = 0.00;
                                $amount = floatval($d['Amount']) - floatval($d['creditNoteProductTotal']);
                                $nrmTax = floatval($d['normalTax']) - floatval($d['CnormalTax']);
                                $comTax = floatval($d['compoundTax']) - floatval($d['CcompoundTax']);
                                $susTax = floatval($d['suspendedTax']) - floatval($d['CsuspendedTax']);
                                $tax = floatval($nrmTax) + floatval($comTax);
                                $taxWithoutAmount = floatval($amount) - floatval($tax);
                                
                                $subNormalTax += $nrmTax;
                                $subComTax += $comTax;
                                $subSusTax += $susTax;
                                $netNormalTax += $nrmTax;
                                $netComTax += $comTax;
                                $netSusTax += $susTax;
                                $subAmount += $taxWithoutAmount;
                                $netAmount += $taxWithoutAmount;
                                $netNormalAmount += $amount;
                                $subNormalAmount += $amount;
                                $in = ["", "", $d['InvID'],
                                    str_replace(',', "", $d['CusTitle'] . ' ' . $d['CusName']),
                                    $amount,
                                    $nrmTax,
                                    $comTax,
                                    $susTax,
                                    $taxWithoutAmount
                                ];
                                $salesData.=implode(",", $in) . "\n";
                            }
                        }                    

                        $in = ["",
                            "",
                            "",
                            "Total Sales $date",
                            $subNormalAmount,
                            $subNormalTax,
                            $subComTax,
                            $subSusTax,
                            $subAmount
                        ];
                        $salesData.=implode(",", $in) . "\n";
                    }
                }
            }
            $in = ["",
                "", "", "", "", "","", "Total Sales",
                $netNormalAmount
            ];
            $salesData.=implode(",", $in) . "\n";

            $in = ["",
                "", "", "", "", "","", "Total Sales Without TAX",
                $netAmount
            ];
            $salesData.=implode(",", $in) . "\n";

            $in = ["", "", "", "", "", "","",
                "NBT Tax Total",
                $netNormalTax
            ];
            $salesData.=implode(",", $in) . "\n";

            $in = ["", "", "", "", "", "","",
                "VAT Tax Total",
                $netComTax
            ];
            $salesData.=implode(",", $in) . "\n";

            $in = ["", "", "", "", "", "","",
                "Suspended Tax Total",
                $netSusTax
            ];
            $salesData.=implode(",", $in) . "\n";
        } else {
            $title = '';
            $tit = 'DAILY SALES REPORT';
            $in = ["", $tit];
            $title = implode(",", $in) . "\n";

            $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = ["Period : " . $fromDate . '-' . $toDate];
            $title.=implode(",", $in) . "\n";

            $arrs = ["SALES PERSON", "DATE", "INVOICE ID", "CUSTOMER NAME", "TOTAL AMOUNT"];
            $header = implode(",", $arrs);
            $salesData = '';
            foreach ($dailySalesData['dailySalesData'] as $sID => $data) {
                $in = [$data['salesPersonSortName']];
                $salesData.=implode(",", $in) . "\n";
                if ($data['salesPersonSortName']) {
                    foreach ($data['data'] as $date => $invoiceData) {
                        $subAmount = 0.00;
                        $subTax = 0.00;
                        $in = ["", $date];
                        $salesData.=implode(",", $in) . "\n";

                        foreach ($invoiceData as $d) {
                            
                            $amount = $d['Amount'] - $d['creditNoteProductTotal'];
                            $subAmount += $amount;

                            $in = ["", "", $d['InvID'],
                                str_replace(',', "", $d['CusTitle'] . ' ' . $d['CusName']),
                                $amount
                            ];
                            $salesData.=implode(",", $in) . "\n";
                        }

                        $in = ["", "", "", "Total Sales $date", $subAmount];
                        $salesData.=implode(",", $in) . "\n";
                        $netAmount += $subAmount;
                    }
                    
                }
            }
            $in = ["", "", "", "Total Sales", $netAmount];
            $salesData.=implode(",", $in) . "\n";
        }

        $result = [
            'salesData' => $salesData,
            'header' => $header,
            'title' => $title,
        ];

        return $result;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $dailySalesData
     * @param string $checkTaxStatus
     * @return array $salesData
     */
    private function _getDailySaleData($dailySalesData, $checkTaxStatus, $cD, $fromDate, $toDate, $cusCategories)
    {
        $netTax = 0;
        $netAmount = 0;
        $netNormalTax = 0.00;
        $netComTax = 0.00;
        $netSusTax = 0.00;
        $netNormalAmount = 0.00;
        $cusCatFlag = false;
        if(!empty($cusCategories)){
            $cusCatFlag = true;
        }
        if ($checkTaxStatus == 'true') {
            $title = '';
            $tit = 'DAILY SALES REPORT';
            $in = ["", $tit];
            $title = implode(",", $in) . "\n";

            $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
            $title.=implode(",", $in) . "\n";

            $in = ["Period : " . $fromDate . '-' . $toDate];
            $title.=implode(",", $in) . "\n";
            if($cusCatFlag){
                $arrs = array("DATE", "ID", "CUSTOMER NAME","CUSTOMER CATEGORY", "TOTAL AMOUNT","NBT TAX AMOUNT","VAT TAX AMOUNT","SUSPENDED TAX AMOUNT", "TOTAL AMOUNT WITHOUT TAX");
            } else {
                $arrs = array("DATE", "ID", "CUSTOMER NAME", "TOTAL AMOUNT","NBT TAX AMOUNT","VAT TAX AMOUNT", "SUSPENDED TAX AMOUNT", "TOTAL AMOUNT WITHOUT TAX");
            }
            $header = implode(",", $arrs);
            $salesData = '';

            foreach ($dailySalesData['dailySalesData'] as $date => $data) {
                $subAmount = 0.00;
                $subNormalTax = 0.00;
                $subComTax = 0.00;
                $subSusTax = 0.00;
                $subNormalAmount = 0.00;
                $in = [$date];
                $salesData.=implode(",", $in) . "\n";

                foreach ($data as $invID => $d) {
                    $amount = 0.00;
                    $nrmTax = 0.00;
                    $comTax = 0.00;
                    $susTax = 0.00;
                    $amount = floatval($d['Amount']) - floatval($d['creditNoteProductTotal']);
                    $nrmTax = floatval($d['normalTax']) - floatval($d['CnormalTax']);
                    $comTax = floatval($d['compoundTax']) - floatval($d['CcompoundTax']);
                    $susTax = floatval($d['suspendedTax']) - floatval($d['CsuspendedTax']);
                    $tax = floatval($nrmTax) + floatval($comTax);
                    $taxWithoutAmount = floatval($amount) - floatval($tax);
                    $subNormalTax += $nrmTax;
                    $subComTax += $comTax;
                    $subSusTax += $susTax;
                    $netNormalTax += $nrmTax;
                    $netComTax += $comTax;
                    $netSusTax += $susTax;
                    $subAmount += $taxWithoutAmount;
                    $netAmount += $taxWithoutAmount;
                    $netNormalAmount += $amount;
                    $subNormalAmount += $amount;
            
                    if($cusCatFlag){
                        $in = ["", $d['InvID'], str_replace(',', "", $d['CusTitle'] . ' ' . $d['CusName']),
                            $d['customerCategoryName'],
                            $amount,
                            $nrmTax,
                            $comTax,
                            $susTax,
                            $taxWithoutAmount
                        ];
                    } else {
                        $in = ["", $d['InvID'], str_replace(',', "", $d['CusTitle'] . ' ' . $d['CusName']),
                            $amount,
                            $nrmTax,
                            $comTax,
                            $susTax,
                            $taxWithoutAmount
                        ];
                        
                    }
                    $salesData.=implode(",", $in) . "\n";
                }
                if($cusCatFlag){
                    $in = ["Total Sales $date",
                    "", "","", $subNormalAmount,$subNormalTax,
                        $subComTax,
                        $subSusTax,
                        $subAmount
                    ];
                } else {
                    $in = ["Total Sales $date",
                    "", "", $subNormalAmount,$subNormalTax,
                        $subComTax,
                        $subSusTax,
                        $subAmount
                    ];    
                }
                
                $salesData.=implode(",", $in) . "\n";
            }
            if($cusCatFlag){
                $in = ["", "","", "", "","", "", "Total Sales", $netNormalAmount];
                $salesData.=implode(",", $in) . "\n";

                $in = ["", "", "","", "","", "", "Total Sales Without Tax", $netAmount];
                $salesData.=implode(",", $in) . "\n";

                $in = ["", "", "", "", "","","", "NBT Tax Total", $netNormalTax];
                $salesData.=implode(",", $in) . "\n";

                $in = ["", "", "", "", "","", "", "VAT Tax Total", $netComTax];
                $salesData.=implode(",", $in) . "\n";

                $in = ["", "", "", "", "","", "", "Suspended Tax Total", $netSusTax];
                $salesData.=implode(",", $in) . "\n";
            } else{
                $in = ["", "", "", "","", "", "Total Sales", $netNormalAmount];
                $salesData.=implode(",", $in) . "\n";

                $in = ["", "", "", "","", "", "Total Sales Without Tax", $netAmount];
                $salesData.=implode(",", $in) . "\n";

                $in = ["", "", "", "", "","", "NBT Tax Total", $netNormalTax];
                $salesData.=implode(",", $in) . "\n";

                $in = ["", "", "", "", "", "", "VAT Tax Total", $netComTax];
                $salesData.=implode(",", $in) . "\n";
                
                $in = ["", "", "", "", "", "", "Suspended Tax Total", $netSusTax];
                $salesData.=implode(",", $in) . "\n";
            }
        } else {
            $title = '';
            $tit = 'DAILY SALES REPORT';
            $in = ["", $tit];
            $title = implode(",", $in) . "\n";

            $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $in = ["Period : " . $fromDate . '-' . $toDate];
            $title.=implode(",", $in) . "\n";

            if($cusCatFlag){
                $arrs = ["DATE", "ID", "CUSTOMER NAME", "CUSTOMER CATEGORY", "TOTAL AMOUNT"];
            } else {
                $arrs = ["DATE", "ID", "CUSTOMER NAME", "TOTAL AMOUNT"];
            }
            $header = implode(",", $arrs);
            $salesData = '';
            foreach ($dailySalesData['dailySalesData'] as $date => $data) {

                $subAmount = 0;
                $in = [$date];
                $salesData.=implode(",", $in) . "\n";

                foreach ($data as $invID => $d) {                    
                    $amount = $d['Amount'] - $d['creditNoteProductTotal'];
                    
                    $netAmount += $amount;
                    $subAmount += $amount;

                    if($cusCatFlag){
                        $in = ["", $d['InvID'], str_replace(',', "", $d['CusTitle'] . ' ' . $d['CusName']), $d['customerCategoryName'], $amount];
                    } else {
                        $in = ["", $d['InvID'], str_replace(',', "", $d['CusTitle'] . ' ' . $d['CusName']), $amount];
                        
                    }
                    $salesData.=implode(",", $in) . "\n";
                }
                if($cusCatFlag){
                    $in = ["Total Sales $date", "", "", "",$subAmount];
                } else {
                    $in = ["Total Sales $date", "", "", $subAmount];
                        
                }
                
                $salesData.=implode(",", $in) . "\n";
            }
            if($cusCatFlag){
                    $in = ["Total Sales","","", "", $netAmount];
                } else {
                    $in = ["Total Sales","", "", $netAmount];
                        
                }
            
            $salesData.=implode(",", $in) . "\n";
        }
        
        $result = [
            'salesData' => $salesData,
            'header' => $header,
            'title' => $title,
        ];

        return $result;
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $dailySalesGraphData
     */
    public function getDailySalesGraphDataAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $request->getPost('locationID');
            $cusCategories = $request->getPost('cusCategories');

            $dailySalesData = $this->_getDailySalesDetails($fromDate, $toDate, NULL, $locationID, $cusCategories);

            $resultSet = array();
            foreach ($dailySalesData['dailySalesData'] as $key => $d) {
                $salesAmount = 0;
                $taxAmount = 0;
                $flag = FALSE;

                foreach ($d as $data) {
                    $salesAmountWithCreditNote = $data['Amount'] - $data['creditNoteProductTotal'] - $data['creditNoteProductTax'];
                    $taxAmountWithCreditNote = $data['Tax'] - $data['creditNoteProductTax'];
                    
                    if ($key == $data['Day']) {
                        $salesAmount += $salesAmountWithCreditNote;
                        $taxAmount += $taxAmountWithCreditNote;
                        $resultSet[$key] = array('salesAmount' => $salesAmount, 'taxAmount' => $taxAmount);
                        $flag = TRUE;
                    }
                }
                if ($flag == FALSE) {
                    $resultSet[$key] = $d;
                }
            }
            $dailySalesDataSet = array('dailySalesData' => $resultSet, 'cSymbol' => $this->companyCurrencySymbol);

            $this->data = $dailySalesDataSet;
            $this->status = true;
            $this->msg = $this->getMessage('INFO_SALESREPORT_SALEGRAPH');
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param Date $year
     * @return array $result
     */
    public function getAnnualSalesDetails($year = NULL, $locationID = NULL, $cusCategories = NULL)
    {
        if (isset($year)) {
            $annualSalesWithTax = $this->CommonTable('Invoice\Model\InvoiceTable')->getAnnualSalesData($year, $locationID, $cusCategories);
            // $annualTax = $this->CommonTable('Invoice\Model\InvoiceTable')->getAnnualSalesTaxData($year, $locationID, $cusCategories);
            $fromDate = $year . '-01-01';
            $toDate = $year . '-12-31';
            $respondTax = $this->CommonTable('Invoice\Model\InvoiceTable')->getMonthlySalesTaxDetails($fromDate, $toDate, $locationID, $customerCategory);
            $annualTax = [];

            //get all suspended taxes
            $simpleTaxDetails = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
            $suspendedTaxes = [];
            foreach ($simpleTaxDetails as $key => $value) {
                if ( $value->taxSuspendable == 1 ) {
                    $suspendedTaxes[$value->id] = $value->id;
                }
            }

            foreach ($respondTax as $key => $value) {
                if ($value['Month'] === 'January' || $value['Month'] === 'February' || $value['Month'] === 'March') {
                    $quarter = "Q1Tax";
                } else if ($value['Month'] === 'April' || $value['Month'] === 'May' || $value['Month'] === 'June') {
                    $quarter = "Q2Tax";
                } else if ($value['Month'] === 'July' || $value['Month'] === 'August' || $value['Month'] === 'September') {
                    $quarter = "Q3Tax";
                } else if ($value['Month'] === 'October' || $value['Month'] === 'November' || $value['Month'] === 'December') {
                    $quarter = "Q4Tax";
                }
                if ($value['salesInvoiceSuspendedTax'] == '1' && array_key_exists($value['taxID'], $suspendedTaxes)) {
                    $annualTax[$quarter]['suspendedTax'] += floatval($value['salesInvoiceProductTaxAmount']);
                } else {
                    if ($value['taxType'] == "v") {
                        $annualTax[$quarter]['normalTax'] += floatval($value['salesInvoiceProductTaxAmount']);
                    } else if ($value['taxType'] == "c"){
                        $annualTax[$quarter]['compoundTax'] += floatval($value['salesInvoiceProductTaxAmount']);
                    }
                }
            }

            $creditNoteProductTotal = $this->CommonTable('Invoice\Model\InvoiceTable')->getAnnualCreditNoteProductData($year, $locationID, $cusCategories);
            $creditNoteProductTaxTotal = $this->CommonTable('Invoice\Model\InvoiceTable')->getAnnualCreditNoteProductTaxData($year, $locationID, $cusCategories);

            if (empty($creditNoteProductTotal[0])) {
                $creditNoteProductTotal[0] = [];
            }

            if (empty($creditNoteProductTaxTotal[0])) {
                $creditNoteProductTaxTotal[0] = [];
            }

            $result[0] = array_merge(
                    $annualSalesWithTax[0], 
                    $annualTax, 
                    $creditNoteProductTotal[0],
                    $creditNoteProductTaxTotal[0]
            );

            if (!$result) {
                return null;
            }
            return $result;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml()
     */
    public function viewAnnualSalesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $year = $request->getPost('year');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $locationID = $request->getPost('locationID');
            $cusCategories = $request->getPost('cusCategories');

            $translator = new Translator();
            $name = $translator->translate('Annual Sales Report');
            $period = $year;

            $annualSalesData = $this->getAnnualSalesDetails($year, $locationID, $cusCategories);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $annualSalesView = new ViewModel(array(
                'cD' => $companyDetails,
                'annualSalesData' => $annualSalesData,
                'year' => $year,
                'headerTemplate' => $headerViewRender,
                'checkTaxStatus' => $checkTaxStatus,

            ));
            $annualSalesView->setTemplate('reporting/sales-report/generate-annual-sales-pdf');

            $this->html = $annualSalesView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateAnnualSalesPdfAction()
    {
        $requset = $this->getRequest()->getQuery();
        if ($requset) {
            $year = $requset['year'];
            $checkTaxStatus = $requset['checkTaxStatus'];
            $locationID = ($requset['locationID'] == 'null') ? "" : explode(",", $requset['locationID']);
            $cusCategories = ($requset['cusCategories'] == 'null') ? "" : explode(",", $requset['cusCategories']);
           
            $translator = new Translator();
            $name = $translator->translate('Annual Sales Report');
            $period = $year;

            $annualSalesData = $this->getAnnualSalesDetails($year, $locationID, $cusCategories);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewAnnualSales = new ViewModel(array(
                'cD' => $companyDetails,
                'annualSalesData' => $annualSalesData,
                'checkTaxStatus' => $checkTaxStatus,
                'headerTemplate' => $headerViewRender,
                'year' => $year
            ));

            $viewAnnualSales->setTemplate('reporting/sales-report/generate-annual-sales-pdf');
            $viewAnnualSales->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewAnnualSales);
            $this->downloadPDF('annual-sales-report', $htmlContent);
            exit;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * Generate Annual sales  CSV file
     * @return csv
     */
    public function generateAnnualSalesSheetAction()
    {
        $request = $this->getRequest();        
        if ($request->isPost()) {
            $year = $request->getPost('year');
            $checkTaxStatus = $request->getPost('checkTaxStatus');
            $locationID = $request->getPost('locationID');
            $cusCategories = $request->getPost('cusCategories');
            
            $annualSalesData = $this->getAnnualSalesDetails($year, $locationID, $cusCategories);
            $cD = $this->getCompanyDetails();

            if ($annualSalesData) {
                $title = '';
                $tit = 'ANNUAL SALES REPORT';
                $in = ['', $tit];
                $title = implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $in = ['Period : ' . $year];
                $title.=implode(",", $in) . "\n";

                if ($checkTaxStatus == 'true') {
                    $arrs = ["YEAR", "QUARTER", "TAX", "SUSPENDED TAX", "SALES AMOUNT"];
                    $header = implode(",", $arrs);
                    $arr = '';

                    foreach ($annualSalesData as $data) {
                        $q1Tax = floatval($data['Q1Tax']['normalTax']) + floatval($data['Q1Tax']['compoundTax']) - floatval($data['Q1CreditNoteTax']);
                        $q2Tax = floatval($data['Q2Tax']['normalTax']) + floatval($data['Q2Tax']['compoundTax']) - floatval($data['Q2CreditNoteTax']);
                        $q3Tax = floatval($data['Q3Tax']['normalTax']) + floatval($data['Q3Tax']['compoundTax']) - floatval($data['Q3CreditNoteTax']);
                        $q4Tax = floatval($data['Q4Tax']['normalTax']) + floatval($data['Q4Tax']['compoundTax']) - floatval($data['Q4CreditNoteTax']);

                        $q1SusTax = floatval($data['Q1Tax']['suspendedTax']);
                        $q2SusTax = floatval($data['Q2Tax']['suspendedTax']);
                        $q3SusTax = floatval($data['Q3Tax']['suspendedTax']);
                        $q4SusTax = floatval($data['Q4Tax']['suspendedTax']);

                        $q1Sales = floatval($data['Q1Amount']) - floatval($data['Q1CreditNote']);
                        $q2Sales = floatval($data['Q2Amount']) - floatval($data['Q2CreditNote']);
                        $q3Sales = floatval($data['Q3Amount']) - floatval($data['Q3CreditNote']);
                        $q4Sales = floatval($data['Q4Amount']) - floatval($data['Q4CreditNote']);
                        
                        $TotalTax = $q1Tax + $q2Tax + $q3Tax + $q4Tax;
                        $TotalSusTax = $q1SusTax + $q2SusTax + $q3SusTax + $q4SusTax;
                        $TotalAmount = $q1Sales + $q2Sales + $q3Sales + $q4Sales;

                        $in1 = [$year, 'Quarter 1', $q1Tax, $q1SusTax, $q1Sales];
                        $arr.=implode(",", $in1) . "\n";

                        $in2 = ["", 'Quarter 2', $q2Tax, $q2SusTax, $q2Sales];
                        $arr.=implode(",", $in2) . "\n";

                        $in3 = ["", 'Quarter 3', $q3Tax, $q3SusTax, $q3Sales];
                        $arr.=implode(",", $in3) . "\n";

                        $in4 = ["", 'Quarter 4', $q4Tax, $q4SusTax, $q4Sales];
                        $arr.=implode(",", $in4) . "\n";

                        $in5 = ["", "", "", "Total Sales", $TotalAmount];
                        $arr.=implode(",", $in5) . "\n";

                        $in6 = ["", "", "", "Total Tax", $TotalTax];
                        $arr.=implode(",", $in6) . "\n";

                        $in7 = ["", "", "", "Total Suspended Tax", $TotalSusTax];
                        $arr.=implode(",", $in7) . "\n";

                        $in8 = ["", "", "", "Sales Excluding Tax", $TotalAmount - $TotalTax];
                        $arr.=implode(",", $in8) . "\n";
                    }
                } else {
                    $arrs = array("YEAR", "QUARTER", "AMOUNT");
                    $header = implode(",", $arrs);
                    $arr = '';
                    foreach ($annualSalesData as $data) {
                        
                        $q1Sales = $data['Q1Amount'] - $data['Q1CreditNote'];
                        $q2Sales = $data['Q2Amount'] - $data['Q2CreditNote'];
                        $q3Sales = $data['Q3Amount'] - $data['Q3CreditNote'];
                        $q4Sales = $data['Q4Amount'] - $data['Q4CreditNote'];
                        $TotalAmount = $q1Sales + $q2Sales + $q3Sales + $q4Sales;
                        
                        $year = $data['Year'];

                        $in1 = [$year, "Quarter 1", $q1Sales];
                        $arr.=implode(",", $in1) . "\n";

                        $in2 = ["", "Quarter 2", $q2Sales];
                        $arr.=implode(",", $in2) . "\n";

                        $in3 = ["", "Quarter 3", $q3Sales];
                        $arr.=implode(",", $in3) . "\n";

                        $in4 = ["", "Quarter 4", $q4Sales];
                        $arr.=implode(",", $in4) . "\n";

                        $arr.=implode(",", []) . "\n";
                        $in6 = ["Total Sales : $year", "", $TotalAmount];
                        $arr.=implode(",", $in6) . "\n";
                    }
                }
            } else {
                $arr = "no matching records found\n";
            }
            $name = "Annual_Sales_Report.csv";            
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;            
            return $this->JSONRespond();            
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return JSONRespond
     */
    public function getAnnuvalSalesGraphDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $year = $request->getPost('year');
            $locationID = $request->getPost('locationID');
            $cusCategories = $request->getPost('cusCategories');
            $annualSalesData = $this->getAnnualSalesDetails($year, $locationID, $cusCategories);
            
            $annualSalesDataSet = array('annualSalesData' => $annualSalesData, 'cSymbol' => $this->companyCurrencySymbol);

            $this->data = $annualSalesDataSet;
            $this->status = true;
            $this->msg = $this->getMessage('INFO_SALESREPORT_ANNUAL_SALEGRAPH');
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @return array $respond
     */
    public function getSalesSummeryDetails($fromDate, $toDate, $locationID, $cusCategories = NULL)
    {
        if (isset($fromDate) && isset($toDate)) {
            $salesSummeryWithOutTax = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesSummeryWithOutTax($fromDate, $toDate, $locationID, $cusCategories);

            $salesSummeryWithOutTaxArray = [];
            foreach ($salesSummeryWithOutTax as $value) {
                $salesSummeryWithOutTaxArray[$value['customerID']][] = $value;
            }

            $creditNoteData = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesSummeryCreditNoteData($fromDate, $toDate, $locationID, $cusCategories);
            $creditNoteDataArray = [];
            foreach ($creditNoteData as $value) {
                $creditNoteDataArray[$value['customerID']][] = $value;
            }

            $creditNoteTaxData = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesSummeryCreditNoteTaxData($fromDate, $toDate, $locationID, $cusCategories);

            $creditNoteTaxDataArray = [];
            foreach ($creditNoteTaxData as $value) {
                $creditNoteTaxDataArray[$value['customerID']][] = $value;
            }
            $summeryData = array_replace_recursive($salesSummeryWithOutTaxArray, $creditNoteDataArray, $creditNoteTaxDataArray);

            $dataSet = [];
            foreach ($summeryData as $wT) {

                foreach ($wT as $dd) {
                    $invoiceTax = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesSummeryWithTax($fromDate, $toDate, $dd['salesInvoiceID']);
                    $dataSet[$dd['customerID']][$dd['salesInvoiceID']] = $dd;
                    $dataSet[$dd['customerID']][$dd['salesInvoiceID']]['TotalTax'] = $invoiceTax[0]['TotalTax'] - $dd['creditNoteProductTax'];
                    if ($dd['salesInvoiceSuspendedTax'] == 1) {
                        $dataSet[$dd['customerID']][$dd['salesInvoiceID']]['TotalAmount'] = $dd['TotalAmount'] + $invoiceTax[0]['TotalTax'];
                    }
                }
            }

            if (!$dataSet) {
                return null;
            }
            return $dataSet;
        }
    }

    public function viewSalesSummeryAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $request->getPost('locationID');
            $cusCategories = $request->getPost('cusCategories');

            $translator = new Translator();
            $name = $translator->translate('Sales Summary Report');
            $period = $fromDate . ' - ' . $toDate;

            $salesSummeryData = $this->getSalesSummeryDetails($fromDate, $toDate, $locationID, $cusCategories);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $salesSummeryView = new ViewModel(array(
                'cD' => $companyDetails,
                'salesSummeryData' => $salesSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
                'cusCategories' => $cusCategories
            ));
            $salesSummeryView->setTemplate('reporting/sales-report/generate-sales-summery-pdf');

            $this->html = $salesSummeryView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateSalesSummeryPdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromDate = $request['fromDate'];
            $toDate = $request['toDate'];
            $locationID = ($request['locationID'] == 'null') ? "" : explode(",", $request['locationID']);
            $cusCategories = ($request['cusCategories'] == 'null') ? "" : explode(",", $request['cusCategories']);
            

            $translator = new Translator();
            $name = $translator->translate('Sales Summary Report');
            $period = $fromDate . ' - ' . $toDate;

            $salesSummeryData = $this->getSalesSummeryDetails($fromDate, $toDate, $locationID, $cusCategories);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewSalesSummery = new ViewModel(array(
                'cD' => $companyDetails,
                'salesSummeryData' => $salesSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
                'cusCategories' => $cusCategories
            ));

            $viewSalesSummery->setTemplate('reporting/sales-report/generate-sales-summery-pdf');
            $viewSalesSummery->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewSalesSummery);
            $this->downloadPDF('sales-summary-report', $htmlContent);

            exit;
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * Generate Sales Summery csv Sheet
     * @return csv
     */
    public function generateSalesSummerySheetAction()
    {
        $request = $this->getRequest();        
        if ($request->isPost()) {            
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationID = $request->getPost('locationID');
            $cusCategories = $request->getPost('cusCategories');
            
            $salesSummeryData = $this->getSalesSummeryDetails($fromDate, $toDate, $locationID, $cusCategories);
            $cD = $this->getCompanyDetails();

            if ($salesSummeryData) {
                $netTax = 0;
                $netAmount = 0;
                $title = '';
                $tit = 'SALES SUMMARY REPORT';
                $in = ["", $tit];
                $title = implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $in = ["Period : " . $fromDate . '-' . $toDate];
                $title.=implode(",", $in) . "\n";

                $arrs = ["CUSTOMER NAME", "TOTAL EXCLUDING TAX", "TAX"];
                $header = implode(",", $arrs);
                $arr = '';

                $netAmount = 0;
                $netTax = 0;
                foreach ($salesSummeryData as $cData) {
                    $customerTitle = '';
                    $customerName = '';
                    $totalAmount = 0;
                    $totalTax = 0;
                    foreach ($cData as $data) {
                        $customerTitle = $data['customerTitle'];
                        $customerName = str_replace(',', "",$data['customerName']);
                        $amount = $data['TotalAmount'] - $data['creditNoteProductTotal'];
                        $amount = $amount ? $amount : 0;
                        $tax = $data['TotalTax'];
                        $tax = $tax ? $tax : 0;
                        $totalAmount += $amount;
                        $netAmount += ($amount - $tax);
                        $totalTax += $tax;
                        $netTax += $tax;
                    }
                    $in = [ $customerTitle . ' ' . $customerName, $totalAmount - $totalTax, $totalTax];
                    $arr.=implode(",", $in) . "\n";
                }
                $in = ['Net Total', $netAmount, $netTax];
                $arr.=implode(",", $in) . "\n";
            } else {
                $arr = 'no matching records found \n';
            }
            
            $name = "sales-summary-report.csv";            
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;            
            return $this->JSONRespond();            
        }
    }

    /**
     * @author Sandun  Dissanayake<sandun@thinkcube.com>
     * @return array $respond
     */
    public function getPaymentsSummeryDetails($fromDate = null, $toDate = null, $sort = null, $paymentNumber = null, $locationNumber = null)
    {
        if (isset($fromDate) && isset($toDate)) {
            $respon = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsSummeryData($fromDate, $toDate, $sort, $paymentNumber, $locationNumber);
            $respond = $respon ? $respon : array();
            if (!$respond) {
                return null;
            }
            $finalData = array();
            $i = 0;
            foreach ($respond as $res) {
                if (in_array('1', $paymentNumber) && isset($res['incomingPaymentMethodCashId'])) {
                    $res['paymentMethodName'] = 'Cash';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('2', $paymentNumber) && isset($res['incomingPaymentMethodChequeId'])) {
                    $res['paymentMethodName'] = 'Cheque';
                    $res['paymentMethodReferenceNumber'] = $res['incomingPaymentMethodChequeNumber'];
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('3', $paymentNumber) && isset($res['incomingPaymentMethodCreditCardId'])) {
                    $res['paymentMethodName'] = 'Credit Card';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('4', $paymentNumber) && isset($res['incomingPaymentMethodLoyaltyCardId'])) {
                    $res['paymentMethodName'] = 'Loyalty Card';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('5', $paymentNumber) && isset($res['incomingPaymentMethodBankTransferId'])) {
                    $res['paymentMethodName'] = 'Bank Transfer';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                } else if (in_array('6', $paymentNumber) && isset($res['incomingPaymentMethodGiftCardId'])) {
                    $res['paymentMethodName'] = 'Gift Card';
                    $finalData[$res['incomingPaymentCode']][$i] = $res;
                }
                $i++;
            }
            return $finalData;
        }
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function viewPaymentsSummeryAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $sort = $request->getPost('sort');
            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');
            $translator = new Translator();
            $name = $translator->translate('Payment Summary Report');
            $period = $fromDate . ' - ' . $toDate;

            $paymentSummeryData = $this->getPaymentsSummeryDetails($fromDate, $toDate, $sort, $paymentNumber, $locationNumber);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $paymentSummeryView = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentSummeryData' => $paymentSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));
            $paymentSummeryView->setTemplate('reporting/sales-report/generate-payments-summery-pdf');

            $this->html = $paymentSummeryView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generatePaymentsSummeryPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $translator = new Translator();
            $name = $translator->translate('Payment Summary Report');
            $period = $fromDate . ' - ' . $toDate;

            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');

            $paymentSummeryData = $this->getPaymentsSummeryDetails($fromDate, $toDate, $sort = NULL, $paymentNumber, $locationNumber);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $viewPaymentSummery = new ViewModel(array(
                'cD' => $companyDetails,
                'paymentSummeryData' => $paymentSummeryData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'headerTemplate' => $headerViewRender,
            ));

            $viewPaymentSummery->setTemplate('reporting/sales-report/generate-payments-summery-pdf');
            $viewPaymentSummery->setTerminal(true);

            $htmlContent = $this->viewRendererHtmlContent($viewPaymentSummery);
            $pdfPath = $this->downloadPDF('payments-summary-report', $htmlContent, true);
            $this->data = $pdfPath;
            $this->status = true;

            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * Generate Payment Summery CSV Sheet
     * @return CSV Sheet
     */
    public function generatePaymentsSummerySheetAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $paymentNumber = $request->getPost('paymentNumber');
            $locationNumber = $request->getPost('locationNumber');

            $paymentsSummeryData = $this->getPaymentsSummeryDetails($fromDate, $toDate, $sort, $paymentNumber, $locationNumber);
            $cD = $this->getCompanyDetails();

            if ($paymentsSummeryData) {
                $title = '';
                $tit = 'PAYMENT SUMMARY REPORT';
                $in = [ "", "", "", $tit];
                $title = implode(",", $in) . "\n";

                $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
                $title.=implode(",", $in) . "\n";

                $in = [$cD[0]->companyName, $cD[0]->companyAddress, 'Tel: ' . $cD[0]->telephoneNumber];
                $title.=implode(",", $in) . "\n";

                $in = ["Period : " . $fromDate . '-' . $toDate];
                $title.=implode(",", $in) . "\n";

                $arrs = ["PAYMENT NO", "DATE", "CUSTOMER", "LOCATION", "PAYMENT METHOD AND AMOUNT", "CHEQUE NO", "AMOUNT", "REMARKS"];
                $header = implode(",", $arrs);
                $arr = '';
                foreach ($paymentsSummeryData as $paymentsSummeryData1) {
                    $rowNo = 0;
                    foreach ($paymentsSummeryData1 as $data) {
                        if ($rowNo != 0) {
                            $data['incomingPaymentCode'] = '';
                        }
                        if ($rowNo != count($paymentsSummeryData1) - 1) {
                            $data['incomingPaymentAmount'] = '';
                        }
                        $in = [$data['incomingPaymentCode'], $data['incomingPaymentDate'],
                            $data['customerTitle'] . ' ' . $data['customerName'],
                            $data['locationName'],
                            '(' . $data['paymentMethodName'] . ') ' . $data['incomingInvoicePaymentAmount'],
                            $data['paymentMethodReferenceNumber'],
                            $data['incomingPaymentAmount'],
                            $data['incomingPaymentMemo']
                        ];
                        $arr.=implode(",", $in) . "\n";
                        $rowNo++;
                    }
                }
            } else {
                $arr = 'no matching records founds \n';
            }

            $name = "payment_summary_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * given html content which related to gross profit report
     * @return JSONRespondHtml
     */
    public function viewGrossProfitAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $locationList = $request->getPost('locationList');
            $cusCategories = $request->getPost('cusCategories');

            $grossProfitData = $this->getService('SalesReportService')->_getGrossProfitData($locationList, $fromDate, date('Y-m-d H:i:s', strtotime($toDate)), $cusCategories);
            $name = 'Gross Profit Report';

            $headerView = $this->headerViewTemplate($name, $period = $fromDate . ' - ' . $toDate);
            $headerView->setTemplate('reporting/template/headerTemplate');
            $headerViewRender = $this->htmlRender($headerView);

            $grossProfitView = new ViewModel(array(
                'grossProfitData' => $grossProfitData,
                'cSymbol' => $this->companyCurrencySymbol,
                'headerTemplate' => $headerViewRender,
            ));
            $grossProfitView->setTemplate('reporting/sales-report/generate-gross-profit-pdf');

            $this->html = $grossProfitView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * given pdf path which related to gross profit report
     * @return PDF path
     */
    public function generateGrossProfitPdfAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['gross-profit-report'], 'pdf');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * given csv path which related to gross profit report
     * @return CSV path
     */
    public function generateGrossProfitSheetAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $postData = $request->getPost()->toArray(); // get report request data

            $config = $this->getServiceLocator()->get('config');

            $meta = $this->getReportJobCallbackData($config['report']['gross-profit-report'], 'csv');

            $jobDetails = ['meta' => $meta, 'data' => $postData ]; // create job details array

            $result = ReportJob::add($jobDetails); // add to queue

            if (!$result) { // if erorr occures
                return $this->returnJsonError('An error occurred while job adding to queue.');
            }

            return $this->returnJsonSuccess($result, 'Report has been added to queue.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('An error occurred while adding to queue.');
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @return array $grossProfitData
     */
    private function _getGrossProfitData($locationList, $fromDate, $toDate, $cusCategories = NULL)
    {
        if (isset($locationList) && isset($fromDate) && isset($toDate)) {
            $getSalesInvoiceData = $this->CommonTable('Inventory\Model\ItemOutTable')->getSalesInvoiceDetails($fromDate, $toDate, $cusCategories);
            $getCreditNoteData = $this->CommonTable('Inventory\Model\ItemInTable')->getCreditNoteDetails($fromDate, $toDate, $cusCategories);
            $mergeData = array_merge_recursive($getCreditNoteData, $getSalesInvoiceData);
            $grossInvoiceCreditData = [];

            //Sales Invoice and Credit note related data put into one array
            //beacasue of easy to calculations
            foreach ($mergeData as $key => $d) {
                $date = $d['date'];
                $documentType = NULL;
                $documentNo = NULL;
                $documentValue = NULL;
                $documentCost = NULL;
                if (isset($d['itemOutDocumentType'])) {
                
                    $documentType = $d['itemOutDocumentType'];
                    $documentNo = $d['salesInvoiceCode'];
                    $documentCost = (floatval($d['totalInvoiceCost']) - (floatval($d['itemOutQty']) * floatval($d['itemInDiscount'])));
                    $documentValue = $d['itemOutQty']*($d['itemOutPrice']-$d['itemOutDiscount']);
                    //get invoice discount
                    $invoiceDetails = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoiceByID($d['itemOutDocumentID']);
                    // $nonInventoryItemTotal = $this->CommonTable("Invoice\Model\InvoiceTable")->getNonInventoryItemTotal($d['itemOutDocumentID']);
                    $nonInventoryItemTotal = 0;
                    //create array and add invoiceIds to it for optimizing quary
                    $invoiceIdsTempArray = [];
                    if(!array_key_exists($d['itemOutDocumentType'], $invoiceIdsTempArray)){
                        //get non inventory details
                        $nonInventoryDataSet = $this->CommonTable("Invoice\Model\InvoiceTable")->getNonAllInventoryItemTotalByInvoiceID($d['itemOutDocumentID']);
                        $nonInvetCalculatedTotal = 0;
                        
                        foreach ($nonInventoryDataSet as $key => $nonSingleValue) {
                            if($nonSingleValue['salesInvoiceProductDiscount'] != null && $nonSingleValue['salesInvoiceProductDiscountType'] == 'precentage'){
                                
                                $nonInvetCalculatedTotal += (floatval($nonSingleValue['salesInvoiceProductQuantity']) * floatval($nonSingleValue['salesInvoiceProductPrice'])) * (100- floatval($nonSingleValue['salesInvoiceProductDiscount']))/100;
                            
                            } else if( $nonSingleValue['salesInvoiceProductDiscount'] != null && $nonSingleValue['salesInvoiceProductDiscountType'] == 'value') {

                                $nonInvetCalculatedTotal += (floatval($nonSingleValue['salesInvoiceProductQuantity']) * floatval($nonSingleValue['salesInvoiceProductPrice'])) - floatval($nonSingleValue['salesInvoiceProductDiscount']);
                            } else {

                                $nonInvetCalculatedTotal += (floatval($nonSingleValue['salesInvoiceProductQuantity']) * floatval($nonSingleValue['salesInvoiceProductPrice']));
                            }
                            
                        }
                        $invoiceIdsTempArray[$d['itemOutDocumentType']] = $d['itemOutDocumentType'];
                        $nonInventoryItemTotal = $nonInvetCalculatedTotal;
                        
                    }
                    

                    $nonInventoryItemTotal = ($nonInventoryItemTotal) ? $nonInventoryItemTotal : 0;
                    $invoiceWiseTotalDiscount = ($invoiceDetails->salesInvoiceWiseTotalDiscount) ? $invoiceDetails->salesInvoiceWiseTotalDiscount : 0;
                    
                    // $documentValue = $documentValue - $invoiceWiseTotalDiscount;
                    $invoiceWiseTotalDiscount = $invoiceDetails->salesInvoiceWiseTotalDiscount;
                                        
                    $grnID = $d['itemInDocumentID'];
                    $locationProductID = $d['itemInLocationProductID'];
                    $getProductTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($grnID, $locationProductID);
                    $totalTax = 0;
                    foreach ($getProductTax as $t) {
                        $totalTax += $t['grnTaxAmount'] / $t['grnProductTotalQty'];
                    }
                    $grossInvoiceCreditData['invoice'][$d['itemOutDocumentID']][] = [
                        'locationID' => $d['locationID'],
                        'locationName' => $d['locationName'],
                        'locationCode' => $d['locationCode'],
                        'date' => $date,
                        'documentNo' => $documentNo,
                        'documentType' => $documentType,
                        'documentValue' => $documentValue,
                        'nonInventoryItemTotal' => $nonInventoryItemTotal,
                        'documentCost' => $documentCost,
                        'totalTax' => $totalTax,
                        'qty' => $d['itemOutQty'],
                        'cusName' => $d['customerCode'].' - '.$d['customerName'],
                        'invoiceWiseTotalDiscount' => $invoiceWiseTotalDiscount,
                    ];

                } else if (isset($d['itemInDocumentType'])) {

                    $documentType = $d['itemInDocumentType'];
                    $documentNo = $d['creditNoteCode'];
                    $documentValue = $d['creditNoteTotal'];
                    $documentCost = $d['cNoteTotalCost'] - ($d['itemInQty'] * $d['itemInDiscount']);

                    $getCreditNoteTax = $this->CommonTable("Invoice\Model\CreditNoteProductTaxTable")->getCreditNoteProductTax($d['creditNoteID'], $d['itemInLocationProductID'], $d['locationID']);
                    $totalTax = 0;
                    foreach ($getCreditNoteTax as $t) {
                        $totalTax += $t['creditNoteTaxAmount'] / $t['creditNoteProductQuantity'];
                    }

                    $grossInvoiceCreditData['credit'][$d['creditNoteID']][] = [
                        'locationID' => $d['locationID'],
                        'locationName' => $d['locationName'],
                        'locationCode' => $d['locationCode'],
                        'date' => $date,
                        'documentNo' => $documentNo,
                        'documentType' => $documentType,
                        'documentValue' => $documentValue,
                        'nonInventoryItemTotal' => 0,
                        'documentCost' => $documentCost,
                        'totalTax' => $totalTax,
                        'qty' => $d['itemInQty'],
                        'cusName' => $d['customerCode'].' - '.$d['customerName']

                    ];
                }
            }
            
            $invoiceData = [];
            if ($grossInvoiceCreditData['invoice']) {
                foreach ($grossInvoiceCreditData['invoice'] as $iKey => $subData) {
                    $documentCost = 0;
                    $totalTax = 0;
                    $documentValue = 0;
                    $invoiceWiseTotalDiscount = 0;
                    foreach ($subData as $key => $value) {
                        
                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $nonInventoryItemTotal = $value['nonInventoryItemTotal'];
                        $documentValue += $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];
                        $invoiceWiseTotalDiscount = $value['invoiceWiseTotalDiscount'];
                    }
                    $invoiceData[$iKey] = ['locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentCost' => $documentCost,
                        'documentNo' => $documentNo,
                        'documentType' => $documentType,
                        'documentValue' => floatval($documentValue) - floatval($invoiceWiseTotalDiscount),
                        // 'documentValue' => $documentInvValue,
                        'nonInventoryItemTotal' => $nonInventoryItemTotal,
                        'totalTax' => $totalTax,
                        'cusName' => $value['cusName']
                        ];
                }
            }


            $creditNoteData = [];
            if ($grossInvoiceCreditData['credit']) {
                foreach ($grossInvoiceCreditData['credit'] as $cKey => $subData) {
                    $documentCost = 0;
                    $totalTax = 0;
                    foreach ($subData as $key => $value) {
                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $documentValue = $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];

                    }
                    //calculate creditNote wise total taxes
                    $totalTax = $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->getTotalCreditNoteProductTax($cKey)->current();

                    $creditNoteData[$cKey] = ['locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax['totalTax'],
                        'cusName' => $value['cusName']
                    ];
                }
            }

            // $grossProfitData = array_values($invoiceData + $creditNoteData);
            $grossProfitData = array_merge($invoiceData, $creditNoteData);

            
            //get jobcard related invoices
            $getJobCardInvoices = $this->CommonTable("Invoice\Model\InvoiceTable")->getJobCardInvoices($fromDate, $toDate, $cusCategories);
            foreach ($getJobCardInvoices as $key => $value) {
                $documentType = NULL;
                $documentNo = NULL;
                $documentValue = NULL;
                $documentCost = NULL;
                $jobCardID = '';
                $jobCardType = '';
                $date = '';

                if ($value['activityID'] != NULL) {
                    $jobCardID = $value['activityID'];
                    $jobCardType = 'Activity';
                } else if ($value['jobID'] != NULL) {
                    $jobCardID = $value['jobID'];
                    $jobCardType = 'Job';
                } else if ($value['projectID'] != NULL) {
                    $jobCardID = $value['projectID'];
                    $jobCardType = 'Project';
                }

                $jobCardInvoiceData = $this->CommonTable("Inventory\Model\ItemOutTable")->getInvoiceJobCardDetails($jobCardID, $jobCardType);
                if (!empty($jobCardInvoiceData)) {
                    foreach ($jobCardInvoiceData as $v) {
                        $documentType = $value['itemOutDocumentType'];
                        $documentNo = $value['salesInvoiceCode'];
                        $documentValue = $value['salesinvoiceTotalAmount'];
                        $documentCost = $v['totalInvoiceCost'] - ($v['itemInDiscount'] * $v['itemOutQty']);
                        $date = $value['salesInvoiceIssuedDate'];

                        $grnID = $v['itemInDocumentID'];
                        $locationProductID = $v['itemInLocationProductID'];
                        $getProductTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($grnID, $locationProductID);
                        $totalTax = 0;
                        foreach ($getProductTax as $t) {
                            $totalTax += $t['grnTaxAmount'] / $t['grnProductTotalQty'];
                        }

                        $grossInvoiceCreditData['jobCard'][$value['salesInvoiceID']][] = array(
                            'locationID' => $value['locationID'],
                            'locationName' => $value['locationName'],
                            'locationCode' => $value['locationCode'],
                            'date' => $date,
                            'documentNo' => $documentNo,
                            'documentType' => "Sales Invoice",
                            'documentValue' => $documentValue,
                            'documentCost' => $documentCost,
                            'totalTax' => $totalTax,
                            'qty' => $v['itemOutQty'],
                            'cusName' => $value['customerCode'].' - '.$value['customerName']
                            
                        );
                    }
                }
            }

            //get delivery note related invoices
            $getDeliveryNoteInvoices = $this->CommonTable("Invoice\Model\InvoiceTable")->getDeliveryNoteInvoices($fromDate, $toDate, $cusCategories);
            //for validate outvalues


            $outIds = [];
            foreach ($getDeliveryNoteInvoices as $key => $value) {
                $documentType = NULL;
                $documentNo = NULL;
                $documentValue = NULL;
                $documentCost = NULL;
                $jobCardID = '';
                $jobCardType = '';
                $date = '';
                $deliveryNoteID = ($value['salesInvoiceProductDocumentID'] != NULL) ? $value['salesInvoiceProductDocumentID'] : '';
                $deliveryNoteData = $this->CommonTable("Inventory\Model\ItemOutTable")->getInvoiceDeliveryNoteDetails($deliveryNoteID, $documentType = 'Delivery Note');
                if (!empty($deliveryNoteData)) {
                    foreach ($deliveryNoteData as $d) {
                            $outIds[] = $d['itemOutID'];
                            $checkProductInInvoice = $this->CommonTable("Invoice\Model\InvoiceProductTable")->gelSalesInvoiceProductDetailsByInvoiceIDAndLocationProductID($value['salesInvoiceID'], $d['itemOutLocationProductID'])->current();
                            if(isset($checkProductInInvoice['salesInvoiceProductID'])){
                                $documentType = $d['itemOutDocumentType'];
                                $documentNo = $value['salesInvoiceCode'];
                                $documentValue = (floatval($value['salesInvoiceProductTotal'])-floatval($value['saleInvoiceTotalTax']));
                                $documentCost = ($d['itemInPrice'] * $value['salesInvoiceProductQuantity']) - ($d['itemInDiscount'] * $d['itemOutQty']);
                                $date = $value['salesInvoiceIssuedDate'];
                                
                                $grnID = $d['itemInDocumentID'];
                                $locationProductID = $d['itemInLocationProductID'];
                                $getProductTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($grnID, $locationProductID);
                                $totalTax = 0;
                                foreach ($getProductTax as $t) {
                                    $totalTax += $t['grnTaxAmount'] / $t['grnProductTotalQty'];
                                }

                                $grossInvoiceCreditData['delivery'][$value['salesInvoiceID']][] = array(
                                    'locationID' => $value['locationID'],
                                    'locationName' => $value['locationName'],
                                    'locationCode' => $value['locationCode'],
                                    'date' => $date,
                                    'documentNo' => $documentNo,
                                    'documentType' => "Sales Invoice",
                                    'documentValue' => $documentValue,
                                    'documentCost' => $documentCost,
                                    'totalTax' => $totalTax,
                                    'qty' => $d['itemOutQty'],
                                    'cusName' => $value['customerCode'].' - '.$value['customerName']
                                );
                            }
                    }
                }
            }
           
            $count = count($grossProfitData);
            if ($grossInvoiceCreditData['jobCard']) {
                foreach ($grossInvoiceCreditData['jobCard'] as $subData) {
                    $documentCost = 0;
                    $totalTax = 0;
                    foreach ($subData as $key => $value) {
                        
                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $documentValue = $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];
                        $cusNamee = $value['customerCode'].' - '.$value['customerName'];
                    }
                    $grossProfitData[++$count] = [
                        'locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax,
                        'cusName' => $cusNamee
                    ];
                }
            }

            if ($grossInvoiceCreditData['delivery']) {
                foreach ($grossInvoiceCreditData['delivery'] as $subData) {
                                        
                    $documentCost = 0;
                    $totalTax = 0;
                    $documentValue = 0;
                    foreach ($subData as $key => $value) {
                        $locationID = $value['locationID'];
                        $locationName = $value['locationName'];
                        $locationCode = $value['locationCode'];
                        $date = $value['date'];
                        $documentValue += $value['documentValue'];
                        $documentCost += $value['documentCost'];
                        $documentType = $value['documentType'];
                        $documentNo = $value['documentNo'];
                        $totalTax += $value['totalTax'] * $value['qty'];
                        $cusName = $value['cusName'];
                    }
                    $grossProfitData[++$count] = [
                        'locationID' => $locationID,
                        'locationName' => $locationName,
                        'locationCode' => $locationCode,
                        'date' => $date,
                        'documentValue' => $documentValue,
                        'documentCost' => $documentCost,
                        'documentType' => $documentType,
                        'documentNo' => $documentNo,
                        'totalTax' => $totalTax,
                        'cusName' => $cusName

                    ];
                }
            }

            //sorting data on $grossProfitData array according to date element
            $sortData = [];
            foreach ($grossProfitData as $key => $r) {
                $sortData[$key] = ($r['date']);
            }
            //sort $grossProfitData array according to sorted $sortData array on 'date' element
            array_multisort($sortData, SORT_ASC, $grossProfitData);

            $arr = [];



            foreach ($grossProfitData as $element) {

                $documentValue = (isset($arr[$element['documentNo']])) ? $arr[$element['documentNo']]['documentValue'] + $element['documentValue'] : $element['documentValue'];
                
                $documentCost = (isset($arr[$element['documentNo']])) ? $arr[$element['documentNo']]['documentCost'] + $element['documentCost'] : $element['documentCost'];

                $arr[$element['documentNo']] = [
                    'locationID' => $element['locationID'],
                    'locationName' => $element['locationName'],
                    'locationCode' => $element['locationCode'],
                    'date' => $element['date'],
                    'documentValue' => $documentValue,
                    'documentCost' => $documentCost,
                    'documentType' => $element['documentType'],
                    'documentNo' => $element['documentNo'],
                    'totalTax' => $element['totalTax'],
                    'cusName' => $element['cusName'],
                ];
            }

            $locWiseGrossProfitData = array_fill_keys($locationList, '');
            foreach ($arr as $key => $v) {
                if (array_key_exists($v['locationID'], $locWiseGrossProfitData)) {
                    $locWiseGrossProfitData[$v['locationID']]['locGPD'][$key] = $v;
                    $locWiseGrossProfitData[$v['locationID']]['locationName'] = $v['locationName'];
                    $locWiseGrossProfitData[$v['locationID']]['locationCode'] = $v['locationCode'];
                }
            }

            return array_filter($locWiseGrossProfitData);
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @return html content
     */
    public function viewCreditNoteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');            
            $salesPersonIds = $request->getPost('salesPersonIds');
            $locationIds = $request->getPost('locationList');
            $cusCategories = $request->getPost('cusCategories');
            
            $creditNoteData = $this->getCreditNoteDetails($fromDate, $toDate, $locationIds, $salesPersonIds, $cusCategories);
            //set data to html file which given out put
            $translator = new Translator();
            $reportContentData['name'] = $translator->translate('Credit Note Report');
            $reportContentData['period'] = $fromDate . '-' . $toDate;
            $reportContentData['reportData'] = $creditNoteData;
            $reportContentData['htmlFilePath'] = 'reporting/sales-report/generate-credit-note-pdf';
            $creditNoteView = $this->reportHtmlContentPrepare($reportContentData);

            $this->html = $creditNoteView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @return path of PDF
     */
    public function generateCreditNotePdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $salesPersonIds = $request->getPost('salesPersonIds');
            $locationIds = $request->getPost('locationList');
            $cusCategories = $request->getPost('cusCategories');
            
            $creditNoteData = $this->getCreditNoteDetails($fromDate, $toDate, $locationIds, $salesPersonIds, $cusCategories);
            //set data to html file which given out put
            $translator = new Translator();
            $reportContentData['name'] = $translator->translate('Credit Note Report');
            $reportContentData['period'] = $fromDate . '-' . $toDate;
            $reportContentData['reportData'] = $creditNoteData;
            $reportContentData['htmlFilePath'] = 'reporting/sales-report/generate-credit-note-pdf';
            $creditNoteView = $this->reportHtmlContentPrepare($reportContentData);

            //puts html content to pdf
            $creditNoteView->setTerminal(TRUE);
            $htmlContent = $this->viewRendererHtmlContent($creditNoteView);
            $pdfPath = $this->downloadPDF('credit-note', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @return path of CSV
     */
    public function generateCreditNoteCsvAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $salesPersonIds = $request->getPost('salesPersonIds');
            $locationIds = $request->getPost('locationList');
            $cusCategories = $request->getPost('cusCategories');
            
            $creditNoteData = $this->getCreditNoteDetails($fromDate, $toDate, $locationIds, $salesPersonIds, $cusCategories);
            $companyDetails = $this->getCompanyDetails();

            //arrange data which related to CSV file
            $title = '';
            $tit = 'CREDIT NOTE DETAILS REPORT';
            $in = ["", $tit];
            $title = implode(",", $in) . "\n";

            $in = ['Report Generated: ' . date('Y-M-d h:i:s a')];
            $title.=implode(",", $in) . "\n";

            $title .= $companyDetails[0]->companyName . "\n";
            $title .= $companyDetails[0]->companyAddress . "\n";
            $title .= 'Tel: ' . $companyDetails[0]->telephoneNumber . "\n";

            $arrs = ["SALES REPRESENTATIVE", "CREDIT NOTE CODE", "CREDIT NOTE AMOUNT", "CREDIT NOTE DATE", "INVOICE CODE", "INVOICE AMOUNT", "CUSTOMER", "LOCATION"];
            $header = implode(",", $arrs);
            $arr = '';
            if (!empty($creditNoteData)) {                
                $totalCreditNoteAmount = 0;
                $totalInvoiceAmount = 0;
                foreach ($creditNoteData as $spData) {
                    $in = [
                        $spData['salesPersonName']
                    ];
                    $arr.=implode(",", $in) . "\n";
                    
                    foreach ($spData['data'] as $r) {
                        $totalCreditNoteAmount+=$r['creditNoteAmount'];
                        $totalInvoiceAmount+=$r['invoiceAmount'];
                        
                        $in = [
                            '',
                            $r['creditNoteCode'],
                            number_format($r['creditNoteAmount'], 2, '.', ''),
                            $r['creditNoteDate'],
                            $r['invoiceCode'],
                            number_format($r['invoiceAmount'], 2, '.', ''),
                            $r['customer'],
                            $r['location']
                        ];
                        $arr.=implode(",", $in) . "\n";                        
                    }
                }
                $in = [
                    '',
                    'CREDIT NOTE TOTAL',
                    number_format($totalCreditNoteAmount, 2, '.', ''),
                ];
                $arr.=implode(",", $in) . "\n";                
            } else {
                $arr = "No related data found\n";
            }
            
            //creates a CSV files by previous arranged array($arr)
            //and then given a path of CSV files which have creates
            $name = "credit_note_details";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }
    
    private function getCreditNoteDetails($fromDate, $toDate, $locationIds, $salesPersonIds, $cusCategories = NULL) 
    {
        $salesPersonData = [];        
        $withDefaultSalesPerson = (in_array('other', $salesPersonIds)) ? true : false;
        
        $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteDetailsBySalesPersonIds($fromDate, $toDate, $locationIds, $salesPersonIds, $withDefaultSalesPerson, $cusCategories);
        foreach ($creditNoteDetails as $creditNote) {
            $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($creditNote['salesInvoiceID']);

            $numOfSP = (sizeof($relatedSalesPersons) == 0) ?  1 : sizeof($relatedSalesPersons);

            $salesPersonId = (!empty($creditNote['salesPersonID'])) ? $creditNote['salesPersonID'] : 'other';
            if (!array_key_exists($salesPersonId, $salesPersonData)) {
                $salesPersonName = ($salesPersonId == 'other') ? 'Other' : $creditNote['salesPersonSortName']. ' ' .$creditNote['salesPersonLastName'];
                $salesPersonData[$salesPersonId] = [
                    'salesPersonName' => $salesPersonName
                ];
                $salesPersonData[$salesPersonId]['cusCategory'] =  ($cusCategories) ? 1 : null;
            }
            
            $salesPersonData[$salesPersonId]['data'][] = [
                'creditNoteCode' => $creditNote['creditNoteCode'],
                'creditNoteAmount' => round(( floatval($creditNote['creditNoteTotal']) / $numOfSP), 2),
                'creditNoteDate' => $creditNote['creditNoteDate'],
                'invoiceCode' => $creditNote['salesInvoiceCode'],
                'invoiceAmount' => round((floatval($creditNote['salesinvoiceTotalAmount']) / $numOfSP), 2),
                'customer' => $creditNote['customerName'] . '[' . $creditNote['customerCode']. ']',
                'location' => $creditNote['locationName'],
                'cusCategory' => $creditNote['customerCategoryName'],
                'directCreditNoteFlag' => $creditNote['creditNoteDirectFlag']
            ];            
        }  

        return $salesPersonData;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * Generate JSON Object from given data
     * @return \Zend\View\Model\JsonModel
     */
    public function getAuditTrailDetailsAction()
    {
        $this->setLogMessage("Audit Trail Report viewed");
        $request = $this->getRequest();

        if ($request->isPost()) {

            $user_name = $request->getPost('user_name');
            $from_date = $request->getPost('from_date');
            $to_date = $request->getPost('to_date');

            /**
             * @author Malitta <malitta@thinkcube.com>
             * Refactored previous code
             */
            $data = NULL;

            if ($user_name != null) {
                $res = $this->CommonTable('User\Model\UserTable')->getUserByUsername($user_name);
                $data = $this->CommonTable('User\Model\LoggerTable')->getAuditTrailDetails($res->userID, $from_date, $to_date);
            } else {
                $data = $this->CommonTable('User\Model\LoggerTable')->getAuditTrailDetails(NULL, $from_date, $to_date);
            }

            // if no data is found
            if (empty($data)) {
                $nodata[0] = 'nodata';
                return new JsonModel($nodata);
            }

            // if data is found, prepare array with user details and send as json
            $usersCache = array();
            foreach ($data as $key => $log) {
                $logs[$key] = $log;

                // to avoid unnecessary DB calls, cache result
                if (isset($usersCache[$log->userID])) {
                    $logs[$key]->userDetails = $usersCache[$log->userID];
                } else {
                    $logs[$key]->userDetails = $usersCache[$log->userID] = $this->CommonTable('User\Model\UserTable')->getUser($log->userID);
                }
            }

            return new JsonModel($logs);
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * Generate Audit Trail Excel Sheet
     * @return Excel Sheet
     */
    public function generateAuditTrailExcelAction()
    {
        $this->setLogMessage("Audit Trail Excel sheet generated");
        $request = $this->getRequest()->getQuery();

        if ($request != null) {

            $user_name = $request['user_name'];
            $from_date = $request['from_date'];
            $to_date = $request['to_date'];

            /**
             * @author Malitta <malitta@thinkcube.com>
             * Refactored previous code
             */
            $data = NULL;

            if ($user_name != null) {
                $res = $this->CommonTable('User\Model\UserTable')->getUserByUsername($user_name);
                $data = $this->CommonTable('User\Model\LoggerTable')->getAuditTrailDetails($res->userID, $from_date, $to_date);
            } else {
                $data = $this->CommonTable('User\Model\LoggerTable')->getAuditTrailDetails(NULL, $from_date, $to_date);
            }

            // if no data is found
            if (empty($data)) {
                $nodata[0] = 'nodata';
                return new JsonModel($nodata);
            }

            // if data is found, prepare array with user details and send as json
            $usersCache = array();
            foreach ($data as $key => $log) {
                $logs[$key] = $log;

                // to avoid unnecessary DB calls, cache result
                if (isset($usersCache[$log->userID])) {
                    $logs[$key]->userDetails = $usersCache[$log->userID];
                } else {
                    $logs[$key]->userDetails = $usersCache[$log->userID] = $this->CommonTable('User\Model\UserTable')->getUser($log->userID);
                }
            }

            $arrs = array("DATE AND TIME", "USER", "TRANSACTION TYPE", "REF. NO");
            $header = implode(",", $arrs);
            $arr = '';

            if ($logs != null) {
                foreach ($logs as $data) {
                    if (sizeof($data) == 0)
                        $arr = "no matching records found\n";
                    else {
                        $in = array(
                            0 => $data->timeStamp,
                            1 => $data->userDetails->username,
                            2 => $data->logTitle,
                            3 => $data->refID
                        );
                        $arr.=implode(",", $in) . "\n";
                    }
                }
            }

            $name = "Audit_trail_report.xls";
            $size = "4GB";
            $size_f = strlen($header . "\n" . $arr);
            $this->excelHeader($name, $size, $size_f, $title, $header, $arr);
        }
    }

    public function getMonthlySalesDetailsBySalesPerson($fromMonth = NULL, $toMonth = NULL, $locationID = NULL, $customerCategory = NULL, $salesPersonIds = NULL)
    {
        if (isset($fromMonth) && isset($toMonth)) {
            $withDefaultSalesPerson = (in_array('other', $salesPersonIds)) ? true : false;
            $respondedAmount = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesDataBySalesPersonID($fromMonth, $toMonth, $locationID, $customerCategory,$salesPersonIds, $withDefaultSalesPerson);
            $respondTax = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesTaxDetailsBySalesPersonID($fromMonth, $toMonth, $locationID, $customerCategory,$salesPersonIds, $withDefaultSalesPerson);

            //get all suspended taxes
            $simpleTaxDetails = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
            $suspendedTaxes = [];
            foreach ($simpleTaxDetails as $key => $value) {
                if ( $value->taxSuspendable == 1 ) {
                    $suspendedTaxes[$value->id] = $value->id;
                }
            }

            $taxData = [];

            $tempArray = [];
            // set monthly invoice details
            $respondAmount = [];
            foreach ($respondedAmount as $key => $resAmo) {
                if ($resAmo['salesPersonID'] != "0") {
                    $salesPersonID = (is_null($resAmo['salesPersonID'])) ? 'other' : $resAmo['salesPersonID'];
                    $respondAmount[$salesPersonID][$resAmo['MonthWithYear']]['Month'] = $resAmo['Month'];
                    $respondAmount[$salesPersonID][$resAmo['MonthWithYear']]['MonthWithYear'] = $resAmo['MonthWithYear'];   
                    $respondAmount[$salesPersonID][$resAmo['MonthWithYear']]['Year'] = $resAmo['Year'];   
                    $respondAmount[$salesPersonID][$resAmo['MonthWithYear']]['TotalAmount'] += (is_null($resAmo['salesPersonID'])) ? $resAmo['TotalAmount'] : $resAmo['relatedSalesAmount'];   
                    $respondAmount[$salesPersonID][$resAmo['MonthWithYear']]['salesPersonID'] = (is_null($resAmo['salesPersonID'])) ? 'other' : $resAmo['salesPersonID'];
                    $respondAmount[$salesPersonID][$resAmo['MonthWithYear']]['salesPersonSortName'] = (is_null($resAmo['salesPersonSortName'])) ? 'Others' : $resAmo['salesPersonSortName'];
                    $respondAmount[$salesPersonID][$resAmo['MonthWithYear']]['salesPersonLastName'] = (is_null($resAmo['salesPersonLastName'])) ? '-' : $resAmo['salesPersonLastName'];
                    $respondAmount[$salesPersonID][$resAmo['MonthWithYear']]['deleted'] = $resAmo['deleted'];
                }
            }

            $resDetails = [];
            foreach ($respondAmount as $key => $value) {
                foreach ($value as $ke => $val) {
                    $resDetails[$key."_".$val['MonthWithYear']] = $val;
                }
            }
            
            foreach ($respondTax as $key => $value) {
                if ($value['salesPersonID'] != "0") {
                    $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($value['salesInvoiceID']);
                    $numOfSP = (is_null($value['salesPersonID'])) ? 1 : sizeof($relatedSalesPersons);
                    $salesPersonID = (is_null($value['salesPersonID'])) ? 'other' : $value['salesPersonID'];
                    $tempArray[$salesPersonID][$value['MonthWithYear']]['Month'] = $value['Month'];   
                    $tempArray[$salesPersonID][$value['MonthWithYear']]['MonthWithYear'] = $value['MonthWithYear'];   
                    $tempArray[$salesPersonID][$value['MonthWithYear']]['Year'] = $value['Year'];   
                    $tempArray[$salesPersonID][$value['MonthWithYear']]['salesInvoiceProductID'] = $value['salesInvoiceProductID'];
                    if ($value['salesInvoiceSuspendedTax'] == '1' && array_key_exists($value['taxID'], $suspendedTaxes)) {
                        $tempArray[$salesPersonID][$value['MonthWithYear']]['suspendedTax'] += round((floatval($value['salesInvoiceProductTaxAmount']) / $numOfSP), 2);
                    } else {
                        if ($value['taxType'] == "v") {
                            $tempArray[$salesPersonID][$value['MonthWithYear']]['normalTax'] += round((floatval($value['salesInvoiceProductTaxAmount']) / $numOfSP),2);
                            $tempArray[$salesPersonID][$value['MonthWithYear']]['compoundTax'] += 0;   
                        } else if ($value['taxType'] == "c"){
                            $tempArray[$salesPersonID][$value['MonthWithYear']]['compoundTax'] += round((floatval($value['salesInvoiceProductTaxAmount']) / $numOfSP),2);;
                            $tempArray[$salesPersonID][$value['MonthWithYear']]['normalTax'] += 0;   
                        }
                    }

                    $tempArray[$salesPersonID][$value['MonthWithYear']]['deleted'] = $value['deleted'];   
                    $tempArray[$salesPersonID][$value['MonthWithYear']]['salesPersonID'] = $salesPersonID;   
                }
            }

            foreach ($tempArray as $key => $value) {
                foreach ($value as $ke => $val) {
                    $taxData[$key.'_'.$val['MonthWithYear']] = $val;
                }
            }
            
            //get credit note total values without tax
            $respondedCreditNote = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesCreditNoteDataBySalesPersonID($fromMonth, $toMonth, $locationID, $customerCategory,$salesPersonIds,$withDefaultSalesPerson);
            $resCredData = [];
            foreach ($respondedCreditNote as $key => $resCreAmo) {
                if ($resCreAmo['salesPersonID'] != "0") {
                    $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($resCreAmo['salesInvoiceID']);
                    $numOfSP = (is_null($resCreAmo['salesPersonID'])) ? 1 : sizeof($relatedSalesPersons);
                    $salesPersonID = (is_null($resCreAmo['salesPersonID'])) ? 'other' : $resCreAmo['salesPersonID'];
                    $resCredData[$salesPersonID][$resCreAmo['MonthWithYear']]['Month'] = $resCreAmo['Month'];
                    $resCredData[$salesPersonID][$resCreAmo['MonthWithYear']]['MonthWithYear'] = $resCreAmo['MonthWithYear'];   
                    $resCredData[$salesPersonID][$resCreAmo['MonthWithYear']]['Year'] = $resCreAmo['Year'];   
                    $resCredData[$salesPersonID][$resCreAmo['MonthWithYear']]['creditNoteProductTotal'] += round(($resCreAmo['creditNoteProductTotal'] / $numOfSP), 2);
                    $resCredData[$salesPersonID][$resCreAmo['MonthWithYear']]['salesPersonID'] = $salesPersonID;
                    $resCredData[$salesPersonID][$resCreAmo['MonthWithYear']]['salesPersonSortName'] = (is_null($resCreAmo['salesPersonSortName'])) ? 'Others' : $resCreAmo['salesPersonSortName'];
                    $resCredData[$salesPersonID][$resCreAmo['MonthWithYear']]['salesPersonLastName'] = $resCreAmo['salesPersonLastName'];
                    $resCredData[$salesPersonID][$resCreAmo['MonthWithYear']]['deleted'] = $resCreAmo['deleted'];
                }
            }

            $respondCreditNote = [];
            foreach ($resCredData as $key => $value) {
                foreach ($value as $ke => $val) {
                    $respondCreditNote[$key.'_'.$val['MonthWithYear']] = $val;
                }
            }

            //get credit note tax details
            $respondCreditNoteTax = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesCreditNoteTaxDataBySalesPersonID($fromMonth, $toMonth, $locationID, $customerCategory,$salesPersonIds,$withDefaultSalesPerson);

            $cnTaxData = [];
            $cnTempArray = []; 
            
            foreach ($respondCreditNoteTax as $key => $value) {
                if ($value['salesPersonID'] != "0") {
                    $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($value['salesInvoiceID']);
                    $numOfSP = (is_null($value['salesPersonID'])) ? 1 : sizeof($relatedSalesPersons);
                    $salesPersonID = (is_null($value['salesPersonID'])) ? 'other' : $value['salesPersonID'];
                    $cnTempArray[$salesPersonID][$value['MonthWithYear']]['Month'] = $value['Month'];   
                    $cnTempArray[$salesPersonID][$value['MonthWithYear']]['MonthWithYear'] = $value['MonthWithYear'];   
                    $cnTempArray[$salesPersonID][$value['MonthWithYear']]['Year'] = $value['Year'];   
                    if ($value['salesInvoiceSuspendedTax'] == '1' && array_key_exists($value['id'], $suspendedTaxes)) {
                        $cnTempArray[$salesPersonID][$value['MonthWithYear']]['cnSuspendedTax'] += round((floatval($value['creditNoteTaxAmount']) / $numOfSP), 2);
                    } else {
                        if ($value['taxType'] == "v") {
                            $cnTempArray[$salesPersonID][$value['MonthWithYear']]['cnNormalTax'] += round((floatval($value['creditNoteTaxAmount']) / $numOfSP), 2);
                        } else if ($value['taxType'] == "c"){
                            $cnTempArray[$salesPersonID][$value['MonthWithYear']]['cnCompoundTax'] += round((floatval($value['creditNoteTaxAmount']) / $numOfSP), 2);
                        }
                    }   
                    $cnTempArray[$salesPersonID][$value['MonthWithYear']]['deleted'] = $value['deleted'];   
                    $cnTempArray[$salesPersonID][$value['MonthWithYear']]['salesPersonID'] = $salesPersonID;   
                }
            }
            foreach ($cnTempArray as $key => $value) {
                foreach ($value as $ke => $val) {
                    $cnTaxData[$key.'_'.$val['MonthWithYear']] = $val;
                }
            }

            $arrayCounter = 0;
            $commonArray = [];
            for ($i=0; $i < 4; $i++) { 
                if ($i == 0 && sizeof($resDetails) > 0) {
                    $commonArray[$arrayCounter] = $resDetails;
                    $arrayCounter++;
                } else if ($i == 1 && sizeof($taxData) > 0) {
                    $commonArray[$arrayCounter] = $taxData;
                    $arrayCounter++;
                } else if ($i == 2 && sizeof($respondCreditNote) > 0) {
                    $commonArray[$arrayCounter] = $respondCreditNote;
                    $arrayCounter++;
                } else if ($i == 3 && sizeof($cnTaxData) > 0) {
                    $commonArray[$arrayCounter] = $cnTaxData;
                    $arrayCounter++;
                }
            }

            $recArrayCount = 0;
            $respondtwo = $commonArray[0];
            for ($i=0; $i < sizeof($commonArray) ; $i++) { 
                if ($i != 0 ) {
                    $respondtwo = array_replace_recursive($respondtwo, $commonArray[$i]);
                }
            }
            
            $respond = [];
            foreach ($respondtwo as $key => $value) {
                $respond[] = $value;
            }

            $respondYears = $this->CommonTable('Invoice\Model\InvoiceTable')->
                    getMonthlySalesDataYears($fromMonth, $toMonth, $locationID, $customerCategory);            
            $months = $this->CommonTable('Invoice\Model\InvoiceTable')->getMonthsYearBetweenTwoDates($fromMonth, $toMonth);
            $salesPerson = $this->CommonTable('User\Model\SalesPersonTable')->getSalesPersonByIds($salesPersonIds);
            $monthlySalesDetails = array();
            $years = $respondYears;

            foreach ($salesPerson as $sP) {
                foreach ($months as $month) {
                    $temp = explode(' ', $month);
                    $monthlySalesDetails[$sP['salesPersonSortName']][$temp[1]][$temp[0]] = array('Month' => $temp[0], 'MonthWithYear' => $month, 'Year' => $temp[1], 'TotalTax' => 0, 'TotalAmount' => 0);
                }
            }
            
            if ($withDefaultSalesPerson) {
                foreach ($months as $month) {
                    $temp = explode(' ', $month);
                    $monthlySalesDetails['Others'][$temp[1]][$temp[0]] = array('Month' => $temp[0], 'MonthWithYear' => $month, 'Year' => $temp[1], 'TotalTax' => 0, 'TotalAmount' => 0);
                }
            }
            
            foreach ($respond as $res) {
                $monthlySalesDetails[$res['salesPersonSortName']][$res['Year']][$res['Month']] = $res;
            }
            return $monthlySalesDetails;
        }
    }

}
