<?php
namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\I18n\Translator\Translator;

class BudgetReportController extends CoreController
{

    public function budgetReportViewAction()
    {

    	if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
           
        $fiscalPeriodID = $postData['fiscalPeriodID'];
        $accountIDs = $postData['accountIDs'];
        $dimensionType = $postData['dimensionType'];
        $dimensionValue = $postData['dimensionValue'];
        $accountType = $postData['accountType'];

        if ($accountType == "0" || $accountType == null) {
            $financeAccountTypes = [];
        } else if ($accountType == "pandlac") {
            $financeAccountTypes = [4,6,7,3];
        } else if ($accountType == "balanceshtac") {
            $financeAccountTypes = [1,2,5];
        }
        $budgetData = $this->getService('BudgetReportService')->getBudgetData($fiscalPeriodID, $accountIDs, $dimensionType, $dimensionValue, $financeAccountTypes);
        $cD = $this->getCompanyDetails();
        
        $translator = new Translator();
        $name = $translator->translate('Budget Analysis Report');
        $period = $budgetData['fiscalPeriodData'];
        
        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $budgetDataView = new ViewModel(array(
            'budgetData' => $budgetData,
            'fiscalPeriod' => $budgetData['fiscalPeriodData'],
            'headerTemplate' => $headerViewRender,
            'cD' => $companyDetails)
        );
        
        $budgetDataView->setTemplate('reporting/budget-report/generate-budget-view');

        $this->html = $budgetDataView;
        $this->status = true;
        return $this->JSONRespondHtml();
    }


    public function generateBudgetReportPdfAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
           
        $fiscalPeriodID = $postData['fiscalPeriodID'];
        $accountIDs = $postData['accountIDs'];
        $dimensionType = $postData['dimensionType'];
        $dimensionValue = $postData['dimensionValue'];
        $accountType = $postData['accountType'];

        if ($accountType == "0" || $accountType == null) {
            $financeAccountTypes = [];
        } else if ($accountType == "pandlac") {
            $financeAccountTypes = [4,6,7,3];
        } else if ($accountType == "balanceshtac") {
            $financeAccountTypes = [1,2,5];
        }
        $budgetData = $this->getService('BudgetReportService')->getBudgetData($fiscalPeriodID, $accountIDs, $dimensionType, $dimensionValue, $financeAccountTypes);
        
        $cD = $this->getCompanyDetails();
        $translator = new Translator();
        $name = $translator->translate('Budget Analysis Report');
        $period = $budgetData['fiscalPeriodData'];

        $headerView = $this->headerViewTemplate($name, $period);
        $headerView->setTemplate('reporting/template/headerTemplate');
        $headerViewRender = $this->htmlRender($headerView);

        $budgetDataView = new ViewModel(array(
            'budgetData' => $budgetData,
            'fiscalPeriod' => $budgetData['fiscalPeriodData'],
            'headerTemplate' => $headerViewRender,
            'cD' => $companyDetails)
        );

        $budgetDataView->setTemplate('reporting/budget-report/generate-budget-view');
        $budgetDataView->setTerminal(true);

        // get rendered view into variable
        $htmlContent = $this->viewRendererHtmlContent($budgetDataView);
        $pdfPath = $this->downloadPDF('budget-analysis-report', $htmlContent, true, true);
        
        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function budgetReportSheetAction() 
    {
        $this->status = false;
        $this->msg = 'invalid request';
        $this->html = '';

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost()->toArray();
           
            $fiscalPeriodID = $postData['fiscalPeriodID'];
            $accountIDs = $postData['accountIDs'];
            $dimensionType = $postData['dimensionType'];
            $dimensionValue = $postData['dimensionValue'];
            $accountType = $postData['accountType'];

            if ($accountType == "0" || $accountType == null) {
                $financeAccountTypes = [];
            } else if ($accountType == "pandlac") {
                $financeAccountTypes = [4,6,7,3];
            } else if ($accountType == "balanceshtac") {
                $financeAccountTypes = [1,2,5];
            }
            
            $budgetData = $this->getService('BudgetReportService')->getBudgetData($fiscalPeriodID, $accountIDs, $dimensionType, $dimensionValue, $financeAccountTypes);

            if ($budgetData['budgetData']) {
                $period = $budgetData['fiscalPeriodData'];
                $companyDetails = $this->getCompanyDetails();
                $data = array(
                    "Budget Analysis Report"
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $data) . "\n";

                $data = array(
                    $companyDetails[0]->companyName,
                    $companyDetails[0]->companyAddress,
                    "Tel: " . $companyDetails[0]->telephoneNumber,
                    "Period : " . $period
                );

                $title.=implode(",", $data) . "\n";

                $data = array(
                    "Period :" . $period
                );
                $title.=implode(",", $data) . "\n";


                $tableHead = array('Account Name');
                foreach ($budgetData['fiscalPeriodColoumnData'] as $value) {
                    array_push($tableHead,"");
                    array_push($tableHead,$value);
                } 
                $header.=implode(",", $tableHead) . "\n";

                $tableHead = array(" ");
                foreach ($budgetData['fiscalPeriodColoumnData'] as $value) {
                    array_push($tableHead,"Actual Value");
                    array_push($tableHead,"Budget Value");
                }

                $header.=implode(",", $tableHead) . "\n";

                foreach ($budgetData['budgetData'] as $key => $bValue) { 
                    $data1 = array(strtoupper($key));
                    $arr.=implode(",", $data1) . "\n";     

                    foreach ($bValue as $val) { 
                        $data2 = array($val['financeAccountsName']);          

                        foreach ($budgetData['fiscalPeriodColoumnData'] as $fKey => $fVal) { 
                            if ($val[$fKey]['budgetValue'] >= 0) {
                                array_push($data2, floatval($val[$fKey]['currentDebit']));
                            } else {
                                $currentCredit = floatval($val[$fKey]['currentCredit']) * -1;
                                array_push($data2, $currentCredit);
                            }

                            array_push($data2, floatval($val[$fKey]['budgetValue']));
                        }
                        $arr.=implode(",", $data2) . "\n";     
                    }  
                }
            } else {
                $arr = "no matching records found";
            }
            $name = "budget_analysis_report.csv";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }
}
