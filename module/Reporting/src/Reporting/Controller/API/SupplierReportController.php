<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains supplier Report related controller functions
 */

namespace Reporting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\I18n\Translator\Translator;

class SupplierReportController extends CoreController
{

    protected $userRoleID;
    protected $allLocations;
    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'inve_report_upper_menu';

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param Array $supplierIds
     * @return $supLists
     */
    private function _getSupplierDetails($supplierIds = null, $isAllSuppliers = false)
    {
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        if (isset($supplierIds) || $isAllSuppliers) {
            $supData = array_fill_keys($supplierIds, '');
            $paymentVoucherData = $this->CommonTable('Inventory\Model\SupplierTable')->supplierPaymentVoucherDetails($supplierIds, $isAllSuppliers);
            $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->supplierDebitNoteDetails($supplierIds, $isAllSuppliers);
            $supplierData = array_merge_recursive($paymentVoucherData, $debitNoteData);

            foreach ($supplierData as $key => $row) {
                $supData[$row['supplierID']][$key] = $row;
            }

            return $supData;
        }
    }

    public function viewAgedSupplierDataAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $supplierIds = $request->getPost("supplierIds");
            $endDate = $request->getPost("toDate");
            $isAllSuppliers = $request->getPost("isAllSuppliers");
            $advanceRow = $request->getPost('advancedColoumn');
            $translator = new Translator();
            $name = $translator->translate('Supplier Aged Analysis Report');
            $period = $endDate;
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }
            if($advanceDetails){
                $invoiceAgedSupplierData = $this->getAdvanceAgedSupplierDetails($supplierIds, $endDate, $isAllSuppliers);
            } else {
                $invoiceAgedSupplierData = $this->getAgedSupplierDetails($supplierIds, $endDate, $isAllSuppliers);
            }
                        
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateEndDate');
            $headerViewRender = $this->htmlRender($headerView);

            $agedSupplierView = new ViewModel(array(
                'aSD' => $invoiceAgedSupplierData,
                'endDate' => $endDate,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'advanceDetails' => $advanceDetails,
            ));
            $agedSupplierView->setTemplate('reporting/supplier-report/generate-aged-supplier-pdf');

            $this->html = $agedSupplierView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateAgedSupplierDataPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIds = $request->getPost("supplierIds");
            $endDate = $request->getPost("toDate");
            $isAllSuppliers = $request->getPost("isAllSuppliers");
            $advanceRow = $request->getPost('advancedColoumn');
            $translator = new Translator();
            $name = $translator->translate('Supplier Aged Analysis Report');
            $period = $endDate;
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }

            if($advanceDetails){
                $invoiceAgedSupplierData = $this->getAdvanceAgedSupplierDetails($supplierIds, $endDate, $isAllSuppliers);
            } else {
                $invoiceAgedSupplierData = $this->getAgedSupplierDetails($supplierIds, $endDate, $isAllSuppliers);
            }

            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period);
            $headerView->setTemplate('reporting/template/headerTemplateEndDate');
            $headerViewRender = $this->htmlRender($headerView);

            $agedSupplierView = new ViewModel(array(
                'aSD' => $invoiceAgedSupplierData,
                'endDate' => $endDate,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
                'advanceDetails' => $advanceDetails,
            ));

            $agedSupplierView->setTemplate('reporting/supplier-report/generate-aged-supplier-pdf');
            $agedSupplierView->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($agedSupplierView);
            $pdfPath = $this->downloadPDF('supplier-aged-analysis-report', $htmlContent, true);

            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateAgedSupplierDataSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
           $supplierIds = $request->getPost("supplierIds");
            $endDate = $request->getPost("toDate");
            $isAllSuppliers = $request->getPost("isAllSuppliers");
            $advanceRow = $request->getPost('advancedColoumn');
            $translator = new Translator();
            $name = $translator->translate('Supplier Aged Analysis Report');
            $period = $endDate;
            $advanceDetails = false;
            if($advanceRow == 'true'){
                $advanceDetails = true;
            }

            if($advanceDetails){
                $invoiceAgedSupplierData = $this->getAdvanceAgedSupplierDetails($supplierIds, $endDate, $isAllSuppliers);
            } else {
                $invoiceAgedSupplierData = $this->getAgedSupplierDetails($supplierIds, $endDate, $isAllSuppliers);
            }
            $cD = $this->getCompanyDetails();
            if ($invoiceAgedSupplierData) {
                $title = '';
                $tit = 'SUPPLIER AGED ANALYSIS REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => $tit,
                    4 => "",
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => "End Date",
                    4 => $endDate,
                    5 => "",
                    6 => "",
                    7 => "",
                    8 => ""
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-m-d h:i:s a')
                );

                $title.=implode(",", $in) . "\n";
                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                if($advanceDetails){
                    $arrs = array("SUPPLIER NAME", "SUPPLIER CODE", "0 - 1 DAYS","1 - 30 DAYS", "31 - 60 DAYS","61 - 90 DAYS", "91 - 180 DAYS","181 - 270 DAYS", "271 - 365 DAYS", "> 365", "TOTAL");
                } else {
                    $arrs = array("SUPPLIER NAME", "SUPPLIER CODE", "WITHIN 7 DAYS", "WITHIN 14 DAYS", "WITHIN 30 DAYS", "OVER 30 DAYS", "TOTAL");
                    
                }
                    

                $header = implode(",", $arrs);
                $arr = '';
                $str = array();

                if (sizeof($invoiceAgedSupplierData) == 0) {
                    $in = "no matching records found in Invoice";
                    $arr.=implode(",", $in) . "\n";
                } else {
                    $currentTotal = 0.00;
                    $withIn7Total = 0.00;
                    $withIn14Total = 0.00;
                    $withIn30Total = 0.00;
                    $withIn60Total = 0.00;
                    $withIn90Total = 0.00;
                    $withIn180Total = 0.00;
                    $withIn270Total = 0.00;
                    $withIn365Total = 0.00;
                    $over365Total = 0.00;
                    $over30Total = 0.00;
                    $invTotal = 0.00;
                    foreach ($invoiceAgedSupplierData as $a) {
                        if (!empty($a)) {
                            $refNo = $a['purchaseInvoiceCode'];
                            $date = $a['purchaseInvoiceIssueDate'];
                            $withIn7 = $a['withIn7Days'];
                            $withIn7 = $withIn7 ? $withIn7 : 0;
                            $withIn7Total += $withIn7;
                            $withIn14 = $a['withIn14Days'];
                            $withIn14 = $withIn14 ? $withIn14 : 0;
                            $withIn14Total += $withIn14;
                            $withIn30 = $a['withIn30Days'];
                            $withIn30 = $withIn30 ? $withIn30 : 0;
                            $withIn30Total += $withIn30;
                            if($advanceDetails){
                                $current = $a['current'];
                                $current = $current ? $current : 0;
                                $currentTotal += $current;

                                $withIn60 = $a['withIn60Days'];
                                $withIn60 = $withIn60 ? $withIn60 : 0;
                                $withIn60Total += $withIn60;

                                $withIn90 = $a['withIn90Days'];
                                $withIn90 = $withIn90 ? $withIn90 : 0;
                                $withIn90Total += $withIn90;

                                $withIn180 = $a['withIn180Days'];
                                $withIn180 = $withIn180 ? $withIn180 : 0;
                                $withIn180Total += $withIn180;

                                $withIn270 = $a['withIn270Days'];
                                $withIn270 = $withIn270 ? $withIn270 : 0;
                                $withIn270Total += $withIn270;

                                $withIn365 = $a['withIn365Days'];
                                $withIn365 = $withIn365 ? $withIn365 : 0;
                                $withIn365Total += $withIn365;

                                $over365 = $a['over365'];
                                $over365 = $over365 ? $over365 : 0;
                                $over365Total += $over365;
                                $Total = $current + $withIn30 + $withIn60 + $withIn90 + $withIn180 + $withIn270 + $withIn365 + $over365;
                            } else {
                                $over30 = $a['over30'];
                                $over30 = $over30 ? $over30 : 0;
                                $over30Total += $over30;
                                $Total = $withIn7 + $withIn14 + $withIn30 + $over30;
                                
                            }

                            $invTotal += $Total;
                            if ($Total > 1) {
                                if($advanceDetails){
                                    $in = array(
                                    0 => str_replace(",", " ", $a['sT'] . ' ' . $a['sN']),
                                    1 => $a['supCD'],
                                    2 => $current,
                                    3 => $withIn30,
                                    4 => $withIn60,
                                    5 => $withIn90,
                                    6 => $withIn180,
                                    7 => $withIn270,
                                    8 => $withIn365,
                                    9 => $over365,
                                    10 => $Total
                                );    
                                } else {
                                    $in = array(
                                        0 => str_replace(",", " ", $a['sT'] . ' ' . $a['sN']),
                                        1 => $a['supCD'],
                                        2 => $withIn7,
                                        3 => $withIn14,
                                        4 => $withIn30,
                                        5 => $over30,
                                        6 => $Total
                                    );
                                    
                                }
                                $arr.=implode(",", $in) . "\n";
                            }
                                
                            
                        }
                    }
                    
                    if($advanceDetails){
                        $in = array(
                        0 => "",
                        1 => "Total Purchase Invoice",
                        2 => $currentTotal,
                        3 => $withIn30Total,
                        4 => $withIn60Total,
                        5 => $withIn90Total,
                        6 => $withIn180Total,
                        7 => $withIn270Total,
                        8 => $withIn365Total,
                        9 => $over365Total,
                        10 => $invTotal
                    );    
                    } else {
                        $in = array(
                            0 => "",
                            1 => "Total Purchase Invoice",
                            2 => $withIn7Total,
                            3 => $withIn14Total,
                            4 => $withIn30Total,
                            5 => $over30Total,
                            6 => $invTotal
                        );
                        
                    }
                        
                    
                    $arr.=implode(",", $in) . "\n";
                }
            } else {
                $arr = 'no matching records found\n';
            }
        }
        $name = "Supplier-aged-analysis";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function getAdvanceAgedSupplierDetails($supplierIds = null, $endDate = null, $isAllSuppliers = false)
    {
        
        if (isset($supplierIds) && isset($endDate)) {
            $supplierDetails = array();
            $agedSupplierDeatailsByPiTable = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getAgedSupplierPiAdvanceData($supplierIds, $endDate, $groupBy = false, $isAllSuppliers);

            //get payment voucher details
            $paymentVoucherSupOutstanding = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getAgedSupplierPaymentVoucherAdvanceData($supplierIds, $endDate, $isAllSuppliers);
            
            $processedAdvancePiData = $this->processPiToAdvanceAges($agedSupplierDeatailsByPiTable, $endDate);
            $processedAdvancePvData = $this->processPvToAdvanceAges($paymentVoucherSupOutstanding, $endDate);

            foreach ($processedAdvancePiData as $c) {
                $debitNotes = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPiDebitNoteTotalByPiId($c['purchaseInvoiceID']);
                if($debitNotes){
                    $current = is_null($c['Current']) ? 0 : $c['Current'] - $debitNotes['total'];
                    $withIn30Days = is_null($c['WithIn30Days']) ? 0 : $c['WithIn30Days'] - $debitNotes['total'];
                    $withIn60Days = is_null($c['WithIn60Days']) ? 0 : $c['WithIn60Days'] - $debitNotes['total'];
                    $withIn90Days = is_null($c['WithIn90Days']) ? 0 : $c['WithIn90Days'] - $debitNotes['total'];
                    $withIn180Days = is_null($c['WithIn180Days']) ? 0 : $c['WithIn180Days'] - $debitNotes['total'];
                    $withIn270Days = is_null($c['WithIn270Days']) ? 0 : $c['WithIn270Days'] - $debitNotes['total'];
                    $withIn365Days = is_null($c['WithIn365Days']) ? 0 : $c['WithIn365Days'] - $debitNotes['total'];
                    $over365 = is_null($c['Over365']) ? 0 : $c['Over365'] - $debitNotes['total'] ;
                }

                //if first time of supplierID coming from $supplierDetails array
                if (empty($supplierDetails[$c['purchaseInvoiceSupplierID']])) {
                    $currentTotal = 0;
                    $withIn30DaysTotal = 0;
                    $withIn60DaysTotal = 0;
                    $withIn90DaysTotal = 0;
                    $withIn180DaysTotal = 0;
                    $withIn270DaysTotal = 0;
                    $withIn365DaysTotal = 0;
                    $over365Total = 0;
                } else {
                    //if already have supplierID related data on $supplierDetails array
                    //get previous value from $supplierDetails array to calculations
                    $currentTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['current'];
                    $withIn30DaysTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn30Days'];
                    $withIn60DaysTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn60Days'];
                    $withIn90DaysTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn90Days'];
                    $withIn180DaysTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn180Days'];
                    $withIn270DaysTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn270Days'];
                    $withIn365DaysTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn365Days'];
                    $over365Total = $supplierDetails[$c['purchaseInvoiceSupplierID']]['over365'];
                }
                    

                $currentTotal += $current;
                $withIn30DaysTotal += $withIn30Days;
                $withIn60DaysTotal += $withIn60Days;
                $withIn90DaysTotal += $withIn90Days;
                $withIn180DaysTotal += $withIn180Days;
                $withIn270DaysTotal += $withIn270Days;
                $withIn365DaysTotal += $withIn365Days;
                $over365Total += $over365;

                
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['sT'] = $c['supplierTitle'];
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['sN'] = $c['supplierName'];
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['supCD'] = $c['supplierCode'];
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['current'] = $currentTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn30Days'] = $withIn30DaysTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn60Days'] = $withIn60DaysTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn90Days'] = $withIn90DaysTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn180Days'] = $withIn180DaysTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn270Days'] = $withIn270DaysTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn365Days'] = $withIn365DaysTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['over365'] = $over365Total;

            }

            //add paymentVoucher details
            foreach ($processedAdvancePvData as $key => $outstanding) {
                                    
                if (empty($supplierDetails[$outstanding['paymentVoucherSupplierID']])) {
                    
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['current'] = is_null($outstanding['Current']) ? 0 : $outstanding['Current'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn30Days'] = is_null($outstanding['WithIn30Days']) ? 0 : $outstanding['WithIn30Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn60Days'] = is_null($outstanding['WithIn60Days']) ? 0 : $outstanding['WithIn60Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn90Days'] = is_null($outstanding['WithIn90Days']) ? 0 : $outstanding['WithIn90Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn180Days'] = is_null($outstanding['WithIn180Days']) ? 0 : $outstanding['WithIn180Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn270Days'] = is_null($outstanding['WithIn270Days']) ? 0 : $outstanding['WithIn270Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn365Days'] = is_null($outstanding['WithIn365Days']) ? 0 : $outstanding['WithIn365Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['over365'] = is_null($outstanding['Over365']) ? 0 : $outstanding['Over365'];
                    
                } else {

                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['current'] += is_null($outstanding['Current']) ? 0 : $outstanding['Current'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn30Days'] += is_null($outstanding['WithIn30Days']) ? 0 : $outstanding['WithIn30Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn60Days'] += is_null($outstanding['WithIn60Days']) ? 0 : $outstanding['WithIn60Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn90Days'] += is_null($outstanding['WithIn90Days']) ? 0 : $outstanding['WithIn90Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn180Days'] += is_null($outstanding['WithIn180Days']) ? 0 : $outstanding['WithIn180Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn270Days'] += is_null($outstanding['WithIn270Days']) ? 0 : $outstanding['WithIn270Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn365Days'] += is_null($outstanding['WithIn365Days']) ? 0 : $outstanding['WithIn365Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['over365'] += is_null($outstanding['Over365']) ? 0 : $outstanding['Over365'];
                    
                }

                $supplierDetails[$outstanding['paymentVoucherSupplierID']]['sT'] = $outstanding['supplierTitle'];
                $supplierDetails[$outstanding['paymentVoucherSupplierID']]['sN'] = $outstanding['supplierName'];
                $supplierDetails[$outstanding['paymentVoucherSupplierID']]['supCD'] = $outstanding['supplierCode'];
                $supplierDetails[$outstanding['paymentVoucherSupplierID']]['customerCategoryName'] = $outstanding['customerCategoryName'];
            }
            
            return $supplierDetails;
        }
    }


    public function getAgedSupplierDetails($supplierIds = null, $endDate = null, $isAllSuppliers = false)
    {
        if (isset($supplierIds) && isset($endDate)) {
            $supplierDetails = array();

            $agedSupplierDeatailsByPiTable = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getAgedSupplierPiData($supplierIds, $endDate, $groupBy = false, $isAllSuppliers);

            //get payment voucher details
            $paymentVoucherSupOutstanding = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getAgedSupplierPaymentVoucherData($supplierIds, $endDate, $isAllSuppliers);

            $processedPiData = $this->processPiToAges($agedSupplierDeatailsByPiTable, $endDate);
            $processedPvData = $this->processPvToAges($paymentVoucherSupOutstanding, $endDate);
        
            foreach ($processedPiData as $c) {
                $debitNotes = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPiDebitNoteTotalByPiId($c['purchaseInvoiceID']);
                if($debitNotes){
                    $current = is_null($c['Current']) ? 0 : $c['Current'];
                    if (is_null($c['Current']) && is_null($c['WithIn7Days'])) {
                        $withIn7Days = 0;
                    } else if (!is_null($c['Current']) && is_null($c['WithIn7Days'])) {
                        $withIn7Days = floatval($current) - $debitNotes['total'];
                    } else {
                        $withIn7Days = $c['WithIn7Days'] - $debitNotes['total'] + floatval($current);
                    }
                    $withIn14Days = is_null($c['WithIn14Days']) ? 0 : $c['WithIn14Days'] - $debitNotes['total'];
                    $withIn30Days = is_null($c['WithIn30Days']) ? 0 : $c['WithIn30Days'] - $debitNotes['total'];
                    $over30 = is_null($c['Over30']) ? 0 : $c['Over30'] - $debitNotes['total'] ;
                }
                
                if (empty($supplierDetails[$c['purchaseInvoiceSupplierID']])) {
                    $withIn7DaysTotal = 0;
                    $withIn14DaysTotal = 0;
                    $withIn30DaysTotal = 0;
                    $over30Total = 0;
                } else {
                    //if already have supplierID related data on $supplierDetails array
                    //get previous value from $supplierDetails array to calculations

                    $withIn7DaysTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn7Days'];
                    $withIn14DaysTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn14Days'];
                    $withIn30DaysTotal = $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn30Days'];
                    $over30Total = $supplierDetails[$c['purchaseInvoiceSupplierID']]['over30'];
                }
                    

                $withIn7DaysTotal += $withIn7Days;
                $withIn14DaysTotal += $withIn14Days;
                $withIn30DaysTotal += $withIn30Days;
                $over30Total += $over30;
                
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['sT'] = $c['supplierTitle'];
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['sN'] = $c['supplierName'];
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['supCD'] = $c['supplierCode'];
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn7Days'] = $withIn7DaysTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn14Days'] = $withIn14DaysTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['withIn30Days'] = $withIn30DaysTotal;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['over30'] = $over30Total;
                $supplierDetails[$c['purchaseInvoiceSupplierID']]['customerCategoryName'] = $c['customerCategoryName'];
                
            }
            //add paymentVoucher details
            foreach ($processedPvData as $key => $outstanding) {
                                
                if (empty($supplierDetails[$outstanding['paymentVoucherSupplierID']])) {
                    $current = is_null($outstanding['Current']) ? 0 : $outstanding['Current'];
                    if (is_null($outstanding['Current']) && is_null($outstanding['WithIn7Days'])) {
                        $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn7Days'] = 0;
                    } else if (!is_null($outstanding['Current']) && is_null($outstanding['WithIn7Days'])) {
                        $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn7Days'] = floatval($current);
                    } else {
                        $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn7Days'] = floatval($current) + $outstanding['WithIn7Days'];
                    }
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn14Days'] = is_null($outstanding['WithIn14Days']) ? 0 : $outstanding['WithIn14Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn30Days'] = is_null($outstanding['WithIn30Days']) ? 0 : $outstanding['WithIn30Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['over30'] = is_null($outstanding['Over30']) ? 0 : $outstanding['Over30'];
                    
                } else {
                    $current = is_null($outstanding['Current']) ? 0 : $outstanding['Current'];
                    if (is_null($outstanding['Current']) && is_null($outstanding['WithIn7Days'])) {
                        $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn7Days'] += 0;
                    } else if (!is_null($outstanding['Current']) && is_null($outstanding['WithIn7Days'])) {
                        $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn7Days'] += floatval($current);
                    } else {
                        $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn7Days'] += floatval($current) + $outstanding['WithIn7Days'];
                    }
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn14Days'] += is_null($outstanding['WithIn14Days']) ? 0 : $outstanding['WithIn14Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['withIn30Days'] += is_null($outstanding['WithIn30Days']) ? 0 : $outstanding['WithIn30Days'];
                    $supplierDetails[$outstanding['paymentVoucherSupplierID']]['over30'] += is_null($outstanding['Over30']) ? 0 : $outstanding['Over30'];
                    
                }

                $supplierDetails[$outstanding['paymentVoucherSupplierID']]['sT'] = $outstanding['supplierTitle'];
                $supplierDetails[$outstanding['paymentVoucherSupplierID']]['sN'] = $outstanding['supplierName'];
                $supplierDetails[$outstanding['paymentVoucherSupplierID']]['supCD'] = $outstanding['supplierCode'];
                $supplierDetails[$outstanding['paymentVoucherSupplierID']]['customerCategoryName'] = $outstanding['customerCategoryName'];
            }
            return $supplierDetails;
        }
    }


    public function processPiToAges($dataSet, $endDate) {
        $data = [];
        foreach ($dataSet as $key => $value) {
            $piID = $value['purchaseInvoiceID'];
            $payament = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentsDetailsByPurchaseInvoiceIDForAging($piID, $endDate)->current();

            $paidAmount = (!empty($payament['paymentTotal'])) ? $payament['paymentTotal'] : 0;

            $end = strtotime($endDate);
            $invDate = strtotime($value['purchaseInvoiceIssueDate']);

            $diff = $end - $invDate;

            $days = round($diff / (60 * 60 * 24));

            if (floatval($paidAmount) < floatval($value['purchaseInvoiceTotal'])) {

                $value['Current'] = NULL;
                $value['WithIn7Days'] = NULL;
                $value['WithIn14Days'] = NULL;
                $value['WithIn30Days'] = NULL;
                $value['Over30'] = NULL;

                if ($days == 0) {
                    $value['Current'] =   floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);                   

                } elseif ($days > 0 && $days <= 7) {
                    $value['WithIn7Days'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);
                } elseif ($days > 7 && $days <= 14) {
                    $value['WithIn14Days'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);
                } elseif ($days > 14 && $days <= 30) {
                    $value['WithIn30Days'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);
                } else {
                    $value['Over30'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);

                }

                $data[] = $value;
            }

        }
           
        return $data;

    }

    public function processPiToAdvanceAges($dataSet, $endDate) {
        $data = [];
        foreach ($dataSet as $key => $value) {
            $piID = $value['purchaseInvoiceID'];
            $payament = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentsDetailsByPurchaseInvoiceIDForAging($piID, $endDate)->current();

            $paidAmount = (!empty($payament['paymentTotal'])) ? $payament['paymentTotal'] : 0;

            $end = strtotime($endDate);
            $invDate = strtotime($value['purchaseInvoiceIssueDate']);

            $diff = $end - $invDate;

            $days = round($diff / (60 * 60 * 24));

            if (floatval($paidAmount) < floatval($value['purchaseInvoiceTotal'])) {

                $value['Current'] = NULL;
                $value['WithIn30Days'] = NULL;
                $value['WithIn60Days'] = NULL;
                $value['WithIn90Days'] = NULL;
                $value['WithIn180Days'] = NULL;
                $value['WithIn270Days'] = NULL;
                $value['WithIn365Days'] = NULL;
                $value['Over365'] = NULL;

                if ($days == 0) {
                    $value['Current'] =   floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);                   
                } elseif ($days > 0 && $days <= 30) {
                    $value['WithIn30Days'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);
                } elseif ($days > 30 && $days <= 60) {
                    $value['WithIn60Days'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);
                } elseif ($days > 60 && $days <= 90) {
                    $value['WithIn90Days'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);
                } elseif ($days > 90 && $days <= 180) {
                    $value['WithIn180Days'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);
                } elseif ($days > 180 && $days <= 270) {
                    $value['WithIn270Days'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);
                } elseif ($days > 270 && $days <= 365) {
                    $value['WithIn365Days'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);
                } else {
                    $value['Over365'] = floatval($value['purchaseInvoiceTotal']) - floatval($paidAmount);

                }

                $data[] = $value;
            }

        }

           
        return $data;

    }

    public function processPvToAges($dataSet, $endDate) {
        $data = [];
        foreach ($dataSet as $key => $value) {

            $pvID = $value['paymentVoucherID'];
            $payament = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentsDetailsByPaymentVaoucherIDForAging($pvID, $endDate)->current();

            $paidAmount = (!empty($payament['paymentTotal'])) ? $payament['paymentTotal'] : 0;

            $end = strtotime($endDate);
            $invDate = strtotime($value['paymentVoucherIssuedDate']);

            $diff = $end - $invDate;

            $days = round($diff / (60 * 60 * 24));

            if (floatval($paidAmount) < floatval($value['paymentVoucherTotal'])) {

                $value['Current'] = NULL;
                $value['WithIn7Days'] = NULL;
                $value['WithIn14Days'] = NULL;
                $value['WithIn30Days'] = NULL;
                $value['Over30'] = NULL;


                if ($days == 0) {
                    $value['Current'] =   floatval($value['paymentVoucherTotal']) - floatval($paidAmount);                   

                } elseif ($days > 0 && $days <= 7) {
                    $value['WithIn7Days'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);
                } elseif ($days > 7 && $days <= 14) {
                    $value['WithIn14Days'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);
                } elseif ($days > 14 && $days <= 30) {
                    $value['WithIn30Days'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);
                } else {
                    $value['Over30'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);

                }

                $data[] = $value;
            }

        }
       
        return $data;

    }

    public function processPvToAdvanceAges($dataSet, $endDate) {
        $data = [];
        foreach ($dataSet as $key => $value) {

            $pvID = $value['paymentVoucherID'];
            $payament = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentsDetailsByPaymentVaoucherIDForAging($pvID, $endDate)->current();

            $paidAmount = (!empty($payament['paymentTotal'])) ? $payament['paymentTotal'] : 0;

            $end = strtotime($endDate);
            $invDate = strtotime($value['paymentVoucherIssuedDate']);

            $diff = $end - $invDate;

            $days = round($diff / (60 * 60 * 24));

            if (floatval($paidAmount) < floatval($value['paymentVoucherTotal'])) {

                $value['Current'] = NULL;
                $value['WithIn30Days'] = NULL;
                $value['WithIn60Days'] = NULL;
                $value['WithIn90Days'] = NULL;
                $value['WithIn180Days'] = NULL;
                $value['WithIn270Days'] = NULL;
                $value['WithIn365Days'] = NULL;
                $value['Over365'] = NULL;

                if ($days == 0) {
                    $value['Current'] =   floatval($value['paymentVoucherTotal']) - floatval($paidAmount);                   
                } elseif ($days > 0 && $days <= 30) {
                    $value['WithIn30Days'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);
                } elseif ($days > 30 && $days <= 60) {
                    $value['WithIn60Days'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);
                } elseif ($days > 60 && $days <= 90) {
                    $value['WithIn90Days'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);
                } elseif ($days > 90 && $days <= 180) {
                    $value['WithIn180Days'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);
                } elseif ($days > 180 && $days <= 270) {
                    $value['WithIn270Days'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);
                } elseif ($days > 270 && $days <= 365) {
                    $value['WithIn365Days'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);
                } else {
                    $value['Over365'] = floatval($value['paymentVoucherTotal']) - floatval($paidAmount);

                }

                $data[] = $value;
            }

        }


        return $data;

    }


    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return JSONRespondHtml
     */
    public function viewSupplierDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIds = $request->getPost('supplierIds');
            //if suplierIds empty when assign all supplierIds to that array
            if (empty($supplierIds)) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
                foreach ($supplierData as $s) {
                    $supplierIds[$s['supplierID']] = $s['supplierID'];
                }
            }
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $supLists = $this->_getSupplierDetails($supplierIds, $isAllSuppliers);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Supplier Details Report');

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $sDetailsView = new ViewModel(array(
                'sL' => $supLists,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));
            $sDetailsView->setTemplate('reporting/supplier-report/generate-supplier-details-pdf');

            $this->setLogMessage("Retrive suppliers details for supplier deatails view report.");
            $this->html = $sDetailsView;
            $this->msg = $this->getMessage('INFO_SUPP_REPORT_SUPPDATA');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * return CSV file
     */
    public function generateSupplierDetailsSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIDs = $request->getPost('supplierIds');
            //if suplierIds empty when assign all supplierIds to that array
            if (empty($supplierIDs)) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
                foreach ($supplierData as $s) {
                    $supplierIDs[$s['supplierID']] = $s['supplierID'];
                }
            }
            $isAllSuppliers = $request->getPost('isAllSuppliers');

            $cD = $this->getCompanyDetails();
            $supLists = $this->_getSupplierDetails($supplierIDs, $isAllSuppliers);

            if ($supLists) {
                $title = '';
                $tit = 'SUPPLIER DETAILS REPORT';
                $in = array(
                    0 => "",
                    1 => "",
                    2 => "",
                    3 => $tit,
                    4 => "",
                    5 => "",
                    6 => ""
                );

                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a')
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => ''
                );

                $title.=implode(",", $in) . "\n";

                $arrs = array("NO", "SUPPLIER CODE", "SUPPLIER NAME", "TELE: NO", "TOT. PURCHASES", "ADDRESS");
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;
                foreach ($supLists as $key => $data) {
                    $netPaymentVoucher = 0;
                    $netDebitNote = 0;
                    foreach ($data as $value) {
                        $totPaymentVoucher = $value['totPaymentVoucher'] ? $value['totPaymentVoucher'] : 0.00;
                        $netPaymentVoucher += $totPaymentVoucher;
                        $totDebitNote = $value['totDebitNote'] ? $value['totDebitNote'] : 0.00;
                        $netDebitNote += $totDebitNote;
                        $adrs = $value['supplierAddress'] ? $value['supplierAddress'] : '-';
                        $tNo = $value['supplierTelephoneNumber'] ? $value['supplierTelephoneNumber'] : '-';
                    }
                    $in = array(
                        0 => $i,
                        1 => "{$value['supplierCode']}",
                        2 => $value['supplierTitle'] . ' ' . $value['supplierName'],
                        3 => $tNo,
                        4 => $netPaymentVoucher - $netDebitNote,
                        5 => "{$adrs}",
                    );
                    $arr.=implode(",", $in) . "\n";
                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }
        }

        $name = "supplier_details_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->setLogMessage("Retrive suppliers details for supplier deatails CSV report.");
        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return $pdf
     */
    public function generateSupplierDetailsPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIDs = $request->getPost('supplierIds');
            //if suplierIds empty when assign all supplierIds to that array
            if (empty($supplierIDs)) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
                foreach ($supplierData as $s) {
                    $supplierIDs[$s['supplierID']] = $s['supplierID'];
                }
            }
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $supLists = $this->_getSupplierDetails($supplierIDs, $isAllSuppliers);
            $companyDetails = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Supplier Details Report');

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);


            $viewSupplierDetails = new ViewModel(array(
                'sL' => $supLists, 'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));

            $viewSupplierDetails->setTemplate('reporting/supplier-report/generate-supplier-details-pdf');
            $viewSupplierDetails->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewSupplierDetails);
            $pdfPath = $this->downloadPDF('supplier-details-report', $htmlContent, true);

            $this->setLogMessage("Retrive suppliers details for supplier deatails PDF report.");
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    private function _getSupplierWiseItemDetails($supplierIds = null, $isAllSuppliers = false)
    {
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        if (isset($supplierIds) || $isAllSuppliers) {
            $supLists = array_fill_keys($supplierIds, '');

            $resSuppliers = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierNameList($supplierIds, $isAllSuppliers);
            foreach ($resSuppliers as $row) {
                $supLists[$row['sID']]['sName'] = $row['sName'];
                $supLists[$row['sID']]['sTitle'] = $row['sTitle'];
                $supLists[$row['sID']]['sCD'] = $row['sCD'];
                $supLists[$row['sID']]['products'] = '';
            }

            $paymentVoucherData = $this->CommonTable('Inventory\Model\SupplierTable')->supplierWisePaymentVoucherItemDetails($supplierIds, $isAllSuppliers);
            $debitNoteData = $this->CommonTable('Inventory\Model\SupplierTable')->supplierWiseDebitNoteItemDetails($supplierIds, $isAllSuppliers);
            $supllierData = array_merge_recursive($paymentVoucherData, $debitNoteData);

            foreach ($supllierData as $key => $row) {
                $supLists[$row['sID']]['products'][$row['pID']][$key] = $row;
                $resProductUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomByProductID($row['pID']);
                foreach ($resProductUom as $value) {
                    if ($value['productUomConversion'] == 1) {
                        $supLists[$row['sID']]['products'][$row['pID']][$key]['uomAbbr'] = $value['uomAbbr'];
                        break;
                    }
                }
            }

            return $supLists;
        }
    }

    public function supplierWiseItemDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIDs = $request->getPost('supplierIds');
            //if suplierIds empty when assign all supplierIds to that array
            if (empty($supplierIDs)) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
                foreach ($supplierData as $s) {
                    $supplierIDs[$s['supplierID']] = $s['supplierID'];
                }
            }
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $translator = new Translator();
            $name = $translator->translate('Supplier Wise Item Report');

            $supLists = $this->_getSupplierWiseItemDetails($supplierIDs, $isAllSuppliers);
            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $sWiseItemDetailsView = new ViewModel(array(
                'sL' => $supLists,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));
            $sWiseItemDetailsView->setTemplate('reporting/supplier-report/generate-supplier-wise-item-pdf');

            $this->setLogMessage("Retrive suppliers wise items details for supplier wise item deatails view report.");
            $this->html = $sWiseItemDetailsView;
            $this->msg = $this->getMessage('INFO_SUPP_REPORT_SUPPITEM_DATA');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }


    public function generateSupplierWiseItemSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIDs = $request->getPost('supplierIds');
            //if suplierIds empty when assign all supplierIds to that array
            if (empty($supplierIDs)) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
                foreach ($supplierData as $s) {
                    $supplierIDs[$s['supplierID']] = $s['supplierID'];
                }
            }
            $isAllSuppliers = $request->getPost('isAllSuppliers');

            $supLists = $this->_getSupplierWiseItemDetails($supplierIDs, $isAllSuppliers);
            $cD = $this->getCompanyDetails();

            if ($supLists) {
                $title = '';
                $tit = 'SUPPLIER WISE ITEM REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                    3 => "",
                );

                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a'),
                    1 => '',
                    2 => '',
                    3 => '',
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                $arrs = array("NO", "SUPPLIER NAME - CODE", "ITEM NAME - CODE", "QUANTITY");
                $header = implode(",", $arrs);
                $arr = '';
                $i = 1;

                foreach ($supLists as $key => $value) {
                    $count = 1;
                    if ($value['products'] != '') {
                        foreach ($value['products'] as $subkey => $pData) {
                            $totQty = 0;
                            $totPurchase = 0;
                            $totDebitNote = 0;
                            foreach ($pData as $v) {
                                $pName = $v['pName'] ? $v['pName'] : '-';
                                $pCD = $v['pCD'] ? $v['pCD'] : '-';
                                $totPurchaseQty = $v['totPurchaseQty'] ? $v['totPurchaseQty'] : 0.00;
                                $totPurchase += $totPurchaseQty;
                                $totDebitNoteQty = $v['totDebitNoteQty'] ? $v['totDebitNoteQty'] : 0.00;
                                $totDebitNote += $totDebitNoteQty;
                                $uomAbbr = $v['uomAbbr'] ? $v['uomAbbr'] : ' ';
                            }
                            $totQty = $totPurchase - $totDebitNote;
                            if ($totQty > 0) {
                                if ($count == 1) {
                                    $in = array(
                                        0 => $i,
                                        1 => $value['sTitle'] . ' ' . $value['sName'] . ' - ' . $value['sCD'],
                                        2 => $pName . ' - ' . $pCD,
                                        3 => $totQty . ' ' . $uomAbbr
                                    );
                                    $arr.=implode(",", $in) . "\n";
                                    $count++;
                                } else {
                                    $in = array(
                                        0 => '',
                                        1 => '',
                                        2 => $pName . ' - ' . $pCD,
                                        3 => $totQty . ' ' . $uomAbbr
                                    );
                                    $arr.=implode(",", $in) . "\n";
                                }
                            }
                        }
                    } else {
                        $in = array(
                            0 => $i,
                            1 => $value['sTitle'] . ' ' . $value['sName'] . ' - ' . $value['sCD'],
                            2 => '-',
                            3 => '-',
                        );
                        $arr.=implode(",", $in) . "\n";
                    }

                    $i++;
                }
            } else {
                $arr = "no matching records found\n";
            }

            $name = "supplier_wise_item_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->setLogMessage("Retrive suppliers wise items details for supplier wise item deatails CSV report.");
            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function generateSupplierWiseItemPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIDs = $request->getPost('supplierIds');
            //if suplierIds empty when assign all supplierIds to that array
            if (empty($supplierIDs)) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
                foreach ($supplierData as $s) {
                    $supplierIDs[$s['supplierID']] = $s['supplierID'];
                }
            }
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $supLists = $this->_getSupplierWiseItemDetails($supplierIDs, $isAllSuppliers);
            $cD = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Supplier Wise Item Report');

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewSupplierWiseItem = new ViewModel(array(
                'sL' => $supLists, 'cD' => $cD,
                'headerTemplate' => $headerViewRender,
            ));

            $viewSupplierWiseItem->setTemplate('reporting/supplier-report/generate-supplier-wise-item-pdf');
            $viewSupplierWiseItem->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewSupplierWiseItem);
            $pdfPath = $this->downloadPDF('supplier-wise-item-report', $htmlContent, true);

            $this->setLogMessage("Retrive suppliers wise items details for supplier wise item deatails PDF report.");
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function supplierWiseTransactionAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $supplierIds = $request->getPost('supplierIds');
        $fromDate = $request->getPost('fromDate');
        $toDate = $request->getPost('toDate');
        $summaryOnly = $request->getPost('summaryData');

        //if suplierIds empty when assign all supplierIds to that array
        if (empty($supplierIds)) {
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
            foreach ($supplierData as $s) {
                $supplierIds[$s['supplierID']] = $s['supplierID'];
            }
        }
        $isAllSuppliers = $request->getPost('isAllSuppliers');
        $allTransactionDetails = $this->getSupplierWiseAllTranssactions($supplierIds, '0000-00-00', $toDate);
        $transactionDetails = $this->getSupplierWiseAllTranssactions($supplierIds, $fromDate, $toDate);
        $openningBalance = array(
            'all' => $allTransactionDetails['outstanding'],
            'current' => $transactionDetails['outstanding']
            );

        $companyDetails = $this->getCompanyDetails();
        $translator = new Translator();
        $name = $translator->translate('Supplier All Transaction Report');
        $headerView = $this->headerViewTemplate($name, $period = NULL);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $sDetailsView = new ViewModel(array(
            'supDetails' => $transactionDetails,
            'openningBalance' => $openningBalance,
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
            'parentDataSet' => $allTransactionDetails['parentDataSet'],
            'summaryOnly' => $summaryOnly
        ));
        $sDetailsView->setTemplate('reporting/supplier-report/generate-supplier-all-transaction-details');

        $this->setLogMessage("Retrive suppliers details for supplier deatails view report.");
        $this->html = $sDetailsView;
        $this->msg = $this->getMessage('INFO_SUPP_REPORT_SUPPDATA');
        $this->status = true;
        return $this->JSONRespondHtml();

    }
    public function generateSupplierWiseTransactionPdfAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $supplierIds = $request->getPost('supplierIds');
        $fromDate = $request->getPost('fromDate');
        $toDate = $request->getPost('toDate');
        $summaryOnly = $request->getPost('summaryData');
        //if suplierIds empty when assign all supplierIds to that array
        if (empty($supplierIds)) {
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
            foreach ($supplierData as $s) {
                $supplierIds[$s['supplierID']] = $s['supplierID'];
            }
        }
        $isAllSuppliers = $request->getPost('isAllSuppliers');
        $allTransactionDetails = $this->getSupplierWiseAllTranssactions($supplierIds, '0000-00-00', $toDate);
        $transactionDetails = $this->getSupplierWiseAllTranssactions($supplierIds, $fromDate, $toDate);
        $openningBalance = array(
            'all' => $allTransactionDetails['outstanding'],
            'current' => $transactionDetails['outstanding']
        );
        $companyDetails = $this->getCompanyDetails();
        $translator = new Translator();
        $name = $translator->translate('Supplier All Transaction Report');

        $headerView = $this->headerViewTemplate($name, $period = NULL);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $viewSupplierWiseItem = new ViewModel(array(
            'supDetails' => $transactionDetails,
            'openningBalance' => $openningBalance,
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
            'parentDataSet' => $allTransactionDetails['parentDataSet'],
            'summaryOnly' => $summaryOnly
        ));
        $viewSupplierWiseItem->setTemplate('reporting/supplier-report/generate-supplier-all-transaction-details');
        $viewSupplierWiseItem->setTerminal(true);

        // get rendered view into variable
        $htmlContent = $this->viewRendererHtmlContent($viewSupplierWiseItem);
        $pdfPath = $this->downloadPDF('supplier-wise-transaction-report', $htmlContent, true);

        $this->setLogMessage("Retrive suppliers wise items details for supplier wise item deatails PDF report.");
        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function generateSupplierTransactionSheetAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $supplierIds = $request->getPost('supplierIds');
        $fromDate = $request->getPost('fromDate');
        $toDate = $request->getPost('toDate');
        $summaryOnly = $request->getPost('summaryData');
        //if suplierIds empty when assign all supplierIds to that array
        if (empty($supplierIds)) {
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
            foreach ($supplierData as $s) {
                $supplierIds[$s['supplierID']] = $s['supplierID'];
            }
        }
        $isAllSuppliers = $request->getPost('isAllSuppliers');
        $allTransactionDetails = $this->getSupplierWiseAllTranssactions($supplierIds, '0000-00-00', $toDate);
        $transactionDetails = $this->getSupplierWiseAllTranssactions($supplierIds, $fromDate, $toDate);
        $openningBalance = array(
            'all' => $allTransactionDetails['outstanding'],
            'current' => $transactionDetails['outstanding']
        );
        $parentDataSet = $allTransactionDetails['parentDataSet'];
        $cD = $this->getCompanyDetails();

        if (count($transactionDetails['docDetails']) != 0) {
            $title = '';
            $tit = 'SUPPLIER ALL TRANSACTION REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
                3 => "",
            );

            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => 'Report Generated: ' . date('Y-M-d h:i:s a'),
                1 => '',
                2 => '',
                3 => '',
            );
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => $cD[0]->companyName,
                1 => $cD[0]->companyAddress,
                2 => 'Tel: ' . $cD[0]->telephoneNumber
            );
            $title.=implode(",", $in) . "\n";
            if($summaryOnly != 'true'){
                $arrs = array("SUPPLIER NAME", "DOCUMENT TYPE", "DOCUMENT CODE", "DATE", "STATUS", "PARENT DOC", "AMOUNT", "CUMULATIVE TOTAL");
                $header = implode(",", $arrs);
                
            } else {
                $arrs = array("SUPPLIER NAME", "OPENNING BALANCE", "CUMULATIVE TOTAL");
                $header = implode(",", $arrs);
            }
            $arr = '';

            foreach ($transactionDetails['supDetails'] as $supKey => $supData) {
                $openningBalanceVal = floatval($openningBalance['all'][$supKey]) - floatval($openningBalance['current'][$supKey]);
                $realOpnBlnce = $openningBalanceVal;
                if($summaryOnly != 'true'){
                    $in = array(
                        0 => $supData['sName'].' - '.$supData['sCD'],
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => 'opening Balance',
                        7 => $openningBalanceVal
                    );
                    $arr.=implode(",", $in) . "\n";
                    
                }
                foreach ($transactionDetails['docDetails'] as $key => $transValue){
                    if($transValue['supplier'] == $supKey){
                        $parentDoc = "";
                        if($transValue['docType'] == 'payments'){

                            if(isset($parentDataSet[$transValue['code']]['PIs'] )){

                                $parentDoc = 'PI / '.implode(" - ", $parentDataSet[$transValue['code']]['PIs']);

                            }

                            if(isset($parentDataSet[$transValue['code']]['PVs'] )){

                                $parentDoc .= ' PV / '.implode(" - ", $parentDataSet[$transValue['code']]['PVs']);
                            }
                        }else{
                            $parentDoc = $parentDataSet[$transValue['code']];
                        }

                        if($transValue['docType'] == 'payment Voucher' ||
                            $transValue['docType'] == 'purchase Invoice' ||
                                $transValue['docType'] == 'debit Note Payment') {

                            $openningBalanceVal += $transValue['total'];
                            
                        } else if($transValue['docType'] == 'debit Note'){
                            $openningBalanceVal -= $transValue['total'];
                           
                        } else if($transValue['type'] == null){
                            $openningBalanceVal -= ($transValue['total'] - $transValue['totalCreditAmount']);
                            
                        } else {

                            $openningBalanceVal -= $transValue['total'];
                            
                        }
                        if($summaryOnly != 'true'){
                            $in = array(
                                0 => $supData['sName'].' - '.$supData['sCD'],
                                1 => $transValue['docType'],
                                2 => $transValue['code'],
                                3 => $transValue['issueDate'],
                                4 => $transValue['statusName'],
                                5 => $parentDoc,
                                6 => $transValue['total'],
                                7 => $openningBalanceVal
                            );
                            $arr.=implode(",", $in) . "\n";
                            
                        }


                    }

                }
                if($summaryOnly == 'true'){
                    $in = array(
                        0 => $supData['sName'].' - '.$supData['sCD'],
                        1 => $realOpnBlnce,
                        2 => $openningBalanceVal
                    );
                    $arr.=implode(",", $in) . "\n";
                            
                }

                $grandTotal += $openningBalanceVal;
            }
            if($summaryOnly == 'true'){
                    $in = array(
                        0 => '',
                        1 => 'Grand Total',
                        2 => $grandTotal
                    );
                    $arr.=implode(",", $in) . "\n";
                            
            } else {
                $in = array(
                        0 => '',
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => '',
                        6 => 'Grand Total',
                        7 => $grandTotal
                    );
                    $arr.=implode(",", $in) . "\n";
                
            }            
        } else {
            $arr = "no matching records found\n";
        }

        $name = "supplier_all_transaction_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->setLogMessage("Retrive suppliers wise items details for supplier wise item deatails CSV report.");
        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    private function getSupplierWiseAllTranssactions($supplierIDs = [], $fromDate = null, $toDate = null)
    {
        $status = ['3','4','6'];
        //get all purchaseInvoices
        $purchaseInvoiceDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);
        // get all grn details
        $grnDetails = $this->CommonTable('Inventory\Model\GrnTable')->getGrnBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);
        // get all po details
        $poDetails = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrderBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);
        // get all paymentVoucher details
        $pVDetails = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchersBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);
        // get all payment details
        $paymentDetails = $this->CommonTable('SupplierPaymentsTable')->getPaymentsBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);
        //get all return details
        $supplierReturns = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);
        //get all Debit note details
        $debitNoteDetails = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);
        //get all debitNote Payment details
        $debitNotePaymentDetails = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentsBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);

        $poDataSet = [];
        $grnDataSet = [];
        $piDataSet = [];
        $pVDataSet = [];
        $paymentDataSet = [];
        $returnDataSet = [];
        $debitNoteDataSet = [];
        $debitNotePaymentDataSet = [];
        $supplierRecievedAmount = [];
        $supplierNotRecievedAmount = [];
        $totalOutstanding = [];
        $docDataSet = [];
        foreach ($poDetails as $key => $poValue) {
            $poDataSet[$poValue['purchaseOrderID']]['code'] = $poValue['purchaseOrderCode'];
            $poDataSet[$poValue['purchaseOrderID']]['issueDate'] = $poValue['purchaseOrderExpDelDate'];
            $poDataSet[$poValue['purchaseOrderID']]['total'] = $poValue['purchaseOrderTotal'];
            $poDataSet[$poValue['purchaseOrderID']]['statusName'] = $poValue['statusName'];
            $poDataSet[$poValue['purchaseOrderID']]['supplier'] = $poValue['purchaseOrderSupplierID'];
            $poDataSet[$poValue['purchaseOrderID']]['docType'] = 'purchase Order';
        }

        foreach ($grnDetails as $key => $grnValue) {
            $grnDataSet[$grnValue['grnID']]['code'] = $grnValue['grnCode'];
            $grnDataSet[$grnValue['grnID']]['issueDate'] = $grnValue['grnDate'];
            $grnDataSet[$grnValue['grnID']]['total'] = $grnValue['grnTotal'];
            $grnDataSet[$grnValue['grnID']]['statusName'] = $grnValue['statusName'];
            $grnDataSet[$grnValue['grnID']]['supplier'] = $grnValue['grnSupplierID'];
            $grnDataSet[$grnValue['grnID']]['docType'] = 'grn';
            $supplierNotRecievedAmount[$grnValue['grnSupplierID']] += floatval($grnValue['grnTotal']);
            if($grnValue['grnProductDocumentId'] != ''){
                $grnDataSet[$grnValue['grnID']]['parentCode'] = 'PO / '.$poDataSet[$grnValue['grnProductDocumentId']]['code'];

            }
        }

        $previous = null;
        foreach ($purchaseInvoiceDetails as $key => $pIValue) {
            $piDataSet[$pIValue['purchaseInvoiceID']]['code'] = $pIValue['purchaseInvoiceCode'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['issueDate'] = $pIValue['purchaseInvoiceIssueDate'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['total'] = $pIValue['purchaseInvoiceTotal'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['supplier'] = $pIValue['purchaseInvoiceSupplierID'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['statusName'] = $pIValue['statusName'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['docType'] = 'purchase Invoice';
            if($previous != $pIValue['purchaseInvoiceID']){
                $totalOutstanding[$pIValue['purchaseInvoiceSupplierID']] += floatval($pIValue['purchaseInvoiceTotal']);
                $previous = $pIValue['purchaseInvoiceID'];
            }
            if($pIValue['purchaseInvoicePoID'] != ''){
                $piDataSet[$pIValue['purchaseInvoiceID']]['parentCode'] = 'PO / '.$poDataSet[$pIValue['purchaseInvoicePoID']]['code'];
                $parentDataSet[$pIValue['purchaseInvoiceCode']] = 'PO / '.$poDataSet[$pIValue['purchaseInvoicePoID']]['code'];
                $supplierNotRecievedAmount[$pIValue['purchaseInvoiceSupplierID']] += floatval($pIValue['purchaseInvoiceTotal']);

            }
            else if($pIValue['purchaseInvoiceGrnID'] != '') {
                $piDataSet[$pIValue['purchaseInvoiceID']]['parentCode'] = 'GRN / '.$grnDataSet[$pIValue['purchaseInvoiceGrnID']]['code'];
                $parentDataSet[$pIValue['purchaseInvoiceCode']] = 'GRN / '.$grnDataSet[$pIValue['purchaseInvoiceGrnID']]['code'];

            } else {
                $supplierNotRecievedAmount[$pIValue['purchaseInvoiceSupplierID']] += floatval($pIValue['purchaseInvoiceTotal']);
            }

        }

        $previous = null;
        foreach ($pVDetails as $key => $pVValue) {
            $pVDataSet[$pVValue['paymentVoucherID']]['code'] = $pVValue['paymentVoucherCode'];
            $pVDataSet[$pVValue['paymentVoucherID']]['issueDate'] = $pVValue['paymentVoucherIssuedDate'];
            $pVDataSet[$pVValue['paymentVoucherID']]['total'] = $pVValue['paymentVoucherTotal'];
            $pVDataSet[$pVValue['paymentVoucherID']]['supplier'] = $pVValue['paymentVoucherSupplierID'];
            $pVDataSet[$pVValue['paymentVoucherID']]['statusName'] = $pVValue['statusName'];
            $pVDataSet[$pVValue['paymentVoucherID']]['docType'] = 'payment Voucher';

            if($previous != $pVValue['paymentVoucherID']){
                $totalOutstanding[$pVValue['paymentVoucherSupplierID']] += floatval($pVValue['paymentVoucherTotal']);
                $previous = $pVValue['paymentVoucherID'];
            }
            $supplierNotRecievedAmount[$pVValue['paymentVoucherSupplierID']] += floatval($pVValue['paymentVoucherTotal']);

        }

       $previous = null;
        foreach ($paymentDetails as $key => $paymentValue) {
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['code'] = $paymentValue['outgoingPaymentCode'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['issueDate'] = $paymentValue['outgoingPaymentDate'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['total'] = $paymentValue['outgoingPaymentPaidAmount'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['supplier'] = $paymentValue['supplierID'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['statusName'] = $paymentValue['statusName'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['totalCreditAmount'] += $paymentValue['outgoingPaymentCreditAmount'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['docType'] = 'payments';
            $supplierRecievedAmount[$paymentValue['supplierID']] += $paymentValue['outgoingPaymentPaidAmount'];
            $totalOutstanding[$paymentValue['supplierID']] += floatval($paymentValue['outgoingPaymentCreditAmount']);

            if($previous != $paymentValue['outgoingPaymentID']){
                $totalOutstanding[$paymentValue['supplierID']] -= floatval($paymentValue['outgoingPaymentPaidAmount']);
                $previous = $paymentValue['outgoingPaymentID'];
            }



            if($paymentValue['invoiceID'] != 0){
                $parentDataSet[$paymentValue['outgoingPaymentCode']]['PIs'][] = $piDataSet[$paymentValue['invoiceID']]['code'];
            }
            if($paymentValue['paymentVoucherId'] != 0){
                $parentDataSet[$paymentValue['outgoingPaymentCode']]['PVs'][] = $pVDataSet[$paymentValue['paymentVoucherId']]['code'];
            }

            if($paymentValue['outgoingPaymentType'] == 'advance'){
                $paymentDataSet[$paymentValue['outgoingPaymentID']]['type'] = 'advance Payment';
                $paymentDataSet[$paymentValue['outgoingPaymentID']]['docType'] = 'advance Payment';
                // $totalOutstanding[$paymentValue['supplierID']] -= floatval($paymentValue['outgoingPaymentAmount']);
            }

        }


        foreach ($supplierReturns as $key => $returnValue) {
            $returnDataSet[$returnValue['purchaseReturnID']]['code'] = $returnValue['purchaseReturnCode'];
            $returnDataSet[$returnValue['purchaseReturnID']]['issueDate'] = $returnValue['purchaseReturnDate'];
            $returnDataSet[$returnValue['purchaseReturnID']]['total'] = $returnValue['purchaseReturnTotal'];
            $returnDataSet[$returnValue['purchaseReturnID']]['supplier'] = $returnValue['grnSupplierID'];
            $returnDataSet[$returnValue['purchaseReturnID']]['statusName'] = $returnValue['statusName'];
            $returnDataSet[$returnValue['purchaseReturnID']]['parentCode'] = 'PI / '.$returnValue['grnCode'];
            $returnDataSet[$returnValue['purchaseReturnID']]['docType'] = 'returns';
            $supplierNotRecievedAmount[$returnValue['grnSupplierID']] -= floatval($returnValue['purchaseReturnTotal']);
        }

        $previous = null;
        foreach ($debitNoteDetails as $key => $debitNoteValue) {
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['code'] = $debitNoteValue['debitNoteCode'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['issueDate'] = $debitNoteValue['debitNoteDate'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['total'] = $debitNoteValue['debitNoteTotal'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['supplier'] = $debitNoteValue['supplierID'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['statusName'] = $debitNoteValue['statusName'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['parentCode'] = 'PI / '.$piDataSet[$debitNoteValue['purchaseInvoiceID']]['code'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['docType'] = 'debit Note';
            $parentDataSet[$debitNoteValue['debitNoteCode']] = 'PI / '.$piDataSet[$debitNoteValue['purchaseInvoiceID']]['code'];
            if($previous != $debitNoteValue['debitNoteID']){
                $totalOutstanding[$debitNoteValue['supplierID']] -= floatval($debitNoteValue['debitNoteTotal']);
                $previous = $debitNoteValue['debitNoteID'];
            }
            $supplierNotRecievedAmount[$debitNoteValue['supplierID']] -= floatval($debitNoteValue['debitNoteTotal']);
        }

        $previous = null;
        foreach ($debitNotePaymentDetails as $key => $debitNotePaymentValue) {

            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['code'] = $debitNotePaymentValue['debitNotePaymentCode'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['issueDate'] = $debitNotePaymentValue['debitNotePaymentDate'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['total'] = $debitNotePaymentValue['debitNotePaymentAmount'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['supplier'] = $debitNotePaymentValue['supplierID'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['statusName'] = $debitNotePaymentValue['statusName'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['parentCode'] = 'DN / '.$debitNoteDataSet[$debitNotePaymentValue['debitNoteID']]['code'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['docType'] = 'debit Note Payment';
            $parentDataSet[$debitNotePaymentValue['debitNotePaymentCode']] = 'DN / '.$debitNoteDataSet[$debitNotePaymentValue['debitNoteID']]['code'];
            if($previous != $debitNotePaymentValue['debitNotePaymentID']){
                $totalOutstanding[$debitNotePaymentValue['supplierID']] += floatval($debitNotePaymentValue['debitNotePaymentAmount']);
                $previous = $debitNotePaymentValue['debitNotePaymentID'];
            }
            $supplierRecievedAmount[$debitNotePaymentValue['supplierID']] -= $debitNotePaymentValue['debitNotePaymentAmount'];
        }

        // $arr = array_merge($paymentDataSet,array_merge($pVDataSet,array_merge($piDataSet,array_merge($poDataSet,array_merge($grnDataSet ,array_merge($returnDataSet,array_merge($debitNotePaymentDataSet,$debitNoteDataSet)))))));
        $arr = array_merge($paymentDataSet,array_merge($pVDataSet,array_merge($piDataSet,array_merge($debitNotePaymentDataSet,$debitNoteDataSet))));
        $sortData = Array();
        foreach ($arr as $key => $r) {
            $sortData[$key] = strtotime($r['issueDate']);
        }
        array_multisort($sortData, SORT_ASC, $arr);


        $suplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliersByIDs($supplierIDs);
        $supplierData = [];
        foreach ($suplierDetails as $key => $supplierValue) {
            $supplierData[$supplierValue['sID']] = $supplierValue;
        }
        $returnDataSet = [];
        $returnDataSet['supDetails'] = $supplierData;
        $returnDataSet['docDetails'] = $arr;
        $returnDataSet['supRecivedAmount'] = $supplierRecievedAmount;
        $returnDataSet['supNotRecivedAmount'] = $supplierNotRecievedAmount;
        $returnDataSet['outstanding'] = $totalOutstanding;
        $returnDataSet['parentDataSet'] = $parentDataSet;

        return $returnDataSet;
    }

    public function viewSupplierBalanceAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $supplierIds = $request->getPost('supplierIds');
        $fromDate = $request->getPost('fromDate');
        $toDate = $request->getPost('toDate');
        $plusToDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));

        //if suplierIds empty when assign all supplierIds to that array
        if (empty($supplierIds)) {
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
            foreach ($supplierData as $s) {
                $supplierIds[$s['supplierID']] = $s['supplierID'];
            }
        }
        $isAllSuppliers = $request->getPost('isAllSuppliers');
        $allTransactionDetails = $this->getSupplierOpeningBalanceDetails($supplierIds, '0000-00-00', $fromDate);
        $transactionDetails = $this->getSupplierOpeningBalanceDetails($supplierIds, $fromDate, $plusToDate);
        $openningBalances = $allTransactionDetails['supDetails'];

        $companyDetails = $this->getCompanyDetails();
        $translator = new Translator();
        $name = $translator->translate('Supplier Credit and Outstanding Balance Report');
        $headerView = $this->headerViewTemplate($name, $period = NULL);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $sDetailsView = new ViewModel(array(
            'supDetails' => $transactionDetails,
            'openningBalance' => $openningBalances,
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
            'parentDataSet' => $transactionDetails['parentDataSet'],
            'summaryOnly' => $summaryOnly
        ));
        $sDetailsView->setTemplate('reporting/supplier-report/generate-supplier-balance-details');

        $this->setLogMessage("Retrive suppliers details for supplier deatails view report.");
        $this->html = $sDetailsView;
        $this->msg = $this->getMessage('INFO_SUPP_REPORT_SUPPDATA');
        $this->status = true;
        return $this->JSONRespondHtml();

    }

    public function generateSupplierBalanceDetailsPdfAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $supplierIds = $request->getPost('supplierIds');
        $fromDate = $request->getPost('fromDate');
        $toDate = $request->getPost('toDate');
        $plusToDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
        //if suplierIds empty when assign all supplierIds to that array
        if (empty($supplierIds)) {
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
            foreach ($supplierData as $s) {
                $supplierIds[$s['supplierID']] = $s['supplierID'];
            }
        }
        $isAllSuppliers = $request->getPost('isAllSuppliers');
        $allTransactionDetails = $this->getSupplierOpeningBalanceDetails($supplierIds, '0000-00-00', $fromDate);
        $transactionDetails = $this->getSupplierOpeningBalanceDetails($supplierIds, $fromDate, $plusToDate);
        $openningBalances = $allTransactionDetails['supDetails'];

        $companyDetails = $this->getCompanyDetails();
        $translator = new Translator();
        $name = $translator->translate('Supplier Credit and Outstanding Balance Report');

        $headerView = $this->headerViewTemplate($name, $period = NULL);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $viewSupplierWiseItem = new ViewModel(array(
            'supDetails' => $transactionDetails,
            'openningBalance' => $openningBalances,
            'cD' => $companyDetails,
            'headerTemplate' => $headerViewRender,
            'parentDataSet' => $transactionDetails['parentDataSet'],
            'summaryOnly' => $summaryOnly
        ));
        $viewSupplierWiseItem->setTemplate('reporting/supplier-report/generate-supplier-balance-details');
        $viewSupplierWiseItem->setTerminal(true);

        // get rendered view into variable
        $htmlContent = $this->viewRendererHtmlContent($viewSupplierWiseItem);
        $pdfPath = $this->downloadPDF('supplier-credit-outstanding-balance-report', $htmlContent, true);

        $this->setLogMessage("Retrive suppliers wise balance details for supplier wise balance deatails PDF report.");
        $this->data = $pdfPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    public function generateSupplierBalanceDetailsSheetAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $supplierIds = $request->getPost('supplierIds');
        $fromDate = $request->getPost('fromDate');
        $toDate = $request->getPost('toDate');
        $plusToDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));
        //if suplierIds empty when assign all supplierIds to that array
        if (empty($supplierIds)) {
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
            foreach ($supplierData as $s) {
                $supplierIds[$s['supplierID']] = $s['supplierID'];
            }
        }
        $isAllSuppliers = $request->getPost('isAllSuppliers');
        $allTransactionDetails = $this->getSupplierOpeningBalanceDetails($supplierIds, '0000-00-00', $fromDate);
        $transactionDetails = $this->getSupplierOpeningBalanceDetails($supplierIds, $fromDate, $plusToDate);
        $openningBalances = $allTransactionDetails['supDetails'];

        $parentDataSet = $transactionDetails['parentDataSet'];
        $cD = $this->getCompanyDetails();

        if (count($transactionDetails['docDetails']) != 0) {
            $title = '';
            $tit = 'SUPPLIER CREDIT AND OUTSTANDING BALANCE REPORT';
            $in = array(
                0 => "",
                1 => $tit,
                2 => "",
                3 => "",
            );

            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => 'Report Generated: ' . date('Y-M-d h:i:s a'),
                1 => '',
                2 => '',
                3 => '',
            );
            $title.=implode(",", $in) . "\n";

            $in = array(
                0 => $cD[0]->companyName,
                1 => $cD[0]->companyAddress,
                2 => 'Tel: ' . $cD[0]->telephoneNumber
            );
            $title.=implode(",", $in) . "\n";
            $arrs = array("SUPPLIER NAME", "DOCUMENT TYPE", "DOCUMENT CODE", "DATE", "STATUS", "PARENT DOC", "AMOUNT", "SUPPLIER OUTSTANDING", "SUPPLIER CREDIT");
            $header = implode(",", $arrs);
                
            $arr = '';

            foreach ($transactionDetails['supDetails'] as $supKey => $supData) {
                $tempOpeningOutsatnding = $openningBalances[$supData['sID']]['openingOutstanding'];
                $tempOpeningCredit = $openningBalances[$supData['sID']]['openingCredit'];
                $in = array(
                    0 => $supData['sName'].' - '.$supData['sCD'],
                    1 => '',
                    2 => '',
                    3 => '',
                    4 => '',
                    5 => '',
                    6 => 'opening Balance',
                    7 => $tempOpeningOutsatnding,
                    8 => $tempOpeningCredit
                );
                $arr.=implode(",", $in) . "\n";
                    
                foreach ($transactionDetails['docDetails'] as $key => $transValue){
                    if($transValue['supplier'] == $supKey){
                        $parentDoc = "";
                        if($transValue['docType'] == 'payments'){
                            if(isset($parentDataSet[$transValue['code']]['PIs'] )){
                                $parentDoc = 'PI / '.implode(" - ", $parentDataSet[$transValue['code']]['PIs']);
                            }
                            if(isset($parentDataSet[$transValue['code']]['PVs'] )){
                                $parentDoc .= ' PV / '.implode(" - ", $parentDataSet[$transValue['code']]['PVs']);
                            }
                        }else{
                            $parentDoc = $parentDataSet[$transValue['code']];
                        }

                        if ($transValue['docType'] == "purchase Invoice") {
                            $tempOpeningOutsatnding += $transValue['total'];
                            $docAmount = $transValue['total'];
                        } else if ($transValue['docType'] == "payment Voucher") {
                            $tempOpeningOutsatnding += $transValue['total'];
                            $docAmount = $transValue['total'];
                        } else if ($transValue['docType'] == "debit Note") {
                            if ($transValue['invoiceBalanceAmount'] >= $transValue['total']) {
                                if ($tempOpeningOutsatnding >= $transValue['total']) {
                                    $tempOpeningOutsatnding -= $transValue['total'];
                                } else {
                                    $creditDiffForDebitNote = $transValue['total'] - $tempOpeningOutsatnding;
                                    $tempOpeningOutsatnding = 0;
                                    $tempOpeningCredit += $creditDiffForDebitNote;
                                }
                            } else {
                                $tempOpeningOutsatnding -= $transValue['invoiceBalanceAmount'];
                                $creditDiffForDebitNote = $transValue['total'] - $transValue['invoiceBalanceAmount'];
                                $tempOpeningCredit += $creditDiffForDebitNote; 
                            }
                            $docAmount = $transValue['total'];
                        } else if ($transValue['docType'] == "debit Note Payment") {
                            if ($tempOpeningCredit >= $transValue['total']) {
                                $tempOpeningCredit -= $transValue['total'];
                            } else {
                                $tempOpeningCredit = 0;
                            }
                            $docAmount = $transValue['total'];
                        } else if ($transValue['docType'] == "payments") {
                            $invoiceLeftToPayAmount = ($transValue['invoiceLeftToPayAmount'] == 0) ? $transValue['purchaseInvoiceTotal'] : $transValue['invoiceLeftToPayAmount'];
                            $outgoingPaymentBalance = $transValue['total'] - $invoiceLeftToPayAmount;
                            if ($outgoingPaymentBalance < 0) {
                                $outgoingPaymentBalance = 0;
                            }
                            $tempOpeningCredit += $outgoingPaymentBalance;
                            $tempOpeningCredit -= $transValue['totalCreditAmount'];

                            if ($transValue['total'] >= $invoiceLeftToPayAmount) {
                                $tempOpeningOutsatnding -= $invoiceLeftToPayAmount;
                            } else {
                                $tempOpeningOutsatnding -= $transValue['total'];
                            }

                            $docAmount = $transValue['total'];
                        } else if ($transValue['docType'] == "advance Payment") {
                            $tempOpeningCredit += $transValue['total'];
                            $docAmount = $transValue['total'];
                        }


                        $in = array(
                            0 => $supData['sName'].' - '.$supData['sCD'],
                            1 => $transValue['docType'],
                            2 => $transValue['code'],
                            3 => explode(" ",$transValue['issueDate'])[0],
                            4 => $transValue['statusName'],
                            5 => $parentDoc,
                            6 => $docAmount,
                            7 => $tempOpeningOutsatnding,
                            8 => $tempOpeningCredit
                        );
                        $arr.=implode(",", $in) . "\n";
                    }

                }
                $grandOutstandingTotal += $tempOpeningOutsatnding;
                $grandCreditTotal += $tempOpeningCredit;
            }
            $in = array(
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => '',
                    4 => '',
                    5 => '',
                    6 => 'Grand Total',
                    7 => $grandOutstandingTotal,
                    8 => $grandCreditTotal
                );
                $arr.=implode(",", $in) . "\n";
                
        } else {
            $arr = "no matching records found\n";
        }

        $name = "supplier_credit_outstanding_balance_report";
        $csvContent = $this->csvContent($title, $header, $arr);
        $csvPath = $this->generateCSVFile($name, $csvContent);

        $this->setLogMessage("Retrive suppliers wise balance details for supplier wise balance deatails CSV report.");
        $this->data = $csvPath;
        $this->status = true;
        return $this->JSONRespond();
    }

    private function getSupplierOpeningBalanceDetails($supplierIDs = [], $fromDate = null, $toDate = null)
    {
        $status = ['3','4','6'];
        //get all purchaseInvoices
        $purchaseInvoiceDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBySupplierIDsForSupplierBalance($supplierIDs, $status, $fromDate, $toDate);
        // get all grn details
        $grnDetails = $this->CommonTable('Inventory\Model\GrnTable')->getGrnBySupplierIDsForSupplierBalance($supplierIDs, $status, $fromDate, $toDate);
        // get all po details
        $poDetails = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrderBySupplierIDsForSupplierBalance($supplierIDs, $status, $fromDate, $toDate);
        // get all paymentVoucher details
        $pVDetails = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchersBySupplierIDsForSupplierBalance($supplierIDs, $status, $fromDate, $toDate);
        // get all payment details
        $paymentDetails = $this->CommonTable('SupplierPaymentsTable')->getPaymentsBySupplierIDsForSupplierBalance($supplierIDs, $status, $fromDate, $toDate);
        //get all Debit note details
        $debitNoteDetails = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteBySupplierIDsForSupplierBalance($supplierIDs, $status, $fromDate, $toDate);
        //get all debitNote Payment details
        $debitNotePaymentDetails = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentsBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);

        $poDataSet = [];
        $grnDataSet = [];
        $piDataSet = [];
        $pVDataSet = [];
        $paymentDataSet = [];
        $debitNoteDataSet = [];
        $debitNotePaymentDataSet = [];
        $supplierRecievedAmount = [];
        $supplierNotRecievedAmount = [];
        $docDataSet = [];
        foreach ($poDetails as $key => $poValue) {
            $poDataSet[$poValue['purchaseOrderID']]['code'] = $poValue['purchaseOrderCode'];
            $poDataSet[$poValue['purchaseOrderID']]['issueDate'] = $poValue['createdTimeStamp'];
            $poDataSet[$poValue['purchaseOrderID']]['total'] = $poValue['purchaseOrderTotal'];
            $poDataSet[$poValue['purchaseOrderID']]['statusName'] = $poValue['statusName'];
            $poDataSet[$poValue['purchaseOrderID']]['supplier'] = $poValue['purchaseOrderSupplierID'];
            $poDataSet[$poValue['purchaseOrderID']]['docType'] = 'purchase Order';
        }

        foreach ($grnDetails as $key => $grnValue) {
            $grnDataSet[$grnValue['grnID']]['code'] = $grnValue['grnCode'];
            $grnDataSet[$grnValue['grnID']]['issueDate'] = $grnValue['createdTimeStamp'];
            $grnDataSet[$grnValue['grnID']]['total'] = $grnValue['grnTotal'];
            $grnDataSet[$grnValue['grnID']]['statusName'] = $grnValue['statusName'];
            $grnDataSet[$grnValue['grnID']]['supplier'] = $grnValue['grnSupplierID'];
            $grnDataSet[$grnValue['grnID']]['docType'] = 'grn';
            $supplierNotRecievedAmount[$grnValue['grnSupplierID']] += floatval($grnValue['grnTotal']);
            if($grnValue['grnProductDocumentId'] != ''){
                $grnDataSet[$grnValue['grnID']]['parentCode'] = 'PO / '.$poDataSet[$grnValue['grnProductDocumentId']]['code'];

            }
        }

        foreach ($purchaseInvoiceDetails as $key => $pIValue) {
            $piDataSet[$pIValue['purchaseInvoiceID']]['code'] = $pIValue['purchaseInvoiceCode'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['issueDate'] = $pIValue['createdTimeStamp'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['total'] = $pIValue['purchaseInvoiceTotal'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['supplier'] = $pIValue['purchaseInvoiceSupplierID'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['statusName'] = $pIValue['statusName'];
            $piDataSet[$pIValue['purchaseInvoiceID']]['docType'] = 'purchase Invoice';
            
            if($pIValue['purchaseInvoicePoID'] != ''){
                $piDataSet[$pIValue['purchaseInvoiceID']]['parentCode'] = 'PO / '.$poDataSet[$pIValue['purchaseInvoicePoID']]['code'];
                $parentDataSet[$pIValue['purchaseInvoiceCode']] = 'PO / '.$poDataSet[$pIValue['purchaseInvoicePoID']]['code'];
                $supplierNotRecievedAmount[$pIValue['purchaseInvoiceSupplierID']] += floatval($pIValue['purchaseInvoiceTotal']);

            }
            else if($pIValue['purchaseInvoiceGrnID'] != '') {
                $piDataSet[$pIValue['purchaseInvoiceID']]['parentCode'] = 'GRN / '.$grnDataSet[$pIValue['purchaseInvoiceGrnID']]['code'];
                $parentDataSet[$pIValue['purchaseInvoiceCode']] = 'GRN / '.$grnDataSet[$pIValue['purchaseInvoiceGrnID']]['code'];

            } else {
                $supplierNotRecievedAmount[$pIValue['purchaseInvoiceSupplierID']] += floatval($pIValue['purchaseInvoiceTotal']);
            }

        }

        foreach ($pVDetails as $key => $pVValue) {
            $pVDataSet[$pVValue['paymentVoucherID']]['code'] = $pVValue['paymentVoucherCode'];
            $pVDataSet[$pVValue['paymentVoucherID']]['issueDate'] = $pVValue['createdTimeStamp'];
            $pVDataSet[$pVValue['paymentVoucherID']]['total'] = $pVValue['paymentVoucherTotal'];
            $pVDataSet[$pVValue['paymentVoucherID']]['supplier'] = $pVValue['paymentVoucherSupplierID'];
            $pVDataSet[$pVValue['paymentVoucherID']]['statusName'] = $pVValue['statusName'];
            $pVDataSet[$pVValue['paymentVoucherID']]['docType'] = 'payment Voucher';

            $supplierNotRecievedAmount[$pVValue['paymentVoucherSupplierID']] += floatval($pVValue['paymentVoucherTotal']);

        }

        foreach ($paymentDetails as $key => $paymentValue) {
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['code'] = $paymentValue['outgoingPaymentCode'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['issueDate'] = $paymentValue['createdTimeStamp'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['total'] = $paymentValue['outgoingPaymentPaidAmount'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['supplier'] = $paymentValue['supplierID'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['statusName'] = $paymentValue['statusName'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['totalCreditAmount'] += $paymentValue['outgoingPaymentCreditAmount'];
            $paymentDataSet[$paymentValue['outgoingPaymentID']]['totalCashAmount'] += $paymentValue['outgoingInvoiceCashAmount'];

            if ($paymentValue['outgoingPaymentType'] == 'invoice') {

                $invData = $this->CommonTable('SupplierInvoicePaymentsTable')->getAllSupplierInvoiceDetailsByPaymentID($paymentValue['outgoingPaymentID']);

                foreach ($invData as $key99 => $value99) {
                    if (!is_null($value99['purchaseInvoiceTotal'])) {
                        // $paymentDataSet[$paymentValue['outgoingPaymentID']]['invoiceLeftToPayAmount'] += $paymentValue['invoiceLeftToPayAmount'];
                        // $invPaid = $this->CommonTable('SupplierPaymentsTable')->getPiTotalPaymentsByPiIdAnd($value99['invoiceID'], $paymentValue['outgoingPaymentID'])->current();
                        $invPayedAmount = (!empty($value99['purchaseInvoicePayedAmount']) && $value99['purchaseInvoicePayedAmount'] != 0) ? floatval($value99['purchaseInvoicePayedAmount']) : 0;
                        $debitDetails = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPiDebitNoteTotalByPiIdForSupplierBalance($value99['invoiceID'], $paymentValue['createdTimeStamp']);
                        if ($debitDetails) {
                            $purchaseInvoiceTotal += floatval($value99['purchaseInvoiceTotal']) - floatval($debitDetails['total']);
                        } else {
                            $purchaseInvoiceTotal += $value99['purchaseInvoiceTotal'];
                        }
                        $paymentDataSet[$paymentValue['outgoingPaymentID']]['invoiceLeftToPayAmount'] += ($invPayedAmount == 0) ? $purchaseInvoiceTotal : (floatval($value99['purchaseInvoiceTotal']) - floatval($invPayedAmount));
                        $paymentDataSet[$paymentValue['outgoingPaymentID']]['purchaseInvoiceTotal'] += $value99['purchaseInvoiceTotal'];
                    }
                    if (!is_null($value99['paymentVoucherTotal'])) {
                        $paymentDataSet[$paymentValue['outgoingPaymentID']]['purchaseInvoiceTotal'] += $value99['paymentVoucherTotal'];
                        $paymentDataSet[$paymentValue['outgoingPaymentID']]['invoiceLeftToPayAmount'] += $value99['left_to_pay_pv'];
                    }
                    if($value99['outgoingPaymentType'] == 'invoice'){
                        $paymentDataSet[$paymentValue['outgoingPaymentID']]['docType'] = 'payments';
                    }  

                    if($value99['invoiceID'] != 0){
                        $parentDataSet[$paymentValue['outgoingPaymentCode']]['PIs'][] = $piDataSet[$value99['invoiceID']]['code'];
                    }
                    if($value99['paymentVoucherId'] != 0){
                        $parentDataSet[$paymentValue['outgoingPaymentCode']]['PVs'][] = $pVDataSet[$value99['paymentVoucherId']]['code'];
                    } 
                }
            }
            if($paymentValue['outgoingPaymentType'] == 'invoice'){
                $paymentDataSet[$paymentValue['outgoingPaymentID']]['docType'] = 'payments';
            }
            $supplierRecievedAmount[$paymentValue['supplierID']] += $paymentValue['outgoingPaymentAmount'];

            if($paymentValue['outgoingPaymentType'] == 'advance'){
                $paymentDataSet[$paymentValue['outgoingPaymentID']]['type'] = 'advance Payment';
                $paymentDataSet[$paymentValue['outgoingPaymentID']]['docType'] = 'advance Payment';
            }
        }

        foreach ($debitNoteDetails as $key => $debitNoteValue) {
            if ($debitNoteValue['purchaseInvoiceID'] != null) {
                $invoiceBalanceAfterPayment = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBalanceAmountAfterPayment($debitNoteValue['purchaseInvoiceID'], $status);
                $invoiceBalanceAmount = $invoiceBalanceAfterPayment['remainingInvoiceAmount'];
                $invoiceBalanceAfterDebitNote = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPurchaseInvoiceRelatedDebitNotesForInvoiceRemainingAmount($debitNoteValue['purchaseInvoiceID'], $status, $debitNoteValue['debitNoteID']);
                if (!is_null($invoiceBalanceAfterDebitNote)) {
                    $invoiceBalanceAmount = $invoiceBalanceAmount + $invoiceBalanceAfterDebitNote['TotalDebitAmountAfterInvoice'];
                }
            } else {
                $invoiceBalanceAmount = 0;
            }
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['code'] = $debitNoteValue['debitNoteCode'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['debitNoteID'] = $debitNoteValue['debitNoteID'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['piId'] = $debitNoteValue['purchaseInvoiceID'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['issueDate'] = $debitNoteValue['createdTimeStamp'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['total'] = $debitNoteValue['debitNoteTotal'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['supplier'] = $debitNoteValue['supplierID'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['statusName'] = $debitNoteValue['statusName'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['invoiceBalanceAmount'] = $invoiceBalanceAmount;
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['parentCode'] = 'PI / '.$piDataSet[$debitNoteValue['purchaseInvoiceID']]['code'];
            $debitNoteDataSet[$debitNoteValue['debitNoteID']]['docType'] = 'debit Note';
            $parentDataSet[$debitNoteValue['debitNoteCode']] = 'PI / '.$piDataSet[$debitNoteValue['purchaseInvoiceID']]['code'];
            $supplierNotRecievedAmount[$debitNoteValue['supplierID']] -= floatval($debitNoteValue['debitNoteTotal']);
        }

        foreach ($debitNotePaymentDetails as $key => $debitNotePaymentValue) {

            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['code'] = $debitNotePaymentValue['debitNotePaymentCode'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['issueDate'] = $debitNotePaymentValue['createdTimeStamp'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['total'] = $debitNotePaymentValue['debitNotePaymentAmount'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['supplier'] = $debitNotePaymentValue['supplierID'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['statusName'] = $debitNotePaymentValue['statusName'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['parentCode'] = 'DN / '.$debitNoteDataSet[$debitNotePaymentValue['debitNoteID']]['code'];
            $debitNotePaymentDataSet[$debitNotePaymentValue['debitNotePaymentID']]['docType'] = 'debit Note Payment';
            $parentDataSet[$debitNotePaymentValue['debitNotePaymentCode']] = 'DN / '.$debitNoteDataSet[$debitNotePaymentValue['debitNoteID']]['code'];
            $supplierRecievedAmount[$debitNotePaymentValue['supplierID']] -= $debitNotePaymentValue['debitNotePaymentAmount'];
        }

        $arr = array_merge($paymentDataSet,array_merge($pVDataSet,array_merge($piDataSet,array_merge($debitNotePaymentDataSet,$debitNoteDataSet))));
        $sortData = Array();
        foreach ($arr as $key => $r) {
            $sortData[$key] = strtotime($r['issueDate']);
        }
        array_multisort($sortData, SORT_ASC, $arr);


        $suplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliersByIDs($supplierIDs);
        $supplierData = [];
        foreach ($suplierDetails as $key => $supplierValue) {
            $supplierData[$supplierValue['sID']] = $supplierValue;
            $openingCredit=0;
            $openingOutstanding=0;

            foreach ($arr as $key => $value) {
                if($value['supplier'] == $supplierValue['sID']){
                    if ($value['docType'] == "purchase Invoice") {
                        $openingOutstanding += $value['total'];
                    } else if ($value['docType'] == "payment Voucher") {
                        $openingOutstanding += $value['total'];
                    } else if ($value['docType'] == "debit Note") {
                       
                        if ($value['piId'] != null) {
                             $invoiceBalanceAfterPayment = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBalanceAmountAfterPayment($value['piId'], $status);
                            $invoiceBalanceAmount = $invoiceBalanceAfterPayment['remainingInvoiceAmount'];
                            $invoiceBalanceAfterDebitNote = $this->CommonTable('Inventory\Model\DebitNoteTable')->getPurchaseInvoiceRelatedDebitNotesForInvoiceRemainingAmount($value['piId'], $status, $value['debitNoteID']);
                            if (!is_null($invoiceBalanceAfterDebitNote)) {
                                $invoiceBalanceAmount = $invoiceBalanceAmount + $invoiceBalanceAfterDebitNote['TotalDebitAmountAfterInvoice'];
                            }
                        } else {
                            $invoiceBalanceAmount = 0;
                        }
                        if ($invoiceBalanceAmount >= $value['total']) {
                            if ($openingOutstanding >= $value['total']) {
                                $openingOutstanding -= $value['total'];
                            } else {
                                $creditDiffForDebitNote = $value['total'] - $openingOutstanding;
                                $openingOutstanding = 0;
                                $openingCredit += $creditDiffForDebitNote;
                            }
                        } else {
                            $openingOutstanding -= $invoiceBalanceAmount;
                            $creditDiffForDebitNote = $value['total'] - $invoiceBalanceAmount;
                            $openingCredit += $creditDiffForDebitNote; 
                        }
                    } else if ($value['docType'] == "debit Note Payment") {
                        if ($openingCredit >= $value['total']) {
                            $openingCredit -= $value['total'];
                        } else {
                            $openingCredit = 0;
                        }
                    } else if ($value['docType'] == "payments") {
                        $invoiceLeftToPayAmount = ($value['invoiceLeftToPayAmount'] == 0) ? $value['purchaseInvoiceTotal'] : $value['invoiceLeftToPayAmount'];
                        $outgoingPaymentBalance = $value['total'] - $invoiceLeftToPayAmount;
                        if ($outgoingPaymentBalance < 0) {
                            $outgoingPaymentBalance = 0;
                        }
                        $openingCredit += $outgoingPaymentBalance;
                        $openingCredit -= $value['totalCreditAmount'];

                        if ($value['total'] >= $invoiceLeftToPayAmount) {
                            $openingOutstanding -= $invoiceLeftToPayAmount;
                        } else {
                            $openingOutstanding -= $value['total'];
                        }
                    } else if ($value['docType'] == "advance Payment") {
                        $openingCredit += $value['total'];
                    }
                }
            }

            $supplierData[$supplierValue['sID']]['openingOutstanding'] = $openingOutstanding;
            $supplierData[$supplierValue['sID']]['openingCredit'] = $openingCredit;

        }
        $returnDataSet = [];
        $returnDataSet['supDetails'] = $supplierData;
        $returnDataSet['docDetails'] = $arr;
        $returnDataSet['supRecivedAmount'] = $supplierRecievedAmount;
        $returnDataSet['supNotRecivedAmount'] = $supplierNotRecievedAmount;
        $returnDataSet['parentDataSet'] = $parentDataSet;

        return $returnDataSet;
    }
    public function viewSupplierAndDocumentWiseItemDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIDs = $request->getPost('supplierIds');
            //if suplierIds empty when assign all supplierIds to that array
            if (empty($supplierIDs)) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
                foreach ($supplierData as $s) {
                    $supplierIDs[$s['supplierID']] = $s['supplierID'];
                }
            }
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $docType = $request->getPost('docType');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $processdDataSet = $this->getSupplierAndDocumentWiseProcessData($docType, $fromDate, $toDate, $supplierIDs);

            $translator = new Translator();
            $name = $translator->translate('Supplier And Document Wise Item Details Report');

            $companyDetails = $this->getCompanyDetails();

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $sWiseItemDetailsView = new ViewModel(array(
                'sL' => $processdDataSet,
                'cD' => $companyDetails,
                'headerTemplate' => $headerViewRender,
            ));
            $sWiseItemDetailsView->setTemplate('reporting/supplier-report/generate-supplier-and-document-wise-item-pdf');

            $this->setLogMessage("Retrive suppliers and document wise items details for supplier and document wise item deatails view report.");
            $this->html = $sWiseItemDetailsView;
            $this->msg = $this->getMessage('INFO_SUPP_REPORT_SUPPITEM_DATA');
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function generateSupplierAndDocumentWiseItemPdfAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIDs = $request->getPost('supplierIds');
            //if suplierIds empty when assign all supplierIds to that array
            if (empty($supplierIDs)) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
                foreach ($supplierData as $s) {
                    $supplierIDs[$s['supplierID']] = $s['supplierID'];
                }
            }
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $docType = $request->getPost('docType');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $processdDataSet = $this->getSupplierAndDocumentWiseProcessData($docType, $fromDate, $toDate, $supplierIDs);

            $cD = $this->getCompanyDetails();
            $translator = new Translator();
            $name = $translator->translate('Supplier And Document Wise Item Details Report');

            $headerView = $this->headerViewTemplate($name, $period = NULL);
            $headerView->setTemplate('reporting/template/headerTemplateNormal');
            $headerViewRender = $this->htmlRender($headerView);

            $viewSupplierWiseItem = new ViewModel(array(
                'sL' => $processdDataSet,
                'headerTemplate' => $headerViewRender,
            ));

            $viewSupplierWiseItem->setTemplate('reporting/supplier-report/generate-supplier-and-document-wise-item-pdf');
            $viewSupplierWiseItem->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewSupplierWiseItem);
            $pdfPath = $this->downloadPDF('supplier-and-document-wise-item-report', $htmlContent, true);

            $this->setLogMessage("Retrive suppliers and document wise items details for supplier and document wise item deatails PDF report.");
            $this->data = $pdfPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }


    public function generateSupplierAndDocumentWiseItemSheetAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $supplierIDs = $request->getPost('supplierIds');
            //if suplierIds empty when assign all supplierIds to that array
            if (empty($supplierIDs)) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAllAsc();
                foreach ($supplierData as $s) {
                    $supplierIDs[$s['supplierID']] = $s['supplierID'];
                }
            }
            $isAllSuppliers = $request->getPost('isAllSuppliers');
            $docType = $request->getPost('docType');
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');

            $processdDataSet = $this->getSupplierAndDocumentWiseProcessData($docType, $fromDate, $toDate, $supplierIDs);

            $cD = $this->getCompanyDetails();

            if (sizeof($processdDataSet) > 0) {
                $title = '';
                $tit = 'SUPPLIER  AND DOCUMENT WISE ITEM DETAILS REPORT';
                $in = array(
                    0 => "",
                    1 => $tit,
                    2 => "",
                    3 => "",
                );

                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => 'Report Generated: ' . date('Y-M-d h:i:s a'),
                    1 => '',
                    2 => '',
                    3 => '',
                );
                $title.=implode(",", $in) . "\n";

                $in = array(
                    0 => $cD[0]->companyName,
                    1 => $cD[0]->companyAddress,
                    2 => 'Tel: ' . $cD[0]->telephoneNumber
                );
                $title.=implode(",", $in) . "\n";

                if ($fromDate != null && $toDate != null) {
                    $in = ['Period: ' . $fromDate . ' - ' . $toDate];
                    $title.=implode(",", $in) . "\n";
                }
                $arrs = array("SUPPLIER NAME", "DOCUMENT", "ITEM CODE AND NAME", "QUANTITY", "PRICE", "TOTAL");
                $header = implode(",", $arrs);
                $arr = '';
                $total = 0;
                foreach ($processdDataSet as $key => $value) {
                    $supplierNameArr = split(':', $key);
                    $supName = $supplierNameArr[1];

                    $in = array(
                        0 => $supName,
                        1 => '',
                        2 => '',
                        3 => '',
                        4 => '',
                        5 => ''
                    );
                    $arr.=implode(",", $in) . "\n";
                        

                    foreach ($value as $key2 => $value2) { 
                        $in = array(
                            0 => '',
                            1 => $value2['code'],
                            2 => '',
                            3 => '',
                            4 => '',
                            5 => ''
                        );
                        $arr.=implode(",", $in) . "\n";

                        foreach ($value2['products'] as $key3 => $value3) {
                            $total += (floatval($value3['piPP']) * floatval($value3['piPQ']));
                            $in = array(
                                0 => '',
                                1 => '',
                                2 => $value3['piPC'].'-'.$value3['piPN'],
                                3 => $value3['piPQ'].' '.$value3['piPUom'],
                                4 => $value3['piPP'],
                                5 => floatval($value3['piPP']) * $value3['piPQ']
                            );
                            $arr.=implode(",", $in) . "\n";

                        }

                    }
                }


                $in = array(
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => '',
                    4 => '',
                    5 => ''
                );
                $arr.=implode(",", $in) . "\n";

                $in = array(
                    0 => '',
                    1 => '',
                    2 => '',
                    3 => 'Total',
                    4 => '',
                    5 => $total
                );
                $arr.=implode(",", $in) . "\n";
                
            } else {
                $arr = "no matching records found\n";
            }

            $name = "supplier_and_document_wise_item_details_report";
            $csvContent = $this->csvContent($title, $header, $arr);
            $csvPath = $this->generateCSVFile($name, $csvContent);

            $this->setLogMessage("Retrive suppliers and document wise items details for supplier and document wise item deatails CSV report.");
            $this->data = $csvPath;
            $this->status = true;
            return $this->JSONRespond();
        }
    }


    public function getSupplierAndDocumentWiseProcessData($docType, $fromDate, $toDate, $supplierIDs) {
        $poDataSet = [];
        $piDataSet = [];
        $grnDataSet = [];

        $status = ['3','4','6'];
        //get all purchaseInvoices

        $poDetails = [];
        $purchaseInvoiceDetails = [];
        $grnDetails = [];

        if ($docType == 'po') {
            $poDetails = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrderBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);

            foreach ($poDetails as $key => $poValue) {

                $supKey = $poValue['purchaseOrderSupplierID'].':'.$poValue['supplierName'].'-'.$poValue['supplierCode'];

                $poDataSet[$supKey][$poValue['purchaseOrderID']]['code'] = $poValue['purchaseOrderCode'];
                $poDataSet[$supKey][$poValue['purchaseOrderID']]['docType'] = 'purchase Order';
                $poProductDetails = $this->CommonTable('Inventory\Model\PurchaseOrderProductTable')->getAllPoProductsByPoIDForSupReport($poValue['purchaseOrderID']);
                $poDataSet[$supKey][$poValue['purchaseOrderID']]['products'] = $poProductDetails;


                $poProducts = [];
                foreach ($poProductDetails as $key1 => $row) {

                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                    $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['purchaseOrderProductPrice'], $productUom);
                    $thisqty = $this->getProductQuantityViaDisplayUom($row['purchaseOrderProductQuantity'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];
                    $productKey = $row['productID'];

                    $poProducts[$productKey] = array('piPC' => $row['productCode'], 'piPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'piPP' => $unitPrice, 'piPQ' => $thisqty['quantity'], 'piPT' => $row['purchaseOrderProductTotal'], 'piPUom' => $thisqty['uomAbbr']);
                }

                $poDataSet[$supKey][$poValue['purchaseOrderID']]['products'] = $poProducts;
            }
            $processdDataSet = $poDataSet;

        } elseif ($docType == 'pi') {

            $purchaseInvoiceDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);
            $previous = null;
            foreach ($purchaseInvoiceDetails as $key => $pIValue) {

                $supKey = $pIValue['purchaseInvoiceSupplierID'].':'.$pIValue['supplierName'].'-'.$pIValue['supplierCode'];

                $piDataSet[$supKey][$pIValue['purchaseInvoiceID']]['code'] = $pIValue['purchaseInvoiceCode'];
                $piDataSet[$supKey][$pIValue['purchaseInvoiceID']]['issueDate'] = $pIValue['purchaseInvoiceIssueDate'];
                $piDataSet[$supKey][$pIValue['purchaseInvoiceID']]['total'] = $pIValue['purchaseInvoiceTotal'];
                $piDataSet[$supKey][$pIValue['purchaseInvoiceID']]['supplier'] = $pIValue['purchaseInvoiceSupplierID'];
                $piDataSet[$supKey][$pIValue['purchaseInvoiceID']]['statusName'] = $pIValue['statusName'];
                $piDataSet[$supKey][$pIValue['purchaseInvoiceID']]['docType'] = 'purchase Invoice';
                $purchaseInvoiceProducts = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPIsByPIIDForSupReport($pIValue['purchaseInvoiceID']);


                $piProducts = [];
                foreach ($purchaseInvoiceProducts as $key1 => $row) {

                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                    $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['purchaseInvoiceProductPrice'], $productUom);
                    $thisqty = $this->getProductQuantityViaDisplayUom($row['purchaseInvoiceProductTotalQty'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];
                    $productKey = $row['productID'];

                    $piProducts[$productKey] = array('piPC' => $row['productCode'], 'piPN' => $row['productName'], 'lPID' => $row['locationProductID'], 'piPP' => $unitPrice, 'piPD' => $row['purchaseInvoiceProductDiscount'], 'piPQ' => $thisqty['quantity'], 'piPT' => $row['purchaseInvoiceProductTotal'], 'piPUom' => $thisqty['uomAbbr']);
                }

                $piDataSet[$supKey][$pIValue['purchaseInvoiceID']]['products'] = $piProducts;
            }
            $processdDataSet = $piDataSet;
            
        } elseif ($docType == 'grn') {

            $grnDetails = $this->CommonTable('Inventory\Model\GrnTable')->getGrnBySupplierIDs($supplierIDs, $status, $fromDate, $toDate);
            foreach ($grnDetails as $key => $grnValue) {
                $supKey = $grnValue['grnSupplierID'].':'.$grnValue['supplierName'].'-'.$grnValue['supplierCode'];

                $grnDataSet[$supKey][$grnValue['grnID']]['code'] = $grnValue['grnCode'];
                $grnDataSet[$supKey][$grnValue['grnID']]['issueDate'] = $grnValue['grnDate'];
                $grnDataSet[$supKey][$grnValue['grnID']]['total'] = $grnValue['grnTotal'];
                $grnDataSet[$supKey][$grnValue['grnID']]['statusName'] = $grnValue['statusName'];
                $grnDataSet[$supKey][$grnValue['grnID']]['supplier'] = $grnValue['grnSupplierID'];
                $grnDataSet[$supKey][$grnValue['grnID']]['docType'] = 'grn';

                $grnProductsDetails = $this->CommonTable('Inventory\Model\GrnTable')->getGrnForPreviewByGrnID($grnValue['grnID']);


                $grnProductData = $this->setGrnDataToReturnFormat($grnProductsDetails, false);
                $grnProducts = [];
                foreach ($grnProductData[$grnValue['grnID']]['gProducts'] as $key1 => $row) {

                    $productKey = $row['productID'];

                    $grnProducts[$productKey] = array('piPC' => $row['gPC'], 'piPN' => $row['gPN'], 'lPID' => $row['lPID'], 'piPP' => $row['gPP'], 'piPQ' => $row['gPQ'], 'piPT' => $row['gPT'], 'piPUom' => $row['gPUom']);
                }
                $grnDataSet[$supKey][$grnValue['grnID']]['products'] = $grnProducts;
            }

            $processdDataSet = $grnDataSet;
        }

        return $processdDataSet;
    }


    private function setGrnDataToReturnFormat($grnData, $isSelectedUomWise = false)
    {
        $rowCount = 0;
        $tempProduct = [];
        $btchData = [];
        $serialData = [];
        $tempG = array();
        $taxDetails = [];

        foreach ($grnData as $key => $row) {

                $grnID = $row['grnID'];
                $tempG['gID'] = $row['grnID'];
                $tempG['code'] = $row['grnCode'];
                $tempG['gSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
                $tempG['gSID'] = $row['grnSupplierID'];
                $tempG['gSR'] = $row['grnSupplierReference'];
                $tempG['gRL'] = $row['locationCode'] . '-' . $row['locationName'];
                $tempG['retriveLocation'] = $row['grnRetrieveLocation'];
                $tempG['gD'] = $this->convertDateToUserFormat($row['grnDate']);
                $tempG['gDC'] = $row['grnDeliveryCharge'];
                $tempG['gT'] = $row['grnTotal'];
                $tempG['gC'] = $row['grnComment'];
                $tempG['isApproved'] = $row['isApprovedThroughWF'];
                $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($row['productID']);

                if ($isSelectedUomWise) {
                    $unitPrice = $row['grnProductPrice'];
                    $productQty = $row['grnProductTotalQty'];
                    $uomSelect = $row['grnProductUom'];
                    $uomData = $this->CommonTable('Settings/Model/UomTable')->getUom($uomSelect);
                    $uomAbbr = $uomData->uomAbbr;
                } else {
                    $unitPrice = $this->getProductUnitPriceViaDisplayUom($row['grnProductPrice'], $productUom);
                    $productQtyDetails = $this->getProductQuantityViaDisplayUom($row['grnProductTotalQty'], $productUom);
                    $productQty = $productQtyDetails['quantity'];
                    $uomSelect = $row['uomID'];
                    $uomAbbr = $productQtyDetails['uomAbbr'];
                }

                
                

                if($row['grnUploadFlag']){
                    if(isset($tempProduct[$rowCount]['bP'][$row['productBatchID']]) && $row['productSerialID']){

                    } else if(!$row['productBatchID'] && $row['productSerialID'] && $tempProduct[$rowCount]['lPID'] == $row['locationProductID']){

                    } else if($tempProduct[$rowCount]['lPID'] != $row['locationProductID']){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    } else if($tempProduct[$rowCount]['gPP'] != $unitPrice){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    }
                } else {
                    if($tempProduct[$rowCount]['lPID'] != $row['locationProductID']){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    } else if($tempProduct[$rowCount]['gPP'] != $unitPrice){
                        $rowCount++;
                        $serialData = [];
                        $btchData = [];
                        $taxDetails = [];
                    }

                }
                $discSymbol = null;
                if($row['grnProductDiscountType'] == 'value'){
                    $discSymbol = $this->companyCurrencySymbol;
                } else if($row['grnProductDiscountType'] == 'percentage') {
                    $discSymbol = '%';
                }

                $tempProduct[$rowCount] = array(
                    'gPC' => $row['productCode'],
                    'gPN' => $row['productName'],
                    'lPID' => $row['locationProductID'],
                    'gPP' => $unitPrice,
                    'gPD' => $row['grnProductDiscount'],
                    'gPQ' => $productQty,
                    'gPT' => $row['grnProductTotal'],
                    'gPUom' => $uomAbbr,
                    'gPUomDecimal' => $row['uomDecimalPlace'],
                    'gPDT' => $discSymbol,
                    'productID' => $row['productID'],
                    'productUomID' => $uomSelect,
                    'productPoID' => $row['grnProductDocumentId'],
                    'productType' => $row['productTypeID'],
                    'grnProductID' => $row['grnProductID'],
                    );

                if ($row['grnTaxID'] != NULL) {

                    $taxDetails[$row['grnTaxID']] = array(
                        'pTN' => $row['taxName'],
                        'pTP' => $row['grnTaxPrecentage'],
                        'pTA' => $row['grnTaxAmount'],
                        'TXID' => $row['grnTaxID'],

                        );
                    $tempProduct[$rowCount]['pT'] = $taxDetails;
                }

                if($row['productBatchID']){

                    $btchData[$row['productBatchID']] = array(
                        'bC' => $row['productBatchCode'],
                        'bED' => $row['productBatchExpiryDate'],
                        'bW' => $row['productBatchWarrantyPeriod'],
                        'bMD' => $row['productBatchManufactureDate'],
                        'bQ' => $row['grnProductQuantity'],
                        'bPrice' => $row['productBatchPrice'],
                        );
                    $tempProduct[$rowCount]['bP'] = $btchData;

                    if($row['productSerialID']){
                        $serialData[$row['productSerialID']] = array(
                            'sC' => $row['productSerialCode'],
                            'sW' => $row['productSerialWarrantyPeriod'],
                            'sED' => $row['productSerialExpireDate'],
                            'sBC' => $row['productBatchCode']
                            );
                        $tempProduct[$rowCount]['sP'] = $serialData;

                    }

                } else if($row['productSerialID']){
                    $serialData[$row['productSerialID']] = array(
                        'sC' => $row['productSerialCode'],
                        'sW' => $row['productSerialWarrantyPeriod'],
                        'sED' => $row['productSerialExpireDate'],
                        'sBC' => $row['productBatchCode']
                        );
                    $tempProduct[$rowCount]['sP'] = $serialData;
                }
                $tempG['gProducts'] = $tempProduct;
                $grnDetails[$row['grnID']] = $tempG;
                $grnDetails['GRNID'] = $grnID;
        }
        return $grnDetails;
    }
}
