<?php

namespace Reporting\Controller\API;

use Core\Controller\API\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class InvoiceReportController extends CoreController
{

    protected $userID;
    protected $salesByInvoiceTable;
    protected $productTable;
    protected $paymentsTable;
    protected $loggerTable;
    protected $userTable;
    protected $taxAmountTable;

    public function viewMonthlyInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromMonth = $request->getPost('fromMonth');
            $toMonth = $request->getPost('toMonth');
            $checkTaxStatus = $request->getPost('checkTaxStatus');

            $respondAmount = $this->getMonthlyInvoiceDetails($fromMonth, $toMonth);
            $companyDetails = $this->getCompanyDetails();

            $monthlyInvoiceView = new ViewModel(array(
                'monthlyInvoiceData' => $respondAmount,
                'checkTaxStatus' => $checkTaxStatus,
                'fromMonth' => $fromMonth,
                'toMonth' => $toMonth,
                'cD' => $companyDetails));


            $monthlyInvoiceView->setTemplate('reporting/invoice-report/generate-monthly-invoice-pdf');

            $this->html = $monthlyInvoiceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getMonthlyInvoiceDetails($fromMonth = null, $toMonth = null)
    {
        if (isset($fromMonth) && isset($toMonth)) {

            $respondAmount = $this->getSalesByInvoiceTable()->getMonthlyInvoiceData($fromMonth, $toMonth, $locationID = "");
            $respondTax = $this->getSalesByInvoiceTable()->getMonthlySalesTaxData($fromMonth, $toMonth, $locationID = "");
            $respond = array_replace_recursive($respondAmount, $respondTax);

            $respondYears = $this->getSalesByInvoiceTable()->getMonthlySalesDataYears($fromMonth, $toMonth, $locationID = "");
            $months = $this->getSalesByInvoiceTable()->getMonthsYearBetweenTwoDates($fromMonth, $toMonth, $locationID = "");

            $monthlySalesDetails = [];
            $years = $respondYears;

            foreach ($years as $year) {
                $monthlySalesDetails[$year] = array();
            }

            return $respondAmount;
        }
    }

    public function generateMonthlyInvoicePdfAction()
    {
        $request = $this->getRequest()->getQuery();
        if ($request) {
            $fromMonth = $request['fromMonth'];
            $toMonth = $request['toMonth'];
            $checkTaxStatus = $request['checkTaxStatus'];

            $monthlyInvoiceDetails = $this->getMonthlyInvoiceDetails($fromMonth, $toMonth);
            $cD = $this->getCompanyDetails();

            $viewMonthlyInvoice = new ViewModel(array(
                'cD' => $cD,
                'monthlyInvoiceData' => $monthlyInvoiceDetails,
                'fromMonth' => $fromMonth,
                'toMonth' => $toMonth,
                'checkTaxStatus' => $checkTaxStatus
            ));

            $viewMonthlyInvoice->setTemplate('reporting/invoice-report/generate-monthly-invoice-pdf');
            $viewMonthlyInvoice->setTerminal(true);

            // get rendered view into variable
            $htmlContent = $this->viewRendererHtmlContent($viewMonthlyInvoice);
            $this->downloadPDF('monthly-invoice-report', $htmlContent);

            exit;
        }
    }

    public function viewDailyInvoiceAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $fromDate = $request->getPost('fromDate');
            $toDate = $request->getPost('toDate');
            $checkTaxStatus = $request->getPost('checkTaxStatus');

            $dailyInvoiceData = $this->getDailyInvoiceDetails($fromDate, $toDate);
            $cD = $this->getCompanyDetails();

            $dailyInvoiceView = new ViewModel(array(
                'cD' => $cD,
                'dailyInvoiceData' => $dailyInvoiceData,
                'fromDate' => $fromDate,
                'toDate' => $toDate,
                'checkTaxStatus' => $checkTaxStatus
            ));

            $dailyInvoiceView->setTemplate('reporting/invoice-report/generate-daily-invoice-pdf');

            $this->html = $dailyInvoiceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getDailyInvoiceDetails($fromDate = null, $toDate = null)
    {
        if (isset($fromDate) && isset($toDate)) {
            $respond = $this->getSalesByInvoiceTable()->getDailyInvoiceData($fromDate, $toDate, $locationID = "");
            $respondDays = $this->getSalesByInvoiceTable()->getDailySalesDataDays($fromDate, $toDate);

            return $respond;
        }
    }

    public function viewAnnualInvoiceAction()
    {

        $request = $this->getRequest();

        if ($request->isPost()) {
            $year = $request->getPost('year');

            $checkTaxStatus = $request->getPost('checkTaxStatus');

            $annualInvoiceData = $this->getAnnualInvoiceDetails($year);
            $companyDetails = $this->getCompanyDetails();

            $annualInvoiceView = new ViewModel(array(
                'cD' => $companyDetails,
                'annualInvoiceData' => $annualInvoiceData,
                'year' => $year,
                'checkTaxStatus' => $checkTaxStatus
            ));

            $annualInvoiceView->setTemplate('reporting/invoice-report/generate-annual-invoice-pdf');

            $this->html = $annualInvoiceView;
            $this->status = true;
            return $this->JSONRespondHtml();
        }
    }

    public function getAnnualInvoiceDetails($year = null)
    {
        if (isset($year)) {
            $annualInvoiceWithTax = $this->getSalesByInvoiceTable()->getAnnualInvoiceData($year);

            return $annualInvoiceWithTax;
        }
    }

    public function getProductTable()
    {
        if (!$this->productTable) {
            $sm = $this->getServiceLocator();
            $this->productTable = $sm->get('Invoice\Model\ProductTable');
        }
        return $this->productTable;
    }

    public function getPayementsTable()
    {
        if (!$this->paymentsTable) {
            $sm = $this->getServiceLocator();
            $this->paymentsTable = $sm->get('Invoice\Model\PaymentsTable');
        }
        return $this->paymentsTable;
    }

    public function getSalesByInvoiceTable()
    {
        if (!$this->salesByInvoiceTable) {
            $sm = $this->getServiceLocator();
            $this->salesByInvoiceTable = $sm->get('Invoice\Model\InvoiceTable');
        }
        return $this->salesByInvoiceTable;
    }

    public function getLoggerTable()
    {
        if (!$this->loggerTable) {
            $sm = $this->getServiceLocator();
            $this->loggerTable = $sm->get('User\Model\LoggerTable');
        }
        return $this->loggerTable;
    }

    public function getUserTable()
    {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }

}
