<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains supplier Report related controller functions
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class SupplierReportController extends CoreController
{

    protected $userRoleID;
    protected $allLocations;
    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'purchasing_report_upper_menu';

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->userRoleID = $this->user_session->userActiveLocation['roleID'];
        $this->allLocations = $this->user_session->userAllLocations;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'purchasing_side_menu'.$this->packageID;
            $this->upperMenus = 'purchasing_report_upper_menu'.$this->packageID;
        }
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Supplier Report main tab
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Supplier', 'PURCHASING');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/supplier-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/accounting.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        $companyDetails = $this->getCompanyDetails();

        $this->setLogMessage("Supplier report create page accessed.");
        return new ViewModel(array(
            'com_data' => $companyDetails));
    }

}
