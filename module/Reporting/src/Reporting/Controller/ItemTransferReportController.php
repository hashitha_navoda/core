<?php

/**
 * @author SANDUN <sandun@thinkcube.com>
 * This file contains item transfer Report related controller functions
 */

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class ItemTransferReportController extends CoreController
{

    protected $userRoleID;
    protected $allLocations;
    protected $sideMenus = 'inventory_side_menu';
    protected $upperMenus = 'inve_report_upper_menu';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->userRoleID = $this->user_session->userActiveLocation['roleID'];
        $this->allLocations = $this->user_session->userAllLocations;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Item transfer Report main tab
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Item Transfer', 'INVENTORY');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/item-transfer-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/accounting.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');
        $this->getViewHelper('headLink')->prependStylesheet('/assets/bootstrap/css/bootstrap-select.min.css');

        $prc = array();
        $locations = array();

        $resLoc = $this->CommonTable('Core\Model\LocationTable')->activeFetchAll();
        foreach ($resLoc as $row) {
            $locations[$row->locationID] = $row;
        }

        $companyDetails = $this->getCompanyDetails();

        return new ViewModel(array('proList' => $prc,
            'locList' => $locations,
            'com_data' => $companyDetails));
    }

}
