<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class PaymentReportController extends CoreController
{

    protected $userRoleID;
    protected $allLocations;
    protected $sideMenus = 'purchasing_side_menu';
    protected $upperMenus = 'purchasing_report_upper_menu';

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->userRoleID = $this->user_session->userActiveLocation['roleID'];
        $this->allLocations = $this->user_session->userAllLocations;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'purchasing_side_menu'.$this->packageID;
            $this->upperMenus = 'purchasing_report_upper_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {

        $this->getSideAndUpperMenus('Reports', 'Payments', 'PURCHASING');
//        $this->getSideAndUpperMenus('Reports', 'Purchasing', 'Payments');
        $this->getSideAndUpperMenus('Reports', 'Payments');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/payment-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/assets/bootstrap/js/bootstrap-select.min.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');
        $this->getViewHelper('headLink')->prependStylesheet('/assets/bootstrap/css/bootstrap-select.min.css');

        $locations = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getLocationdata();
        $paymentType = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPaymentTypeData();

        $locationList = array();
        $paymentList = array();

        foreach ($locations as $p) {
            $locationList[$p['locationID']] = $p;
        }

        foreach ($paymentType as $pt) {
            $paymentList[$pt['paymentMethodID']] = $pt;
        }

        $this->setLogMessage("Supplier payment report create page accessed.");
        return new ViewModel(array(
            'locationList' => $locationList,
            'paymentList' => $paymentList));
    }

}
