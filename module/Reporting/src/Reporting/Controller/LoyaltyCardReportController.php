<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class LoyaltyCardReportController extends CoreController
{
    protected $sideMenus  = 'crm_side_menu';
    protected $upperMenus = 'crm_report_upper_menu';
    
    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'LoyalityCardReport', 'CRM');
        
        $company = $this->getCompanyDetails();
        
        $bankList = $this->CommonTable('Expenses\Model\BankTable')->getBankListForDropDown();
        
        return new ViewModel(array('company'=>$company,'banks'=>$bankList));
    }

}
