<?php

namespace Reporting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;


class QuotationReportController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_report_upper_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Reports', 'Quotation', 'SALES');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/common-report.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/report-pdf/quotation-report.js');
        $this->getViewHelper('headLink')->prependStylesheet('/css/report.css');

        //set payment types to the array
        $paymentTerm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = [];
        foreach ($paymentTerm as $pt) {
            $paymentTerms[$pt->paymentTermID] = $pt->paymentTermName;
        }
        
        //locations set to the array
        $locations = $this->allLocations;
        $location = [];
        foreach ($locations as $key => $value) {
            $location[$key] = $value['locationName'];
        }
        
        //quotation status values set to the array
        $quotationStatus = [];
        $quotationStatus[3] = 'Open';
        $quotationStatus[4] = 'Close';
        $quotationStatus[5] = 'Cancel';

        //Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }
        

        //get CustomerCategory list
        $customerCategory = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        
        return new ViewModel([
            'paymentTypes' => $paymentTerms,
            'locations' => $location,
            'quotStatus' => $quotationStatus,
            'salesPersons' => $salesPersons,
            'cusCategory' => $customerCategory
        ]);
    }

}
