<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains UnitMeasureTable Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\UnitMeasureTable;
use Invoice\Model\UnitMeasure;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class UnitMeasureTableTest extends PHPUnit_Framework_TestCase
{

    public function testFetchAllReturnsAllUnitMeasures()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $unitMeasureTable = new UnitMeasureTable($mockTableGateway);

        $this->assertSame($resultSet, $unitMeasureTable->fetchAll());
    }

    public function testCanRetrieveAnUnitMeasureByItsId()
    {
        $unitMeasure = new UnitMeasure();
        $unitMeasure->exchangeArray(array('unitOfMeasuresId' => 123,
            'name' => 'kilo gram',
            'abbrevation' => 'kg',
            'decimalPlace' => '3',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new UnitMeasure());
        $resultSet->initialize(array($unitMeasure));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $unitMeasureTable = new UnitMeasureTable($mockTableGateway);

        $this->assertSame($unitMeasure, $unitMeasureTable->getUnitMeasure(123));
    }

    public function testSaveUnitMeasureWillInsertNewUnitMeasuresIfTheyDontAlreadyHaveAnId()
    {
        $unitMeasureData = array(
            'unitOfMeasuresId' => 123,
            'name' => 'kilo gram',
            'abbrevation' => 'kg',
            'decimalPlace' => '3',
        );
        $unitMeasure = new UnitMeasure();
        $unitMeasure->exchangeArray($unitMeasureData);

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new UnitMeasure());
        $resultSet->initialize(array($unitMeasure));



        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select', 'insert'), array(), '', false);
        $mockTableGateway->expects($this->any())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));
        $mockTableGateway->expects($this->any())
                ->method('insert')
                ->with($unitMeasureData);

        $unitMeasureTable = new UnitMeasureTable($mockTableGateway);
        $unitMeasureTable->saveUnitMeasure($unitMeasure);
    }

    public function testExceptionIsThrownWhenGettingNonexistentUnitMeasure()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new UnitMeasure());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $unitMeasureTable = new UnitMeasureTable($mockTableGateway);

        try {
            $unitMeasureTable->getUnitMeasure(123);
        } catch (\Exception $e) {
            $this->assertSame('Could not find row 123', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }

}

