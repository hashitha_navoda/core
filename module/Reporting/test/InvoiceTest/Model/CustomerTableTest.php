<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains Customer related testing functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\CustomerTable;
use Invoice\Model\Customer;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class CustomerTableTest extends PHPUnit_Framework_TestCase
{

    /**
     * This function tests Customer Fetch All
     */
    public function testFetchAllReturnsAllCustomers()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $customerTable = new CustomerTable($mockTableGateway);

        $this->assertSame($resultSet, $customerTable->fetchAll());
    }

    /**
     * This function tests fetch customer by ID
     */
    public function testCanRetrieveACustomerByHisId()
    {
        $customer = new Customer();
        $customer->exchangeArray(array(
            'customerID' => 9,
            'customerName' => 'Damith Thamara',
            'customerShortName' => 'damiya',
            'currency' => '1',
            'telephoneNumber' => '94716338274',
            'email' => 'damith.thamara@gmail.com',
            'address' => '69,katukurunda,henegama',
            'vatNumber' => 658,
            'paymentTerm' => 0,
            'currentBalance' => 120000,
            'creditLimit' => 200000,
            'discount' => 69,
            'other' => 'primary',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Customer());
        $resultSet->initialize(array($customer));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('customerID' => 9))
                ->will($this->returnValue($resultSet));

        $customerTable = new CustomerTable($mockTableGateway);

        $this->assertSame($customer, $customerTable->getCustomerByID(9));
    }

    /**
     * This function fetch a customer by his name
     */
    public function testCanRetrieveACustomerByHisName()
    {
        $customer = new Customer();
        $customer->exchangeArray(array(
            'customerID' => 9,
            'customerName' => 'Damith Thamara',
            'customerShortName' => 'damiya',
            'currency' => '1',
            'telephoneNumber' => '94716338274',
            'email' => 'damith.thamara@gmail.com',
            'address' => '69,katukurunda,henegama',
            'vatNumber' => 658,
            'paymentTerm' => 0,
            'currentBalance' => 120000,
            'creditLimit' => 200000,
            'discount' => 69,
            'other' => 'primary',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Customer());
        $resultSet->initialize(array($customer));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('customerName' => 'Damith Thamara'))
                ->will($this->returnValue($resultSet));

        $customerTable = new CustomerTable($mockTableGateway);

        $this->assertSame($customer, $customerTable->getCustomerByName($customer->customerName));
    }

    /**
     * This test will fetch a customer bu his Short Name
     */
    public function testCanRetrieveACustomerByHisShortName()
    {
        $customer = new Customer();
        $customer->exchangeArray(array(
            'customerID' => 9,
            'customerName' => 'Damith Thamara',
            'customerShortName' => 'damiya',
            'currency' => '1',
            'telephoneNumber' => '94716338274',
            'email' => 'damith.thamara@gmail.com',
            'address' => '69,katukurunda,henegama',
            'vatNumber' => 658,
            'paymentTerm' => 0,
            'currentBalance' => 120000,
            'creditLimit' => 200000,
            'discount' => 69,
            'other' => 'primary',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Customer());
        $resultSet->initialize(array($customer));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('customerShortName' => 'damiya'))
                ->will($this->returnValue($resultSet));

        $customerTable = new CustomerTable($mockTableGateway);

        $this->assertSame($customer, $customerTable->getCustomerByShortname($customer->customerShortName));
    }

    /**
     * This function test Delete customer by his CustomerID
     */
    public function testCanDeleteACustomerByItsId()
    {
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('delete'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('delete')
                ->with(array('customerID' => 163));

        $customerTable = new CustomerTable($mockTableGateway);
        $customerTable->deleteCustomer(163);
    }

    /**
     * This function tests update customer By his CustomerID
     */
    public function testCanUpdateACustomerByHisId()
    {
        $customer = new Customer();
        $customer->exchangeArray(array(
            'customerID' => 9,
            'customerName' => 'Damith Thamara',
            'customerShortName' => 'damiya',
            'currency' => '1',
            'telephoneNumber' => '94716338274',
            'email' => 'damith.thamara@gmail.com',
            'address' => '69,katukurunda,henegama',
            'vatNumber' => 658,
            'paymentTerm' => 0,
            'currentBalance' => 120000,
            'creditLimit' => 200000,
            'discount' => 69,
            'other' => 'primary',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Customer());
        $resultSet->initialize(array($customer));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select', 'update'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('customerID' => 9))
                ->will($this->returnValue($resultSet));
        $mockTableGateway->expects($this->once())
                ->method('update')
                ->with(array(
                    'customerID' => 9,
                    'customerName' => 'Damith Thamara',
                    'customerShortName' => 'damiya',
                    'currency' => '1',
                    'telephoneNumber' => '94716338274',
                    'email' => 'damith.thamara@gmail.com',
                    'address' => '69,katukurunda,henegama',
                    'vatNumber' => 658,
                    'paymentTerm' => 0,
                    'currentBalance' => 120000,
                    'creditLimit' => 200000,
                    'discount' => 69,
                    'other' => 'primary',), array('customerID' => 9));
        $customerTable = new CustomerTable($mockTableGateway);

        $customerTable->updateCustomer($customer);
    }

    public function testCanInsertACustomer()
    {
        $customer = new Customer();
        $customer->exchangeArray(array(
            'customerID' => 9,
            'customerName' => 'Damith Thamara',
            'customerShortName' => 'dam',
            'currency' => '1',
            'telephoneNumber' => '94716338274',
            'email' => 'damith.thamara@gmail.com',
            'address' => '69,katukurunda,henegama',
            'vatNumber' => 658,
            'paymentTerm' => 0,
            'currentBalance' => 120000,
            'creditLimit' => 200000,
            'discount' => 69,
            'other' => 'primary',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Customer());
        $resultSet->initialize(array($customer));
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select', 'insert'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('customerShortName' => 'dam'))
                ->will($this->returnValue($resultSet));
        $mockTableGateway->expects($this->any())
                ->method('insert')
                ->with(array(
                    'customerID' => 9,
                    'customerName' => 'Damith Thamara',
                    'customerShortName' => 'dam',
                    'currency' => '1',
                    'telephoneNumber' => '94716338274',
                    'email' => 'damith.thamara@gmail.com',
                    'address' => '69,katukurunda,henegama',
                    'vatNumber' => 658,
                    'paymentTerm' => 0,
                    'currentBalance' => 120000,
                    'creditLimit' => 200000,
                    'discount' => 69,
                    'other' => 'primary'));
        $customerTable = new CustomerTable($mockTableGateway);
        $customerTable->saveCustomer($customer);
    }

}

