<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains ProductTaxTable Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\ProductTaxTable;
use Invoice\Model\ProductTax;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class ProductTaxTableTest extends PHPUnit_Framework_TestCase
{

    public function testFetchAllReturnsAllProductTaxs()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $productTaxTable = new ProductTaxTable($mockTableGateway);

        $this->assertSame($resultSet, $productTaxTable->fetchAll());
    }

    public function testCanRetrieveAnProductTaxTableByItsId()
    {
//        $productTax = new ProductTax();
//        $productTax->exchangeArray(array(
//            'taxID' => 123,
//            'productID' => 234,
//        ));
//        $resultSet = new ResultSet();
//        $resultSet->setArrayObjectPrototype(new ProductTax());
//        $resultSet->initialize(array($productTax));
//        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
//        $mockTableGateway->expects($this->once())
//                ->method('select')
//                ->with(array('productID' => 123))
//                ->will($this->returnValue($resultSet));
//
//        $productTaxTable = new ProductTaxTable($mockTableGateway);
//
//        $this->assertSame($productTax, $productTaxTable->getProductTax(123));
    }

    public function testSaveProductTaxWillInsertNewProductTaxsIfTheyDontAlreadyHaveAnId()
    {
        $productTaxData = array(
            'taxID' => 123,
            'productID' => 234);
//        $productTax = new ProductTax();
//        $productTax->exchangeArray($productTaxData);

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('insert'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('insert')
                ->with($productTaxData);

        $productTaxTable = new ProductTaxTable($mockTableGateway);
        $productTaxTable->saveProductTax($productTaxData);
    }

    public function testExceptionIsThrownWhenGettingNonexistentProductTax()
    {
//        $resultSet = new ResultSet();
//        $resultSet->setArrayObjectPrototype(new ProductTax());
//        $resultSet->initialize(array());
//
//        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
//        $mockTableGateway->expects($this->once())
//                ->method('select')
//                ->with(array('productID' => 234))
//                ->will($this->returnValue($resultSet));
//
//        $productTaxTable = new ProductTaxTable($mockTableGateway);
//
//        try {
//            $productTaxTable->getProductTax(123);
//        } catch (\Exception $e) {
//            $this->assertSame('Could not find row 123', $e->getMessage());
//            return;
//        }
//
//        $this->fail('Expected exception was not thrown');
    }

}

