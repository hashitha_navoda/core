<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Supplier Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\Supplier;
use PHPUnit_Framework_TestCase;

class SupplierTest extends PHPUnit_Framework_TestCase
{

    public function testSupplierInitialState()
    {
        $supplier = new Supplier();

        $this->assertNull($supplier->id, '"id" should initially be null');
        $this->assertNull($supplier->name, '"name" should initially be null');
        $this->assertNull($supplier->address, '"address" should initially be null');
        $this->assertNull($supplier->telephoneNumber, '"telephoneNumber" should initially be null');
        $this->assertNull($supplier->email, '"email" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $supplier = new Supplier();
        $data = array(
            'id' => 123,
            'name' => 'some name',
            'address' => 'some address',
            'telephoneNumber' => 'some telephoneNumber',
            'email' => 'some email',
        );


        $supplier->exchangeArray($data);

        $this->assertSame($data['id'], $supplier->id, '"id" was not set correctly');
        $this->assertSame($data['name'], $supplier->name, '"name" was not set correctly');
        $this->assertSame($data['address'], $supplier->address, '"address" was not set correctly');
        $this->assertSame($data['telephoneNumber'], $supplier->telephoneNumber, '"telephoneNumber" was not set correctly');
        $this->assertSame($data['email'], $supplier->email, '"email" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $supplier = new Supplier();

        $supplier->exchangeArray(array(
            'id' => 123,
            'name' => 'some name',
            'address' => 'some address',
            'telephoneNumber' => 'some telephoneNumber',
            'email' => 'some email',
        ));
        $supplier->exchangeArray(array());

        $this->assertNull($supplier->id, '"id" should have defaulted to null');
        $this->assertNull($supplier->name, '"name" should have defaulted to null');
        $this->assertNull($supplier->address, '"address" should have defaulted to null');
        $this->assertNull($supplier->telephoneNumber, '"telephoneNumber" should have defaulted to null');
        $this->assertNull($supplier->email, '"email" should have defaulted to null');
    }

}

