<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Company Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\Company;
use PHPUnit_Framework_TestCase;

class CompanyTest extends PHPUnit_Framework_TestCase
{

    public function testCompanyInitialState()
    {
        $company = new Company();

        $this->assertNull($company->id, '"id" should initially be null');
        $this->assertNull($company->companyName, '"companyName" should initially be null');
        $this->assertNull($company->companyAddress, '"companyAddress" should initially be null');
        $this->assertNull($company->postalCode, '"postalCode" should initially be null');
        $this->assertNull($company->telephoneNumber, '"telephoneNumber" should initially be null');
        $this->assertNull($company->email, '"email" should initially be null');
        $this->assertNull($company->country, '"country" should initially be null');
        $this->assertNull($company->website, '"website" should initially be null');
        $this->assertNull($company->brNumber, '"brNumber" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $company = new Company();
        $data = array('companyName' => 'some companyName',
            'id' => 123,
            'companyAddress' => 'some companyAddress',
            'postalCode' => 'some postalCode',
            'telephoneNumber' => 'some telephoneNumber',
            'email' => 'some email',
            'country' => 'some country',
            'website' => 'some website',
            'brNumber' => 'some brNumber',);


        $company->exchangeArray($data);

        $this->assertSame($data['id'], $company->id, '"id" was not set correctly');
        $this->assertSame($data['companyName'], $company->companyName, '"companyName" was not set correctly');
        $this->assertSame($data['companyAddress'], $company->companyAddress, '"companyAddress" was not set correctly');
        $this->assertSame($data['postalCode'], $company->postalCode, '"postalCode" was not set correctly');
        $this->assertSame($data['telephoneNumber'], $company->telephoneNumber, '"telephoneNumber" was not set correctly');
        $this->assertSame($data['email'], $company->email, '"email" was not set correctly');
        $this->assertSame($data['country'], $company->country, '"country" was not set correctly');
        $this->assertSame($data['website'], $company->website, '"website" was not set correctly');
        $this->assertSame($data['brNumber'], $company->brNumber, '"brNumber" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $company = new Company();

        $company->exchangeArray(array(
            'companyName' => 'some companyName',
            'id' => 123,
            'companyAddress' => 'some companyAddress',
            'postalCode' => 'some postalCode',
            'telephoneNumber' => 'some telephoneNumber',
            'email' => 'some email',
            'country' => 'some country',
            'website' => 'some website',
            'brNumber' => 'some brNumber'));
        $company->exchangeArray(array());

        $this->assertNull($company->id, '"id" should have defaulted to null');
        $this->assertNull($company->companyName, '"companyName" should have defaulted to null');
        $this->assertNull($company->companyAddress, '"companyAddress" should have defaulted to null');
        $this->assertNull($company->postalCode, '"postalCode" should have defaulted to null');
        $this->assertNull($company->telephoneNumber, '"telephoneNumber" should have defaulted to null');
        $this->assertNull($company->email, '"email" should have defaulted to null');
        $this->assertNull($company->country, '"country" should have defaulted to null');
        $this->assertNull($company->website, '"website" should have defaulted to null');
        $this->assertNull($company->brNumber, '"brNumber" should have defaulted to null');
    }

}

