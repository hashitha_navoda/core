<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Tax Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\Tax;
use PHPUnit_Framework_TestCase;

class TaxTest extends PHPUnit_Framework_TestCase
{

    public function testTaxInitialState()
    {
        $tax = new Tax();

        $this->assertNull($tax->id, '"id" should initially be null');
        $this->assertNull($tax->taxName, '"taxName" should initially be null');
        $this->assertNull($tax->taxType, '"taxType" should initially be null');
        $this->assertNull($tax->taxPrecentage, '"taxPrecentage" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $tax = new Tax();
        $data = array(
            'id' => 123,
            'taxName' => 'some taxName',
            'taxType' => 'some taxType',
            'taxPrecentage' => 'some taxPrecentage',
        );


        $tax->exchangeArray($data);

        $this->assertSame($data['id'], $tax->id, '"id" was not set correctly');
        $this->assertSame($data['taxName'], $tax->taxName, '"taxName" was not set correctly');
        $this->assertSame($data['taxType'], $tax->taxType, '"taxType" was not set correctly');
        $this->assertSame($data['taxPrecentage'], $tax->taxPrecentage, '"taxPrecentage" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $tax = new Tax();

        $tax->exchangeArray(array(
            'id' => 123,
            'taxName' => 'some taxName',
            'taxType' => 'some taxType',
            'taxPrecentage' => 'some taxPrecentage',));
        $tax->exchangeArray(array());

        $this->assertNull($tax->id, '"id" should have defaulted to null');
        $this->assertNull($tax->taxName, '"taxName" should have defaulted to null');
        $this->assertNull($tax->taxType, '"taxType" should have defaulted to null');
        $this->assertNull($tax->taxPrecentage, '"taxPrecentage" should have defaulted to null');
    }

}

