<?php

namespace InvoiceTest\Model;

use Invoice\Model\Customer;
use PHPUnit_Framework_TestCase;

class CustomerTest extends PHPUnit_Framework_TestCase
{

    public function testCustomerInitialState()
    {
        $customer = new Customer();

        $this->assertNull($customer->customerID, '"customerID" should initially be null');
        $this->assertNull($customer->customerName, '"customerName" should initially be null');
        $this->assertNull($customer->customerShortName, '"customerShortName" should initially be null');
        $this->assertNull($customer->currency, '"currency" should initially be null');
        $this->assertNull($customer->telephoneNumber, '"telephoneNumber" should initially be null');
        $this->assertNull($customer->email, '"email" should initially be null');
        $this->assertNull($customer->address, '"address" should initially be null');
        $this->assertNull($customer->vatNumber, '"vatNumber" should initially be null');
        $this->assertNull($customer->paymentTerm, '"paymentTerm" should initially be null');
        $this->assertNull($customer->creditLimit, '"creditLimit" should initially be null');
        $this->assertNull($customer->currentBalance, '"currentBalance" should initially be null');
        $this->assertNull($customer->discount, '"discount" should initially be null');
        $this->assertNull($customer->other, '"other" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $customer = new Customer();
        $data = array(
            'customerID' => 58,
            'customerName' => 'testcustomer',
            'customerShortName' => 'testcus',
            'currency' => '1',
            'telephoneNumber' => '0716225874',
            'email' => 'testcustomer@thinkcube.com',
            'address' => 'Rajagiriya',
            'vatNumber' => 5689,
            'paymentTerm' => 2,
            'currentBalance' => 102000,
            'creditLimit' => 500000,
            'discount' => 13,
            'other' => 'tall',
        );

        $customer->exchangeArray($data);

        $this->assertSame($data['customerID'], $customer->customerID, '"customerID" was not set correctly');
        $this->assertSame($data['customerName'], $customer->customerName, '"customerName" was not set correctly');
        $this->assertSame($data['customerShortName'], $customer->customerShortName, '"customerShortName" was not set correctly');
        $this->assertSame($data['currency'], $customer->currency, '"currency" was not set correctly');
        $this->assertSame($data['telephoneNumber'], $customer->telephoneNumber, '"telephoneNumber" was not set correctly');
        $this->assertSame($data['email'], $customer->email, '"email" was not set correctly');
        $this->assertSame($data['address'], $customer->address, '"address" was not set correctly');
        $this->assertSame($data['vatNumber'], $customer->vatNumber, '"vatNumber" was not set correctly');
        $this->assertSame($data['paymentTerm'], $customer->paymentTerm, '"paymentTerm" was not set correctly');
        $this->assertSame($data['creditLimit'], $customer->creditLimit, '"creditLimit" was not set correctly');
        $this->assertSame($data['currentBalance'], $customer->currentBalance, '"currentBalance" was not set correctly');
        $this->assertSame($data['discount'], $customer->discount, '"discount" was not set correctly');
        $this->assertSame($data['other'], $customer->other, '"other" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $customer = new Customer();

        $customer->exchangeArray(array(
            'customerID' => 58,
            'customerName' => 'testcustomer',
            'customerShortName' => 'testcus',
            'currency' => '1',
            'telephoneNumber' => '0716225874',
            'email' => 'testcustomer@thinkcube.com',
            'address' => 'Rajagiriya',
            'vatNumber' => 5689,
            'paymentTerm' => 2,
            'currentBalance' => 102000,
            'creditLimit' => 500000,
            'discount' => 13,
            'other' => 'tall',
        ));

        $customer->exchangeArray(array());

        $this->assertNull($customer->customerID, '"customerID" should have defaulted to null');
        $this->assertNull($customer->customerName, '"customerName" should have defaulted to null');
        $this->assertNull($customer->customerShortName, '"customerShortName" should have defaulted to null');
        $this->assertNull($customer->currency, '"currency" should have defaulted to null');
        $this->assertNull($customer->telephoneNumber, '"telephoneNumber" should have defaulted to null');
        $this->assertNull($customer->email, '"email" should have defaulted to null');
        $this->assertNull($customer->address, '"address" should have defaulted to null');
        $this->assertNull($customer->vatNumber, '"vatNumber" should have defaulted to null');
        $this->assertNull($customer->paymentTerm, '"paymentTerm" should have defaulted to null');
        $this->assertNull($customer->creditLimit, '"creditLimit" should have defaulted to null');
        $this->assertNull($customer->currentBalance, '"currentBalance" should have defaulted to null');
        $this->assertNull($customer->discount, '"discount" should have defaulted to null');
        $this->assertNull($customer->other, '"other" should have defaulted to null');
    }

}
?>