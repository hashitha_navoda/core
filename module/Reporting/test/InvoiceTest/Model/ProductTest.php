<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Product Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\Product;
use PHPUnit_Framework_TestCase;

class ProductTest extends PHPUnit_Framework_TestCase
{

    public function testProductInitialState()
    {
        $product = new Product();

        $this->assertNull($product->id, '"id" should initially be null');
        $this->assertNull($product->productCode, '"productCode" should initially be null');
        $this->assertNull($product->productName, '"productName" should initially be null');
        $this->assertNull($product->description, '"description" should initially be null');
        $this->assertNull($product->category, '"category" should initially be null');
        $this->assertNull($product->unitOfMeasure, '"unitOfMeasure" should initially be null');
        $this->assertNull($product->price, '"price" should initially be null');
        $this->assertNull($product->discount, '"discount" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $product = new Product();
        $data = array('productCode' => 'some productCode',
            'id' => 123,
            'productName' => 'some productName',
            'description' => 'some description',
            'category' => 'some category',
            'unitOfMeasure' => 'some unitOfMeasure',
            'price' => 'some price',
            'discount' => 'some discount',
        );


        $product->exchangeArray($data);

        $this->assertSame($data['id'], $product->id, '"id" was not set correctly');
        $this->assertSame($data['productCode'], $product->productCode, '"productCode" was not set correctly');
        $this->assertSame($data['productName'], $product->productName, '"productName" was not set correctly');
        $this->assertSame($data['description'], $product->description, '"description" was not set correctly');
        $this->assertSame($data['category'], $product->category, '"category" was not set correctly');
        $this->assertSame($data['unitOfMeasure'], $product->unitOfMeasure, '"unitOfMeasure" was not set correctly');
        $this->assertSame($data['price'], $product->price, '"price" was not set correctly');
        $this->assertSame($data['discount'], $product->discount, '"discount" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $product = new Product();

        $product->exchangeArray(array(
            'productCode' => 'some productCode',
            'id' => 123,
            'productName' => 'some productName',
            'description' => 'some description',
            'category' => 'some category',
            'unitOfMeasure' => 'some unitOfMeasure',
            'price' => 'some price',
            'discount' => 'some discount',
        ));
        $product->exchangeArray(array());

        $this->assertNull($product->id, '"id" should have defaulted to null');
        $this->assertNull($product->productCode, '"productCode" should have defaulted to null');
        $this->assertNull($product->productName, '"productName" should have defaulted to null');
        $this->assertNull($product->description, '"description" should have defaulted to null');
        $this->assertNull($product->category, '"category" should have defaulted to null');
        $this->assertNull($product->unitOfMeasure, '"unitOfMeasure" should have defaulted to null');
        $this->assertNull($product->price, '"price" should have defaulted to null');
        $this->assertNull($product->discount, '"discount" should have defaulted to null');
    }

}

