<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Reference Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\Reference;
use PHPUnit_Framework_TestCase;

class ReferenceTest extends PHPUnit_Framework_TestCase
{

    public function testReferenceInitialState()
    {
        $reference = new Reference();

        $this->assertNull($reference->referenceType, '"referenceType" should initially be null');
        $this->assertNull($reference->currentReference, '"currentReference" should initially be null');
        $this->assertNull($reference->nextReference, '"nextReference" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $reference = new Reference();
        $data = array(
            'referenceType' => 123,
            'currentReference' => 'some currentReference',
            'nextReference' => 'some nextReference',
        );


        $reference->exchangeArray($data);

        $this->assertSame($data['referenceType'], $reference->referenceType, '"referenceType" was not set correctly');
        $this->assertSame($data['currentReference'], $reference->currentReference, '"currentReference" was not set correctly');
        $this->assertSame($data['nextReference'], $reference->nextReference, '"nextReference" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $reference = new Reference();

        $reference->exchangeArray(array(
            'referenceType' => 123,
            'currentReference' => 'some currentReference',
            'nextReference' => 'some nextReference',
        ));
        $reference->exchangeArray(array());

        $this->assertNull($reference->referenceType, '"referenceType" should have defaulted to null');
        $this->assertNull($reference->currentReference, '"currentReference" should have defaulted to null');
        $this->assertNull($reference->nextReference, '"nextReference" should have defaulted to null');
    }

}

