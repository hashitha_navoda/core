<?php

namespace InvoiceTest\Model;

use Invoice\Model\Quotation;
use PHPUnit_Framework_TestCase;

class QuotationTest extends PHPUnit_Framework_TestCase
{

    public function testQuotationInitialState()
    {
        $quotation = new Quotation();

        $this->assertNull($quotation->customer_name, '"customer_name" should initially be null');
        $this->assertNull($quotation->payment_term, '"payment_term" should initially be null');
        $this->assertNull($quotation->issue_date, '"issue_date" should initially be null');
        $this->assertNull($quotation->expire_date, '"expire_date" should initially be null');
        $this->assertNull($quotation->quotation_no, '"quotation_no" should initially be null');
        $this->assertNull($quotation->comment, '"comment" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $quotation = new Quotation();
        $data = array(
            'qot_no' => 'test_1',
            'issue_date' => '10/1/2013',
            'expire_date' => '10/1/2013',
            'cust_name' => "Prathap",
            'cust_id' => 'test_22',
            'payment_term' => 2,
            'cmnt' => 'dddddddddddddddddddddd',
            'tax' => 230.22,
            'total_discount' => 1000.10,
            'total_amount' => 150000.50,
            'branch' => "Wadduwa",
        );

        $quotation->exchangeArray($data);

        $this->assertSame($data['qot_no'], $quotation->quotation_no, '"qot_no" was not set correctly');
        $this->assertSame($data['issue_date'], $quotation->issue_date, '"issue_date" was not set correctly');
        $this->assertSame($data['expire_date'], $quotation->expire_date, '"expire_date" was not set correctly');
        $this->assertSame($data['cust_name'], $quotation->customer_name, '"cust_name" was not set correctly');
//        $this->assertSame($data['cust_id'], $quotation-> , '"cust_id" was not set correctly');
        $this->assertSame($data['payment_term'], $quotation->payment_term, '"payment_term" was not set correctly');
        $this->assertSame($data['cmnt'], $quotation->comment, '"cmnt" was not set correctly');
        $this->assertSame($data['tax'], $quotation->tax, '"tax" was not set correctly');
        $this->assertSame($data['total_discount'], $quotation->total_discount, '"total_discount" was not set correctly');
        $this->assertSame($data['total_amount'], $quotation->total, '"total_amount" was not set correctly');
//        $this->assertSame($data['branch'], $quotation-> , '"branch" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $quotation = new Quotation();

        $quotation->exchangeArray(array(
            'qot_no' => 'test_1',
            'issue_date' => '10/1/2013',
            'expire_date' => '10/1/2013',
            'cust_name' => "Prathap",
            'cust_id' => 'test_22',
            'payment_term' => 2,
            'cmnt' => 'dddddddddddddddddddddd',
            'tax' => 230.22,
            'total_discount' => 1000.10,
            'total_amount' => 150000.50,
            'branch' => "Wadduwa",
        ));

        $quotation->exchangeArray(array());

        $this->assertNull($quotation->quotation_no, ' "qot_no" should have defaulted to null');
        $this->assertNull($quotation->issue_date, ' "issue_date" should have defaulted to null');
        $this->assertNull($quotation->expire_date, ' "expire_date" should have defaulted to null');
        $this->assertNull($quotation->customer_name, '"cust_name" should have defaulted to null');
//        $this->assertNull($quotation-> , '"cust_id" should have defaulted to null');
        $this->assertNull($quotation->payment_term, '"payment_term" should have defaulted to null');
        $this->assertNull($quotation->comment, '"cmnt" should have defaulted to null');
        $this->assertNull($quotation->tax, '"tax" should have defaulted to null');
        $this->assertNull($quotation->total_discount, '"total_discount" should have defaulted to null');
        $this->assertNull($quotation->total, '"total_amount" should have defaulted to null');
//        $this->assertNull($quotation-> , '"branch" should have defaulted to null');
    }

    public function testGetArrayCopyReturnsAnArrayWithPropertyValues()
    {
        $quotation = new Quotation();
        $data = array(
            'qot_no' => 'test_1',
            'issue_date' => '10/1/2013',
            'expire_date' => '10/1/2013',
            'cust_name' => "Prathap",
            'cust_id' => 'test_22',
            'payment_term' => 2,
            'cmnt' => 'dddddddddddddddddddddd',
            'tax' => 230.22,
            'total_discount' => 1000.10,
            'total_amount' => 150000.50,
            'branch' => "Wadduwa",
        );

        $quotation->exchangeArray($data);
        $copyArray = $quotation->getArrayCopy();

        $this->assertSame($data['qot_no'], $copyArray['quotation_no'], '"qot_no" was not set correctly');
        $this->assertSame($data['issue_date'], $copyArray['issue_date'], '"issue_date" was not set correctly');
        $this->assertSame($data['expire_date'], $copyArray['expire_date'], '"expire_date" was not set correctly');
        $this->assertSame($data['cust_name'], $copyArray['customer_name'], '"cust_name" was not set correctly');
//        $this->assertSame($data['cust_id'], $copyArray[''], '"cust_id" was not set correctly');
        $this->assertSame($data['payment_term'], $copyArray['payment_term'], '"payment_term" was not set correctly');
        $this->assertSame($data['cmnt'], $copyArray['comment'], '"cmnt" was not set correctly');
        $this->assertSame($data['tax'], $copyArray['tax'], '"tax" was not set correctly');
        $this->assertSame($data['total_discount'], $copyArray['total_discount'], '"total_discount" was not set correctly');
        $this->assertSame($data['total_amount'], $copyArray['total'], '"total_amount" was not set correctly');
//        $this->assertSame($data['branch'], $copyArray[''] , '"branch" was not set correctly');
    }

    public function testInputFiltersAreSetCorrectly()
    {
        $quotation = new Quotation();

        $inputFilter = $quotation->getInputFilter();

        $this->assertSame(4, $inputFilter->count());
        $this->assertTrue($inputFilter->has('qot_no'));
        $this->assertTrue($inputFilter->has('cust_name'));
        $this->assertTrue($inputFilter->has('issue_date'));
        $this->assertTrue($inputFilter->has('expire_date'));
    }

}

