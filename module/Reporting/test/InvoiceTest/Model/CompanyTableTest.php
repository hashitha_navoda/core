<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains CompanyTable Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\CompanyTable;
use Invoice\Model\Company;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class CompanyTableTest extends PHPUnit_Framework_TestCase
{

// TO-DO licenceCount column added to company table, so need to modify the TESTs
    public function testFetchAllReturnsAllCompanys()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $companyTable = new CompanyTable($mockTableGateway);

        $this->assertSame($resultSet, $companyTable->fetchAll());
    }

    public function testCanRetrieveAnCompanyByItsId()
    {
        $company = new Company();
        $company->exchangeArray(array('id' => 123,
            'companyName' => 'VHAMS',
            'companyAddress' => '316/B Old kandy road dalugama kelaniya',
            'postalCode' => '12121',
            'telephoneNumber' => '0783827226',
            'email' => 'ashanmadushkaperera@gmail.com',
            'country' => 'Sri Lanka',
            'website' => 'VHAMS',
            'brNumber' => '21212'));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Company());
        $resultSet->initialize(array($company));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $companyTable = new CompanyTable($mockTableGateway);

        $this->assertSame($company, $companyTable->getCompany(123));
    }

    public function testSaveCompanyWillInsertNewCompanysIfTheyDontAlreadyHaveAnId()
    {
        $companyData = array('companyName' => 'VHAMS',
            'companyAddress' => '316/B Old kandy road dalugama kelaniya',
            'postalCode' => '12121',
            'telephoneNumber' => '0783827226',
            'email' => 'ashanmadushkaperera@gmail.com',
            'country' => 'Sri Lanka',
            'website' => 'VHAMS',
            'brNumber' => '21212');
        $company = new Company();
        $company->exchangeArray($companyData);

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('insert'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('insert')
                ->with($companyData);

        $companyTable = new CompanyTable($mockTableGateway);
        $companyTable->saveCompany($company);
    }

    public function testExceptionIsThrownWhenGettingNonexistentCompany()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Company());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $companyTable = new CompanyTable($mockTableGateway);

        try {
            $companyTable->getCompany(123);
        } catch (\Exception $e) {
            $this->assertSame('Could not find row 123', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }

}
