<?php

namespace InvoiceTest\Model;

use Invoice\Model\QuotationReceipt;
use PHPUnit_Framework_TestCase;

class QuotationReceiptTest extends PHPUnit_Framework_TestCase
{

    public function testQuotationReceiptInitialState()
    {
        $quotationReceipt = new QuotationReceipt();

        $this->assertNull($quotationReceipt->quotation_id, '"quotation_id" should initially be null');
        $this->assertNull($quotationReceipt->product_code, '"product_code" should initially be null');
        $this->assertNull($quotationReceipt->product_name, '"product_name" should initially be null');
        $this->assertNull($quotationReceipt->unit_price, '"unit_price" should initially be null');
        $this->assertNull($quotationReceipt->quantity, '"quantity" should initially be null');
        $this->assertNull($quotationReceipt->discount, '"discount" should initially be null');
        $this->assertNull($quotationReceipt->price, '"price" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $quotationReceipt = new QuotationReceipt();
        $data = array(
            'qot_no' => 'test_1',
            'item_code' => 'itm_01',
            'item' => 'Testing Item',
            'unit_price' => 22500.50,
            'quantity' => 10,
            'discount' => 2,
            'total_cost' => 225005,
        );

        $quotationReceipt->exchangeArray($data);

        $this->assertSame($data['qot_no'], $quotationReceipt->quotation_id, '"qot_no" was not set correctly');
        $this->assertSame($data['item_code'], $quotationReceipt->product_code, '"product_code" was not set correctly');
        $this->assertSame($data['item'], $quotationReceipt->product_name, '"product_name" was not set correctly');
        $this->assertSame($data['unit_price'], $quotationReceipt->unit_price, '"unit_price" was not set correctly');
        $this->assertSame($data['quantity'], $quotationReceipt->quantity, '"quantity" was not set correctly');
        $this->assertSame($data['discount'], $quotationReceipt->discount, '"discount" was not set correctly');
        $this->assertSame($data['total_cost'], $quotationReceipt->price, '"price" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $quotationReceipt = new QuotationReceipt();

        $data = array(
            'qot_no' => 'test_1',
            'item_code' => 'itm_01',
            'item' => 'Testing Item',
            'unit_price' => 22500.50,
            'quantity' => 10,
            'discount' => 2,
            'total_cost' => 225005,
        );
        $quotationReceipt->exchangeArray($data);

        $quotationReceipt->exchangeArray(array());

        $this->assertNull($quotationReceipt->quotation_id, ' "quotation_id" should have defaulted to null');
        $this->assertNull($quotationReceipt->product_code, ' "product_code" should have defaulted to null');
        $this->assertNull($quotationReceipt->product_name, ' "product_name" should have defaulted to null');
        $this->assertNull($quotationReceipt->unit_price, '"unit_price" should have defaulted to null');
        $this->assertNull($quotationReceipt->quantity, '"quantity" should have defaulted to null');
        $this->assertNull($quotationReceipt->discount, '"discount" should have defaulted to null');
        $this->assertNull($quotationReceipt->price, '"price" should have defaulted to null');
    }

    public function testGetArrayCopyReturnsAnArrayWithPropertyValues()
    {
        $quotationReceipt = new QuotationReceipt();

        $data = array(
            'qot_no' => 'test_1',
            'item_code' => 'itm_01',
            'item' => 'Testing Item',
            'unit_price' => 22500.50,
            'quantity' => 10,
            'discount' => 2,
            'total_cost' => 225005,
        );

        $quotationReceipt->exchangeArray($data);
        $copyArray = $quotationReceipt->getArrayCopy();

        $this->assertSame($data['qot_no'], $copyArray['quotation_id'], '"quotation_id" was not set correctly');
        $this->assertSame($data['item_code'], $copyArray['product_code'], '"product_code" was not set correctly');
        $this->assertSame($data['item'], $copyArray['product_name'], '"product_name" was not set correctly');
        $this->assertSame($data['unit_price'], $copyArray['unit_price'], '"unit_price" was not set correctly');
        $this->assertSame($data['quantity'], $copyArray['quantity'], '"quantity" was not set correctly');
        $this->assertSame($data['discount'], $copyArray['discount'], '"discount" was not set correctly');
        $this->assertSame($data['total_cost'], $copyArray['price'], '"price" was not set correctly');
    }

    public function testInputFiltersAreSetCorrectly()
    {
        $quotationReceipt = new QuotationReceipt();

        $inputFilter = $quotationReceipt->getInputFilter();

        $this->assertSame(5, $inputFilter->count());
        $this->assertTrue($inputFilter->has('item_code'));
        $this->assertTrue($inputFilter->has('item'));
        $this->assertTrue($inputFilter->has('unit_price'));
        $this->assertTrue($inputFilter->has('discount'));
        $this->assertTrue($inputFilter->has('quantity'));
    }

}

