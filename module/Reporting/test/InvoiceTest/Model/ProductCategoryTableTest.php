<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains ProductCategoryTable Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\ProductCategoryTable;
use Invoice\Model\ProductCategory;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class ProductCategoryTableTest extends PHPUnit_Framework_TestCase
{

    public function testFetchAllReturnsAllProductCategorys()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $productCategoryTable = new ProductCategoryTable($mockTableGateway);

        $this->assertSame($resultSet, $productCategoryTable->fetchAll());
    }

    public function testCanRetrieveAnProductCategoryByItsId()
    {
        $productCategory = new ProductCategory();
        $productCategory->exchangeArray(array(
            'productCategoriesID' => 123,
            'categoryName' => 'car',
            'categoryType' => 'alion',
            'prefix' => 'alion',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new ProductCategory());
        $resultSet->initialize(array($productCategory));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('productCategoriesID' => 123))
                ->will($this->returnValue($resultSet));

        $productCategoryTable = new ProductCategoryTable($mockTableGateway);

        $this->assertSame($productCategory, $productCategoryTable->getProductCategory(123));
    }

    public function testSaveProductCategoryWillInsertNewProductCategorysIfTheyDontAlreadyHaveAnId()
    {
        $productCategoryData = array(
            'productCategoriesID' => 123,
            'categoryName' => 'car',
            'categoryType' => 'alion',
            'prefix' => 'alion',
        );
        $productCategory = new ProductCategory();
        $productCategory->exchangeArray($productCategoryData);
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new ProductCategory());
        $resultSet->initialize(array($productCategory));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select', 'insert'), array(), '', false);
        $mockTableGateway->expects($this->any())
                ->method('select')
                ->with(array('productCategoriesID' => 123))
                ->will($this->returnValue($resultSet));
        $mockTableGateway->expects($this->any())
                ->method('insert')
                ->with($productCategoryData);

        $productCategoryTable = new ProductCategoryTable($mockTableGateway);
        $productCategoryTable->saveProductCategory($productCategory);
    }

    public function testExceptionIsThrownWhenGettingNonexistentProductCategory()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new ProductCategory());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('productCategoriesID' => 123))
                ->will($this->returnValue($resultSet));

        $productCategoryTable = new ProductCategoryTable($mockTableGateway);

        try {
            $productCategoryTable->getProductCategory(123);
        } catch (\Exception $e) {
            $this->assertSame('Could not find row 123', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }

}

