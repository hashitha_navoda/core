<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains ProductSupplier Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\ProductSupplier;
use PHPUnit_Framework_TestCase;

class ProductSupplierTest extends PHPUnit_Framework_TestCase
{

    public function testProductSupplierInitialState()
    {
        $productSupplier = new ProductSupplier();

        $this->assertNull($productSupplier->id, '"id" should initially be null');
        $this->assertNull($productSupplier->productID, '"productID" should initially be null');
        $this->assertNull($productSupplier->supplierID, '"supplierID" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $productSupplier = new ProductSupplier();

        $data = array(
            'id' => 123,
            'productID' => 'some productID',
            'supplierID' => 'some supplierID',
        );


        $productSupplier->exchangeArray($data);

        $this->assertSame($data['id'], $productSupplier->id, '"id" was not set correctly');
        $this->assertSame($data['productID'], $productSupplier->productID, '"productID" was not set correctly');
        $this->assertSame($data['supplierID'], $productSupplier->supplierID, '"supplierID" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $productSupplier = new ProductSupplier();

        $productSupplier->exchangeArray(array(
            'id' => 123,
            'productID' => 'some productID',
            'supplierID' => 'some supplierID',));
        $productSupplier->exchangeArray(array());

        $this->assertNull($productSupplier->id, '"id" should have defaulted to null');
        $this->assertNull($productSupplier->productID, '"productID" should have defaulted to null');
        $this->assertNull($productSupplier->supplierID, '"supplierID" should have defaulted to null');
    }

}

