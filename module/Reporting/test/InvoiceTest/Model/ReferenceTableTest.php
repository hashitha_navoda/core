<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains ReferenceTable Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\ReferenceTable;
use Invoice\Model\Reference;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class ReferenceTableTest extends PHPUnit_Framework_TestCase
{

    public function testFetchAllReturnsAllReferences()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $referenceTable = new ReferenceTable($mockTableGateway);

        $this->assertSame($resultSet, $referenceTable->fetchAll());
    }

    public function testCanRetrieveAnReferenceByItsId()
    {
        $reference = new Reference();
        $reference->exchangeArray(array(
            'referenceType' => 'aaaa',
            'currentReference' => 'aaaa',
            'nextReference' => 'aaaa1',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Reference());
        $resultSet->initialize(array($reference));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('referenceType' => 'aaaa'))
                ->will($this->returnValue($resultSet));

        $referenceTable = new ReferenceTable($mockTableGateway);

        $this->assertSame($reference, $referenceTable->getReference('aaaa'));
    }

    public function testSaveReferenceWillInsertNewReferencesIfTheyDontAlreadyHaveAnId()
    {
        $referenceData = array(
            'referenceType' => 'aaaa',
            'currentReference' => 'aaaa',
            'nextReference' => 'aaaa1',);
        $reference = new Reference();
        $reference->exchangeArray($referenceData);

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('insert'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('insert')
                ->with($referenceData);

        $referenceTable = new ReferenceTable($mockTableGateway);
        $referenceTable->saveReference($reference);
    }

    public function testExceptionIsThrownWhenGettingNonexistentReference()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Reference());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('referenceType' => 'aaaa'))
                ->will($this->returnValue($resultSet));

        $referenceTable = new ReferenceTable($mockTableGateway);

        try {
            $referenceTable->getReference('aaaa');
        } catch (\Exception $e) {
            $this->assertSame('Could not find row aaaa', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }

}

