<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains SupplierTable Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\SupplierTable;
use Invoice\Model\Supplier;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class SupplierTableTest extends PHPUnit_Framework_TestCase
{

    public function testFetchAllReturnsAllSuppliers()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $supplierTable = new SupplierTable($mockTableGateway);

        $this->assertSame($resultSet, $supplierTable->fetchAll());
    }

    public function testCanRetrieveAnSupplierByItsId()
    {
        $supplier = new Supplier();
        $supplier->exchangeArray(array('id' => 123,
            'name' => 'VHAMS',
            'address' => '316/B Old kandy road dalugama kelaniya',
            'telephoneNumber' => '0783827226',
            'email' => 'ashanmadushkaperera@gmail.com',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Supplier());
        $resultSet->initialize(array($supplier));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $supplierTable = new SupplierTable($mockTableGateway);

        $this->assertSame($supplier, $supplierTable->getSupplier(123));
    }

    public function testSaveSupplierWillInsertNewSuppliersIfTheyDontAlreadyHaveAnId()
    {
//        $supplierData = array('name' => 'VHAMS',
//            'address' => '316/B Old kandy road dalugama kelaniya',
//            'telephoneNumber' => '0783827226',
//            'email' => 'ashanmadushkaperera@gmail.com',
//        );
//        $supplier = new Supplier();
//        $supplier->exchangeArray($supplierData);
//
//        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('insert'), array(), '', false);
//        $mockTableGateway->expects($this->once())
//                ->method('insert')
//                ->with($supplierData);
//
//        $supplierTable = new SupplierTable($mockTableGateway);
//        $supplierTable->saveSupplier($supplier);
    }

    public function testExceptionIsThrownWhenGettingNonexistentSupplier()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Supplier());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $supplierTable = new SupplierTable($mockTableGateway);

        try {
            $supplierTable->getSupplier(123);
        } catch (\Exception $e) {
            $this->assertSame('Could not find row 123', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }

}

