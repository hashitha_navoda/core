<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains ProductTable Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\ProductTable;
use Invoice\Model\Product;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class ProductTableTest extends PHPUnit_Framework_TestCase
{

    public function testFetchAllReturnsAllProducts()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $productTable = new ProductTable($mockTableGateway);

        $this->assertSame($resultSet, $productTable->fetchAll());
    }

    public function testCanRetrieveAnProductByItsId()
    {
        $product = new Product();
        $product->exchangeArray(array(
            'id' => 123,
            'productName' => 'some productName',
            'description' => 'some description',
            'category' => 'some category',
            'unitOfMeasure' => 'some unitOfMeasure',
            'price' => 'some price',
            'discount' => 'some discount',));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Product());
        $resultSet->initialize(array($product));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $productTable = new ProductTable($mockTableGateway);
        $this->assertSame($product, $productTable->getProduct(123));
    }

    public function testSaveProductWillInsertNewProductsIfTheyDontAlreadyHaveAnId()
    {
        $productData = array(
            'id' => 123,
            'productName' => 'pen',
            'description' => 'good pen',
            'category' => 4,
            'unitOfMeasure' => 11,
            'price' => 1200,
            'discount' => 2,);
        $product = new Product();
        $product->exchangeArray($productData);

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Product());
        $resultSet->initialize(array($product));
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select', 'insert'), array(), '', false);
        $mockTableGateway->expects($this->any())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));
        $mockTableGateway->expects($this->any())
                ->method('insert')
                ->with($productData);


        $productTable = new ProductTable($mockTableGateway);
        $productTable->saveProduct($product);
    }

    public function testExceptionIsThrownWhenGettingNonexistentProduct()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Product());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $productTable = new ProductTable($mockTableGateway);

        try {
            $productTable->getProduct(123);
        } catch (\Exception $e) {
            $this->assertSame('Could not find row 123', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }

}

