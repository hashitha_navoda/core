<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains UnitMeasure Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\UnitMeasure;
use PHPUnit_Framework_TestCase;

class UnitMeasureTest extends PHPUnit_Framework_TestCase
{

    public function testUnitMeasureInitialState()
    {
        $unitMeasure = new UnitMeasure();

        $this->assertNull($unitMeasure->unitOfMeasuresId, '"unitOfMeasuresId" should initially be null');
        $this->assertNull($unitMeasure->name, '"name" should initially be null');
        $this->assertNull($unitMeasure->abbrevation, '"abbrevation" should initially be null');
        $this->assertNull($unitMeasure->decimalPlace, '"decimalPlace" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $unitMeasure = new UnitMeasure();
        $data = array(
            'unitOfMeasuresId' => 123,
            'name' => 'some name',
            'abbrevation' => 'some abbrevation',
            'decimalPlace' => 'some decimalPlace',
        );


        $unitMeasure->exchangeArray($data);

        $this->assertSame($data['unitOfMeasuresId'], $unitMeasure->unitOfMeasuresId, '"unitOfMeasuresId" was not set correctly');
        $this->assertSame($data['name'], $unitMeasure->name, '"name" was not set correctly');
        $this->assertSame($data['abbrevation'], $unitMeasure->abbrevation, '"abbrevation" was not set correctly');
        $this->assertSame($data['decimalPlace'], $unitMeasure->decimalPlace, '"decimalPlace" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $unitMeasure = new UnitMeasure();

        $unitMeasure->exchangeArray(array(
            'unitOfMeasuresId' => 123,
            'name' => 'some name',
            'abbrevation' => 'some abbrevation',
            'decimalPlace' => 'some decimalPlace'
        ));
        $unitMeasure->exchangeArray(array());

        $this->assertNull($unitMeasure->unitOfMeasuresId, '"unitOfMeasuresId" should have defaulted to null');
        $this->assertNull($unitMeasure->name, '"name" should have defaulted to null');
        $this->assertNull($unitMeasure->abbrevation, '"abbrevation" should have defaulted to null');
        $this->assertNull($unitMeasure->decimalPlace, '"decimalPlace" should have defaulted to null');
    }

}

