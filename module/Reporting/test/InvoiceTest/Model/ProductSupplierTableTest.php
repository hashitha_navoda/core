<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains ProductSupplierTable Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\ProductSupplierTable;
use Invoice\Model\ProductSupplier;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class ProductSupplierTableTest extends PHPUnit_Framework_TestCase
{

    public function testFetchAllReturnsAllProductSuppliers()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $productSupplierTable = new ProductSupplierTable($mockTableGateway);

        $this->assertSame($resultSet, $productSupplierTable->fetchAll());
    }

    public function testCanRetrieveAnProductSupplierByItsId()
    {
        $productSupplier = new ProductSupplier();
        $productSupplier->exchangeArray(array(
            'id' => 123,
            'productID' => 'xxxx1',
            'supplierID' => 234,
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new ProductSupplier());
        $resultSet->initialize(array($productSupplier));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $productSupplierTable = new ProductSupplierTable($mockTableGateway);

        $this->assertSame($productSupplier, $productSupplierTable->getProductSupplier(123));
    }

    public function testSaveProductSupplierWillInsertNewProductSuppliersIfTheyDontAlreadyHaveAnId()
    {
        $productSupplierData = array(
            'id' => 123,
            'productID' => 'xxxx1',
            'supplierID' => 234);
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('insert'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('insert')
                ->with($productSupplierData);

        $productSupplierTable = new ProductSupplierTable($mockTableGateway);
        $productSupplierTable->saveProductSupplier($productSupplierData);
    }

    public function testExceptionIsThrownWhenGettingNonexistentProductSupplier()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new ProductSupplier());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $productSupplierTable = new ProductSupplierTable($mockTableGateway);

        try {
            $productSupplierTable->getProductSupplier(123);
        } catch (\Exception $e) {
            $this->assertSame('Could not find row 123', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }

}

