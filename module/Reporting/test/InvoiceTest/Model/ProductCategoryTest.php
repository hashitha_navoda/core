<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains ProductCategory Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\ProductCategory;
use PHPUnit_Framework_TestCase;

class ProductCategoryTest extends PHPUnit_Framework_TestCase
{

    public function testProductCategoryInitialState()
    {
        $productCategory = new ProductCategory();

        $this->assertNull($productCategory->productCategoriesID, '"productCategoriesID" should initially be null');
        $this->assertNull($productCategory->categoryName, '"categoryName" should initially be null');
        $this->assertNull($productCategory->categoryType, '"categoryType" should initially be null');
        $this->assertNull($productCategory->prefix, '"prefix" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $productCategory = new ProductCategory();
        $data = array(
            'productCategoriesID' => 123,
            'categoryName' => 'some categoryName',
            'categoryType' => 'some categoryType',
            'prefix' => 'some prefix',
        );


        $productCategory->exchangeArray($data);

        $this->assertSame($data['productCategoriesID'], $productCategory->productCategoriesID, '"productCategoriesID" was not set correctly');
        $this->assertSame($data['categoryName'], $productCategory->categoryName, '"categoryName" was not set correctly');
        $this->assertSame($data['categoryType'], $productCategory->categoryType, '"categoryType" was not set correctly');
        $this->assertSame($data['prefix'], $productCategory->prefix, '"prefix" was not set correctly');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $productCategory = new ProductCategory();

        $productCategory->exchangeArray(array(
            'productCategoriesID' => 123,
            'categoryName' => 'some categoryName',
            'categoryType' => 'some categoryType',
            'prefix' => 'some prefix',
        ));
        $productCategory->exchangeArray(array());

        $this->assertNull($productCategory->productCategoriesID, '"productCategoriesID" should have defaulted to null');
        $this->assertNull($productCategory->categoryName, '"categoryName" should have defaulted to null');
        $this->assertNull($productCategory->categoryType, '"categoryType" should have defaulted to null');
        $this->assertNull($productCategory->prefix, '"prefix" should have defaulted to null');
    }

}

