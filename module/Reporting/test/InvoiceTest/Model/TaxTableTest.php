<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains TaxTable Module Test functions
 */

namespace InvoiceTest\Model;

use Invoice\Model\TaxTable;
use Invoice\Model\Tax;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class TaxTableTest extends PHPUnit_Framework_TestCase
{

    public function testFetchAllReturnsAllTaxs()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $taxTable = new TaxTable($mockTableGateway);

        $this->assertSame($resultSet, $taxTable->fetchAll());
    }

    public function testCanRetrieveAnTaxByItsId()
    {
        $tax = new Tax();
        $tax->exchangeArray(array('id' => 123,
            'taxName' => 'vat',
            'taxType' => 'vat',
            'taxPrecentage' => '12',
        ));

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Tax());
        $resultSet->initialize(array($tax));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $taxTable = new TaxTable($mockTableGateway);

        $this->assertSame($tax, $taxTable->getTax(123));
    }

    public function testSaveTaxWillInsertNewTaxsIfTheyDontAlreadyHaveAnId()
    {
        $taxData = array('id' => 123,
            'taxName' => 'vat',
            'taxType' => 'vat',
            'taxPrecentage' => '12',);
        $tax = new Tax();
        $tax->exchangeArray($taxData);
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Tax());
        $resultSet->initialize(array($tax));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select', 'insert'), array(), '', false);
        $mockTableGateway->expects($this->any())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));
        $mockTableGateway->expects($this->any())
                ->method('insert')
                ->with($taxData);

        $taxTable = new TaxTable($mockTableGateway);
        $taxTable->saveTax($tax);
    }

    public function testExceptionIsThrownWhenGettingNonexistentTax()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new Tax());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('id' => 123))
                ->will($this->returnValue($resultSet));

        $taxTable = new TaxTable($mockTableGateway);

        try {
            $taxTable->getTax(123);
        } catch (\Exception $e) {
            $this->assertSame('Could not find row 123', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }

}

