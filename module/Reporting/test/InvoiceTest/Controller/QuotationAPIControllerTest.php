<?php

namespace InvoiceTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class QuotationAPIControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;

    public function setUp()
    {
        $path = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
        $this->setApplicationConfig(
                include $path . '/config/application.config.php'
        );
        parent::setUp();
        ;
    }

    public function testCreateQuotationCanBeAccessed()
    {
        $this->dispatch('/quotationAPI/createQuotation');
        $this->assertResponseStatusCode(200);

        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\QuotationAPI');
        $this->assertControllerClass('QuotationAPIController');
        $this->assertMatchedRouteName('quotationAPI');
    }

}

?>