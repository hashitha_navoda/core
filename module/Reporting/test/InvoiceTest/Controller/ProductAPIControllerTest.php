<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains ProductAPI controller Test functions
 */

namespace InvoiceTest\Controller;

use InvoiceTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Invoice\Controller\ProductAPIController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_TestCase;

class ProductAPIControllerTest extends \PHPUnit_Framework_TestCase
{

    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;
    protected $traceError = true;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new ProductAPIController();
        $this->request = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'productAPI'));
        $this->event = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'index');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testAddCategoryActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'addCategory');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testReferenceActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'reference');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testAddUnitMeasureActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'addUnitMeasure');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPrefixActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'prefix');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testUpdatePrefixActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'updatePrefix');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testCheckCodeActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'checkCode');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetProductSupplierTableReturnsAnInstanceOfProductSupplierTable()
    {
        $this->assertInstanceOf('Invoice\Model\ProductSupplierTable', $this->controller->getProductSupplierTable());
    }

    public function testGetProductTableReturnsAnInstanceOfProductTable()
    {
        $this->assertInstanceOf('Invoice\Model\ProductTable', $this->controller->getProductTable());
    }

    public function testGetUnitMeasureTableReturnsAnInstanceOfUnitMeasureTable()
    {
        $this->assertInstanceOf('Invoice\Model\UnitMeasureTable', $this->controller->getUnitMeasureTable());
    }

    public function testGetProductCategoryTableReturnsAnInstanceOfProductCategoryTable()
    {
        $this->assertInstanceOf('Invoice\Model\ProductCategoryTable', $this->controller->getProductCategoryTable());
    }

    public function testGetProductTaxTableReturnsAnInstanceOfProductTaxTable()
    {
        $this->assertInstanceOf('Invoice\Model\ProductTaxTable', $this->controller->getProductTaxTable());
    }

    public function testGetReferenceTableReturnsAnInstanceOfReferenceTable()
    {
        $this->assertInstanceOf('Invoice\Model\ReferenceTable', $this->controller->getReferenceTable());
    }

    public function testGetTaxTableReturnsAnInstanceOfTaxTable()
    {
        $this->assertInstanceOf('Invoice\Model\TaxTable', $this->controller->getTaxTable());
    }

    public function testPreviewActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'preview');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();
        $this->assertEquals(302, $response->getStatusCode());
    }

}

