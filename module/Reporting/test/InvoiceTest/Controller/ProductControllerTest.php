<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Product controller Test functions
 */

namespace InvoiceTest\Controller;

use InvoiceTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use Invoice\Controller\ProductController;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_TestCase;

class ProductControllerTest extends \PHPUnit_Framework_TestCase
{

    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;
    protected $traceError = true;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new ProductController();
        $this->request = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'product'));
        $this->event = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'index');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testGetSupplierTableReturnsAnInstanceOfSupplierTable()
    {
        $this->assertInstanceOf('Invoice\Model\SupplierTable', $this->controller->getSupplierTable());
    }

    public function testGetUnitMeasureTableReturnsAnInstanceOfUnitMeasureTable()
    {
        $this->assertInstanceOf('Invoice\Model\UnitMeasureTable', $this->controller->getUnitMeasureTable());
    }

    public function testGetProductCategoryTableReturnsAnInstanceOfProductCategoryTable()
    {
        $this->assertInstanceOf('Invoice\Model\ProductCategoryTable', $this->controller->getProductCategoryTable());
    }

    public function testGetTaxTableReturnsAnInstanceOfTaxTable()
    {
        $this->assertInstanceOf('Invoice\Model\TaxTable', $this->controller->getTaxTable());
    }

}

