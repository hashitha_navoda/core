<?php

namespace InvoiceTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class QuotationControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;

    public function setUp()
    {
        $path = dirname(dirname(dirname(dirname(dirname(dirname(__FILE__))))));
        $this->setApplicationConfig(
                include $path . '/config/application.config.php'
        );
        parent::setUp();
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/quotation');
        $this->assertResponseStatusCode(200);

        $this->assertModuleName('Invoice');
        $this->assertControllerName('Invoice\Controller\Quotation');
        $this->assertControllerClass('QuotationController');
        $this->assertMatchedRouteName('quotation');
    }

}

?>