<?php

/**
 * Description of Configuration
 *
 * @author sandun <sandun@thinkcube.com>
 *
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Settings\Controller\Tax' => 'Settings\Controller\TaxController',
            'Settings\Controller\TaxAPI' => 'Settings\Controller\TaxAPIController',
            'Settings\Controller\Company' => 'Settings\Controller\CompanyController',
            'Settings\Controller\Approver' => 'Settings\Controller\ApproverController',
            'Settings\Controller\PurchaseRequisitionType' => 'Settings\Controller\PurchaseRequisitionTypeController',
            'Settings\Controller\PurchaseRequisitionCategory' => 'Settings\Controller\PurchaseRequisitionCategoryController',
            'Settings\Controller\CompanyAPI' => 'Settings\Controller\CompanyAPIController',
            'Settings\Controller\Template' => 'Settings\Controller\TemplateController',
            'Settings\Controller\API\Template' => 'Settings\Controller\API\TemplateController',
            'Settings\Controller\Location' => 'Settings\Controller\LocationController',
            'Settings\Controller\Wizard' => 'Settings\Controller\WizardController',
            'Settings\Controller\API\Wizard' => 'Settings\Controller\API\WizardController',
            'Settings\Controller\Uom' => 'Settings\Controller\UomController',
            'Settings\Controller\API\Uom' => 'Settings\Controller\API\UomController',
            'Settings\Controller\Loyalty' => 'Settings\Controller\LoyaltyController',
            'Settings\Controller\Promotion' => 'Settings\Controller\PromotionController',
            'Settings\Controller\API\Promotion' => 'Settings\Controller\API\PromotionController',
            'Settings\Controller\CustomerEvent' => 'Settings\Controller\CustomerEventController',
            'Settings\Controller\API\CustomerEvent' => 'Settings\Controller\API\CustomerEventController',
            'Settings\Controller\ItemAttribute' => 'Settings\Controller\ItemAttributeController',
            'Settings\Controller\API\ItemAttribute' => 'Settings\Controller\API\ItemAttributeController',
            'Settings\Controller\AuditLog' => 'Settings\Controller\AuditLogController',
            'Settings\Controller\API\AuditLog' => 'Settings\Controller\API\AuditLogController',
            'Settings\Controller\LandingCostType' => 'Settings\Controller\LandingCostTypeController',
            'Settings\Controller\API\LandingCostType' => 'Settings\Controller\API\LandingCostTypeController',
            'Settings\Controller\RestAPI\Promotion' => 'Settings\Controller\RestAPI\PromotionRestfullController',
            'Settings\Controller\Dimension' => 'Settings\Controller\DimensionController',
            'Settings\Controller\API\Dimension' => 'Settings\Controller\API\DimensionController',
            'Settings\Controller\SmsSettings' => 'Settings\Controller\SmsSettingsController',
            'Settings\Controller\API\SmsSettings' => 'Settings\Controller\API\SmsSettingsController',
            'Settings\Controller\API\PurchaseRequisitionCategory' => 'Settings\Controller\API\PurchaseRequisitionCategoryController',
            'Settings\Controller\API\PurchaseRequisitionType' => 'Settings\Controller\API\PurchaseRequisitionTypeController',
            'Settings\Controller\API\Approver' => 'Settings\Controller\API\ApproverController',
            'Settings\Controller\ApprovalWorkflowSettings' => 'Settings\Controller\ApprovalWorkflowSettingsController',
            'Settings\Controller\API\ApprovalWorkflowSettings' => 'Settings\Controller\API\ApprovalWorkflowSettingsController',
            'Settings\Controller\ApprovalWorkflow' => 'Settings\Controller\ApprovalWorkflowController',
            'Settings\Controller\API\ApprovalWorkflow' => 'Settings\Controller\API\ApprovalWorkflowController',
            'Settings\Controller\Signature' => 'Settings\Controller\SignatureController',
            'Settings\Controller\API\Signature' => 'Settings\Controller\API\SignatureController',
            'Settings\Controller\SmsNotification' => 'Settings\Controller\SmsNotificationController',
            'Settings\Controller\API\SmsNotification' => 'Settings\Controller\API\SmsNotificationController',
            'Settings\Controller\RestAPI\SmsNotification' => 'Settings\Controller\RestAPI\SmsNotificationRestfullController',
            'Settings\Controller\DiscountSchemes' => 'Settings\Controller\DiscountSchemesController',
            'Settings\Controller\API\DiscountSchemes' => 'Settings\Controller\API\DiscountSchemesController',
            'Settings\Controller\DisplaySettings' => 'Settings\Controller\DisplaySettingsController',
            'Settings\Controller\API\DisplaySettings' => 'Settings\Controller\API\DisplaySettingsController',
            'Settings\Controller\API\DraftMessage' => 'Settings\Controller\API\DraftMessageController',
        ),
    ),
    // The following section is new and should be added to your file
    'router' => array(
        'routes' => array(
            'tax' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/tax[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Tax',
                        'action' => 'index',
                    ),
                ),
            ),
            'purchase-requisition-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchase-requisition-category[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\PurchaseRequisitionCategory',
                        'action' => 'index',
                    ),
                ),
            ),
            'taxAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/taxAPI[/][:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Settings\Controller',
                        'controller' => 'TaxAPI',
                    ),
                ),
            ),
            'company' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/company[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Company',
                        'action' => 'index',
                    ),
                ),
            ),
            'approver' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/approver[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Approver',
                        'action' => 'index',
                    ),
                ),
            ),
            'approval-workflow-settings' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/approval-workflow-settings[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\ApprovalWorkflowSettings',
                        'action' => 'index',
                    ),
                ),
            ),
            'approval-workflow-settings-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/approval-workflow-settings[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Settings\Controller\API',
                        'controller' => 'ApprovalWorkflowSettings',
                    ),
                ),
            ),
            'discount-schemes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/discount-schemes[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\DiscountSchemes',
                        'action' => 'index',
                    ),
                ),
            ),
            'discount-schemes-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/discount-schemes[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Settings\Controller\API',
                        'controller' => 'DiscountSchemes',
                    ),
                ),
            ),
            'display-settings' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/display-settings[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\DisplaySettings',
                        'action' => 'index',
                    ),
                ),
            ),
            'display-settings-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/api/display-settings[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Settings\Controller\API',
                        'controller' => 'DisplaySettings',
                    ),
                ),
            ),
            'approval-workflow' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/approval-workflow[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\ApprovalWorkflow',
                        'action' => 'create',
                    ),
                ),
            ),
            'approval-workflow-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/approval-workflow[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Settings\Controller\API',
                        'controller' => 'ApprovalWorkflow',
                    ),
                ),
            ),
            'companyAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/companyAPI[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Settings\Controller',
                        'controller' => 'CompanyAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'company-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/company[/:action]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Settings\Controller',
                        'controller' => 'CompanyAPI',
                        'action' => 'getCompanyDetails',
                    ),
                ),
            ),
            'custom-currency-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/customCurrency/Api[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Settings\Controller',
                        'controller' => 'CompanyAPI',
                        'action' => 'addCustomCurrency',
                    ),
                ),
            ),
            'template' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/template[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Template',
                        'action' => 'index',
                    ),
                ),
            ),
            'template-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/template[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        '__NAMESPACE__' => 'Settings\Controller\API',
                        'controller' => 'Template',
                        'action' => 'index',
                    ),
                ),
            ),
            'category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/category[/][:action][:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\Category',
                        'action' => 'index',
                    ),
                ),
            ),
            'category-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/company/category[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Inventory\Controller\API\Category',
                        'action' => 'index',
                    ),
                ),
            ),
            'location' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/location[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Location',
                        'action' => 'index',
                    ),
                ),
            ),
            'uom' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/uom[/][:action][:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Uom',
                        'action' => 'index',
                    ),
                ),
            ),
            'uom-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/settings/uom[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\Uom',
                        'action' => 'index',
                    ),
                ),
            ),
            'item-attributes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/item-attributes[/][:action][:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\ItemAttribute',
                        'action' => 'index',
                    ),
                ),
            ),
            
            'item-attribute-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/settings/item-attribute[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\ItemAttribute',
                        'action' => 'index',
                    ),
                ),
            ),
            'wizard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/wizard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Wizard',
                        'action' => 'index',
                    ),
                ),
            ),
            'wizard-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/settings/wizard[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\Wizard',
                        'action' => 'index',
                    ),
                ),
            ),
            'loyalty' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/loyalty[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Loyalty',
                        'action' => 'index',
                    ),
                ),
            ),
            'promotions' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/promotions[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Promotion',
                        'action' => 'create',
                    ),
                ),
            ),
            'promotions-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/promotions[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\Promotion',
                        'action' => 'save',
                    ),
                ),
            ),
             'approver-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/approver[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\Approver',
                        'action' => 'save',
                    ),
                ),
            ),
            'customer-event' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/customer-event[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\CustomerEvent',
                        'action' => 'index',
                    ),
                ),
            ),
            'customer-event-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/customer-event[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\CustomerEvent',
                        'action' => 'index',
                    ),
                ),
            ),
            'audit-log' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/audit-log[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\AuditLog',
                        'action' => 'auditLog',
                    ),
                ),
            ),
            'audit-log-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/audit-log-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\AuditLog',
                        'action' => 'searchAuditLogByDateFilter',
                    ),
                ),
            ),
            'dimension' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dimension[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Dimension',
                        'action' => 'index',
                    ),
                ),
            ),
            'signature' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/signature[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\Signature',
                        'action' => 'index',
                    ),
                ),
            ),
            'signature-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/signature-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\Signature',
                        'action' => 'index',
                    ),
                ),
            ),
            'smsSettings' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/smsSettings[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\SmsSettings',
                        'action' => 'index',
                    ),
                ),
            ),
            'purchase-requisition-type' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchase-requisition-type[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\PurchaseRequisitionType',
                        'action' => 'index',
                    ),
                ),
            ),
            'dimension-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dimension-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\Dimension',
                        'action' => 'index',
                    ),
                ),
            ),
            'sms-settings-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sms-settings-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\SmsSettings',
                        'action' => 'index',
                    ),
                ),
            ),
             'purchase-requisition-category-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchase-requisition-category-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\PurchaseRequisitionCategory',
                        'action' => 'index',
                    ),
                ),
            ),
            'purchase-requisition-type-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/purchase-requisition-type-api[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\PurchaseRequisitionType',
                        'action' => 'index',
                    ),
                ),
            ),
            'landing-cost-type' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/settings/landing-cost-type[/][:action][:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\LandingCostType',
                        'action' => 'index',
                    ),
                ),
            ),
            'landing-cost-type-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/settings/landing-cost-type[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\LandingCostType',
                        'action' => 'save',
                    ),
                ),
            ),
            'promotion-rest' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/promotions[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\RestAPI\Promotion'
                    ),
                ),
            ),
            'sms-notification' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sms-notification[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\SmsNotification',
                        'action' => 'index',
                    ),
                ),
            ),
            'sms-notification-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/sms-notification-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\SmsNotification',
                        'action' => 'save',
                    ),
                ),
            ),
            'sms-notification-rest-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/sms-configuration[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\RestAPI\SmsNotification'
                    ),
                ),
            ),
            'send-sms' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/sms-send[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\RestAPI\SmsNotification'
                    ),
                ),
            ),
            'draft-message-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/draft-message[/][:action][/:param1][/:id]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Settings\Controller\API\DraftMessage',
                        'action' => 'save',
                    ),
                ),
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'settings' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
);
?>
