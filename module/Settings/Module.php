<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Settings;

use Zend\EventManager\StaticEventManager as StaticEventManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use Settings\Model\Tax;
use Settings\Model\TaxTable;
use Settings\Model\TaxCompound;
use Settings\Model\TaxCompoundTable;
use Settings\Model\Company;
use Settings\Model\CompanyTable;
use Settings\Model\DisplaySetup;
use Settings\Model\DisplaySetupTable;
use Settings\Model\Location;
use Settings\Model\LocationTable;
use Settings\Model\Template;
use Settings\Model\TemplateTable;
use Settings\Model\Uom;
use Settings\Model\UomTable;
use Settings\Model\Reference;
use Settings\Model\ReferencePrefix;
use Settings\Model\ReferenceTemp;
use Settings\Model\ReferenceTable;
use Settings\Model\ReferencePrefixTable;
use Settings\Model\Loyalty;
use Settings\Model\LoyaltyTable;
use Settings\Model\CustomerLoyalty;
use Settings\Model\CustomerLoyaltyTable;
use Settings\Model\Promotion;
use Settings\Model\PromotionTable;
use Settings\Model\PromotionProduct;
use Settings\Model\PromotionProductTable;
use Settings\Model\InvoicePromotion;
use Settings\Model\InvoicePromotionTable;
use Settings\Model\PromotionEmail;
use Settings\Model\PromotionEmailTable;
use Settings\Model\PromotionEmailLocation;
use Settings\Model\PromotionEmailLocationTable;
use Settings\Model\PromotionEmailProduct;
use Settings\Model\PromotionEmailProductTable;
use Settings\Model\PromotionEmailCustomer;
use Settings\Model\PromotionEmailCustomerTable;
use Settings\Model\PromotionEmailCustomerEvent;
use Settings\Model\PromotionEmailCustomerEventTable;
use Settings\Model\IncomingPaymentMethodLoyaltyCard;
use Settings\Model\IncomingPaymentMethodLoyaltyCardTable;
use Settings\Model\CustomCurrency;
use Settings\Model\CustomCurrencyTable;
use Settings\Model\DocumentType;
use Settings\Model\DocumentTypeTable;
use Settings\Model\LocationTemplate;
use Settings\Model\LocationTemplateTable;
use Settings\Model\Setting;
use Settings\Model\SettingTable;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeTable;
use Settings\Model\ItemAttributeValue;
use Settings\Model\ItemAttributeValueTable;
use Settings\Model\ItemAttributeMap;
use Settings\Model\ItemAttributeMapTable;
use Settings\Model\TemplateItemAttributeMap;
use Settings\Model\TemplateItemAttributeMapTable;
use Settings\Model\ItemAttributeValueMap;
use Settings\Model\ItemAttributeValueMapTable;
use Settings\Model\PromotionCombination;
use Settings\Model\PromotionCombinationTable;
use Settings\Model\ItemBaseCombinationRule;
use Settings\Model\ItemBaseCombinationRuleTable;
use Settings\Model\AttributeBaseCombinationRule;
use Settings\Model\AttributeBaseCombinationRuleTable;
use Settings\Model\Dimension;
use Settings\Model\DimensionTable;
use Settings\Model\AdministrationOfficial;
use Settings\Model\AdministrationOfficialTable;
use Settings\Model\DimensionValue;
use Settings\Model\DimensionValueTable;
use Settings\Model\SmsConfiguration;
use Settings\Model\SmsConfigurationTable;
use Settings\Model\Approver;
use Settings\Model\ApproverTable;
use Settings\Model\OrganizationAdditionalDetail;
use Settings\Model\OrganizationAdditionalDetailTable;
use Settings\Model\PurchaseRequisitionCategory;
use Settings\Model\PurchaseRequisitionCategoryTable;
use Settings\Model\PurchaseRequisitionType;
use Settings\Model\PurchaseRequisitionTypeTable;
use Settings\Model\PurchaseRequisitionTypeLimit;
use Settings\Model\PurchaseRequisitionTypeLimitTable;
use Settings\Model\PurchaseRequisitionTypeLimitApprover;
use Settings\Model\PurchaseRequisitionTypeLimitApproverTable;
use Settings\Model\ApprovalDocumentDetails;
use Settings\Model\ApprovalDocumentDetailsTable;
use Settings\Model\ApprovalWorkflows;
use Settings\Model\ApprovalWorkflowsTable;
use Settings\Model\ApprovalWorkflowLimits;
use Settings\Model\ApprovalWorkflowLimitsTable;
use Settings\Model\ApprovalWorkflowLimitApprovers;
use Settings\Model\ApprovalWorkflowLimitApproversTable;
use Settings\Model\SignatureDetails;
use Settings\Model\SignatureDetailsTable;
use Settings\Model\TemplateSignature;
use Settings\Model\TemplateSignatureTable;
use Settings\Model\PromotionEmailContact;
use Settings\Model\PromotionEmailContactTable;
use Settings\Model\SmsType;
use Settings\Model\SmsTypeTable;
use Settings\Model\SmsIncluded;
use Settings\Model\SmsIncludedTable;
use Settings\Model\SmsTypeIncluded;
use Settings\Model\SmsTypeIncludedTable;
use Settings\Model\PosTemplateDetails;
use Settings\Model\PosTemplateDetailsTable;
use Settings\Model\DiscountScheme;
use Settings\Model\DiscountSchemeTable;
use Settings\Model\DiscountSchemeCondition;
use Settings\Model\DiscountSchemeConditionTable;
use Settings\Model\DraftMessage;
use Settings\Model\DraftMessageTable;
//use Settings\Model\ReferenceTempTable;

class Module
{

    public $user_session;

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $this->getConfig();

        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

        $sharedManager->attach('Zend\Mvc\Application', 'dispatch.error', function($e) use ($sm) {
            if ($e->getParam('exception')) {
                //$sm->get('Zend\Log\Logger')->crit($e->getParam('exception'));
            }
        }
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'Company' => 'Settings\Controller\CompanyController',
                'LandingCostTypeController' => 'Settings\Controller\LandingCostTypeController',
                'DiscountSchemesController' => 'Settings\Controller\DiscountSchemesController',
                'DimensionController' => 'Settings\Controller\DimensionController',
                'CompanyController' => 'Settings\Controller\CompanyController',
                'ApproverController' => 'Settings\Controller\ApproverController',
                'NoticeService' => 'Settings\Service\NoticeService',
                'DimensionService' => 'Settings\Service\DimensionService',
                'CompanyService' => 'Settings\Service\CompanyService',
                'ApproverService' => 'Settings\Service\ApproverService',
                'DiscountSchemeService' => 'Settings\Service\DiscountSchemeService',
                'SmsSettingService' => 'Settings\Service\SmsSettingService',
                'LandingCostTypeService' => 'Settings\Service\LandingCostTypeService',
                'SmsNotificationService' => 'Settings\Service\SmsNotificationService',
                'TemplateService' => 'Settings\Service\TemplateService',
            ),
            'shared' => array(
                'NoticeService' => true,
                'DimensionService' => true,
                'CompanyService' => true,
                'SmsSettingService' => true,
                'ApproverService' => true,
                'DiscountSchemeService' => true,
            ),
            'aliases' => array(
                'ReferenceTable' => 'Settings\Model\ReferenceTable',
                'ReferencePrefixTable' => 'Settings\Model\ReferencePrefixTable',
                'CustomerEventTable' => 'Settings\Model\CustomerEventTable',
                'DisplaySetupTable' => 'Settings\Model\DisplaySetupTable',
                'TaxCompoundTable' => 'Settings\Model\TaxCompoundTable',
                'DimensionTable' => 'Settings\Model\DimensionTable',
                'SignatureDetailsTable' => 'Settings\Model\SignatureDetailsTable',
                'DiscountSchemeTable' => 'Settings\Model\DiscountSchemeTable',
                'DiscountSchemeConditionTable' => 'Settings\Model\DiscountSchemeConditionTable',
                'AdministrationOfficialTable' => 'Settings\Model\AdministrationOfficialTable',
                'AdministrationOfficial' => 'Settings\Model\AdministrationOfficial',
                'SmsConfigurationTable' => 'Settings\Model\SmsConfigurationTable',
                'ApproverTable' => 'Settings\Model\ApproverTable',
                'OrganizationAdditionalDetailTable' => 'Settings\Model\OrganizationAdditionalDetailTable',
                'DimensionValueTable' => 'Settings\Model\DimensionValueTable',
                'LocationTable' => 'Settings\Model\LocationTable',
                'PromotionTable' => 'Settings\Model\PromotionTable',
                'ItemAttributeTable' => 'Settings\Model\ItemAttributeTable',
                'ItemAttributeValueTable' => 'Settings\Model\ItemAttributeValueTable',
                'PromotionCombinationTable' => 'Settings\Model\PromotionCombinationTable',
                'ItemBaseCombinationRuleTable' => 'Settings\Model\ItemBaseCombinationRuleTable',
                'AttributeBaseCombinationRuleTable' => 'Settings\Model\AttributeBaseCombinationRuleTable',
                'ApprovalDocumentDetailsTable' => 'Settings\Model\ApprovalDocumentDetailsTable',
                'ApprovalWorkflowsTable' => 'Settings\Model\ApprovalWorkflowsTable',
                'ApprovalWorkflowLimitsTable' => 'Settings\Model\ApprovalWorkflowLimitsTable',
                'ApprovalWorkflowLimitApproversTable' => 'Settings\Model\ApprovalWorkflowLimitApproversTable',
                'TemplateSignatureTable' => 'Settings\Model\TemplateSignatureTable',
                'PromotionEmailContactTable' => 'Settings\Model\PromotionEmailContactTable',
                //'MyCompanyTable' => 'Settings\Model\CompanyTable'
                'SmsTypeTable' => 'Settings\Model\SmsTypeTable',
                'SmsIncludedTable' => 'Settings\Model\SmsIncludedTable',
                'SmsTypeIncludedTable' => 'Settings\Model\SmsTypeIncludedTable',
                'PosTemplateDetailsTable' => 'Settings\Model\PosTemplateDetailsTable',
                'DraftMessageTable' => 'Settings\Model\DraftMessageTable',
            ),
            'factories' => array(
                'Settings\Model\TaxTable' => function($sm) {
                    $tableGateway = $sm->get('TaxTableGateway');
                    $table = new TaxTable($tableGateway);
                    return $table;
                },
                'TaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Tax());
                    return new TableGateway('tax', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\TaxCompoundTable' => function($sm) {
                    $tableGateway = $sm->get('TaxCompoundTableGateway');
                    $table = new TaxCompoundTable($tableGateway);
                    return $table;
                },
                'TaxCompoundTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TaxCompound());
                    return new TableGateway('taxCompound', $dbAdapter, null, $resultSetPrototype);
                },
                'CompanyTable' => function($sm) {
                    $tableGateway = $sm->get('CompanyTableGateway');
                    $table = new CompanyTable($tableGateway);
                    return $table;
                },
                'CompanyTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Company());
                    return new TableGateway('company', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\DisplaySetupTable' => function($sm) {
                    $tableGateway = $sm->get('DisplaySetupTableGateway');
                    $table = new DisplaySetupTable($tableGateway);
                    return $table;
                },
                'DisplaySetupTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DisplaySetup());
                    return new TableGateway('displaySetup', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\LocationTable' => function($sm) {
                    $tableGateway = $sm->get('LocationTableGateway');
                    $table = new LocationTable($tableGateway);
                    return $table;
                },
                'LocationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Location());
                    return new TableGateway('location', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\TemplateTable' => function($sm) {
                    $tableGateway = $sm->get('TemplateTableGateway');
                    $table = new TemplateTable($tableGateway);
                    return $table;
                },
                'TemplateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Template());
                    return new TableGateway('template', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\UomTable' => function($sm) {
                    $tableGateway = $sm->get('Settings\Model\UomTableGateway');
                    $table = new UomTable($tableGateway);
                    return $table;
                },
                'Settings\Model\UomTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Uom());
                    return new TableGateway('uom', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\ReferenceTable' => function($sm) {
                    $tableGateway = $sm->get('ReferenceTableGateway');
                    $table = new ReferenceTable($tableGateway);
                    return $table;
                },
                'ReferenceTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ReferenceTemp());
                    return new TableGateway('reference', $dbAdapter, null, $resultSetPrototype);
                },
//                'Settings\Model\ReferenceTempTable' => function($sm) {
//            $tableGateway = $sm->get('ReferenceTempTableGateway');
//            $table = new ReferenceTempTable($tableGateway);
//            return $table;
//        },
//                'ReferenceTempTableGateway' => function ($sm) {
//            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
//            $resultSetPrototype = new ResultSet();
//            $resultSetPrototype->setArrayObjectPrototype(new ReferenceTemp());
//            return new TableGateway('referenceTemp', $dbAdapter, null, $resultSetPrototype);
//        },
                'Settings\Model\ReferencePrefixTable' => function($sm) {
                    $tableGateway = $sm->get('ReferencePrefixTableGateway');
                    $table = new ReferencePrefixTable($tableGateway);
                    return $table;
                },
                'ReferencePrefixTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ReferencePrefix());
                    return new TableGateway('referencePrefix', $dbAdapter, null, $resultSetPrototype);
                },
                'LoyaltyTable' => function($sm) {
                    $tableGateway = $sm->get('LoyaltyTableGateway');
                    $table = new LoyaltyTable($tableGateway);
                    return $table;
                },
                'LoyaltyTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Loyalty());
                    return new TableGateway('loyalty', $dbAdapter, null, $resultSetPrototype);
                },
                'CustomerLoyaltyTable' => function($sm) {
                    $tableGateway = $sm->get('CustomerLoyaltyTableGateway');
                    $table = new CustomerLoyaltyTable($tableGateway);
                    return $table;
                },
                'CustomerLoyaltyTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new CustomerLoyalty());
                    return new TableGateway('customerLoyalty', $dbAdapter, null, $resultSetPrototype);
                },
                'IncomingPaymentMethodLoyaltyCardTable' => function($sm) {
                    $tableGateway = $sm->get('IncomingPaymentMethodLoyaltyCardTableGateway');
                    $table = new IncomingPaymentMethodLoyaltyCardTable($tableGateway);
                    return $table;
                },
                'IncomingPaymentMethodLoyaltyCardTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new IncomingPaymentMethodLoyaltyCard());
                    return new TableGateway('incomingPaymentMethodLoyaltyCard', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PromotionTable' => function($sm) {
                    $tableGateway = $sm->get('PromotionTableGateway');
                    $table = new PromotionTable($tableGateway);
                    return $table;
                },
                'PromotionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Promotion());
                    return new TableGateway('promotion', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PromotionProductTable' => function($sm) {
                    $tableGateway = $sm->get('PromotionProductTableGateway');
                    $table = new PromotionProductTable($tableGateway);
                    return $table;
                },
                'PromotionProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PromotionProduct());
                    return new TableGateway('promotionProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\InvoicePromotionTable' => function($sm) {
                    $tableGateway = $sm->get('InvoicePromotionTableGateway');
                    $table = new InvoicePromotionTable($tableGateway);
                    return $table;
                },
                'InvoicePromotionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new InvoicePromotion());
                    return new TableGateway('invoicePromotion', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PromotionEmailTable' => function($sm) {
                    $tableGateway = $sm->get('PromotionEmailTableGateway');
                    $table = new PromotionEmailTable($tableGateway);
                    return $table;
                },
                'PromotionEmailTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PromotionEmail());
                    return new TableGateway('promotionEmail', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PromotionEmailLocationTable' => function($sm) {
                    $tableGateway = $sm->get('PromotionEmailLocationTableGateway');
                    $table = new PromotionEmailLocationTable($tableGateway);
                    return $table;
                },
                'PromotionEmailLocationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PromotionEmailLocation());
                    return new TableGateway('promotionEmailLocation', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PromotionEmailProductTable' => function($sm) {
                    $tableGateway = $sm->get('PromotionEmailProductTableGateway');
                    $table = new PromotionEmailProductTable($tableGateway);
                    return $table;
                },
                'PromotionEmailProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PromotionEmailProduct());
                    return new TableGateway('promotionEmailProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PromotionEmailCustomerTable' => function($sm) {
                    $tableGateway = $sm->get('PromotionEmailCustomerTableGateway');
                    $table = new PromotionEmailCustomerTable($tableGateway);
                    return $table;
                },
                'PromotionEmailCustomerTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PromotionEmailCustomer());
                    return new TableGateway('promotionEmailCustomer', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\CustomerEventTable' => function($sm) {
                    $tableGateway = $sm->get('CustomerEventTableGateway');
                    return new Model\CustomerEventTable($tableGateway);
                },
                'CustomerEventTableGateway' => function($sm) {
                    $adapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\CustomerEvent());
                    return new TableGateway('customerEvent', $adapter, null, $resultSetPrototype);
                },
                'Settings\Model\PromotionEmailCustomerEventTable' => function($sm) {
                    $tableGateway = $sm->get('PromotionEmailCustomerEventTableGateway');
                    $table = new PromotionEmailCustomerEventTable($tableGateway);
                    return $table;
                },
                'PromotionEmailCustomerEventTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PromotionEmailCustomerEvent());
                    return new TableGateway('promotionEmailCustomerEvent', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\DocumentTypeTable' => function($sm) {
                    $tableGateway = $sm->get('DocumentTypeTableGateway');
                    $table = new DocumentTypeTable($tableGateway);
                    return $table;
                },
                'DocumentTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DocumentType());
                    return new TableGateway('documentType', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\LocationTemplateTable' => function($sm) {
                    $tableGateway = $sm->get('LocationTemplateTableGateway');
                    $table = new LocationTemplateTable($tableGateway);
                    return $table;
                },
                'LocationTemplateTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new LocationTemplate());
                    return new TableGateway('locationTemplate', $dbAdapter, null, $resultSetPrototype);
                },
                'SettingTable' => function($sm) {
                    $tableGateway = $sm->get('SettingTableGateway');
                    $table = new SettingTable($tableGateway);
                    return $table;
                },
                'SettingTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Setting());
                    return new TableGateway('setting', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\ItemAttributeTable' => function($sm) {
                    $tableGateway = $sm->get('ItemAttributeTableGateway');
                    $table = new ItemAttributeTable($tableGateway);
                    return $table;
                },
                'ItemAttributeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ItemAttribute());
                    return new TableGateway('itemAttribute', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\ItemAttributeValueTable' => function($sm) {
                    $tableGateway = $sm->get('ItemAttributeValueTableGateway');
                    $table = new ItemAttributeValueTable($tableGateway);
                    return $table;
                },
                'ItemAttributeValueTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ItemAttributeValue());
                    return new TableGateway('itemAttributeValue', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\ItemAttributeValueMapTable' => function($sm) {
                    $tableGateway = $sm->get('ItemAttributeValueMapTableGateway');
                    $table = new ItemAttributeValueMapTable($tableGateway);
                    return $table;
                },
                'ItemAttributeValueMapTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ItemAttributeValueMap());
                    return new TableGateway('itemAttributeValueMap', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\DimensionTable' => function($sm) {
                    $tableGateway = $sm->get('DimensionTableGateway');
                    $table = new DimensionTable($tableGateway);
                    return $table;
                },
                'DimensionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Dimension());
                    return new TableGateway('dimension', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\SignatureDetailsTable' => function($sm) {
                    $tableGateway = $sm->get('SignatureDetailsTableGateway');
                    $table = new SignatureDetailsTable($tableGateway);
                    return $table;
                },
                'SignatureDetailsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SignatureDetails());
                    return new TableGateway('signatureDetails', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\DiscountSchemeTable' => function($sm) {
                    $tableGateway = $sm->get('DiscountSchemeTableGateway');
                    $table = new DiscountSchemeTable($tableGateway);
                    return $table;
                },
                'DiscountSchemeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DiscountScheme());
                    return new TableGateway('discountSchemes', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\DiscountSchemeConditionTable' => function($sm) {
                    $tableGateway = $sm->get('DiscountSchemeConditionTableGateway');
                    $table = new DiscountSchemeConditionTable($tableGateway);
                    return $table;
                },
                'DiscountSchemeConditionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DiscountSchemeCondition());
                    return new TableGateway('discountSchemeConditions', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\TemplateSignatureTable' => function($sm) {
                    $tableGateway = $sm->get('TemplateSignatureTableGateway');
                    $table = new TemplateSignatureTable($tableGateway);
                    return $table;
                },
                'TemplateSignatureTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TemplateSignature());
                    return new TableGateway('templateSignature', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\AdministrationOfficialTable' => function($sm) {
                    $tableGateway = $sm->get('AdministrationOfficialTableGateway');
                    $table = new AdministrationOfficialTable($tableGateway);
                    return $table;
                },
                'AdministrationOfficialTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AdministrationOfficial());
                    return new TableGateway('administrationOfficials', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\SmsConfigurationTable' => function($sm) {
                    $tableGateway = $sm->get('SmsConfigurationTableGateway');
                    $table = new SmsConfigurationTable($tableGateway);
                    return $table;
                },
                'SmsConfigurationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SmsConfiguration());
                    return new TableGateway('smsConfiguration', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\ApproverTable' => function($sm) {
                    $tableGateway = $sm->get('ApproverTableGateway');
                    $table = new ApproverTable($tableGateway);
                    return $table;
                },
                'ApproverTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Approver());
                    return new TableGateway('approvers', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\OrganizationAdditionalDetailTable' => function($sm) {
                    $tableGateway = $sm->get('OrganizationAdditionalDetailTableGateway');
                    $table = new OrganizationAdditionalDetailTable($tableGateway);
                    return $table;
                },
                'OrganizationAdditionalDetailTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new OrganizationAdditionalDetail());
                    return new TableGateway('oranizationAdditionalDetial', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PurchaseRequisitionCategoryTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseRequisitionCategoryTableGateway');
                    $table = new PurchaseRequisitionCategoryTable($tableGateway);
                    return $table;
                },
                'PurchaseRequisitionCategoryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseRequisitionCategory());
                    return new TableGateway('purchaseRequisitionCategory', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PurchaseRequisitionTypeTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseRequisitionTypeTableGateway');
                    $table = new PurchaseRequisitionTypeTable($tableGateway);
                    return $table;
                },
                'PurchaseRequisitionTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseRequisitionType());
                    return new TableGateway('purchaseRequisitionType', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PurchaseRequisitionTypeLimitTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseRequisitionTypeLimitTableGateway');
                    $table = new PurchaseRequisitionTypeLimitTable($tableGateway);
                    return $table;
                },
                'PurchaseRequisitionTypeLimitTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseRequisitionTypeLimit());
                    return new TableGateway('purchaseRequisitionTypeLimit', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PurchaseRequisitionTypeLimitApproverTable' => function($sm) {
                    $tableGateway = $sm->get('PurchaseRequisitionTypeLimitApproverTableGateway');
                    $table = new PurchaseRequisitionTypeLimitApproverTable($tableGateway);
                    return $table;
                },
                'PurchaseRequisitionTypeLimitApproverTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PurchaseRequisitionTypeLimitApprover());
                    return new TableGateway('purchaseRequisitionTypeLimitApprover', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\DimensionValueTable' => function($sm) {
                    $tableGateway = $sm->get('DimensionValueTableGateway');
                    $table = new DimensionValueTable($tableGateway);
                    return $table;
                },
                'DimensionValueTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DimensionValue());
                    return new TableGateway('dimensionValues', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\ItemAttributeMapTable' => function($sm) {
                    $tableGateway = $sm->get('ItemAttributeMapTableGateway');
                    $table = new ItemAttributeMapTable($tableGateway);
                    return $table;
                },
                'ItemAttributeMapTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ItemAttributeMap());
                    return new TableGateway('itemAttributeMap', $dbAdapter, null, $resultSetPrototype);
                }, 
                'Settings\Model\TemplateItemAttributeMapTable' => function($sm) {
                    $tableGateway = $sm->get('TemplateItemAttributeMapTableGateway');
                    $table = new TemplateItemAttributeMapTable($tableGateway);
                    return $table;
                },
                'TemplateItemAttributeMapTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new TemplateItemAttributeMap());
                    return new TableGateway('templateItemAttributeMap', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PromotionCombinationTable' => function($sm) {
                    $tableGateway = $sm->get('PromotionCombinationTableGateway');
                    $table = new PromotionCombinationTable($tableGateway);
                    return $table;
                },
                'PromotionCombinationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PromotionCombination());
                    return new TableGateway('promotionCombination', $dbAdapter, null, $resultSetPrototype);
                }, 
                'Settings\Model\ItemBaseCombinationRuleTable' => function($sm) {
                    $tableGateway = $sm->get('ItemBaseCombinationRuleTableGateway');
                    $table = new ItemBaseCombinationRuleTable($tableGateway);
                    return $table;
                },
                'ItemBaseCombinationRuleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ItemBaseCombinationRule());
                    return new TableGateway('itemBaseCombinationRule', $dbAdapter, null, $resultSetPrototype);
                }, 
                'Settings\Model\AttributeBaseCombinationRuleTable' => function($sm) {
                    $tableGateway = $sm->get('AttributeBaseCombinationRuleTableGateway');
                    $table = new AttributeBaseCombinationRuleTable($tableGateway);
                    return $table;
                },
                'AttributeBaseCombinationRuleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new AttributeBaseCombinationRule());
                    return new TableGateway('attributeBaseCombinationRule', $dbAdapter, null, $resultSetPrototype);
                }, 
                'Settings\Model\ApprovalDocumentDetailsTable' => function($sm) {
                    $tableGateway = $sm->get('ApprovalDocumentDetailsTableGateway');
                    $table = new ApprovalDocumentDetailsTable($tableGateway);
                    return $table;
                },
                'ApprovalDocumentDetailsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ApprovalDocumentDetails());
                    return new TableGateway('approvalDocumentDetails', $dbAdapter, null, $resultSetPrototype);
                }, 
                'Settings\Model\ApprovalWorkflowsTable' => function($sm) {
                    $tableGateway = $sm->get('ApprovalWorkflowsTableGateway');
                    $table = new ApprovalWorkflowsTable($tableGateway);
                    return $table;
                },
                'ApprovalWorkflowsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ApprovalWorkflows());
                    return new TableGateway('approvalWorkflows', $dbAdapter, null, $resultSetPrototype);
                }, 
                'Settings\Model\ApprovalWorkflowLimitsTable' => function($sm) {
                    $tableGateway = $sm->get('ApprovalWorkflowLimitsTableGateway');
                    $table = new ApprovalWorkflowLimitsTable($tableGateway);
                    return $table;
                },
                'ApprovalWorkflowLimitsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ApprovalWorkflowLimits());
                    return new TableGateway('approvalWorkflowLimits', $dbAdapter, null, $resultSetPrototype);
                }, 
                'Settings\Model\ApprovalWorkflowLimitApproversTable' => function($sm) {
                    $tableGateway = $sm->get('ApprovalWorkflowLimitApproversTableGateway');
                    $table = new ApprovalWorkflowLimitApproversTable($tableGateway);
                    return $table;
                },
                'ApprovalWorkflowLimitApproversTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new ApprovalWorkflowLimitApprovers());
                    return new TableGateway('workflowLimitApprovers', $dbAdapter, null, $resultSetPrototype);
                }, 
                'Settings\Model\PromotionEmailContactTable' => function($sm) {
                    $tableGateway = $sm->get('PromotionEmailContactTableGateway');
                    $table = new PromotionEmailContactTable($tableGateway);
                    return $table;
                },
                'PromotionEmailContactTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PromotionEmailContact());
                    return new TableGateway('promotionEmailContact', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\SmsTypeTable' => function($sm) {
                    $tableGateway = $sm->get('SmsTypeTableTableGateway');
                    $table = new SmsTypeTable($tableGateway);
                    return $table;
                },
                'SmsTypeTableTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SmsType());
                    return new TableGateway('smsType', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\SmsIncludedTable' => function($sm) {
                    $tableGateway = $sm->get('SmsIncludedTableTableGateway');
                    $table = new SmsIncludedTable($tableGateway);
                    return $table;
                },
                'SmsIncludedTableTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SmsIncluded());
                    return new TableGateway('smsIncluded', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\SmsTypeIncludedTable' => function($sm) {
                    $tableGateway = $sm->get('SmsTypeIncludedTableTableGateway');
                    $table = new SmsTypeIncludedTable($tableGateway);
                    return $table;
                },
                'SmsTypeIncludedTableTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new SmsTypeIncluded());
                    return new TableGateway('smsTypeIncluded', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\PosTemplateDetailsTable' => function($sm) {
                    $tableGateway = $sm->get('PosTemplateDetailsTableTableGateway');
                    $table = new PosTemplateDetailsTable($tableGateway);
                    return $table;
                },
                'PosTemplateDetailsTableTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new PosTemplateDetails());
                    return new TableGateway('posTemplateDetails', $dbAdapter, null, $resultSetPrototype);
                },
                'Settings\Model\DraftMessageTable' => function($sm) {
                    $tableGateway = $sm->get('DraftMessageTableTableGateway');
                    $table = new DraftMessageTable($tableGateway);
                    return $table;
                },
                'DraftMessageTableTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new DraftMessage());
                    return new TableGateway('draftMessages', $dbAdapter, null, $resultSetPrototype);
                },
            )
        );
    }

}
