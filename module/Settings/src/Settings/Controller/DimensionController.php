<?php
namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class DimensionController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;


    public function indexAction()
    {
        $this->getSideAndUpperMenus('Dimensions', null, null);

        $dimensionValueAddView = new ViewModel();
        $dimensionValueAddView->setTemplate('settings/dimension/dimension-value-add-modal');

        //get rate card list
        $paginator = $this->getPaginatedDimensions();
        $rateCardView = new ViewModel(array(
                            'dimensions' => $paginator,
                            'paginated' => true,
                        ));

        $rateCardView->setTemplate('settings/dimension/index');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/dimension.js');
        $rateCardView->addChild($dimensionValueAddView, 'dimensionValueAddView');

        return $rateCardView;
    }

    public function getPaginatedDimensions($perPage = 6, $param = null)
    {

        $this->dimensions = $this->CommonTable('Settings/Model/DimensionTable')->fetchAll(true);
        if ($param == null) {
            $this->dimensions->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->dimensions->setCurrentPageNumber($param);     
        }
        $this->dimensions->setItemCountPerPage($perPage);
        return $this->dimensions;
    }
}