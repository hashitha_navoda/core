<?php
namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class SignatureController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;


    public function indexAction()
    {
        $this->getSideAndUpperMenus('User Signatures', null, null);

        $companys = $this->CommonTable('CompanyTable')->fetchAll();
        $company = $companys->current();

        $dimensionValueAddView = new ViewModel();
        $companyFolder = $this->getCompanyImageFolderName();
        // $form = new CompanyForm($countries);
        // $dimensionValueAddView->setTemplate('settings/dimension/dimension-value-add-modal');

        //get rate card list
        $paginator = $this->getPaginatedSignatures();
        $rateCardView = new ViewModel(array(
                            'signatures' => $paginator,
                            'paginated' => true,
                            'companyFolder' => $companyFolder
                        ));

        $rateCardView->setTemplate('settings/signature/index');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/signature.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/style.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.min.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/script.js');
        // $this->getViewHelper('HeadScript')->prependFile('/js/settings/company.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.js');
        // $rateCardView->addChild($dimensionValueAddView, 'dimensionValueAddView');

        return $rateCardView;
    }

    public function getPaginatedSignatures($perPage = 6, $param = null)
    {

        $this->signatures = $this->CommonTable('Settings/Model/SignatureDetailsTable')->fetchAll(true);
        if ($param == null) {
            $this->signatures->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->signatures->setCurrentPageNumber($param);     
        }
        $this->signatures->setItemCountPerPage($perPage);
        return $this->signatures;
    }
}