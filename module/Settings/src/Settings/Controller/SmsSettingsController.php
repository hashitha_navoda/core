<?php
namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Settings\Form\SmsSettingForm;

class SmsSettingsController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'notification_settings_upper_menu';
    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;


    public function indexAction()
    {
        $this->getSideAndUpperMenus('Notification Settings', 'SMS Setting', null);

        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        $smsConfs = (object) $this->CommonTable('Settings\Model\SmsConfigurationTable')->fetchAll();

        $config = $this->getServiceLocator()->get('config');
        $serviceProvider = $config['sms_service_provider'];
       
        $form = new SmsSettingForm($serviceProvider);

        if ($smsConfs) {
            $form->get('serviceProvider')->setValue($smsConfs->serviceProvider);
            $form->get('uName')->setValue($smsConfs->userName);
            $form->get('pword')->setValue($smsConfs->password);
            $form->get('alias')->setValue($smsConfs->alias);
        }

        $index = new ViewModel(array(
            'form' => $form,
            'smsServiceStatus' => $displaySettings->smsServiceStatus
        ));

        $index->setTemplate('settings/sms-settings/index');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/sms-setting.js');

        return $index;
    }
}