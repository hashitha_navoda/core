<?php

namespace Settings\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Form\ItemAttributeForm;

class ItemAttributeController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'settings_products_upper_menu';

    public function indexAction()
    {

        $this->getSideAndUpperMenus('Product Setup', 'Item Attributes');

        $attributeList = $this->getPaginatedAttribute();

        $uomDetails = [];
        $uomList = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        foreach ($uomList as $key => $value) {
            $uomDetails[$value->uomID] = $value->uomName;
        }
        array_unshift($uomDetails,"None");
        $attribute = new ItemAttributeForm(
            array(
                'uomList' => $uomDetails,
            )
        );
        $valueAdd = new ViewModel(array('form' =>$attribute));
        $valueAdd->setTemplate('settings/item-attribute/add-values');
          
        $view = new ViewModel(array(
            'form' => $attribute,
            'attributeList' => $attributeList,
            'paginated' => true
        ));
        $view->addChild($valueAdd, 'itemAttrValue');

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/item-attribute.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return $view;
    }

    protected function getPaginatedAttribute($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Settings/Model/ItemAttributeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        
        return $this->paginator;
    }

}
