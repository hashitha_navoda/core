<?php

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class ApprovalWorkflowSettingsController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = null;
    protected $downMenus = 'approval_workflow_down_menu';
    protected $paginator;

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', null, null, 'Approval Workflow Settings');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/approval-workflow-setup.js');

        $getShowingApprovalWorkflowDocuments = $this->getShowingApprovalWorkflowDocumentsData();

        $workflowSettingView = new ViewModel(
            array(
                'showingDocumentList' => $getShowingApprovalWorkflowDocuments
            )
           
        );

        return $workflowSettingView;
    }

    public function getShowingApprovalWorkflowDocumentsData() {
        $documents = $this->CommonTable('Settings/Model/ApprovalDocumentDetailsTable')->fetchAll();

        $showingDocumentList = [];

        foreach ($documents as $key => $doc) {
            $showingDocumentList[$doc['documentTypeID']] = [
                "docID" => $doc['documentTypeID'],
                "documentName" => $doc['documentTypeName'],
                "isActive" => ($doc['isActive'] == "1") ? 1:0,
                "icon" => ($doc['isActive'] == "1") ? 'fa-check-square-o': 'fa-square-o' 
            ];
        }

        return $showingDocumentList;
    }

}
