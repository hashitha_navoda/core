<?php

namespace Settings\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class DisplaySettingsController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'general_settings_upper_menu';

    public function indexAction()
    {

        $this->getSideAndUpperMenus('General Settings', 'Display Settings');
        $this->useAccounting = $this->user_session->useAccounting;

        $displayData = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();


        $displayArr = [];

        if (array_key_exists('isUseSalesInvoiceReturn', $displayData)) {
            $temp = [
                'displayCol' => 'isUseSalesInvoiceReturn', 
                'displayTitle' => 'Sales Invoice Return',
                'isActive' => ($displayData['isUseSalesInvoiceReturn'] == "1") ? 1:0, 
                'icon' => ($displayData['isUseSalesInvoiceReturn'] == "1") ? 'fa-check-square-o': 'fa-square-o',
            ];

            $displayArr[] = $temp; 
        }

        if (array_key_exists('productWiseSalesPersonEnable', $displayData)) {
            $temp = [
                'displayCol' => 'productWiseSalesPersonEnable', 
                'displayTitle' => 'Product Wise Sales Person',
                'isActive' => ($displayData['productWiseSalesPersonEnable'] == "1") ? 1:0, 
                'icon' => ($displayData['productWiseSalesPersonEnable'] == "1") ? 'fa-check-square-o': 'fa-square-o',
            ];

            $displayArr[] = $temp; 
        }

        $view = new ViewModel(array(
            'displayData' => $displayArr
        ));

        $view->setTemplate('settings/general-settings/display-settings.phtml');

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/display-settings.js');

        return $view;
    }
}
