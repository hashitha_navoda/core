<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains products promotions related controller functions
 */

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Settings\Form\CreatePromotionForm;
use Settings\Model\Promotion;
use Settings\Model\PromotionProduct;
use Settings\Model\InvoicePromotion;

/**
 * company controller class create
 */
class PromotionController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'settings_promotions_upper_menu';
    protected $downMenus = 'promotions_up_down_menu';
    protected $paginator;
    protected $allLocations;

    public function createAction()
    {
        $this->getSideAndUpperMenus('Promotions', 'Add Promotion', 'CRM', 'Creations');
        $config = $this->getServiceLocator()->get('config');
        $userDateFormat = $this->convertUserDateFormatToPhpFormat();
        $currentdate = $this->getUserDateTime($this->getGMTDateTime(), $userDateFormat);
        $userDateFormat = $this->getUserDateFormat();
        $createPromotionForm = new CreatePromotionForm($config['promotionTypes'], $userDateFormat, $currentdate);
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/promotion.js');
        $locationID = $this->user_session->userActiveLocation['locationID'];


        $ruleAdd = new ViewModel();
        $ruleAdd->setTemplate('settings/promotion/add-rules');


        $attrDetails = [];
        $itemAttrDataSet = $this->CommonTable('/settings/Model/itemAttributeValueTable')->selectAll();
        foreach ($itemAttrDataSet as $key => $value) {
            $attrDetails[$value['itemAttributeID']] = array(
                'itemAttributeID' => $value['itemAttributeID'],
                'description' => $value['itemAttributeCode']." - ".$value['itemAttributeName'],
                );
        }
        // $itemAttributeAddView = new ViewModel(
        //     array(
        //         'itemAttrDetails' => $attrDetails,
        //     ));

        $ruleAttrAdd = new ViewModel(array(
                'itemAttrDetails' => $attrDetails,
            ));
        $ruleAttrAdd->setTemplate('settings/promotion/add-attr-rule');


        $view = new ViewModel(array(
            'form' => $createPromotionForm,
            'locationID' => $locationID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol
        ));
        $view->addChild($ruleAdd, 'combintionRule');
        $view->addChild($ruleAttrAdd, 'combintionAttrRule');
        return $view;
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Promotions', 'View Promotion', 'CRM', 'Creations');
        $config = $this->getServiceLocator()->get('config');
        $userDateFormat = $this->convertUserDateFormatToPhpFormat();
        $currentdate = $this->getUserDateTime($this->getGMTDateTime(), $userDateFormat);
        $userDateFormat = $this->getUserDateFormat();
        $createPromotionForm = new CreatePromotionForm($config['promotionTypes'], $userDateFormat, $currentdate);
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/promotion.js');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $promotionID = $this->params()->fromRoute('param1');
        $promotionDetails = $this->getPromotionDetails($promotionID);
        $createPromotionForm->get('promotionName')->setValue($promotionDetails[$promotionID]['promoName']);
        $createPromotionForm->get('promotionFrom')->setValue($promotionDetails[$promotionID]['promoFromDate']);
        $createPromotionForm->get('promotionTo')->setValue($promotionDetails[$promotionID]['promoToDate']);
        $createPromotionForm->get('promotionType')->setValue($promotionDetails[$promotionID]['promoType'])->setAttribute('disabled', 'true');
        $createPromotionForm->get('promotionDescription')->setValue($promotionDetails[$promotionID]['promoDesc']);
        $createPromotionForm->setAttribute("class", "form-horizontal edit");
        $createPromotionForm->setAttribute("data-id", $promotionID);



        $ruleAdd = new ViewModel();
        $ruleAdd->setTemplate('settings/promotion/add-rules');


        $attrDetails = [];
        $itemAttrDataSet = $this->CommonTable('/settings/Model/itemAttributeValueTable')->selectAll();
        foreach ($itemAttrDataSet as $key => $value) {
            $attrDetails[$value['itemAttributeID']] = array(
                'itemAttributeID' => $value['itemAttributeID'],
                'description' => $value['itemAttributeCode']." - ".$value['itemAttributeName'],
                );
        }
        // $itemAttributeAddView = new ViewModel(
        //     array(
        //         'itemAttrDetails' => $attrDetails,
        //     ));

        $ruleAttrAdd = new ViewModel(array(
                'itemAttrDetails' => $attrDetails,
            ));
        $ruleAttrAdd->setTemplate('settings/promotion/add-attr-rule');

        $view = new ViewModel(array(
            'form' => $createPromotionForm,
            'locationID' => $promotionDetails[$promotionID]['promoLocationID'],
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'promoData' => $promotionDetails[$promotionID]
        ));

        $view->addChild($ruleAdd, 'combintionRule');
        $view->addChild($ruleAttrAdd, 'combintionAttrRule');
        return $view;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Promotions', 'View Promotion', 'CRM', 'Creations');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->getPaginatedPromotions($locationID);
        $promotionListView = new ViewModel(array(
            'promotions' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'paginated' => TRUE
        ));
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/promotion.js');
        return $promotionListView;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Promotions', 'View Promotion', 'CRM', 'Creations');
        $promotionID = $this->params()->fromRoute('param1');
        $promotionDetails = $this->getPromotionDetails($promotionID);
        $promotionDetails[$promotionID]['companyCurrencySymbol'] = $this->companyCurrencySymbol;
        $promoView = new ViewModel($promotionDetails[$promotionID]);
        return $promoView;
    }

    public function emailsAction()
    {
        $this->upperMenus = 'settings_promotion_emails_upper_menu';
        $this->getSideAndUpperMenus('Promotions', 'Products & Services', 'CRM', 'Campaigns');
        $allLocations = $this->allLocations;

        foreach ($allLocations as $key => $value) {
            $locations[$key] = $value['locationName'] . '-' . $value['locationCode'];
        }

        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        // $smsServiceStatus =$displaySettings->smsServiceStatus;
        // var_dump($displaySettings->smsServiceStatus);
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/promotion.js');

        return new ViewModel(array(
            'locations' => $locations,
            'smsServiceStatus'=>$displaySettings->smsServiceStatus,
        ));
    }

    public function directPromotionAction()
    {
        $this->upperMenus = 'settings_promotion_emails_upper_menu';
        $this->getSideAndUpperMenus('Promotions', 'Direct Promotions', 'CRM', 'Campaigns');
        $allLocations = $this->allLocations;

        foreach ($allLocations as $key => $value) {
            $locations[$key] = $value['locationName'] . '-' . $value['locationCode'];
        }

        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        // $smsServiceStatus =$displaySettings->smsServiceStatus;
        // var_dump($displaySettings->smsServiceStatus);
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/direct-promotional-sms-email.js');

        $view = new ViewModel(array(
            'locations' => $locations,
            'smsServiceStatus'=>$displaySettings->smsServiceStatus,
        ));

        $contactListView = new ViewModel();
        $contactListView->setTemplate('invoice/contacts/contact-list-modal');


        $draftMessagesView = new ViewModel();
        $draftMessagesView->setTemplate('settings/promotion/draft-message-modal');

        $view->addChild($contactListView, 'contactListView');
        $view->addChild($draftMessagesView, 'draftMessagesView');

        return $view;
    }

    public function viewEmailsAction()
    {
        $this->upperMenus = 'settings_promotion_emails_upper_menu';
        $this->getSideAndUpperMenus('Promotions', 'View Email', 'CRM', 'Campaigns');
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/promotion.js');
        $this->getPaginatedPromotionEmails();
        $promoEmailList = new ViewModel(array(
            'paginated' => TRUE,
            'promoEmails' => $this->paginator
        ));
        $promoEmailList->setTemplate('settings/promotion/list-promotion-emails');
        return $promoEmailList;
    }

    public function customerEventsAction()
    {
        $this->upperMenus = 'settings_promotion_emails_upper_menu';
        $this->getSideAndUpperMenus('Promotions', 'Customer Events', 'CRM', 'Campaigns');
        $dateFormat = $this->getUserDateFormat();
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/promotion.js');

        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

        return new ViewModel(array(
            'dateFormat' => $dateFormat,
            'smsServiceStatus'=>$displaySettings->smsServiceStatus,
        ));
    }

    public function customerCategoriesAction()
    {

    }

    private function getPromotionDetails($promotionID)
    {
        $promotionType = $this->CommonTable('Settings\Model\PromotionTable')->getPromotion($promotionID)['promotionType'];
        $promotionData = $this->CommonTable('Settings\Model\PromotionTable')->getPromotionWithDetails($promotionID, $promotionType);
        if ($promotionType == 1 || $promotionType == 3) {
            $promotionDetails = array();
            foreach ($promotionData as $promo) {
                $tempPromo['promoID'] = $promo['promotionID'];
                $tempPromo['promoName'] = $promo['promotionName'];
                $tempPromo['promoFromDate'] = $promo['promotionFromDate'];
                $tempPromo['promoToDate'] = $promo['promotionToDate'];
                $tempPromo['promoType'] = $promo['promotionType'];
                $tempPromo['promoDesc'] = $promo['promotionDescription'];
                $tempPromo['promoLocationID'] = $promo['promotionLocation'];
                if ($promo['promotionLocation'] != NULL) {
                    $tempPromo['promoLocationName'] = $this->CommonTable('Settings\Model\LocationTable')->getLocationByID($promo['promotionLocation'])->locationName;
                } else {
                    $tempPromo['promoLocationName'] = 'Apply for every location';
                }
                $tempPromo['promoStatusID'] = $promo['promotionStatus'];
                $tempPromo['promoStatus'] = $this->getStatusCode($promo['promotionStatus']);
                $tempPromo['promoStatus'] = $this->getStatusCode($promo['promotionStatus']);

                $relatedCombinations = $this->CommonTable('Settings\Model\PromotionCombinationTable')->getRelatedCombinationsByPromotionId($promo['promotionID']);

                $combinations = [];
                foreach ($relatedCombinations as $key => $combination) {

                    $rules = [];

                    if ($promo['promotionType'] == 1) {
                        $relatedRules = $this->CommonTable('Settings\Model\ItemBaseCombinationRuleTable')->getRelatedRulesByCombinationId($combination['promotionCombinationID']);

                        foreach ($relatedRules as $key => $rule) {
                            $rule['productName'] = $rule['productCode'].'-'.$rule['productName'];
                            $rules[] = $rule;
                        }
                    } elseif ($promo['promotionType'] == 3) {
                        $relatedRules = $this->CommonTable('Settings\Model\AttributeBaseCombinationRuleTable')->getRelatedRulesByCombinationId($combination['promotionCombinationID']);

                        foreach ($relatedRules as $key => $rule) {
                            if (!empty($rule['attributeID'])) {
                                $itemAttrData = $this->CommonTable('Settings\Model\ItemAttributeTable')->getDataByItemAttrID($rule['attributeID'])->current();
                                $rule['attributeTypeName'] = $itemAttrData['itemAttributeCode'].'-'.$itemAttrData['itemAttributeName'];
                                $rule['attributeType'] = $itemAttrData['itemAttributeID'];
                            } elseif ($rule['attributeID'] == 0) {
                                $rule['attributeTypeName'] = "Quantity";
                            }

                            if (!empty($rule['attributeValueID'])) {
                                $itemAttrValData = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getValueByItemAtributeValueID($rule['attributeValueID'])->current();
                                $rule['attributeValName'] = $itemAttrValData['itemAttributeValueDescription'];
                                $rule['attributeValID'] = $itemAttrValData['itemAttributeValueID'];
                            }
                            $rules[] = $rule;
                        }
                    }

                    $combination['ruleSet'] = $rules;
                    $combinations [] = $combination;
                }
                $tempPromo['combinations'] = $combinations;
                $promotionDetails[$promo['promotionID']] = $tempPromo;
            }
        } else if ($promotionType == 2) {
            $promotionDetails = array();
            $promoArray = $promotionData[0];
            $tempPromo['promoID'] = $promoArray['promotionID'];
            $tempPromo['promoName'] = $promoArray['promotionName'];
            $tempPromo['promoFromDate'] = $promoArray['promotionFromDate'];
            $tempPromo['promoToDate'] = $promoArray['promotionToDate'];
            $tempPromo['promoType'] = $promoArray['promotionType'];
            $tempPromo['promoDesc'] = $promoArray['promotionDescription'];
            $tempPromo['promoLocationID'] = $promoArray['promotionLocation'];
            if ($promoArray['promotionLocation'] != NULL) {
                $tempPromo['promoLocationName'] = $this->CommonTable('Settings\Model\LocationTable')->getLocationByID($promoArray['promotionLocation'])->locationName;
            } else {
                $tempPromo['promoLocationName'] = 'Apply for every location';
            }
            $tempPromo['promoStatusID'] = $promoArray['promotionStatus'];
            $tempPromo['promoStatus'] = $this->getStatusCode($promoArray['promotionStatus']);
            $tempPromo['invoicePromotionID'] = $promoArray['invoicePromotionID'];
            $tempPromo['minValue'] = $promoArray['invoicePromotionMinValue'];
            $tempPromo['maxValue'] = $promoArray['invoicePromotionMaxValue'];
            $tempPromo['discountType'] = $promoArray['invoicePromotionDiscountType'];
            $tempPromo['discountAmount'] = $promoArray['invoicePromotionDiscountAmount'];
            $promotionDetails[$promoArray['promotionID']] = $tempPromo;
        }
        return $promotionDetails;
    }

    private function getPaginatedPromotions($locationID = NULL)
    {
        if ($locationID == NULL) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
        }
        $this->paginator = $this->CommonTable('Settings\Model\PromotionTable')->getPromotions(TRUE, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    private function getPaginatedPromotionEmails()
    {
        $this->paginator = $this->CommonTable('Settings\Model\PromotionEmailTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

}
