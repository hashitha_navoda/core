<?php

/**
 * @author Sandun Dissanayake <sandun@thinkcube.com>
 * Contains methods which related to Wizard
 */

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Settings\Form\CompanyForm;
use Settings\Form\CreateLocationForm;
use Settings\Form\UomForm;
use Settings\Form\TaxForm;
use Inventory\Form\CategoryForm;
use Accounting\Form\GlAccountSetupForm;
use Zend\Session\Container;

class WizardController extends CoreController
{
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public $statusArray = array(
        "1" => "Active",
        "0" => "Deactive",
        "2" => "Lock"
    );

    public function companyAction()
    {
        $this->layout('settings/layout/wizard-layout.phtml');
        $this->layout()->setVariable('header', 'Company');

        $companys = $this->CommonTable('CompanyTable')->fetchAll();
        $company = $companys->current();
        $companyFolder = $this->getCompanyImageFolderName();

        $config = $this->getServiceLocator()->get('config');
        $countries = $config['all_countries'];
        $form = new CompanyForm($countries);

        /**
         * use for set default currency in the begining...
         */
        //if company details alredy have then set submit button as update and bind company details to company form otherwise set as Save
        if (!$company) {
            $form->get('submit')->setValue('Save');
        } else {
            $form->bind($company);
            $form->get('submit')->setValue('Update');
        }

        // ViewModel for companyIndex
        $companyIndex = new viewModel(array(
            'form' => $form,
            'company' => ($company) ? $company : '',
            'cdn_url' => $this->cdnUrl,
            'companyFolder' => $companyFolder));
        $companyIndex->setTemplate('settings/company/index');

        // ViewModel for companyContent
        $companyView = new ViewModel(array('header' => 'Company'));
        $companyView->addChild($companyIndex, 'companyContent');

        $this->getViewHelper('HeadLink')->prependStylesheet('/css/style.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.min.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/script.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/company.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');

        return $companyView;
    }

    public function locationAction()
    {
        $this->layout('settings/layout/wizard-layout.phtml');
        $form = new CreateLocationForm($this->statusArray);

        $this->getPaginatedLocations();

        $locationIndex = new ViewModel(array(
            'form' => $form)
        );
        $locationIndex->setTemplate('settings/location/location-add');

        $locationView = new ViewModel();
        $locationView->addChild($locationIndex, 'locationContent');

        $searchLocation = new ViewModel(array('locations' => $this->paginator));
        $searchLocation->setTemplate('settings/location/index');
        $locationView->addChild($searchLocation, 'searchLocationContent');


        $viewHeader = new ViewModel();
        $viewHeader->setTemplate("settings/location/location-header");
        $locationView->addChild($viewHeader, 'locationHeader');

        return $locationView;
    }

    public function referenceAction()
    {
        $this->layout('settings/layout/wizard-layout.phtml');

        $locationData = [];
        $referencess = [];
        $locationReference = [];

        //get all reference details
        $reference = $this->CommonTable('Settings\Model\ReferenceTable')->fetchAll();
        foreach ($reference as $t) {
            if (($t->referenceID == 1 || $t->referenceID == 2 || $t->referenceID == 3 || $t->referenceID == 4 || $t->referenceID == 5 || $t->referenceID == 6 || $t->referenceID == 7 || $t->referenceID == 8 || $t->referenceID == 9 || $t->referenceID == 10 || $t->referenceID == 11 || $t->referenceID == 12 || $t->referenceID == 13 || $t->referenceID == 14 || $t->referenceID == 15 || $t->referenceID == 16) && $t->locationID == '') {
                $referencess[$t->referenceID] = $t;
            }
            if (($t->referenceID == 1 || $t->referenceID == 2 || $t->referenceID == 3 || $t->referenceID == 4 || $t->referenceID == 5 || $t->referenceID == 6 || $t->referenceID == 7 || $t->referenceID == 8 || $t->referenceID == 9 || $t->referenceID == 10 || $t->referenceID == 11 || $t->referenceID == 12 || $t->referenceID == 13 || $t->referenceID == 14 || $t->referenceID == 15 || $t->referenceID == 16) && $t->locationID != '') {
                $locationReference[$t->referenceID] = $t;
            }
        }

        //get all active locations
        $locations = $this->CommonTable('Settings\Model\LocationTable')->activeFetchAll();
        foreach ($locations as $l) {
            $locationData[$l->locationID] = $l;
        }

        //create view model for this action
        $viewmodel = new ViewModel(array(
            'reference' => $referencess,
            'locationReference' => $locationReference,
            'locations' => $locationData,
        ));
        $viewmodel->setTemplate('settings/company/reference');

        $referenceView = new ViewModel();
        $referenceView->addChild($viewmodel, 'referenceContent');

        //append all css and script file for this phtml file
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/reference.js');
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');

        return $referenceView;
    }

    public function uomAction()
    {
        $this->layout('settings/layout/wizard-layout.phtml');

        $form = new UomForm();
        $uomList = $this->CommonTable('Settings/Model/UomTable')->fetchAll();

        $view = new ViewModel(array(
            'form' => $form,
            'uomList' => $uomList,
        ));
        $view->setTemplate('settings/uom/index');

        $uomView = new ViewModel();
        $uomView->addChild($view, 'uomContent');

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/uom.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return $uomView;
    }

    public function taxAction()
    {
        $this->layout('settings/layout/wizard-layout.phtml');
        $this->layout()->setVariable('header', 'Tax');
        $currencyValue = $this->getDefaultCurrency();
        $this->CommonTable('Settings\Model\DisplaySetupTable')->savedefaultDisplaySetup($currencyValue);
        $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
        $data = $result->current();
        $Country = $this->CommonTable('CompanyTable')->fetchAll();
        $this->user_session->userSetCountry = $Country->current()->country;
        $this->user_session->companyCurrencySymbol = $data['currencySymbol'];
        $languages = $this->getLanguageForSelectedCountry();
        $this->user_session->userLanguages = $languages;
        $tr_number = $this->CommonTable('CompanyTable')->getTRNumber();

        $tax_result = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();

        while ($row = $tax_result->current()) {
            $taxes[] = array(
                'taxID' => $row->id,
                'taxName' => $row->taxName,
                'taxType' => $row->taxType,
                'taxPrecentage' => $row->taxPrecentage,
                'state' => $row->state
            );
        }

        if (isset($taxes) && is_array($taxes)) {
            foreach ($taxes as $tx) {
                if ($tx['taxType'] == "v")
                    $tx_ar[$tx['taxID']] = $tx['taxName'];
            }
        }

        $tax_form = new TaxForm(array(
            "name" => "tax_form",
            "tr_number" => (isset($tr_number->trNumber)) ? $tr_number->trNumber : null,
            "taxes" => (isset($tx_ar)) ? $tx_ar : null,
            "chrg_tax" => isset($tr_number->trNumber) ? "y" : "n",
        ));
        $taxListView = new ViewModel(array(
            'taxes' => (isset($taxes)) ? $taxes : NULL
        ));
        $taxListView->setTemplate('settings/tax/tax-list');

        $tax_index = new ViewModel(array(
            'tx_form' => $tax_form,
            'taxes' => (isset($taxes)) ? $taxes : NULL,
            'cmp_tx' => (isset($tx_ar)) ? $tx_ar : null,
            'oldTrNumber' => (isset($tr_number->oldTaxRegNumber)) ? $tr_number->oldTaxRegNumber : null,
            'useAccounting' => $this->useAccounting,
        ));

        $tax_index->addChild($taxListView, 'taxList');

        $tax_index->setTemplate('settings/tax/index');

        $taxContent = new ViewModel();
        $taxContent->addChild($tax_index, 'taxContent');

        return $taxContent;
    }

    public function categoryAction()
    {
        $this->layout('settings/layout/wizard-layout');
        $categoryList = $this->CommonTable('Inventory/Model/CategoryTable')->fetchAll();

        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $form = new CategoryForm($allCategoryList);

        $view = new ViewModel(array(
            'form' => $form,
            'categoryList' => $categoryList
        ));
        $view->setTemplate('inventory/category/index');
        $categoryContent = new ViewModel();
        $categoryContent->addChild($view, 'categoryContent');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['categories']);
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/category.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return $categoryContent;
    }

    public function finishSetupAction()
    {
        $this->layout('settings/layout/wizard-layout');
        $this->layout()->setVariable('header', 'Finish Setup');

        $finishSetupView = new ViewModel();
        $finishSetupView->setTemplate('settings/wizard/finish-setup');

        return $finishSetupView;
    }

    public function accountsAction()
    {
        $this->layout('settings/layout/wizard-layout');
        $this->layout()->setVariable('header', 'Define Accounts');

        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/gl-account-setup.js');

        $companys = $this->CommonTable('CompanyTable')->fetchAll();
        $company = $companys->current();

        $useAccounting = $company->companyUseAccounting;

        $glAccountSetupForm = new GlAccountSetupForm('CreateGlAccountSetup');
        $glAccountSetupForm->get('saveGlAccountSetup')->setAttribute("class",'hidden');

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $financeAccountsArray = $this->getFinanceAccounts();
        
        if (!empty($useAccounting) && count($glAccountExist) > 0 ) {
            
            $glAccountSetupData = $glAccountExist->current();
            
            $glAccountSetupID = $glAccountSetupData->glAccountSetupID;
            $glAccountSetupForm->get('glAccountSetupID')->setValue($glAccountSetupID);        
            $glAccountSetupForm->get('saveGlAccountSetup')->setValue("Update");        
            
            $glAccountSetupItemDefaultSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
            $glAccountSetupItemDefaultSalesAccountName = $financeAccountsArray[$glAccountSetupItemDefaultSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultSalesAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultSalesAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultSalesAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultSalesAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultSalesAccountName);        
            
            $glAccountSetupItemDefaultInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
            $glAccountSetupItemDefaultInventoryAccountName = $financeAccountsArray[$glAccountSetupItemDefaultInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultInventoryAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultInventoryAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultInventoryAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultInventoryAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultInventoryAccountName);        
            
            $glAccountSetupItemDefaultCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
            $glAccountSetupItemDefaultCOGSAccountName = $financeAccountsArray[$glAccountSetupItemDefaultCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultCOGSAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultCOGSAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultCOGSAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultCOGSAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultCOGSAccountName);        
            
            $glAccountSetupItemDefaultAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
            $glAccountSetupItemDefaultAdjusmentAccountName = $financeAccountsArray[$glAccountSetupItemDefaultAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultAdjusmentAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultAdjusmentAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultAdjusmentAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultAdjusmentAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultAdjusmentAccountName);

            $glAccountSetupSalesAndCustomerReceivableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
            $glAccountSetupSalesAndCustomerReceivableAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerReceivableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerReceivableAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerReceivableAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerReceivableAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerReceivableAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerReceivableAccountName);        
        
            $glAccountSetupSalesAndCustomerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
            $glAccountSetupSalesAndCustomerSalesAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerSalesAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerSalesAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerSalesAccountName);        
            
            $glAccountSetupSalesAndCustomerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
            $glAccountSetupSalesAndCustomerSalesDiscountAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerSalesDiscountAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesDiscountAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerSalesDiscountAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesDiscountAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerSalesDiscountAccountName);        
            
            $glAccountSetupSalesAndCustomerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
            $glAccountSetupSalesAndCustomerAdvancePaymentAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerAdvancePaymentAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerAdvancePaymentAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerAdvancePaymentAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerAdvancePaymentAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerAdvancePaymentAccountName);        
            
            $glAccountSetupPurchasingAndSupplierPayableAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID;
            $glAccountSetupPurchasingAndSupplierPayableAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierPayableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierPayableAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPayableAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierPayableAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPayableAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierPayableAccountName);        
            
            $glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID;
            $glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountName);        
            
            $glAccountSetupPurchasingAndSupplierGrnClearingAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID;
            $glAccountSetupPurchasingAndSupplierGrnClearingAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierGrnClearingAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierGrnClearingAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierGrnClearingAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierGrnClearingAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierGrnClearingAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierGrnClearingAccountName);        
            
            $glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID;
            $glAccountSetupPurchasingAndSupplierAdvancePaymentAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountName);        
    
            $glAccountSetupGeneralExchangeVarianceAccountID = $glAccountSetupData->glAccountSetupGeneralExchangeVarianceAccountID;
            $glAccountSetupGeneralExchangeVarianceAccountName = $financeAccountsArray[$glAccountSetupGeneralExchangeVarianceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralExchangeVarianceAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralExchangeVarianceAccountID')->setAttribute('data-id',$glAccountSetupGeneralExchangeVarianceAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralExchangeVarianceAccountID')->setAttribute('data-value',$glAccountSetupGeneralExchangeVarianceAccountName);        
            
            $glAccountSetupGeneralProfitAndLostYearAccountID = $glAccountSetupData->glAccountSetupGeneralProfitAndLostYearAccountID;
            $glAccountSetupGeneralProfitAndLostYearAccountName = $financeAccountsArray[$glAccountSetupGeneralProfitAndLostYearAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralProfitAndLostYearAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralProfitAndLostYearAccountID')->setAttribute('data-id',$glAccountSetupGeneralProfitAndLostYearAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralProfitAndLostYearAccountID')->setAttribute('data-value',$glAccountSetupGeneralProfitAndLostYearAccountName);        

            $glAccountSetupGeneralBankChargersAccountID = $glAccountSetupData->glAccountSetupGeneralBankChargersAccountID;
            $glAccountSetupGeneralBankChargersAccountName = $financeAccountsArray[$glAccountSetupGeneralBankChargersAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralBankChargersAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralBankChargersAccountID')->setAttribute('data-id',$glAccountSetupGeneralBankChargersAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralBankChargersAccountID')->setAttribute('data-value',$glAccountSetupGeneralBankChargersAccountName);     

            $glAccountSetupGeneralDeliveryChargersAccountID = $glAccountSetupData->glAccountSetupGeneralDeliveryChargersAccountID;
            $glAccountSetupGeneralDeliveryChargersAccountName = $financeAccountsArray[$glAccountSetupGeneralDeliveryChargersAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralDeliveryChargersAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralDeliveryChargersAccountID')->setAttribute('data-id',$glAccountSetupGeneralDeliveryChargersAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralDeliveryChargersAccountID')->setAttribute('data-value',$glAccountSetupGeneralDeliveryChargersAccountName);           
            
            $glAccountSetupGeneralLoyaltyExpenseAccountID = $glAccountSetupData->glAccountSetupGeneralLoyaltyExpenseAccountID;
            $glAccountSetupGeneralLoyaltyExpenseAccountName = $financeAccountsArray[$glAccountSetupGeneralLoyaltyExpenseAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralLoyaltyExpenseAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralLoyaltyExpenseAccountID')->setAttribute('data-id',$glAccountSetupGeneralLoyaltyExpenseAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralLoyaltyExpenseAccountID')->setAttribute('data-value',$glAccountSetupGeneralLoyaltyExpenseAccountName);           
        }else {
            $glAccountSetupID = 1;
            $glAccountSetupForm->get('glAccountSetupID')->setValue($glAccountSetupID);        
            $glAccountSetupForm->get('saveGlAccountSetup')->setValue("Save");        
            
            $glAccountSetupItemDefaultSalesAccountID = 30;
            $glAccountSetupItemDefaultSalesAccountName = $financeAccountsArray[$glAccountSetupItemDefaultSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultSalesAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultSalesAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultSalesAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultSalesAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultSalesAccountName);        
            
            $glAccountSetupItemDefaultInventoryAccountID = 8;
            $glAccountSetupItemDefaultInventoryAccountName = $financeAccountsArray[$glAccountSetupItemDefaultInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultInventoryAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultInventoryAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultInventoryAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultInventoryAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultInventoryAccountName);        
            
            $glAccountSetupItemDefaultCOGSAccountID = 86;
            $glAccountSetupItemDefaultCOGSAccountName = $financeAccountsArray[$glAccountSetupItemDefaultCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultCOGSAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultCOGSAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultCOGSAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultCOGSAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultCOGSAccountName);        
            
            $glAccountSetupItemDefaultAdjusmentAccountID = 87;
            $glAccountSetupItemDefaultAdjusmentAccountName = $financeAccountsArray[$glAccountSetupItemDefaultAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultAdjusmentAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultAdjusmentAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultAdjusmentAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultAdjusmentAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultAdjusmentAccountName);

            $glAccountSetupSalesAndCustomerReceivableAccountID = 6;
            $glAccountSetupSalesAndCustomerReceivableAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerReceivableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerReceivableAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerReceivableAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerReceivableAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerReceivableAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerReceivableAccountName);        
        
            $glAccountSetupSalesAndCustomerSalesAccountID = 30;
            $glAccountSetupSalesAndCustomerSalesAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerSalesAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerSalesAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerSalesAccountName);        
            
            $glAccountSetupSalesAndCustomerSalesDiscountAccountID = 32;
            $glAccountSetupSalesAndCustomerSalesDiscountAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerSalesDiscountAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesDiscountAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerSalesDiscountAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesDiscountAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerSalesDiscountAccountName);        
            
            $glAccountSetupSalesAndCustomerAdvancePaymentAccountID = 20;
            $glAccountSetupSalesAndCustomerAdvancePaymentAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerAdvancePaymentAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerAdvancePaymentAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerAdvancePaymentAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerAdvancePaymentAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerAdvancePaymentAccountName);        
            
            $glAccountSetupPurchasingAndSupplierPayableAccountID = 16;
            $glAccountSetupPurchasingAndSupplierPayableAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierPayableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierPayableAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPayableAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierPayableAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPayableAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierPayableAccountName);        
            
            $glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID = 35;
            $glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountName);        
            
            $glAccountSetupPurchasingAndSupplierGrnClearingAccountID = 82;
            $glAccountSetupPurchasingAndSupplierGrnClearingAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierGrnClearingAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierGrnClearingAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierGrnClearingAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierGrnClearingAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierGrnClearingAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierGrnClearingAccountName);        
            
            $glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID = 9;
            $glAccountSetupPurchasingAndSupplierAdvancePaymentAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountName);        
    
            $glAccountSetupGeneralExchangeVarianceAccountID = 84;
            $glAccountSetupGeneralExchangeVarianceAccountName = $financeAccountsArray[$glAccountSetupGeneralExchangeVarianceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralExchangeVarianceAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralExchangeVarianceAccountID')->setAttribute('data-id',$glAccountSetupGeneralExchangeVarianceAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralExchangeVarianceAccountID')->setAttribute('data-value',$glAccountSetupGeneralExchangeVarianceAccountName);        
            
            $glAccountSetupGeneralProfitAndLostYearAccountID = 29;
            $glAccountSetupGeneralProfitAndLostYearAccountName = $financeAccountsArray[$glAccountSetupGeneralProfitAndLostYearAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralProfitAndLostYearAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralProfitAndLostYearAccountID')->setAttribute('data-id',$glAccountSetupGeneralProfitAndLostYearAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralProfitAndLostYearAccountID')->setAttribute('data-value',$glAccountSetupGeneralProfitAndLostYearAccountName);        

            $glAccountSetupGeneralBankChargersAccountID = 78;
            $glAccountSetupGeneralBankChargersAccountName = $financeAccountsArray[$glAccountSetupGeneralBankChargersAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralBankChargersAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralBankChargersAccountID')->setAttribute('data-id',$glAccountSetupGeneralBankChargersAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralBankChargersAccountID')->setAttribute('data-value',$glAccountSetupGeneralBankChargersAccountName);     

            $glAccountSetupGeneralDeliveryChargersAccountID = 85;
            $glAccountSetupGeneralDeliveryChargersAccountName = $financeAccountsArray[$glAccountSetupGeneralDeliveryChargersAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralDeliveryChargersAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralDeliveryChargersAccountID')->setAttribute('data-id',$glAccountSetupGeneralDeliveryChargersAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralDeliveryChargersAccountID')->setAttribute('data-value',$glAccountSetupGeneralDeliveryChargersAccountName);           
            
            $glAccountSetupGeneralLoyaltyExpenseAccountID = 83;
            $glAccountSetupGeneralLoyaltyExpenseAccountName = $financeAccountsArray[$glAccountSetupGeneralLoyaltyExpenseAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralLoyaltyExpenseAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralLoyaltyExpenseAccountID')->setAttribute('data-id',$glAccountSetupGeneralLoyaltyExpenseAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralLoyaltyExpenseAccountID')->setAttribute('data-value',$glAccountSetupGeneralLoyaltyExpenseAccountName);
        }
        
        $glAccountSetupView = new ViewModel(
            array(
                'glAccountSetupForm' => $glAccountSetupForm,
            )
        );

        $glAccountSetupView->setTemplate('accounting/gl-account-setup/index');

        $display = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll();
        $displaysetup = $display->current();

        $FIFOCostingFlag = $displaysetup->FIFOCostingFlag; 
        $averageCostingFlag = $displaysetup->averageCostingFlag;

        $accountsView = new ViewModel(
            array(
                'useAccounting' => $useAccounting,
                'FIFOCostingFlag' => $FIFOCostingFlag,
                'averageCostingFlag' => $averageCostingFlag,
            )
        );

        $accountsView->addChild($glAccountSetupView, 'glAccountSetupView');

        return $accountsView;
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function gets paginated locations
     */
    private function getPaginatedLocations()
    {
        $this->paginator = $this->CommonTable('Settings\Model\LocationTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage();
    }

}
