<?php

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class ApprovalWorkflowController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'approval_workflow_upper_menu';
    protected $downMenus = 'approval_workflow_down_menu';
    protected $paginator;

    public function createAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'Create', null, 'Approval Workflows');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/approval-workflow.js');

        $documents = $this->CommonTable('Settings/Model/ApprovalDocumentDetailsTable')->getActiveWFDocumentTypes();

        $employees = $this->CommonTable('Settings\Model\ApproverTable')->fetchAll();
        $employeeList = [];
        foreach ($employees as $employee) {
            $employeeList[] = $employee;
        }

        $workflowView = new ViewModel(
            array(
                'employees' => $employeeList,
                'documents' => $documents
            )
           
        );

        return $workflowView;
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'View', null, 'Approval Workflows');
        $workflowId = $paramIn = $this->params()->fromRoute('param1');
        $wfData = $this->CommonTable("Settings\Model\ApprovalWorkflowsTable")->getWorkflowDetailsByID($workflowId);
        $wfDetails = array();
        // $financeAccountsArray = $this->getFinanceAccounts();
        foreach ($wfData as $wfType) {
            $wfTmp = array();
            $wfTmp['workflowID'] = $wfType['approvalWorkflowID'];
            $wfTmp['workflowName'] = $wfType['workflowName'];
            $wfTmp['workflowCode'] = $wfType['workflowCode'];
            $wfTmp['rootDocumentTypeID'] = $wfType['rootDocumentTypeID'];
            $wfTmp['documentTypeName'] = $wfType['documentTypeName'];
            $wfTmp['workflowStatus'] = $wfType['workflowStatus'];
            $wfLimitsArray = isset($wfDetails['workflowLimits']) ? $wfDetails['workflowLimits'] : array();
            $wfLimitApprovers = isset($wfLimitsArray[$wfType['approvalWorkflowLimitsID']]['wfLimitApprovers']) ? $wfLimitsArray[$wfType['approvalWorkflowLimitsID']]['wfLimitApprovers'] : array();
            if ($wfType['approvalWorkflowLimitsID'] != NULL) {
                $wfLimitsArray[$wfType['approvalWorkflowLimitsID']] = array(
                    'approvalWorkflowLimitsID' => $wfType['approvalWorkflowLimitsID'],
                    'approvalWorkflowLimitMin' => $wfType['approvalWorkflowLimitMin'],
                    'approvalWorkflowLimitMax' => $wfType['approvalWorkflowLimitMax'],
                );
                $wfLimitApprovers[$wfType['workflowLimitApproverID']] = $wfType['workflowLimitApprover'];
                $wfLimitsArray[$wfType['approvalWorkflowLimitsID']]['wfLimitApprovers'] = $wfLimitApprovers;
            }
            $wfTmp['workflowLimits'] = $wfLimitsArray;
            $wfDetails = $wfTmp;
        }


        $this->getViewHelper('HeadScript')->prependFile('/js/settings/approval-workflow.js');
        // $expenseCategories = $this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->fetchAll();
        $employees = $this->CommonTable('Settings\Model\ApproverTable')->fetchAll();
        $employeeList = [];
        foreach ($employees as $employee) {
            $employeeList[] = $employee;
        }
        return new ViewModel(array(
            'data' => $wfDetails,
            'employees' => $employeeList,
            'workflowID' => $workflowId
        ));
    }


    public function listAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'View', null, 'Approval Workflows');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/approval-workflow.js');
        $this->getPaginatedExpenseTypes();
        return new ViewModel(array(
            'approvalWorkflows' => $this->paginator,
            'paginated' => TRUE));
    }

    private function getPaginatedExpenseTypes()
    {

        $this->paginator = $this->CommonTable('Settings\Model\ApprovalWorkflowsTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(6);
    }

}
