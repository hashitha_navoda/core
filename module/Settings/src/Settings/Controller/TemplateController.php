<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * Contains methods related to Document Template management in Company Setup area
 */

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class TemplateController extends CoreController
{

    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;
    protected $templateDetails;

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
    }

    public function indexAction()
    {
        exit;
    }

    public function getDefaultTemplate($documentTypeName)
    {
        if (!$this->templateDetails) {
            $tplTable = $this->CommonTable('Settings\Model\TemplateTable');
            $this->templateDetails = $tplTable->getDefaultTemplate($documentTypeName)->current();
            $this->templateDetails['templateOptions'] = (isset($this->templateDetails['templateOptions'])) ? $this->templateDetails['templateOptions'] : "";
            $this->templateDetails['templateOptions'] = json_decode($this->templateDetails['templateOptions'], true) ? : [];
        }

        return $this->templateDetails;
    }

    public function getTemplatesForDocument($documentTypeName)
    {
        $tplTable = $this->CommonTable('Settings\Model\TemplateTable');
        return $tplTable->getTemplatesForDocType($documentTypeName);
    }

    public function getTemplateByID($templateID)
    {
        if (!$this->templateDetails) {
            $tplTable = $this->CommonTable('Settings\Model\TemplateTable');
            $this->templateDetails = $tplTable->get($templateID)->current();
            $this->templateDetails['templateOptions'] = (isset($this->templateDetails['templateOptions'])) ? $this->templateDetails['templateOptions'] : "";
            $this->templateDetails['templateOptions'] = json_decode($this->templateDetails['templateOptions'], true) ? : [];
        }

        return $this->templateDetails;
    }

    public function renderDefaultTemplate($documentType, $data, $file = false)
    {
        $template = $this->getDefaultTemplate($documentType);
        return $this->render($template, $data, $file);
    }

    public function renderTemplateByID($templateID, $data, $file = false, $isPdf = false)
    {
        $template = $this->getTemplateByID($templateID);

        if ($isPdf && $template['isAdvanceTemplateEnabled']) {
            return $this->renderAdvancedTemplate($template, $data, $file);
        }

        return $this->render($template, $data, $file);
    }

    public function renderAdvancedTemplate($template, $data, $file = false)
    {
        $templateContent = [
            'header' => $template['templateHeader'],
            'body' => $template['templateBody'],
            'footer' => $template['templateFooter']
        ];

        $vars = $this->getAllTemplateVars();
        $templateVars = $vars[$template['documentTypeID']];

        // Prepare array using the passed data
        foreach ($templateVars as $varGroup) {
            foreach ($varGroup as $placeholder => $var) {
                $templateDataVars["[" . ($placeholder) . "]"] = (isset($data[$var[0]])) ? htmlentities($data[$var[0]]) : '';
            }
        }

        foreach ($templateContent as $key => $content) {

            if (!$content && $key != 'footer') {
                continue;
            }

            // Replace the non-editable content area (where the actual data table should come) with a placeholder
            $content = preg_replace("/(<noneditabletable>)(.*)(<\/noneditabletable>)/s", '[Data Table]', $content);

            // Array containing actual data to replace the placeholders in template content
            $templateDataVars['[Data Table]'] = $data['data_table'];
            if(isset($data['payment_table'])){
                $templateDataVars['[Payment Table]'] = $data['payment_table'];
            }

            if(isset($data['cheque_table'])){
                $templateDataVars['[Cheque Table]'] = $data['cheque_table'];
            }

            // Make final content by replacing the placeholders
            $fixedTemplateContent = str_replace(array_keys($templateDataVars), array_values($templateDataVars), $content);
            $baseUrl = $this->getBaseUrl();

            $view = new ViewModel(array($key => $fixedTemplateContent, 'baseUrl' => $baseUrl));
            $view->setTemplate('/core/advance-templates/' . $key);
            $view->setTerminal(TRUE);

            $htmlContent = $this->getServiceLocator()->get('viewrenderer')->render($view);
            $htmlContent = $this->minifyHtml($htmlContent);

            if ($file) {
                $this->writePdfContentToFile($file, $key, $template['documentTypeName'], $htmlContent);
            }
        }

        return true;
    }

    public function render($template, $data, $file = false)
    {

        $templateContent = $template['templateContent'];
        $documentSize = $template['documentSizeName'];

        // Replace the non-editable content area (where the actual data table should come) with a placeholder
        $templateContent = preg_replace("/(<noneditabletable>)(.*)(<\/noneditabletable>)/s", '[Data Table]', $templateContent);

        $vars = $this->getAllTemplateVars();
        $templateVars = $vars[$template['documentTypeID']];

        // Prepare array using the passed data
        foreach ($templateVars as $varGroup) {
            foreach ($varGroup as $placeholder => $var) {
                $templateDataVars["[" . ($placeholder) . "]"] = (isset($data[$var[0]])) ? htmlentities($data[$var[0]]) : '';
            }
        }

        // Array containing actual data to replace the placeholders in template content
        $templateDataVars['[Data Table]'] = $data['data_table'];
        if(isset($data['payment_table'])){
            $templateDataVars['[Payment Table]'] = $data['payment_table'];
        }

        if(isset($data['cheque_table'])){
            $templateDataVars['[Cheque Table]'] = $data['cheque_table'];
        }

        // Make final content by replacing the placeholders
        $fixedTemplateContent = str_replace(array_keys($templateDataVars), array_values($templateDataVars), $templateContent);

        $view = new ViewModel(array('body' => $fixedTemplateContent));
        $view->setTemplate('/core/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $documentSize))));
        $view->setTerminal(TRUE);

        $htmlContent = $this->getServiceLocator()
                ->get('viewrenderer')
                ->render($view);

        $htmlContent = $this->minifyHtml($htmlContent);

        // write document to file
        if ($file) {
            $this->writeDocumentToFile($file, $template['documentTypeName'], $htmlContent);
        }

        return $htmlContent;
    }

    public function writePdfContentToFile($filename, $contentType, $documentTypeName, $htmlContent)
    {
        $file = $filename;
        $arr = str_split($file, 2);
        $filename = $contentType . '.html';
        $path = $this->getDocumentPath($documentTypeName) . "/" . $arr[0] . "/" . $arr[1] . "/" . substr($file, 4);

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        file_put_contents($path . '/' . $filename, $htmlContent);

        return true;
    }

    public function writeDocumentToFile($filename, $documentTypeName, $htmlContent)
    {
        $page = null;
        if (preg_match('/^[a-zA-Z0-9]*\-pg([0-9]*)/', $filename, $matches) && $matches[1]) {
            $page = $matches[1];
            $filename = str_replace('-pg' . $page, '', $filename);
        }

        $file = $filename;
        $arr = str_split($file, 2);
        $filename = substr($file, 4) . '.html';
        $path = $this->getDocumentPath($documentTypeName) . "/" . $arr[0] . "/" . $arr[1];

        if ($page) {
            $filename = $page . '.html';
            $path = $path . '/' . substr($file, 4);
        }
        $originalFileName = substr($file, 4) . '-original.html';

        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }

        file_put_contents($path . '/' . $filename, $htmlContent);
        if (!file_exists($path . '/' . $originalFileName)) {
            file_put_contents($path . '/' . $originalFileName, $htmlContent);
        }

        return true;
    }

    public function getDocumentPath($documentTypeName)
    {
        $docFolder = strtolower(preg_replace('/([^a-zA-Z0-9])*/', '', $documentTypeName));

        return $this->getCompanyDataFolder() . '/documents/' . $docFolder;
    }

    public function getDocumentFile($filename, $documentTypeName)
    {
        $filename = $filename . '.html';

        $docFolder = strtolower(preg_replace('/([^a-zA-Z0-9])*/', '', $documentTypeName));

        $path = $this->getCompanyDataFolder() . '/documents/' . $docFolder;

        if (file_exists(($path . '/' . $filename))) {
            return file_get_contents($path . '/' . $filename);
        } else {
            return false;
        }
    }

    public function minifyHtml($html)
    {
        $search = array(
            '/\>[^\S]+/s', // strip whitespaces after tags, except space
            '/[^\S]+\</s', // strip whitespaces before tags, except space
            '/(\s)+/s'       // shorten multiple whitespace sequences
        );

        $replace = array(
            '>',
            '<',
            '\\1'
        );

        $html = preg_replace($search, $replace, $html);

        return $html;
    }

    public function getAllTemplateVars()
    {

        $config = $this->getServiceLocator()->get('config');
        $tplVars = $config["template-vars"];

        return $tplVars;
    }

}
