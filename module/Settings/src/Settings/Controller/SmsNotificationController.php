<?php
namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Settings\Form\SmsNotificationForm;

class SmsNotificationController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'sms_notification_upper_menu';
    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;

     /**
     * Action for loading the After invoice view.
     *
     * @return ViewModel Returns a Zend view model.
     * @author Sahan Siriwardhana <sahan@thinkcube.com>
     */
    public function afterInvoiceAction()
    {
        $this->getSideAndUpperMenus('Sms Notification', 'After Invoice', 'CRM');

        $config = $this->getServiceLocator()->get('config');
        $smsSendType = $config['SMS_SEND_TYPE'];
        
        $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("After Invoice");

        $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);

        while ($row = $smsIncludedDetailsByType->current()) {
            $smsIncluded[] = array(
                'value' => $row['smsTypeIncludedId'],
                'label' => $row['smsIncludedName'],
                'selected' => $row['isSelected'],
            );
        }
        
        $form = new SmsNotificationForm(array(
            'smsSendType' =>$smsSendType,
            'smsIncludedDetails' => $smsIncluded,
        ));
        if ($smsTypeDetails) {
            $form->get('messageDescription')->setValue($smsTypeDetails->smsTypeMessage);
            $form->get('smsSendType')->setValue((int)$smsTypeDetails->smsSendType);
        }

        $index = new ViewModel(array(
            'form' => $form,
            'smsTypeIsEnable' => $smsTypeDetails->isEnable,
            'smsTypeID' => $smsTypeDetails->smsTypeID,
            'smsIncludedIsSelected' => $smsTypeDetails->isIncludeInvoiceDetails,
            'note' => 'You can send SMS notification after the invoice',
            'info' => array(
                'automatic-info' => 'Message will be sent automatically if the customer is registered with valid mobile number .',
                'manual-info' => 'The user has to send the SMS manually after invoice by clicking "Send SMS" button'
            ),
            'includeLabel' => 'Include invoice details'
        ));

        $index->setTemplate('settings/sms-notification/index');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/sms-notification.js');

        return $index;
    }

     /**
     * Action for loading the After Receipt view.
     *
     * @return ViewModel Returns a Zend view model.
     * @author Sahan Siriwardhana <sahan@thinkcube.com>
     */
    public function afterReceiptAction()
    {
        $this->getSideAndUpperMenus('Sms Notification', 'After Receipt', 'CRM');

        $config = $this->getServiceLocator()->get('config');
        $smsSendType = $config['SMS_SEND_TYPE'];
       
        $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("After Receipt");

        $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);

        while ($row = $smsIncludedDetailsByType->current()) {
            $smsIncluded[] = array(
                'value' => $row['smsTypeIncludedId'],
                'label' => $row['smsIncludedName'],
                'selected' => $row['isSelected'],
            );
        }
        
        $form = new SmsNotificationForm(array(
            'smsSendType' =>$smsSendType,
            'smsIncludedDetails' => $smsIncluded,
        ));
        if ($smsTypeDetails) {
            $form->get('messageDescription')->setValue($smsTypeDetails->smsTypeMessage);
            $form->get('smsSendType')->setValue((int)$smsTypeDetails->smsSendType);
        }

        $index = new ViewModel(array(
            'form' => $form,
            'smsTypeIsEnable' => $smsTypeDetails->isEnable,
            'smsTypeID' => $smsTypeDetails->smsTypeID,
            'smsIncludedIsSelected' => $smsTypeDetails->isIncludeInvoiceDetails,
            'note' => 'You can send SMS notification to customers after the receipt of the invoice',
            'info' => array(
                'automatic-info' => 'Message will be sent automatically if the customer is registered with valid mobile number.  <br/> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp <strong>Use automatic option for POS payment notifications .</strong>',
                'manual-info' => 'The user has to send the SMS manually after invoice by clicking "Send SMS" button'
            ),
            'includeLabel' => 'Include payment details'
        ));

        $index->setTemplate('settings/sms-notification/index');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/sms-notification.js');

        return $index;
    }

    /**
     * Action for loading the Due Balance view.
     *
     * @return ViewModel Returns a Zend view model.
     * @author Sahan Siriwardhana <sahan@thinkcube.com>
     */
    public function dueBalanceAction()
    {
        $this->getSideAndUpperMenus('Sms Notification', 'Due Balance', 'CRM');

        $config = $this->getServiceLocator()->get('config');
        // $smsSendType = $config['SMS_SEND_TYPE'];
        $smsSendType =array(
                2 => 'Manual',
            );
        $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("Due Balance");

        $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);

        while ($row = $smsIncludedDetailsByType->current()) {
            $smsIncluded[] = array(
                'value' => $row['smsTypeIncludedId'],
                'label' => $row['smsIncludedName'],
                'selected' => $row['isSelected'],
            );
        }
        
        $form = new SmsNotificationForm(array(
            'smsSendType' =>$smsSendType,
            'smsIncludedDetails' => $smsIncluded,
        ));
        // var_dump($smsTypeDetails);
        if ($smsTypeDetails) {
            $form->get('messageDescription')->setValue($smsTypeDetails->smsTypeMessage);
            $form->get('smsSendType')->setValue((int)$smsTypeDetails->smsSendType);
        }

        $index = new ViewModel(array(
            'form' => $form,
            'smsTypeIsEnable' => $smsTypeDetails->isEnable,
            'smsTypeID' => $smsTypeDetails->smsTypeID,
            'smsIncludedIsSelected' => $smsTypeDetails->isIncludeInvoiceDetails,
            'note' => 'You can send SMS notification to customers regarding their amount of due balance',
            'info' => array(
                'automatic-info' => 'Message will be sent automatically to the selected customers and on the defined day frame',
                'manual-info' => 'Message will be sent manually to the selected customers'
            ),
            'smsTypeDayFrame' => $smsTypeDetails->smsTypeDayFrame,
            'smsTypeDayFrameDay' => $smsTypeDetails->smsTypeDayFrameDay,
            'smsTypeSelectedCustomerType' => $smsTypeDetails->smsTypeSelectedCustomerType,
        ));

        $index->setTemplate('settings/sms-notification/index-due-balance');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/sms-notification.js');

        return $index;
    }

    /**
     * Action for loading the Due Balance view.
     *
     * @return ViewModel Returns a Zend view model.
     * @author Sahan Siriwardhana <sahan@thinkcube.com>
     */
    public function dueInvoiceAction()
    {
        $this->getSideAndUpperMenus('Sms Notification', 'Due Invoice', 'CRM');

        $config = $this->getServiceLocator()->get('config');
        // $smsSendType = $config['SMS_SEND_TYPE'];
        $smsSendType =array(
                2 => 'Manual',
        );
        $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("Due Invoice");

        $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);

        while ($row = $smsIncludedDetailsByType->current()) {
            $smsIncluded[] = array(
                'value' => $row['smsTypeIncludedId'],
                'label' => $row['smsIncludedName'],
                'selected' => $row['isSelected'],
            );
        }
        
        $form = new SmsNotificationForm(array(
            'smsSendType' =>$smsSendType,
            'smsIncludedDetails' => $smsIncluded,
        ));
        // var_dump($smsTypeDetails);
        if ($smsTypeDetails) {
            $form->get('messageDescription')->setValue($smsTypeDetails->smsTypeMessage);
            $form->get('smsSendType')->setValue((int)2);
        }

        $index = new ViewModel(array(
            'form' => $form,
            'smsTypeIsEnable' => $smsTypeDetails->isEnable,
            'smsTypeID' => $smsTypeDetails->smsTypeID,
            'smsIncludedIsSelected' => $smsTypeDetails->isIncludeInvoiceDetails,
            'note' => 'You can send SMS notification to remind customers regarding their upcoming due invoices',
            'info' => array(
                'automatic-info' => 'Message will be sent automatically to the selected customers and on the defined day frame',
                'manual-info' => 'Message will be sent manually to the selected customers'
            ),
            'smsTypeDayFrame' => $smsTypeDetails->smsTypeDayFrame,
            'smsTypeDayFrameDay' => $smsTypeDetails->smsTypeDayFrameDay,
            'smsTypeSelectedCustomerType' => $smsTypeDetails->smsTypeSelectedCustomerType,
        ));

        $index->setTemplate('settings/sms-notification/index-due-invoice');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/sms-notification.js');

        return $index;
    }

    /**
     * Action for loading the Due Balance view.
     *
     * @return ViewModel Returns a Zend view model.
     * @author Sahan Siriwardhana <sahan@thinkcube.com>
     */
    public function serviceReminderAction()
    {
        $this->getSideAndUpperMenus('Sms Notification', 'Service Reminder', 'CRM');

        $config = $this->getServiceLocator()->get('config');
        // $smsSendType = $config['SMS_SEND_TYPE'];
        $smsSendType =array(
                2 => 'Manual',
        );
        $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("Service Reminder");

        $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);

        while ($row = $smsIncludedDetailsByType->current()) {
            $smsIncluded[] = array(
                'value' => $row['smsTypeIncludedId'],
                'label' => $row['smsIncludedName'],
                'selected' => $row['isSelected'],
            );
        }
        
        $form = new SmsNotificationForm(array(
            'smsSendType' =>$smsSendType,
            'smsIncludedDetails' => $smsIncluded,
        ));
        // var_dump($smsTypeDetails);
        if ($smsTypeDetails) {
            $form->get('messageDescription')->setValue($smsTypeDetails->smsTypeMessage);
            $form->get('smsSendType')->setValue((int)2);
        }

        $index = new ViewModel(array(
            'form' => $form,
            'smsTypeIsEnable' => $smsTypeDetails->isEnable,
            'smsTypeID' => $smsTypeDetails->smsTypeID,
            'smsIncludedIsSelected' => $smsTypeDetails->isIncludeInvoiceDetails,
            'note' => 'You can send SMS notification to remind customers for the next invoice/service',
            'info' => array(
                'automatic-info' => 'Message will be sent automatically to the selected customers and on the defined day frame',
                'manual-info' => 'Message will be sent manually to the selected customers'
            ),
            'smsTypeDayFrame' => $smsTypeDetails->smsTypeDayFrame,
            'smsTypeDayFrameDay' => $smsTypeDetails->smsTypeDayFrameDay,
            'smsTypeSelectedCustomerType' => $smsTypeDetails->smsTypeSelectedCustomerType,
        ));

        $index->setTemplate('settings/sms-notification/index-service-reminder');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/sms-notification.js');

        return $index;
    }
    
    /**
     * Action for loading the Due Balance view.
     *
     * @return ViewModel Returns a Zend view model.
     * @author Sahan Siriwardhana <sahan@thinkcube.com>
     */
    public function postDatedChequeAction()
    {
        $this->getSideAndUpperMenus('Sms Notification', 'Post Dated Cheque', 'CRM');

        $config = $this->getServiceLocator()->get('config');
        // $smsSendType = $config['SMS_SEND_TYPE'];
        $smsSendType =array(
                2 => 'Manual',
        );
        $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("Post Dated Cheque");

        $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);

        while ($row = $smsIncludedDetailsByType->current()) {
            $smsIncluded[] = array(
                'value' => $row['smsTypeIncludedId'],
                'label' => $row['smsIncludedName'],
                'selected' => $row['isSelected'],
            );
        }
        
        $form = new SmsNotificationForm(array(
            'smsSendType' =>$smsSendType,
            'smsIncludedDetails' => $smsIncluded,
        ));
        // var_dump($smsTypeDetails);
        if ($smsTypeDetails) {
            $form->get('messageDescription')->setValue($smsTypeDetails->smsTypeMessage);
            $form->get('smsSendType')->setValue((int)2);
        }

        $index = new ViewModel(array(
            'form' => $form,
            'smsTypeIsEnable' => $smsTypeDetails->isEnable,
            'smsTypeID' => $smsTypeDetails->smsTypeID,
            'smsIncludedIsSelected' => $smsTypeDetails->isIncludeInvoiceDetails,
            'note' => 'You can send SMS notification to remind customers regarding their upcoming dated cheque',
            'info' => array(
                'automatic-info' => 'Message will be sent automatically to the selected customers and on the defined day frame',
                'manual-info' => 'Message will be sent manually to the selected customers'
            ),
            'smsTypeDayFrame' => $smsTypeDetails->smsTypeDayFrame,
            'smsTypeDayFrameDay' => $smsTypeDetails->smsTypeDayFrameDay,
            'smsTypeSelectedCustomerType' => $smsTypeDetails->smsTypeSelectedCustomerType,
        ));

        $index->setTemplate('settings/sms-notification/index-postdated-cheque');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/sms-notification.js');

        return $index;
    }
}