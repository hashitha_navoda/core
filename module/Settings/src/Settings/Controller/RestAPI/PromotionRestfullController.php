<?php

namespace Settings\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;
use Inventory\Model\Product;

/**
 *
 * ProductRestfullController
 *
 *
 * Restfull controller for products
 */
class PromotionRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList($searchKey = "", $timestamp = "")
    {
        $this->service = $this->getService('PromotionService');
        $locationID = $this->getRequest()->getHeaders()->get("Locationid")->getFieldValue();

        $promotionDetails = $this->service->getPromotionDetailsForDesktopPOS($locationID);

        if ($promotionDetails['status']) {
            return $this->returnJsonSuccess($promotionDetails['data'], $promotionDetails['msg']);
        }

        return $this->returnJsonError($products['msg']);
    }

    public function get($id)
    {
        return new JsonModel(['status' => true, 'data' => null, 'msg' => 'successfuly done']);
    }

    public function create($data)
    {
        # code...
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
