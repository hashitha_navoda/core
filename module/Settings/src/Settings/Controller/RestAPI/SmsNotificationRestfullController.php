<?php

namespace Settings\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;
use Inventory\Model\Product;

/**
 *
 * SmsNotificationRestfullController
 *
 *
 * Restful controller for products
 */
class SmsNotificationRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList($searchKey = "", $timestamp = "")
    {
        $this->service = $this->getService('SmsNotificationService');

        $promotionDetails = $this->service->getSmsNotificationDataForPOS();

        if ($promotionDetails['status']) {
            return $this->returnJsonSuccess($promotionDetails['data'], $promotionDetails['msg']);
        }

        return $this->returnJsonError($products['msg']);
    }

    public function get($id)
    {
        return new JsonModel(['status' => true, 'data' => null, 'msg' => 'successfuly done']);
    }

    public function create($data)
    {
        $this->service = $this->getService('SmsNotificationService');

        $this->beginTransaction();
        $smsSendDetails = $this->service->sendSmsNotification($data);

        if ($smsSendDetails['status']) {
            $this->commit();
            return $this->returnJsonSuccess($smsSendDetails['data'], $smsSendDetails['msg']);
        }

        $this->rollback();
        return $this->returnJsonError($smsSendDetails['msg']);
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
