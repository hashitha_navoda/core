<?php

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Settings\Form\CustomerEventForm;
use Invoice\Form\AddCustomerForm;

/**
 * Description of CustomerEventController
 *
 * @author shermilan
 */
class CustomerEventController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'customer_event_upper_menu';
    protected $paginator;

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Customer Event', 'Create', 'CRM');
        $this->pagintedCustomerEvent();
        $customerEventForm = new CustomerEventForm();
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/customer-event.js');
//      For display toggle purpose of add and edit view
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/modalAddCustomer.js');
        $view = new ViewModel([
            'customerEventForm' => $customerEventForm,
            'customerEvents' => $this->paginator,
            'paginated' => true,
        ]);
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();

        $currencies = array();
        $currencies['0'] = '';
        foreach ($currencyResultSet as $row) {
            $currencies[$row->currencyID] = $row->currencyName;
        }
        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        foreach ($paymentTermsResultSet as $row) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }

        // get person Title (Mr, Miss ..) from personTitle.config file
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];
        $countries = ['' => ''] + $config['all_countries'];

//      get Active Customer Evnt for customer creation form
        $customerEvents = $this->CommonTable('Settings\Model\CustomerEventTable')->getActiveCutomerEvents();
        $customerEvenList = [];
        $customerEvenList[''] = '';
        foreach ($customerEvents as $cEvent) {
            $customerEvenList[$cEvent['customerEventID']] = $cEvent['customerEventName'];
        }

//        get customer Code
        $cusRefData = $this->getReferenceNoForLocation(20, $locationID);
        $cusRid = $refData["refNo"];
        $locationReferenceID = $cusRefData["locRefID"];
        $customerCode = $this->getReferenceNumber($locationReferenceID);
        $currencyValue = $this->getDefaultCurrency();

        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

        //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $customerForm = new AddCustomerForm(NULL, $paymentTerms, $currencies, $customerTitle, $currencyValue, $customerCategories, "yyyy-mm-dd", NULL, $customerEvenList, $customerPriceLists);
        $customerForm->get('customerFormsubmit')->setValue(_("Add New Customer"));
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);
        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
        ));
        $customerAddView->setTemplate('invoice/customer/customer-add-modal');
        $view->addChild($customerAddView, 'customerAddView');
        return $view;
    }

    public function pagintedCustomerEvent($rowCount = 6)
    {
        $this->paginator = $this->CommonTable('Settings\Model\CustomerEventTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($rowCount);
    }

}
