<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contain tax related controller functions
 */

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Settings\Form\TaxForm;

class TaxController extends CoreController
{

    protected $userID;
    protected $useAccounting;

    public function __construct()
    {
        $user_session = new Container('ezBizUser');
        $this->userID = $user_session->userID;
        $this->useAccounting= $user_session->useAccounting;
    }

    public function indexAction()
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $this->getEvent()->getViewModel()->sidemenu = $globaldata['company_side_menu'];
        $this->getEvent()->getViewModel()->sidemenuselected = 'Tax';
        $this->getEvent()->getViewModel()->uppermenuselected = 'Tax';
        $tr_number = $this->CommonTable('CompanyTable')->getTRNumber();

        $tax_result = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();

        while ($row = $tax_result->current()) {
            $taxes[] = array(
                'taxID' => $row->id,
                'taxName' => $row->taxName,
                'taxType' => $row->taxType,
                'taxPrecentage' => $row->taxPrecentage,
                'state' => $row->state
            );
        }
        if (isset($taxes) && is_array($taxes)) {
            foreach ($taxes as $tx) {
                if ($tx['taxType'] == "v")
                    $tx_ar[$tx['taxID']] = strtoupper($tx['taxName']);
            }
        }

        $tax_form = new TaxForm(array(
            "name" => "tax_form",
            "tr_number" => (isset($tr_number->trNumber)) ? $tr_number->trNumber : null,
            "taxes" => (isset($tx_ar)) ? $tx_ar : null,
            "chrg_tax" => isset($tr_number->trNumber) ? "y" : "n",
        ));
        $taxListView = new ViewModel(array(
            'taxes' => (isset($taxes)) ? $taxes : NULL
        ));
        $taxListView->setTemplate('settings/tax/tax-list');

        $tax_index = new ViewModel(array(
            'tx_form' => $tax_form,
            'taxes' => (isset($taxes)) ? $taxes : NULL,
            'cmp_tx' => (isset($tx_ar)) ? $tx_ar : null,
            'oldTrNumber' => (isset($tr_number->oldTaxRegNumber)) ? $tr_number->oldTaxRegNumber : null,
            'useAccounting' => $this->useAccounting
        ));

        $tax_index->addChild($taxListView, 'taxList');
        return $tax_index;
    }

    protected function getViewHelper($helperName)
    {
        return $this->getServiceLocator()->get('viewhelpermanager')->get($helperName);
    }

}
