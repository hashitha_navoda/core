<?php

/**
 * @author PRASA7  <prasanna@thinkcube.com>
 * This file contains Location related controller functions
 */

namespace Settings\Controller;

use Core\Controller\CoreController;
use Settings\Form\CreateLocationForm;
use Settings\Model\Location;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class LocationController extends CoreController
{

    protected $user_session;
    protected $userID;
    protected $username;
    protected $cdnUrl;
    public $statusArray = array(
        "1" => "Active",
        "0" => "Inactive",
        "2" => "Lock"
    );

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->packageID = $this->user_session->packageID;
    }

    /**
     * create new location
     * @return \Zend\View\Model\ViewModel
     */
    public function createAction()
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $this->getEvent()->getViewModel()->sidemenu = $globaldata['company_side_menu'];
        $this->getEvent()->getViewModel()->sidemenuselected = 'Location';
        $form = new CreateLocationForm($this->statusArray);
        $view = new ViewModel(array('form' => $form));
        return $view;
    }

    /**
     * edit locations
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction()
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $this->getEvent()->getViewModel()->sidemenu = $globaldata['company_side_menu'];
        $this->getEvent()->getViewModel()->sidemenuselected = 'Location';
        $form = new CreateLocationForm($this->statusArray);

        $locationID = $this->params()->fromRoute('param1');
        $result = $this->CommonTable('Settings\Model\LocationTable')->getLocation($locationID);

        $form->get('locationID')->setValue($locationID);
        $form->get('locationCode')->setValue($result->locationCode);
        $form->get('locationName')->setValue($result->locationName);
        $form->get('locationEmail')->setValue($result->locationEmail);
        $form->get('locationTelephone')->setValue($result->locationTelephone);
        $form->get('locationFax')->setValue($result->locationFax);
        $form->get('locationTax')->setValue($result->locationTax);
        $form->get('locationAddressLine1')->setValue($result->locationAddressLine1);
        $form->get('locationAddressLine2')->setValue($result->locationAddressLine2);
        $form->get('locationAddressLine3')->setValue($result->locationAddressLine3);
        $form->get('locationAddressLine4')->setValue($result->locationAddressLine4);
        $form->get('locationStatus')->setValue($result->locationStatus);
        $viewmodel = new ViewModel(array(
            'form' => $form
        ));
        return $viewmodel;
    }

    public function indexAction()
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $this->getEvent()->getViewModel()->sidemenu = $globaldata['company_side_menu'];
        $this->getEvent()->getViewModel()->sidemenuselected = 'Location';

        $this->getPaginatedLocations();

        $locationCount = $this->CommonTable('Settings\Model\LocationTable')->fetchAll();

        $hideLocationCreate = false;
        if ($this->packageID == "1") {
            $hideLocationCreate = true;
        } else if ($this->packageID == "2") {
            if (count($locationCount) >= 5) {
                $hideLocationCreate = true;
            }
        } 

        $viewHeader = new ViewModel(array(
            'hideLocationCreate' => $hideLocationCreate
        ));
        $viewHeader->setTemplate("settings/location/location-header");

        $userActiveLocationID = $this->user_session->userActiveLocation['locationID'];
        $view = new ViewModel(array(
            'locations' => $this->paginator,
            'userCurrentLocation' => $userActiveLocationID
                )
        );

        $view->addChild($viewHeader, 'locationHeader');
        $this->setLogMessage("Location list accessed");
        return $view;
    }

    public function viewLocationListAction()
    {
        $this->getPaginatedLocations();
        $userActiveLocationID = $this->user_session->userActiveLocation['locationID'];
        $view = new ViewModel(array(
            'locations' => $this->paginator,
            'userCurrentLocation' => $userActiveLocationID
                )
        );

        $view->setTemplate("settings/location/index");

        $this->html = $view;
        return $this->JSONRespondHtml();
    }

//    public function getLocationViewAction()
//    {
//        $request = $this->getRequest();
//
//        $locationExistingDetails = $this->CommonTable('Settings\Model\LocationTable')->getLocation($request->getPost('locationID'));
//
//        $view = new ViewModel(array('location' => $locationExistingDetails));
//        $view->setTemplate("settings/location/location-view");
//        $view->setTerminal(TRUE);
//        $locationID = $request->getPost('locationID');
//        $this->setLogMessage("Location viewed(id:$locationID)");
//        $this->html = $view;
//        return $this->JSONRespondHtml();
//    }

    public function getLocationAddViewAction()
    {
        $form = new CreateLocationForm($this->statusArray);

        $view = new ViewModel(array('form' => $form));
        $view->setTemplate("settings/location/location-add");
        $view->setTerminal(TRUE);
        $this->setLogMessage("Location add view loaded");
        $this->html = $view;
        return $this->JSONRespondHtml();
    }

    /**
     * This fucntion adds users
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return location list
     */
    public function addLocationAction()
    {
        $location = new Location();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $form = new CreateLocationForm($this->statusArray);
            $form->setInputFilter($location->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->addLocation($location, $request);
            }
        }

        $this->getPaginatedLocations();

        $view = new ViewModel(array(
            'locations' => $this->paginator
                )
        );

        $view->setTemplate("settings/location/index");

        $this->html = $view;
        return $this->JSONRespondHtml();
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param Object $location
     * @param request Object $request
     * @return JsonRespond
     */
    public function addLocation($location, $request)
    {
        if (isset($location, $request)) {
            $this->beginTransaction();
            $location->exchangeArray($request->getPost());
            $res = $this->CommonTable('Settings\Model\LocationTable')->saveLocation($location, $this->createEntity());
            $products = $this->CommonTable('Inventory\Model\ProductTable')->getGlobalProducts();
            foreach ($products as $key) {
                $keys = (object) $key;
                $entityID = $this->createEntity();
                $data = array(
                    'productID' => (int)$keys->productID,
                    'locationID' => (int)$res,
                    'entityID' => (int)$entityID,
                    'locationDiscountPercentage' => $keys->productDiscountPercentage,
                    'locationDiscountValue' => $keys->productDiscountValue,
                    'locationProductMinInventoryLevel' => $keys->productDefaultMinimumInventoryLevel,
                    'locationProductReOrderLevel' => $keys->productDefaultReorderLevel,
                    'defaultSellingPrice' => $keys->productDefaultSellingPrice,
                );
                $locationProduct = new \Inventory\Model\LocationProduct();
                $locationProduct->exchangeArray($data);
                $this->CommonTable('Inventory\Model\LocationProductTable')->saveLocationProduct($locationProduct);
            };
            if ($res != false) {
                $this->commit();
                $this->setLogMessage("Location created successfully");
                $this->flashMessenger()->addMessage($this->getMessage('SUCC_LOC_CREATE'));
                /// $this->msg = $this->getMessage('SUCC_LOC_CREATE'); //"Location successfully created";
                $this->status = TRUE;
            } else {
                $this->rollback();
                $this->setLogMessage("Location creation failed");
                $this->flashMessenger()->addMessage($this->getMessage('ERR_LOC_CREATE'));
                $this->msg = $this->getMessage('ERR_LOC_CREATE'); //"Location creation failed";
                $this->status = FALSE;
            }
        }
    }

    public function addLocationWizardAction()
    {
        $location = new Location();
        $request = $this->getRequest();

        if ($request->isPost()) {
            $form = new CreateLocationForm($this->statusArray);
            $form->setInputFilter($location->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $this->addLocation($location, $request);
            }

            $this->getPaginatedLocations();

            $view = new ViewModel(array(
                'locations' => $this->paginator
                    )
            );

            $view->setTemplate("settings/wizard/location");

            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getLocationUpdateFormAction()
    {
        $request = $this->getRequest();

        $form = new CreateLocationForm($this->statusArray);

        $locationExistingDetails = $this->CommonTable('Settings\Model\LocationTable')->getLocation($request->getPost('locationID'));

        $form->bind($locationExistingDetails);

        $view = new ViewModel(array('form' => $form));
        $view->setTemplate("settings/location/location-add");
        $view->setTerminal(TRUE);
        $this->setLogMessage("Location add view loaded");
        $this->msg = $this->getMessage('SUCC_LOC_UPDATE');
        $this->status = true;
        $this->html = $view;
        return $this->JSONRespondHtml();
    }

    /**
     * This fucntion adds users
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return location list
     */
    public function updateLocationAction()
    {
        $location = new Location();

        $request = $this->getRequest();

        if ($request->isPost()) {
            $state = true;
            if ($request->getPost()->locationStatus != 1) {
                $defaultLocation = $this->CommonTable('User\Model\UserTable')->checkDefaultLocation($request->getPost()->locationID);
                if ($defaultLocation->current()) {
                    $state = false;
                }
            }
            if ($state) {
                $form = new CreateLocationForm($this->statusArray);
                $form->setInputFilter($location->getInputFilter());
                $form->setData($request->getPost());

                if ($form->isValid()) {

                    $existingLocation = $this->CommonTable('Settings\Model\LocationTable')->getLocation($request->getPost()->locationID);

                    $location->exchangeArray($request->getPost());

                    $res = $this->CommonTable('Settings\Model\LocationTable')->saveLocation($location, $existingLocation->entityID);
                    if ($res) {
                        $this->updateEntity($existingLocation->entityID);
                        $this->setLogMessage("Location update successfully");
                        $this->flashMessenger()->addMessage($this->getMessage('SUCC_LOC_UPDATE'));
                        $this->msg = $this->getMessage('SUCC_LOC_UPDATE');
                        $this->status = TRUE;
                    } else {
                        $this->setLogMessage("No changes to update");
                        $this->msg = $this->getMessage('ERR_LOC_UPDATE');
                        $this->status = 'info';
                    }

                    $this->getPaginatedLocations();

                    $view = new ViewModel(array(
                        'locations' => $this->paginator
                            )
                    );

                    $view->setTemplate("settings/location/index");

                    $this->html = $view;
                    return $this->JSONRespondHtml();
                }
            } else {
                $this->msg = $this->getMessage('ERR_LOC_SETDEFAULT');
                $this->status = FALSE;
            }
        } else {
            $this->msg = $this->getMessage('ERR_LOC_INVREQ');
            $this->status = FALSE;
        }
        return $this->JSONRespond();
    }

    public function deleteLocationAction()
    {
        //Check whether the location is eligible to be deleted
        $result = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductsFromLocation($this->getRequest()->getPost('locationID'))->current();

        if ($result) {
            $this->status = FALSE;
            $this->msg = $this->getMessage('ERR_LOC_DELE');
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
        $res = $this->CommonTable('Settings\Model\LocationTable')->deleteLocation($this->getRequest()->getPost('locationID'));
        $this->getPaginatedLocations();

        $view = new ViewModel(array(
            'locations' => $this->paginator
                )
        );

        $view->setTemplate("settings/location/index");
        if ($res['status'] == TRUE) {
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_LOC_DELE');
            $this->html = $view;
            return $this->JSONRespondHtml();
        } else {
            $this->status = FALSE;
            $this->msg = $this->getMessage('ERR_LOC_DELE');
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getLocationsFromSearchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $searchLocationString = $request->getPost('searchKey');

            if ($searchLocationString != '') {
                $locations = $this->CommonTable('Settings\Model\LocationTable')->getLocationsforSearch($searchLocationString);
                $userActiveLocationID = $this->user_session->userActiveLocation['locationID'];
                $view = new ViewModel(array(
                    'locations' => $locations,
                    'paginator' => false,
                    'userCurrentLocation' => $userActiveLocationID
                        )
                );

                $view->setTemplate("settings/location/index");

                $this->html = $view;
            } else {
                $this->getPaginatedLocations();
                $userActiveLocationID = $this->user_session->userActiveLocation['locationID'];
                $view = new ViewModel(array(
                    'locations' => $this->paginator,
                    'userCurrentLocation' => $userActiveLocationID
                        )
                );
                $view->setTemplate("settings/location/index");
                $this->html = $view;
            }
            return $this->JSONRespondHtml();
        }
    }

    public function checkLocationAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            if ($this->CommonTable('Settings\Model\LocationTable')->checkLocationByCode($request->getPost('locationID'), $request->getPost('locationCode'))) {
                $this->status = TRUE;
                $this->msg = $this->getMessage('ERR_LOC_CODEEXIST');
            }
        }
        return $this->JSONRespond();
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function gets paginated locations
     */
    private function getPaginatedLocations()
    {
        $this->paginator = $this->CommonTable('Settings\Model\LocationTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

}

///////////////////////END LOCATION CONTROLLER////////////////////////
