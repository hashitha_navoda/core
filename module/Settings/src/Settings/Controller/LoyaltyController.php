<?php

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Settings\Form\LoyaltyForm;
use Settings\Model\Loyalty;
use Zend\Session\Container;

class LoyaltyController extends CoreController
{

    protected $paginator;
    protected $sideMenus = 'crm_side_menu';
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Loyalty Card', null, 'CRM', null);

        $this->paginator = $this->CommonTable('LoyaltyTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(15);

        $view = new ViewModel(['loyalty' => $this->paginator]);
        $this->setLogMessage("Loyalty card list accessed");
        return $view;
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Loyalty Card', null, 'CRM', null);

        $id = $this->params()->fromRoute('param1');

        $data = null;
        if (isset($id)) {
            $data = $this->CommonTable('LoyaltyTable')->getLoyalty($id);
        }

        $loyalty_form = new LoyaltyForm($data);

        if($this->useAccounting == 1){

            $paymentMethodData = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethodById(4);
            $accountID = 0;
            $accountName = '';
            if($paymentMethodData['paymentMethodInSales'] == 1){
                $accountName = $paymentMethodData['salesFinanceAccountsCode'].'_'.$paymentMethodData['salesFinanceAccountsName'];
                $accountID = $paymentMethodData['paymentMethodSalesFinanceAccountID'];
            }

            if(!empty($data)){
                $accountID = $data->loyaltyGlAccountID;
                $faccountData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($accountID);
                $accountName = $faccountData['financeAccountsCode'].'_'.$faccountData['financeAccountsName'];
            }

            $loyalty_form->get('loyaltyGlAccountID')->setAttribute('data-id', $accountID);
            $loyalty_form->get('loyaltyGlAccountID')->setAttribute('data-value', $accountName);
        }

        $view = new ViewModel(array(
            "loyalty_form" => $loyalty_form,
            "currency" => $this->companyCurrencySymbol,
            "update" => isset($id),
            "useAccounting" => $this->useAccounting
        ));

        $this->setLogMessage("Loyalty card create form accessed");
        return $view;
    }

    public function storeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $loyalty = new Loyalty();
            $loyalty->exchangeArray($request->getPost());

            $this->CommonTable('LoyaltyTable')->save($loyalty);
            if (isset($loyalty->id)) {
                $msg = "Loyalty card updated successfully";
            } else {
                $msg = "Loyalty card saved successfully";
            }
            $this->setLogMessage($msg);

            if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                $this->status = true;
                $this->msg = $msg;
                return $this->JSONRespond();
            }

            $this->flashMessenger()->addMessage($msg);
        }
        return $this->redirect()->toRoute('loyalty');
    }

    public function customerListAction()
    {
        $this->getSideAndUpperMenus('Loyalty Card', null, 'CRM', null);

        $loyaltyType = $this->params()->fromRoute('param1');
        $page = (int) $this->params()->fromRoute('param2', 1);

        $this->paginator = $this->CommonTable('CustomerLoyaltyTable')->getLoyaltyCustomerList($loyaltyType);
        $this->paginator->setCurrentPageNumber($page);
        $this->paginator->setItemCountPerPage(15);


        return new ViewModel(['cust_list' => $this->paginator, 'params' => ['param1' => $loyaltyType, 'param2' => $page]]);
    }

    public function getAvailableLoyaltyCodesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $loyalty_id = $request->getPost('loyalty_type');
            $codes_list = $this->CommonTable('LoyaltyTable')->getAvailableLoyaltyCodesList($loyalty_id);
            $this->data = $codes_list;
            $this->status = true;
            return $this->JSONRespond();
        }
        $this->status = false;
        return $this->JSONRespond();
    }

    public function searchLoyaltyCardsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');

            $loyaltyCards = $this->CommonTable('LoyaltyTable')->searchLoyaltyCardsForDropdown($searchKey);
            $loyaltyCardList = [];
            foreach ($loyaltyCards as $loyaltyCard) {
                $temp['value'] = $loyaltyCard['loyaltyID'];
                $temp['text']  = $loyaltyCard['loyaltyName'];
                $loyaltyCardList[] = $temp;
            }

            $this->data = array('list' => $loyaltyCardList);
            return $this->JSONRespond();
        }
    }

    public function searchLoyaltyCardNumbersForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');

            $customerLoyaltyCards = $this->CommonTable('CustomerLoyaltyTable')->searchLoyaltyCardNumbersForDropdown($searchKey);
            $customerLoyaltyCardList = [];
            foreach ($customerLoyaltyCards as $loyaltyCard) {
                $temp['value'] = $loyaltyCard['customerLoyaltyCode'];
                $temp['text']  = $loyaltyCard['customerLoyaltyCode'];
                $customerLoyaltyCardList[] = $temp;
            }

            $this->data = array('list' => $customerLoyaltyCardList);
            return $this->JSONRespond();
        }
    }

}
