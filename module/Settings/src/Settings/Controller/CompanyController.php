<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Company related controller functions
 */

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Settings\Form\CompanyForm;
use Settings\Form\DisplayForm;
use Settings\Form\CustomCurrencyForm;
use Zend\Session\Container;

/**
 * company controller class create
 */
class CompanyController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $downMenus = 'organization_structure_down_menu';
    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        $this->jobModuleEnabled = $this->user_session->jobModuleEnabled;
    }


    /**
     * company form creted in this method
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
//set company index side menu and upper menues
        $this->getSideAndUpperMenus('Organization', null, null);
//get company details
        $companys = $this->CommonTable('CompanyTable')->fetchAll();
        $company = $companys->current();
//get company image saving folder
        $companyFolder = $this->getCompanyImageFolderName();
//create company form object
        $config = $this->getServiceLocator()->get('config');
        $countries = $config['all_countries'];
        $form = new CompanyForm($countries);
//if company details alredy have then set submit button as update and bind company details to company form otherwise set as Save
        if (!$company) {
            $form->get('submit')->setValue('Save');
        } else {
            $form->bind($company);
            $form->get('submit')->setValue('Update');
        }
//create view model with attributes
        $viewmodel = new ViewModel(array(
            'form' => $form,
            'company' => ($company) ? $company : '',
            'cdn_url' => $this->cdnUrl,
            'companyFolder' => $companyFolder
        ));
//append all css and script file for this phtml file
        $this->setLogMessage('Company form accessed');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/style.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/jcrop/jquery.Jcrop.min.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/script.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/company.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.min.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/jcrop/jquery.Jcrop.js');
//return view model
        return $viewmodel;
    }

    public function displayAction()
    {
//set company display side menu and upper menues
        $this->getSideAndUpperMenus('Display Setup', null, null);

//get currency details
        $currency = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        foreach ($currency as $t) {
            $currencytypes[$t->currencyID] = $t->currencyName . ' - ' . $t->currencySymbol;
        }

//get timezone list
        $timeZoneList = array();

        $config = $this->getServiceLocator()->get('config');
        $timeZones = $config['timezones'];

        $country = $config['defaultCountry'];

        $dateformats = $config['dateformats']; ////////////////////////

        foreach ($timeZones as $timeKey => $timezone) {
            $timeZoneList[$timezone] = $timeKey;
        }

        $currencyValue = $this->getDefaultCurrency();

        //get display setup table details
        $display = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll();
        $displaysetup = $display->current();

        // has item in
        $hasItemIn = $this->CommonTable('Inventory\Model\ItemInTable')->hasItemIn();

        $currentDateFormat = $displaysetup->dateFormat; //
        $FIFOCostingFlag = $displaysetup->FIFOCostingFlag; 
        $averageCostingFlag = $displaysetup->averageCostingFlag;
        $posPrintoutImageConversionStatus = $displaysetup->posPrintoutImageConversionStatus; 
        //create dispaly form passing currencyTypes and timeZoneList
        $form = new DisplayForm(array(
            'currency' => $currencytypes,
            'timeZoneList' => $timeZoneList,
            'currencyCountry' => $currencyValue,
                ), $dateformats);

        //disable currancy drop down
        if (count($displaysetup) > 0 && !empty($displaysetup->currencyID)) {
            $form->get('currencyID')->setAttribute('disabled', 'true');
        }
        if ($displaysetup->customerCreditLimitApplyFlag) {
            $form->get('customerCreditLimitApplyFlag')->setValue(true);
        }

        if ($displaysetup->posPrintoutImageConversionStatus) {
            $form->get('posPrintoutImageConversionStatus')->setValue(true);
        }

//if dispaly setup details already in database then set dispayform submit button as update otherwise set as save
        if (!$displaysetup) {
            $form->get('submit')->setValue('Save');
        } else {
            $form->bind($displaysetup);
            $form->get('submit')->setValue('Update');
        }

        $tplListViews = $this->getTemplateListsView();

        // template detail placeholders list
        $config = $this->getServiceLocator()->get('config');
        $tplVars = $config["template-vars"];

        // get template document sizes
        $tplTable = $this->CommonTable('Settings\Model\TemplateTable');
        $docSizes = $tplTable->getTemplateDocumentSizes();

        $itemAttributes = [];
        $itemAttributeRes = $this->CommonTable('Settings\Model\itemAttributeValueTable')->selectAll();

        foreach ($itemAttributeRes as $value) {
            $itemAttributes[$value['itemAttributeID']] = $value['itemAttributeName'];
        }


        $signatures = [];
        $signatureDetails = $this->CommonTable('Settings\Model\SignatureDetailsTable')->fetchAll();

        foreach ($signatureDetails as $val) {
            $signatures[$val['signatureDetailsID']] = 'Signature OF '.$val['signatureHolderName'];
        }
        //create view model
        $viewmodel = new ViewModel(array(
            'form' => $form,
            'display' => $displaysetup,
            'tplListViews' => $tplListViews,
            'tplVars' => $tplVars,
            'signatures' => $signatures,
            'docSizes' => $docSizes,
            'FIFOCostingFlag' => $FIFOCostingFlag,
            'averageCostingFlag' => $averageCostingFlag,
            'hasItemIn' => $hasItemIn,
            'itemAttributes' => $itemAttributes,
            'packageID' => $this->packageID
        ));

        //append all css and script file for this phtml file
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/display.js');
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/company.css');
        $this->getViewHelper('HeadLink')->prependStylesheet('/assets/bootstrap/css/bootstrap-select.min.css');
        $this->getViewHelper('HeadScript')->prependFile('/assets/bootstrap/js/bootstrap-select.min.js');

        $this->setLogMessage('Display form accessed');

        return $viewmodel;
    }

    public function getTemplateListsView()
    {
        // get template lists
        $groupedTpls = $this->getTemplateLists();

        $tplListViews = array();

        // get all view partials to array
        foreach ($groupedTpls as $docId => $tpls) {
            if ($this->packageID == "1") {
                if ($docId == 1 || $docId == 6 || $docId == 7 || $docId == 8 || $docId == 10 || $docId == 12 || $docId == 14 || $docId == 16 || $docId == 20 || $docId == 21 || $docId == 24 || $docId == 25 || $docId == 13) {
                    $view = new ViewModel(array('docTypeName' => $tpls['documentTypeName'], 'tpls' => $tpls['sizes']));
                    $view->setTemplate('/settings/company/template-list');
                    $view->setTerminal(true);
                    
                    $tplListViews[$docId]['docName'] = $tpls['documentTypeName'];
                    $tplListViews[$docId]['html'] = $this->getServiceLocator()
                            ->get('viewrenderer')
                            ->render($view);    
                }
            } else if ($this->packageID == "2") {
                if ($docId == 1 || $docId == 2 || $docId == 4 || $docId == 5 || $docId == 6 || $docId == 7 || $docId == 8 || $docId == 10 || $docId == 11 || $docId == 12 || $docId == 13 || $docId == 14 || $docId == 15 || $docId == 16 || $docId == 20 || $docId == 21 || $docId == 24 || $docId == 25) {
                    $view = new ViewModel(array('docTypeName' => $tpls['documentTypeName'], 'tpls' => $tpls['sizes']));
                    $view->setTemplate('/settings/company/template-list');
                    $view->setTerminal(true);
                    
                    $tplListViews[$docId]['docName'] = $tpls['documentTypeName'];
                    $tplListViews[$docId]['html'] = $this->getServiceLocator()
                            ->get('viewrenderer')
                            ->render($view);    
                }
            } else {
                $view = new ViewModel(array('docTypeName' => $tpls['documentTypeName'], 'tpls' => $tpls['sizes']));
                $view->setTemplate('/settings/company/template-list');
                $view->setTerminal(true);
                
                $tplListViews[$docId]['docName'] = $tpls['documentTypeName'];
                $tplListViews[$docId]['html'] = $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
            }


        }

        return $tplListViews;
    }

    //get all template datails from this function
    public function getTemplateLists()
    {
        //get template details
        $tplTable = $this->CommonTable('Settings\Model\TemplateTable');
        $allTpls = $tplTable->fetchAll();

        $groupedTpls = array();

        // group by document type and template size
        foreach ($allTpls as $tpl) {
            $groupedTpls[$tpl['documentTypeID']]['documentTypeName'] = $tpl['documentTypeName'];
            $groupedTpls[$tpl['documentTypeID']]['sizes'][$tpl['documentSizeID']]['documentTypeName'] = $tpl['documentTypeName'];
            $groupedTpls[$tpl['documentTypeID']]['sizes'][$tpl['documentSizeID']]['templates'][] = $tpl;
        }

        return $groupedTpls;
    }

//setting page of all references create in here
    public function referenceAction()
    {
//set reference action side menu and upper menues
        $this->getSideAndUpperMenus('Add Reference', null, null);

        $locationData = [];
        $referencess = [];
        $locationReference = [];

//get all reference details
        $reference = $this->CommonTable('Settings\Model\ReferenceTable')->selectAll();
        foreach ($reference as $t) {
            if ($t[referenceNameID] == 1 || $t[referenceNameID] == 2 || $t[referenceNameID] == 3 || $t[referenceNameID] == 4 || $t[referenceNameID] == 5 || $t[referenceNameID] == 6 || $t[referenceNameID] == 7 || $t[referenceNameID] == 8 || $t[referenceNameID] == 9 || $t[referenceNameID] == 10 || $t[referenceNameID] == 11 || $t[referenceNameID] == 12 || $t[referenceNameID] == 13 || $t[referenceNameID] == 14 || $t[referenceNameID] == 15 || $t[referenceNameID] == 16 || $t[referenceNameID] == 17 || $t[referenceNameID] == 18 || $t[referenceNameID] == 19 || $t[referenceNameID] == 20 || $t[referenceNameID] == 21 || $t[referenceNameID] == 22 || $t[referenceNameID] == 23 || $t[referenceNameID] == 24 || $t[referenceNameID] == 25 || $t[referenceNameID] == 26 || $t[referenceNameID] == 27 || $t[referenceNameID] == 29 || $t[referenceNameID] == 30 || $t[referenceNameID] == 31 || $t[referenceNameID] == 33 || $t[referenceNameID] == 34 || $t[referenceNameID] == 39 || $t[referenceNameID] == 42) {
                $referencess[$t[referenceNameID]] = $t;
            }
        }
        $reference2 = $this->CommonTable('Settings\Model\ReferenceTable')->selectAllForView();
        foreach ($reference2 as $t) {
            if ($t[referenceNameID] == 1 || $t[referenceNameID] == 2 || $t[referenceNameID] == 3 || $t[referenceNameID] == 4 || $t[referenceNameID] == 5 || $t[referenceNameID] == 6 || $t[referenceNameID] == 7 || $t[referenceNameID] == 8 || $t[referenceNameID] == 9 || $t[referenceNameID] == 10 || $t[referenceNameID] == 11 || $t[referenceNameID] == 12 || $t[referenceNameID] == 13 || $t[referenceNameID] == 14 || $t[referenceNameID] == 15 || $t[referenceNameID] == 16 || $t[referenceNameID] == 17 || $t[referenceNameID] == 18 || $t[referenceNameID] == 19 || $t[referenceNameID] == 20 || $t[referenceNameID] == 21 || $t[referenceNameID] == 22 || $t[referenceNameID] == 23 || $t[referenceNameID] == 24 || $t[referenceNameID] == 25 || $t[referenceNameID] == 26 || $t[referenceNameID] == 27 || $t[referenceNameID] == 29 || $t[referenceNameID] == 30 || $t[referenceNameID] == 31 || $t[referenceNameID] == 33 || $t[referenceNameID] == 34 || $t[referenceNameID] == 39 || $t[referenceNameID] == 42) {
                $locationReference[$t[referenceNameID]] = $t;
            }
        }


        //get all active locations
        $locations = $this->CommonTable('Settings\Model\LocationTable')->activeFetchAll();
        foreach ($locations as $l) {
            $locationData[$l->locationID] = $l;
        }


//create view model for this action
        $viewmodel = new ViewModel(array(
            'reference' => $referencess,
            'locationReference' => $locationReference,
            'locations' => $locationData,
            'packageID' => $this->packageID,
            'jobModuleEnabled' => $this->jobModuleEnabled,
        ));

        //append all css and script file for this phtml file
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/reference.js');
        $this->getViewHelper('HeadScript')->prependFile('/assets/tinymce/tinymce.min.js');

        $this->setLogMessage('Display form accessed');

        return $viewmodel;
    }

//create page of custom Currency
    public function customCurrencyAction()
    {
//set custom currency action side menu and upper menues
        $this->getSideAndUpperMenus('Custom Currency', null, null);

        $customCurrencyList = $this->getPaginatedCurrency();
        $data = [];
        $value = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll(FALSE);
        $displaysetup = (object) $this->CommonTable('Core\model\DisplaySetupTable')->fetchAllDetails()->current();
//        $config = $this->getServiceLocator()->get('config');
        foreach ($value as $val) {
            $data[$val->currencyID] = $val->currencyName;
        }

        $form = new CustomCurrencyForm(array('currencyList' => $data));
        $form->get('customCurrencySymbol')->setAttribute('disabled', 'true');
        $view = new ViewModel(array(
            'form' => $form,
            'customCurrencyList' => $customCurrencyList,
            'paginated' => true,
            'companyCurrencyId' => $displaysetup->currencyID,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/custom-currency.js');

        return $view;
    }

    protected function getPaginatedCurrency($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Currency list accessed');

        return $this->paginator;
    }

    public function administraionOfficialAction()
    {
        $this->getSideAndUpperMenus('Organization Structure', null, null, 'Administraion Officials');

        $paginator = $this->getPaginatedOfficials();

        $userdateFormat = $this->getUserDateFormat();
        $employeeView = new ViewModel(array(
                                'paginated' => true,
                                'employees' => $paginator,
                                'userdateFormat' => $userdateFormat
                            ));
        
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $employeeView->addChild($attachmentsView, 'attachmentsView');
        
        $employeeView->setTemplate('settings/company/administraion-official');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/administraion-official.js');
        return $employeeView;
    }

    public function additionalDetailAction()
    {
        $this->getSideAndUpperMenus('Organization Structure', null, null, 'Additional Details');

        $paginator = $this->getPaginatedAdditionaldetails();
        $view = new ViewModel(array(
                            'adDetails' => $paginator,
                            'paginated' => true,
                        ));

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $view->addChild($attachmentsView, 'attachmentsView');

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/additional-detail.js');

        return $view;
    }

    public function getPaginatedOfficials($perPage = 6, $param = null)
    {
        $this->employee = $this->CommonTable('Settings/Model/AdministrationOfficialTable')->fetchAll(true);
        if ($param == null) {
            $this->employee->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->employee->setCurrentPageNumber($param);     
        }
        $this->employee->setItemCountPerPage($perPage);
        return $this->employee;
    }

    public function getPaginatedAdditionaldetails($perPage = 6, $param = null)
    {

        $this->dta = $this->CommonTable('Settings/Model/OrganizationAdditionalDetailTable')->fetchAll(true);
        if ($param == null) {
            $this->dta->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->dta->setCurrentPageNumber($param);     
        }
        $this->dta->setItemCountPerPage($perPage);
        return $this->dta;
    }
    
}
