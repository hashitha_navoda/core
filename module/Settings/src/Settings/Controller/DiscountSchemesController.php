<?php

namespace Settings\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class DiscountSchemesController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'settings_products_upper_menu';

    public function indexAction()
    {

        $this->getSideAndUpperMenus('Product Setup', 'Discount Schemes');
        $this->useAccounting = $this->user_session->useAccounting;
        
        // create array for gl accounts

        $ruleAdd = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol
        ));
        $ruleAdd->setTemplate('settings/discount-schemes/add-conditions');

        $schemeList = $this->getPaginatedCostTypes();
        $view = new ViewModel(array(
            'schemeList' => $schemeList,
            'paginated' => true,
            'useAccounting' => ($this->useAccounting == 1)? true :false,
            'companyCurrencySymbol' => $this->companyCurrencySymbol
            
        ));

        $view->addChild($ruleAdd, 'conditionRule');

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/discount-schemes.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return $view;
    }

    public function getPaginatedCostTypes($perPage = 6, $fromSearch = false)
    {
        $this->paginator = $this->CommonTable('Settings\Model\DiscountSchemeTable')->fetchAll(true);
        if($fromSearch) {
            $this->paginator->setCurrentPageNumber(1);
        } else {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        }
        $this->paginator->setItemCountPerPage($perPage);
        
        return $this->paginator;
    }

}
