<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * This file contains Units of Measure related controller functions
 */

namespace Settings\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Form\UomForm;

class UomController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'settings_products_upper_menu';

    public function indexAction()
    {

        $this->getSideAndUpperMenus('Product Setup', 'Unit of Measure Setup');

        $uomList = $this->getPaginatedUom();

        $form = new UomForm();

        $view = new ViewModel(array(
            'form' => $form,
            'uomList' => $uomList,
            'paginated' => true
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/uom.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return $view;
    }

    protected function getPaginatedUom($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Settings/Model/UomTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Unit of Measure list accessed');

        return $this->paginator;
    }

}
