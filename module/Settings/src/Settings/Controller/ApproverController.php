<?php

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class ApproverController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = null;
    protected $downMenus = 'approval_workflow_down_menu';
    protected $paginator;

    public function createAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', null, null, 'Approvers');
        $paginator = $this->getPaginatedApprovers();

        $userdateFormat = $this->getUserDateFormat();
        $employeeView = new ViewModel(array(
                                'paginated' => true,
                                'employees' => $paginator,
                                'userdateFormat' => $userdateFormat
                            ));
        
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/approver.js');
        return $employeeView;
    }

    public function getPaginatedApprovers($perPage = 6, $param = null)
    {
        $this->employee = $this->CommonTable('Settings/Model/ApproverTable')->fetchAll(true);
        if ($param == null) {
            $this->employee->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->employee->setCurrentPageNumber($param);     
        }
        $this->employee->setItemCountPerPage($perPage);
        return $this->employee;
    }

}
