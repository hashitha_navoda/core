<?php

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;


class PurchaseRequisitionCategoryController extends CoreController
{

    /**
     * Create Purchase Requisition Category
     * @return JsonModel
     */
    public function addAction()
    {

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $this->beginTransaction();
            $entityID = $this->createEntity();
            $expenseCategoryData = array(
                'purchaseRequisitionCategoryName' => $postData['expenseCategoryName'],
                'purchaseRequisitionCategoryParentId' => ($postData['expenseCategoryParent'] == 0) ? NULL : $postData['expenseCategoryParent'],
                'purchaseRequisitionCategoryStatus' => $this->getStatusID('active'),
                'entityId' => $entityID
            );
            $categoryData = new \Settings\Model\PurchaseRequisitionCategory();
            $categoryData->exchangeArray($expenseCategoryData);
            $savedCategoryID = $this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->create($categoryData);
            if ($savedCategoryID) {
                $this->commit();
                $this->msg = $this->getMessage('SUCC_PRCAT_SAVE');
                $this->status = TRUE;
                $listView = $this->getCategoryListView();
                $this->data = $this->htmlRender($listView);
            } else {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_PRCAT_SAVE');
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function updateAction()
    {
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $purchaseRequisitionCategoryId = $postData['expenseCategoryId'];
            $categoryData = array(
                'purchaseRequisitionCategoryName' => $postData['expenseCategoryName'],
                'purchaseRequisitionCategoryParentId' => ($postData['expenseCategoryParent'] == 0) ? NULL : $postData['expenseCategoryParent'],
            );
            $dataUpdated = $this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->update($categoryData, $purchaseRequisitionCategoryId);
            if ($dataUpdated) {
                $this->msg = $this->getMessage('SUCC_PRCAT_UPD');
                $this->status = TRUE;
                $listView = $this->getCategoryListView();
                $this->data = $this->htmlRender($listView);
            } else {
                $this->msg = $this->getMessage('ERR_PRCAT_UPD');
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function changeCategoryStatusAction()
    {
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $purchaseRequisitionCategoryId = $postData['expenseCategoryId'];
            $changeStatusTo = $this->getStatusID($postData['changeStatusTo']);
            if ($this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->changeStatus($changeStatusTo, $purchaseRequisitionCategoryId)) {
                $this->status = TRUE;
            } else {
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $purchaseRequisitionCategoryId = $request->getPost('expenseCategoryId');
            //check category used for expense types
            $pRTypes = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeTable')->getPRTypesByCategory($purchaseRequisitionCategoryId);
            if (count($pRTypes) > 0) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PRCAT_DEL');
            } else {
                $expenseCategoryData = $this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->getCategoryByID($purchaseRequisitionCategoryId);
                $entityID = $expenseCategoryData['entityId'];
                $this->updateDeleteInfoEntity($entityID);
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_PRCAT_DEL');
            }

            return $this->JSONRespond();
        }
    }

    private function getCategoryListView()
    {
        $paginator = $this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->fetchAll(TRUE);
        $paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $paginator->setItemCountPerPage(6);
        $expCategoryListView = new ViewModel(array(
            'categories' => $paginator,
            'paginated' => TRUE));
        $expCategoryListView->setTemplate('settings/purchase-requisition-category/list');
        $expCategoryListView->setTerminal(FALSE);
        return $expCategoryListView;
    }

}
