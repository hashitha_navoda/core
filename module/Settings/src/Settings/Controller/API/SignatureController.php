<?php
namespace Settings\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\SignatureDetails;
use Zend\View\Model\JsonModel;

class SignatureController extends CoreController
{

    public function saveSignatureAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();

            $post = [
                'signatureHolderName' => $request->getPost('signatureHolderName')
            ];

            $signatureDetails = new SignatureDetails();
            $signatureDetails->exchangeArray($post);


            $signatureDetailsID = $this->CommonTable('Settings\Model\SignatureDetailsTable')->saveSignature($signatureDetails);


            if ($signatureDetailsID) {
                $this->commit();
                $this->data = $this->CommonTable('Settings\Model\SignatureDetailsTable')->get($signatureDetailsID)->current();
                $this->msg = $this->getMessage('SUCC_SIG_CREATE');
                $this->status = true;
                return $this->JSONRespond();
            } else {
                $this->msg = $this->getMessage('ERR_SIG_CREATE');
                $this->status = false;
                $this->rollback();
                return $this->JSONRespond();
            }

        }
    }


    public function searchAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $searchSignatureString = $request->getPost('searchKey');
            $companyFolder = $this->getCompanyImageFolderName();


            if (!empty($searchSignatureString)) {
                $signatures = $this->CommonTable('Settings\Model\SignatureDetailsTable')->SignatureSearchByKey($searchSignatureString);
                $view = new ViewModel(array(
                    'signatures' => $signatures,
                    'paginator' => false,
                    'companyFolder' => $companyFolder
                ));

                $view->setTemplate("settings/signature/list.phtml");

                $this->html = $view;
                $this->status = true;
            } else {
                $this->getPaginatedSignatures();
                $view = new ViewModel(array(
                    'signatures' => $this->signatures,
                    'paginated' => true,
                    'companyFolder' => $companyFolder
                ));

                $view->setTemplate("settings/signature/list.phtml");
                $this->html = $view;
                $this->status = true;
            }
            return $this->JSONRespondHtml();
        }
    }

    //delete dimension by dimension id
    public function deleteSignatureAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $data = $request->getPost();
        $signatureId = $data['signatureId'];

        if (!empty($signatureId)) {
            $sigData = $this->CommonTable('Settings\Model\SignatureDetailsTable')->get($signatureId)->current();
            $result = $this->CommonTable('Settings\Model\SignatureDetailsTable')->deleteSignatureDetails($signatureId);
            if ($result) {

                $previewImg = $this->getCompanyDataFolder() . "/signature/thumbs/" . $sigData['signatureImageID']; //get existing preview image  name
                $imageFile = explode("_preview.", $sigData['signatureImageID']);
                $originalImage = $this->getCompanyDataFolder() . "/signature/" . $imageFile[0] . "." . $imageFile[1]; //get existing original image name
                //delete existing preview image
                if (file_exists($previewImg)) {
                    @unlink($previewImg);
                }
                //delete existing original image
                if (file_exists($originalImage)) {
                    @unlink($originalImage);
                }

                $this->msg = $this->getMessage('SUCC_SIG_DELETE');
                $this->status = true;
                $this->flashMessenger()->addMessage($this->msg);
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_SIG_DELETE');
                $this->rollback();

            }
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_SIG_DELETE');
            $this->rollback();
        }

        return $this->JSONRespond();
    }

    public function previewAction()
    {
        $result = $this->CommonTable('Settings\Model\SignatureDetailsTable')->get($_POST['ID'])->current();


        //remove any image file that already exists with the same name
        $previewImg = $this->getCompanyDataFolder() . "/signature/thumbs/" . $result['signatureImageID']; //get existing preview image  name
        $imageFile = explode("_preview.", $result['signatureImageID']);
        $originalImage = $this->getCompanyDataFolder() . "/signature/" . $imageFile[0] . "." . $imageFile[1]; //get existing original image name

        // //delete existing preview image
        // if (file_exists($previewImg)) {
        //     @unlink($previewImg);
        //     $this->CommonTable('Settings\Model\SignatureDetailsTable')->deleteSignatureDetails($_POST['ID']);
        // }
        // //delete existing original image
        // if (file_exists($originalImage)) {
        //     @unlink($originalImage);
        // }

        $imgname = $this->previewImage("/signature", $_POST['ID']);
        $data = array(
            'signatureImageID' => $imgname,
        );
        $updatesignatureImageID = $this->CommonTable('Settings\Model\SignatureDetailsTable')->updateSignature($data, $_POST['ID']);
        // $imgUrl = array(
        //     'productImageURL' => $imgname,
        // );
        // $this->CommonTable('Inventory\Model\ProductTable')->updateImageUrl($imgUrl, $_POST['ID']);


        return new JsonModel(array($imgname));
    }

    public function getPaginatedSignatures($perPage = 6, $param = null)
    {

        $this->signatures = $this->CommonTable('Settings/Model/SignatureDetailsTable')->fetchAll(true);
        if ($param == null) {
            $this->signatures->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        } else {
            $this->signatures->setCurrentPageNumber($param);     
        }
        $this->signatures->setItemCountPerPage($perPage);
        return $this->signatures;
    }
}