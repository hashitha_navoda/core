<?php

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class ApprovalWorkflowSettingsController extends CoreController
{

    public function getApprovalWorkflowDocumentsAction() {

        $this->beginTransaction();
        $documents = $this->CommonTable('Settings/Model/ApprovalDocumentDetailsTable')->fetchAll();

        $showingDocumentList = [];

        foreach ($documents as $key => $doc) {
            $showingDocumentList[$doc['documentTypeID']] = [
                "docID" => $doc['documentTypeID'],
                "documentName" => $doc['documentTypeName'],
                "isActive" => ($doc['isActive'] == "1") ? 1:0,
                "icon" => ($doc['isActive'] == "1") ? 'fa-check-square-o': 'fa-square-o' 
            ];
        }


        $this->commit();
        $this->status = true;
        $this->data = $showingDocumentList;
        $this->msg = $this->getMessage("SUCC_GET_WF_DOC_DETAILS");
        return $this->JSONRespond();
    }


    public function updateWFDocAction() {

       $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $data = $request->getPost();

        foreach ($data['workFlowDocument'] as $key => $value) {
            $dataSet = [
                'isActive' => $value['isActive']
            ];
            $updateWFDoc = $this->CommonTable('Settings/Model/ApprovalDocumentDetailsTable')->updateWFDoc($dataSet, $value['docID']);

            if (!$updateWFDoc) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_UPDATE_WF_DOC_DETAILS');
                $this->rollback();
                return $this->JSONRespond();
            }
        }

        $this->commit();
        $this->status = true;
        $this->data = [];
        $this->msg = $this->getMessage("SUCC_UPDATE_WF_DOC_DETAILS");
        return $this->JSONRespond();
    }

}
