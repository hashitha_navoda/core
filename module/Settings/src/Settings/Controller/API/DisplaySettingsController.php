<?php
namespace Settings\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeValue;
use Settings\Form\ItemAttributeForm;
use Zend\View\Model\JsonModel;

class DisplaySettingsController extends CoreController
{

    public function updateAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }

        $displayData = $req->getPost('displayData');
        $dData = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        $id = $dData['displaySetupID'];



        if ($displayData['isUseSalesInvoiceReturn'] == 0 && $dData['isUseSalesInvoiceReturn'] == '1') {
            $invoiceReturns = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->getOpenInvoiceReturnByInvoiceReturnID();

            if (sizeof($invoiceReturns) > 0) {
                $this->status = false;
                $this->msg = $this->getMessage('HAS_OPEN_INV_RETURNS');
                return $this->JSONRespond();
            }
        }

        $_SESSION['ezBizUser']->displaySettings->isUseSalesInvoiceReturn = $displayData['isUseSalesInvoiceReturn'];
    
        $respond = $this->CommonTable('Settings\Model\DisplaySetupTable')->updateDisplaySetupDetails($displayData, $id);
       
        $this->status = true;
        $this->msg = $this->getMessage('SUCCESS_UPDATE_DISPLAY_SETTINGS');
        return $this->JSONRespond();
        
    }
 
}
