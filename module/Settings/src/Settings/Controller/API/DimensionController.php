<?php
namespace Settings\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeValue;
use Settings\Form\ItemAttributeForm;
use Zend\View\Model\JsonModel;

class DimensionController extends CoreController
{

    public function saveDimensionAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('DimensionService')->createDimension($request->getPost());
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }


    public function searchAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $result = $this->getService('DimensionService')->dimensionSearchByKey($request->getPost(), (int) $this->params()->fromRoute('param1', 1));

        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();
    }

    public function getRelatedDimensionValuesAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('DimensionService')->getRelatedDimensionValues($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    public function updateDimensionAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('DimensionService')->updateDimension($request->getPost(), $this->userID);

        if (!$respond['status']) {

            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        } else {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
        }

        return $this->JSONRespond();

    }

    //delete dimension by dimension id
    public function deleteDimensionAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('DimensionService')->deleteDimension($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }
}