<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * Contains API methods related to Document Template management in Company Setup area
 */

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\Template;
use Settings\Model\TemplateItemAttributeMap;
use Settings\Model\TemplateSignature;

class TemplateController extends CoreController
{

    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
    }

    public function getAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $templateID = $request->getPost('templateID');
            $res = $this->CommonTable('Settings\Model\TemplateTable')->get($templateID);
            $itemAttr = $this->CommonTable('Settings\Model\TemplateItemAttributeMapTable')->getTemplateItemAttributeByTemplateID($templateID);
            $templateSignatures = $this->CommonTable('Settings\Model\TemplateSignatureTable')->getTemplateSignatureByTemplateID($templateID);

            if ($res) {
                $templateData = $res->current();
                $templateData['templateContent'] = preg_replace('/^(\<div\>\<\/div\>)/', '', $templateData['templateContent']);
                $templateData['templateOptions'] = json_decode($templateData['templateOptions']) ? : [];
                $templateData['templateAdvancedOptions'] = (!empty($templateData['templateAdvancedOptions'])) ? json_decode($templateData['templateAdvancedOptions']) : [];
                $templateData['templateItemAtrribute'] = (!$itemAttr) ? [] : $itemAttr; 

                // TODO - this should be taken from the relevant controller matching to the document type
                // TODO - remove this and take from config / controller static var
                if (!isset($templateData['templateOptions']->product_table)) {

                    if (!is_object($templateData['templateOptions'])) {
                        $templateData['templateOptions'] = new \stdClass;
                    }

                    if ($templateData['documentTypeID'] == "1") {
                        $defaultTemplateOptions = $this->getServiceLocator()->get('config')['template-data-table']['invoice'];
                    } else if ($templateData['documentTypeID'] == "2") {
                        $defaultTemplateOptions = $this->getServiceLocator()->get('config')['template-data-table']['quotation'];
                    } else if ($templateData['documentTypeID'] == "3") {
                        $defaultTemplateOptions = $this->getServiceLocator()->get('config')['template-data-table']['sales_order'];
                    } else if ($templateData['documentTypeID'] == "4") {
                        $defaultTemplateOptions = $this->getServiceLocator()->get('config')['template-data-table']['delivery_note'];
                    } else {
                        $defaultTemplateOptions = $this->getServiceLocator()->get('config')['template-data-table']['invoice'];
                    }

                    $templateData['templateOptions']->product_table = $defaultTemplateOptions;
                }

                $itemAttributes = [];
                $itemAttributeRes = $this->CommonTable('Settings\Model\itemAttributeValueTable')->selectAll();

                foreach ($itemAttributeRes as $value) {
                    $itemAttributes[$value['itemAttributeID']] = $value['itemAttributeName'];
                }

                $selectedSignatures = [];
                if ($templateData['isEnableSignatureVerify'] == 1) {
                    foreach ($templateSignatures as $key => $value) {
                        $selectedSignatures[] = $value['signatureID'];
                    }
                }




                $templateData['itemAttributes'] = $itemAttributes;
                $templateData['selectedSignatures'] = $selectedSignatures;

                $this->data = $templateData;
                $this->status = true;
            } else {
                $this->msg = $this->getMessage('ERR_TEMP_RETRIEVE');
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            // Todo - check if this is the only template in it's page size

            $templateID = $request->getPost('templateID');
            $res = $this->CommonTable('Settings\Model\TemplateTable')->deleteTemplate($templateID);

            if ($res) {
                $this->msg = $this->getMessage('SUCC_TEMP_DELE');
                $this->status = true;
            } else {
                $this->msg = $this->getMessage('ERR_TEMP_DELE');
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

    public function changeDefaultAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $templateID = $request->getPost('templateID');
            $this->CommonTable('Settings\Model\TemplateTable')->updateDefaultTemplate($templateID);

            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function createAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $post = [
                'templateName' => $request->getPost('templateName'),
                'documentTypeID' => $request->getPost('documentTypeID'),
                'documentSizeID' => $request->getPost('documentSizeID')
            ];


            $sampleContent = $this->CommonTable('Settings\Model\TemplateTable')->getSampleContent($post['documentTypeID'], $post['documentSizeID']);
            $post['templateContent'] = $sampleContent['templateContent'];

            $template = new Template();
            $template->exchangeArray($post);

            $templateID = $this->CommonTable('Settings\Model\TemplateTable')->addTemplate($template);

            if ($templateID) {
                $this->data = $this->CommonTable('Settings\Model\TemplateTable')->get($templateID)->current();
                $this->msg = $this->getMessage('SUCC_TEMP_CREATE');
                $this->status = true;
            } else {
                $this->msg = $this->getMessage('ERR_TEMP_UPDATE');
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

    public function updateAction()
    {
        try {

            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError($this->getMessage('ERR_INVALID_REQUEST'));
            }

            $postData = $request->getPost()->toArray();
            $postData['isAdvanceTemplateEnabled'] = (int)filter_var($postData['isAdvanceTemplateEnabled'], FILTER_VALIDATE_BOOLEAN);

            $advanceOptions = $this->validateAdvanceOptions($postData['templateAdvancedOptions']);
            $postData['templateOptions'] = json_encode($postData['templateOptions']);
            $postData['templateAdvancedOptions'] = json_encode($advanceOptions);
            $attributeData = $postData['attributeData'];

            $content = explode('<noneditabletable>', $postData['templateContent']);
            $templateHeader = ($content[0]) ? $content[0] : null;

            $content = explode('</noneditabletable>', $postData['templateContent']);
            $templateFooter = ($content[1]) ? $content[1] : null;

            preg_match("/(<noneditabletable>)(.*)(<\/noneditabletable>)/s", $postData['templateContent'], $matches);
            $templateBody = ($matches[0]) ? $matches[0] : "<noneditabletable><\noneditabletable>";

            if ($advanceOptions['repeatHeader'] && $advanceOptions['repeatFooter']) {
                $postData['templateHeader'] = $templateHeader;
                $postData['templateBody'] = $templateBody;
                $postData['templateFooter'] = $templateFooter;
            } else if ($advanceOptions['repeatHeader'] && !$advanceOptions['repeatFooter']) {
                $postData['templateHeader'] = $templateHeader;
                $postData['templateBody'] = $templateBody . '' . $templateFooter;
                $postData['templateFooter'] = NULL;
            } else if (!$advanceOptions['repeatHeader'] && $advanceOptions['repeatFooter']) {
                $postData['templateHeader'] = NULL;
                $postData['templateBody'] = $templateHeader . '' . $templateBody;
                $postData['templateFooter'] = $templateFooter;
            } else {
                $postData['templateHeader'] = NULL;
                $postData['templateBody'] = $templateHeader . '' . $templateBody . '' . $templateFooter;
                $postData['templateFooter'] = NULL;
            }

            if (isset($postData['tplUpdateFlag'])) {
                $postData['tplUpdateFlag'] = (int)filter_var($postData['tplUpdateFlag'], FILTER_VALIDATE_BOOLEAN);
            } else {
                $postData['tplUpdateFlag'] = 0;
            }
           
            unset($postData['attributeData']);
            $selectedSignatures = $postData['selectedSignatures'];
            unset($postData['selectedSignatures']);

            $result = $this->CommonTable('Settings\Model\TemplateTable')->updateTemplate($postData, $postData['templateID']);

            if (!$result) {
                return $this->returnJsonError($this->getMessage('ERR_TEMP_UPDATE'));
            }

            //delete related all template signatures by template id
            $saveTemplateSignature = $this->CommonTable('Settings\Model\TemplateSignatureTable')->deleteTemplateSignature($postData['templateID']);

            if ($postData['isEnableSignatureVerify'] == 1 && sizeof($selectedSignatures) > 0) {
                foreach ($selectedSignatures as $key => $value) {
                    $dataSet = [
                        "templateID" => $postData['templateID'],
                        "signatureID" => $value
                    ];

                    $templateSignatureData = new TemplateSignature();
                    $templateSignatureData->exchangeArray($dataSet);
                    $saveTemplateSignature = $this->CommonTable('Settings\Model\TemplateSignatureTable')->saveTempalteSignature($templateSignatureData);

                    if (!$saveTemplateSignature) {
                        return $this->returnJsonError($this->getMessage('ERR_TEMP_UPDATE'));
                    }

                }
            }

            $attribute_01 = NULL;
            $attribute_02 = NULL;
            $attribute_03 = NULL;
            if(!empty($attributeData)) {
                foreach ($attributeData as $key => $value) {
                    if ($key == "attribute_01") {
                        $attribute_01 = $value;
                    }
                    if ($key == "attribute_02") {
                        $attribute_02 = $value;
                    }
                    if ($key == "attribute_03") {
                        $attribute_03 = $value;
                    }
                }

                $itemAttrData = [
                    'templateID' => $postData['templateID'],
                    'attributeOneID' => $attribute_01,
                    'attributeTwoID' => $attribute_02,
                    'attributeThreeID' => $attribute_03
                ];

                $itemAttributeData = new TemplateItemAttributeMap();
                $itemAttributeData->exchangeArray($itemAttrData);

                $checkTemplateItemAttribute = $this->CommonTable('Settings\Model\TemplateItemAttributeMapTable')->getTemplateItemAttributeByTemplateID($postData['templateID']);
                if ($checkTemplateItemAttribute) {
                    $deleteTemplateAttr = $this->CommonTable('Settings\Model\TemplateItemAttributeMapTable')->deleteMapDetailsByTempalteID($postData['templateID']);
                    if ($deleteTemplateAttr) {
                        $saveRes = $this->CommonTable('Settings\Model\TemplateItemAttributeMapTable')->saveItemAttributeMap($itemAttributeData);
                        if (!$saveRes) {
                            return $this->returnJsonError($this->getMessage('ERR_TEMP_UPDATE'));
                        }
                    } else {
                        return $this->returnJsonError($this->getMessage('ERR_TEMP_UPDATE'));
                    }
                } else {
                    $saveRes = $this->CommonTable('Settings\Model\TemplateItemAttributeMapTable')->saveItemAttributeMap($itemAttributeData);
                    if (!$saveRes) {
                        return $this->returnJsonError($this->getMessage('ERR_TEMP_UPDATE'));
                    }
                }
            }

            return $this->returnJsonSuccess(['list' => $this->getServiceLocator()->get('Company')->getTemplateListsView()], $this->getMessage('SUCC_TEMP_UPDATE'));

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError($this->getMessage('ERR_TEMP_UPDATE'));
        }
    }

    /**
     * Validate advanced template attributes
     */
    private function validateAdvanceOptions($options)
    {
        $advanceOptions = [];

        $rules = [
            'isCustomPage' => FILTER_VALIDATE_BOOLEAN,
            'repeatHeader' => FILTER_VALIDATE_BOOLEAN,
            'repeatFooter' => FILTER_VALIDATE_BOOLEAN,
            'pageHeight' => FILTER_VALIDATE_INT,
            'pageWidth' => FILTER_VALIDATE_INT,
            'marginTop' => FILTER_VALIDATE_INT,
            'marginBottom' => FILTER_VALIDATE_INT,
            'marginLeft' => FILTER_VALIDATE_INT,
            'marginRight' => FILTER_VALIDATE_INT,
            'headerSpacing' => FILTER_VALIDATE_INT,
            'footerSpacing' => FILTER_VALIDATE_INT,
            'zoomFactor' => FILTER_VALIDATE_FLOAT,
            'isPortrait' => FILTER_VALIDATE_BOOLEAN
        ];

        foreach ($options as $key => $value) {

            if (in_array($key, array_keys($rules))) {
                $value = (empty($value) && $rules[$key] == FILTER_VALIDATE_INT) ? 0 : $value;
                $advanceOptions[$key] = filter_var($value, $rules[$key]);
            } else {
                $advanceOptions[$key] = $value;
            }

        }

        return $advanceOptions;
    }

    public function getPosTemplateDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $templateDetailsList = array();
            $templateDetails = $this->CommonTable('Settings\Model\PosTemplateDetailsTable')->fetchAll();
            
            $companyTbl = $this->CommonTable('CompanyTable')->fetchAll();
            $company = $companyTbl->current();

            $temp = array();
            foreach ($templateDetails as $data) {
                $temp['posTemplateDetailsID'] = $data['posTemplateDetailsID'];
                $temp['posTemplateDetailsAttribute']  = $data['posTemplateDetailsAttribute'];
                $temp['posTemplateDetailsIsSelected'] = $data['posTemplateDetailsIsSelected'];
                $temp['posTemplateDetailsByDefaultSelected']  = $data['posTemplateDetailsByDefaultSelected'];
                $templateDetailsList[] = $temp;
            }
            
            $additionalTextForPos = $company->additionalTextForPos;

            $this->status = TRUE;
            $this->data = array('list' => $templateDetailsList, 'additionalTextForPos' => $additionalTextForPos);
            return $this->JSONRespond();
        }
    }

    public function updateTemplateDetailsAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('TemplateService')->updateTemplateDetails($request->getPost());
        
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

}
