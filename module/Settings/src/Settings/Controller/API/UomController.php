<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * This file contains Units of Measure related controller functions
 */

namespace Settings\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\Uom;
use Settings\Form\UomForm;
use Zend\View\Model\JsonModel;

class UomController extends CoreController
{

    public function addAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $post = [
                'uomName' => $req->getPost('uomName'),
                'uomAbbr' => $req->getPost('uomAbbr'),
                'uomDecimalPlace' => $req->getPost('uomDecimalPlace'),
                'uomState' => $req->getPost('uomState')
            ];
            $wizardLength = $req->getPost('wizardLength');

            $uom = new Uom();
            $form = new UomForm();

            $form->setInputFilter($uom->getInputFilter());
            $form->setData($post);

            $recordFromSameName = $this->CommonTable('Settings/Model/UomTable')->getUomByName($post['uomName']);

            if ($recordFromSameName == null) {
                if ($form->isValid()) {
                    $uom->exchangeArray($post);
                    $this->CommonTable('Settings/Model/UomTable')->saveUom($uom);
                    // get uom list to return to view

                    if ($wizardLength == 1) {
                        $uomList = $this->CommonTable('Settings/Model/UomTable')->fetchAll();
                        $htmlOutput = $this->_getListViewHtml($uomList, false);
                    } else {
                        $uomList = $this->getPaginatedUom();
                        $htmlOutput = $this->_getListViewHtml($uomList, true);
                    }
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_UOM_ADD');
                    $this->html = $htmlOutput;
                    return $this->JSONRespondHtml();
                } else {
                    // validation error
                    $this->status = false;
                    $this->msg = _($this->getMessage('REQ_UOM_REQUIRE'));
                    return $this->JSONRespond();
                }
            } else {
                // record already exists
                $this->status = false;
                $this->msg = _($this->getMessage('ERR_UOM_EXIST'));
                return $this->JSONRespond();
            }
        }
    }

    public function updateAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $checkUom = $this->CommonTable('Settings/Model/UomTable')->checkUomForInactive($req->getPost('uomID'));
            $uomState = $req->getPost('uomState');
            if ($uomState == 0 && $checkUom) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_UOM_NOUPDATE');
                return $this->JSONRespond();
            } else {
                $post = [
                    'uomName' => $req->getPost('uomName'),
                    'uomAbbr' => $req->getPost('uomAbbr'),
                    'uomDecimalPlace' => $req->getPost('uomDecimalPlace'),
                    'uomState' => $uomState,
                    'uomID' => $req->getPost('uomID')
                ];

                $uom = new Uom();
                $form = new UomForm();

                $form->setInputFilter($uom->getInputFilter());
                $form->setData($post);

                if ($form->isValid()) {
                    $uom->exchangeArray($post);
                    $this->CommonTable('Settings/Model/UomTable')->updateUom($uom);

                    // get uom list to return to view
                    $uomList = $this->getPaginatedUom();
                    $htmlOutput = $this->_getListViewHtml($uomList, true);

                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_UOM_UPDATE');
                    $this->html = $htmlOutput;
                    return $this->JSONRespondHtml();
                } else {
                    // validation error
                    $this->status = false;
                    $this->msg = _($this->getMessage('REQ_UOM_REQUIRE'));
                    return $this->JSONRespond();
                }
            }
        }
    }

    public function changeStateAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $post = [
                'uomState' => (int) $req->getPost('uomState'),
                'uomID' => $req->getPost('uomID')
            ];
            $checkUom = $this->CommonTable('Settings/Model/UomTable')->checkUomForInactive($req->getPost('uomID'));
            if ($checkUom) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_UOM_NOINACTIVE');
                return $this->JSONRespond();
            } else {
                $uom = new Uom();
                $uom->exchangeArray($post);
                $this->CommonTable('Settings/Model/UomTable')->updateUomState($uom);

                // get uom list to return to view
                $uomList = $this->getPaginatedUom();
                $htmlOutput = $this->_getListViewHtml($uomList, true);

                $verb = ($post['uomState']) ? 'activated' : 'deactivated';
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_UOM_STATUS', array($verb));
                $this->html = $htmlOutput;
                return $this->JSONRespondHtml();
            }
        }
    }

    public function deleteAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $uomID = $req->getPost('uomID');
            $checkUom = $this->CommonTable('Settings/Model/UomTable')->checkUomForDelete($uomID);
            if ($checkUom) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_UOM_DEL');
                return $this->JSONRespond();
            } else {
                $this->CommonTable('Settings/Model/UomTable')->deleteUom($uomID);

                // get uom list to return to view
                $uomList = $this->getPaginatedUom();
                $htmlOutput = $this->_getListViewHtml($uomList, true);

                $this->status = true;
                $this->msg = $this->getMessage('SUCC_UOM_DEL');
                $this->html = $htmlOutput;
                return $this->JSONRespondHtml();
            }
        }
    }

    public function searchAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $keyword = $req->getPost('keyword');

            if (!empty($keyword)) {
                $tbl = $this->CommonTable('Settings/Model/UomTable');
                $uomList = $tbl->getUomforSearch($keyword);
                $paginated = false;
            } else {
                $uomList = $this->getPaginatedUom();
                $paginated = true;
            }

            $htmlOutput = $this->_getListViewHtml($uomList, $paginated);

            $this->status = true;
            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        }
    }

    private function _getListViewHtml($uomList = array(), $paginated = false)
    {
        $view = new ViewModel(array(
            'uomList' => $uomList,
            'paginated' => $paginated
        ));

        $view->setTerminal(true);
        $view->setTemplate('settings/uom/list.phtml');

        return $view;
    }

    public function getAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $uomID = $req->getPost('uomID');

            $tbl = $this->CommonTable('Settings/Model/UomTable');
            $uomData = $tbl->getUom($uomID);

            $this->status = true;
            $this->data = $uomData;
            return $this->JSONRespond();
        }
    }

    protected function getPaginatedUom($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Settings/Model/UomTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Unit of Measure list accessed');

        return $this->paginator;
    }

    /**
     * This function is used to search uom for dropdown
     */
    public function searchUomForDropdownAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $searchKey = $request->getPost('searchKey');
        $uoms = $this->CommonTable('Settings/Model/UomTable')->getUomforSearch($searchKey);
        $uomList = array();
        foreach ($uoms as $val) {
            $temp['value'] = $val->uomID;
            $temp['text'] = $val->uomName . '(' . $val->uomAbbr.')';
            $uomList[] = $temp;
        }
        $this->data = array('list' => $uomList);
        return $this->JSONRespond();
    }

}
