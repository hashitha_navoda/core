<?php
namespace Settings\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeValue;
use Settings\Form\ItemAttributeForm;
use Zend\View\Model\JsonModel;

class LandingCostTypeController extends CoreController
{

    public function saveAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('LandingCostTypeService')->saveLandingCostType($req->getPost());
        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
        
    }

    public function deleteCostTypeAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('LandingCostTypeService')->deleteLandingCostType($req->getPost());
        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    public function searchAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $respond = $this->getService('LandingCostTypeService')->searchLandingCostType($req->getPost());

        if (!$respond['status']) {
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }

        $view = new ViewModel(array(
            'costTypeList' => $respond['costTypeList'],
            'paginated' => $respond['paginated']
        ));
        
        $view->setTerminal(true);
        $this->status = true;
        $view->setTemplate('settings/landing-cost-type/list');
        $this->html = $view;
        return $this->JSONRespondHtml();
        
    }

    
}
