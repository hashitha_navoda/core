<?php
namespace Settings\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeValue;
use Settings\Form\ItemAttributeForm;
use Zend\View\Model\JsonModel;

class ItemAttributeController extends CoreController
{

    public function saveAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $status = false;
            return $this->JSONRespond();
        }

        $rqDataSet = $req->getPost();
        $codeExits = $this->CommonTable('Settings\Model\ItemAttributeTable')
            ->getDetailsByItemAttributeCode($rqDataSet['attrCode'])->current();
        if(!empty($codeExits)){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ITEM_ATTR_CODE_EXITS');
            return $this->JSONRespond();
        }
        
        $entityID = $this->createEntity();
        $itemAttrData = array(
            'itemAttributeCode' => $rqDataSet['attrCode'],
            'itemAttributeName' => $rqDataSet['attrName'],
            'itemAttributeState' => $rqDataSet['attrState'], 
            'entityID' => $entityID,
            );

        $itemAttrDetails = new ItemAttribute();
        $itemAttrDetails->exchangeArray($itemAttrData);
        $savedID =$this->CommonTable('Settings\Model\ItemAttributeTable')->saveItemAttribute($itemAttrDetails);
        if($savedID == null){
           $this->status = false;
           return $this->JSONRespond();
        }
        if(sizeof($rqDataSet['attrValue']) > 0 ){
            foreach ($rqDataSet['attrValue'] as $key => $value) {
                if($key == 0){
                    continue;
                }
                $itemAttrValEntityID = $this->createEntity();
                $itemAttrValueData = array(
                    'itemAttributeID' => $savedID,
                    'itemAttributeValueDescription' => $value['description'],
                    'itemAttributeValueUomID' => $value['uomID'],
                    'entityID' => $itemAttrValEntityID,
                    );

                $itemAttrValueDetails = new ItemAttributeValue();
                $itemAttrValueDetails->exchangeArray($itemAttrValueData);
                $saveItemAttrValue = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->saveItemAttrValues($itemAttrValueDetails);

                if($saveItemAttrValue == null ){
                    $this->status = false;
                    return $this->JSONRespond();
                }
            }
        }
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_SAVE_ITEM_ATTR');
        return $this->JSONRespond();
    }

    public function viewSubItemAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $status = false;
            return $this->JSONRespond();
        }
        $rqDataSet = $req->getPost();
        $subDataSet = [];
        $dataSet = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeID($rqDataSet['id']);
        foreach ($dataSet as $key => $value) {
            $isSet = $this->CommonTable('Settings\Model\ItemAttributeValueMapTable')->getDetaisByItemAttrValueID($value['itemAttributeValueID'])->current();
            if(!empty($isSet)){
                $flag = 'cannotDel';
            } else {
                $flag ='canDel';
            }
            $subDataSet[$key] = array(
                'itemAttributeValueID' => $value['itemAttributeValueID'],
                'itemAttributeValueDescription' => $value['itemAttributeValueDescription'],
                'uomName' => (!empty($value['uomName'])) ? $value['uomName'] : 'None',
                'uomID' => $value['itemAttributeValueUomID'],
                'delFlag' => $flag, 
                );
        }
        
        if(sizeof($subDataSet) > 0){
            $this->status = true;
            $this->data = $subDataSet;
            return $this->JSONRespond();     
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_NO_ITEM_ATTR_VALUE');
            return $this->JSONRespond();
        }    
    }

    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $itemAttrID = $request->getPost('itemAttrID');
            $status = $request->getPost('status');
            $updateState = $this->CommonTable('Settings\Model\ItemAttributeTable')->updateItemAttrState($itemAttrID, $status);
            if ($updateState) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_ITEMATTR_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

    public function loadDataToUpdateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $itemAttrID = $request->getPost('itemAttrID');
            $dataSet = $this->CommonTable('Settings\Model\ItemAttributeTable')->getDataByItemAttrID($itemAttrID)->current();
            
            if ($dataSet) {
                $this->status = true;
                $this->data = $dataSet;
                return $this->JSONRespond();
            } else {
                $this->status = false;
                return $this->JSONRespond();
            }
        }
    }

    public function updateAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $status = false;
            return $this->JSONRespond();
        }

        $rqDataSet = $req->getPost();
        $itemAttrData = array(
            'itemAttributeCode' => $rqDataSet['attrCode'],
            'itemAttributeName' => $rqDataSet['attrName'],
            'itemAttributeState' => $rqDataSet['attrState'], 
            'itemAttributeID' => $rqDataSet['updatedAttrID'],
        );

        $updated = $this->CommonTable('Settings\Model\ItemAttributeTable')->updateItemAttribute($itemAttrData);
        if(!$updated){
           $this->status = false;
           return $this->JSONRespond();
        }
        if(sizeof($rqDataSet['attrValue']) > 0 ){
            $this->deleteExistingItemAttrValues($rqDataSet['updatedAttrID']);
            foreach ($rqDataSet['attrValue'] as $key => $value) {
                if($key == 0){
                    continue;
                }
                $itemAttrValEntityID = $this->createEntity();
                $itemAttrValueData = array(
                    'itemAttributeID' => $rqDataSet['updatedAttrID'],
                    'itemAttributeValueDescription' => $value['description'],
                    'itemAttributeValueUomID' => $value['uomID'],
                    'entityID' => $itemAttrValEntityID,
                    );

                $itemAttrValueDetails = new ItemAttributeValue();
                $itemAttrValueDetails->exchangeArray($itemAttrValueData);
                $saveItemAttrValue = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->saveItemAttrValues($itemAttrValueDetails);

                if($saveItemAttrValue == null ){
                    $this->status = false;
                    return $this->JSONRespond();
                }
            }
        }
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_UPDATE_ITEM_ATTR');
        return $this->JSONRespond();
    }

    public function deleteItemAttributeByItemAttributeIdAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        
        $itemAttrID = $request->getPost('ItemAttrID');
        $productDetails = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->getDetailsByItemAttrID($itemAttrID)->current();    
        
        if(!empty($productDetails)){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DELETE_ITEM_ATTR');
            return $this->JSONRespond();
        } else {
            $itemAttrDetails = $this->CommonTable('Settings\Model\ItemAttributeTable')->getDataByItemAttrID($itemAttrID)->current();
            $this->updateDeleteInfoEntity($itemAttrDetails['entityID']);
            $this->status = true;
            $msg = $this->getMessage('SUCC_ITEM_ATTR_DELE');
            $this->flashMessenger()->addMessage($msg);
            return $this->JSONRespond();
        }
    }

    public function deleteExistingItemAttrValues($attrID)
    {
        $valueSet = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeID($attrID);
        foreach ($valueSet as $key => $value) {
            $updateEntityStatus = $this->CommonTable('Core\Model\EntityTable')->softDeleteEntity($value['entityID']);
        }
        return $updateEntityStatus;
    }

    public function getSearchValueAction()
    {
        $request = $this->getRequest();
      
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        $searchKey = $request->getPost('searchKey');
        $paginated = false;
        if (!empty($searchKey)) {
            $itemList = $this->CommonTable('Settings/Model/ItemAttributeTable')->itemAttributeSearchByKey($searchKey);
            $this->status = true;
        } else {
            $this->status = true;
            $itemList = $this->getPaginatedAttribute();
            $paginated = true;
        }
        $view = new ViewModel(array(
            'itemAttrList' => $itemList,
            'paginated' => $paginated
        ));
        $view->setTerminal(true);
        $view->setTemplate('settings/item-attribute/list');
        $this->html = $view;
        return $this->JSONRespondHtml();
    }


    protected function getPaginatedAttribute($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Settings/Model/ItemAttributeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        return $this->paginator;
    }

    public function getItemAttributesByProductIdAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $status = false;
            return $this->JSONRespond();
        }
        $rqDataSet = $req->getPost();
        $proID = $rqDataSet['id'];
        $attrDetailsSet = [];
        $itemAttributeDetails = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->getDetailsByProductID($proID);
        foreach ($itemAttributeDetails as $key => $value) {
            $attrDetailsSet[$value['itemAttributeID']][] = $value['itemAttributeValueID'];
        }
        if(count($attrDetailsSet) > 0){
            $this->status = true;
            $this->data = $attrDetailsSet;
            return $this->JSONRespond();
        } else {
            $this->status = false;
            return $this->JSONRespond();
        }
    }

    public function getItemAttributeRelatedValuesAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $status = false;
            return $this->JSONRespond();
        }
        $rqDataSet = $req->getPost();
        $attrID = $rqDataSet['attributeID'];
        
        $itemAttributeValues = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getAttributeValuesByAttributeId($attrID);

        $attrValues = [];
        foreach ($itemAttributeValues as $key => $value) {
            $attrValues[$value['itemAttributeValueID']] = [
                "value" => $value['itemAttributeValueID'],
                "description" => $value['itemAttributeValueDescription']
            ];
        }

        if(count($attrValues) > 0){
            $this->status = true;
            $this->data = $attrValues;
            return $this->JSONRespond();
        } else {
            $this->status = false;
            return $this->JSONRespond();
        }
    }
}
