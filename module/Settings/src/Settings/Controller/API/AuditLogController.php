<?php

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

/**
 * Description of auditLogController
 *
 * @author ashan
 */
class AuditLogController extends CoreController
{

    public function searchAuditLogByDateFilterAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromDate = $this->convertDateToStandardFormat($request->getPost('fromDate'));
            $toDate = $this->convertDateToStandardFormat($request->getPost('toDate'));
            
            $auditLogData = $this->CommonTable('Core\Model\LogTable')->getAuditLogByDateFilter($fromDate,$toDate);
            $listview = new ViewModel([
                'auditLog' => $auditLogData,
            ]);

            $listview->setTerminal(true);
            $listview->setTemplate('settings/audit-log/audit-log-list');

            $this->setLogMessage("Retrive audit log list by date filer for list view.");
            $this->status = true;
            $this->html = $listview;
            return $this->JSONRespondHtml();
        }
    }

     public function searchAuditLogDetaislByLogIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $logID = $request->getPost('logID');
            
            $auditLogFeature = $this->CommonTable('Core\Model\LogDetailsTable')->getLogDetailsByLogID($logID)->current();

            $attchemntLogPreviewFlag = false;
            if ($auditLogFeature['featureAction'] == "storeFiles") {
                $attchemntLogPreviewFlag = true;
            } 
           
            $auditLogDetailsData = $this->CommonTable('Core\Model\LogDetailsTable')->getLogDetailsByLogID($logID);
            $listview = new ViewModel([
                'auditLogDetails' => $auditLogDetailsData,
                'attchemntLogPreviewFlag' => $attchemntLogPreviewFlag,
            ]);

            $listview->setTerminal(true);
            $listview->setTemplate('settings/audit-log/audit-log-details');

            $this->setLogMessage("Retrive audit log details by audit logID.");
            $this->status = true;
            $this->html = $listview;
            return $this->JSONRespondHtml();
        }
    }
}
