<?php

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class PurchaseRequisitionTypeController extends CoreController
{

    public function addAction()
    {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost()['expTypePostData'];
            $this->beginTransaction();
            $entityId = $this->createEntity();
            $pRTypeData = array(
                'purchaseRequisitionTypeName' => $post['expTypeName'],
                'purchaseRequisitionTypeCategory' => $post['expTypeCat'],
                'purchaseRequisitionTypeApproverEnabled' => $post['approverEnabled'],
                'purchaseRequisitionTypeStatus' => $this->getStatusID('active'),
                'entityId' => $entityId
            );
            $pRTypeModel = new \Settings\Model\PurchaseRequisitionType();
            $pRTypeModel->exchangeArray($pRTypeData);
            $savedPrTypeId = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeTable')->create($pRTypeModel);
            if ($savedPrTypeId) {
                if ($post['approverEnabled']) {
                    foreach ($post['expValueLimits'] as $expValueLimit) {
                        $pRTypeLimitData = array(
                            'purchaseRequisitionTypeId' => $savedPrTypeId,
                            'purchaseRequisitionTypeLimitMin' => $expValueLimit['min'],
                            'purchaseRequisitionTypeLimitMax' => $expValueLimit['max']
                        );
                        $pRTypeLimitModel = new \Settings\Model\PurchaseRequisitionTypeLimit();
                        $pRTypeLimitModel->exchangeArray($pRTypeLimitData);
                        $savedPRTypeLimitId = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeLimitTable')->create($pRTypeLimitModel);
                        foreach ($expValueLimit['approvers'] as $approver) {
                            $pRTypeLimitApproverData = array(
                                'purchaseRequisitionTypeLimitId' => $savedPRTypeLimitId,
                                'purchaseRequisitionTypeLimitApprover' => $approver,
                            );
                            $pRTypeLimitApproverModel = new \Settings\Model\PurchaseRequisitionTypeLimitApprover();
                            $pRTypeLimitApproverModel->exchangeArray($pRTypeLimitApproverData);
                            $savedExpenseTypeLimitApproverId = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeLimitApproverTable')->create($pRTypeLimitApproverModel);
                        }
                    }
                    $this->commit();
                    $this->status = TRUE;
                    $this->msg = $this->getMessage('SUCC_PRTYP_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                } else {
                    $this->commit();
                    $this->status = TRUE;
                    $this->msg = $this->getMessage('SUCC_PRTYP_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                }
            } else {
                $this->rollback();
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PRTYP_SAVE');
            }
            return $this->JSONRespond();
        }
    }

    public function editAction()
    {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost()['expTypePostData'];
            $this->beginTransaction();
            $editExpTypeId = $post['editId'];
            $expenseTypeData = array(
                'purchaseRequisitionTypeName' => $post['expTypeName'],
                'purchaseRequisitionTypeCategory' => $post['expTypeCat'],
                'purchaseRequisitionTypeApproverEnabled' => $post['approverEnabled'],
            );
            $isUpdated = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeTable')->update($expenseTypeData, $editExpTypeId);

            if (!$isUpdated) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PRTYP_UPD');
                return $this->JSONRespond();
            }
            $expTypeData = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeTable')->getPRTypeByID($editExpTypeId);
            $this->updateEntity($expTypeData['entityId']);
            //delete all approver limits and approvers belongs to old expTypeId and reinsert new data
            if ($post['approverEnabled']) {
                //deleting process
                $oldApproverLimits = $this->CommonTable("Settings\Model\PurchaseRequisitionTypeLimitTable")->getLimitsByTypeId($editExpTypeId);
                foreach ($oldApproverLimits as $limit) {
                    //deletes approvers for current limit
                    $approversDeleted = $this->CommonTable("Settings\Model\PurchaseRequisitionTypeLimitApproverTable")->deleteByExpTypeLimitId($limit['purchaseRequisitionTypeLimitId']);
                }
                //delete approve limits
                $limitsDeleted = $this->CommonTable("Settings\Model\PurchaseRequisitionTypeLimitTable")->deleteByExpTypeId($editExpTypeId);

                foreach ($post['expValueLimits'] as $expValueLimit) {
                    $expenseTypeLimitData = array(
                        'purchaseRequisitionTypeId' => $editExpTypeId,
                        'purchaseRequisitionTypeLimitMin' => $expValueLimit['min'],
                        'purchaseRequisitionTypeLimitMax' => $expValueLimit['max']
                    );
                    $expensetypeLimitModel = new \Settings\Model\PurchaseRequisitionTypeLimit();
                    $expensetypeLimitModel->exchangeArray($expenseTypeLimitData);
                    $savedExpenseTypeLimitId = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeLimitTable')->create($expensetypeLimitModel);
                    foreach ($expValueLimit['approvers'] as $approver) {
                        $expenseTypeLimitApproverData = array(
                            'purchaseRequisitionTypeLimitId' => $savedExpenseTypeLimitId,
                            'purchaseRequisitionTypeLimitApprover' => $approver,
                        );
                        $expenseTypeLimitApproverModel = new \Settings\Model\PurchaseRequisitionTypeLimitApprover();
                        $expenseTypeLimitApproverModel->exchangeArray($expenseTypeLimitApproverData);
                        $savedExpenseTypeLimitApproverId = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeLimitApproverTable')->create($expenseTypeLimitApproverModel);
                    }
                }
                $this->commit();
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_PRTYP_UPD');
                $this->flashMessenger()->addMessage($this->msg);
            } else {

                //deleting process
                $oldApproverLimits = $this->CommonTable("Settings\Model\PurchaseRequisitionTypeLimitTable")->getLimitsByTypeId($editExpTypeId);
                foreach ($oldApproverLimits as $limit) {
                    //deletes approvers for current limit
                    $approversDeleted = $this->CommonTable("Settings\Model\PurchaseRequisitionTypeLimitApproverTable")->deleteByExpTypeLimitId($limit['purchaseRequisitionTypeLimitId']);
                }
                //delete approve limits
                $limitsDeleted = $this->CommonTable("Settings\Model\PurchaseRequisitionTypeLimitTable")->deleteByExpTypeId($editExpTypeId);

                $this->commit();
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_PRTYP_UPD');
                $this->flashMessenger()->addMessage($this->msg);
            }

            return $this->JSONRespond();
        }
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getPost()) {
            $expTypeId = $this->request->getPost('expTypeId');
            $expTypeData = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeTable')->getPRTypeByID($expTypeId);
            if ($this->updateDeleteInfoEntity($expTypeData['entityId'])) {
                $this->msg = $this->getMessage('SUCC_PRTYP_DEL');
                $this->status = TRUE;
            } else {
                $this->msg = $this->getMessage('ERR_PRTYP_DEL');
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function changeExpenseTypeStatusAction()
    {
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $purchaseRequisitionTypeId = $postData['expenseTypeId'];
            $changeStatusTo = $this->getStatusID($postData['changeStatusTo']);
            if ($this->CommonTable('Settings\Model\PurchaseRequisitionTypeTable')->changeStatus($changeStatusTo, $purchaseRequisitionTypeId)) {
                $this->status = TRUE;
            } else {
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function getExpenseTypeBySearchAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $purchaseRequisitionTypeId = $searchrequest->getPost('searchKey');
            $expenseTypes = $this->CommonTable('Settings/Model/PurchaseRequisitionTypeTable')->searchExpenseTypeForDropDown($purchaseRequisitionTypeId);

            $expenseTypeList = array();
            foreach ($expenseTypes as $expenseType) {
                array_push($expenseTypeList, array(
                    'value' => $expenseType['purchaseRequisitionTypeId'],
                    'text' => $expenseType['purchaseRequisitionTypeName']
                    )
                );
            }

            $this->data = array('list' => $expenseTypeList);
            return $this->JSONRespond();
        }
    }
}
