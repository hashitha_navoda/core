<?php
namespace Settings\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeValue;
use Settings\Form\ItemAttributeForm;
use Zend\View\Model\JsonModel;

class SmsNotificationController extends CoreController
{

    public function updateSmsTypeAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('SmsNotificationService')->updateSmsType($request->getPost());
        
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    public function disableSmsTypeAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('SmsNotificationService')->disableSmsTypeNotification($_POST['smsTypeId']);
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

     /**
     * Save smsType message
     * @param postData
     * @return JSONRespond
     */
    public function saveSmsTypeMessageAction(){
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('SmsNotificationService')->saveSmsTypeMessage($request->getPost());
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

     /**
     * Save smsType message
     * @param postData
     * @return JSONRespond
     */

    public function sendNotificationSmsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $customers = $post['selectedSmsCustomers'];
            $smsBody = trim($post['smsTypeMessage']);
            $smsTypeId = (int) $post['smsTypeId'];
            $isIncludeInvoiceDetails = (int) $post['isIncludeInvoiceDetails'];

            $error_message = '';
            $success_message = '';
            
            foreach ($customers as $customer) {
                $message = $smsBody."\n";
                
                if($isIncludeInvoiceDetails){
                    $smsIncludes = $post['smsIncluded'];
                    if(\sizeof($smsIncludes) > 0){
                        foreach ($smsIncludes as $key) {
                            $smsIncludedDetails = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedById((int) $key);
                            switch ($smsIncludedDetails->current()['smsIncludedName']) {
                                case "Total due amount":
                                    $message .= 'Total due amount : '.$customer['dueAmount']."\n";
                                    break;
                                case "Number of due invoices":
                                    $message .= 'Number of due invoices : '.$customer['noOfDueInvoices']."\n";
                                    break;
                                case "Invoice with due date":
                                    $message .= 'Invoice with due date : '.$customer['invoicesDetails']."\n";
                                    break;   
                                case "Last invoice date":
                                    $message .= 'Last invoice date : '.$customer['lastInvoiceDate']."\n";
                                    break;   
                                case "Last invoice amount":
                                    $message .= 'Last invoice amount : '.$customer['salesinvoiceTotalAmount']."\n";
                                    break;   
                                case "Salesperson of last invoice":
                                    $message .= 'Salesperson of last invoice : '.$customer['salesPersonSortName']."\n";
                                    break;
                                case "Cheque Date":
                                    $message .= 'Cheque Date : '.$customer['postdatedChequeDate']."\n";
                                    break;
                                case "Cheque amount":
                                    $message .= 'Cheque amount : '.$customer['incomingPaymentPaidAmount']."\n";
                                    break;
                                case "Cheque number":
                                    $message .= 'Cheque number : '.$customer['chequeNumber']."\n";
                                    break;
                                default:
                                    break;
                            }
                        }
                    }
                }
                $sendRes = $this->sendSms($customer['customerTelephoneNumber'], $message);
                if($sendRes){
                    $success_message .= $customer['customerName'].", ";
                }else{
                    $error_message .= $customer['customerName'].", ";
                }
            }

            $this->status = true;
            if(strlen($error_message) > 0){
                $error_message = $this->getMessage('ERR_PROMO_SMS_SEND',array($error_message));
            }
            if(strlen($success_message) > 0){
                $success_message = $this->getMessage('SUCC_PROMO_SMS_SEND',array($success_message));
            }
            $this->data = array('error_msg' => $error_message, 'success_msg' => $success_message);
            return $this->JSONRespond();
        }
    }

     /**
     * Save smsType message
     * @param postData
     * @return JSONRespond
     */
    public function getPostDatedChequeAction(){
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        
        if ($request->isPost()) {
            $post = $request->getPost();
            $startDate = $post['startDate'];
            $endDate = trim($post['endDate']);

            $customers = [];
            $tempCusts =  $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getPostdatedChequesBetweenDateRange($startDate,$endDate);
            $i = 0;
            foreach ($tempCusts as $tempCust) {
                // var_dump($tempCust);
                if ($tempCust['customerID'] != 0) {
                        $customers[$i] = array(
                            'customerCode' => $tempCust['customerCode'], 
                            'customerName' => $tempCust['customerTitle'] . ' ' . $tempCust['customerName'],
                            'customerTelephoneNumber' => $tempCust['customerTelephoneNumber'],
                            'incomingPaymentPaidAmount' =>number_format($tempCust['incomingPaymentPaidAmount'], 2),
                            'postdatedChequeDate' => $tempCust['postdatedChequeDate'],
                            'chequeNumber'=> $tempCust['incomingPaymentMethodChequeNumber']
                        );
                        $i++;
                }
            }
            $this->status = TRUE;
            $this->data = $customers;
            return $this->JSONRespond();
        }
    }

}