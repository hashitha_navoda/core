<?php

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class ApproverController extends CoreController
{

    public function createApproverAction() {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('ApproverService')->createApprover($request->getPost(), $this->userID);
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    public function changeStatusIDAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $updateState = $this->getService('ApproverService')->updateApproverState($request->getPost());
        if ($updateState['status']) {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($updateState['msg']);
        } else {
            $this->status = false;
            $this->msg = $this->getMessage($updateState['msg']);
            $this->rollback();
        }
        return $this->JSONRespond();
    }

    public function updateApproverAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('ApproverService')->updateApprover($request->getPost(), $this->userID);

        if (!$respond['status']) {

            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        } else {
            $this->commit();
            $this->status = true;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
        }

        return $this->JSONRespond();
    }

    public function deleteApproverAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('ApproverService')->deleteApprover($request->getPost(), $this->userID);

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    public function searchApproverAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $result = $this->getService('ApproverService')->approverSearchByKey($request->getPost(), (int) $this->params()->fromRoute('param1', 1));

        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();

    }


    /**
     * This function is used to search approvers for dropdown
     */
    public function searchApproversForDropdownAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $respond = $this->getService('ApproverService')->searchApproversForDropdown($request->getPost());

        $this->data = array('list' => $respond);
        return $this->JSONRespond();
    }
}
