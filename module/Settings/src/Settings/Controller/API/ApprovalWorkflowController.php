<?php

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class ApprovalWorkflowController extends CoreController
{

    public function saveAction() {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost()['workflowPostData'];

            $checkApprovalWorkflowsCodeDataSet = $this->CommonTable('Settings\Model\ApprovalWorkflowsTable')->checkApprovalWorkflowsByCode($post['workflowCode']);
            if ($checkApprovalWorkflowsCodeDataSet->count() > 0) { 
                $this->status = FALSE;
                $this->msg = $this->getMessage('WORKFLOW_CODE_ALREADY_EXIST');
                return $this->JSONRespond();
            }
            
            $this->beginTransaction();
            $entityId = $this->createEntity();
            $workflowData = array(
                'workflowName' => $post['workflowName'],
                'workflowCode' => $post['workflowCode'],
                'rootDocumentTypeID' => $post['documentID'],
                'workflowStatus' => $this->getStatusID('active'),
                'entityId' => $entityId
            );

            $workflowModel = new \Settings\Model\ApprovalWorkflows();
            $workflowModel->exchangeArray($workflowData);

            $workflowId = $this->CommonTable('Settings\Model\ApprovalWorkflowsTable')->saveApprovalWorkflows($workflowModel);

            if ($workflowId) {
                foreach ($post['workflowValueLimits'] as $workflowLimit) {
                        $workflowLimitData = array(
                            'approvalWorkflowID' => $workflowId,
                            'approvalWorkflowLimitMin' => $workflowLimit['min'],
                            'approvalWorkflowLimitMax' => $workflowLimit['max']
                        );
                        $workflowLimitModel = new \Settings\Model\ApprovalWorkflowLimits();
                        $workflowLimitModel->exchangeArray($workflowLimitData);

                        $savedApprovalWorkflowLimitId = $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->saveApprovalWorkflowLimits($workflowLimitModel);
                        foreach ($workflowLimit['approvers'] as $approver) {
                            $wfLimitApproverData = array(
                                'workflowLimitID' => $savedApprovalWorkflowLimitId,
                                'workflowLimitApprover' => $approver,
                            );
                            $wfLimitApproverModel = new \Settings\Model\ApprovalWorkflowLimitApprovers();
                            $wfLimitApproverModel->exchangeArray($wfLimitApproverData);
                            $savedWFLimitApproverId = $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->saveWorkflowLimitApprovers($wfLimitApproverModel);
                        }
                }
                $this->commit();
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_APPR_WF_SAVE');
                $this->flashMessenger()->addMessage($this->msg);
            } else {
                $this->rollback();
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_APPR_WF_SAVE');
            }
            return $this->JSONRespond();
        }
    }

    public function changeWorkFlowStatusAction() {
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $workflowID = $postData['workflowId'];
            $changeStatusTo = $this->getStatusID($postData['changeStatusTo']);
            if ($this->CommonTable('Settings\Model\ApprovalWorkflowsTable')->changeStatus($changeStatusTo, $workflowID)) {
                $this->status = TRUE;
            } else {
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function editAction()
    {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost()['workflowPostData'];
            $this->beginTransaction();
            $workflowID = $post['editId'];
            $workflowData = array(
                'workflowName' => $post['workflowName'],
                'workflowCode' => $post['workflowCode'],
            );
            $isUpdated = $this->CommonTable('Settings\Model\ApprovalWorkflowsTable')->update($workflowData, $workflowID);

            if (!$isUpdated) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PRTYP_UPD');
                return $this->JSONRespond();
            }
            $wfData = $this->CommonTable('Settings\Model\ApprovalWorkflowsTable')->getWFByID($workflowID);
            $this->updateEntity($wfData['entityId']);
            //delete all approver limits and approvers belongs to old expTypeId and reinsert new data


            //deleting process
            $oldApproverLimits = $this->CommonTable("Settings\Model\ApprovalWorkflowLimitsTable")->getLimitsByWFId($workflowID);
            foreach ($oldApproverLimits as $limit) {
                //deletes approvers for current limit
                $approversDeleted = $this->CommonTable("Settings\Model\ApprovalWorkflowLimitApproversTable")->deleteByWFLimitId($limit['approvalWorkflowLimitsID']);
            }
            //delete approve limits
            $limitsDeleted = $this->CommonTable("Settings\Model\ApprovalWorkflowLimitsTable")->deleteByWFId($workflowID);

            foreach ($post['workflowValueLimits'] as $workflowLimit) {
                $workflowLimitData = array(
                    'approvalWorkflowID' => $workflowID,
                    'approvalWorkflowLimitMin' => $workflowLimit['min'],
                    'approvalWorkflowLimitMax' => $workflowLimit['max']
                );
                $workflowLimitModel = new \Settings\Model\ApprovalWorkflowLimits();
                $workflowLimitModel->exchangeArray($workflowLimitData);

                $savedApprovalWorkflowLimitId = $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->saveApprovalWorkflowLimits($workflowLimitModel);
                foreach ($workflowLimit['approvers'] as $approver) {
                    $wfLimitApproverData = array(
                        'workflowLimitID' => $savedApprovalWorkflowLimitId,
                        'workflowLimitApprover' => $approver,
                    );
                    $wfLimitApproverModel = new \Settings\Model\ApprovalWorkflowLimitApprovers();
                    $wfLimitApproverModel->exchangeArray($wfLimitApproverData);
                    $savedWFLimitApproverId = $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->saveWorkflowLimitApprovers($wfLimitApproverModel);
                }
            }
            $this->commit();
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_WF_UPD');
            $this->flashMessenger()->addMessage($this->msg);

            return $this->JSONRespond();
        }
    }

    public function deleteAction() {
        if ($this->getRequest()->getPost()) {
            $workflowId = $this->request->getPost('workflowId');
            $wfData = $this->CommonTable('Settings\Model\ApprovalWorkflowsTable')->getWFByID($workflowId);
            if ($this->updateDeleteInfoEntity($wfData['entityId'])) {
                $this->msg = $this->getMessage('SUCC_WF_DEL');
                $this->status = TRUE;
            } else {
                $this->msg = $this->getMessage('ERR_WF_DEL');
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }
}
