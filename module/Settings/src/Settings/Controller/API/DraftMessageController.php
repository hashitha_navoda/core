<?php
namespace Settings\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeValue;
use Settings\Form\ItemAttributeForm;
use Zend\View\Model\JsonModel;
use Settings\Model\DraftMessage;

class DraftMessageController extends CoreController
{

    /**
     * Save draft message
     * @return JSONRespond
     */
    public function saveAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost())
        {
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }

        $post = $request->getPost()->toArray();

        $this->beginTransaction();
        $dataInserted = FALSE;
        $entityID = $this->createEntity();

        $draftData = array(
            'draftMessageName' => $post['draftMessageName'],
            'draftMessageMessage' => $post['draftMessageMessage'],
            'draftMessageDate' => $this->convertDateToStandardFormat(date('Y-m-d')),
            'entityID' => $entityID,
        );

        $draftMessage = new DraftMessage();
        $draftMessage->exchangeArray($draftData);
        $draftID = $this->CommonTable('Settings\Model\DraftMessageTable')->save($draftMessage);

        if($draftID == null){
            $this->rollback();
            $this->msg = $this->getMessage('ERR_DRAFT_MSG_SAVE');
            $this->status = false;
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_DRAFT_MSG_SAVE');
        $this->data = $draftID;
        return $this->JSONRespond();
    }

    /**
     * Get draft message
     * @return JSONRespond
     */
    public function getDraftMessageListAction()
    {
        $draftMessageData = $this->CommonTable('Settings\Model\DraftMessageTable')->fetchAll();

        $draftMessages = [];
        foreach ($draftMessageData as $key => $draftMessage) {
            $draftMessages[] = array(
                'draftMessageID' => $draftMessage->draftMessageID,
                'draftMessageName' => $draftMessage->draftMessageName,
                'draftMessageMessage' => $draftMessage->draftMessageMessage,
                'draftMessageDate' => $draftMessage->draftMessageDate,
            );
        }
        
        $this->data = $draftMessages;
        $this->status = true;
        return $this->JSONRespond();
    }

    /**
     * Update draft message
     * @return JSONRespond
     */
    public function updateAction()
    {
        $request = $this->getRequest();
        if(!$request->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }

        $post = $request->getPost()->toArray();
        $id = $post['draftMessageID'];

        $draftData = array(
            'draftMessageName' => $post['draftMessageName'],
            'draftMessageMessage' => $post['draftMessageMessage']
        );
        
        $respond = $this->CommonTable('Settings\Model\DraftMessageTable')->updateDraftMessage($draftData, (int)$id);
       
        $this->status = true;
        $this->msg = $this->getMessage('SUCCESS_UPDATE_DRAFT_MSG');
        return $this->JSONRespond();
        
    }

}