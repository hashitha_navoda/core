<?php

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Settings\Model\CustomerEvent;
use Settings\Form\CustomerEventForm;
use Zend\View\Model\ViewModel;

/**
 * Description of CustomerEventController
 *
 * @author shermilan
 */
class CustomerEventController extends CoreController
{

    protected $paginator;

    public function saveAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
//          check the name already exists
            $isExists = $this->CommonTable('Settings\Model\CustomerEventTable')->getByName($request->getPost('customerEventName'));
            if (count($isExists) > 0) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUS_EVENT_NAME_DUPLICATE');
                return $this->JSONRespond();
            }
            $customerEvent = new CustomerEvent();
            $customerEventForm = new CustomerEventForm();
            $customerEventForm->setInputFilter($customerEvent->getInputFilter());
            $customerEventForm->setData($request->getPost());
            if ($customerEventForm->isValid()) {
                $customerEvent->exchangeArray($customerEventForm->getData());
                $customerEvent->entityID = $this->createEntity();
                $insertedID = $this->CommonTable('Settings\Model\CustomerEventTable')->save($customerEvent);
                if ($insertedID) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_CUS_EVENT_SAVE');
                    $this->pagintedCustomerEvent();
                    $listView = new ViewModel([
                        'customerEvents' => $this->paginator,
                        'paginated' => TRUE,
                    ]);
                    $listView->setTemplate('settings/customer-event/list');
                    $listView->setTerminal(true);
//                  send back newly added customer Event
                    $insertedCustomerEvent = $this->CommonTable('Settings\Model\CustomerEventTable')->getByID($insertedID)->current();
                    $this->data = ['key' => $insertedID, 'value' => $insertedCustomerEvent['customerEventName']];
                    $this->html = $listView;
                    return $this->JSONRespondHtml();
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUS_EVENT_SAVE');
                return $this->JSONRespond();
            }
        }
    }

    public function pagintedCustomerEvent($rowCount = 6)
    {
        $this->paginator = $this->CommonTable('Settings\Model\CustomerEventTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($rowCount);
    }

    public function updateStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerEventID = $request->getPost('customerEventID');

            $customerEvent = $this->CommonTable('Settings\Model\CustomerEventTable')->getByID($customerEventID)->current();

            if ($customerEvent) {
                if ($customerEvent['customerEventStatus']) {
                    $customerEventStatus = '0';
                } else {
                    $customerEventStatus = '1';
                }

                $updated = $this->CommonTable('Settings\Model\CustomerEventTable')->updateStatus($customerEventID, $customerEventStatus);
                if ($updated) {
                    $this->pagintedCustomerEvent();
                    $listView = new ViewModel([
                        'customerEvents' => $this->paginator,
                        'paginated' => TRUE,
                    ]);
                    $listView->setTemplate('settings/customer-event/list');
                    $listView->setTerminal(true);
                    $this->html = $listView;
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_CUS_EVENT_UPDATE_STATUS');
                    return $this->JSONRespondHtml();
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUS_EVENT_UPDATE_STATUS');
                    return $this->JSONRespond();
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUS_EVENT_DEL_UPDATE_STATUS');
                return$this->JSONRespond();
            }
        }
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerEventID = $request->getPost('customerEventID');
//          check customer event is already used in customer
            $isUsed = $this->CommonTable('Invoice\Model\CustomerTable')->getByCustomerEventID($customerEventID);
            
            if (count($isUsed) > 0) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUS_EVENT_DELETE_USED_IN_CUSTOMER');
                return $this->JSONRespond();
            }

            $customerEvent = $this->CommonTable('Settings\Model\CustomerEventTable')->getByID($customerEventID)->current();
            $isDeleted = $this->updateDeleteInfoEntity($customerEvent['entityID']);
            if ($isDeleted) {
                $this->pagintedCustomerEvent();
                $listView = new ViewModel([
                    'customerEvents' => $this->paginator,
                    'paginated' => TRUE,
                ]);
                $listView->setTemplate('settings/customer-event/list');
                $listView->setTerminal(true);
                $this->html = $listView;
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_CUS_EVENT_DELETED');
                return $this->JSONRespondHtml();
            }
        }
    }

    public function editAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerEventID = $request->getPost('customerEventID');
            $customerEvent = $this->CommonTable('Settings\Model\CustomerEventTable')->getByID($customerEventID)->current();
            if ($customerEvent) {
                $this->status = true;
                $this->data = $customerEvent;
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUS_EVENT_DELTED_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

    public function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $isExists = $this->CommonTable('Settings\Model\CustomerEventTable')->getByName($request->getPost('customerEventName'))->current();

            if ($isExists && $isExists['customerEventID'] != $request->getPost('customerEventID')) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUS_EVENT_NAME_DUPLICATE');
                return $this->JSONRespond();
            }

            $customerEvent = new CustomerEvent();
            $customerEventForm = new CustomerEventForm();
            $customerEventForm->setInputFilter($customerEvent->getInputFilter());
            $customerEventForm->setData($request->getPost());
            if ($customerEventForm->isValid()) {
                $customerEvent->exchangeArray($customerEventForm->getData());
                $isUpdated = $this->CommonTable('Settings\Model\CustomerEventTable')->update($customerEvent);
                if ($isUpdated) {
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_CUS_EVENT_UPDATE');
                    $this->pagintedCustomerEvent();
                    $listView = new ViewModel([
                        'customerEvents' => $this->paginator,
                        'paginated' => TRUE,
                    ]);
                    $listView->setTemplate('settings/customer-event/list');
                    $listView->setTerminal(true);
                    $this->html = $listView;
                    return $this->JSONRespondHtml();
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUS_EVENT_SAVE');
                return $this->JSONRespond();
            }
        }
    }

    public function searchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $searchKey = trim($request->getPost('searchKey'));
            if ($searchKey == '') {
                $this->pagintedCustomerEvent();
                $customerEvents = $this->paginator;
                $paginated = true;
            } else {
                $paginated = false;
                $customerEvents = $this->CommonTable('Settings\Model\CustomerEventTable')->searchByName($searchKey);
            }

            $listview = new ViewModel([
                'customerEvents' => $customerEvents,
                'paginated' => $paginated,
            ]);

            $listview->setTerminal(true);
            $listview->setTemplate('settings/customer-event/list');
            $this->status = true;
            $this->html = $listview;
            return $this->JSONRespondHtml();
        }
    }

    public function getCustomerEventsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost('locationID');
            $fromDate = $this->convertDateToStandardFormat($post['fromDate']);
            $toDate = $this->convertDateToStandardFormat($post['toDate']);
            $customerEvents = $this->CommonTable("Settings\Model\CustomerEventTable")->getByDate($fromDate, $toDate);
            $temp = array();
            foreach ($customerEvents as $customerEvent) {
                $temp['value'] = $customerEvent['customerEventID'];
                $temp['text'] = $customerEvent['customerEventName'];
                $customerEventList[] = $temp;
            }
            $this->data = array('list' => $customerEventList);
            return $this->JSONRespond();
        }
    }

    public function getCustomersForCustomerEventsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerEvents = $request->getPost('customerEvents');
            $customers = [];
            foreach ($customerEvents as $customerEvent) {
                $tempCusts = $this->CommonTable("Invoice\Model\CustomerTable")->getCustomersByCustomerEvent($customerEvent);
                foreach ($tempCusts as $tempCust) {
                    if ($tempCust['customerID'] != 0) {
                        $customers[$tempCust['customerID']] = array(
                            'name' => $tempCust['customerTitle'] . ' ' . $tempCust['customerName'],
                            'email' => $tempCust['customerProfileEmail'],
                            'customerStatus' => $tempCust['customerStatus']==1?'Active':'Inactive'
                        );
                    }
                }
            }
            $this->status = TRUE;
            $this->data = $customers;
            return $this->JSONRespond();
        }
    }

}
