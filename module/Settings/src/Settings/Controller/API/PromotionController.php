<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains products promotions related controller functions
 */

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Settings\Model\Promotion;
use Settings\Model\PromotionProduct;
use Settings\Model\InvoicePromotion;
use Settings\Model\PromotionEmail;
use Settings\Model\PromotionEmailLocation;
use Settings\Model\PromotionEmailProduct;
use Settings\Model\PromotionEmailCustomer;
use Settings\Model\PromotionEmailContact;
use Settings\Model\PromotionCombination;
use Settings\Model\ItemBaseCombinationRule;
use Settings\Model\AttributeBaseCombinationRule;
use Zend\View\Model\ViewModel;

/**
 * company controller class create
 */
class PromotionController extends CoreController
{

    public function saveAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost('promoData');

            //save Promotion
            $this->beginTransaction();
            $dataInserted = FALSE;
            $entityID = $this->createEntity();
            $locationID = $post['locationID'];

            $promotionData = array(
                'promotionName' => $post['promoName'],
                'promotionFromDate' => $this->convertDateToStandardFormat($post['promoFromDate']),
                'promotionToDate' => $this->convertDateToStandardFormat($post['promoToDate']),
                'promotionType' => $post['promoType'],
                'promotionLocation' => $locationID,
                'promotionStatus' => $this->getStatusID('active'),
                'promotionDescription' => $post['promoDesc'],
                'entityID' => $entityID,
            );
            $promotion = new Promotion();
            $promotion->exchangeArray($promotionData);
            $promotionID = $this->CommonTable('Settings\Model\PromotionTable')->save($promotion);


            if ($promotionID) {

                if ($post['promoType'] == 1 || $post['promoType'] == 3) {

                    foreach ($post['promotionCombinations'] as $promotionCombination) {
                        $promotionCombinationData = array(
                            'promotionID' => $promotionID,
                            'combinationName' => $promotionCombination['combinationName'],
                            'conditionType' => $promotionCombination['majorCondition'],
                            'discountType' => $promotionCombination['discountType'],
                            'discountAmount' => $promotionCombination['amount']
                        );

                        $promotionCombinationDataSet = new PromotionCombination();
                        $promotionCombinationDataSet->exchangeArray($promotionCombinationData);
                        $promotionCombinationID = $this->CommonTable('Settings\Model\PromotionCombinationTable')->save($promotionCombinationDataSet);

                        if ($promotionCombinationID) {
                            if  ($post['promoType'] == 1) {
                                foreach ($promotionCombination['ruleSet'] as $key => $rule) {
                                    $itemRuleData = array(
                                        'combinationID' => $promotionCombinationID,
                                        'rule' => $rule['ruleType'],
                                        'quantity' => $rule['quantity'],
                                        'productID' => $rule['productID'],
                                    );

                                    $itemRule = new ItemBaseCombinationRule();
                                    $itemRule->exchangeArray($itemRuleData);
                                    $ruleId = $this->CommonTable('Settings\Model\ItemBaseCombinationRuleTable')->save($itemRule);
                                }

                                if (!$ruleId) {
                                    $this->rollback();
                                    $this->msg = $this->getMessage('ERR_PROMO_SAVE');
                                    $this->status = false;
                                } else {
                                    $dataInserted = true;
                                }
                            } elseif ($post['promoType'] == 3) {
                                foreach ($promotionCombination['ruleSet'] as $key => $rule) {
                                    $itemRuleData = array(
                                        'combinationID' => $promotionCombinationID,
                                        'rule' => $rule['ruleType'],
                                        'quantity' => floatval($rule['quantity']),
                                        'attributeID' => ($rule['attributeType'] == 'quantity') ? 0 : floatval($rule['attributeType']),
                                        'attributeValID' => floatval($rule['attributeValID']),
                                    );

                                    $itemAttrRule = new AttributeBaseCombinationRule();
                                    $itemAttrRule->exchangeArray($itemRuleData);
                                    $ruleId = $this->CommonTable('Settings\Model\AttributeBaseCombinationRuleTable')->save($itemAttrRule);

                                }

                                if (!$ruleId) {
                                    $this->rollback();
                                    $this->msg = $this->getMessage('ERR_PROMO_SAVE');
                                    $this->status = false;
                                } else {
                                    $dataInserted = true;
                                }
                            }
                        }
                    }
                }

            }

            if ($post['promoType'] == 2) {
                //value based promotion
                $invoicePromotionData = array(
                    'invoicePromotionPromotionID' => $promotionID,
                    'invoicePromotionMinValue' => $post['promoValueMin'],
                    'invoicePromotionMaxValue' => $post['promoValueMax'],
                    'invoicePromotionDiscountType' => $post['promoValueDiscountType'],
                    'invoicePromotionDiscountAmount' => $post['promoValueDiscountAmount']
                );
                $invoicePromotion = new InvoicePromotion();
                $invoicePromotion->exchangeArray($invoicePromotionData);
                $invoicePromotionID = $this->CommonTable('Settings\Model\InvoicePromotionTable')->save($invoicePromotion);
                if ($invoicePromotionID) {
                    $dataInserted = TRUE;
                }
            }
            if ($dataInserted) {
                $this->commit();
                $this->msg = $this->getMessage('SUCC_PROMO_SAVE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->status = true;
            } else {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_PROMO_SAVE');
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    public function updateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost('promoData');
            $promotionID = $this->params()->fromRoute('param1');
            //update Promotion
            $this->beginTransaction();
            $dataInserted = FALSE;
            $locationID = ($post['locationID'] == '') ? NULL : $post['locationID'];

            //convert date format to database format
            $startingTime = '';
            $endingTime = '';
            if ($post['promoFromDate'] != '') {
                $startingTimes = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $post['promoFromDate']);
                $startingTime = date('Y-m-d H:i:s', strtotime($startingTimes));
            }
            if ($post['promoToDate'] != '') {
                $endingTimes = preg_replace("/(\d+)\D+(\d+)\D+(\d+)/", "$3-$2-$1", $post['promoToDate']);
                $endingTime = date('Y-m-d H:i:s', strtotime($endingTimes));
            }

            $promotionData = array(
                'promotionName' => $post['promoName'],
                'promotionFromDate' => $startingTime,
                'promotionToDate' => $endingTime,
                'promotionLocation' => $locationID,
                'promotionDescription' => $post['promoDesc'],
            );
            $promotionUpdated = $this->CommonTable('Settings\Model\PromotionTable')->edit($promotionData, $promotionID);
            if ($post['promoType'] == 1 || $post['promoType'] == 3) {

                $relatedCombinations = $this->CommonTable('Settings\Model\PromotionCombinationTable')->getRelatedCombinationsByPromotionId($promotionID);

                foreach ($relatedCombinations as $key => $combination) {

                    if ($post['promoType'] == 1) {
                      
                        $this->CommonTable('Settings\Model\ItemBaseCombinationRuleTable')->deleteByCombinationID($combination['promotionCombinationID']);
                       
                    } elseif ($post['promoType'] == 3) {
                        
                        $this->CommonTable('Settings\Model\AttributeBaseCombinationRuleTable')->deleteByCombinationID($combination['promotionCombinationID']);
                       
                    }

                }
                    
                //delete promotion combinations
                $this->CommonTable('Settings\Model\PromotionCombinationTable')->deleteByPromotionID($promotionID);

                foreach ($post['promotionCombinations'] as $promotionCombination) {
                    $promotionCombinationData = array(
                        'promotionID' => $promotionID,
                        'combinationName' => $promotionCombination['combinationName'],
                        'conditionType' => $promotionCombination['majorCondition'],
                        'discountType' => $promotionCombination['discountType'],
                        'discountAmount' => $promotionCombination['amount']
                    );

                    $promotionCombinationDataSet = new PromotionCombination();
                    $promotionCombinationDataSet->exchangeArray($promotionCombinationData);
                    $promotionCombinationID = $this->CommonTable('Settings\Model\PromotionCombinationTable')->save($promotionCombinationDataSet);

                    if ($promotionCombinationID) {
                        if  ($post['promoType'] == 1) {
                            foreach ($promotionCombination['ruleSet'] as $key => $rule) {
                                $itemRuleData = array(
                                    'combinationID' => $promotionCombinationID,
                                    'rule' => $rule['ruleType'],
                                    'quantity' => $rule['quantity'],
                                    'productID' => $rule['productID'],
                                );

                                $itemRule = new ItemBaseCombinationRule();
                                $itemRule->exchangeArray($itemRuleData);
                                $ruleId = $this->CommonTable('Settings\Model\ItemBaseCombinationRuleTable')->save($itemRule);
                            }

                            if (!$ruleId) {
                                $this->rollback();
                                $this->msg = $this->getMessage('ERR_PROMO_SAVE');
                                $this->status = false;
                            } else {
                                $dataInserted = true;
                            }
                        } elseif ($post['promoType'] == 3) {
                            foreach ($promotionCombination['ruleSet'] as $key => $rule) {
                                $itemRuleData = array(
                                    'combinationID' => $promotionCombinationID,
                                    'rule' => $rule['ruleType'],
                                    'quantity' => floatval($rule['quantity']),
                                    'attributeID' => ($rule['attributeType'] == 'quantity') ? 0 : floatval($rule['attributeType']),
                                    'attributeValID' => floatval($rule['attributeValID']),
                                );

                                $itemAttrRule = new AttributeBaseCombinationRule();
                                $itemAttrRule->exchangeArray($itemRuleData);
                                $ruleId = $this->CommonTable('Settings\Model\AttributeBaseCombinationRuleTable')->save($itemAttrRule);

                            }

                            if (!$ruleId) {
                                $this->rollback();
                                $this->msg = $this->getMessage('ERR_PROMO_SAVE');
                                $this->status = false;
                            } else {
                                $dataInserted = true;
                            }
                        }
                    }
                }
            } else if ($post['promoType'] == 2) {
                //value based promotion
                $invoicePromotionData = array(
                    'invoicePromotionPromotionID' => $promotionID,
                    'invoicePromotionMinValue' => $post['promoValueMin'],
                    'invoicePromotionMaxValue' => $post['promoValueMax'],
                    'invoicePromotionDiscountType' => $post['promoValueDiscountType'],
                    'invoicePromotionDiscountAmount' => $post['promoValueDiscountAmount']
                );
                $invoicePromotionID = $this->CommonTable('Settings\Model\InvoicePromotionTable')->editByPromotionID($invoicePromotionData, $promotionID);
                if ($invoicePromotionID) {
                    $dataInserted = TRUE;
                }
            }
            if ($dataInserted) {
                $this->commit();
                $this->msg = $this->getMessage('SUCC_PROMO_UPDATE');
                $this->flashMessenger()->addMessage($this->msg);
                $this->status = true;
            } else {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_PROMO_UPDATE');
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    public function getPromotionAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $promotionID = $request->getPost('promotionID');
            $promotionDetails = $this->getPromotionDetails($promotionID);
            $this->status = TRUE;
            $this->data = $promotionDetails[$promotionID];
        } else {
            $this->status = FALSE;
        }
        return $this->JSONRespond();
    }

    public function changePromotionStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $promotionID = $request->getPost('promotionID');
            $changeStatusTo = $request->getPost('changeStatusTo');
            if ($this->CommonTable('Settings\Model\PromotionTable')->changeStatus($changeStatusTo, $promotionID)) {
                $this->status = TRUE;
            } else {
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $promotionID = $request->getPost('promotionID');
            $promotionData = $this->CommonTable('Settings\Model\PromotionTable')->getPromotion($promotionID);
            $entityID = $promotionData['entityID'];
            if ($this->updateDeleteInfoEntity($entityID)) {
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_PROMO_DEL');
                $this->flashMessenger()->addMessage($this->msg);
            } else {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PROMO_DEL');
            }
            return $this->JSONRespond();
        }
    }

    private function getPromotionDetails($promotionID)
    {

        $this->service = $this->getService('PromotionService');
        $promotionDetails = $this->service->getPromotionDetails($promotionID);
        
        return $promotionDetails;
    }

    public function savePromotionEmailAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $customers = $post['customer'];
            $locations = $post['location'];
            $products = $post['product'];
            $imageRecieved = FALSE;
            if ($_FILES['emailPromotionFile']['name'] != '') {
                $imageRecieved = TRUE;
                //create images folder if not exists
                $name = $_FILES['emailPromotionFile']['tmp_name'];
                $path = $this->getCompanyDataFolder() . "/images";
                if (!is_dir($path)) {
                    mkdir($path, 0777, TRUE);
                }

                $ext = substr($_FILES['emailPromotionFile']['name'], strrpos($_FILES['emailPromotionFile']['name'], '.') + 1);
                $imageName = md5(microtime() . rand(500000, 100000000)) . "." . $ext;
                $imagePath = $path . "/" . $imageName;
                $companyNameHash = $this->getCompanyImageFolderName();
                $emailImagePath = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . $_SERVER['SERVER_NAME'] . "/userfiles/" . $companyNameHash . "/images/" . $imageName;
                move_uploaded_file($name, $imagePath);
            }

            $body = $post['emailBody'];
            if (isset($imagePath)) {
                $body = $body . "<img src='$emailImagePath'>";
            }


            $this->beginTransaction();
            $entityID = $this->createEntity();
            $promotionEmailData = array(
                'promotionEmailName' => $post['emailPromotionName'],
                'promotionEmailSubject' => $post['emailPromotionSubject'],
                'promotionEmailContent' => $post['emailBody'],
                'promotionEmailImage' => isset($imageName) ? $imageName : NULL,
                'entityID' => $entityID
            );

            $promotionEmail = new PromotionEmail();
            $promotionEmail->exchangeArray($promotionEmailData);
            $savedPromotionEmailID = $this->CommonTable('Settings\Model\PromotionEmailTable')->save($promotionEmail);
            if ($savedPromotionEmailID) {
                foreach ($locations as $location) {
                    $promotionEmailLocationData = array(
                        'promotionEmail' => $savedPromotionEmailID,
                        'promotionEmailLocation' => $location,
                    );
                    $promotionEmailLocation = new PromotionEmailLocation();
                    $promotionEmailLocation->exchangeArray($promotionEmailLocationData);
                    $savedPromotionEmailLocationID = $this->CommonTable('Settings\Model\PromotionEmailLocationTable')->save($promotionEmailLocation);
                }
                foreach ($products as $product) {
                    if ($product != 0) {
                        $promotionEmailProductData = array(
                            'promotionEmail' => $savedPromotionEmailID,
                            'promotionEmailProduct' => $product,
                        );
                        $promotionEmailProduct = new PromotionEmailProduct();
                        $promotionEmailProduct->exchangeArray($promotionEmailProductData);
                        $savedPromotionEmailProductID = $this->CommonTable('Settings\Model\PromotionEmailProductTable')->save($promotionEmailProduct);
                    }
                }
                foreach ($customers as $customer) {
                    $promotionEmailCustomerData = array(
                        'promotionEmail' => $savedPromotionEmailID,
                        'promotionEmailCustomer' => $customer,
                    );
                    $promotionEmailCustomer = new PromotionEmailCustomer();
                    $promotionEmailCustomer->exchangeArray($promotionEmailCustomerData);
                    $savedPromotionEmailCustomerID = $this->CommonTable('Settings\Model\PromotionEmailCustomerTable')->save($promotionEmailCustomer);
                    $customerEmail = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerAndPrimaryProfileByID($customer)->customerProfileEmail;
                    if ($customerEmail != '') {
                        $this->sendEmail($customerEmail, $post['emailPromotionSubject'], $body, NULL);
                    }
                }
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PROMO_EMAIL_SAVE');
                $this->flashMessenger()->addMessage($this->msg);
                return $this->JSONRespond();
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PROMO_EMAIL_SAVE');
                return $this->JSONRespond();
            }
        }
    }

    public function saveDirectPromotionEmailAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $contacts = $post['contacts'];
            $contactListArr = $post['contactsList'];

            $contactsData = $this->CommonTable('Invoice\Model\ContactsTable')->fetchAll(false);

            $contactList = [];
            foreach ($contactsData as $key => $contact) {
                $emails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->getRelatedEmailByContactID($contact['contactID']);

                if (sizeof($emails) > 0) {
                    if (array_key_exists(1, $emails)) {
                        $relateEmail = $emails[1];
                    } elseif (array_key_exists(2, $emails)) {
                        $relateEmail = $emails[2];
                    } elseif (array_key_exists(3, $emails)) {
                        $relateEmail = $emails[3];
                    }
                    $contact['email'] = $relateEmail;

                    $contactList[$contact['contactID']] = $contact['email'];

                }
            }
        
            $imageRecieved = FALSE;
            if ($_FILES['emailPromotionFile']['name'] != '') {
                $imageRecieved = TRUE;
                //create images folder if not exists
                $name = $_FILES['emailPromotionFile']['tmp_name'];
                $path = $this->getCompanyDataFolder() . "/images";
                if (!is_dir($path)) {
                    mkdir($path, 0777, TRUE);
                }

                $ext = substr($_FILES['emailPromotionFile']['name'], strrpos($_FILES['emailPromotionFile']['name'], '.') + 1);
                $imageName = md5(microtime() . rand(500000, 100000000)) . "." . $ext;
                $imagePath = $path . "/" . $imageName;
                $companyNameHash = $this->getCompanyImageFolderName();
                $emailImagePath = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . $_SERVER['SERVER_NAME'] . "/userfiles/" . $companyNameHash . "/images/" . $imageName;
                move_uploaded_file($name, $imagePath);
            }

            $body = $post['emailBody'];
            if (isset($imagePath)) {
                $body = $body . "<img src='$emailImagePath'>";
            }

            $this->beginTransaction();
            $entityID = $this->createEntity();
            $promotionEmailData = array(
                'promotionEmailName' => $post['emailPromotionName'],
                'promotionEmailSubject' => $post['emailPromotionSubject'],
                'promotionEmailContent' => $post['emailBody'],
                'promotionEmailImage' => isset($imageName) ? $imageName : NULL,
                'entityID' => $entityID,
                'isContactBasePromoEmail' => 1
            );

            $promotionEmail = new PromotionEmail();
            $promotionEmail->exchangeArray($promotionEmailData);
            $savedPromotionEmailID = $this->CommonTable('Settings\Model\PromotionEmailTable')->save($promotionEmail);
            if ($savedPromotionEmailID) {
                
                foreach ($contacts as $contact) {
                    $promotionEmailContactData = array(
                        'promotionEmailID' => $savedPromotionEmailID,
                        'contactID' => $contact,
                    );
                    $promotionEmailContact = new PromotionEmailContact();
                    $promotionEmailContact->exchangeArray($promotionEmailContactData);
                    $savedPromotionEmailContactID = $this->CommonTable('Settings\Model\PromotionEmailContactTable')->save($promotionEmailContact);
                    $contactEmail = $contactList[$contact];
                    if ($contactEmail != '') {
                        $this->sendEmail($contactEmail, $post['emailPromotionSubject'], $body, NULL);
                    }
                }

                // Check contact list is selected
                if(isset($post['contactsList'])){
                    foreach ($contactListArr as $contactListID) {// Loop contact list
    
                        // Get related contact IDs for relevant contact list id 
                        $contactIDs = $this->CommonTable('Invoice\Model\ContactListContactsTable')->getRelatedNumbersByContactListID($contactListID);
                        foreach($contactIDs as $contactID){ // Loop contact IDs
                            
                            $emails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->getRelatedEmailByContactID($contactID);

                            if (sizeof($emails) > 0) {
                                $relateEmail = "";
                                if (array_key_exists(1, $emails)) {
                                    $relateEmail = $emails[1];
                                } elseif (array_key_exists(2, $emails)) {
                                    $relateEmail = $emails[2];
                                } elseif (array_key_exists(3, $emails)) {
                                    $relateEmail = $emails[3];
                                }
                                
                                $promotionEmailContactData = array(
                                    'promotionEmailID' => $savedPromotionEmailID,
                                    'contactID' => $contactID,
                                );

                                $promotionEmailContact = new PromotionEmailContact();
                                $promotionEmailContact->exchangeArray($promotionEmailContactData);
                                $savedPromotionEmailContactID = $this->CommonTable('Settings\Model\PromotionEmailContactTable')->save($promotionEmailContact);
                                
                                if ($relateEmail != '') {
                                    $this->sendEmail($relateEmail, $post['emailPromotionSubject'], $body, NULL);
                                }
                            }
                        }
                    }
                }

                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PROMO_EMAIL_SAVE');
                $this->flashMessenger()->addMessage($this->msg);
                return $this->JSONRespond();
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PROMO_EMAIL_SAVE');
                return $this->JSONRespond();
            }
        }
    }

    public function savePromotionEmailCustomerEventsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $customers = $post['customer'];
            $customerEvents = $post['customerEvents'];
            $imageRecieved = FALSE;
            if ($_FILES['emailPromotionFile']['name'] != '') {
                $imageRecieved = TRUE;
                //create images folder if not exists
                $name = $_FILES['emailPromotionFile']['tmp_name'];
                $path = $this->getCompanyDataFolder() . "/images";
                if (!is_dir($path)) {
                    mkdir($path, 0777, TRUE);
                }

                $ext = substr($_FILES['emailPromotionFile']['name'], strrpos($_FILES['emailPromotionFile']['name'], '.') + 1);
                $imageName = md5(microtime() . rand(500000, 100000000)) . "." . $ext;
                $imagePath = $path . "/" . $imageName;
                $companyNameHash = $this->getCompanyImageFolderName();
                $emailImagePath = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . $_SERVER['SERVER_NAME'] . "/userfiles/" . $companyNameHash . "/images/" . $imageName;
                move_uploaded_file($name, $imagePath);
            }
            $body = $post['emailBody'];
            if (isset($imagePath)) {
                $body = $body . "<img src='$emailImagePath'>";
            }


            $this->beginTransaction();
            $entityID = $this->createEntity();
            $promotionEmailData = array(
                'promotionEmailName' => $post['emailPromotionName'],
                'promotionEmailSubject' => $post['emailPromotionSubject'],
                'promotionEmailContent' => $post['emailBody'],
                'promotionEmailImage' => isset($imageName) ? $imageName : NULL,
                'entityID' => $entityID
            );

            $promotionEmail = new PromotionEmail();
            $promotionEmail->exchangeArray($promotionEmailData);
            $savedPromotionEmailID = $this->CommonTable('Settings\Model\PromotionEmailTable')->save($promotionEmail);
            if ($savedPromotionEmailID) {
                foreach ($customerEvents as $customerEvent) {
                    if ($customerEvent != 0) {
                        $promotionEmailCustomerEventData = array(
                            'promotionEmail' => $savedPromotionEmailID,
                            'promotionEmailCustomerEvent' => $customerEvent,
                        );
                        $promotionEmailCustomerEvent = new \Settings\Model\PromotionEmailCustomerEvent();
                        $promotionEmailCustomerEvent->exchangeArray($promotionEmailCustomerEventData);
                        $savedPromotionEmailCustomerEventId = $this->CommonTable('Settings\Model\PromotionEmailCustomerEventTable')->save($promotionEmailCustomerEvent);
                    }
                }
                foreach ($customers as $customer) {
                    $promotionEmailCustomerData = array(
                        'promotionEmail' => $savedPromotionEmailID,
                        'promotionEmailCustomer' => $customer,
                    );
                    $promotionEmailCustomer = new PromotionEmailCustomer();
                    $promotionEmailCustomer->exchangeArray($promotionEmailCustomerData);
                    $savedPromotionEmailCustomerID = $this->CommonTable('Settings\Model\PromotionEmailCustomerTable')->save($promotionEmailCustomer);
                    $customerEmail = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerAndPrimaryProfileByID($customer)->customerProfileEmail;
                    if ($customerEmail != '') {
                        $this->sendEmail($customerEmail, $post['emailPromotionSubject'], $body, NULL);
                    }
                }
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PROMO_EMAIL_SAVE');
                $this->flashMessenger()->addMessage($this->msg);
                return $this->JSONRespond();
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PROMO_EMAIL_SAVE');
                return $this->JSONRespond();
            }
        }
    }

    public function getEmailPromotionAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $promoEmailID = $request->getPost('promEmailID');
            $promotionEmailDetails = $this->getPromotionEmailDetails($promoEmailID);
            $promoEmailView = new ViewModel($promotionEmailDetails);
            $promoEmailView->setTemplate('settings/promotion/view-promotion-email');
            $promotionEmailHtml = $this->htmlRender($promoEmailView);
            $this->status = TRUE;
            $this->data = $promotionEmailHtml;
            return $this->JSONRespond();
        }
    }

    private function getPromotionEmailDetails($promoEmailID)
    {
        $promotionEmailData = $this->CommonTable("Settings\Model\PromotionEmailTable")->getPromotionEmail($promoEmailID);
        $promotionEmailDetails = array();
        $tempProEm = array();
        $imagepath = "/userfiles/" . $this->getCompanyImageFolderName() . "/images/";
        foreach ($promotionEmailData as $data) {
            $tempProEm['proEmID'] = $data['promotionEmailID'];
            $tempProEm['proEmName'] = $data['promotionEmailName'];
            $tempProEm['proEmSub'] = $data['promotionEmailSubject'];
            $tempProEm['proEmBody'] = $data['promotionEmailContent'];
            if ($data['promotionEmailImage'] != '') {
                $image = $imagepath . $data['promotionEmailImage'];
                $tempProEm['proEmBody'] = $tempProEm['proEmBody'] . "<img src='$image' height='400' width='500'>";
            }
            $locations = isset($promotionEmailDetails['locations']) ? $promotionEmailDetails['locations'] : array();
            $products = isset($promotionEmailDetails['products']) ? $promotionEmailDetails['products'] : array();
            $customers = isset($promotionEmailDetails['customers']) ? $promotionEmailDetails['customers'] : array();
            $customerEvents = isset($promotionEmailDetails['customerEvents']) ? $promotionEmailDetails['customerEvents'] : array();

            if ($data['promotionEmailLocation'] != NULL) {
                $locations[$data['promotionEmailLocation']] = $data['locationName'];
            }
            if ($data['promotionEmailProduct'] != NULL) {
                $products[$data['promotionEmailProduct']] = $data['productName'] . "-" . $data['productCode'];
            }

            if ($data['promotionEmailCustomer'] != NULL) {
                $customers[$data['promotionEmailCustomer']] = $data['customerTitle'] . $data['customerName'];
            }

            if ($data['promotionEmailCustomerEvent'] != NULL) {
                $customerEvents[$data['promotionEmailCustomerEvent']] = $data['customerEventName'];
            }

            $tempProEm['locations'] = $locations;
            $tempProEm['products'] = $products;
            $tempProEm['customers'] = $customers;
            $tempProEm['customerEvents'] = $customerEvents;
            $promotionEmailDetails = $tempProEm;
        }

        return $promotionEmailDetails;
    }

    public function promotionsAction()
    {
        $promotionData = [];
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $promotions = $this->CommonTable('Settings\Model\PromotionTable')->getToDayPromotions(date("Y-m-d"), $locationID);

        foreach ($promotions as $key => $value) {
           $promo = $this->getPromotionDetails($value['promotionID']);
           $promotionData[] = $promo[$value['promotionID']];
           // error_log(json_encode($promo));
        }
        
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_POS_PRODUCT_DETAILS');
        $this->data = ['promotion_details' => $promotionData];
        return $this->JSONRespond();
    }

    public function savePromotionSmsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $customers = $post['customer'];
            $smsBody = trim($post['smsBody']);

            $error_message = '';
            $success_message = '';

            foreach ($customers as $customer) {
                $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerAndPrimaryProfileByID($customer);
                $sendRes = $this->sendSms($customerDetails->customerTelephoneNumber, $smsBody);
                if($sendRes){
                    $success_message .= $customerDetails->customerTitle." ". $customerDetails->customerName.", ";
                }else{
                    $error_message .= $customerDetails->customerTitle." ".$customerDetails->customerName.", ";
                }
            }

            $this->status = true;
            if(strlen($error_message) > 0){
                $error_message = $this->getMessage('ERR_PROMO_SMS_SEND',array($error_message));
            }
            if(strlen($success_message) > 0){
                $success_message = $this->getMessage('SUCC_PROMO_SMS_SEND',array($success_message));
            }
            $this->data = array('error_msg' => $error_message, 'success_msg' => $success_message);
            return $this->JSONRespond();
        }
    }

    public function saveDirectPromotionSmsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            
            $post = $request->getPost();
            $contacts = $post['contacts'];
            $smsBody = trim($post['smsBody']);
            $contactListArr = $post['contactsList'];

            $contactsData = $this->CommonTable('Invoice\Model\ContactsTable')->fetchAll(false);
            
            $contactList = [];
            foreach ($contactsData as $key => $contact) {
                $numbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRelatedNumbersByContactID($contact['contactID']);
                
                if (sizeof($numbers) > 0) {
                    if (array_key_exists(1, $numbers)) {
                        $relateTel = $numbers[1];
                    } elseif (array_key_exists(2, $numbers)) {
                        $relateTel = $numbers[2];
                    } elseif (array_key_exists(3, $numbers)) {
                        $relateTel = $numbers[3];
                    }
                    $contact['tel'] = $relateTel;

                    $contactList[$contact['contactID']] = $contact['tel'];
                }
            }
            

            $error_message = '';
            $success_message = '';

            foreach ($contacts as $contact) {
                
                $tel = $contactList[$contact];
                $sendRes = $this->sendSms($tel, $smsBody);
                if($sendRes){
                    $success_message .= $customerDetails->customerTitle." ". $customerDetails->customerName.", ";
                }else{
                    $error_message .= $customerDetails->customerTitle." ".$customerDetails->customerName.", ";
                }
            }

            if(isset($post['contactsList'])){
                foreach ($contactListArr as $contactListID) {// Loop contact list

                    // Get related contact IDs for relevant contact list id 
                    $contactIDs = $this->CommonTable('Invoice\Model\ContactListContactsTable')->getRelatedNumbersByContactListID($contactListID);
                    foreach($contactIDs as $contactID){ // Loop contact IDs
                        $contactDetails = $this->CommonTable('Invoice\Model\ContactsTable')->getContactById($contactID)->current();
                        
                        $numbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRelatedNumbersByContactID($contactID);

                        if (sizeof($numbers) > 0) {
                            $relateTel = ""; 
                            if (array_key_exists(1, $numbers)) {
                                $relateTel = $numbers[1];
                            } elseif (array_key_exists(2, $numbers)) {
                                $relateTel = $numbers[2];
                            } elseif (array_key_exists(3, $numbers)) {
                                $relateTel = $numbers[3];
                            }
                            
                            $firstName = $contactDetails['firstName'];
                            $lastName = (!empty($contactDetails['lastName'])) ? $contactDetails['lastName'] : '';
                            $fullName = $firstName.' '.$lastName;

                            $sendRes = $this->sendSms($relateTel, $smsBody);
                            if($sendRes){
                                $success_message .= $fullName.", ";
                            }else{
                                $error_message .= $fullName.", ";
                            }

                        }
                    }
                }

            }

            $this->status = true;
            if(strlen($error_message) > 0){
                $error_message = $this->getMessage('ERR_PROMO_SMS_SEND',array($error_message));
            }
            if(strlen($success_message) > 0){
                $success_message = $this->getMessage('SUCC_PROMO_SMS_SEND',array($success_message));
            }
            $this->data = array('error_msg' => $error_message, 'success_msg' => $success_message);
            return $this->JSONRespond();
        }
    }

}
