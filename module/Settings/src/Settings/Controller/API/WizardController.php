<?php

/**
 * @author Sandun Dissanayake <sandun@thinkcube.com>
 * This file contains Wizard related controller functions
 */

namespace Settings\Controller\API;

use Core\Controller\CoreController;
use Settings\Form\UomForm;
use Settings\Model\Uom;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Settings\Model\DisplaySetup;

class WizardController extends CoreController
{
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function checkCompanyAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $company = $this->CommonTable('CompanyTable')->fetchAll();
            $wState = $company->current()->wizardState;

            if ($wState) {
                $this->data = $wState;
                $this->status = true;
                $this->msg = $this->getMessage('ERR_WIZ_DATAROW');
                return $this->JSONRespond();
            } else {
                $this->data = 0;
                $this->status = false;
                $this->msg = $this->getMessage('ERR_WIZ_NODATAROW');
                return $this->JSONRespond();
            }
        }
    }

    public function addUomAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $post = [
                'uomName' => $req->getPost('uomName'),
                'uomAbbr' => $req->getPost('uomAbbr'),
                'uomDecimalPlace' => $req->getPost('uomDecimalPlace'),
                'uomState' => $req->getPost('uomState')
            ];

            $uom = new Uom();
            $form = new UomForm();

            $form->setInputFilter($uom->getInputFilter());
            $form->setData($post);

            $recordFromSameName = $this->CommonTable('Settings/Model/UomTable')->getUomByName($post['uomName']);

            if ($recordFromSameName == null) {
                if ($form->isValid()) {
                    $uom->exchangeArray($post);
                    $this->CommonTable('Settings/Model/UomTable')->saveUom($uom);

                    // get uom list to return to view
                    $uomList = $this->CommonTable('Settings/Model/UomTable')->fetchAll();
                    $htmlOutput = $this->_getListViewHtml($uomList, false);

                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_UOM_ADD');
                    $this->html = $htmlOutput;
                    return $this->JSONRespondHtml();
                } else {
                    // validation error
                    $this->status = false;
                    $this->msg = _($this->getMessage('REQ_UOM_REQUIRE'));
                    return $this->JSONRespond();
                }
            } else {
                // record already exists
                $this->status = false;
                $this->msg = _($this->getMessage('ERR_UOM_EXIST'));
                return $this->JSONRespond();
            }
        }
    }

    private function _getListViewHtml($uomList = array(), $paginated = false)
    {
        $view = new ViewModel(array(
            'uomList' => $uomList,
            'paginated' => $paginated
        ));

        $view->setTerminal(true);
        $view->setTemplate('settings/uom/list.phtml');

        return $view;
    }

    public function updateWizardStateAction()
    {
        try {
            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }

            $state = $request->getPost('state');

            $this->beginTransaction();

            $updateStatus = $this->CommonTable('CompanyTable')->updateWizardState($state);
            if (!$updateStatus) {
                $this->rollback();
                return $this->returnJsonError('Error occoured while updating wizard status');
            }

            $config = $this->getServiceLocator()->get('config');
            if ($state != $config['wizardCompleteState']) { // if not completion state
                $this->user_session->wizardComplete = false;
                $this->user_session->useAccounting = 0;
                $this->commit();
                return $this->returnJsonSuccess(null, 'Wizard status has been successfully updated');
            }

            $defaultSettings['costingSettings'] = $config['default_accounts_settings']['costing_method'];
            $result = $this->getService('AccountService')->enableAccountsModule($defaultSettings);
            if (!$result['status']) {
                $this->rollback();
                return $this->returnJsonError($result['msg']);
            }

            $result = $this->getService('AccountService')->setDefaultGLAccounts($config['default_gl_accounts']);
            if (!$result['status']) {
                $this->rollback();
                return $this->returnJsonError($result['msg']);
            }

            $result = $this->getService('AccountService')->setGLAccountsForDefaultCustomer($config['default_customer_gl_accounts']);
            if (!$result['status']) {
                $this->rollback();
                return $this->returnJsonError($result['msg']);
            }

            $result = $this->getService('AccountService')->setDefaultPaymentFinanceAccounts($config['default_payment_method_finance_accounts']);
            if (!$result['status']) {
                $this->rollback();
                return $this->returnJsonError($result['msg']);
            }

            $result = $this->getService('AccountService')->createDefaultFiscalPeriod($this->userID);
            if (!$result['status']) {
                $this->rollback();
                return $this->returnJsonError($result['msg']);
            }

            $result = $this->getService('ReferenceService')->setDefaultReferences($config['documentReference']);
            if (!$result['status']) {
                $this->rollback();
                return $this->returnJsonError($result['msg']);
            }

            $this->user_session->wizardComplete = true;
            $this->user_session->useAccounting = 1;
            $this->commit();

            return $this->returnJsonSuccess(null, 'Wizard successfully completed');
            
        } catch (\Exception $ex) {
            $this->rollback();
            error_log($ex->getMessage());
            return $this->returnJsonError('Error occoured while updating wizard status');
        }
    }

    public function updateUseAccountingFalgAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $flag = $request->getPost('flag');
            $costingMethod = $request->getPost('costingMethod');

            $FIFOCosting = 0;
            $averageCosting = 0;
            if($costingMethod == "FIFOCosting"){
                $FIFOCosting = 1;
            }else if($costingMethod == "averageCosting"){
                $averageCosting = 1;
            }

            $stateUpdate = $this->CommonTable('CompanyTable')->updateUseAccountingFlag($flag);
            if ($stateUpdate) {
                $this->user_session->useAccounting = $flag;

                $displayData = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll()->current();

                $displaySetupID = 0 ;
                if(!empty($displayData->displaySetupID)){
                    $displaySetupID = $displayData->displaySetupID;
                }

                $dataArray = array(
                    'averageCostingFlag' => $averageCosting,
                    'FIFOCostingFlag' => $FIFOCosting,
                    'displaySetupID' => $displaySetupID,
                );

                $display = new DisplaySetup();
                $display->exchangeArray($dataArray);


                $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->saveCostingMethod($display);

                if($result){
                    $this->status = true;
                    $this->msg = 'Wizard finance account flag and costing method updated successfully..!';
                }else{
                    $this->status = false;
                    $this->msg = 'Failed to save costing method.';    
                }
            }else{
                $this->status = false;
                $this->msg = 'Failed to update wizard finance account falg.';
            }

            return $this->JSONRespond();
        }
    }

    public function checkIsUserLogInFirstTimeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $checkUserLogin = $this->CommonTable('User\Model\UserTable')->checkUserLoginFirstTime($this->user_session->userID);

            if ($checkUserLogin) {
                $this->status = TRUE;
                return $this->JSONRespond();
            } else {
                $this->status = FALSE;
                return $this->JSONRespond();
            }
        }
    }

    public function updateUserLoggedAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $stateUpdate = $this->CommonTable('User\Model\UserTable')->updateUserLog($this->user_session->userID);

            if ($stateUpdate) {
                $this->status = true;
                $this->msg = 'Wizard State updated successfully..!';
                return $this->JSONRespond();
            }
        }
    }

}
