<?php
namespace Settings\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeValue;
use Settings\Form\ItemAttributeForm;
use Zend\View\Model\JsonModel;

class DiscountSchemesController extends CoreController
{

    public function saveAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $respond = $this->getService('DiscountSchemeService')->saveDiscountScheme($req->getPost(), $locationID, $this->userID);
        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
        
    }


    public function getSchemeConditionsAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $respond = $this->getService('DiscountSchemeService')->getSchemeConditions($req->getPost());

        if (!$respond) {
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }

        $this->status = true;
        $this->data = array('list' => $respond); 
        return $this->JSONRespond();
        
    }

    public function deleteSchemeAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('DiscountSchemeService')->deleteScheme($req->getPost(), $this->userID);
        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    public function searchAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $respond = $this->getService('DiscountSchemeService')->searchDiscountScheme($req->getPost());

        if (!$respond['status']) {
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }

        $view = new ViewModel(array(
            'schemeList' => $respond['schemeList'],
            'paginated' => $respond['paginated']
        ));
        
        $view->setTerminal(true);
        $this->status = true;
        $view->setTemplate('settings/discount-schemes/list');
        $this->html = $view;
        return $this->JSONRespondHtml();
        
    }


    public function searchDiscountSchemeForDropdownAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $respond = $this->getService('DiscountSchemeService')->searchDiscountSchemeForDropdown($req->getPost());

        if (!$respond) {
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }

        $this->data = array('list' => $respond); 
        return $this->JSONRespond();
        
    }

    
}
