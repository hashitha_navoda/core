<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains TaxAPI related controller functions
 */

namespace Settings\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Core\Controller\CoreController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Settings\Form\TaxForm;
use Settings\Model\Tax;
use Settings\Model\Company;
use Settings\Model\TaxCompound;

class TaxAPIController extends CoreController
{

    public function addTaxAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $post_array['id'] = $post['tax_id'];
            $post_array['taxName'] = $post['taxName'];
            $post_array['taxType'] = (!empty($post['taxType'])) ? "c" : "v";
            $post_array['taxPrecentage'] = $post['taxPrecentage'];
            $post_array['taxSuspendable'] = $post['taxSuspendable'];
            $post_array['taxSalesAccountID'] = $post['taxSalesAccountID'];
            $post_array['taxPurchaseAccountID'] = $post['taxPurchaseAccountID'];
            $checkTax = $this->CommonTable('Settings\Model\TaxTable')->getTaxByName($post_array['taxName']);
            if ($checkTax != NULL) {
                $this->msg = $this->getMessage('ERR_TAXAPI_EXIST');
                $this->status = false;
                return $this->JSONRespond();
            } else {

                $tax = new Tax();
                $tax->exchangeArray($post_array);
                $result = $this->CommonTable('Settings\Model\TaxTable')->saveTax($tax);
                if ($post_array['taxType'] == 'c') {
                    $taxCompound = new TaxCompound();
                    foreach ($post['compoundTax'] as $taxID) {
                        $data = array(
                            'compoundTaxID' => $result,
                            'simpleTaxID' => $taxID
                        );
                        $taxCompound->exchangeArray($data);
                        $saveResult = $this->CommonTable('Settings\Model\TaxCompoundTable')->saveCompoundTax($taxCompound);
                    }
                }
                $tax_result = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
                while ($row = $tax_result->current()) {
                    $taxes[] = array(
                        'taxID' => $row->id,
                        'taxName' => $row->taxName,
                        'taxType' => $row->taxType,
                        'taxPrecentage' => $row->taxPrecentage,
                        'state' => $row->state
                    );
                }

                $taxListView = new ViewModel(array(
                    'taxes' => (isset($taxes)) ? $taxes : NULL
                ));
                $taxListView->setTemplate('settings/tax/tax-list');

                $this->status = true;
                $this->msg = $this->getMessage('SUCC_TAXAPI_ADD');
//                $this->flashMessenger()->addMessage($this->msg);
                return $this->JSONRespond();
            }
        }
    }

    public function updateTaxAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $post_array['id'] = $post['tax_id'];
            $post_array['taxName'] = $post['taxName'];
            $post_array['taxType'] = (!empty($post['taxType'])) ? "c" : "v";
            $post_array['taxPrecentage'] = $post['taxPrecentage'];
            $post_array['taxSuspendable'] = $post['taxSuspendable'];
            $post_array['taxSalesAccountID'] = $post['taxSalesAccountID'];
            $post_array['taxPurchaseAccountID'] = $post['taxPurchaseAccountID'];
            $tax = new Tax();
            $tax->exchangeArray($post_array);
            $data = array(
                'id' => $tax->id,
                'taxName' => $tax->taxName,
                'taxType' => $tax->taxType,
                'taxPrecentage' => $tax->taxPrecentage,
                'taxSuspendable' => $tax->taxSuspendable,
                'taxSalesAccountID' => $tax->taxSalesAccountID,
                'taxPurchaseAccountID' => $tax->taxPurchaseAccountID,
            );
            $this->CommonTable('Settings\Model\TaxTable')->updateTax($data, $post_array['id']);
            if ($post_array['taxType'] == 'v') {
                $this->CommonTable('Settings\Model\TaxCompoundTable')->deleteCompoundTax($post_array['id']);
            }
            if ($post_array['taxType'] == 'c') {
                $this->CommonTable('Settings\Model\TaxCompoundTable')->deleteCompoundTax($post_array['id']);
                $taxCompound = new TaxCompound();
                foreach ($post['compoundTax'] as $taxID) {
                    $data = array(
                        'compoundTaxID' => $post_array['id'],
                        'simpleTaxID' => $taxID
                    );
                    $taxCompound->exchangeArray($data);
                    $saveResult = $this->CommonTable('Settings\Model\TaxCompoundTable')->saveCompoundTax($taxCompound);
                }
            }
            $tax_result = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
            while ($row = $tax_result->current()) {
                $taxes[] = array(
                    'taxID' => $row->id,
                    'taxName' => $row->taxName,
                    'taxType' => $row->taxType,
                    'taxPrecentage' => $row->taxPrecentage,
                    'state' => $row->state
                );
            }

            $taxListView = new ViewModel(array(
                'taxes' => (isset($taxes)) ? $taxes : NULL
            ));
            $taxListView->setTemplate('settings/tax/tax-list');
            $this->msg = $this->getMessage('SUCC_TAXAPI_UPDATE');
            $this->status = true;
            $this->html = $taxListView;
            return $this->JSONRespondHtml();
        }
    }

    public function changeTaxStateAction()
    {
        $request = $this->getRequest();
        $data = array(
            'state' => $request->getPost('state'),
        );

        $this->CommonTable('Settings\Model\TaxTable')->updateTax($data, $request->getPost('id'));
        $this->msg = $this->getMessage('SUCC_TAXAPI_CHANGESTATUS');
        $this->status = true;
        return $this->JSONRespond();
    }

    public function updateTRnumberAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $TRnumber = $request->getPost('TRnumber');
            if ($TRnumber == '' || $TRnumber == null) {
                $tax = $this->CommonTable('CompanyTable')->getTRNumber();
                $oldTRnumber = $tax->oldTaxRegNumber;
            } else {
                $oldTRnumber = $TRnumber;
            }
            $company = new Company();
            $companyID = $this->CommonTable('CompanyTable')->getCompany()->id;
            $company->exchangeArray(array(
                'id' => $companyID,
                'taxRegNumber' => $TRnumber,
                'oldTaxRegNumber' => $oldTRnumber,
            ));
            $res = $this->CommonTable('CompanyTable')->updateTRNumber($company);
            if ($res == 1) {
                $this->msg = $this->getMessage('SUCC_TAXAPI_ENABLE');
                $this->status = 'success';
                $this->data = true;
            } else {
                $this->msg = $this->getMessage('ERR_TAXAPI_SAMENO');
                $this->status = 'info';
                $this->data = true;
            }

            return $this->JSONRespond();
        } else {
            $this->msg = $this->getMessage('ERR_TAXAPI_ENABLE');
            $this->status = 'error';
            return $this->JSONRespond();
        }
    }

    public function getTaxAction()
    {
        if ($this->isTaxRegistered()) {
            $request = $this->getRequest();
            $taxDetails = $this->CommonTable('Settings\Model\TaxTable')->getTax($request->getPost('id'));
            $taxDetailsArray = [];
            $CompoundTaxDetailsArray = [];
            $financeAccountsArray = $this->getFinanceAccounts();

            foreach ($taxDetails as $row) {
                $taxDetailsArray['tN'] = $row['taxName'];
                $taxDetailsArray['tT'] = $row['taxType'];
                $taxDetailsArray['tP'] = $row['taxPrecentage'];
                $taxDetailsArray['uT'] = $row['usedTax'];
                $taxDetailsArray['tS'] = $row['taxSuspendable'];
                $taxDetailsArray['tSAID'] = $row['taxSalesAccountID'];
                $taxDetailsArray['tSAN'] = $financeAccountsArray[$row['taxSalesAccountID']]['financeAccountsCode']."_".$financeAccountsArray[$row['taxSalesAccountID']]['financeAccountsName'];
                $taxDetailsArray['tPAID'] = $row['taxPurchaseAccountID'];
                $taxDetailsArray['tPAN'] = $financeAccountsArray[$row['taxPurchaseAccountID']]['financeAccountsCode']."_".$financeAccountsArray[$row['taxPurchaseAccountID']]['financeAccountsName'];
                $CompoundTaxDetailsArray[] = $row['simpleTaxID'];
            }
            $taxDetailsArray['cT'] = $CompoundTaxDetailsArray;
            return new JsonModel($taxDetailsArray);
        } else {
            return new JsonModel(NULL);
        }
    }

    public function deleteTaxAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $taxID = $request->getPost('taxID');
            $tax = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxID);
            if ($tax['taxType'] = 'c') {
                $tax = $this->CommonTable('Settings\Model\TaxCompoundTable')->deleteCompoundTax($taxID);
            }
            $result = $this->CommonTable('Settings\Model\TaxTable')->deleteTax($taxID);
            if ($result == 1) {
                $tax_result = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
                while ($row = $tax_result->current()) {
                    $taxes[] = array(
                        'taxID' => $row->id,
                        'taxName' => $row->taxName,
                        'taxType' => $row->taxType,
                        'taxPrecentage' => $row->taxPrecentage,
                        'state' => $row->state
                    );
                }

                $taxListView = new ViewModel(array(
                    'taxes' => (isset($taxes)) ? $taxes : NULL
                ));
                $taxListView->setTemplate('settings/tax/tax-list');
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_TAXAPI_DELE');
                $this->html = $taxListView;
                return $this->JSONRespondHtml();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_TAXAPI_DELE');
                return $this->JSONRespond();
            }
        }
    }

    public function getTaxAmountAction()
    {
        if ($this->isTaxRegistered()) {
            $req = $this->getRequest();
            $post = $req->getPost();
            $pro_taxes = $this->CommonTable('Invoice\Model\ProductTaxTable')->getProductTaxes($post["item_id"]);
            $taxObject = $this->CommonTable('Invoice\Model\ProductTaxTable')->getProductTaxes($post["item_id"]);

            $taxArray = array(FALSE);
            while ($r = $taxObject->current()) {
                $taxArray[] = $r->taxID;
            }
            $taxes = array(FALSE);
            while ($row = $pro_taxes->current()) {
                $tax_dtl_set = $this->CommonTable('Settings\Model\TaxTable')->getTax($row->taxID);
                if ($tax_dtl_set->taxType == 'v') {
                    $taxes[$tax_dtl_set->taxName] = $post['price'] * $tax_dtl_set->taxPrecentage / 100;
                } else if ($tax_dtl_set->taxType == 'c') {
                    $cmp_tx = explode(",", $tax_dtl_set->compoundTax);
                    $tx_val = 0;
                    foreach ($cmp_tx as $tx) {
                        if (!empty($tx)) {
                            $tx_dtls = $this->CommonTable('Settings\Model\TaxTable')->getTax($tx);
                            if ($tx_dtls->taxType == 'v' && in_array($tx_dtls->id, $taxArray)) {
                                $tx_val += $post['price'] * $tx_dtls->taxPrecentage / 100;
                            }
                        }
                    }
                    $price_with_tax = $post['price'] + $tx_val;
                    $taxes[$tax_dtl_set->taxName] = $price_with_tax * $tax_dtl_set->taxPrecentage / 100;
                }
            }
            return new JsonModel($taxes);
        } else {
            return new JsonModel(array());
        }
    }

    public function getCustomTaxAmountAction()
    {
        $req = $this->getRequest();
        $post = $req->getPost();
        $taxes = array(FALSE);
        if (sizeof($post['checkedTaxes']) > 0) {
            $taxArray = $post['checkedTaxes'];
            foreach ($post['checkedTaxes'] as $tax) {
                $tax_dtl_set = $this->CommonTable('Settings\Model\TaxTable')->getTaxByName($tax);
                if ($tax_dtl_set->taxType == 'v') {
                    $taxes[$tax_dtl_set->taxName] = $post['price'] * $tax_dtl_set->taxPrecentage / 100;
                } else if ($tax_dtl_set->taxType == 'c') {
                    $cmp_tx = explode(",", $tax_dtl_set->compoundTax);
                    $tx_val = 0;
                    foreach ($cmp_tx as $tx) {
                        if (!empty($tx)) {
                            $tx_dtls = $this->CommonTable('Settings\Model\TaxTable')->getTax($tx);
                            if ($tx_dtls->taxType == 'v' && in_array($tx_dtls->taxName, $taxArray)) {
                                $tx_val += $post['price'] * $tx_dtls->taxPrecentage / 100;
                            }
                        }
                    }
                    $price_with_tax = $post['price'] + $tx_val;
                    $taxes[$tax_dtl_set->taxName] = $price_with_tax * $tax_dtl_set->taxPrecentage / 100;
                }
            }
        }
        return new JsonModel($taxes);
    }

    public function getTaxRatesAction()
    {
        $tx_rates = $this->CommonTable('Settings\Model\TaxTable')->getTaxRates();
        if ($tx_rates) {
            while ($tx = $tx_rates->current()) {
                $tx_array[$tx->taxName] = $tx->taxPrecentage;
            }
            return new JsonModel($tx_array);
        }
        return new JsonModel(array(FALSE));
    }

    public function isTaxRegistered()
    {
        $tr_number = $this->CommonTable('CompanyTable')->getTRNumber();
        return isset($tr_number->trNumber) ? TRUE : FALSE;
    }

}

/////////////////// END OF TAXAPI CONTROLLER \\\\\\\\\\\\\\\\\\\\