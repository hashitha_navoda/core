<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains CompanyAPI related controller functions
 */

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Settings\Model\Company;
use Settings\Model\Reference;
use Settings\Model\ReferencePrefix;
use Settings\Form\CompanyForm;
use Settings\Model\DisplaySetup;
use Invoice\Model\ProductCategory;
use Settings\Form\DisplayForm;
use Invoice\Form\UnitMeasureForm;
use Invoice\Form\ProductCategoryForm;
use Invoice\Model\UnitMeasure;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use Core\Model\Currency;

/**
 * comapanyApI class create
 */
class CompanyAPIController extends CoreController
{

    protected $userID;
    protected $username;
    protected $company;
    protected $user_session;

    /**
     * compamy details form is submit by this method
     * @return \Zend\View\Model\JsonModel
     */
    public function indexAction()
    {
        $request = $this->getRequest();
        //check whether request is post otherwise send error message
        if ($request->isPost()) {
            //create company form
            $form = new CompanyForm();
            //create company model
            $company = new Company();
            //set input filter for compay form
            $form->setInputFilter($company->getInputFilter());
            //set post data to company form
            $form->setData($request->getPost());
            //check wheter form is valid, if it valida save the company details otherwise send erro message
            if ($form->isValid()) {
                // Get company current licence Count and store as it as
                $companyDetails = $this->CommonTable('CompanyTable')->getCompanyDetails();
                // if not inserted into table or first time get from licence config file
                $config = $this->getServiceLocator()->get('config');
                $licenceCount = isset($companyDetails[0]['licenceCount']) ? $companyDetails[0]['licenceCount'] : $config['licence']['licenceCount'];
                $request->getPost()['licenceCount'] = $licenceCount;

                //pass data to saveCompany function for add data to database
                $companyData = array_intersect_key($request->getPost()->toArray(), array_flip(array(
                    'companyName',
                    'companyAddress',
                    'telephoneNumber',
                    'telephoneNumber2',
                    'telephoneNumber3',
                    'faxNumber',
                    'postalCode',
                    'email',
                    'country',
                    'website',
                    'brNumber'
                )));

                $this->CommonTable('CompanyTable')->updateCompanyDetails($companyData);
                $languages = $this->getLanguageForSelectedCountry();
                $this->user_session->userLanguages = $languages;
                $this->setLogMessage('Company details saved ');
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_COMAPI_SAVE');
                $this->data = null;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_COMAPI_SAVECOM');
            }
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_COMAPI_NOTPOST');
        }
        return $this->JSONRespond();
    }

    //dispaly details save in here
    public function displayAddAction()
    {
        $request = $this->getRequest();
        //check whether request is post, if request is post save display data otherwise return error message
        if ($request->isPost()) {

            $requestData = $request->getPost();

//            $config = $config = $this->getServiceLocator()->get('config');
//            $dateformats = $config['dateformats'];

            if ($requestData->customerCreditLimitApplyFlag == '') {
                $requestData->customerCreditLimitApplyFlag = 0;
            }

            if ($requestData->posPrintoutImageConversionStatus == '') {
                $requestData->posPrintoutImageConversionStatus = 0;
            }

//creata display setup form
            $form = new DisplayForm($data, $dateformats);
//create display setup model
            $display = new DisplaySetup();
            //set input filter to displays setup form
            $form->setInputFilter($display->getInputFilter());

//           User cant update the system currencyID
            $form->getInputFilter()->remove('currencyID');

            //set post data to form variables
            $form->setData($requestData);

            $displayDetails = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
            if (!$displayDetails) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_COMAPI_SAVEDISPLAY');
            }
            // get current costing method
            $currentCostingMethod = ($displayDetails['averageCostingFlag']) ? 'averageCosting' : 'FIFOCosting';
            // check whether item in to the system
            $hasItemIn = $this->CommonTable('Inventory\Model\ItemInTable')->hasItemIn();

            if ($hasItemIn && ($currentCostingMethod != $requestData['costingMethod'])) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_COMAPI_COSTING_METHOD');
                return $this->JSONRespond();
            }
        
//check whether form is valid if form is valida call to saveDisplayStup function otherwise return error message.
            if ($form->isValid()) {
//set post data to exchange array
                $display->exchangeArray($request->getPost());
                if($requestData['costingMethod'] == 'averageCosting'){
                    $display->averageCostingFlag = 1;
                }else if($requestData['costingMethod'] == 'FIFOCosting'){
                    $display->FIFOCostingFlag = 1;
                }
//pass display setup data to saveDisplaySetup function.
                $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->saveDisplaySetup($display);
//check whether dispaly data saved successfully. if saved successfully return success message otherwise send error message.
                if ($result) {
                    $this->setLogMessage('Display details saved ');
//set session variable company currencySymbol and timeZone
                    $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
                    $data = $result->current();
                    $currencyId = $data['currencyID'];

                    $datas = array(
                        'currencyID' => $currencyId,
                        'currencyRate' => 1,
                    );
                    $currency = new Currency();
                    $currency->exchangeArray($datas);
                    $status = $this->CommonTable('Core\Model\CurrencyTable')->updateCurrency($currency);

                    $this->user_session->companyCurrencySymbol = $data['currencySymbol'];
                    $this->user_session->timeZone = $data['timeZone'];
                    $this->user_session->dateFormat = $data['dateFormat'];    ////////////////////////////
                    $this->status = True;
                    $this->msg = $this->getMessage('SUCC_COMAPI_DISPLAY');
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_COMAPI_SAVEDISPLAY');
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PRDCTAPI_OCCUR');
            }
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_COMAPI_REQ');
        }
        return $this->JSONRespond();
    }

//update reference table details
    public function prefixUpdateAction()
    {
        $request = $this->getRequest();
//check whether request is post
        if ($request->isPost()) {
            $data = $request->getPost('prefixData');

            $rr = 0;
            $this->beginTransaction();
            foreach ($data as $key) {
//check wheter data array element keys are null or not null
                if (json_decode($key)->id != '') {
                    $id = json_decode($key)->id;
                    $referenceName = json_decode($key)->referenceName;
                    $locationID = json_decode($key)->locationID;
                    $type = json_decode($key)->type;
                    $digits = json_decode($key)->digits;
                    $currentnumber = json_decode($key)->nextNumber;
                    $localGlobleType = json_decode($key)->localGlobleType;

//get reference by reference id and location id
                    $rresult = $this->CommonTable('Settings\Model\ReferencePrefixTable')->getReference($id, $locationID);
                    $locationReferenceID = (isset($rresult)) ? $rresult->locationReferenceID : '';
                    $referenceID = (isset($rresult)) ? $rresult->referenceID : '';

//check whether this prefix and digits use for anoter prefix type.
                    $checkReference = $this->CommonTable('Settings\Model\ReferencePrefixTable')->getReferenceByPrefixCharAndReferenceDigits($type, $digits, $id);

                    if ($checkReference->count() > 0) {
                        $this->rollback();
                        $this->status = FALSE;
                        $this->msg = $this->getMessage('ERR_REF_USED_IN_ANOTHER_MODULE', array($type, $digits));
                        return $this->JSONRespond();
                    }

//check whether reference already defined or not
                    if ($rresult) {
                        $string = sprintf("%0" . $digits . "d", $currentnumber);
                        $referenceNumber = $type . $string;
                        $tname = '';
                        $tcolum = '';
                        $location = '';
                        $nameType = '';
                        switch ($referenceID) {
                            case 1:
                                $tname = 'quotation';
                                $tcolum = 'quotationCode';
                                $location = 'quotationBranch';
                                $nameType = 'Quotation';
                                break;

                            case 2:
                                $tname = 'salesOrders';
                                $tcolum = 'soCode';
                                $location = 'branch';
                                $nameType = 'Sales Order';
                                break;

                            case 3:
                                $tname = 'salesInvoice';
                                $tcolum = 'salesInvoiceCode';
                                $location = 'locationID';
                                $nameType = 'Sales Invoice';
                                break;

                            case 4:
                                $tname = 'incomingPayment';
                                $tcolum = 'incomingPaymentCode';
                                $location = 'locationID';
                                $nameType = 'Payment';
                                break;

                            case 5:
                                $tname = 'incomingPayment';
                                $tcolum = 'incomingPaymentCode';
                                $location = 'locationID';
                                $nameType = 'Advance payment';
                                break;

                            case 6:
                                $tname = 'deliveryNote';
                                $tcolum = 'deliveryNoteCode';
                                $location = 'deliveryNoteLocationID';
                                $nameType = 'Delivery Note';
                                break;
                            case 7:
                                $tname = 'salesReturn';
                                $tcolum = 'salesReturnCode';
                                $location = 'locationID';
                                $nameType = 'Sales Returns';
                                break;
                            case 8:
                                $tname = 'creditNote';
                                $tcolum = 'creditNoteCode';
                                $location = 'locationID';
                                $nameType = 'Credit Note';
                                break;
                            case 9:
                                $tname = 'purchaseOrder';
                                $tcolum = 'purchaseOrderCode';
                                $location = 'purchaseOrderRetrieveLocation';
                                $nameType = 'Purchase order';
                                break;

                            case 10:
                                $tname = 'grn';
                                $tcolum = 'grnCode';
                                $location = 'grnRetrieveLocation';
                                $nameType = 'Grn';
                                break;
//TODO after creating debit note belove comment should be remove and edit
//                            case 11:
//                                $tname = 'debitNote';
//                                $tcolum = 'grnCode';
//                                $location = 'grnRetrieveLocation';
//                                  $nameType = 'Debit note';
//                                break;
                            case 12:
                                $tname = 'goodsIssue';
                                $tcolum = 'goodsIssueCode';
                                $location = 'locationID';
                                $nameType = 'Adjusment';
                                break;
                            case 13:
                                $tname = 'outgoingPayment';
                                $tcolum = 'outgoingPaymentCode';
                                $location = 'locationID';
                                $nameType = 'Supplier payment';
                                break;
                            case 14:
                                $tname = 'purchaseInvoice';
                                $tcolum = 'purchaseInvoiceCode';
                                $location = 'purchaseInvoiceRetrieveLocation';
                                $nameType = 'Purchase Invoice';
                                break;
                            case 16:
                                $tname = 'transfer';
                                $tcolum = 'transferCode';
                                $location = 'locationIDOut';
                                $nameType = 'Transfer';
                                break;
//                            case 17:
//                                $tname = 'creditNotePaymentDetails';
//                                $tcolum = 'paymentMethodID';
//                                $location = 'LocationID';
//                                $nameType = 'Credit Note Payments';

                            case 20:
                                $tname = 'customer';
                                $tcolum = 'customerCode';
                                $location = 'locationID';
                                $nameType = 'Customer';
                                break;
                            case 21:
                                $tname = 'project';
                                $tcolum = 'projectCode';
                                $location = 'locationId';
                                $nameType = 'Project Reference';
                                break;
                            case 22:
                                $tname = 'job';
                                $tcolum = 'jobReferenceNumber';
                                $location = 'locationID';
                                $nameType = 'Job Reference';
                                break;
                            case 23:
                                $tname = 'activity';
                                $tcolum = 'activityCode';
                                $location = 'locationID';
                                $nameType = 'Activity Reference';
                                break;
                            case 24:
                                $tname = 'inquiryLog';
                                $tcolum = 'inquiryLogReference';
                                $location = 'locationID';
                                $nameType = 'Inquiry Log Reference';
                                break;
                            case 25:
                                $tname = 'salesInvoice';
                                $tcolum = 'salesInvoiceCode';
                                $location = 'locationID';
                                $nameType = 'Sales Invoice';
                                break;
                            case 27:
                                $tname = 'pettyCashVoucher';
                                $tcolum = 'pettyCashVoucherNo';
                                $location = 'locationID';
                                $nameType = 'Petty Cash Voucher';
                                break;
                            case 29:
                                $tname = 'dispatchNote';
                                $tcolum = 'dispatchNoteCode';
                                $location = 'locationID';
                                $nameType = 'Dispatch Note';
                                break;
                            case 30:
                                $tname = 'journalEntry';
                                $tcolum = 'journalEntryCode';
                                $location = 'locationID';
                                $nameType = 'Journal Entry';
                                break;
                            case 31:
                                $tname = 'journalEntry';
                                $tcolum = 'journalEntryCode';
                                $location = 'locationID';
                                $nameType = 'Journal Entry Template';
                                break;
                            case 34:
                                $tname = 'supplier';
                                $tcolum = 'supplierCode';
                                $location = 'locationID';
                                $nameType = 'Supplier';
                                break;
                            case 39:
                                $tname = 'purchaseRequisition';
                                $tcolum = 'purchaseRequisitionCode';
                                $location = 'locationID';
                                $nameType = 'Purchase Requisition';
                                break;
                            case 42:
                                $tname = 'income';
                                $tcolum = 'incomeCode';
                                $location = 'incomeLocationID';
                                $nameType = 'Income';
                                break;
                            
                        }
//if edited refrence related number alredy have then given error message
                        if ($referenceID == 15) {
                            $ldata = $this->CommonTable('Core\Model\CoreTable')->checkReferenceNumberForPurchaseReturns($referenceNumber, $locationID);
                        } else if ($referenceID == 11) {
                            $ldata = false;
                        } else if ($referenceID == 17) {
                            $ldata = false;
                        } else if ($referenceID == 18) {
                            $ldata = false;
                        } else if ($referenceID == 26) {
                            $ldata = false;
                        } else if ($referenceID == 33) {
                            $ldata = false;
                        } else if ($referenceID == 20) {
                            $ldata = false;
                        } else {
                            $ldata = $this->CommonTable('Core\Model\CoreTable')->checkReferenceNumber($tname, $tcolum, $location, $referenceNumber, $locationID);
                        }

                        if ($ldata) {
                            $this->rollback();
                            $this->status = false;
                            if ($locationID) {
                                $this->msg = $this->getMessage('ERR_COMAPI_SAVELOC', array($referenceNumber, $nameType, $ldata['locationName']));
//$this->msg = $referenceNumber . ' This ' . $nameType . ' number already used in ' . $ldata['locationName'] . ' location';
                            } else {
                                $this->msg = $this->getMessage('ERR_COMAPI_NOUSED', array($referenceNumber, $nameType));
//$this->msg = $referenceNumber . ' This ' . $nameType . ' number already used';
                            }
                            return $this->JSONRespond();
                        }

//update the reference details
                        $data = array(
                            'referencePrefixCHAR' => $type,
                            'referencePrefixNumberOfDigits' => $digits,
                            'referencePrefixCurrentReference' => $currentnumber,
                        );
                        $data2 = array(
                            'referenceTypeID' => $localGlobleType,
                            'referenceName' => $referenceName,
                        );
                        $result = $this->CommonTable('Settings\Model\ReferencePrefixTable')->updateReferenceByID($data, $locationReferenceID);
                        $setTemp = $this->CommonTable('Settings\Model\ReferenceTable')->updateReferenceByID($data2, $id);
                        if ($result != true) {
                            $rr+=1;
                        } else {
                            $this->setLogMessage('Reference sucssesfully updated ' . $type, $id);
                        }
                    } else {
//                        $referenceRow = $this->CommonTable('Settings\Model\ReferenceTable')->fetchAll();
//save the refrence details

                        $data = array(
                            'referencePrefixCHAR' => $type,
                            'locationID' => $locationID,
                            'referenceNameID' => $id,
                            'referencePrefixNumberOfDigits' => $digits,
                            'referencePrefixCurrentReference' => $currentnumber,
                        );
                        $data2 = array(
                            'referenceTypeID' => $localGlobleType,
                            'referenceName' => $referenceName,
                            'referenceNameID' => $id,
                        );
                        $data3 = array(
                            'referenceTypeID' => $localGlobleType,
                            'referenceName' => $referenceName,
                        );
                        //set reference exchange array

                        $reference = new Reference();
                        $reference->exchangeArray($data2);

                        $referencePrefix = new ReferencePrefix();
                        $referencePrefix->exchangeArray($data);


                        $check = $this->CommonTable('Settings\Model\ReferenceTable')->getReference($reference);
                        if (!$check) {
                            $result = $this->CommonTable('Settings\Model\ReferenceTable')->saveReference($reference);
                        } else {
                            $setTemp = $this->CommonTable('Settings\Model\ReferenceTable')->updateReferenceByID($data3, $id);
                        }
                        $result1 = $this->CommonTable('Settings\Model\ReferencePrefixTable')->saveReference($referencePrefix);

                        if ($result != true) {
                            $rr+=1;
                        } else {
                            $this->setLogMessage('Reference sucssesfully saved ' . $type);
                        }
                    }
                }
            }
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_COMAPI_ADD');
//            if ($rr == 0) {
//                $this->status = true;
//                $this->msg = $this->getMessage('SUCC_COMAPI_ADD');
//            } else {
//                $this->status = false;
//                $this->msg = $this->getMessage('ERR_COMAPI_REFADD');
//            }
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_COMAPI_REQ');
        }
        return $this->JSONRespond();
    }

    public function getGlobalLocalTypeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $referenceID = $request->getPost('referenceID');
            $data = $this->CommonTable('Settings\Model\ReferenceTable')->getReferenceType($referenceID);
            $this->status = true;
            $this->data = $data;
            return $this->JSONRespond();
        }
    }

//get location related reference data
    public function getLocationPrefixDataAction()
    {
        $request = $this->getRequest();
//check whether request is post request
        if ($request->isPost()) {
            $referenceID = $request->getPost('referenceID');
            //get the reference by reference id
            $result = $this->CommonTable('Settings\Model\ReferencePrefixTable')->selectAllData($referenceID);
//            $result = $this->CommonTable('Settings\Model\ReferenceTable')->getReferenceByReferenceID($referenceID);
            $locationReference = [];
            foreach ($result as $r) {
                $locationReference[$r[locationID]] = $r;
//                $locationReference[$r->locationID] = $r;
            }


            $this->status = true;
            $this->msg = $this->getMessage('SUCC_COMAPI_LREFACCESS');
            $this->data = $locationReference;
        } else {
            $this->status = true;
            $this->msg = $this->getMessage('ERR_PRDCTAPI_OCCUR');
        }
        return $this->JSONRespond();
    }

    public function getGlobalPrefixDataAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $referenceID = $request->getPost('referenceID');
            //get the reference by reference id
            $result = $this->CommonTable('Settings\Model\ReferencePrefixTable')->getReferenceByReferenceID($referenceID);
//            $result = $this->CommonTable('Settings\Model\ReferenceTable')->getReferenceByReferenceID($referenceID);
            $locationReference = [];

            foreach ($result as $r) {
                $locationReference[1] = $r;
            }
            if ($locationReference) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_COMAPI_LREFACCESS');
                $this->data = $locationReference;
            } else {
                $locationReference[1]['referencePrefixID'] = null;
                $locationReference[1]['referenceNameID'] = null;
                $locationReference[1]['locationID'] = null;
                $locationReference[1]['referencePrefixCHAR'] = null;
                $locationReference[1]['referencePrefixNumberOfDigits'] = null;
                $locationReference[1]['referencePrefixCurrentReference'] = null;

                $this->status = true;
                $this->msg = $this->getMessage('SUCC_COMAPI_LREFACCESS');
                $this->data = $locationReference;
            }
        } else {
            $this->status = true;
            $this->msg = $this->getMessage('ERR_PRDCTAPI_OCCUR');
        }
        return $this->JSONRespond();
    }

    public function getLocationPrefixDataForPreviewAction()
    {
        $request = $this->getRequest();
//check whether request is post request
        if ($request->isPost()) {
            $referenceID = $request->getPost('referenceID');
            //get the reference by reference id
            for ($i = 1; $i < 44; $i++) {
                $rID = $i;
                $result = $this->CommonTable('Settings\Model\ReferencePrefixTable')->getReferenceByReferenceID($rID);
                $resultt = [];
                foreach ($result as $a) {
                    $resultt[$a[referenceNameID]] = $a;
                }
                $value[$i] = $resultt;
            }
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_COMAPI_LREFACCESS');
            $this->data = $value;
        } else {
            $this->status = true;
            $this->msg = $this->getMessage('ERR_PRDCTAPI_OCCUR');
        }
        return $this->JSONRespond();
    }

//get locatiion related reference data
    public function locationRelatedPrefixAction()
    {
        $request = $this->getRequest();
//check whether reqest is post request
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
//get all reference by location id
            $result = $this->CommonTable('Settings\Model\ReferenceTable')->getReferenceByLocationID($locationID);
            $locationReference = [];
            foreach ($result as $lref) {
                $locationReference[$lref->referenceID] = $lref->referenceType . ' - ' . $lref->currentReference;
            }
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_COMAPI_LREFACCESS');
            $this->data = $locationReference;
        } else {
            $this->status = true;
            $this->msg = $this->getMessage('ERR_PRDCTAPI_OCCUR');
        }
        return $this->JSONRespond();
    }

    /**
     * image upload action
     * @return \Zend\View\Model\ViewModel
     */
    public function previewAction()
    {
        $company = $this->CommonTable('CompanyTable')->getCompany();
        $companyID = $company->id;

//get existing company image name
        $result = $this->CommonTable('CompanyTable')->getImageUrl($companyID);

        $previewImage = $this->getCompanyDataFolder() . "/company/thumbs/" . $result['logoID'];

//get existing preview image name
        $imageFile = explode("_preview.", $result['logoID']);
        $originalImage = $this->getCompanyDataFolder() . "/company/" . $imageFile[0] . "." . $imageFile[1]; //get existing original image name
//remove any image file that already exists with the same name
        if (file_exists($previewImage)) {
            @unlink($previewImage);
        }
        if (file_exists($originalImage)) {
            @unlink($originalImage);
        }
        $imgname = $this->previewImage("/company", $companyID);

        $data = array(
            'logoID' => $imgname,
        );
        $this->CommonTable('CompanyTable')->updateImageUrl($data, $companyID);
        return new JsonModel(array($imgname));
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * Return company Details
     */
    public function getCompanyDetailsAction()
    {
        $companyDetails = $this->CommonTable('CompanyTable')->getCompany(1);
        if ($companyDetails) {
            $companyData = array(
                'companyName' => $companyDetails->companyName,
                'companyAddress' => $companyDetails->companyAddress,
                'postalCode' => $companyDetails->postalCode,
                'telephoneNumber' => $companyDetails->telephoneNumber,
                'email' => $companyDetails->email,
                'country' => $companyDetails->country,
                'website' => $companyDetails->website,
                'brNumber' => $companyDetails->brNumber,
                'trNumber' => $companyDetails->trNumber,
                'logoID' => $companyDetails->logoID,
                'wizardState' => $companyDetails->wizardState,
                'companyGracePeriod' => $companyDetails->companyGracePeriod,
                'companyAccountType' => $companyDetails->companyAccountType,
                'userGracePeriod' => $companyDetails->userGracePeriod,
                'companyLicenseExpireDate' => $companyDetails->companyLicenseExpireDate,
            );
            return new JsonModel($companyData);
        } else {
            $this->httpRespond(406, 'No company Details');
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param int $gracePeriod
     */
    public function updateCompanyGracePeriodAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $gracePeriod = $request->getPost('companyGracePeriod');
            if ($gracePeriod) {
                $gracePeriodVal = (int) $gracePeriod;
                $data = array(
                    'companyGracePeriod' => $gracePeriodVal,
                    'userGracePeriod' => $gracePeriodVal
                );
                $this->CommonTable('CompanyTable')->updateCompanyDetails($data);
                $this->httpRespond(200, 'Successfully updated company grace period');
            } else {
                $this->httpRespond(406, 'Unacceptable value for the grace period');
            }
        } else {
            $this->httpRespond(400, 'Empty input params');
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param timestamp $liceseExpireDate
     */
    public function updateCompanyLicenseExpireDateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $licenseExpireDate = $request->getPost('companyLicenseExpireDate');
            if ($licenseExpireDate) {
                $data = array(
                    'companyLicenseExpireDate' => $licenseExpireDate
                );
                $this->CommonTable('CompanyTable')->updateCompanyDetails($data);
                $this->httpRespond(200, 'Successfully updated company grace period');
            } else {
                $this->httpRespond(406, 'Unacceptable value for company license expire date');
            }
        } else {
            $this->httpRespond(400, 'Empty input params');
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     */
    public function getLogDetailsAction()
    {
        $logData = $this->CommonTable('Core\model\LogTable')->getCompleteLogDetails();

        if (!empty($logData)) {
            return new JsonModel($logData);
        } else {
            $this->httpRespond(204, 'Empty log details');
        }
    }

    //update custom currency
    public function updateCustomCurrencyAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {

            $currencyID = $req->getPost('currencyName');

            //customer currency save
            $post = [
                'currencyID' => $currencyID,
                'currencyRate' => $req->getPost('currencyRate'),
            ];
            $currency = new Currency();
            $currency->exchangeArray($post);
            $status = $this->CommonTable('Core\Model\CurrencyTable')->updateCurrency($currency);
            if ($status) {
                $customCurrencyList = $this->getPaginatedCurrency();
                $htmlOutput = $this->_getCustomCurrencyListViewHtml($customCurrencyList, true);

                $this->status = true;
                $this->msg = $this->getMessage('SUCC_UPDATE_CUSTOM_CURRENCY');
                $this->data = array('customCurrencyList' => $customCurrencyList, 'customCurrencyId' => $currencyID);

                $this->html = $htmlOutput;
                return $this->JSONRespondHtml();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_UPDATE_CUSTOM_CURRENCY');
                return $this->JSONRespond();
            }
        }
    }

    //get custom currency list
    private function _getCustomCurrencyListViewHtml($customCurrencyList = array(), $paginated = false)
    {
        $view = new ViewModel(array(
            'customCurrencyList' => $customCurrencyList,
            'paginated' => $paginated
        ));

        $view->setTerminal(true);
        $view->setTemplate('settings/company/custom-currency-list.phtml');

        return $view;
    }

    protected function getPaginatedCurrency($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
        $this->setLogMessage('Currency list accessed');

        return $this->paginator;
    }

    public function searchCustomCurrencyAction()
    {
        $req = $this->getRequest();
        if ($req->isPost()) {
            $keyword = $req->getPost('keyword');

            if (!empty($keyword)) {
                $customCurrencyList = $this->CommonTable('Core\Model\CurrencyTable')->getCurrencyforSearch($keyword);
                $paginated = false;
            } else {
                $customCurrencyList = $this->getPaginatedCurrency();
                $paginated = true;
            }

            $htmlOutput = $this->_getCustomCurrencyListViewHtml($customCurrencyList, $paginated);

            $this->status = true;
            $this->html = $htmlOutput;
            return $this->JSONRespondHtml();
        }
    }

    public function createOfficialAction() {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('CompanyService')->createOfficial($request->getPost(), $this->userID);
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    public function changeStatusIDAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $updateState = $this->getService('CompanyService')->updateOfficialState($request->getPost());
        if ($updateState['status']) {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage($updateState['msg']);
        } else {
            $this->status = false;
            $this->msg = $this->getMessage($updateState['msg']);
            $this->rollback();
        }
        return $this->JSONRespond();
    }

    public function updateAdministrationOfficialAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('CompanyService')->updateAdministrationOfficial($request->getPost(), $this->userID);

        if (!$respond['status']) {

            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        } else {
            $this->commit();
            $this->status = true;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
        }

        return $this->JSONRespond();
    }

    public function getUploadedAttachemntsAction() {
        $request = $this->getRequest()->getQuery('employeeID');
        $respond = $this->getService('CompanyService')->getUploadedAttachemnts($request);
        $this->status = true;
        $this->data = $respond['data'];
        return $this->JSONRespondHtml();
    }

    public function deleteOfficialAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('CompanyService')->deleteOfficial($request->getPost(), $this->userID);

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }

    public function searchOfficialAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $result = $this->getService('CompanyService')->officialSearchByKey($request->getPost(), (int) $this->params()->fromRoute('param1', 1));

        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();

    }

    public function saveAdditionalDetailAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            $this->rollback();
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('CompanyService')->createAdditionalDetail($request->getPost());
        if (!$respond['status']) {
            $this->status = false;
            $this->data = $respond['data'];
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->data = $respond['data'];
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    public function getUploadedAttachemntsOfAdditionalDetailAction() {
        $request = $this->getRequest()->getQuery('addID');
        $respond = $this->getService('CompanyService')->getUploadedAttachemntsOfAdditionalDetail($request);
        $this->status = true;
        $this->data = $respond['data'];
        return $this->JSONRespondHtml();
    }

    public function updateAdditionalDetailsAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('CompanyService')->updateAdditionalDetails($request->getPost(), $this->userID);

        if (!$respond['status']) {

            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        } else {
            $this->commit();
            $this->data = $respond['data'];
            $this->status = true;
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
        }

        return $this->JSONRespond();

    }

    public function searchAdditionalDetailsAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }
        $result = $this->getService('CompanyService')->searchAdditionalDetail($request->getPost(), (int) $this->params()->fromRoute('param1', 1));

        $this->status = $result['status'];
        $this->html = $result['view'];
        return $this->JSONRespondHtml();
    }

    public function deleteAdditionDetailAction() {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $respond = $this->getService('CompanyService')->deleteAdditionDetail($request->getPost());

        if ($respond['status']) {
            $this->msg = $this->getMessage($respond['msg']);
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->msg = $this->getMessage($respond['msg']);
            $this->rollback();
        }

        $this->status = $respond['status'];
        $this->data = $respond['data'];
        return $this->JSONRespond();
    }
}
