<?php

namespace Settings\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class LandingCostTypeController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'settings_products_upper_menu';

    public function indexAction()
    {

        $this->getSideAndUpperMenus('Product Setup', 'Landing Cost Type');
        $this->useAccounting = $this->user_session->useAccounting;
        
        // create array for gl accounts
        $defaultGlAccounts = [
            'productSalesAccountID' => '',
            'productSalesAccountName' => '',
            'productInventoryAccountID' => '',
            'productInventoryAccountName' => '',
            'productCOGSAccountID' => '',
            'productCOGSAccountName' => '',
            'productAdjusmentAccountID' => '',
            'productAdjusmentAccountName' => ''
        ];
        $financeAccountsArray = $this->getFinanceAccounts();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $defaultGlAccounts['productSalesAccountID'] = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $defaultGlAccounts['productSalesAccountName'] = $financeAccountsArray[$glAccountSetupData->glAccountSetupItemDefaultSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupData->glAccountSetupItemDefaultSalesAccountID]['financeAccountsName'];
                
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $defaultGlAccounts['productInventoryAccountID'] = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $defaultGlAccounts['productInventoryAccountName'] = $financeAccountsArray[$glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID]['financeAccountsName'];
                
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $defaultGlAccounts['productCOGSAccountID'] = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $defaultGlAccounts['productCOGSAccountName'] = $financeAccountsArray[$glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID]['financeAccountsName'];
                
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $defaultGlAccounts['productAdjusmentAccountID'] = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $defaultGlAccounts['productAdjusmentAccountName'] = $financeAccountsArray[$glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID]['financeAccountsName'];
            }
        }

        $costTypeList = $this->getPaginatedCostTypes();
        $view = new ViewModel(array(
            'costTypeList' => $costTypeList,
            'paginated' => true,
            'useAccounting' => ($this->useAccounting == 1)? true :false,
            'glAcc' => $defaultGlAccounts
            
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/landing-cost-type.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return $view;
    }

    public function getPaginatedCostTypes($perPage = 6, $fromSearch = false)
    {
        $this->paginator = $this->CommonTable('Inventory\Model\CompoundCostTypeTable')->fetchAll(true);
        if($fromSearch) {
            $this->paginator->setCurrentPageNumber(1);
        } else {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        }
        $this->paginator->setItemCountPerPage($perPage);
        
        return $this->paginator;
    }

}
