<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Audit log related controller functions
 */

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * Audit log controller class create
 */
class AuditLogController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $userID;
    protected $username;
    protected $user_session;

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
    }

    public function auditLogAction()
    {
    	$globaldata = $this->getServiceLocator()->get('config');
        $this->getEvent()->getViewModel()->sidemenu = $globaldata['company_side_menu'];
        $this->getEvent()->getViewModel()->sidemenuselected = 'Audit Trial';

        $this->getPaginatedAuditLog();

        $dateFormat = $this->getUserDateFormat();
        $view = new ViewModel(array(
            	'auditLog' => $this->paginator,
            	'dateFormat' => $dateFormat,
            )
        );

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/audit-log.js');

        $this->setLogMessage("Audit log accessed.");
        return $view;
    }

    protected function getPaginatedAuditLog($perPage = 10)
    {
        $this->paginator = $this->CommonTable('Core\Model\LogTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
    }

}
