<?php

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class PurchaseRequisitionCategoryController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'pr_category_upper_menu';
    protected $downMenus = 'approval_workflow_down_menu';
    protected $paginator;

    public function createAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'Create', null, 'Purchase Requisition Categories');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/purchaseRequisitionType.js');
        $categories = $this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->fetchAll();
        $this->getPaginatedExpenseCategories();
        $pRCategoryListView = new ViewModel(array(
            'categories' => $this->paginator,
            'paginated' => TRUE));
        $pRCategoryListView->setTemplate('settings/purchase-requisition-category/list');
        $pRCategoryListView->setTerminal(FALSE);
        $pRCategoryCreateView = new ViewModel(array(
            'categories' => $categories,
        ));
        $pRCategoryCreateView->addChild($pRCategoryListView, 'categoryList');
        return $pRCategoryCreateView;
    }

    private function getPaginatedExpenseCategories()
    {

        $this->paginator = $this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(6);
    }

}
