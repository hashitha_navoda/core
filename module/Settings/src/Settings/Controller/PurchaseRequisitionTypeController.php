<?php

namespace Settings\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class PurchaseRequisitionTypeController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'pr_types_upper_menu';
    protected $downMenus = 'approval_workflow_down_menu';
    protected $paginator;

    public function createAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'Create', null, 'Purchase Requisition Types');
        $pRCategories = $this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->fetchAll();
        $employees = $this->CommonTable('Settings\Model\ApproverTable')->fetchAll();
        $employeeList = [];
        foreach ($employees as $employee) {
            $employeeList[] = $employee;
        }
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/purchaseRequisitionType.js');
        return new ViewModel(array(
            'expenseCategories' => $pRCategories,
            'employees' => $employeeList
        ));
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'View', null, 'Purchase Requisition Types');
        $editExpTypeId = $paramIn = $this->params()->fromRoute('param1');
        $expTypeData = $this->CommonTable("Settings\Model\PurchaseRequisitionTypeTable")->getExpenseTypeDetailsByID($editExpTypeId);
        $expTypeDetails = array();
        $financeAccountsArray = $this->getFinanceAccounts();
        foreach ($expTypeData as $expType) {
            $expTmp = array();
            $expTmp['expenseTypeId'] = $expType['purchaseRequisitionTypeId'];
            $expTmp['expenseTypeName'] = $expType['purchaseRequisitionTypeName'];
            $expTmp['expenseTypeCategory'] = $expType['purchaseRequisitionTypeCategory'];
            $expTmp['expenseTypeApproverEnabled'] = $expType['purchaseRequisitionTypeApproverEnabled'];
            $expTmp['expenseTypeStatus'] = $expType['purchaseRequisitionTypeStatus'];
            $expTypeLimitsArray = isset($expTypeDetails['expTypeLimits']) ? $expTypeDetails['expTypeLimits'] : array();
            $expTypeLimitApprovers = isset($expTypeLimitsArray[$expType['purchaseRequisitionTypeLimitId']]['expTypeLimitApprovers']) ? $expTypeLimitsArray[$expType['purchaseRequisitionTypeLimitId']]['expTypeLimitApprovers'] : array();
            if ($expType['purchaseRequisitionTypeLimitId'] != NULL) {
                $expTypeLimitsArray[$expType['purchaseRequisitionTypeLimitId']] = array(
                    'expenseTypeLimitId' => $expType['purchaseRequisitionTypeLimitId'],
                    'expenseTypeLimitMin' => $expType['purchaseRequisitionTypeLimitMin'],
                    'expenseTypeLimitMax' => $expType['purchaseRequisitionTypeLimitMax'],
                );
                $expTypeLimitApprovers[$expType['purchaseRequisitionTypeLimitApproverId']] = $expType['purchaseRequisitionTypeLimitApprover'];
                $expTypeLimitsArray[$expType['purchaseRequisitionTypeLimitId']]['expTypeLimitApprovers'] = $expTypeLimitApprovers;
            }
            $expTmp['expTypeLimits'] = $expTypeLimitsArray;
            $expTypeDetails = $expTmp;
        }

        $this->getViewHelper('HeadScript')->prependFile('/js/settings/purchaseRequisitionType.js');
        $expenseCategories = $this->CommonTable('Settings\Model\PurchaseRequisitionCategoryTable')->fetchAll();
        $employees = $this->CommonTable('Settings\Model\ApproverTable')->fetchAll();
        $employeeList = [];
        foreach ($employees as $employee) {
            $employeeList[] = $employee;
        }
        return new ViewModel(array(
            'data' => $expTypeDetails,
            'expenseCategories' => $expenseCategories,
            'employees' => $employeeList,
            'editExpTypeId' => $editExpTypeId
        ));
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'View', null, 'Purchase Requisition Types');
        $this->getViewHelper('HeadScript')->prependFile('/js/settings/purchaseRequisitionType.js');
        $this->getPaginatedExpenseTypes();
        return new ViewModel(array(
            'expTypes' => $this->paginator,
            'paginated' => TRUE));
    }

    private function getPaginatedExpenseTypes()
    {

        $this->paginator = $this->CommonTable('Settings\Model\PurchaseRequisitionTypeTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(6);
    }

}
