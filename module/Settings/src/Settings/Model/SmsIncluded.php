<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SmsIncluded
{

    public $smsIncludedId;
    public $smsIncludedName;
   
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->smsIncludedId = (!empty($data['smsIncludedId'])) ? $data['smsIncludedId'] : null;
        $this->smsIncludedName = (!empty($data['smsIncludedName'])) ? $data['smsIncludedName'] : null;
    }

}