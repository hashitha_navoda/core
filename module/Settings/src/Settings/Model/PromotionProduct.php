<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PromotionProduct
{

    public $promotionProductID;
    public $promotionProductPromotionID;
    public $promotionProductProductID;
    public $promotionProductMinQty;
    public $promotionProductMaxQty;
    public $promotionProductDiscountType;
    public $promotionProductDiscountAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->promotionProductID = (!empty($data['promotionProductID'])) ? $data['promotionProductID'] : null;
        $this->promotionProductPromotionID = (!empty($data['promotionProductPromotionID'])) ? $data['promotionProductPromotionID'] : null;
        $this->promotionProductProductID = (!empty($data['promotionProductProductID'])) ? $data['promotionProductProductID'] : null;
        $this->promotionProductMinQty = (!empty($data['promotionProductMinQty'])) ? $data['promotionProductMinQty'] : null;
        $this->promotionProductMaxQty = (!empty($data['promotionProductMaxQty'])) ? $data['promotionProductMaxQty'] : null;
        $this->promotionProductDiscountType = (!empty($data['promotionProductDiscountType'])) ? $data['promotionProductDiscountType'] : null;
        $this->promotionProductDiscountAmount = (!empty($data['promotionProductDiscountAmount'])) ? $data['promotionProductDiscountAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
