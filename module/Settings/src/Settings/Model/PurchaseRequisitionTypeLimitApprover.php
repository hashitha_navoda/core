<?php

namespace Settings\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

class PurchaseRequisitionTypeLimitApprover implements InputFilterAwareInterface
{

    public $purchaseRequisitionTypeLimitApproverId;
    public $purchaseRequisitionTypeLimitId;
    public $purchaseRequisitionTypeLimitApprover;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * Job employee ID mapped to expenseTypeLimitApprover
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->purchaseRequisitionTypeLimitApproverId = (isset($data['purchaseRequisitionTypeLimitApproverId'])) ? $data['purchaseRequisitionTypeLimitApproverId'] : null;
        $this->purchaseRequisitionTypeLimitId = (isset($data['purchaseRequisitionTypeLimitId'])) ? $data['purchaseRequisitionTypeLimitId'] : null;
        $this->purchaseRequisitionTypeLimitApprover = (isset($data['purchaseRequisitionTypeLimitApprover'])) ? $data['purchaseRequisitionTypeLimitApprover'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
