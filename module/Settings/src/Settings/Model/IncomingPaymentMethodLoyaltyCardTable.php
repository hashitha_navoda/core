<?php

namespace Settings\Model;

use Zend\Db\Sql\Sql;
use Zend\Db\Exception;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Select;

class IncomingPaymentMethodLoyaltyCardTable
{

    protected
            $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $loyalty = $this->tableGateway->select();
        return $loyalty;
    }

    public function save(IncomingPaymentMethodLoyaltyCard $incomingPaymentMethodLoyaltyCard)
    {
        $data = array(
            'incomingPaymentMethodLoyaltyCardID' => $incomingPaymentMethodLoyaltyCard->incomingPaymentMethodLoyaltyCardID,
            'customerLoyaltyID' => $incomingPaymentMethodLoyaltyCard->customerLoyaltyID,
            'incomingPaymentID' => $incomingPaymentMethodLoyaltyCard->incomingPaymentID,
            'collectedPoints' => $incomingPaymentMethodLoyaltyCard->collectedPoints,
            'redeemedPoints' => $incomingPaymentMethodLoyaltyCard->redeemedPoints,
            'expired' => $incomingPaymentMethodLoyaltyCard->expired
        );

        $id = $incomingPaymentMethodLoyaltyCard->incomingPaymentMethodLoyaltyCardID;
        if ($id == 0) {
            if ($this->tableGateway->insert($data)) {
                return $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            }
        } else {
            if ($this->getIncomingPaymentMethodLoyaltyCard()) {

            } else {
                throw new \Exception('Could not update loyalty points.');
            }
        }
    }

    public function getIncomingPaymentMethodLoyaltyCardByID($id)
    {
        $rowset = $this->tableGateway->select(array('incomingPaymentMethodLoyaltyCardID' => (int) $id));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function getIncomingPaymentMethodLoyaltyCardByPaymentID($paymentId)
    {
        $rowset = $this->tableGateway->select(array('incomingPaymentID' => (int) $paymentId));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

}
