<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class ItemAttributeMapTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveItemAttributeMap($itemAttributeMap)
    {
        $itemAttributeData = array(
            'itemAttributeID' => $itemAttributeMap['itemAttributeID'],
            'productID' => $itemAttributeMap['productID'],
        );
    	
        $this->tableGateway->insert($itemAttributeData);
        return $this->tableGateway->lastInsertValue;
    }

    public function getDetailsByProductID($proID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemAttributeMap')
                ->columns(array("*"))
                ->join('itemAttributeValueMap',
                     'itemAttributeMap.itemAttributeMapID =  itemAttributeValueMap.itemAttributeMapID',
                      array("*"), 
                      "left")
                ->where(array('itemAttributeMap.productID' => $proID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
    public function deleteMapDetailsByID($itemAttributeMapID)
    {
        try {
            $result = $this->tableGateway->delete(array('itemAttributeMapID' => $itemAttributeMapID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getDetailsByItemAttrID($itemAttrID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemAttributeMap')
                ->columns(array("*"))
                ->join('product',
                     'itemAttributeMap.productID =  product.productID',
                      array('entityID'), 
                      "left")
                ->join('entity',
                     'product.entityID =  entity.entityID',
                      array('deleted'), 
                      "left")
                ->where(array('entity.deleted' => '0', 'itemAttributeMap.itemAttributeID' => $itemAttrID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
