<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Document Type Functions
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DocumentType
{

    public $documentTypeID;
    public $documentTypeName;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->documentTypeID = (!empty($data['documentTypeID'])) ? $data['documentTypeID'] : null;
        $this->documentTypeName = (!empty($data['documentTypeName'])) ? $data['documentTypeName'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }


}
