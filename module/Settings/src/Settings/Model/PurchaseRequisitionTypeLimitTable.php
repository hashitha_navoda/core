<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

class PurchaseRequisitionTypeLimitTable
{

    /**
     * @var TableGateway
     */
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function create(PurchaseRequisitionTypeLimit $pRTypeLimit)
    {
        $data = array(
            'purchaseRequisitionTypeLimitId' => $pRTypeLimit->purchaseRequisitionTypeLimitId,
            'purchaseRequisitionTypeId' => $pRTypeLimit->purchaseRequisitionTypeId,
            'purchaseRequisitionTypeLimitMin' => $pRTypeLimit->purchaseRequisitionTypeLimitMin,
            'purchaseRequisitionTypeLimitMax' => $pRTypeLimit->purchaseRequisitionTypeLimitMax,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function getLimitsByTypeId($purchaseRequisitionTypeId)
    {
        $results = $this->tableGateway->select(array('purchaseRequisitionTypeId' => $purchaseRequisitionTypeId));
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = (array) $result;
        }
        return $resultsArray;
    }

    public function deleteByExpTypeId($purchaseRequisitionTypeId)
    {
        if ($this->tableGateway->delete(array('purchaseRequisitionTypeId' => $purchaseRequisitionTypeId))) {
            return true;
        } else {
            return false;
        }
    }

}
