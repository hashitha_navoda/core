<?php
namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Setting {
	public $settingId;
	public $settingName;
	public $locationID;
	public $userID;
	public $settingAttribute;

	public function exchangeArray($data){
		$this->settingId = $data['settingId'] ? $data['settingId'] : null;
		$this->settingName = $data['settingName'] ? $data['settingName'] : null;
		$this->locationID = $data['locationID'] ? $data['locationID'] : null;
		$this->userID = $data['userID'] ? $data['userID'] : null;
		$this->settingAttribute = $data['settingAttribute'] ? $data['settingAttribute'] : null;
	}

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
