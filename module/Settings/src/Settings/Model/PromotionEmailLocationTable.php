<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

Class PromotionEmailLocationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(PromotionEmailLocation $promotionEmailLocation)
    {
        $promotionEmailLocationData = array(
            'promotionEmailLocationID' => $promotionEmailLocation->promotionEmailLocationID,
            'promotionEmail' => $promotionEmailLocation->promotionEmail,
            'promotionEmailLocation' => $promotionEmailLocation->promotionEmailLocation,
        );

        $this->tableGateway->insert($promotionEmailLocationData);
        return $this->tableGateway->lastInsertValue;
    }

}
