<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

class PurchaseRequisitionTypeLimitApproverTable
{

    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function create(PurchaseRequisitionTypeLimitApprover $PurchaseRequisitionTypeLimitApprover)
    {
        $data = array(
            'purchaseRequisitionTypeLimitApproverId' => $PurchaseRequisitionTypeLimitApprover->purchaseRequisitionTypeLimitApproverId,
            'purchaseRequisitionTypeLimitId' => $PurchaseRequisitionTypeLimitApprover->purchaseRequisitionTypeLimitId,
            'purchaseRequisitionTypeLimitApprover' => $PurchaseRequisitionTypeLimitApprover->purchaseRequisitionTypeLimitApprover,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function deleteByExpTypeLimitId($purchaseRequisitionTypeLimitId)
    {
        if ($this->tableGateway->delete(array('purchaseRequisitionTypeLimitId' => $purchaseRequisitionTypeLimitId))) {
            return true;
        } else {
            return false;
        }
    }

}
