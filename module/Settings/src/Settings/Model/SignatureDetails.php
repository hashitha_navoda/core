<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SignatureDetails
{

    public $signatureDetailsID;
    public $signatureHolderName;
    public $signatureImageID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->signatureDetailsID = (!empty($data['signatureDetailsID'])) ? $data['signatureDetailsID'] : null;
        $this->signatureHolderName = (!empty($data['signatureHolderName'])) ? $data['signatureHolderName'] : null;
        $this->signatureImageID = (!empty($data['signatureImageID'])) ? $data['signatureImageID'] : null;
    }

}