<?php

namespace Settings\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

class PurchaseRequisitionCategory implements InputFilterAwareInterface
{

    public $purchaseRequisitionCategoryId;
    public $purchaseRequisitionCategoryName;
    public $purchaseRequisitionCategoryParentId;
    public $purchaseRequisitionCategoryStatus;
    public $entityId;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->purchaseRequisitionCategoryId = (isset($data['purchaseRequisitionCategoryId'])) ? $data['purchaseRequisitionCategoryId'] : null;
        $this->purchaseRequisitionCategoryName = (isset($data['purchaseRequisitionCategoryName'])) ? $data['purchaseRequisitionCategoryName'] : null;
        $this->purchaseRequisitionCategoryParentId = (isset($data['purchaseRequisitionCategoryParentId'])) ? $data['purchaseRequisitionCategoryParentId'] : null;
        $this->purchaseRequisitionCategoryStatus = (isset($data['purchaseRequisitionCategoryStatus'])) ? $data['purchaseRequisitionCategoryStatus'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
