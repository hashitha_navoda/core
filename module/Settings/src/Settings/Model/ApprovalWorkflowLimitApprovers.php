<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ApprovalWorkflowLimitApprovers
{

    public $workflowLimitApproverID;
    public $workflowLimitID;
    public $workflowLimitApprover;
   
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->workflowLimitApproverID = (!empty($data['workflowLimitApproverID'])) ? $data['workflowLimitApproverID'] : null;
        $this->workflowLimitID = (!empty($data['workflowLimitID'])) ? $data['workflowLimitID'] : null;
        $this->workflowLimitApprover = (!empty($data['workflowLimitApprover'])) ? $data['workflowLimitApprover'] : null;
    }

}