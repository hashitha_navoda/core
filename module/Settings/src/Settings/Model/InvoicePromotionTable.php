<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

Class InvoicePromotionTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(InvoicePromotion $invoicePromotion)
    {
        $invoicePromotionData = array(
            'invoicePromotionPromotionID' => $invoicePromotion->invoicePromotionPromotionID,
            'invoicePromotionMinValue' => $invoicePromotion->invoicePromotionMinValue,
            'invoicePromotionMaxValue' => $invoicePromotion->invoicePromotionMaxValue,
            'invoicePromotionDiscountType' => $invoicePromotion->invoicePromotionDiscountType,
            'invoicePromotionDiscountAmount' => $invoicePromotion->invoicePromotionDiscountAmount
        );

        $this->tableGateway->insert($invoicePromotionData);
        return $this->tableGateway->lastInsertValue;
    }

    public function editByPromotionID($data, $promotionID)
    {
        try {
            $this->tableGateway->update($data, array('invoicePromotionPromotionID' => $promotionID));
            return TRUE;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return FALSE;
        }
    }

}
