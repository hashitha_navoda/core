<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class DimensionValueTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dimensionValues');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function saveDimensionValue($dimensionValueData)
    {
        $dimensionValueData = array(
            'dimensionId' => $dimensionValueData->dimensionId,
            'dimensionValue' => $dimensionValueData->dimensionValue
        );
        if ($this->tableGateway->insert($dimensionValueData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }   


    public function getRelatedDimensionValues($dimensionId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('dimensionValues');
        $select->where(array('dimensionValues.dimensionId' => $dimensionId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getDimensionByDimensionValueID($dimensionValueId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('dimensionValues');
        $select->join('dimension','dimension.dimensionId = dimensionValues.dimensionId', array('dimensionName'));
        $select->where(array('dimensionValues.dimensionValueId' => $dimensionValueId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getDimensionByDimensionValueIDArr($dimensionValueIds)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('dimensionValues');
        $select->join('dimension','dimension.dimensionId = dimensionValues.dimensionId', array('dimensionName'));
        $select->where->in('dimensionValues.dimensionValueId', $dimensionValueIds);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    
}