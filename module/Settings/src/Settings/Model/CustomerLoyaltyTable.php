<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;

class CustomerLoyaltyTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $loyalty = $this->tableGateway->select();
        return $loyalty;
    }

    public function save(CustomerLoyalty $customerLoyalty)
    {
        $data = array(
            'customerID' => $customerLoyalty->customer_id,
            'loyaltyID' => $customerLoyalty->loyalty_id,
            'customerLoyaltyCode' => $customerLoyalty->loyalty_code,
            'customerLoyaltyPoints' => $customerLoyalty->loyalty_points,
            'customerLoyaltyCreateDate' => $customerLoyalty->loyalty_card_create_date,
            'customerLoyaltyStatus' => $customerLoyalty->status,
        );

        if ((int) $customerLoyalty->id == 0) {
            $this->tableGateway->insert($data);
        } else {
            throw new \Exception('Loyalty Card id does not exist');
        }

        return $customerLoyalty;
    }

    public function updateLoyaltyCard($cust_id, $loyaltyID, $loyalty_code, $date)
    {
        $data = array(
            'loyaltyID' => $loyaltyID,
            'customerLoyaltyCode' => $loyalty_code,
            'customerLoyaltyCreateDate' => $date,
            'customerLoyaltyStatus' => 1
        );
        
        $all_cards = $this->tableGateway->select(array('customerID' => (int) $cust_id));
        $card = $this->getLoyaltyCardByCustID($cust_id); // This function returns currenty active card
        
        $loyaltyID = (is_null($loyaltyID)) ? $card->loyalty_id : $loyaltyID;        
        if ($all_cards->count()) {            
            if ($card && $card->loyalty_id != $loyaltyID) {
                $data['customerLoyaltyPoints'] = $card->loyalty_points;
                $res = $this->tableGateway->update($data, array('customerID' => (int) $cust_id, 'loyaltyID' => (int) $loyaltyID));
                $this->deactivateLoyaltyCard($card->id);
            }


            // If re-activatig previously used card
            while ($crd = $all_cards->current()) {
                if ($crd->loyalty_id == $loyaltyID) {
                    $data['loyaltyID'] = $crd->loyalty_id;
                    $data['customerLoyaltyCode'] = $crd->loyalty_code;
                    $res = $this->tableGateway->update($data, array('customerID' => (int) $cust_id, 'loyaltyID' => (int) $loyaltyID));
                    return $res;
                }
            }
        }

        // If user has no loyalty card previously create new loyalty card
        $data['customerID'] = $cust_id;
        if ($loyaltyID != '0') {
            $customer_loyalty = new CustomerLoyalty();
            $customer_loyalty->exchangeArray($data);
            return $this->save($customer_loyalty);
        }
    }

    public function deactivateLoyaltyCard($customerLoyaltyID)
    {
        $data = array(
            'customerLoyaltyPoints' => null,
            'customerLoyaltyStatus' => 0
        );
        return $this->tableGateway->update($data, array('customerLoyaltyID' => $customerLoyaltyID));
    }

    public function updateLoyaltyPoints($id, $points)
    {
        $data = array('customerLoyaltyPoints' => $points);
        return $this->tableGateway->update($data, array('customerLoyaltyID' => $id));
    }

    public function getLoyaltyCardByCustID($id)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $select = new Select();
        $select->from('customerLoyalty')
                ->join('loyalty', 'loyalty.loyaltyID = customerLoyalty.loyaltyID', array('*'))
                ->where('customerLoyalty.customerLoyaltyStatus = 1')
                ->where('customerLoyalty.customerID = "' . $id . '"');
        $statement = $dbAdapter->createStatement();
        $select->prepareStatement($dbAdapter, $statement);

        $row_set = $statement->execute();
        return (object) $row_set->current();
    }

    public function getLoyaltyCardByCardNo($cardNo)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $select = new Select();
        $select->from('customerLoyalty')
                ->join('loyalty', 'loyalty.loyaltyID = customerLoyalty.loyaltyID', array('*'))
                ->join('customer', 'customer.customerID = customerLoyalty.customerID')
                ->where('customerLoyalty.customerLoyaltyStatus = 1')
                ->where('customerLoyalty.customerLoyaltyCode = "' . $cardNo . '"');
        $statement = $dbAdapter->createStatement();
        $select->prepareStatement($dbAdapter, $statement);

        $row_set = $statement->execute();
        return $row_set->current();
    }

    public function getLoyaltyCardByPhoneNo($phoneNo)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $select = new Select();
        $select->from('customerLoyalty')
                ->join('loyalty', 'loyalty.loyaltyID = customerLoyalty.loyaltyID', array('*'))
                ->join('customer', 'customer.customerID = customerLoyalty.customerID', array('*'))
                ->where('customerLoyalty.customerLoyaltyStatus = 1')
                ->where('customer.customerTelephoneNumber = "' . $phoneNo . '"');
        $statement = $dbAdapter->createStatement();
        $select->prepareStatement($dbAdapter, $statement);

        $row_set = $statement->execute();
        return $row_set->current();
    }

    public function getLoyaltyCardByCustomerID($customerID)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $select = new Select();
        $select->from('customerLoyalty')
                ->join('loyalty', 'loyalty.loyaltyID = customerLoyalty.loyaltyID', array('*'))
                ->join('customer', 'customer.customerID = customerLoyalty.customerID', array('*'))
                ->where('customerLoyalty.customerLoyaltyStatus = 1')
                ->where('customer.customerID = "' . $customerID . '"');
        $statement = $dbAdapter->createStatement();
        $select->prepareStatement($dbAdapter, $statement);

        $row_set = $statement->execute();
        return $row_set->current();
    }

    public function getLoyaltyCustomerList($loyaltyId)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $select = new Select();
        $select->from('customerLoyalty')
                ->join('loyalty', 'loyalty.loyaltyID = customerLoyalty.loyaltyID', array('*'))
                ->join('customer', 'customer.customerID = customerLoyalty.customerID', array('*'))
                ->where('customerLoyalty.customerLoyaltyStatus = 1')
                ->where('loyalty.loyaltyID = ' . $loyaltyId);
        $statement = $dbAdapter->createStatement();
        $select->prepareStatement($dbAdapter, $statement);

        $row_set = $statement->execute();

        $list = array();
        foreach ($row_set as $row) {
            $list[] = $row;
        }
        $adapter = new ArrayAdapter($list);
        return new Paginator($adapter);
    }

    /**
     * Get loyalty card numbers for dropdown
     * @param string $searchKey
     * @return mixed
     */
    public function searchLoyaltyCardNumbersForDropdown($searchKey)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $select = new Select();
        $select->from('customerLoyalty')
                ->where->isNotNull('customerLoyaltyCode')
                ->where->like('customerLoyaltyCode', '%' . $searchKey . '%');
        $select->group('customerLoyaltyCode');
        $statement = $dbAdapter->createStatement();
        $select->prepareStatement($dbAdapter, $statement);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getLoyaltyCardByCustomerLoyaltyID($customerLoyaltyID)
    {
        $dbAdapter = $this->tableGateway->getAdapter();
        $select = new Select();
        $select->from('customerLoyalty')
                ->where('customerLoyalty.customerLoyaltyStatus = 1')
                ->where('customerLoyalty.customerLoyaltyID = "' . $customerLoyaltyID . '"');
        $statement = $dbAdapter->createStatement();
        $select->prepareStatement($dbAdapter, $statement);

        $row_set = $statement->execute();
        return $row_set->current();
    }
}
