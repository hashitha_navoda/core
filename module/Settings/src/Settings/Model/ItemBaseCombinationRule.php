<?php

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ItemBaseCombinationRule implements InputFilterAwareInterface
{

    public $itemBaseCombinationRuleID;
    public $combinationID;
    public $productID;
    public $rule;
    public $quantity;

    public function exchangeArray($data)
    {
        $this->itemBaseCombinationRuleID = (!empty($data['itemBaseCombinationRuleID'])) ? $data['itemBaseCombinationRuleID'] : Null;
        $this->combinationID = (!empty($data['combinationID'])) ? $data['combinationID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->rule = (!empty($data['rule'])) ? $data['rule'] : null;
        $this->quantity = (!empty($data['quantity'])) ? $data['quantity'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}