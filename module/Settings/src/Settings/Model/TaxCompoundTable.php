<?php

/**
 * @author Damith Thamara   <damith@thinkcube.com>
 * This file contains taxCompound table related funtions
 */

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Exception;
use Zend\Db\Sql\Sql;

class TaxCompoundTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCompoundTax(TaxCompound $taxCompound)
    {
        $data = array(
            'compoundTaxID' => $taxCompound->compoundTaxID,
            'simpleTaxID' => $taxCompound->simpleTaxID
        );

        $result = $this->tableGateway->insert($data);
        return $result;
    }

    public function deleteCompoundTax($compoundTaxID)
    {
        $this->tableGateway->delete(array('compoundTaxID' => $compoundTaxID));
    }

    /**
     * Get simple taxes by compound tax id and product id
     * @param int $compoundTaxId
     * @param int $productId
     * @return mixed
     */
    public function getSimpleTaxByCompoundTaxIdAndProductId($compoundTaxId, $productId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('taxCompound');
        $select->columns(["simpleTaxID"])
               ->join('tax', 'tax.id = taxCompound.simpleTaxID', ['*'], 'left')
               ->join('productTax', 'tax.id = productTax.taxID', ['productID'], 'left')
               ->where(['taxCompound.compoundTaxID' => $compoundTaxId, 'productTax.productID' => $productId]);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

     public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

}

