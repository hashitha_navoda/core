<?php

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Loyalty implements InputFilterAwareInterface
{

    public $id;
    public $name;
    public $prefix;
    public $code_degits;
    public $code_current_no;
    public $no_of_cards;
    public $active_min_val;
    public $active_max_val;
    public $earning_per_point;
    public $redeem_per_point;
    public $loyaltyGlAccountID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['loyaltyID'])) ? $data['loyaltyID'] : Null;
        $this->name = (!empty($data['loyaltyName'])) ? $data['loyaltyName'] : null;
        $this->prefix = (!empty($data['loyaltyPrefix'])) ? $data['loyaltyPrefix'] : null;
        $this->code_degits = (!empty($data['loyaltyCodeDegits'])) ? $data['loyaltyCodeDegits'] : null;
        $this->code_current_no = (!empty($data['loyaltyCodeCurrentNo'])) ? $data['loyaltyCodeCurrentNo'] : null;
        $this->no_of_cards = (!empty($data['loyaltyNoOfCards'])) ? $data['loyaltyNoOfCards'] : null;
        $this->active_min_val = (!empty($data['loyaltyActiveMinVal'])) ? $data['loyaltyActiveMinVal'] : null;
        $this->active_max_val = (!empty($data['loyaltyActiveMaxVal'])) ? $data['loyaltyActiveMaxVal'] : null;
        $this->earning_per_point = (!empty($data['loyaltyEarningPerPoint'])) ? $data['loyaltyEarningPerPoint'] : null;
        $this->redeem_per_point = (!empty($data['loyaltyRedeemPerPoint'])) ? $data['loyaltyRedeemPerPoint'] : null;
        $this->status = (isset($data['loyaltyStatus'])) ? $data['loyaltyStatus'] : null;
        $this->loyaltyGlAccountID = (!empty($data['loyaltyGlAccountID'])) ? $data['loyaltyGlAccountID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
