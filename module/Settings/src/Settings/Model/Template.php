<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Template
{

    public $templateID;
    public $templateName;
    public $documentTypeID;
    public $documentSizeID;
    public $templateDetails;
    public $templateFooterDetails;
    public $templateFooterShow;
    public $templateDefault;
    public $templateSample;
    public $templateHeader;
    public $templateBody;
    public $templateFooter;
    public $templateAdvancedOptions;
    public $isAdvanceTemplateEnabled;
    public $tplUpdateFlag;

    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->templateID = (!empty($data['templateID'])) ? $data['templateID'] : null;
        $this->templateName = (!empty($data['templateName'])) ? $data['templateName'] : null;
        $this->documentTypeID = (!empty($data['documentTypeID'])) ? $data['documentTypeID'] : null;
        $this->documentSizeID = (!empty($data['documentSizeID'])) ? $data['documentSizeID'] : null;
        $this->templateContent = (!empty($data['templateContent'])) ? $data['templateContent'] : null;
        $this->templateFooterDetails = (!empty($data['templateFooterDetails'])) ? $data['templateFooterDetails'] : null;
        $this->templateFooterShow = (!empty($data['templateFooterShow'])) ? $data['templateFooterShow'] : 0;
        $this->templateDefault = (!empty($data['templateDefault'])) ? $data['templateDefault'] : 0;
        $this->templateSample = (!empty($data['templateSample'])) ? $data['templateSample'] : 0;
        $this->templateHeader = (!empty($data['templateHeader'])) ? $data['templateHeader'] : null;
        $this->templateBody = (!empty($data['templateBody'])) ? $data['templateBody'] : null;
        $this->templateFooter = (!empty($data['templateFooter'])) ? $data['templateFooter'] : null;
        $this->templateAdvancedOptions = (!empty($data['templateAdvancedOptions'])) ? $data['templateAdvancedOptions'] : [];
        $this->isAdvanceTemplateEnabled = (!empty($data['isAdvanceTemplateEnabled'])) ? $data['isAdvanceTemplateEnabled'] : 0;
        $this->tplUpdateFlag = (!empty($data['tplUpdateFlag'])) ? $data['tplUpdateFlag'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
