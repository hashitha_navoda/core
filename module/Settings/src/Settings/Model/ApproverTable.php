<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class ApproverTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvers');
        $select->where(array('deleted' => '0'));
        $select->order('approverID DESC');
        $select->join('entity', 'approvers.entityID =entity.entityID', array("deleted"), "left");
        $select->where(array('entity.deleted' => 0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function checkApproverIDNovalid($IDNO, $approverID=null, $updataMode = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('approvers')
                ->columns(array('*'))
                ->join('entity', 'approvers.entityID =entity.entityID', array("deleted"), "left")
                ->where(array('entity.deleted' => 0, 'approvers.nic' => $IDNO));
        if ($updataMode) {
            $select->where->notEqualTo('approvers.approverID',$approverID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function saveApprovers($officialData)
    {
        $data = array(
            'firstName' => $officialData->firstName,
            'lastName' => $officialData->lastName,
            'address' => $officialData->address,
            'email' => $officialData->email,
            'telephoneNo' => $officialData->telephoneNo,
            'nic' => $officialData->nic,
            'entityID' => $officialData->entityID,
            'status' => '1',
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateApproverState($approverID, $status)
    {
        $data = array(
            'status' => $status,
        );
        try {
            $this->tableGateway->update($data, array('approverID' => $approverID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getApproverSearchByKey($key=null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvers')
                ->columns(array('*'))
                ->join('entity', 'approvers.entityID = entity.entityID', array('*'), 'left');
        if (!is_null($key)) {
            $select->where(new PredicateSet(array(new Operator('approvers.firstName', 'like', '%' . $key . '%'),new Operator('approvers.lastName', 'like', '%' . $key . '%'), new Operator('approvers.telephoneNo', 'like', '%' . $key . '%'), new Operator('approvers.nic', 'like', '%' . $key . '%')), PredicateSet::OP_OR));
        }

        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getApproversBySearchKeyForDropdown($key = null) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvers')
            ->columns(array('*'))
            ->join('entity', 'approvers.entityID = entity.entityID', array('*'), 'left');
        if (!is_null($key)) {
            $select->where(new PredicateSet(array(new Operator('approvers.firstName', 'like', '%' . $key . '%'), new Operator('approvers.lastName', 'like', '%' . $key . '%')), PredicateSet::OP_OR));
        }
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getApproverByID($Id) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvers')
            ->columns(array('*'))
            ->join('entity', 'approvers.entityID = entity.entityID', array('*'), 'left')
            ->where(array('entity.deleted' => 0, 'approvers.approverID' => $Id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    
}