<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

Class PromotionEmailTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotionEmail');

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            $promotionEmailList = array();
            foreach ($results as $result) {
                $promotionEmailList[] = $result;
            }
            return $promotionEmailList;
        }
    }

    public function save(PromotionEmail $promotionEmail)
    {
        $promotionEmailData = array(
            'promotionEmailID' => $promotionEmail->promotionEmailID,
            'promotionEmailName' => $promotionEmail->promotionEmailName,
            'promotionEmailSubject' => $promotionEmail->promotionEmailSubject,
            'promotionEmailContent' => $promotionEmail->promotionEmailContent,
            'promotionEmailImage' => $promotionEmail->promotionEmailImage,
            'entityID' => $promotionEmail->entityID,
        );

        $this->tableGateway->insert($promotionEmailData);
        return $this->tableGateway->lastInsertValue;
    }

    public function getPromotionEmails($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotionEmail');
        $select->join('promotionEmailLocation', 'promotionEmail.promotionEmailID= promotionEmailLocation.promotionEmail', array('*'));
        $select->join('promotionEmailProduct', 'promotionEmail.promotionEmailID= promotionEmailProduct.promotionEmail', array('*'));
        $select->join('promotionEmailCustomer', 'promotionEmail.promotionEmailID= promotionEmailCustomer.promotionEmail', array('*'));

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            $promotionEmailList = array();
            foreach ($results as $result) {
                $promotionEmailList[] = $result;
            }
            return $promotionEmailList;
        }
    }

    public function getPromotionEmail($promotionEmailID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotionEmail');
        $select->join('promotionEmailLocation', 'promotionEmail.promotionEmailID= promotionEmailLocation.promotionEmail', array('*'), 'left');
        $select->join('promotionEmailProduct', 'promotionEmail.promotionEmailID= promotionEmailProduct.promotionEmail', array('*'), 'left');
        $select->join('promotionEmailCustomer', 'promotionEmail.promotionEmailID= promotionEmailCustomer.promotionEmail', array('*'), 'left');
        $select->join('promotionEmailCustomerEvent', 'promotionEmail.promotionEmailID= promotionEmailCustomerEvent.promotionEmail', array('*'), 'left');
        $select->join('location', 'promotionEmailLocation.promotionEmailLocation= location.locationID', array('locationName'), 'left');
        $select->join('product', 'promotionEmailProduct.promotionEmailProduct= product.productID', array('productName', 'productCode'), 'left');
        $select->join('customer', 'promotionEmailCustomer.promotionEmailCustomer= customer.customerID', array('customerName', 'customerTitle'), 'left');
        $select->join('customerEvent', 'promotionEmailCustomerEvent.promotionEmailCustomerEvent= customerEvent.customerEventID', array('customerEventName'), 'left');
        $select->where(array('promotionEmail.promotionEmailID' => $promotionEmailID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $promotionEmailData = array();
        foreach ($results as $result) {
            $promotionEmailData[] = $result;
        }
        return $promotionEmailData;
    }

}
