<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Approver
{

    public $approverID;
    public $firstName;
    public $lastName;
    public $telephoneNo;
    public $nic;
    public $address;
    public $email;
    public $entityID;
    public $status;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->approverID = (!empty($data['approverID'])) ? $data['approverID'] : null;
        $this->firstName = (!empty($data['firstName'])) ? $data['firstName'] : null;
        $this->lastName = (!empty($data['lastName'])) ? $data['lastName'] : null;
        $this->telephoneNo = (!empty($data['telephoneNo'])) ? $data['telephoneNo'] : null;
        $this->nic = (!empty($data['nic'])) ? $data['nic'] : null;
        $this->address = (!empty($data['address'])) ? $data['address'] : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
    }

}