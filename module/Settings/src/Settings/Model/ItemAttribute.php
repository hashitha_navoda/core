<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ItemAttribute
{

    public $itemAttributeID;
    public $itemAttributeCode;
    public $itemAttributeName;
    public $itemAttributeState;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->itemAttributeID = (!empty($data['itemAttributeID'])) ? $data['itemAttributeID'] : null;
        $this->itemAttributeCode = (!empty($data['itemAttributeCode'])) ? $data['itemAttributeCode'] : null;
        $this->itemAttributeName = (!empty($data['itemAttributeName'])) ? $data['itemAttributeName'] : null;
        $this->itemAttributeState = (!empty($data['itemAttributeState'])) ? $data['itemAttributeState'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : 1;
        
    }

}