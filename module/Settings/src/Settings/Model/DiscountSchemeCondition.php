<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DiscountSchemeCondition
{

    public $discountSchemeConditionsID;
    public $discountSchemesID;
    public $minQty;
    public $maxQty;
    public $discountType;
    public $discountPercentage;
    public $discountValue;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->discountSchemeConditionsID = (!empty($data['discountSchemeConditionsID'])) ? $data['discountSchemeConditionsID'] : null;
        $this->discountSchemesID = (!empty($data['discountSchemesID'])) ? $data['discountSchemesID'] : null;
        $this->minQty = (!empty($data['minQty'])) ? $data['minQty'] : null;
        $this->maxQty = (!empty($data['maxQty'])) ? $data['maxQty'] : null;
        $this->discountType = (!empty($data['discountType'])) ? $data['discountType'] : null;
        $this->discountPercentage = (!empty($data['discountPercentage'])) ? $data['discountPercentage'] : null;
        $this->discountValue = (!empty($data['discountValue'])) ? $data['discountValue'] : null;
    }

}