<?php
namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class SettingTable{
	protected $tableGateway;

	public function __construct(TableGateway $tableGateway){
		$this->tableGateway = $tableGateway;
	}

	public function getSettings($setting, $locationId, $userId){

		$where = array('settingName' => $setting['name']);
		if($setting['type'] == 'L'){
			$where['locationID'] = $locationId;
		}else if($setting['type'] == 'U'){
			$where['userID'] = $userId;
		}

		$row = $this->tableGateway->select($where);

		if(!$row->count()){
			return false;
		}

		return $row->current();
	}

	public function saveSettings($setting, $attributes, $locationId, $userId){

		$savedSettings = $this->getSettings($setting, $locationId, $userId);

		$attrJson = $savedSettings ? $savedSettings->settingAttribute : $setting['default'];
		$attr = json_decode($attrJson);

		foreach ($attributes as $key => $value) {
			$attr->$key = $value;
		}

		$data = array(
			'settingName' => $setting['name'],
			'locationID' => $setting['type'] == 'L' ? $locationId : null,
			'userID' => $setting['type'] == 'U' ? $userId : null,
			'settingAttribute' => json_encode($attr)
		);
		if($savedSettings){
			// update currently saved settings
			// If setting type is G - put only setting name on where clause
			// If setting type is L - put setting name and locationID on where clause
			// If setting type is U - put setting name and userID on where clause
			$where = array(
				'settingName' => $setting['name'],
			);

			if ($setting['type'] == 'L') {
				$where['locationID'] = $locationId;
			}elseif ($setting['type'] = 'U') {
				$where['userID'] = $userId;
			}

	    	$this->tableGateway->update($data, $where);
	    	return true;
		}else{
			// save new settings
			// IF setting type is G - locationID and userID will be null and one record will be inserted
			// IF setting type is L - userID will be null and one record will be inserted for each location
			// IF setting type is U - a record will be inserted for each user
			$this->tableGateway->insert($data);
	    	return $this->tableGateway->lastInsertValue;
		}
	}
}
