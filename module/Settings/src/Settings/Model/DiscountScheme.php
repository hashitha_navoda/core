<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DiscountScheme
{

    public $discountSchemesID;
    public $discountSchemeCode;
    public $discountSchemeName;
    public $discountType;
    public $locationID;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->discountSchemesID = (!empty($data['discountSchemesID'])) ? $data['discountSchemesID'] : null;
        $this->discountSchemeCode = (!empty($data['discountSchemeCode'])) ? $data['discountSchemeCode'] : null;
        $this->discountSchemeName = (!empty($data['discountSchemeName'])) ? $data['discountSchemeName'] : null;
        $this->discountType = (!empty($data['discountType'])) ? $data['discountType'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

}