<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains CompanyTable Functions
 */

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class CompanyTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function getCompany($id = NULL)
    {
        //Skip the id validation and return the first row
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('company')
                ->columns(array('*'))
                ->order('id ASC')->limit(1);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        $row = (object) $rowset->current();
        if (!$row) {
            return NULL;
        } else {
            return $row;
        }
    }

    public function getWizardState()
    {
        $rowset = $this->tableGateway->select(array('wizardState'));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function updateWizardState($state)
    {
        $data = array(
            'wizardState' => $state,
            'apiKey' => $this->getCompany()->apiKey,
            'companyLicenseExpireDate' => $this->getCompany()->companyLicenseExpireDate,
            'companyAccountType' => $this->getCompany()->companyAccountType
        );

        try {
            $this->tableGateway->update($data);
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public function updateUseAccountingFlag($falg)
    {
        $data = array(
            'companyUseAccounting' => $falg,
        );

        try {
            $this->tableGateway->update($data);
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public function saveCompany(Company $company)
    {
        $data = array(
            'companyName' => $company->companyName,
            'companyAddress' => $company->companyAddress,
            'postalCode' => $company->postalCode,
            'telephoneNumber' => $company->telephoneNumber,
            'telephoneNumber2' => $company->telephoneNumber,
            'telephoneNumber3' => $company->telephoneNumber,
            'faxNumber' => $company->telephoneNumber,
            'email' => $company->email,
            'country' => $company->country,
            'website' => $company->website,
            'brNumber' => $company->brNumber,
            'companyGracePeriod' => $company->companyGracePeriod,
            'userGracePeriod' => $company->userGracePeriod,
            'companyLicenseExpireDate' => $company->companyLicenseExpireDate,
            'companyAccountType' => $company->companyAccountType,
            'apiKey' => $company->apiKey,
        );

        $id = (int) $company->id;
        if ($id == 0) {
            $this->tableGateway->insert($data);
            $cid = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        } else {
            if ($this->getCompany($id)) {
                $cid = $this->getCompany()->id;
                $data['apiKey'] = $this->getCompany()->apiKey;
                $data['companyLicenseExpireDate'] = $this->getCompany()->companyLicenseExpireDate;
                $data['companyAccountType'] = $this->getCompany()->companyAccountType;
                $this->tableGateway->update($data, array('id' => $cid));
            } else {
                throw new \Exception('company id does not exist');
            }
        }
        return $cid;
    }

    /**
     * @author Prathap Weerasinghe <prathap@thinkcube.com>
     * @param \Invoice\Model\Company $company
     * @return bool
     */
    public function updateTRNumber(Company $company)
    {
        $data = array(
            'taxRegNumber' => $company->trNumber,
            'oldTaxRegNumber' => $company->oldTaxRegNumber
        );
        return $this->tableGateway->update($data, array('id' => $company->id));
    }

    /**
     * @author Prathap Weerasinghe <prathap@thinkcube.net>
     * @return string
     */
    public function getTRNumber()
    {
        $statement = $this->tableGateway->select(function ($select) {
            $select->columns(array('id', 'taxRegNumber', 'oldTaxRegNumber'));
            $select->where(array('id' => $this->getCompany()->id));
        });
        return $statement->current();
    }

    public function getGracePeriod()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('company')
                ->columns(array('gracePeriod'))
                ->where(array('company.id' => $this->getCompany()->id));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset->current();
    }

    public function getUserGracePeriod()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('company')
                ->columns(array('userGracePeriod'))
                ->where(array('company.id' => $this->getCompany()->id));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset->current();
    }

    public function updateCompanyLogo(Company $company)
    {
        $data = array(
            'id' => $company->id,
            'logoID' => $company->logoID
        );

        if ($this->tableGateway->update($data, array('id' => $company->id))) {
            return TRUE;
        } else {
            throw new \Exception('Company logo update failed');
        }
    }

    public function getCompanyDetails()
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('company');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }
        return $countArray;
    }

    public function updateImageUrl($data, $companyID)
    {
        try {
            $this->tableGateway->update($data, array('id' => $companyID));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function getImageUrl($companyID)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('company')
                ->columns(array('logoID'))
                ->where(array('company.id' => $companyID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset->current();
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $licenceCount
     * @param type $companyID
     * @return boolean
     */
    public function updateLicenceCount($licenceCount, $companyID)
    {
        $data = array('licenceCount' => $licenceCount);
        try {
            $this->tableGateway->update($data, array('id' => $companyID));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * get count of what wizardFlag is 1
     * @return null|boolean
     */
    public function checkWizardCompleted()
    {
        $rowset = $this->tableGateway->select(array('wizardComplete' => 1));

        if (count($rowset) == 0) {
            return NULL;
        }
        return true;
    }

    public function updateWizardComplete()
    {
        $data = array(
            'wizardComplete' => 1
        );
        return $this->tableGateway->update($data);
    }

    public function updateCompanyDetails($data)
    {
        try {
            $companyId = $this->getCompany()->id;
            $this->tableGateway->update($data, ['id' => $companyId]);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * get count of what companyUseAccounting is 1
     * @return null|boolean
     */
    public function checkCompanyUseAccounting()
    {
        $rowset = $this->tableGateway->select(array('companyUseAccounting' => 1));

        if (count($rowset) == 0) {
            return false;
        }
        return true;
    }

}
