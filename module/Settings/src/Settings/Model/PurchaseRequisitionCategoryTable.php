<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;


class PurchaseRequisitionCategoryTable
{

    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('purchaseRequisitionCategory')
                    ->columns(array('*'))
                    ->join(array('parentPRCat' => 'purchaseRequisitionCategory'), 'purchaseRequisitionCategory.purchaseRequisitionCategoryParentId = parentPRCat.purchaseRequisitionCategoryId', array('purchaseRequisitionCategoryParentName' => new Expression('parentPRCat.purchaseRequisitionCategoryName')), 'left')
                    ->join('entity', 'purchaseRequisitionCategory.entityId = entity.entityID', array('deleted'))
                    ->where(array('entity.deleted' => '0'));
            $paginatorAdapter = new DbSelect($select, $adapter);
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        } else {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('purchaseRequisitionCategory')
                    ->columns(array('*'))
                    ->join(array('parentPRCat' => 'purchaseRequisitionCategory'), 'purchaseRequisitionCategory.purchaseRequisitionCategoryParentId = parentPRCat.purchaseRequisitionCategoryId', array('purchaseRequisitionCategoryParentName' => new Expression('parentPRCat.purchaseRequisitionCategoryName')), 'left')
                    ->join('entity', 'purchaseRequisitionCategory.entityId = entity.entityID', array('deleted'))
                    ->where(array('entity.deleted' => '0'));
            $select->where->notEqualto('purchaseRequisitionCategory.purchaseRequisitionCategoryStatus',2);
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            $resultsArray = [];
            foreach ($results as $result) {
                $resultsArray[] = $result;
            }
            return $resultsArray;
        }
    }

    public function getCategoryByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionCategory')
                ->columns(array('*'))
                ->where(array('purchaseRequisitionCategory.purchaseRequisitionCategoryId' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    /**
     * Create new account
     * @param \Expenses\Model\Account $account
     * @param string $entityId
     * @return mixed
     */
    public function create(PurchaseRequisitionCategory $purchaseRequisitionCategory)
    {
        $data = array(
            'purchaseRequisitionCategoryId' => $purchaseRequisitionCategory->purchaseRequisitionCategoryId,
            'purchaseRequisitionCategoryName' => $purchaseRequisitionCategory->purchaseRequisitionCategoryName,
            'purchaseRequisitionCategoryParentId' => $purchaseRequisitionCategory->purchaseRequisitionCategoryParentId,
            'purchaseRequisitionCategoryStatus' => $purchaseRequisitionCategory->purchaseRequisitionCategoryStatus,
            'entityId' => $purchaseRequisitionCategory->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function update($data, $purchaseRequisitionCategoryId)
    {
        if ($this->tableGateway->update($data, array('purchaseRequisitionCategoryId' => $purchaseRequisitionCategoryId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function changeStatus($status, $purchaseRequisitionCategoryId)
    {
        if ($this->tableGateway->update(array('purchaseRequisitionCategoryStatus' => $status), array('purchaseRequisitionCategoryId' => $purchaseRequisitionCategoryId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
