<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class DiscountSchemeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

     public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('discountSchemes')
                    ->columns(array('*'))
                    ->join('entity', 'discountSchemes.entityID = entity.entityID', array('deleted'), 'left')
                    ->order(array('discountSchemes.discountSchemesID DESC'))
                    ->where(array("entity.deleted" => 0));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function save($schemeData)
    {
        $scData = array(
            'discountSchemeCode' => $schemeData->discountSchemeCode,
            'discountSchemeName' => $schemeData->discountSchemeName,
            'discountType' => $schemeData->discountType,
            'locationID' => $schemeData->locationID,
            'entityID' => $schemeData->entityID
        );

        if ($this->tableGateway->insert($scData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getSchemeBySchemeID($discountSchemesID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('discountSchemes');
        $select->join('entity', 'discountSchemes.entityID = entity.entityID', array('deleted'), 'left');
        $select->where(array('discountSchemes.discountSchemesID' => $discountSchemesID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute()->current();

        return $rowset;
    }

    public function discountSchemeSearchByKey($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('discountSchemes');
        $select->join('entity', 'discountSchemes.entityID = entity.entityID', array('deleted'), 'left');
        $select->where(array('entity.deleted' => '0'));
        $select->where(new PredicateSet(array(new Operator('discountSchemeName', 'like', '%' . $searchKey . '%'), new Operator('discountSchemeCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function searchAccountsForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('discountSchemes')
                ->columns(array(
                    '*'
                    ))
                ->join('entity', 'discountSchemes.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0))
                ->where(new PredicateSet(array(new Operator('discountSchemeName', 'like', '%' . $searchKey . '%'), new Operator('discountSchemeCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));       
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getSchemeByCode($code)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('discountSchemes');
        $select->join('entity', 'discountSchemes.entityID = entity.entityID', array('deleted'), 'left');
        $select->where(array('entity.deleted' => 0, 'discountSchemeCode' => $code));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}