<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PosTemplateDetails
{

    public $posTemplateDetailsID;
    public $posTemplateDetailsAttribute;
    public $posTemplateDetailsIsSelected;
    public $posTemplateDetailsByDefaultSelected;

    public function exchangeArray($data)
    {
        $this->posTemplateDetailsID = (!empty($data['posTemplateDetailsID'])) ? $data['posTemplateDetailsID'] : null;
        $this->posTemplateDetailsAttribute = (!empty($data['posTemplateDetailsAttribute'])) ? $data['posTemplateDetailsAttribute'] : null;
        $this->posTemplateDetailsIsSelected = (!empty($data['posTemplateDetailsIsSelected'])) ? $data['posTemplateDetailsIsSelected'] : 0;
        $this->posTemplateDetailsByDefaultSelected = (!empty($data['posTemplateDetailsByDefaultSelected'])) ? $data['posTemplateDetailsByDefaultSelected'] : 0;
    }
}