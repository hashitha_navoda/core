<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class SmsConfigurationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('smsConfiguration');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    public function saveSmsConfiguration($confData)
    {
        $confData = array(
            'serviceProvider' => $confData->serviceProvider,
            'userName' => $confData->userName,
            'password' => $confData->password,
            'alias' => $confData->alias,
        );
        if ($this->tableGateway->insert($confData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateSmsConfiguration($smsConfigurationId, $confData)
    {
        $confData = array(
            'serviceProvider' => $confData->serviceProvider,
            'userName' => $confData->userName,
            'password' => $confData->password,
            'alias' => $confData->alias,
        );
        try {
            $this->tableGateway->update($confData, array('smsConfigurationId' => $smsConfigurationId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
}