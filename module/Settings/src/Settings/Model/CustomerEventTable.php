<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

/**
 * Description of CustomerEventTable
 *
 * @author shermilan
 */
class CustomerEventTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerEvent');
        $select->join('entity', 'customerEvent.entityID = entity.entityID', ['deleted'], 'left');
        $select->order(['customerEventName']);

        if ($paginated) {
            $paginatedAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatedAdapter);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function getByName($customerEventName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerEvent');
        $select->join('entity', 'customerEvent.entityID = entity.entityID', ['deleted'], 'left');
        $select->where(['customerEventName' => $customerEventName]);
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function save(CustomerEvent $customerEvent)
    {
        $data = [
            'customerEventName' => $customerEvent->customerEventName,
            'customerEventDuration' => $customerEvent->customerEventDuration,
            'customerEventStartDate' => $customerEvent->customerEventStartDate,
            'customerEventEndDate' => $customerEvent->customerEventEndDate,
            'customerEventStatus' => $customerEvent->customerEventStatus,
            'entityID' => $customerEvent->entityID,
        ];

        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function getByID($customerEventID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerEvent');
        $select->join('entity', 'customerEvent.entityID = entity.entityID', ['deleted'], 'left');
        $select->where(['deleted' => '0', 'customerEventID' => $customerEventID]);
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getByDate($fromDate, $todate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerEvent');
        $select->join('entity', 'customerEvent.entityID = entity.entityID', ['deleted'], 'left');
        $select->where(['deleted' => '0']);
        if ($fromDate != NULL && $todate != NULL) {
            $select->where->between("customerEventStartDate", $fromDate, $todate);
        }
        $statment = $sql->prepareStatementForSqlObject($select);
        $results = $statment->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function updateStatus($customerEventID, $customerEventStatus)
    {
        return $this->tableGateway->update(['customerEventStatus' => $customerEventStatus], ['customerEventID' => $customerEventID]);
    }

    public function update(CustomerEvent $customerEvent)
    {
        $data = [
            'customerEventName' => $customerEvent->customerEventName,
            'customerEventDuration' => $customerEvent->customerEventDuration,
            'customerEventStartDate' => $customerEvent->customerEventStartDate,
            'customerEventEndDate' => $customerEvent->customerEventEndDate,
        ];
        $this->tableGateway->update($data, ['customerEventID' => $customerEvent->customerEventID]);
        return TRUE; 
    }

    public function searchByName($customerEventName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerEvent');
        $select->join('entity', 'customerEvent.entityID = entity.entityID', ['deleted'], 'left');
        $select->where(new PredicateSet(array(new Operator('customerEventName', 'like', '%' . $customerEventName . '%'))));
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getActiveCutomerEvents()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerEvent');
        $select->join('entity', 'customerEvent.entityID = entity.entityID', ['deleted'], 'left');
        $select->where(['deleted' => '0']);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

}
