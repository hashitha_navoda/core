<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SmsTypeIncluded
{

    public $smsTypeIncludedId;
    public $smsTypeID;
    public $smsIncludedId;
    public $isSelected;
   
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->smsTypeIncludedId = (!empty($data['smsTypeIncludedId'])) ? $data['smsTypeIncludedId'] : null;
        $this->smsTypeID = (!empty($data['smsTypeID'])) ? $data['smsTypeID'] : null;
        $this->smsIncludedId = (!empty($data['smsIncludedId'])) ? $data['smsIncludedId'] : null;
        $this->isSelected = (!empty($data['isSelected'])) ? $data['isSelected'] : 0;
    }

}