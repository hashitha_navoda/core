<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SmsType
{

    public $smsTypeID;
    public $smsTypeName;
    public $smsTypeMessage;
    public $isIncludeInvoiceDetails;
    public $smsSendType;
    public $isEnable;
   
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->smsTypeID = (!empty($data['smsTypeID'])) ? $data['smsTypeID'] : null;
        $this->smsTypeName = (!empty($data['smsTypeName'])) ? $data['smsTypeName'] : null;
        $this->smsTypeMessage = (!empty($data['smsTypeMessage'])) ? $data['smsTypeMessage'] : null;
        $this->isIncludeInvoiceDetails = (!empty($data['isIncludeInvoiceDetails'])) ? $data['isIncludeInvoiceDetails'] : 0;
        $this->smsSendType = (!empty($data['smsSendType'])) ? $data['smsSendType'] : 1;
        $this->isEnable = (!empty($data['isEnable'])) ? $data['isEnable'] : 0;
        $this->smsTypeDayFrame = (!empty($data['smsTypeDayFrame'])) ? $data['smsTypeDayFrame'] : null;
        $this->smsTypeDayFrameDay = (!empty($data['smsTypeDayFrameDay'])) ? $data['smsTypeDayFrameDay'] : null;
        $this->smsTypeSelectedCustomerType = (!empty($data['smsTypeSelectedCustomerType'])) ? $data['smsTypeSelectedCustomerType'] : null;
    }

}