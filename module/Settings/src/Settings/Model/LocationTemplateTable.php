<?php
namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class LocationTemplateTable{
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getLocationTemplate($locationID, $documentTypeID){
    	$sql = new Sql($this->tableGateway->getAdapter());
    	$select = $sql->select();
    	$select->from('locationTemplate');
    	$select->columns(array('documentTypeID', 'templateID'));
    	$select->where(array('locationID = ' . $locationID));
    	if(is_array($documentTypeID)){
	    	$select->where->in('documentTypeID', $documentTypeID);
    	}else{
    		$select->where(array('documentTypeID = ' . $documentTypeID));
    	}

    	$query = $sql->prepareStatementForSqlObject($select);
    	$rowset = $query->execute();

    	return $rowset;
    }

    public function saveLocationTemplate($locationTemplate){
    	$data = array(
    		'locationID' => $locationTemplate->locationID,
    		'documentTypeID' => $locationTemplate->documentTypeID,
    		'templateID' => $locationTemplate->templateID
    	);

    	$this->tableGateway->insert($data);
    	return $this->tableGateway->lastInsertValue;
    }

    public function updateLocationTemplate($data, $locationID, $documentTypeID){
    	$this->tableGateway->update($data, array('locationID' => $locationID, 'documentTypeID' => $documentTypeID));
    	return true;
    }

}
