<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ApprovalWorkflowLimits
{

    public $approvalWorkflowLimitsID;
    public $approvalWorkflowID;
    public $approvalWorkflowLimitMin;
    public $approvalWorkflowLimitMax;
   
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->approvalWorkflowLimitsID = (!empty($data['approvalWorkflowLimitsID'])) ? $data['approvalWorkflowLimitsID'] : null;
        $this->approvalWorkflowID = (!empty($data['approvalWorkflowID'])) ? $data['approvalWorkflowID'] : null;
        $this->approvalWorkflowLimitMin = (!empty($data['approvalWorkflowLimitMin'])) ? $data['approvalWorkflowLimitMin'] : null;
        $this->approvalWorkflowLimitMax = (!empty($data['approvalWorkflowLimitMax'])) ? $data['approvalWorkflowLimitMax'] : null;
    }

}