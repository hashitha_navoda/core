<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Db\Sql\Sql;

class SmsTypeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginator = false)
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function getSmsTypeBySmsTypeName($name)
    {
        $rowset = $this->tableGateway->select(array('smsTypeName' =>  $name));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    /**
     * Update smsType 
     * @param array $data
     * @param int $id
     * @return boolean
     */
    public function updateSmsType($data, $id)
    {
        try {
            $this->tableGateway->update($data, ['smsTypeID' => (int) $id]);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

}
