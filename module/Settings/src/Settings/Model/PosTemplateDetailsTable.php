<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class PosTemplateDetailsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('posTemplateDetails');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }


    /**
     * Fetch all data
     * @return table 
     */
    public function fetchAllData()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * Update smsType 
     * @param array $data
     * @param int $id
     * @return boolean
     */
    public function updateTemplateDetails($data,$id)
    {
        try {
            $this->tableGateway->update($data, array('posTemplateDetailsID' => (int) $id));
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }
}