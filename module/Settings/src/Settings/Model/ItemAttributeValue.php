<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ItemAttributeValue
{

    public $itemAttributeValueID;
    public $itemAttributeID;
    public $itemAttributeValueDescription;
    public $itemAttributeValueUomID;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->itemAttributeValueID = (!empty($data['itemAttributeValueID'])) ? $data['itemAttributeValueID'] : null;
        $this->itemAttributeID = (!empty($data['itemAttributeID'])) ? $data['itemAttributeID'] : null;
        $this->itemAttributeValueDescription = (!empty($data['itemAttributeValueDescription'])) ? $data['itemAttributeValueDescription'] : null;
        $this->itemAttributeValueUomID = (!empty($data['itemAttributeValueUomID'])) ? $data['itemAttributeValueUomID'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : 0;
        
    }

}