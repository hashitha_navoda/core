<?php

/**
 * @author Damith Thamara   <damith@thinkcube.com>
 * This file contains tax table related funtions
 */

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Exception;
use Zend\Db\Sql\Expression;

class TaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adpater = $this->tableGateway->getAdapter();
        $productcategory = new TableGateway('tax', $adpater);
        $rowset = $productcategory->select(function (Select $select) {
            $select->order('taxType DESC');
        });
        return $rowset;
    }

    public function getTax($id)
    {
        $id = (int) $id;
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('tax');
        $select->join('taxCompound', 'tax.id = taxCompound.compoundTaxID', array(
            'simpleTaxID', 'usedTax' => new Expression("(SELECT COUNT(simpleTaxID) from taxCompound WHERE simpleTaxID = '" . $id . "')")), 'left');
        $select->where(array('tax.id = ?' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count() == 0) {
            return NULL;
        }

        return $results;
    }

    public function getSimpleTax($ID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('tax');
        $select->where(array('tax.id = ?' => $ID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = $results->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getAllTax($taxID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('tax');
        $select->join('taxCompound', 'tax.id = taxCompound.compoundTaxID', array('simpleTaxID'), 'left');
        $select->join(['sTax' => 'tax'], 'taxCompound.simpleTaxID = sTax.id', ['sTaxName' => 'taxName', 'sTaxPrecentage' => 'taxPrecentage', 'sState' => 'state'], 'left');
        if (!is_null($taxID)) {
            $select->where->equalTo('tax.id',$taxID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if ($results->count() == 0) {
            return NULL;
        } else {
            return $results;
        }
    }

    public function getTaxByName($taxname)
    {
        $rowset = $this->tableGateway->select(array('taxName' => $taxname));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getTaxRates()
    {
        $rowset = $this->tableGateway->select();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function saveTax(Tax $tax)
    {
        $data = array(
            'taxName' => $tax->taxName,
            'taxType' => $tax->taxType,
            'taxPrecentage' => $tax->taxPrecentage,
            'state' => $tax->state,
            'taxSuspendable' => $tax->taxSuspendable,
            'taxSalesAccountID' => $tax->taxSalesAccountID,
            'taxPurchaseAccountID' => $tax->taxPurchaseAccountID,
        );
        try {
            $this->tableGateway->insert($data);
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;
        } catch (Exception $e) {
            return NULL;
        }
    }

    public function updateTax($data, $id)
    {
        $this->tableGateway->update($data, array('id' => $id));
    }

    public function deleteTax($tax_id)
    {
        try {
            $result = $this->tableGateway->delete(array('id' => $tax_id));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * get count of rows what wizardFlag is 1
     * @return null|boolean
     */
    public function checkWizardFlag()
    {
        $rowset = $this->tableGateway->select(array('wizardFlag' => 1));

        if (count($rowset) == 0) {
            return NULL;
        }
        return true;
    }

    //getTaxes  by tax ids.
    public function getTaxes(array $taxes)
    {
        // zend where->in expects array keys to be of type integer
        $taxes = array_values($taxes);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('tax')
                ->where->in('tax.id', $taxes);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        //conver result to a array and return
        return iterator_to_array($results); 
    }

}
