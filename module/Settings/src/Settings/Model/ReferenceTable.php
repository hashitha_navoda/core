<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

Class ReferenceTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function selectAllForDefaultCheck($reference)
    {
        $rowset = $this->tableGateway->select(array('referenceNameID' => $reference));
        return $rowset;
    }

    public function selectAll()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('reference')
                ->columns(array('referenceNameID'))
                ->join('referencePrefix', 'reference.referenceNameID = referencePrefix.referenceNameID', array(
                    'referencePrefixID',
                    'locationID',
                    'referencePrefixCHAR',
                    'referencePrefixNumberOfDigits',
                    'referencePrefixCurrentReference',
                        ), 'left')
                ->where(array('reference.referenceTypeID' => '0'))
        ->where->isNull('referencePrefix.locationID');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function selectAllForView()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('reference')
                ->columns(array('referenceNameID'))
                ->where(array('reference.referenceTypeID' => '1'));


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getReferenceType($reference)
    {

        $rowset = $this->tableGateway->select(array('referenceNameID' => $reference));
        $row = $rowset->current();
        if ($row) {
            return $row->referenceID;
        } else {
            return '0';
        }
    }

    public function getReference(Reference $reference)
    {
        $rowset = $this->tableGateway->select(array('referenceNameID' => $reference->locationReferenceID));
        $row = $rowset->current();

        return $row;
    }

    public function getReferenceByLocationReferenceID($locationReferenceID)
    {
        $rowset = $this->tableGateway->select(array('locationReferenceID' => $locationReferenceID));
        $row = $rowset->current();
        return $row;
    }

    public function getReferenceByLocationID($locationID)
    {
        $rowset = $this->tableGateway->select(array('locationID' => $locationID));
        return $rowset;
    }

    public function getReferenceByReferenceID($referenceID)
    {
        $rowset = $this->tableGateway->select(array('referenceID' => $referenceID));
        return $rowset;
    }

    public function getReferenceByType($id)
    {
        $rowset = $this->tableGateway->select(array('referenceType' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function saveReference(reference $reference)
    {

        $data = array(
            'referenceNameID' => $reference->locationReferenceID,
            'referenceName' => $reference->referenceName,
            'referenceTypeID' => $reference->referenceTypeID,
        );
        $this->tableGateway->insert($data);
        return true;
    }

    public function saveDefaultReference($refName, $refType, $refID)
    {
        $data = array(
            'referenceNameID' => $refID,
            'referenceName' => $refName,
            'referenceTypeID' => $refType,
        );
        $this->tableGateway->insert($data);
        return true;
    }

    public function updateReference(Reference $reference)
    {
        $data = array(
            'currentReference' => $reference->currentReference,
        );

        if ($this->getReference($reference->referenceID)) {
            $this->tableGateway->update($data, array('referenceID' => $reference->referenceID));
        } else {
            throw new \Exception('Reference id does not exist');
        }

        return true;
    }

    public function updateReferenceByID($reference, $id)
    {
        if ($id) {
            $this->tableGateway->update($reference, array('referenceNameID' => $id));
        } else {
            throw new \Exception('Reference id does not exist');
        }
        return true;
    }

    public function updateCategoryReferenceByID(reference $reference)
    {
        $data = array(
            'currentReference' => $reference->currentReference,
        );

        if ($this->getReference($reference->referenceID)) {
            $this->tableGateway->update($data, array('referenceID' => $reference->referenceID));
        } else {
            throw new \Exception('Reference id does not exist');
        }

        return true;
    }

    public function updateReferenceTypeByID(reference $reference)
    {
        $data = array(
            'referenceType' => $reference->referenceType,
        );
        if ($this->getReference($reference->referenceID)) {
            $this->tableGateway->update($data, array('referenceID' => $reference->referenceID));
        } else {
            throw new \Exception('Reference id does not exist');
        }

        return true;
    }

    /**
     * Get existing reference ids
     */
    public function getExistingReferenceIds()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('reference')
                ->columns(array('referenceNameID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $referenceIds = [];

        foreach ($results as $result) {
            $referenceIds[] = $result['referenceNameID'];
        }

        return $referenceIds;
    }

}
