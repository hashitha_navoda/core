<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class ApprovalWorkflowLimitApproversTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    public function saveWorkflowLimitApprovers($officialData)
    {
        $data = array(
            'workflowLimitID' => $officialData->workflowLimitID,
            'workflowLimitApprover' => $officialData->workflowLimitApprover
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function deleteByWFLimitId($workflowLimitID)
    {
        if ($this->tableGateway->delete(array('workflowLimitID' => $workflowLimitID))) {
            return true;
        } else {
            return false;
        }
    }

    public function getLimitApproversByWFLimitId($wfLimitId)
    {
        $results = $this->tableGateway->select(array('workflowLimitID' => $wfLimitId));
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = (array) $result;
        }
        return $resultsArray;
    } 
}