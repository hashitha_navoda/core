<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Document Type Table Functions
 */

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class DocumentTypeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function getDocumentTypeByName($name)
    {
        $rowset = $this->tableGateway->select(array('documentTypeName' => $name));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }
}
