<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ApprovalWorkflows
{

    public $approvalWorkflowID;
    public $workflowName;
    public $workflowCode;
    public $rootDocumentTypeID;
    public $workflowStatus;
    public $entityId;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->approvalWorkflowID = (!empty($data['approvalWorkflowID'])) ? $data['approvalWorkflowID'] : null;
        $this->workflowName = (!empty($data['workflowName'])) ? $data['workflowName'] : null;
        $this->workflowCode = (!empty($data['workflowCode'])) ? $data['workflowCode'] : null;
        $this->rootDocumentTypeID = (!empty($data['rootDocumentTypeID'])) ? $data['rootDocumentTypeID'] : null;
        $this->workflowStatus = (!empty($data['workflowStatus'])) ? $data['workflowStatus'] : null;
        $this->entityId = (!empty($data['entityId'])) ? $data['entityId'] : null;
    }

}