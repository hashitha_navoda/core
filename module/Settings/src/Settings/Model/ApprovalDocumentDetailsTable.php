<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class ApprovalDocumentDetailsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvalDocumentDetails');
        $select->where(array('isShow' => '1'));
        $select->join('documentType', 'approvalDocumentDetails.docID =documentType.documentTypeID', array("*"), "left");
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }



    public function getActiveWFDocumentTypes()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvalDocumentDetails');
        $select->where(array('isShow' => '1', 'isActive' => 1));
        $select->join('documentType', 'approvalDocumentDetails.docID =documentType.documentTypeID', array("*"), "left");
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $dataSet = [];
        if (sizeof($result) > 0) {

            foreach ($result as $key => $value) {
                $dataSet[] = $value;
            }
        }

        return $dataSet;
    }


    public function checkDocIsActiveForApprovalWF($docID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvalDocumentDetails');
        $select->where(array('docID' => $docID));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        // $dataSet = [];
        // if (sizeof($result) > 0) {

        //     foreach ($result as $key => $value) {
        //         $dataSet[] = $value;
        //     }
        // }

        return $result->current();
    }


    public function updateWFDoc($dataSet, $docID)
    {
        try {
            $this->tableGateway->update($dataSet, array('docID' => $docID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
        return true;
    }

}