<?php

namespace Settings\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Description of CustomerEvent
 *
 * @author shermilan
 */
class CustomerEvent implements InputFilterAwareInterface
{

    public $customerEventID;
    public $customerEventName;
    public $customerEventDuration;
    public $customerEventStartDate;
    public $customerEventEndDate;
    public $customerEventStatus;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->customerEventID = (isset($data['customerEventID'])) ? $data['customerEventID'] : null;
        $this->customerEventName = (isset($data['customerEventName'])) ? $data['customerEventName'] : null;
        $this->customerEventDuration = (isset($data['customerEventDuration'])) ? $data['customerEventDuration'] : null;
        $this->customerEventStartDate = (isset($data['customerEventStartDate'])) ? $data['customerEventStartDate'] : null;
        $this->customerEventEndDate = (isset($data['customerEventEndDate'])) ? $data['customerEventEndDate'] : null;
        $this->customerEventStatus = (isset($data['customerEventStatus'])) ? $data['customerEventStatus'] : 1;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : null;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                    $factory->createInput(array(
                        'name' => 'customerEventID',
                        'required' => FALSE,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Int',
                            ),
                        ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'customerEventName',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'StringLength',
                                        'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min' => 1,
                                            'max' => 100,
                                        ),
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'customerEventDuration',
                                'required' => false,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Int',
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'customerEventStartDate',
                                'required' => false,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Date',
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'customerEventEndDate',
                                'required' => false,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Date',
                                    ),
                                ),
                            )
                    )
            );
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

}
