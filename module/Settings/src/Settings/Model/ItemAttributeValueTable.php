<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class ItemAttributeValueTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemAttribute');
        $select->order('jobTypeName DESC');
        $select->join('entity', 'jobType.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function saveItemAttrValues(ItemAttributeValue $itemAttrValues)
    {
        $itemAttributeData = array(
            'itemAttributeID' => $itemAttrValues->itemAttributeID,
            'itemAttributeValueDescription' => $itemAttrValues->itemAttributeValueDescription,
            'itemAttributeValueUomID' => $itemAttrValues->itemAttributeValueUomID,
            'entityID' => $itemAttrValues->entityID,
        );

        $this->tableGateway->insert($itemAttributeData);
        return $this->tableGateway->lastInsertValue;
    }

    public function getSubValueByItemAtributeID($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('itemAttributeValue')
                ->columns(array('*'))
                ->join('entity', 'itemAttributeValue.entityID=  entity.entityID', array("*"), "left")
                ->join('uom', 'itemAttributeValue.itemAttributeValueUomID=uom.uomID', array('*'), 'left')
                ->where(array('itemAttributeValue.itemAttributeID' => $id, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    
    }

    public function selectAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemAttributeValue');
        $select->join(array('iAVE' => 'entity'), 'itemAttributeValue.entityID = iAVE.entityID',array('e' => 'deleted'));
        $select->join('itemAttribute', 'itemAttributeValue.itemAttributeID = itemAttribute.itemAttributeID',
             array('*'),'left');
        $select->join(array('iAE' => 'entity'), 'itemAttribute.entityID = iAE.entityID');
        $select->where(array('iAE.deleted'=> 0 , 'iAVE.deleted' => 0 ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }


    public function getAttributeValuesByAttributeId($itemAttributeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('itemAttributeValue')
                ->columns(array('itemAttributeValueDescription', 'itemAttributeValueID'))
                ->join('entity', 'itemAttributeValue.entityID = entity.entityID', array("*"), "left")
                ->where(array('itemAttributeValue.itemAttributeID' => $itemAttributeID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;

    }

    public function getSubValueByItemAtributeIdAndProductID($itemAttributeID, $productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('itemAttributeValue')
                ->columns(array('itemAttributeValueDescription'))
                ->join('entity', 'itemAttributeValue.entityID = entity.entityID', array("*"), "left")
                ->join('itemAttributeValueMap', 'itemAttributeValue.itemAttributeValueID = itemAttributeValueMap.itemAttributeValueID', array("itemAttributeValueID"), "left")
                ->join('itemAttributeMap', 'itemAttributeValueMap.itemAttributeMapID = itemAttributeMap.itemAttributeMapID', array("productID"), "left")
                ->where(array('itemAttributeMap.itemAttributeID' => $itemAttributeID, 'entity.deleted' => 0, 'itemAttributeMap.productID' => $productID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;

    
    }


    public function getValueByItemAtributeValueID($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('itemAttributeValue')
                ->columns(array('*'))
                ->where(array('itemAttributeValue.itemAttributeValueID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    
    }
   
}