<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PromotionEmailCustomerEvent
{

    public $promotionEmailCustomerEventId;
    public $promotionEmail;
    public $promotionEmailCustomerEvent;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->promotionEmailCustomerEventId = (!empty($data['promotionEmailCustomerEventId'])) ? $data['promotionEmailCustomerEventId'] : null;
        $this->promotionEmail = (!empty($data['promotionEmail'])) ? $data['promotionEmail'] : null;
        $this->promotionEmailCustomerEvent = (!empty($data['promotionEmailCustomerEvent'])) ? $data['promotionEmailCustomerEvent'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
