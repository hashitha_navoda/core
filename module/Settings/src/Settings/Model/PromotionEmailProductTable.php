<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

Class PromotionEmailProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(PromotionEmailProduct $promotionEmailProduct)
    {
        $promotionEmailProductData = array(
            'promotionEmailProductID' => $promotionEmailProduct->promotionEmailProductID,
            'promotionEmail' => $promotionEmailProduct->promotionEmail,
            'promotionEmailProduct' => $promotionEmailProduct->promotionEmailProduct,
        );

        $this->tableGateway->insert($promotionEmailProductData);
        return $this->tableGateway->lastInsertValue;
    }

}
