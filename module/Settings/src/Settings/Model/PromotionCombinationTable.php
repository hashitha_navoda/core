<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\IsNull;

Class PromotionCombinationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(PromotionCombination $promotionCombination)
    {
        $combinationData = array(
            'combinationName' => $promotionCombination->combinationName,
            'conditionType' => $promotionCombination->conditionType,
            'promotionID' => $promotionCombination->promotionID,
            'discountType' => $promotionCombination->discountType,
            'discountAmount' => $promotionCombination->discountAmount
        );

        $this->tableGateway->insert($combinationData);
        return $this->tableGateway->lastInsertValue;
    }


    public function getRelatedCombinationsByPromotionId($promotionID) {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotionCombination');
        $select->where(array('promotionID' => $promotionID));
       
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function deleteByPromotionID($promotionID)
    {
        return $this->tableGateway->delete(array('promotionID' => $promotionID));
    }

    
}
