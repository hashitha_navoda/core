<?php

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethodLoyaltyCard implements InputFilterAwareInterface
{

    public $incomingPaymentMethodLoyaltyCardID;
    public $customerLoyaltyID;
    public $incomingPaymentID;
    public $collectedPoints;
    public $loyalty_points;
    public $loyalty_card_create_date;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodLoyaltyCardID = (!empty($data['incomingPaymentMethodLoyaltyCardID'])) ? $data['incomingPaymentMethodLoyaltyCardID'] : Null;
        $this->customerLoyaltyID = (!empty($data['customerLoyaltyID'])) ? $data['customerLoyaltyID'] : Null;
        $this->incomingPaymentID = (!empty($data['incomingPaymentID'])) ? $data['incomingPaymentID'] : Null;
        $this->collectedPoints = (!empty($data['collectedPoints'])) ? $data['collectedPoints'] : 0;
        $this->redeemedPoints = (!empty($data['redeemedPoints'])) ? $data['redeemedPoints'] : 0;
        $this->expired = (!empty($data['expired'])) ? $data['expired'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
