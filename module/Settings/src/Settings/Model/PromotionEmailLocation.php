<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PromotionEmailLocation
{

    public $promotionEmailLocationID;
    public $promotionEmail;
    public $promotionEmailLocation;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->promotionEmailLocationID = (!empty($data['promotionEmailLocationID'])) ? $data['promotionEmailLocationID'] : null;
        $this->promotionEmail = (!empty($data['promotionEmail'])) ? $data['promotionEmail'] : null;
        $this->promotionEmailLocation = (!empty($data['promotionEmailLocation'])) ? $data['promotionEmailLocation'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
