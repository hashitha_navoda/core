<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TemplateSignature
{

    public $templateSignatureID;
    public $templateID;
    public $signatureID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->templateSignatureID = (!empty($data['templateSignatureID'])) ? $data['templateSignatureID'] : null;
        $this->templateID = (!empty($data['templateID'])) ? $data['templateID'] : null;
        $this->signatureID = (!empty($data['signatureID'])) ? $data['signatureID'] : null;
    }

}