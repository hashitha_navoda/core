<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the TaxCompound Model
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TaxCompound implements InputFilterAwareInterface
{

    public $id;
    public $compoundTaxID;
    public $simpleTaxID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->compoundTaxID = (!empty($data['compoundTaxID'])) ? $data['compoundTaxID'] : null;
        $this->simpleTaxID = (!empty($data['simpleTaxID'])) ? $data['simpleTaxID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

