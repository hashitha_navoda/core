<?php

namespace Settings\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;


class PurchaseRequisitionType implements InputFilterAwareInterface
{

    public $purchaseRequisitionTypeId;
    public $purchaseRequisitionTypeName;
    public $purchaseRequisitionTypeCategory;
    public $purchaseRequisitionTypeApproverEnabled;
    public $purchaseRequisitionTypeStatus;
    public $entityId;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->purchaseRequisitionTypeId = (isset($data['purchaseRequisitionTypeId'])) ? $data['purchaseRequisitionTypeId'] : null;
        $this->purchaseRequisitionTypeName = (isset($data['purchaseRequisitionTypeName'])) ? $data['purchaseRequisitionTypeName'] : null;
        $this->purchaseRequisitionTypeCategory = (isset($data['purchaseRequisitionTypeCategory'])) ? $data['purchaseRequisitionTypeCategory'] : null;
        $this->purchaseRequisitionTypeApproverEnabled = (isset($data['purchaseRequisitionTypeApproverEnabled'])) ? $data['purchaseRequisitionTypeApproverEnabled'] : 0;
        $this->purchaseRequisitionTypeStatus = (isset($data['purchaseRequisitionTypeStatus'])) ? $data['purchaseRequisitionTypeStatus'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
