<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoicePromotion
{

    public $invoicePromotionID;
    public $invoicePromotionPromotionID;
    public $invoicePromotionMinValue;
    public $invoicePromotionMaxValue;
    public $invoicePromotionDiscountType;
    public $invoicePromotionDiscountAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->invoicePromotionID = (!empty($data['invoicePromotionID'])) ? $data['invoicePromotionID'] : null;
        $this->invoicePromotionPromotionID = (!empty($data['invoicePromotionPromotionID'])) ? $data['invoicePromotionPromotionID'] : null;
        $this->invoicePromotionMinValue = (!empty($data['invoicePromotionMinValue'])) ? $data['invoicePromotionMinValue'] : null;
        $this->invoicePromotionMaxValue = (!empty($data['invoicePromotionMaxValue'])) ? $data['invoicePromotionMaxValue'] : null;
        $this->invoicePromotionDiscountType = (!empty($data['invoicePromotionDiscountType'])) ? $data['invoicePromotionDiscountType'] : null;
        $this->invoicePromotionDiscountAmount = (!empty($data['invoicePromotionDiscountAmount'])) ? $data['invoicePromotionDiscountAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
