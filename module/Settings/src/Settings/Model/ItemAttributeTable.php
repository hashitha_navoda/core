<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class ItemAttributeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemAttribute');
        $select->join('entity', 'itemAttribute.entityID = entity.entityID', array('deleted'));
        $select->where(array('deleted' => '0'));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function saveItemAttribute(ItemAttribute $itemAttribute)
    {
        $itemAttributeData = array(
            'itemAttributeCode' => $itemAttribute->itemAttributeCode,
            'itemAttributeName' => $itemAttribute->itemAttributeName,
            'itemAttributeState' => $itemAttribute->itemAttributeState,
            'entityID' => $itemAttribute->entityID,
        );

        $this->tableGateway->insert($itemAttributeData);
        return $this->tableGateway->lastInsertValue;
    }

    public function updateItemAttrState($itemAttrID, $status)
    {
        $data = array(
            'itemAttributeState' => $status,
        );
        try {
            $this->tableGateway->update($data, array('itemAttributeID' => $itemAttrID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getDataByItemAttrID($itemAttributeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemAttribute');
        $select->where(array('itemAttribute.itemAttributeID' => $itemAttributeID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updateItemAttribute($dataSet)
    {
        $itemAttrDetails = array(
            'itemAttributeName' => $dataSet['itemAttributeName'],
            'itemAttributeState' => $dataSet['itemAttributeState'],
        );
        try {
            $this->tableGateway->update($itemAttrDetails, array('itemAttributeID' => $dataSet['itemAttributeID']));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function itemAttributeSearchByKey($key)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemAttribute')
                ->columns(array('*'))
                ->join('entity', 'itemAttribute.entityID=  entity.entityID', array("deleted"), "left");
        $select->where(
            new PredicateSet(array(new Operator('itemAttribute.itemAttributeCode', 'like', '%' . $key . '%'), 
            new Operator('itemAttribute.itemAttributeName', 'like', '%' . $key . '%')),
             PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getDetailsByItemAttributeCode($itemAttrCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemAttribute');
        $select->where(array('itemAttribute.itemAttributeCode' => $itemAttrCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

   
}