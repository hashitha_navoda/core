<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Db\Sql\Sql;

class DraftMessageTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginator = false)
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(DraftMessage $draftMessage)
    {
        $draftMessageData = array(
            'draftMessageName' => $draftMessage->draftMessageName,
            'draftMessageMessage' => $draftMessage->draftMessageMessage,
            'draftMessageDate' => $draftMessage->draftMessageDate,
            'entityID' => $draftMessage->entityID,
        );
        
        $this->tableGateway->insert($draftMessageData);
        return $this->tableGateway->lastInsertValue;
    }

    /**
     * Update smsType Included
     * @param array $data
     * @param int $id
     * @return boolean
     */
    public function updateDraftMessage($data, $id)
    {
        try {
            $this->tableGateway->update($data, ['draftMessageID' => (int) $id]);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Get loyalty card holder details
     * @param array $customerIds
     * @param array $loyaltyCardIds
     * @param array $customerLoyaltyCodes
     * @return mixed
     */
    public function getSmsTypeIncludedById($smsTypeIncludedId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('smsTypeIncluded')
                ->join('smsIncluded', 'smsTypeIncluded.smsIncludedId = smsIncluded.smsIncludedId', ['smsIncludedName'], 'inner');
        $select->where->equalTo('smsTypeIncluded.smsTypeIncludedId', (int) $smsTypeIncludedId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

}
