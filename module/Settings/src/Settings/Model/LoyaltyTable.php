<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Db\Sql\Sql;

class LoyaltyTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginator = false)
    {
        $resultSet = $this->tableGateway->select();
        if ($paginator) {
            $cards = array();
            while ($row = $resultSet->current()) {
                $cards[] = $row;
            }
            $adapter = new ArrayAdapter($cards);
            return new Paginator($adapter);
        }

        return $resultSet;
    }

    public function save(Loyalty $loyalty)
    {
        $data = array(
            'loyaltyName' => $loyalty->name,
            'loyaltyPrefix' => $loyalty->prefix,
            'loyaltyCodeDegits' => $loyalty->code_degits,
            'loyaltyCodeCurrentNo' => $loyalty->code_current_no,
            'loyaltyNoOfCards' => $loyalty->no_of_cards,
            'loyaltyActiveMinVal' => $loyalty->active_min_val,
            'loyaltyActiveMaxVal' => $loyalty->active_max_val,
            'loyaltyEarningPerPoint' => $loyalty->earning_per_point,
            'loyaltyRedeemPerPoint' => $loyalty->redeem_per_point,
            'loyaltyStatus' => $loyalty->status,
            'loyaltyGlAccountID' => $loyalty->loyaltyGlAccountID,
        );

        if ((int) $loyalty->id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($cur_data = $this->getLoyalty($loyalty->id)) {

                $data = array(
                    'loyaltyName' => $data['loyaltyName'] == NULL ? $cur_data->name : $data['loyaltyName'],
                    'loyaltyPrefix' => $data['loyaltyPrefix'] == NULL ? $cur_data->prefix : $data['loyaltyPrefix'],
                    'loyaltyCodeDegits' => $data['loyaltyCodeDegits'] == NULL ? $cur_data->code_degits : $data['loyaltyCodeDegits'],
                    'loyaltyCodeCurrentNo' => $data['loyaltyCodeCurrentNo'] == NULL ? $cur_data->code_current_no : $data['loyaltyCodeCurrentNo'],
                    'loyaltyNoOfCards' => $data['loyaltyNoOfCards'] == NULL ? $cur_data->no_of_cards : $data['loyaltyNoOfCards'],
                    'loyaltyActiveMinVal' => $data['loyaltyActiveMinVal'] == NULL ? $cur_data->active_min_val : $data['loyaltyActiveMinVal'],
                    'loyaltyActiveMaxVal' => $data['loyaltyActiveMaxVal'] == NULL ? $cur_data->active_max_val : $data['loyaltyActiveMaxVal'],
                    'loyaltyEarningPerPoint' => $data['loyaltyEarningPerPoint'] == NULL ? $cur_data->earning_per_point : $data['loyaltyEarningPerPoint'],
                    'loyaltyRedeemPerPoint' => $data['loyaltyRedeemPerPoint'] == NULL ? $cur_data->redeem_per_point : $data['loyaltyRedeemPerPoint'],
                    'loyaltyStatus' => $data['loyaltyStatus'] == NULL ? $cur_data->status : $data['loyaltyStatus'],
                    'loyaltyGlAccountID' => $data['loyaltyGlAccountID'] == NULL ? $cur_data->loyaltyGlAccountID : $data['loyaltyGlAccountID'],
                );
                $this->tableGateway->update($data, array('loyaltyID' => $loyalty->id));
            } else {
                throw new \Exception('Loyalty Card id does not exist');
            }
        }

        return $loyalty;
    }

    public function getLoyalty($id)
    {
        $rowset = $this->tableGateway->select(array('loyaltyID' => (int) $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getLoyaltyList()
    {
        $rowset = $this->tableGateway->select(array('loyaltyStatus' => 1));
        if (!$rowset) {
            throw new \Exception("Could not find any row");
        }
        return $rowset;
    }

    public function getCurrentLoyaltyCode($id)
    {
        $rowset = $this->tableGateway->select(array('loyaltyID' => $id));
        if ($rowset) {
            $row = $rowset->current();
            return $row->prefix . str_pad($row->code_current_no, $row->code_degits, 0, STR_PAD_LEFT);
        } else {
            throw new \Exception("Could not find any row");
        }
        return false;
    }

    public function getAvailableLoyaltyCodesList($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $loyaltyCustomerTable = new TableGateway('customerLoyalty', $adapter);
        $loyaltyCards = $loyaltyCustomerTable->select(array('loyaltyID' => $id))->toArray();

        $usedCodesList = array_column($loyaltyCards, 'customerLoyaltyCode');

        $rowset = $this->tableGateway->select(array('loyaltyID' => $id));
        $codeFormat = $rowset->current();

        // if no loyalty card exists by the given name, return blank array
        if (!$codeFormat) {
            return array();
        }

        $noCards = $codeFormat->no_of_cards != null ? $codeFormat->no_of_cards : 0;
        $availableCards = array();
        $startingNo = $codeFormat->code_current_no;

        for ($startingNo; $noCards > count($availableCards); $startingNo++) {
            $nextCode = $codeFormat->prefix . str_pad(($startingNo), $codeFormat->code_degits, 0, STR_PAD_LEFT);

            if (!in_array($nextCode, $usedCodesList)) {
                $availableCards[$nextCode] = $nextCode;
            } else {
                $noCards = $noCards - 1;
            }
        }

        return $availableCards;
    }

    /**
     * Get loyalty cards for dropdown
     * @param string $searchKey
     * @return array
     */
    public function searchLoyaltyCardsForDropdown($searchKey)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('loyalty')
                ->where->like('loyaltyName', '%' . $searchKey . '%');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * Get loyalty card holder details
     * @param array $customerIds
     * @param array $loyaltyCardIds
     * @param array $customerLoyaltyCodes
     * @return mixed
     */
    public function getLoyaltyCardHoldersDetails($customerIds = [], $loyaltyCardIds = [], $customerLoyaltyCodes = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerLoyalty')
                ->join('loyalty', 'customerLoyalty.loyaltyID = loyalty.loyaltyID', ['loyaltyName'], 'left')
                ->join('customer', 'customerLoyalty.customerID = customer.customerID', ['customerName', 'customerCode', 'customerTelephoneNumber'], 'left');
        if ($customerIds) {
            $select->where->in('customerLoyalty.customerID', $customerIds);
        }
        if ($loyaltyCardIds) {
            $select->where->in('customerLoyalty.loyaltyID', $loyaltyCardIds);
        }
        if ($customerLoyaltyCodes) {
            $select->where->in('customerLoyalty.customerLoyaltyCode', $customerLoyaltyCodes);
        }
        $select->order('customer.customerCode ASE');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

}
