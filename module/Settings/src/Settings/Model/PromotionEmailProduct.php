<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PromotionEmailProduct
{

    public $promotionEmailProductID;
    public $promotionEmail;
    public $promotionEmailProduct;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->promotionEmailProductID = (!empty($data['promotionEmailProductID'])) ? $data['promotionEmailProductID'] : null;
        $this->promotionEmail = (!empty($data['promotionEmail'])) ? $data['promotionEmail'] : null;
        $this->promotionEmailProduct = (!empty($data['promotionEmailProduct'])) ? $data['promotionEmailProduct'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
