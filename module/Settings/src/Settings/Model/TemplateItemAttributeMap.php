<?php 
namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class TemplateItemAttributeMap
{

    public $templateItemAttributeMapID;
    public $templateID;
    public $attributeOneID;
    public $attributeTwoID;
    public $attributeThreeID;

    public function exchangeArray($data)
    {
        $this->templateItemAttributeMapID = (!empty($data['templateItemAttributeMapID'])) ? $data['templateItemAttributeMapID'] : null;
        $this->templateID = (!empty($data['templateID'])) ? $data['templateID'] : null;
        $this->attributeOneID = (!empty($data['attributeOneID'])) ? $data['attributeOneID'] : null;
        $this->attributeTwoID = (!empty($data['attributeTwoID'])) ? $data['attributeTwoID'] : null;
        $this->attributeThreeID = (!empty($data['attributeThreeID'])) ? $data['attributeThreeID'] : null;
              
    }

}