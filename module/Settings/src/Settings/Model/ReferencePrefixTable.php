<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

Class ReferencePrefixTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();

        return $rowset;
    }

    public function selectAllData($referenceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('referencePrefix')
                ->columns(array('*'))
                ->where(array('referencePrefix.referenceNameID' => $referenceID, new \Zend\Db\Sql\Predicate\IsNotNull('referencePrefix.locationID')));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getReference($id, $locationID = NULL)
    {

        $rowset = $this->tableGateway->select(array('referenceNameID' => $id, 'locationID' => $locationID));
        $row = $rowset->current();

        return $row;
    }

    public function getReferenceByLocationReferenceID($referencePrefixID)
    {
        $rowset = $this->tableGateway->select(array('referencePrefixID' => $referencePrefixID));
        $row = $rowset->current();
        return $row;
    }

    public function getReferenceByLocationID($locationID)
    {
        $rowset = $this->tableGateway->select(array('locationID' => $locationID));
        return $rowset;
    }

    public function getReferenceByReferenceID($referenceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('referencePrefix')
                ->columns(array('*'))
                ->where(array('referencePrefix.referenceNameID' => $referenceID, new \Zend\Db\Sql\Predicate\IsNull('referencePrefix.locationID')));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getReferenceByType($id)
    {
        $rowset = $this->tableGateway->select(array('referenceType' => $id));
        $row = $rowset->current();
        return $row;
    }

    public function saveReference(ReferencePrefix $reference)
    {
        $data = array(
            'referenceNameID' => $reference->referenceID,
            'locationID' => $reference->locationID,
            'referencePrefixCHAR' => $reference->referenceType,
            'referencePrefixnumberOfDigits' => $reference->numberOfDigits,
            'referencePrefixcurrentReference' => $reference->currentReference,
        );
        $this->tableGateway->insert($data);
        return true;
    }

    public function saveDefaultReference($nameID, $prefix, $digits, $current)
    {
        $data = array(
            'referenceNameID' => $nameID,
            'locationID' => null,
            'referencePrefixCHAR' => $prefix,
            'referencePrefixnumberOfDigits' => $digits,
            'referencePrefixcurrentReference' => $current,
        );
        $this->tableGateway->insert($data);
        return true;
    }

    public function updateReference(Reference $reference)
    {
        $data = array(
            'currentReference' => $reference->currentReference,
        );

        if ($this->getReference($reference->referenceID)) {
            $this->tableGateway->update($data, array('referenceID' => $reference->referenceID));
        } else {
            throw new \Exception('Reference id does not exist');
        }

        return true;
    }

    public function updateReferenceByID($reference, $referencePrefixID)
    {

        if (!$this->getReferenceByLocationReferenceID($referencePrefixID)) {
            return false;
        }
        try {
            $this->tableGateway->update($reference, array('referencePrefixID' => $referencePrefixID));
            return true;
        } catch (Exception $e) {
            return false;            
        }
        
    }

    public function updateCategoryReferenceByID(reference $reference)
    {
        $data = array(
            'currentReference' => $reference->currentReference,
        );

        if ($this->getReference($reference->referenceID)) {
            $this->tableGateway->update($data, array('referenceID' => $reference->referenceID));
        } else {
            throw new \Exception('Reference id does not exist');
        }

        return true;
    }

    public function updateReferenceTypeByID(reference $reference)
    {
        $data = array(
            'referenceType' => $reference->referenceType,
        );
        if ($this->getReference($reference->referenceID)) {
            $this->tableGateway->update($data, array('referenceID' => $reference->referenceID));
        } else {
            throw new \Exception('Reference id does not exist');
        }

        return true;
    }

    public function getReferenceByPrefixCharAndReferenceDigits($referenceChar, $referenceDigits, $nameID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('referencePrefix')
                ->columns(array("*"))
                ->where(array('referencePrefix.referencePrefixCHAR' => $referenceChar, 'referencePrefix.referencePrefixNumberOfDigits' => $referenceDigits))
        ->where->notEqualTo('referencePrefix.referenceNameID', $nameID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

}
