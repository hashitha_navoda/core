<?php
namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class LocationTemplate {
	public $locationTemplateID;
	public $locationID;
	public $documentTypeID;
	public $templateID;

	public function exchangeArray($data){
		$this->locationTemplateID = $data['locationTemplateID'] ? $data['locationTemplateID'] : null;
		$this->locationID = $data['locationID'] ? $data['locationID'] : null;
		$this->documentTypeID = $data['documentTypeID'] ? $data['documentTypeID'] : null;
		$this->templateID = $data['templateID'] ? $data['templateID'] : null;
	}

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
