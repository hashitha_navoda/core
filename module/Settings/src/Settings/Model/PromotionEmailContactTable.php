<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

Class PromotionEmailContactTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save($promotionEmailContact)
    {
        $promotionEmailContactData = array(
            'promotionEmailContactID' => $promotionEmailContact->promotionEmailContactID,
            'promotionEmailID' => $promotionEmailContact->promotionEmailID,
            'contactID' => $promotionEmailContact->contactID,
        );
        $this->tableGateway->insert($promotionEmailContactData);
        return $this->tableGateway->lastInsertValue;
    }

}
