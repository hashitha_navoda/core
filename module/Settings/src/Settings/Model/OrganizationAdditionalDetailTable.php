<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class OrganizationAdditionalDetailTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('oranizationAdditionalDetial');
        $select->where(array('deleted' => '0'));
        $select->order('oranizationAdditionalDetialID DESC');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getDetailsByOranizationAdditionalDetialName($oranizationAdditionalDetialName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('oranizationAdditionalDetial')
                ->columns(array('oranizationAdditionalDetialName'))
                ->where(array('oranizationAdditionalDetialName' => $oranizationAdditionalDetialName));
        $select->where(array('deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }   

    public function saveOrganizationData($data)
    {
        $data = array(
            'oranizationAdditionalDetialName' => $data->oranizationAdditionalDetialName,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

     //get dimension for search
    public function organizationDetailSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('oranizationAdditionalDetial')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in oranizationAdditionalDetialName )>0,POSITION(\'' . $keyword . '\' in oranizationAdditionalDetialName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in oranizationAdditionalDetialID )>0,POSITION(\'' . $keyword . '\' in oranizationAdditionalDetialID), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(oranizationAdditionalDetialName ), CHAR_LENGTH(oranizationAdditionalDetialID )) '),
                    '*'));
        $select->where(new PredicateSet(array(new Operator('oranizationAdditionalDetial.oranizationAdditionalDetialName', 'like', '%' . $keyword . '%'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->where(array('deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function updateDeleteDetail($oranizationAdditionalDetialID)
    {
        $dimensionData = array(
            'deleted' => '1',
        );
        try {
            $this->tableGateway->update($dimensionData, array('oranizationAdditionalDetialID' => $oranizationAdditionalDetialID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
}