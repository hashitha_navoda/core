<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DimensionValue
{

    public $dimensionValueId;
    public $dimensionId;
    public $dimensionValue;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->dimensionValueId = (!empty($data['dimensionValueId'])) ? $data['dimensionValueId'] : null;
        $this->dimensionId = (!empty($data['dimensionId'])) ? $data['dimensionId'] : null;
        $this->dimensionValue = (!empty($data['dimensionValue'])) ? $data['dimensionValue'] : null;
    }

}