<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class TemplateSignatureTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('templateSignature');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

   
    public function saveTempalteSignature($signatureData)
    {
        $signatureData = array(
            'templateID' => $signatureData->templateID,
            'signatureID' => $signatureData->signatureID,
        );
        if ($this->tableGateway->insert($signatureData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getTemplateSignatureByTemplateID($templateID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('templateSignature');
        $select->join('signatureDetails', 'templateSignature.signatureID=  signatureDetails.signatureDetailsID', array("signatureImageID","signatureHolderName"), "left");
        $select->where(array('templateID' => $templateID));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }


    public function deleteTemplateSignature($templateID)
    {
        try {
            $this->tableGateway->delete(array('templateID' => $templateID));
            return TRUE;
        } catch (\Exception $exc) {
            return FALSE;
        }
    }
}