<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\IsNull;

Class PromotionTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(Promotion $promotion)
    {
        $promotionData = array(
            'promotionName' => $promotion->promotionName,
            'promotionFromDate' => $promotion->promotionFromDate,
            'promotionToDate' => $promotion->promotionToDate,
            'promotionType' => $promotion->promotionType,
            'promotionLocation' => $promotion->promotionLocation,
            'promotionStatus' => $promotion->promotionStatus,
            'promotionDescription' => $promotion->promotionDescription,
            'entityID' => $promotion->entityID,
        );

        $this->tableGateway->insert($promotionData);
        return $this->tableGateway->lastInsertValue;
    }

    public function edit($data, $promotionID)
    {
        if ($this->tableGateway->update($data, array('promotionID' => $promotionID))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @param locationID Plase pass global to retrieve global products only.
     * Passing a locationID will returtn promotions with global promotions
     */
    public function getPromotions($paginated = FALSE, $locationID = FALSE, $status = FALSE, $withDeleted = FALSE)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotion');
        $select->join('entity', 'promotion.entityID= entity.entityID', array('deleted'));
        if (!$withDeleted) {
            $select->where(array('deleted' => '0'));
        }
        if ($locationID == 'global') {
            $select->where(new PredicateSet(array(new IsNull('promotionLocation'))));
        } else if ($locationID != NULL) {
            $select->where(new PredicateSet(array(new IsNull('promotionLocation'), new Operator('promotionLocation', '=', $locationID)), PredicateSet::OP_OR));
        }

        if ($status != NULL) {
            $select->where(array('promotionStatus' => $status));
        }

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            $promotionList = array();
            foreach ($results as $result) {
                $promotionList[] = $result;
            }
            return $promotionList;
        }
    }

    public function getPromotion($promotionID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotion');
        $select->where(array('promotionID' => $promotionID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    public function getPromotionWithDetails($promotionID, $promotionType = 1)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotion');
        if ($promotionType == 1) {
            // $select->join('promotionProduct', 'promotion.promotionID= promotionProduct.promotionProductPromotionID');
            // $select->join('product', 'promotionProduct.promotionProductProductID = product.productID', array('*', 'promoProductID' => new Expression('product.productID')));
            // $select->join('locationProduct', 'product.productID = locationProduct.productID AND promotion.promotionLocation=locationProduct.locationID', array('*'), 'left');
        } else if ($promotionType == 2) {
            $select->join('invoicePromotion', 'promotion.promotionID= invoicePromotion.invoicePromotionPromotionID');
        }
        $select->where(array('promotionID' => $promotionID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $promotionDetailsList = array();
        foreach ($results as $result) {
            $promotionDetailsList[] = $result;
        }
        return $promotionDetailsList;
    }

    public function changeStatus($status, $promotionID)
    {
        if ($this->tableGateway->update(array('promotionStatus' => $status), array('promotionID' => $promotionID))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     * get today related all active promotions
     */
    public function getToDayPromotions($today, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotion');
        $select->join('entity', 'promotion.entityID= entity.entityID', array('deleted'));
        $select->join('invoicePromotion', 'promotion.promotionID = invoicePromotion.invoicePromotionPromotionID', array('*'),'left');
        $select->where(array('deleted' => '0'));
        $select->where(new PredicateSet(array(new IsNull('promotionLocation'), new Operator('promotionLocation', Operator::OPERATOR_EQUAL_TO)), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('promotionFromDate', Operator::OPERATOR_LESS_THAN_OR_EQUAL_TO), new Operator('promotionToDate', Operator::OPERATOR_GREATER_THAN_OR_EQUAL_TO)), PredicateSet::OP_AND));
        $select->where(array('promotionStatus' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $today = date('Y-m-d', strtotime($today));
        $results = $statement->execute([0, $locationID, $today, $today]);
        $promotionList = array();
        foreach ($results as $result) {
            $promotionList[] = $result;
        }
        return $promotionList;
    }
   

    /**
    * get itemwise promotion details that related to the given promotionID.
    * @param int $promotionID
    * return array
    *
    */
    public function getItemWisePromotionDetails($promotionID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotion');
        $select->join('promotionProduct', 'promotion.promotionID = promotionProduct.promotionProductPromotionID',array('promotionProductProductID'),'left');
        $select->where(array('promotionID' => $promotionID,'promotionType'=>1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $promotionDetailsList = array();
        foreach ($results as $result) {
            $promotionDetailsList[] = $result['promotionProductProductID'];
        }
        return $promotionDetailsList;
    }

    public function getPromotionDetailsByInvoiceID($invoiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotion');
        $select->join('promotionProduct', 'promotion.promotionID = promotionProduct.promotionProductPromotionID',array('promotionProductProductID'),'left');
        $select->join('salesInvoice', 'promotion.promotionID = salesInvoice.promotionID',array('salesInvoiceID'));
        $select->join('invoicePromotion', 'promotion.promotionID = invoicePromotion.invoicePromotionPromotionID',array('invoicePromotionDiscountType','invoicePromotionDiscountAmount'));
        $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID));
       
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $promotionDetails = array();
        foreach ($results as $result) {
            $promotionDetails[] = $result;
        }
        return $promotionDetails;
    }

    //get all promotion without deleted ones.
    public function getAllPromotion(){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('promotion');
        $select->join('entity', 'promotion.entityID = entity.entityID',array('*'),'left');
        $select->where(array('entity.deleted' => 0));
       
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

}
