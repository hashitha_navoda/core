<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class DimensionTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dimension');
        $select->where(array('deleted' => '0'));
        $select->order('dimensionId DESC');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getDimensionByDimensionName($dimensionName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('dimension')
                ->columns(array('dimensionName'))
                ->where(array('dimensionName' => $dimensionName));
        $select->where(array('deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }   

    public function saveDimension($dimensionData)
    {
        $dimensionData = array(
            'dimensionName' => $dimensionData->dimensionName,
        );
        if ($this->tableGateway->insert($dimensionData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDimensionByDimensionID($dimensionId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('dimension');
        $select->where(array('dimension.dimensionId' => $dimensionId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

     //get dimension for search
    public function dimensionSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dimension')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in dimensionName )>0,POSITION(\'' . $keyword . '\' in dimensionName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in dimensionId )>0,POSITION(\'' . $keyword . '\' in dimensionId), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(dimensionName ), CHAR_LENGTH(dimensionId )) '),
                    '*'));
        $select->where(new PredicateSet(array(new Operator('dimension.dimensionName', 'like', '%' . $keyword . '%'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->where(array('deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function updateDeleteDimension($dimensionId)
    {
        $dimensionData = array(
            'deleted' => '1',
        );
        try {
            $this->tableGateway->update($dimensionData, array('dimensionId' => $dimensionId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
}