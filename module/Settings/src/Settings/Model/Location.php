<?php

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Location implements InputFilterAwareInterface
{

    public $locationID;
    public $locationCode;
    public $locationName;
    public $locationAddressLine1;
    public $locationAddressLine2;
    public $locationAddressLine3;
    public $locationAddressLine4;
    public $locationEmail;
    public $locationTelephone;
    public $locationFax;
    public $locationTax;
    public $locationStatus;
    public $entityID;
    public $inputFilter;

    public function exchangeArray($data)
    {
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : 0;
        $this->locationCode = (!empty($data['locationCode'])) ? $data['locationCode'] : NULL;
        $this->locationName = (!empty($data['locationName'])) ? $data['locationName'] : NULL;
        $this->locationAddressLine1 = (!empty($data['locationAddressLine1'])) ? $data['locationAddressLine1'] : NULL;
        $this->locationAddressLine2 = (!empty($data['locationAddressLine2'])) ? $data['locationAddressLine2'] : NULL;
        $this->locationAddressLine3 = (!empty($data['locationAddressLine3'])) ? $data['locationAddressLine3'] : NULL;
        $this->locationAddressLine4 = (!empty($data['locationAddressLine4'])) ? $data['locationAddressLine4'] : NULL;
        $this->locationEmail = (!empty($data['locationEmail'])) ? $data['locationEmail'] : NULL;
        $this->locationTelephone = (!empty($data['locationTelephone'])) ? $data['locationTelephone'] : NULL;
        $this->locationFax = (!empty($data['locationFax'])) ? $data['locationFax'] : NULL;
        $this->locationTax = (!empty($data['locationTax'])) ? $data['locationTax'] : 1;
        $this->locationStatus = (!empty($data['locationStatus'])) ? $data['locationStatus'] : NULL;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : NULL;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationCode',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 30,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationName',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
//            $inputFilter->add($factory->createInput(array(
//                        'name' => 'locationEmail',
//                        'validators' => array(
//                            array(
//                                'name' => 'EmailAddress',
//                                'options' => array(
//                                    'message' => 'This is not a valid email address'
//                                )
//                            ),
//                        ),
//            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
