<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class ApprovalWorkflowsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvalWorkflows');
        $select->join('entity', 'approvalWorkflows.entityID =entity.entityID', array("deleted"), "left");
        $select->join('documentType', 'approvalWorkflows.rootDocumentTypeID =documentType.documentTypeID', array("*"), "left");
        $select->where(array('entity.deleted' => 0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function changeStatus($status, $approvalWorkflowID)
    {
        if ($this->tableGateway->update(array('workflowStatus' => $status), array('approvalWorkflowID' => $approvalWorkflowID))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function saveApprovalWorkflows($officialData)
    {
        $data = array(
            'workflowName' => $officialData->workflowName,
            'workflowCode' => $officialData->workflowCode,
            'rootDocumentTypeID' => $officialData->rootDocumentTypeID,
            'workflowStatus' => $officialData->workflowStatus,
            'entityId' => $officialData->entityId
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getWorkflowDetailsByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvalWorkflows')
                ->columns(array('*'))
                ->join('approvalWorkflowLimits', 'approvalWorkflows.approvalWorkflowID = approvalWorkflowLimits.approvalWorkflowID', array('*'), 'left')
                ->join('workflowLimitApprovers', 'approvalWorkflowLimits.approvalWorkflowLimitsID = workflowLimitApprovers.workflowLimitID', array('*'), 'left')
                ->join('documentType', 'approvalWorkflows.rootDocumentTypeID = documentType.documentTypeID', array('*'), 'left')
                ->where(array('approvalWorkflows.approvalWorkflowID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function update($data, $approvalWorkflowID)
    {
        try {
            $this->tableGateway->update($data, array('approvalWorkflowID' => $approvalWorkflowID));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public function getWFByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvalWorkflows')
                ->columns(array('*'))
                ->where(array('approvalWorkflows.approvalWorkflowID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }


    public function getApprovalWorkflowsByDocID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('approvalWorkflows')
                ->columns(array('*'))
                ->join('entity', 'approvalWorkflows.entityID =entity.entityID', array("deleted"), "left")
                ->where(array('approvalWorkflows.rootDocumentTypeID' => $id, 'entity.deleted' => 0, 'approvalWorkflows.workflowStatus' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function checkApprovalWorkflowsByCode($workflowCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('approvalWorkflows')
                ->columns(array('*'))
                ->join('entity', 'approvalWorkflows.entityID =entity.entityID', array("deleted"), "left")
                ->where(array('approvalWorkflows.workflowCode' => $workflowCode, 'entity.deleted' => 0, 'approvalWorkflows.workflowStatus' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    
}