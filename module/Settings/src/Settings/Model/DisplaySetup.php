<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains company Model Functions
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DisplaySetup
{

    public $displaySetupID;
    public $currencyID;
    public $timeZone;
    public $dateFormat;
    public $timeFormat;
    public $invoiceTemplate;
    public $quotationTemplate;
    public $salesOrderTemplate;
    public $paymentsTemplate;
    public $advancePaymentsTemplate;
    public $FIFOCostingFlag;
    public $averageCostingFlag;
    public $jobBusinessType;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->displaySetupID = (!empty($data['displaySetupID'])) ? $data['displaySetupID'] : null;
        $this->currencyID = (!empty($data['currencyID'])) ? $data['currencyID'] : null;
        $this->timeZone = (!empty($data['timeZone'])) ? $data['timeZone'] : null;
        $this->dateFormat = (!empty($data['dateFormat'])) ? $data['dateFormat'] : null;
        $this->timeFormat = (!empty($data['timeFormat'])) ? $data['timeFormat'] : null;
        $this->invoiceTemplate = (!empty($data['invoiceTemplate'])) ? $data['invoiceTemplate'] : null;
        $this->quotationTemplate = (!empty($data['quotationTemplate'])) ? $data['quotationTemplate'] : null;
        $this->salesOrderTemplate = (!empty($data['salesOrderTemplate'])) ? $data['salesOrderTemplate'] : null;
        $this->paymentsTemplate = (!empty($data['paymentsTemplate'])) ? $data['paymentsTemplate'] : null;
        $this->advancePaymentsTemplate = (!empty($data['advancePaymentsTemplate'])) ? $data['advancePaymentsTemplate'] : null;
        $this->FIFOCostingFlag = (!empty($data['FIFOCostingFlag'])) ? $data['FIFOCostingFlag'] : 0;
        $this->averageCostingFlag = (!empty($data['averageCostingFlag'])) ? $data['averageCostingFlag'] : 0;
        $this->customerCreditLimitApplyFlag = (!empty($data['customerCreditLimitApplyFlag'])) ? $data['customerCreditLimitApplyFlag'] : 0;
        $this->jobBusinessType = (!empty($data['jobBusinessType'])) ? $data['jobBusinessType'] : null;
        $this->posPrintoutImageConversionStatus = (!empty($data['posPrintoutImageConversionStatus'])) ? $data['posPrintoutImageConversionStatus'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'displaySetupID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'currencyID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'timeZone',
                        'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'dateFormat',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'timeFormat',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
