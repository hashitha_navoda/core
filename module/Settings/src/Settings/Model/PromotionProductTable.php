<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

Class PromotionProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(PromotionProduct $promotionProduct)
    {
        $promotionProductData = array(
            'promotionProductPromotionID' => $promotionProduct->promotionProductPromotionID,
            'promotionProductProductID' => $promotionProduct->promotionProductProductID,
            'promotionProductMinQty' => $promotionProduct->promotionProductMinQty,
            'promotionProductMaxQty' => $promotionProduct->promotionProductMaxQty,
            'promotionProductDiscountType' => $promotionProduct->promotionProductDiscountType,
            'promotionProductDiscountAmount' => $promotionProduct->promotionProductDiscountAmount,
        );

        $this->tableGateway->insert($promotionProductData);
        return $this->tableGateway->lastInsertValue;
    }

    public function deleteByPromotionID($promotionID)
    {
        return $this->tableGateway->delete(array('promotionProductPromotionID' => $promotionID));
    }

}
