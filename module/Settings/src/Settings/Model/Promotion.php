<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Promotion
{

    public $promotionID;
    public $promotionName;
    public $promotionFromDate;
    public $promotionToDate;
    public $promotionType;
    public $promotionLocation;
    public $promotionStatus;
    public $promotionDescription;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->promotionID = (!empty($data['promotionID'])) ? $data['promotionID'] : null;
        $this->promotionName = (!empty($data['promotionName'])) ? $data['promotionName'] : null;
        $this->promotionFromDate = (!empty($data['promotionFromDate'])) ? $data['promotionFromDate'] : null;
        $this->promotionToDate = (!empty($data['promotionToDate'])) ? $data['promotionToDate'] : null;
        $this->promotionType = (!empty($data['promotionType'])) ? $data['promotionType'] : null;
        $this->promotionLocation = (!empty($data['promotionLocation'])) ? $data['promotionLocation'] : null;
        $this->promotionStatus = (!empty($data['promotionStatus'])) ? $data['promotionStatus'] : null;
        $this->promotionDescription = (!empty($data['promotionDescription'])) ? $data['promotionDescription'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
