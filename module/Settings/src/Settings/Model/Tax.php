<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the Tax Model
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Tax implements InputFilterAwareInterface
{

    public $id;
    public $taxName;
    public $taxType;
    public $taxPrecentage;
    public $state;
    public $taxSuspendable;
    public $taxSalesAccountID;
    public $taxPurchaseAccountID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->taxName = (!empty($data['taxName'])) ? $data['taxName'] : null;
        $this->taxType = (!empty($data['taxType'])) ? $data['taxType'] : null;
        $this->taxPrecentage = (!empty($data['taxPrecentage'])) ? $data['taxPrecentage'] : null;
        $this->state = (!empty($data['state'])) ? $data['state'] : 1;
        $this->taxSuspendable = (!empty($data['taxSuspendable'])) ? $data['taxSuspendable'] : 0;
        $this->taxSalesAccountID = (!empty($data['taxSalesAccountID'])) ? $data['taxSalesAccountID'] : 0;
        $this->taxPurchaseAccountID = (!empty($data['taxPurchaseAccountID'])) ? $data['taxPurchaseAccountID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'taxName',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'taxType',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'taxPrecentage',
                        'required' => false,
                        'validators' => array(
                            array('name' => 'Float'),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

