<?php

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CustomerLoyalty implements InputFilterAwareInterface
{

    public $id;
    public $loyalty_id;
    public $customer_id;
    public $loyalty_code;
    public $loyalty_points;
    public $loyalty_card_create_date;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['customerLoyaltyID'])) ? $data['customerLoyaltyID'] : Null;
        $this->loyalty_id = (!empty($data['loyaltyID'])) ? $data['loyaltyID'] : Null;
        $this->customer_id = (!empty($data['customerID'])) ? $data['customerID'] : Null;
        $this->loyalty_code = (!empty($data['customerLoyaltyCode'])) ? $data['customerLoyaltyCode'] : Null;
        $this->loyalty_points = (!empty($data['customerLoyaltyPoints'])) ? $data['customerLoyaltyPoints'] : Null;
        $this->loyalty_card_create_date = (!empty($data['customerLoyaltyCreateDate'])) ? $data['customerLoyaltyCreateDate'] : Null;
        $this->status = (!empty($data['customerLoyaltyStatus'])) ? $data['customerLoyaltyStatus'] : 1;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
