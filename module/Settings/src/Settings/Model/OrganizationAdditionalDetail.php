<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class OrganizationAdditionalDetail
{

    public $oranizationAdditionalDetialID;
    public $oranizationAdditionalDetialName;
    public $deleted;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->oranizationAdditionalDetialID = (!empty($data['oranizationAdditionalDetialID'])) ? $data['oranizationAdditionalDetialID'] : null;
        $this->oranizationAdditionalDetialName = (!empty($data['oranizationAdditionalDetialName'])) ? $data['oranizationAdditionalDetialName'] : null;
        $this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : null;
    }

}