<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class AdministrationOfficial
{

    public $administrationOfficialID;
    public $firstName;
    public $lastName;
    public $telephoneNo;
    public $nic;
    public $address;
    public $email;
    public $dateOfBirth;
    public $gender;
    public $entityID;
    public $status;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->administrationOfficialID = (!empty($data['administrationOfficialID'])) ? $data['administrationOfficialID'] : null;
        $this->firstName = (!empty($data['firstName'])) ? $data['firstName'] : null;
        $this->lastName = (!empty($data['lastName'])) ? $data['lastName'] : null;
        $this->telephoneNo = (!empty($data['telephoneNo'])) ? $data['telephoneNo'] : null;
        $this->nic = (!empty($data['nic'])) ? $data['nic'] : null;
        $this->address = (!empty($data['address'])) ? $data['address'] : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
        $this->dateOfBirth = (!empty($data['dateOfBirth'])) ? $data['dateOfBirth'] : null;
        $this->gender = (!empty($data['gender'])) ? $data['gender'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : null;
    }

}