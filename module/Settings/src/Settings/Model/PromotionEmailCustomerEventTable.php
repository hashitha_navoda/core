<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

Class PromotionEmailCustomerEventTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(PromotionEmailCustomerEvent $promotionEmailCustomerEvent)
    {
        $promotionEmailCustomerEventData = array(
            'promotionEmailCustomerEventId' => $promotionEmailCustomerEvent->promotionEmailCustomerEventId,
            'promotionEmail' => $promotionEmailCustomerEvent->promotionEmail,
            'promotionEmailCustomerEvent' => $promotionEmailCustomerEvent->promotionEmailCustomerEvent,
        );

        $this->tableGateway->insert($promotionEmailCustomerEventData);
        return $this->tableGateway->lastInsertValue;
    }

}
