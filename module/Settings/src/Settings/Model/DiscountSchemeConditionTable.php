<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class DiscountSchemeConditionTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function save($schemeConData)
    {

        $scData = array(
            'discountSchemesID' => $schemeConData->discountSchemesID,
            'minQty' => $schemeConData->minQty,
            'maxQty' => $schemeConData->maxQty,
            'discountType' => $schemeConData->discountType,
            'discountPercentage' => $schemeConData->discountPercentage,
            'discountValue' => $schemeConData->discountValue,
        );

        if ($this->tableGateway->insert($scData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getSchemeConditionsBySchemeID($discountSchemesID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('discountSchemeConditions');
        $select->where(array('discountSchemeConditions.discountSchemesID' => $discountSchemesID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        $results = [];
        foreach ($rowset as $key => $value) {
            $results[] = $value;
        }



        return $results;
    }
}