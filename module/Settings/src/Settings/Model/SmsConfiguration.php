<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SmsConfiguration
{

    public $smsConfigurationId;
    public $userName;
    public $password;
    public $alias;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->smsConfigurationId = (!empty($data['smsConfigurationId'])) ? $data['smsConfigurationId'] : null;
        $this->userName = (!empty($data['userName'])) ? $data['userName'] : null;
        $this->password = (!empty($data['password'])) ? $data['password'] : null;
        $this->alias = (!empty($data['alias'])) ? $data['alias'] : null;
        $this->serviceProvider = (!empty($data['serviceProvider'])) ? $data['serviceProvider'] : null;
    }

}