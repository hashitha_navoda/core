<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PromotionEmail
{

    public $promotionEmailID;
    public $promotionEmailName;
    public $promotionEmailSubject;
    public $promotionEmailContent;
    public $promotionEmailImage;
    public $entityID;
    public $isContactBasePromoEmail;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->promotionEmailID = (!empty($data['promotionEmailID'])) ? $data['promotionEmailID'] : null;
        $this->promotionEmailName = (!empty($data['promotionEmailName'])) ? $data['promotionEmailName'] : null;
        $this->promotionEmailSubject = (!empty($data['promotionEmailSubject'])) ? $data['promotionEmailSubject'] : null;
        $this->promotionEmailContent = (!empty($data['promotionEmailContent'])) ? $data['promotionEmailContent'] : null;
        $this->promotionEmailImage = (!empty($data['promotionEmailImage'])) ? $data['promotionEmailImage'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->isContactBasePromoEmail = (!empty($data['isContactBasePromoEmail'])) ? $data['isContactBasePromoEmail'] : false;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
