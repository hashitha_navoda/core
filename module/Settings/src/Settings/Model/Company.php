<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains company Model Functions
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Company
{

    public $id;
    public $companyName;
    public $companyAddress;
    public $postalCode;
    public $telephoneNumber;
    public $telephoneNumber2;
    public $telephoneNumber3;
    public $faxNumber;
    public $email;
    public $country;
    public $website;
    public $brNumber;
    public $trNumber;
    public $logoID;
    public $wizardState;
    public $companyGracePeriod;
    public $companyAccountType;
    public $userGracePeriod;
    public $companyLicenseExpireDate;
    public $apiKey;
    public $companyUseAccounting;
    public $taxRegNumber;
    public $additionalTextForPos;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->companyName = (!empty($data['companyName'])) ? $data['companyName'] : null;
        $this->companyAddress = (!empty($data['companyAddress'])) ? $data['companyAddress'] : '';
        $this->postalCode = (!empty($data['postalCode'])) ? $data['postalCode'] : null;
        $this->telephoneNumber = (!empty($data['telephoneNumber'])) ? $data['telephoneNumber'] : null;
        $this->telephoneNumber2 = (!empty($data['telephoneNumber2'])) ? $data['telephoneNumber2'] : null;
        $this->telephoneNumber3 = (!empty($data['telephoneNumber3'])) ? $data['telephoneNumber3'] : null;
        $this->faxNumber = (!empty($data['faxNumber'])) ? $data['faxNumber'] : null;
        $this->email = (!empty($data['email'])) ? $data['email'] : null;
        $this->country = (!empty($data['country'])) ? $data['country'] : null;
        $this->website = (!empty($data['website'])) ? $data['website'] : null;
        $this->brNumber = (!empty($data['brNumber'])) ? $data['brNumber'] : null;
        $this->trNumber = (!empty($data['taxRegNumber'])) ? $data['taxRegNumber'] : null;
        $this->oldTaxRegNumber = (!empty($data['oldTaxRegNumber'])) ? $data['oldTaxRegNumber'] : null;
        $this->logoID = (!empty($data['logoID'])) ? $data['logoID'] : null;
        $this->wizardState = (!empty($data['wizardState'])) ? $data['wizardState'] : null;
        $this->companyGracePeriod = (!empty($data['companyGracePeriod'])) ? $data['companyGracePeriod'] : 0;
        $this->userGracePeriod = (!empty($data['userGracePeriod'])) ? $data['userGracePeriod'] : 0;
        $this->companyLicenseExpireDate = (!empty($data['companyLicenseExpireDate'])) ? $data['companyLicenseExpireDate'] : '0000-00-00 00:00:00';
        $this->companyAccountType = (!empty($data['companyAccountType'])) ? $data['companyAccountType'] : 1;
        $this->apiKey = (!empty($data['apiKey'])) ? $data['apiKey'] : null;
        $this->companyUseAccounting = (!empty($data['companyUseAccounting'])) ? $data['companyUseAccounting'] : 0;
        $this->taxRegNumber = (!empty($data['taxRegNumber'])) ? $data['taxRegNumber'] : null;
        $this->additionalTextForPos = (!empty($data['additionalTextForPos'])) ? $data['additionalTextForPos'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'id',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'companyName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'companyAddress',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'postalCode',
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'telephoneNumber',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'email',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'country',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'website',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'brNumber',
                        'required' => false,
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
