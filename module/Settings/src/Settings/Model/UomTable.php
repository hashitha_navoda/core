<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Unit Measure Table Functions
 */

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class UomTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select('uom');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Uom());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $adpater = $this->tableGateway->getAdapter();
        $tg = new TableGateway('uom', $adpater);
        $rowset = $tg->select(function (Select $select) {
            $select->order('uomAbbr ASC');
        });

        return $rowset;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return return active row set
     */
    public function activeFetchAll()
    {
        $rowset = $this->tableGateway->select(function (Select $select) {
            $select->where(array('uomState' => 1))->order('uomDecimalPlace DESC');
        });

        return $rowset;
    }

    public function getUom($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('uomID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function getUomByName($name)
    {
        $rowset = $this->tableGateway->select(array('uomName' => $name));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function getUomByAbbr($abbr)
    {
        $rowset = $this->tableGateway->select(array('abbrevation' => $abbr));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function saveUom(Uom $uom)
    {

        // if uomID is passed, update current record
        if ($uom->uomID != null) {
            return $this->updateUom($uom);
        }

        $data = array(
            'uomName' => $uom->uomName,
            'uomAbbr' => $uom->uomAbbr,
            'uomDecimalPlace' => $uom->uomDecimalPlace,
            'uomState' => $uom->uomState,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateUom(Uom $uom)
    {
        $data = array(
            'uomAbbr' => $uom->uomAbbr,
            'uomName' => $uom->uomName,
            'uomDecimalPlace' => $uom->uomDecimalPlace,
            'uomState' => $uom->uomState,
        );
        $this->tableGateway->update($data, array('uomID' => $uom->uomID));
        return true;
    }

    public function updateUomState(Uom $uom)
    {
        $data = array(
            'uomState' => $uom->uomState,
        );
        $this->tableGateway->update($data, array('uomID' => $uom->uomID));
        return true;
    }

    public function deleteUom($id)
    {
        try {
            $this->tableGateway->delete(array('uomID' => $id));
            return TRUE;
        } catch (\Exception $exc) {
            return FALSE;
        }
    }

    public function getUomforSearch($keyword)
    {
        $tbl = $this->tableGateway;
        $adaptor = $tbl->getAdapter();
        $sql = new Sql($adaptor);
        $select = $sql->select();
        $select->from('uom');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in uomName )>0,POSITION(\'' . $keyword . '\' in uomName), 9999),'
                    . 'IF(POSITION(\'' . $keyword . '\' in uomAbbr )>0,POSITION(\'' . $keyword . '\' in uomAbbr), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(uomName ), CHAR_LENGTH(uomAbbr )) '),
            '*',
        ));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->where(new PredicateSet(array(new Operator('uomName', 'like', '%' . $keyword . '%'), new Operator('uomAbbr', 'like', '%' . $keyword . '%')), PredicateSet::OP_OR));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        $uomArray = array();
        foreach ($rowset as $val) {
            $uomArray[] = (object) $val;
        }
        return (object) $uomArray;
//        $rowset = $tbl->select(function (Select $select) use ($keyword) {
//            $select->where->like('uomName', $keyword . '%')->or->like('uomAbbr', $keyword . '%');
//            $select->order('uomName ASC')->limit(2);
//        });
//        return $rowset;
    }

    public function checkUomForInactive($uomID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->where(array('productUom.uomID' => $uomID, 'productUom.productUomBase' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function checkUomForDelete($uomID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('productUom')
                ->columns(array('*'))
                ->where(array('productUom.uomID' => $uomID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * get count of rows what wizardFlag is 1
     * @return null|boolean
     */
    public function checkWizardFlag()
    {
        $rowset = $this->tableGateway->select(array('wizardFlag' => 1));

        if (count($rowset) == 0) {
            return NULL;
        }
        return true;
    }

}
