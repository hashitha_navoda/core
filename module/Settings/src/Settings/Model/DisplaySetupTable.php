<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains DisplaySetupTable Functions
 */

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class DisplaySetupTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function fetchAllDetails()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('displaySetup');
        $select->join('currency', 'currency.currencyID = displaySetup.currencyID', array('*'));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    public function getDisplay($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('displaySetupID' => $id));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $id");
        }
        return $row;
    }

    public function savedefaultDisplaySetup($value)
    {
        $value = (int) $value;

        $data = array(
            'currencyID' => $value,
        );
        $this->tableGateway->update($data, array('displaySetupID' => '1'));
    }

    public function saveDisplaySetup(DisplaySetup $DisplaySetup)
    {

        $id = (int) $DisplaySetup->displaySetupID;
        $data = array(
            'currencyID' => $DisplaySetup->currencyID,
            'timeZone' => $DisplaySetup->timeZone,
            'dateFormat' => $DisplaySetup->dateFormat,
            'timeFormat' => $DisplaySetup->timeFormat,
            'invoiceTemplate' => $DisplaySetup->invoiceTemplate,
            'quotationTemplate' => $DisplaySetup->quotationTemplate,
            'salesOrderTemplate' => $DisplaySetup->salesOrderTemplate,
            'paymentsTemplate' => $DisplaySetup->paymentsTemplate,
            'advancePaymentsTemplate' => $DisplaySetup->advancePaymentsTemplate,
            'FIFOCostingFlag' => $DisplaySetup->FIFOCostingFlag,
            'averageCostingFlag' => $DisplaySetup->averageCostingFlag,
            'customerCreditLimitApplyFlag' => $DisplaySetup->customerCreditLimitApplyFlag,
            'posPrintoutImageConversionStatus' => $DisplaySetup->posPrintoutImageConversionStatus,
        );

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getDisplay($id)) {
                if (!$data['currencyID'])
                    unset($data['currencyID']);

                $this->tableGateway->update($data, array('displaySetupID' => $id));
            } else {
                throw new \Exception('company id does not exist');
            }
        }
        return 1;
    }

     public function saveCostingMethod(DisplaySetup $DisplaySetup)
    {

        $id = (int) $DisplaySetup->displaySetupID;
        $data = array(
            'FIFOCostingFlag' => $DisplaySetup->FIFOCostingFlag,
            'averageCostingFlag' => $DisplaySetup->averageCostingFlag
        );

        if ($id == 0) {
            $this->tableGateway->insert($data);
        } else {
            if ($this->getDisplay($id)) {
                if (!$data['currencyID'])
                    unset($data['currencyID']);

                $this->tableGateway->update($data, array('displaySetupID' => $id));
            } else {
                throw new \Exception('company id does not exist');
            }
        }
        return 1;
    }

    public function getSystemCurrency()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('displaySetup')
            ->columns(array('*'))
            ->join('currency', 'displaySetup.currencyID = currency.currencyID', array('*'),'left');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result; 
    }

    /**
     * Update display setup details
     */
    public function updateDisplaySetupDetails($data, $id)
    {
        try {
            $this->tableGateway->update($data, ['displaySetupID' => $id]);
            return true;
        } catch (\Exception $ex) {
            return false;
        }
    }

}
