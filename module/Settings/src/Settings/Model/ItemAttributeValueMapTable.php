<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class ItemAttributeValueMapTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveItemAttributeValueMap($itemAttributeValueMap)
    {
        $itemAttributeData = array(
            'itemAttributeMapID' => $itemAttributeValueMap['itemAttributeMapID'],
            'itemAttributeValueID' => $itemAttributeValueMap['itemAttributeValueID'],
        );

        $this->tableGateway->insert($itemAttributeData);
        return $this->tableGateway->lastInsertValue;
    }
    public function deleteMapDetailsByitemAttrValueMapID($itemAttributeValueMapID)
    {
        try {
            $result = $this->tableGateway->delete(array('itemAttributeValueMapID' => $itemAttributeValueMapID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }
    public function getDetaisByItemAttrValueID($itemAttrValueID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemAttributeValueMap');
        $select->join('itemAttributeMap','itemAttributeValueMap.itemAttributeMapID = itemAttributeMap.itemAttributeMapID', array('productID'),'left');
        $select->join('product','itemAttributeMap.productID = product.productID', array('entityID'),'left');
        $select->join('entity','product.entityID = entity.entityID', array('deleted'),'left');
        $select->where(array('itemAttributeValueMap.itemAttributeValueID' => $itemAttrValueID, 'entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}