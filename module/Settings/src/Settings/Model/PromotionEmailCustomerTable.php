<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

Class PromotionEmailCustomerTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(PromotionEmailCustomer $promotionEmailCustomer)
    {
        $promotionEmailCustomerData = array(
            'promotionEmailCustomerID' => $promotionEmailCustomer->promotionEmailCustomerID,
            'promotionEmail' => $promotionEmailCustomer->promotionEmail,
            'promotionEmailCustomer' => $promotionEmailCustomer->promotionEmailCustomer,
        );
        $this->tableGateway->insert($promotionEmailCustomerData);
        return $this->tableGateway->lastInsertValue;
    }

}
