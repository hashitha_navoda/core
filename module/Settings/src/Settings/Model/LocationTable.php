<?php

namespace Settings\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class LocationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('location')
                    ->columns(array('*'))
                    ->join('entity', 'location.entityID = entity.entityID', array('deleted'), 'left')
                    ->order('location.locationName ASC')
                    ->where(array('entity.deleted' => '0'));
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('location')
                ->columns(array('*'))
                ->join('entity', 'location.entityID = entity.entityID', array('deleted'), 'left')
                ->order('location.locationName ASC')
                ->where(array('entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return $resultSet;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     *
     * This will return whole active locations
     * @return array return locationID s
     */
    public function getActiveLocations()
    {
        $adpater = $this->tableGateway->getAdapter();
        $location = new TableGateway('location', $adpater);
        $rowset = $location->select(function (Select $select) {
            $select->where(array('locationStatus' => 1));
            $select->columns(array('locationID'));
        });
        return $rowset;
    }

    public function gerActiveUserLocation($userID)
    {
        try {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('userLocation')
                    ->columns(array('*'))
                    ->join('location', 'userLocation.locationID = location.locationID', array('*'), 'left')
                    ->join('entity', 'location.entityID = entity.entityID', array('deleted'), 'left')
                    ->order('location.locationName ASC')
                    ->where(array('entity.deleted' => '0', 'userLocation.userID' => $userID, 'location.locationStatus' => 1));
            $statement = $sql->prepareStatementForSqlObject($select);
            $resultSet = $statement->execute();
            return $resultSet;
        } catch (\Exception $exc) {
            echo $exc;
        }exit();
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return return active row set
     */
    public function activeFetchAll()
    {
        $rowset = $this->tableGateway->select(array('locationStatus' => 1));
        return $rowset;
    }

    public function getLocation($locationID)
    {
        $locationID = (int) $locationID;
        $rowset = $this->tableGateway->select(array('locationID' => $locationID));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $locationID");
        }
        return $row;
    }

    public function saveLocation(Location $location, $entityID)
    {
        $data = array(
            'locationID' => $location->locationID,
            'locationCode' => $location->locationCode,
            'locationName' => $location->locationName,
            'locationAddressLine1' => $location->locationAddressLine1,
            'locationAddressLine2' => $location->locationAddressLine2,
            'locationAddressLine3' => $location->locationAddressLine3,
            'locationAddressLine4' => $location->locationAddressLine4,
            'locationEmail' => $location->locationEmail,
            'locationTelephone' => $location->locationTelephone,
            'locationFax' => $location->locationFax,
            'locationTax' => $location->locationTax,
            'locationStatus' => $location->locationStatus,
            'entityID' => $entityID
        );

        $locationID = (int) $location->locationID;

        if ($locationID == 0) {
            if ($this->tableGateway->insert($data)) {
                $id = $this->tableGateway->lastInsertValue;
                return $id;
            } else {
                return FALSE;
            }
        } else {
            if ($this->getLocation($locationID)) {
                if ($this->tableGateway->update($data, array('locationID' => $locationID))) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                throw new \Exception('Form id does not exist');
                return FALSE;
            }
        }
    }

    public function deleteLocation($locationID)
    {
        try {
            $result = $this->tableGateway->delete(array('locationID' => $locationID));
            return array('status' => TRUE, 'exception' => NULL);
        } catch (\Exception $e) {
            return array('status' => FALSE, 'exception' => $e->getMessage());
        }
    }

    public function getLocationsforSearch($searchLocationString)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $searchLocationString . '\' in location.locationName )>0,POSITION(\'' . $searchLocationString . '\' in location.locationName), 9999),'
                    . 'IF(POSITION(\'' . $searchLocationString . '\' in location.locationCode )>0,POSITION(\'' . $searchLocationString . '\' in location.locationCode), 9999),'
                    . 'IF(POSITION(\'' . $searchLocationString . '\' in location.locationEmail )>0,POSITION(\'' . $searchLocationString . '\' in location.locationEmail), 9999),'
                    . 'IF(POSITION(\'' . $searchLocationString . '\' in location.locationAddressLine1 )>0,POSITION(\'' . $searchLocationString . '\' in location.locationAddressLine1), 9999),'
                    . 'IF(POSITION(\'' . $searchLocationString . '\' in location.locationAddressLine2 )>0,POSITION(\'' . $searchLocationString . '\' in location.locationAddressLine2), 9999),'
                    . 'IF(POSITION(\'' . $searchLocationString . '\' in location.locationAddressLine3 )>0,POSITION(\'' . $searchLocationString . '\' in location.locationAddressLine3), 9999),'
                    . 'IF(POSITION(\'' . $searchLocationString . '\' in location.locationAddressLine4 )>0,POSITION(\'' . $searchLocationString . '\' in location.locationAddressLine4), 9999),'
                    . 'IF(POSITION(\'' . $searchLocationString . '\' in location.locationTelephone )>0,POSITION(\'' . $searchLocationString . '\' in location.locationTelephone), 9999),'
                    . 'IF(POSITION(\'' . $searchLocationString . '\' in location.locationFax )>0,POSITION(\'' . $searchLocationString . '\' in location.locationFax), 9999)'
                    . ') '),
            'len' => new Expression('LEAST(CHAR_LENGTH(location.locationName ),'
                    . ' CHAR_LENGTH(location.locationCode ),'
                    . ' CHAR_LENGTH(location.locationEmail ),'
                    . ' CHAR_LENGTH(location.locationAddressLine1 ),'
                    . ' CHAR_LENGTH(location.locationAddressLine2 ),'
                    . ' CHAR_LENGTH(location.locationAddressLine3 ),'
                    . ' CHAR_LENGTH(location.locationAddressLine4 ),'
                    . ' CHAR_LENGTH(location.locationTelephone ),'
                    . ' CHAR_LENGTH(location.locationFax )'
                    . ') '),
            '*',
        ));
        $select->from('location');
        $select->join('entity', 'location.entityID = entity.entityID', array('deleted'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(
            new Operator('location.locationName', 'like', '%' . $searchLocationString . '%'),
            new Operator('location.locationCode', 'like', '%' . $searchLocationString . '%'),
            new Operator('location.locationEmail', 'like', '%' . $searchLocationString . '%'),
            new Operator('location.locationAddressLine1', 'like', '%' . $searchLocationString . '%'),
            new Operator('location.locationAddressLine2', 'like', '%' . $searchLocationString . '%'),
            new Operator('location.locationAddressLine3', 'like', '%' . $searchLocationString . '%'),
            new Operator('location.locationAddressLine4', 'like', '%' . $searchLocationString . '%'),
            new Operator('location.locationTelephone', 'like', '%' . $searchLocationString . '%'),
            new Operator('location.locationFax', 'like', '%' . $searchLocationString . '%')
                ), PredicateSet::OP_OR));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $result = $this->tableGateway->selectWith($select);
        return $result;
    }

    public function checkLocationByCode($locationID, $locationCode)
    {
        $rowset = $this->tableGateway->select(function (Select $select)use ($locationID, $locationCode) {

            if (!is_null($locationID))
                $select->where("locationID != '$locationID'");

            $select->where("locationCode = '$locationCode'");
        });
        $row = $rowset->current();
        if ($row) {
            return TRUE;
        }
        return FALSE;
    }

    public function getLocationIdByCode($locationCode)
    {
        $locationCode = (int) $locationCode;
        $rowset = $this->tableGateway->select(array('locationCode' => $locationCode));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $locationCode");
        }
        return $row;
    }

    public function getLocationByID($locationID)
    {
        $locationID = (int) $locationID;
        $rowset = $this->tableGateway->select(array('locationID' => $locationID));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $locationID");
        }
        return $row;
    }

    public function getUserAllocatedLocationsByuserID($userID)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = "SELECT l.locationID, locationName, userLocationID, (IFNULL(userLocationID,0) > 0) as active
                FROM location l
                left outer join (select * FROM userLocation WHERE userID = $userID GROUP BY locationID) ul on ul.locationID = l.locationID
                join entity e on e.entityID = l.entityID
                WHERE e.deleted = 0 AND l.locationStatus = 1
                GROUP BY l.locationID";

        $rowset = $adapter->query($sql, $adapter::QUERY_MODE_EXECUTE);
        $locations = array();
        while ($row = $rowset->current()) {
            array_push($locations, $row);
        }
        return $locations;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * get count of rows what wizardFlag is 1
     * @return null|boolean
     */
    public function checkWizardFlag()
    {
        $rowset = $this->tableGateway->select(array('wizardFlag' => 1));

        if (count($rowset) == 0) {
            return NULL;
        }
        return true;
    }

    /*
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * get location for payment voucher
     */

    public function getLocationName()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = "SELECT locationName, locationCode, locationID FROM location WHERE locationStatus ='1'";
        $rowset = $adapter->query($sql, $adapter::QUERY_MODE_EXECUTE);
        $locations = array();
        while ($row = $rowset->current()) {
            array_push($locations, $row);
        }
        return $locations;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com
     * check active location is available or not
     * @param type $locationID
     * @return boolean
     */
    public function isActiveLocation($locationID)
    {
        $locID = (int) $locationID;
        $rowSet = $this->tableGateway->select(array('locationID' => $locID, 'locationStatus' => 1));
        $row = $rowSet->current();
        if (!$row) {
            return NULL;
        }
        return TRUE;
    }

    /**
     * Update the location cash Amount
     * @param type $locationID
     * @param type $locationCashInHand
     */
    public function updateLocationAmount($locationID, $locationCashInHand)
    {
        $data = ['locationCashInHand' => $locationCashInHand];
        if ($this->tableGateway->update($data, array('locationID' => $locationID))) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get location cash in hand by locationID
     * @param type $locationID
     * @return type
     */
    public function getLocationCashInHandAmount($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('location');
        $select->columns(['locationID', 'locationCashInHand']);
        $select->where(['locationID' => $locationID]);
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getActiveNonDeletedLocations()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('location')
                ->columns(array('*'))
                ->join('entity', 'location.entityID = entity.entityID', array('deleted'), 'left')
                ->order('location.locationName ASC')
                ->where(array('entity.deleted' => '0', 'locationStatus' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return $resultSet;
    }

}
