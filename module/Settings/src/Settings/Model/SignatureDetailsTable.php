<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class SignatureDetailsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('signatureDetails');
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

   
    public function saveSignature($signatureData)
    {
        $signatureData = array(
            'signatureHolderName' => $signatureData->signatureHolderName,
        );
        if ($this->tableGateway->insert($signatureData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function get($signatureID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('signatureDetails');
        $select->where(array('signatureDetailsID' => $signatureID));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    

    public function updateSignature($data, $signatureID)
    {
        try {
            $this->tableGateway->update($data, ['signatureDetailsID' => $signatureID]);
            return true;
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    public function deleteSignatureDetails($signatureID)
    {
        try {
            $this->tableGateway->delete(array('signatureDetailsID' => $signatureID));
            return TRUE;
        } catch (\Exception $exc) {
            return FALSE;
        }
    }

     //get dimension for search
    public function SignatureSearchByKey($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('signatureDetails')
                ->columns(array(
                    'posi' => new Expression('LEAST(IF(POSITION(\'' . $keyword . '\' in signatureHolderName )>0,POSITION(\'' . $keyword . '\' in signatureHolderName), 9999),'
                            . 'IF(POSITION(\'' . $keyword . '\' in signatureDetailsID )>0,POSITION(\'' . $keyword . '\' in signatureDetailsID), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(signatureHolderName ), CHAR_LENGTH(signatureDetailsID )) '),
                    '*'));
        $select->where(new PredicateSet(array(new Operator('signatureDetails.signatureHolderName', 'like', '%' . $keyword . '%'))));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }
}