<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PromotionEmailContact
{

    public $promotionEmailContactID;
    public $promotionEmailID;
    public $contactID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->promotionEmailContactID = (!empty($data['promotionEmailContactID'])) ? $data['promotionEmailContactID'] : null;
        $this->promotionEmailID = (!empty($data['promotionEmailID'])) ? $data['promotionEmailID'] : null;
        $this->contactID = (!empty($data['contactID'])) ? $data['contactID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
