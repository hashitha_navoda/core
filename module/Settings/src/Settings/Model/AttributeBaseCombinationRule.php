<?php

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class AttributeBaseCombinationRule implements InputFilterAwareInterface
{

    public $attributeBaseCombinationRuleID;
    public $combinationID;
    public $attributeID;
    public $attributeValueID;
    public $rule;
    public $quantity;

    public function exchangeArray($data)
    {
        $this->attributeBaseCombinationRuleID = (!empty($data['attributeBaseCombinationRuleID'])) ? $data['attributeBaseCombinationRuleID'] : null;
        $this->combinationID = (!empty($data['combinationID'])) ? $data['combinationID'] : null;

        if (empty($data['attributeID'])) {

            if ($data['attributeID'] == 0) {
                $this->attributeID = 0;
            } else {
                $this->attributeID = null;
            }
        } else {
           $this->attributeID = $data['attributeID'];
        }
        $this->rule = (!empty($data['rule'])) ? $data['rule'] : null;
        $this->attributeValueID = (!empty($data['attributeValID'])) ? $data['attributeValID'] : null;
        $this->quantity = (!empty($data['quantity'])) ? $data['quantity'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}