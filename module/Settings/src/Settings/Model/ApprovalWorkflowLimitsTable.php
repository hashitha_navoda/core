<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class ApprovalWorkflowLimitsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    
    public function saveApprovalWorkflowLimits($officialData)
    {
        $data = array(
            'approvalWorkflowID' => $officialData->approvalWorkflowID,
            'approvalWorkflowLimitMin' => $officialData->approvalWorkflowLimitMin,
            'approvalWorkflowLimitMax' => $officialData->approvalWorkflowLimitMax,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getLimitsByWFId($wfId)
    {
        $results = $this->tableGateway->select(array('approvalWorkflowID' => $wfId));
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = (array) $result;
        }
        return $resultsArray;
    }

    public function deleteByWFId($approvalWorkflowID)
    {
        if ($this->tableGateway->delete(array('approvalWorkflowID' => $approvalWorkflowID))) {
            return true;
        } else {
            return false;
        }
    }

}