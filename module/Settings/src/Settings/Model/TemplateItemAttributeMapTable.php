<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class TemplateItemAttributeMapTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveItemAttributeMap($itemAttributeMap)
    {
        $itemAttributeData = array(
            'templateID' => $itemAttributeMap->templateID,
            'attributeOneID' => $itemAttributeMap->attributeOneID,
            'attributeTwoID' => $itemAttributeMap->attributeTwoID,
            'attributeThreeID' => $itemAttributeMap->attributeThreeID,
        );
    	
        $this->tableGateway->insert($itemAttributeData);
        return $this->tableGateway->lastInsertValue;
    }

    public function getTemplateItemAttributeByTemplateID($templateID) {
        $rowset = $this->tableGateway->select(array('templateID' => $templateID));
        $row = $rowset->current();
        if (!$row) {
            return false;
        }
        return $row;
    }

    public function deleteMapDetailsByTempalteID($templateID)
    {
        try {
            $result = $this->tableGateway->delete(array('templateID' => $templateID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }
}
