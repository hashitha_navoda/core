<?php 
namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ItemAttributeMap
{

    public $itemAttributeMapID;
    public $itemAttributeID;
    public $productID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->itemAttributeMapID = (!empty($data['itemAttributeMapID'])) ? $data['itemAttributeMapID'] : null;
        $this->itemAttributeID = (!empty($data['itemAttributeID'])) ? $data['itemAttributeID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
              
    }

}