<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Db\Sql\Sql;

class SmsIncludedTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginator = false)
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

}
