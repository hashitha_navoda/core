<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ApprovalDocumentDetails
{

    public $approvalDocumentDetailID;
    public $docID;
    public $isActive;
    public $isShow;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->approvalDocumentDetailID = (!empty($data['approvalDocumentDetailID'])) ? $data['approvalDocumentDetailID'] : null;
        $this->docID = (!empty($data['docID'])) ? $data['docID'] : null;
        $this->isActive = (!empty($data['isActive'])) ? $data['isActive'] : null;
        $this->isShow = (!empty($data['isShow'])) ? $data['isShow'] : null;
    }

}