<?php

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PromotionCombination implements InputFilterAwareInterface
{

    public $promotionCombinationID;
    public $promotionID;
    public $combinationName;
    public $conditionType;
    public $discountType;
    public $discountAmount;

    public function exchangeArray($data)
    {
        $this->promotionCombinationID = (!empty($data['promotionCombinationID'])) ? $data['promotionCombinationID'] : Null;
        $this->promotionID = (!empty($data['promotionID'])) ? $data['promotionID'] : null;
        $this->combinationName = (!empty($data['combinationName'])) ? $data['combinationName'] : null;
        $this->conditionType = (!empty($data['conditionType'])) ? $data['conditionType'] : null;
        $this->discountType = (!empty($data['discountType'])) ? $data['discountType'] : null;
        $this->discountAmount = (!empty($data['discountAmount'])) ? $data['discountAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
