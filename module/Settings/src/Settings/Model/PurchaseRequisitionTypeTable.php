<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PurchaseRequisitionTypeTable
{

    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('purchaseRequisitionType')
                    ->columns(array('*'))
                    ->join('purchaseRequisitionCategory', 'purchaseRequisitionType.purchaseRequisitionTypeCategory = purchaseRequisitionCategory.purchaseRequisitionCategoryId', array('purchaseRequisitionCategoryName'))
                    ->join('entity', 'purchaseRequisitionType.entityId = entity.entityID', array('deleted'))
                    ->where(array('entity.deleted' => '0'));
            $paginatorAdapter = new DbSelect($select, $adapter);
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        } else {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('purchaseRequisitionType')
                    ->columns(array('*'))
                    ->join('purchaseRequisitionTypeLimit', 'purchaseRequisitionType.purchaseRequisitionTypeId = purchaseRequisitionTypeLimit.purchaseRequisitionTypeId', array('*'), 'left')
                    ->join('purchaseRequisitionTypeLimitApprover', 'purchaseRequisitionTypeLimit.purchaseRequisitionTypeLimitId = purchaseRequisitionTypeLimitApprover.purchaseRequisitionTypeLimitId', array('*'), 'left');
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            $resultsArray = [];
            foreach ($results as $result) {
                $resultsArray[] = $result;
            }
            return $resultsArray;
        }
    }

    public function getPRTypeByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionType')
                ->columns(array('*'))
                ->where(array('purchaseRequisitionType.purchaseRequisitionTypeId' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    public function getExpenseTypeDetailsByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionType')
                ->columns(array('*'))
                ->join('purchaseRequisitionTypeLimit', 'purchaseRequisitionType.purchaseRequisitionTypeId = purchaseRequisitionTypeLimit.purchaseRequisitionTypeId', array('*'), 'left')
                ->join('purchaseRequisitionTypeLimitApprover', 'purchaseRequisitionTypeLimit.purchaseRequisitionTypeLimitId = purchaseRequisitionTypeLimitApprover.purchaseRequisitionTypeLimitId', array('*'), 'left')
                ->where(array('purchaseRequisitionType.purchaseRequisitionTypeId' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function create(PurchaseRequisitionType $purchaseRequisitionType)
    {
        $data = array(
            'purchaseRequisitionTypeId' => $purchaseRequisitionType->purchaseRequisitionTypeId,
            'purchaseRequisitionTypeName' => $purchaseRequisitionType->purchaseRequisitionTypeName,
            'purchaseRequisitionTypeCategory' => $purchaseRequisitionType->purchaseRequisitionTypeCategory,
            'purchaseRequisitionTypeApproverEnabled' => $purchaseRequisitionType->purchaseRequisitionTypeApproverEnabled,
            'purchaseRequisitionTypeStatus' => $purchaseRequisitionType->purchaseRequisitionTypeStatus,
            'entityId' => $purchaseRequisitionType->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function update($data, $purchaseRequisitionTypeId)
    {
        try {
            $this->tableGateway->update($data, array('purchaseRequisitionTypeId' => $purchaseRequisitionTypeId));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public function changeStatus($status, $purchaseRequisitionTypeId)
    {
        if ($this->tableGateway->update(array('purchaseRequisitionTypeStatus' => $status), array('purchaseRequisitionTypeId' => $purchaseRequisitionTypeId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function searchExpenseTypeForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionType');
        $select->join('entity', 'purchaseRequisitionType.entityID=  entity.entityID', array("*"), "left");
        $select->where(array("entity.deleted" => 0));
        $select->where(array("purchaseRequisitionType.purchaseRequisitionTypeStatus" => 1));
        $select->where->like('purchaseRequisitionTypeName', '%' . $searchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getApproversByTypeIDAndPriceRange($typeID, $totalPrice)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionType');
        $select->join('entity', 'purchaseRequisitionType.entityID=  entity.entityID', array("*"), "left");
        $select->join('purchaseRequisitionTypeLimit', 'purchaseRequisitionType.purchaseRequisitionTypeId=  purchaseRequisitionTypeLimit.purchaseRequisitionTypeId', array("*"), "left");
        $select->join('purchaseRequisitionTypeLimitApprover', 'purchaseRequisitionTypeLimit.purchaseRequisitionTypeLimitId=  purchaseRequisitionTypeLimitApprover.purchaseRequisitionTypeLimitId', array("*"), "left");
        $select->join('approvers', 'purchaseRequisitionTypeLimitApprover.purchaseRequisitionTypeLimitApprover=  approvers.approverID', array('firstName', 'lastName', 'email'), "left");
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $select->where(new PredicateSet(array(new Operator('purchaseRequisitionType.purchaseRequisitionTypeId', '=', $typeID))));
        $select->where(new PredicateSet(array(new Operator('purchaseRequisitionTypeLimit.purchaseRequisitionTypeLimitMin', '<=', $totalPrice))));
        $select->where(new PredicateSet(array(new Operator('purchaseRequisitionTypeLimit.purchaseRequisitionTypeLimitMax', '>=', $totalPrice))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getExpeneTypes()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionType');
        $select->columns(['purchaseRequisitionTypeName', 'purchaseRequisitionTypeId']);
        $select->join('entity', 'purchaseRequisitionType.entityID=  entity.entityID', ['deleted'], "left");
        $select->where(array("entity.deleted" => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getPRTypesByCategory($categoryId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseRequisitionType');
        $select->columns(array('*'));
        $select->join('entity', 'purchaseRequisitionType.entityID=  entity.entityID', ['deleted'], "left");
        $select->where(array("entity.deleted" => 0));
        $select->where(array("purchaseRequisitionType.purchaseRequisitionTypeCategory" => $categoryId));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

}
