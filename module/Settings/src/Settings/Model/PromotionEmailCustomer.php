<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PromotionEmailCustomer
{

    public $promotionEmailCustomerID;
    public $promotionEmail;
    public $promotionEmailCustomer;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->promotionEmailCustomerID = (!empty($data['promotionEmailCustomerID'])) ? $data['promotionEmailCustomerID'] : null;
        $this->promotionEmail = (!empty($data['promotionEmail'])) ? $data['promotionEmail'] : null;
        $this->promotionEmailCustomer = (!empty($data['promotionEmailCustomer'])) ? $data['promotionEmailCustomer'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
