<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\IsNull;

Class ItemBaseCombinationRuleTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function save(ItemBaseCombinationRule $rule)
    {
        $ruleData = array(
            'combinationID' => $rule->combinationID,
            'productID' => $rule->productID,
            'rule' => $rule->rule,
            'quantity' => $rule->quantity
        );

        $this->tableGateway->insert($ruleData);
        return $this->tableGateway->lastInsertValue;
    }

    public function getRelatedRulesByCombinationId($combinationID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemBaseCombinationRule');
        $select->columns(array('*', 'ruleType' => new Expression('itemBaseCombinationRule.rule')));
        $select->join('product', 'itemBaseCombinationRule.productID = product.productID', array('productName', 'productCode'));
        $select->where(array('combinationID' => $combinationID));
       
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function deleteByCombinationID($combinationID)
    {
        return $this->tableGateway->delete(array('combinationID' => $combinationID));
    }

    
}
