<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Dimension
{

    public $dimensionId;
    public $dimensionName;
    public $deleted;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->dimensionId = (!empty($data['dimensionId'])) ? $data['dimensionId'] : null;
        $this->dimensionName = (!empty($data['dimensionName'])) ? $data['dimensionName'] : null;
        $this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : 0;
    }

}