<?php

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class AdministrationOfficialTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('administrationOfficials');
        $select->where(array('deleted' => '0'));
        $select->order('administrationOfficialID DESC');
        $select->join('entity', 'administrationOfficials.entityID =entity.entityID', array("deleted"), "left");
        $select->where(array('entity.deleted' => 0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function checkOfficialIDNovalid($IDNO, $officialId=null, $updataMode = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('administrationOfficials')
                ->columns(array('*'))
                ->join('entity', 'administrationOfficials.entityID =entity.entityID', array("deleted"), "left")
                ->where(array('entity.deleted' => 0, 'administrationOfficials.nic' => $IDNO));
        if ($updataMode) {
            $select->where->notEqualTo('administrationOfficials.administrationOfficialID',$officialId);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function saveOfficials($officialData)
    {
        $data = array(
            'firstName' => $officialData->firstName,
            'lastName' => $officialData->lastName,
            'address' => $officialData->address,
            'email' => $officialData->email,
            'telephoneNo' => $officialData->telephoneNo,
            'nic' => $officialData->nic,
            'entityID' => $officialData->entityID,
            'dateOfBirth' => $officialData->dateOfBirth,
            'gender' => $officialData->gender,
            'status' => '1',
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateOfficialState($administrationOfficialID, $status)
    {
        $data = array(
            'status' => $status,
        );
        try {
            $this->tableGateway->update($data, array('administrationOfficialID' => $administrationOfficialID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getOfficialSearchByKey($key=null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('administrationOfficials')
                ->columns(array('*'))
                ->join('entity', 'administrationOfficials.entityID = entity.entityID', array('*'), 'left');
        if (!is_null($key)) {
            $select->where(new PredicateSet(array(new Operator('administrationOfficials.firstName', 'like', '%' . $key . '%'),new Operator('administrationOfficials.lastName', 'like', '%' . $key . '%'), new Operator('administrationOfficials.telephoneNo', 'like', '%' . $key . '%'), new Operator('administrationOfficials.nic', 'like', '%' . $key . '%')), PredicateSet::OP_OR));
        }

        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }
}