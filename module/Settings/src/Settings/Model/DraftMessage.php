<?php 

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DraftMessage
{

    public $draftMessageID;
    public $draftMessageName;
    public $draftMessageMessage;
    public $draftMessageDate;
   
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->draftMessageID = (!empty($data['draftMessageID'])) ? $data['draftMessageID'] : null;
        $this->draftMessageName = (!empty($data['draftMessageName'])) ? $data['draftMessageName'] : null;
        $this->draftMessageMessage = (!empty($data['draftMessageMessage'])) ? $data['draftMessageMessage'] : null;
        $this->draftMessageDate = (!empty($data['draftMessageDate'])) ? $data['draftMessageDate'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

}