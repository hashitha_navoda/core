<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Reference Model Functions
 */

namespace Settings\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Reference implements InputFilterAwareInterface
{

    public $locationReferenceID;
    public $referenceID;
    public $referenceName;
    public $locationID;
    public $referenceType;
    public $numberOfDigits;
    public $currentReference;
    public $localGlobalType;
    protected $inputFilter;                      // <-- Add this variable

    public function exchangeArray($data)
    {

        $this->locationReferenceID = (isset($data['referenceNameID'])) ? $data['referenceNameID'] : null;
        $this->referenceID = (isset($data['referenceID'])) ? $data['referenceID'] : null;
        $this->referenceName = (isset($data['referenceName'])) ? $data['referenceName'] : null;
        $this->referenceTypeID = (isset($data['referenceTypeID'])) ? $data['referenceTypeID'] : null;
        $this->locationID = (isset($data['locationID'])) ? $data['locationID'] : null;
        $this->referenceType = (isset($data['referenceType'])) ? $data['referenceType'] : null;
        $this->numberOfDigits = (isset($data['numberOfDigits'])) ? $data['numberOfDigits'] : null;
        $this->currentReference = (isset($data['currentReference'])) ? $data['currentReference'] : null;
        $this->localGlobalType = (isset($data['localGlobalType'])) ? $data['localGlobalType'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationReferenceID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'referenceID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'referenceType',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'referenceName',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'numberOfDigits',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'currentReference',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
