<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Settings\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class TemplateTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('template');
        $select->join('documentType', 'template.documentTypeID = documentType.documentTypeID', array('*'));
        $select->join('documentSize', 'template.documentSizeID = documentSize.documentSizeID', array('*'));
        $select->where(array('templateSample = 0'));
        $select->order('template.documentTypeID');
        $select->order('documentSize.documentSizeName');

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    public function get($templateID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('template');
        $select->join('documentType', 'template.documentTypeID = documentType.documentTypeID', array('*'));
        $select->join('documentSize', 'template.documentSizeID = documentSize.documentSizeID', array('*'));
        $select->where(array('templateSample = 0'));
        $select->where(array('templateID' => $templateID));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    public function getTemplatesForDocType($documentTypeName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('template');
        $select->join('documentType', 'template.documentTypeID = documentType.documentTypeID', array('*'));
        $select->join('documentSize', 'template.documentSizeID = documentSize.documentSizeID', array('*'));
        $select->where(array('templateSample = 0'));
        $select->where(array('documentTypeName' => $documentTypeName));
        $select->order('documentSize.documentSizeID');

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    public function getTemplateDocumentSizes()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('documentSize');
        $select->order('documentSize.documentSizeName');

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    public function getSampleContent($documentTypeID, $documentSizeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('template');
        $select->where(array('documentTypeID' => $documentTypeID));
        $select->where(array('documentSizeID' => $documentSizeID));
        $select->where(array('templateSample' => 1));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset->current();
    }

    public function getDefaultTemplate($docTypeName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('template');
        $select->join('documentType', 'template.documentTypeID = documentType.documentTypeID', array('*'));
        $select->join('documentSize', 'template.documentSizeID = documentSize.documentSizeID', array('*'));
        $select->where(array('documentTypeName' => $docTypeName));
        $select->where(array('templateSample = 0'));
        $select->where(array('templateDefault = 1'));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    public function deleteTemplate($templateID)
    {
        return $this->tableGateway->delete(array('templateID' => $templateID));
    }

    public function updateDefaultTemplate($templateID)
    {
        // get document type
        $documentTypeID = $this->tableGateway->select(array('templateID' => $templateID))
                        ->current()->documentTypeID;

        // reset all templates to default before updating current one
        $this->tableGateway->update(array('templateDefault' => 0), array('documentTypeID' => $documentTypeID));

        $this->tableGateway->update(array('templateDefault' => 1), array('templateID' => $templateID));

        return true;
    }

    public function updateTemplate($data, $templateID)
    {
        $this->tableGateway->update($data, array('templateID' => $templateID));
        return true;
    }

    public function addTemplate($template)
    {
        $data = [
            'templateName' => $template->templateName,
            'documentTypeID' => $template->documentTypeID,
            'documentSizeID' => $template->documentSizeID,
            'templateContent' => $template->templateContent
        ];

        $this->tableGateway->insert($data);
        return $this->tableGateway->lastInsertValue;
    }

    public function getTemplateNamesForDocTypes ($documentTypeName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('template');
        $select->columns(array('templateID', 'templateName', 'templateDefault'));
        $select->join('documentType', 'template.documentTypeID = documentType.documentTypeID', array('documentTypeID', 'documentTypeName'));
        $select->where(array('templateSample = 0'));
        if(is_array($documentTypeName)){
            $select->where->in('documentTypeName', $documentTypeName);
        }else{
            $select->where(array('documentTypeName' => $documentTypeName));   
        }

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset;
    }

    public function isAdvancedTemplateEnabled($templateId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from("template");
        $select->columns(['isAdvanceTemplateEnabled']);
        $select->where(['templateID' => $templateId]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        $row = $rowset->current();

        return $row['isAdvanceTemplateEnabled'];
    }

}
