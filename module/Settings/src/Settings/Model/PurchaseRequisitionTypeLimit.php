<?php

namespace Settings\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;


class PurchaseRequisitionTypeLimit implements InputFilterAwareInterface
{

    public $purchaseRequisitionTypeLimitId;
    public $purchaseRequisitionTypeId;
    public $purchaseRequisitionTypeLimitMin;
    public $purchaseRequisitionTypeLimitMax;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->purchaseRequisitionTypeLimitId = (isset($data['purchaseRequisitionTypeLimitId'])) ? $data['purchaseRequisitionTypeLimitId'] : null;
        $this->purchaseRequisitionTypeId = (isset($data['purchaseRequisitionTypeId'])) ? $data['purchaseRequisitionTypeId'] : null;
        $this->purchaseRequisitionTypeLimitMin = (isset($data['purchaseRequisitionTypeLimitMin'])) ? $data['purchaseRequisitionTypeLimitMin'] : null;
        $this->purchaseRequisitionTypeLimitMax = (isset($data['purchaseRequisitionTypeLimitMax'])) ? $data['purchaseRequisitionTypeLimitMax'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
