<?php

namespace Settings\Service;

use Core\Contracts\SearchableFromCode;
use Inventory\Model\CompoundCostType;
use Core\Service\BaseService;
use Illuminate\Support\Collection;
use Illuminate\Validation\Factory as Validator;


class LandingCostTypeService extends BaseService
{
	
	public function saveLandingCostType($data)
	{
		// first need to check cost type code exist or not
		$costTypeDetails = $this->getModel('CompoundCostTypeTable')->getCostTypeDetailsByCode($data['costCode'])->current();
        if ($costTypeDetails != null) {
        	return [
        		'status' => false,
        		'msg' => 'ERR_COST_TYPE_CODE_USE'
        	];
        }

        // check product code availability
        $productCode = 'NONINVENT_'.$data['costCode'];
        $productDetails = $this->getModel('ProductTable')->getProductByCode($productCode,false,false,true);

        if ($productDetails != null) {
        	return [
        		'status' => false,
        		'msg' => 'ERR_PRODUCT_CODE_IN_USE'
        	];	
        }
        
        // save Cost Type
        $costTypeArray = [
        	'compoundCostTypeName' => $data['costName'],
        	'compoundCostTypeCode' => $data['costCode'] 
        ];
        $costType = new CompoundCostType();
        $costType->exchangeArray($costTypeArray);

        $saveCostType = $this->getModel('CompoundCostTypeTable')->save($costType);
        if ($saveCostType == null) {
        	return [
        		'status' => false,
        		'msg' => 'ERR_COST_TYPE_SAVE'
        	];	
        }
        // get user active location
        $locationID = $this->user_session->userActiveLocation['locationID'];

        // save product details
        $productSaveDataSet = [
        	'code' => $data['costCode'],
        	'name' => $data['costName'],
        	'productSalesAccountID' => $data['productSalesAccountID'],
        	'productInventoryAccountID' => $data['productInventoryAccountID'],
        	'productCOGSAccountID' => $data['productCOGSAccountID'],
        	'productAdjusmentAccountID' => $data['productAdjusmentAccountID']
        ];
        $pI = $this->getServiceLocator()->get("PurchaseInvoiceController");
        $productStatus =  $pI->insertCompundCostItems($productSaveDataSet, $locationID, true);
        if ($productStatus == null) {
        	return [
        		'status' => false,
        		'msg' => 'ERR_PRODUCT_SAVE'
        	]; 
        }

        return [
    		'status' => true,
    		'msg' => 'SUCC_SAVE_COST_TYPE'
    	];
	}

    public function deleteLandingCostType($data)
    {
        $updatedData = [
            'compoundCostTypeIsDelete' => 1
        ];
        $deleteCostType = $this->getModel('CompoundCostTypeTable')->update($updatedData, $data['costTypeID']);
        if (!$deleteCostType) {
            return [
                'status' => false,
                'msg' => 'ERR_PRODUCT_DELETE'
            ];   
        }
        return [
            'status' => true,
            'msg' => 'SUCC_PRODUCT_DELE'
        ];
    }

    public function searchLandingCostType($data)
    {
        $paginated = false;
        if (!empty($data['searchKey'])) {
            $costTypeList = $this->getModel('CompoundCostTypeTable')->compoundCostTypeSearchByKey($data['searchKey']);
            $status = true;
        } else {
            $status = true;
            $landedCT = $this->getServiceLocator()->get("LandingCostTypeController");
            $costTypeList =  $landedCT->getPaginatedCostTypes(6, true);
            $paginated = true;
        }
        return [
            'status' => $status,
            'costTypeList' => $costTypeList,
            'paginated' => $paginated
        ];
        
    }

}