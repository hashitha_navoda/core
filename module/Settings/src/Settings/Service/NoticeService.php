<?php

namespace Settings\Service;

use Core\Contracts\SearchableFromCode;
use Core\Model\Notice;
use Core\Service\BaseService;
use Illuminate\Support\Collection;
use Illuminate\Validation\Factory as Validator;


class NoticeService extends BaseService
{
	/**
	 * Create notice
	 * @param  array $data [
	 *                     		'notice' => 'sample notice',
	 *                     		'noticeLink' => 'http://www.link.com',
	 *                     		'noticeName' => 'notice',
	 *                     		'noticeType' => 'warning',
	 *                     		'noticeExpireDate' => '2018-01-01'
	 *                     ]
	 */
	public function createNotice($data)
	{

        if ($data['notice'] == '' || $data['noticeType'] == '') {
			return $this->returnError('Unacceptable vales for notice or notice type');
    	}

        $noticeStatuses = $this->getServiceLocator()->get('config')['noticeStatus'];
        $noticeTypeID = array_search($data['noticeType'], $noticeStatuses);
        $noticeModel = new Notice();
        $data = array(
            'notice' => $data['notice'],
            'noticeLink' => $data['noticeLink'],
            'noticeName' => $data['noticeName'],
            'noticeType' => $noticeTypeID,
            'noticeExpireDate' => $data['noticeExpireDate']
        );

        $noticeModel->exchangeArray($data);
        if ($this->getModel('Core\model\NoticeTable')->saveNotice($noticeModel)) {
        	return $this->returnSuccess([], 'Successfully added the notice');
        }
		return $this->returnError('Unable to save the notice');
	}


	/**
	 * Get notice by given expire date and notice name
	 * @param  date $date  '2018-01-11'
	 * @param  string $title 'sample notice'
	 * @return array
	 */
	public function getNoticeByDateAndTitle($date, $title)
	{
		$notice = $this->getModel('Core\model\NoticeTable')->getNoticeByDateAndTitle($date, $title);

		if ($notice) {
			return $this->returnSuccess($notice, 'Notice Successfully retrieved');
		}

		return $this->returnError('No notice retrieved');
	}
}