<?php

namespace Settings\Service;

use Core\Service\BaseService;
use Settings\Model\AdministrationOfficial;
use Settings\Model\OrganizationAdditionalDetail;
use Zend\View\Model\ViewModel;

class CompanyService extends BaseService {
	
	/**
    * this function use to save official.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function createOfficial($data, $userId)
    {
        
        $employeeFirstName = $data['employeeFirstName'];
        $employeeSecondName = $data['employeeSecondName'];
        $employeeAddress = $data['employeeAddress'];
        $employeeTP = $data['employeeTP'];
        $employeeEmail = $data['employeeEmail'];
        $employeeIDNo = $data['employeeIDNo'];
        $employeeGender = ($data['employeeGender'] == "male") ? 1 : 0;
        $employeeDOB = $this->convertDateToStandardFormat($data['employeeDOB']);
        $employeeID = null;

        $employeeIDNoDataset = $this->getModel('AdministrationOfficialTable')->checkOfficialIDNovalid($employeeIDNo);

        if ($employeeIDNoDataset->count() > 0) { 
            return $this->returnError('ERR_OFFIC_ID_NO_ALREDY_EXIST', 'EMPLOYEE_ID_NO_EXIST');
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data = array(
            'administrationOfficialID' => $employeeID,
            'firstName' => $employeeFirstName,
            'lastName' => $employeeSecondName,
            'address' => $employeeAddress,
            'telephoneNo' => $employeeTP,
            'email' => $employeeEmail,
            'nic' => $employeeIDNo,
            'dateOfBirth' => $employeeDOB,
            'gender' => $employeeGender,
            'entityID' => $entityID
        );


        $employeeData = new AdministrationOfficial();
        $employeeData->exchangeArray($data);
        
        $savedResult = $this->getModel('AdministrationOfficialTable')->saveOfficials($employeeData);

        if (!$savedResult) {
            return $this->returnError('ERR_OFFIC_SAVE', 'EMPLOYEE_SAVE_FAILD');
        } 

        return $this->returnSuccess($savedResult, 'SUCC_OFFIC_SAVE');
    }

    /**
    * this function use to update official.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function updateAdministrationOfficial($data, $userId)
    {
        $employeeIDNo = $data['employeeIDNo'];

        $employeeIDNoDataset = $this->getModel('AdministrationOfficialTable')->checkOfficialIDNovalid($employeeIDNo, $data['employeeID'], true);
        
        if ($employeeIDNoDataset->count() > 0) { 
            return $this->returnError('ERR_OFFIC_ID_NO_ALREDY_EXIST', 'EMPLOYEE_ID_NO_EXIST');
        }

        $oldEmpId = $data['employeeID'];
        $entityService = $this->getService('EntityService');
        $deleteRecord = $entityService->updateDeleteInfoEntity($data['entityID'], $userId);
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data['entityID'] = $entityID;
        $data['employeeDOB'] = $this->convertDateToStandardFormat($data['employeeDOB']);

        $data = array(
            'administrationOfficialID' => null,
            'firstName' => $data['employeeFirstName'],
            'lastName' => $data['employeeSecondName'],
            'address' => $data['employeeAddress'],
            'telephoneNo' => $data['employeeTP'],
            'email' => $data['employeeEmail'],
            'nic' => $data['employeeIDNo'],
            'dateOfBirth' => $data['employeeDOB'],
            'gender' => ($data['employeeGender'] == "male") ? 1 : 0,
            'entityID' => $data['entityID']
        );

        $employeeData = new AdministrationOfficial();
        $employeeData->exchangeArray($data);

        $savedResult = $this->getModel('AdministrationOfficialTable')->saveOfficials($employeeData);
        if (!$savedResult) {
            return $this->returnError('ERR_OFFIC_UPDATE', 'EMPLOYEEE_FAILD_UPDATE');
        } else {
            return $this->returnSuccess($savedResult, 'SUCC_OFFIC_UPDATE');
        }
    }

    /**
    * this function use to update official state.
    * @param array $data
    * return array
    **/
    public function updateOfficialState($data)
    {
        $officialID = $data['officialID'];
        $status = $data['status'];
        $result = $this->getModel('AdministrationOfficialTable')->updateOfficialState($officialID, $status);

        if ($result) {
            return $this->returnSuccess('EMPLOYEE_STATE_SUCESSFULLY_UPDATE', 'SUCC_OFFIC_STATUS_UPDATE');
        } else {
            return $this->returnError('ERR_OFFIC_STATUS_UPDATE', 'EMPLOYEE_STATE_FAILD_UPDATE');
        }

    }

    public function getUploadedAttachemnts($id) {
        $uploadedAttachments = [];
        $attachmentData = $this->getModel('DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($id, 36);
        $companyName = $this->getSubdomain();
        $path = '/userfiles/' . md5($companyName) . "/attachments";

        foreach ($attachmentData as $value) {
            $temp = [];
            $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
            $temp['documentRealName'] = $value['documentRealName'];
            $temp['docLink'] = $path . "/" .$value['documentName'];
            $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
        }

        
        return [
            'status' => true,
            'data' => $uploadedAttachments
        ];
    }

    public function getUploadedAttachemntsOfAdditionalDetail($id) {
        $uploadedAttachments = [];
        $attachmentData = $this->getModel('DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($id, 37);
        $companyName = $this->getSubdomain();
        $path = '/userfiles/' . md5($companyName) . "/attachments";

        foreach ($attachmentData as $value) {
            $temp = [];
            $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
            $temp['documentRealName'] = $value['documentRealName'];
            $temp['docLink'] = $path . "/" .$value['documentName'];
            $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
        }

        
        return [
            'status' => true,
            'data' => $uploadedAttachments
        ];
    }

    /**
     * This function is used to delete official
     **/
    public function deleteOfficial($data, $userId)
    {
        $employeeID = $data['employeeID'];
        if (!empty($employeeID)) {
            $entityService = $this->getService('EntityService');
            $result = $entityService->updateDeleteInfoEntity($data['entityID'], $userId);
            if ($result) {
                return $this->returnSuccess(null,'SUCC_OFFIC_DELETE');
            } else {
                return $this->returnError('ERR_OFFIC_DELETE',null);
            }
        } else {
            return $this->returnError('ERR_OFFIC_DELETE',null);
        }
    }

    /**
    * this function use to search official by key
    * @param array $data
    * @param int $param
    * return array
    **/
    public function officialSearchByKey($data, $param)
    {
        $paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];

        if (!empty($key)) {
            $officialList = $this->getModel('AdministrationOfficialTable')->getOfficialSearchByKey($key);
            $status = true;
        } else {
            $status = true;
            $CompanyController = $this->getServiceLocator()->get("CompanyController");
            $officialList = $CompanyController->getPaginatedOfficials(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'employees' => $officialList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('settings/company/list-admin-officials.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];
    }

    public function createAdditionalDetail($data)
    {
        $addiName = $data['addiName'];
        
        $result = $this->getModel('OrganizationAdditionalDetailTable')->getDetailsByOranizationAdditionalDetialName($addiName);

        if ($result->count() > 0) { 
            return $this->returnError('ERR_ADDI_NAME_ALREDY_EXIST', 'NAME_EXIST');
        }

        $data = array(
            'oranizationAdditionalDetialName' => $addiName,
        );

        $addData = new OrganizationAdditionalDetail();
        $addData->exchangeArray($data);    
        $savedResult = $this->getModel('OrganizationAdditionalDetailTable')->saveOrganizationData($addData);

        if (!$savedResult) {
            return $this->returnError('ERR_ADDI_CREATE', 'DIMENSION_SAVE_FAILD');
        } else {
            return $this->returnSuccess($savedResult, 'SUCC_ADDI_SAVE');
        }
    }

     public function updateAdditionalDetails($data) {
        $addiId = $data['addiId'];

        $deleteFlag = $this->getModel('OrganizationAdditionalDetailTable')->updateDeleteDetail($addiId);
        if ($deleteFlag) {
            $updatedFlag = $this->createAdditionalDetail($data);
            if ($updatedFlag['status']) {
                return $this->returnSuccess($updatedFlag['data'], 'SUCC_ADDI_UPDATE');
            } else {
                return $this->returnError('ERR_ADDI_UPDATE', null);
            }
        } else {
            return $this->returnError('ERR_ADDI_UPDATE', null);
        }
    }

    public function searchAdditionalDetail($data, $param)
    {
        $paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];

        if (!empty($key)) {
            $addiDetailList = $this->getModel('OrganizationAdditionalDetailTable')->organizationDetailSearchByKey($key);
            $status = true;
        } else {
            $status = true;
            $companyController = $this->getServiceLocator()->get("CompanyController");
            $addiDetailList = $companyController->getPaginatedAdditionaldetails(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'adDetails' => $addiDetailList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('settings/company/list-additional-detail.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];
    }

    public function deleteAdditionDetail($data) {
        $addiID = $data['addiID'];

        if (!empty($addiID)) {
            $result = $this->getModel('OrganizationAdditionalDetailTable')->updateDeleteDetail($addiID);
            if ($result) {
                return $this->returnSuccess(null, 'SUCC_ADDI_DELETE');
            } else {
                return $this->returnError('ERR_ADDI_DELTE', null);
            }
        } else {
            return $this->returnError('ERR_ADDI_DELTE', null); 
        }
    }
}