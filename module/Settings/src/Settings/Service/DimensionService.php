<?php

namespace Settings\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Settings\Model\Dimension;
use Settings\Model\DimensionValue;

class DimensionService extends BaseService
{
    /**
    * this function use to save dimension
    * @param array $data
    * return array
    **/
    public function createDimension($data)
    {
        $dimensionName = $data['dimensionName'];
        $dimensionValues = $data['mainDimensionValues'];
        
        $result = $this->getModel('DimensionTable')->getDimensionByDimensionName($dimensionName);

        if ($result->count() > 0) { 
            return $this->returnError('ERR_DIMENSION_NAME_ALREDY_EXIST', 'DIMENSION_EXIST');
        }

        $data = array(
            'dimensionName' => $dimensionName,
        );

        $dimensionData = new Dimension();
        $dimensionData->exchangeArray($data);    
        $savedResult = $this->getModel('DimensionTable')->saveDimension($dimensionData);

        if (!$savedResult) {
            return $this->returnError('ERR_DIMENSION_CREATE', 'DIMENSION_SAVE_FAILD');
        } else {
            foreach ($dimensionValues as $key => $value) {
                $dimensionValData = array(
                    'dimensionId' => $savedResult,
                    'dimensionValue' => $value['value'],
                );
                
                $dimensionValueData = new DimensionValue();
                $dimensionValueData->exchangeArray($dimensionValData);  

                $savedDimensionValue = $this->getModel('DimensionValueTable')->saveDimensionValue($dimensionValueData);

                if (!$savedDimensionValue) {
                    return $this->returnError('ERR_DIMENSION_VALUE_CREATE', '');
                }

            }
            return $this->returnSuccess('DIMENSION_SAVE_SUCESSFULLY', 'SUCC_DIMENSION_SAVE');
        }
    }


    public function dimensionSearchByKey($data, $param)
    {
        $paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];

        if (!empty($key)) {
            $dimensionList = $this->getModel('DimensionTable')->dimensionSearchByKey($key);
            $status = true;
        } else {
            $status = true;
            $dimensionController = $this->getServiceLocator()->get("DimensionController");
            $dimensionList = $dimensionController->getPaginatedDimensions(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'dimensions' => $dimensionList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('settings/dimension/list.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];
    }

    public function getRelatedDimensionValues($data)
    {
        $dimensionId = ($data['dimensionId'] == "") ? null: $data['dimensionId'];
        $result = $this->getModel('DimensionValueTable')->getRelatedDimensionValues($dimensionId);

        $mainDimensionValues = [];
        foreach ($result as $key => $value) {
            $mainDimensionValues[$value['dimensionValueId']] = [
                'value' =>  $value['dimensionValue']
            ];
        }

        if ($result) {
            return $this->returnSuccess($mainDimensionValues,null);
        } else {
            return $this->returnError('ERR_RETRIVE_DIMENSION_VALUES',null);
        }
            
    } 


    /**
     * This function is used update dimension
     * @param $postData
     */
    public function updateDimension($data) {
        $dimensionId = $data['dimensionId'];

        $deleteFlag = $this->getModel('DimensionTable')->updateDeleteDimension($dimensionId);
        if ($deleteFlag) {
            $updatedFlag = $this->createDimension($data);
            if ($updatedFlag['status']) {
                return $this->returnSuccess($updatedFlag['data'], 'SUCC_DIMENSION_UPDATE');
            } else {
                return $this->returnError('ERR_DIMENSION_UPDATE', null);
            }
        } else {
            return $this->returnError('ERR_DIMENSION_UPDATE', null);
        }
    }

    public function deleteDimension($data) {
        $dimensionId = $data['dimensionId'];

        if (!empty($dimensionId)) {
            $result = $this->getModel('DimensionTable')->updateDeleteDimension($dimensionId);
            if ($result) {
                return $this->returnSuccess(null, 'SUCC_DIMENSION_DELETE');
            } else {
                return $this->returnError('ERR_DIMENSION_DELTE', null);
            }
        } else {
            return $this->returnError('ERR_DIMENSION_DELTE', null); 
        }
    }

}