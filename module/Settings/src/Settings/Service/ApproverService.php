<?php

namespace Settings\Service;

use Core\Service\BaseService;
use Settings\Model\Approver;
use Settings\Model\OrganizationAdditionalDetail;
use Zend\View\Model\ViewModel;

class ApproverService extends BaseService {
	
	/**
    * this function use to save official.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function createApprover($data, $userId)
    {
        
        $employeeFirstName = $data['employeeFirstName'];
        $employeeSecondName = $data['employeeSecondName'];
        $employeeAddress = $data['employeeAddress'];
        $employeeTP = $data['employeeTP'];
        $employeeEmail = $data['employeeEmail'];
        $employeeIDNo = $data['employeeIDNo'];
        $employeeID = null;

        $employeeIDNoDataset = $this->getModel('ApproverTable')->checkApproverIDNovalid($employeeIDNo);

        if ($employeeIDNoDataset->count() > 0) { 
            return $this->returnError('ERR_APPRO_ID_NO_ALREDY_EXIST', 'EMPLOYEE_ID_NO_EXIST');
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data = array(
            'approverID' => $employeeID,
            'firstName' => $employeeFirstName,
            'lastName' => $employeeSecondName,
            'address' => $employeeAddress,
            'telephoneNo' => $employeeTP,
            'email' => $employeeEmail,
            'nic' => $employeeIDNo,
            'entityID' => $entityID
        );


        $employeeData = new Approver();
        $employeeData->exchangeArray($data);
        
        $savedResult = $this->getModel('ApproverTable')->saveApprovers($employeeData);

        if (!$savedResult) {
            return $this->returnError('ERR_APPRO_SAVE', 'EMPLOYEE_SAVE_FAILD');
        } 

        return $this->returnSuccess($savedResult, 'SUCC_APPRO_SAVE');
    }

    /**
    * this function use to update official.
    * @param array $data
    * @param int $userId
    * return array
    **/
    public function updateApprover($data, $userId)
    {
        $employeeIDNo = $data['employeeIDNo'];

        $employeeIDNoDataset = $this->getModel('ApproverTable')->checkApproverIDNovalid($employeeIDNo, $data['employeeID'], true);
        
        if ($employeeIDNoDataset->count() > 0) { 
            return $this->returnError('ERR_APPRO_ID_NO_ALREDY_EXIST', 'EMPLOYEE_ID_NO_EXIST');
        }

        $oldEmpId = $data['employeeID'];
        $entityService = $this->getService('EntityService');
        $deleteRecord = $entityService->updateDeleteInfoEntity($data['entityID'], $userId);
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $data['entityID'] = $entityID;
        $data['employeeDOB'] = $this->convertDateToStandardFormat($data['employeeDOB']);

        $data = array(
            'approverID' => null,
            'firstName' => $data['employeeFirstName'],
            'lastName' => $data['employeeSecondName'],
            'address' => $data['employeeAddress'],
            'telephoneNo' => $data['employeeTP'],
            'email' => $data['employeeEmail'],
            'nic' => $data['employeeIDNo'],
            'entityID' => $data['entityID']
        );

        $employeeData = new Approver();
        $employeeData->exchangeArray($data);

        $savedResult = $this->getModel('ApproverTable')->saveApprovers($employeeData);
        if (!$savedResult) {
            return $this->returnError('ERR_APPRO_UPDATE', 'EMPLOYEEE_FAILD_UPDATE');
        } else {
            return $this->returnSuccess($savedResult, 'SUCC_APPRO_UPDATE');
        }
    }

    /**
    * this function use to update official state.
    * @param array $data
    * return array
    **/
    public function updateApproverState($data)
    {
        $approverID = $data['officialID'];
        $status = $data['status'];
        $result = $this->getModel('ApproverTable')->updateApproverState($approverID, $status);

        if ($result) {
            return $this->returnSuccess('EMPLOYEE_STATE_SUCESSFULLY_UPDATE', 'SUCC_APPRO_STATUS_UPDATE');
        } else {
            return $this->returnError('ERR_APPRO_STATUS_UPDATE', 'EMPLOYEE_STATE_FAILD_UPDATE');
        }

    }

    /**
     * This function is used to delete official
     **/
    public function deleteApprover($data, $userId)
    {
        $approverID = $data['employeeID'];
        if (!empty($approverID)) {
            $entityService = $this->getService('EntityService');
            $result = $entityService->updateDeleteInfoEntity($data['entityID'], $userId);
            if ($result) {
                return $this->returnSuccess(null,'SUCC_APPRO_DELETE');
            } else {
                return $this->returnError('ERR_APPRO_DELETE',null);
            }
        } else {
            return $this->returnError('ERR_APPRO_DELETE',null);
        }
    }

    /**
    * this function use to search official by key
    * @param array $data
    * @param int $param
    * return array
    **/
    public function approverSearchByKey($data, $param)
    {
        $paginated = false;
        $key = ($data['searchKey'] == '') ? null : $data['searchKey'];

        if (!empty($key)) {
            $officialList = $this->getModel('ApproverTable')->getApproverSearchByKey($key);
            $status = true;
        } else {
            $status = true;
            $ApproverController = $this->getServiceLocator()->get("ApproverController");
            $officialList = $ApproverController->getPaginatedApprovers(6, $param);
            $paginated = true;
        }
       
        $view = new ViewModel(array(
            'employees' => $officialList,
            'paginated' => $paginated,
        ));

        $view->setTerminal(true);
        $view->setTemplate('settings/approver/list.phtml');
        
        return [
            'view' => $view,
            'status' => $status
        ];
    }

     /**
     * This function is used to search approvers for dropdown
     */
    public function searchApproversForDropdown($data) {
        $searchKey = $data['searchKey'];
        $employees = $this->getModel('ApproverTable')->getApproversBySearchKeyForDropdown($searchKey);
        $employeeList = array();
        foreach ($employees as $emp) {
            $temp['value'] = $emp['approverID'];
            $temp['text'] = $emp['firstName'];
            $employeeList[] = $temp;
        }
        return $employeeList;
    }
}