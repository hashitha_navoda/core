<?php

namespace Settings\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Settings\Model\SmsConfiguration;

class SmsNotificationService extends BaseService
{
    /**
    * this function use to update sms type config
    * @param array $data
    * return array
    **/
    public function updateSmsType($data)
    {
        $smsTypeData = [
            'smsTypeMessage' => $data->smsTypeMessage,
            'isIncludeInvoiceDetails' => (bool) $data->isIncludeInvoiceDetails ? 1 : 0,
            'smsSendType' =>(int) $data->smsSendType,
            'isEnable' => 1,
            'smsTypeDayFrame' => $data->dayFrame,
            'smsTypeDayFrameDay' => (int) $data->smsTypeDayFrameDay,
            'smsTypeSelectedCustomerType' => (int) $data->smsTypeSelectedCustomerType
        ];
        $response = $this->getModel('SmsTypeTable')->updateSmsType($smsTypeData,(int) $data->smsTypeId);
        if($response){
            if(\sizeof($data->includeInVoiceDetails)>0){
                foreach ($data->includeInVoiceDetails as $key ) {
                    $smsTypeIncludeData=[
                        'isSelected' => (bool) $key["isChecked"]
                    ];
                    $responseSmsTypeIncluded = $this->getModel('SmsTypeIncludedTable')->updateSmsTypeIncluded($smsTypeIncludeData,(int) $key["value"]);
                }
            }
            return $this->returnSuccess(null, 'SUCCESS_SMS_TYPE_SAVED');
        }else{
            return $this->returnError('ERR_SMS_TYPE_SAVED');   
        }
    }

    /**
    * this function use to deactivate sms type config
    * @param int $data
    * return array
    **/
    public function disableSmsTypeNotification($data) {
        $smsTypeData = [
            'isEnable' => 0,
        ];
        $response = $this->getModel('SmsTypeTable')->updateSmsType($smsTypeData,(int) $data);
        if($response){
            return $this->returnSuccess(null, 'SUCCESS_SMS_NOTIFICATION_DEACTIVATED');
        }else{
            return $this->returnError('ERR_SMS_NOTIFICATION_DEACTIVATE');   
        }
    }


    /**
    * this function use to save sms type msg
    * @param array $data
    * return array
    **/
    public function saveSmsTypeMessage($data) {
        $smsTypeData = [
            'smsTypeMessage' => $data->smsTypeMessage,
        ];
        $response = $this->getModel('SmsTypeTable')->updateSmsType($smsTypeData,(int) $data->smsTypeId);
        if($response){
            return $this->returnSuccess(null, 'SUCCESS_SMS_MSG_SAVED');
        }else{
            return $this->returnError('ERR_SMS_MSG_SAVED');   
        }    
    }

    /**
    * this function use to get postdated cheque by date
    * @param array $data
    * return array
    **/
    public function getPostdatedChequeDataByDateRange($data) {
        $smsTypeData = [
            'smsTypeMessage' => $data->smsTypeMessage,
        ];
        $response = $this->getModel('SupplierPaymentMethodNumbersTable')->getPostdatedChequesByDateRange($smsTypeData,(int) $data->smsTypeId);
        if($response){
            return $this->returnSuccess(null, 'SUCCESS_SMS_MSG_SAVED');
        }
        return $this->returnError('ERR_SMS_MSG_SAVED');   
    }

    /**
    * this function use to get sms notification data for POS
    * 
    * return array
    **/
    public function getSmsNotificationDataForPOS() {
        // Get sms configuration and sms service status
        $displaySettings = (object) $this->getModel('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        $smsConfig = (object) $this->getModel('Settings\Model\SmsConfigurationTable')->fetchAll();

        if ($smsConfig) {
            $smsConfiguration = array(
                'serviceProvider' => (int)$smsConfig->serviceProvider,
                'userName' => $smsConfig->userName,
                'password' => md5($smsConfig->password),
                'alias' => $smsConfig->alias,
            );
        }
        $smsConfiguration['smsServiceStatus'] = (int)$displaySettings->smsServiceStatus;

        // Get after invoice details 
        $smsTypeDetails = (object) $this->getModel('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("After Invoice");
        $smsIncludedDetailsByType = (object) $this->getModel('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);
        $smsIncluded =[];

        while ($row = $smsIncludedDetailsByType->current()) {
            $smsIncluded[$row['smsTypeIncludedId']] = array(
                'includedName' => $row['smsIncludedName'],
                'selected' => $row['isSelected'],
            );
        }

        $smsType = array(
            'id' => $smsTypeDetails->smsTypeID,
            'smsTypeName' => $smsTypeDetails->smsTypeName,
            'smsTypeMessage' => $smsTypeDetails->smsTypeMessage,
            'isIncludeInvoiceDetails' => $smsTypeDetails->isIncludeInvoiceDetails,
            'smsSendType' => $smsTypeDetails->smsSendType,
            'isEnable' => $smsTypeDetails->isEnable,
            'included' => $smsIncluded
        );

        $data = array(
            'smsConfiguration'=>$smsConfiguration,
            'smsType' => $smsType,
        );

        return $this->returnSuccess($data); 
    }

    public function sendSmsNotification($data){
        $post = $data;

        // Check telephone number is set
        if(!$post['telephoneNo']){
            return $this->returnError('ERR_SMS_TELEPHONE_NO');
        }

        // Check message is set
        if(!$post['message']){
            return $this->returnError('ERR_SMS_MSG');
        }

        $telephoneNo = $post['telephoneNo'];
        $message = $post['message'];

        // Validate telephone number
        $checkTele = !!preg_match('/^94\d{9}$/', "$telephoneNo");
        
        if (!$checkTele) {
            if (!!preg_match('/^\+94\d{9}$/', "$telephoneNo")) {
                $telephoneNo = ltrim($telephoneNo,'+');
            } else if (!!preg_match('/^0\d{9}$/', "$telephoneNo")) {
                $telephoneNo = ltrim($telephoneNo,'0');
                $telephoneNo = "94".$telephoneNo;
            } else if (!!preg_match('/\d{9}$/', "$telephoneNo")) {
                $telephoneNo = "94".$telephoneNo;
            } 
        }
        $checkTele = !!preg_match('/^94\d{9}$/', "$telephoneNo");
        
        if (!$checkTele) {
            return $this->returnError('ERR_TELEPHONE_NO');
        }
        
        $displaySettings = (object) $this->getModel('DisplaySetupTable')->fetchAllDetails()->current();

        // Check sms service is enable or not
        if (!$displaySettings->smsServiceStatus) {
            return $this->returnError('ERR_SMS_DISABLE_NOTIFICATION_SERVICE');
        }

        $smsConf = (object) $this->getModel('SmsConfigurationTable')->fetchAll();
        $res = $this->sendSms($telephoneNo, $message);

        if((int) $smsConf->serviceProvider == 1){ // Check sms send status while sms service provider is MOBITEL
            if((int)$res == 200){
                return $this->returnSuccess(null, 'SUCCESS_SMS_SEND');
            }
        }elseif((int) $smsConf->serviceProvider == 2){ // Check sms send status while sms service provider is DIALOG
            if($res){
                return $this->returnSuccess(null, 'SUCCESS_SMS_SEND');
            }
        }

        return $this->returnError('ERR_SMS_SEND');
    }

}
