<?php

namespace Settings\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Settings\Model\SmsConfiguration;

class SmsSettingService extends BaseService
{
    /**
    * this function use to save sms conf
    * @param array $data
    * return array
    **/
    public function saveSmsConfiguration($data)
    {
        if((int) $data->serviceProvider == 1){
            require_once('public/ESMSWS.php');
            $res = serviceTest("",$data->username,$data->password,"");
            
            if ($res->return == "SUCCESS") {
                $session = createSession('',$data->username,$data->password,'');
                $session_validity = isSession($session);
                if ($session_validity) {
                    $msgRes = sendMessages($session,$data->alias,'Test message form ezbiz',array($data->telephone),1); 
                    closeSession($session);
                    if ($msgRes == 169) {
                        return $this->returnError('ERR_INVALID_ALIAS');
                    } else if ($msgRes == 200) {
                        $data = array(
                            'serviceProvider' => $data->serviceProvider,
                            'userName' => $data->username,
                            'password' => $data->password,
                            'alias' => $data->alias,
                            'clientRef' => $data->clientRef,
                        );

                        $confData = new SmsConfiguration();
                        $confData->exchangeArray($data);

                        $checkSmsConf = $this->getModel('SmsConfigurationTable')->fetchAll();
                        if (!$checkSmsConf) {
                            $savedResult = $this->getModel('SmsConfigurationTable')->saveSmsConfiguration($confData);
                            if (!$savedResult) {
                                return $this->returnError('ERR_SMS_CONF_SAVE');
                            } else {
                                $displayData = [
                                    'smsServiceStatus' => 1,
                                ];
                                $saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);
                                return $this->returnSuccess(null, 'SUCCESS_SMS_CONF_SAVE');
                            }
                        } else {
                            $savedResult = $this->getModel('SmsConfigurationTable')->updateSmsConfiguration($checkSmsConf['smsConfigurationId'], $confData);
                            if (!$savedResult) {
                                return $this->returnError('ERR_SMS_CONF_SAVE');
                            } else {
                                $displayData = [
                                    'smsServiceStatus' => 1,
                                ];
                                $saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);
                                return $this->returnSuccess(null, 'SUCCESS_SMS_CONF_SAVE');
                            }
                        }
                        
                    } else {
                        return $this->returnError('ERR_INVALID_SMS_CONF');
                    }

                } else {
                    return $this->returnError('ERR_INVALID_USERNAME_PWRD_SESSION');
                }
            } else {
                return $this->returnError('ERR_INVALID_USERNAME_PWRD');
            }
        }else if((int) $data->serviceProvider == 2){
            $data = array(
                'serviceProvider' => $data->serviceProvider,
                'userName' => $data->username,
                'password' => $data->password,
                'alias' => $data->alias,
                'clientRef' => $data->clientRef,
            );
            $confData = new SmsConfiguration();
            $confData->exchangeArray($data);

            $checkSmsConf = $this->getModel('SmsConfigurationTable')->fetchAll();
            if (!$checkSmsConf) {
                $savedResult = $this->getModel('SmsConfigurationTable')->saveSmsConfiguration($confData);
                if (!$savedResult) {
                    return $this->returnError('ERR_SMS_CONF_SAVE');
                } else {
                    $displayData = [
                        'smsServiceStatus' => 1,
                    ];
                    $saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);
                    return $this->returnSuccess(null, 'SUCCESS_SMS_CONF_SAVE');
                }
            } else {
                $savedResult = $this->getModel('SmsConfigurationTable')->updateSmsConfiguration($checkSmsConf['smsConfigurationId'], $confData);
                if (!$savedResult) {
                    return $this->returnError('ERR_SMS_CONF_SAVE');
                } else {
                    $displayData = [
                        'smsServiceStatus' => 1,
                    ];
                    $saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);
                    return $this->returnSuccess(null, 'SUCCESS_SMS_CONF_SAVE');
                }
            }
        }
    }

    public function disableSmsConfigurationAction() {
        $displayData = [
            'smsServiceStatus' => 0,
        ];
        $saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);
        return $this->returnSuccess(null, 'SUCCESS_SMS_CONF_DEACTIVATED');
    }
}