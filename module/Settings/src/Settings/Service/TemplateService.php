<?php

namespace Settings\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Settings\Model\SmsConfiguration;

class TemplateService extends BaseService
{
    /**
    * this function use to update template details
    * @param array $data
    * return array
    **/
    public function updateTemplateDetails($data)
    {
        foreach ($data['templateDetails'] as $value) {
            $updateData = [
                'posTemplateDetailsIsSelected' => (int)$value['value'],
            ];
            $response = $this->getModel('PosTemplateDetailsTable')->updateTemplateDetails($updateData,(int) $value['id']);
        }

        $updateDataForAdditionalText = ['additionalTextForPos' => $data['additionalText']];
        $res = $this->getModel('CompanyTable')->updateCompanyDetails($updateDataForAdditionalText);

        if($response){
        return $this->returnSuccess(null, 'SUCC_UPDATE_POS_TEMPLATE');
        }else{
            return $this->returnError('ERR_UPDATE_POS_TEMPLATE');   
        }
    }
}