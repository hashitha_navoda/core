<?php

namespace Settings\Service;

use Core\Contracts\SearchableFromCode;
use Inventory\Model\CompoundCostType;
use Settings\Model\DiscountScheme;
use Settings\Model\DiscountSchemeCondition;
use Core\Service\BaseService;
use Illuminate\Support\Collection;
use Illuminate\Validation\Factory as Validator;


class DiscountSchemeService extends BaseService
{
	
	public function saveDiscountScheme($data, $locationID, $userId)
	{

		// first need to check cost type code exist or not
		$schemeDetails = $this->getModel('DiscountSchemeTable')->getSchemeByCode($data['schemeCode'])->current();
        $discountType = ($data['discountType'] == '1') ? 'value' : 'percentage';
        if ($schemeDetails != null) {
        	return [
        		'status' => false,
        		'msg' => 'ERR_SCHEME_CODE_USE'
        	];
        }

        // check product code availability
        $productCode = 'NONINVENT_'.$data['costCode'];
        $productDetails = $this->getModel('ProductTable')->getProductByCode($productCode,false,false,true);

        if ($productDetails != null) {
        	return [
        		'status' => false,
        		'msg' => 'ERR_PRODUCT_CODE_IN_USE'
        	];	
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];
        
        // save Cost Type
        $schemArr = [
        	'discountSchemeCode' => $data['schemeCode'],
        	'discountSchemeName' => $data['schemeName'],
            'discountType' => $discountType,
            'locationID' => $locationID,
            'entityID' => $entityID
        ];
        $schemeData = new DiscountScheme();
        $schemeData->exchangeArray($schemArr);

        $saveDiscountScheme = $this->getModel('DiscountSchemeTable')->save($schemeData);
        if ($saveDiscountScheme == null) {
        	return [
        		'status' => false,
        		'msg' => 'ERR_DIS_SCHEME_SAVE'
        	];	
        }



        foreach ($data['addedSchemeRules'] as $key => $value) {

            $discountValue = null;
            $discountPercentage = null;

            if ($discountType == 'value') {
                $discountValue = $value['discount'];
            }

            if ($discountType == 'percentage') {
                $discountPercentage = $value['discount'];
            }

            // save scheme condtions
            $conditionsDataSet = [
            	'discountSchemesID' => $saveDiscountScheme,
            	'minQty' => $value['minQty'],
                'maxQty' => $value['maxQty'],
            	'discountType' => $discountType,
            	'discountPercentage' => $discountPercentage,
            	'discountValue' => $discountValue
            ];

            $schemeConData = new DiscountSchemeCondition();
            $schemeConData->exchangeArray($conditionsDataSet);

            $saveDiscountSchemeCondtion = $this->getModel('DiscountSchemeConditionTable')->save($schemeConData);
            if ($saveDiscountSchemeCondtion == null) {
                return [
                    'status' => false,
                    'msg' => 'ERR_DIS_SCHEME_SAVE'
                ];  
            }
        }
        
        return [
    		'status' => true,
    		'msg' => 'SUCC_SAVE_DISC_SCHEME'
    	];
	}

    public function getSchemeConditions($data)
    {

        $schemeConditions = $this->getModel('DiscountSchemeConditionTable')->getSchemeConditionsBySchemeID($data['schemeID']);

        if (!$schemeConditions) {
            return [
                'status' => false,
                'msg' => 'ERR_PRODUCT_DELETE'
            ];   
        }
        return $schemeConditions;
    }


    public function searchDiscountSchemeForDropdown($data)
    {


        $searchKey = $data['searchKey'];

        $schems = $this->getModel('DiscountSchemeTable')->searchAccountsForDropDown($searchKey);

        $schemList = array();
        foreach ($schems as $accts) {
            $temp['value'] = $accts['discountSchemesID'];
            $temp['text'] = $accts['discountSchemeCode']."_".$accts['discountSchemeName'];
            $schemList[] = $temp;
        }

        return $schemList;
    }

    public function deleteScheme($data, $userId)
    {

        $schemeData = $this->getModel('DiscountSchemeTable')->getSchemeBySchemeID($data['schemeID']);
        $relatedItems = $this->getModel('ProductTable')->getProductsByDiscountSchemeID($data['schemeID']);


        if (sizeof($relatedItems) > 0) {
            return [
                'status' => false,
                'msg' => 'ERR_HAS_RELATED_ITEMS'
            ];   
        }

        $entityService = $this->getService('EntityService');
        $deleteScheme = $entityService->updateDeleteInfoEntity($schemeData['entityID'], $userId);

       
        if (!$deleteScheme) {
            return [
                'status' => false,
                'msg' => 'ERR_SCHEME_DELETE'
            ];   
        }
        return [
            'status' => true,
            'msg' => 'SUCC_SCHEME_DELE'
        ];
    }

    public function searchDiscountScheme($data)
    {
        $paginated = false;
        if (!empty($data['searchKey'])) {
            $costTypeList = $this->getModel('DiscountSchemeTable')->discountSchemeSearchByKey($data['searchKey']);
            $status = true;
        } else {
            $status = true;
            $landedCT = $this->getServiceLocator()->get("DiscountSchemesController");
            $costTypeList =  $landedCT->getPaginatedCostTypes(6, true);
            $paginated = true;
        }
        return [
            'status' => $status,
            'schemeList' => $costTypeList,
            'paginated' => $paginated
        ];
        
    }

}