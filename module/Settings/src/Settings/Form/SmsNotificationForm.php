<?php
namespace Settings\Form;

use Zend\Form\Form;

class SmsNotificationForm extends Form {

    public function __construct($data) {
        parent::__construct('customCurrency');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->setAttribute('id', "smsNotificationForm");
        
        $this->add(array(
            'name' => 'messageDescription',
            'type' => 'Textarea',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'messageDescription',
                'class' => 'form-control _disable_hor_resize _custom_textarea',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'smsIncluded',
            'type' => 'Zend\Form\Element\MultiCheckbox',
             'options' => array(
                'value_options' => $data['smsIncludedDetails'],
                'label_attributes' => array(
                    'class' => 'check_box_sms',
                ),
            ),
            'attributes' => array(
                'class' => 'taxes check_box_mar_right',
            ),
        ));
        $this->add(array(
            'name' => 'smsSendType',
            'type' => 'Zend\Form\Element\Radio',
            'attributes' => array(
                'id' => 'smsSendType',
                'class' => 'smsSendType'
            ),
            'options' => array(
                'value_options' => $data['smsSendType'],
                'label_attributes' => array(
                    'class' => 'radio_button',
                ),
            )
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'save-sms-setting',
                'class' => 'btn btn-primary make-full-width',
            ),
        ));
    }

}
