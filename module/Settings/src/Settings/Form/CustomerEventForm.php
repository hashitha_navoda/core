<?php

namespace Settings\Form;

use Zend\Form\Form;

/**
 * Description of CustomerEventForm
 *
 * @author shermilan
 */
class CustomerEventForm extends Form {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('name', 'create-customer-form');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'customerEventID',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'customerEventID'
            ),
        ));

        $this->add(array(
            'name' => 'customerEventName',
            'type' => 'text',
            'attributes' => array(
                'id' => 'customerEventName',
                'class' => 'form-control',
                'placeholder' => _(''),
            ),
        ));

        $this->add(array(
            'name' => 'customerEventDuration',
            'type' => 'text',
            'attributes' => array(
                'id' => 'customerEventDuration',
                'class' => 'form-control',
                'placeholder' => _(''),
            ),
        ));

        $this->add(array(
            'name' => 'customerEventStartDate',
            'type' => 'text',
            'attributes' => array(
                'id' => 'customerEventStartDate',
                'class' => 'form-control',
                'class' => 'form-control curser-pointer',
                'style' => 'background-color: white',
                'readonly' => TRUE,
            ),
        ));

        $this->add(array(
            'name' => 'customerEventEndDate',
            'type' => 'text',
            'attributes' => array(
                'id' => 'customerEventEndDate',
                'class' => 'form-control',
                'class' => 'form-control curser-pointer',
                'style' => 'background-color: white',
                'readonly' => TRUE,
            ),
        ));

        $this->add(array(
            'name' => 'saveCustomerEvent',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'saveCustomerEvent',
                'class' => 'btn btn-primary make-full-width',
                'value' => _('Save'),
            ),
        ));

        $this->add(array(
            'name' => 'cancelCustomerEvent',
            'attributes' => array(
                'type' => 'reset',
                'id' => 'cancelCustomerEvent',
                'class' => 'btn btn-default make-full-width',
                'value' => _('Cancel'),
            ),
        ));

        $this->add(array(
            'name' => 'saveAndCreateCustomer',
            'attributes' => array(
                'type' => 'button',
                'id' => 'saveAndCreateCustomer',
                'class' => 'btn btn-primary make-full-width',
                'value' => _('Save & Create Customer'),
            ),
        ));

        $this->add(array(
            'name' => 'updateCustomerEvent',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'updateCustomerEvent',
                'class' => 'btn btn-primary make-full-width',
                'value' => _('Update')
            ),
        ));

        $this->add(array(
            'name' => 'resetCustomerEvent',
            'attributes' => array(
                'type' => 'reset',
                'id' => 'resetCustomerEvent',
                'class' => 'btn btn-default',
                'value' => _('Reset')
            ),
        ));

        $this->add(array(
            'name' => 'updateAndCreateCustomer',
            'attributes' => array(
                'type' => 'button',
                'id' => 'updateAndCreateCustomer',
                'class' => 'btn btn-primary make-full-width',
                'value' => _('Update & Create Customer'),
            ),
        ));
    }

}
