<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains company related Form fields
 */

namespace Settings\Form;

use Zend\Form\Form;

class CompanyForm extends Form {

    public function __construct($countries = NULL) {
        parent::__construct('company');
        $this->setAttribute('method', 'post');
//        $this->setAttribute('onsubmit', 'validate_form();');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'companyName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'companyName',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'companyAddress',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'companyAddress',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'postalCode',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'postalCode',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'telephoneNumber',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'telephoneNumber',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'telephoneNumber2',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'telephoneNumber2',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'telephoneNumber3',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'telephoneNumber3',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'faxNumber',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'faxNumber',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'email',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'email',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'country',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'country',
                'class' => 'form-control selectpicker',
                'style' => '',
                'data-live-search' => true,
            ),
            'options' => array(
                'empty_option' => 'Select a country',
                'value_options' => $countries,
            )
        ));
        $this->add(array(
            'name' => 'website',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'website',
                'class' => 'form-control',
                'style' => '',
                'placeholder' => 'eg. http://website.com'
            ),
        ));
        $this->add(array(
            'name' => 'brNumber',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'brNumber',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'create-company-button',
                'class' => 'btn btn-primary make-full-width',
            ),
        ));
    }

}
