<?php

namespace Settings\Form;

use Zend\Form\Form;

class ItemAttributeForm extends Form
{

    public function __construct($data)
    {
        parent::__construct('itemAttribute');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $uom = $data['uomList'];

        $this->add(array(
            'name' => 'itemAttributeID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'itemAttributeCode',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'itemAttributeCode',
                'class' => 'form-control',
                'placeholder' => _('code')
            ),
        ));

        $this->add(array(
            'name' => 'itemAttributeName',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'itemAttributeName',
                'class' => 'form-control',
                'placeholder' => _('name')
            ),
        ));

        $this->add(array(
            'name' => 'itemAttributeState',
            'type' => 'select',
            'options' => array(
                'value_options' => array(
                    1 => 'Active',
                    0 => 'Inactive'
                )
            ),
            'attributes' => array(
                'id' => 'itemAttributeState',
                'class' => 'form-control'
            ),
        ));
        $this->add(array(
            'name' => 'itemAttributeUom',
            'type' => 'select',
            'options' => array(
                'default_value' =>'none',
                'value_options' => $uom,
            ),
            'attributes' => array(
                'id' => 'itemAttributeUom',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'id' => 'add-item-attribute-button',
                'class' => 'btn btn-primary',
            ),
        ));
    }

}
