<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains display related Form fields
 */

namespace Settings\Form;

use Zend\Form\Form;

class DisplayForm extends Form
{

    public function __construct($data = NULL, $dateformats)
    {

        parent::__construct('display');
        $this->setAttribute('method', 'post');
        $this->setAttribute('onsubmit', 'validate_form();');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'displaySetupID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'currencyID',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'currencyID',
                'class' => 'form-control selectpicker',
                'style' => '',
                'placeholder' => _('SriLankan Ruppes'),
                'value' => $data['currencyCountry'],
            ),
            'options' => array(
//                'default_option' => 'Select Currencyyyyy',
                'value_options' => $data['currency'],
            ),
        ));

        $this->add(array(
            'name' => 'timeZone',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'timeZone',
                'class' => 'form-control selectpicker',
                'style' => '',
                'data-live-search' => "true",
                'data-size' => '8',
            ),
            'options' => array(
                'value_options' => $data['timeZoneList'],
            ),
        ));
        $this->add(array(
            'name' => 'dateFormat',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'dateFormat',
                'class' => 'form-control selectpicker',
                'style' => '',
                'placeholder' => 'DD-MM-YYYY',
            ),
            'options' => array(
                'value_options' => isset($dateformats) ? $dateformats : NULL,
            )
        ));
        $this->add(array(
            'name' => 'timeFormat',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'timeFormat',
                'class' => 'form-control selectpicker',
                'style' => '',
                'placeholder' => '24 Hour',
            ),
            'options' => array(
                'value_options' => array(
                    '12' => '12 Hours',
                    '24' => '24 Hours'
                ),
            )
        ));
        $this->add(array(
            'name' => 'customerCreditLimitApplyFlag',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => true,
                'uncheckedValue' => false
            ),
            'attributes' => array(
                'id' => 'customerCreditLimitApplyFlag'
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'create-display-button',
                'class' => 'btn btn-primary',
            ),
        ));
        $this->add(array(
            'name' => 'posPrintoutImageConversionStatus',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => true,
                'uncheckedValue' => false
            ),
            'attributes' => array(
                'id' => 'posPrintoutImageConversionStatus'
            ),
        ));
    }

}
