<?php

namespace Settings\Form;

use Zend\Form\Form;

class LoyaltyForm extends Form {

    public function __construct($data) {
        parent::__construct('loyalty_form');
        $this->setAttribute("class", "form-horizontal");

        $this->add(array(
            'name' => 'loyaltyID',
            'type' => 'hidden',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->id) ? $data->id : null
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyName',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->name) ? $data->name : null
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyPrefix',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->prefix) ? $data->prefix : null
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyCodeDegits',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->code_degits) ? $data->code_degits : null
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyCodeCurrentNo',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->code_current_no) ? $data->code_current_no : null
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyNoOfCards',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->no_of_cards) ? $data->no_of_cards : null
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyActiveMinVal',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->active_min_val) ? $data->active_min_val : 0
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyActiveMaxVal',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->active_max_val) ? $data->active_max_val : 0
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyEarningPerPoint',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->earning_per_point) ? $data->earning_per_point : null
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyRedeemPerPoint',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'value' => isset($data->redeem_per_point) ? $data->redeem_per_point : null
            ),
        ));
        $this->add(array(
            'name' => 'loyaltyStatus',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'checked_value' => "1",
                'unchecked_value' => "0",
            ),
            'attributes' => array(
                'id' => 'loyaltyStatus',
                'data-on-text' => 'Enable',
                'data-off-text' => 'Disable',
                'data-handle-width' => 'auto',
                'class' => 'checkbox',
                'value' => isset($data->status) ? $data->status : null
            ),
        ));

        $this->add(array(
            'name' => 'loyaltyGlAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'loyaltyGlAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));
        
        $this->add(array(
            'name' => 'save',
            'type' => 'submit',
            'attributes' => array(
                'class' => 'btn btn-primary make-full-width saveLoyalty',
                'value' => ($data == null) ? _('Save Card') : _('Update Card'),
            ),
        ));
    }

}
