<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Settings\Form;

use Zend\Form\Form;

class CreatePromotionForm extends Form {

    public function __construct($promotionType, $dateFormat, $currentdate) {
        parent::__construct('createPromotion');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->setAttribute('id', "createPromotionForm");



        $this->add(array(
            'name' => 'promotionName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'promotionName',
                'autocomplete' => false,
                'autofocus' => true,
                'class' => 'form-control',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'promotionFrom',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'promotionFrom',
                'class' => 'form-control',
                'placeholder' => _('Date'),
                'data-date-format' => $dateFormat,
                'value' => $currentdate,
            ),
        ));

        $this->add(array(
            'name' => 'promotionTo',
            'type' => 'Text',
            'attributes' => array(
                'id' => 'promotionTo',
                'class' => 'form-control',
                'placeholder' => _('Date'),
                'data-date-format' => $dateFormat,
                'value' => $currentdate,
            ),
        ));


        $this->add(array(
            'name' => 'promotionType',
            'type' => 'Zend\Form\Element\Select',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'promotionType',
                'class' => 'form-control',
            ),
            'options' => array(
                'value_options' => isset($promotionType) ? $promotionType : NULL,
            )
        ));

        $this->add(array(
            'name' => 'promotionDescription',
            'type' => 'Textarea',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'promotionDescription',
                'class' => 'form-control _disable_hor_resize _custom_textarea',
                'autocomplete' => 'off'
            ),
        ));
    }

}
