<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains Tax Form.
 */

namespace Settings\Form;

use Zend\Form\Form;

class TaxForm extends Form
{

    public function __construct($data)
    {

        parent::__construct($data['name']);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $data['name']);


        $this->add(array(
            'name' => 'tax_id',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'tax_id',
            ),
        ));

        $this->add(array(
            'name' => 'has_tax',
            'type' => 'Zend\Form\Element\Radio',
            'attributes' => array(
                'value' => isset($data["chrg_tax"]) ? $data["chrg_tax"] : 'y',
            ),
            'options' => array(
                'value_options' => array(
                    'y' => _(' Yes'),
                    'n' => _(' No'),
                ),
                'label_attributes' => array(
                    'class' => 'radio_button',
                ),
            )
        ));

        $this->add(array(
            'name' => 'tax_reg',
            'type' => 'Text',
            'attributes' => array(
                'value' => $data['tr_number'],
                'class' => 'form-control',
                'id' => 'tax_reg',
                'placeholder' => _('eg : 88')
            ),
        ));

        $this->add(array(
            'name' => 'taxName',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'tax_name',
                'placeholder' => _('eg : Sales Tax')
            ),
        ));

        $this->add(array(
            'name' => 'taxPrecentage',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'amount',
                'placeholder' => _('eg : 10%')
            ),
        ));

        $this->add(array(
            'name' => 'taxType',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checked_value' => 'c',
                'unchecked_value' => 'v',
            ),
            'attributes' => array(
                'id' => 'compound',
            ),
        ));
        /**
         * Taxes for compound tax
         */
        $this->add(array(
            'name' => 'compoundTax',
            'type' => 'Zend\Form\Element\MultiCheckbox',
            'options' => array(
                'value_options' => $data['taxes'],
                'label_attributes' => array(
                    'class' => 'check_box',
                ),
            ),
            'attributes' => array(
                'class' => 'taxes check_box_mar_right',
            ),
        ));

        $this->add(array(
            'name' => 'taxSuspendable',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checked_value' => true,
                'unchecked_value' => false,
            ),
            'attributes' => array(
                'id' => 'taxSuspendable',
            ),
        ));

        $this->add(array(
            'name' => 'taxSalesAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'taxSalesAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an account',
            )
        ));

        $this->add(array(
            'name' => 'taxPurchaseAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'taxPurchaseAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an account',
            )
        ));
    }

}

?>
