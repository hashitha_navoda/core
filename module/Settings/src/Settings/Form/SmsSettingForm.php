<?php
namespace Settings\Form;

use Zend\Form\Form;

class SmsSettingForm extends Form {

    public function __construct($serviceProvider) {
        parent::__construct('customCurrency');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        
        $this->add(array(
            'name' => 'serviceProvider',
            'type' => 'Select',
            
            'options' => array(
                'empty_option' => 'Select a service provider',
                'value_options' => $serviceProvider,
            ),
            'populate'=>(array("currency" => "US Dollar")),
            'attributes' => array(
                'id' => 'serviceProvider',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'uName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'uName',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'pword',
            'type' => 'password',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'pword',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'alias',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'alias',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'telephone',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'telephone',
                'class' => 'form-control',
                'style' => '',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'save-sms-setting',
                'class' => 'btn btn-primary make-full-width',
            ),
        ));
    }

}
