<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains custom currancy related Form fields
 */

namespace Settings\Form;

use Zend\Form\Form;

class CustomCurrencyForm extends Form {

    public function __construct($data) {
        parent::__construct('customCurrency');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'customCurrencyId',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => 'customCurrencyName',
            'type' => 'Select',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'customCurrencyName',
                'class' => 'form-control',
                'style' => '',
            ),
            'options' => array(
                'value_options' => $data['currencyList'],
            )
        ));
        $this->add(array(
            'name' => 'customCurrencySymbol',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'customCurrencySymbol',
                'class' => 'form-control',
                'style' => '',
            ),
        ));
        $this->add(array(
            'name' => 'customCurrencyRate',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'customCurrencyRate',
                'class' => 'form-control',
                'style' => '',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Save',
                'id' => 'create-currency-button',
                'class' => 'btn btn-primary make-full-width',
            ),
        ));
    }

}
