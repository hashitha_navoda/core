<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Settings\Form;

use Zend\Form\Form;

class UomForm extends Form
{

    public function __construct()
    {
        parent::__construct('uom');
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");

        $this->add(array(
            'name' => 'uomID',
            'type' => 'Hidden',
        ));

        $this->add(array(
            'name' => 'uomName',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'uomName',
                'class' => 'form-control',
                'placeholder' => _('Eg: Kilogram')
            ),
        ));

        $this->add(array(
            'name' => 'uomAbbr',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'uomAbbr',
                'class' => 'form-control',
                'placeholder' => _('Eg: kg')
            ),
        ));

        $this->add(array(
            'name' => 'uomDecimalPlace',
            'type' => 'select',
            'options' => array(
                'value_options' => array(0, 1, 2, 3, 4)
            ),
            'attributes' => array(
                'id' => 'uomDecimalPlace',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'uomState',
            'type' => 'select',
            'options' => array(
                'value_options' => array(
                    1 => 'Active',
                    0 => 'Inactive'
                )
            ),
            'attributes' => array(
                'id' => 'uomState',
                'class' => 'form-control'
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Add Category',
                'id' => 'add-uom-button',
                'class' => 'btn btn-primary',
                'data-dismiss' => "modal",
            ),
        ));
    }

}
