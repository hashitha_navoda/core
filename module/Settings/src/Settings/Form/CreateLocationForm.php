<?php

namespace Settings\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class CreateLocationForm extends Form
{

    public function __construct($statusArray)
    {
        // we want to ignore the name passed
        parent::__construct('location');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');

        $this->add(array(
            'name' => 'locationID',
            'attributes' => array(
                'type' => 'hidden',
                'class' => 'form-control',
                'id' => 'locationID'
            ),
        ));

        $this->add(array(
            'name' => 'locationCode',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('ABC-123'),
                'id' => 'locationCode'
            ),
        ));
        $this->add(array(
            'name' => 'locationName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('Location Name'),
                'id' => 'locationName'
            ),
        ));
        $this->add(array(
            'name' => 'locationEmail',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('example@email.com'),
                'id' => 'locationEmail'
            ),
        ));
        $this->add(array(
            'name' => 'locationTelephone',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
//                'placeholder' => '+94XXXXXXX, +94XXXXXXX, +94XXXXXXX',
                'id' => 'locationTelephone'
            ),
        ));
        $this->add(array(
            'name' => 'locationFax',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
//                'placeholder' => '+94XXXXXXX, +94XXXXXXX, +94XXXXXXX',
                'id' => 'locationFax'
            ),
        ));
        $this->add(array(
            'name' => 'locationAddressLine1',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('No: 19'),
                'id' => 'locationAddressLine1'
            ),
        ));
        $this->add(array(
            'name' => 'locationAddressLine2',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('1st Lane'),
                'id' => 'locationAddressLine2'
            ),
        ));
        $this->add(array(
            'name' => 'locationAddressLine3',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('City'),
                'id' => 'locationAddressLine3'
            ),
        ));
        $this->add(array(
            'name' => 'locationAddressLine4',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('Country'),
                'id' => 'locationAddressLine4'
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'locationStatus',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'locationStatus',
            ),
            'options' => array(
                'value_options' => $statusArray,
            )
        ));

        $this->add(array(
            'name' => 'add-location-button',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save Location'),
                'id' => 'add-location-button',
                'class' => 'btn btn-primary'
            ),
        ));

        $this->add(array(
            'name' => 'reset-location-button',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Reset'),
                'id' => 'reset-location-button',
                'class' => 'btn btn-default'
            ),
        ));

        $this->add(array(
            'name' => 'list-location-button',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Back'),
                'id' => 'list-locations',
                'class' => 'btn btn-default'
            ),
        ));

        $this->add(array(
            'name' => 'locationTax',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => false,
                'checked_value' => '1',
                'unchecked_value' => '0'
            ),
            'attributes' => array(
                'id' => 'locationTax',
                'data-on-text' => 'Enable',
                'data-off-text' => 'Disable',
                'data-handle-width' => 'auto',
                'class' => 'checkbox',
                'value' => "1"
            ),
        ));
    }

}
