<?php

/**
 * @author PRASA7  <prasanna@thinkcube.com>
 * This class test the user test table funtions
 */

namespace UserTest\Model;

use User\Model\UserTable;
use User\Model\User;
use Zend\Db\ResultSet\ResultSet;
use PHPUnit_Framework_TestCase;

class UserTableTest extends PHPUnit_Framework_TestCase
{

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function tests whether fetch all returns all users
     */
    public function testFetchAllReturnsAllUsers()
    {
        $resultSet = new ResultSet();
        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with()
                ->will($this->returnValue($resultSet));

        $userTable = new UserTable($mockTableGateway);

        $this->assertSame($resultSet, $userTable->fetchAll());
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function tests whether user can be retrieved by ID
     */
    public function testCanRetrieveUserByID()
    {
        $user = new User();
        $data = array(
            'userID' => 123,
            'username' => "JohnStv9",
            'firstName' => "Jeo",
            'lastName' => "Perera",
            'email1' => "jeo@abc.com",
            'password' => md5($user->userID + $user->password + $user->userID),
            'roleID' => 23,
            'createdTimeStamp' => time()
        );
        $user->exchangeArray($data);

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new User());
        $resultSet->initialize(array($user));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('userID' => 123))
                ->will($this->returnValue($resultSet));

        $userTable = new UserTable($mockTableGateway);

        $this->assertSame($user, $userTable->getUser(123));
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function tests whether the save user will insert new users if they dont already have an ID
     */
    public function testSaveUserWillInsertNewUsersIfTheyDontAlreadyHaveAnId()
    {
        $user = new User();

        $userData = array(
            'username' => "JohnStv9",
            'firstName' => "Jeo",
            'lastName' => "Perera",
            'email1' => "jeo@abc.com",
            'password' => md5($user->userID + $user->password + $user->userID),
            'roleID' => 23,
            'createdTimeStamp' => time());

        $user->exchangeArray($userData);

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('insert'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('insert')
                ->with($userData);

        $userTable = new UserTable($mockTableGateway);
        $userTable->saveUser($user);
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function tests whether the save user will update existing users if they already have an ID
     */
    public function testSaveUserWillUpdateExistingUsersIfTheyAlreadyHaveAnId()
    {
        $user = new User();

        $userData = array(
            'userID' => 123,
            'username' => "JohnStv9",
            'firstName' => "Jeo",
            'lastName' => "Perera",
            'email1' => "jeo@abc.com",
            'password' => md5($user->userID + $user->password + $user->userID),
            'roleID' => 23,
            'createdTimeStamp' => time());

        $user->exchangeArray($userData);

        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new User());
        $resultSet->initialize(array($user));

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select', 'update'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('userID' => 123))
                ->will($this->returnValue($resultSet));
        $mockTableGateway->expects($this->once())
                ->method('update')
                ->with(array(
                    'username' => "JohnStv9",
                    'firstName' => "Jeo",
                    'lastName' => "Perera",
                    'email1' => "jeo@abc.com",
                    'password' => $user->password,
                    'roleID' => 23,
                    'createdTimeStamp' => time()), array('userID' => 123));

        $userTable = new UserTable($mockTableGateway);
        $userTable->saveUser($user);
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function tests whether exception is thrown when getting nonexistent user
     */
    public function testExceptionIsThrownWhenGettingNonexistentUser()
    {
        $resultSet = new ResultSet();
        $resultSet->setArrayObjectPrototype(new User());
        $resultSet->initialize(array());

        $mockTableGateway = $this->getMock('Zend\Db\TableGateway\TableGateway', array('select'), array(), '', false);
        $mockTableGateway->expects($this->once())
                ->method('select')
                ->with(array('userID' => 123))
                ->will($this->returnValue($resultSet));

        $userTable = new UserTable($mockTableGateway);

        try {
            $userTable->getUser(123);
        } catch (\Exception $e) {
            $this->assertSame('Could not find row 123', $e->getMessage());
            return;
        }

        $this->fail('Expected exception was not thrown');
    }

}

