<?php

namespace UserTest\Model;

use User\Model\User;
use PHPUnit_Framework_TestCase;

class UserTest extends PHPUnit_Framework_TestCase
{

    public function testUserInitialState()
    {
        $user = new User();

        $this->assertNull($user->userID, '"userID" should initially be null');
        $this->assertNull($user->username, '"username" should initially be null');
        $this->assertNull($user->firstName, '"firstName" should initially be null');
        $this->assertNull($user->lastName, '"lastName" should initially be null');
        $this->assertNull($user->email1, '"email1" should initially be null');
        $this->assertNull($user->password, '"password" should initially be null');
        $this->assertNull($user->roleID, '"roleID" should initially be null');
        $this->assertNull($user->createdTimeStamp, '"createdTimeStamp" should initially be null');
    }

    public function testExchangeArraySetsPropertiesCorrectly()
    {
        $user = new User();
        $data = array(
            'userID' => 123,
            'username' => "JohnStv9",
            'firstName' => "Jeo",
            'lastName' => "Perera",
            'email1' => "jeo@abc.com",
            'password' => md5($user->userID + $user->password + $user->userID),
            'roleID' => 23,
            'createdTimeStamp' => time()
        );

        $user->exchangeArray($data);

        $this->assertSame($data['userID'], $user->userID, '"userID" should have defaulted to null');
        $this->assertSame($data['username'], $user->username, '"username" should have defaulted to null');
        $this->assertSame($data['firstName'], $user->firstName, '"firstName" should have defaulted to null');
        $this->assertSame($data['lastName'], $user->lastName, '"lastName" should have defaulted to null');
        $this->assertSame($data['email1'], $user->email1, '"email1" should have defaulted to null');
        $this->assertSame($data['password'], $user->password, '"password" should have defaulted to null');
        $this->assertSame($data['roleID'], $user->roleID, '"roleID" should have defaulted to null');
        $this->assertSame($data['createdTimeStamp'], $user->createdTimeStamp, '"createdTimeStamp " should have defaulted to null');
    }

    public function testExchangeArraySetsPropertiesToNullIfKeysAreNotPresent()
    {
        $user = new User();
        $data = array(
            'userID' => 123,
            'username' => "JohnStv9",
            'firstName' => "Jeo",
            'lastName' => "Perera",
            'email1' => "jeo@abc.com",
            'password' => md5($user->userID + $user->password + $user->userID),
            'roleID' => 23,
            'createdTimeStamp' => time()
        );

        $user->exchangeArray($data);

        $this->assertSame($data['userID'], $user->userID, '"userID" was not set correctly');
        $this->assertSame($data['username'], $user->username, '"username" was not set correctly');
        $this->assertSame($data['firstName'], $user->firstName, '"firstName" was not set correctly');
        $this->assertSame($data['lastName'], $user->lastName, '"lastName" was not set correctly');
        $this->assertSame($data['email1'], $user->email1, '"email1" was not set correctly');
        $this->assertSame($data['password'], $user->password, '"password" was not set correctly');
        $this->assertSame($data['roleID'], $user->roleID, '"roleID" was not set correctly');
        $this->assertSame($data['createdTimeStamp'], $user->createdTimeStamp, '"createdTimeStamp " was not set correctly');
    }

}

