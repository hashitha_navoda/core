<?php

namespace UserTest\Controller;

use UserTest\Bootstrap;
use Zend\Mvc\Router\Http\TreeRouteStack as HttpRouter;
use User\Controller\UserController;
use User\Controller\Plugin\UserAuthenticate;
use Zend\Http\Request;
use Zend\Http\Response;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router\RouteMatch;
use PHPUnit_Framework_TestCase;

class UserControllerTest extends \PHPUnit_Framework_TestCase
{

    protected $controller;
    protected $request;
    protected $response;
    protected $routeMatch;
    protected $event;

    protected function setUp()
    {
        $serviceManager = Bootstrap::getServiceManager();
        $this->controller = new UserController();
        $this->request = new Request();
        $this->routeMatch = new RouteMatch(array('controller' => 'index'));
        $this->event = new MvcEvent();
        $config = $serviceManager->get('Config');
        $routerConfig = isset($config['router']) ? $config['router'] : array();
        $router = HttpRouter::factory($routerConfig);

        $this->event->setRouter($router);
        $this->event->setRouteMatch($this->routeMatch);
        $this->controller->setEvent($this->event);
        $this->controller->setServiceLocator($serviceManager);
        $this->controller->getPluginManager()->setInvokableClass('userauthenticate', "User\Controller\Plugin\UserAuthenticate");
        $this->controller->getPluginManager()->setInvokableClass('logger', "User\Controller\Plugin\Logger");

        //$this->controller->se ('AppEngine-Auth', "AppEngine\Controller\AuthController");
        //$this->controller->setEvent('AppEngine-Auth');
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->routeMatch->setParam('action', 'login');
        $this->routeMatch->setParam('param1', 'user');
        $this->routeMatch->setParam('param2', 'index');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testIndexActionRedirect()
    {
        $this->routeMatch->setParam('action', 'index');

        $result = $this->controller->dispatch($this->request);
        $response = $this->controller->getResponse();

        $this->assertEquals(302, $response->getStatusCode());
    }

}
