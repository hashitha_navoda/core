<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'User\Controller\User' => 'User\Controller\UserController',
            'User\Controller\UserApi' => 'User\Controller\UserApiController',
            'User\Controller\Emailconf' => 'User\Controller\EmailconfController',
            'User\Controller\DashBoard' => 'User\Controller\DashBoardController',
            'User\Controller\DashBoardAPI' => 'User\Controller\DashBoardAPIController',
            'User\Controller\SalesPerson' => 'User\Controller\SalesPersonController',
            'User\Controller\MobileDashBoard' => 'User\Controller\MobileDashBoardController',
            'User\Controller\RestAPI\PosDashBoard' => 'User\Controller\RestAPI\PosDashboardRestfullController',
            'User\Controller\RestAPI\PosUsers' => 'User\Controller\RestAPI\PosUsersRestfullController',
            'User\Controller\RestAPI\PosUserRoles' => 'User\Controller\RestAPI\PosUserRolesRestfullController',
#            'User\Controller\Wizard' => 'User\Controller\WizardController',
#            'User\Controller\WizardAPI' => 'User\Controller\WizardAPIController'
        ),
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'SendMail' => 'User\Controller\Plugin\SendMail',
            'UserAuthenticate' => 'User\Controller\Plugin\UserAuthenticate',
            'Logger' => 'User\Controller\Plugin\Logger',
        ),
    ),
    'router' => array(
        'routes' => array(
            'user' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/user[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\User',
                        'action' => 'index',
                    ),
                ),
            ),
            'userAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/userAPI[/:action]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'UserApi',
                    ),
                ),
            ),
            'user-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/user/[:action]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'UserApi',
                    ),
                ),
            ),
            'SalesPerson' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/salesPerson[/:action][/:param1]',
                    'defaults' => array(
                        '__NAMESPACE__' => 'User\Controller',
                        'controller' => 'SalesPerson',
                        'action' => 'create',
                    ),
                ),
            ),
            'emailconf' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/emailconf[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\Emailconf',
                        'action' => 'index',
                    ),
                ),
            ),
            'dashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dashboard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\DashBoard',
                        'action' => 'index',
                    ),
                ),
            ),
            'dashboardAPI' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/dashboardAPI[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\DashBoardAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'mobileDashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/mobileDashBoard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\MobileDashBoard',
                        'action' => 'index',
                    ),
                ),
            ),
            'posDashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/pos-dashboard[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\RestAPI\PosDashBoard'
                    ),
                ),
            ),
            'posUsers' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/pos-users[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\RestAPI\PosUsers'
                    ),
                ),
            ),
            'posUserRoles' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/user-roles[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\RestAPI\PosUserRoles'
                    ),
                ),
            ),
            'wizard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/wizard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'User\Controller\Wizard',
                        'action' => 'index',
                    ),
                ),
            ),
            'ssoprovider' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/ssoprovider[/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'AppEngine\Controller\Index',
                        'action' => 'ssoprovider',
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'user' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'ssoService' => getenv('SSOSERVICE'),
    'defaultLanguage' => "en_US",
);
