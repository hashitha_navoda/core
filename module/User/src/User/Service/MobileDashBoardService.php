<?php

namespace User\Service;

use Core\Service\ReportService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;

class MobileDashBoardService extends ReportService
{
	public function getInvoiceAndPaidAmountsForDonut($data)
	{
		$currentDate = date('Y-m-d');
		$monthFirstDate = date('Y-m-01');
		$yearFirstDate = date('Y-01-01');
		
		// get sales daily details
		$dailyInvoiceDetails = $this->getModel('InvoiceTable')->getInvoiceTotalByTimePerid($currentDate,null, 'daily')->current();

		// get monthly sales details
		$monthlyInvoiceDetails = $this->getModel('InvoiceTable')->getInvoiceTotalByTimePerid($currentDate, $monthFirstDate, 'monthly')->current();

		// get annual sales details
		$annualInvoiceDetails = $this->getModel('InvoiceTable')->getInvoiceTotalByTimePerid($currentDate, $yearFirstDate, 'annual')->current();

		// get current date paymentDetals
		$dailyPaymentDetails = $this->getModel('PaymentsTable')->getPaymentValueByGivenDateRange($currentDate, null, 'daily')->current();
		// get monthly payments
		$monthlyPaymentDetails = $this->getModel('PaymentsTable')->getPaymentValueByGivenDateRange($currentDate, $monthFirstDate, 'monthly')->current();

		// get annual payments
		$annualPaymentDetails = $this->getModel('PaymentsTable')->getPaymentValueByGivenDateRange($currentDate, $yearFirstDate, 'annual')->current();

		// set daily details to the donut
		if ($dailyInvoiceDetails['TotalSales'] == 0 && $dailyPaymentDetails['totalPayments'] == 0) {
			$dailyDetails = [1];
			$dailyDetailColour = ['#FFEB3B'];
		} else if ($dailyInvoiceDetails['TotalSales'] != 0 && $dailyPaymentDetails['totalPayments'] == 0) {
			$dailyDetails = [$dailyInvoiceDetails['TotalSales']];
			$dailyDetailColour = ['#24d9e2'];
		} else if ($dailyInvoiceDetails['TotalSales'] == 0 && $dailyPaymentDetails['totalPayments'] != 0) {
			$dailyDetails = [$dailyPaymentDetails['totalPayments']];
			$dailyDetailColour = ['#7c41f4'];
		} else if (floatval($dailyInvoiceDetails['TotalSales']) - floatval($dailyPaymentDetails['totalPayments']) > 0 ) {
			$dailyDetails = [floatval($dailyInvoiceDetails['TotalSales']) - floatval($dailyPaymentDetails['totalPayments']), floatval($dailyPaymentDetails['totalPayments'])];
			$dailyDetailColour = ['#24d9e2', '#7c41f4'];
		} else  {
			$dailyDetails = [$dailyPaymentDetails['totalPayments']];
			$dailyDetailColour = ['#7c41f4'];
		}
		
		// set monthly data to the donut
		if ($monthlyInvoiceDetails['TotalSales'] == 0 && $monthlyPaymentDetails['totalPayments'] == 0) {
			$monthlyDetails = [1];
			$monthlyDetailColour = ['#FFEB3B'];
		} else if ($monthlyInvoiceDetails['TotalSales'] != 0 && $monthlyPaymentDetails['totalPayments'] == 0) {
			$monthlyDetails = [$monthlyInvoiceDetails['TotalSales']];
			$monthlyDetailColour = ['#24d9e2'];
		} else if ($monthlyInvoiceDetails['TotalSales'] == 0 && $monthlyPaymentDetails['totalPayments'] != 0) {
			$monthlyDetails = [$monthlyPaymentDetails['totalPayments']];
			$monthlyDetailColour = ['#7c41f4'];
		} else if (floatval($monthlyInvoiceDetails['TotalSales']) - floatval($monthlyPaymentDetails['totalPayments']) > 0 ) {
			$monthlyDetails = [floatval($monthlyInvoiceDetails['TotalSales']) - floatval($monthlyPaymentDetails['totalPayments']), floatval($monthlyPaymentDetails['totalPayments'])];
			$monthlyDetailColour = ['#24d9e2', '#7c41f4'];
		} else  {
			$monthlyDetails = [$monthlyPaymentDetails['totalPayments']];
			$monthlyDetailColour = ['#7c41f4'];
		}

		// set annual data to the donut
		if ($annualInvoiceDetails['TotalSales'] == 0 && $annualPaymentDetails['totalPayments'] == 0) {
			$annualDetails = [1];
			$annualDetailColour = ['#FFEB3B'];
		} else if ($annualInvoiceDetails['TotalSales'] != 0 && $annualPaymentDetails['totalPayments'] == 0) {
			$annualDetails = [$annualInvoiceDetails['TotalSales']];
			$annualDetailColour = ['#24d9e2'];
		} else if ($annualInvoiceDetails['TotalSales'] == 0 && $annualPaymentDetails['totalPayments'] != 0) {
			$annualDetails = [$annualPaymentDetails['totalPayments']];
			$annualDetailColour = ['#7c41f4'];
		} else if (floatval($annualInvoiceDetails['TotalSales']) - floatval($annualPaymentDetails['totalPayments']) > 0 ) {
			$annualDetails = [floatval($annualInvoiceDetails['TotalSales']) - floatval($annualPaymentDetails['totalPayments']), floatval($annualPaymentDetails['totalPayments'])];
			$annualDetailColour = ['#24d9e2', '#7c41f4'];
		} else  {
			$annualDetails = [$annualPaymentDetails['totalPayments']];
			$annualDetailColour = ['#7c41f4'];
		}




		return [
			'daily' => [
				'invTotal' => number_format(floatval($dailyInvoiceDetails['TotalSales']),2),
				'payTotal' => number_format(floatval($dailyPaymentDetails['totalPayments']),2),
				'remPayment' => number_format(floatval($dailyInvoiceDetails['TotalSales']) - floatval($dailyPaymentDetails['totalPayments']), 2) 
			],
			'monthly' => [
				'invTotal' => number_format(floatval($monthlyInvoiceDetails['TotalSales']), 2),
				'payTotal' => number_format(floatval($monthlyPaymentDetails['totalPayments']), 2),
				'remPayment' => number_format(floatval($monthlyInvoiceDetails['TotalSales']) - floatval($monthlyPaymentDetails['totalPayments']), 2)
			],
			'annual' => [
				'invTotal' => number_format(floatval($annualInvoiceDetails['TotalSales']), 2),
				'payTotal' => number_format(floatval($annualPaymentDetails['totalPayments']), 2),
				'remPayment' => number_format(floatval($annualInvoiceDetails['TotalSales']) - floatval($annualPaymentDetails['totalPayments']), 2)
			],
			'dailyDetail' => $dailyDetails,
			'dailyColour' => $dailyDetailColour,
			'monthlyDetail' => $monthlyDetails,
			'monthlyColour' => $monthlyDetailColour,
			'annualDetail' => $annualDetails,
			'annualColour' => $annualDetailColour
		];
		
	}

	public function getInvoicePaymentDetailsForDonut($data) 
	{
		$currentDate = date('Y-m-d');
		$monthFirstDate = date('Y-m-01');
		$lastMonthLastDate = date('Y-m-d', strtotime('-1 days', strtotime($monthFirstDate)));
		$yearFirstDate = date('Y-01-01');
		$lastYearLastDate = date('Y-m-d', strtotime('-1 days', strtotime($yearFirstDate)));
		$tommorowDate = date('Y-m-d', strtotime('+1 days', strtotime($currentDate)));
		
		$dailyPayments = [
			'cash' => 0,
			'cheque' => 0,
			'credit_card' => 0,
			'other' => 0,
			'credit' => 0,
			'total' => 0
		];
		$monthlyPayments = [
			'cash' => 0,
			'cheque' => 0,
			'credit_card' => 0,
			'other' => 0,
			'credit' => 0,
			'total' => 0
		];
		$annualPayments = [
			'cash' => 0,
			'cheque' => 0,
			'credit_card' => 0,
			'other' => 0,
			'credit' => 0,
			'total' => 0
		];
		
		// get all payment details
		$allPaymentValues = $this->getModel('PaymentsTable')->getPaymentDetailsForDonut($currentDate, $yearFirstDate);

		foreach ($allPaymentValues as $key => $value) {
			// get current date details
			if ($value['incomingPaymentDate'] == $currentDate) {
				if ($value['incomingPaymentMethodCashId'] != 0 && $value['incomingPaymentMethodCashId'] != null) {
					$dailyPayments['cash'] += $value['incomingPaymentMethodAmount'];
				} else if ($value['incomingPaymentMethodChequeId'] != 0 && $value['incomingPaymentMethodChequeId'] != null) {
					$dailyPayments['cheque'] += $value['incomingPaymentMethodAmount'];
				} else if ($value['incomingPaymentMethodCreditCardId'] != 0 && $value['incomingPaymentMethodCreditCardId'] != null) {
					$dailyPayments['credit_card'] += $value['incomingPaymentMethodAmount'];
				} else {
					$dailyPayments['other'] += $value['incomingPaymentMethodAmount'];
				}

				$dailyPayments['credit'] += $value['incomingPaymentCreditAmount']; 
				$dailyPayments['total'] += $value['incomingPaymentMethodAmount'];
				$dailyPayments['total'] += $value['incomingPaymentCreditAmount'];


			}

			if ($lastMonthLastDate < $value['incomingPaymentDate']  && $value['incomingPaymentDate'] < $tommorowDate) {
				if ($value['incomingPaymentMethodCashId'] != 0 && $value['incomingPaymentMethodCashId'] != null) {
					$monthlyPayments['cash'] += $value['incomingPaymentMethodAmount'];
				} else if ($value['incomingPaymentMethodChequeId'] != 0 && $value['incomingPaymentMethodChequeId'] != null) {
					$monthlyPayments['cheque'] += $value['incomingPaymentMethodAmount'];
				} else if ($value['incomingPaymentMethodCreditCardId'] != 0 && $value['incomingPaymentMethodCreditCardId'] != null) {
					$monthlyPayments['credit_card'] += $value['incomingPaymentMethodAmount'];
				} else {
					$monthlyPayments['other'] += $value['incomingPaymentMethodAmount'];
				}

				$monthlyPayments['credit'] += $value['incomingPaymentCreditAmount']; 
				$monthlyPayments['total'] += $value['incomingPaymentMethodAmount'];
				$monthlyPayments['total'] += $value['incomingPaymentCreditAmount'];
				
			}

			if ($lastYearLastDate < $value['incomingPaymentDate']  && $value['incomingPaymentDate'] < $tommorowDate) {
				if ($value['incomingPaymentMethodCashId'] != 0 && $value['incomingPaymentMethodCashId'] != null) {
					$annualPayments['cash'] += $value['incomingPaymentMethodAmount'];
				} else if ($value['incomingPaymentMethodChequeId'] != 0 && $value['incomingPaymentMethodChequeId'] != null) {
					$annualPayments['cheque'] += $value['incomingPaymentMethodAmount'];
				} else if ($value['incomingPaymentMethodCreditCardId'] != 0 && $value['incomingPaymentMethodCreditCardId'] != null) {
					$annualPayments['credit_card'] += $value['incomingPaymentMethodAmount'];
				} else {
					$annualPayments['other'] += $value['incomingPaymentMethodAmount'];
				}

				$annualPayments['credit'] += $value['incomingPaymentCreditAmount']; 
				$annualPayments['total'] += $value['incomingPaymentMethodAmount'];
				$annualPayments['total'] += $value['incomingPaymentCreditAmount'];
			}
		}
		$dailyDonut = [];
		$dailyDonutColour = [];
		
		foreach ($dailyPayments as $key => $value) {
			if ($key == 'cash' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#39ddaa');
			} else if ($key == 'cheque' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#3262f2');
			} else if ($key == 'credit_card' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#da95ed');
			} else if ($key == 'credit' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#f2bd37');
			} else if ($key == 'other' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#e0735e');
			}

			$dailyPayments[$key] = number_format($value, 2);
		}
		if (sizeof($dailyDonut) == 0){
			array_push($dailyDonut, 1);
			array_push($dailyDonutColour,'#FFEB3B');


		}
		
		$monthlyDonut = [];
		$monthlyDonutColour = [];
		
		foreach ($monthlyPayments as $key => $value) {
			if ($key == 'cash' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#39ddaa');
			} else if ($key == 'cheque' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#3262f2');
			} else if ($key == 'credit_card' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#da95ed');
			} else if ($key == 'credit' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#f2bd37');
			} else if ($key == 'other' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#e0735e');
			}
			$monthlyPayments[$key] = number_format($value, 2);
		}
		if (sizeof($monthlyDonut) == 0){
			array_push($monthlyDonut, 1);
			array_push($monthlyDonutColour,'#FFEB3B');

		}

		$annualDonut = [];
		$annualDonutColour = [];
		foreach ($annualPayments as $key => $value) {
			if ($key == 'cash' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#39ddaa');
			} else if ($key == 'cheque' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#3262f2');
			} else if ($key == 'credit_card' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#da95ed');
			} else if ($key == 'credit' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#f2bd37');
			} else if ($key == 'other' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#e0735e');
			
			}
			$annualPayments[$key] = number_format($value, 2);
		}
		if (sizeof($annualDonut) == 0){
			array_push($annualDonut, 1);
			array_push($annualDonutColour,'#FFEB3B');

		}
			
		return [
			'daily' => $dailyPayments,
			'monthly' => $monthlyPayments,
			'annual' => $annualPayments,
			'dailyDonut' => $dailyDonut,
			'dailyDonutColour' => $dailyDonutColour,
			'monthlyDonut' => $monthlyDonut,
			'monthlyDonutColour' => $monthlyDonutColour,
			'annualDonut' => $annualDonut,
			'annualDonutColour' => $annualDonutColour
		];
	}

	public function getPurchaseInvoiceAndPurchasePaymentsForDonut($data)
	{
		$currentDate = date('Y-m-d');
		$monthFirstDate = date('Y-m-01');
		$yearFirstDate = date('Y-01-01');
		
		// get purchase daily details
		$dailyPurchaseInvoiceDetails = $this->getModel('PurchaseInvoiceTable')->getInvoiceTotalByTimePerid($currentDate,null, 'daily')->current();
		
		// get monthly sales details
		$monthlyPurchaseInvoiceDetails = $this->getModel('PurchaseInvoiceTable')->getInvoiceTotalByTimePerid($currentDate, $monthFirstDate, 'monthly')->current();

		// get annual sales details
		$annualPurchaseInvoiceDetails = $this->getModel('PurchaseInvoiceTable')->getInvoiceTotalByTimePerid($currentDate, $yearFirstDate, 'annual')->current();

		// get current date paymentDetals
		$dailyPaymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentValueByGivenDateRange($currentDate, null, 'daily')->current();
				
		// get monthly payments
		$monthlyPaymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentValueByGivenDateRange($currentDate, $monthFirstDate, 'monthly')->current();

		// get annual payments
		$annualPaymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentValueByGivenDateRange($currentDate, $yearFirstDate, 'annual')->current();
		
		// set daily details to the donut
		if ($dailyPurchaseInvoiceDetails['TotalSales'] == 0 && $dailyPaymentDetails['totalMoneyPayments'] + $dailyPaymentDetails['totalCreditPayments'] == 0) {
			$dailyDetails = [1];
			$dailyDetailColour = ['#FFEB3B'];
		} else if ($dailyPurchaseInvoiceDetails['TotalSales'] != 0 && $dailyPaymentDetails['totalMoneyPayments'] + $dailyPaymentDetails['totalCreditPayments'] == 0) {
			$dailyDetails = [$dailyPurchaseInvoiceDetails['TotalSales']];
			$dailyDetailColour = ['#24d9e2'];
			
		} else if ($dailyPurchaseInvoiceDetails['TotalSales'] == 0 && $dailyPaymentDetails['totalMoneyPayments'] + $dailyPaymentDetails['totalCreditPayments'] != 0) {
			$dailyDetails = [$dailyPaymentDetails['totalMoneyPayments'] + $dailyPaymentDetails['totalCreditPayments']];
			$dailyDetailColour = ['#7c41f4'];
		} else if (floatval($dailyPurchaseInvoiceDetails['TotalSales']) - floatval($dailyPaymentDetails['totalMoneyPayments'] + $dailyPaymentDetails['totalCreditPayments']) > 0 ) {
			$dailyDetails = [floatval($dailyPurchaseInvoiceDetails['TotalSales']) - floatval($dailyPaymentDetails['totalMoneyPayments'] + $dailyPaymentDetails['totalCreditPayments']), floatval($dailyPaymentDetails['totalMoneyPayments'] + $dailyPaymentDetails['totalCreditPayments'])];
			$dailyDetailColour = ['#24d9e2', '#7c41f4'];
		} else  {
			$dailyDetails = [$dailyPaymentDetails['totalMoneyPayments'] + $dailyPaymentDetails['totalCreditPayments']];
			$dailyDetailColour = ['#7c41f4'];
		}
		
		// set monthly data to the donut
		if ($monthlyPurchaseInvoiceDetails['TotalSales'] == 0 && $monthlyPaymentDetails['totalMoneyPayments'] + $monthlyPaymentDetails['totalCreditPayments'] == 0) {
			$monthlyDetails = [1];
			$monthlyDetailColour = ['#FFEB3B'];
		} else if ($monthlyPurchaseInvoiceDetails['TotalSales'] != 0 && $monthlyPaymentDetails['totalMoneyPayments'] + $monthlyPaymentDetails['totalCreditPayments'] == 0) {
			$monthlyDetails = [$monthlyPurchaseInvoiceDetails['TotalSales']];
			$monthlyDetailColour = ['#24d9e2'];
		} else if ($monthlyPurchaseInvoiceDetails['TotalSales'] == 0 && $monthlyPaymentDetails['totalMoneyPayments'] + $monthlyPaymentDetails['totalCreditPayments'] != 0) {
			$monthlyDetails = [$monthlyPaymentDetails['totalMoneyPayments'] + $monthlyPaymentDetails['totalCreditPayments']];
			$monthlyDetailColour = ['#7c41f4'];
		} else if (floatval($monthlyPurchaseInvoiceDetails['TotalSales']) - floatval($monthlyPaymentDetails['totalMoneyPayments'] + $monthlyPaymentDetails['totalCreditPayments']) > 0 ) {
			$monthlyDetails = [floatval($monthlyPurchaseInvoiceDetails['TotalSales']) - floatval($monthlyPaymentDetails['totalMoneyPayments'] + $monthlyPaymentDetails['totalCreditPayments']), floatval($monthlyPaymentDetails['totalMoneyPayments'] + $monthlyPaymentDetails['totalCreditPayments'])];
			$monthlyDetailColour = ['#24d9e2', '#7c41f4'];
		} else  {
			$monthlyDetails = [$monthlyPaymentDetails['totalMoneyPayments'] + $monthlyPaymentDetails['totalCreditPayments']];
			$monthlyDetailColour = ['#7c41f4'];
		}

		// set annual data to the donut
		if ($annualPurchaseInvoiceDetails['TotalSales'] == 0 && $annualPaymentDetails['totalMoneyPayments'] + $annualPaymentDetails['totalCreditPayments'] == 0) {
			$annualDetails = [1];
			$annualDetailColour = ['#FFEB3B'];
		} else if ($annualPurchaseInvoiceDetails['TotalSales'] != 0 && $annualPaymentDetails['totalMoneyPayments'] + $annualPaymentDetails['totalCreditPayments'] == 0) {
			$annualDetails = [$annualPurchaseInvoiceDetails['TotalSales']];
			$annualDetailColour = ['#24d9e2'];
		} else if ($annualPurchaseInvoiceDetails['TotalSales'] == 0 && $annualPaymentDetails['totalMoneyPayments'] + $annualPaymentDetails['totalCreditPayments'] != 0) {
			$annualDetails = [$annualPaymentDetails['totalMoneyPayments'] + $annualPaymentDetails['totalCreditPayments']];
			$annualDetailColour = ['#7c41f4'];
		} else if (floatval($annualPurchaseInvoiceDetails['TotalSales']) - floatval($annualPaymentDetails['totalMoneyPayments'] + $annualPaymentDetails['totalCreditPayments']) > 0 ) {
			$annualDetails = [floatval($annualPurchaseInvoiceDetails['TotalSales']) - floatval($annualPaymentDetails['totalMoneyPayments'] + $annualPaymentDetails['totalCreditPayments']), floatval($annualPaymentDetails['totalMoneyPayments'] + $annualPaymentDetails['totalCreditPayments'])];
			$annualDetailColour = ['#24d9e2', '#7c41f4'];
		} else  {
			$annualDetails = [$annualPaymentDetails['totalMoneyPayments'] + $annualPaymentDetails['totalCreditPayments']];
			$annualDetailColour = ['#7c41f4'];
		}


		return [
			'daily' => [
				'invTotal' => number_format(floatval($dailyPurchaseInvoiceDetails['TotalSales']), 2),
				'payTotal' => number_format((floatval($dailyPurchaseInvoiceDetails['TotalSales']) - (floatval($dailyPaymentDetails['totalMoneyPayments']) + floatval($dailyPaymentDetails['totalCreditPayments'])) < 0 ) ? (floatval($dailyPurchaseInvoiceDetails['TotalSales'])) : floatval($dailyPaymentDetails['totalMoneyPayments']) + floatval($dailyPaymentDetails['totalCreditPayments']), 2),
				'remPayment' => number_format((floatval($dailyPurchaseInvoiceDetails['TotalSales']) - (floatval($dailyPaymentDetails['totalMoneyPayments']) + floatval($dailyPaymentDetails['totalCreditPayments'])) > 0 ) ? floatval($dailyPurchaseInvoiceDetails['TotalSales']) - (floatval($dailyPaymentDetails['totalMoneyPayments']) + floatval($dailyPaymentDetails['totalCreditPayments'])) : 0, 2)
			],
			'monthly' => [
				'invTotal' => number_format(floatval($monthlyPurchaseInvoiceDetails['TotalSales']), 2),
				'payTotal' => number_format((floatval($monthlyPurchaseInvoiceDetails['TotalSales']) - (floatval($monthlyPaymentDetails['totalMoneyPayments']) + floatval($monthlyPaymentDetails['totalCreditPayments'])) < 0 ) ? floatval($monthlyPurchaseInvoiceDetails['TotalSales']) : floatval($monthlyPaymentDetails['totalMoneyPayments']) + floatval($monthlyPaymentDetails['totalCreditPayments']), 2),
				'remPayment' => number_format((floatval($monthlyPurchaseInvoiceDetails['TotalSales']) - (floatval($monthlyPaymentDetails['totalMoneyPayments']) + floatval($monthlyPaymentDetails['totalCreditPayments'])) > 0) ? floatval($monthlyPurchaseInvoiceDetails['TotalSales']) - (floatval($monthlyPaymentDetails['totalMoneyPayments']) + floatval($monthlyPaymentDetails['totalCreditPayments'])) : 0, 2)
			],
			'annual' => [
				'invTotal' => number_format(floatval($annualPurchaseInvoiceDetails['TotalSales']), 2),
				'payTotal' => number_format((floatval($annualPurchaseInvoiceDetails['TotalSales']) - (floatval($annualPaymentDetails['totalMoneyPayments']) + floatval($annualPaymentDetails['totalCreditPayments'])) < 0) ? floatval($annualPurchaseInvoiceDetails['TotalSales']) : (floatval($annualPaymentDetails['totalMoneyPayments']) + floatval($annualPaymentDetails['totalCreditPayments'])), 2),
				'remPayment' => number_format((floatval($annualPurchaseInvoiceDetails['TotalSales']) - (floatval($annualPaymentDetails['totalMoneyPayments']) + floatval($annualPaymentDetails['totalCreditPayments'])) > 0 ) ? floatval($annualPurchaseInvoiceDetails['TotalSales']) - (floatval($annualPaymentDetails['totalMoneyPayments']) + floatval($annualPaymentDetails['totalCreditPayments'])) : 0, 2) 
			],
			'dailyDetail' => $dailyDetails,
			'dailyColour' => $dailyDetailColour,
			'monthlyDetail' => $monthlyDetails,
			'monthlyColour' => $monthlyDetailColour,
			'annualDetail' => $annualDetails,
			'annualColour' => $annualDetailColour
		];
	}

	public function getPurchaseInvoicePaymentDetailsForDonut($data)
	{
		$currentDate = date('Y-m-d');
		$monthFirstDate = date('Y-m-01');
		$lastMonthLastDate = date('Y-m-d', strtotime('-1 days', strtotime($monthFirstDate)));
		$yearFirstDate = date('Y-01-01');
		$lastYearLastDate = date('Y-m-d', strtotime('-1 days', strtotime($yearFirstDate)));
		$tommorowDate = date('Y-m-d', strtotime('+1 days', strtotime($currentDate)));
		
		$dailyPayments = [
			'cash' => 0,
			'cheque' => 0,
			'credit_card' => 0,
			'other' => 0,
			'credit' => 0,
			'total' => 0
		];
		$monthlyPayments = [
			'cash' => 0,
			'cheque' => 0,
			'credit_card' => 0,
			'other' => 0,
			'credit' => 0,
			'total' => 0
		];
		$annualPayments = [
			'cash' => 0,
			'cheque' => 0,
			'credit_card' => 0,
			'other' => 0,
			'credit' => 0,
			'total' => 0
		];
		
		// get all payment details
		$allPaymentValues = $this->getModel('SupplierPaymentsTable')->getPaymentDetailsForDonut($currentDate, $yearFirstDate);
		foreach ($allPaymentValues as $key => $value) {
			
			// get current date details
			if ($value['outgoingPaymentDate'] == $currentDate) {
				if ($value['outGoingPaymentMethodID'] == 1) {
					$dailyPayments['cash'] += $value['outgoingInvoiceCashAmount'];
				} else if ($value['outGoingPaymentMethodID'] == 2) {
					$dailyPayments['cheque'] += $value['outgoingInvoiceCashAmount'];
				} else if ($value['outGoingPaymentMethodID'] == 3) {
					$dailyPayments['credit_card'] += $value['outgoingInvoiceCashAmount'];
				} else {
					$dailyPayments['other'] += $value['outgoingInvoiceCashAmount'];
				}

				$dailyPayments['credit'] += $value['outgoingInvoiceCreditAmount']; 
				$dailyPayments['total'] += $value['outgoingInvoiceCashAmount'];
				$dailyPayments['total'] += $value['outgoingInvoiceCreditAmount'];


			}

			if ($lastMonthLastDate < $value['outgoingPaymentDate']  && $value['outgoingPaymentDate'] < $tommorowDate) {
				if ($value['outGoingPaymentMethodID'] == 1) {
					$monthlyPayments['cash'] += $value['outgoingInvoiceCashAmount'];
				} else if ($value['outGoingPaymentMethodID'] == 2) {
					$monthlyPayments['cheque'] += $value['outgoingInvoiceCashAmount'];
				} else if ($value['outGoingPaymentMethodID'] == 3) {
					$monthlyPayments['credit_card'] += $value['outgoingInvoiceCashAmount'];
				} else {
					$monthlyPayments['other'] += $value['outgoingInvoiceCashAmount'];
				}

				$monthlyPayments['credit'] += $value['outgoingInvoiceCreditAmount']; 
				$monthlyPayments['total'] += $value['outgoingInvoiceCashAmount'];
				$monthlyPayments['total'] += $value['outgoingInvoiceCreditAmount'];
				
			}

			if ($lastYearLastDate < $value['outgoingPaymentDate']  && $value['outgoingPaymentDate'] < $tommorowDate) {
				if ($value['outGoingPaymentMethodID'] == 1) {
					$annualPayments['cash'] += $value['outgoingInvoiceCashAmount'];
				} else if ($value['outGoingPaymentMethodID'] == 2) {
					$annualPayments['cheque'] += $value['outgoingInvoiceCashAmount'];
				} else if ($value['outGoingPaymentMethodID'] == 3) {
					$annualPayments['credit_card'] += $value['outgoingInvoiceCashAmount'];
				} else {
					$annualPayments['other'] += $value['outgoingInvoiceCashAmount'];
				}

				$annualPayments['credit'] += $value['outgoingInvoiceCreditAmount']; 
				$annualPayments['total'] += $value['outgoingInvoiceCashAmount'];
				$annualPayments['total'] += $value['outgoingInvoiceCreditAmount'];
			}
		}

		$dailyDonut = [];
		$dailyDonutColour = [];
		
		foreach ($dailyPayments as $key => $value) {
			if ($key == 'cash' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#39ddaa');
			} else if ($key == 'cheque' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#3262f2');
			} else if ($key == 'credit_card' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#da95ed');
			} else if ($key == 'credit' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#f2bd37');
			} else if ($key == 'other' && $value != 0) {
				array_push($dailyDonut, $value);
				array_push($dailyDonutColour,'#e0735e');
			
			}
			$dailyPayments[$key] = number_format($value, 2);
		}
		if (sizeof($dailyDonut) == 0){
			array_push($dailyDonut, 1);
			array_push($dailyDonutColour,'#FFEB3B');

		}

		$monthlyDonut = [];
		$monthlyDonutColour = [];
		
		foreach ($monthlyPayments as $key => $value) {
			if ($key == 'cash' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#39ddaa');
			} else if ($key == 'cheque' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#3262f2');
			} else if ($key == 'credit_card' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#da95ed');
			} else if ($key == 'credit' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#f2bd37');
			} else if ($key == 'other' && $value != 0) {
				array_push($monthlyDonut, $value);
				array_push($monthlyDonutColour,'#e0735e');
			}
			$monthlyPayments[$key] = number_format($value, 2);
		}
		if (sizeof($monthlyDonut) == 0){
			array_push($monthlyDonut, 1);
			array_push($monthlyDonutColour,'#FFEB3B');

		}

		$annualDonut = [];
		$annualDonutColour = [];
		foreach ($annualPayments as $key => $value) {
			if ($key == 'cash' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#39ddaa');
			} else if ($key == 'cheque' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#3262f2');
			} else if ($key == 'credit_card' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#da95ed');
			} else if ($key == 'credit' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#f2bd37');
			} else if ($key == 'other' && $value != 0) {
				array_push($annualDonut, $value);
				array_push($annualDonutColour,'#e0735e');
			
			}
			$annualPayments[$key] = number_format($value, 2);
		}
		if (sizeof($annualDonut) == 0){
			array_push($annualDonut, 1);
			array_push($annualDonutColour,'#FFEB3B');

		}

		return [
			'daily' => $dailyPayments,
			'monthly' => $monthlyPayments,
			'annual' => $annualPayments,
			'dailyDonut' => $dailyDonut,
			'dailyDonutColour' => $dailyDonutColour,
			'monthlyDonut' => $monthlyDonut,
			'monthlyDonutColour' => $monthlyDonutColour,
			'annualDonut' => $annualDonut,
			'annualDonutColour' => $annualDonutColour

		];
	}

	public function getDataForSalesDashBoard($data)
	{
		$currentDate = date('Y-m-d');
		
		// get daily invoice 
		$dailyInvoiceDetails = $this->getModel('InvoiceTable')->getInvoiceTotalByTimePerid($currentDate,null, 'daily')->current();
		$dailySalesTotal = ($dailyInvoiceDetails['TotalSales'] == '') ? 0 : $dailyInvoiceDetails['TotalSales']; 

		// get current date paymentDetals
		$dailyPaymentDetails = $this->getModel('PaymentsTable')->getPaymentValueByGivenDateRange($currentDate, null, 'daily')->current();
		$totalPayments = ($dailyPaymentDetails['totalPayments'] == '') ? 0 : $dailyPaymentDetails['totalPayments'];

		// get current date paymentDetails with all payment types
		$allPaymentValues = $this->getModel('PaymentsTable')->getPaymentDetailsForDonut($currentDate);

		$dailyPayments = [
			'cash' => 0,
			'cheque' => 0,
			'credit_card' => 0,
			'other' => 0,
			'credit' => 0
		];
		$totalPayment = 0;

		foreach ($allPaymentValues as $key => $value) {
			
			if ($value['incomingPaymentDate'] == $currentDate) {
				if ($value['incomingPaymentMethodCashId'] != 0 && $value['incomingPaymentMethodCashId'] != null) {
					$dailyPayments['cash'] += $value['incomingPaymentMethodAmount'];
				} else if ($value['incomingPaymentMethodChequeId'] != 0 && $value['incomingPaymentMethodChequeId'] != null) {
					$dailyPayments['cheque'] += $value['incomingPaymentMethodAmount'];
				} else if ($value['incomingPaymentMethodCreditCardId'] != 0 && $value['incomingPaymentMethodCreditCardId'] != null) {
					$dailyPayments['credit_card'] += $value['incomingPaymentMethodAmount'];
				} else {
					$dailyPayments['other'] += $value['incomingPaymentMethodAmount'];
				}

				$dailyPayments['credit'] += $value['incomingPaymentCreditAmount']; 
				$totalPayment += $value['incomingPaymentMethodAmount'];
				$totalPayment += $value['incomingPaymentCreditAmount'];
			}
		}
		$paymentSet = [];
		$paymentColur = [];
		
		foreach ($dailyPayments as $key => $value) {
			if ($key == 'cash' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#39ddaa');
			} else if ($key == 'cheque' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#3262f2');
			} else if ($key == 'credit_card' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#da95ed');
			} else if ($key == 'credit' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#f2bd37');
			} else if ($key == 'other' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#e0735e');
			
			}
		}
		if (sizeof($paymentSet) == 0){
			array_push($paymentSet, 1);
			array_push($paymentColur,'#FFEB3B');
		}

		// get daily invoice count
		$invoiceCount = $this->getModel('InvoiceTable')->getDailyInvoiceCount($currentDate)->current();
		$invCount = ($invoiceCount['TotalInvoiceCount'] == '') ? 0 : $invoiceCount['TotalInvoiceCount'];

		// get daily payment count
		$paymentCount = $this->getModel('PaymentsTable')->getPaymentCountByGivenDate($currentDate)->current();

		if ($dailySalesTotal == 0) {
			// thats mean no any invoices
			$salesValues = [1];
			$salesColors = ['#FFEB3B'];
		} else if ($totalPayments > 0 && $dailySalesTotal - $totalPayments <= 0) {
			$salesValues = [$dailySalesTotal];
			$salesColors = ['#7c41f4'];

		} else if ($totalPayments > 0 && $dailySalesTotal - $totalPayments > 0) {
			$salesValues = [floatval($totalPayments), $dailySalesTotal - $totalPayments];
			$salesColors = ['#7c41f4', '#24d9e2'];

		} else {
			$salesValues = [$dailySalesTotal];
			$salesColors = ['#24d9e2'];

		}
		return [
			'salesValue' => $salesValues,
			'salesColour' => $salesColors,
			'totalPayment' => $paymentSet,
			'paymentColour' => $paymentColur,
			'invCount' => $invCount,
			'payCount' => $paymentCount['totalPaymentCount'],
		];
	}

	public function getDataForPurchaseDashBoard()
	{
		$currentDate = date('Y-m-d');
		// get purchase daily details
		$dailyPurchaseInvoiceDetails = $this->getModel('PurchaseInvoiceTable')->getInvoiceTotalByTimePerid($currentDate,null, 'daily')->current();
		$dailyPurchaseTotal = ($dailyPurchaseInvoiceDetails['TotalSales'] == '') ? 0 : $dailyPurchaseInvoiceDetails['TotalSales']; 

		// get current date paymentDetals
		$dailyPaymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentValueByGivenDateRange($currentDate, null, 'daily')->current();
		$monyTotal = ($dailyPaymentDetails['totalMoneyPayments'] == '') ? 0 : $dailyPaymentDetails['totalMoneyPayments'];
		$crdtTotal =  ($dailyPaymentDetails['totalCreditPayments'] == '') ? 0 : $dailyPaymentDetails['totalCreditPayments'];
		$totalPayments = $monyTotal + $crdtTotal; 

		// get current date paymentDetails with all payment types
		$allPaymentValues = $this->getModel('SupplierPaymentsTable')->getPaymentDetailsForDonut($currentDate, $yearFirstDate);

		$dailyPayments = [
			'cash' => 0,
			'cheque' => 0,
			'credit_card' => 0,
			'other' => 0,
			'credit' => 0,
			'total' => 0
		];

		foreach ($allPaymentValues as $key => $value) {
			
			// get current date details
			if ($value['outgoingPaymentDate'] == $currentDate) {
				if ($value['outGoingPaymentMethodID'] == 1) {
					$dailyPayments['cash'] += $value['outgoingInvoiceCashAmount'];
				} else if ($value['outGoingPaymentMethodID'] == 2) {
					$dailyPayments['cheque'] += $value['outgoingInvoiceCashAmount'];
				} else if ($value['outGoingPaymentMethodID'] == 3) {
					$dailyPayments['credit_card'] += $value['outgoingInvoiceCashAmount'];
				} else {
					$dailyPayments['other'] += $value['outgoingInvoiceCashAmount'];
				}

				$dailyPayments['credit'] += $value['outgoingInvoiceCreditAmount']; 
				$dailyPayments['total'] += $value['outgoingInvoiceCashAmount'];
				$dailyPayments['total'] += $value['outgoingInvoiceCreditAmount'];


			}
		}

		$paymentSet = [];
		$paymentColur = [];
		
		foreach ($dailyPayments as $key => $value) {
			if ($key == 'cash' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#39ddaa');
			} else if ($key == 'cheque' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#3262f2');
			} else if ($key == 'credit_card' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#da95ed');
			} else if ($key == 'credit' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#f2bd37');
			} else if ($key == 'other' && $value != 0) {
				array_push($paymentSet, $value);
				array_push($paymentColur,'#e0735e');
			
			}
		}
		if (sizeof($paymentSet) == 0){
			array_push($paymentSet, 1);
			array_push($paymentColur,'#FFEB3B');
		}

		// get daily invoice count
		$invoiceCount = $this->getModel('PurchaseInvoiceTable')->getDailyInvoiceCount($currentDate)->current();
		$invCount = ($invoiceCount['TotalSalesCount'] == '') ? 0 : $invoiceCount['TotalSalesCount'];

		// get daily payment count
		$paymentCount = $this->getModel('SupplierPaymentsTable')->getPaymentCountByGivenDate($currentDate)->current();

		
		if ($dailyPurchaseTotal == 0) {
			// thats mean no any invoices
			$salesValues = [1];
			$salesColors = ['#FFEB3B'];
		} else if ($totalPayments > 0 && $dailyPurchaseTotal - $totalPayments <= 0) {
			$salesValues = [$dailyPurchaseTotal];
			$salesColors = ['#7c41f4'];

		} else if ($totalPayments > 0 && $dailyPurchaseTotal - $totalPayments > 0) {
			$salesValues = [$totalPayments, $dailyPurchaseTotal - $totalPayments];
			$salesColors = ['#7c41f4', '#24d9e2'];

		} else {
			$salesValues = [$dailyPurchaseTotal];
			$salesColors = ['#24d9e2'];

		}
		

		return [
			'salesValue' => $salesValues,
			'salesColour' => $salesColors,
			'totalPayment' => $paymentSet,
			'paymentColour' => $paymentColur,
			'invCount' => $invCount,
			'payCount' => $paymentCount['totalPaymentCount'],

		];
		
	}

	public function getDailySalesInvoiceDetails()
	{
		$currentDate = date('Y-m-d');
		// get yesterday today and tommorow salesInvoice details
		$yesterday = date('Y-m-d', strtotime('-1 days', strtotime($currentDate)));
		$tommorow = date('Y-m-d', strtotime('+1 days', strtotime($currentDate)));
		$salesInvoiceDetails = $this->getModel('InvoiceTable')->getInvoiceTotalByDate($yesterday,$tommorow);
		$basicChartFig = [];
		$showCurrentDate = date('m-d');
		
		foreach ($salesInvoiceDetails as $key => $value) {
			if ($value['salesInvoiceIssuedDate'] == $yesterday) {
				$basicChartFig[] = [
					'day' => date('d/m', strtotime('-1 days', strtotime(date('Y-m-d')))),
					'value' => round($value['dailyInvTotal'], 2)
				]; 
			} else if ($value['salesInvoiceIssuedDate'] == $currentDate) {
				$basicChartFig[] = [
					'day' => date('d/m'),
					'value' => round($value['dailyInvTotal'], 2)
				];
			} else if ($value['salesInvoiceIssuedDate'] == $tommorow){
				$basicChartFig[] = [
					'day' => date('d/m', strtotime('+1 days', strtotime(date('Y-m-d')))),
					'value' => round($value['dailyInvTotal'], 2)
				]; 
			}
		}

		$dailyDetails = $this->calculateYaxisValue($basicChartFig);
		return [
			'value' => $dailyDetails['value'],
			'divider' => $dailyDetails['divider']
		];

	}

	public function getDailyPurchaseInvoiceDetails()
	{

		$currentDate = date('Y-m-d');
		// get yesterday today and tommorow salesInvoice details
		$yesterday = date('Y-m-d', strtotime('-1 days', strtotime($currentDate)));
		$tommorow = date('Y-m-d', strtotime('+1 days', strtotime($currentDate)));
		$purchaseInvoiceDetails = $this->getModel('PurchaseInvoiceTable')->getInvoiceTotalByDate($yesterday,$tommorow);
		$basicChartFig = [];
		
		foreach ($purchaseInvoiceDetails as $key => $value) {
			if ($value['purchaseInvoiceIssueDate'] == $yesterday) {
				$basicChartFig[] = [
					'day' => date('d/m', strtotime('-1 days', strtotime(date('Y-m-d')))),
					'value' => round($value['TotalSales'], 2)
				]; 
			} else if ($value['purchaseInvoiceIssueDate'] == $currentDate) {
				$basicChartFig[] = [
					'day' => date('d/m'),
					'value' => round($value['TotalSales'], 2)
				];
			} else if ($value['purchaseInvoiceIssueDate'] == $tommorow) {
				$basicChartFig[] = [
					'day' => date('d/m', strtotime('+1 days', strtotime(date('Y-m-d')))),
					'value' => round($value['TotalSales'], 2)
				]; 
			}
			
		}
		$dailyDetails = $this->calculateYaxisValue($basicChartFig);
		return [
			'value' => $dailyDetails['value'],
			'divider' => $dailyDetails['divider']
		];
	}

	public function calculateYaxisValue($dataArray)
	{
		$maxValue = max(array_column($dataArray, 'value'));
		$divider = 1;
		$dividerFormat = '';
		if ($maxValue > 1000000) {
			$divider = 1000000;
			$dividerFormat = '1000000';
		} else if ($maxValue > 100000) {
			$divider = 100000;
			$dividerFormat = '100000';
		} else if ($maxValue > 10000) {
			$divider = 10000;
			$dividerFormat = '10000';
		} 
		foreach ($dataArray as $key => $value) {
			$dataArray[$key]['value'] = round($dataArray[$key]['value']/$divider, 2);
		}

		return ['value' => $dataArray, 'divider' => $dividerFormat]; 
	}

	public function getDataForSalesInvoiceBarChart($data)
	{
		$currentDate = date('Y-m-d');
		$monthFirstDate = date('Y-m-01');
		$lastTen = date('Y-m-d', strtotime('-10 days', strtotime($currentDate)));
		$lastMonthLastDate = date('Y-m-d', strtotime('-1 days', strtotime($monthFirstDate)));
		$yearFirstDate = date('Y-01-01');
		$lastYearLastDate = date('Y-m-d', strtotime('-1 days', strtotime($yearFirstDate)));
		$tommorowDate = date('Y-m-d', strtotime('+1 days', strtotime($currentDate)));
		$fiveYearsBeforeDate = date('Y-m-d', strtotime('-1825 days', strtotime($yearFirstDate)));
		
		$daily = [];
		$monthly = [];
		$yearly = [];

		// get daily sales invoice 
		$salesInvoiceDailyDetails = $this->getModel('InvoiceTable')->getInvoiceTotalByDate($lastTen,$currentDate);
		foreach ($salesInvoiceDailyDetails as $key => $sIvalue) {
			$valueDate = explode("-", $sIvalue['salesInvoiceIssuedDate']);
			$tmp = [
				'date' => date('d', strtotime($sIvalue['salesInvoiceIssuedDate'])),
				'value' => round($sIvalue['dailyInvTotal'],2)
			];
			array_push($daily, $tmp);
			
		} 
		
		// get monthly details		
		$salesInvoiceDetails = $this->getModel('InvoiceTable')->getInvoiceTotalByDate($lastYearLastDate,$currentDate);
		foreach ($salesInvoiceDetails as $key => $value) {
			
			$valueMonth = explode("-", $value['salesInvoiceIssuedDate']);
			if ($monthly[$valueMonth[1]]) {
				$monthly[$valueMonth[1]] += floatval($value['dailyInvTotal']);
			} else {
				$monthly[$valueMonth[1]] = floatval($value['dailyInvTotal']);

			}	
		}

		// get last five years details
		$salesInvoiceAnnualDetails = $this->getModel('InvoiceTable')->getInvoiceTotalByDate($fiveYearsBeforeDate,$currentDate);
		foreach ($salesInvoiceAnnualDetails as $key => $annValue) {
			
			$valueYear = explode("-", $annValue['salesInvoiceIssuedDate']);
			if ($yearly[$valueYear[0]]) {
				$yearly[$valueYear[0]] += floatval($annValue['dailyInvTotal']);
			} else {
				$yearly[$valueYear[0]] = floatval($annValue['dailyInvTotal']);

			}	
		}
		$monthData = [];
		foreach ($monthly as $key => $value) {
			$tmp = [
				'date' => $key,
				'value' => round($value,2)
			];
			array_push($monthData, $tmp);
		}
		$yearData = [];
		foreach ($yearly as $key => $value) {
			$tmp = [
				'date' => substr($key, -2),
				'value' => round($value,2)
			];
			array_push($yearData, $tmp);
		}
		$dailyDetails = $this->calculateYaxisValue($daily);
		$monthlyDetails = $this->calculateYaxisValue($monthData);
		$annualDetails = $this->calculateYaxisValue($yearData);

		return [
			'daily' => $dailyDetails['value'],
			'monthly' => $monthlyDetails['value'],
			'yearly' => $annualDetails['value'],
			'dailyDivider' => $dailyDetails['divider'],
			'monthlyDivider' => $monthlyDetails['divider'],
			'yearlyDivider' => $annualDetails['divider'],
		];	
	}

	public function getDataForSalesPaymentBarChart()
	{
		$currentDate = date('Y-m-d');
		$monthFirstDate = date('Y-m-01');
		$lastMonthLastDate = date('Y-m-d', strtotime('-1 days', strtotime($monthFirstDate)));
		$lastTen = date('Y-m-d', strtotime('-10 days', strtotime($currentDate)));
		$yearFirstDate = date('Y-01-01');
		$lastYearLastDate = date('Y-m-d', strtotime('-1 days', strtotime($yearFirstDate)));
		$tommorowDate = date('Y-m-d', strtotime('+1 days', strtotime($currentDate)));
		$fiveYearsBeforeDate = date('Y-m-d', strtotime('-1825 days', strtotime($yearFirstDate)));
		
		$daily = [];
		$monthly = [];
		$yearly = [];

		// get daily payments 
		$salesPaymentDailyDetails = $this->getModel('PaymentsTable')->getDateWisePaymentDetails($lastTen, $currentDate);
		foreach ($salesPaymentDailyDetails as $key => $sPValue) {
			$valueDate = explode("-", $sPValue['incomingPaymentDate']);
			$tmp = [
				'date' => date('d', strtotime($sPValue['incomingPaymentDate'])),
				'value' => round($sPValue['totalPaymentCount'],2)
			];
			array_push($daily, $tmp);
		} 
		// get monthly details		
		$salesMonthlyPaymentDetails = $this->getModel('PaymentsTable')->getDateWisePaymentDetails($yearFirstDate,$currentDate);
		foreach ($salesMonthlyPaymentDetails as $key => $value) {
			
			$valueMonth = explode("-", $value['incomingPaymentDate']);
			if ($monthly[$valueMonth[1]]) {
				$monthly[$valueMonth[1]] += floatval($value['totalPaymentCount']);
			} else {
				$monthly[$valueMonth[1]] = floatval($value['totalPaymentCount']);

			}	
		}

		// get last five years details
		$salesAnnualPaymentDetails = $this->getModel('PaymentsTable')->getDateWisePaymentDetails($fiveYearsBeforeDate,$currentDate);
		foreach ($salesAnnualPaymentDetails as $key => $annValue) {
			$valueYear = explode("-", $annValue['incomingPaymentDate']);
			if ($yearly[$valueYear[0]]) {
				$yearly[$valueYear[0]] += floatval($annValue['totalPaymentCount']);
			} else {
				$yearly[$valueYear[0]] = floatval($annValue['totalPaymentCount']);

			}	
		}

		$monthData = [];
		foreach ($monthly as $key => $value) {
			$tmp = [
				'date' => $key,
				'value' => round($value,2)
			];
			array_push($monthData, $tmp);
		}
		$yearData = [];
		foreach ($yearly as $key => $value) {
			$tmp = [
				'date' => substr($key, -2),
				'value' => round($value,2)
			];
			array_push($yearData, $tmp);
		}

			
		$dailyDetails = $this->calculateYaxisValue($daily);
		$monthlyDetails = $this->calculateYaxisValue($monthData);
		$annualDetails = $this->calculateYaxisValue($yearData);

		return [
			'daily' => $dailyDetails['value'],
			'monthly' => $monthlyDetails['value'],
			'yearly' => $annualDetails['value'],
			'dailyDivider' => $dailyDetails['divider'],
			'monthlyDivider' => $monthlyDetails['divider'],
			'yearlyDivider' => $annualDetails['divider'],
		];		
	}

	public function getDataForPurchaseInvoiceBarChart()
	{
		$currentDate = date('Y-m-d');
		$monthFirstDate = date('Y-m-01');
		$lastTen = date('Y-m-d', strtotime('-10 days', strtotime($currentDate)));
		$lastMonthLastDate = date('Y-m-d', strtotime('-1 days', strtotime($monthFirstDate)));
		$yearFirstDate = date('Y-01-01');
		$lastYearLastDate = date('Y-m-d', strtotime('-1 days', strtotime($yearFirstDate)));
		$tommorowDate = date('Y-m-d', strtotime('+1 days', strtotime($currentDate)));
		$fiveYearsBeforeDate = date('Y-m-d', strtotime('-1825 days', strtotime($yearFirstDate)));
		
		$daily = [];
		$monthly = [];
		$yearly = [];

		// get daily sales invoice 
		$purchaseInvoiceDailyDetails = $this->getModel('PurchaseInvoiceTable')->getInvoiceTotalByDate($lastTen,$currentDate);
		foreach ($purchaseInvoiceDailyDetails as $key => $sIvalue) {
			$valueDate = explode("-", $sIvalue['purchaseInvoiceIssueDate']);
			$tmp = [
				'date' => date('d', strtotime($sIvalue['purchaseInvoiceIssueDate'])),
				'value' => round($sIvalue['TotalSales'], 2)
			];
			array_push($daily, $tmp);
		} 
		// get monthly details		
		$purchaseInvoiceMonthlyDetails = $this->getModel('PurchaseInvoiceTable')->getInvoiceTotalByDate($lastYearLastDate,$currentDate);
		foreach ($purchaseInvoiceMonthlyDetails as $key => $value) {
			
			$valueMonth = explode("-", $value['purchaseInvoiceIssueDate']);
			if ($monthly[$valueMonth[1]]) {
				$monthly[$valueMonth[1]] += floatval($value['TotalSales']);
			} else {
				$monthly[$valueMonth[1]] = floatval($value['TotalSales']);

			}	
		}

		// get last five years details
		$purchaseInvoiceAnnualDetails = $this->getModel('PurchaseInvoiceTable')->getInvoiceTotalByDate($fiveYearsBeforeDate,$currentDate);
		foreach ($purchaseInvoiceAnnualDetails as $key => $annValue) {
			
			$valueYear = explode("-", $annValue['purchaseInvoiceIssueDate']);
			if ($yearly[$valueYear[0]]) {
				$yearly[$valueYear[0]] += floatval($annValue['TotalSales']);
			} else {
				$yearly[$valueYear[0]] = floatval($annValue['TotalSales']);

			}	
		}
		
		$monthData = [];
		foreach ($monthly as $key => $value) {
			$tmp = [
				'date' => $key,
				'value' => round($value, 2)
			];
			array_push($monthData, $tmp);
		}
		$yearData = [];
		foreach ($yearly as $key => $value) {
			$tmp = [
				'date' => substr($key, -2),
				'value' => round($value, 2)
			];
			array_push($yearData, $tmp);
		}
		
		$dailyDetails = $this->calculateYaxisValue($daily);
		$monthlyDetails = $this->calculateYaxisValue($monthData);
		$annualDetails = $this->calculateYaxisValue($yearData);
		
		return [
			'daily' => $dailyDetails['value'],
			'monthly' => $monthlyDetails['value'],
			'yearly' => $annualDetails['value'],
			'dailyDivider' => $dailyDetails['divider'],
			'monthlyDivider' => $monthlyDetails['divider'],
			'yearlyDivider' => $annualDetails['divider'],
		];		
			
	}

	public function getDataForPurchasePaymentBarChart()
	{
		$currentDate = date('Y-m-d');
		$monthFirstDate = date('Y-m-01');
		$lastTen = date('Y-m-d', strtotime('-10 days', strtotime($currentDate)));
		$lastMonthLastDate = date('Y-m-d', strtotime('-1 days', strtotime($monthFirstDate)));
		$yearFirstDate = date('Y-01-01');
		$lastYearLastDate = date('Y-m-d', strtotime('-1 days', strtotime($yearFirstDate)));
		$tommorowDate = date('Y-m-d', strtotime('+1 days', strtotime($currentDate)));
		$fiveYearsBeforeDate = date('Y-m-d', strtotime('-1825 days', strtotime($yearFirstDate)));
		
		$daily = [];
		$monthly = [];
		$yearly = [];

		// get daily payments 
		$purchasePaymentDailyDetails = $this->getModel('SupplierPaymentsTable')->getPaymentValuesByGivenDateRange($lastTen, $currentDate);
		foreach ($purchasePaymentDailyDetails as $key => $sPValue) {
			$valueDate = explode("-", $sPValue['outgoingPaymentDate']);
			$tmp = [
				'date' => date('d', strtotime($sPValue['outgoingPaymentDate'])),
				'value' => $sPValue['paymentTotal']
			];
			array_push($daily, $tmp);
		} 
		// get monthly details		
		$purchaseMonthlyPaymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentValuesByGivenDateRange($yearFirstDate,$currentDate);
		foreach ($purchaseMonthlyPaymentDetails as $key => $value) {
			
			$valueMonth = explode("-", $value['outgoingPaymentDate']);
			if ($monthly[$valueMonth[1]]) {
				$monthly[$valueMonth[1]] += floatval($value['paymentTotal']);
			} else {
				$monthly[$valueMonth[1]] = floatval($value['paymentTotal']);

			}	
		}

		// get last five years details
		$purchaseAnnualPaymentDetails = $this->getModel('SupplierPaymentsTable')->getPaymentValuesByGivenDateRange($fiveYearsBeforeDate,$currentDate);
		foreach ($purchaseAnnualPaymentDetails as $key => $annValue) {
			$valueYear = explode("-", $annValue['outgoingPaymentDate']);
			if ($yearly[$valueYear[0]]) {
				$yearly[$valueYear[0]] += floatval($annValue['paymentTotal']);
			} else {
				$yearly[$valueYear[0]] = floatval($annValue['paymentTotal']);

			}	
		}

		$monthData = [];
		foreach ($monthly as $key => $value) {
			$tmp = [
				'date' => $key,
				'value' => $value
			];
			array_push($monthData, $tmp);
		}
		$yearData = [];
		foreach ($yearly as $key => $value) {
			$tmp = [
				'date' => substr($key, -2),
				'value' => $value
			];
			array_push($yearData, $tmp);
		}

		$dailyDetails = $this->calculateYaxisValue($daily);
		$monthlyDetails = $this->calculateYaxisValue($monthData);
		$annualDetails = $this->calculateYaxisValue($yearData);
		
		return [
			'daily' => $dailyDetails['value'],
			'monthly' => $monthlyDetails['value'],
			'yearly' => $annualDetails['value'],
			'dailyDivider' => $dailyDetails['divider'],
			'monthlyDivider' => $monthlyDetails['divider'],
			'yearlyDivider' => $annualDetails['divider'],
		];			
	}

	public function getNotificationCount($data)
	{
		$notificCount = $this->getModel('NotificationTable')->getNotificationCountForMobileApp();
		if ($notificCount['notificCount'] > 100) {
			return '99+';
		} else {
			return $notificCount['notificCount'];
		}
		
		
	}

	public function getNotificationForMobile($data)
	{
		$notifications = $this->getModel('NotificationTable')->getAllOpenNotificationToTheMobile();
		$locationDetails = $this->getModel('LocationTable')->fetchAll();
		$locations = [];
		
		if (sizeof($notifications) > 0) {
			foreach ($locationDetails as $key => $value) {
				$locations[$value['locationID']] = $value['locationName'];
			}
			$notification = [];
			foreach ($notifications as $key => $value) {
				$time = $this->timeAgo($value['notificationUpdatedDateTime']);
				
				$notification[] = [
					'title' => $value['notificationTitle'],
					'body' => 'In '.$locations[$value['locationID']].' location , '. $value['notificationBody'],
					'date' => $time,
					'id' =>  $value['notificationID']
				];
				# code...
			}
			
		} else {
			$notification[] = [
					'title' => '',
					'body' => '',
					'date' => '',
					'id' =>  ''
				];
		}
		
		return $notification;
	}


	public function getDashBoardDataForPOS($locationID)
	{
		return [
			'invoicePaymentDetails' => $this->getInvoiceAndPaidAmountsForDonut(),
			'invoicePaymentDetailsWithPaymentMethod' => $this->getInvoicePaymentDetailsForDonut(),
		];
	}

	public function getCompanyDetails()
	{
		// get compnay details
		$companyDetails = $this->getModel('CompanyTable')->getCompany();
		return $companyDetails->companyName;
	}

	public function getUserDetails()
	{
		// get users
		$usersData = $this->getModel('UserTable')->fetchAll();
		$users = [];
		foreach ($usersData as $key => $value) {
			$users[] = $value;
		}
		return $users;
	}

	public function getUserRoles()
	{
		// get user roles
		$rolesData = $this->getModel('RoleTable')->fetchAll();
		$roles = [];
		foreach ($rolesData as $key => $value) {
			$roles[] = $value;
		}
		return $roles;
	}

}