<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class RoleFeatureTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function checkFeatureEnability($roleID, $featureID)
    {
        $rowset = $this->tableGateway->select(array('roleID' => $roleID, 'featureID' => $featureID));

        $row = $rowset->current();

        if (!$row) {
            return NULL;
        }

        return $row->enable;
    }

    public function getFeatureEnablityByUserID($controller, $action, $userID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('roleFeature')
                ->columns(array('*'))
                ->join('feature', 'roleFeature.featureID = feature.featureID', array('*'), 'left')
                ->join('user', 'user.roleID = roleFeature.roleID', array('roleID', 'userID'), 'left')
                ->where(array(
                    'user.userID' => $userID,
                    'feature.featureController' => $controller,
                    'feature.featureAction' => $action));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $value) {
            return $value['roleFeatureEnabled'];
        }
    }

    public function chengeFeatureEnability($roleID, $featureID, $enable)
    {
        $data = array(
            'roleID' => $roleID,
            'featureID' => $featureID,
            'roleFeatureEnabled' => $enable,
        );

        if ($this->tableGateway->update($data, array('roleID' => $roleID, 'featureID' => $featureID))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function addRoleFeature($roleID, $featureID, $enable)
    {
        $data = array(
            'roleID' => $roleID,
            'featureID' => $featureID,
            'roleFeatureEnabled' => $enable,
        );

        $this->tableGateway->insert($data);
    }

    public function updateRoleFeature($roleFeatureID, $enable)
    {
        $data = array(
            'roleFeatureEnabled' => $enable,
        );

        $this->tableGateway->update($data, array('roleFeatureID' => $roleFeatureID));
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     */
    public function getGroupedRoleFeatures()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('roleFeature');
        $select->join('feature', 'roleFeature.featureID = feature.featureID', array('*'));
        $select->join('module', 'feature.moduleID = module.moduleID', array('moduleName'));
        $select->join('role', 'roleFeature.roleID = role.roleID', array('*'));
        $select->order(array('module.moduleName'));
        $select->order(array('feature.featureCategory'));
        $select->where(array('featureType' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    public function deleteRoleFeaturesByRoleID($roleID)
    {
        try {
            $result = $this->tableGateway->delete(array('roleID' => $roleID));
            return $result;
        } catch (\Exception $e) {
            $error = FALSE;
            return $error;
        }
    }

    public function getNotApiRoleFeaturesByRole($roleName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('roleFeature');
        $select->join('feature', 'roleFeature.featureID = feature.featureID', array('*'));
        $select->join('role', 'roleFeature.roleID = role.roleID', array('*'));
        $select->where(array('roleName' => $roleName));
        $select->where(array('featureType' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    public function getEnablesFeatureIDs($roleID)
    {
        $rowset = $this->tableGateway->select(array('roleID' => $roleID, 'roleFeatureEnabled' => 1));

        while ($row = $rowset->current()) {
            $featureIDs[] = $row->featureID;
        }

        return $featureIDs;
    }

}

?>
