<?php

namespace User\Model;

class Role
{

    public $roleID;
    public $roleName;

    public function exchangeArray($data)
    {
        $this->roleID = (isset($data['roleID'])) ? $data['roleID'] : null;
        $this->roleName = (isset($data['roleName'])) ? $data['roleName'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

?>
