<?php

namespace User\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class SalesPersonTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $isWithInactiveSalesPersons = FALSE)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('salesPerson')
                    ->columns(array('*'))
                    ->join('user', 'salesPerson.userID=user.userID', array('userUsername'), 'left')
                    ->order('salesPerson.salesPersonFirstName ASC');
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        }
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesPerson')
                ->columns(array('*'))
                ->order('salesPersonFirstName ASC');
        if (!$isWithInactiveSalesPersons) {
            $select->where(array("salesPerson.salesPersonStatus" => 1));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return (object) $resultSet;
    }

    public function searchSelesPersonByKeyword($keyword, $paginated = FALSE)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('salesPerson')
                    ->columns(array('*'))
                    ->join('user', 'salesPerson.userID=user.userID', array('userUsername'), 'left')
                    ->order('salesPersonFirstName ASC');
            $select->where(new PredicateSet(array(
                new Operator('salesPerson.salesPersonSortName', 'like', '%' . $keyword . '%'),
                new Operator('salesPerson.salesPersonFirstName', 'like', '%' . $keyword . '%'),
                new Operator('salesPerson.salesPersonLastName', 'like', '%' . $keyword . '%'),
                    ), PredicateSet::OP_OR));
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        }
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesPerson')
                ->columns(array('*'))
                ->order('salesPersonFirstName ASC');
        $select->where(new PredicateSet(array(
            new Operator('salesPerson.salesPersonSortName', 'like', '%' . $keyword . '%'),
            new Operator('salesPerson.salesPersonFirstName', 'like', '%' . $keyword . '%'),
            new Operator('salesPerson.salesPersonLastName', 'like', '%' . $keyword . '%'),
                ), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return (object) $resultSet;
    }

    public function searchBySortName($sortName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesPerson')
                ->columns(array('*'))
                ->where(array('salesPersonSortName' => $sortName));
        $statment = $sql->prepareStatementForSqlObject($select);
        $resutset = $statment->execute();
        return $resutset;
    }

    public function savaSalesPerson(SalesPerson $salesPerson)
    {
        $data = array(
            'salesPersonSortName' => $salesPerson->salesPersonSortName,
            'salesPersonTitle' => $salesPerson->salesPersonTitle,
            'salesPersonFirstName' => $salesPerson->salesPersonFirstName,
            'salesPersonLastName' => $salesPerson->salesPersonLastName,
            'salesPersonEmail' => $salesPerson->salesPersonEmail,
            'salesPersonAddress' => $salesPerson->salesPersonAddress,
            'salesPersonTelephoneNumber' => $salesPerson->salesPersonTelephoneNumber,
            'userID' => $salesPerson->userID,
        );

        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        if ($insertedID) {
            return $insertedID;
        } else {
            return FALSE;
        }
    }

    public function deleteSalesPerson($salesPersonID)
    {
        return $this->tableGateway->delete(array('salesPersonID' => $salesPersonID));
    }

    public function getSalesPersonByID($salesPersonID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesPerson')
                ->columns(array('*'))
                ->where(array('salesPersonID' => $salesPersonID));
        $statment = $sql->prepareStatementForSqlObject($select);
        $resutset = $statment->execute();
        return $resutset;
    }

    public function updateSalesPerson(SalesPerson $salesPerson)
    {
        $data = array(
            'salesPersonSortName' => $salesPerson->salesPersonSortName,
            'salesPersonTitle' => $salesPerson->salesPersonTitle,
            'salesPersonFirstName' => $salesPerson->salesPersonFirstName,
            'salesPersonLastName' => $salesPerson->salesPersonLastName,
            'salesPersonEmail' => $salesPerson->salesPersonEmail,
            'salesPersonAddress' => $salesPerson->salesPersonAddress,
            'salesPersonTelephoneNumber' => $salesPerson->salesPersonTelephoneNumber,
            'userID' => $salesPerson->userID,
        );
        $result = $this->tableGateway->update($data, array('salesPersonID' => $salesPerson->salesPersonID));
        return $result;
    }

    public function getSalesPersonByIds($salesPersonIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesPerson')
                ->columns(array('*'))
        ->where->in('salesPersonID', $salesPersonIds);
        $statment = $sql->prepareStatementForSqlObject($select);
        $resutset = $statment->execute();
        return $resutset;
    }

    /**
     * @author Madawa Chandrarathne <madawa@thinkcube.com>
     * @param type $searchKey
     * @return type
     */
    public function searchSalesPersonsForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesPerson')
                ->columns(array('salesPersonID', 'salesPersonFirstName', 'salesPersonLastName', 'salesPersonSortName'))
                ->where(new PredicateSet(array(new Operator('salesPersonFirstName', 'like', '%' . $searchKey . '%'), new Operator('salesPersonLastName', 'like', '%' . $searchKey . '%'), new Operator('salesPersonSortName', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $select->quantifier('DISTINCT');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get sales person details by id
     * @param type $salesPersonId
     * @return type
     */
    public function getSalesPersonDetailsBySalesPersonId($salesPersonId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesPerson')
                ->columns(array('*'))
        ->where->equalTo('salesPersonID', $salesPersonId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * @param string $userID
     * @return  array
     */
    public function getSalesPersonIDByActiveUserId($userID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesPerson')
                ->columns(array('salesPersonID'))
        ->where->equalTo('userID', $userID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

    public function changeStatus($status, $salesPersonId)
    {
        if ($this->tableGateway->update(array('salesPersonStatus' => $status), array('salesPersonID' => $salesPersonId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
