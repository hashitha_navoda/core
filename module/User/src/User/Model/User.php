<?php

namespace User\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class User implements InputFilterAwareInterface
{

    public $ID;
    public $username;
    public $firstName;
    public $lastName;
    public $email1;
    public $userPassword;
    public $defautLocation;
    public $defaultLanguage;
    public $userActivated;
    public $userResetPasswordToken;
    public $userResetPasswordExpire;
    public $roleID;
    public $userShowWelcome;
    public $entityID;
    public $userTypeID;
    public $userFirstLogin;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->ID = (!empty($data['userID'])) ? $data['userID'] : null;
        $this->username = (!empty($data['userUsername'])) ? $data['userUsername'] : null;
        $this->firstName = (!empty($data['userFirstName'])) ? $data['userFirstName'] : null;
        $this->lastName = (!empty($data['userLastName'])) ? $data['userLastName'] : null;
        $this->email1 = (!empty($data['userEmail1'])) ? $data['userEmail1'] : null;
        $this->userPassword = (!empty($data['userPassword'])) ? $data['userPassword'] : null;
        $this->defaultLanguage = (!empty($data['defaultLanguage'])) ? $data['defaultLanguage'] : 'en_US'; //// defaultvalue cahnged from 1 to en_US
        $this->defautLocation = (!empty($data['defautLocation'])) ? $data['defautLocation'] : null;
        $this->userActivated = (!empty($data['userActivated'])) ? $data['userActivated'] : 1;
        $this->userResetPasswordToken = (!empty($data['userResetPasswordToken'])) ? $data['userResetPasswordToken'] : null;
        $this->userResetPasswordExpire = (!empty($data['userResetPasswordExpire'])) ? $data['userResetPasswordExpire'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->userTypeID = (!empty($data['userTypeID'])) ? $data['userTypeID'] : null;
        if (!empty($data['roleID']) && $data['userTypeID'] == 1) {
            $this->roleID = $data['roleID'];
        } else {
            if ($data['userTypeID'] == 2) {
                $this->roleID = null;
            } 
        }

        // $this->roleID = (isset($data['roleID'])) ? $data['roleID'] : '1';
        $this->userShowWelcome = (!empty($data['userShowWelcome'])) ? $data['userShowWelcome'] : 0;
        $this->userFirstLogin = (empty($data['userFirstLogin']) && $data['userFirstLogin'] == 0) ? $data['userFirstLogin'] : 1;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'userUsername',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'userFirstName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'userLastName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'userEmail1',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'message' => 'This is not a valid email address'
                                )
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'roleID',
                        // 'required' => true,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'userTypeID',
                        'required' => true,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'userPassword',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 6,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
