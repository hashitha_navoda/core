<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class RoleTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function addRole($roleName)
    {
        $data = array(
            'roleName' => $roleName
        );

        if ($this->tableGateway->insert($data)) {
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;
        }
        return FALSE;
    }

    public function getRoleIdByName($roleName)
    {
        $row = $this->tableGateway->select(array(
            'roleName' => $roleName
        ));

        return $row->current()->roleID;
    }

    public function deleteRoleByID($roleID)
    {
        try {
            $result = $this->tableGateway->delete(array('roleID' => $roleID));
            return $result;
        } catch (\Exception $e) {
            $error = FALSE;
            return $error;
        }
    }

    public function deleteRoleByName($roleName)
    {
        return $this->tableGateway->delete(array('roleName' => $roleName));
    }

}

?>
