<?php

namespace User\Model;

class RoleFeature
{

    public $id;
    public $roleID;
    public $featureID;
    public $enable;

    public function exchangeArray($data)
    {
        $this->id = (isset($data['id'])) ? $data['id'] : null;
        $this->roleID = (isset($data['roleID'])) ? $data['roleID'] : null;
        $this->featureID = (isset($data['featureID'])) ? $data['featureID'] : null;
        $this->enable = (isset($data['roleFeatureEnabled'])) ? $data['roleFeatureEnabled'] : ((isset($data['enable'])) ? $data['enable'] : NULL);
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

?>
