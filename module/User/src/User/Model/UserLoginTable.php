<?php

/**
 * @author sahrmilan <sharmilan@thinkcube.com>
 */

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use User\Model\UserLogin;

class UserLoginTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savaUserLogin(UserLogin $userLogin)
    {
        $data = array(
            'userID' => $userLogin->userID,
            'sessionToken' => $userLogin->sessionToken,
            'lastLogin' => $userLogin->lastLogin,
        );

        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        if ($insertedID) {
            return $insertedID;
        } else {
            return FALSE;
        }
    }

    public function deleteUserLoginByUserID($userID)
    {
        return $this->tableGateway->delete(array('userID' => $userID));
    }

    public function deleteUserLogin($userID, $sessionToken)
    {

        return $this->tableGateway->delete(array(
                    'userID' => $userID,
                    'sessionToken' => $sessionToken
        ));
    }

    public function getUserloginByUserID($userID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('userLogin')
                ->columns(array('*'))
                ->where(array('userID' => $userID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultArray = [];
        foreach ($resultSet as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function updateLastLogin(UserLogin $userLogin)
    {
        $data = array(
            'lastLogin' => $userLogin->lastLogin,
        );
        $result = $this->tableGateway->update($data, array(
            'userLoginID' => $userLogin->userLoginID,
            'sessionToken' => $userLogin->sessionToken
        ));
        return $result;
    }

    public function updateSessionToken(UserLogin $userLogin)
    {
        $data = array(
            'sessionToken' => $userLogin->sessionToken,
            'lastLogin' => $userLogin->lastLogin,
        );
        $result = $this->tableGateway->update($data, array('userID' => $userLogin->userID));
        return $result;
    }

}
