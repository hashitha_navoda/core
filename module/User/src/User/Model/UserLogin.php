<?php

namespace User\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class UserLogin implements InputFilterAwareInterface
{

    public $userLoginID;
    public $userID;
    public $sessionToken;
    public $lastLogin;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->userLoginID = (isset($data['userLoginID'])) ? $data['userLoginID'] : null;
        $this->userID = (isset($data['userID'])) ? $data['userID'] : null;
        $this->sessionToken = (isset($data['sessionToken'])) ? $data['sessionToken'] : null;
        $this->lastLogin = (isset($data['lastLogin'])) ? $data['lastLogin'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
