<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class FeatureTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getFeatureId($Featurename)
    {
        $rowset = $this->tableGateway->select(array('featureName' => $Featurename));
        $row = $rowset->current();

        if (!$row) {
            return NULL;
        }
        return $row;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param String Module Name
     * @return type
     */
    public function getFeaturesByModule($moduleName, $withAPI = FALSE)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('feature');
        $select->join('module', 'feature.moduleID = module.moduleID', array('moduleName'));
        $select->where(array('moduleName' => $moduleName));
        if ($withAPI == FALSE) {
            $select->where(array('featureType' => 0));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     */
    public function getGroupedFeatures($withAPI = FALSE)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('feature');
        $select->join('module', 'feature.moduleID = module.moduleID', array('moduleName'));
        $select->order(array('module.moduleName'));
        $select->order(array('feature.featureCategory'));
        if ($withAPI == FALSE) {
            $select->where(array('featureType' => 0));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    public function getFeaturenameByFeatureID($featureID)
    {
        $rowset = $this->tableGateway->select(array('featureID' => $featureID));
        $row = $rowset->current();

        if (!$row) {
            return NULL;
        }
        return $row->featureName;
    }

}

?>
