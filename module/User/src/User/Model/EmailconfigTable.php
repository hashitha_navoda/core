<?php

namespace User\Model;

use Zend\Db\TableGateway\TableGateway;

class EmailconfigTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getConfiguration()
    {
        $rowset = $this->tableGateway->select();
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function saveConfiguration(Emailconfig $Emailconfig)
    {
        $data = array(
            'id' => $Emailconfig->id,
            'email' => $Emailconfig->email,
            'password' => $Emailconfig->password,
            'mailName' => $Emailconfig->mailName,
            'host' => $Emailconfig->host,
            'port' => $Emailconfig->port,
        );
        $result = $this->getConfiguration();
        if ($result == NULL) {
            error_log("Null conf");
            $this->tableGateway->insert($data);
        } else {
            $res = $this->getConfiguration($Emailconfig->email);
            if ($res) {
                $data['id'] = $res->id;
                $this->tableGateway->update($data, array('id' => $res->id));
            } else {
                throw new \Exception('Emaill address does not exist');
            }
        }
    }


    public function removeConfiguration()
    {
        $this->tableGateway->delete();
    }

}

?>
