<?php

namespace User\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class UserSetting implements InputFilterAwareInterface
{

    public $userSettingID;
    public $userID;
    public $userSettingLeftWidgetOrder;
    public $userSettingRightWidgetOrder;
    public $userSettingWidgetStatus;
    public $widgetCheckboxList;
    protected $inputFilter;
    public static $leftWidgetArray = array("revenue", "ovrdueinvoices", "cashoutflw");
    public static $rightWidgetArray = array("cashinflw", "cashflw", "revenuein", "cashoutflwgraph");

    public function exchangeArray($data)
    {
        $this->userID = (isset($data['userID'])) ? $data['userID'] : null;
        $this->userSettingID = (isset($data['userSettingID'])) ? $data['userSettingID'] : null;
        $this->userSettingLeftWidgetOrder = (isset($data['userSettingLeftWidgetOrder'])) ? $data['userSettingLeftWidgetOrder'] : null;
        $this->userSettingRightWidgetOrder = (isset($data['userSettingRightWidgetOrder'])) ? $data['userSettingRightWidgetOrder'] : null;
        $this->userSettingWidgetStatus = (isset($data['userSettingWidgetStatus'])) ? $data['userSettingWidgetStatus'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

}
