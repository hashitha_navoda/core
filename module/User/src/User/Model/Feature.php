<?php

namespace User\Model;

class Feature
{

    public $featureID;
    public $featureName;
    public $featureController;
    public $featureAction;
    public $featureType;
    public $moduleID;

    public function exchangeArray($data)
    {
        $this->featureID = (isset($data['featureID'])) ? $data['featureID'] : null;
        $this->featureName = (isset($data['featureName'])) ? $data['featureName'] : null;
        $this->featureController = (isset($data['featureController'])) ? $data['featureController'] : null;
        $this->featureAction = (isset($data['featureAction'])) ? $data['featureAction'] : null;
        $this->featureType = (isset($data['featureType'])) ? $data['featureType'] : 0;
        $this->moduleID = (isset($data['moduleID'])) ? $data['moduleID'] : 1;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

?>
