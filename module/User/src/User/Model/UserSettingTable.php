<?php

/**
 * @author sandun  <sandun@thinkcube.com>
 * There contains Usersetting's functions
 */

namespace User\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use User\Model\UserSetting;

class UserSettingTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function addWidget($data)
    {
        $this->tableGateway->insert($data);
        return true;
    }

    public function updateWidget($lWiArray, $rWiArray, $userID)
    {

        $data = array(
            'userID' => $userID,
            'userSettingleftWidgetOrder' => $lWiArray,
            'userSettingrightWidgetOrder' => $rWiArray,
            'userSettingWidgetStatus' => 1
        );

        $rowset = $this->tableGateway->select(array('userID' => $userID));

        if (!$rowset->current()) {
            $this->tableGateway->insert($data);
        } else {
            $this->tableGateway->update($data, array('userID' => $userID));
        }

        return TRUE;
    }

    public function updateCheckboxList($checkboxListArray, $userID)
    {
        $data = array(
            'userID' => $userID,
            'widgetCheckboxList' => $checkboxListArray,
            'widgetStatus' => 1
        );


        $this->tableGateway->update($data, array('userID' => $userID));


        return TRUE;
    }

    public function getWidgetSettings($userID = 0)
    {
        $rowset = $this->tableGateway->select(array('userID' => $userID, 'userSettingWidgetStatus' => 1));
        return $rowset->current();
    }

}
