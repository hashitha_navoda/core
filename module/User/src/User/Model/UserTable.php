<?php

namespace User\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class UserTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('user')
                    ->columns(array('*'))
                    ->join('entity', 'user.entityID = entity.entityID', array('deleted'), 'left')
                    ->order('user.userUsername ASC')
                    ->where(array('entity.deleted' => '0'));
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        }
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('user')
                ->columns(array('*'))
                ->join('entity', 'user.entityID = entity.entityID', array('deleted'), 'left')
                ->order('user.userUsername ASC')
                ->where(array('entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return (object) $resultSet;
    }

    public function getUser($userID)
    {
        $userID = (int) $userID;
        $rowset = $this->tableGateway->select(array('userID' => $userID));
        $row = $rowset->current();
        if (!$row) {
            return FALSE;
        }
        return $row;
    }

    public function getUsersByRoleID($roleID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('user')
                ->columns(array('*'))
                ->where(array('roleID' => $roleID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultArray = [];
        foreach ($resultSet as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getUserforSearch($name)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('user');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $name . '\' in user.userUsername )>0,POSITION(\'' . $name . '\' in user.userUsername), 9999), '
                    . 'IF(POSITION(\'' . $name . '\' in user.userFirstName )>0,POSITION(\'' . $name . '\' in user.userFirstName), 9999), '
                    . 'IF(POSITION(\'' . $name . '\' in user.userLastName )>0,POSITION(\'' . $name . '\' in user.userLastName), 9999), '
                    . 'IF(POSITION(\'' . $name . '\' in user.userEmail1 )>0,POSITION(\'' . $name . '\' in user.userEmail1), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(user.userUsername ), '
                    . 'CHAR_LENGTH(user.userFirstName ),'
                    . 'CHAR_LENGTH(user.userLastName ),'
                    . 'CHAR_LENGTH(user.userEmail1 )'
                    . ') '),
            '*',
        ));
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
//        $select->order('userUsername ASC');
        $select->join('entity', 'user.entityID = entity.entityID', array('deleted'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(
            new Operator('user.userUsername', 'like', '%' . $name . '%'),
            new Operator('user.userFirstName', 'like', '%' . $name . '%'),
            new Operator('user.userLastName', 'like', '%' . $name . '%'),
            new Operator('user.userEmail1', 'like', '%' . $name . '%')
                ), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }

    public function getUserByID($userID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('user')
                ->columns(array('*'))
                ->join('entity', 'user.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'userID' => $userID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();

        if ($resultSet->count() == 0) {
            return NULL;
        }
        return (object) $resultSet->current();
    }

    public function saveUser(User $user, $entityID)
    {

        $data = array(
            'userUsername' => $user->username,
            'userFirstName' => $user->firstName,
            'userLastName' => $user->lastName,
            'userEmail1' => $user->email1,
            'userPassword' => $user->userPassword,
            'roleID' => $user->roleID,
            'defaultLanguage' => $user->defaultLanguage,
            'defautLocation' => $user->defautLocation,
            'userTypeID' => $user->userTypeID,
            'entityID' => $entityID,
            'userActivated' => $user->userActivated
        );

        $userID = (int) $user->userID;
        if ($userID == 0 AND $this->getUserByUsername($user->username) != NULL) {
            return FALSE;
        }

        if ($userID == 0) {
            if ($this->tableGateway->insert($data)) {
                return $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            } else {
                return FALSE;
            }
        } else {
            if ($this->getUser($userID)) {
                if ($this->tableGateway->update($data, array('userID' => $userID))) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                throw new \Exception('Form id does not exist');
                return FALSE;
            }
        }
    }

    public function updateUser($data, $userID)
    {
        $result = $this->tableGateway->update($data, array('userID' => $userID));
        return true;
    }

    public function updateUserActive($userID, $userActivated)
    {
        $data = array('userActivated' => $userActivated);
        $result = $this->tableGateway->update($data, array('userID' => $userID));
        return $result;
    }

    public function getActiveUsers($isWithoutPosUsers = true)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('user');
        $select->order('userUsername ASC');
        $select->join('entity', 'user.entityID = entity.entityID', array('deleted'));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('user.userActivated', '=', '1'))));

        if ($isWithoutPosUsers) {
            $select->where(new PredicateSet(array(new Operator('user.userTypeID', '=', '1'))));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultArray = [];
        foreach ($resultSet as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function deleteUser($userID)
    {
        return $this->tableGateway->delete(array('userID' => $userID));
    }

    public function getUserByUsername($username)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('user')
                ->columns(array('*'))
                ->join('entity', 'user.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'userUsername' => $username));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();

        if ($resultSet->count() == 0) {
            return NULL;
        }
        return (object) $resultSet->current();
    }

    public function changePassword(User $user)
    {
        $userID = (int) $user->userID;

        $existingUserDetails = $this->getUser($userID);
        $data = array(
            'password' => md5($existingUserDetails->username . $user->password . $existingUserDetails->username),
        );

        if (isset($existingUserDetails)) {
            if ($this->tableGateway->update($data, array('userID' => $userID))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            throw new \Exception('Form id does not exist');
            return FALSE;
        }
    }

    public function getUserDefaultLocationByUserID($userID)
    {

        $userID = (int) $userID;

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('user');
        $select->join('location', 'user.defautLocation = location.locationID', array('locationID', 'locationName', 'locationCode'));
        $select->where(array('userID' => $userID, 'location.locationStatus' => 1));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        return $rowset->current();
    }

    public function getDefaultLocationsByuserID($userID)
    {
        $userID = (int) $userID;
        $rowset = $this->tableGateway->select(array('userID' => $userID));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $userID");
        }
        return $row->defautLocation;
    }

    public function updateUserDefaultLocation($user)
    {
        try {
            $this->tableGateway->update(array('defautLocation' => $user['defautLocation']), array('userID' => $user['userID']));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
        return true;
    }

    public function getEntityIDbyUserID($userID)
    {
        $rowset = $this->tableGateway->select(array('userID' => $userID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }

        return $row->entityID;
    }

    public function checkDefaultLocation($locationID)
    {
        $rowset = $this->tableGateway->select(array('defautLocation' => $locationID));
        return $rowset;
    }

    public function getAllUsersWithLicenseData()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('user');
        $select->columns(array('userID', 'userUsername', 'userFirstName', 'userLastName', 'userEmail1','userActivated'));
        $select->join('entity', 'user.entityID = entity.entityID', array('deleted'));
        $select->join('licence', 'user.userID = licence.userID', array('licenceName', 'licenceStartingDate', 'licenceExpireDate'), 'left');
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultArray = [];
        foreach ($resultSet as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function checkUserLoginFirstTime($userID)
    {
        $rowset = $this->tableGateway->select(array('userShowWelcome' => 0, 'userID' => $userID));

        if (count($rowset) == 0) {
            return NULL;
        }
        return true;
    }

    public function updateUserLog($userID)
    {
        $data = array(
            'userShowWelcome' => 1
        );

        try {
            $this->tableGateway->update($data, array('userID' => $userID));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

}
