<?php

namespace User\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SalesPerson implements InputFilterAwareInterface
{

    public $salesPersonID;
    public $salesPersonSortName;
    public $salesPersonTitle;
    public $salesPersonFirstName;
    public $salesPersonLastName;
    public $salesPersonEmail;
    public $salesPersonAddress;
    public $salesPersonStatus;
    public $salesPersonTelephoneNumber;
    public $userID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->salesPersonID = (isset($data['salesPersonID'])) ? $data['salesPersonID'] : null;
        $this->salesPersonSortName = (isset($data['salesPersonSortName'])) ? $data['salesPersonSortName'] : null;
        $this->salesPersonTitle = (isset($data['salesPersonTitle'])) ? $data['salesPersonTitle'] : null;
        $this->salesPersonFirstName = (isset($data['salesPersonFirstName'])) ? $data['salesPersonFirstName'] : null;
        $this->salesPersonLastName = (isset($data['salesPersonLastName'])) ? $data['salesPersonLastName'] : null;
        $this->salesPersonEmail = (isset($data['salesPersonEmail'])) ? $data['salesPersonEmail'] : null;
        $this->salesPersonAddress = (isset($data['salesPersonAddress'])) ? $data['salesPersonAddress'] : null;
        $this->salesPersonTelephoneNumber = (isset($data['salesPersonTelephoneNumber'])) ? $data['salesPersonTelephoneNumber'] : null;
        $this->userID = (isset($data['userID'])) ? $data['userID'] : 1;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesPersonSortName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesPersonFirstName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesPersonLastName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesPersonEmail',
                        'required' => true,
                        'validators' => array(
                            array(
                                'name' => 'EmailAddress',
                                'options' => array(
                                    'message' => 'This is not a valid email address'
                                )
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesPersonTitle',
                        'required' => true,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesPersonAddress',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesPersonTelephoneNumber',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
