<?php

/**
 * @author Damith Thamara  <damith@thinkcube.com>
 * This file contains User related controller functions
 */

namespace User\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use User\Model\User;
use User\Form\UserForm;
use User\Form\UserEditForm;
use User\Form\UserLoginForm;
use User\Form\ProfileEditForm;
use User\Model\UserLogin;
use User\Model\UserSetting;
use Inventory\Model\ItemIn;
use Core\Library\RestApiRoutes;

class UserController extends CoreController {

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'user_upper_menu';
    protected $paginator;
    protected $userID;
    protected $username;
    protected $cdnUrl;
    protected $user_session;
    protected $displaySettings;
    protected $taxTable;
    protected $locationTable;
    protected $licenceTable;
    protected $userLoginTable;
    protected $userCountry;

    public function __construct() {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->packageID = $this->user_session->packageID;
        $this->jobModuleEnabled = $this->user_session->jobModuleEnabled;
        $this->crmModuleEnabled = $this->user_session->crmModuleEnabled;
    }

    public function forcePasswordResetAction()
    {
        $token = substr(md5(uniqid(mt_rand(), true)), 0, 30);
        $user = $this->CommonTable('User\Model\UserTable')->getUser($this->userID);

        $data = array(
            'userResetPasswordToken' => $token,
            'userResetPasswordExpire' => gmdate('Y-m-d H:i:s', strtotime('+60 minutes'))
        );

        $this->CommonTable('User\Model\UserTable')->updateUser($data, $this->userID);

        $view = new viewModel(array(
                                'userID' => $this->userID,
                                'token' => $token
                            ));
        $view->setTerminal(true);
        return $view;
    }

    /**
     * This function view the user create page
     * @author ashan madushka  <ashan@thinkcube.com>
     * @return \Zend\View\Model\ViewModel
     */
    public function createAction() {
        $this->getSideAndUpperMenus('User', 'Users');
        $roleRes = $this->CommonTable('User\Model\RoleTable')->fetchAll();

        $roles = array();
        foreach ($roleRes as $role) {
            $roles[$role->roleID] = $role->roleName;
        }

        $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyLicence();
        $licenses = array();
        $isEmptyLicense = FALSE;
        if (empty($emptyLicenses)) {
            $isEmptyLicense = TRUE;
        }

        $licenses[0] = ' ';
        foreach ($emptyLicenses as $emptyLicense) {
            $licenses[$emptyLicense['licenceID']] = $emptyLicense['licenceName'] . '  ' . $emptyLicense['licenceExpireDate'];
        }

        $userTypes = [
            1 => "ezBiz System User",
            2 => "POS User"
        ];


        $form = new UserForm($roles, $licenses, $userTypes);

        $viewmodel = new ViewModel(array(
            'form' => $form,
            'isEmptyLicense' => $isEmptyLicense,
        ));
        return $viewmodel;
    }

    /**
     * This function view the user edit page
     * @author ashan madushka  <ashan@thinkcube.com>
     * @return \Zend\View\Model\ViewModel
     */
    public function editAction() {
        $this->getSideAndUpperMenus('User', 'Users');
        $roleRes = $this->CommonTable('User\Model\RoleTable')->fetchAll();

        $roles = array();
        foreach ($roleRes as $role) {
            $roles[$role->roleID] = $role->roleName;
        }
        $userID = $this->params()->fromRoute('param1');

//          Add license to drop down list
        $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyAndAssignedLicenceByUserID($userID);
        $licenses = array();
        $isEmptyLicense = FALSE;
        if (empty($emptyLicenses)) {
            $isEmptyLicense = TRUE;
        }

        $licenses[0] = ' ';
        foreach ($emptyLicenses as $emptyLicense) {
            $licenses[$emptyLicense['licenceID']] = $emptyLicense['licenceName'] . '  ' . $emptyLicense['licenceExpireDate'];
        }
        $userLicense = $this->CommonTable('Core\Model\LicenceTable')->getLicenceByUserID($userID);
        foreach ($userLicense as $lice) {
            $userLicenseID = $lice['licenceID'];
        }

        $userTypes = [
            1 => "ezBiz System User",
            2 => "POS User"
        ];

        $form = new UserEditForm($roles, $licenses, $userTypes);
        $rett = $this->CommonTable('User\Model\UserTable')->getUser($userID);
        $form->get('userID')->setValue($userID);
        $form->get('userTypeID')->setValue($rett->userTypeID);
        $form->get('userFirstName')->setValue($rett->firstName);
        $form->get('userLastName')->setValue($rett->lastName);
        $form->get('userEmail1')->setValue($rett->email1);
        $form->get('roleID')->setValue($rett->roleID);
        $form->get('licenseID')->setValue(isset($userLicenseID) ? $userLicenseID : null);
        $viewmodel = new ViewModel(array(
            'form' => $form,
            'username' => $rett->username,
            'isEmptyLicense' => $isEmptyLicense
        ));
//            $viewmodel->setTerminal(true);
        return $viewmodel;
    }

    /**
     * This function lists the users locationd
     * @author ashan madushka  <ashan@thinkcube.com>
     * @return \Zend\View\Model\ViewModel
     */
    public function editUserLocationAction() {
        $this->getSideAndUpperMenus('User', 'Users');
        $userID = $this->params()->fromRoute('param1');
        $locations = $this->CommonTable('Core\Model\LocationTable')->getUserAllocatedLocationsByuserID($userID);
        $defaultLocation = $this->CommonTable('User\Model\UserTable')->getDefaultLocationsByuserID($userID);

        $viewModel = new ViewModel(array(
            'locations' => $locations,
            'defaultLocation' => $defaultLocation,
            'userID' => $userID
        ));
        return $viewModel;
    }

    public function indexAction() {
        $globaldata = $this->getServiceLocator()->get('config');
        $this->getSideAndUpperMenus('User', 'Users');
        $this->getPaginatedUsers();

        $userCount = $this->CommonTable('User\Model\UserTable')->fetchAll();
        $hideUserCreate = false;
        if ($this->packageID == "1") {
            if (count($userCount) >= 2) {
                $hideUserCreate = true;
            }
        } else if ($this->packageID == "2") {
            if (count($userCount) >= 4) {
                $hideUserCreate = true;
            }
        } else if ($this->packageID == "3") {
            if (count($userCount) >= 10) {
                $hideUserCreate = true;
            }
        } 

        $viewHeader = new ViewModel(array(
            'hideUserCreate' => $hideUserCreate
        ));
        $viewHeader->setTemplate("user/user/user-header");

        $view = new ViewModel(array(
            'users' => $this->paginator
                )
        );

        $view->addChild($viewHeader, 'userHeader');
        $this->setLogMessage("User list accessed");
        return $view;
    }

    ////////////////////////END INDEX ACTION////////////////////////

    /**
     * This function list the users to front end
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return type JSON
     */
    public function viewUserListAction() {
        $this->getPaginatedUsers();

        $view = new ViewModel(array(
            'users' => $this->paginator
                )
        );
        $view->setTemplate("user/user/index");

        $this->setLogMessage("User list accessed");
        $this->html = $view;
        return $this->JSONRespondHtml();
    }

    /**
     * This fucntion adds users
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return boolean
     */
    public function addAction() {

        $roleRes = $this->CommonTable('User\Model\RoleTable')->fetchAll();

        $roles = array();
        foreach ($roleRes as $role) {
            $roles[$role->roleID] = $role->roleName;
        }

        $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyLicence();
        $licenses = array();
        $licenses[0] = ' ';
        foreach ($emptyLicenses as $emptyLicense) {
            $licenses[$emptyLicense['licenceID']] = $emptyLicense['licenceName'] . '  ' . $emptyLicense['licenceExpireDate'];
        }
        $userTypes = [
            1 => "ezBiz System User",
            2 => "POS User"
        ];

        $form = new UserForm($roles, $licenses, $userTypes);

        $request = $this->getRequest();

        if ($request->isPost()) {
            $user = new User();
            $form->setInputFilter($user->getInputFilter());
            $form->getInputFilter()->remove('licenseID');
            $form->setData($request->getPost('user'));
            if ($form->isValid()) {
                $user->exchangeArray($request->getPost('user'));
                $entityID = $this->createEntity();
                //create password salt
                $passwordSalt = md5(mt_rand());
                $password = md5($request->getPost('user')['userPassword'] . $passwordSalt);
                $user->userPassword = $password . ':' . $passwordSalt;
                $licenseID = $request->getPost('user')['licenseID'];
                $lastInsertID = $this->CommonTable('User\Model\UserTable')->saveUser($user, $entityID);
                if ($licenseID != 0) {
                    $this->CommonTable('Core\Model\LicenceTable')->assignLicenceForUserID($licenseID, $lastInsertID);
                }
                /**
                 * @author sandun dissanyake <sandun@thinkcube.com>
                 * insert into UserSetting Table to widgets list, related to last Registered user which currently available on application
                 */
                $this->CommonTable('User\Model\UserSettingTable')->updateWidget(json_encode(UserSetting::$leftWidgetArray), json_encode(UserSetting::$rightWidgetArray), $lastInsertID);
            }

            //Allocate locations
            $userLocation = new \Core\Model\UserLocation;

            $sm = $this->getServiceLocator();
            $locationUserTable = $sm->get('Core\Model\UserLocationTable');

            $userDetails = $this->CommonTable('User\Model\UserTable')->getUserByUsername($request->getPost('user')['userUsername']);

            foreach ($request->getPost('location') as $locationID) {
                $userLocation->exchangeArray(array(
                    'userID' => $userDetails->userID,
                    'locationID' => $locationID
                        )
                );
                $res2 = $locationUserTable->saveUserLocation($userLocation);
            }

            if ($res2) {
                $this->setLogMessage("Added successfully");
                $msg = $this->getMessage('SUCC_USERCONTRO_USERCREATE');
                $status = TRUE;
            } else {
                $this->setLogMessage("Adding failed");
                $msg = $this->getMessage('ERR_USERCONTRO_USERCREATE');
                $status = FALSE;
            }
        }

        $insertedUsername = $request->getPost('user')['userUsername'];
        if ($status) {
            $this->flashMessenger()->addMessage("User $insertedUsername created successfully.");
        }
        $this->setLogMessage("User list accessed");
        $this->status = $status;
        $this->msg = $msg;
        return $this->JSONRespond();
    }

    /////////////////////////END USER ADD///////////////////////////

    /**
     * This function delete users by ID
     * @author PRASA7  <prasanna@thinkcube.com>
     * @param String $userID
     */
    public function deleteAction($userID = NULL) {

        $userID = $this->params()->fromRoute('param1', NULL);
        if (!is_null($userID)) {
            $deletingUserDetails = $this->CommonTable('User\Model\UserTable')->getUser($userID);
            if ($userID != $this->userID) {

                $res = $this->updateDeleteInfoEntity($this->CommonTable('User\Model\UserTable')->getEntityIDbyUserID($userID));

                if ($res) {
                    $this->CommonTable('Core\Model\LicenceTable')->makeEmptyLicenceByUserID($userID);
                    $this->setLogMessage("deleted Successfully");
                    $this->msg = $this->getMessage('SUCC_USERCONTRO_USERDELETE', array($deletingUserDetails->username));
                    $this->status = TRUE;
                }
            } else {
                $this->setLogMessage("You can't delete yourselves");
                $this->msg = $this->getMessage('ERR_USERCONTRO_USERDELETE', array($deletingUserDetails->username));
                $this->status = FALSE;
            }
        }

        $this->getPaginatedUsers();

        $view = new ViewModel(array(
            'users' => $this->paginator
                )
        );
        $view->setTemplate("user/user/index");

        $this->setLogMessage("User list accessed");
        $this->html = $view;
        return $this->JSONRespondHtml();
    }

    /**
     * This function edit the users by ID
     * @author PRASA7  <prasanna@thinkcube.com>
     */
    public function editProfileAction() {
        $globaldata = $this->getServiceLocator()->get('config');
        $this->getEvent()->getViewModel()->sidemenu = $globaldata['company_side_menu'];
        $this->getEvent()->getViewModel()->sidemenuselected = 'User';

        $roleRes = $this->CommonTable('User\Model\RoleTable')->fetchAll();

        $roles = array();
        foreach ($roleRes as $role) {
            $roles[$role->roleID] = $role->roleName;
        }

        $config = $this->getServiceLocator()->get('config');
        $country = $config['defaultCountry'];

        $language = $config['countries'][$country];

        $userTypes = [
            1 => "ezBiz System User",
            2 => "POS User"
        ];

        $form = new ProfileEditForm($roles, $language, $userTypes);



        $usersExistingDetails = $this->CommonTable('User\Model\UserTable')->getUser($this->userID);
//        $form->bind($usersExistingDetails);
        //$defaultLanguage->
        $form->get('userID')->setValue($this->userID);
        $form->get('roleID')->setValue($usersExistingDetails->roleID);
        $form->get('userTypeID')->setValue($usersExistingDetails->userTypeID);
        $form->get('userFirstName')->setValue($usersExistingDetails->firstName);
        $form->get('userLastName')->setValue($usersExistingDetails->lastName);
        $form->get('userEmail1')->setValue($usersExistingDetails->email1);
        $form->get('defaultLanguage')->setValue($usersExistingDetails->defaultLanguage);
        $form->get('userPassword')->setValue(NULL);

        $viewModel = new ViewModel(array(
            'form' => $form,
            'username' => $usersExistingDetails->username
        ));

        $viewModel->setTemplate("user/user/user-profile-edit-form");
        return $viewModel;
    }

    //////////////////END GET USER TABLE///////////////////////////

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function gets paginated users
     */
    private function getPaginatedUsers() {
        $this->paginator = $this->CommonTable('User\Model\UserTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    //////////////////END PAGINATED USERS//////////////////////////

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function sends a mail to relavent user with link to reset password
     */
    public function forgotPasswordAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $username = $post['username'];

            if ($username == '' || $username == NULL) {
                return new JsonModel(array(
                    'status' => false,
                    'msg' => 'Please provide a username'
                ));
            } else {
                $userCheck = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
                if ($userCheck) {
                    $generatedRandomToken = substr(md5(uniqid(mt_rand(), true)), 0, 30);
                    $passResetUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/user/resetPassword/?uname=' . $userCheck->userUsername . '&token=' . $generatedRandomToken;
                    $data = array(
                        'userResetPasswordToken' => $generatedRandomToken,
                        'userResetPasswordExpire' => gmdate('Y-m-d H:i:s', strtotime('+60 minutes')));

                    $this->CommonTable('User\Model\UserTable')->updateUser($data, $userCheck->userID);

                    $resetPassEmailView = new ViewModel(array(
                        'name' => $userCheck->userFirstName,
                        'link' => $passResetUrl
                    ));
                    $resetPassEmailView->setTemplate('/core/email/reset-password');
                    $renderedEmailHtml = $this->getServiceLocator()
                            ->get('viewrenderer')
                            ->render($resetPassEmailView);
                    $this->sendEmail($userCheck->userEmail1, 'Reset Your ezBiz Password', $renderedEmailHtml);
                    $this->status = true;
                    $this->msg = "An email with instrunctions to reset your password is sent to your email address - $userCheck->userEmail1";
                    return $this->JSONRespond();
                } else {
                    $this->status = false;
                    $this->msg = 'Username is not valid';
                    return $this->JSONRespond();
                }
            }
        } else {
            $logview = new ViewModel();
            $logview->setTemplate('/user/user/login');
            $logview->setTerminal(TRUE);
            return $logview;
        }
    }

    public function resetPasswordAction() {


        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();

            $userID = $post['userID'];
            $userCheck = $this->CommonTable('User\Model\UserTable')->getUserByID($userID);
            $error = [];

            if (!$userCheck) {
                $this->flashMessenger()->addMessage('Invalid username');
                header("location:/user/login");
                exit();
            }

            if ($userCheck->userResetPasswordToken != $post['token']) {
                $this->flashMessenger()->addMessage('Invalid reset password token');
                header("location:/user/login");
                exit();
            }

            if ($this->getGMTDateTime() > $userCheck->userResetPasswordExpire) {
                $this->flashMessenger()->addMessage('Password reset token expired.!');
                header("location:/user/login");
                exit();
            }

            if ($post['password'] == '' || $post['passwordConfirm'] == '' || $post['password'] != $post['passwordConfirm']) {
                $resetView = new ViewModel(array(
                    'username' => $userCheck->userUsername,
                    'userID' => $userID,
                    'token' => $post['token'],
                    'msg' => 'Both passwords should be matched'
                ));
                $resetView->setTerminal(TRUE);
                if ($userCheck->userFirstLogin) {
                    $resetView->setTemplate('user/user/force-password-reset.phtml');
                }
                return $resetView;
            }

            if (!preg_match('/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,20}$/', $post['password'])) {
                $resetView = new ViewModel(array(
                    'username' => $userCheck->userUsername,
                    'userID' => $userID,
                    'token' => $post['token'],
                    'msg' => $this->getMessage('ERR_USER_PASS_FORMAT')
                ));
                $resetView->setTerminal(TRUE);
                if ($userCheck->userFirstLogin) {
                    $resetView->setTemplate('user/user/force-password-reset.phtml');
                }
                return $resetView;
             }

            $passwordData = explode(':', $userCheck->userPassword);
            $storedPassword = $passwordData[0];
            $password = md5($post['password'] . $passwordData[1]);

            if ($storedPassword == $password) {
                $resetView = new ViewModel(array(
                    'username' => $userCheck->userUsername,
                    'userID' => $userID,
                    'token' => $post['token'],
                    'msg' => 'Please provide a different password other than your old password'
                ));
                $resetView->setTerminal(TRUE);
                if ($userCheck->userFirstLogin) {
                    $resetView->setTemplate('user/user/force-password-reset.phtml');
                }
                return $resetView;
            }

            $passwordSalt = md5(mt_rand());
            $password = md5($post['password'] . $passwordSalt);
            $newPassword = $password . ':' . $passwordSalt;
            $data = array(
                'userPassword' => $newPassword,
                'userResetPasswordToken' => NULL,
                'userResetPasswordExpire' => NULL,
                'userFirstLogin' => 0
            );

            $this->CommonTable('User\Model\UserTable')->updateUser($data, $userID);

            if ($userCheck->userFirstLogin) {
                header("Location: /user/login");
                exit();
            }

            $logView = new ViewModel(array(
                'msg' => 'Password update successfully. Please login to continue',
                'success' => TRUE
            ));
            $logView->setTemplate('/user/user/login');
            $logView->setTerminal(TRUE);
            return $logView;
        }
        else {

            $uname = $this->params()->fromQuery('uname');
            $token = $this->params()->fromQuery('token');

            if ($uname == '' || $uname == NULL || $token == '' || $token == '') {
                $this->flashMessenger()->addMessage("Invalid reset password url");
                header("location:/user/login");
                exit();
            }

            $userCheck = $this->CommonTable('User\Model\UserTable')->getUserByUsername($uname);
            if (!$userCheck) {
                header("location:/user/login");
                exit();
            }

            if ($userCheck->userResetPasswordToken != $token) {
                $this->flashMessenger()->addMessage("Invalid password reset token");
                header("location:/user/login");
                exit();
            }

            if ($this->getGMTDateTime() > $userCheck->userResetPasswordExpire) {
                $this->flashMessenger()->addMessage("Password reset token expired.!");
                header("location:/user/login");
                exit();
            }

            $resetView = new ViewModel(array(
                'username' => $userCheck->userUsername,
                'userID' => $userCheck->userID,
                'token' => $token
            ));

            $resetView->setTerminal(TRUE);
            return $resetView;
        }
    }

    public function loginMobileAction()
    {
        $request = new \Zend\Json\Server\Request\Http;


        $userName = json_decode($request->getRawJson())->username;
        $passWord = json_decode($request->getRawJson())->password;

        if ($userName == '' || $userName == NULL || $passWord == '' || $passWord == NULL)
        {
            $this->status = false;
            $this->msg = 'Both username and password should be entered.';
            return $this->JSONRespond();
        }
        // get user details by user name
        $userCheck = $this->CommonTable('User\Model\UserTable')->getUserByUsername($userName);

        // validate password
        $passwordData = explode(':', $userCheck->userPassword);
        $storedPassword = $passwordData[0];
        $password = md5($passWord . $passwordData[1]);

         if ($storedPassword != $password) {

            $this->status = false;
            $this->msg = 'The username and password you entered is incorrect.';
            return $this->JSONRespond();

        } elseif ($userCheck->userActivated == '0') {
            $this->status = false;
            $this->msg = 'Your account is deactivated. Please contact the Admin.';
            return $this->JSONRespond();

        } else {
            // check company license
            $companyLicence = $this->checkCompanyLicence(1);
            if (!$companyLicence->status) {
                $this->status = false;
                $this->msg = $companyLicence->msg;
                return $this->JSONRespond();
            }

            // check user licence
            $licenseCheck = $this->checkUserLicence($userCheck->userID);
            if (!$licenseCheck->status) {
                $this->status = false;
                $this->msg = $licenseCheck->msg;
                return $this->JSONRespond();
            }

            $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
            $company = $this->CommonTable('CompanyTable')->fetchAll()->current();
            $config = $this->getServiceLocator()->get('config');
            // create jwtToken
            $respond = RestApiRoutes::createJwtToken($config, $userName, 'mobile_dashboard_app_jwt_details', $userCheck->userFirstName, $displaySettings->currencySymbol, $this->getUserLocationNamesForPos($userCheck->userID), $this->getUserDefaultLocationForPos($userCheck->userID), $company, $userCheck->userID);

            $this->status = true;
            $this->msg = 'success login';
            $this->data = $respond;
            return $this->JSONRespond();

        }

    }

    /**
     * This function is used for login the users
     * @author Damith <damith@thinkcube.com>
     * @return boolean
     */
    public function loginAction($triggered = FALSE) {
        $Country = $this->CommonTable('CompanyTable')->fetchAll();
        $currentCountry = $Country->current()->country;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $userAuthentication = FALSE;
            if ($post['username'] == '' || $post['username'] == NULL || $post['password'] == '' || $post['password'] == NULL) {
                $logView = new ViewModel(array(
                    'msg' => 'Both username and password should be entered.',
                    'country' => $currentCountry,
                ));
                $logView->setTemplate('/user/user/login');
                $logView->setTerminal(TRUE);
                return $logView;
            } else {

                $username = $post['username'];

                $config['db']['username'] = $this->user_session->dbname;

                // For demo account, create a new DB from a fresh DB template
                if (getenv("ENV") == "demo" && !$this->user_session->dbname) {

                    $sourceDB = array(
                        'host' => getenv("SOURCEDBHOST"),
                        'user' => getenv("SOURCEDBUSER"),
                        'pass' => getenv("SOURCEDBPASS"),
                        'name' => getenv("SOURCEDBNAME")
                    );

                    $destDB = array(
                        'host' => getenv("DBHOST"),
                        'user' => getenv("DBUSER"),
                        'pass' => getenv("DBPASS"),
                        'name' => getenv("DBNAME")
                    );

                    $connection = mysql_connect($destDB['host'], $destDB['user'], $destDB['pass']);

                    if (!$connection) {
                        die('Error connecting to database.');
                    }

                    // Create database
                    $sql = "CREATE DATABASE {$destDB['name']}";
                    mysql_query($sql, $connection);

                    $sqlpath = '/tmp';

                    // import database
                    // leading space in the shell command to avoid saving the command in history because it includes the password
                    $command = <<<BASH

 mysqldump -u{$sourceDB['user']} -h {$sourceDB['host']} -p'{$sourceDB['pass']}' {$sourceDB['name']} > {$sqlpath}/{$destDB['name']}.sql && whoami &&
 mysql -u{$destDB['user']} -h {$destDB['host']} -p'{$destDB['pass']}' {$destDB['name']} < {$sqlpath}/{$destDB['name']}.sql

BASH;
                    exec("{$command} 2>&1", $output);
                    $this->user_session->dbname = $destDB['name'];
                }

                $userCheck = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);

                if ($userCheck) {
                    if ($userCheck->userPassword == '' || $userCheck->userPassword == NULL) {
                        $generatedRandomToken = substr(md5(uniqid(mt_rand(), true)), 0, 30);
                        $passResetUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/user/resetPassword/?uname=' . $userCheck->userUsername . '&token=' . $generatedRandomToken;
                        $data = array(
                            'userResetPasswordToken' => $generatedRandomToken,
                            'userResetPasswordExpire' => gmdate('Y-m-d H:i:s', strtotime('+60 minutes')));

                        $this->CommonTable('User\Model\UserTable')->updateUser($data, $userCheck->userID);
                        $resetPassEmailView = new ViewModel(array(
                            'name' => $userCheck->userFirstName,
                            'link' => $passResetUrl
                        ));
                        $resetPassEmailView->setTemplate('/core/email/reset-password');
                        $renderedEmailHtml = $this->getServiceLocator()
                                ->get('viewrenderer')
                                ->render($resetPassEmailView);
                        $this->sendEmail($userCheck->userEmail1, 'Reset Your ezBiz Password', $renderedEmailHtml);
                        $logView = new ViewModel(array(
                            'msg' => "An email with instructions to reset your password has been sent to your email address - $userCheck->userEmail1",
                            'success' => TRUE,
                            'country' => $currentCountry,
                        ));
                        $logView->setTemplate('/user/user/login');
                        $logView->setTerminal(TRUE);
                        return $logView;
                    }
                    $passwordData = explode(':', $userCheck->userPassword);
                    $storedPassword = $passwordData[0];
                    $password = md5($post['password'] . $passwordData[1]);

                    if ($storedPassword != $password) {
                        $logView = new ViewModel(array(
                            'msg' => 'The username and password you entered is incorrect.',
                            'country' => $currentCountry,
                        ));
                        $logView->setTemplate('/user/user/login');
                        $logView->setTerminal(TRUE);
                        return $logView;
                    } elseif ($userCheck->userActivated == '0') {
                        $logView = new ViewModel(array(
                            'msg' => 'Your account is deactivated. Please contact the Admin.',
                            'country' => $currentCountry,
                        ));
                        $logView->setTemplate('/user/user/login');
                        $logView->setTerminal(TRUE);
                        return $logView;
                    } else {


                        //Company license check
                        $companyLicence = $this->checkCompanyLicence(1);
                        if (!$companyLicence->status) {
                            $logView = new ViewModel(array(
                                'msg' => $companyLicence->msg
                            ));
                            $logView->setTemplate('/user/user/login');
                            $logView->setTerminal(TRUE);
                            return $logView;
                        }

                        //check user license
                        //comment-disable user licence check

                        $licenseCheck = $this->checkUserLicence($userCheck->userID);
                        if (!$licenseCheck->status) {
                            $logView = new ViewModel(array(
                                'msg' => $licenseCheck->msg
                            ));
                            $logView->setTemplate('/user/user/login');
                            $logView->setTerminal(TRUE);
                            return $logView;
                        } else {
                            $userAuthentication = TRUE;
                        }
                    }
                } else {
                    $logView = new ViewModel(array(
                        'msg' => 'The username and password you entered is incorrect.',
                        'country' => $currentCountry,
                    ));
                    $logView->setTemplate('/user/user/login');
                    $logView->setTerminal(TRUE);
                    return $logView;
                }
            }
            //check single user login
            $singleInstanceCheckResult = $this->isSingleInstance($userCheck->userID, $userCheck->userUsername);

            if (!$singleInstanceCheckResult && !isset($_SESSION['forceLogin'])) {
                $_SESSION['forceLogin'] = TRUE;
                $logView = new ViewModel(array(
                    'msg' => "You seem to have logged into this account using another device or browser.\n If you wish to continue logging in using this device, enter your username and password again to sign in"
                ));
                $logView->setTemplate('/user/user/login');
                $logView->setTerminal(TRUE);
                return $logView;
            }

            //create Session
            $this->createSession($userCheck);

            $uri = $this->getRequest()->getUri();

            $returnUrl = $post['returnTo'];
            $returnTo = $uri->getScheme() . '://' . $uri->getHost() . '/' . trim($returnUrl, '/');

            $this->insertUserLogin($userCheck->userID, $userCheck->userUsername);

            $this->setLogMessage("Logged in");
            $log = $this->getServiceLocator()->get('Log');
            $log->saveLog();
//            $log = $e->getApplication()->getServiceManager()->get('Log');
            //create remember me token
            if (isset($post['rememberme']) && $post['rememberme'] == TRUE) {
                setcookie($this->user_session->username . '_remember', $this->user_session->sessionToken, time() + (10 * 365 * 24 * 60 * 60), "/");
                setcookie('user', $this->user_session->username, time() + (10 * 365 * 24 * 60 * 60), "/");
            } else {
                if (isset($_COOKIE[$this->user_session->username . '_remember'])) {
                    unset($_COOKIE[$this->user_session->username . '_remember']);
                    setcookie($this->user_session->username . '_remember', '', time() - 3600);
                }
                if (isset($_COOKIE['user'])) {
                    unset($_COOKIE['user']);
                    setcookie('user', '', time() - 3600);
                }
            }


            //When user login 1st time, wizard is appeared
            $wizardState = $this->getWizardUrl();

            if ($wizardState == false) {
                header("location:" . $returnTo);
            } else {
                header("location:/wizard/" . $wizardState);
            }

            exit();
        } else {

            //check remember me
            if (isset($_COOKIE['user'])) {
                if (isset($_COOKIE[$_COOKIE['user'] . '_remember'])) {
                    $userRemCheck = $this->CommonTable('User\Model\UserTable')->getUserByUsername($_COOKIE['user']);
                    if ($userRemCheck) {
                        $userLoggedDetails = $this->getUserLoginTable()->getUserloginByUserID($userRemCheck->userID);
                        if (isset($userLoggedDetails[0])) {
                            if ($_COOKIE[$_COOKIE['user'] . '_remember'] == $userLoggedDetails[0]['sessionToken']) {
                                $this->createSession($userRemCheck);
                                $this->insertUserLogin($userRemCheck->userID, $userRemCheck->userUsername);
                                setcookie($this->user_session->username . '_remember', $this->user_session->sessionToken, time() + (10 * 365 * 24 * 60 * 60), "/");
                                //set logged in message
                                $this->setLogMessage("Logged in");
                                $log = $this->getServiceLocator()->get('Log');
                                $log->saveLog();
                                if ($triggered) {
                                    $returnUrl = '/';
                                } else {
                                    $returnUrl = urldecode($this->params()->fromRoute('param1'));
                                }
                                $uri = $this->getRequest()->getUri();
                                $returnTo = $uri->getScheme() . '://' . $uri->getHost() . '/' . trim($returnUrl, '/');
                                header("location:" . $returnTo);
                                exit();
                            }
                        } else {
                            unset($_COOKIE[$_COOKIE['user'] . '_remember']);
                            setcookie($_COOKIE[$_COOKIE['user'] . '_remember'], '', time() - 3600);
                            unset($_COOKIE['user']);
                            setcookie('user', '', time() - 3600);
                        }
                    }
                }
            }

            if ($triggered) {
                $returnUrl = '/';
            } else {
                $returnUrl = urldecode($this->params()->fromRoute('param1'));
            }

            if (isset($this->userID)) {
                header("location:/");
                exit();
            } else {
                $msg = NULL;
                $flashMessages = $this->flashMessenger()->getMessages();
                if (isset($flashMessages)) {
                    $msg = $flashMessages[0];
                }
                $loginView = new ViewModel(array('msg' => $msg, 'returnTo' => $returnUrl, 'country' => $currentCountry,));
                $loginView->setTemplate('/user/user/login');
                $loginView->setTerminal(TRUE);
                return $loginView;
            }
        }
    }

    /**
     * @author Damith Thamara<damith@thinkcube.com>
     * Create User Session
     * @param Object $Usercheck
     * E.g getUserByUsername function
     */
    public function createSession($userCheck) {
        $config = $this->getServiceLocator()->get('config');
        $company = $this->CommonTable('CompanyTable')->fetchAll()->current();
        $languages = $this->getLanguageForSelectedCountry();
        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        $jobSettings = (object) $this->CommonTable('Jobs\Model\JobGeneralSettingsTable')->fetchAll();
        $dateCheck = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID('-','Universal Date Restriction Override Capability',$userCheck->userID);


        $jobAdminFlag = true;
        $jobStoreKeeperFlag = true;
        // $getJobUserRole = $this->CommonTable('Jobs\Model\JobUserRoleTable')->getJobUserRoleByUserID($userCheck->userID);
        // foreach ($getJobUserRole as $key => $value) {
        //     if ($value['jobRoleID'] == 1) {
        //         $jobAdminFlag = true;
        //     }
        //     if ($value['jobRoleID'] == 2) {
        //         $jobStoreKeeperFlag = true;
        //     }
        // }

        $this->user_session->UniversalDateRestrictionOverride = ($dateCheck==1)?true:false;
        $this->user_session->userID = $userCheck->userID;
        $this->user_session->username = $userCheck->userUsername;
        $this->user_session->fullName = $userCheck->userFirstName;
        $this->user_session->roleID = $userCheck->roleID;
        $this->user_session->companyDetails = $this->CommonTable('CompanyTable')->getCompany(1);
        $this->user_session->displaySettings = $displaySettings;
        $this->user_session->jobSettings = $jobSettings;
        $this->user_session->jobAdmin = $jobAdminFlag;
        $this->user_session->jobStoreKeeper = $jobStoreKeeperFlag;
        $this->user_session->lang = $userCheck->defaultLanguage;
        $this->user_session->userActiveLocation = $this->getUserDefaultLocation();
        $this->user_session->userLocations = $this->getUserLocationNames();
        $this->user_session->userAllLocations = $this->user_session->userLocations;
        $this->user_session->companyCurrencySymbol = $displaySettings->currencySymbol;
        $this->user_session->companyCurrencyId = $displaySettings->currencyID;
        $this->user_session->companyCurrencyName = $displaySettings->currencyName;
        $this->user_session->timeZone = $displaySettings->timeZone;
        $this->user_session->jobBusinessType = $displaySettings->jobBusinessType;
        $this->user_session->packageID = $displaySettings->packageID;
        $this->user_session->jobModuleEnabled = $displaySettings->jobModuleEnabled;
        $this->user_session->crmModuleEnabled = $displaySettings->crmModuleEnabled;
        $this->user_session->productWiseSalesPersonEnable = $displaySettings->productWiseSalesPersonEnable;
        $this->user_session->lastLogin = $this->getGMTDateTime();
        $this->user_session->userSetCountry = $company->country;
        $this->user_session->userLanguages = $languages;
        $this->user_session->subdomain = $this->getSubdomain();
        $this->user_session->wizardComplete = ($company->wizardState == $config['wizardCompleteState']);
        $this->user_session->useAccounting =$company->companyUseAccounting;
    }

    /**
     * @auther sharmilan <sharmilan@thinkcube.com>]
     * Running only the login
     */
    public function isSingleInstance($userID, $username) {
        $user = $this->getUserLoginTable()->getUserloginByUserID($userID);
        if (isset($user[0])) {
            $currentDateTime = new \DateTime($this->getGMTDateTime());
            $lastLoginTime = new \DateTime($user[0]['lastLogin']);

            $minsInterval = $currentDateTime->diff($lastLoginTime)->format('%H:%i:%s');
            $config = $this->getServiceLocator()->get('config');


            if (isset($_COOKIE[$username])) {
                if ($_COOKIE[$username] == $user[0]['sessionToken']) {
                    return TRUE;
                } else {
                    if (strtotime($minsInterval) < strtotime($config['timeInterval']['logoutInterval'])) {
                        unset($_POST);
                        unset($_REQUEST);
                        return FALSE;
                    }
                }
            } else {
                if (strtotime($minsInterval) < strtotime($config['timeInterval']['logoutInterval'])) {
                    unset($_POST);
                    unset($_REQUEST);
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * running all the routing
     * TODO: refactor
     */
    public function checkAndUpdateSingleInstace() {

        $userID = $this->user_session->userID;
        $currentDateTime = new \DateTime($this->getGMTDateTime());
        $sessionTime = new \DateTime($this->user_session->lastLogin);
        $minsInterval = $currentDateTime->diff($sessionTime)->format('%H:%i:%s');
        $sessionToken = $this->user_session->sessionToken;
        $user = $this->getUserLoginTable()->getUserloginByUserID($userID);

        $config = $this->getServiceLocator()->get('config');

        if (strtotime($minsInterval) >= strtotime($config['timeInterval']['logoutInterval'])) {
            if (isset($user[0])) {
                if ($user[0]['sessionToken'] != $sessionToken) {
                    $this->logoutAction();
                    exit();
                } else {
                    $userLogin = new UserLogin();
                    $data = array('userLoginID' => $user[0]['userLoginID'],
                        'lastLogin' => $this->getGMTDateTime(),
                        'sessionToken' => $sessionToken,
                    );
                    $userLogin->exchangeArray($data);
                    $this->getUserLoginTable()->updateLastLogin($userLogin);
                    $this->user_session->lastLogin = $this->getGMTDateTime();
                }
            }
        } else {
            if (isset($user[0])) {
                if ($user[0]['sessionToken'] != $sessionToken) {
                    $this->logoutAction();
                    exit();
                } else if (strtotime($minsInterval) >= strtotime($config['timeInterval']['updateInterval'])) {

                    setcookie($this->username, $sessionToken, time() + $config['timeInterval']['cookiePeriod'], "/");
                    $userLogin = new UserLogin();
                    $data = array('userLoginID' => $user[0]['userLoginID'],
                        'lastLogin' => $this->getGMTDateTime(),
                        'sessionToken' => $sessionToken,
                    );
                    $userLogin->exchangeArray($data);
                    $this->getUserLoginTable()->updateLastLogin($userLogin);
                    $this->user_session->lastLogin = $this->getGMTDateTime();
                }
            } else {
                $this->logoutAction();
                exit();
            }
        }
    }

    /**
     * store to the userLogin table when loggin
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $userID
     * @param type $userName
     */
    public function insertUserLogin($userID, $userName) {

        $sessionToken = crypt($userName, $this->_generateRandomString());
        $config = $this->getServiceLocator()->get('config');
        setcookie($userName, $sessionToken, time() + $config['timeInterval']['cookiePeriod'], "/");
        $this->user_session->sessionToken = $sessionToken;
        $data = array('userID' => $userID, 'sessionToken' => $sessionToken, 'lastLogin' => $this->getGMTDateTime());
        $userLogin = new UserLogin();
        $userLogin->exchangeArray($data);
        $this->getUserLoginTable()->deleteUserLoginByUserID($userID);
        $this->getUserLoginTable()->savaUserLogin($userLogin);
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $companyID
     * @return \Zend\View\Model\JsonModel
     */
    public function checkCompanyLicence($companyID) {
        $companyDetails = $this->CommonTable('CompanyTable')->getCompany($companyID);
        $companylicenceExpireDate = $companyDetails->companyLicenseExpireDate;
        $companyGracePeriod = $companyDetails->companyGracePeriod;
        $licenseExpireDate = date('Y-m-d H:i:s', strtotime($companylicenceExpireDate . '+' . $companyGracePeriod . 'days'));
        if ($this->getGMTDateTime() > $licenseExpireDate) {
            return new JsonModel(array('status' => FALSE, 'msg' => 'Sorry, Your Company license has been expired. Please contact our Help Desk via info@ezbizapp.com or +94 114331188'));
        } else {
            return new JsonModel(array('status' => TRUE, 'msg' => ''));
        }
    }

    /**
     * This function logouts the user
     * @author Damith  <damith@thinkcube.com>
     * @return type
     */
    public function logoutAction() {

        $config = $this->getServiceLocator()->get('config');
        $ssoService = $config['ssoService'];

        $user_session = $this->user_session;

        $uri = $this->getRequest()->getUri();

        switch ($ssoService) {
            case "demo":
                $user_session->userID = NULL;
                $user_session->getManager()->destroy();

                $view = new ViewModel();
                $view->setTemplate("user/user/demo_logout");
                return $view;
                break;

            default :
                $this->setLogMessage("Logout");
                $log = $this->getServiceLocator()->get('Log');
                $log->saveLog();
                $this->getUserLoginTable()->deleteUserLogin($user_session->userID, $user_session->sessionToken);
                $user_session->userID = NULL;
                $user_session->getManager()->destroy();
                $this->loginAction(TRUE);
                break;
        }
    }

    ////////////////////////END USER LOGIN/////////////////////////

    /**
     * This function returns the auth returns the auth error
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return \Zend\View\Model\ViewModel
     */
    public function autherrorAction() {
        $view = new ViewModel();
        return $view;
    }

    /**
     * This function returns all user logger details
     * @author PRASA7  <prasanna@thinkcube.com>
     */
    public function getAllLoggerDetailsAction() {
        $globaldata = $this->getServiceLocator()->get('config');
        $this->getEvent()->getViewModel()->sidemenu = $globaldata['company_side_menu'];
        $this->getEvent()->getViewModel()->sidemenuselected = 'Audit Trail';

        $allLogs = $this->CommonTable('User\Model\LoggerTable')->fetchAll(true);
        $allLogs->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $allLogs->setItemCountPerPage(10);

        foreach ($allLogs as $key => $log) {
            $logs[$key] = $log;
            $logs[$key]->userDetails = $this->CommonTable('User\Model\UserTable')->getUser($log->userID);
        }

        $this->setLogMessage("Audit Trail accessed");
        return new ViewModel(array(
            'logs' => $logs,
            'allLogs' => $allLogs
        ));
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param string $username
     * This function check user licence
     * @return JSONobject
     */
    public function checkUserLicence($userID) {
        $license = $this->getLicenceTable()->getLicenseDetailsByUserID($userID);
        if ($license) {
            $gracePeriod = $this->CommonTable('CompanyTable')->getUserGracePeriod()['userGracePeriod'];
            //if the license is free
            if (strtotime($license['licenceExpireDate']) < 0) {
                return new JsonModel(array('status' => TRUE, 'msg' => ''));
            }
            $licenseExpireDate = date('Y-m-d H:i:s', strtotime($license['licenceExpireDate'] . '+' . $gracePeriod . 'days'));
            if ($this->getGMTDateTime() > $licenseExpireDate) {
                return new JsonModel(array('status' => FALSE, 'msg' => 'Sorry, Your license has been expired. Please contact your admin.'));
            } else {
                return new JsonModel(array('status' => TRUE, 'msg' => ''));
            }
        } else {
            return new JsonModel(array('status' => FALSE, 'msg' => 'Sorry, You have not been assigned a license. Please contact your admin.'));
        }
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function returns logger table
     * @return logger table
     */
    public function getLoggerTable() {
        if (!$this->loggerTable) {
            $sm = $this->getServiceLocator();
            $this->loggerTable = $sm->get('User\Model\LoggerTable');
        }
        return $this->loggerTable;
    }

    /**
     * This function returns user role permissions
     * @author Damith Thamara  <damith@thinkcube.com>
     * @return \Zend\View\Model\ViewModel
     */
    public function getUserRolePermissionsAction() {
        $this->getSideAndUpperMenus('User', 'Edit User Roles');
        $this->getViewHelper('HeadScript')->prependFile('/js/user/user.permission.js');
        $currentRoleID = $this->user_session->roleID;
        $currentRoleName;
        $groupedRoleFeatures = $this->CommonTable('User\Model\RoleFeatureTable')->getGroupedRoleFeatures();
        $rolefeatureDetails = array();
        foreach ($groupedRoleFeatures as $roleFeature) {
            $featureDetails = (isset($rolefeatureDetails[$roleFeature['roleName']]) ? $rolefeatureDetails[$roleFeature['roleName']] : array());
            $featureModuleSet = (isset($featureDetails[$roleFeature['moduleName']]) ? $featureDetails[$roleFeature['moduleName']] : array());
            $featureCategorySet = (isset($featureModuleSet[$roleFeature['featureCategory']]) ? $featureModuleSet[$roleFeature['featureCategory']] : array());
            if ($roleFeature['roleID'] == $currentRoleID) {
                $currentRoleName = $roleFeature['roleName'];
            }
            $featureData = array(
                'roleFeatureID' => $roleFeature['roleFeatureID'],
                'roleID' => $roleFeature['roleID'],
                'roleFeatureEnabled' => $roleFeature['roleFeatureEnabled'],
                'featureID' => $roleFeature['featureID'],
                'featureName' => $roleFeature['featureName'],
                'featureController' => $roleFeature['featureController'],
                'featureAction' => $roleFeature['featureAction'],
                'featureCategory' => $roleFeature['featureCategory'],
                'featureType' => $roleFeature['featureType'],
                'moduleID' => $roleFeature['moduleID'],
                'moduleName' => $roleFeature['moduleName'],
            );
            $featureCategorySet[$roleFeature['featureID']] = $featureData;
            $featureModuleSet[$roleFeature['featureCategory']] = $featureCategorySet;
            $featureDetails[$roleFeature['moduleName']] = $featureModuleSet;
            $rolefeatureDetails[$roleFeature['roleName']] = $featureDetails;
        }
        $view = new ViewModel(array(
            'roleFeatureDeatails' => $rolefeatureDetails,
            'currentRoleName' => $currentRoleName
        ));
        return $view;
    }

    /**
     * This function adds role permissions
     * @author Damith Thamara
     * @return \Zend\View\Model\ViewModel
     */
    public function addUserRolePermissionsAction() {
        $this->getSideAndUpperMenus('User', 'Add User Roles');
        $this->getViewHelper('HeadScript')->prependFile('/js/user/user.permission.js');

        $featureSet = $this->CommonTable('User\Model\FeatureTable')->getGroupedFeatures();
        $featureDetails = array();
        foreach ($featureSet as $feature) {
            if ($this->jobModuleEnabled == '0' && $this->crmModuleEnabled == "0") {
                if ($feature['moduleID'] != 13 && $feature['moduleID'] != 10) {
                    $featureModuleSet = (isset($featureDetails[$feature['moduleName']]) ? $featureDetails[$feature['moduleName']] : array());
                    $featureCategorySet = (isset($featureModuleSet[$feature['featureCategory']]) ? $featureModuleSet[$feature['featureCategory']] : array());
                    $featureData = array(
                        'featureID' => $feature['featureID'],
                        'featureName' => $feature['featureName'],
                        'featureController' => $feature['featureController'],
                        'featureAction' => $feature['featureAction'],
                        'featureCategory' => $feature['featureCategory'],
                        'featureType' => $feature['featureType'],
                        'moduleID' => $feature['moduleID'],
                        'moduleName' => $feature['moduleName'],
                    );
                    $featureCategorySet[$feature['featureID']] = $featureData;
                    $featureModuleSet[$feature['featureCategory']] = $featureCategorySet;
                    $featureDetails[$feature['moduleName']] = $featureModuleSet;
                }
            } else if ($this->jobModuleEnabled == '0') {
                if ($feature['moduleID'] != 13) {
                    $featureModuleSet = (isset($featureDetails[$feature['moduleName']]) ? $featureDetails[$feature['moduleName']] : array());
                    $featureCategorySet = (isset($featureModuleSet[$feature['featureCategory']]) ? $featureModuleSet[$feature['featureCategory']] : array());
                    $featureData = array(
                        'featureID' => $feature['featureID'],
                        'featureName' => $feature['featureName'],
                        'featureController' => $feature['featureController'],
                        'featureAction' => $feature['featureAction'],
                        'featureCategory' => $feature['featureCategory'],
                        'featureType' => $feature['featureType'],
                        'moduleID' => $feature['moduleID'],
                        'moduleName' => $feature['moduleName'],
                    );
                    $featureCategorySet[$feature['featureID']] = $featureData;
                    $featureModuleSet[$feature['featureCategory']] = $featureCategorySet;
                    $featureDetails[$feature['moduleName']] = $featureModuleSet;
                }
            } else if ($this->crmModuleEnabled == "0") {
                if ($feature['moduleID'] != 10) {
                    $featureModuleSet = (isset($featureDetails[$feature['moduleName']]) ? $featureDetails[$feature['moduleName']] : array());
                    $featureCategorySet = (isset($featureModuleSet[$feature['featureCategory']]) ? $featureModuleSet[$feature['featureCategory']] : array());
                    $featureData = array(
                        'featureID' => $feature['featureID'],
                        'featureName' => $feature['featureName'],
                        'featureController' => $feature['featureController'],
                        'featureAction' => $feature['featureAction'],
                        'featureCategory' => $feature['featureCategory'],
                        'featureType' => $feature['featureType'],
                        'moduleID' => $feature['moduleID'],
                        'moduleName' => $feature['moduleName'],
                    );
                    $featureCategorySet[$feature['featureID']] = $featureData;
                    $featureModuleSet[$feature['featureCategory']] = $featureCategorySet;
                    $featureDetails[$feature['moduleName']] = $featureModuleSet;
                }
            } else {
                $featureModuleSet = (isset($featureDetails[$feature['moduleName']]) ? $featureDetails[$feature['moduleName']] : array());
                $featureCategorySet = (isset($featureModuleSet[$feature['featureCategory']]) ? $featureModuleSet[$feature['featureCategory']] : array());
                $featureData = array(
                    'featureID' => $feature['featureID'],
                    'featureName' => $feature['featureName'],
                    'featureController' => $feature['featureController'],
                    'featureAction' => $feature['featureAction'],
                    'featureCategory' => $feature['featureCategory'],
                    'featureType' => $feature['featureType'],
                    'moduleID' => $feature['moduleID'],
                    'moduleName' => $feature['moduleName'],
                );
                $featureCategorySet[$feature['featureID']] = $featureData;
                $featureModuleSet[$feature['featureCategory']] = $featureCategorySet;
                $featureDetails[$feature['moduleName']] = $featureModuleSet;
            }
        } 

        $view = new ViewModel(array(
            'featureDetails' => $featureDetails,
        ));
        return $view;
    }

    /**
     * This functionn get enable feature names by ID
     * @param type $roleID
     * @return type
     */
    private function getEnabledFeatureNamesByRoleID($roleID) {
        foreach ($this->CommonTable('User\Model\RoleFeatureTable')->getEnablesFeatureIDs($roleID) as $featureID) {
            $featureNames[] = $this->CommonTable('User\Model\FeatureTable')->getFeaturenameByFeatureID($featureID);
        }

        return $featureNames;
    }

    /**
     * This function gets all roles from database
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return type
     */
    private function getAllRoles() {
        $res = $this->CommonTable('User\Model\RoleTable')->fetchAll();
        while ($row = $res->current())
            $roles[] = $row;
        return $roles;
    }

    /**
     * This function returns all features from database
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return type
     */
    private function getAllFeatures() {
        $res = $this->CommonTable('User\Model\FeatureTable')->fetchAll();
        while ($row = $res->current()) {
            $roles[] = $row;
        }
        return $roles;
    }

    /**
     * This function returns all role features from database
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return type
     */
    private function getAllRoleFeatures() {
        $res = $this->CommonTable('User\Model\RoleFeatureTable')->fetchAll();
        while ($row = $res->current()) {
            $roles[] = $row;
        }
        return $roles;
    }

    public function feedBackAction() {
        return new ViewModel();
    }

    public function getUserLoginTable() {
        if (!$this->userLoginTable) {
            $sm = $this->getServiceLocator();
            $this->userLoginTable = $sm->get('User\Model\UserLoginTable');
        }
        return $this->userLoginTable;
    }

    /**
     * This function changes user language
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return type
     */
    public function changeLanguageAction() {
        $config = $this->getServiceLocator()->get('config');
        $this->user_session->lang = $this->params()->fromRoute('param1', $config['defaultLanguage']);
        return $this->redirect()->toUrl($this->params()->fromQuery('returnTo'));
    }

    /**
     * This function changes user active location
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return type
     */
    public function changeActiveLocationAction() {
        $defaultLocation = $this->getUserDefaultLocation();
        $existingLocationID = $this->params()->fromRoute('param1', $defaultLocation['locationID']);
        $sm = $this->getServiceLocator();
        $locationTable = $sm->get('Core\Model\LocationTable');

        $this->user_session->userActiveLocation = (array) $locationTable->getLocationByID($existingLocationID);
        $this->user_session->userLocations = $this->getUserLocationNames($existingLocationID);

        $returnRoute = $this->params()->fromQuery('returnTo');
        $newRoute =  explode('/', $returnRoute);
        $controAndAction = $newRoute[1].'/'.$newRoute[2];
        //use to rederect 'list' page when location change at edit page
        switch ($controAndAction) {
            case 'invoice/edit':
                $returnRoute = '/invoice/view';
                break;
            case 'quotation/Edit':
                $returnRoute = '/quotation/list';
                break;
            case 'salesOrders/edit':
                $returnRoute = '/salesOrders/list';
                break;
            case 'delivery-note/edit':
                $returnRoute = '/delivery-note/view';
                break;
            case 'inquiry-log/edit':
                $returnRoute = '/inquiry-log/list';
                break;
            case 'promotions/edit':
                $returnRoute = '/promotions/list';
                break;
            case 'bom/edit':
                $returnRoute = '/bom/list';
                break;
            case 'project/edit':
                $returnRoute = '/project/list';
                break;
            case 'job/edit':
                $returnRoute = '/job/list';
                break;
            case 'activity/edit':
                $returnRoute = '/activity/list';
                break;
            case 'product/edit':
                $returnRoute = '/product/list';
                break;
            default:
                $returnRoute = $returnRoute;
                break;
        }

        return $this->redirect()->toUrl($returnRoute);
    }

    /**
     * This function returns user location names without the active location
     * @author PRASA7  <prasanna@thinkcube.com>
     * @param type $activeLocationID
     * @return type
     */
    public function getUserLocationNames($activeLocationID = null) {
        $sm = $this->getServiceLocator();
        $locationUserTable = $sm->get('Core\Model\UserLocationTable');

        $locations = $locationUserTable->getUserLocationsByID($this->user_session->userID);

        if ($activeLocationID == null)
            $activeLocationID = $this->user_session->userActiveLocation['locationID'];

//        unset($locations[$activeLocationID]);

        return $locations;
    }

    /**
     * This function returns all user locations
     * @return type array
     */
    public function getAllUserLocationNames() {
        $sm = $this->getServiceLocator();
        $locationUserTable = $sm->get('Core\Model\UserLocationTable');

        $locations = $locationUserTable->getUserLocationsByID($this->user_session->userID);

        return $locations;
    }

    /**
     * This function returns user default location
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return type
     */
    public function getUserDefaultLocation() {
        return $this->CommonTable('User\Model\UserTable')->getUserDefaultLocationByUserID($this->user_session->userID);
    }

    /**
     * This function updates user locations
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return type
     */
    public function updateLocationsAction() {

        $request = $this->getRequest();

        if ($request->isPost()) {
            $checkLocation = $this->CommonTable('Settings\Model\LocationTable')->isActiveLocation($request->getPost('user')['defautLocation']);

            if ($checkLocation) {
                $res = $this->CommonTable('User\Model\UserTable')->updateUserDefaultLocation($request->getPost('user'));

                //Allocate locations
                $sm = $this->getServiceLocator();
                $locationUserTable = $sm->get('Core\Model\UserLocationTable');

                $existingLocations = $locationUserTable->getUserLocationsByID($request->getPost('user')['userID']);

                $existingLocationKeys = array();

                foreach ($existingLocations as $key => $value) {
                    if (!in_array($key, $request->getPost('location'))) {
                        $res2 = $locationUserTable->deleteUserLocation($key, $request->getPost('user')['userID']);
                    }
                    array_push($existingLocationKeys, $key);
                }

                $newUserLocations = array_diff($request->getPost('location'), $existingLocationKeys);

                foreach ($newUserLocations as $value2) {
                    $res2 = $locationUserTable->insertUserLocation($value2, $request->getPost('user')['userID']);
                }

                if ($res == 1 || $res2 == 1) {
                    $this->setLogMessage("Added successfully");
                    $msg = "User locations updated successfully.Please re-login to ensure the change in user location."; //$this -> getMessage('SUCC_USERCONTRO_USERLOCAT_UPDATE');
                    $status = TRUE;
                } else {
                    $this->setLogMessage("Update failed");
                    $msg = "User locations update faild"; //$this -> getMessage('ERR_USERCONTRO_USERLOCAT_UPDATE');
                    $status = FALSE;
                }
            } else {
                $msg = "The default location that you have selected is in inactive state. Please select an active location as the default location.";
                $status = FALSE;
            }
        }

        $this->getPaginatedUsers();

        $view = new ViewModel(array(
            'users' => $this->paginator
                )
        );
        $view->setTemplate("user/user/index");

        $this->setLogMessage("User list accessed");
        $this->html = $view;
        $this->status = $status;
        $this->msg = $msg;
        return $this->JSONRespondHtml();
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * getting wizard URL which one should want to link when user login first time
     * @return string|boolean
     */
    private function getWizardUrl() {
        //check whether wizard is completed
        $company = $this->CommonTable('CompanyTable')->fetchAll();
        $wState = $company->current();

        if ($wState == false) {
            $wizardState = 1;
        } else {
            $wizardState = ($wState->wizardState) ? : 1;
        }

        //get relevant url from wizardState array on ezbiz.config.php
        $wizardToUrl = $this->getServiceLocator()->get('config')['wizardState'][$wizardState];

        //if $wizardState given Url,return that URL
        if ($wizardToUrl) {
            return $wizardToUrl;
        } else {
            return FALSE;
        }
    }

    /**
     * view the manage license
     * @return \Zend\View\Model\ViewModel
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function manageLicenseAction() {
        $this->getSideAndUpperMenus('User', 'Manage Licenses');

        $users = $this->CommonTable('User\Model\UserTable')->getActiveUsers();
        $assignedLicenses = $this->CommonTable('Core\Model\LicenceTable')->getAssignedLicence();
        $totalNoOfLicense = count($this->CommonTable('Core\Model\LicenceTable')->fetchAll());
        $userlist = array();
        foreach ($users as $user) {
            $temp = array(
                'username' => $user['userUsername'],
                'firstname' => $user['userFirstName'],
                'lastname' => $user['userLastName']
            );
            foreach ($assignedLicenses as $assignedLicense) {
                if ($user['userID'] == $assignedLicense['userID']) {
                    $temp['licenseID'] = $assignedLicense['licenceID'];
                    if (strtotime($assignedLicense['licenceExpireDate']) < 0) {
                        $temp['licenseName'] = $assignedLicense['licenceName'] . ' - Free License';
                    } else {
                        $temp['licenseName'] = $assignedLicense['licenceName'] . ' - ' . date('Y-m-d', strtotime($assignedLicense['licenceExpireDate']));
                    }
                }
            }
            $userlist[$user['userID']] = $temp;
        }

        $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyLicence();
        $licenses = array();
        foreach ($emptyLicenses as $emptyLicense) {
            if (strtotime($emptyLicense['licenceExpireDate']) < 0) {
                $licenses[$emptyLicense['licenceID']] = $emptyLicense['licenceName'] . ' - Free License';
            } else {
                $licenses[$emptyLicense['licenceID']] = $emptyLicense['licenceName'] . '  ' . date('Y-m-d', strtotime($emptyLicense['licenceExpireDate']));
            }
        }

        $companyDetails = $this->CommonTable('CompanyTable')->getCompany(1);
        $companyLicenseDate = date('Y-m-d', strtotime($companyDetails->companyLicenseExpireDate));
        $view = new ViewModel(array(
            'userlist' => $userlist,
            'licenselist' => $licenses,
            'noOfEmptyLicense' => count($emptyLicenses),
            'noOfUser' => count($users),
            'noOfAssignedUser' => count($assignedLicenses),
            'totalNoOfLicense' => $totalNoOfLicense,
            'companyLicenseExpireDate' => $companyLicenseDate
        ));
        $this->getViewHelper('HeadScript')->prependFile('/js/user/manage-license.js');
        return $view;
    }

    /**
     * accessing from manage license drag and drop
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function updateUserLicenseAction() {
        $request = $this->getRequest();
        $post = $request->getPost();
        if (isset($post['userID']) && isset($post['licenseID'])) {
            if ($this->user_session->roleID != 1) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_USERCONTRO_USERLICENSE_PERMISSION');
                return $this->JSONRespondHtml();
            }
            $deletedLicense = $this->CommonTable('Core\Model\LicenceTable')->makeEmptyLicenceByUserID($post['userID']);
            $updatedLicense = $this->CommonTable('Core\Model\LicenceTable')->assignLicenceForUserID($post['licenseID'], $post['userID']);
            if ($updatedLicense) {
                $assignedLicenses = $this->CommonTable('Core\Model\LicenceTable')->getAssignedLicence();
                $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyLicence();
                $this->data = array(
                    'noOfEmptyLicense' => count($emptyLicenses),
                    'noOfAssignedUser' => count($assignedLicenses),
                );
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_USERCONTRO_USERLICENSE_UPDATE');
                return $this->JSONRespondHtml();
            }
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_USERCONTRO_USERLICENSE_UPDATE');
        return $this->JSONRespond();
    }

    /**
     * remove user license from manage license
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function removeUserLicenseAction() {
        $request = $this->getRequest();
        $post = $request->getPost();
        if (isset($post['userID'])) {
            if ($this->user_session->roleID != 1) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_USERCONTRO_USERLICENSE_PERMISSION');
                return $this->JSONRespondHtml();
            }
            if ($post['userID'] == $this->userID) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_USERCONTRO_USERLICENSE_DELETE_OWN');
                return $this->JSONRespondHtml();
            }
            $modifiedLicense = $this->CommonTable('Core\Model\LicenceTable')->makeEmptyLicenceByUserID($post['userID']);
            if ($modifiedLicense) {
                $assignedLicenses = $this->CommonTable('Core\Model\LicenceTable')->getAssignedLicence();
                $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyLicence();
                $this->data = array(
                    'noOfEmptyLicense' => count($emptyLicenses),
                    'noOfAssignedUser' => count($assignedLicenses),
                );
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_USERCONTRO_USERLICENSE_DELETE');
                return $this->JSONRespondHtml();
            }
            $this->status = false;
            $this->msg = $this->getMessage('ERR_USERCONTRO_USERLICENSE_DELETE');
            return $this->JSONRespondHtml();
        }
    }

    /**
     * remove all User licenses from manage license
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function smartRemoveUserLicenseAction() {
        if ($this->user_session->roleID != 1) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_USERCONTRO_USERLICENSE_PERMISSION');
            return $this->JSONRespondHtml();
        }
        $users = $this->CommonTable('User\Model\UserTable')->getActiveUsers();
        foreach ($users as $user) {
            if ($user['userID'] != $this->userID) {
                $this->CommonTable('Core\Model\LicenceTable')->makeEmptyLicenceByUserID($user['userID']);
            }
        }
        $this->status = TRUE;
        $this->msg = $this->getMessage('SUCC_USERCONTRO_USERLICENSE_SMRT_DELETE');
        return $this->JSONRespond();
    }

    /**
     * Assign all empty licenses to users from manage license
     * @author sharmilan <sharimilan@thinkcube.com>
     * @return type
     */
    public function smartUpdateUserLicenseAction() {
        if ($this->user_session->roleID != 1) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_USERCONTRO_USERLICENSE_PERMISSION');
            return $this->JSONRespondHtml();
        }
        $users = $this->CommonTable('User\Model\UserTable')->getActiveUsers();
        $assignedLicenses = $this->CommonTable('Core\Model\LicenceTable')->getAssignedLicence();
        $unlicenseUsers = array();
        foreach ($users as $user) {
            $check = false;
            foreach ($assignedLicenses as $assignedLicense) {
                if ($user['userID'] == $assignedLicense['userID']) {
                    $check = true;
                    break;
                }
            }
            if (!$check) {
                $unlicenseUsers[] = $user;
            }
        }

        $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyLicence();
        $i = 0;
        foreach ($emptyLicenses as $emptyLicense) {
            $this->CommonTable('Core\Model\LicenceTable')->assignLicenceForUserID($emptyLicense['licenceID'], $unlicenseUsers[$i]['userID']);
            $i++;
        }
        $this->msg = $this->getMessage('SUCC_USERCONTRO_USERLICENSE_SMRT_UPDATE');
        $this->status = true;
        return $this->JSONRespond();
    }

    public function payAction(){
        $this->getSideAndUpperMenus('Licenses & Payments');
        $comapnyDetails = $this->CommonTable('CompanyTable')->getCompany(1);
        $apiKey = $comapnyDetails->apiKey;
        $subdomain = $this->user_session->subdomain;
        $rootDomain = $this->getServiceLocator()->get('config')['servername'];
        $portalUrl = "https://$rootDomain/portal/login?subdomain=$subdomain&key=$apiKey";
        $viewModel = new ViewModel(array(
            'portalUrl' => $portalUrl
        ));
        $viewModel->setTemplate("user/user/payment");

        return $viewModel;
    }

    public function accountLicenseAction() {
        $this->upperMenus = 'portal_payments_upper_menu';
        $this->getSideAndUpperMenus('Licenses & Payments', 'Account License');
        $comapnyDetails = $this->CommonTable('CompanyTable')->getCompany(1);
        $apiKey = $comapnyDetails->apiKey;
        $subdomain = $this->user_session->subdomain;
        $redirect_to = 'account_license';
        $rootDomain = $this->getServiceLocator()->get('config')['servername'];
        $portalUrl = "https://$rootDomain/portal/login?subdomain=$subdomain&key=$apiKey&redirect_to=$redirect_to";
        $viewModel = new ViewModel(array(
            'portalUrl' => $portalUrl
        ));
        $viewModel->setTemplate("user/user/pay");

        return $viewModel;
    }

    public function userLicensesAction() {
        $this->upperMenus = 'portal_payments_upper_menu';
        $this->getSideAndUpperMenus('Licenses & Payments', 'User Licenses');
        $comapnyDetails = $this->CommonTable('CompanyTable')->getCompany(1);
        $apiKey = $comapnyDetails->apiKey;
        $subdomain = $this->user_session->subdomain;
        $redirect_to = 'user_licenses';
        $rootDomain = $this->getServiceLocator()->get('config')['servername'];
        $portalUrl = "https://$rootDomain/portal/login?subdomain=$subdomain&key=$apiKey&redirect_to=$redirect_to";
        $viewModel = new ViewModel(array(
            'portalUrl' => $portalUrl
        ));
        $viewModel->setTemplate("user/user/pay");

        return $viewModel;
    }

    public function changePaymentMethodAction() {
        $this->upperMenus = 'portal_payments_upper_menu';
        $this->getSideAndUpperMenus('Licenses & Payments', 'Payment Methods');
        $comapnyDetails = $this->CommonTable('CompanyTable')->getCompany(1);
        $apiKey = $comapnyDetails->apiKey;
        $subdomain = $this->user_session->subdomain;
        $redirect_to = 'change_pay_method';
        $rootDomain = $this->getServiceLocator()->get('config')['servername'];
        $portalUrl = "https://$rootDomain/portal/login?subdomain=$subdomain&key=$apiKey&redirect_to=$redirect_to";
        $viewModel = new ViewModel(array(
            'portalUrl' => $portalUrl
        ));
        $viewModel->setTemplate("user/user/pay");

        return $viewModel;
    }

    public function costingMigrateAction() {

        //truncate item in table

        $this->CommonTable('Inventory\Model\ItemInTable')->truncateTable();

        $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->getAllLocationProductsWithAllDetails();

        $migrationProducts = array();
        foreach ($productData as $product) {
            $skip = false;
            $itemInArray = array();
            $itemInArray['itemInDocumentType'] = 'Goods Received Note';
            $itemInArray['itemInDocumentID'] = '1';
            $itemInArray['itemInLocationProductID'] = $product['locationProductID'];
            $itemInArray['itemInPrice'] = $product['defaultSellingPrice'];
            $itemInArray['itemInSoldQty'] = '0';
            $itemInArray['itemInDateAndTime'] = $this->getGMTDateTime();

            if ($product['productSerialID'] && $product['productBatchID']) {
                if ($product['productSerialSold'] == '1') {
                    $skip = TRUE;
                } else {
                    $itemInArray['itemInBatchID'] = $product['productBatchID'];
                    $itemInArray['itemInSerialID'] = $product['productSerialID'];
                    $itemInArray['itemInQty'] = 1;
                }
            } else if ($product['productSerialID']) {
                if ($product['productSerialSold'] == '1') {
                    $skip = TRUE;
                } else {
                    $itemInArray['itemInBatchID'] = NULL;
                    $itemInArray['itemInSerialID'] = $product['productSerialID'];
                    $itemInArray['itemInQty'] = 1;
                }
            } else if ($product['productBatchID']) {
                if ($product['productBatchQuantity'] > 0) {
                    $itemInArray['itemInBatchID'] = $product['productBatchID'];
                    $itemInArray['itemInSerialID'] = NULL;
                    $itemInArray['itemInQty'] = $product['productBatchQuantity'];
                } else {
                    $skip = TRUE;
                }
            } else {
                $itemInArray['itemInBatchID'] = NULL;
                $itemInArray['itemInSerialID'] = NULL;
                $itemInArray['itemInQty'] = $product['locationProductQuantity'];
            }
            if (!$skip && $itemInArray['itemInQty'] > 0) {
                $migrationProducts[] = $itemInArray;
            }
        }

        foreach ($migrationProducts as &$migrationProduct) {
            $locationProID = $migrationProduct['itemInLocationProductID'];
            $grnProducts = $this->CommonTable('Inventory\Model\GrnProductTable')->getGrnProductByLocationProductID($locationProID, TRUE);
            $purchaseInvoiceProducts = $this->CommonTable('Inventory\Model\PurchaseInvoiceProductTable')->getPurchaseInvoiceProductByLocationProductID($locationProID, TRUE);
            $goodIssueProducts = $this->CommonTable('Inventory\Model\GoodsIssueProductTable')->getGoodsIssueProductByLocationProductID($locationProID, TRUE);

            $combinedProductDetails = array_merge($grnProducts, $purchaseInvoiceProducts, $goodIssueProducts);

            $itemPreviouslyInserted = FALSE;

            if (count($combinedProductDetails) > 0) {
                $itemPreviouslyInserted = TRUE;
                $date = $combinedProductDetails[0]['itemInDate'];
                $lastInsertedProData = $combinedProductDetails[0];
            }

            foreach ($combinedProductDetails as $combinedProduct) {

                if ($combinedProduct['itemInDate'] > $date) {
                    $date = $combinedProduct['itemInDate'];
                    $lastInsertedProData = $combinedProduct;
                }
            }

            if ($itemPreviouslyInserted) {
                $migrationProduct['itemInDocumentType'] = $lastInsertedProData['documentType'];
                $migrationProduct['itemInDocumentID'] = $lastInsertedProData['documentID'];
                $migrationProduct['itemInPrice'] = $lastInsertedProData['itemInPrice'];
                $migrationProduct['itemInDateAndTime'] = $lastInsertedProData['itemInDate'];
            }
        }

        foreach ($migrationProducts as $migrateProduct) {
            $itemInModel = new ItemIn();
            $itemInModel->exchangeArray($migrateProduct);
            $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
        }
        exit();
    }

    public function getLicenceTable() {
        if (!$this->licenceTable) {
            $sm = $this->getServiceLocator();
            $this->licenceTable = $sm->get('Core\Model\LicenceTable');
        }

        return $this->licenceTable;
    }

    public function getUserLocationNamesForPos($userID) {
        $sm = $this->getServiceLocator();
        $locationUserTable = $sm->get('Core\Model\UserLocationTable');

        $locations = $locationUserTable->getUserLocationsByID($userID);

        return $locations;
    }

    public function getUserDefaultLocationForPos($userID) {
        return $this->CommonTable('User\Model\UserTable')->getUserDefaultLocationByUserID($userID);
    }

}

///////////////////////END USER CONTROLLER////////////////////////


