<?php

/**
 * @author Sandun Dissanayake <sandun@thinkcube.com>
 * This file contains Dash BoardAPI related controller functions
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Reporting\Model\Sales;
use Reporting\Model\SalesTable;
use Zend\View\Model\JsonModel;
use Invoice\Model\SalesOrderTable;
use Invoice\Model\SalesOrder;
use Invoice\Model\Invoice;
use Invoice\Model\CreditNoteTable;
use Invoice\Model\PaymentsTable;
use Invoice\Model\InvoiceTable;
use Invoice\Model\PaymentMethodsTable;
use Invoice\Model\QuotationTable;

class DashBoardAPIController extends CoreController
{

    protected $userID;
    protected $username;
    protected $user_session;
    protected $appEngineUrl;
    public $data;
    public $status;
    public $msg;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->appEngineUrl = "http://usermgt.thinkcube.net";
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * DashBoard revenue function
     * TODO - deduct credit note values (Yet to implemented)
     */
    public function getRevenueAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d', strtotime($request->getPost('currentDay')));
            $period = $request->getPost('period');
            $backTimePeriod = 14;
            if ($period == 'day') {
                $backTimePeriod = 14;
            } else if ($period == 'month') {
                $backTimePeriod = 12;
            } else if ($period == 'year') {
                $backTimePeriod = 5;
            }
            $backDate = date('Y-m-d', strtotime($currentDate . '-' . $backTimePeriod . $period));
            $getInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getRevenueData($backDate, $currentDate, $period);
            $getCreditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditData($backDate, $currentDate, $period);

            //if there has credit note value then it is reduced by total revenue
            foreach ($getInvoiceData as $key => $i) {
                $flag = FALSE;
                $index = NULL;
                $creditTotal = 0;
                foreach ($getCreditNoteData as $c) {
                    if ($i['Date'] == $c['Date']) {
                        $flag = TRUE;
                        $index = $key;
                        $creditTotal = $c['TotalCredit'];
                        break;
                    }
                }
                if ($flag) {
                    $getInvoiceData[$index]['TotalRevenue'] = $getInvoiceData[$index]['TotalRevenue'] - $creditTotal;
                }
            }

            $revenue = array();
            if ($period == 'day') {
                $tmpCurrentDate = $currentDate;
                for ($i = $backTimePeriod; $i >= 0; $i--) {
                    $tmpBackDate = date('d', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                    $tmpBackMonth = date('M', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                    $dataFlag = FALSE;
                    foreach ($getInvoiceData as $res) {
                        if ($res['Date'] == $tmpBackDate) {
                            $revenue[] = array('x' => date("M", strtotime($res['Month'])) . ' ' . $res['Date'], 'y' => number_format((float) $res['TotalRevenue'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                    }
                    if ($dataFlag == FALSE) {
                        $revenue[] = array('x' => $tmpBackMonth . ' ' . $tmpBackDate, 'y' => '0.00');
                    }
                }
            } else if ($period == 'month') {
                $tmpCurrentDate = $currentDate;
                for ($i = $backTimePeriod; $i >= 0; $i--) {
                    $tmpBackMonth = date('F', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                    $tmpBackYear = date('Y', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                    $dataFlag = FALSE;
                    foreach ($getInvoiceData as $res) {
                        if ($res['Month'] == $tmpBackMonth && $res['Year'] == $tmpBackYear) {
                            $revenue[] = array('x' => date("M", strtotime($res['Month'])) . '-' . $res['Year'], 'y' => number_format((float) $res['TotalRevenue'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                    }
                    if ($dataFlag == FALSE) {
                        $revenue[] = array('x' => date("M", strtotime($tmpBackMonth)) . '-' . $tmpBackYear, 'y' => '0.00');
                    }
                }
            } else if ($period == 'year') {
                $tmpCurrentDate = $currentDate;
                for ($i = $backTimePeriod; $i >= 0; $i--) {
                    $tmpBackYear = date('Y', strtotime($tmpCurrentDate . '-' . $i . 'year'));
                    $dataFlag = FALSE;
                    foreach ($getInvoiceData as $res) {
                        if ($res['Year'] == $tmpBackYear) {
                            $revenue[] = array('x' => $res['Year'], 'y' => number_format((float) $res['TotalRevenue'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                    }
                    if ($dataFlag == FALSE) {
                        $revenue[] = array('x' => $tmpBackYear, 'y' => '0.00');
                    }
                }
            }
            header('Content-Type: application/json');
            $revenuedata = array(
                'revenue' => $revenue,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            );
            echo json_encode($revenuedata);
            exit();
        } else {
            exit();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * DashBoard cashInFlow function
     */
    public function getCashInFlowAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d', strtotime($request->getPost('currentDay')));
            $period = $request->getPost('period');
            $backTimePeriod = 14;
            if ($period == 'day') {
                $backTimePeriod = 14;
            } else if ($period == 'month') {
                $backTimePeriod = 12;
            } else if ($period == 'year') {
                $backTimePeriod = 5;
            }
            $backDate = date('Y-m-d', strtotime($currentDate . '-' . $backTimePeriod . $period));
            $result = $this->CommonTable('Invoice\Model\PaymentsTable')->getCashInFlowData($backDate, $currentDate, $period);
            $cashInFlow = array();
            if ($period == 'day') {
                $tmpCurrentDate = $currentDate;
                for ($i = $backTimePeriod; $i >= 0; $i--) {
                    $tmpBackDate = date('d', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                    $tmpBackMonth = date('M', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                    $dataFlag = FALSE;
                    if ($result != NULL) {
                        foreach ($result as $res) {
                            if ($res['Date'] == $tmpBackDate) {
                                $cashInFlow[] = array('x' => date("M", strtotime($res['Month'])) . ' ' . $res['Date'], 'y' => number_format((float) $res['TotalCashFlow'], 2, '.', ''));
                                $dataFlag = TRUE;
                            }
                        }
                    }
                    if ($dataFlag == FALSE) {
                        $cashInFlow[] = array('x' => $tmpBackMonth . ' ' . $tmpBackDate, 'y' => '0.00');
                    }
                }
            } else if ($period == 'month') {
                $tmpCurrentDate = $currentDate;
                for ($i = $backTimePeriod; $i >= 0; $i--) {
                    $tmpBackMonth = date('F', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                    $tmpBackYear = date('Y', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                    $dataFlag = FALSE;
                    if ($result != NULL) {
                        foreach ($result as $res) {
                            if ($res['Month'] == $tmpBackMonth && $res['Year'] == $tmpBackYear) {
                                $cashInFlow[] = array('x' => date("M", strtotime($res['Month'])) . '-' . $res['Year'], 'y' => number_format((float) $res['TotalCashFlow'], 2, '.', ''));
                                $dataFlag = TRUE;
                            }
                        }
                    }
                    if ($dataFlag == FALSE) {
                        $cashInFlow[] = array('x' => date("M", strtotime($tmpBackMonth)) . '-' . $tmpBackYear, 'y' => '0.00');
                    }
                }
            } else if ($period == 'year') {
                $tmpCurrentDate = $currentDate;
                for ($i = $backTimePeriod; $i >= 0; $i--) {
                    $tmpBackYear = date('Y', strtotime($tmpCurrentDate . '-' . $i . 'year'));
                    $dataFlag = FALSE;
                    if ($result != NULL) {
                        foreach ($result as $res) {
                            if ($res['Year'] == $tmpBackYear) {
                                $cashInFlow[] = array('x' => $res['Year'], 'y' => number_format((float) $res['TotalCashFlow'], 2, '.', ''));
                                $dataFlag = TRUE;
                            }
                        }
                    }
                    if ($dataFlag == FALSE) {
                        $cashInFlow[] = array('x' => $tmpBackYear, 'y' => '0.00');
                    }
                }
            }
            header('Content-Type: application/json');
            $cashInFlowData = array(
                'cashInFlow' => $cashInFlow,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            );
            echo json_encode($cashInFlowData);
            exit();
        } else {
            exit();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * Dashboard Due Invoices Function
     */
    function getDueInvoicesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d', strtotime($request->getPost('currentDay')));
            $period = $request->getPost('period');
            $frontDate;
            $backTimePeriod = 20;
            if ($period == '1-20') {
                $frontDate = $currentDate;
            } else if ($period == '20-40') {
                $frontDate = date('Y-m-d', strtotime($currentDate . '- 20 day'));
            } else if ($period == '40-60') {
                $frontDate = date('Y-m-d', strtotime($currentDate . '- 40 day'));
            }
            $backDate = date('Y-m-d', strtotime($frontDate . '-' . $backTimePeriod . 'day'));
            $result = $this->CommonTable('Invoice\Model\InvoiceTable')->getDueInvoicesData($backDate, $frontDate);
            $dueInvoices = array();
            $tmpCurrentDate = $frontDate;
            for ($i = $backTimePeriod; $i >= 0; $i--) {
                $tmpBackDate = date('d', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                $tmpBackMonth = date('M', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                $dataFlag = FALSE;
                foreach ($result as $res) {
                    if ($res['Date'] == $tmpBackDate) {
                        $dueInvoices[] = array('x' => date("M", strtotime($res['Month'])) . ' ' . $res['Date'], 'y' => number_format((float) $res['TotalOverdue'], 2, '.', ''));
                        $dataFlag = TRUE;
                    }
                }
                if ($dataFlag == FALSE) {
                    $dueInvoices[] = array('x' => $tmpBackMonth . ' ' . $tmpBackDate, 'y' => '0.00');
                }
            }
            header('Content-Type: application/json');
            $dueInvoicesData = array(
                'dueInvoices' => $dueInvoices,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            );
            echo json_encode($dueInvoicesData);
            exit();
        } else {
            exit();
        }
    }

    /**
     * @author Sandun Dissanayake<sandun@thinkcube.com>
     * getting which related to cash inflow and revenue in graph data
     * @return array
     */
    function getRevenueAndCashFlowDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $currentYearRevenueData = $this->CommonTable('Invoice\Model\InvoiceTable')->getCurrentRevenueBy('year');
            $currentYearCreditData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCurrentCreditBy('year');
            $currentMonthRevenueData = $this->CommonTable('Invoice\Model\InvoiceTable')->getCurrentRevenueBy('month');
            $currentMonthCreditData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCurrentCreditBy('month');
            $currentDateRevenueData = $this->CommonTable('Invoice\Model\InvoiceTable')->getCurrentRevenueBy('date');
            $currentDateCreditData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCurrentCreditBy('date');
            $currentYearCashInflowData = $this->CommonTable('Invoice\Model\PaymentsTable')->getCurrentCashInFlowBy('year');
            $currentMonthInflowData = $this->CommonTable('Invoice\Model\PaymentsTable')->getCurrentCashInFlowBy('month');
            $currentDateInflowData = $this->CommonTable('Invoice\Model\PaymentsTable')->getCurrentCashInFlowBy('date');
            $currentYearCashOutData = $this->CommonTable('SupplierPaymentsTable')->getCurrentCashOutFlowBy('year');
            $currentYearAdvancePaymentData = $this->CommonTable('SupplierPaymentsTable')->getCurrentAdvancedPaymentBy('year', $paymentMethod = 1);
            $currentMonthCashOutData = $this->CommonTable('SupplierPaymentsTable')->getCurrentCashOutFlowBy('month');
            $currentMonthAdvancePaymentData = $this->CommonTable('SupplierPaymentsTable')->getCurrentAdvancedPaymentBy('month', $paymentMethod = 1);
            $currentDateCashOutData = $this->CommonTable('SupplierPaymentsTable')->getCurrentCashOutFlowBy('date');
            $currentDateAdvancePaymentData = $this->CommonTable('SupplierPaymentsTable')->getCurrentAdvancedPaymentBy('date', $paymentMethod = 1);
            $currentDateCreditPaymentCashOutData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCurrentCashOutFlowBy('date');
            $currentMonthCreditPaymentCashOutData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCurrentCashOutFlowBy('month');
            $currentYearCreditPaymentCashOutData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCurrentCashOutFlowBy('year');

            $yearRev = $currentYearRevenueData->current();
            $yearCredit = $currentYearCreditData->current();
            $monthRev = $currentMonthRevenueData->current();
            $monthCredit = $currentMonthCreditData->current();
            $dayRev = $currentDateRevenueData->current();
            $dayCredit = $currentDateCreditData->current();
            $yearCashInFlow = $currentYearCashInflowData->current();
            $monthCashInFlow = $currentMonthInflowData->current();
            $dayCashInFlow = $currentDateInflowData->current();
            $yearCashOutFlow = $currentYearCashOutData->current();
            $yearAdvancePaymentFlow = $currentYearAdvancePaymentData->current();
            $monthCashOutFlow = $currentMonthCashOutData->current();
            $monthAdvancePaymentFlow = $currentMonthAdvancePaymentData->current();
            $dateCashOutFlow = $currentDateCashOutData->current();
            $dateAdvancePaymentFlow = $currentDateAdvancePaymentData->current();
            $dayCreditPaymentCashOutFlow = $currentDateCreditPaymentCashOutData->current();
            $monthCreditPaymentCashOutFlow = $currentMonthCreditPaymentCashOutData->current();
            $yearCreditPaymentCashOutFlow = $currentYearCreditPaymentCashOutData->current();

            $data = array(
                'yearRevenue' => $yearRev['TotalRevenue'] - $yearCredit['TotalCredit'],
                'monthRevenue' => $monthRev['TotalRevenue'] - $monthCredit['TotalCredit'],
                'dayRevenue' => $dayRev['TotalRevenue'] - $dayCredit['TotalCredit'],
                'yearCashInFlow' => $yearCashInFlow['TotalCashInFlow'],
                'monthCashInFlow' => $monthCashInFlow['TotalCashInFlow'],
                'dayCashInFlow' => $dayCashInFlow['TotalCashInFlow'],
                'yearCashOutFlow' => $yearCashOutFlow['TotalCashOutFlow'] + $yearAdvancePaymentFlow['TotalCashOutFlow'] + $yearCreditPaymentCashOutFlow['TotalCashOutFlow'],
                'monthCashOutFlow' => $monthCashOutFlow['TotalCashOutFlow'] + $monthAdvancePaymentFlow['TotalCashOutFlow'] + $monthCreditPaymentCashOutFlow['TotalCashOutFlow'],
                'dayCashOutFlow' => $dateCashOutFlow['TotalCashOutFlow'] + $dateAdvancePaymentFlow['TotalCashOutFlow'] + $dayCreditPaymentCashOutFlow['TotalCashOutFlow'],
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            );

            $this->data = $data;
            $this->msg = 'Reveune and Cashflow data';
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $period
     * @param type $mergeResult
     * @return array $result
     */
    private function _sortCashOutFlowArray($period, $mergeResult)
    {
        //sort $mergeResult array according to $period element
        $sortResult = array();
        foreach ($mergeResult as $key => $row) {
            $sortResult[$key] = $row[$period];
        }
        array_multisort($sortResult, SORT_ASC, $mergeResult);

        //If there have more data with same $period element
        //Then calculate total of TotalCashOutFlow elements which contain same $period element

        $result = array();
        $recordFlag = FALSE;
        $date = NULL;
        $cashFlowTotal = 0;
        $count = 0;
        foreach ($mergeResult as $key => $data) {
            if ($date == $data[$period]) {
                $cashFlowTotal += $data['TotalCashOutFlow'];
                $result[$count]['TotalCashOutFlow'] = $cashFlowTotal;
                $recordFlag = TRUE;
            }
            if ($recordFlag == FALSE) {
                $count++;
                $cashFlowTotal = $data['TotalCashOutFlow'];
                $result[$count] = $data;
            }
            $date = $data[$period];
            $recordFlag = FALSE;
        }

        return $result;
    }

    /**
     * @author Sandun<sandun@thinkcube.com>
     * getting only cash outgoingpayment data
     * @return JSON cashOutFlowData
     */
    public function getCashOutFlowAction()
    {
        $request = $this->getRequest();
        $mergeResult = array();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d', strtotime($request->getPost('currentDay')));
            $period = $request->getPost('period');
            $backTimePeriod = 14;
            if ($period == 'day') {
                $backTimePeriod = 14;
            } else if ($period == 'month') {
                $backTimePeriod = 12;
            } else if ($period == 'year') {
                $backTimePeriod = 5;
            }
            $backDate = date('Y-m-d', strtotime($currentDate . '-' . $backTimePeriod . $period));
            $invoiceCashPayment = $this->CommonTable('SupplierPaymentsTable')->getCashOutFlowData($backDate, $currentDate, $period);
            $advanceCashPayment = $this->CommonTable('SupplierPaymentsTable')->getAdvancedPaymentData($backDate, $currentDate, $period, $paymentType = 1);
            $pettyCashFloats = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getActivePettyCashFloats($backDate, $currentDate, $period);

            $invoiceCashPayment = $invoiceCashPayment == null ? array() : $invoiceCashPayment;
            $advanceCashPayment = $advanceCashPayment == null ? array() : $advanceCashPayment;
            $pettyCashFloats = $pettyCashFloats == null ? array() : $pettyCashFloats;

            $mergeResult = array_merge($invoiceCashPayment, $advanceCashPayment, $pettyCashFloats);

            $cashOutFlow = array();
            $result = array();
            if ($period == 'day') {
                $result = $this->_sortCashOutFlowArray($period = 'Date', $mergeResult);
                $tmpCurrentDate = $currentDate;
                for ($i = $backTimePeriod; $i >= 0; $i--) {
                    $tmpBackDate = date('d', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                    $tmpBackMonth = date('M', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                    $dataFlag = FALSE;
                    foreach ($result as $res) {
                        if ($res['Date'] == $tmpBackDate) {
                            $cashOutFlow[] = array('x' => date("M", strtotime($res['Month'])) . ' ' . $res['Date'], 'y' => number_format((float) $res['TotalCashOutFlow'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                    }
                    if ($dataFlag == FALSE) {
                        $cashOutFlow[] = array('x' => $tmpBackMonth . ' ' . $tmpBackDate, 'y' => '0.00');
                    }
                }
            } else if ($period == 'month') {
                $result = $this->_sortCashOutFlowArray($period = 'Month', $mergeResult);

                $tmpCurrentDate = $currentDate;
                for ($i = $backTimePeriod; $i >= 0; $i--) {
                    $tmpBackMonth = date('F', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                    $tmpBackYear = date('Y', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                    $dataFlag = FALSE;
                    foreach ($result as $res) {
                        if ($res['Month'] == $tmpBackMonth && $res['Year'] == $tmpBackYear) {
                            $cashOutFlow[] = array('x' => date("M", strtotime($res['Month'])) . '-' . $res['Year'], 'y' => number_format((float) $res['TotalCashOutFlow'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                    }
                    if ($dataFlag == FALSE) {
                        $cashOutFlow[] = array('x' => date("M", strtotime($tmpBackMonth)) . '-' . $tmpBackYear, 'y' => '0.00');
                    }
                }
            } else if ($period == 'year') {
                $result = $this->_sortCashOutFlowArray($period = 'Year', $mergeResult);

                $tmpCurrentDate = $currentDate;
                for ($i = $backTimePeriod; $i >= 0; $i--) {
                    $tmpBackYear = date('Y', strtotime($tmpCurrentDate . '-' . $i . 'year'));
                    $dataFlag = FALSE;
                    foreach ($result as $res) {

                        if ($res['Year'] == $tmpBackYear) {
                            $cashOutFlow[] = array('x' => $res['Year'], 'y' => number_format((float) $res['TotalCashOutFlow'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                    }
                    if ($dataFlag == FALSE) {
                        $cashOutFlow[] = array('x' => $tmpBackYear, 'y' => '0.00');
                    }
                }
            }
            header('Content-Type: application/json');

            $cashOutFlowData = array(
                'cashOutFlow' => $cashOutFlow,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            );
            echo json_encode($cashOutFlowData);
            exit();
        } else {
            exit();
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>     *
     * @return \Zend\View\Model\JsonModel
     * add or update widget order function
     */
    public function sortwidgetAction()
    {
        $request = $this->getRequest();

        if ($request->isPost() && $this->userID != NULL) {
            $leftWdata = $request->getPost('leftWidget');
            $rightWdata = $request->getPost('rightWidget');
            $vals = $request->getPost('vals');

            $leftWArr = array();
            $rightWArr = array();
            parse_str($leftWdata, $leftWArr);
            parse_str($rightWdata, $rightWArr);
            $leftWOrder = json_encode($leftWArr['block']);
            $rightWOrder = json_encode($rightWArr['block']);

            if (!empty($vals)) {

                $respond = $this->CommonTable('User\Model\UserSettingTable')->updateWidget(json_encode($vals), $this->userID);

                $this->msg = 'Widget checkboxList successfully updated';
                $this->data = $respond;
                $this->status = TRUE;

                return $this->JSONRespond();
            } else {
                $respond = $this->CommonTable('User\Model\UserSettingTable')->updateWidget($leftWOrder, $rightWOrder, $this->userID);

                $this->msg = 'sortwidget or Widget checkboxList successfully updated';
                $this->data = $respond;
                $this->status = TRUE;

                return $this->JSONRespond();
            }
        }
    }

    public function getDashboardDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'year';
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'months';
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'week';
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $subPeriod = 'day';
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }
            
            $chequeData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalRecivedChequesByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $recivedChequeTotal = 0;
            foreach ($chequeData as $value) {
                $exitsingCheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($value['incomingPaymentMethodChequeId']);
                if (is_null($exitsingCheque)) {
                    $recivedChequeTotal += floatval($value['incomingPaymentMethodAmount']);         
                }
            }

            $lastPeriodChequeData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalRecivedChequesByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $lastPeriodRecivedChequeTotal = 0;
            foreach ($lastPeriodChequeData as $value) {
                $exitsingCheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($value['incomingPaymentMethodChequeId']);
                if (is_null($exitsingCheque)) {
                    $lastPeriodRecivedChequeTotal += floatval($value['incomingPaymentMethodAmount']);         
                }
            }

            $recivedChequeProfitLoss = (($recivedChequeTotal - $lastPeriodRecivedChequeTotal) / $recivedChequeTotal) * 100;

            $settledInvoiceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId(null, 0, $fromDate, $toDate);
            $settledInvoiceChequesTotal = 0;
            foreach ($settledInvoiceCheques as $value) {
                $settledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }

            $settledAdvanceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId(null,0, $fromDate, $toDate);
            foreach ($settledAdvanceCheques as $value) {
                $settledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }


            $lastPeriodSettledInvoiceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId(null, 0, $lastPeriodFromDate, $lastPeriodToDate);
            $lastPeriodSettledInvoiceChequesTotal = 0;
            foreach ($lastPeriodSettledInvoiceCheques as $value) {
                $lastPeriodSettledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }

            $lastPeriodSettledAdvanceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId(null,0, $lastPeriodFromDate, $lastPeriodToDate);
            foreach ($lastPeriodSettledAdvanceCheques as $value) {
                $lastPeriodSettledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }

            $settledChequeProfitLoss = (($settledInvoiceChequesTotal - $lastPeriodSettledInvoiceChequesTotal) / $settledInvoiceChequesTotal) * 100;

            $lastYearInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationID($fromDate,$toDate,$locationID);


            $lastYearCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $creditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $totalSales = floatval($invoiceData['invTotal']) - floatval($creditnoteData['total']);
            $totalSalesLastYear = floatval($lastYearInvoiceData['invTotal']) - floatval($lastYearCreditnoteData['total']);

            $totalSalesProfitLoss = (($totalSales - $totalSalesLastYear) / $totalSales) * 100;

            $proIds = [];
            $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDropdown([$locationID], $productSearchKey = NULL, $isSearch = FALSE);

            foreach ($productData as $value) {
                $proIds[$value['productID']] = $value['productID'];
            }
            $globalStockValueData = $this->getService('StockInHandReportService')->_getGlobalWiseStockValueData($proIds, [$locationID], "1", "");

            $totalStockValue = 0;
            $totalStockSellingValue = 0;
            foreach ($globalStockValueData as $key => $data) {
                $availableQty = 0;
                $totalCostValue = 0;
                $uPrice = 0;
                $availableQtyAmount = 0;
                $expectSellValue = 0;
                foreach ($data['proD'] as $key => $value) {
                    $aQty = $value['availableQty'] ? $value['availableQty'] : 0.00;
                    $expectSellValue += ($aQty * floatval($value['productDefaultSellingPrice']));
                    $availableQty += $aQty;
                    $costValue = $value['totalCostValue'] ? $value['totalCostValue'] : 0.00;
                    $discount = isset($value['discountValue']) ? $value['discountValue'] : 0;
                    $tax = isset($value['taxValue']) ? $value['taxValue'] : 0;
                    $totalCostValue += (($costValue + $tax) - $discount);
                }
                if ($availableQty != 0 && $availableQty != NULL) {
                    $uPrice = ($totalCostValue) / $availableQty;
                }
                $availableQtyAmount = $uPrice * $availableQty;
                $totalStockValue += $availableQtyAmount;
                $totalStockSellingValue += $expectSellValue;
            }

            $bestMovingItems = $this->CommonTable('Inventory\Model\ItemOutTable')->getBestMovingItemByDateRange($fromDate, $toDate,$locationID);

            $movingCount = array_column($bestMovingItems, 'movingCount');

            array_multisort($movingCount, SORT_DESC, $bestMovingItems);

            header('Content-Type: application/json');
            $widgetData = array(
                'recivedChequeTotal' => $recivedChequeTotal,
                'lastPeriodRecivedChequeTotal' => $lastPeriodRecivedChequeTotal,
                'settledInvoiceChequesTotal' => $settledInvoiceChequesTotal,
                'bestMovingItems' => $bestMovingItems,
                'lastPeriodSettledInvoiceChequesTotal' => $lastPeriodSettledInvoiceChequesTotal,
                'recivedChequeProfitLoss' => round($recivedChequeProfitLoss,2),
                'settledChequeProfitLoss' => round($settledChequeProfitLoss,2),
                'totalSales' => round($totalSales,2),
                'totalSalesLastYear' => round($totalSalesLastYear,2),
                'totalSalesProfitLoss' => round($totalSalesProfitLoss,2),
                'totalStockValue' => round($totalStockValue,2),
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function getTotalSalesDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'year';
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'months';
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'week';
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $subPeriod = 'day';
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }
            
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $lastYearCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $creditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $totalSales = floatval($invoiceData['invTotal']) - floatval($creditnoteData['total']);
            $totalSalesLastYear = floatval($lastYearInvoiceData['invTotal']) - floatval($lastYearCreditnoteData['total']);

            $totalSalesProfitLoss = (($totalSales - $totalSalesLastYear) / $totalSales) * 100;

            header('Content-Type: application/json');
            $widgetData = array(
                'totalSales' => round($totalSales,2),
                'totalSalesLastYear' => round($totalSalesLastYear,2),
                'totalSalesProfitLoss' => round($totalSalesProfitLoss,2),
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                // 'totalStockValue' => round($totalStockValue,2),
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function getTotalStockDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'year';
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'months';
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'week';
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $subPeriod = 'day';
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }
            
            $proIds = [];
            $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->searchLocationWiseInventoryProductsForDashboard([$locationID], $productSearchKey = NULL, $isSearch = FALSE);

            foreach ($productData as $value) {
                $proIds[$value['productID']] = $value['productID'];
            }
            $globalStockValueData = $this->getService('StockInHandReportService')->_getGlobalWiseStockValueData($proIds, [$locationID], "1", "");

            $totalStockValue = 0;
            $totalStockSellingValue = 0;
            foreach ($globalStockValueData as $key => $data) {
                $availableQty = 0;
                $totalCostValue = 0;
                $uPrice = 0;
                $availableQtyAmount = 0;
                $expectSellValue = 0;
                foreach ($data['proD'] as $key => $value) {
                    $aQty = $value['availableQty'] ? $value['availableQty'] : 0.00;
                    $expectSellValue += ($aQty * floatval($value['productDefaultSellingPrice']));
                    $availableQty += $aQty;
                    $costValue = $value['totalCostValue'] ? $value['totalCostValue'] : 0.00;
                    $discount = isset($value['discountValue']) ? $value['discountValue'] : 0;
                    $tax = isset($value['taxValue']) ? $value['taxValue'] : 0;
                    $totalCostValue += (($costValue + $tax) - $discount);
                }
                if ($availableQty != 0 && $availableQty != NULL) {
                    $uPrice = ($totalCostValue) / $availableQty;
                }
                $availableQtyAmount = $uPrice * $availableQty;
                $totalStockValue += $availableQtyAmount;
                $totalStockSellingValue += $expectSellValue;
            }

            header('Content-Type: application/json');
            $widgetData = array(
                'totalStockValue' => round($totalStockValue,2),
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function getChequeDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'year';
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'months';
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'week';
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $subPeriod = 'day';
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }
            
            $chequeData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalRecivedChequesByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $recivedChequeTotal = 0;
            foreach ($chequeData as $value) {
                $exitsingCheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($value['incomingPaymentMethodChequeId']);
                if (is_null($exitsingCheque)) {
                    $recivedChequeTotal += floatval($value['incomingPaymentMethodAmount']);         
                }
            }

            $lastPeriodChequeData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalRecivedChequesByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $lastPeriodRecivedChequeTotal = 0;
            foreach ($lastPeriodChequeData as $value) {
                $exitsingCheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($value['incomingPaymentMethodChequeId']);
                if (is_null($exitsingCheque)) {
                    $lastPeriodRecivedChequeTotal += floatval($value['incomingPaymentMethodAmount']);         
                }
            }

            $recivedChequeProfitLoss = (($recivedChequeTotal - $lastPeriodRecivedChequeTotal) / $recivedChequeTotal) * 100;

            $settledInvoiceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountIdForDashboard(null, 0, $fromDate, $toDate);
            $settledInvoiceChequesTotal = 0;


            foreach ($settledInvoiceCheques as $value) {
                $settledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }

            $settledAdvanceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountIdForDashboard(null,0, $fromDate, $toDate);
            foreach ($settledAdvanceCheques as $value) {
                $settledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }


            $lastPeriodSettledInvoiceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountIdForDashboard(null, 0, $lastPeriodFromDate, $lastPeriodToDate);
            $lastPeriodSettledInvoiceChequesTotal = 0;
            foreach ($lastPeriodSettledInvoiceCheques as $value) {
                $lastPeriodSettledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }

            $lastPeriodSettledAdvanceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountIdForDashboard(null,0, $lastPeriodFromDate, $lastPeriodToDate);
            foreach ($lastPeriodSettledAdvanceCheques as $value) {
                $lastPeriodSettledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }

            $settledChequeProfitLoss = (($settledInvoiceChequesTotal - $lastPeriodSettledInvoiceChequesTotal) / $settledInvoiceChequesTotal) * 100;

            header('Content-Type: application/json');
            $widgetData = array(
                'recivedChequeTotal' => $recivedChequeTotal,
                'lastPeriodRecivedChequeTotal' => $lastPeriodRecivedChequeTotal,
                'settledInvoiceChequesTotal' => $settledInvoiceChequesTotal,
                // 'bestMovingItems' => $bestMovingItems,
                'lastPeriodSettledInvoiceChequesTotal' => $lastPeriodSettledInvoiceChequesTotal,
                'recivedChequeProfitLoss' => round($recivedChequeProfitLoss,2),
                'settledChequeProfitLoss' => round($settledChequeProfitLoss,2),
                'companyCurrencySymbol'=> $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }
}
