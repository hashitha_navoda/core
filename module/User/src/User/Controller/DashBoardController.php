<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains Dash Board related controller functions
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use User\Model\User;
use User\Form\UserForm;
use User\Form\UserLoginForm;
use Zend\Http\Request;
use Zend\Http\Client;
use User\Model\Logger;
use Invoice\Model\InvoiceTable;
use Core\Controller\CoreController;

class DashBoardController extends CoreController
{

    protected $userID;
    protected $username;
    protected $user_session;
    protected $appEngineUrl;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->appEngineUrl = "http://usermgt.thinkcube.net";
    }

    public function indexAction()
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $this->getEvent()->getViewModel()->sidemenu = $globaldata['home_menu'];
        $this->getEvent()->getViewModel()->sidemenuselected = 'Home';

//        Check user have permission to view dashboard
        $controller = 'User\Controller\DashBoard';
        $action = 'index';
        $isDisabled = $this->checkFeatureEnableByUserID($controller, $action, $this->userID);
        if ($isDisabled) {
            return $isDisabled;
        }

        $notifi = $this->getServiceLocator()->get("NotificationController");
        $notificationData = $notifi->setViewVariables();

        $companyDetails = $this->CommonTable('CompanyTable')->getCompany();
        $locationID = $this->user_session->userActiveLocation['locationID'];

        $paginator = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getUndepositedCheques(false, $locationID);

        $settledInvoiceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId(null, 0, null, null);
        $IssuedCheques = [];
        foreach ($settledInvoiceCheques as $value) {
            $IssuedCheques[] = $value;
        }

        $settledAdvanceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId(null,0, null, null);
        foreach ($settledAdvanceCheques as $value) {
            $IssuedCheques[] = $value;
        }


        $viewmodel = new ViewModel(array(
            'companyName' => $companyDetails->companyName,
            'notificationData' => $notificationData,
            'cheques' => $paginator,
            'issuedCheques' => $IssuedCheques,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));


        $this->getViewHelper('HeadLink')->prependStylesheet('/css/dashboard.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/dashboard.js');

        return $viewmodel;
    }

}
