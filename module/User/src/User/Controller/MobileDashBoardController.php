<?php


namespace User\Controller;

use Zend\Mvc\Controller\AbstractRestfulController;
use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Reporting\Model\Sales;
use Reporting\Model\SalesTable;
use Zend\View\Model\JsonModel;
use Invoice\Model\SalesOrderTable;
use Invoice\Model\SalesOrder;
use Invoice\Model\Invoice;
use Invoice\Model\CreditNoteTable;
use Invoice\Model\PaymentsTable;
use Invoice\Model\InvoiceTable;
use Invoice\Model\PaymentMethodsTable;
use Invoice\Model\QuotationTable;

class MobileDashBoardController extends CoreController
{
	protected $userID;
    protected $username;
    protected $user_session;
    protected $appEngineUrl;
    public $data;
    public $status;
    public $msg;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->appEngineUrl = "http://usermgt.thinkcube.net";
    }

    
    public function getInvoiceAndPaidAmountsForDonutAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getInvoiceAndPaidAmountsForDonut($requestData);

        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond();
        
    }

    public function getInvoicePaymentDetailsForDonutAction()
    {
		$request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getInvoicePaymentDetailsForDonut($requestData);

        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond();

    }

    public function getPurchaseInvoiceAndPurchasePaymentsForDonutAction()
    {
		$request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getPurchaseInvoiceAndPurchasePaymentsForDonut($requestData);

        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond();
          	
    }

    public function getPurchaseInvoicePaymentDetailsForDonutAction()
    {
		$request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getPurchaseInvoicePaymentDetailsForDonut($requestData);
         
        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond(); 	
    }

    public function getDataForSalesDashBoardAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getDataForSalesDashBoard($requestData);

        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond(); 
    }

    public function getDataForPurchaseDashBoardAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getDataForPurchaseDashBoard($requestData);

        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond();  
    }

    public function getDataForSalesInvoiceBarChartAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getDataForSalesInvoiceBarChart($requestData);
        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond(); 
           
    }

    public function getDataForSalesPaymentBarChartAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getDataForSalesPaymentBarChart($requestData);
        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond();

    }

    public function getDataForPurchaseInvoiceBarChartAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getDataForPurchaseInvoiceBarChart($requestData);

        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond();  
    }

    public function getDataForPurchasePaymentBarChartAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getDataForPurchasePaymentBarChart($requestData);
        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond();   
    }
    public function getNotificationCountAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getNotificationCount($requestData);
        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond();   
    }

    public function getNotificationForMobileAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getNotificationForMobile($requestData);
        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond(); 

    }

    public function getDailySalesInvoiceDetailsAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getDailySalesInvoiceDetails($requestData);
        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond(); 

    }

    public function getDailyPurchaseInvoiceDetailsAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getDailyPurchaseInvoiceDetails($requestData);
        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond(); 

    }

    public function getCompanyDetailsAction()
    {
        $request = $this->getRequest();
        $requestData = $request->getPost();
        $respondData = $this->getService('MobileDashBoardService')->getCompanyDetails($requestData);
        $this->status = true;
        $this->data = $respondData;
        return $this->JSONRespond();        
    }

}