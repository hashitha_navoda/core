<?php

/**
 * @author sharmilan<sharmilan@thinkcube.com>
 *
 */

namespace User\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use User\Form\SalesPersonForm;
use User\Model\SalesPerson;
use Zend\I18n\Translator\Translator;

class SalesPersonController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'user_upper_menu';
    protected $downMenus;
    protected $paginator;
    protected $searchPaginator;
    protected $salesPersonID;
    protected $user_session;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * which display the list of sales person
     * @return \Zend\View\Model\ViewModel
     */
    public function indexAction()
    {
        $this->getSideAndUpperMenus('User', 'Sales Person');

        $header = new ViewModel();
        $header->setTemplate('user/sales-person/sales-person-header');

        $this->getpaginatedSalesPerson();

        $view = new ViewModel(array(
            'salesPersons' => $this->paginator
        ));
        $view->addChild($header, 'salesPersonHeader');
        return $view;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * which display the add new sales person Form
     * @return type
     */
    public function createAction()
    {
        $this->getSideAndUpperMenus('User', 'Sales Person');

        // get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $supplierTitle = $config['personTitle'];
        $activeUsers = $this->CommonTable('User\Model\UserTable')->getActiveUsers();
        $users = array();
        foreach ($activeUsers as $activeUser) {
            $users[$activeUser['userID']] = $activeUser['userUsername'];
        }
        $form = new SalesPersonForm(array(
            'suplierTitle' => $supplierTitle,
            'activeUse' => $users,
        ));
        $form->get('update-sales-person-button')->setAttribute('class', 'hidden');
        $form->get('update-sales-person-button')->setAttribute('type', 'button');
        $translator = new Translator();
        $panelTitle = $translator->translate('Create new Sales Person');
        $viewmodel = new ViewModel(array(
            'form' => $form,
            'panelTitle' => $panelTitle));

//        $viewmodel->setTerminal(true);
        $viewmodel->setTemplate('user/sales-person/create-sales-person');
        $this->status = TRUE;
        $this->html = $viewmodel;
//        return $this->JSONRespondHtml();
        return $viewmodel;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * which store to the database and return list
     */
    public function saveAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        $result = $this->CommonTable('User\Model\SalesPersonTable')->searchBySortName($post['salesPersonSortName']);

        if ($result->current()) {

            $this->msg = $this->getMessage('ERR_SALESPERSONCON_SNAME_EXIST');
            $this->status = 'false';
            return $this->JSONRespond();
        } else {

            $postData['salesPersonSortName'] = $post['salesPersonSortName'];
            $postData['salesPersonTitle'] = $post['salesPersonTitle'];
            $postData['salesPersonFirstName'] = $post['salesPersonFirstName'];
            $postData['salesPersonLastName'] = $post['salesPersonLastName'];
            $postData['salesPersonEmail'] = $post['salesPersonEmail'];
            $postData['salesPersonTelephoneNumber'] = $post['salesPersonTelephoneNumber'];
            $postData['salesPersonAddress'] = $post['salesPersonAddress'];
            $postData['userID'] = $post['activeUserId'];

            $salesPerson = new SalesPerson();
            $salesPerson->exchangeArray($postData);
            $insertedID = $this->CommonTable('User\Model\SalesPersonTable')->savaSalesPerson($salesPerson);

            if ($insertedID) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_SALESPERSONCON_SALESPER_ADD');
                $this->flashMessenger()->addMessage($this->getMessage('SUCC_SALESPERSONCON_SALESPER_ADD'));
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_SALESPERSONCON_SALESPER_ADD');
                return $this->JSONRespondHtml();
            }
        }
    }

    /**
     * @author sharmilan <sharmilan@thincube.com>
     * @param type $salesPersonID
     */
//    TO-DO if deleted sales person must delete all other  corresponded places
    public function deleteAction($salesPersonID = NULL)
    {
        $salesPersonID = $this->params()->fromRoute('param1', NULL);
        $relatedSalesInvoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataBySalesPersonId($salesPersonID);

        if (count($relatedSalesInvoices) > 0) {
            $this->msg = $this->getMessage('ERR_HAS_RELATED_SALES_INVOICES');
            $this->status = 'false';
            return $this->JSONRespond();
        }

        $isDeleted = $this->CommonTable('User\Model\SalesPersonTable')->deleteSalesPerson($salesPersonID);
        if ($isDeleted) {
            $this->getpaginatedSalesPerson();

            $view = new ViewModel(array(
                'salesPersons' => $this->paginator
            ));

            $view->setTerminal(true);
            $view->setTemplate('user/sales-person/index');

            $this->status = true;
            $this->data = '';
            $this->html = $view;
            $this->msg = $this->getMessage('SUCC_SALESPERSONCON_SALESPER_DELETE');
            return $this->JSONRespondHtml();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_SALESPERSONCON_SALESPER_DELETE');
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $salesPersonID
     */
    public function editAction()
    {
        $this->getSideAndUpperMenus('User', 'Sales Person');
        // get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $supplierTitle = $config['personTitle'];
        $activeUsers = $this->CommonTable('User\Model\UserTable')->getActiveUsers();
        $users = array();
        foreach ($activeUsers as $activeUser) {
            $users[$activeUser['userID']] = $activeUser['userUsername'];
        }

        $form = new SalesPersonForm(array(
            'suplierTitle' => $supplierTitle,
            'activeUse' => $users,
        ));
        $translator = new Translator();
        $panelTitle = $translator->translate('Edit Sales Person');
        $viewmodel = new ViewModel(array(
            'form' => $form,
            'panelTitle' => $panelTitle));

        $salesPersonID = $this->params()->fromRoute('param1');
        $result = $this->CommonTable('User\Model\SalesPersonTable')->getSalesPersonByID($salesPersonID);
        $SPDetails = $result->current();
        $form->get('create-sales-person-button')->setAttribute('class', 'hidden');
        $form->get('create-sales-person-button')->setAttribute('type', 'button');
        $form->get('salesPersonID')->setValue($salesPersonID);
        $form->get('salesPersonSortName')->setValue($SPDetails['salesPersonSortName']);
        $form->get('salesPersonTitle')->setValue($SPDetails['salesPersonTitle']);
        $form->get('salesPersonFirstName')->setValue($SPDetails['salesPersonFirstName']);
        $form->get('salesPersonLastName')->setValue($SPDetails['salesPersonLastName']);
        $form->get('salesPersonEmail')->setValue($SPDetails['salesPersonEmail']);
        $form->get('salesPersonAddress')->setValue($SPDetails['salesPersonAddress']);
        $form->get('salesPersonTelephoneNumber')->setValue($SPDetails['salesPersonTelephoneNumber']);
        $form->get('salesPersonActiveUser')->setValue($SPDetails['userID']);

        $viewmodel->setTemplate('user/sales-person/create-sales-person');
        return $viewmodel;
    }

    public function editSaveAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        $result = $this->CommonTable('User\Model\SalesPersonTable')->searchBySortName($post['salesPersonSortName']);
        $isExistsName = $result->current();
        if ($isExistsName == NULL || $isExistsName['salesPersonID'] == $post['salesPersonID']) {
            $postData['salesPersonID'] = $post['salesPersonID'];
            $postData['salesPersonSortName'] = $post['salesPersonSortName'];
            $postData['salesPersonTitle'] = $post['salesPersonTitle'];
            $postData['salesPersonFirstName'] = $post['salesPersonFirstName'];
            $postData['salesPersonLastName'] = $post['salesPersonLastName'];
            $postData['salesPersonEmail'] = $post['salesPersonEmail'];
            $postData['salesPersonTelephoneNumber'] = $post['salesPersonTelephoneNumber'];
            $postData['salesPersonAddress'] = $post['salesPersonAddress'];
            $postData['userID'] = $post['activeUserId'];

            $salesPerson = new SalesPerson();
            $salesPerson->exchangeArray($postData);
            $result = $this->CommonTable('User\Model\SalesPersonTable')->updateSalesPerson($salesPerson);

            $this->status = true;
            $this->msg = $this->getMessage('SUCC_SALESPERSONCON_SALESPER_UPDATE');
            $this->flashMessenger()->addMessage($this->getMessage('SUCC_SALESPERSONCON_SALESPER_UPDATE'));
            return $this->JSONRespond();
        } else {
            $this->status = false;
            $this->data = '';
            $this->msg = $this->getMessage('ERR_SALESPERSONCON_SNAME_EXIST');
            return $this->JSONRespondHtml();
        }
    }

    private function getpaginatedSalesPerson($size = 10)
    {
        $this->paginator = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($size);
    }

    public function searchSalesPersonAction($keyword = NULL)
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $keyword = $searchrequest->getPost('keyword');
            $this->getpaginatedSalesPersonBySearch($keyword);
            $view = new ViewModel(array(
                'salesPersons' => $this->searchPaginator
                    )
            );
            $view->setTerminal(true);
            $view->setTemplate("user/sales-person/index");
            $this->status = true;
            $this->html = $view;
            return $this->JSONRespondHtml();
        }
    }

    public function getpaginatedSalesPersonBySearch($keyword, $size = 10)
    {
        $this->searchPaginator = $this->CommonTable('User\Model\SalesPersonTable')->searchSelesPersonByKeyword($keyword, true);
        $this->searchPaginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->searchPaginator->setItemCountPerPage($size);
    }

    /**
     * get sales person list
     * @return JsonModel
     */
    public function searchSalesPersonForDropdownAction()
    {
        $this->status = false;
        $this->msg = 'invaid request';
        $this->data = null;

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $salesPersons = $this->CommonTable("User/Model/SalesPersonTable")->searchSalesPersonsForDropDown($searchKey);
            $$salesPersonList = array();
            foreach ($salesPersons as $salesPerson) {
                $temp['value'] = $salesPerson['salesPersonID'];
                $temp['text'] = $salesPerson['salesPersonFirstName'] . ' ' . $salesPerson['salesPersonLastName'] . ' (' . $salesPerson['salesPersonSortName'] . ')';
                $customerList[] = $temp;
            }
            $this->status = true;
            $this->msg = 'sales person list retrived';
            $this->data = array('list' => $customerList);
        }

        return $this->JSONRespond();
    }

    /**
     * get sales person list
     * @return JsonModel
     */
    public function changeSalesPersonStatusAction()
    {
       if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $salesPersonID = $postData['salesPersonID'];
            $changeStatusTo = $this->getStatusID($postData['changeStatusTo']);

            if ($this->CommonTable('User/Model/SalesPersonTable')->changeStatus($changeStatusTo, $salesPersonID)) {
                $this->status = TRUE;
            } else {
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

}
