<?php

namespace User\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;

/**
 *
 * PosDashboardRestfullController
 *
 *
 * Restfull controller for products
 */
class PosUsersRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList($searchKey = "", $timestamp = "")
    {

        $this->service = $this->getService('MobileDashBoardService');
        // $locationID = $this->getRequest()->getHeaders()->get("Locationid")->getFieldValue();

        $userData = $this->service->getUserDetails();
        // var_dump($userData).die();
        if ($userData) {
            return $this->returnJsonSuccess($userData, "SUCCESS");
        }

        return $this->returnJsonError("ERROR");
    }

    public function get($id)
    {

    }

    public function create($data)
    {
        # code...
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
