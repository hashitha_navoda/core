<?php

namespace User\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;

/**
 *
 * PosDashboardRestfullController
 *
 *
 * Restfull controller for products
 */
class PosDashboardRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList($searchKey = "", $timestamp = "")
    {
        $this->service = $this->getService('MobileDashBoardService');
        $locationID = $this->getRequest()->getHeaders()->get("Locationid")->getFieldValue();

        $dashbardData = $this->service->getDashBoardDataForPOS($locationID);
        if ($dashbardData) {
            return $this->returnJsonSuccess($dashbardData, "SUCCESS");
        }

        return $this->returnJsonError("ERROR");
    }

    public function get($id)
    {

    }

    public function create($data)
    {
        # code...
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
