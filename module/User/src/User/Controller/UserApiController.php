<?php

/**
 * @author PRASA7  <prasanna@thinkcube.com>
 * This file contains User related api functions
 */

namespace User\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use User\Form\UserForm;
use User\Form\UserEditForm;
use User\Model\User;
use Core\Model\Licence;
use Core\Model\Notice;
use Core\Model\UserNotice;
use User\Model\UserSetting;
use Zend\View\Model\JsonModel;

class UserAPIController extends CoreController
{

    protected $userID;
    protected $user_session;
    protected $paginator;
    protected $_sm;
    protected $userTable;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
    }

    public function isFirstLogin()
    {
        $user = $this->getUserTable()->getUser($this->userID);
        return $user->userFirstLogin;
    }

    /**
     * This function return the user create form
     * @author PRASA7  <prasanna@thinkcube.com>
     * @return \Zend\View\Model\ViewModel
     */
    public function getCreateFormAction()
    {

        $roleRes = $this->CommonTable('User\Model\RoleTable')->fetchAll();

        $roles = array();
        foreach ($roleRes as $role) {
            $roles[$role->roleID] = $role->roleName;
        }

        $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyLicence();
        $licenses = array();
        $isEmptyLicense = FALSE;
        if (empty($emptyLicenses)) {
            $isEmptyLicense = TRUE;
        }

        $licenses[0] = ' ';
        foreach ($emptyLicenses as $emptyLicense) {
            $licenses[$emptyLicense['licenceID']] = $emptyLicense['licenceName'] . '  ' . $emptyLicense['licenceExpireDate'];
        }

        $form = new UserForm($roles, $licenses);

        $viewmodel = new ViewModel(array(
            'form' => $form,
            'isEmptyLicense' => $isEmptyLicense,
        ));

        $viewmodel->setTerminal(true);
        $viewmodel->setTemplate('user/user-api/user-create-form');
        $this->status = TRUE;
        $this->html = $viewmodel;
        return $this->JSONRespondHtml();
    }

    /////////////////////////END USER CREATE FORM/////////////////////////

    public function getDeleteConfirmBoxAction()
    {
        $viewmodel = new ViewModel();
        $viewmodel->setTerminal(true);
        $viewmodel->setTemplate('user/user-api/user-delete-confirm-box');
        return $viewmodel;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * this for call madal of user feedback/index
     * @return \Zend\View\Model\ViewModel feedback modal
     */
    public function userfeedbackAction()
    {
        $viewModel = new ViewModel();
        $request = $this->getRequest();
        $viewModel->setTemplate('user/feedback/index')
                // only show if call from js
                ->setTerminal($request->isXmlHttpRequest());
        return $viewModel;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function changeUserStatusAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {

            $userID = $searchrequest->getPost('userID');
            $user = $this->getUserTable()->getUser($userID);
            $loginUserRoleID = $this->getUserTable()->getUser($this->userID)->roleID;
            $currentStatus = $user->userActivated;
            if ($loginUserRoleID != 1) {
                $this->status = FALSE;
                $this->msg = 'Sorry, Only admin can change the user status.';
                return $this->JSONRespond();
            }

            if ($userID == $this->userID) {
                $this->status = FALSE;
                $this->msg = 'You Cannot modify your own status.!';
                return $this->JSONRespond();
            }
            if ($currentStatus == '1') {
                $userActivated = '0';
                $this->getUserTable()->updateUserActive($userID, $userActivated);
                $this->status = TRUE;
                $this->msg = 'User deactivated successfully.';
                $this->data = 'unChecked';
                return $this->JSONRespond();
            } else if ($currentStatus == '0') {
                /**
                  $companyLicenceCount = $this->getCompanyDetails()[0]->licenceCount;
                  $activeUser = $this->getUserTable()->getActiveUsers();
                  if ((count($activeUser) == $companyLicenceCount) || (count($emptylicence) <= 0)) {
                  $config = $this->getServiceLocator()->get('config');
                  $this->status = TRUE;
                  $this->data = $config['licence']['portalURL'];
                  $this->msg = 'Sorry, You don\'t have enough licence to activate this user';
                  return $this->JSONRespond();
                  }

                  $licenceID = $emptylicence[0]['licenceID'];
                  $this->getLicenceTable()->assignLicenceForUserID($licenceID, $userID);
                 * */
                $userActivated = '1';
                $this->getUserTable()->updateUserActive($userID, $userActivated);
                $this->status = TRUE;
                $this->msg = 'User activated successfully.';
                $this->data = 'checked';
                return $this->JSONRespond();
            }
        }
        $this->msg = 'Cannot update the user status.';
        $this->status = FALSE;
        return $this->JSONRespond();
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param array  $licenses array
     * $licence [licenceName|licenceStartingDate|licenceExpireDate]
     */
    public function createLicencesAction()
    {
        $request = $this->getRequest();
        $licencesEncoded = $request->getPost('licences');

        $licences = json_decode($licencesEncoded);

        if (!empty($licences)) {
            foreach ($licences as $licence) {
                $licence = (array) $licence;
                //check license exists

                $data = array(
                    'licenceName' => $licence['licenceName'],
                    'licenceStartingDate' => $licence['licenceStartingDate'],
                    'licenceExpireDate' => $licence['licenceExpireDate'],
                );

                $licenceCheck = $this->CommonTable('Core\Model\LicenceTable')->getLicenceByLicenseName($licence['licenceName']);
                //if license exists update it
                if ($licenceCheck) {
                    if (!$this->CommonTable('Core\Model\LicenceTable')->updateLicenceByName($data, $licence['licenceName'])) {
                        $this->httpRespond(406, 'Unable to update licenses');
                    }
                } else {
                    $licenceObj = new Licence();
                    $licenceObj->exchangeArray($data);
                    if (!$this->CommonTable('Core\Model\LicenceTable')->saveLicence($licenceObj)) {
                        $this->httpRespond(406, 'Unable to save licenses');
                    }
                }
            }
            $this->httpRespond(200, 'Successfully saved licenses');
        } else {
            $this->httpRespond(406, 'No license data to create');
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param string $username
     * @param string $userEmail
     * @param string $companyName
     * @param boolean $companyAccountType
     * @param int $companyGracePeriod
     * @param timestamp $companyLicenseExpireDate
     * This function creates user admin and assing a license and assing a location
     */
    public function initializeAccountAction()
    {
        $request = $this->getRequest();
        $userName = $request->getPost('username');
        $userEmail = $request->getPost('userEmail');
        $companyName = $request->getPost('companyName');
        $companyAccountType = $request->getPost('companyAccountType');
        $companyGracePeriod = $request->getPost('companyGracePeriod');
        $package = $request->getPost('package');
        $apiKey = $request->getPost('apiKey');
        $companyLicenseExpireDate = $request->getPost('companyLicenseExpireDate');
        if ($userName == '' || $userEmail == '' || $companyName == '' || $companyAccountType == '' || $companyGracePeriod == '' || $companyLicenseExpireDate == '') {
            $this->httpRespond(406, 'Required fiels were missing');
        }

        $licencesEncoded = $request->getPost('licences');

        $licences = json_decode($licencesEncoded);

        $this->beginTransaction();

        if (!empty($licences)) {
            foreach ($licences as $licence) {
                $licence = (array) $licence;
                $data = array(
                    'licenceName' => $licence['licenceName'],
                    'licenceStartingDate' => $licence['licenceStartingDate'],
                    'licenceExpireDate' => $licence['licenceExpireDate'],
                );
                $licenceObj = new Licence();
                $licenceObj->exchangeArray($data);
                if (!$this->CommonTable('Core\Model\LicenceTable')->saveLicence($licenceObj)) {
                    $this->rollback();
                    $this->httpRespond(406, 'Unable to save licenses');
                }
            }
        } else {
            $this->rollback();
            $this->httpRespond(406, 'No license data to create');
        }


        $roleRes = $this->CommonTable('User\Model\RoleTable')->fetchAll();

        $roles = array();
        foreach ($roleRes as $role) {
            $roles[$role->roleID] = $role->roleName;
        }

        $form = new UserForm($roles);
        if ($request->isPost()) {

            $passwordSalt = md5(mt_rand());
            $passwordString = $this->_generateRandomString(6);
            $password = md5($passwordString . $passwordSalt);
            $userPassword = $password . ':' . $passwordSalt;


            $data = array(
                'userUsername' => $userName,
                'userFirstName' => 'User',
                'userLastName' => 'Last Name',
                'userEmail1' => $userEmail,
                'roleID' => '1',
                'userPassword' => $userPassword,
                'userTypeID' => '1',
                'defautLocation' => '1'
            );
            $user = new User();
            $form->setInputFilter($user->getInputFilter());
            $form->getInputFilter()->remove('licenseID');
            $form->setData($data);
            $checkUser = $this->CommonTable('User\Model\UserTable')->getUserByUsername($userName);
            if ($checkUser) {
                $this->rollback();
                $this->httpRespond(406, 'Username already exists');
            }
            if ($form->isValid()) {
                $user->exchangeArray($data);
                $entityID = $this->createEntity();
                $insertedUserID = $this->CommonTable('User\Model\UserTable')->saveUser($user, $entityID);
                /**
                 * @author sandun dissanyake <sandun@thinkcube.com>
                 * insert into UserSetting Table to widgets list, related to last Registered user which currently available on application
                 */
                $this->CommonTable('User\Model\UserSettingTable')->updateWidget(json_encode(UserSetting::$leftWidgetArray), json_encode(UserSetting::$rightWidgetArray), $insertedUserID);
                if (!$insertedUserID) {
                    $this->rollback();
                    $this->httpRespond(406, 'Unable to create the user');
                }
                $licenses = $this->CommonTable('Core\Model\LicenceTable')->fetchAll();
                if (isset($licenses[0])) {
                    if (!$this->CommonTable('Core\Model\LicenceTable')->assignLicenceForUserID($licenses[0]['licenceID'], $insertedUserID)) {
                        $this->rollback();
                        $this->httpRespond(406, 'Unable to assign a license to the admin');
                    }
                } else {
                    $this->rollback();
                    $this->httpRespond(406, 'No license to assign the user');
                }
            }

            //Allocate location
            $userLocation = new \Core\Model\UserLocation;
            $userDetails = $this->CommonTable('User\Model\UserTable')->getUserByUsername($userName);
            $userLocation->exchangeArray(array(
                'userID' => $userDetails->userID,
                'locationID' => 1
                    )
            );
            $res2 = $this->CommonTable('Core\Model\UserLocationTable')->saveUserLocation($userLocation);

            //Add company Name
            $company = new \Settings\Model\Company;
            //set post data to exchange array
            $companyData = array(
                'companyName' => $companyName,
                'companyAddress' => '',
                'apiKey' => $apiKey,
                'companyLicenseExpireDate' => $companyLicenseExpireDate,
                'companyAccountType' => $companyAccountType,
            );
            $company->exchangeArray($companyData);
            //pass data to saveCompany function for add data to database
            $comanyID = $this->CommonTable('CompanyTable')->saveCompany($company);

            $packageData = array(
                'packageID' => $package,
                'jobModuleEnabled' => 0,
                'crmModuleEnabled' => 1
            );

            $savePackageID = $this->CommonTable('Settings\Model\DisplaySetupTable')->updateDisplaySetupDetails($packageData, 1);

            if ($res2 && $comanyID) {
                $this->commit();
                $domain = 'https://' . $_SERVER['HTTP_HOST'];

                // Generate HTML email template
                $initiateLoginView = new ViewModel(array(
                    'name' => $userName,
                    'username' => $userName,
                    'password' => $passwordString,
                    'domain' => $domain
                ));
                $initiateLoginView->setTemplate('/core/email/account-creation');

                $initiateLoginHtml = $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($initiateLoginView);

                // Get plain text email template
                $initiateLoginViewPlain = new ViewModel(array(
                    'name' => $userName,
                    'username' => $userName,
                    'password' => $passwordString,
                    'domain' => $domain
                ));
                $initiateLoginViewPlain->setTemplate('/core/email/account-creation-plain');

                $initiateLoginPlainText = $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($initiateLoginViewPlain);

                $this->sendEmail($userEmail, 'Login to your ezBiz account', array($initiateLoginHtml, $initiateLoginPlainText));
                $this->httpRespond(200, 'Successfully data inserted');
            } else {
                $this->rollback();
                $this->httpRespond(406, 'Data not matched');
            }
        } else {
            $this->httpRespond(400, 'Post data is null');
        }
    }

    /**
     * @author Damith Thamara<damith@thinkcube.com>
     * This function returns user list with licence details
     */
    public function getUserListAction()
    {
        $userList = $this->CommonTable('User\Model\UserTable')->getAllUsersWithLicenseData();
        if ($userList) {
            return new JsonModel($userList);
        } else {
            $this->httpRespond(406, 'Empty user data');
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param timestamp $liceseExpireDate
     */
    public function updateUserLicenseExpireDateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $licenceExpireDate = $request->getPost('licenceExpireDate');
            $licenceName = $request->getPost('licenceName');
            if ($licenceExpireDate && $licenceName) {
                $licenseCheck = $this->CommonTable('Core\Model\LicenceTable')->getLicenceByLicenseName($licenceName);
                if ($licenseCheck) {
                    $data = array(
                        'licenceExpireDate' => $licenceExpireDate
                    );
                    $this->CommonTable('Core\Model\LicenceTable')->updateLicenceByName($data, $licenceName);
                    $this->httpRespond(200, 'Successfully updated license');
                } else {
                    $data = array(
                        'licenceName' => $licenceName,
                        'licenceStartingDate' => $this->getGMTDateTime(),
                        'licenceExpireDate' => $licenceExpireDate,
                    );
                    $licenceObj = new Licence();
                    $licenceObj->exchangeArray($data);
                    if (!$this->CommonTable('Core\Model\LicenceTable')->saveLicence($licenceObj)) {
                        $this->httpRespond(406, 'Unable to save licenses');
                    }
                    $this->httpRespond(200, 'Successfully updated license');
                }
            } else {
                $this->httpRespond(406, 'Unacceptable value for  license expire date and license name');
            }
        } else {
            $this->httpRespond(400, 'Empty input params');
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param timestamp $liceseExpireDate
     */
    public function updateCompanyLicenseExpireDateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $companyLicenceExpireDate = $request->getPost('companyLicenseExpireDate');
            if ($companyLicenceExpireDate) {
                $data = array(
                    'companyLicenseExpireDate' => $companyLicenceExpireDate
                );
                $this->CommonTable('CompanyTable')->updateCompanyDetails($data);
                $this->httpRespond(200, 'Successfully updated license');
            } else {
                $this->httpRespond(406, 'Unacceptable value for  license expire date');
            }
        } else {
            $this->httpRespond(400, 'Empty input params');
        }
    }

    /**
     * @author Damith Thamara<damith@thinkcube.com>
     * This function uses as a rest call to push notices to user
     * @param string  $notice
     * @param string  $noticeType
     */
    public function addNoticeAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->httpRespond(400, 'Empty input params');
        }

        $test = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getValidFiscalPeriod('2018-01-10');
        $company = $this->CommonTable('CompanyTable')->fetchAll()->current();


        $res = $this->getService('NoticeService')
            ->createNotice($request->getPost());

        $this->httpRespond($res['status'] ? 200: 406, $res['msg']);
    }

    /**
     * @author Damith Thamara<damith@thinkcube.com>
     * This function uses as a rest call to delete notices from user
     * @param string  $notice
     * @param string  $noticeType
     */
    public function deleteNoticeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $noticeName = $request->getPost('noticeName');
            if ($noticeName == '') {
                $this->httpRespond(406, 'Unacceptable vales for notice name');
            } else {
                $noticeID = $this->CommonTable('Core\model\NoticeTable')->getNoticeIDByName($noticeName)->noticeID;
                if ($noticeID) {
                    $this->CommonTable('Core\model\UserNoticeTable')->deleteUserNoticeByID($noticeID);
                    if ($this->CommonTable('Core\model\NoticeTable')->deleteNoticeByID($noticeID)) {
                        $this->httpRespond(200, 'successfully deleted the notice');
                    } else {
                        $this->httpRespond(406, 'Unable to delete the notice');
                    }
                } else {
                    $this->httpRespond(406, 'Unable to find a notice by name');
                }
            }
        } else {
            $this->httpRespond(400, 'Empty input params');
        }
    }

    /**
     * @author Damith Thamara<damith@thinkcube.com>
     */
    public function removeNoticeForUserAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $noticeID = $request->getPost('noticeID');
            $data = array(
                'userID' => $this->userID,
                'noticeID' => $noticeID
            );
            $userNotice = new UserNotice();
            $userNotice->exchangeArray($data);
            if ($this->CommonTable('Core\model\UserNoticeTable')->saveUserNotice($userNotice)) {
                $this->httpRespond(200, 'Data inserted successfully');
            }
        } else {
            $this->httpRespond(400, 'Empty input params');
        }
    }

    public function getLicenceTable()
    {
        if (!$this->licenceTable) {
            $sm = $this->getServiceLocator();
            $this->licenceTable = $sm->get('Core\Model\LicenceTable');
        }

        return $this->licenceTable;
    }

    public function getUserTable()
    {
        if (!$this->userTable) {
            $sm = $this->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }

    public function getUsersAction()
    {
        $users = [];
        foreach ($this->CommonTable('User\Model\UserTable')->fetchAll() as $user) {
            $users[] = $user;
        }

        $this->status = true;
        $this->data = $users;

        return $this->JSONRespond($users);
    }

    public function getuserfromsearchAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {

            $searchKey = $searchrequest->getPost('searchKey');
            if ($searchKey != '') {
                $users = $this->CommonTable('User\Model\UserTable')->getUserforSearch($searchKey);
                $searchview = new ViewModel(array(
                    'users' => $users,
                    'paginated' => false
                        )
                );
                $searchview->setTerminal(true);
                $searchview->setTemplate('user/user/index');
                $this->html = $searchview;
            } else {
                $this->getPaginatedUsers();
                $searchview = new ViewModel(array(
                    'users' => $this->paginator
                        )
                );
                $searchview->setTerminal(true);
                $searchview->setTemplate('user/user/index');
                $this->html = $searchview;
            }
            return $this->JSONRespondHtml();
        }
    }

    public function userUpdateAction()
    {

        $roleRes = $this->CommonTable('User\Model\RoleTable')->fetchAll();
        $roles = array();
        foreach ($roleRes as $role) {
            $roles[$role->roleID] = $role->roleName;
        }

        $updaterequest = $this->getRequest();
        if ($updaterequest->isPost()) {
            //Update user in AppEngine
            $updatePasswordFlag = TRUE;
            $passwordValidation = FALSE;
            $currentPassword = $updaterequest->getPost('currentPassword');
            $newPassword = $updaterequest->getPost('userPassword');
            $newPasswordConfirm = $updaterequest->getPost('passwordC');
            if ($currentPassword == '' || $newPassword == '' || $newPasswordConfirm == '') {
                $updatePasswordFlag = FALSE;
            }
            $updateUserID = $updaterequest->getPost('userID');
            $licenseID = $updaterequest->getPost('licenseID');
            $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyAndAssignedLicenceByUserID($updateUserID);
            $licenses = array();
            $licenses[0] = ' ';
            foreach ($emptyLicenses as $emptyLicense) {
                $licenses[$emptyLicense['licenceID']] = $emptyLicense['licenceName'] . '  ' . $emptyLicense['licenceExpireDate'];
            }

            $userTypes = [
                1 => "ezBiz System User",
                2 => "POS User"
            ];

            $userform = new UserEditForm($roles, $licenses, $userTypes);

            $existingUserDetails = $this->CommonTable('User\Model\UserTable')->getUser($updateUserID);
            if ($updatePasswordFlag) {
                $passwordData = explode(':', $existingUserDetails->userPassword);
                $storedPassword = $passwordData[0];

                if ($storedPassword == md5($currentPassword . $passwordData[1])) {
                    if ($newPassword == $newPasswordConfirm) {
                        if ($storedPassword == md5($newPassword . $passwordData[1])) {
                            $this->status = FALSE;
                            $this->msg = $this->getMessage('ERR_USERAPICONTRO_PWD_REPEAT');
                            $this->data = FALSE;
                            return $this->JSONRespond();
                        } else if (!preg_match('/^(?=.*[A-Z])(?=.*[a-z])(?=.*\d).{8,20}$/', $newPassword)) {
                            $this->status = FALSE;
                            $this->msg = $this->getMessage('ERR_USER_PASS_FORMAT');
                            $this->data = FALSE;
                            return $this->JSONRespond();
                        } else {
                            $passwordValidation = TRUE;
                        }
                    } else {
                        $this->status = FALSE;
                        $this->msg = $this->getMessage('ERR_USERAPICONTRO_PWD_PASSNOTMATCH');
                        $this->data = FALSE;
                        return $this->JSONRespond();
                    }
                } else {
                    $this->status = FALSE;
                    $this->msg = $this->getMessage('ERR_USERAPICONTRO_PWD_NOTCORRECT');
                    $this->data = FALSE;
                    return $this->JSONRespond();
                }
            }

            $userupdatemodel = new User();
            $userform->setInputFilter($userform->getInputFilter());
            $userform->getInputFilter()->remove('licenseID');
            $userform->getInputFilter()->remove('userTypeID');
            if (!$updaterequest->getPost('roleID')) {
                $userform->getInputFilter()->remove('roleID');
            }
            $userform->setData($updaterequest->getPost());
            if ($userform->isValid()) {
                $userupdatemodel->exchangeArray($updaterequest->getPost());
                $userupdatemodel->username = $existingUserDetails->username;
                $userupdatemodel->defaultLanguage = $existingUserDetails->defaultLanguage;

                $defaultLanguage;
                if (is_null($updaterequest->getPost('defaultLanguage'))) {
                    $defaultLanguage = $existingUserDetails->defaultLanguage;
                } else {
                    $defaultLanguage = $updaterequest->getPost('defaultLanguage');
                }

                if ($passwordValidation) {
                    $passwordSalt = md5(mt_rand());
                    $Hashedpassword = md5($newPassword . $passwordSalt);
                    $newCombinedPassword = $Hashedpassword . ':' . $passwordSalt;
                    $data = array(
                        'userUsername' => $userupdatemodel->username,
                        'userFirstName' => $userupdatemodel->firstName,
                        'userLastName' => $userupdatemodel->lastName,
                        'userEmail1' => $userupdatemodel->email1,
                        'userTypeID' => $userupdatemodel->userTypeID,
                        'userPassword' => $newCombinedPassword,
                        'defaultLanguage' => $defaultLanguage,
                        'roleID' => $userupdatemodel->roleID,
                    );
                } else {
                    $data = array(
                        'userUsername' => $userupdatemodel->username,
                        'userFirstName' => $userupdatemodel->firstName,
                        'userLastName' => $userupdatemodel->lastName,
                        'userEmail1' => $userupdatemodel->email1,
                        'userTypeID' => $userupdatemodel->userTypeID,
                        'defaultLanguage' => $defaultLanguage,
                        'roleID' => $userupdatemodel->roleID,
                    );
                }
                
                $updateStatus = $this->CommonTable('User\Model\UserTable')->updateUser($data, $userupdatemodel->ID);
                
                if ($updateUserID == $this->userID) {
                    $this->user_session->fullName = $userupdatemodel->firstName;
                    $updatedLanguage = $defaultLanguage;


                    $this->user_session->lang = $updatedLanguage;
                }
                $username = $existingUserDetails->username;

                if ($updateStatus == FALSE) {
                    $this->setLogMessage("Update failed");
                    $this->msg = $this->getMessage('ERR_USERAPICONTRO_UPDATE_FAIL', array($username));
                    $this->status = FALSE;
                    $this->data = FALSE;
                    return $this->JSONRespond();
                } else {
//              add / delete license to user
                    if (isset($licenseID) && $licenseID == 0) {
                        $this->CommonTable('Core\Model\LicenceTable')->makeEmptyLicenceByUserID($updateUserID);
                    } else if (isset($licenseID)) {
                        $this->CommonTable('Core\Model\LicenceTable')->makeEmptyLicenceByUserID($updateUserID);
                        $this->CommonTable('Core\Model\LicenceTable')->assignLicenceForUserID($licenseID, $updateUserID);
                    }
                    $this->getPaginatedUsers();
                    $view = new ViewModel(array(
                        'users' => $this->paginator
                    ));
                    $view->setTemplate('user/user/index');

                    $this->setLogMessage("Updated");
                    $this->msg = $this->getMessage('SUCC_USERAPICONTRO_UPDATE', array($username));
                    $this->status = TRUE;
                    $this->data = TRUE;
                    $this->html = $view;
                    $this->flashMessenger()->addMessage($this->msg);
                    return $this->JSONRespondHtml();
                }
            } else {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_USERAPICONTRO_INVALID_DATA');
                $this->data = FALSE;
                return $this->JSONRespond();
            }
        }
    }

    public function getuserdetailsAction()
    {
        $a = array();
        $re = $this->CommonTable('User\Model\UserTable')->fetchAll();
        while ($row = $re->current())
            $a[] = $row;
        return new JsonModel($a);
    }

    public function getUserEditFormAction()
    {

        $roleRes = $this->CommonTable('User\Model\RoleTable')->fetchAll();

        $roles = array();
        foreach ($roleRes as $role) {
            $roles[$role->roleID] = $role->roleName;
        }

        $request = $this->getRequest();
        if ($request->isPost()) {
            $userID = $request->getPost('userID');
//          Add license to drop down list
            $emptyLicenses = $this->CommonTable('Core\Model\LicenceTable')->getEmptyAndAssignedLicenceByUserID($userID);
            $licenses = array();
            $isEmptyLicense = FALSE;
            if (empty($emptyLicenses)) {
                $isEmptyLicense = TRUE;
            }

            $licenses[0] = ' ';
            foreach ($emptyLicenses as $emptyLicense) {
                $licenses[$emptyLicense['licenceID']] = $emptyLicense['licenceName'] . '  ' . $emptyLicense['licenceExpireDate'];
            }
            $userLicense = $this->CommonTable('Core\Model\LicenceTable')->getLicenceByUserID($userID);
            foreach ($userLicense as $lice) {
                $userLicenseID = $lice['licenceID'];
            }

            $form = new UserEditForm($roles, $licenses);
            $rett = $this->CommonTable('User\Model\UserTable')->getUser($userID);
            $form->get('userID')->setValue($userID);
            $form->get('userFirstName')->setValue($rett->firstName);
            $form->get('userLastName')->setValue($rett->lastName);
            $form->get('userEmail1')->setValue($rett->email1);
            $form->get('roleID')->setValue($rett->roleID);
            $form->get('licenseID')->setValue(isset($userLicenseID) ? $userLicenseID : null);
            $viewmodel = new ViewModel(array(
                'form' => $form,
                'username' => $rett->username,
                'isEmptyLicense' => $isEmptyLicense
            ));
            $viewmodel->setTerminal(true);
            return $viewmodel;
        }
    }

    public function getUserLocationAllocateViewAction()
    {
        $locations = $this->CommonTable('Core\Model\LocationTable')->getActiveNonDeletedLocations();
        $viewModel = new ViewModel(array(
            'locations' => $locations
        ));
        $viewModel->setTemplate("user/user-api/user-location-allocate-view");
        $viewModel->setTerminal(true);
        $this->html = $viewModel;

        return $this->JSONRespondHtml();
    }

    public function getUserLocationEditViewAction()
    {
        $request = $this->getRequest();
        $locations = $this->CommonTable('Core\Model\LocationTable')->getUserAllocatedLocationsByuserID($request->getPost('userID'));
        $defaultLocation = $this->CommonTable('User\Model\UserTable')->getDefaultLocationsByuserID($request->getPost('userID'));

        $viewModel = new ViewModel(array(
            'locations' => $locations,
            'defaultLocation' => $defaultLocation,
            'userID' => $request->getPost('userID')
        ));

        $viewModel->setTemplate("user/user-api/user-location-edit-view");
        $viewModel->setTerminal(true);
        $this->html = $viewModel;

        return $this->JSONRespondHtml();
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * @param String $userID
     */
    public function deleteAction($userID = NULL)
    {

        $userID = $this->params()->fromRoute('param1', NULL);
        if (!is_null($userID)) {
            $this->CommonTable('User\Model\UserTable')->deleteUser($userID);
        }
        return $this->redirect()->toRoute('user');
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     */
    public function getAllLoggerDetailsAction()
    {
        $res = $this->CommonTable('User\Model\LoggerTable')->fetchAll();
        while ($row = $res->current())
            $logs[] = $row;
        $this->data = $logs;
        return $this->JSONRespond();
    }

    /**
     * @author Damith Karunathilaka <damith@thinkcube.com>
     * This is the user role add function
     * @return type
     */
    public function addRoleAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $roleName = $request->getPost('roleName');
            $checkedFeatures = $request->getPost('featureList');
            $checkRoleExist = $this->CommonTable('User\Model\RoleTable')->getRoleIdByName($roleName);
            if ($checkRoleExist == TRUE) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_USERAPICONTRO_ROLEEXIST');
                $this->data = FALSE;
            } else {
                $insertedRoleID = $this->CommonTable('User\Model\RoleTable')->addRole($roleName);
                $allFeatures = $this->CommonTable('User\Model\FeatureTable')->fetchAll();
                foreach ($allFeatures as $feature) {
                    $enable = 1;
                    if ($feature->featureType == 0 && !in_array($feature->featureID, $checkedFeatures)) {
                        $enable = 0;
                    }
                    $this->CommonTable('User\Model\RoleFeatureTable')->addRoleFeature($insertedRoleID, $feature->featureID, $enable);
                }
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_USERAPICONTRO_ROLEADD');
                $this->data = TRUE;
                $this->flashMessenger()->addMessage($this->msg);
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Damith Karunathilaka <damith@thinkcube.com>
     * This is the user role update function
     * @return type
     */
    public function changeRolePermissionAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {

            $roleName = $request->getPost('roleName');
            $checkedFeatures = $request->getPost('featureList');

            $allRoleFeatures = $this->CommonTable('User\Model\RoleFeatureTable')->getNotApiRoleFeaturesByRole($roleName);
            foreach ($allRoleFeatures as $feature) {
                $enable = 0;
                if (in_array($feature['featureID'], $checkedFeatures)) {
                    $enable = 1;
                }
                $this->CommonTable('User\Model\RoleFeatureTable')->updateRoleFeature($feature['roleFeatureID'], $enable);
            }
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_USERAPICONTRO_ROLEUPDATE');
            $this->data = TRUE;
            $this->data = TRUE;
            $this->flashMessenger()->addMessage($this->msg);

            return $this->JSONRespond();
        }
    }

    public function creditLimitExceedApprovalAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $username = trim($request->getPost('username'));
            $password = trim($request->getPost('password'));
            $featureIDForExceed = $this->CommonTable('User\Model\FeatureTable')->getFeatureId('VerifyCreditLimitExceed');
            $result = $this->authenticateAUserForAFeature($username, $password, $featureIDForExceed->featureID);
            if ($result == "authenticated") {
                $this->data = TRUE;
            } else if ($result == "wrongPassword") {
                $this->msg = $this->getMessage('ERR_USERAPICONTRO_INCPWD');
                $this->status = 'error';
            } elseif ($result == "wrongUser") {
                $this->msg = $this->getMessage('ERR_USERAPICONTRO_INCUSRNAME');
                $this->status = 'error';
            }
            return $this->JSONRespond();
        }
    }

    public function authenticateAUserForAFeature($username, $password, $featureID)
    {
        $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
        if ($user) {
            if ($user->password == md5($username . $password . $username)) {
                $availableFeatures = $this->CommonTable('User\Model\RoleFeatureTable')->getEnablesFeatureIDs($user->roleID);
                if (in_array($featureID, $availableFeatures)) {
                    $msg = $this->getMessage('INFO_USERAPICONTRO_AUTH');
                    return $msg;
                }
            } else {
                $msg = $this->getMessage('INFO_USERAPICONTRO_WRPWD');
                return $msg;
            }
        } else {
            $msg = $this->getMessage('INFO_USERAPICONTRO_WRUSR');
            return $msg;
        }
    }

    public function roleDeleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $roleID = $this->CommonTable('User\Model\RoleTable')->getRoleIdByName($request->getPost('roleName'));
            $roleUsers = $this->CommonTable('User\Model\UserTable')->getUsersByRoleID($roleID);
            if (empty($roleUsers)) {
                if ($this->CommonTable('User\Model\RoleFeatureTable')->deleteRoleFeaturesByRoleID($roleID)) {
                    $this->CommonTable('User\Model\RoleTable')->deleteRoleByID($roleID);
                    $this->status = TRUE;
                    $this->msg = $this->getMessage('SUCC_USERAPICONTRO_ROLEDELETE');
                    $this->data = TRUE;
                    $this->flashMessenger()->addMessage($this->msg);
                }
            } else {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_USERAPICONTRO_ROLEDELETE');
                $this->data = FALSE;
            }
        }

        return $this->JSONRespond();
    }

    /**
     * @author Damith Karunathilaka <damith@thinkcube.com>
     */
    public function sendUserFeedbackAction()
    {
        $feedbackRequest = $this->getRequest();
        $postData = $feedbackRequest->getPost();
        $feedbackText = "<h2>" . $this->user_session->companyDetails->companyName . "</h2>";
        $feedbackText = $feedbackText . "<h4>User - " . $this->user_session->username . "</h4>";
        $feedbackText = $feedbackText . "<h2>EzBiz Invoice App Feedback</h2>";
        $feedbackText = $feedbackText . "<table>";
        $feedbackText = $feedbackText . "<tr><td><b>Satisfied With Application : </b></td><td>" . $postData->satisfied . "</td></tr>";
        $feedbackText = $feedbackText . "<tr><td><b>Functionality (out of 5) : </b></td><td>" . $postData->functionality . "</td></tr>";
        $feedbackText = $feedbackText . "<tr><td><b>Usability (out of 5) : </b></td><td>" . $postData->usability . "</td></tr>";
        $feedbackText = $feedbackText . "<tr><td><b>Navigation (out of 5) : </b></td><td>" . $postData->navigation . "</td></tr>";
        $feedbackText = $feedbackText . "<tr><td><b>Loading Time (out of 5) : </b></td><td>" . $postData->loadingTime . "</td></tr>";
        $feedbackText = $feedbackText . "<tr><td><b>Recommend for other businesses  : </b></td><td>" . $postData->recommend . "</td></tr>";
        $feedbackText = $feedbackText . "<tr><td><b>Suggestions  : </b></td><td>" . $postData->suggestions . "</td></tr>";
        $feedbackText = $feedbackText . "</table>";
        $to = "support@ezbizapp.com";
        $fromName = "ezBiz";
        $subject = "Feedback";
        $file = null;
        $Bcc = null;
        $Cc = null;
        $textContent = null;
        $htmlMarkup = $feedbackText;
        $this->sendEmail($to, $subject, $htmlMarkup, $file, $fromName);
        $this->status = TRUE;
        $this->msg = $this->getMessage('SUCC_USERAPICONTRO_FEEDBACK_SENT');
        $this->data = TRUE;

        return $this->JSONRespond();
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function gets paginated users
     */
    private function getPaginatedUsers()
    {
        $paginated = TRUE;
        $this->paginator = $this->CommonTable('User\Model\UserTable')->fetchAll($paginated);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

//////////////////END PAGINATED USERS//////////////////////////

    private function getAllFeatures()
    {


        $res = $this->CommonTable('User\Model\FeatureTable')->fetchAll();
        while ($row = $res->current())
            $roles[] = $row;
        return $roles;
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function is used to change password for a logged in user
     * @return type JSON
     */
    public function changePasswordAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $oldPassword = $request->getPost('currentPassword');
            $newPassword = $request->getPost('userPassword');
            $newPasswordConfirmation = $request->getPost('passwordC');

            if ($this->userID == NULL)
                $this->userID = $request->getPost('userID');

            if (is_null($this->userID)) {
                $this->msg = $this->getMessage('ERR_USERAPICONTRO_UID_REQ');
                $this->status = FALSE;
                return $this->JSONRespond();
            }

            if (($oldPassword == "" ) || ($newPassword == "" ) || ($newPasswordConfirmation == "")) {
                $this->msg = $this->getMessage('ERR_USERAPICONTRO_PWD_CONFMISS');
                $this->status = FALSE;
                return $this->JSONRespond();
            }

            $existingUserDetails = $this->CommonTable('User\Model\UserTable')->getUser($this->userID);

            if ($existingUserDetails->password == md5($existingUserDetails->username . $oldPassword . $existingUserDetails->username)) {

                if ($newPassword == $newPasswordConfirmation) {

                    if ($existingUserDetails->password != md5($existingUserDetails->username . $newPassword . $existingUserDetails->username)) {

//Update password in AppEngine
                        $config = $this->getServiceLocator()->get('Config');
                        $identity = $config['app-engine-config']['sso_api_url'];

                        $userAppEngine = $this->getServiceLocator()->get('AppEngine-User');
                        $userAppEngine->username = $existingUserDetails->username;
                        $userAppEngine->password = md5($newPassword);
                        $userAppEngine->nickname = $existingUserDetails->username;
                        $userAppEngine->identity = $identity;
                        $userAppEngine->key = "username";

                        $appEnginePwdUpdateStatus = $userAppEngine->update();

                        if ($appEnginePwdUpdateStatus) {
                            $this->setLogMessage("Password updated Successfully in AppEngine");
                        } else {
                            $this->setLogMessage("Password update failed in AppEngine");
                        }

//Update password in ezbiz database
                        $userPwdUpdateModel = new User();
                        $userPwdUpdateModel->userID = $this->userID;
                        $userPwdUpdateModel->password = $newPasswordConfirmation;

                        $ezbizPwdUpdateStatus = $this->CommonTable('User\Model\UserTable')->changePassword($userPwdUpdateModel);

                        if ($ezbizPwdUpdateStatus) {
                            $this->setLogMessage("Password updated Successfully");
                        } else {
                            $this->setLogMessage("Password update failed");
                        }

                        if ($appEnginePwdUpdateStatus && $ezbizPwdUpdateStatus) {
                            $this->msg = $this->getMessage('SUCC_USERAPICONTRO_PWD_UPDATE');
                            $this->status = TRUE;
                        } else {
                            $this->msg = $this->getMessage('ERR_USERAPICONTRO_USR_UPDATE');
                            $this->status = FALSE;
                        }
                    } else {
                        $this->msg = $this->getMessage('ERR_USERAPICONTRO_PWD_REUSE');
                        $this->status = FALSE;
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_USERAPICONTRO_PWD_MISMATCH');
                    $this->status = FALSE;
                }
            } else {
                $this->msg = $this->getMessage('ERR_USERAPICONTRO_PWD_INCORRECT');
                $this->status = FALSE;
            }
        } else {
            $this->msg = $this->getMessage('ERR_USERAPICONTRO_DATAPOST');
            $this->status = FALSE;
        }
        return $this->JSONRespond();
    }

//////////////////END PASSWORD CHANGE//////////////////////////

    public function sendMailAction()
    {


        $to = "prasannadangalla@gmail.com";
        $from = "ezbiztc@gmail.com";
        $fromName = "ezBiz Invoice";
        $subject = "ezInvoice";
        $file = "/home/prasa7/Desktop/1.docx";
        $Bcc = "pdmweerasinghe@gmail.com";
        $Cc = "damith.thamara@gmail.com";
        $textContent = "HHHHHH";
        $htmlMarkup = "<h1>HHHHH</h1>";

        $this->SendMail()->sendMailUsingGmail($to, $from, $fromName, $subject, $Bcc, $Cc, $textContent, $htmlMarkup, $file
        );
    }

    public function checkUserByNameAction()
    {

        $res = $this->CommonTable('User\Model\UserTable')->getUserByUsername($this->getRequest()->getPost('username'));
        if (isset($res)) {
            $this->status = TRUE;
        } else {
            $this->status = FALSE;
        }

        return $this->JSONRespond();
    }

    /*
     * @auther Sandun  <sandun@thinkcube.com>
     * This function is used to authenticate, user is completed the wizard
     * or not then rederect to wizard completed state
     */

    public function checkWizardAuthAction()
    {
        $company = $this->CommonTable('CompanyTable')->getWizardState();

        $wizardState = isset($company->wizardState) ? $company->wizardState : 1;
        if ($wizardState == 4) {
            return TRUE;
        } else {
            //get relevant url from wizardState array on ezbiz.config.php
            $wizardToUrl = $this->getServiceLocator()->get('config')['wizardState'][$wizardState];
            //if $wizardState given Url,return that URL
            if ($wizardToUrl) {
                return $wizardToUrl;
            } else {
                return FALSE;
            }
        }
    }

    public function getAuditTrailAction()
    {
        $lastLog = $this->getRequest()->getPost('lastLog');
        $date = ($lastLog == null || $lastLog == 'null') ? null : $lastLog;
        $user_data = null;

        $log = $this->CommonTable('Core\model\LogTable');
        // $logList = $log->getLogUntillToday($date);
        $activity = $log->getLastActivity();
        $lastActivity = $activity[0];


        if ($this->getRequest('fetchUserData')) {
            $company = $this->CommonTable('CompanyTable')->getCompany();

            if ($company->wizardState == 4) {

                $user = $this->getUserTable()->getUserByUsername('admin');

                $user_data = array(
                    'company_name' => $company->companyName,
                    'company_email' => $company->email,
                    'company_phone' => $company->telephoneNumber,
                    'country' => $company->country,
                    'user_email' => ($user != null) ? $user->userEmail1 : ""
                );
            }
        }

        $today = date('Y-m-d');
        $jETransactions = $this->CommonTable('Accounting\Model\JournalEntryTable')->getDailyTransactions($today);

        $dayTransaction['date'] = $today;
        $dayTransaction['count'] = count($jETransactions);

        $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();
        foreach ($fiscalPeriod as $key => $value) {
            if(($value['fiscalPeriodStartDate'] <= $today && $today <= $value['fiscalPeriodEndDate'])){
                if (is_null($value['fiscalPeriodParentID'])) {
                    $lastYearFiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getPreviousFiscalPeriods($value['fiscalPeriodStartDate'], true)->current();
                    $lastYearFiscalPeriodData = [];
                    if ($lastYearFiscalPeriod) {
                        $lastYearFiscalPeriodStatus = ($lastYearFiscalPeriod['fiscalPeriodStatusID'] == "14") ? 'Open' : 'Close';

                        $lastYearFiscalPeriodData['lastYearFiscalPeriodStatus'] = $lastYearFiscalPeriodStatus;
                        $lastYearFiscalPeriodData['lastYearFiscalPeriodID'] = $lastYearFiscalPeriod['fiscalPeriodID'];
                    }
                } else {
                    $lastMonthFiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getPreviousFiscalPeriods($value['fiscalPeriodStartDate'], false)->current();
                    $lastMonthFiscalPeriodData = [];
                    if ($lastMonthFiscalPeriod) {
                        $lastMonthFiscalPeriodStatus = ($lastMonthFiscalPeriod['fiscalPeriodStatusID'] == "14") ? 'Open' : 'Close';

                        $lastMonthFiscalPeriodData['lastMonthFiscalPeriodStatus'] = $lastMonthFiscalPeriodStatus;
                        $lastMonthFiscalPeriodData['lastMonthFiscalPeriodID'] = $lastMonthFiscalPeriod['fiscalPeriodID'];
                    }
                }
            }
        }

        return new JsonModel(array(
            'dayTransaction' => $dayTransaction,
            'lastActivity' => $lastActivity,
            'lastYearFiscalPeriodData' => $lastYearFiscalPeriodData,
            'lastMonthFiscalPeriodData' => $lastMonthFiscalPeriodData,
            'userData' => ($user_data != null) ? json_encode($user_data) : null
        ));
    }

    public function checkDefaultLocationAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $locationDetails = (object) $this->CommonTable("Settings/Model/LocationTable")->getLocation($locationID);
            if ($locationDetails->locationStatus == '0') {
                $this->status = true;
            } else {
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }


    /**
     * This function is used to get all users
     */
    public function getAllUserDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $userData = [];
            $userResult = $this->CommonTable('User\Model\UserTable')->getAllUsersWithLicenseData();
            foreach ($userResult as $key => $value) {
                $userData[$value['userID']] = $value['userFirstName'] . ' ' . $value['userLastName'];
            }

            $this->data = $userData;
            return $this->JSONRespond();
        }
    }

    public function setJobRole($role, $value = false) 
    {
        if ($role == "jobAdmin") {
            $this->user_session->jobAdmin = $value;
        } else if ($role == "jobStoreKeeper") {
            $this->user_session->jobStoreKeeper = $value;
        }
    }
}

/////////////////////////END OF USER API CONTROLLER///////////////////////
