<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 *
 */

namespace User\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\Redirect;

class UserAuthenticate extends AbstractPlugin
{
    /* This function authenticate user features
     * @author Damith Thamara <damith@thinkcube.com>
     * ///INSTRUCTIONS\\\
     * $plugin = $this->UserAuthenticate();
     * $get = $plugin->userAuthenticate($userid,$featurename);
     * E.g. $get = $plugin->userAuthenticate('4', 'EditCompany');
     */

    protected $userTable;
    protected $roleTable;
    protected $featureTable;
    protected $roleFeatureTable;
    protected $user_session;

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
    }

    public function userAuthenticate($UserId, $featureName, $returnTo, $isAPI = false)
    {

        $returnParams = explode('/', $returnTo);
        if (!isset($returnParams[1]))
            $returnParams[1] = NUll;

        if ($returnParams[1] == '') {

            $res = explode("\\", $this->getController()->getEvent()->getRouteMatch('home')->getParam('controller'));
            $returnParams[1] = strtolower($res[2]);
        }
        if (!$isAPI) {
            $this->user_session->returnParamOne = $returnParams[1];

            $this->user_session->returnParamTwo = $returnParams[2] = (isset($returnParams[2]) ? $returnParams[2] : NULL);
        }
        if (is_null($UserId)) {
            $userLoggedIn = FALSE;
        } else {
            $userLoggedIn = TRUE;
            $flag = FALSE;
            if (in_array($featureName, $this->user_session->featureNames)) {
                $flag = TRUE;
            }
        }

        if ($userLoggedIn == TRUE) {
            if ($flag == TRUE) {
                return TRUE;
            } else {
                return $this->getController()->redirect()->toRoute('user', array('action' => 'autherror'));
            }
        } else {
            return $this->getController()->redirect()->toRoute('user', array('action' => 'login', 'param1' => $returnParams[1], 'param2' => $returnParams[2]));
        }
    }

    public function getUserTable()
    {
        if (!$this->userTable) {
            $sm = $this->getController()->getServiceLocator();
            $this->userTable = $sm->get('User\Model\UserTable');
        }
        return $this->userTable;
    }

    public function getRoleTable()
    {
        if (!$this->roleTable) {
            $sm = $this->getController()->getServiceLocator();
            $this->roleTable = $sm->get('User\Model\RoleTable');
        }
        return $this->roleTable;
    }

    public function getFeatureTable()
    {
        if (!$this->featureTable) {
            $sm = $this->getController()->getServiceLocator();
            $this->featureTable = $sm->get('User\Model\FeatureTable');
        }
        return $this->featureTable;
    }

    public function getRoleFeatureTable()
    {
        if (!$this->roleFeatureTable) {
            $sm = $this->getController()->getServiceLocator();
            $this->roleFeatureTable = $sm->get('User\Model\RoleFeatureTable');
        }
        return $this->roleFeatureTable;
    }

/////////////// END USER AUTHENTICATION//////////////////
}

?>
