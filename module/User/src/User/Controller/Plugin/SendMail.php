<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 *
 */

namespace User\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mail\Message;
use Zend\Mime\Message as MimeMessage;
use Zend\Mime\Part as MimePart;
use Zend\Mail\Transport\SmtpOptions;
use AppEngine\Model\Email;

class SendMail extends AbstractPlugin
{
    /* This function send Emails
     * @author Damith Thamara <damith@thinkcube.com>
     * Instructions
     * Add statements to the controller like below
     *  $plugin = $this->SendMail();
     *  $plugin->sendMail();
     */

    /**
     * Mail configurations
     * @var array
     */
    protected $config;

    /**
     * API endpoint
     * @var string
     */
    protected $api_url;

    /**
     * To email addresses
     * @var json
     */
    private $to;

    /**
     * From email addresses
     * @var json
     */
    private $from;

    /**
     * CC email addresses
     * @var json
     */
    private $cc;

    /**
     * BCC email addresses
     * @var json
     */
    private $bcc;

    /**
     * Subject of the email
     * @var string
     */
    private $subject;

    /**
     * Message contents of the email
     * @var json
     */
    private $message;

    public function __construct($config)
    {
        $this->config = $config;
    }

    public function sendMailUsingGmail($to, $from, $fromname, $subject = null, $Bcc = null, $Cc = null, $textContent = null, $htmlMarkup = null, $files = [])
    {
        $message = new Message();
        $message->addFrom($from, $fromname);
        $message->addTo($to);
        $message->setSubject($subject);
        $message->setEncoding("UTF-8");
        $message->getHeaders('MIME-Version', '1.0');
        $message->getHeaders('Content-Transfer-Encoding', '8bit');
        $message->getHeaders('X-Mailer:', 'PHP/' . phpversion());

        if (!is_null($Bcc))
            $message->addCc($Bcc);

        if (!is_null($Cc))
            $message->addCc($Cc);

        $text = new MimePart($textContent);
        $text->type = "text/plain";

        $html = new MimePart($htmlMarkup);
        $html->type = "text/html";
        $html->charset = 'utf-8';

        $body = new MimeMessage();
        $body->setParts(array($text, $html));

        if (!empty($files)) {
            foreach ($files as $fileData) {
                $fileType = mime_content_type($fileData['file']);
                $fileInfo = pathinfo($fileData['file']);
                $fileName = $fileInfo['basename'];
                $file = new MimePart(fopen($fileData['file'], 'r'));

                switch ($fileType) {
                    case "image/jpeg":
                        $file->type = 'image/jpeg';
                        break;
                    case "image/png":
                        $file->type = 'image/png';
                        break;
                    case "image/gif":
                        $file->type = 'image/gif';
                        break;
                    case "application/pdf":
                        $file->type = 'application/pdf';
                        break;
                    case "application/msword":
                        $file->type = 'application/msword';
                        break;
                    case "application/vnd.ms-excel":
                        $file->type = 'application/vnd.ms-excel';
                        break;
                    case "application/zip":
                        $file->type = 'application/zip';
                        break;
                    default:
                        $fileExtention = pathinfo( $fileData['file'], PATHINFO_EXTENSION);
                        $file->type = ($fileExtention == 'csv') ? 'text/csv' : 'text/plain';
                }

                $file->filename = $fileName;
                $file->disposition = \Zend\Mime\Mime::DISPOSITION_INLINE;
                $file->encoding = \Zend\Mime\Mime::ENCODING_QUOTEDPRINTABLE;

                $body->addPart($file);
            }
        }

        $message->setBody($body);
        $message->setEncoding("UTF-8");
        $message->getHeaders()->get('content-type')->setType('multipart/mixed');

        $options = new SmtpOptions(array(
            'name' => 'thinkcube',
            'host' => $this->config['host'],
            'port' => '465',
            'connection_class' => 'login',
            'connection_config' => array(
                'username' => $this->config['username'],
                'password' => $this->config['password'],
                'ssl' => 'ssl',
            ),
        ));

        $transport = new \Zend\Mail\Transport\Smtp;
        $transport->setOptions($options);
        $transport->send($message);
        return TRUE;
    }

    /////////////// END SEND MAIL//////////////////

    public function sendSwisherMail($to, $from, $fromName, $subject = null, $Bcc = null, $Cc = null, $textContent = null, $htmlMarkup = null, $files = [])
    {
        //encoding Files and create message content array
        $messageContent = array();
        foreach ($files as $file) {
            $content = array(
                'type' => 'attachment',
                'content' => array('fileName' => $file['fileName'], 'encoding' => 'base64', 'content' => base64_encode(file_get_contents($file['file'])))
            );
            $messageContent[] = $content;
        }

        $messageContent[] = array('type' => 'html', 'content' => $htmlMarkup);

        if (is_null(getenv('APPID')) || is_null(getenv('APPSECRET'))) {
            error_log("***************************************************");
            error_log("Email Service Verification Failed. APPID or APPSECRET is not valid");
            return FALSE;
        }

        //get access for the swisher library
        $swisher = new \Swisher\SwisherService(getenv('APPID'), getenv('APPSECRET'));

        $email = $swisher->get('email');

        $data = array(
            'to' => array(
                array('name' => '', 'email' => $to)
            ),
            'from' => array('name' => $fromName, 'email' => $from),
            'subject' => $subject,
            'message' => $messageContent
        );
        $result = $email->send($data);
        return $result['status'];
    }

}

?>
