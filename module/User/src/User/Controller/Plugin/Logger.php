<?php

/**
 * @author PRASA7 <prasanna@thinkcube.com>
 *
 */

namespace User\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Session\Container;
use Zend\Mvc\Controller\Plugin\Redirect;
use User\Model\Log as LoggerModel;

class Logger extends AbstractPlugin
{

    protected $loggerTable;
    protected $userID;
    protected $username;
    protected $user_session;

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
    }

    /**
     * This function logs user success activities
     * @author PRASA7  <prasanna@thinkcube.com>
     * @param type $message : Log message
     * @param type $module : Module name
     * @param type $toDB = TRUE if need to store the log to  database
     * @param type $logout : To handle logout function
     */
    public function logSuccess($message, $refID = NULL, $module = NULL, $toDB = NULL, $logout = NULL)
    {
        $access = date("D M d H:i:s Y");
        if (!isset($_SERVER['REMOTE_ADDR']))
            $_SERVER['REMOTE_ADDR'] = NUll;

        $prefix = "[$access] [success] : [client] {$_SERVER['REMOTE_ADDR']} : ezBiz-$module module";
        openlog($prefix, LOG_PERROR, LOG_USER);

        if (isset($this->userID)) {
            if ($toDB == TRUE) {
                $data = array(
                    'logTitle' => $message,
                    'refID' => $refID,
                    'userID' => $this->userID,
                    'module' => $module,
                    'ip' => $_SERVER['REMOTE_ADDR']
                );

                $logger = new LoggerModel();
                $logger->exchangeArray($data);
                $this->getLoggerTable()->saveLog($logger);
            }
            syslog(LOG_INFO, "'$this->username' : '$this->userID' : '$message'");
        }

        if ($logout == TRUE) {
            $res = explode(":", $message);
            $res1 = explode("'", $res[1]);
            $data = array(
                'logTitle' => $res[2],
                'userID' => $res1[1],
                'module' => $module,
                'ip' => $_SERVER['REMOTE_ADDR']
            );

            $logger = new LoggerModel();
            $logger->exchangeArray($data);
            $this->getLoggerTable()->saveLog($logger);
            syslog(LOG_INFO, "$message");
        }
        closelog();
    }

    /**
     * This function handles the erros in the system
     * @author PRASA7  <prasanna@thinkcube.com>
     * @param type $message
     * @param type $module
     * @param type $logout
     */
    public function logError($message, $module = NULL)
    {
        $access = date("D M d H:i:s Y");
        $prefix = "[$access] [error] : [client] {$_SERVER['REMOTE_ADDR']} : ezBiz-$module module";
        openlog($prefix, LOG_PERROR, LOG_USER);

        syslog(LOG_ERR, "'$message'");

        closelog();
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * This function returns logger table
     * @return logger table
     */
    public function getLoggerTable()
    {
        if (!$this->loggerTable) {
            $sm = $this->getController()->getServiceLocator();
            $this->loggerTable = $sm->get('User\Model\LoggerTable');
        }
        return $this->loggerTable;
    }

///////////////////////////////////////////////////////////
}

?>
