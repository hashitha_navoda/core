<?php
/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains email configuration related functions
 */

namespace User\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use User\Form\EmailconfForm;
use User\Model\Emailconfig;

//use User\Model\EmailconfigTable;

class EmailconfController extends AbstractActionController
{

    public function indexAction()
    {
        $form = new EmailconfForm();
        $form->get('submit')->setValue('Save');
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost()->toArray();
            $Emailconfig = new Emailconfig();
            $form->setInputFilter($Emailconfig->getInputFilter());
            $form->setData($data);
            if ($form->isValid()) {
                $Emailconfig->exchangeArray($data);
                $this->CommonTable('User\Model\EmailconfigTable')->saveConfiguration($Emailconfig);
                ?>
                <html>
                    <script type="text/javascript"> ;
                        window.alert("Your data has been saved");
                    </script>;
                </html>
                <?php
            }
        } elseif (!$request->isPost()) {
            $dat = $this->CommonTable('User\Model\EmailconfigTable')->getConfiguration();
            if ($dat != NULL) {
                $form->bind($dat);
            }
        }
        return array('form' => $form);
    }

}

/////////////////// END OF EMAIL CONFIGURATION CONTROLLER \\\\\\\\\\\\\\\\\\\\
