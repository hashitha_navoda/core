<?php

namespace User\Form;

use Zend\Form\Form;

class UserLoginForm extends Form
{

    public function __construct()
    {
        parent::__construct('UserLogin');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'id' => 'username',
            'name' => 'userUsername',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'class' => 'form-control',
                'required' => true,
                'style' => 'width: 150px',
                'placeholder' => 'Enter Username ',
            ),
        ));
        $this->add(array(
            'id' => 'password',
            'name' => 'userPassword',
            'type' => 'Zend\Form\Element\Password',
            'options' => array(
            ),
            'attributes' => array(
                'required' => 'required',
                'class' => 'form-control',
                'style' => 'width: 150px',
                'placeholder' => 'Enter Password',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
