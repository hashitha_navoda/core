<?php

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class UserEditForm extends Form
{

    public function __construct($roles, $licenses = null, $userTypes)
    {
        // we want to ignore the name passed
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'userID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'userID'
            ),
        ));
        $this->add(array(
            'name' => 'userFirstName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('Jeo'),
                'id' => 'firstName'
            )
        ));
        $this->add(array(
            'name' => 'userLastName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('Smith'),
                'id' => 'lastName'
            )
        ));
        $this->add(array(
            'name' => 'userEmail1',
            'attributes' => array(
                'type' => 'email',
                'class' => 'form-control',
                'placeholder' => _('jsmith@example.com'),
                'id' => 'email1'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'roleID',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'roleID',
            ),
            'options' => array(
                'value_options' => $roles,
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'userTypeID',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'userTypeID',
            ),
            'options' => array(
                'value_options' => $userTypes,
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'licenseID',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'licenseID',
                'required' => FALSE,
            ),
            'options' => array(
                'value_options' => !is_null($licenses) ? $licenses : NULL,
            )
        ));
        $this->add(array(
            'name' => 'userCurrentPassword',
            'attributes' => array(
                'type' => 'password',
                'class' => 'form-control',
                'placeholder' => _(''),
                'id' => 'currentPassword',
                'autocomplete' => 'off'
            )
        ));
        $this->add(array(
            'name' => 'userPassword',
            'attributes' => array(
                'type' => 'password',
                'class' => 'form-control',
                'placeholder' => _(''),
                'id' => 'password'
            )
        ));
        $this->add(array(
            'name' => 'passwordC',
            'attributes' => array(
                'type' => 'password',
                'class' => 'form-control',
                'placeholder' => _(''),
                'id' => 'passwordC'
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save changes',
                'id' => 'create-user-button',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
