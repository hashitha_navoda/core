<?php

namespace User\Form;

use Zend\Form\Form;

class SalesPersonForm extends Form
{

    public function __construct($data)
    {
        $SalesPersonTitle = $data['suplierTitle'];
        $activeUser = $data['activeUse'];
        // we want to ignore the name passed
        parent::__construct('salesPerson');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'salesPersonID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'salesPersonID'
            ),
        ));
        $this->add(array(
            'name' => 'salesPersonSortName',
            'attributes' => array(
                'type' => 'text',
                'autofocus' => true,
                'class' => 'form-control',
                'placeholder' => 'JSmith',
                'id' => 'salesPersonSortName'
            )
        ));
        $this->add(array(
            'name' => 'salesPersonFirstName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'Jeo',
                'id' => 'salesPersonFirstName'
            )
        ));
        $this->add(array(
            'name' => 'salesPersonLastName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'Smith',
                'id' => 'salesPersonLastName'
            )
        ));
        $this->add(array(
            'name' => 'salesPersonEmail',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'jsmith@example.com',
                'id' => 'salesPersonEmail'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'salesPersonTitle',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'salesPersonTitle',
                'title' => ''
            ),
            'options' => array(
                'value_options' => $SalesPersonTitle,
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'salesPersonActiveUser',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'salesPersonActiveUser',
                'title' => ''
            ),
            'options' => array(
                'empty_option' => ' ',
                'value_options' => $activeUser,
            )
        ));
        $this->add(array(
            'name' => 'salesPersonTelephoneNumber',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'salesPersonTelephoneNumber',
//                'placeholder' => _('0723000000')
            ),
        ));
        $this->add(array(
            'name' => 'salesPersonAddress',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => _('Enter current Address'),
                'id' => 'salesPersonAddress'
            ),
        ));
        $this->add(array(
            'name' => 'list-sales-person-button',
            'attributes' => array(
                'type' => 'button',
                'value' => _('Back'),
                'id' => 'list-sales-person-button',
                'class' => 'btn btn-default make-full-width'
            ),
        ));
        $this->add(array(
            'name' => 'reset-sales-person-button',
            'attributes' => array(
                'type' => 'button',
                'value' => _('Reset'),
                'id' => 'reset-sales-person-button',
                'class' => 'btn btn-default make-full-width'
            ),
        ));
        $this->add(array(
            'name' => 'create-sales-person-button',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'create-sales-person-button',
                'class' => 'btn btn-primary make-full-width'
            ),
        ));
        $this->add(array(
            'name' => 'update-sales-person-button',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Update'),
                'id' => 'update-sales-person-button',
                'class' => 'btn btn-primary make-full-width'
            ),
        ));
    }

}
