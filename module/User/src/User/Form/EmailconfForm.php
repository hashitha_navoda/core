<?php

namespace User\Form;

use Zend\Form\Form;

class EmailconfForm extends Form
{

    public function __construct()
    {
        parent::__construct('Emailconf');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'id',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'id' => 'email',
            'name' => 'email',
            'type' => 'Zend\Form\Element\Email',
            'options' => array(
            ),
            'attributes' => array(
                'class' => 'form-control',
                'required' => true,
//                'oninvalid' => "setCustomValidity('This feild is required')",
                'style' => 'width: 300px',
                'placeholder' => 'Enter Email Address ',
            ),
        ));
        $this->add(array(
            'id' => 'password',
            'name' => 'password',
            'type' => 'Zend\Form\Element\Password',
            'style' => 'width: 300px',
            'options' => array(
            ),
            'attributes' => array(
                'required' => 'required',
                'class' => 'form-control',
                'style' => 'width: 300px',
                'placeholder' => 'Enter Password',
            ),
        ));
        $this->add(array(
            'id' => 'mailName',
            'name' => 'mailName',
            'type' => 'Text',
            'style' => 'width: 300px',
            'options' => array(
            ),
            'attributes' => array(
                'required' => 'required',
                'class' => 'form-control',
                'style' => 'width: 300px',
                'placeholder' => 'E.g. Gmail ',
            ),
        ));
        $this->add(array(
            'id' => 'host',
            'name' => 'host',
            'type' => 'Text',
            'style' => 'width: 300px',
            'options' => array(
            ),
            'attributes' => array(
                'required' => 'required',
                'class' => 'form-control',
                'style' => 'width: 300px',
                'placeholder' => 'E.g. smtp.gmail.com',
            ),
        ));
        $this->add(array(
            'id' => 'port',
            'name' => 'port',
            'type' => 'Text',
            'style' => 'width: 300px',
            'options' => array(
            ),
            'attributes' => array(
                'required' => 'required',
                'class' => 'form-control',
                'style' => 'width: 150px',
                'placeholder' => 'E.g 465',
            ),
        ));

        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'isd' => 'submitbutton',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
