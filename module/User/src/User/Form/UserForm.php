<?php

namespace User\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class UserForm extends Form {

    public function __construct($roles, $licenses, $userTypes) {
        // we want to ignore the name passed
        parent::__construct('user');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'userID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'userID'
            ),
        ));
        $this->add(array(
            'name' => 'userUsername',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'joesmith',
                'id' => 'userUsername'
            )
        ));
        $this->add(array(
            'name' => 'userFirstName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'Jeo',
                'id' => 'userFirstName'
            )
        ));
        $this->add(array(
            'name' => 'userLastName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'Smith',
                'id' => 'userLastName'
            )
        ));
        $this->add(array(
            'name' => 'userEmail1',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => 'jsmith@example.com',
                'id' => 'userEmail1'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'roleID',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'roleID',
            ),
            'options' => array(
                'value_options' => $roles,
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'userTypeID',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'userTypeID',
            ),
            'options' => array(
                'value_options' => $userTypes,
            )
        ));
        $this->add(array(
            'name' => 'userPassword',
            'attributes' => array(
                'type' => 'password',
                'class' => 'form-control',
                'placeholder' => '',
                'id' => 'userPassword',
                'autocomplete' => 'off'
            )
        ));
        $this->add(array(
            'name' => 'userPasswordC',
            'attributes' => array(
                'type' => 'password',
                'class' => 'form-control',
                'placeholder' => '',
                'id' => 'userPasswordC'
            )
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'licenseID',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'licenseID',
            ),
            'options' => array(
                'value_options' => !is_null($licenses) ? $licenses : NULL,
            )
        ));
        $this->add(array(
            'name' => 'list-user-button',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Back',
                'id' => 'list-user-button',
                'class' => 'btn btn-default'
            ),
        ));
        $this->add(array(
            'name' => 'reset-user-button',
            'attributes' => array(
                'type' => 'reset',
                'value' => 'Reset',
                'id' => 'reset-user-button',
                'class' => 'btn btn-default make-full-width xs-no-margin'
            ),
        ));
        $this->add(array(
            'name' => 'add-location-button',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Next',
                'id' => 'create-user-button',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
