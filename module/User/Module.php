<?php

namespace User;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Emailconfig;
use User\Model\EmailconfigTable;
use User\Model\RoleFeature;
use User\Model\RoleFeatureTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;

class Module
{

    public $user_session;

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $this->getConfig();

        $application = $e->getApplication();
        $sm = $application->getServiceManager();
        $sharedManager = $application->getEventManager()->getSharedManager();

        $sharedManager->attach('Zend\Mvc\Application', 'dispatch.error', function($e) use ($sm) {
            if ($e->getParam('exception')) {
                //$sm->get('Zend\Log\Logger')->crit($e->getParam('exception'));
            }
        }
        );
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            
            'aliases' => array(
                'SalesPersonTable' => 'User\Model\SalesPersonTable',
                'UserTable' => 'User\Model\UserTable',
                'RoleTable' => 'User\Model\RoleTable'
            ),
            'invokables' => array(
                'MobileDashBoardService' => 'User\Service\MobileDashBoardService',
            ),
            'factories' => array(
                'User\Model\UserTable' => function($sm) {
                    $tableGateway = $sm->get('UserTableGateway');
                    $table = new UserTable($tableGateway);
                    return $table;
                },
                'UserTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new User());
                    return new TableGateway('user', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\EmailconfigTable' => function($sm) {
                    $tableGateway = $sm->get('EmailconfigTableGateway');
                    $table = new Model\EmailconfigTable($tableGateway);
                    return $table;
                },
                'EmailconfigTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Emailconfig());
                    return new TableGateway('emailConfig', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\RoleTable' => function($sm) {
                    $tableGateway = $sm->get('RoleTableGateway');
                    $table = new Model\RoleTable($tableGateway);
                    return $table;
                },
                'RoleTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Role);
                    return new TableGateway('role', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\FeatureTable' => function($sm) {
                    $tableGateway = $sm->get('FeatureTableGateway');
                    $table = new Model\FeatureTable($tableGateway);
                    return $table;
                },
                'FeatureTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Feature);
                    return new TableGateway('feature', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\RoleFeatureTable' => function($sm) {
                    $tableGateway = $sm->get('RoleFeatureTableGateway');
                    $table = new Model\RoleFeatureTable($tableGateway);
                    return $table;
                },
                'RoleFeatureTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new RoleFeature());
                    return new TableGateway('roleFeature', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\SalesPersonTable' => function($sm) {
                    $tableGateway = $sm->get('SalesPersonTableGateway');
                    $table = new Model\SalesPersonTable($tableGateway);
                    return $table;
                },
                'SalesPersonTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\SalesPerson());
                    return new TableGateway('salesPerson', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\UserLoginTable' => function($sm) {
                    $tableGateway = $sm->get('UserLoginTableGateway');
                    $table = new Model\UserLoginTable($tableGateway);
                    return $table;
                },
                'UserLoginTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\UserLogin());
                    return new TableGateway('userLogin', $dbAdapter, null, $resultSetPrototype);
                },
                'User\Model\UserSettingTable' => function($sm) {
                    $tableGateway = $sm->get('UserSettingsTableGateway');
                    $table = new Model\UserSettingTable($tableGateway);
                    return $table;
                },
                'UserSettingsTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\UserSetting());
                    return new TableGateway('userSetting', $dbAdapter, null, $resultSetPrototype);
                },
            ),
        );
    }

}
