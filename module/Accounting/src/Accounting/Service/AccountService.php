<?php

namespace Accounting\Service;

use Core\Service\BaseService;
use Accounting\Model\GlAccountSetup;
use Accounting\Model\FiscalPeriod;
use Zend\EventManager\EventInterface;

class AccountService extends BaseService
{
    /**
     * For enable account module
     */
    public function enableAccountsModule($data)
    {
        try {
            // enable accounting
            $updateStatus = $this->getModel('CompanyTable')->updateCompanyDetails(['companyUseAccounting' => 1]);
            if (!$updateStatus) {
                return $this->returnError('Accounting module activation has been failed.');
            }
            
            // update costing method
            $updateStatus = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($data['costingSettings'], 1);
            if (!$updateStatus) {
                return $this->returnError('Costing method update has been failed.');
            }

            return $this->returnSuccess(null, 'Accounting module has been activated.');

        } catch (\Exception $ex) {
            return $this->returnError('Error occurred while activation account module.');
        }
    }

    /**
     * Set default gl-accounts
     */
    public function setDefaultGLAccounts($data)
    {
        try {
            $glAccounts = $this->getModel('GlAccountSetupTable')->fetchAll()->current();
            if (!empty($glAccounts)) {
                return $this->returnSuccess(null, 'Default gl-accounts has been already set.');
            }

            $glAccountSetup = new GlAccountSetup();
            $glAccountSetup->exchangeArray($data);
            $result = $this->getModel('GlAccountSetupTable')->saveGlAccountSetup($glAccountSetup);
            if (!$result) {
                return $this->returnError('Error occurred while setting default gl-accounts.');
            }
            return $this->returnSuccess(null, 'Default gl-accounts has been set.');

        } catch (\Exception $ex) {
            return $this->returnError('Error occurred while setting default gl-accounts.');
        }
    }

    /**
     * Set default gl-accounts for default customer
     */
    public function setGLAccountsForDefaultCustomer($data)
    {
        try {
            $defaultCustomer = $this->getModel('CustomerTable')->getCustomerById(0); // get default customer
            if (is_null($defaultCustomer)) {
                return $this->returnError('Default customer not exist.');
            }

            $result = $this->getModel('CustomerTable')->updateCustomerAccountsByCustomerID($data, 0);
            if (!$result) {
                return $this->returnError('Error occurred while setting gl-accounts for default customer.');
            }
            return $this->returnSuccess(null, 'gl-accounts has been set for default customer.');

        } catch (\Exception $ex) {
            return $this->returnError('Error occurred while setting gl-accounts for default customer.');
        }
    }

    /**
     * Set default payment finance accounts
     */
    public function setDefaultPaymentFinanceAccounts($paymentAccounts)
    {
        try {

            foreach ($paymentAccounts as $paymentMethodId => $paymentAccount)
            {
                $status = $this->getModel('PaymentMethodTable')->updatePaymentMethods($paymentAccount, $paymentMethodId);

                if (!$status) {
                    return $this->returnError('Error occurred while updating default payment method accounts.');
                }
            }

            return $this->returnSuccess(null, 'Default payment method accounts has been updated.');

        } catch (\Exception $ex) {
            return $this->returnError('Error occurred while updating default payment method accounts.');
        }
    }

    /**
     * Create default fiscal period
     */
    public function createDefaultFiscalPeriod($userId)
    {
        try {
            $fiscalPeriods = $this->getModel('FiscalPeriodTable')->getAllFiscalPeriods();
            if (count($fiscalPeriods) > 0) { // check wheter fiscal period set
                return $this->returnSuccess(null, 'Fiscal Period has been already created.');
            }

            $fiscalPeriod = new FiscalPeriod();
            // save parent fiscal periad
            $parentFiscalPeriod = $this->getFiscalPeriod($userId);
            $fiscalPeriod->exchangeArray($parentFiscalPeriod);
            $parentId = $this->getModel('FiscalPeriodTable')->saveFiscalPeriod($fiscalPeriod);

            //save child fiscal periads
            $childFiscalPeriods = $this->getFiscalPeriod($userId, $parentId);
            foreach ($childFiscalPeriods as $childFiscalPeriod) {
                $fiscalPeriod->exchangeArray($childFiscalPeriod);
                $result = $this->getModel('FiscalPeriodTable')->saveFiscalPeriod($fiscalPeriod);
                if (!$result) {
                    return $this->returnError('Error occurred while creating fiscal period.');
                }
            }
            return $this->returnSuccess(null, 'Fiscal Period has been created.');
            
        } catch (\Exception $ex) {
            return $this->returnError('Error occurred while creating default fiscal period.');
        }
    }

    /**
     * Get fiscal period details
     */
    private function getFiscalPeriod($userId, $parentId = null)
    {
        $period = [
            'fiscalPeriodStatusID' => 14
        ];
        
        $dateTime = new \DateTime();
        $entityService = $this->getService('EntityService');
        
        $currentMonth = (int)$dateTime->format('m');
        $startYear = $dateTime->format('Y');
        $endYear = $dateTime->format('Y');

        if ($currentMonth < 4) {
            $startYear = $dateTime->format('Y') - 1;
        } else {
            $endYear = $dateTime->format('Y') + 1;
        }

        if (is_null($parentId)) {
            $period['fiscalPeriodStartDate'] = $dateTime->setDate($startYear, 4, 1)->format('Y-m-d');
            $period['fiscalPeriodEndDate'] = $dateTime->setDate($endYear, 3, 31)->format('Y-m-d');
            $period['entityID'] = $entityService->createEntity($userId)['data']['entityID'];
            return $period;
        }

        $periods = [];
        for ($m = 4; $m <= 15; $m++) {
            
            if ($m > 12) {
                $month = $m-12;
                $year = $endYear;
            } else {
                $month = $m;
                $year = $startYear;
            }
            $period['fiscalPeriodStartDate'] = $dateTime->setDate($year, $month, 1)->format('Y-m-d');
            $period['fiscalPeriodEndDate'] = $dateTime->setDate($year, $month, $dateTime->format('t'))->format('Y-m-d');
            $period['fiscalPeriodParentID'] = $parentId;
            $period['entityID'] = $entityService->createEntity($userId)['data']['entityID'];
            $periods[] = $period;
        }
        return $periods;
    }

    public function updateBankAccountBalance(EventInterface $event)
    {

        $financeAccountsID = $event->getParam('glAccountID');

        $financeAcDetails = $this->getModel('FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($financeAccountsID)->current();
        $accountBalance = $financeAcDetails['financeAccountsDebitAmount'] - $financeAcDetails['financeAccountsCreditAmount'];
        
        $accountDetails = $this->getModel('AccountTable')->getAccountDetailsByFinanceAccountId($financeAccountsID);

        foreach ($accountDetails as $value) {
            $data = array(
                'accountBalance' => $accountBalance,
            );

            $this->getModel('AccountTable')->updateAccount($data,$value['accountId']);
        }
    }
}
