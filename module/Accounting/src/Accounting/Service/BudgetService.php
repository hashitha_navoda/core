<?php

namespace Accounting\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Accounting\Model\BudgetConfiguration;
use Accounting\Model\Budget;
use Accounting\Model\BudgetDetails;

class BudgetService extends BaseService
{
    /**
    * this function use to save budget conf
    * @param array $data
    * return array
    **/
    public function saveBudgetConfiguration($data)
    {
        $data = array(
            'isNotificationEnabled' => $data->isNotificationEnabled,
            'isBlockedEnabled' => $data->isBlockedEnabled,
        );

        $confData = new BudgetConfiguration();
        $confData->exchangeArray($data);

        $checkBudgetConf = $this->getModel('BudgetConfigurationTable')->fetchAll();
        if (!$checkBudgetConf) {
            $savedResult = $this->getModel('BudgetConfigurationTable')->saveBudgetConfiguration($confData);
            if (!$savedResult) {
                return $this->returnError('ERR_BUDGET_CONF_SAVE');
            } else {
                $displayData = [
                    'isBudgetEnabled' => 1,
                ];
                $saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);
                $this->setLogMessage('Budget is enabled');
                return $this->returnSuccess(null, 'SUCCESS_BUDGET_CONF_SAVE');
            }
        } else {
            $savedResult = $this->getModel('BudgetConfigurationTable')->updateBudgetConfiguration($checkBudgetConf['budgetConfigurationId'], $confData);
            if (!$savedResult) {
                return $this->returnError('ERR_BUDGET_CONF_SAVE');
            } else {
                $displayData = [
                    'isBudgetEnabled' => 1,
                ];
                $saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);
                $this->setLogMessage('Budget is enabled');
                return $this->returnSuccess(null, 'SUCCESS_BUDGET_CONF_SAVE');
            }
        }
    }

    public function disableBudgetConfiguration() {
        $displayData = [
            'isBudgetEnabled' => 0,
        ];
        $saveBusinessType = $this->getModel('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);
        $this->setLogMessage('Budget is disabled');
        return $this->returnSuccess(null, 'SUCCESS_BUDGET_CONF_DEACTIVATED');
    }

    public function deleteBudget($data) {
        $deleteResult = $this->getModel('BudgetTable')->updateBudgetAsDeleted($data->budgetID);
        $oldData = $this->getModel('BudgetTable')->getBudgetByBudgetID($data->budgetID)->current();

        if (!$deleteResult) {
            return $this->returnError('ERR_BUDGET_DELTE');
        }
        $this->setLogMessage('Budget '.$oldData['budgetName'].' is deleted.');
        return $this->returnSuccess(null, 'SUCCESS_BUDGET_DELTE');
    }


    public function saveBudget($postData)
    {
        $data = array(
            'budgetName' => $postData->budgetName,
            'fiscalPeriodID' => $postData->fiscalPeriodID,
            'dimension' => ($postData->dimension == "") ? null : $postData->dimension,
            'dimensionValue' => ($postData->dimensionValue == "") ? null : $postData->dimensionValue,
            'interval' => $postData->interval,
            'deleted' => 0,
        );

        $budgetData = new Budget();
        $budgetData->exchangeArray($data);

        $savedResult = $this->getModel('BudgetTable')->saveBudget($budgetData);
        if (!$savedResult) {
            return $this->returnError('ERR_BUDGET_DATA_SAVE');
        } else {
            foreach ($postData->budgetData as $value) {
                $bData = array(
                    'budgetId' => $savedResult,
                    'fiscalPeriodID' => $value['fiscalPeriodID'],
                    'financeAccountID' => $value['financeAccountID'],
                    'budgetValue' => $value['budgetValue']
                );

                $budgetDetails = new BudgetDetails();
                $budgetDetails->exchangeArray($bData);

                $savedRes = $this->getModel('BudgetDetailsTable')->saveBudgetDetails($budgetDetails);

                if (!$savedRes) {
                    return $this->returnError('ERR_BUDGET_DATA_SAVE');
                } 
            }
            $this->setLogMessage('Budget '.$postData->budgetName.' is created.');
            return $this->returnSuccess(null, 'SUCCESS_BUDGET_DATA_SAVE');
        }
    }

    public function getBudgetDataForEdit($data)
    {
        $budgetRes = $this->getModel('BudgetTable')->getBudgetByBudgetID($data->budgetID);

        $budgetData = [];
        foreach ($budgetRes as $value) {
            $tempG = array();
            $tempG['budgetId'] = $value['budgetId'];
            $tempG['budgetName'] = $value['budgetName'];
            $tempG['fiscalPeriodID'] = $value['fiscalPeriodID'];
            $tempG['dimension'] = $value['dimension'];
            $tempG['dimensionValue'] = $value['dimensionValue'];
            $tempG['interval'] = $value['interval'];


            if($value['dimension'] == "job" || $value['dimension'] == "project") {
                if ($value['dimension'] == "job") {
                    $jobData = $this->getModel('JobTable')->getJobsByJobID($value['dimensionValue']);
                    $dimensionTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                    $tempG['dimensionTxt'] = $dimensionTxt;
                } else if ($value['dimension'] == "project") {
                    $proData = $this->getModel('ProjectTable')->getProjectByProjectId($value['dimensionValue']);
                    $dimensionTxt = $proData->projectCode.'-'.$proData->projectName;
                    $tempG['dimensionTxt'] = $dimensionTxt;
                }
            }

            $budgetAccountData = (isset($budgetData[$value['budgetId']]['budgetAccountData'])) ? $budgetData[$value['budgetId']]['budgetAccountData'] : array();

            if (!is_null($value['budgetDetailsId'])) {
                $budgetAccountData[$value['budgetDetailsId']] = array('subFiscalPeriodID' => $value['subFiscalPeriodID'],'financeAccountID' => $value['financeAccountID'], 'budgetValue' => floatval($value['budgetValue']));
            }

            $tempG['budgetAccountData'] = $budgetAccountData;
            $budgetData[$value['budgetId']] = $tempG;
        }

        return $budgetData[$data->budgetID];
    }


    public function updateBudget($postData)
    {
        $deleteResult = $this->getModel('BudgetTable')->updateBudgetAsDeleted($postData->editedBudgetID);

        if (!$deleteResult) {
            return $this->returnError('ERR_BUDGET_DELTE');
        } else {
            $oldData = $this->getModel('BudgetTable')->getBudgetByBudgetID($postData->editedBudgetID)->current();
            $changeArray = $this->getFiscalPeriodChageDetails($oldData, $postData);
            //set log details
            $previousData = json_encode($changeArray['previousData']);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            return $this->saveBudget($postData);
        }
    }

    public function getFiscalPeriodChageDetails($row, $data)
    {
        $oldFiscal = $this->getModel('FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($row['fiscalPeriodID']);
        $oldDimension = $this->getModel('DimensionValueTable')->getDimensionByDimensionValueID($row['dimensionValue'])->current();
        $previousData = [];
        $previousData['Budget Name'] = $row['budgetName'];
        $previousData['Budget Interval'] = $row['interval'];
        $previousData['Fiscal Period'] = $oldFiscal['fiscalPeriodStartDate'].' - '.$oldFiscal['fiscalPeriodEndDate'];
        $previousData['Dimension'] = $oldDimension['dimensionName'];
        $previousData['Dimension Value'] = $oldDimension['dimensionValue'];

        $newFiscal = $this->getModel('FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($data['fiscalPeriodID']);
        $newDimension = $this->getModel('DimensionValueTable')->getDimensionByDimensionValueID($data['dimensionValue'])->current();
        $newData = [];
        $newData['Budget Name'] = $data['budgetName'];
        $newData['Budget Interval'] = $data['interval'];
        $newData['Fiscal Period'] = $newFiscal['fiscalPeriodStartDate'].' - '.$newFiscal['fiscalPeriodEndDate'];
        $newData['Dimension'] = $newDimension['dimensionName'];
        $newData['Dimension Value'] = $newDimension['dimensionValue'];

        return array('previousData' => $previousData, 'newData' => $newData);
    }


    public function getAccountClassByReport($data)
    {
        if ($data->reportType == "pandlac") {
            $financeAccountTypes = [1,2,5];
        } else {
            $financeAccountTypes = [4,6,7,3];
        }

        $result = $this->getModel('FinanceAccountClassTable')->getAllActiveAccountClassesByAccountTypeIDForReportClassification($financeAccountTypes);

        $financeAccountClassArray = [];
        foreach ($result as $value) {
            $financeAccountClassArray[] = $value['financeAccountClassID'];
        }

        return $financeAccountClassArray;
    }
}