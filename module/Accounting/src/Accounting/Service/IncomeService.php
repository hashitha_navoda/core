<?php

namespace Accounting\Service;

use Core\Service\BaseService;
use Zend\I18n\Translator\Translator;
use Zend\ViewModel;
use Accounting\Model\Income;
use Accounting\Model\IncomeNonItemProduct;
use Accounting\Model\IncomeProductTax;
use Accounting\Model\IncomeProduct;
use Accounting\Model\IncomeSubProduct;
use Inventory\Model\ItemOut;
use Invoice\Model\Payments;
use Invoice\Model\InvoicePayments;
use Invoice\Model\Customer;
use Invoice\Model\IncomingPaymentMethodCash;
use Invoice\Model\IncomingPaymentMethodCheque;
use Invoice\Model\IncomingPaymentMethodCreditCard;
use Invoice\Model\IncomingPaymentMethodBankTransfer;
use Invoice\Model\IncomingPaymentMethodGiftCard;
use Invoice\Model\IncomingPaymentMethodLC;
use Invoice\Model\IncomingPaymentMethodTT;
use Invoice\Model\IncomingPaymentMethod;

class IncomeService extends BaseService
{
    /**
    * this function use to save income
    * @param array $data
    * return array
    **/
    public function saveIncome($data, $userLocation, $userID, $companyCurrencyId)
    {

        $products = $data['products'];
        $subProducts = $data['subProducts'];
        $nonItemProducts = $data['nonItempr'];
        $paymentData = $data['paymentPostData'];
        $incomeVoucherCode = $data['incomeCode'];

        //check whether job code alredy in database or not.
        $referenceService = $this->getService('ReferenceService');
        $reference = $referenceService->getReferenceNumber('42', $userLocation);
        $locationReferenceID = $reference['data']['locationReferenceID'];
        while ($incomeVoucherCode) {
            if ($this->getModel('IncomeTable')->checkIncomeByCode($incomeVoucherCode, $data['locationID'])->current()) {
                if ($locationReferenceID) {
                    $newIncomeVoucherCode = $referenceService->getReferencePrefixNumber($locationReferenceID);
                    if ($incomeVoucherCode == $newIncomeVoucherCode) {
                        $referenceService->updateReferenceNumber($locationReferenceID);
                        $incomeVoucherCode = $referenceService->getReferencePrefixNumber($locationReferenceID);
                    } else {
                        $incomeVoucherCode = $newIncomeVoucherCode;
                    }
                } else {
                    return $this->returnError('ERR_JOB_CODE_EXIST', null);
                }
            } else {
                break;
            }
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];

        $itemFlagValue = 0;
        if (count($products) > 0 && count($nonItemProducts) > 0) {
            $itemFlagValue = 3;
        } else if (count($nonItemProducts) > 0) {
            $itemFlagValue = 2;
        } else if (count($products) > 0) {
            $itemFlagValue = 1;
        }

        $incomeVoucherData = array(
            'incomeCode' => $incomeVoucherCode,
            'customerID' => ($data['customerID'] == "") ? NULL : $data['customerID'],
            'incomeAccountID' => ($data['accountID'] == "") ? NULL : $data['accountID'],
            'incomeLocationID' => $data['locationID'],
            'incomeDate' => $this->convertDateToStandardFormat($data['date']),
            'incomeComment' => $data['comment'],
            'incomeTotal' => $data['incomeTotal'],
            'incomeItemFlag' => $itemFlagValue,
            'entityID' => $entityID
        );

        $saveData = new Income();
        $saveData->exchangeArray($incomeVoucherData);

        $incomeID = $this->getModel('IncomeTable')->saveIncome($saveData);
        if (!$incomeID) {
            return $this->returnError('ERR_SAVE_INCOME');
        } else {
            $accountProduct = array();
            if ($itemFlagValue == 3 || $itemFlagValue == 2) {
                foreach ($nonItemProducts as $value) {
                    $incomeNonItemdetails = Array(
                        'incomeID' => $incomeID,
                        'incomeNonItemProductDiscription' => $value['description'],
                        'incomeNonItemProductQuantity' => $value['qty'],
                        'incomeNonItemProductUnitPrice' => $value['unitPrice'],
                        'incomeNonItemProductTotalPrice' => $value['totalPrice'],
                        'incomeNonItemProductGLAccountID' => $value['exType']
                    );
                    $incomeNonItem = new IncomeNonItemProduct();
                    $incomeNonItem->exchangeArray($incomeNonItemdetails);
                    $incomeNonItemID = $this->getModel('IncomeNonItemProductTable')->saveNonItemProduct($incomeNonItem);
                    if (!$incomeNonItemID) {
                        return $this->returnError('ERR_SAVE_INCOME');
                    }

                    if(isset($accountProduct[$value['exType']]['creditTotal'])){
                        $accountProduct[$value['exType']]['creditTotal'] += ($value['unitPrice'] * $value['qty']);
                    }else{
                        $accountProduct[$value['exType']]['creditTotal'] = $value['unitPrice'] * $value['qty'];
                        $accountProduct[$value['exType']]['debitTotal'] = 0.00;
                        $accountProduct[$value['exType']]['accountID'] = $value['exType'];
                    }

                    if (sizeof($value['tax']) > 0) {
                        $addedTaxValue = 0;
                        foreach ($value['tax'] as $nonItmTxKy => $nonItmTxVlu) {
                            $taxData = $this->getModel('TaxTable')->getSimpleTax($nonItmTxVlu);
                           
                            $itemPrice = $value['qty'] * $value['unitPrice'];
                            if($taxData['taxType'] == 'v'){
                                $taxValue = ($itemPrice * $taxData['taxPrecentage']) / 100;    
                            } else {
                                $taxValue = (($itemPrice + $addedTaxValue) * $taxData['taxPrecentage']) / 100;
                            }

                            $nonItemTaxData = array(
                                'incomeID' => $incomeID,
                                'incomeProductIdOrNonItemProductID' => $incomeNonItemID,
                                'incomeTaxID' => $nonItmTxVlu,
                                'incomeTaxPrecentage' => $taxData['taxPrecentage'],
                                'incomeTaxAmount' => $taxValue,
                                'incomeTaxItemOrNonItemProduct' => 'nonItem',
                            );

                            $incomeTaxData = New IncomeProductTax();
                            $incomeTaxData->exchangeArray($nonItemTaxData);
                            $incomeProductTaxID = $this->getModel('IncomeProductTaxTable')->saveIncomeProductTax($incomeTaxData);

                            if (!$incomeProductTaxID) {
                                return $this->returnError('ERR_SAVE_INCOME');
                            }

                            $taxData = $this->getModel('TaxTable')->getSimpleTax($nonItmTxVlu);
                            if(empty($taxData['taxSalesAccountID'])){
                                return $this->returnError('ERR_TAX_SALES_ACCOUNT');
                            }

                            //set tax gl accounts for the journal Entry
                            if(isset($accountProduct[$taxData['taxSalesAccountID']]['creditTotal'])){
                                $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] += $taxValue;
                            }else{
                                $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = $taxValue;
                                $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                            }
                            $addedTaxValue = $taxValue;
                        }
                    }
                }
            }
            $accountArray = [];
            if ($itemFlagValue == 3 || $itemFlagValue == 1) {
                $saveProducts = $this->saveIncomeInventoryProducts($products, $subProducts, $data['locationID'], $incomeID);
                
                if (!$saveProducts['status']) {
                    return $this->returnError($saveProducts['msg']);
                }
                $accountArray = $saveProducts['data']['accounts'];

                foreach ($accountArray as $key => $value) {
                    if (isset($accountProduct[$key])) {
                        $accountProduct[$key]['creditTotal'] += $value['creditTotal'];
                        $accountProduct[$key]['debitTotal'] += $value['debitTotal'];
                    } else {
                        $accountProduct[$key] = $value;
                    }
                }
            }

            $paymentData['customerID'] = $data['customerID'];
            $paymentData['date'] = $data['date'];

            $payemntResult = $this->makePayment($paymentData, $data['locationID'], $incomeID, $companyCurrencyId, $data['incomeTotal'], $userId);


            if (!$payemntResult['status']) {
                return $this->returnError($payemntResult['msg']);
            }

            foreach ($payemntResult['data']['accounts'] as $key => $value) {
                if (isset($accountProduct[$key])) {
                    $accountProduct[$key]['creditTotal'] += $value['creditTotal'];
                    $accountProduct[$key]['debitTotal'] += $value['debitTotal'];
                } else {
                    $accountProduct[$key] = $value;
                }
            }

            if ($locationReferenceID) {
                $referenceService->updateReferenceNumber($locationReferenceID);
            }



            return $this->returnSuccess(['accountProduct' => $accountProduct, 'incomeID' => $incomeID, 'incomeVoucherCode' => $incomeVoucherCode, 'date' => $incomeVoucherData['incomeDate'], 'eventParameter' => $saveProducts['data']['eventParameter']], "SUCCESS");

        }
    }

    public function editIncome($data) 
    {

        $incomeID = $data->incomeID;
        $incomingPaymentID = $data->incomingPaymentID;
        $paymentComment = $data->paymentComment;
        $incomeComment = $data->incomeComment;
        $date = $data->date;
        $paymentMethods = $data->paymentMethods;

        $paymentData = $this->getModel('PaymentsTable')->getPaymentsByPaymentID($incomingPaymentID)->current();
        $oldPaymentMemo = $paymentData['incomingPaymentMemo'];
        $oldPaymentDate = $paymentData['incomingPaymentDate'];
        
        $incomeVoucherData = array(
            'incomeDate' => $this->convertDateToStandardFormat($date),
            'incomeComment' => $incomeComment,
        );

        $result = $this->getModel('IncomeTable')->updateIncome($incomeVoucherData, $incomeID);
        if (!$result) {
            return $this->returnError('ERR_UPADTE_INCOME');
        } else {

            if ($oldPaymentMemo == $paymentComment && $oldPaymentDate == $this->convertDateToStandardFormat($date)) {
                $res = 1;
            } else {
                $dataPayment = array(
                    'incomingPaymentMemo' => $paymentComment,
                    'incomingPaymentDate' => $this->convertDateToStandardFormat($date),
                );

                $res = $this->getModel('PaymentsTable')->update($dataPayment, $incomingPaymentID);
            }

            if ($res != 1) {
                return $this->returnError('ERR_UPADTE_INCOME');
            } else {

                
                foreach ($paymentMethods as $key => $value) {
                    if ($value['methodID'] == 2) {
                        $previousChequeData = $this->getModel('IncomingPaymentMethodChequeTable')->getChequeDetailsByChequeId($value['methodTypeID'])->current();
                        
                        if ($value['checkNumber'] != $previousChequeData['incomingPaymentMethodChequeNumber'] || $value['bank'] != $previousChequeData['incomingPaymentMethodChequeBankName']) {

                            $dataPayment = array(
                                'incomingPaymentMethodChequeBankName' => $value['bank'],
                                'incomingPaymentMethodChequeNumber' => $value['checkNumber'],
                            );
                            $updateCheck = $this->getModel('IncomingPaymentMethodChequeTable')->updateIncomingPaymentMethodCheque($dataPayment, $value['methodTypeID']);
                        }

                    } elseif ($value['methodID'] == 5) {
                        $previousTraData = $this->getModel('IncomingPaymentMethodBankTransferTable')->getBankTransferDataByBankTransferId($value['methodTypeID'])->current();

                        if ($value['bankID'] != $previousTraData['incomingPaymentMethodBankTransferBankId'] &&  $value['accountID'] != $previousChequeData['incomingPaymentMethodBankTransferAccountId']) {

                            $dataPayment = array(
                                'incomingPaymentMethodBankTransferBankId' => $value['bankID'],
                                'incomingPaymentMethodBankTransferAccountId' => $value['accountID'],
                            );
                            $updateCheck = $this->getModel('IncomingPaymentMethodBankTransferTable')->updateBankTransfer($dataPayment, $value['methodTypeID']);
                        }
                    }
                }

                $jdata = array(
                    'journalEntryDate' => $this->convertDateToStandardFormat($date),
                );

                $updateStatus = $this->getModel('JournalEntryTable')->updateJournalEntryDocumentStatus($jdata, 43,$incomeID);

                if (!$updateStatus) {
                    return $this->returnError('ERR_UPDATED_INCOME');
                } else {
                    return $this->returnSuccess(null, 'SUCC_UPADTE_INCOME');
                }
            }
        }

    }

    public function makePayment($data_set, $locationID, $incomeID, $companyCurrencyId, $incomeTotal, $userId)
    {
        $customCurrencyId = $companyCurrencyId;
        
        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->getModel('CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = $customCurrencyData->currencyRate;

        $customerID = $data_set['customerID'];
        $date = $this->convertDateToStandardFormat($data_set['date']);
        $amount = $data_set['amount'] * $customCurrencyRate;
        $discount = ($data_set['discount'] != '') ? $data_set['discount'] * $customCurrencyRate : 0.00;
        $memo = $data_set['memo'];
        $paymentTerm = 1;
        $paymentType = "income";
        $locationID = $locationID;
        $creditAmount = ($data_set['creditAmount'] != '') ? $data_set['creditAmount'] * $customCurrencyRate : 0.00;
        $paymentMethods = $data_set['paymentMethods'];
        $pos = false;
        $balance = isset($data_set['balance']) ? $data_set['balance'] * $customCurrencyRate : null;
        $totalPaidAmount = isset($data_set['totalPaidAmount']) ? $data_set['totalPaidAmount'] * $customCurrencyRate : null;
        $restBalance = $data_set['restBalance'] * $customCurrencyRate;
        // $loyaltyData = $data_set['loyaltyData'];
        $referenceService = $this->getService('ReferenceService');
        $reference = $referenceService->getReferenceNumber(4, $locationID);
        $locationReferenceID = $reference['data']['locationReferenceID'];
        $paymentCode = $referenceService->getReferencePrefixNumber($locationReferenceID);

        $customer = $this->getModel('CustomerTable')->getCustomerByID($customerID);
        $cCurrentbalancess = floatval($customer->customerCurrentBalance) + floatval($incomeTotal);
        $cCurrentCredit = $customer->customerCurrentCredit;
        
        $accountProduct = array();
        $glAccountExist = $this->getModel('GlAccountSetupTable')->fetchAll();
        $glAccountData = $glAccountExist->current();



        if ($customerID) {
            $customerOverPaymentAccountIsSet = true;
            if($discount > 0 && empty($customer->customerSalesDiscountAccountID)){
                return $this->returnError('ERR_CUSTOMER_SALES_DISCOUNT_ACCOUNT_NOT_SET');
            }else if($creditAmount > 0 && empty($customer->customerAdvancePaymentAccountID)){
                $customerOverPaymentAccountIsSet = false;
            }
        }

        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];
        $paymentdata = array(
            'incomingPaymentCode' => $paymentCode,
            'incomingPaymentDate' => $date,
            'paymentTermID' => $paymentTerm,
            'incomingPaymentAmount' => $amount,
            'customerID' => $customerID,
            'incomingPaymentDiscount' => $discount,
            'incomingPaymentMemo' => $memo,
            'incomingPaymentType' => $paymentType,
            'locationID' => $locationID,
            'entityID' => $entityID,
            'incomingPaymentCreditAmount' => $creditAmount,
            'pos' => $pos,
            'incomingPaymentBalance' => $balance,
            'incomingPaymentPaidAmount' => $totalPaidAmount,
            'customCurrencyId' => $customCurrencyId,
            'incomingPaymentCustomCurrencyRate' => $customCurrencyRate,
        );

        $payment = new Payments;
        $payment->exchangeArray($paymentdata);

        $paymentID = $this->getModel('PaymentsTable')->savePayments($payment);
        if (!$paymentID) {
            return $this->returnError('ERR_CUSTPAY_API_SAVE');
        }

        $invTotalAmout = $incomeTotal * $customCurrencyRate;
      
        $invoicePayments = array(
            'incomingPaymentID' => $paymentID,
            'salesInvoiceID' => null,
            'incomingInvoicePaymentAmount' => $invTotalAmout,
            'incomingInvoicePaymentCurrencyGainOrLoss' => 0,
            'incomeID' => $incomeID,
        );

        $invPayment = new InvoicePayments;
        $invPayment->exchangeArray($invoicePayments);
        $invoicePaymentId = $this->getModel('InvoicePaymentsTable')->saveInvoicePayment($invPayment);
        if (!$invoicePaymentId) {
            return $this->returnError('ERR_CUSTPAY_API_INVOICE_PAYMENT_SAVE');
        }

    
        $newCurrentBalance = $cCurrentbalancess - $invTotalAmout;
        // when come from pos, rest balance mark as a credit balance. that's why comment bellow line
        // $newCreditBalance = $cCurrentCredit - $creditAmount + $restBalance;

        $newCreditBalance = ($totalPaidAmount + $creditAmount) - $invTotalAmout;
        if ($newCreditBalance > 0) {
            $newCreditBalance = $cCurrentCredit + $newCreditBalance - $creditAmount;
        } else {
            $newCreditBalance = $cCurrentCredit - $creditAmount;
        }

        $customerupdate = array(
            'customerID' => $customerID,
            'customerCurrentBalance' => $newCurrentBalance,
            'customerCurrentCredit' => $newCreditBalance,
        );
        $customerData = new Customer;
        $customerData->exchangeArray($customerupdate);
        $customerUpdateResult = $this->getModel('CustomerTable')->updateCustomerCurrentAndCreditBalance($customerData);
        if ($customerUpdateResult != TRUE) {
            return $this->returnError('ERR_CUSTPAY_API_CUST_UPDATE');
        }


        $totalCRAmounts = 0;
        $accountProduct = array();
        $loyaltyDataValues = $this->getModel('CustomerLoyaltyTable')->getLoyaltyCardByCustID($customerID);

        foreach ($paymentMethods as $methodValue) {
            if($methodValue['paidAmount']>0){
                $cashPaymentId = '';
                $chequePaymentId = '';
                $creditCardPaymentId = '';
                $bankTransferId = '';
                $giftCardPaymentId = '';
                $lCPaymentId = '';
                $tTPaymentId = '';
                if ($methodValue['methodID'] == 1) {
                    $cashPayment = array(
                        'incomingPaymentMethodCashId' => '',
                        'cashAccountID' => $methodValue['cashAccountID'],
                        );
                    $cashPaymentData = new IncomingPaymentMethodCash;
                    $cashPaymentData->exchangeArray($cashPayment);
                    $cashPaymentId = $this->getModel('IncomingPaymentMethodCashTable')->saveCashPayment($cashPaymentData);
                    if (!$cashPaymentId) {
                        return $this->returnError('ERR_CUSTPAY_API_PAYMETHOD_SAVE');
                    }
                    //update the cash in hand to current locations
                    $currentLocationDetails = $this->getModel('LocationTable')->getLocationCashInHandAmount($locationID)->current();
                    $newlocationCashInHand = $currentLocationDetails['locationCashInHand'] + $amount - $discount;
                    $this->getModel('LocationTable')->updateLocationAmount($locationID, $newlocationCashInHand);
                } else if ($methodValue['methodID'] == 2) {
                    $chequePayment = array(
                        'incomingPaymentMethodChequeNumber' => $methodValue['checkNumber'],
                        'incomingPaymentMethodChequeBankName' => $methodValue['bank'],
                        'postdatedChequeStatus' => $methodValue['postdatedStatus'],
                        'postdatedChequeDate' => ($methodValue['postdatedStatus'] == 1) ? $methodValue['postdatedDate'] : null,
                        'incomingPaymentMethodChequeAccNo' => $methodValue['chequeAccountID']
                        );
                    $chequePaymentData = new IncomingPaymentMethodCheque;
                    $chequePaymentData->exchangeArray($chequePayment);
                    $chequePaymentId = $this->getModel('IncomingPaymentMethodChequeTable')->saveChequePayment($chequePaymentData);
                    if (!$chequePaymentId) {
                        return $this->returnError('ERR_CUSTPAY_API_PAYMETHOD_SAVE');
                    }
                } else if ($methodValue['methodID'] == 3) {
                    // update card payment account
                    if ($data_set['cardType'] && $data_set['cardType'] != '') {
                        $cardPaymentAccountID = $this->getModel('CardTypeTable')->getAccountIDByID($data_set['cardType'])->current();
                        $cardTypeAccount = $this->getModel('AccountTable')->getAccountByAccountId($cardPaymentAccountID['accountID']);
                        $newAccountBalance = $cardTypeAccount['accountBalance'] + ($methodValue['paidAmount'] * $customCurrencyRate - $discount) / $cardTypeAccount['currencyRate'];
                        $accountUpdated = $this->getModel('AccountTable')->updateAccount(['accountBalance' => $newAccountBalance], $cardPaymentAccountID['accountID']);
                        if (!$accountUpdated) {
                            return $this->returnError('ERR_CUSPAY_NOT_UPDATE_CARD_PAYMENT_ACCOUNT');
                        }
                    }

                    $creditCardPayment = array(
                        'incomingPaymentMethodCreditCardNumber' => $methodValue['cardnumber'],
                        'incomingPaymentMethodCreditReceiptNumber' => $methodValue['reciptnumber'],
                        'cardAccountID' => ($cardPaymentAccountID['accountID']) ? $cardPaymentAccountID['accountID'] : null,
                        'cardTypeID' => $data_set['cardType'] ? $data_set['cardType'] : NULL
                        );
                    $creditCardPaymentData = new IncomingPaymentMethodCreditCard;
                    $creditCardPaymentData->exchangeArray($creditCardPayment);
                    $creditCardPaymentId = $this->getModel('IncomingPaymentMethodCreditCardTable')->saveCreditCardPayment($creditCardPaymentData);
                    if (!$creditCardPaymentId) {
                        return $this->returnError('ERR_CUSTPAY_API_PAYMETHOD_SAVE');
                    }
                } else if ($methodValue['methodID'] == 5) {
                    $bankTransferPayment = array(
                        'incomingPaymentMethodBankTransferBankId' => $methodValue['bankID'],
                        'incomingPaymentMethodBankTransferAccountId' => $methodValue['accountID'],
                        'incomingPaymentMethodBankTransferCustomerBankName' => $methodValue['customerBank'],
                        'incomingPaymentMethodBankTransferCustomerAccountNumber' => $methodValue['customerAccountNumber'],
                        'incomingPaymentMethodBankTransferRef' => $methodValue['bankTransferRef'],
                        );
                    $bankTransferPaymentData = new IncomingPaymentMethodBankTransfer;
                    $bankTransferPaymentData->exchangeArray($bankTransferPayment);
                    $bankTransferId = $this->getModel('IncomingPaymentMethodBankTransferTable')->saveBankTransferPayment($bankTransferPaymentData);
                    if (!$bankTransferId) {
                        return $this->returnError('ERR_CUSTPAY_API_PAYMETHOD_SAVE');
                    } else {
                        if ($methodValue['accountID'] != '') {
                            $accountData = $this->getModel('AccountTable')->getAccountByAccountId($methodValue['accountID']);
                            $newAccountBalance = $accountData['accountBalance'] + (($methodValue['paidAmount'] * $customCurrencyRate) / $accountData['currencyRate']);
                            $accountUpdate = array(
                                'accountBalance' => $newAccountBalance,
                                );
                            $update = $this->getModel('AccountTable')->updateAccount($accountUpdate, $methodValue['accountID']);
                            if (!$update) {
                                return $this->returnError('ERR_CUSTPAY_API_PAYMETHOD_SAVE');
                            }
                        }
                    }
                } else if ($methodValue['methodID'] == 6) {
                    $giftCardId = $methodValue['giftCardID'];
                    $giftCardPayment = array(
                        'giftCardId' => $giftCardId,
                        );
                    $giftCardPaymentData = new IncomingPaymentMethodGiftCard;
                    $giftCardPaymentData->exchangeArray($giftCardPayment);
                    $giftCardPaymentId = $this->getModel('IncomingPaymentMethodGiftCardTable')->saveGiftCardPayment($giftCardPaymentData);
                    if (!$giftCardPaymentId) {
                        return $this->returnError('ERR_CUSTPAY_API_PAYMETHOD_SAVE');
                    } else {
                        $giftData = array(
                            'giftCardStatus' => 4,
                            );
                        $this->getModel('GiftCardTable')->updateGiftCardDetailsByGiftCardId($giftData, $giftCardId);
                    }
                } else if ($methodValue['methodID'] == 7) {
                    $LCPaymentReference = $methodValue['lcPaymentReference'];
                    $lCPayment = array(
                        'incomingPaymentMethodLCReference' => $LCPaymentReference,
                        );
                    $lCPaymentData = new IncomingPaymentMethodLC;
                    $lCPaymentData->exchangeArray($lCPayment);
                    $lCPaymentId = $this->getModel('IncomingPaymentMethodLCTable')->saveLCPaymentMethod($lCPaymentData);
                    if (!$lCPaymentId) {
                        return $this->returnError('ERR_CUSTPAY_API_PAYMETHOD_SAVE');
                    }
                } else if ($methodValue['methodID'] == 8) {
                    $TTPaymentReference = $methodValue['ttPaymentReference'];
                    $tTPayment = array(
                        'incomingPaymentMethodTTReference' => $TTPaymentReference,
                        );
                    $tTPaymentData = new IncomingPaymentMethodTT;
                    $tTPaymentData->exchangeArray($tTPayment);
                    $tTPaymentId = $this->getModel('IncomingPaymentMethodTTTable')->saveTTPaymentMethod($tTPaymentData);
                    if (!$tTPaymentId) {
                        return $this->returnError('ERR_CUSTPAY_API_PAYMETHOD_SAVE');
                    }
                }

                $paymentMethodAmount = $methodValue['paidAmount'] * $customCurrencyRate;
                $totalCRAmounts += $paymentMethodAmount;

                $paymentMethodData = $this->getModel('PaymentMethodTable')->getPaymentMethodById($methodValue['methodID']);
                $selectedAccountID = null;
                if(!($methodValue['methodID'] == 4 || $methodValue['methodID'] == 6)){
                    // then payment type is cash and user change Gl account by manualy.
                    if($methodValue['methodID'] == 1 && $methodValue['cashAccountID'] != ''){
                        $selectedAccountID = $methodValue['cashAccountID'];

                    } else if($methodValue['methodID'] == 2 && $methodValue['chequeAccountID'] != ''){
                        $selectedAccountID = $methodValue['chequeAccountID'];

                    } else if($methodValue['methodID'] == 3 && $methodValue['creditAccountID'] != ''){
                        $selectedAccountID = $methodValue['creditAccountID'];

                    } else if($methodValue['methodID'] == 5 && $methodValue['bankTransferAccountID'] != ''){
                        $selectedAccountID = $methodValue['bankTransferAccountID'];

                    } else if($methodValue['methodID'] == 7 && $methodValue['lcAccountID'] != ''){
                        $selectedAccountID = $methodValue['lcAccountID'];

                    } else if($methodValue['methodID'] == 8 && $methodValue['ttAccountID'] != ''){
                        $selectedAccountID = $methodValue['ttAccountID'];

                    } else {
                        if(empty($paymentMethodData['paymentMethodSalesFinanceAccountID'])){
                            return $this->returnError('ERR_PAY_PAYMETHOD_SALES_ACCOUNT_NOT_SET');
                        } else {
                           $selectedAccountID =  $paymentMethodData['paymentMethodSalesFinanceAccountID'];
                        }

                    }
                    if(isset($accountProduct[$selectedAccountID]['debitTotal'])){
                        $accountProduct[$selectedAccountID]['debitTotal'] += $paymentMethodAmount;
                    }else{
                        $accountProduct[$selectedAccountID]['debitTotal'] = $paymentMethodAmount;
                        $accountProduct[$selectedAccountID]['creditTotal'] = 0.00;
                        $accountProduct[$selectedAccountID]['accountID'] = $selectedAccountID;
                    }

                }else{
                    if($methodValue['methodID'] == 4){
                        if(empty($loyaltyDataValues->loyaltyGlAccountID)){
                            return $this->returnError('ERR_PAY_LOYALTY_CARD_GL_ACCOUNT_NOT_SET');
                        }

                        if(isset($accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['debitTotal'])){
                            $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['debitTotal'] += $paymentMethodAmount;
                        }else{
                            $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['debitTotal'] = $paymentMethodAmount;
                            $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['creditTotal'] = 0.00;
                            $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['accountID'] = $loyaltyDataValues->loyaltyGlAccountID;
                        }

                    } else if($methodValue['methodID'] == 6){
                        $giftCardId = $methodValue['giftCardID'];
                        $giftCardData = $this->getModel('GiftCardTable')->getGiftCardDetailsByGiftCardID($giftCardId);
                        if(empty($giftCardData->giftCardGlAccountID)){
                            return $this->returnError('ERR_PAY_GIFTCARD_GL_ACCOUNT_NOT_SET');
                        }

                        if(isset($accountProduct[$giftCardData->giftCardGlAccountID]['debitTotal'])){
                            $accountProduct[$giftCardData->giftCardGlAccountID]['debitTotal'] += $paymentMethodAmount;
                        }else{
                            $accountProduct[$giftCardData->giftCardGlAccountID]['debitTotal'] = $paymentMethodAmount;
                            $accountProduct[$giftCardData->giftCardGlAccountID]['creditTotal'] = 0.00;
                            $accountProduct[$giftCardData->giftCardGlAccountID]['accountID'] = $giftCardData->giftCardGlAccountID;
                        }
                    }
                }

                $paymentMethod = array(
                    'incomingPaymentId' => $paymentID,
                    'incomingPaymentMethodAmount' => $paymentMethodAmount,
                    'incomingPaymentMethodCashId' => $cashPaymentId,
                    'incomingPaymentMethodChequeId' => $chequePaymentId,
                    'incomingPaymentMethodCreditCardId' => $creditCardPaymentId,
                    'incomingPaymentMethodBankTransferId' => $bankTransferId,
                    'incomingPaymentMethodLoyaltyCardId' => $loyaltyCardId,
                    'incomingPaymentMethodGiftCardId' => $giftCardPaymentId,
                    'incomingPaymentMethodLCId' => $lCPaymentId,
                    'incomingPaymentMethodTTId' => $tTPaymentId,
                    'entityId' => $entityService->createEntity($userId)['data']['entityID']
                    );
                $PaymentMethodData = new IncomingPaymentMethod;
                $PaymentMethodData->exchangeArray($paymentMethod);
                $paymentMethodID = $this->getModel('IncomingPaymentMethodTable')->savePaymentMethod($PaymentMethodData);
                if (!$paymentMethodID) {
                    return $this->returnError('ERR_CUSTPAY_API_PAYMETHOD_SAVE');
                }
            }
        }

        
        //If customer has loyalty, then update customer's loyalty points
        // if (isset($loyaltyData['cust_loyalty_id'])) {
        //     $data = [
        //         'customerLoyaltyID' => $loyaltyData['cust_loyalty_id'],
        //         'incomingPaymentID' => $paymentID,
        //         'collectedPoints' => $loyaltyData['earned_points'],
        //         'redeemedPoints' => $loyaltyData['redeemed_points'],
        //         'expired' => 0
        //     ];

        //     $history = new IncomingPaymentMethodLoyaltyCard();
        //     $history->exchangeArray($data);

        //     $loyaltyCardId = $this->CommonTable('IncomingPaymentMethodLoyaltyCardTable')->save($history);
        //     if (!$loyaltyCardId) {
        //         return array(
        //             'state' => false,
        //             'msg' => $this->getMessage('ERR_CUSTPAY_API_PAYMETHOD_SAVE'),
        //             'customerID' => $customerID
        //             );
        //     }

        //         // Update total loyalty points of the customer
        //     $Lpoints = ((float) $loyaltyData['current_points'] + (float) $loyaltyData['earned_points']) - (float) $loyaltyData['redeemed_points'];
        //     $this->CommonTable('CustomerLoyaltyTable')->updateLoyaltyPoints($loyaltyData['cust_loyalty_id'], $Lpoints);

        //     if($this->useAccounting == 1 && $loyaltyDataValues->loyaltyID != '' && isset($loyaltyData['earned_points'])){
        //         if(empty($loyaltyDataValues->loyaltyGlAccountID)){
        //             return array(
        //                 'state' => false,
        //                 'msg' => $this->getMessage('ERR_PAY_LOYALTY_CARD_GL_ACCOUNT_NOT_SET',array($loyaltyDataValues->customerLoyaltyCode)),
        //             );
        //         }
        //         if(empty($glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID)){
        //             return array(
        //                 'state' => false,
        //                 'msg' => $this->getMessage('ERR_PAY_LOYALTY_EXPENSE_GL_ACCOUNT_NOT_SET'),
        //             );
        //         }

        //         if(isset($accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['creditTotal'])){
        //             $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['creditTotal'] += $loyaltyData['earned_points'];
        //         }else{
        //             $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['creditTotal'] = $loyaltyData['earned_points'];
        //             $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['debitTotal'] = 0.00;
        //             $accountProduct[$loyaltyDataValues->loyaltyGlAccountID]['accountID'] = $loyaltyDataValues->loyaltyGlAccountID;
        //         }

        //         if(isset($accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['debitTotal'])){
        //             $accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['debitTotal'] += $loyaltyData['earned_points'];
        //         }else{
        //             $accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['debitTotal'] = $loyaltyData['earned_points'];
        //             $accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['creditTotal'] = 0.00;
        //             $accountProduct[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['accountID'] = $glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID;
        //         }
        //     }
        // }
            
        $overAmount = ($totalCRAmounts + $creditAmount) - ($invTotalAmout - $discount);

        if($overAmount > 0 ){
            if(empty($customer->customerAdvancePaymentAccountID)){
                return $this->returnError('ERR_CUSTOMER_ADVANCE_ACCOUNT_NOT_SET');
            }

            if(isset($accountProduct[$customer->customerAdvancePaymentAccountID]['creditTotal'])){
                $accountProduct[$customer->customerAdvancePaymentAccountID]['creditTotal'] += $overAmount;
            }else{
                $accountProduct[$customer->customerAdvancePaymentAccountID]['creditTotal'] = $overAmount;
                $accountProduct[$customer->customerAdvancePaymentAccountID]['debitTotal'] = 0.00;
                $accountProduct[$customer->customerAdvancePaymentAccountID]['accountID'] = $customer->customerAdvancePaymentAccountID;
            }
        }

        if($creditAmount > 0 ){
            if($customerOverPaymentAccountIsSet){
                if(isset($accountProduct[$customer->customerAdvancePaymentAccountID]['debitTotal'])){
                    $accountProduct[$customer->customerAdvancePaymentAccountID]['debitTotal'] += $creditAmount;
                }else{
                    $accountProduct[$customer->customerAdvancePaymentAccountID]['debitTotal'] = $creditAmount;
                    $accountProduct[$customer->customerAdvancePaymentAccountID]['creditTotal'] = 0.00;
                    $accountProduct[$customer->customerAdvancePaymentAccountID]['accountID'] = $customer->customerAdvancePaymentAccountID;
                }
            }else{
                if(isset($accountProduct[$customer->customerReceviableAccountID]['debitTotal'])){
                    $accountProduct[$customer->customerReceviableAccountID]['debitTotal'] += $creditAmount;
                }else{
                    $accountProduct[$customer->customerReceviableAccountID]['debitTotal'] = $creditAmount;
                    $accountProduct[$customer->customerReceviableAccountID]['creditTotal'] = 0.00;
                    $accountProduct[$customer->customerReceviableAccountID]['accountID'] = $customer->customerReceviableAccountID;
                }
            }
        }

        
        if($discount > 0){
            if(isset($accountProduct[$customer->customerSalesDiscountAccountID]['debitTotal'])){
                $accountProduct[$customer->customerSalesDiscountAccountID]['debitTotal'] += $discount;
            }else{
                $accountProduct[$customer->customerSalesDiscountAccountID]['debitTotal'] = $discount;
                $accountProduct[$customer->customerSalesDiscountAccountID]['creditTotal'] = 0.00;
                $accountProduct[$customer->customerSalesDiscountAccountID]['accountID'] = $customer->customerSalesDiscountAccountID;
            }
            $totalCRAmounts += $discount;
        }

        return $this->returnSuccess(['accounts' => $accountProduct], "SUCCESS_PAY");
    }



    public function saveIncomeInventoryProducts($products, $subProducts, $locationOut, $incomeID)
    {
        $customCurrencyRate = 1;
        $accountProduct = array();
        foreach ($products as $key => $p) {

            $batchProducts = $subProducts[$p['productID']];
            
            $productID = $p['productID'];
            $deliveryQty = $p['deliverQuantity']['qty'];
            $productType = $p['productType'];
            $locationProductOUT = $this->getModel('LocationProductTable')->getLocationProduct($productID, $locationOut);
            $averageCostingPrice = $locationProductOUT->locationProductAverageCostingPrice;
            $locationProductID = $locationProductOUT->locationProductID;

            $pData = $this->getModel('ProductTable')->getProductByProductID($productID);

            if(empty($pData['productInventoryAccountID'])){
                return $this->returnError('ERR_GRN_PRODUCT_ACCOUNT');
            }

            //check whether Product Cost of Goods Sold Gl account id set or not
            if(empty($pData['productCOGSAccountID'])){
                return $this->returnError('ERR_GRN_PRODUCT_ACCOUNT_COGS');
            }

            if(empty($pData['productSalesAccountID'])){
                return $this->returnError('ERR_PRPODUCT_SALES_ACCOUNT');
            }

            if ($productType != 2) {
                if ($locationProductOUT->locationProductQuantity < $deliveryQty) {
                    return $this->returnError('ERR_DELINOTEAPI_PRODUCT_UPDATE');
                }
                $locationProductOUTData = array(
                    'locationProductQuantity' => $locationProductOUT->locationProductQuantity - $deliveryQty,
                );
                $this->getModel('LocationProductTable')->updateQty($locationProductOUTData, $productID, $locationOut);
            }

            //calculate discount value
            $discountValue = 0;
            if ($p['productDiscountType'] == "precentage") {
                $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $customCurrencyRate;
            } else if ($p['productDiscountType'] == "value") {
                $discountValue = $p['productDiscount'] * $customCurrencyRate;
            }

            if ($p['productDiscountType'] == 'value') {
                $p['productDiscount'] = $p['productDiscount'] * $customCurrencyRate;
            }

            $incomeProductData = array(
                'incomeID' => $incomeID,
                'productID' => $productID,
                'incomeProductPrice' => $p['productPrice'] * $customCurrencyRate,
                'incomeProductDiscount' => $p['productDiscount'],
                'incomeProductDiscountType' => $p['productDiscountType'],
                'incomeProductTotal' => $p['productTotal'] * $customCurrencyRate,
                'incomeProductQuantity' => $deliveryQty
            );

            $incomeProduct = new IncomeProduct;
            $incomeProduct->exchangeArray($incomeProductData);
            $incomeProductID = $this->getModel('IncomeProductTable')->saveIncomeProduct($incomeProduct);
            $locationProduct = $this->getModel('LocationProductTable')->getLocationProduct($productID, $locationOut);
            $locationPID = $locationProduct->locationProductID;
            
            $pTotal = $p['productTotal'] - $p['pTax']['tTA'];

            $totalCRAmounts+=$pTotal;
            if(isset($accountProduct[$pData['productSalesAccountID']]['creditTotal'])){
                $accountProduct[$pData['productSalesAccountID']]['creditTotal'] += $pTotal;
            }else{
                $accountProduct[$pData['productSalesAccountID']]['creditTotal'] = $pTotal;
                $accountProduct[$pData['productSalesAccountID']]['debitTotal'] = 0.00;
                $accountProduct[$pData['productSalesAccountID']]['accountID'] = $pData['productSalesAccountID'];
            }

            if ($p['pTax']) {
                foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                    $incomePTaxesData = array(
                        'incomeProductIdOrNonItemProductID' => $incomeProductID,
                        'incomeID' => $incomeID,
                        'incomeTaxID' => $taxKey,
                        'incomeTaxPrecentage' => $productTax['tP'],
                        'incomeTaxAmount' => $productTax['tA'] * $customCurrencyRate,
                        'incomeTaxItemOrNonItemProduct' => 'inventory'
                    );
                    
                    $incomePTaxM = new IncomeProductTax();
                    $incomePTaxM->exchangeArray($incomePTaxesData);
                    $this->getModel('IncomeProductTaxTable')->saveIncomeProductTax($incomePTaxM);

                    $taxData = $this->getModel('TaxTable')->getSimpleTax($taxKey);
                    if(empty($taxData['taxSalesAccountID'])){
                        return $this->returnError('ERR_TAX_SALES_ACCOUNT');
                    }

                    $totalCRAmounts += $productTax['tA'] * $customCurrencyRate;
                    //set tax gl accounts for the journal Entry
                    if(isset($accountProduct[$taxData['taxSalesAccountID']]['creditTotal'])){
                        $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] += $productTax['tA'] * $customCurrencyRate;
                    }else{
                        $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = $productTax['tA'] * $customCurrencyRate;
                        $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                    }
                }
            }

            //check and insert items to itemout table
            if ($productType != 2) {
                if (!count($batchProducts) > 0) {
                    $sellingQty = $deliveryQty;
                    while ($sellingQty != 0) {
                        $itemInDetails = $this->getModel('ItemInTable')->getFirstAvailableProductDetails($locationProductOUT->locationProductID);
                        if (!$itemInDetails) {
                            break;
                        }
                        if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                            $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Income',
                                'itemOutDocumentID' => $incomeID,
                                'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $sellingQty,
                                'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()['data']['currentTime']
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->getModel('ItemOutTable')->saveItemOut($itemOutM);
                            $this->getModel('ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                            if($averageCostingFlag == 1){
                            //set gl accounts for the journal entry
                                $productTotal = $sellingQty * $averageCostingPrice;
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }else{
                                $productTotal = $sellingQty * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }
                            $sellingQty = 0;
                            break;
                        } else {
                            $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Income',
                                'itemOutDocumentID' => $incomeID,
                                'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()['data']['currentTime']
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->getModel('ItemOutTable')->saveItemOut($itemOutM);
                            $this->getModel('ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                            if($averageCostingFlag == 1){
                            //set gl accounts for the journal entry
                                $productTotal = $itemInDetails['itemInRemainingQty'] * $averageCostingPrice;
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }else{
                                $productTotal = $itemInDetails['itemInRemainingQty'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }
                            $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                        }
                    }
                }
            }
        
            if (count($batchProducts) > 0) {
                        
                foreach ($batchProducts as $batchKey => $batchValue) {
                    if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                        $this->saveIncomeSubProductData($batchValue, $incomeProductID);
                        $batchProduct = $this->getModel('ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                        $productBatchQuentity = $batchProduct->productBatchQuantity - $batchValue['qtyByBase'];
                        if ($batchProduct->productBatchQuantity < $batchValue['qtyByBase']) {
                            return $this->returnError('ERR_DELINOTEAPI_PRODUCT_UPDATE');
                        }
                        if ($batchValue['serialID']) {
                            $itemInDetails = $this->getModel('ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProductOUT->locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Income',
                                'itemOutDocumentID' => $incomeID,
                                'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                'itemOutBatchID' => $batchValue['batchID'],
                                'itemOutSerialID' => $batchValue['serialID'],
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $batchValue['qtyByBase'],
                                'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()['data']['currentTime']
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->getModel('ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                            $this->getModel('ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                            if($averageCostingFlag == 1){
                            //set gl accounts for the journal entry
                                $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }else{
                                $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }
                        } else {
                            $itemInDetails = $this->getModel('ItemInTable')->getFirstAvailableBatchProductDetails($locationProductOUT->locationProductID, $batchValue['batchID']);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Income',
                                'itemOutDocumentID' => $incomeID,
                                'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                'itemOutBatchID' => $batchValue['batchID'],
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $batchValue['qtyByBase'],
                                'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()['data']['currentTime']
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->getModel('ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                            $this->getModel('ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                            if($averageCostingFlag == 1){
                            //set gl accounts for the journal entry
                                $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }else{
                                $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }
                        }

                        $productBatchQty = array(
                            'productBatchQuantity' => $productBatchQuentity,
                        );
                        $this->getModel('ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                        if ($batchValue['serialID']) {
                            $checkSerial = $this->getModel('ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                            if ($checkSerial->locationProductID != $locationProductOUT->locationProductID || $checkSerial->productSerialSold == 1) {
                                return $this->returnError('ERR_DELINOTEAPI_PRODUCT_UPDATE');
                            }
                            $serialProductData = array(
                                'productSerialSold' => '1',
                            );
                            $this->getModel('ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                        }
                    } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                        $this->saveIncomeSubProductData($batchValue, $incomeProductID);
                        $checkSerial = $this->getModel('ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                        if ($checkSerial->locationProductID != $locationProductOUT->locationProductID || $checkSerial->productSerialSold == 1) {
                            return $this->returnError('ERR_DELINOTEAPI_PRODUCT_UPDATE');
                        }

                        ///insert to the item out table
                        $itemInDetails = $this->getModel('ItemInTable')->getFirstAvailableSerialProductDetails($locationProductOUT->locationProductID, $batchValue['serialID']);
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Income',
                            'itemOutDocumentID' => $incomeID,
                            'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => $batchValue['serialID'],
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => 1,
                            'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                            'itemOutDiscount' => $discountValue,
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDateAndTime' => $this->getGMTDateTime()['data']['currentTime']
                        );

                        $itemOutM->exchangeArray($itemOutData);
                        $this->getModel('ItemOutTable')->saveItemOut($itemOutM);
                        $this->getModel('ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);

                        if($averageCostingFlag == 1){
                            //set gl accounts for the journal entry
                            $productTotal = 1 * $averageCostingPrice;
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                            }

                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                            }
                        }else{
                            $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                            }

                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                            }
                        }

                        $serialProductData = array(
                            'productSerialSold' => '1',
                        );
                        $this->getModel('ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                    }
                }
            } else {
                   // $this->saveTransferProductData(array('batchID' => 0, 'serialID' => 0, 'qtyByBase' => $p['transferQuantity']['qtyByBase']), $trasferID, $productID);
            }
        }

        // call product updated event
        $productIDs = array_map(function($element) {
            return $element['productID'];
        }, $products);

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationOut]];

        return $this->returnSuccess(['accounts' => $accountProduct, 'eventParameter' => $eventParameter], "SUCCESS");


    }

    function saveIncomeSubProductData($batchValue, $incomeProductID)
    {
        $data = array(
            'incomeProductID' => $incomeProductID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'incomeProductSubQuantity' => $batchValue['qtyByBase'],
            'incomeSubProductsWarranty' => $batchValue['warranty'],
        );
        $incomeSubProduct = new IncomeSubProduct;
        $incomeSubProduct->exchangeArray($data);

        $incomeSubProductID = $this->getModel('IncomeSubProductTable')->saveIncomeSubProduct($incomeSubProduct);
        return $incomeSubProductID;
    }
}