<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\YearEndClosingForm;


class BudgetController extends CoreController
{

	protected $sideMenus = 'accounting_side_menu';
    protected $downMenus = 'account_settings_up_down_menu';
    protected $upperMenus = 'budget_upper_menu';

    public function indexAction()
    {
        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        
        if ($displaySettings->isBudgetEnabled == 1) {
            return $this->listAction();
        } else {
            $this->getSideAndUpperMenus('Settings', null, 'Accounting', 'Budgeting');
            $this->getViewHelper('HeadScript')->prependFile('/js/accounting/budgeting.js');

            $budgetView = new ViewModel(
                array(
                'isBudgetEnabled' => $displaySettings->isBudgetEnabled
                )
            );

            $budgetView->setTemplate('accounting/budget/index');
            return $budgetView;
        }

    }


    public function createAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Create Budget', 'Accounting', 'Budgeting');
        $fAccountClassData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClasses();
        
        $budgetID = $this->params()->fromRoute('param1');

        $editMode = false;
        if (!is_null($budgetID)) {
            $editMode = true;
        }

        $financeAccounts = array();

        foreach ($fAccountClassData as $key => $value) {
            $financeAccounts[$value['financeAccountClassID']]['financeAccountClassID'] = $value['financeAccountClassID'];
            $financeAccounts[$value['financeAccountClassID']]['financeAccountTypesName'] = $value['financeAccountTypesName'];
            $financeAccounts[$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];
            $fAccountsData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getHierarchyByAccountClassID($value['financeAccountClassID']);
            $financeAccounts[$value['financeAccountClassID']]['financeAccounts'] = $fAccountsData ;
        }

        $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllUnlockedFiscalPeriods();

        $fiscalPeriodArray = array();
        foreach ($fiscalPeriodData as $value) {
                $fiscalPeriodArray[$value['fiscalPeriodID']] = $value['fiscalPeriodStartDate']." - ".$value['fiscalPeriodEndDate'];
        }

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $budgetCreate = new ViewModel(array(
             'fiscalPeriods' => $fiscalPeriodArray,
             'dimensions' => $dimensions,
             'financeAccounts' => $financeAccounts,
             'editMode' => $editMode,
             'budgetID' => $budgetID,
        ));

        $budgetCreate->setTemplate('accounting/budget/create');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/budgeting.js');
        return $budgetCreate;
    }


    public function listAction() 
    {
        $this->getSideAndUpperMenus('Settings', 'View Budgets', 'Accounting', 'Budgeting');

        $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllUnlockedFiscalPeriods();

        $fiscalPeriodArray = array();
        foreach ($fiscalPeriodData as $value) {
                $fiscalPeriodArray[$value['fiscalPeriodID']] = $value['fiscalPeriodStartDate']." - ".$value['fiscalPeriodEndDate'];
        }

        $this->getPaginatedBudgets();
        $budgetList = new ViewModel(array(
            'budgets' => $this->paginator,
            'fiscalPeriods' => $fiscalPeriodArray
        ));

        $budgetList->setTemplate('accounting/budget/list');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/budget-list.js');
        return $budgetList;

    }

    private function getPaginatedBudgets()
    {
        $this->paginator = $this->CommonTable('Accounting\Model\BudgetTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function settingAction() 
    {
        $this->getSideAndUpperMenus('Settings', 'Budget Setting', 'Accounting', 'Budgeting');

        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/budgeting.js');
        
        $budgetConfs = (object) $this->CommonTable('Accounting\Model\BudgetConfigurationTable')->fetchAll();

        $budgetSetting = new ViewModel(
            array(
            'isBudgetEnabled' => $displaySettings->isBudgetEnabled,
            'isNotificationEnabled' => $budgetConfs->isNotificationEnabled,
            'isBlockedEnabled' => $budgetConfs->isBlockedEnabled,
            'editMode' => true,
            )
        );

        $budgetSetting->setTemplate('accounting/budget/index');
        return $budgetSetting;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Settings', 'View Budgets', 'Accounting', 'Budgeting');
        $fAccountClassData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClasses();
        
        $budgetID = $this->params()->fromRoute('param1');
        $editMode = true;

        $financeAccounts = array();

        foreach ($fAccountClassData as $key => $value) {
            $financeAccounts[$value['financeAccountClassID']]['financeAccountClassID'] = $value['financeAccountClassID'];
            $financeAccounts[$value['financeAccountClassID']]['financeAccountTypesName'] = $value['financeAccountTypesName'];
            $financeAccounts[$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];
            $fAccountsData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getHierarchyByAccountClassID($value['financeAccountClassID']);
            $financeAccounts[$value['financeAccountClassID']]['financeAccounts'] = $fAccountsData ;
        }

        $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllUnlockedFiscalPeriods();

        $fiscalPeriodArray = array();
        foreach ($fiscalPeriodData as $value) {
                $fiscalPeriodArray[$value['fiscalPeriodID']] = $value['fiscalPeriodStartDate']." - ".$value['fiscalPeriodEndDate'];
        }

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $budgetCreate = new ViewModel(array(
             'fiscalPeriods' => $fiscalPeriodArray,
             'dimensions' => $dimensions,
             'financeAccounts' => $financeAccounts,
             'editMode' => $editMode,
             'budgetID' => $budgetID,
        ));

        $budgetCreate->setTemplate('accounting/budget/view');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/budget-view.js');
        return $budgetCreate;
    }

}