<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\FiscalPeriodForm;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Fiscal Period related actions
 */
class FiscalPeriodController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'account_settings_accounts_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function listAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Fiscal Period', 'Accounting', 'Accounts Settings');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/fiscal-period.js');

        $this->getPaginatedFiscalPeriod();
        $statusList = $this->getStatusesList();
        $userdateFormat = $this->getUserDateFormat(); 

        $fiscalPeriodView = new ViewModel(
        	array(
            	'fiscalPeriod' => $this->paginator,
            	'statusList' => $statusList,
            	'dateFormat' => $userdateFormat,
            )
        );
        foreach ($statusList as $key => $value) {
        	if($key == 11 || $key == 14 ){
        		$status[$key] = $value;
        	}
        }

        $fiscalPeriodForm = new FiscalPeriodForm( 'CreateFiscalPeriod',array('status' => $status));

      	$fiscalPeriodForm->get('fiscalPeriodStartDate')->setAttribute('data-date-format',$userdateFormat);
      	$fiscalPeriodForm->get('fiscalPeriodEndDate')->setAttribute('data-date-format',$userdateFormat);

        $fiscalPeriodCreate = new ViewModel(
            array(
            	'fiscalPeriodForm' => $fiscalPeriodForm,
            	'statusList' => $status,
            	'userdateFormat' => $userdateFormat,
            )
        );

        $fiscalPeriodCreate->setTemplate('/accounting/fiscal-period/create');
        $fiscalPeriodView->addChild($fiscalPeriodCreate, 'fiscalPeriodCreate');

        return $fiscalPeriodView;
    }

     private function getPaginatedFiscalPeriod()
    {
        $this->paginator = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

}
