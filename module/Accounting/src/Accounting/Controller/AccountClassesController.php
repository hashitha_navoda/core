<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\FinanceAccountClassForm;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Account Classes related actions
 */
class AccountClassesController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'account_classes_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Create', 'Accounting', 'Account Classes');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/account-class.js');

        $financeAccountClassID = $this->params()->fromRoute('param1');
        
        $fAccountsTypesData = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->fetchAll();
        $fAccountTypesArray = array();
        foreach ($fAccountsTypesData as $value) {
            $fAccountTypesArray[$value->financeAccountTypesID] = $value->financeAccountTypesName;           
        }

        $fiananceAccountClassForm = new FinanceAccountClassForm( 'CreateFinanceAccountClass',
                array(
            'financeAccountTypes' => $fAccountTypesArray,
        ));

        if ($financeAccountClassID) {
            $financeAccounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAllAccountsByAccountClassID($financeAccountClassID); 
            $accountClass = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAccountClassDataByAccountClassID($financeAccountClassID);
            $fiananceAccountClassForm->get('financeAccountTypesID')->setValue($accountClass['financeAccountTypesID']);
            if(count($financeAccounts) > 0){
                $fiananceAccountClassForm->get('financeAccountTypesID')->setAttribute('disabled','disabled');
            }
            $fiananceAccountClassForm->get('financeAccountClassID')->setValue($financeAccountClassID);
            $fiananceAccountClassForm->get('financeAccountClassName')->setValue($accountClass['financeAccountClassName']);
            $fiananceAccountClassForm->get('entityId')->setValue($accountClass['entityID']);
            $fiananceAccountClassForm->get('createFinanceAccountClass')->setValue('Update');
        }
        
        $financeAccountClassView = new ViewModel(
            array(
            'financeAccountClassForm'=>$fiananceAccountClassForm,
            )
        );
        return $financeAccountClassView;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Settings', 'View Account Classes', 'Accounting', 'Account Classes');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/account-class-list.js');

        $this->getPaginatedAccountClasses();
        $accountClassView = new ViewModel(array(
            'financeAccountClass' => $this->paginator,
            'statuses' => $this->getStatusesList(),
                )
        );
        return $accountClassView;
    }

     private function getPaginatedAccountClasses()
    {
        $this->paginator = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }
}
