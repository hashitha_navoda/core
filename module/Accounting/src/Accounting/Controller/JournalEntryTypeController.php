<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\JournalEntryTypeForm;
use Accounting\Form\JournalEntryTypeApproversForm;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains journal Entry Type related actions
 */
class JournalEntryTypeController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'account_settings_accounts_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Journal Entry Type', 'Accounting', 'Accounts Settings');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/journal-entry-type.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/journal-entry-type-list.js');

        $this->getPaginatedJournalEntryType();
        $journalEntryTypeView = new ViewModel(array(
            'journalEntryType' => $this->paginator,
                )
        );

        $journalEntryTypeForm = new JournalEntryTypeForm( 'CreateJournalEntryType');
        $journalEntryTypeApproversForm = new JournalEntryTypeApproversForm( 'CreateJournalEntryTypeApprovers');


        $journalEntryTypeCreate = new ViewModel(
            array(
            'journalEntryTypeForm' => $journalEntryTypeForm,
            'journalEntryTypeApproversForm' => $journalEntryTypeApproversForm,
            )
        );

        $journalEntryTypeCreate->setTemplate('/accounting/journal-entry-type/journal-entry-type-create');
        $journalEntryTypeView->addChild($journalEntryTypeCreate, 'journalEntryTypeCreate');

        return $journalEntryTypeView;
    }

    public function listAction()
    {

        $this->getPaginatedJournalEntryType();
        $journalEntryView = new ViewModel(array(
            'journalEntryType' => $this->paginator,
                )
        );

        return $journalEntryView;
    }

     private function getPaginatedJournalEntryType()
    {
        $this->paginator = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

}
