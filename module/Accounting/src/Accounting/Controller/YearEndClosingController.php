<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\YearEndClosingForm;


class YearEndClosingController extends CoreController
{

	protected $sideMenus = 'accounting_side_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Settings', null, 'Accounting', 'Year End Closing');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/year-end-closing.js');

        $yearEndClosingForm = $this->getYearEndClosingForm();

        $yearEndClosingView = new ViewModel(
            array(
            'yearEndClosingForm'=>$yearEndClosingForm,
            )
        );

        return $yearEndClosingView;
    }

    private function getYearEndClosingForm()
    {
        $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllUnlockedFiscalPeriods();

        $fiscalPeriodArray = array();
        foreach ($fiscalPeriodData as $value) {
                $fiscalPeriodArray[$value['fiscalPeriodID']] = $value['fiscalPeriodStartDate']." - ".$value['fiscalPeriodEndDate'];
        }
        
    	$FinanceAccountsData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAll();
        
        $FinanceAccountsArray = array();
        foreach ($FinanceAccountsData as $value) {
                if ($value['financeAccountStatusID'] == "1") {
                    $FinanceAccountsArray[$value['financeAccountsID']] = $value['financeAccountsCode']."_".$value['financeAccountsName'];
                }
        }

        $yearEndClosingForm = new YearEndClosingForm('CreateFinanceGroups',
                array(
            'financeAccounts' => $FinanceAccountsArray,
            'fiscalPeriods' => $fiscalPeriodArray,
        ));

        return $yearEndClosingForm;
    }

}