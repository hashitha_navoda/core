<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\YearEndClosingForm;


class AccountDashboardController extends CoreController
{

	protected $sideMenus = 'accounting_side_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Dashboard', null, 'Accounting');
        
        //get existing banks
        $bankList = array();
        $banks = $this->CommonTable('Expenses\Model\AccountTable')->fetchAll(false, array('account.accountId DESC'));
        foreach ($banks as $bank) {
            $bankList[$bank['accountId']] = $bank;
        }

        $viewmodel = new ViewModel(array(
            'bankList' => $bankList,
            'companyCurrencySymbol' => $this->companyCurrencySymbol
        ));

        $this->getViewHelper('HeadLink')->prependStylesheet('/css/account-dashboard.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/account-dashboard.js');

        return $viewmodel;
    }
}
