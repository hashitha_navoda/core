<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\JournalEntryForm;
use Accounting\Form\JournalEntryAccountsForm;
use Zend\I18n\Translator\Translator;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Journal Entries related actions
 */
class JournalEntriesController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'journal_entries_upper_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Journal Entries', 'Create', 'Accounting');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/journal-entry.js');

        $journalEntryID = $this->params()->fromRoute('param1');

        $journalEntryForm = $this->getJournalEntryForm();
        $journalEntryAccountsForm = $this->getJournalEntryAccountsForm();

        $journalEntryAccountsData = [];
        if($journalEntryID != ''){
            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryTypeID = $journalEntries['journalEntryTypeID'];
            $journalEntryTypeName = $journalEntries['journalEntryTypeName'];
            $journalEntryIsReverse = $journalEntries['journalEntryIsReverse'];
            $journalEntryComment = $journalEntries['journalEntryComment'];
            $journalEntryForm->get('journalEntryTypeID')->setAttribute('data-id', $journalEntryTypeID)->setAttribute('data-value',$journalEntryTypeName);
            $journalEntryForm->get('journalEntryIsReverse')->setAttribute('data-checked', $journalEntryIsReverse);
            $journalEntryForm->get('journalEntryTemplateStatus')->setAttribute('disabled', true);
            $journalEntryForm->get('journalEntryForOpeningBalance')->setAttribute('disabled', true);
            $journalEntryForm->get('journalEntryComment')->setValue($journalEntryComment);
            
            $JEntryAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);
        
            foreach ($JEntryAccounts as $key => $value) {
                $journalEntryAccountsData[] = array(
                    'financeAccountsID' => $value['financeAccountsID'],
                    'financeAccountsCodeAndName' => $value['financeAccountsCode']."_".$value['financeAccountsName'],
                    'financeGroupsID' => $value['financeGroupsID'],
                    'financeGroupsName' => $value['financeGroupsName'],
                    'journalEntryAccountsDebitAmount' => $value['journalEntryAccountsDebitAmount'],
                    'journalEntryAccountsCreditAmount' => $value['journalEntryAccountsCreditAmount'],
                    'journalEntryAccountsMemo' => $value['journalEntryAccountsMemo'],
                );
            }
        }

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

      	$locationID = $this->user_session->userActiveLocation['locationID'];
      	$refData = $this->getReferenceNoForLocation(30, $locationID);
      	$rid = $refData["refNo"];
      	$lrefID = $refData["locRefID"];
        $journalEntryForm->get('journalEntryCode')->setValue($rid)->setAttribute('data-id', $rid)->setAttribute('disabled',true);
        
      	//taking the user selected date format from the database
        $userdateFormat = $this->getUserDateFormat(); 
      	$journalEntryForm->get('journalEntryDate')->setAttribute('data-date-format',$userdateFormat);
      	
      	$journalEntryView = new ViewModel(
            array(
            	'journalEntryForm' => $journalEntryForm,
                'journalEntryAccountsForm' => $journalEntryAccountsForm,
            	'journalEntryAccounts' => $journalEntryAccountsData,
            )
        );

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $journalEntryView->addChild($dimensionAddView, 'dimensionAddView');

      	if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Journal Entry Reference Number not set';
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_ADD_REF');
            } else {
                $title = 'Journal Entry Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $journalEntryView->addChild($refNotSet, 'refNotSet');
        }

        return $journalEntryView;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Journal Entries', 'View Journal Entries', 'Accounting');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/journal-entry-list.js');

        $documentTypes = $this->CommonTable('Settings\Model\DocumentTypeTable')->fetchAll();

        $docTypes = array();
        foreach ($documentTypes as $dkey => $dvalue) {
            if(!($dvalue->documentTypeID == 2 || $dvalue->documentTypeID == 3 || $dvalue->documentTypeID == 8 || $dvalue->documentTypeID == 9 || $dvalue->documentTypeID == 15 || $dvalue->documentTypeID == 19 || $dvalue->documentTypeID == 21 || $dvalue->documentTypeID == 22 || $dvalue->documentTypeID == 23)){
                $docTypes[] = $dvalue;
            }
        }

        $this->getPaginatedJournalEntry(false);
        
        $dateFormat = $this->getUserDateFormat();
        $journalEntryView = new ViewModel(array(
            'journalEntry' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'documentTypes' => (object) $docTypes,
            'dateFormat' => $dateFormat,
            )
        );

        return $journalEntryView;
    }

    public function journalEntryTemplateListAction()
    {
        $this->getSideAndUpperMenus('Journal Entries', 'View Journal Entries Template', 'Accounting');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/journal-entry-list.js');

        $this->getPaginatedJournalEntry(true);
        $openingbalanceJournalEntry = $this->CommonTable('Accounting\Model\JournalEntryTable')->fetchAll(false,false,true);
        $journalEntryView = new ViewModel(array(
            'journalEntry' => $this->paginator,
            'openingbalanceJournalEntry' => count($openingbalanceJournalEntry),
            'statuses' => $this->getStatusesList(),
                )
        );

        return $journalEntryView;
    }

     private function getPaginatedJournalEntry($journalEntryTemplateFlag = false)
    {
        $this->paginator = $this->CommonTable('Accounting\Model\JournalEntryTable')->fetchAll(true,$journalEntryTemplateFlag);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

     private function getJournalEntryForm()
    {
        $journalEntryForm = new JournalEntryForm('CreateJournalEntry');

        return $journalEntryForm;
    }

     private function getJournalEntryAccountsForm()
    {

        $journalEntryAccountsForm = new JournalEntryAccountsForm('CreateJournalEntryAccounts',
                array(
            'financeAccounts' => array(''),
            'financeGroups' => array(''),
        ));

        return $journalEntryAccountsForm;
    }
    //////////////////////////////////////////////
    public function documentPreviewAction()
    {
        $jEID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($jEID);
        

        $path = "/journal-entries/document/"; //.$invoiceID;
        $translator = new Translator();
        $createNew = $translator->translate('Create JE');
        $createPath = "/journal-entries/create";
        
        $data["email"] = array(
            "to" => '',
            "subject" => "Journal Entry from " . $data['companyName'],
            "body" => <<<EMAILBODY


Thank you for your business. <br /><br />

Below Journal Entry document is attached herewith. <br /><br />

<strong>Journal Entry Code:</strong> {$data['jECode']} <br />
<strong>Journal Entry Date:</strong> {$data['jEDate']} <br />
<strong>Journal Entry Comment:</strong> {$data['jEComment']} <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );
        $documentType = 'Journal Entry';
              
        $jobView = $this->getCommonPreview($data, $path, $createNew, $documentType, $jEID, $createPath);
        $jobView->setTerminal(TRUE);
        return $jobView;
    }

    public function documentPdfAction()
    {
        $documentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        $documentType = 'Journal Entry';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/accounting/journal-entries/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($documentID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($documentID, $documentType, $documentData, $templateID);

        return;
    }

    public function getDataForDocument($jEID)
    {
        if (!empty($this->_jEViewData)) {
            return $this->_jEViewData;
        }
        //get company details
        $data = $this->getDataForDocumentView();

        //get journalEntry Details
        $jEDetails = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDetailsByJEID($jEID);
        $jEDataSet = [];
        foreach ($jEDetails as $jKey => $jEValue) {
            $jEDataSet['jECode'] = $jEValue['journalEntryCode'];
            $jEDataSet['jEDate'] = $jEValue['journalEntryDate'];
            $jEDataSet['jEComment'] = $jEValue['journalEntryComment'];
            $jEDataSet['location'] = $jEValue['locationName'].' - '.$jEValue['locationCode'];

            $accountSet = array(
                'accountName' => $jEValue['financeAccountsName'],
                'accountCode' => $jEValue['financeAccountsCode'],
                'debit' => $jEValue['journalEntryAccountsDebitAmount'],
                'credit' => $jEValue['journalEntryAccountsCreditAmount'],
                'memo' => $jEValue['journalEntryAccountsMemo'],
                );
            $jEDataSet['accounts'][] = $accountSet;
            
        }
            
        $jECompleteDataSet = array_merge($data, $jEDataSet);
       
        
        return $this->_jEViewData = $jECompleteDataSet;
    }

    public function documentAction()
    {        

        $documentType = 'Journal Entry';
        $jEID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $jEID, $templateID);

        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->getDataForDocument($jEID);

        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);

        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->getDocumentDataTable($jEID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
        ///////////////
    }

    public function getDocumentDataTable($jEID, $documentSize = 'A4')
    {
        
        $data_table_vars = $this->getDataForDocument($jEID);
        
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/accounting/journal-entries/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }


    //////////////////////////////////////////////


}
