<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\FinanceGroupsForm;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Groups related actions
 */
class GroupsController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'groups_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Create', 'Accounting', 'Groups');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/groups.js');

        $financeGroupsID = $this->params()->fromRoute('param1');
        
        $fiananceGroupsForm = $this->getFinanceGroupsForm($financeGroupsID);

        if ($financeGroupsID) {
            $groups = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->getGroupsDataByGroupsID($financeGroupsID);
            $fiananceGroupsForm->get('financeGroupsName')->setValue($groups['financeGroupsName']);
            $fiananceGroupsForm->get('financeGroupsShortName')->setValue($groups['financeGroupsShortName']);
            $fiananceGroupsForm->get('financeGroupTypesID')->setValue($groups['financeGroupTypesID']);
            $fiananceGroupsForm->get('financeGroupsAddress')->setValue($groups['financeGroupsAddress']);
            $fiananceGroupsForm->get('financeGroupsParentID')->setValue($groups['financeGroupsParentID']);
            $fiananceGroupsForm->get('entityID')->setValue($groups['entityID']);
            $fiananceGroupsForm->get('financeGroupsID')->setValue($financeGroupsID);
            $fiananceGroupsForm->get('createFinanceGroups')->setValue('Update');
        }
        
        $financeGroupsView = new ViewModel(
            array(
            'financeGroupsForm'=>$fiananceGroupsForm,
            )
        );
        return $financeGroupsView;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Settings', 'View Groups', 'Accounting', 'Groups');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/groups-list.js');

        $this->getPaginatedGroups();
        $groupsView = new ViewModel(array(
            'financeGroups' => $this->paginator,
                )
        );
        return $groupsView;
    }

     private function getPaginatedGroups()
    {
        $this->paginator = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    private function getFinanceGroupsForm($financeGroupsID = NULL)
    {
    	$fGroupTypesData = $this->CommonTable('Accounting\Model\FinanceGroupTypesTable')->fetchAll();
        $fGroupTypesArray = array();
        foreach ($fGroupTypesData as $value) {
            $fGroupTypesArray[$value->financeGroupTypesID] = $value->financeGroupTypesName;           
        }

        $fGroupsData = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->fetchAll();
        $fGroupsArray = array();
        foreach ($fGroupsData as $value) {
            if($financeGroupsID != $value['financeGroupsID']){
                $fGroupsArray[$value['financeGroupsID']] = $value['financeGroupsName'];           
            }
        }

        $fiananceGroupsForm = new FinanceGroupsForm('CreateFinanceGroups',
                array(
            'financeGroupTypes' => $fGroupTypesArray,
            'financeGroups' => $fGroupsArray,
        ));

        return $fiananceGroupsForm;
    }
}
