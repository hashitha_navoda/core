<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Invoice\Form\DeliveryNoteForm;
use Inventory\Form\SupplierForm;
use Zend\I18n\Translator\Translator;
use Zend\Session\Container;
use Invoice\Form\CustomerPaymentsForm;

class IncomeController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'incomes_upper_menu';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Incomes', 'Create Income', 'Accounting');
       
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(42, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

//      Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();
        //get price list
        $priceList = $this->getActivePriceList();

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        foreach ($paymentTermsResultSet as $row) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'userLocations',
            'taxes'
        ]);

        $deliveryNoteForm = new DeliveryNoteForm(
                array(
            'salesPersons' => $salesPersons,
            'paymentTerms' => $paymentTerms,
            'customCurrency' => $currency,
            'priceList' => $priceList,
                )
        );

        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(false, array('bank.bankName' => 'ASC'));
        foreach ($banks as $t) {
            $t = (object) $t;
            $bank[$t->bankId] = $t->bankName;
        }

        $gCard = $this->CommonTable('Inventory\Model\GiftCardTable')->getAllOpenGiftCards();
        foreach ($gCard as $g) {
            $g = (object) $g;
            $giftCards[$g->giftCardId] = $g->giftCardCode;
        }

        $paymethod = array();
        $payMethodAcc = array();
        $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentMethos as $t) {
            if($t['paymentMethodInSales'] == 1){
                $paymethod[$t['paymentMethodID']] = $t['paymentMethodName'];
                $payMethodAcc[$t['paymentMethodID']] = $t;
            }
        }

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();
        $form2 = new CustomerPaymentsForm(array(
            'paymentTerm' => $paymentTerms,
            'banks' => $bank,
            'giftCards' => $giftCards,
            'paymentMethod' => $paymethod,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'companyCurrencyId' => $this->companyCurrencyId,
            'customCurrency' => $currency,
        ));

        $displaySetup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll()->current();
        $deliveryNoteForm->get('customCurrencyId')->setValue($displaySetup->currencyID);
        $deliveryNoteForm->get('discount')->setAttribute('id', 'deliveryNoteDiscount');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/incomeVoucher.js');
        // $this->getViewHelper('HeadScript')->prependFile('/js/invoice/payments/payments.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/calculations.js');

        $userdateFormat = $this->getUserDateFormat(); ////////

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');

        $cardTypes = $this->CommonTable('Expenses\Model\CardTypeTable')->getAssignedCardTypes();
        foreach ($cardTypes as $cardType) {
            $cardTypeList[$cardType['cardTypeID']] = $cardType['cardTypeName'];
        }

        $deliveryNoteAddView = new ViewModel(
                array(
            'deliveryNoteForm' => $deliveryNoteForm,
            'form2' => $form2,
            'locationID' => $locationID,
            'locationName' => $locationName,
            'locationCode' => $locationCode,
            'referenceNumber' => $rid,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'packageID' => $this->packageID,
            'userdateFormat' => $userdateFormat,
            'cardTypeList' => ($cardTypeList) ? $cardTypeList : [],
            'payMethodAcc' =>$payMethodAcc,
            'useAccounting' => $this->useAccounting
                )
        );
        $deliveryNoteProductsView = new ViewModel();
        $deliveryNoteProductsView->setTemplate('invoice/delivery-note/delivery-note-add-products');
        $deliveryNoteAddView->addChild($deliveryNoteProductsView, 'deliveryNoteAddProducts');
        $deliveryNoteAddView->addChild($dimensionAddView, 'dimensionAddView');
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Delivery Note Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Delivery Note Reference Number has reache the maximum limit ';
                $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $deliveryNoteAddView->addChild($refNotSet, 'refNotSet');
        }
        return $deliveryNoteAddView;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Incomes', 'View Income', 'Accounting');
        $this->setLogMessage("Income View List accessed");

        $this->getPaginatedIncomes();

        $userdateFormat = $this->getUserDateFormat();
        $incomeView = new ViewModel(array(
            'income' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'userdateFormat' => $userdateFormat,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
                )
        );
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'allCustomerNames'
        ]);
       
        return $incomeView;
    }

    //payment method edit action
    public function editAction()
    {
        $this->getSideAndUpperMenus('Incomes', 'View Income', 'Accounting');
        $incomeID = $this->params()->fromRoute('param1');
        $incomeData = $this->CommonTable('Accounting\Model\IncomeTable')->getIncomeforSearch($incomeID)->current();
        $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getIncomePaymentsByIncomeID($incomeID)->current();
        $paymentMethods = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getPaymentMethodAllDetailsByPaymentId($paymentData['incomingPaymentID']);

        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(false, array('bank.bankName' => 'ASC'));
        foreach ($banks as $t) {
            $t = (object) $t;
            $bank[$t->bankId] = $t->bankName;
        }

        $gCard = $this->CommonTable('Inventory\Model\GiftCardTable')->getAllOpenGiftCards();
        foreach ($gCard as $g) {
            $g = (object) $g;
            $giftCards[$g->giftCardId] = $g->giftCardCode;
        }

        $currencyRate = 1;
        if ($paymentData['incomingPaymentCustomCurrencyRate'] > 0) {
            $currencyRate = $paymentData['incomingPaymentCustomCurrencyRate'];
        }
        $currencySymbol = $this->companyCurrencySymbol;
        if ($paymentData['customCurrencyId'] != '') {
            $currencySymbol = $paymentData['currencySymbol'];
        }
        $userdateFormat = $this->getUserDateFormat();

        $paymentEdit = new ViewModel(array(
            'paymentMethods' => $paymentMethods,
            'banks' => $bank,
            'giftCards' => $giftCards,
            'paymentCode' => $paymentData['incomingPaymentCode'],
            'incomeID' => $incomeID,
            'comment' => $paymentData['incomingPaymentMemo'],
            'incomingPaymentAmount' => $paymentData['incomingPaymentAmount'],
            'incomingPaymentID' => $paymentData['incomingPaymentID'],
            'currencyRate' => $currencyRate,
            'userdateFormat' => $userdateFormat,
            'incomeData' => $incomeData,
            'currencySymbol' => $currencySymbol,
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/editIncome.js');
        return $paymentEdit;
    }

    private function getPaginatedIncomes()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Accounting\Model\IncomeTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function previewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $data = $this->_getDataForIncomeView($paramIn);
            $data['incomeCode'] = $data['incomeCode'];
            $data['total'] = number_format($data['incomeTotal'], 2);
            $path = "/income/document/"; //.$paramIn;
            $translator = new Translator();
            $createNew = $translator->translate('New Income');
            $createPath = "/income/create";

            $globaldata = $this->getServiceLocator()->get('config');

            $data["email"] = array(
                "to" => $data['customerEmail'],
                "subject" => "Income from " . $data['companyName'],
                "body" => <<<EMAILBODY

Dear {$data['customerName']}, <br /><br />

Thank you for your inquiry. <br /><br />

An Income has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>Income Number:</strong> {$data['incomeCode']} <br />
<strong>Income Amount:</strong> {$data['currencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
            );

            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountFlag = (count($glAccountExist) > 0)? true :false;
            $journalEntryValues = [];
            if($glAccountFlag){
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('43',$paramIn);
                $journalEntryID = $journalEntryData['journalEntryID'];
                $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
                $journalEntryValues = $jeResult['JEDATA'];
            }

            $JEntry = new ViewModel(
                array(
                 'journalEntry' => $journalEntryValues,
                 'closeHidden' => true
                 )
                );
            $JEntry->setTemplate('accounting/journal-entries/view-modal');

            $documentType = 'Income';
            $state = $globaldata['statuses'][$data['piSt']];

            $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paramIn, $createPath);
            $preview->addChild($JEntry, 'JEntry');
            $additionalButton = new ViewModel(array('state' => $state, 'data' => $data,'glAccountFlag' => $glAccountFlag));
            $additionalButton->setTemplate('accounting/income/additionalButton');
            $preview->addChild($additionalButton, 'additionalButton');

            $preview->setTerminal(true);

            return $preview;
        }
    }

    public function documentAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = 'Income';
        $incomeID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // $paymentData=$this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentIDByPaymentVoucherID($incomeID);
        // $paymentIDs = array();

        // foreach($paymentData as $key => $value) {
        //     $paymentIDs[]=$value['paymentID'];
        // }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $incomeID, $templateID);
//        if ($document = $tpl->getDocumentFile($filename, $documentType)) {
//            echo $document;
//            exit;
//        }
        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForIncomeView($incomeID);
        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);
        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($incomeID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                // $data['payment_table'] = $this->_getDocumentPaymentTable($paymentIDs, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])).'-payment'));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    // private function _getDocumentPaymentTable($paymentIDs, $documentSize = 'A4') {
    //     $data_table_vars = array();
    //     $currencySymbol = null;
    //     $total = 0.0;
    //     $discountTot = 0.0;
    //     $i = 0;
    //     if(count($paymentIDs) > 0 ){
    //         foreach ($paymentIDs as $pi){


    //             $data_table_vars[] = $this->_getDataForPaymentView($pi);
    //             $currencySymbol = $data_table_vars[0]['currencySymbol'];
    //             $leftToPay = $data_table_vars[$i]['leftToPay'];
    //             $total += $data_table_vars[$i]['total'];
    //             $discountTot += $data_table_vars[$i]['cr_discount'];
    //             $i++;
    //         }
    //     }

    //     $view = new ViewModel(
    //         array(
    //             'paymentDetails' => $data_table_vars,
    //             'currencySymbol' => $currencySymbol,
    //             'settledTotal' => $total,
    //             'discountTotal' => $discountTot,
    //             'leftToPay' => $leftToPay
    //         ));
    //     $view->setTemplate('/inventory/purchase-invoice/templates/' . strtolower($documentSize));
    //     $view->setTerminal(TRUE);

    //     return $this->getServiceLocator()
    //                     ->get('viewrenderer')
    //                     ->render($view);
    // }


    public function _getDocumentDataTable($incomeID, $documentSize = 'A4')
    {
        $data_table_vars = $this->_getDataForIncomeView($pvID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/accounting/income/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }


    public function _getDataForIncomeView($incomeID)
    {
        if (!empty($this->_incViewDetails)) {
            return $this->_incViewDetails;
        }
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $data = $this->getDataForDocumentView(); // get comapny details
        $incomePData = $this->CommonTable('Accounting\Model\IncomeTable')->getIncomeProductByIncomeID($incomeID)->current();

        $inventoryFlag = true;
        if (!$incomePData) {
            $inventoryFlag = false;
        }
        $incomeData = $this->CommonTable('Accounting\Model\IncomeTable')->getIncomeDetailsByIncomeID($incomeID, $locationID, $inventoryFlag);
        $incomeDetails = array();
        foreach ($incomeData as $row) {
            $incomeDetails['incomeID'] = $row['incomeID'];
            $incomeDetails['incomeCode'] = $row['incomeCode'];
            $incomeDetails['customerName'] = $row['customerTitle'] . ' ' . $row['customerName'];
            $incomeDetails['incomeLocation'] = $row['locationCode'] . '-' . $row['locationName'];
            $incomeDetails['incomeDate'] = $this->convertDateToUserFormat($row['incomeDate']);
            $incomeDetails['incomeTotal'] = $row['incomeTotal'];
            $incomeDetails['incomeComment'] = $row['incomeComment'];
            $incomeDetails['status'] = $row['status'];
            $incomeDetails['userUsername'] = $row['userUsername'];
            $incomeDetails['createdTimeStamp'] = $this->getUserDateTime($row['createdTimeStamp']);
            if ($incomeDetails['itemProduct'][$row['incomeProductID']]['productID'] != $row['incomeProductID']) {
                $incomeDetails['itemProduct'][$row['incomeProductID']]['incomeProductID'] = $row['incomeProductID'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['locationProductID'] = $row['locationProductID'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['incomeProductQuantity'] = $row['incomeProductQuantity'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['incomeProductPrice'] = $row['incomeProductPrice'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['incomeProductTotal'] = $row['incomeProductTotal'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['productID'] = $row['productID'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['productCode'] = $row['productCode'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['productName'] = $row['productName'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['uomID'] = $row['uomID'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['productUomConversion'] = $row['productUomConversion'];
                $incomeDetails['itemProduct'][$row['incomeProductID']]['uomAbbr'] = $row['uomAbbr'];

                //calculate discount value
                $discountValue = 0;
                if ($row['incomeProductDiscountType'] == "precentage") {
                    $discountValue = ($row['incomeProductPrice'] / 100 * $row['incomeProductDiscount']) * $row['incomeProductQuantity'];
                } else if ($row['incomeProductDiscountType'] == "value") {
                    $discountValue = $row['productDiscount'];
                }

                $incomeDetails['itemProduct'][$row['incomeProductID']]['itemDiscount'] = $discountValue;
                if ($row['incomeTaxItemOrNonItemProduct'] == 'inventory' && $row['incomeProductIdOrNonItemProductID'] == $row['incomeProductID']) {
                    $incomeDetails['itemProduct'][$row['incomeProductID']]['tax'][$row['incomeTaxID']]['incomeTaxID'] = $row['incomeTaxID'];
                    $incomeDetails['itemProduct'][$row['incomeProductID']]['tax'][$row['incomeTaxID']]['incomeTaxPrecentage'] = $row['incomeTaxPrecentage'];
                    $incomeDetails['itemProduct'][$row['incomeProductID']]['tax'][$row['incomeTaxID']]['incomeTaxAmount'] = $row['incomeTaxAmount'];
                    $incomeDetails['itemProduct'][$row['incomeProductID']]['tax'][$row['incomeTaxID']]['taxName'] = $row['taxName'];
                }
            }
            if (!is_null($row['incomeNonItemProductID'])) {
                if ($incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['productID'] != $row['incomeNonItemProductID']) {
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['productID'] = $row['incomeNonItemProductID'];
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['incomeNonItemProductDiscription'] = $row['incomeNonItemProductDiscription'];
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['incomeNonItemProductQuantity'] = $row['incomeNonItemProductQuantity'];
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['incomeNonItemProductUnitPrice'] = $row['incomeNonItemProductUnitPrice'];
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['incomeNonItemProductTotalPrice'] = $row['incomeNonItemProductTotalPrice'];

                   
                    $gl_type = $row['financeAccountsCode'] . '_' . $row['financeAccountsName'];
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['incomeNonItemProductGLAccount'] = $gl_type;
                }
                if ($row['incomeTaxItemOrNonItemProduct'] != 'item' && $row['incomeProductIdOrNonItemProductID'] == $row['incomeNonItemProductID']) {
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['tax'][$row['incomeTaxID']]['incomeTaxID'] = $row['incomeTaxID'];
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['tax'][$row['incomeTaxID']]['incomeTaxPrecentage'] = $row['incomeTaxPrecentage'];
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['tax'][$row['incomeTaxID']]['incomeTaxAmount'] = $row['incomeTaxAmount'];
                    $incomeDetails['nonItemProduct'][$row['incomeNonItemProductID']]['tax'][$row['incomeTaxID']]['taxName'] = $row['taxName'];
                }
            }
        }
        $dataa = array_merge($data, $incomeDetails);
        // $paymentVoucherMethodData = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentsDetailsByPaymentVoucherID($incomeID);
        // $pvMethod="";
        // foreach ($paymentVoucherMethodData as $key => $value) {
        //     if($value['paymentMethodID']==2){
        //         $pvMethod.=$value['outGoingPaymentMethodReferenceNumber']."(".$value['bankName'].")"."\r\n";
        //     }
        // }
        // $dataa['check_and_bank'] = $pvMethod;
        $dataa['today_date'] = gmdate('Y-m-d');
        $dataa['currencySymbol'] = $this->companyCurrencySymbol;
        return $this->_incViewDetails = $dataa;
    }
}
