<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\GlAccountSetupForm;
use Accounting\Form\PaymentMethodSetupForm;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Gl Account Setup related actions
 */
class GlAccountSetupController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'account_settings_accounts_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Settings', 'GL Account Setup', 'Accounting', 'Accounts Settings');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/gl-account-setup.js');

        $glAccountSetupForm = new GlAccountSetupForm('CreateGlAccountSetup');

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if (count($glAccountExist) > 0) {
            
            $glAccountSetupData = $glAccountExist->current();
            
            $financeAccountsArray = $this->getFinanceAccounts();

            $glAccountSetupID = $glAccountSetupData->glAccountSetupID;
            $glAccountSetupForm->get('glAccountSetupID')->setValue($glAccountSetupID);        
            $glAccountSetupForm->get('saveGlAccountSetup')->setValue("Update");        
            
            $glAccountSetupItemDefaultSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
            $glAccountSetupItemDefaultSalesAccountName = $financeAccountsArray[$glAccountSetupItemDefaultSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultSalesAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultSalesAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultSalesAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultSalesAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultSalesAccountName);        
            
            $glAccountSetupItemDefaultInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
            $glAccountSetupItemDefaultInventoryAccountName = $financeAccountsArray[$glAccountSetupItemDefaultInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultInventoryAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultInventoryAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultInventoryAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultInventoryAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultInventoryAccountName);        
            
            $glAccountSetupItemDefaultCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
            $glAccountSetupItemDefaultCOGSAccountName = $financeAccountsArray[$glAccountSetupItemDefaultCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultCOGSAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultCOGSAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultCOGSAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultCOGSAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultCOGSAccountName);        
            
            $glAccountSetupItemDefaultAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
            $glAccountSetupItemDefaultAdjusmentAccountName = $financeAccountsArray[$glAccountSetupItemDefaultAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupItemDefaultAdjusmentAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupItemDefaultAdjusmentAccountID')->setAttribute('data-id',$glAccountSetupItemDefaultAdjusmentAccountID);        
            $glAccountSetupForm->get('glAccountSetupItemDefaultAdjusmentAccountID')->setAttribute('data-value',$glAccountSetupItemDefaultAdjusmentAccountName);

            $glAccountSetupSalesAndCustomerReceivableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
            $glAccountSetupSalesAndCustomerReceivableAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerReceivableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerReceivableAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerReceivableAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerReceivableAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerReceivableAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerReceivableAccountName);        
        
            $glAccountSetupSalesAndCustomerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
            $glAccountSetupSalesAndCustomerSalesAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerSalesAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerSalesAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerSalesAccountName);        
            
            $glAccountSetupSalesAndCustomerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
            $glAccountSetupSalesAndCustomerSalesDiscountAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerSalesDiscountAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesDiscountAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerSalesDiscountAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerSalesDiscountAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerSalesDiscountAccountName);        
            
            $glAccountSetupSalesAndCustomerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
            $glAccountSetupSalesAndCustomerAdvancePaymentAccountName = $financeAccountsArray[$glAccountSetupSalesAndCustomerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupSalesAndCustomerAdvancePaymentAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerAdvancePaymentAccountID')->setAttribute('data-id',$glAccountSetupSalesAndCustomerAdvancePaymentAccountID);        
            $glAccountSetupForm->get('glAccountSetupSalesAndCustomerAdvancePaymentAccountID')->setAttribute('data-value',$glAccountSetupSalesAndCustomerAdvancePaymentAccountName);        
            
            $glAccountSetupPurchasingAndSupplierPayableAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID;
            $glAccountSetupPurchasingAndSupplierPayableAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierPayableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierPayableAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPayableAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierPayableAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPayableAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierPayableAccountName);        
            
            $glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID;
            $glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountName);        
            
            $glAccountSetupPurchasingAndSupplierGrnClearingAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID;
            $glAccountSetupPurchasingAndSupplierGrnClearingAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierGrnClearingAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierGrnClearingAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierGrnClearingAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierGrnClearingAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierGrnClearingAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierGrnClearingAccountName);        
            
            $glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID;
            $glAccountSetupPurchasingAndSupplierAdvancePaymentAccountName = $financeAccountsArray[$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID')->setAttribute('data-id',$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID);        
            $glAccountSetupForm->get('glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID')->setAttribute('data-value',$glAccountSetupPurchasingAndSupplierAdvancePaymentAccountName);        
    
            $glAccountSetupGeneralExchangeVarianceAccountID = $glAccountSetupData->glAccountSetupGeneralExchangeVarianceAccountID;
            $glAccountSetupGeneralExchangeVarianceAccountName = $financeAccountsArray[$glAccountSetupGeneralExchangeVarianceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralExchangeVarianceAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralExchangeVarianceAccountID')->setAttribute('data-id',$glAccountSetupGeneralExchangeVarianceAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralExchangeVarianceAccountID')->setAttribute('data-value',$glAccountSetupGeneralExchangeVarianceAccountName);        
            
            $glAccountSetupGeneralProfitAndLostYearAccountID = $glAccountSetupData->glAccountSetupGeneralProfitAndLostYearAccountID;
            $glAccountSetupGeneralProfitAndLostYearAccountName = $financeAccountsArray[$glAccountSetupGeneralProfitAndLostYearAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralProfitAndLostYearAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralProfitAndLostYearAccountID')->setAttribute('data-id',$glAccountSetupGeneralProfitAndLostYearAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralProfitAndLostYearAccountID')->setAttribute('data-value',$glAccountSetupGeneralProfitAndLostYearAccountName);        

            $glAccountSetupGeneralBankChargersAccountID = $glAccountSetupData->glAccountSetupGeneralBankChargersAccountID;
            $glAccountSetupGeneralBankChargersAccountName = $financeAccountsArray[$glAccountSetupGeneralBankChargersAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralBankChargersAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralBankChargersAccountID')->setAttribute('data-id',$glAccountSetupGeneralBankChargersAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralBankChargersAccountID')->setAttribute('data-value',$glAccountSetupGeneralBankChargersAccountName);     

            $glAccountSetupGeneralDeliveryChargersAccountID = $glAccountSetupData->glAccountSetupGeneralDeliveryChargersAccountID;
            $glAccountSetupGeneralDeliveryChargersAccountName = $financeAccountsArray[$glAccountSetupGeneralDeliveryChargersAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralDeliveryChargersAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralDeliveryChargersAccountID')->setAttribute('data-id',$glAccountSetupGeneralDeliveryChargersAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralDeliveryChargersAccountID')->setAttribute('data-value',$glAccountSetupGeneralDeliveryChargersAccountName);           
            
            $glAccountSetupGeneralLoyaltyExpenseAccountID = $glAccountSetupData->glAccountSetupGeneralLoyaltyExpenseAccountID;
            $glAccountSetupGeneralLoyaltyExpenseAccountName = $financeAccountsArray[$glAccountSetupGeneralLoyaltyExpenseAccountID]['financeAccountsCode']."_".$financeAccountsArray[$glAccountSetupGeneralLoyaltyExpenseAccountID]['financeAccountsName'];
            $glAccountSetupForm->get('glAccountSetupGeneralLoyaltyExpenseAccountID')->setAttribute('data-id',$glAccountSetupGeneralLoyaltyExpenseAccountID);        
            $glAccountSetupForm->get('glAccountSetupGeneralLoyaltyExpenseAccountID')->setAttribute('data-value',$glAccountSetupGeneralLoyaltyExpenseAccountName);           
            
            
        }
        
        $glAccountSetupView = new ViewModel(
            array(
                'glAccountSetupForm' => $glAccountSetupForm,
            )
        );
        return $glAccountSetupView;
    }

    public function paymentMethodAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Payment Method Setup', 'Accounting', 'Accounts Settings');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/payment-method-setup.js');

        $paymentMethodSetupForm = new PaymentMethodSetupForm('CreatePaymentMethodSetup');

        $paymentMethods = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();

        $financeAccountsArray = $this->getFinanceAccounts();

        foreach ($paymentMethods as $key => $value) {
            $value = (object) $value;
            switch ($value->paymentMethodID) {
                case '1':
                    $salesCashFinanceAccountID = $value->paymentMethodSalesFinanceAccountID;
                    $salesCashFinanceAccountName = $financeAccountsArray[$salesCashFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$salesCashFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('salesCashFinanceAccountID')->setAttribute('data-id',$salesCashFinanceAccountID);        
                    $paymentMethodSetupForm->get('salesCashFinanceAccountID')->setAttribute('data-value',$salesCashFinanceAccountName);        
                    $paymentMethodSetupForm->get('salesCashFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInSales);        
                    $paymentMethodSetupForm->get('salesCashFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    
                    $purchaseCashFinanceAccountID = $value->paymentMethodPurchaseFinanceAccountID;
                    $purchaseCashFinanceAccountName = $financeAccountsArray[$purchaseCashFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$purchaseCashFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('purchaseCashFinanceAccountID')->setAttribute('data-id',$purchaseCashFinanceAccountID);        
                    $paymentMethodSetupForm->get('purchaseCashFinanceAccountID')->setAttribute('data-value',$purchaseCashFinanceAccountName);        
                    $paymentMethodSetupForm->get('purchaseCashFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInPurchase);        
                    $paymentMethodSetupForm->get('purchaseCashFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    break;

                 case '7':
                    $salesLcFinanceAccountID = $value->paymentMethodSalesFinanceAccountID;
                    $salesLcFinanceAccountName = $financeAccountsArray[$salesLcFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$salesLcFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('salesLcFinanceAccountID')->setAttribute('data-id',$salesLcFinanceAccountID);        
                    $paymentMethodSetupForm->get('salesLcFinanceAccountID')->setAttribute('data-value',$salesLcFinanceAccountName);        
                    $paymentMethodSetupForm->get('salesLcFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInSales);        
                    $paymentMethodSetupForm->get('salesLcFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    
                    $purchaseLcFinanceAccountID = $value->paymentMethodPurchaseFinanceAccountID;
                    $purchaseLcFinanceAccountName = $financeAccountsArray[$purchaseLcFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$purchaseLcFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('purchaseLcFinanceAccountID')->setAttribute('data-id',$purchaseLcFinanceAccountID);        
                    $paymentMethodSetupForm->get('purchaseLcFinanceAccountID')->setAttribute('data-value',$purchaseLcFinanceAccountName);        
                    $paymentMethodSetupForm->get('purchaseLcFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInPurchase);        
                    $paymentMethodSetupForm->get('purchaseLcFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    break;

                case '8':
                    $salesTtFinanceAccountID = $value->paymentMethodSalesFinanceAccountID;
                    $salesTtFinanceAccountName = $financeAccountsArray[$salesTtFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$salesTtFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('salesTtFinanceAccountID')->setAttribute('data-id',$salesTtFinanceAccountID);        
                    $paymentMethodSetupForm->get('salesTtFinanceAccountID')->setAttribute('data-value',$salesTtFinanceAccountName);        
                    $paymentMethodSetupForm->get('salesTtFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInSales);        
                    $paymentMethodSetupForm->get('salesTtFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    
                    $purchaseTtFinanceAccountID = $value->paymentMethodPurchaseFinanceAccountID;
                    $purchaseTtFinanceAccountName = $financeAccountsArray[$purchaseTtFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$purchaseTtFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('purchaseTtFinanceAccountID')->setAttribute('data-id',$purchaseTtFinanceAccountID);        
                    $paymentMethodSetupForm->get('purchaseTtFinanceAccountID')->setAttribute('data-value',$purchaseTtFinanceAccountName);        
                    $paymentMethodSetupForm->get('purchaseTtFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInPurchase);        
                    $paymentMethodSetupForm->get('purchaseTtFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    break;

                case '3':
                    $salesCardFinanceAccountID = $value->paymentMethodSalesFinanceAccountID;
                    $salesCardFinanceAccountName = $financeAccountsArray[$salesCardFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$salesCardFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('salesCardFinanceAccountID')->setAttribute('data-id',$salesCardFinanceAccountID);        
                    $paymentMethodSetupForm->get('salesCardFinanceAccountID')->setAttribute('data-value',$salesCardFinanceAccountName);        
                    $paymentMethodSetupForm->get('salesCardFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInSales);        
                    $paymentMethodSetupForm->get('salesCardFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    
                    $purchaseCardFinanceAccountID = $value->paymentMethodPurchaseFinanceAccountID;
                    $purchaseCardFinanceAccountName = $financeAccountsArray[$purchaseCardFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$purchaseCardFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('purchaseCardFinanceAccountID')->setAttribute('data-id',$purchaseCardFinanceAccountID);        
                    $paymentMethodSetupForm->get('purchaseCardFinanceAccountID')->setAttribute('data-value',$purchaseCardFinanceAccountName);        
                    $paymentMethodSetupForm->get('purchaseCardFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInPurchase);        
                    $paymentMethodSetupForm->get('purchaseCardFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    break;

                case '6':
                    $salesGiftCardFinanceAccountID = $value->paymentMethodSalesFinanceAccountID;
                    $salesGiftCardFinanceAccountName = $financeAccountsArray[$salesGiftCardFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$salesGiftCardFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('salesGiftCardFinanceAccountID')->setAttribute('data-id',$salesGiftCardFinanceAccountID);        
                    $paymentMethodSetupForm->get('salesGiftCardFinanceAccountID')->setAttribute('data-value',$salesGiftCardFinanceAccountName);        
                    $paymentMethodSetupForm->get('salesGiftCardFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInSales);        
                    $paymentMethodSetupForm->get('salesGiftCardFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    
                    $purchaseGiftCardFinanceAccountID = $value->paymentMethodPurchaseFinanceAccountID;
                    $purchaseGiftCardFinanceAccountName = $financeAccountsArray[$purchaseGiftCardFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$purchaseGiftCardFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('purchaseGiftCardFinanceAccountID')->setAttribute('data-id',$purchaseGiftCardFinanceAccountID);        
                    $paymentMethodSetupForm->get('purchaseGiftCardFinanceAccountID')->setAttribute('data-value',$purchaseGiftCardFinanceAccountName);        
                    $paymentMethodSetupForm->get('purchaseGiftCardFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInPurchase);        
                    $paymentMethodSetupForm->get('purchaseGiftCardFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    break;

                case '2':
                    $salesChequeFinanceAccountID = $value->paymentMethodSalesFinanceAccountID;
                    $salesChequeFinanceAccountName = $financeAccountsArray[$salesChequeFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$salesChequeFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('salesChequeFinanceAccountID')->setAttribute('data-id',$salesChequeFinanceAccountID);        
                    $paymentMethodSetupForm->get('salesChequeFinanceAccountID')->setAttribute('data-value',$salesChequeFinanceAccountName);        
                    $paymentMethodSetupForm->get('salesChequeFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInSales);        
                    $paymentMethodSetupForm->get('salesChequeFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    
                    $purchaseChequeFinanceAccountID = $value->paymentMethodPurchaseFinanceAccountID;
                    $purchaseChequeFinanceAccountName = $financeAccountsArray[$purchaseChequeFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$purchaseChequeFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('purchaseChequeFinanceAccountID')->setAttribute('data-id',$purchaseChequeFinanceAccountID);        
                    $paymentMethodSetupForm->get('purchaseChequeFinanceAccountID')->setAttribute('data-value',$purchaseChequeFinanceAccountName);        
                    $paymentMethodSetupForm->get('purchaseChequeFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInPurchase);        
                    $paymentMethodSetupForm->get('purchaseChequeFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    break;

                case '4':
                    $salesLoyaltyCardFinanceAccountID = $value->paymentMethodSalesFinanceAccountID;
                    $salesLoyaltyCardFinanceAccountName = $financeAccountsArray[$salesLoyaltyCardFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$salesLoyaltyCardFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('salesLoyaltyCardFinanceAccountID')->setAttribute('data-id',$salesLoyaltyCardFinanceAccountID);        
                    $paymentMethodSetupForm->get('salesLoyaltyCardFinanceAccountID')->setAttribute('data-value',$salesLoyaltyCardFinanceAccountName);        
                    $paymentMethodSetupForm->get('salesLoyaltyCardFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInSales);        
                    $paymentMethodSetupForm->get('salesLoyaltyCardFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    
                    $purchaseLoyaltyCardFinanceAccountID = $value->paymentMethodPurchaseFinanceAccountID;
                    $purchaseLoyaltyCardFinanceAccountName = $financeAccountsArray[$purchaseLoyaltyCardFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$purchaseLoyaltyCardFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('purchaseLoyaltyCardFinanceAccountID')->setAttribute('data-id',$purchaseLoyaltyCardFinanceAccountID);        
                    $paymentMethodSetupForm->get('purchaseLoyaltyCardFinanceAccountID')->setAttribute('data-value',$purchaseLoyaltyCardFinanceAccountName);        
                    $paymentMethodSetupForm->get('purchaseLoyaltyCardFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInPurchase);        
                    $paymentMethodSetupForm->get('purchaseLoyaltyCardFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    break;

                case '5':
                    $salesBankTransferFinanceAccountID = $value->paymentMethodSalesFinanceAccountID;
                    $salesBankTransferFinanceAccountName = $financeAccountsArray[$salesBankTransferFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$salesBankTransferFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('salesBankTransferFinanceAccountID')->setAttribute('data-id',$salesBankTransferFinanceAccountID);        
                    $paymentMethodSetupForm->get('salesBankTransferFinanceAccountID')->setAttribute('data-value',$salesBankTransferFinanceAccountName);        
                    $paymentMethodSetupForm->get('salesBankTransferFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInSales);        
                    $paymentMethodSetupForm->get('salesBankTransferFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    
                    $purchaseBankTransferFinanceAccountID = $value->paymentMethodPurchaseFinanceAccountID;
                    $purchaseBankTransferFinanceAccountName = $financeAccountsArray[$purchaseBankTransferFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$purchaseBankTransferFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('purchaseBankTransferFinanceAccountID')->setAttribute('data-id',$purchaseBankTransferFinanceAccountID);        
                    $paymentMethodSetupForm->get('purchaseBankTransferFinanceAccountID')->setAttribute('data-value',$purchaseBankTransferFinanceAccountName);        
                    $paymentMethodSetupForm->get('purchaseBankTransferFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInPurchase);        
                    $paymentMethodSetupForm->get('purchaseBankTransferFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    break;

                case '9':
                    $salesUniformVoucherFinanceAccountID = $value->paymentMethodSalesFinanceAccountID;
                    $salesUniformVoucherFinanceAccountName = $financeAccountsArray[$salesUniformVoucherFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$salesUniformVoucherFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('salesUniformVoucherFinanceAccountID')->setAttribute('data-id',$salesUniformVoucherFinanceAccountID);        
                    $paymentMethodSetupForm->get('salesUniformVoucherFinanceAccountID')->setAttribute('data-value',$salesUniformVoucherFinanceAccountName); 
                    $paymentMethodSetupForm->get('salesUniformVoucherFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInSales);        
                    $paymentMethodSetupForm->get('salesUniformVoucherFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    
                    $purchaseUniformVoucherFinanceAccountID = $value->paymentMethodPurchaseFinanceAccountID;
                    $purchasUniformVoucherFinanceAccountName = $financeAccountsArray[$purchaseUniformVoucherFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$purchaseUniformVoucherFinanceAccountID]['financeAccountsName'];
                    $paymentMethodSetupForm->get('purchaseUniformVoucherFinanceAccountID')->setAttribute('data-id',$purchaseGiftCardFinanceAccountID);        
                    $paymentMethodSetupForm->get('purchaseUniformVoucherFinanceAccountID')->setAttribute('data-value',$purchasUniformVoucherFinanceAccountName); 
                    $paymentMethodSetupForm->get('purchaseUniformVoucherFinanceAccountID')->setAttribute('data-checked',$value->paymentMethodInPurchase);        
                    $paymentMethodSetupForm->get('purchaseUniformVoucherFinanceAccountID')->setAttribute('data-methodid',$value->paymentMethodID);        
                    break;
                
                default:
                    # code...
                    break;
            }
        }

        $paymentMethodSetupView = new ViewModel(
            array(
                'paymentMethodSetupForm' => $paymentMethodSetupForm,
            )
        );
        return $paymentMethodSetupView;
    }
}
