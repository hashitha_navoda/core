<?php

namespace Accounting\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\FinanceAccountsForm;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Account  related actions
 */
class AccountsController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'accounts_upper_menu';

    public function createAction()
    {
        $this->getSideAndUpperMenus('Accounts', 'Create', 'Accounting');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/accounts.js');

        $financeAccountsID = $this->params()->fromRoute('param1');
        
        $fiananceAccountsForm = $this->getFinanceAccountForm();
        
        if ($financeAccountsID) {
            $accounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($financeAccountsID);
            $checkAccountIsUsed = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountID($financeAccountsID);
            $fiananceAccountsForm->get('financeAccountClassID')->setValue($accounts['financeAccountClassID']);
            if(count($checkAccountIsUsed) > 0){
                $fiananceAccountsForm->get('financeAccountClassID')->setAttribute('disabled','disabled');
                
            }
            $fiananceAccountsForm->get('financeAccountsID')->setValue($accounts['financeAccountsID']);
            $fiananceAccountsForm->get('financeAccountsCode')->setValue($accounts['financeAccountsCode']);
            $fiananceAccountsForm->get('financeAccountsName')->setValue($accounts['financeAccountsName']);
            $fiananceAccountsForm->get('financeAccountsParentID')->setValue($accounts['financeAccountsParentID']);
            $fiananceAccountsForm->get('financeAccountStatusID')->setValue($accounts['financeAccountStatusID']);
            $fiananceAccountsForm->get('fAccountsParentID')->setValue($accounts['financeAccountsParentID']);
            $fiananceAccountsForm->get('entityID')->setValue($accounts['entityID']);
            $fiananceAccountsForm->get('createFinanceAccounts')->setValue('Update');
        }
        
        $financeAccountsView = new ViewModel(
            array(
            'financeAccountsForm'=>$fiananceAccountsForm,
            )
        );
        return $financeAccountsView;
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Accounts', 'View Accounts', 'Accounting');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/accounts-list.js');

        $this->getPaginatedAccounts();
        $accountsView = new ViewModel(array(
            'financeAccounts' => $this->paginator,
            'statuses' => $this->getStatusesList(),
                )
        );
        return $accountsView;
    }

    public function chartOfAccountsAction()
    {   
        $this->upperMenus = '';
        $this->getSideAndUpperMenus('Chart Of Accounts', '', 'Accounting');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/chart-of-accounts.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/accounting/accounts.js');

        $fiananceAccountsForm = $this->getFinanceAccountForm();

        $fiananceAccountsForm->get('cancelFinanceAccounts')->setValue('Reset');
        $fiananceAccountsForm->get('cancelFinanceAccounts')->setAttribute('data-ncleaerclass',true);

        $financeAccountsView = new ViewModel(
            array(
            'financeAccountsForm'=>$fiananceAccountsForm,
            )
        );
        $financeAccountsView->setTemplate('accounting/accounts/accounts-create-modal.phtml');

        $fAccountClassData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClasses();
        
        $financeAccounts = array();

        foreach ($fAccountClassData as $key => $value) {
            $financeAccounts[$value['financeAccountClassID']]['financeAccountClassID'] = $value['financeAccountClassID'];
            $financeAccounts[$value['financeAccountClassID']]['financeAccountTypesName'] = $value['financeAccountTypesName'];
            $financeAccounts[$value['financeAccountClassID']]['financeAccountClassName'] = $value['financeAccountClassName'];
            $fAccountsData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getHierarchyByAccountClassID($value['financeAccountClassID']);
            $financeAccounts[$value['financeAccountClassID']]['financeAccounts'] = $fAccountsData ;
        }

        $chartOfAccountsView = new ViewModel(array(
            'financeAccounts' => $financeAccounts,
            'statuses' => $this->getStatusesList(),
                )
        );
        
        $chartOfAccountsView->addChild($financeAccountsView, 'financeAccountsView');
        return $chartOfAccountsView;
    }

     private function getPaginatedAccounts()
    {
        $this->paginator = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    private function getFinanceAccountForm()
    {
        $status = $this->getStatusesList();
        $statusList = array();
        foreach ($status as $key => $value) {
            if($value == 'active' || $value == 'inactive' || $value == 'locked'){
                $statusList[$key] = ucwords($value);
            }
        }

        $fAccountsClassData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClasses();
        $fAccountClasses = array();
        foreach ($fAccountsClassData as $value) {
            $fAccountClasses[$value['financeAccountClassID']] = $value['financeAccountClassName'];           
        }

        $fiananceAccountsForm = new FinanceAccountsForm( 'CreateFinanceAccounts',
            array(
                'financeAccounts' => array(''),
                'financeAccountClass' => $fAccountClasses,
                'financeAccountsStatus' => $statusList,
            )
        );

        return $fiananceAccountsForm;
    }
}
