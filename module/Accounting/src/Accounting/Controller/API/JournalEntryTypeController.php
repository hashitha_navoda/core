<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\JournalEntryTypeForm;
use Accounting\Form\JournalEntryTypeApproversForm;
use Accounting\Model\JournalEntryType;
use Accounting\Model\JournalEntryTypeApprovers;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Journal Entry type API related actions
 */
class JournalEntryTypeController extends CoreController
{

    /**
     * Create journal entry type
     * @return JsonModel
     */
    public function saveJournalEntryTypeAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $journalEntryTypeName = $postData['journalEntryTypeName'];

        $journalEntryTypes = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->getJournalEntryTypeByJournalEntryTypeName($journalEntryTypeName);
        
        if(count($journalEntryTypes) > 0){
        	$this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_NAME_EXIST');
            return $this->JSONRespond();
        }

         $journalEntryTypeForm = new JournalEntryTypeForm( 'CreateJournalEntryType');
        
        //set journel entry data and inputfilers for vallidation
        $journalEntryType = new JournalEntryType();
        $journalEntryTypeForm->setInputFilter($journalEntryType->getInputFilter());
        $journalEntryTypeForm->setData($postData);

        if (!$journalEntryTypeForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_CREATE');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $postData['entityID'] = $this->createEntity();
        $journalEntryType->exchangeArray($postData);
        $journalEntryTypeID = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->saveJournalEntryType($journalEntryType);
        
        if (!$journalEntryTypeID) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_CREATE');
            $this->rollback();
            return $this->JSONRespond();
        } 

        
        //save journal entry type approvers data
        if($postData['journalEntryTypeApprovedStatus'] == 1){
        	$journalEntryTypeApprovers = $postData['journalEntryTypeApprovers'];
        	$status = $this->saveJournalEntryTypeApprovers($journalEntryTypeApprovers,$journalEntryTypeID);
        	if(!$status){
        		$this->status = false;
        		$this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_CREATE');
        		$this->rollback();
        		return $this->JSONRespond();
        	}
        }
        $this->setLogMessage('Journal entry type '.$postData['journalEntryTypeName'].' is created.');
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_JOURNAL_ENTRY_TYPE_CREATE');
        $this->data = $journalEntryTypeID;
        $this->flashMessenger()->addMessage($this->msg);
        $this->commit();

        return $this->JSONRespond();
    }

    public function updateJournalEntryTypeAction()
    {
    	if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $journalEntryTypeName = $postData['journalEntryTypeName'];
        $journalEntryTypeID = $postData['journalEntryTypeID'];

        $journalEntryTypes = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->getJournalEntryTypeByJournalEntryTypeName($journalEntryTypeName)->current();
        $oldData = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->getJournalEntryTypeDataByJournalEntryTypeID($journalEntryTypeID);
        
        if(isset($journalEntryTypes['journalEntryTypeID'])){
        	if($journalEntryTypes['journalEntryTypeID'] != $journalEntryTypeID){
        		$this->status = false;
        		$this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_NAME_EXIST');
        		return $this->JSONRespond();
        	}
        }

         $journalEntryTypeForm = new JournalEntryTypeForm( 'CreateJournalEntryType');
        
        //set journel entry data and inputfilers for vallidation
        $journalEntryType = new JournalEntryType();
        $journalEntryTypeForm->setInputFilter($journalEntryType->getInputFilter());
        $journalEntryTypeForm->setData($postData);

        if (!$journalEntryTypeForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_UPDATE');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $journalEntryTypeData = array(
        	'journalEntryTypeName' => $journalEntryTypeName,
        	'journalEntryTypeApprovedStatus' => $postData['journalEntryTypeApprovedStatus'],
        );
        $journalEntryTypeUpdateStatus = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->updateJournalEntryType($journalEntryTypeData,$journalEntryTypeID);
        
        $journalEntryTypeApproversDeleteStatus = $this->CommonTable('Accounting\Model\JournalEntryTypeApproversTable')->deleteJournalEntryTypeApproversByJournalEntryTypeID($journalEntryTypeID);
        if (!$journalEntryTypeUpdateStatus || !$journalEntryTypeApproversDeleteStatus) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_UPDATE');
            $this->rollback();
            return $this->JSONRespond();
        } 

        //save journal entry type approvers data
        if($postData['journalEntryTypeApprovedStatus'] == 1){
        	$journalEntryTypeApprovers = $postData['journalEntryTypeApprovers'];
        	$status = $this->saveJournalEntryTypeApprovers($journalEntryTypeApprovers,$journalEntryTypeID);
        	if(!$status){
        		$this->status = false;
        		$this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_UPDATE');
        		$this->rollback();
        		return $this->JSONRespond();
        	}
        }

        $this->setLogMessage('Journal entry type '.$oldData['journalEntryTypeName'].' is updated to '.$journalEntryTypeName);
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_JOURNAL_ENTRY_TYPE_UPDATE');
        $this->data = $journalEntryTypeID;
        $this->flashMessenger()->addMessage($this->msg);
        $this->commit();

        return $this->JSONRespond();
    }

    private function saveJournalEntryTypeApprovers($journalEntryTypeApprovers,$journalEntryTypeID)
    {
    	foreach ($journalEntryTypeApprovers as $key => $value) {
    		$value['journalEntryTypeID'] = $journalEntryTypeID;
    		
    		$journalEntryTypeApproversForm = new JournalEntryTypeApproversForm( 'CreateJournalEntryTypeApprovers');
        	
        	//set journel entry type approvers data and inputfilers for vallidation
        	$journalEntryTypeApprovers = new JournalEntryTypeApprovers();
        	$journalEntryTypeApproversForm->setInputFilter($journalEntryTypeApprovers->getInputFilter());
        	$journalEntryTypeApproversForm->setData($value);
        	
        	if (!$journalEntryTypeApproversForm->isValid()) {
            	return false;
        	}

        	$journalEntryTypeApprovers->exchangeArray($value);
        	$journalEntryTypeApproversID = $this->CommonTable('Accounting\Model\JournalEntryTypeApproversTable')->saveJournalEntryTypeApprovers($journalEntryTypeApprovers);

        	if(!$journalEntryTypeApproversID){
        		return false;
        	}

    	}
    	return true;
    }

     public function searchJournalEntryTypeForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = array('list'=>'');
            return $this->JSONRespond();
        }
            
        $journalEntryTypeSearchKey = $searchrequest->getPost('searchKey');
        $journalEntryTypes = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->searchJournalEntryTypeForDropDown($journalEntryTypeSearchKey);

        $journalEntryTypeList = array();
        foreach ($journalEntryTypes as $journalEntryType) {
            $temp['value'] = $journalEntryType['journalEntryTypeID'];
            $temp['text'] = $journalEntryType['journalEntryTypeName'];
            $journalEntryTypeList[] = $temp;
        }

        $this->data = array('list' => $journalEntryTypeList);
        return $this->JSONRespond();
    }

    public function getJournalEntryTypeFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $journalEntryTypeID = $searchrequest->getPost('journalEntryTypeID');
        $journalEntryTypes = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->getJournalEntryTypeDataByJournalEntryTypeID($journalEntryTypeID);
        $data[$journalEntryTypes['journalEntryTypeID']] = (object)$journalEntryTypes; 
        
        $journalEntryType = new ViewModel(
            array(
               'journalEntryType' => $data,
            )
        );

        $journalEntryType->setTerminal(TRUE);
        $journalEntryType->setTemplate('accounting/journal-entry-type/dynamic-list');
        return $journalEntryType;
    }

    public function getJournalEntryTypeForEditAction()
    {
    	$searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $journalEntryTypeID = $searchrequest->getPost('journalEntryTypeID');
        $journalEntryTypes = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->getJournalEntryTypeDataByJournalEntryTypeID($journalEntryTypeID);
        
        $journalEntryTypeApprovers = $this->CommonTable('Accounting\Model\JournalEntryTypeApproversTable')->getJournalEntryTypeApproversByJournalEntryTypeID($journalEntryTypeID);
        
        $JETApproversArray = [];
        foreach ($journalEntryTypeApprovers as $key => $value) {
        	 $JETApproversArray[$value['journalEntryTypeApproversID']] = $value;
        }

        $this->status = true;
        $this->data = array('journalEntryType' => $journalEntryTypes, 'journalEntryTypeApprovers' => $JETApproversArray);
        
        return $this->JSONRespond();
    }

    public function deleteJournalEntryTypeAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
           return $this->JSONRespond();
        }
            
        $journalEntryTypeID = $searchrequest->getPost('journalEntryTypeID');

        $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryTypeID($journalEntryTypeID);

        if( count($journalEntries) > 0){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_DELETE_USED');
            return $this->JSONRespond();
        }

        $journalEntryType = $this->CommonTable('Accounting\Model\JournalEntryTypeTable')->getJournalEntryTypeDataByJournalEntryTypeID($journalEntryTypeID);

        $this->beginTransaction();

        $status = $this->updateDeleteInfoEntity($journalEntryType['entityID']);
        
        if ($status) {
            $this->commit();
            $this->setLogMessage('Journal entry type '.$journalEntryType['journalEntryTypeName'].' is deleted.');
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_JOURNAL_ENTRY_TYPE_DELETE');
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TYPE_DELETE');
            $this->rollback();
        }  
        return $this->JSONRespond();
    }

    
}
