<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\FiscalPeriodForm;
use Accounting\Model\FiscalPeriod;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Fiscal Period API related actions
 */
class FiscalPeriodController extends CoreController
{

    /**
     * Create Fiscal Period
     * @return JsonModel
     */
    public function saveFiscalPeriodAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $fiscalPeriodParent = $postData['fiscalPeriodParent'];
        $fiscalPeriodParent['fiscalPeriodStartDate'] = $this->convertDateToStandardFormat($fiscalPeriodParent['fiscalPeriodStartDate']);
        $fiscalPeriodParent['fiscalPeriodEndDate'] = $this->convertDateToStandardFormat($fiscalPeriodParent['fiscalPeriodEndDate']);
        $fiscalPeriodChildren = $postData['fiscalPeriodChildren'];

        $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();
        
        if(count($fiscalPeriod) > 0){
        	foreach ($fiscalPeriod as $key => $value) {
        		if( ($value['fiscalPeriodStartDate'] <= $fiscalPeriodParent['fiscalPeriodStartDate'] &&
        		 	$fiscalPeriodParent['fiscalPeriodStartDate'] <= $value['fiscalPeriodEndDate']) ||
        			($value['fiscalPeriodStartDate'] <= $fiscalPeriodParent['fiscalPeriodEndDate'] &&
        		 	$fiscalPeriodParent['fiscalPeriodEndDate'] <= $value['fiscalPeriodEndDate'])
        			){
        				$this->status = false;
            			$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_DATE_EXIT_IN_RNGE');
            			return $this->JSONRespond();
        		}
        	}
        }

        $fiscalPeriodForm = new FiscalPeriodForm( 'CreateFiscalPeriod');
        
        //set journel entry data and inputfilers for vallidation
        $fiscalPeriod = new FiscalPeriod();
        $fiscalPeriodForm->setInputFilter($fiscalPeriod->getInputFilter());
        $fiscalPeriodForm->setData($fiscalPeriodParent);
        if (!$fiscalPeriodForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_FISCAL_PERIOD_CREATE');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $fiscalPeriodParent['entityID'] = $this->createEntity();
        $fiscalPeriod->exchangeArray($fiscalPeriodParent);
        $fiscalPeriodParentID = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->saveFiscalPeriod($fiscalPeriod);
        
        if (!$fiscalPeriodParentID) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_FISCAL_PERIOD_CREATE');
            $this->rollback();
            return $this->JSONRespond();
        } 

        //save fiscalPeriod child data
        if(count($fiscalPeriodChildren) > 0){
        	foreach ($fiscalPeriodChildren as $key => $value) {
        		$fiscalPeriodChildForm = new FiscalPeriodForm( 'CreateFiscalPeriod');
        		$fiscalPeriodChild = new FiscalPeriod();
        		$fiscalPeriodChildForm->setInputFilter($fiscalPeriodChild->getInputFilter());
        		$fiscalPeriodChildForm->setData($value);

        		if (!$fiscalPeriodChildForm->isValid()) {
        			$this->status = false;
        			$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_CREATE');
        			$this->rollback();
        			return $this->JSONRespond();
        		}

        		$value['entityID'] = $this->createEntity();
        		$value['fiscalPeriodParentID'] = $fiscalPeriodParentID;
        		$value['fiscalPeriodStartDate'] = $this->convertDateToStandardFormat($value['fiscalPeriodStartDate']);
        		$value['fiscalPeriodEndDate'] = $this->convertDateToStandardFormat($value['fiscalPeriodEndDate']);
        		$fiscalPeriodChild->exchangeArray($value);
        		$fiscalPeriodChildID = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->saveFiscalPeriod($fiscalPeriodChild);

        		if (!$fiscalPeriodChildID) {
        			$this->status = false;
        			$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_CREATE');
        			$this->rollback();
        			return $this->JSONRespond();
        		} 
        	}
        }
        $this->setLogMessage('Fiscal period '.$this->convertDateToStandardFormat($fiscalPeriodParent['fiscalPeriodStartDate']).' - '.$this->convertDateToStandardFormat($fiscalPeriodParent['fiscalPeriodEndDate']).' is created.');
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_FISCAL_PERIOD_CREATE');
        $this->data = $journalEntryTypeID;
        $this->flashMessenger()->addMessage($this->msg);
        $this->commit();

        return $this->JSONRespond();
    }

    public function updateFiscalPeriodAction()
    {
    	if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $fiscalPeriodParent = $postData['fiscalPeriodParent'];
        $fiscalPeriodParent['fiscalPeriodStartDate'] = $this->convertDateToStandardFormat($fiscalPeriodParent['fiscalPeriodStartDate']);
        $fiscalPeriodParent['fiscalPeriodEndDate'] = $this->convertDateToStandardFormat($fiscalPeriodParent['fiscalPeriodEndDate']);
        $fiscalPeriodChildren = $postData['fiscalPeriodChildren'];
        $fiscalPeriodID = $fiscalPeriodParent['fiscalPeriodID'];
        $fiscalPeriodHasjournalEntries = $fiscalPeriodParent['fiscalPeriodHasjournalEntries'];

        $oldData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($fiscalPeriodParent['fiscalPeriodID']);

        $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();
        
        if(count($fiscalPeriod) > 0){
        	foreach ($fiscalPeriod as $key => $value) {
        		if( ($value['fiscalPeriodStartDate'] <= $fiscalPeriodParent['fiscalPeriodStartDate'] &&
        		 	$fiscalPeriodParent['fiscalPeriodStartDate'] <= $value['fiscalPeriodEndDate']) ||
        			($value['fiscalPeriodStartDate'] <= $fiscalPeriodParent['fiscalPeriodEndDate'] &&
        		 	$fiscalPeriodParent['fiscalPeriodEndDate'] <= $value['fiscalPeriodEndDate'])
        			){
        			if(!($value['fiscalPeriodID'] == $fiscalPeriodID || $value['fiscalPeriodParentID'] == $fiscalPeriodID )){
        				$this->status = false;
            			$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_DATE_EXIT_IN_RNGE');
            			return $this->JSONRespond();
        			}
        		}
        	}
        }

        $fiscalPeriodForm = new FiscalPeriodForm( 'CreateFiscalPeriod');
        
        //set journel entry data and inputfilers for vallidation
        $fiscalPeriod = new FiscalPeriod();
        $fiscalPeriodForm->setInputFilter($fiscalPeriod->getInputFilter());
        $fiscalPeriodForm->setData($fiscalPeriodParent);
        if (!$fiscalPeriodForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_FISCAL_PERIOD_UPDATE');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $data = array(
        	'fiscalPeriodStartDate' => $fiscalPeriodParent['fiscalPeriodStartDate'],
        	'fiscalPeriodEndDate' => $fiscalPeriodParent['fiscalPeriodEndDate'],
        	'fiscalPeriodStatusID' => $fiscalPeriodParent['fiscalPeriodStatusID'],
        );
        $status =  $this->CommonTable('Accounting\Model\FiscalPeriodTable')->updateFiscalPeriod($data,$fiscalPeriodID);
        
        if (!$status) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_FISCAL_PERIOD_UPDATE');
            $this->rollback();
            return $this->JSONRespond();
        } 

        if($fiscalPeriodHasjournalEntries != 1){
        	$fiscalPeriodChildData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodChildDataByFiscalPeriodParentID($fiscalPeriodID);
        	$deleteStatus = true;
        	foreach ($fiscalPeriodChildData as $key => $value) {
        		if(isset($value['entityID'])){
       				$deleteStatus = $this->updateDeleteInfoEntity($value['entityID']);
       				if (!$deleteStatus) {
            			$this->status = false;
            			$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_UPDATE');
            			$this->rollback();
            			return $this->JSONRespond();
        			} 
    			}
        	}
        }
        //update fiscalPeriod child data
        if(count($fiscalPeriodChildren) > 0){
        	foreach ($fiscalPeriodChildren as $key => $value) {
        		$fiscalPeriodChildForm = new FiscalPeriodForm( 'CreateFiscalPeriod');
        		$fiscalPeriodChild = new FiscalPeriod();
        		$fiscalPeriodChildForm->setInputFilter($fiscalPeriodChild->getInputFilter());
        		$fiscalPeriodChildForm->setData($value);

        		if (!$fiscalPeriodChildForm->isValid()) {
        			$this->status = false;
        			$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_UPDATE');
        			$this->rollback();
        			return $this->JSONRespond();
        		}

        		$fiscalPeriodChildID = $value['fiscalPeriodID'];
        		
        		if($fiscalPeriodHasjournalEntries == 1){
        			$data = array(
        				'fiscalPeriodStatusID' => $value['fiscalPeriodStatusID'] 
        			);
        			$updateStatus =  $this->CommonTable('Accounting\Model\FiscalPeriodTable')->updateFiscalPeriod($data,$fiscalPeriodChildID);
        
        			if (!$updateStatus) {
            			$this->status = false;
            			$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_UPDATE');
            			$this->rollback();
            			return $this->JSONRespond();
        			} 
        		}else{
        			$value['entityID'] = $this->createEntity();
        			$value['fiscalPeriodParentID'] = $fiscalPeriodID;
        			$value['fiscalPeriodStartDate'] = $this->convertDateToStandardFormat($value['fiscalPeriodStartDate']);
        			$value['fiscalPeriodEndDate'] = $this->convertDateToStandardFormat($value['fiscalPeriodEndDate']);
        			$fiscalPeriodChild->exchangeArray($value);
        			$fiscalPeriodChildID = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->saveFiscalPeriod($fiscalPeriodChild);

        			if (!$fiscalPeriodChildID) {
        				$this->status = false;
        				$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_UPDATE');
        				$this->rollback();
        				return $this->JSONRespond();
        			} 
        		}
        	}
        }
        $changeArray = $this->getFiscalPeriodChageDetails($oldData, $fiscalPeriodParent);
        //set log details
        $previousData = json_encode($changeArray['previousData']);
        $newData = json_encode($changeArray['newData']);

        $this->setLogDetailsArray($previousData,$newData);
        $this->setLogMessage('Fiscal period '.$this->convertDateToStandardFormat($oldData['fiscalPeriodStartDate']).' - '.$this->convertDateToStandardFormat($oldData['fiscalPeriodEndDate'])." is updated.");

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_FISCAL_PERIOD_UPDATE');
        $this->data = $fiscalPeriodParentID;
        $this->flashMessenger()->addMessage($this->msg);
        $this->commit();

        return $this->JSONRespond();

    }

    public function getFiscalPeriodChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Fiscal Period Start Date'] = $this->convertDateToStandardFormat($row['fiscalPeriodStartDate']);
        $previousData['Fiscal Period End Date'] = $this->convertDateToStandardFormat($row['fiscalPeriodEndDate']);
        $previousData['Fiscal Period status'] = ($row['fiscalPeriodStatusID'] == '14') ? 'Unlocked' : 'Locked';

        $newData = [];
        $newData['Fiscal Period Start Date'] = $this->convertDateToStandardFormat($data['fiscalPeriodStartDate']);
        $newData['Fiscal Period End Date'] = $this->convertDateToStandardFormat($data['fiscalPeriodEndDate']);
        $newData['Fiscal Period status'] = ($data['fiscalPeriodStatusID'] == '14') ? 'Unlocked' : 'Locked';
        return array('previousData' => $previousData, 'newData' => $newData);
    }

    public function getFiscalPeriodDataAction()
    {
    	if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }


        $postData = $this->getRequest()->getPost();
        $fiscalPeriodID = $postData['fiscalPeriodID'];

        $fiscalPeriodParentData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($fiscalPeriodID);

        $fiscalPeriodStartDate = $fiscalPeriodParentData['fiscalPeriodStartDate'];
        $fiscalPeriodEndDate = $fiscalPeriodParentData['fiscalPeriodEndDate'];

        $fiscalPeriodTransactions = $this->CommonTable('Accounting\Model\JournalEntryTable')->getFiscalPeriodJournalEntries($fiscalPeriodStartDate, $fiscalPeriodEndDate,null)->current();

        if (!$fiscalPeriodTransactions) {
            $fiscalPeriodHasTransactions = false;
        } else {
            $fiscalPeriodHasTransactions = true;
        }

        $fiscalPeriodLockedStatus = $this->CommonTable('Accounting\Model\JournalEntryTable')->getFiscalPeriodJournalEntries($fiscalPeriodStartDate, $fiscalPeriodEndDate,$checkYearEndClosed = true)->current();
        
        if ($fiscalPeriodLockedStatus || ($fiscalPeriodParentData['isCompleteStepOne'] == 1 || $fiscalPeriodParentData['isCompleteStepTwo'] == 1)) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_EDIT_FISCALPERIOD_RANGE_YEAR_END_CLOSED');
            $this->data = null;
            return $this->JSONRespond();
        }

        $fiscalPeriodChildDetails = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodChildDataByFiscalPeriodParentID($fiscalPeriodID);
    
    	$fiscalPeriodChildData = [];
    	foreach ($fiscalPeriodChildDetails as $key => $value) {
    		$fiscalPeriodChildData[$key] = $value;
    	}

    	if(!isset($fiscalPeriodParentData['fiscalPeriodID'])){
    		$this->status = false;
            $this->msg = $this->getMessage('ERR_WHILE_GET_THE_FISCAL_PERIOD_DATA');
            $this->data = null;
            return $this->JSONRespond();
    	}

        //budget data
        $budgetDetails = $this->CommonTable('Accounting\Model\BudgetTable')->getBudgetByFiscalPeriodID($fiscalPeriodID)->current();

    	$this->status = true;
        $this->data = array('fiscalPeriodParentData' => $fiscalPeriodParentData, 'fiscalPeriodChildData' => $fiscalPeriodChildData, 'fiscalPeriodHasTransactions' => $fiscalPeriodHasTransactions, 'budgetData' => $budgetDetails);
    	return $this->JSONRespond();
    }

    public function deleteFiscalPeriodAction()
    {
    	if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost();
        $fiscalPeriodID = $postData['fiscalPeriodID'];

        $fiscalPeriodParentData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($fiscalPeriodID);

        $fiscalPeriodStartDate = $fiscalPeriodParentData['fiscalPeriodStartDate'];
        $fiscalPeriodEndDate = $fiscalPeriodParentData['fiscalPeriodEndDate'];

        $fiscalPeriodTransactions = $this->CommonTable('Accounting\Model\JournalEntryTable')->getFiscalPeriodJournalEntries($fiscalPeriodStartDate, $fiscalPeriodEndDate)->current();

        if ($fiscalPeriodTransactions) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DELETE_FISCALPERIOD_RANGE');
            $this->data = null;
            return $this->JSONRespond();
    
        }
    
    	$this->beginTransaction();
        $deleteParentStatus = $this->updateDeleteInfoEntity($fiscalPeriodParentData['entityID']);
        
        if(!$deleteParentStatus){
        	$this->status = false;
        	$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_DELETE');
        	$this->rollback();
        	return $this->JSONRespond();
        }

        $fiscalPeriodChildData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodChildDataByFiscalPeriodParentID($fiscalPeriodID);
        foreach ($fiscalPeriodChildData as $key => $value) {
        	if(isset($value['entityID'])){
        		$deleteStatus = $this->updateDeleteInfoEntity($value['entityID']);
        		if (!$deleteStatus) {
        			$this->status = false;
        			$this->msg = $this->getMessage('ERR_FISCAL_PERIOD_DELETE');
        			$this->rollback();
        			return $this->JSONRespond();
        		} 
        	}
        }
        $this->setLogMessage('Fiscal period '.$this->convertDateToStandardFormat($fiscalPeriodParentData['fiscalPeriodStartDate']).' - '.$this->convertDateToStandardFormat($fiscalPeriodParentData['fiscalPeriodEndDate'])." is deletd.");
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_FISCAL_PERIOD_DELETE');
        $this->flashMessenger()->addMessage($this->msg);
        $this->commit();

        return $this->JSONRespond();
    }

    public function getFiscalPeriodByIdAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost();
        $fiscalPeriodID = $postData['fiscalPeriodID'];

        $fiscalPeriodParentData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($fiscalPeriodID);

        $this->status = true;
        $this->data = array('fiscalPeriodParentData' => $fiscalPeriodParentData);
        return $this->JSONRespond();
    }
}
