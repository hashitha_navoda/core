<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\FinanceAccountClassForm;
use Accounting\Model\FinanceAccountClass;
use Zend\Session\Container;

class AccountDashboardController extends CoreController
{
    protected $userID;
    protected $cdnUrl;
    protected $user_session;
    protected $paginator;
    protected $company;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function getWidgetDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'year';
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'months';
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'week';
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $subPeriod = 'day';
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $totalBankBalance = 0;
            $banks = $this->CommonTable('Expenses\Model\AccountTable')->fetchAll(true, array('account.accountId DESC'));
            foreach ($banks as $bank) {
                $totalBankBalance += floatval($bank['accountBalance']);
            }

            $cashFlow = $this->getCashFlow($fromDate, $toDate, $subPeriod);
            
            $expenses = $this->getExpense($fromDate, $toDate);

            $income = $this->getIncome($fromDate, $toDate);
            
            $chequeData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalRecivedChequesByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $recivedChequeTotal = 0;
            foreach ($chequeData as $value) {
                $exitsingCheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($value['incomingPaymentMethodChequeId']);
                if (is_null($exitsingCheque)) {
                    $recivedChequeTotal += floatval($value['incomingPaymentMethodAmount']);         
                }
            }

            $lastPeriodChequeData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalRecivedChequesByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $lastPeriodRecivedChequeTotal = 0;
            foreach ($lastPeriodChequeData as $value) {
                $exitsingCheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($value['incomingPaymentMethodChequeId']);
                if (is_null($exitsingCheque)) {
                    $lastPeriodRecivedChequeTotal += floatval($value['incomingPaymentMethodAmount']);         
                }
            }

            $recivedChequeProfitLoss = (($recivedChequeTotal - $lastPeriodRecivedChequeTotal) / $recivedChequeTotal) * 100;

            $settledInvoiceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId(null, 0, $fromDate, $toDate);
            $settledInvoiceChequesTotal = 0;
            foreach ($settledInvoiceCheques as $value) {
                $settledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }

            $settledAdvanceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId(null,0, $fromDate, $toDate);
            foreach ($settledAdvanceCheques as $value) {
                $settledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }


            $lastPeriodSettledInvoiceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId(null, 0, $lastPeriodFromDate, $lastPeriodToDate);
            $lastPeriodSettledInvoiceChequesTotal = 0;
            foreach ($lastPeriodSettledInvoiceCheques as $value) {
                $lastPeriodSettledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }

            $lastPeriodSettledAdvanceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId(null,0, $lastPeriodFromDate, $lastPeriodToDate);
            foreach ($lastPeriodSettledAdvanceCheques as $value) {
                $lastPeriodSettledInvoiceChequesTotal += floatval($value['outGoingPaymentMethodPaidAmount']);
            }

            $settledChequeProfitLoss = (($settledInvoiceChequesTotal - $lastPeriodSettledInvoiceChequesTotal) / $settledInvoiceChequesTotal) * 100;

            $PVDAData = $this->CommonTable('Expenses\Model\PettyCashVoucherDefaultAccountsTable')->fetchAll()->current();

            if($PVDAData){
                $financeAccountsArray = $this->getFinanceAccounts();
                $pettyCashFloatBalance = $financeAccountsArray[$PVDAData->pettyCashVoucherFinanceAccountID]['financeAccountsDebitAmount'] - $financeAccountsArray[$PVDAData->pettyCashVoucherFinanceAccountID]['financeAccountsCreditAmount'];
            }


            $chequeDetailsSummaryData = $this->getChequeDetailSummaryData($period, $toDate);

            header('Content-Type: application/json');
            $widgetData = array(
                'totalBankBalance' => $totalBankBalance,
                'recivedChequeTotal' => $recivedChequeTotal,
                'lastPeriodRecivedChequeTotal' => $lastPeriodRecivedChequeTotal,
                'settledInvoiceChequesTotal' => $settledInvoiceChequesTotal,
                'chequeDetailsSummaryData' => $chequeDetailsSummaryData,
                'lastPeriodSettledInvoiceChequesTotal' => $lastPeriodSettledInvoiceChequesTotal,
                'pettyCashFloatBalance' => $pettyCashFloatBalance,
                'recivedChequeProfitLoss' => round($recivedChequeProfitLoss,2),
                'settledChequeProfitLoss' => round($settledChequeProfitLoss,2),
                'cashFlow' => $cashFlow,
                'expenses' => $expenses,
                'income' => $income,
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function getChequeDetailSummaryData($period, $date)
    {
        $chequeDataByChequeDate = $this->CommonTable('Invoice\Model\PaymentsTable')->getAgedCustomerPayment(null,$date);

        $chequeDataByChequeDateData = [];
        foreach ($chequeDataByChequeDate as $value) {
            $chequeDataByChequeDateData[$value['incomingPaymentMethodChequeId']] = $value;
        }

        $chequeDataByPaymentDate = $this->CommonTable('Invoice\Model\PaymentsTable')->getAgedCustomerPayment($date,null);

        $chequeDataByPaymentDateData = [];
        foreach ($chequeDataByPaymentDate as $value) {
            $chequeDataByPaymentDateData[$value['incomingPaymentMethodChequeId']] = $value;
        }        

        $finalRecivedChequeData = [];
        foreach ($chequeDataByPaymentDateData as $key => $value) {
            if (!array_key_exists($key, $chequeDataByChequeDateData)) {
                $finalRecivedChequeData[$value['incomingPaymentMethodChequeId']] = $value;
                $finalRecivedChequeData[$value['incomingPaymentMethodChequeId']]['date'] = $value['incomingPaymentDate'];
            }
        }

        foreach ($chequeDataByChequeDateData as $value) {
            $finalRecivedChequeData[$value['incomingPaymentMethodChequeId']] = $value;
            $finalRecivedChequeData[$value['incomingPaymentMethodChequeId']]['date'] = $value['postdatedChequeDate'];
        }


        $settledInvoiceChequesByPaymentDate = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedAgedInvoiceCheques($date,null);
        $settledChequesByPaymentDateData = [];
        foreach ($settledInvoiceChequesByPaymentDate as $value) {
            $settledChequesByPaymentDateData[$value['outGoingPaymentMethodsNumbersID']]['amount'] = $value['outgoingInvoiceCashAmount'];
            $settledChequesByPaymentDateData[$value['outGoingPaymentMethodsNumbersID']]['date'] = $value['outgoingPaymentDate'];
        }

        $settledAdvanceChequesByPaymentDate = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedAgedAdvanceCheques($date,null);
        foreach ($settledAdvanceChequesByPaymentDate as $value) {
            $settledChequesByPaymentDateData[$value['outGoingPaymentMethodsNumbersID']]['amount'] = $value['outgoingPaymentAmount'];
            $settledChequesByPaymentDateData[$value['outGoingPaymentMethodsNumbersID']]['date'] = $value['outgoingPaymentDate'];
        }

        $settledInvoiceChequesByChequeDate = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedAgedInvoiceCheques(null, $date);
        $settledChequesByChequeDateData = [];
        foreach ($settledInvoiceChequesByChequeDate as $value) {
            $settledChequesByChequeDateData[$value['outGoingPaymentMethodsNumbersID']]['amount'] = $value['outgoingInvoiceCashAmount'];
            $settledChequesByChequeDateData[$value['outGoingPaymentMethodsNumbersID']]['date'] = $value['outgoingPaymentDate'];
        }

        $settledAdvanceChequesByChequeDate = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedAgedAdvanceCheques(null, $date);
        foreach ($settledAdvanceChequesByChequeDate as $value) {
            $settledChequesByChequeDateData[$value['outGoingPaymentMethodsNumbersID']]['amount'] = $value['outgoingPaymentAmount'];
            $settledChequesByChequeDateData[$value['outGoingPaymentMethodsNumbersID']]['date'] = $value['outgoingPaymentDate'];
        }

        $finalIssuedChequeData = [];
        foreach ($settledChequesByPaymentDateData as $key => $value) {
            if (!array_key_exists($key, $settledChequesByChequeDateData)) {
                $finalIssuedChequeData[$key] = $value;
            }
        }

        foreach ($settledChequesByChequeDateData as $key => $value) {
            $finalIssuedChequeData[$key] = $value;
        }

        $recivedChequeProccessedData = $this->getAgedChequeData($finalRecivedChequeData, $date, $period, $finalIssuedChequeData);

        return $recivedChequeProccessedData;
    }


    public function getAgedChequeData($chequeData, $date, $period, $issuedChequeData)
    {
        $agedChequeData =[];
        switch ($period) {
            case 'thisYear':
                $agedChequeData['0-30']['recivedCheque'] = 0;
                $agedChequeData['0-30']['issuedCheque'] = 0;
                $agedChequeData['30-90']['recivedCheque'] = 0;
                $agedChequeData['30-90']['issuedCheque'] = 0;
                $agedChequeData['90-180']['recivedCheque'] = 0;
                $agedChequeData['90-180']['issuedCheque'] = 0;
                $agedChequeData['180-270']['recivedCheque'] = 0;
                $agedChequeData['180-270']['issuedCheque'] = 0;
                $agedChequeData['270-360']['recivedCheque'] = 0;
                $agedChequeData['270-360']['issuedCheque'] = 0;
                $agedChequeData['360+']['recivedCheque'] = 0;
                $agedChequeData['360+']['issuedCheque'] = 0;
                foreach ($chequeData as $value) {
                    $now = strtotime($date);
                    $your_date = strtotime($value['date']);
                    $datediff = $your_date - $now;
                    $diff = round($datediff / (60 * 60 * 24));

                    if ($diff >= 0 && $diff < 30) {
                        $agedChequeData['0-30']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 30 && $diff < 90) {
                        $agedChequeData['30-90']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 90 && $diff < 180) {
                        $agedChequeData['90-180']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 180 && $diff < 270) {
                        $agedChequeData['180-270']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 270 && $diff < 360) {
                        $agedChequeData['270-360']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 360) {
                        $agedChequeData['360+']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    }
                }

                foreach ($issuedChequeData as $value) {
                    $now = strtotime($date);
                    $your_date = strtotime($value['date']);
                    $datediff = $your_date - $now;
                    $diff = round($datediff / (60 * 60 * 24));

                    if ($diff >= 0 && $diff < 30) {
                        $agedChequeData['0-30']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 30 && $diff < 90) {
                        $agedChequeData['30-90']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 90 && $diff < 180) {
                        $agedChequeData['90-180']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 180 && $diff < 270) {
                        $agedChequeData['180-270']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 270 && $diff < 360) {
                        $agedChequeData['270-360']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 360) {
                        $agedChequeData['360+']['issuedCheque'] += floatval($value['amount']);
                    }
                } 
                break;
            case 'thisMonth':
                $agedChequeData['0-5']['recivedCheque'] = 0;
                $agedChequeData['0-5']['issuedCheque'] = 0;
                $agedChequeData['5-10']['recivedCheque'] = 0;
                $agedChequeData['5-10']['issuedCheque'] = 0;
                $agedChequeData['10-15']['recivedCheque'] = 0;
                $agedChequeData['10-15']['issuedCheque'] = 0;
                $agedChequeData['15-20']['recivedCheque'] = 0;
                $agedChequeData['15-20']['issuedCheque'] = 0;
                $agedChequeData['20-25']['recivedCheque'] = 0;
                $agedChequeData['20-25']['issuedCheque'] = 0;
                $agedChequeData['25-30']['recivedCheque'] = 0;
                $agedChequeData['25-30']['issuedCheque'] = 0;
                foreach ($chequeData as $value) {
                    $now = strtotime($date);
                    $your_date = strtotime($value['date']);
                    $datediff = $your_date - $now;
                    $diff = round($datediff / (60 * 60 * 24));

                    if ($diff >= 0 && $diff < 5) {
                        $agedChequeData['0-5']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 5 && $diff < 10) {
                        $agedChequeData['5-10']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 10 && $diff < 15) {
                        $agedChequeData['10-15']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 15 && $diff < 20) {
                        $agedChequeData['15-20']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 20 && $diff < 25) {
                        $agedChequeData['20-25']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 25 && $diff <= 30) {
                        $agedChequeData['25-30']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    }
                }
                
                foreach ($issuedChequeData as $value) {
                    $now = strtotime($date);
                    $your_date = strtotime($value['date']);
                    $datediff = $your_date - $now;
                    $diff = round($datediff / (60 * 60 * 24));

                    if ($diff >= 0 && $diff < 5) {
                        $agedChequeData['0-5']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 5 && $diff < 10) {
                        $agedChequeData['5-10']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 10 && $diff < 15) {
                        $agedChequeData['10-15']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 15 && $diff < 20) {
                        $agedChequeData['15-20']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 20 && $diff < 25) {
                        $agedChequeData['20-25']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 25 && $diff <= 30) {
                        $agedChequeData['25-30']['issuedCheque'] += floatval($value['amount']);
                    }
                }

                break;
            case 'thisWeek':
                $agedChequeData['0-2']['recivedCheque'] = 0;
                $agedChequeData['0-2']['issuedCheque'] = 0;
                $agedChequeData['2-4']['recivedCheque'] = 0;
                $agedChequeData['2-4']['issuedCheque'] = 0;
                $agedChequeData['4-6']['recivedCheque'] = 0;
                $agedChequeData['4-6']['issuedCheque'] = 0;
                $agedChequeData['6-8']['recivedCheque'] = 0;
                $agedChequeData['6-8']['issuedCheque'] = 0;
                foreach ($chequeData as $value) {
                    $now = strtotime($date);
                    $your_date = strtotime($value['date']);
                    $datediff = $your_date - $now;
                    $diff = round($datediff / (60 * 60 * 24));

                    if ($diff >= 0 && $diff < 2) {
                        $agedChequeData['0-2']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 2 && $diff < 4) {
                        $agedChequeData['2-4']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 4 && $diff < 6) {
                        $agedChequeData['4-6']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff >= 6 && $diff <= 8) {
                        $agedChequeData['6-8']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } 
                }
                foreach ($issuedChequeData as $value) {
                    $now = strtotime($date);
                    $your_date = strtotime($value['date']);
                    $datediff = $your_date - $now;
                    $diff = round($datediff / (60 * 60 * 24));

                    if ($diff >= 0 && $diff < 2) {
                        $agedChequeData['0-2']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 2 && $diff < 4) {
                        $agedChequeData['2-4']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 4 && $diff < 6) {
                        $agedChequeData['4-6']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff >= 6 && $diff <= 8) {
                        $agedChequeData['6-8']['issuedCheque'] += floatval($value['amount']);
                    } 
                }
                
                break;
            case 'thisDay':
                $agedChequeData['0']['recivedCheque'] = 0;
                $agedChequeData['0']['issuedCheque'] = 0;
                $agedChequeData['1']['recivedCheque'] = 0;
                $agedChequeData['1']['issuedCheque'] = 0;
                $agedChequeData['2']['recivedCheque'] = 0;
                $agedChequeData['2']['issuedCheque'] = 0;
                foreach ($chequeData as $value) {
                    $now = strtotime($date);
                    $your_date = strtotime($value['date']);
                    $datediff = $your_date - $now;
                    $diff = round($datediff / (60 * 60 * 24));

                    if ($diff == 0) {
                        $agedChequeData['0']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff == 1) {
                        $agedChequeData['1']['recivedCheque']+= floatval($value['incomingPaymentMethodAmount']);
                    } else if ($diff == 2) {
                        $agedChequeData['2']['recivedCheque'] += floatval($value['incomingPaymentMethodAmount']);
                    }
                }
                foreach ($issuedChequeData as $value) {
                    $now = strtotime($date);
                    $your_date = strtotime($value['date']);
                    $datediff = $your_date - $now;
                    $diff = round($datediff / (60 * 60 * 24));

                    if ($diff == 0) {
                        $agedChequeData['0']['issuedCheque'] += floatval($value['amount']);
                    } else if ($diff == 1) {
                        $agedChequeData['1']['issuedCheque']+= floatval($value['amount']);
                    } else if ($diff == 2) {
                        $agedChequeData['2']['issuedCheque'] += floatval($value['amount']);
                    }
                }
                
                break;
            
            default:
                // code...
                break;
        }

        return $agedChequeData;

    }



    public function getAdvanceAgedCustomerDetailsForDonePayments($cusIds = null, $isAllCustomers = false, $cusCategory = null)
    {
        $customerDetails = array();
        $agedCustomerDeatails = $this->CommonTable('Invoice\Model\PaymentsTable')->getAgedCusForDonePaymentAdvanceData($cusIds, $endDate, $isAllCustomers, $cusCategory);
        
        foreach ($agedCustomerDeatails as $c) {
            if ($c['WithIn30Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                $c['WithIn30Days'] += $c['incomingPaymentCreditAmount'];
            } else if ($c['WithIn60Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                $c['WithIn60Days'] += $c['incomingPaymentCreditAmount'];
            } else if ($c['WithIn90Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                $c['WithIn90Days'] += $c['incomingPaymentCreditAmount'];
            } else if ($c['WithIn180Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                $c['WithIn180Days'] += $c['incomingPaymentCreditAmount'];
            } else if ($c['WithIn270Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                $c['WithIn270Days'] += $c['incomingPaymentCreditAmount'];
            } else if ($c['WithIn365Days'] != null && $c['incomingPaymentCreditAmount'] != null) {
                $c['WithIn365Days'] += $c['incomingPaymentCreditAmount'];
            } else if ($c['Over365'] != null && $c['incomingPaymentCreditAmount'] != null) {
                $c['Over365'] += $c['incomingPaymentCreditAmount'];
            }

            $withIn30Days = is_null($c['WithIn30Days']) ? 0 : $c['WithIn30Days'];
            $withIn60Days = is_null($c['WithIn60Days']) ? 0 : $c['WithIn60Days'];
            $withIn90Days = is_null($c['WithIn90Days']) ? 0 : $c['WithIn90Days'];
            $withIn180Days = is_null($c['WithIn180Days']) ? 0 : $c['WithIn180Days'];
            $withIn270Days = is_null($c['WithIn270Days']) ? 0 : $c['WithIn270Days'];
            $withIn365Days = is_null($c['WithIn365Days']) ? 0 : $c['WithIn365Days'];
            $over365 = is_null($c['Over365']) ? 0 : $c['Over365'];

            //if first time of customerID coming from $customerDetails array
            if (empty($customerDetails[$c['customerID']])) {
                $withIn30DaysTotal = 0;
                $withIn60DaysTotal = 0;
                $withIn90DaysTotal = 0;
                $withIn180DaysTotal = 0;
                $withIn270DaysTotal = 0;
                $withIn365DaysTotal = 0;
                $over365Total = 0;
            } else {
                //if already have customerID related data on $customerDetails array
                //get previous value from $customerDetails array to calculations
                $withIn30DaysTotal = $customerDetails[$c['customerID']]['withIn30Days'];
                $withIn60DaysTotal = $customerDetails[$c['customerID']]['withIn60Days'];
                $withIn90DaysTotal = $customerDetails[$c['customerID']]['withIn90Days'];
                $withIn180DaysTotal = $customerDetails[$c['customerID']]['withIn180Days'];
                $withIn270DaysTotal = $customerDetails[$c['customerID']]['withIn270Days'];
                $withIn365DaysTotal = $customerDetails[$c['customerID']]['withIn365Days'];
                $over365Total = $customerDetails[$c['customerID']]['over365'];
            }
                

            $withIn30DaysTotal += $withIn30Days;
            $withIn60DaysTotal += $withIn60Days;
            $withIn90DaysTotal += $withIn90Days;
            $withIn180DaysTotal += $withIn180Days;
            $withIn270DaysTotal += $withIn270Days;
            $withIn365DaysTotal += $withIn365Days;
            $over365Total += $over365;

            $customerDetails[$c['customerID']]['cT'] = $c['customerTitle'];
            $customerDetails[$c['customerID']]['cN'] = $c['customerName'];
            $customerDetails[$c['customerID']]['cusCD'] = $c['customerCode'];
            $customerDetails[$c['customerID']]['withIn30Days'] = $withIn30DaysTotal;
            $customerDetails[$c['customerID']]['withIn60Days'] = $withIn60DaysTotal;
            $customerDetails[$c['customerID']]['withIn90Days'] = $withIn90DaysTotal;
            $customerDetails[$c['customerID']]['withIn180Days'] = $withIn180DaysTotal;
            $customerDetails[$c['customerID']]['withIn270Days'] = $withIn270DaysTotal;
            $customerDetails[$c['customerID']]['withIn365Days'] = $withIn365DaysTotal;
            $customerDetails[$c['customerID']]['over365'] = $over365Total;
            $customerDetails[$c['customerID']]['customerCategoryName'] = $c['customerCategoryName'];
                
        }
    }

    public function getExpense($fromDate, $toDate)
    {
        $expenseRes = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountTypeIDAndDateRangeForDashbaord([3],$fromDate,$toDate);
  
        $expenseData = [];
        foreach ($expenseRes as $value) {
            $accountBalance = floatval($value['totalDebit']) - floatval($value['totalCredit']);

            if ($accountBalance > 0) {
                $expenseData[$value['financeAccountClassID']] = array('className' => $value['financeAccountClassName'],'accountValue' => $accountBalance);
            }
        }
        

        $revenuedata = array(
            'expenseData' => $expenseData,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        );

        return $revenuedata;
    }
 
    public function getIncome($fromDate, $toDate)
    {
        $incomeRes = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountTypeIDAndDateRangeForDashbaord([4,7],$fromDate,$toDate);

        $incomeData = [];
        foreach ($incomeRes as $value) {
            $accountBalance = floatval($value['totalCredit']) - floatval($value['totalDebit']);

            if ($accountBalance > 0) {
                $incomeData[$value['financeAccountClassID']] = array('className' => $value['financeAccountClassName'],'accountValue' => $accountBalance);
            }
        }
        
        $incdata = array(
            'incomeData' => $incomeData,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        );

        return $incdata;
    }


    public function getDetailExpenseAction() 
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $acClassName = $request->getPost('acClassName');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $expenseRes = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountClassNameAndDateRangeForDashbaord($acClassName,$fromDate,$toDate);

            $expenseData = [];
            foreach ($expenseRes as $value) {
                $accountBalance = floatval($value['totalDebit']) - floatval($value['totalCredit']);

                if ($accountBalance > 0) {
                    $expenseData[$value['financeAccountsID']] = array('accountName' => $value['financeAccountsCode']." - ".$value['financeAccountsName'],'accountValue' => $accountBalance);
                }
            }
            
            header('Content-Type: application/json');
            $revenuedata = array(
                'expenseData' => $expenseData,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            );
            echo json_encode($revenuedata);
            exit();
        } else {
            exit();
        }
    }

    public function getDetailIncomeAction() 
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $acClassName = $request->getPost('acClassName');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }
            
            $incomeRes = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountClassNameAndDateRangeForDashbaord($acClassName,$fromDate,$toDate);

            $incomeData = [];
            foreach ($incomeRes as $value) {
                $accountBalance = floatval($value['totalCredit']) - floatval($value['totalDebit']);

                if ($accountBalance > 0) {
                    $incomeData[$value['financeAccountsID']] = array('accountName' => $value['financeAccountsCode']." - ".$value['financeAccountsName'],'accountValue' => $accountBalance);
                }
            }
            
            header('Content-Type: application/json');
            $revenuedata = array(
                'incomeData' => $incomeData,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            );
            echo json_encode($revenuedata);
            exit();
        } else {
            exit();
        }
    }

    public function getCashFlow($backDate, $currentDate, $period)
    {
        $backTimePeriod = 14;
        if ($period == 'day') {
            $backTimePeriod = 2;
        } else if ($period == 'months') {

            $currentFiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getValidFiscalPeriod($currentDate);
            $backDate = date('Y-m-d', strtotime($currentFiscalPeriod['fiscalPeriodStartDate']));
            $bkDate = strtotime($currentFiscalPeriod['fiscalPeriodStartDate']);
            $crDate = strtotime($currentDate);
            $diff = $this->getMonthDiff($bkDate, $crDate);

            if ($diff != 0) {
                $backTimePeriod = $diff;
            } else {
                $backTimePeriod = 1;
            }
        } else if ($period == 'year') {
            $earliestFiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getEarliestFiscalPeriod();
            $backDate = date('Y-m-d', strtotime($earliestFiscalPeriod['fiscalPeriodStartDate']));
        } else if ($period == 'week') {
            $backTimePeriod = 14;
        }

        if ($period == 'week') {
            $period = 'day';
        }
        if ($period != 'months' &&  $period != 'year') {
            $backDate = date('Y-m-d', strtotime($currentDate . '-' . $backTimePeriod . $period));
        }


        $result = $this->CommonTable('Invoice\Model\PaymentsTable')->getCashInFlowData($backDate, $currentDate, $period);
        $cashInFlow = array();
        if ($period == 'day') {
            $tmpCurrentDate = $currentDate;
            for ($i = $backTimePeriod; $i >= 0; $i--) {
                $tmpBackDate = date('d', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                $tmpBackMonth = date('M', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                $dataFlag = FALSE;
                if ($result != NULL) {
                    foreach ($result as $res) {
                        if ($res['Date'] == $tmpBackDate) {
                            $cashInFlow[] = array('x' => date("M", strtotime($res['Month'])) . ' ' . $res['Date'], 'y' => number_format((float) $res['TotalCashFlow'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                    }
                }
                if ($dataFlag == FALSE) {
                    $cashInFlow[] = array('x' => $tmpBackMonth . ' ' . $tmpBackDate, 'y' => '0.00');
                }
            }
            $cashInFlowFinalData = [];
            foreach ($cashInFlow as $value) {
                $cashInFlowFinalData[$value['x']] += $value['y'];
            }
        } else if ($period == 'months') {
            $tmpCurrentDate = $currentDate;
            for ($i = $backTimePeriod; $i >= 0; $i--) {
                $tmpBackMonth = date('F', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                $tmpBackYear = date('Y', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                $dataFlag = FALSE;
                if ($result != NULL) {
                    foreach ($result as $res) {
                        if ($res['Month'] == $tmpBackMonth && $res['Year'] == $tmpBackYear) {
                            $cashInFlow[] = array('x' => date("M", strtotime($res['Month'])) . '-' . $res['Year'], 'y' => number_format((float) $res['TotalCashFlow'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                    }
                }
                if ($dataFlag == FALSE) {
                    $cashInFlow[] = array('x' => date("M", strtotime($tmpBackMonth)) . '-' . $tmpBackYear, 'y' => '0.00');
                }
            }
            $cashInFlowFinalData = [];
            foreach ($cashInFlow as $value) {
                $cashInFlowFinalData[$value['x']] += $value['y'];
            }
        } else if ($period == 'year') {
            $tmpCurrentDate = $currentDate;
            $allFinanceYears = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->fetchAllByAsc();
            foreach ($allFinanceYears as $key => $value) {
                $value = (object) $value;
                $tmpYear = date('Y', strtotime($value->fiscalPeriodStartDate));
                $dataFlag = FALSE;
                if ($result != NULL) {
                    foreach ($result as $res) {

                        $resDate = date('Y-m-d', strtotime($res['Date'].' '.$res['MonthWithYear']));
                        $startDate = date('Y-m-d', strtotime($value->fiscalPeriodStartDate));
                        $endDate =  date('Y-m-d', strtotime($value->fiscalPeriodEndDate));

                        if ($resDate >= $startDate && $resDate <= $endDate) {
                            $cashInFlow[] = array('x' => $tmpYear, 'y' => number_format((float) $res['TotalCashFlow'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                        if ($dataFlag == FALSE) {
                            $cashInFlow[] = array('x' => $tmpYear, 'y' => '0.00');
                        }
                    }
                }
            }

            $cashInFlowFinalData = [];
            foreach ($cashInFlow as $value) {
                $cashInFlowFinalData[$value['x']] += $value['y'];
            }
        }

        $invoiceCashPayment = $this->CommonTable('SupplierPaymentsTable')->getCashOutFlowData($backDate, $currentDate, $period);
        $advanceCashPayment = $this->CommonTable('SupplierPaymentsTable')->getAdvancedPaymentData($backDate, $currentDate, $period, $paymentType = 1);
        $pettyCashFloats = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getActivePettyCashFloats($backDate, $currentDate, $period);

        $invoiceCashPayment = $invoiceCashPayment == null ? array() : $invoiceCashPayment;
        $advanceCashPayment = $advanceCashPayment == null ? array() : $advanceCashPayment;
        $pettyCashFloats = $pettyCashFloats == null ? array() : $pettyCashFloats;

        $mergeResult = array_merge($invoiceCashPayment, $advanceCashPayment, $pettyCashFloats);

        $cashOutFlow = array();
        $result = array();
        if ($period == 'day') {
            $result = $this->_sortCashOutFlowArray($period = 'Date', $mergeResult);
            $tmpCurrentDate = $currentDate;
            for ($i = $backTimePeriod; $i >= 0; $i--) {
                $tmpBackDate = date('d', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                $tmpBackMonth = date('M', strtotime($tmpCurrentDate . '-' . $i . 'day'));
                $dataFlag = FALSE;
                foreach ($result as $res) {
                    if ($res['Date'] == $tmpBackDate) {
                        $cashOutFlow[] = array('x' => date("M", strtotime($res['Month'])) . ' ' . $res['Date'], 'y' => number_format((float) $res['TotalCashOutFlow'], 2, '.', ''));
                        $dataFlag = TRUE;
                    }
                }
                if ($dataFlag == FALSE) {
                    $cashOutFlow[] = array('x' => $tmpBackMonth . ' ' . $tmpBackDate, 'y' => '0.00');
                }
            }
            $cashOutFlowFinalData = [];
            foreach ($cashOutFlow as $value) {
                $cashOutFlowFinalData[$value['x']] += $value['y'];
            }
        } else if ($period == 'months') {
            $result = $this->_sortCashOutFlowArray($period = 'Month', $mergeResult);

            $tmpCurrentDate = $currentDate;
            for ($i = $backTimePeriod; $i >= 0; $i--) {
                $tmpBackMonth = date('F', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                $tmpBackYear = date('Y', strtotime($tmpCurrentDate . '-' . $i . 'month'));
                $dataFlag = FALSE;
                foreach ($result as $res) {
                    if ($res['Month'] == $tmpBackMonth && $res['Year'] == $tmpBackYear) {
                        $cashOutFlow[] = array('x' => date("M", strtotime($res['Month'])) . '-' . $res['Year'], 'y' => number_format((float) $res['TotalCashOutFlow'], 2, '.', ''));
                        $dataFlag = TRUE;
                    }
                }
                if ($dataFlag == FALSE) {
                    $cashOutFlow[] = array('x' => date("M", strtotime($tmpBackMonth)) . '-' . $tmpBackYear, 'y' => '0.00');
                }
            }
            $cashOutFlowFinalData = [];
            foreach ($cashOutFlow as $value) {
                $cashOutFlowFinalData[$value['x']] += $value['y'];
            }
        } else if ($period == 'year') {
            $result = $this->_sortCashOutFlowArray($period = 'Year', $mergeResult);

            $tmpCurrentDate = $currentDate;
            $allFinanceYears = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->fetchAllByAsc();
            foreach ($allFinanceYears as $key => $value) {
                $value = (object) $value;
                $tmpYear = date('Y', strtotime($value->fiscalPeriodStartDate));
                $dataFlag = FALSE;
                if ($result != NULL) {
                    foreach ($result as $res) {

                        $resDate = date('Y-m-d', strtotime($res['Date'].' '.$res['MonthWithYear']));
                        $startDate = date('Y-m-d', strtotime($value->fiscalPeriodStartDate));
                        $endDate =  date('Y-m-d', strtotime($value->fiscalPeriodEndDate));

                        if ($resDate >= $startDate && $resDate <= $endDate) {
                            $cashOutFlow[] = array('x' => $tmpYear, 'y' => number_format((float) $res['TotalCashOutFlow'], 2, '.', ''));
                            $dataFlag = TRUE;
                        }
                        if ($dataFlag == FALSE) {
                            $cashOutFlow[] = array('x' => $tmpYear, 'y' => '0.00');
                        }
                    }
                }
            }

            $cashOutFlowFinalData = [];
            foreach ($cashOutFlow as $value) {
                $cashOutFlowFinalData[$value['x']] += $value['y'];
            }
        }

        $incdata = array(
            'cashInFlow' => $cashInFlowFinalData,
            'cashOutFlow' => $cashOutFlowFinalData,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        );

        return $incdata;
    }

    private function _sortCashOutFlowArray($period, $mergeResult)
    {
        //sort $mergeResult array according to $period element
        $sortResult = array();
        foreach ($mergeResult as $key => $row) {
            $sortResult[$key] = $row[$period];
        }
        array_multisort($sortResult, SORT_ASC, $mergeResult);

        //If there have more data with same $period element
        //Then calculate total of TotalCashOutFlow elements which contain same $period element

        $result = array();
        $recordFlag = FALSE;
        $date = NULL;
        $cashFlowTotal = 0;
        $count = 0;
        foreach ($mergeResult as $key => $data) {
            if ($date == $data[$period]) {
                $cashFlowTotal += $data['TotalCashOutFlow'];
                $result[$count]['TotalCashOutFlow'] = $cashFlowTotal;
                $recordFlag = TRUE;
            }
            if ($recordFlag == FALSE) {
                $count++;
                $cashFlowTotal = $data['TotalCashOutFlow'];
                $result[$count] = $data;
            }
            $date = $data[$period];
            $recordFlag = FALSE;
        }

        return $result;
    }

    private function getMonthDiff($backDate, $currentDate) {
        $year1 = date('Y', $backDate);
        $year2 = date('Y', $currentDate);

        $month1 = date('m', $backDate);
        $month2 = date('m', $currentDate);

        $diff = (($year2 - $year1) * 12) + ($month2 - $month1);

        return $diff;
    }

    public function getHeaderWidgetDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'year';
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'months';
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'week';
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $subPeriod = 'day';
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $totalBankBalance = 0;
            $banks = $this->CommonTable('Expenses\Model\AccountTable')->getBankAccountBalanceForDashboard()->current();

            $totalBankBalance = floatval($banks['totalBalance']);
            
            $chequeData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalRecivedChequesByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $recivedChequeTotal = 0;
            foreach ($chequeData as $value) {
                $exitsingCheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($value['incomingPaymentMethodChequeId']);
                if (is_null($exitsingCheque)) {
                    $recivedChequeTotal += floatval($value['incomingPaymentMethodAmount']);         
                }
            }

           
            $settledInvoiceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getOutgoingCashAmountOfIssuedCheques(null, 0, $fromDate, $toDate)->current();

            $settledInvoiceChequesTotal = floatval($settledInvoiceCheques['totalOutgoingAmount']);

            $settledAdvanceCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getTotalOutgoingPaymentAmountInAdvancedPaymentIssuedCheques(null,0, $fromDate, $toDate)->current();

            $settledInvoiceChequesTotal = $settledInvoiceChequesTotal + floatval($settledAdvanceCheques['totalOutgoingAmount']);

            $PVDAData = $this->CommonTable('Expenses\Model\PettyCashVoucherDefaultAccountsTable')->fetchAll()->current();


            if($PVDAData){
                $financeAccountsArray = $this->getFinanceAccounts();
                $pettyCashFloatBalance = $financeAccountsArray[$PVDAData->pettyCashVoucherFinanceAccountID]['financeAccountsDebitAmount'] - $financeAccountsArray[$PVDAData->pettyCashVoucherFinanceAccountID]['financeAccountsCreditAmount'];
            }



            header('Content-Type: application/json');
            $widgetData = array(
                'totalBankBalance' => $totalBankBalance,
                'recivedChequeTotal' => $recivedChequeTotal,
                'settledInvoiceChequesTotal' => $settledInvoiceChequesTotal,
                'pettyCashFloatBalance' => $pettyCashFloatBalance,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function updateChartDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'year';
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'months';
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'week';
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $subPeriod = 'day';
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $cashFlow = $this->getCashFlow($fromDate, $toDate, $subPeriod);
            
            
            $chequeDetailsSummaryData = $this->getChequeDetailSummaryData($period, $toDate);

            header('Content-Type: application/json');
            $widgetData = array(
                'chequeDetailsSummaryData' => $chequeDetailsSummaryData,
                'cashFlow' => $cashFlow,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function updateFooterChartDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'year';
                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'months';
                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);
                    
                    $subPeriod = 'week';
                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $subPeriod = 'day';
                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }
            
            $expenses = $this->getExpense($fromDate, $toDate);

            $income = $this->getIncome($fromDate, $toDate);

            header('Content-Type: application/json');
            $widgetData = array(
                'expenses' => $expenses,
                'income' => $income,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }
}
