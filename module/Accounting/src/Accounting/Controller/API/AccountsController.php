<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\FinanceAccountsForm;
use Accounting\Model\FinanceAccounts;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Accounts API related actions
 */
class AccountsController extends CoreController
{

    /**
     * Create Account
     * @return JsonModel
     */
    public function saveAccountsAction()
    {
        if (!$this->getRequest()->isPost()) {
    	   $this->status = false;
    	   $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
    	   $this->data = null;
           return $this->JSONRespond();
        }

    	$postData = $this->getRequest()->getPost()->toArray();

        //this part use to find exists Accounts.
        $accountExists = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountsByAccountsCode($postData['financeAccountsCode']);
        if (count($accountExists) > 0) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_CODE_EXIST');
            return $this->JSONRespond();
        }
    		
        $fiananceAccountsForm = $this->getFinanceAccountForm();

    	$financeAccounts = new FinanceAccounts();
    	$fiananceAccountsForm->setInputFilter($financeAccounts->getInputFilter());
    	$fiananceAccountsForm->setData($postData);

        if (!$fiananceAccountsForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_CREATE');
            return $this->JSONRespond();
        }
    			
        $this->beginTransaction();
        $postData['entityID'] = $this->createEntity();
        $financeAccounts->exchangeArray($postData);
        $financeAccountsID = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->saveFinanceAccount($financeAccounts);

        if ($financeAccountsID) {
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_ACCONTS_CREATE');
            $this->data = $financeAccountClassID;
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_CREATE');
            $this->rollback();
        }

        return $this->JSONRespond();
    }

    /**
     * Update Account
     * @return JsonModel
     */
    public function updateAccountsAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();

        //this part use to find exists Accounts.
        $accountExists = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountsByAccountsCode($postData['financeAccountsCode'])->current();
        if( $accountExists != '' && $accountExists['financeAccountsID'] != $postData['financeAccountsID']){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_CODE_EXIST');
            return $this->JSONRespond();
        }

        if ($accountExists['financeAccountStatusID'] != $postData['financeAccountStatusID'] && $accountExists['financeAccountStatusID'] == "1") {
            $res = $this->validateGlAccountToUpdate($postData['financeAccountsID']);
            if (!$res['status']) {
                $this->status = false;
                $this->msg = $res['msg'];
                return $this->JSONRespond();       
            }
        }

        $fiananceAccountsForm = $this->getFinanceAccountForm();

        $financeAccounts = new FinanceAccounts();
        $fiananceAccountsForm->setInputFilter($financeAccounts->getInputFilter());
        $fiananceAccountsForm->setData($postData);
           
        if (!$fiananceAccountsForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_UPDATE');
            return $this->JSONRespond();
        }
            
        $this->beginTransaction();
        $updateData = array(
            'financeAccountsCode'=>$postData['financeAccountsCode'],
            'financeAccountsName'=>$postData['financeAccountsName'],
            'financeAccountsParentID'=>(!empty($postData['financeAccountsParentID'])) ? $postData['financeAccountsParentID'] : 0,
            'financeAccountClassID'=>$postData['financeAccountClassID'],
            'financeAccountStatusID'=>$postData['financeAccountStatusID'],
        );            
        $status = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->updateFinanceAccounts($updateData , $postData['financeAccountsID']);
        
        if ($status) {
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_ACCONTS_UPDATE');
            $this->data = $postData['financeAccountsID'];
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_UPDATE');
            $this->rollback();
        }

        return $this->JSONRespond();
    }

    /*
    * this is function is used to validate gl account while updating status
    * @param $glaccountID
    */
    public function validateGlAccountToUpdate($glAccountID)
    {
        $defaultAccounts = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->getDefaultAccountByAccountID($glAccountID);
         
        $paymentMethodDefaultAccounts = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethodByGlAccountID($glAccountID);

        $productDefaultAccounts = $this->CommonTable('Inventory\Model\ProductTable')->getProductByGlAccountID($glAccountID);
        
        $customerDefaultAccounts = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByGlAccountID($glAccountID);

        $supplierDefaultAccounts = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierByGlAccountID($glAccountID);

        if (count($defaultAccounts) > 0 && count($paymentMethodDefaultAccounts) == 0 && count($productDefaultAccounts) == 0 && count($customerDefaultAccounts) == 0 && count($supplierDefaultAccounts) == 0) {
            foreach ($defaultAccounts as $key => $value) {
                if ($value['glAccountSetupItemDefaultSalesAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_SALES_AC')];
                } else if ($value['glAccountSetupItemDefaultInventoryAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_INVENTORY_AC')];
                } else if ($value['glAccountSetupItemDefaultCOGSAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_COGS_AC')];
                } else if ($value['glAccountSetupItemDefaultAdjusmentAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_ADJUSTMENT_AC')];
                } else if ($value['glAccountSetupSalesAndCustomerReceivableAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_SALESANDCUSRECIVABLE_AC')];
                } else if ($value['glAccountSetupSalesAndCustomerSalesAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_SALESANDCUSSALES_AC')];
                } else if ($value['glAccountSetupSalesAndCustomerSalesDiscountAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_SALESANDCUSDISCOUNT_AC')];
                } else if ($value['glAccountSetupSalesAndCustomerAdvancePaymentAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_SALESANDCUSADPAY_AC')];
                } else if ($value['glAccountSetupPurchasingAndSupplierPayableAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_PURCANDSUPPAYABLE_AC')];
                } else if ($value['glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_PURCANDSUPDISCOUNT_AC')];
                } else if ($value['glAccountSetupPurchasingAndSupplierGrnClearingAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_PURCANDSUPGRNCLEARING_AC')];
                } else if ($value['glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_ITM_PURCANDSUPADVPAY_AC')];
                } else if ($value['glAccountSetupGeneralExchangeVarianceAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_GENAERALEXCHANGEVARIENCE_AC')];
                } else if ($value['glAccountSetupGeneralProfitAndLostYearAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_GENAERLPROFITANDLOSS_AC')];
                } else if ($value['glAccountSetupGeneralBankChargersAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_GENAERLBANKCHARGES_AC')];
                } else if ($value['glAccountSetupGeneralLoyaltyExpenseAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_GENAERLLOYALTYEXPENSE_AC')];
                } else if ($value['glAccountSetupGeneralDeliveryChargersAccountID'] == $glAccountID) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ACCONTS_GENAERLDELIVERYCHRGE_AC')];
                }             
            }
        } else if (count($defaultAccounts) == 0 && count($paymentMethodDefaultAccounts) > 0 && count($productDefaultAccounts) == 0 && count($customerDefaultAccounts) == 0 && count($supplierDefaultAccounts) == 0) {
            $paymentMethods = Array();
            foreach ($paymentMethodDefaultAccounts as $key => $value) {
                $paymentMethods[] = $value['paymentMethodName'];
            }
            $methods = implode(",",$paymentMethods);        
            $msg = $this->getMessage('ERR_AC_USED_PAY_METHOD', [$methods]);

            return ['status' => false, 'msg' => $msg];
        } else if (count($defaultAccounts) == 0 && count($paymentMethodDefaultAccounts) == 0 && count($productDefaultAccounts) > 0 && count($customerDefaultAccounts) == 0 && count($supplierDefaultAccounts) == 0) {
            if (count($productDefaultAccounts) < 5) {
                $products = Array();
                foreach ($productDefaultAccounts as $key => $value) {
                    $products[] = $value['productCode'].' - '.$value['productName'];
                }
                $pro = implode(",",$products);    
                $msg = $this->getMessage('ERR_AC_USED_PRODUCT', [$pro]);

                return ['status' => false, 'msg' => $msg];
            } else {
                return ['status' => false, 'msg' => $this->getMessage('ERR_AC_USED_PRODUCTS')];
            }
        } else if (count($defaultAccounts) == 0 && count($paymentMethodDefaultAccounts) == 0 && count($productDefaultAccounts) == 0 && count($customerDefaultAccounts) > 0 && count($supplierDefaultAccounts) == 0) {
            if (count($customerDefaultAccounts) < 5) {
                $customers = Array();
                foreach ($customerDefaultAccounts as $key => $value) {
                    $customers[] = $value['customerCode'].' - '.$value['customerName'];
                }
                $cus = implode(",",$customers);    
                $msg = $this->getMessage('ERR_AC_USED_CUSTOMER', [$cus]);

                return ['status' => false, 'msg' => $msg];
            } else {
                return ['status' => false, 'msg' => $this->getMessage('ERR_AC_USED_CUSTOMERS')];
            }
        } else if (count($defaultAccounts) == 0 && count($paymentMethodDefaultAccounts) == 0 && count($productDefaultAccounts) == 0 && count($customerDefaultAccounts) == 0 && count($supplierDefaultAccounts) > 0) {
            if (count($supplierDefaultAccounts) < 5) {
                $suppliers = Array();
                foreach ($supplierDefaultAccounts as $key => $value) {
                    $suppliers[] = $value['supplierCode'].' - '.$value['supplierName'];
                }
                $sup = implode(",",$suppliers);    
                $msg = $this->getMessage('ERR_AC_USED_SUPPLIER', [$sup]);

                return ['status' => false, 'msg' => $msg];
            } else {
                return ['status' => false, 'msg' => $this->getMessage('ERR_AC_USED_SUPPLIERS')];
            }
        } else {
            $msgArray = Array();
            if (count($defaultAccounts) > 0) {
                $msgArray[] = "default accounts";
            }

            if (count($paymentMethodDefaultAccounts) > 0) {
                $msgArray[] = "payment method default accounts";
            }

            if (count($productDefaultAccounts) > 0) {
                $msgArray[] = "product default accounts";
            }

            if (count($customerDefaultAccounts) > 0) {
                $msgArray[] = "customer default accounts";
            }

            if (count($supplierDefaultAccounts) > 0) {
                $msgArray[] = "supplier default accounts";
            }

            if (empty($msgArray)) {
                return ['status' => true];                
            } else {
                $errMsg = implode(",",$msgArray);
                return ['status' => false, 'msg' => $this->getMessage('ERR_AC_STATUS_UPDATE', [ucfirst($errMsg)])];
            }
        }
    }

    public function searchAccountsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data =array('list' => '');
            return $this->JSONRespond();
        }

        $accountsSearchKey = $searchrequest->getPost('searchKey');
        $activeAccountsFlag = ($searchrequest->getPost('addFlag') == 'true') ? true : false ;

        $accounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->searchAccountsForDropDown($accountsSearchKey,$activeAccountsFlag);
        $accountList = array();
        foreach ($accounts as $accts) {
            $temp['value'] = $accts['financeAccountsID'];
            if($activeAccountsFlag){
                $temp['text'] = $accts['financeAccountsCode'];
            }else{
                $temp['text'] = $accts['financeAccountsCode']."_".$accts['financeAccountsName'];
            }
            $accountList[] = $temp;
        }
       
        $this->data = array('list' => $accountList);
        return $this->JSONRespond();
    }

    public function searchActiveAccountsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data =array('list' => '');
            return $this->JSONRespond();
        }

        $accountsSearchKey = $searchrequest->getPost('searchKey');

        $accounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->searchAccountsForDropDown($accountsSearchKey,true);

        $accountList = array();
        foreach ($accounts as $accts) {
            $temp['value'] = $accts['financeAccountsID'];
            $temp['text'] = $accts['financeAccountsCode']."_".$accts['financeAccountsName'];
            $accountList[] = $temp;
        }

        $this->data = array('list' => $accountList);
        $this->setLogMessage("Retrive active GL accounts for dropdown.");
        return $this->JSONRespond();
    }

    public function getAccountsFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data ='';
            return $this->JSONRespond();
        }

        $accountsID = $searchrequest->getPost('accountsID');
        $accounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($accountsID);
        $data[$accounts['financeAccountsID']] = (object)$accounts; 
        $FAccounts = new ViewModel(
            array(
                'financeAccounts' => $data,
                'statuses' => $this->getStatusesList()
            )
        );

        $FAccounts->setTerminal(TRUE);
        $FAccounts->setTemplate('accounting/accounts/dynamic-list');
        return $FAccounts;
    }

    public function deleteAccountAction()
    {
        $searchrequest = $this->getRequest();

        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_DELETE_REQUEST');
            return $this->JSONRespond();
        }
           
        $accountID = $searchrequest->getPost('financeAccountsID');

        $checkGlAccount = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->getDefaultAccountByAccountID($accountID);
        if(count($checkGlAccount) > 0){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DEFAULT_GL_ACCOUNT');
            return $this->JSONRespond();
        }


        $checkPaymentMethodGlAccount = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethodByGlAccountID($accountID);

        if(count($checkPaymentMethodGlAccount) > 0){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_PAYMENT_METHOD_DEFAULT_GL_ACCOUNT');
            return $this->JSONRespond();
        }
        
        $checkParent = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsByParentAccountID($accountID);
        if(count($checkParent) > 0){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_IS_PARENT_OF_ANOTHER');
            return $this->JSONRespond();
        }

        $checkAccountIsUsed = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountID($accountID);
        if(count($checkAccountIsUsed) > 0){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_IS_USED');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $accounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($accountID);
        $status = $this->updateDeleteInfoEntity($accounts['entityID']);
            
        if ($status) {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_ACCONTS_DELETE');
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONTS_DELETE');
            $this->rollback();
        }

        return $this->JSONRespond();
    }

    private function getFinanceAccountForm()
    {
    	$status = $this->getStatusesList();
    	$statusList = array();
    	foreach ($status as $key => $value) {
    		if($value == 'active' || $value == 'inactive' || $value == 'locked'){
    			$statusList[$key] = ucwords($value);
    		}
    	}

    	$fAccountsClassData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClasses();
    	$fAccountClasses = array();
    	foreach ($fAccountsClassData as $value) {
    		$fAccountClasses[$value['financeAccountClassID']] = $value['financeAccountClassName'];           
    	}

    	$fAccountsData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAllActiveAccounts();
    	$fAccounts = array();
    	if(count($fAccountsData) > 0){
    		foreach ($fAccountsData as $value) {
    			$fAccounts[$value['financeAccountsID']] = $value['financeAccountsName'];           
    		}
    	}

    	$fiananceAccountsForm = new FinanceAccountsForm( 'CreateFinanceAccounts',
    		array(
    			'financeAccounts' => $fAccounts,
    			'financeAccountClass' => $fAccountClasses,
    			'financeAccountsStatus' => $statusList,
    		)
    	);

    	return $fiananceAccountsForm;
    }

    public function getAccountsClassRelatedAccountsAction()
    {
        $searchrequest = $this->getRequest();

        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $accountClassID = $searchrequest->getPost('accountClassID');

        $fAccountsData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAllHierarchicalByAccountClassID($accountClassID, true);
        $fAccounts = array();
        if(count($fAccountsData) > 0){
            foreach ($fAccountsData as $value) {
                $fAccounts[$value['financeAccountsID']] = $value['financeAccountsName'];           
            }
        }

        $this->status = true;
        $this->data = array('financeAccounts'=>$fAccounts);
        return $this->JSONRespond();
    }

    public function getFinanceAccountsByAccountsIDAction()
    {
        $searchrequest = $this->getRequest();

        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $financeAccountsID = $searchrequest->getPost('financeAccountsID');

        $fAccountsData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($financeAccountsID);

        $this->status = true;
        $this->data = array('financeAccounts'=>$fAccountsData);
        return $this->JSONRespond();
    }
}
