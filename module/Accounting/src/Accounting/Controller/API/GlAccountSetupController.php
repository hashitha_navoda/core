<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\GlAccountSetupForm;
use Accounting\Model\GlAccountSetup;
/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Gl Accounts Setup API related actions
 */
class GlAccountSetupController extends CoreController
{

    /**
     * save Gl account Setup
     * @return JsonModel
     */
    public function saveGlAccountSetupAction()
    {
        if (!$this->getRequest()->isPost()) {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
           $this->data = null;
           return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();


        $glAccountSetupForm = new GlAccountSetupForm('CreateGlAccountSetup');

        $glAccountSetup = new GlAccountSetup();
        $glAccountSetupForm->setInputFilter($glAccountSetup->getInputFilter());
        $glAccountSetupForm->setData($postData);

        if (!$glAccountSetupForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GL_ACCOUNT_SETUP_CREATE');
            return $this->JSONRespond();
        }
                
        $this->beginTransaction();
        $glAccountSetup->exchangeArray($postData);

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $oldData = $glAccountExist->current();

        if (count($glAccountExist) > 0) {
        	$glAccountSetupID = $oldData->glAccountSetupID;
        	$staus = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->updateGlAccountSetup($glAccountSetup,$glAccountSetupID);
        	$errMsg = "ERR_GL_ACCOUNT_SETUP_UPDATE";
        	$succMsg = "SUCC_GL_ACCOUNT_SETUP_UPDATE";
        }else{
        	$staus = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->saveGlAccountSetup($glAccountSetup);
        	$errMsg = "ERR_GL_ACCOUNT_SETUP_CREATE";
        	$succMsg = "SUCC_GL_ACCOUNT_SETUP_CREATE";
        }

        $customerAccountData = array(
            "customerReceviableAccountID" =>$glAccountSetup->glAccountSetupSalesAndCustomerReceivableAccountID,
            "customerSalesAccountID" =>$glAccountSetup->glAccountSetupSalesAndCustomerSalesAccountID,
            "customerSalesDiscountAccountID" =>$glAccountSetup->glAccountSetupSalesAndCustomerSalesDiscountAccountID,
            "customerAdvancePaymentAccountID" =>$glAccountSetup->glAccountSetupSalesAndCustomerAdvancePaymentAccountID,
        );
        $result = $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerAccountsByCustomerID($customerAccountData,0);

        if($result){
            if ($staus) {
                $changeArray = $this->getGlChageDetails($oldData, $postData);
                //set log details
                $previousData = json_encode($changeArray['previousData']);
                $newData = json_encode($changeArray['newData']);

                $this->setLogDetailsArray($previousData,$newData);
                $this->setLogMessage('Gl Account Setup is updated.');
                $this->status = true;
                $this->msg = $this->getMessage($succMsg);
                $this->flashMessenger()->addMessage($this->msg);
                $this->commit();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage($errMsg);
                $this->rollback();
            }
        }else{
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DEFAULTCUST_ACCOUTS_CREATE');
            $this->rollback();
        }

        return $this->JSONRespond();
    }

    public function getGlChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Sales Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupItemDefaultSalesAccountID)->current()['financeAccountsName'];
        $previousData['Inventory Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupItemDefaultInventoryAccountID)->current()['financeAccountsName'];
        $previousData['Cost of Goods Sold Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupItemDefaultCOGSAccountID)->current()['financeAccountsName'];
        $previousData['Inventory Adjusment Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupItemDefaultAdjusmentAccountID)->current()['financeAccountsName'];
        $previousData['Customer Receivable Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupSalesAndCustomerReceivableAccountID)->current()['financeAccountsName'];
        $previousData['Customer Sales Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupSalesAndCustomerSalesAccountID)->current()['financeAccountsName'];
        $previousData['Customer Sales Discount Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupSalesAndCustomerSalesDiscountAccountID)->current()['financeAccountsName'];
        $previousData['Customer Advance/ Over Payment Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupSalesAndCustomerAdvancePaymentAccountID)->current()['financeAccountsName'];
        $previousData['Purchasing and Supplier Payable Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupPurchasingAndSupplierPayableAccountID)->current()['financeAccountsName'];
        $previousData['Purchasing and Supplier Purchase Discount Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID)->current()['financeAccountsName'];
        $previousData['Purchasing and Supplier GRN Clearing Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupPurchasingAndSupplierGrnClearingAccountID)->current()['financeAccountsName'];
        $previousData['Purchasing and Supplier Advance Payment Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID)->current()['financeAccountsName'];
        $previousData['Exchange Variance Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupGeneralExchangeVarianceAccountID)->current()['financeAccountsName'];
        $previousData['Bank Chargers Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupGeneralBankChargersAccountID)->current()['financeAccountsName'];
        $previousData['Delivery Chargers Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupGeneralDeliveryChargersAccountID)->current()['financeAccountsName'];
        $previousData['Loyalty Expense Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($row->glAccountSetupGeneralLoyaltyExpenseAccountID)->current()['financeAccountsName'];

        $newData = [];
        $newData['Sales Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupItemDefaultSalesAccountID'])->current()['financeAccountsName'];
        $newData['Inventory Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupItemDefaultInventoryAccountID'])->current()['financeAccountsName'];
        $newData['Cost of Goods Sold Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupItemDefaultCOGSAccountID'])->current()['financeAccountsName'];
        $newData['Inventory Adjusment Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupItemDefaultAdjusmentAccountID'])->current()['financeAccountsName'];
        $newData['Customer Receivable Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupSalesAndCustomerReceivableAccountID'])->current()['financeAccountsName'];
        $newData['Customer Sales Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupSalesAndCustomerSalesAccountID'])->current()['financeAccountsName'];
        $newData['Customer Sales Discount Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupSalesAndCustomerSalesDiscountAccountID'])->current()['financeAccountsName'];
        $newData['Customer Advance/ Over Payment Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupSalesAndCustomerAdvancePaymentAccountID'])->current()['financeAccountsName'];
        $newData['Purchasing and Supplier Payable Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupPurchasingAndSupplierPayableAccountID'])->current()['financeAccountsName'];
        $newData['Purchasing and Supplier Purchase Discount Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID'])->current()['financeAccountsName'];
        $newData['Purchasing and Supplier GRN Clearing Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupPurchasingAndSupplierGrnClearingAccountID'])->current()['financeAccountsName'];
        $newData['Purchasing and Supplier Advance Payment Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID'])->current()['financeAccountsName'];
        $newData['Exchange Variance Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupGeneralExchangeVarianceAccountID'])->current()['financeAccountsName'];
        $newData['Bank Chargers Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupGeneralBankChargersAccountID'])->current()['financeAccountsName'];
        $newData['Delivery Chargers Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupGeneralDeliveryChargersAccountID'])->current()['financeAccountsName'];
        $newData['Loyalty Expense Account'] = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($data['glAccountSetupGeneralLoyaltyExpenseAccountID'])->current()['financeAccountsName'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }

    public function updatePaymentMethodSetupAction()
    {
        if (!$this->getRequest()->isPost()) {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
           $this->data = null;
           return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $this->beginTransaction();

        foreach ($postData['methodData'] as $key => $value) {
            
            $salesCheck = (filter_var($value['salesCheck'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) == true)?1:0;
            $purchaseCheck = (filter_var($value['purchaseCheck'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE) == true)?1:0;
            
            $data = array(
                'paymentMethodSalesFinanceAccountID'=> (!empty($value['salesAccountID'])) ? $value['salesAccountID'] : null,
                'paymentMethodPurchaseFinanceAccountID'=> (!empty($value['purchaseAccountID'])) ? $value['purchaseAccountID'] : null,
                'paymentMethodInSales'=> $salesCheck,
                'paymentMethodInPurchase'=> $purchaseCheck
            );

            $updateResult = $this->CommonTable('Core\Model\PaymentMethodTable')->updatePaymentMethods($data,$value['paymentMethodID']);
            if(!$updateResult){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_GL_PAYMENT_METHOD_SETUP_UPDATE');
                return $this->JSONRespond();
            }
        }
        $this->setLogMessage('Payment method setup is updated.');
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_GL_PAYMENT_METHOD_SETUP_UPDATE');
        return $this->JSONRespond();
    }
}
