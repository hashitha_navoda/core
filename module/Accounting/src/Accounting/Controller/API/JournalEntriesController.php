<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\JournalEntryForm;
use Accounting\Form\JournalEntryAccountsForm;
use Accounting\Model\JournalEntry;
use Accounting\Model\JournalEntryAccounts;
use Accounting\Model\JournalEntryApprovers;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Journal Entry API related actions
 */
class JournalEntriesController extends CoreController
{

    /**
     * Create Account
     * @return JsonModel
     */
    public function saveJournalEntryAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $ignoreBudgetLimit = false;

        $this->beginTransaction();

        $resultData = $this->saveJournalEntry($postData);
        $dimensionData = $postData['dimensionData'];

        if($resultData['status']){

            if (!empty($dimensionData)) {
                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$resultData['data']['journalEntryCode']], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $postData['journalEntryAccounts'], $postData['journalEntryDate']);

                if(!$saveRes['status']){
                    return array('status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']);
                }   
            }

            $this->commit();
            $this->flashMessenger()->addMessage($resultData['msg']);
            $this->data = $resultData['data'];
        }else{
              $this->rollback();
        }

        $this->data = $resultData['data'];
        $this->status = $resultData['status'];
        $this->msg = $resultData['msg'];
        return $this->JSONRespond();
    }

    public function searchJournalEntryForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = array('list'=>'');
            return $this->JSONRespond();
        }
            
        $journalEntrySearchKey = $searchrequest->getPost('searchKey');
        $journalEntryTempalteStatus = filter_var($searchrequest->getPost('addFlag'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->searchJournalEntryForDropDown($journalEntrySearchKey);

        $journalEntryList = array();
        foreach ($journalEntries as $journalEntry) {
            if($journalEntryTempalteStatus){
                if($journalEntry['journalEntryTemplateStatus'] == 1){
                    $temp['value'] = $journalEntry['journalEntryID'];
                    $temp['text'] = $journalEntry['journalEntryCode'];
                    $journalEntryList[] = $temp;
                }
            }else{
                if($journalEntry['journalEntryTemplateStatus'] != 1){
                    $temp['value'] = $journalEntry['journalEntryID'];
                    $temp['text'] = $journalEntry['journalEntryCode'];
                    $journalEntryList[] = $temp;
                }
            }
        }

        $this->data = array('list' => $journalEntryList);
        return $this->JSONRespond();
        
    }

    public function getJournalEntryFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $journalEntryID = $searchrequest->getPost('journalEntryID');
        $journalEntryTemplateStatus = filter_var($searchrequest->getPost('journalEntryTemplateStatus'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
        $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryID($journalEntryID);
        $data[$journalEntries['journalEntryID']] = (object)$journalEntries; 
        
        $journalEntry = new ViewModel(
            array(
                'journalEntry' => $data,
                'statuses' => $this->getStatusesList(),
            )
        );

        $journalEntry->setTerminal(TRUE);
        if($journalEntryTemplateStatus){
            $journalEntry->setTemplate('accounting/journal-entries/dynamic-template-list');
        }else{
            $journalEntry->setTemplate('accounting/journal-entries/dynamic-list');
        }
        return $journalEntry;
    }

    public function getJournalEntriesByDocumentTypeIDAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $documentTypeID = $searchrequest->getPost('documentTypeID');
        if ($documentTypeID == "24") {
            $documentTypeID = 6;
            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeID($documentTypeID,$posCreditNote = true,false,false);
        } else if ($documentTypeID == "25") {
            $documentTypeID = 1;
            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeID($documentTypeID,false,$posInvoice = true,false);
        } else if ($documentTypeID == "26") {
            $documentTypeID = 7;
            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeID($documentTypeID,false ,false ,$posPayment = true);
        } elseif ($documentTypeID == "33") {
            $dataSet = $this->CommonTable('Accounting\Model\JournalEntryTable')->fetchAll();
            $journalEntries = [];

            foreach ($dataSet as $key => $value) {
                if ($value['documentTypeID'] == NULL) {
                    $journalEntries[] = $value;
                }
            }
        } else {
            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeID($documentTypeID);
        }
        
        foreach ($journalEntries as $jkey => $jvalue) {
            $data[$jvalue['journalEntryID']] = (object)$jvalue; 
        }
        
        $journalEntry = new ViewModel(
            array(
                'journalEntry' => $data,
                'statuses' => $this->getStatusesList(),
            )
        );

        $journalEntry->setTerminal(TRUE);
        $journalEntry->setTemplate('accounting/journal-entries/dynamic-list');
        
        return $journalEntry;
    }

    public function getJournalEntriesByDateFiltersAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $fromDate = $this->convertDateToStandardFormat($searchrequest->getPost('fromDate'));
        $toDate = $this->convertDateToStandardFormat($searchrequest->getPost('toDate'));
        $documentTypeID = $searchrequest->getPost('documentTypeID');

        if ($documentTypeID == "24") {
            $documentTypeID = 6;
            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDateFilter($fromDate,$toDate,$documentTypeID,$posCreditNote = true,false,false);
        } else if ($documentTypeID == "25") {
            $documentTypeID = 1;
            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDateFilter($fromDate,$toDate,$documentTypeID,false,$posInvoice = true,false);
        } else if ($documentTypeID == "26") {
            $documentTypeID = 7;
            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDateFilter($fromDate,$toDate,$documentTypeID,false ,false ,$posPayment = true);
        } else {
            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDateFilter($fromDate,$toDate,$documentTypeID);
        }
        
        foreach ($journalEntries as $jkey => $jvalue) {
            $data[$jvalue['journalEntryID']] = (object)$jvalue; 
        }
        
        $journalEntry = new ViewModel(
            array(
                'journalEntry' => $data,
                'statuses' => $this->getStatusesList(),
            )
        );

        $journalEntry->setTerminal(TRUE);
        $journalEntry->setTemplate('accounting/journal-entries/dynamic-list');
        
        return $journalEntry;
    }

    public function deleteJournalEntryAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE_REQUEST');
           return $this->JSONRespond();
        }
            
        $journalEntryID = $searchrequest->getPost('journalEntryID');
        
        $this->beginTransaction();

        $result = $this->deleteJournalEntry($journalEntryID);

        if($result['status']){
            $this->commit();
            $this->flashMessenger()->addMessage(['status' => true, 'msg' => $result['msg']]);
        }else{
            $this->rollback();
        }
        
        $this->status = $result['status'];
        $this->msg = $result['msg'];
        return $this->JSONRespond();
    }

    public function getJournalEntryAction()
    {
    	$searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_VIEW_REQUEST');
           return $this->JSONRespond();
        }
            
        $journalEntryID = $searchrequest->getPost('journalEntryID');

        $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
        
         $JEntry = new ViewModel(
            array(
               'journalEntry' => $jeResult['JEDATA'],
            )
        );

        $JEntry->setTerminal(TRUE);
        $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $this->status = true;
        $this->html = $JEntry;

        return $this->JSONRespondHtml();
    }

    public function reverseJournalEntryAction()
    {
    	$searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_REVERSE_REQUEST');
           return $this->JSONRespond();
        }
            
        $JEntryID = $searchrequest->getPost('journalEntryID');
        $journalEntry = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryID($JEntryID);

        $reverseJournalEntryData = array(
        	'journalEntryCode' => $journalEntry['journalEntryCode'],
        	'journalEntryDate' => $this->convertDateToStandardFormat($this->getUserDateTime('','y-m-d')),
            'journalEntryTypeID' => $journalEntry['journalEntryTypeID'],
        	'journalEntryStatusID' => 12,
            'journalEntryIsReverse' => 0,
        	'journalEntryReverseRefID' => $JEntryID,
            'journalEntryComment' => $journalEntry['journalEntryComment'],
        	'journalEntryHashValue' => $journalEntry['journalEntryHashValue'],
        	'locationID' => $journalEntry['locationID'],
        	);
        $journalEntryStatusID = 12;
        $journalEntryForm = $this->getJournalEntryForm();
        //set journel entry Accounts data and inputfilers for vallidation
        $journalEntry = new JournalEntry();
        $journalEntryForm->setInputFilter($journalEntry->getInputFilter());
        $journalEntryForm->setData($reverseJournalEntryData);

        if (!$journalEntryForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_REVERSE');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $reverseJournalEntryData['entityID'] = $this->createEntity();
        $journalEntry->exchangeArray($reverseJournalEntryData);
        $journalEntryID = $this->CommonTable('Accounting\Model\JournalEntryTable')->saveJournalEntry($journalEntry);
        
        if (!$journalEntryID) {
        	$this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_REVERSE');
            $this->rollback();
            return $this->JSONRespond();
        } 

        $JEntryAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($JEntryID);
        
        $journalEntryAccountsData = [];
        foreach ($JEntryAccounts as $key => $value) {
        	$journalEntryAccountsData[] = array(
        		'financeAccountsID' => $value['financeAccountsID'],
        		'financeGroupsID' => $value['financeGroupsID'],
        		'journalEntryAccountsDebitAmount' => $value['journalEntryAccountsCreditAmount'],
        		'journalEntryAccountsCreditAmount' => $value['journalEntryAccountsDebitAmount'],
        		'journalEntryAccountsMemo' => $value['journalEntryAccountsMemo'],
        	);
        }

        //save journal entry accounts data
        $status = $this->saveJournalEntryAccounts($journalEntryAccountsData, $journalEntryID, $journalEntryStatusID);
        if(!$status){
        	$this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_REVERSE');
            $this->rollback();
            return $this->JSONRespond();
        }

        $jdata = array(
        	'journalEntryIsReverse' => 0,
        );

        $updateStatus = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntry($jdata,$JEntryID);
        
        if(!$updateStatus){
        	$this->status = false;
            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_UPDATE_REVERSE_STATUS');
            $this->rollback();
            return $this->JSONRespond();
        }	

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_JOURNAL_ENTRY_REVERSE');
        $this->data = $journalEntryID;
        $this->flashMessenger()->addMessage($this->msg);
        $this->commit();

        return $this->JSONRespond();
    }

     private function getJournalEntryForm()
    {
        $journalEntryForm = new JournalEntryForm('CreateJournalEntry');

        return $journalEntryForm;
    }

     public function approveAction()
    {
        $action = $_GET['action'];
        $journalEntryID = $_GET['journalEntryID'];
        $token = $_GET['token'];
        $actionType = $_GET['actionType'];
        $journalEntryTypeApproverID = $_GET['journalEntryTypeApproverID'];

        $JEntrys = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryID($journalEntryID);
        $journalEntryCode = $JEntrys['journalEntryCode'];
        $entityID = $JEntrys['entityID'];
        
        $this->beginTransaction();
        if ($JEntrys['journalEntryHashValue'] == $token) {
            
            $JEAData = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->getJournalEntryApprovers($journalEntryID, $journalEntryTypeApproverID, $actionType);
            if($actionType == 'Create'){
                $msg = $this->approveCreateJournalEntry($JEAData, $journalEntryID, $journalEntryCode, $actionType,$action);
            }else{
                $msg = $this->approveJournalEntryDelete($JEAData, $journalEntryID, $journalEntryCode, $actionType, $entityID, $action);
            }
            echo $msg;
            exit();
        } else {
            echo 'Token Mismatch';
            exit();
        }
    }

    private function approveCreateJournalEntry($JEAData, $journalEntryID, $journalEntryCode, $actionType, $action)
    {
        if(!isset($JEAData->journalEntryApproversID)){
            return $this->getMessage('ERR_JOURNAL_ENTRY_REJECT_BEFOR_APPROVE',array($journalEntryCode));
        }

        $journalEntryApproversID = $JEAData->journalEntryApproversID;
        if ($action == 'approve') {
            if($JEAData->journalEntryApproversStatus != 'Rejected'){

                $JEApproversdata = array(
                    'journalEntryApproversStatus' => 'Approved',
                    'journalEntryApproversExpire' => 1,
                );

                $updateStatus = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->updateJournalEntryApprovers($JEApproversdata,$journalEntryApproversID);

                $dataJEA = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->getJournalEntryApproversByJournalEntryIDAndType($journalEntryID, $actionType);
                $allApproved = true;
                foreach ($dataJEA as $key => $value) {
                    if($value['journalEntryApproversStatus'] == 'Pending' || $value['journalEntryApproversStatus'] == 'Rejected' ){
                        $allApproved = false;
                        break;
                    }
                }

                if($allApproved){
                    $updateData = array(
                        'journalEntryStatusID' => 12,
                    );

                    $updateStatusJE = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntry($updateData, $journalEntryID);
                    
                    $financeStatus = $this->upgrateFinanceAccountsAmounts($journalEntryID);
                    if(!$financeStatus){
                        $this->rollback();
                        return $this->getMessage('ERR_JOURNAL_ENTRY_APPROVE',array($journalEntryCode));
                    }
                }

                if($updateStatus){
                    $this->commit();
                    $msg = $this->getMessage('SUCC_JOURNAL_ENTRY_APPROVE',array($journalEntryCode));
                }else{
                    $this->rollback();
                    $msg = $this->getMessage('ERR_JOURNAL_ENTRY_APPROVE',array($journalEntryCode));
                }
            }else{
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_REJECT_BEFOR_APPROVE',array($journalEntryCode));
            }
            return $msg;
        } else {

            if($JEAData->journalEntryApproversStatus != 'Approved'){
                $JEApproversdata = array(
                    'journalEntryApproversStatus' => 'Rejected',
                    'journalEntryApproversExpire' => 1,
                    );

                $updateStatus = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->updateJournalEntryApprovers($JEApproversdata,$journalEntryApproversID);

                $updateData = array(
                    'journalEntryStatusID' => 13,
                );

                $updateStatusJE = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntry($updateData, $journalEntryID);

                if($updateStatus){
                    $this->commit();
                    $msg = $this->getMessage('SUCC_JOURNAL_ENTRY_REJECTED',array($journalEntryCode));
                }else{
                    $this->rollback();
                    $msg = $this->getMessage('ERR_JOURNAL_ENTRY_REJECTED',array($journalEntryCode));
                }
            }else{
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_APPROVED_BEFOR_REJECT',array($journalEntryCode));
            }
            return $msg;
        }
    }

    private function approveJournalEntryDelete($JEAData, $journalEntryID, $journalEntryCode, $actionType, $entityID, $action)
    {
        if(!isset($JEAData->journalEntryApproversID)){
            return $this->getMessage('ERR_JOURNAL_ENTRY_REJECT_BEFOR_DELETE_APPROVE',array($journalEntryCode));
        }

        $journalEntryApproversID = $JEAData->journalEntryApproversID;
        if ($action == 'approve') {
            if($JEAData->journalEntryApproversStatus != 'Rejected'){

                $JEApproversdata = array(
                    'journalEntryApproversStatus' => 'Approved',
                    'journalEntryApproversExpire' => 1,
                );

                $updateStatus = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->updateJournalEntryApprovers($JEApproversdata,$journalEntryApproversID);

                $dataJEA = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->getJournalEntryApproversByJournalEntryIDAndType($journalEntryID, $actionType);
                $allApproved = true;
                foreach ($dataJEA as $key => $value) {
                    if($value['journalEntryApproversStatus'] == 'Pending' || $value['journalEntryApproversStatus'] == 'Rejected' ){
                        $allApproved = false;
                        break;
                    }
                }

                if($allApproved){

                    $financeStatus = $this->downgrateFinanceAccountsAmounts($journalEntryID);
                        if(!$financeStatus){
                        $this->rollback();
                        return $this->getMessage('ERR_JOURNAL_ENTRY_DELETE_APPROVE',array($journalEntryCode));
                    }

                    $status = $this->updateDeleteInfoEntity($entityID);
                    $journalEntryReveresData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryByJournalEntryReverseRefID($journalEntryID);
                    if($status && count($journalEntryReveresData) > 0){
                        $JEReverseRefData = $journalEntryReveresData->current();
                        $JERJournalEntryID = $JEReverseRefData['journalEntryID'];
                        
                        $financeStatus = $this->downgrateFinanceAccountsAmounts($JERJournalEntryID);
                        if(!$financeStatus){
                            $this->rollback();
                            return $this->getMessage('ERR_JOURNAL_ENTRY_DELETE_APPROVE',array($journalEntryCode));
                        }
 
                        $status = $this->updateDeleteInfoEntity($JEReverseRefData['entityID']);
                    }
                }

                if($updateStatus){
                    $this->commit();
                    $msg = $this->getMessage('SUCC_JOURNAL_ENTRY_DELETE_APPROVE',array($journalEntryCode));
                }else{
                    $this->rollback();
                    $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE_APPROVE',array($journalEntryCode));
                }
            }else{
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_REJECT_BEFOR_DELETE_APPROVE',array($journalEntryCode));
            }
            return $msg;
        } else {

            if($JEAData->journalEntryApproversStatus != 'Approved'){
                $JEApproversdata = array(
                    'journalEntryApproversStatus' => 'Rejected',
                    'journalEntryApproversExpire' => 1,
                );

                $updateStatus = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->updateJournalEntryApprovers($JEApproversdata,$journalEntryApproversID);

                $dataJEA = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->getJournalEntryApproversByJournalEntryIDAndType($journalEntryID, $actionType);
                foreach ($dataJEA as $key => $value) {
                    $JEApproversdata1 = array(
                        'journalEntryApproversExpire' => 1,
                    );
                    $updateStatus = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->updateJournalEntryApprovers($JEApproversdata1, $value['journalEntryApproversID']);
                }

                if($updateStatus){
                    $this->commit();
                    $msg = $this->getMessage('SUCC_JOURNAL_ENTRY_DELETE_APPROVE_REJECTED',array($journalEntryCode));
                }else{
                    $this->rollback();
                    $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE_APPROVE_REJECTED',array($journalEntryCode));
                }
            }else{
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE_APPROVED_BEFOR_REJECT',array($journalEntryCode));
            }
            return $msg;
        }
    }

    private function upgrateFinanceAccountsAmounts($journalEntryID)
    {   
        $journalEntryAccountsData = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);
        foreach ($journalEntryAccountsData as $key => $JEAccounts) {
            $financeAccountsID = $JEAccounts['financeAccountsID'];

            $financeAccountsValues = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($financeAccountsID);
            $financeCreditAmount = $financeAccountsValues['financeAccountsCreditAmount'] + $JEAccounts['journalEntryAccountsCreditAmount'];
            $financeDebitAmount = $financeAccountsValues['financeAccountsDebitAmount'] + $JEAccounts['journalEntryAccountsDebitAmount'];

            $financeAccountsData = array(
                'financeAccountsCreditAmount' => $financeCreditAmount,
                'financeAccountsDebitAmount' => $financeDebitAmount,
                );
            $financeStaus = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->updateFinanceAccounts($financeAccountsData, $financeAccountsID);

            if(!$financeStaus){
                return false;
            }
        }
        return true;
    }

    public function getJournalEntryTemplateReferenceAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
           return $this->JSONRespond();
        }
            
        $this->status = true;
        $jeTemplate = $searchrequest->getPost('jeTemplate');
        if($jeTemplate){
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $refData = $this->getReferenceNoForLocation(31, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];

            if ($rid == '' || $rid == NULL) {
                $this->status = false;
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TEMPLATE_ADD_REF');
                } else {
                    $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_TEMPLATE_CHANGE_REF');
                }
            }else{
                $this->data = array('refNumber' => $rid);
            }

        }else{
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $refData = $this->getReferenceNoForLocation(30, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];

            if ($rid == '' || $rid == NULL) {
                $this->status = false;
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_ADD_REF');
                } else {
                    $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_CHANGE_REF');
                }
            }else{
                $this->data = array('refNumber' => $rid);
            }
        }
        return $this->JSONRespond();
    }

    public function sendJEEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Journal Entry';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
//            $this->msg = $this->getMessage('SUCC_GRN_EMAIL', array($documentType));
            $this->msg = $this->getMessage('SUCC_PAY_EMAIL');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_GRN_EMAIL', array($documentType));
        return $this->JSONRespond();
    }

    //delete journal entry by document type
    public function deleteJournalEntryByDocumentTypeAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE_REQUEST');
           return $this->JSONRespond();
        }

        $username = trim($request->getPost('username'));
        $password = trim($request->getPost('password'));
        $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
        if ($user) {
            if ($user->roleID == '1') {
                $passwordData = explode(':', $user->userPassword);
                $storedPassword = $passwordData[0];
                $checkPassword = md5($password . $passwordData[1]);

                if ($storedPassword == $checkPassword) {
                    $fromDate = $this->convertDateToStandardFormat($request->getPost('fromDate'));
                    $toDate = $this->convertDateToStandardFormat($request->getPost('toDate'));
                    $documentTypeID = $request->getPost('documentTypeID');

                    if (is_null($fromDate) && is_null($toDate)) {
                        if ($documentTypeID == "24") {
                            $documentTypeID = 6;
                            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeID($documentTypeID,$posCreditNote = true,false,false);
                        } else if ($documentTypeID == "25") {
                            $documentTypeID = 1;
                            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeID($documentTypeID,false,$posInvoice = true,false);
                        } else if ($documentTypeID == "26") {
                            $documentTypeID = 7;
                            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeID($documentTypeID,false ,false ,$posPayment = true);
                        } else {
                            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeID($documentTypeID);
                        }
                    } else {
                        if ($documentTypeID == "24") {
                            $documentTypeID = 6;
                            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDateFilter($fromDate,$toDate,$documentTypeID,$posCreditNote = true,false,false);
                        } else if ($documentTypeID == "25") {
                            $documentTypeID = 1;
                            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDateFilter($fromDate,$toDate,$documentTypeID,false,$posInvoice = true,false);
                        } else if ($documentTypeID == "26") {
                            $documentTypeID = 7;
                            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDateFilter($fromDate,$toDate,$documentTypeID,false ,false ,$posPayment = true);
                        } else {
                            $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDateFilter($fromDate,$toDate,$documentTypeID);
                        }
                    }

                    $this->beginTransaction();
                    foreach ($journalEntries as $key => $value) {
                        $journalEntryID = $value['journalEntryID'];

                        $result = $this->deleteJournalEntry($journalEntryID);

                        if(!$result['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_JOURNAL_ENTRIES_DELETE');
                            return $this->JSONRespond();                
                        }
                    }

                    $this->commit();
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_JOURNAL_ENTRIES_DELETE');
                    return $this->JSONRespond();

                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_USERAPICONTRO_INCPWD');
                    return $this->JSONRespond();
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_VIEWPAY_USER_PRIVILEGE');
                return $this->JSONRespond();
            }
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_VIEWPAY_INVALID_USRNAME_PWD');
            return $this->JSONRespond();
        }
    }

    /**
     * This function is used to post journal entries for opening balance
     * @return JSONRespond
     */
    public function postOpeningBalanceAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $openingBalanceJEData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getOpeningBalanceJournalEntryWithAccounts();

        $draftAccountDebitAmount = 0;
        $draftAccountCreditAmount = 0;
        $JEData = [];
        $i = 1;
        $journalEntryIDs = [];
        $jetDates = [];
        foreach ($openingBalanceJEData as $key => $value) {
            if ($value['financeAccountsID'] == "3000") {
                $draftAccountDebitAmount += floatval($value['journalEntryAccountsDebitAmount']);
                $draftAccountCreditAmount += floatval($value['journalEntryAccountsCreditAmount']);
            } else {
                $JEData['journalEntryAccounts'][$i]['fAccountsIncID'] = $i; 
                $JEData['journalEntryAccounts'][$i]['financeAccountsID'] = $value['financeAccountsID']; 
                $JEData['journalEntryAccounts'][$i]['financeGroupsID'] = $value['financeGroupsID']; 
                $JEData['journalEntryAccounts'][$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsDebitAmount']; 
                $JEData['journalEntryAccounts'][$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsCreditAmount']; 
                $JEData['journalEntryAccounts'][$i]['journalEntryAccountsMemo'] = $value['journalEntryAccountsMemo'];
                $i++; 
            }
            $journalEntryIDs[] = $value['journalEntryID']; 
            $jetDates[] = $value['journalEntryDate'];
        }

        if (!(count(array_unique($jetDates)) === 1)) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_JET_DATES');
            $this->data = null;
            return $this->JSONRespond();
        }

        $journalEntryDate = array_unique($jetDates)[0];

        if ($draftAccountDebitAmount != $draftAccountCreditAmount) {
            if ($draftAccountDebitAmount > $draftAccountCreditAmount) {
                $this->msg = "Can not post journal entries for opening balance. Because there is a balance different of ".($draftAccountDebitAmount - $draftAccountCreditAmount) ." in debit side";
            } else {
                $this->msg = "Can not post journal entries for opening balance. Because there is a balance different of ".($draftAccountCreditAmount - $draftAccountDebitAmount) ." in credit side";
            }            
                
            $this->status = false;
            return $this->JSONRespond();
        } else {
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $journalEntryCode = $this->getReferenceNumber($jelocationReferenceID);
            
            $JEData['journalEntryDate'] = $this->convertDateToStandardFormat($journalEntryDate);
            $JEData['journalEntryCode'] = $journalEntryCode;
            $JEData['journalEntryTypeID'] = "";
            $JEData['journalEntryIsReverse'] = "0";
            $JEData['journalEntryComment'] = "Journal Entry is posted when create Opening balance.";
            $JEData['journalEntryTemplateStatus'] = "false";
            $JEData['ignoreBudgetLimit'] = $this->getRequest()->getPost('ignoreBudgetLimit');

            $resultData = $this->saveJournalEntry($JEData);
            if($resultData['status']){
                foreach (array_unique($journalEntryIDs) as $key => $value) {
                    $deleteResult = $this->deleteJournalEntry($value);
                    if (!$deleteResult['status']) {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $deleteResult['msg'];
                        return $this->JSONRespond();     
                    }
                }

                $updateFinanceAccounts = $this->updateFinanceAccountsAmounts($resultData['data']['journalEntryID']);
                if (!$updateFinanceAccounts) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_WHILE_UPGRADE_FINANCE_ACCOUNTS_AMOUNTS');
                    return $this->JSONRespond();     
                }

                $this->commit();
                $this->flashMessenger()->addMessage($resultData['msg']);
                $this->data = $resultData['data'];
            }else{
                  $this->rollback();
            }

            $this->data = $resultData['data'];
            $this->status = $resultData['status'];
            $this->msg = $resultData['msg'];
            return $this->JSONRespond();
            
        }
    }


    public function updateFinanceAccountsAmounts($journalEntryID)
    {
        $journalEntryAccountsData = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);
        foreach ($journalEntryAccountsData as $key => $JEAccounts) {
            $financeAccountsID = $JEAccounts['financeAccountsID'];

            $financeAccountsValues = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($financeAccountsID);
            $financeCreditAmount = $financeAccountsValues['financeAccountsCreditAmount'] + $JEAccounts['journalEntryAccountsCreditAmount'];
            $financeDebitAmount = $financeAccountsValues['financeAccountsDebitAmount'] + $JEAccounts['journalEntryAccountsDebitAmount'];

            $financeAccountsData = array(
                'financeAccountsCreditAmount' => $financeCreditAmount,
                'financeAccountsDebitAmount' => $financeDebitAmount,
                );
            $financeStaus = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->updateFinanceAccounts($financeAccountsData, $financeAccountsID);

            if(!$financeStaus){
                return false;
            }
        }
        return true;
    }
      
}
