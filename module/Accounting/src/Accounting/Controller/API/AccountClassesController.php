<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\FinanceAccountClassForm;
use Accounting\Model\FinanceAccountClass;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Account Classes API related actions
 */
class AccountClassesController extends CoreController
{

    /**
     * Create Account
     * @return JsonModel
     */
    public function saveAccountClassAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();

        //this part use to find exists Account Classes.
        $accountExists = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getFinanceAccountClassByClassName($postData['financeAccountClassName']);
        if (count($accountExists) > 0) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONT_CLASS_NAME_EXIST');
            return $this->JSONRespond();

        } 

        $fAccountsTypesData = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->fetchAll();
        $fAccountTypesArray = array();
        foreach ($fAccountsTypesData as $value) {
           $fAccountTypesArray[$value->financeAccountTypesID] = $value->financeAccountTypesName;           
        }

        $fiananceAccountClassForm = new FinanceAccountClassForm( 'CreateFinanceAccountClass',
            array(
                'financeAccountTypes' => $fAccountTypesArray,
            )
        );

        $financeAccountClass = new FinanceAccountClass();
        $fiananceAccountClassForm->setInputFilter($financeAccountClass->getInputFilter());
        $fiananceAccountClassForm->setData($postData);

        if (!$fiananceAccountClassForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONT_CLASS_CREATE');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $postData['entityID'] = $this->createEntity();
        $financeAccountClass->exchangeArray($postData);
        $financeAccountClassID = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->saveFinanceAccountClass($financeAccountClass);
        
        if ($financeAccountClassID) {
            $this->status = true;
            $this->setLogMessage('Account class '.$postData['financeAccountClassName'].' is created.');
            $this->msg = $this->getMessage('SUCC_ACCONT_CLASS_CREATE');
            $this->data = $financeAccountClassID;
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONT_CLASS_CREATE');
            $this->rollback();
        }
        return $this->JSONRespond();
    }

    /**
     * Update Account
     * @return JsonModel
     */
    public function updateAccountClassAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        //this part use to find exists Account Classes.
        $oldData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAccountClassDataByAccountClassID($postData['financeAccountClassID']);

        $accountExists = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getFinanceAccountClassByClassName($postData['financeAccountClassName'])->current();
        if( $accountExists != '' && $accountExists['financeAccountClassID'] != $postData['financeAccountClassID']){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONT_CLASS_NAME_EXIST');
            return $this->JSONRespond();
        }

        $fAccountsTypesData = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->fetchAll();
        $fAccountTypesArray = array();
        foreach ($fAccountsTypesData as $value) {
            $fAccountTypesArray[$value->financeAccountTypesID] = $value->financeAccountTypesName;           
        }

        $fiananceAccountClassForm = new FinanceAccountClassForm( 'CreateFinanceAccountClass',
            array(
            	'financeAccountTypes' => $fAccountTypesArray,
        	)
        );

        $financeAccountClass = new FinanceAccountClass();
        $fiananceAccountClassForm->setInputFilter($financeAccountClass->getInputFilter());
        $fiananceAccountClassForm->setData($postData);
           
        if (!$fiananceAccountClassForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONT_CLASS_UPDATE');
            return $this->JSONRespond();
        }
            	
        $this->beginTransaction();
        $updateData = array(
            'financeAccountClassName'=>$postData['financeAccountClassName'],
            'financeAccountTypesID'=>$postData['financeAccountTypesID'],
        );
        $status = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->updateFinanceAccountClass($updateData , $postData['financeAccountClassID']);
        
        if ($status) {
            $changeArray = $this->getAccountClassChageDetails($oldData, $postData);
            //set log details
            $previousData = json_encode($changeArray['previousData']);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage('Account Class '.$oldData['financeAccountClassName'].' is updated.');
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_ACCONT_CLASS_UPDATE');
            $this->data = $postData['financeAccountClassID'];
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_ACCONT_CLASS_UPDATE');
           $this->rollback();
        }
        
        return $this->JSONRespond();
    }

    public function getAccountClassChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Finance Account Class Name'] = $row['financeAccountClassName'];
        $previousData['Finance Account Types Name'] = $row['financeAccountTypesName'];

        $newData = [];
        $newData['Finance Account Class Name'] = $data['financeAccountClassName'];
        $newData['Finance Account Types Name'] = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->getFinanceAccountTypeByID($data['financeAccountTypesID'])['financeAccountTypesName'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }


    public function searchAccountClassesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = array('list'=>'');
            return $this->JSONRespond();
        }
            
        $accountClassSearchKey = $searchrequest->getPost('searchKey');
        $accountClasses = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->searchAccountClassesForDropDown($accountClassSearchKey);

        $accountClassList = array();
        foreach ($accountClasses as $accountClass) {
            $temp['value'] = $accountClass['financeAccountClassID'];
            $temp['text'] = $accountClass['financeAccountClassName'];
            $accountClassList[] = $temp;
        }

        $this->data = array('list' => $accountClassList);
        return $this->JSONRespond();
        
    }

    public function getAccountClassFromSearchAction()
    {
    	$searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $accountClassID = $searchrequest->getPost('accountClassID');
        $accountClass = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAccountClassDataByAccountClassID($accountClassID);
        $data[$accountClass['financeAccountClassID']] = (object)$accountClass; 
        
        $AccountClasses = new ViewModel(
            array(
               'financeAccountClass' => $data,
            )
        );

        $AccountClasses->setTerminal(TRUE);
        $AccountClasses->setTemplate('accounting/account-classes/dynamic-list');
        return $AccountClasses;
    }

    public function deleteAccountClassAction()
    {
    	$searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
    	   $this->status = false;
    	   $this->msg = $this->getMessage('ERR_ACCONT_CLASS_DELETE_REQUEST');
           return $this->JSONRespond();
        }
        	
        $accountClassID = $searchrequest->getPost('financeAccountClassID');
        
        $accounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAllAccountsByAccountClassID($accountClassID);

        if (count($accounts) > 0) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONT_CLASS_DELETE_ACCOUNT_HAVE');
            return $this->JSONRespond();   
        }

        $accountClass = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAccountClassDataByAccountClassID($accountClassID);


        $this->beginTransaction();
        $status = $this->updateDeleteInfoEntity($accountClass['entityID']);

        if ($status) {
            $this->commit();
            $this->setLogMessage('Account class '.$accountClass['financeAccountClassName'].' is deleted.');
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_ACCONT_CLASS_DELETE');
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCONT_CLASS_DELETE');
            $this->rollback();
        }  
        return $this->JSONRespond();
    }
}
