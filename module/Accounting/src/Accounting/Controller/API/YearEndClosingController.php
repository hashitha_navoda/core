<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\YearEndClosingForm;
use Zend\Session\Container;

class YearEndClosingController extends CoreController
{
	protected $useAccounting;

    private $productIds = [];

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

	public function proceedYearEndClosingAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        
        $fiscalPeriodID = $postData['fiscalPeriod'];
        $yearEndClosingAcID = $postData['yearEndClosingAc'];
        $retainedEarningAcID = $postData['retainedEarningAc'];
        $locationID = $postData['currentLocationID'];
        $ignoreBudgetLimit = $postData['ignoreBudgetLimit'];

        $fiscalPeriods = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriodsForYearEndClosing();

        foreach ($fiscalPeriods as $value) {
            if ($fiscalPeriodID == $value['fiscalPeriodID']) {
                break;
            }
            $previousfiscalPeriodID[] = $value['fiscalPeriodID'];
        }
        $previousFiscalPeriodID = end($previousfiscalPeriodID);
        if (!is_null($previousFiscalPeriodID)) {
            $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($previousFiscalPeriodID);
            $fiscalPeriodStartDate = $fiscalPeriodData['fiscalPeriodStartDate'];
            $fiscalPeriodEndDate = $fiscalPeriodData['fiscalPeriodEndDate'];
            $checkFiscalPeriodForYearEndClosed = $this->CommonTable('Accounting\Model\JournalEntryTable')->getYearEndClosedFiscalPeriodJournalEntries($fiscalPeriodStartDate, $fiscalPeriodEndDate)->current();

            if(!$checkFiscalPeriodForYearEndClosed) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PREVIOUS_PERIOD_SHOULD_CLOSED');
                $this->data = null;
                return $this->JSONRespond();
            }
        }

        $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($fiscalPeriodID);

        $fiscalPeriodStartDate = $fiscalPeriodData['fiscalPeriodStartDate'];
        $fiscalPeriodEndDate = $fiscalPeriodData['fiscalPeriodEndDate'];

        $nextFiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getNextFiscalPeriodsForYearEndClosing($fiscalPeriodEndDate)->current();

        if (!$nextFiscalPeriod) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_MISSING_NEXT_PERIOD');
            $this->data = null;
            return $this->JSONRespond();
        }

        $yearEndClosingDate = $nextFiscalPeriod['fiscalPeriodStartDate'];
        $financeAccounts = array();
        $fAccountsTypesDataForForwardingBalance = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->fetchAll();
        
        $this->beginTransaction();
        foreach ($fAccountsTypesDataForForwardingBalance as $typeKey => $typeValue) {
            $fAccountClassDataForForwardingBalance = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClassesByAccountTypeID($typeValue->financeAccountTypesID);

            foreach ($fAccountClassDataForForwardingBalance as $key => $value) {
                $assetAcData = $this->getAccountsDataOfAssetLiabilityEquity($value['financeAccountClassID'], $fiscalPeriodStartDate, $fiscalPeriodEndDate, true);

                foreach ($assetAcData as $assetAcDataValue) {
                    $balanceDiff = $assetAcDataValue->totalCreditAmount - $assetAcDataValue->totalDebitAmount;
                    
                    if ($balanceDiff != 0) {
                        $resultSet = $this->saveJournalEntryDataForForwadingBalanceInYearEndClosing($fiscalPeriodStartDate, $fiscalPeriodEndDate,$assetAcDataValue, $locationID, $yearEndClosingDate, $ignoreBudgetLimit);
                        if (!$resultSet['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $resultSet['msg'];
                            $this->data = $resultSet['data'];
                            return $this->JSONRespond();
                        }
                    }
                    
                }
            }
        }


        $fAccountsTypesData = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->fetchAll();
        foreach ($fAccountsTypesData as $typeKey => $typeValue) {
            $fAccountClassData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClassesByAccountTypeID($typeValue->financeAccountTypesID);

            foreach ($fAccountClassData as $key => $value) {
                $fAccountsData = $this->getHierarchyByAccountClassIDs($value['financeAccountClassID'], $fiscalPeriodStartDate, $fiscalPeriodEndDate, true);
                foreach ($fAccountsData as $value) {

                    $balanceDiff = $value->totalCreditAmount - $value->totalDebitAmount;
                    
                    if ($balanceDiff != 0) {
                        $res = $this->saveJournalEntryDataForYearEndClosing($yearEndClosingAcID ,$fiscalPeriodStartDate, $fiscalPeriodEndDate, $value, $locationID, $yearEndClosingDate, $ignoreBudgetLimit);
                        if (!$res['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $res['msg'];
                            $this->data = $res['data'];
                            return $this->JSONRespond();
                        }
                        $totalDebit += floatval($res['data']['totalDebit']);
                        $totalCredit += floatval($res['data']['totalCredit']);
                    }
                }
            }
        }

        if (is_null($totalDebit) && is_null($totalCredit)) {
            $totalDebit = 0;
            $totalCredit = 0;
        }

        $result = $this->saveJournalEntryDataForYearEndClosingRetainedEarning($yearEndClosingAcID ,$retainedEarningAcID,$fiscalPeriodStartDate, $fiscalPeriodEndDate, $totalDebit ,$totalCredit, $locationID,$yearEndClosingDate, $ignoreBudgetLimit);
        if ($result['status']) {

        	$data = array(
   				'fiscalPeriodStatusID' => 11
   			);

            $this->CommonTable('Accounting\Model\FiscalPeriodTable')->updateFiscalPeriod($data, $fiscalPeriodID);
   			$this->CommonTable('Accounting\Model\FiscalPeriodTable')->updateFiscalPeriodByParentId($data, $fiscalPeriodID);
            $this->commit();
            $this->setLogMessage('Year end is closed for the fiscalPeriod '.$fiscalPeriodStartDate.' - '.$fiscalPeriodEndDate);
        	$this->status = true;
            $this->msg = $this->getMessage('SUCC_YEAR_END_CLOSING');
            $this->data = null;
            return $this->JSONRespond();
        } else {
            $this->rollback();
            $this->status = false;
            $this->msg = $result['msg'];
            $this->data = $result['data'];
            return $this->JSONRespond();
        }
    }


    public function processStepOneOfYearEndClosingAction() {

        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        
        $fiscalPeriodID = $postData['fiscalPeriod'];
        // $yearEndClosingAcID = $postData['yearEndClosingAc'];
        // $retainedEarningAcID = $postData['retainedEarningAc'];
        $locationID = $postData['currentLocationID'];
        $ignoreBudgetLimit = $postData['ignoreBudgetLimit'];

        $fiscalPeriods = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriodsForYearEndClosing();

        foreach ($fiscalPeriods as $value) {
            if ($fiscalPeriodID == $value['fiscalPeriodID']) {
                break;
            }
            $previousfiscalPeriodID[] = $value['fiscalPeriodID'];
        }
        $previousFiscalPeriodID = end($previousfiscalPeriodID);
        if (!is_null($previousFiscalPeriodID)) {
            $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($previousFiscalPeriodID);
            $fiscalPeriodStartDate = $fiscalPeriodData['fiscalPeriodStartDate'];
            $fiscalPeriodEndDate = $fiscalPeriodData['fiscalPeriodEndDate'];

            if ($fiscalPeriodData['isCompleteStepOne'] == 1 && $fiscalPeriodData['isCompleteStepTwo'] == 0) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PREVIOUS_PERIOD_SHOULD_COMPLETE_STEP_TWO');
                $this->data = null;
                return $this->JSONRespond();
            }

            $checkFiscalPeriodForYearEndClosed = $this->CommonTable('Accounting\Model\JournalEntryTable')->getYearEndClosedFiscalPeriodJournalEntries($fiscalPeriodStartDate, $fiscalPeriodEndDate)->current();

            if(!$checkFiscalPeriodForYearEndClosed && !($fiscalPeriodData['isCompleteStepOne'] == 1 && $fiscalPeriodData['isCompleteStepTwo'] == 1)) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PREVIOUS_PERIOD_SHOULD_CLOSED');
                $this->data = null;
                return $this->JSONRespond();
            }
        }

        $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($fiscalPeriodID);

        $fiscalPeriodStartDate = $fiscalPeriodData['fiscalPeriodStartDate'];
        $fiscalPeriodEndDate = $fiscalPeriodData['fiscalPeriodEndDate'];

        $nextFiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getNextFiscalPeriodsForYearEndClosing($fiscalPeriodEndDate)->current();

        if (!$nextFiscalPeriod) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_MISSING_NEXT_PERIOD');
            $this->data = null;
            return $this->JSONRespond();
        }

        $yearEndClosingDate = $nextFiscalPeriod['fiscalPeriodStartDate'];
        $financeAccounts = array();
        $fAccountsTypesDataForForwardingBalance = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->fetchAll();
        
        $this->beginTransaction();
        foreach ($fAccountsTypesDataForForwardingBalance as $typeKey => $typeValue) {
            $fAccountClassDataForForwardingBalance = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClassesByAccountTypeID($typeValue->financeAccountTypesID);

            foreach ($fAccountClassDataForForwardingBalance as $key => $value) {
                $assetAcData = $this->getAccountsDataOfAssetLiabilityEquity($value['financeAccountClassID'], $fiscalPeriodStartDate, $fiscalPeriodEndDate, true);

                foreach ($assetAcData as $assetAcDataValue) {
                    $balanceDiff = $assetAcDataValue->totalCreditAmount - $assetAcDataValue->totalDebitAmount;
                    
                    if ($balanceDiff != 0) {
                        $resultSet = $this->saveJournalEntryDataForForwadingBalanceInYearEndClosing($fiscalPeriodStartDate, $fiscalPeriodEndDate,$assetAcDataValue, $locationID, $yearEndClosingDate, $ignoreBudgetLimit);
                        if (!$resultSet['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $resultSet['msg'];
                            $this->data = $resultSet['data'];
                            return $this->JSONRespond();
                        }
                    }
                    
                }
            }
        }

        $data = array(
            'isCompleteStepOne' => 1
        );

        $this->CommonTable('Accounting\Model\FiscalPeriodTable')->updateFiscalPeriod($data, $fiscalPeriodID);
        $this->CommonTable('Accounting\Model\FiscalPeriodTable')->updateFiscalPeriodByParentId($data, $fiscalPeriodID);


        $this->commit();
        $this->setLogMessage('Year end stage one is closed for the fiscalPeriod '.$fiscalPeriodStartDate.' - '.$fiscalPeriodEndDate);
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_YEAR_END_CLOSING_STAGE_ONE');
        $this->data = null;
        return $this->JSONRespond();

    }



     public function processStepTwoOfYearEndClosingAction() {

        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        
        $fiscalPeriodID = $postData['fiscalPeriod'];
        $yearEndClosingAcID = $postData['yearEndClosingAc'];
        $retainedEarningAcID = $postData['retainedEarningAc'];
        $locationID = $postData['currentLocationID'];
        $ignoreBudgetLimit = $postData['ignoreBudgetLimit'];

        $fiscalPeriods = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriodsForYearEndClosing();

        foreach ($fiscalPeriods as $value) {
            if ($fiscalPeriodID == $value['fiscalPeriodID']) {
                break;
            }
            $previousfiscalPeriodID[] = $value['fiscalPeriodID'];
        }
        $previousFiscalPeriodID = end($previousfiscalPeriodID);
        if (!is_null($previousFiscalPeriodID)) {
            $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($previousFiscalPeriodID);
            $fiscalPeriodStartDate = $fiscalPeriodData['fiscalPeriodStartDate'];
            $fiscalPeriodEndDate = $fiscalPeriodData['fiscalPeriodEndDate'];
            $checkFiscalPeriodForYearEndClosed = $this->CommonTable('Accounting\Model\JournalEntryTable')->getYearEndClosedFiscalPeriodJournalEntries($fiscalPeriodStartDate, $fiscalPeriodEndDate)->current();

            if(!$checkFiscalPeriodForYearEndClosed && !($fiscalPeriodData['isCompleteStepOne'] == 1 && $fiscalPeriodData['isCompleteStepTwo'] == 1)) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PREVIOUS_PERIOD_SHOULD_CLOSED');
                $this->data = null;
                return $this->JSONRespond();
            }
        }

        $fiscalPeriodData = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodDataByFiscalPeriodID($fiscalPeriodID);

        $fiscalPeriodStartDate = $fiscalPeriodData['fiscalPeriodStartDate'];
        $fiscalPeriodEndDate = $fiscalPeriodData['fiscalPeriodEndDate'];

        $nextFiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getNextFiscalPeriodsForYearEndClosing($fiscalPeriodEndDate)->current();

        if (!$nextFiscalPeriod) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_MISSING_NEXT_PERIOD');
            $this->data = null;
            return $this->JSONRespond();
        }

        $yearEndClosingDate = $nextFiscalPeriod['fiscalPeriodStartDate'];
        $financeAccounts = array();
        $fAccountsTypesDataForForwardingBalance = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->fetchAll();
        
        $this->beginTransaction();
        $fAccountsTypesData = $this->CommonTable('Accounting\Model\FinanceAccountTypesTable')->fetchAll();
        foreach ($fAccountsTypesData as $typeKey => $typeValue) {
            $fAccountClassData = $this->CommonTable('Accounting\Model\FinanceAccountClassTable')->getAllActiveAccountClassesByAccountTypeID($typeValue->financeAccountTypesID);

            foreach ($fAccountClassData as $key => $value) {
                $fAccountsData = $this->getHierarchyByAccountClassIDs($value['financeAccountClassID'], $fiscalPeriodStartDate, $fiscalPeriodEndDate, true);
                foreach ($fAccountsData as $value) {

                    $balanceDiff = $value->totalCreditAmount - $value->totalDebitAmount;
                    
                    if ($balanceDiff != 0) {
                        $res = $this->saveJournalEntryDataForYearEndClosing($yearEndClosingAcID ,$fiscalPeriodStartDate, $fiscalPeriodEndDate, $value, $locationID, $yearEndClosingDate, $ignoreBudgetLimit);
                        if (!$res['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $res['msg'];
                            $this->data = $res['data'];
                            return $this->JSONRespond();
                        }
                        $totalDebit += floatval($res['data']['totalDebit']);
                        $totalCredit += floatval($res['data']['totalCredit']);
                    }
                }
            }
        }
        // die();

        if (is_null($totalDebit) && is_null($totalCredit)) {
            $totalDebit = 0;
            $totalCredit = 0;
        }

        $result = $this->saveJournalEntryDataForYearEndClosingRetainedEarning($yearEndClosingAcID ,$retainedEarningAcID,$fiscalPeriodStartDate, $fiscalPeriodEndDate, $totalDebit ,$totalCredit, $locationID,$yearEndClosingDate, $ignoreBudgetLimit);
        if ($result['status']) {

            $data = array(
                'fiscalPeriodStatusID' => 11,
                'isCompleteStepTwo' => 1
            );

            $this->CommonTable('Accounting\Model\FiscalPeriodTable')->updateFiscalPeriod($data, $fiscalPeriodID);
            $this->CommonTable('Accounting\Model\FiscalPeriodTable')->updateFiscalPeriodByParentId($data, $fiscalPeriodID);
            $this->commit();
            $this->setLogMessage('Year end stage two is closed for the fiscalPeriod '.$fiscalPeriodStartDate.' - '.$fiscalPeriodEndDate);
            $this->setLogMessage('Year end is completly closed for the fiscalPeriod '.$fiscalPeriodStartDate.' - '.$fiscalPeriodEndDate);
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_YEAR_END_CLOSING');
            $this->data = null;
            return $this->JSONRespond();
        } else {
            $this->rollback();
            $this->status = false;
            $this->msg = $result['msg'];
            $this->data = $result['data'];
            return $this->JSONRespond();
        }
    }

    public function getHierarchyByAccountClassIDs($accountClassID, $startDate = null, $endDate = null, $activeFlag = false)
    {

        $res = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAllHierarchicalByAccountClassID($accountClassID, $activeFlag);
        $all = array();

        foreach ($res as $loo) {
            $loo->totalCreditAmount = 0.00;
            $loo->totalDebitAmount = 0.00;
            
            $financeAccountTypesID = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountTypeIdByFinanceAccountId($loo->financeAccountsID)->current();
            if($financeAccountTypesID['financeAccountTypesID'] == 4 || $financeAccountTypesID['financeAccountTypesID'] == 6 || $financeAccountTypesID['financeAccountTypesID'] == 7 || $financeAccountTypesID['financeAccountTypesID'] == 3) {
	            $data = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($loo->financeAccountsID, $startDate, $endDate);

	            if($data['journalEntryID'] != NULL){
	                $loo->totalCreditAmount = $data['totalCreditAmount'];
	                $loo->totalDebitAmount = $data['totalDebitAmount'];
	                $loo->journalEntryID = $data['journalEntryID'];
	            }

                //get journal Entry details
                $journalEntrys =  $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRange($loo->financeAccountsID, $startDate, $endDate);
                //get duplicated credit and debit value for this account
                $jEntriesAndDuplicatedCreditDebitValue = $this->getDocumentCommentForRelatedJournalEntry($journalEntrys);
                if(sizeof($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']) > 0){
                    $loo->totalCreditAmount = floatval($loo->totalCreditAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['credit']);
                    $loo->totalDebitAmount = floatval($loo->totalDebitAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['debit']);
                }


	            $all[$loo->financeAccountsID] = $loo;
            }
        }
        return $all;
    }


    private function saveJournalEntryDataForYearEndClosing($yearEndClosingAcID , $fiscalPeriodStartDate ,$fiscalPeriodEndDate, $fAccountsData, $locationID, $yearEndClosingDate, $ignoreBudgetLimit)
    {
        if ($this->useAccounting == 1) {
			$journalEntryDate = $fiscalPeriodEndDate;

        	$yearEndClosingAcDetails = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountTypeIdByFinanceAccountId($yearEndClosingAcID)->current();
            
        	$journalEntryAccounts = array();
        	if ($fAccountsData->totalCreditAmount > $fAccountsData->totalDebitAmount) {
        		$balance = $fAccountsData->totalCreditAmount - $fAccountsData->totalDebitAmount;
        		
        		$journalEntryAccounts[1]['fAccountsIncID'] = 1;
                $journalEntryAccounts[1]['financeAccountsID'] = $fAccountsData->financeAccountsID;
                $journalEntryAccounts[1]['financeGroupsID'] = '';
                $journalEntryAccounts[1]['journalEntryAccountsDebitAmount'] = $balance;
                $journalEntryAccounts[1]['journalEntryAccountsCreditAmount'] = 0;
                $journalEntryAccounts[1]['yearEndCloseFlag'] = 1;
                $journalEntryAccounts[1]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$yearEndClosingAcDetails['financeAccountsCode'].'_'.$yearEndClosingAcDetails['financeAccountsName'];

                $journalEntryAccounts[2]['fAccountsIncID'] = 2;
                $journalEntryAccounts[2]['financeAccountsID'] = $yearEndClosingAcID;
                $journalEntryAccounts[2]['financeGroupsID'] = '';
                $journalEntryAccounts[2]['journalEntryAccountsDebitAmount'] = 0;
                $journalEntryAccounts[2]['journalEntryAccountsCreditAmount'] = $balance;
                $journalEntryAccounts[2]['yearEndCloseFlag'] = 1;
                $journalEntryAccounts[2]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$fAccountsData->financeAccountsCode.'_'.$fAccountsData->financeAccountsName;

                $journalEntryComment = "Created by Year End Closing on ".$fiscalPeriodStartDate. " - ". $fiscalPeriodEndDate;
        	} else if ($fAccountsData->totalCreditAmount < $fAccountsData->totalDebitAmount) {
        		$balance = $fAccountsData->totalDebitAmount - $fAccountsData->totalCreditAmount;

        		$journalEntryAccounts[1]['fAccountsIncID'] = 1;
                $journalEntryAccounts[1]['financeAccountsID'] = $fAccountsData->financeAccountsID;
                $journalEntryAccounts[1]['financeGroupsID'] = '';
                $journalEntryAccounts[1]['journalEntryAccountsDebitAmount'] = 0;
                $journalEntryAccounts[1]['journalEntryAccountsCreditAmount'] = $balance;
                $journalEntryAccounts[1]['yearEndCloseFlag'] = 1;
                $journalEntryAccounts[1]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$yearEndClosingAcDetails['financeAccountsCode'].'_'.$yearEndClosingAcDetails['financeAccountsName'];

                $journalEntryAccounts[2]['fAccountsIncID'] = 2;
                $journalEntryAccounts[2]['financeAccountsID'] = $yearEndClosingAcID;
                $journalEntryAccounts[2]['financeGroupsID'] = '';
                $journalEntryAccounts[2]['journalEntryAccountsDebitAmount'] = $balance;
                $journalEntryAccounts[2]['journalEntryAccountsCreditAmount'] = 0;
                $journalEntryAccounts[2]['yearEndCloseFlag'] = 1;
                $journalEntryAccounts[2]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$fAccountsData->financeAccountsCode.'_'.$fAccountsData->financeAccountsName;

                $journalEntryComment = "Created by Year End Closing on ".$fiscalPeriodStartDate. " - ". $fiscalPeriodEndDate;
        	}
            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $journalEntryDate,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'yearEndClosed' => 1,
                'journalEntryComment' => $journalEntryComment,
                'ignoreBudgetLimit' => $ignoreBudgetLimit
            );
            $resultData = $this->saveJournalEntry($journalEntryData,null,null,false,null,null,true);

            if(!$resultData['status']){
	            $this->rollback();
                $respondSet = array(
                    'status' => false,
                    'data' => $resultData['data'],
                    'msg' => $this->getMessage('ERR_OCURED_WHILE_CREATING_JOURNAL_ENTRY'),
                );
                return $respondSet;
            }

            $res = array(
                'status' => true,
                'msg' => "success",
                'data' => ['totalCredit'=> $fAccountsData->totalCreditAmount,'totalDebit'=> $fAccountsData->totalDebitAmount ]
            );
            return $res;
        } else {
            $this->rollback();
            $respond = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_USER_ACCOUNT_NOT_SET'),
            );
            return $respond;
        }
    }

   	private function saveJournalEntryDataForYearEndClosingRetainedEarning($yearEndClosingAcID ,$retainedEarningAcID,$fiscalPeriodStartDate, $fiscalPeriodEndDate, $totalDebit ,$totalCredit, $locationID,$yearEndClosingDate, $ignoreBudgetLimit)
    {
        if ($this->useAccounting == 1) {
        	$journalEntryDate = $yearEndClosingDate;

        	$yearEndClosingAcDetails = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountTypeIdByFinanceAccountId($yearEndClosingAcID)->current();

        	$retainedEarningAcDetails = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountTypeIdByFinanceAccountId($retainedEarningAcID)->current();
        	
        	$journalEntryAccounts = array();
        	if ($totalCredit > $totalDebit) {
        		$balance = $totalCredit - $totalDebit;
        		
        		$journalEntryAccounts[1]['fAccountsIncID'] = 1;
                $journalEntryAccounts[1]['financeAccountsID'] = $yearEndClosingAcID;
                $journalEntryAccounts[1]['financeGroupsID'] = '';
                $journalEntryAccounts[1]['journalEntryAccountsDebitAmount'] = $balance;
                $journalEntryAccounts[1]['journalEntryAccountsCreditAmount'] = 0;
                $journalEntryAccounts[1]['yearEndCloseFlag'] = 1;
                $journalEntryAccounts[1]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$retainedEarningAcDetails['financeAccountsCode'].'_'.$retainedEarningAcDetails['financeAccountsName'];

                $journalEntryAccounts[2]['fAccountsIncID'] = 2;
                $journalEntryAccounts[2]['financeAccountsID'] = $retainedEarningAcID;
                $journalEntryAccounts[2]['financeGroupsID'] = '';
                $journalEntryAccounts[2]['journalEntryAccountsDebitAmount'] = 0;
                $journalEntryAccounts[2]['journalEntryAccountsCreditAmount'] = $balance;
                $journalEntryAccounts[2]['yearEndCloseFlag'] = 0;
                $journalEntryAccounts[2]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$yearEndClosingAcDetails['financeAccountsCode'].'_'.$yearEndClosingAcDetails['financeAccountsName'];

        	} else if ($totalCredit < $totalDebit) {
        		$balance = $totalDebit - $totalCredit;

        		$journalEntryAccounts[1]['fAccountsIncID'] = 1;
                $journalEntryAccounts[1]['financeAccountsID'] = $yearEndClosingAcID;
                $journalEntryAccounts[1]['financeGroupsID'] = '';
                $journalEntryAccounts[1]['journalEntryAccountsDebitAmount'] = 0;
                $journalEntryAccounts[1]['journalEntryAccountsCreditAmount'] = $balance;
                $journalEntryAccounts[1]['yearEndCloseFlag'] = 1;
                $journalEntryAccounts[1]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$retainedEarningAcDetails['financeAccountsCode'].'_'.$retainedEarningAcDetails['financeAccountsName'];

                $journalEntryAccounts[2]['fAccountsIncID'] = 2;
                $journalEntryAccounts[2]['financeAccountsID'] = $retainedEarningAcID;
                $journalEntryAccounts[2]['financeGroupsID'] = '';
                $journalEntryAccounts[2]['journalEntryAccountsDebitAmount'] = $balance;
                $journalEntryAccounts[2]['journalEntryAccountsCreditAmount'] = 0;
                $journalEntryAccounts[2]['yearEndCloseFlag'] = 0;
                $journalEntryAccounts[2]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$yearEndClosingAcDetails['financeAccountsCode'].'_'.$yearEndClosingAcDetails['financeAccountsName'];

        	} else {
        		$journalEntryAccounts[1]['fAccountsIncID'] = 1;
                $journalEntryAccounts[1]['financeAccountsID'] = $yearEndClosingAcID;
                $journalEntryAccounts[1]['financeGroupsID'] = '';
                $journalEntryAccounts[1]['journalEntryAccountsDebitAmount'] = 0;
                $journalEntryAccounts[1]['journalEntryAccountsCreditAmount'] = 0;
                $journalEntryAccounts[1]['yearEndCloseFlag'] = 1;
                $journalEntryAccounts[1]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$retainedEarningAcDetails['financeAccountsCode'].'_'.$retainedEarningAcDetails['financeAccountsName'];

                $journalEntryAccounts[2]['fAccountsIncID'] = 2;
                $journalEntryAccounts[2]['financeAccountsID'] = $retainedEarningAcID;
                $journalEntryAccounts[2]['financeGroupsID'] = '';
                $journalEntryAccounts[2]['journalEntryAccountsDebitAmount'] = 0;
                $journalEntryAccounts[2]['journalEntryAccountsCreditAmount'] = 0;
                $journalEntryAccounts[2]['yearEndCloseFlag'] = 0;
                $journalEntryAccounts[2]['journalEntryAccountsMemo'] = 'Created for Year End Closing by '.$yearEndClosingAcDetails['financeAccountsCode'].'_'.$yearEndClosingAcDetails['financeAccountsName'];

        	}
            $journalEntryComment = "Created by Year End Closing on ".$fiscalPeriodStartDate. " - ". $fiscalPeriodEndDate;
            
            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $journalEntryDate,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => $journalEntryComment,
                'ignoreBudgetLimit' => $ignoreBudgetLimit
            );
           
            $resultData = $this->saveJournalEntry($journalEntryData,null,null,false,null,null,true);

            if(!$resultData['status']){
                $this->rollback();
                $respondSet = array(
                    'status' => false,
                    'data' => $resultData['data'],
                    'msg' => $this->getMessage('ERR_OCURED_WHILE_CREATING_JOURNAL_ENTRY'),
                );
                return $respondSet;
            } else {
            	$respondSet = array(
	                'status' => true,
	                'msg' => "success",
                );
            	return $respondSet;
            }
        } else {
            $this->rollback();
            $respondSet = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_USER_ACCOUNT_NOT_SET'),
            );
            return $respondSet;
        }
    }

    private function getDocumentCommentForRelatedJournalEntry($jEntries)
    {
        $returnJEntity = [];
        $removedCreditDebit = [];
        foreach ($jEntries as $key => $singleValue) {
            $updated = true;
            switch ($singleValue['documentTypeID']){
                case '1' :
                    $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByInvoiceIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$invoiceDetails['salesInvoiceID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        $singleValue['relatedDocComment'] = $invoiceDetails['salesInvoiceComment'];
                        $singleValue['documentType'] = 'Sales Invoice';
                        $singleValue['documentCode'] = $invoiceDetails['salesInvoiceCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $invoiceDetails['customerCode'].' - '.$invoiceDetails['customerName'];
                    }
                    break;
                case '4' :
                    $deliveryNoteDetails = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteByIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$deliveryNoteDetails['deliveryNoteID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        
                        $singleValue['relatedDocComment'] = $deliveryNoteDetails['deliveryNoteComment'];
                        $singleValue['documentType'] = 'Delivery Note';
                        $singleValue['documentCode'] = $deliveryNoteDetails['deliveryNoteCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $deliveryNoteDetails['customerCode'].' - '.$deliveryNoteDetails['customerName'];
                    }
                    break;
                case '7' :
                    $paymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByPaymentIDForJE($singleValue['journalEntryDocumentID']);
                    if(sizeof($paymentDetails) == 0){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        $paymentDet = $this->createPayDataSet($paymentDetails);
                        
                        $singleValue['relatedDocComment'] = $paymentDet['docComment'];
                        $singleValue['documentType'] = 'Sales Payment';
                        $singleValue['documentCode'] = $paymentDet['docCode'];
                        $singleValue['childDoc'] = $paymentDet['childDoc'];
                        $singleValue['cusName'] = $paymentDet['cusName'];
                    }
                         
                    break;
                case '5' :
                    $returnDetails = $this->CommonTable('SalesReturnsTable')->getReturnforSearchForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$returnDetails['salesReturnID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $returnDetails['salesReturnComment'];
                        $singleValue['documentType'] = 'Sales Return';
                        $singleValue['documentCode'] = $returnDetails['salesReturnCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $returnDetails['customerCode'].' - '.$returnDetails['customerName'];
                    }
                    break;
                case '6' :
                    $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$creditNoteDetails['creditNoteID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $creditNoteDetails['creditNoteComment'];
                        $singleValue['documentType'] = 'Credit Note';
                        $singleValue['documentCode'] = $creditNoteDetails['creditNoteCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $creditNoteDetails['customerCode'].' - '.$creditNoteDetails['customerName'];
                    }
                    break;
                case '17' :
                    $creditNotePayment = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentIDForJE($singleValue['journalEntryDocumentID'], NULL)->current();
                    if(!$creditNotePayment['creditNotePaymentID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $creditNotePayment['creditNotePaymentMemo'];
                        $singleValue['documentType'] = 'Credit Note Payment';
                        $singleValue['documentCode'] = $creditNotePayment['creditNotePaymentCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $creditNotePayment['customerCode'].' - '.$creditNotePayment['customerName'];
                    }
                    break;
                case '12' :
                    $piDetails = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$piDetails['purchaseInvoiceID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $piDetails['purchaseInvoiceComment'];
                        $singleValue['documentType'] = 'Purchase Invoice';
                        $singleValue['documentCode'] = $piDetails['purchaseInvoiceCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $piDetails['supplierCode'].' - '.$piDetails['supplierName'];
                    }
                    break;
                case '14' :
                    $suppPaymentDetails = $this->CommonTable('SupplierPaymentsTable')->getPaymentsByPaymentIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$suppPaymentDetails['outgoingPaymentID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $suppPaymentDetails['outgoingPaymentMemo'];
                        $singleValue['documentType'] = 'Supplier Payment';
                        $singleValue['documentCode'] = $suppPaymentDetails['outgoingPaymentCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $suppPaymentDetails['supplierCode'].' - '.$suppPaymentDetails['supplierName'];
                    }
                    break;
                case '11' :
                    $prDetails = $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPurchaseReturnByIdForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$prDetails['purchaseReturnID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $prDetails['purchaseReturnComment'];
                        $singleValue['documentType'] = 'Purchase Return';
                        $singleValue['documentCode'] = $prDetails['purchaseReturnCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $prDetails['supplierCode'].' - '.$prDetails['supplierName'];
                    }
                    break;
                case '13' :
                    $debitNoteDetails = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$debitNoteDetails['debitNoteID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $debitNoteDetails['debitNoteComment'];
                        $singleValue['documentType'] = 'Debit Note';
                        $singleValue['documentCode'] = $debitNoteDetails['debitNoteCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $debitNoteDetails['supplierCode'].' - '.$debitNoteDetails['supplierName'];
                    }
                    break;
                case '18' :
                    $debitNotePaymentDetails = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentIDForJE($singleValue['journalEntryDocumentID'], NULL)->current();
                    if(!$debitNotePaymentDetails['debitNotePaymentID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $debitNotePaymentDetails['debitNotePaymentMemo'];
                        $singleValue['documentType'] = 'Debit Note Payment';
                        $singleValue['documentCode'] = $debitNotePaymentDetails['debitNotePaymentCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $debitNotePaymentDetails['supplierCode'].' - '.$debitNotePaymentDetails['supplierName'];
                    }
                    break;
                case '10' :
                    $grnDetails = $this->CommonTable('Inventory\Model\GrnTable')->grnDetailsByGrnIdForJE($singleValue['journalEntryDocumentID']);
                    if(!$grnDetails['grnID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $grnDetails['grnComment'];
                        $singleValue['documentType'] = 'GRN';
                        $singleValue['documentCode'] = $grnDetails['grnCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = $grnDetails['supplierCode'].' - '.$grnDetails['supplierName'];
                    }
                    break;
                case '16' :
                    $adjustmentDetails = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getDataForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$adjustmentDetails['goodsIssueID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        
                        $singleValue['relatedDocComment'] = $adjustmentDetails['goodsIssueReason'];
                        $singleValue['documentCode'] = $adjustmentDetails['goodsIssueCode'];
                        $singleValue['childDoc'] = '-';
                        $singleValue['cusName'] = '-';
                        if($adjustmentDetails['goodsIssueTypeID'] == '2'){
                            $singleValue['documentType'] = 'Positive Adjustment';
                        } else {
                            $singleValue['documentType'] = 'Negative Adjustment';
                        }
                    }
                    break;
                case '20' :
                    $pvDetails = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherByPaymentVoucherIDForJE($singleValue['journalEntryDocumentID'])->current();
                    if(!$pvDetails['paymentVoucherID']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                        $updated = false;
                    } else {
                        $singleValue['relatedDocComment'] = $pvDetails['paymentVoucherComment'];
                        $singleValue['documentType'] = 'Payment Voucher';
                        $singleValue['documentCode'] = $pvDetails['paymentVoucherCode'];
                        $singleValue['childDoc'] = '-';
                        if($pvDetails['paymentVoucherSupplierID'] != ''){
                            $singleValue['cusName'] = $pvDetails['supplierCode'].' - '.$pvDetails['supplierName'];
                        } else {
                            $singleValue['cusName'] = $pvDetails['financeAccountsCode'].' - '.$pvDetails['financeAccountsName'];
                        }
                        
                    }

                    break;
                case '32' :
                    $chequeData = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getChequeDepositByChequeDeposiIdForJE($singleValue['journalEntryDocumentID']);
                    if($chequeData['chequeDepositId']){
                        $removedCreditDebit['credit'] += $singleValue['journalEntryAccountsCreditAmount'];
                        $removedCreditDebit['debit'] += $singleValue['journalEntryAccountsDebitAmount'];
                       $updated = false; 
                    }
                    break;
                default :
                    break;
            }
            if($updated){
                $returnJEntity[] = $singleValue;
            }
        }
        $retrunAllData = array(
            'returnJEntity' => $returnJEntity,
            'creditDebitRemovedValues' => $removedCreditDebit,
            );
        return $retrunAllData;
    }

    public function createPayDataSet($paymentDetails)
    {
        $docType = [];
        $returnData = [];
        foreach ($paymentDetails as $key => $value) {

            $returnData['cusName'] = $value['customerCode'].' - '.$value['customerName'];
            $returnData['docCode'] = $value['incomingPaymentCode'];
            $returnData['docComment'] = $value['incomingPaymentMemo'];
            if($value['incomingPaymentMethodChequeNumber'] != null){
                $docType[] = 'cheque No - '. $value['incomingPaymentMethodChequeNumber'];
            } else if($value['incomingPaymentMethodCreditReceiptNumber'] != null){
                $docType[] = 'credit Card ref - '. $value['incomingPaymentMethodCreditReceiptNumber'];
            }
        }
        $returnData['childDoc'] = implode("/", $docType);
        return $returnData;


    }


    /**
     * This function is used to get balance data of account that are not included in profit and loss report
     *
     */
    public function getAccountsDataOfAssetLiabilityEquity($accountClassID, $startDate = null, $endDate = null, $activeFlag = false)
    {

        $res = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAllHierarchicalByAccountClassID($accountClassID, $activeFlag);
        $all = array();

        foreach ($res as $loo) {
            $loo->totalCreditAmount = 0.00;
            $loo->totalDebitAmount = 0.00;
            
            $financeAccountTypesID = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountTypeIdByFinanceAccountId($loo->financeAccountsID)->current();
            if($financeAccountTypesID['financeAccountTypesID'] == 1 || $financeAccountTypesID['financeAccountTypesID'] == 2 || $financeAccountTypesID['financeAccountTypesID'] == 5) {
                $data = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($loo->financeAccountsID, $startDate, $endDate, true);

                if($data['journalEntryID'] != NULL){
                    $loo->totalCreditAmount = $data['totalCreditAmount'];
                    $loo->totalDebitAmount = $data['totalDebitAmount'];
                    $loo->journalEntryID = $data['journalEntryID'];
                }

                //get journal Entry details
                $journalEntrys =  $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRange($loo->financeAccountsID, $startDate, $endDate, true);
                //get duplicated credit and debit value for this account
                $jEntriesAndDuplicatedCreditDebitValue = $this->getDocumentCommentForRelatedJournalEntry($journalEntrys);
                if(sizeof($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']) > 0){
                    $loo->totalCreditAmount = floatval($loo->totalCreditAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['credit']);
                    $loo->totalDebitAmount = floatval($loo->totalDebitAmount) - floatval($jEntriesAndDuplicatedCreditDebitValue['creditDebitRemovedValues']['debit']);
                }


                $all[$loo->financeAccountsID] = $loo;
            }
        }
        return $all;
    }


    private function saveJournalEntryDataForForwadingBalanceInYearEndClosing($fiscalPeriodStartDate, $fiscalPeriodEndDate,$fAccountsData, $locationID, $yearEndClosingDate, $ignoreBudgetLimit)
    {
        if ($this->useAccounting == 1) {
            $journalEntryDate = $yearEndClosingDate;

            
            $journalEntryAccounts = array();
            if ($fAccountsData->totalCreditAmount > $fAccountsData->totalDebitAmount) {
                $balance = $fAccountsData->totalCreditAmount - $fAccountsData->totalDebitAmount;
                
                $journalEntryAccounts[1]['fAccountsIncID'] = 1;
                $journalEntryAccounts[1]['financeAccountsID'] = $fAccountsData->financeAccountsID;
                $journalEntryAccounts[1]['financeGroupsID'] = '';
                $journalEntryAccounts[1]['journalEntryAccountsDebitAmount'] = $balance;
                $journalEntryAccounts[1]['journalEntryAccountsCreditAmount'] = 0;
                $journalEntryAccounts[1]['yearEndCloseFlag'] = 1;
                $journalEntryAccounts[1]['journalEntryAccountsMemo'] = 'Created for Forwading balance of '.$fAccountsData->financeAccountsCode.'_'.$fAccountsData->financeAccountsName.' on year end closing';

                $journalEntryAccounts[2]['fAccountsIncID'] = 2;
                $journalEntryAccounts[2]['financeAccountsID'] = $fAccountsData->financeAccountsID;
                $journalEntryAccounts[2]['financeGroupsID'] = '';
                $journalEntryAccounts[2]['journalEntryAccountsDebitAmount'] = 0;
                $journalEntryAccounts[2]['journalEntryAccountsCreditAmount'] = $balance;
                $journalEntryAccounts[2]['yearEndCloseFlag'] = 0;
                $journalEntryAccounts[2]['journalEntryAccountsMemo'] = 'Created for Forwading balance of '.$fAccountsData->financeAccountsCode.'_'.$fAccountsData->financeAccountsName.' on year end closing';

                $journalEntryComment = "Created by Year End Closing on ".$fiscalPeriodStartDate. " - ". $fiscalPeriodEndDate;
            } else if ($fAccountsData->totalCreditAmount < $fAccountsData->totalDebitAmount) {
                $balance = $fAccountsData->totalDebitAmount - $fAccountsData->totalCreditAmount;

                $journalEntryAccounts[1]['fAccountsIncID'] = 1;
                $journalEntryAccounts[1]['financeAccountsID'] = $fAccountsData->financeAccountsID;
                $journalEntryAccounts[1]['financeGroupsID'] = '';
                $journalEntryAccounts[1]['journalEntryAccountsDebitAmount'] = 0;
                $journalEntryAccounts[1]['journalEntryAccountsCreditAmount'] = $balance;
                $journalEntryAccounts[1]['yearEndCloseFlag'] = 1;
                $journalEntryAccounts[1]['journalEntryAccountsMemo'] = 'Created for Forwading balance of '.$fAccountsData->financeAccountsCode.'_'.$fAccountsData->financeAccountsName.' on year end closing';

                $journalEntryAccounts[2]['fAccountsIncID'] = 2;
                $journalEntryAccounts[2]['financeAccountsID'] = $fAccountsData->financeAccountsID;
                $journalEntryAccounts[2]['financeGroupsID'] = '';
                $journalEntryAccounts[2]['journalEntryAccountsDebitAmount'] = $balance;
                $journalEntryAccounts[2]['journalEntryAccountsCreditAmount'] = 0;
                $journalEntryAccounts[2]['yearEndCloseFlag'] = 0;
                $journalEntryAccounts[2]['journalEntryAccountsMemo'] = 'Created for Forwading balance of '.$fAccountsData->financeAccountsCode.'_'.$fAccountsData->financeAccountsName.' on year end closing';

                $journalEntryComment = "Created by Year End Closing on ".$fiscalPeriodStartDate. " - ". $fiscalPeriodEndDate;
            }
            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $journalEntryDate,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => $journalEntryComment,
                'ignoreBudgetLimit' => $ignoreBudgetLimit
            );
            $resultData = $this->saveJournalEntry($journalEntryData,null,null,false,null,null,true);

            if(!$resultData['status']){
                $this->rollback();
                $respondSet = array(
                    'status' => false,
                    'data' => $resultData['data'],
                    'msg' => $this->getMessage('ERR_OCURED_WHILE_CREATING_JOURNAL_ENTRY'),
                );
                return $respondSet;
            }

            $res = array(
                'status' => true,
                'msg' => "success",
                'data' => ['totalCredit'=> $fAccountsData->totalCreditAmount,'totalDebit'=> $fAccountsData->totalDebitAmount ]
            );
            return $res;
        } else {
            $this->rollback();
            $respond = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_USER_ACCOUNT_NOT_SET'),
            );
            return $respond;
        }
    }
}