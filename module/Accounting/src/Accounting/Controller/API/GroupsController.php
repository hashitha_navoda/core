<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Accounting\Form\FinanceGroupsForm;
use Accounting\Model\FinanceGroups;

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Groups API related actions
 */
class GroupsController extends CoreController
{

    /**
     * Create Account
     * @return JsonModel
     */
    public function saveGroupsAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();

        //this part use to find exists Groups.
        $groupExists = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->getFinanceGroupsByGroupsName($postData['financeGroupsName']);
        if (count($groupExists) > 0) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GROUP_NAME_EXIST');
            return $this->JSONRespond();

        } 

        $financeGroupsForm = $this->getFinanceGroupsForm();

        $financeGroups = new FinanceGroups();
        $financeGroupsForm->setInputFilter($financeGroups->getInputFilter());
        $financeGroupsForm->setData($postData);

        if (!$financeGroupsForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GROUP_CREATE');
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $postData['entityID'] = $this->createEntity();
        $financeGroups->exchangeArray($postData);
        $financeGroupsID = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->saveFinanceGroups($financeGroups);
        
        if ($financeGroupsID) {
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_GROUP_CREATE');
            $this->data = $financeGroupsID;
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GROUP_CREATE');
            $this->rollback();
        }
        return $this->JSONRespond();
    }

    /**
     * Update Account
     * @return JsonModel
     */
    public function updateGroupsAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        
        //this part use to find exists Groups.
        $groupExists = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->getFinanceGroupsByGroupsName($postData['financeGroupsName'])->current();
        if( $groupExists != '' && $groupExists['financeGroupsID'] != $postData['financeGroupsID']){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GROUP_NAME_EXIST');
            return $this->JSONRespond();
        }

        $financeGroupsForm = $this->getFinanceGroupsForm();

        $financeGroups = new FinanceGroups();
        $financeGroupsForm->setInputFilter($financeGroups->getInputFilter());
        $financeGroupsForm->setData($postData);
           
        if (!$financeGroupsForm->isValid()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GROUP_UPDATE');
            return $this->JSONRespond();
        }
                
        $this->beginTransaction();
        $updateData = array(
            'financeGroupsName'=>$postData['financeGroupsName'],
            'financeGroupsShortName'=>$postData['financeGroupsShortName'],
            'financeGroupTypesID'=>$postData['financeGroupTypesID'],
            'financeGroupsAddress'=>$postData['financeGroupsAddress'],
            'financeGroupsParentID'=>$postData['financeGroupsParentID'],
        );
        $status = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->updateFinanceGroups($updateData , $postData['financeGroupsID']);
        
        if ($status) {
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_GROUP_UPDATE');
            $this->data = $postData['financeGroupsID'];
            $this->flashMessenger()->addMessage($this->msg);
            $this->commit();
        } else {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_GROUP_UPDATE');
           $this->rollback();
        }
        
        return $this->JSONRespond();
    }

    public function searchGroupsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = array('list'=>'');
            return $this->JSONRespond();
        }
            
        $groupsSearchKey = $searchrequest->getPost('searchKey');
        $groups = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->searchGroupsForDropDown($groupsSearchKey);

        $groupsList = array();
        foreach ($groups as $group) {
            $temp['value'] = $group['financeGroupsID'];
            $temp['text'] = $group['financeGroupsName'];
            $groupsList[] = $temp;
        }

        $this->data = array('list' => $groupsList);
        return $this->JSONRespond();
        
    }

    public function getGroupsFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $groupsID = $searchrequest->getPost('groupsID');
        $groups = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->getGroupsDataByGroupsID($groupsID);
        $data[$groups['financeGroupsID']] = (object)$groups; 
        
        $groups = new ViewModel(
            array(
               'financeGroups' => $data,
            )
        );

        $groups->setTerminal(TRUE);
        $groups->setTemplate('accounting/groups/dynamic-list');
        return $groups;
    }

    public function deleteGroupsAction()
    {
        $searchrequest = $this->getRequest();
        if (!$searchrequest->isPost()) {
           $this->status = false;
           $this->msg = $this->getMessage('ERR_GROUP_DELETE_REQUEST');
           return $this->JSONRespond();
        }
            
        $groupsID = $searchrequest->getPost('financeGroupsID');
        $financeGroupsData = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->getFinanceGroupsByFinanceGroupParentID($groupsID);
        
        if(count($financeGroupsData) > 0){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GROUP_IS_PARENT_OF_ANOTHER');
            return $this->JSONRespond();       
        }

        $groups = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->getGroupsDataByGroupsID($groupsID);

        $this->beginTransaction();
        $status = $this->updateDeleteInfoEntity($groups['entityID']);

        if ($status) {
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_GROUP_DELETE');
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_GROUP_DELETE');
            $this->rollback();
        }  
        return $this->JSONRespond();
    }

    private function getFinanceGroupsForm()
    {
    	$fGroupTypesData = $this->CommonTable('Accounting\Model\FinanceGroupTypesTable')->fetchAll();
        $fGroupTypesArray = array();
        foreach ($fGroupTypesData as $value) {
            $fGroupTypesArray[$value->financeGroupTypesID] = $value->financeGroupTypesName;           
        }

        $fGroupsData = $this->CommonTable('Accounting\Model\FinanceGroupsTable')->fetchAll();
        $fGroupsArray = array();
        foreach ($fGroupsData as $value) {
            $fGroupsArray[$value['financeGroupsID']] = $value['financeGroupsName'];           
        }

        $fiananceGroupsForm = new FinanceGroupsForm('CreateFinanceGroups',
                array(
            'financeGroupTypes' => $fGroupTypesArray,
            'financeGroups' => $fGroupsArray,
        ));

        return $fiananceGroupsForm;
    }
}
