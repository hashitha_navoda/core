<?php

namespace Accounting\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class IncomeController extends CoreController
{
    public function getIncomeReferenceForLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $refData = $this->getReferenceNoForLocation(42, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];
            if ($rid == '' || $rid == NULL) {
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_PURINV_REFLOCATION');
                } else {
                    $this->msg = $this->getMessage('ERR_PURINV_REFMAX');
                }
                $this->data = FALSE;
                $this->status = false;
            } else {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $rid,
                    'locRefID' => $lrefID,
                );
            }
            return $this->JSONRespond();
        }
    }

    public function saveIncomeAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $dimensionData = $post['dimensionData'];
        $ignoreBudgetLimit = false;
        $this->beginTransaction();

        $userLocation = $this->user_session->userActiveLocation['locationID'];
        $respond = $this->getService('IncomeService')->saveIncome($request->getPost(), $userLocation, $this->userID, $this->companyCurrencyId);

        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }

        if (!is_null($respond['data']['eventParameter'])) {
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $respond['data']['eventParameter']);
        }

        $i=0;
        $journalEntryAccounts = array();

        foreach ($respond['data']['accountProduct'] as $key => $value) {
            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
            $journalEntryAccounts[$i]['financeGroupsID'] = '';
            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Income '.$respond['data']['incomeVoucherCode'].'.';
            $i++;
        }


        //get journal entry reference number.
        $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
        $jelocationReferenceID = $jeresult['locRefID'];
        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

        $journalEntryData = array(
            'journalEntryAccounts' => $journalEntryAccounts,
            'journalEntryDate' => $respond['data']['date'],
            'journalEntryCode' => $JournalEntryCode,
            'journalEntryTypeID' => '',
            'journalEntryIsReverse' => 0,
            'journalEntryComment' => 'Journal Entry is posted when create Income '.$respond['data']['incomeVoucherCode'].'.',
            'documentTypeID' => 43,
            'journalEntryDocumentID' => $respond['data']['incomeID'],
        );

        $resultData = $this->saveJournalEntry($journalEntryData);
        if($resultData['status']){

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($respond['data']['incomeID'],43, 3);
            if(!$jEDocStatusUpdate['status']){
                $this->rollback();
                $this->status = false;
                $this->msg = $jEDocStatusUpdate['msg'];
                return $this->JSONRespond();
            }

            if (!empty($dimensionData)) {
                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$respond['data']['incomeVoucherCode']], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                if(!$saveRes['status']){
                    return array('status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']);
                }   
            } 
            
            $this->commit();
            $this->status = true;
            $this->data = $respond['data']['incomeID'];
            $this->msg = "Income Saved Successfully";
            return $this->JSONRespond();
        }else{
            $this->rollback();
            $this->status = false;
            $this->msg = $resultData['msg'];
            return $this->JSONRespond();
        }
    }

    public function editIncomeAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DATA_SET');
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $respond = $this->getService('IncomeService')->editIncome($request->getPost());
        
        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }

        $this->commit();
        $this->status = true;
        $this->data = null;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    public function retriveCustomerIncomeAction()
    {
        $er = array();
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $cust_id = $request->getPost('customerID');
            $incomes = $this->CommonTable('Accounting\Model\IncomeTable')->getIncomeByCustomerID($cust_id, $locationID);

            while ($t = $incomes->current()) {
                $income[$t['incomeID']] = (object) $t;
            }
            $customerIncome = new ViewModel(
                    array('income' => $income,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol));
            $customerIncome->setTerminal(TRUE);
            $customerIncome->setTemplate('accounting/income/incomeList');
            return $customerIncome;
        }
    }

    public function searchIncomeForDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $sKey = $searchrequest->getPost('searchKey');
            $locationID = $this->getActiveAllLocationsIds();
            $incomes = $this->CommonTable('Accounting\Model\IncomeTable')->searchIncomesForDropDown($locationID, $sKey);

            $incomeList = array();
            foreach ($incomes as $deliveryNote) {
                $temp['value'] = $deliveryNote['incomeID'];
                $temp['text'] = $deliveryNote['incomeCode'];
                $incomeList[] = $temp;
            }

            $this->setLogMessage("Retrive income list for dropdown.");
            $this->data = array('list' => $incomeList);
            return $this->JSONRespond();
        }
    }

    public function getIncomeFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($searchrequest->isPost()) {
            $incomeID = $searchrequest->getPost('incomeID');
            $incomes = $this->CommonTable('Accounting\Model\IncomeTable')->getIncomeforSearch($incomeID);
            while ($t = $incomes->current()) {
                $income[$t['incomeID']] = (object) $t;
            }
            if ($incomes->count() == 0) {
                return new JsonModel(array(FALSE));
            } else {
                $inc = new ViewModel(
                        array('income' => $income,
                    'statuses' => $this->getStatusesList(),
                    'userdateFormat' => $dateFormat,
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,));
                $inc->setTerminal(TRUE);
                $inc->setTemplate('accounting/income/incomeList');
                return $inc;
            }
        }
    }

    public function getIncomeByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($invrequest->isPost()) {
            $fromdate = $this->convertDateToStandardFormat($invrequest->getPost('fromdate'));
            $todate = $this->convertDateToStandardFormat($invrequest->getPost('todate'));
            $customerID = $invrequest->getPost('customerID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $filteredIncome = $this->CommonTable('Accounting\Model\IncomeTable')->getIncomeByDate($fromdate, $todate, $customerID, $locationID);

            while ($t = $filteredIncome->current()) {
                $income[$t['incomeID']] = (object) $t;
            }
            $DateFilteredIncome = new ViewModel(
                    array('income' => $income,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,));
            $DateFilteredIncome->setTerminal(TRUE);
            $DateFilteredIncome->setTemplate('accounting/income/incomeList');
            return $DateFilteredIncome;
        }
    }
}
