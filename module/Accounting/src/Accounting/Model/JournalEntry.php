<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class JournalEntry
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class JournalEntry implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented financeGroupsID. (Primary key)
     */
    public $JournalEntryID;
    
    /**
     * @var string journalEntryCode.
     */
    public $journalEntryCode;

    /**
     * @var date  journalEntryDate.
     */
    public $journalEntryDate;

    /**
     * @var date  journalEntryTypeID.
     */
    public $journalEntryTypeID;

    /**
     * @var date  journalEntryStatusID.
     */
    public $journalEntryStatusID;

    /**
     * @var tynyint  journalEntryIsReverse.
     */
    public $journalEntryIsReverse;

    /**
     * @var int  journalEntryReverseRefID.
     */
    public $journalEntryReverseRefID;

    /**
     * @var string  journalEntryComment.
     */
    public $journalEntryComment;

    /**
     * @var int  locationID.
     */
    public $locationID;

    /**
     * @var int  journalEntryHashValue.
     */
    public $journalEntryHashValue;

     /**
     * @var int  documentTypeID.
     */
    public $documentTypeID;

    /**
     * @var int  journalEntryDocumentID.
     */
    public $journalEntryDocumentID;

     /**
     * @var int  journalEntryTemplateStatus.
     */
    public $journalEntryTemplateStatus;
    
     /**
     * @var int  yearEndClosed.
     */
    public $yearEndClosed;
    
     /**
     * @var int  openingBalanceFlag.
     */
    public $openingBalanceFlag;
    
    /**
     * @var int entityID.
     */
    public $entityID;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->JournalEntryID = (!empty($data['JournalEntryID'])) ? $data['JournalEntryID'] : null;
        $this->journalEntryCode = (!empty($data['journalEntryCode'])) ? $data['journalEntryCode'] : null;
        $this->journalEntryDate = (!empty($data['journalEntryDate'])) ? $data['journalEntryDate'] : null;
        $this->journalEntryTypeID = (!empty($data['journalEntryTypeID'])) ? $data['journalEntryTypeID'] : 0;
        $this->journalEntryStatusID = (!empty($data['journalEntryStatusID'])) ? $data['journalEntryStatusID'] : 0;
        $this->journalEntryIsReverse = (!empty($data['journalEntryIsReverse'])) ? $data['journalEntryIsReverse'] : 0;
        $this->journalEntryReverseRefID = (!empty($data['journalEntryReverseRefID'])) ? $data['journalEntryReverseRefID'] : null;
        $this->journalEntryComment = (!empty($data['journalEntryComment'])) ? $data['journalEntryComment'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->journalEntryHashValue = (!empty($data['journalEntryHashValue'])) ? $data['journalEntryHashValue'] : null;
        $this->documentTypeID = (!empty($data['documentTypeID'])) ? $data['documentTypeID'] : null;
        $this->journalEntryDocumentID = (!empty($data['journalEntryDocumentID'])) ? $data['journalEntryDocumentID'] : null;
        $this->journalEntryTemplateStatus = (!empty($data['journalEntryTemplateStatus'])) ? $data['journalEntryTemplateStatus'] : 0;
        $this->yearEndClosed = (!empty($data['yearEndClosed'])) ? $data['yearEndClosed'] : 0;
        $this->openingBalanceFlag = (!empty($data['openingBalanceFlag'])) ? $data['openingBalanceFlag'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryCode',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryDate',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

             $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryTypeID',
                        'required' => false, 
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryStatusID',
                        'required' => true, 
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryIsReverse',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryComment',
                        'required' => false, 
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 40000,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'locationID',
                        'required' => true, 
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryHashValue',
                        'required' => true, 
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryReverseRefID',
                        'required' => false, 
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryTemplateStatus',
                        'required' => false, 
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
