<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class financeGroupTypesTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Finance Group Types Table Functions
 */
class FinanceGroupTypesTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * FinanceGroupTypesTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

     /**
     * FetchAll finance AccountGroupTypes 
     * @return $resultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }
}
