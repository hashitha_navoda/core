<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class JournalEntryAccounts
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class JournalEntryAccounts implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented JournalEntryAccountsID. (Primary key)
     */
    public $JournalEntryAccountsID;
    
    /**
     * @var int journalEntryID.
     */
    public $journalEntryID;

    /**
     * @var int  financeAccountsID.
     */
    public $financeAccountsID;

    /**
     * @var int  financeGroupsID.
     */
    public $financeGroupsID;

    /**
     * @var double  journalEntryAccountsDebitAmount.
     */
    public $journalEntryAccountsDebitAmount;

    /**
     * @var double  journalEntryAccountsCreditAmount.
     */
    public $journalEntryAccountsCreditAmount;
    
    /**
     * @var string journalEntryAccountsMemo.
     */
    public $journalEntryAccountsMemo;
    
     /**
     * @var Boolean yearEndCloseFlag.
     */
    public $yearEndCloseFlag;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->journalEntryAccountsID = (!empty($data['journalEntryAccountsID'])) ? $data['journalEntryAccountsID'] : null;
        $this->journalEntryID = (!empty($data['journalEntryID'])) ? $data['journalEntryID'] : null;
        $this->financeAccountsID = (!empty($data['financeAccountsID'])) ? $data['financeAccountsID'] : null;
        $this->financeGroupsID = (!empty($data['financeGroupsID'])) ? $data['financeGroupsID'] : null;
        $this->journalEntryAccountsDebitAmount = (!empty($data['journalEntryAccountsDebitAmount'])) ? $data['journalEntryAccountsDebitAmount'] : 0.00;
        $this->journalEntryAccountsCreditAmount = (!empty($data['journalEntryAccountsCreditAmount'])) ? $data['journalEntryAccountsCreditAmount'] : 0.00;
        $this->journalEntryAccountsMemo = (!empty($data['journalEntryAccountsMemo'])) ? $data['journalEntryAccountsMemo'] : null;
        $this->yearEndCloseFlag = (!empty($data['yearEndCloseFlag'])) ? $data['yearEndCloseFlag'] : 0;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryID',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
            	$factory->createInput(
            		array(
            			'name' => 'financeAccountsID',
            			'required' => true,                        
            		)
            	)
            );

            $inputFilter->add(
            	$factory->createInput(
            		array(
            			'name' => 'financeGroupsID',
            			'required' => false,                        
            		)
            	)
            );

            $inputFilter->add(
            	$factory->createInput(
            		array(
            			'name' => 'journalEntryAccountsDebitAmount',
            			'required' => false,                        
            		)
            	)
            );

            $inputFilter->add(
            	$factory->createInput(
            		array(
            			'name' => 'journalEntryAccountsCreditAmount',
            			'required' => false,                        
            		)
            	)
            );


            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryAccountsMemo',
                        'required' => false, 
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 800,
                                ),
                            ),
                        ),                        
                    )
                )
            );
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
