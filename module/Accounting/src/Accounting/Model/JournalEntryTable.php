<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
/**
 * Class JournalEntryTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Journal Entry Table Functions
 */
class JournalEntryTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * JournalEntryTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * FetchAll Journal Entry
     * @return $resultSet
     */
    public function fetchAll($paginated = FALSE, $journalEntryTemplateFlag, $journalEntryOpeningBalanceFlag = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
            ->columns(array('*'))
            ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
            ->order(array('journalEntry.journalEntryID' => 'DESC'))
            ->where(array('deleted' => '0','journalEntryReverseRefID' => null))
            ->order(['journalEntry.journalEntryDate' => 'DESC', 'journalEntry.journalEntryID' => 'DESC']);
        if ($journalEntryTemplateFlag == 1) {
            $select->where(array('journalEntryTemplateStatus' => 1));
        }
        
        if ($journalEntryOpeningBalanceFlag) {
            $select->where(array('openingBalanceFlag' => 1));
        }
        
        if ($paginated) {
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveJournalEntry(JournalEntry $journalEntry)
    {   
         $data = array(
            'journalEntryCode' => $journalEntry->journalEntryCode,
            'journalEntryDate' => $journalEntry->journalEntryDate,
            'journalEntryTypeID' => $journalEntry->journalEntryTypeID,
            'journalEntryStatusID' => $journalEntry->journalEntryStatusID,
            'journalEntryIsReverse' => $journalEntry->journalEntryIsReverse,
            'journalEntryReverseRefID' => $journalEntry->journalEntryReverseRefID,
            'journalEntryComment' => $journalEntry->journalEntryComment,
            'locationID' => $journalEntry->locationID,
            'journalEntryHashValue' => $journalEntry->journalEntryHashValue,
            'documentTypeID' => $journalEntry->documentTypeID,
            'journalEntryDocumentID' => $journalEntry->journalEntryDocumentID,
            'journalEntryTemplateStatus' => $journalEntry->journalEntryTemplateStatus,
            'yearEndClosed' => $journalEntry->yearEndClosed,
            'openingBalanceFlag' => $journalEntry->openingBalanceFlag,
            'entityID' => $journalEntry->entityID,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function getJournalEntryByJournalEntryCode($journalEntryCode)
    {
        $rowset = $this->tableGateway->select(array('journalEntryCode' => $journalEntryCode));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

     public function searchJournalEntryForDropDown($journalEntrySearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array(
                	'posi' => new Expression('POSITION(\'' . $journalEntrySearchKey . '\' in journalEntryCode )>0,POSITION(\'' . $journalEntrySearchKey . '\' in journalEntryCode), 9999'),
                    'len' => new Expression('CHAR_LENGTH(journalEntryCode )'),
                	'*'
                	))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0,'journalEntryReverseRefID' => null))
        		->where(new PredicateSet(array(new Operator('journalEntry.journalEntryCode', 'like', '%' . $journalEntrySearchKey . '%')), PredicateSet::OP_OR))
        		->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryDataByJournalEntryID($journalEntryID)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted', 'createdTimeStamp'), 'left')
                ->join('journalEntryType', 'journalEntry.journalEntryTypeID = journalEntryType.journalEntryTypeID', array('journalEntryTypeName'), 'left')
                ->where(array('entity.deleted'=>0,'journalEntry.journalEntryID' => $journalEntryID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function getJournalEntryByJournalEntryReverseRefID($journalEntryReverseRefID)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0,'journalEntry.journalEntryReverseRefID' => $journalEntryReverseRefID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    

    public function getJournalEntryDataForViewByJournalEntryCode($journalEntryCode)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('journalEntryAccountsID','financeAccountsID','financeGroupsID','journalEntryAccountsDebitAmount','journalEntryAccountsCreditAmount','journalEntryAccountsMemo'), 'left')
                ->join('financeAccounts', 'journalEntryAccounts.financeAccountsID = financeAccounts.financeAccountsID', array('financeAccountsCode','financeAccountsName'), 'left')
                ->where(array('entity.deleted'=>0,'journalEntry.journalEntryCode' => $journalEntryCode));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateJournalEntry($data,$journalEntryID)
    {
    	try {
    		$this->tableGateway->update($data,array('journalEntryID' => $journalEntryID));
    		return true;
    	} catch (Exception $e) {
    		return false;	
    	}
        
    }

    public function updateJournalEntryDocumentStatus($data,$documentTypeID, $documentID)
    {
        try {
            $this->tableGateway->update($data,array('documentTypeID' => $documentTypeID, 'journalEntryDocumentID' => $documentID));
            return true;
        } catch (Exception $e) {
            return false;   
        }
        
    }

    public function getJournalEntryDataByJournalEntryTypeID($journalEntryTypeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0,'journalEntry.journalEntryTypeID' => $journalEntryTypeID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryDataByDocumentTypeIDAndDocumentID($documentTypeID, $documentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0, 'journalEntry.documentTypeID' => $documentTypeID, 'journalEntry.journalEntryDocumentID' => $documentID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function getJournalEntryDataByDocumentTypeID($documentTypeID,$posCreditNote, $posInvoice, $posPayment)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0,'journalEntry.documentTypeID' => $documentTypeID))
                ->order(['journalEntry.journalEntryDate' => 'DESC', 'journalEntry.journalEntryID' => 'DESC']);
        if ($posCreditNote) {
            $select->join('creditNote','creditNote.creditNoteID = journalEntry.journalEntryDocumentID',array('creditNoteID'),'left');
            $select->where(array('creditNote.pos'=>1));
        }
        if ($posInvoice) {
            $select->join('salesInvoice','salesInvoice.salesInvoiceID = journalEntry.journalEntryDocumentID',array('salesInvoiceID'),'left');
            $select->where(array('salesInvoice.pos'=>1));
        }
        if ($posPayment) {
            $select->join('incomingPayment','incomingPayment.incomingPaymentID = journalEntry.journalEntryDocumentID',array('incomingPaymentID'),'left');
            $select->where(array('incomingPayment.pos'=>1));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryDataByDateFilter($fromDate, $toDate, $documentTypeID ,$posCreditNote, $posInvoice, $posPayment)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0));

        if($documentTypeID != ''){
            $select->where(array("journalEntry.documentTypeID" => $documentTypeID));
        }

        if ($posCreditNote) {
            $select->join('creditNote','creditNote.creditNoteID = journalEntry.journalEntryDocumentID',array('creditNoteID'),'left');
            $select->where(array('creditNote.pos'=>1));
        }
        if ($posInvoice) {
            $select->join('salesInvoice','salesInvoice.salesInvoiceID = journalEntry.journalEntryDocumentID',array('salesInvoiceID'),'left');
            $select->where(array('salesInvoice.pos'=>1));
        }
        if ($posPayment) {
            $select->join('incomingPayment','incomingPayment.incomingPaymentID = journalEntry.journalEntryDocumentID',array('incomingPaymentID'),'left');
            $select->where(array('incomingPayment.pos'=>1));
        }

        $select->where->between('journalEntry.journalEntryDate', $fromDate, $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryDataWithAccountDetailsByDocumentTypeIDAndDocumentID($documentTypeID, $documentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID' , array('*'))
                ->where(array('entity.deleted'=>0, 'journalEntry.documentTypeID' => $documentTypeID, 'journalEntry.journalEntryDocumentID' => $documentID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    public function getJournalEntryDetailsByJEID($jEID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('journalEntryAccountsID','financeAccountsID','financeGroupsID','journalEntryAccountsDebitAmount','journalEntryAccountsCreditAmount','journalEntryAccountsMemo'), 'left')
                ->join('location','journalEntry.locationID = location.locationID',array('locationName','locationCode'),'left')
                ->join('financeAccounts', 'journalEntryAccounts.financeAccountsID = financeAccounts.financeAccountsID', array('financeAccountsCode','financeAccountsName'), 'left')
                ->where(array('entity.deleted'=>0,'journalEntry.journalEntryID' => $jEID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getFiscalPeriodJournalEntries($fiscalPeriodStartDate, $fiscalPeriodEndDate, $checkYearEndClosed )
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0));

        $select->where->between('journalEntry.journalEntryDate', $fiscalPeriodStartDate, $fiscalPeriodEndDate);
        if ($checkYearEndClosed) {
            $select->where(array('journalEntry.yearEndClosed' => 1));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getYearEndClosedFiscalPeriodJournalEntries($fiscalPeriodStartDate, $fiscalPeriodEndDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0));

        $select->where->between('journalEntry.journalEntryDate', $fiscalPeriodStartDate, $fiscalPeriodEndDate);
        $select->where(array('journalEntry.yearEndClosed' => 1));;
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryIdByFinancialAcIdAndFiscalPeriod($financeAccountsID, $fiscalPeriodStartDate, $fiscalPeriodEndDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('journalEntryID'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('financeAccountsID'), 'left')
                ->where(array('entity.deleted'=>0,'journalEntryAccounts.financeAccountsID' => $financeAccountsID));
        $select->where->between('journalEntry.journalEntryDate', $fiscalPeriodStartDate, $fiscalPeriodEndDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * This function is used to get opening balance journal enties with journal entry accaount
     */
    public function getOpeningBalanceJournalEntryWithAccounts()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('journalEntryAccountsID','financeAccountsID','financeGroupsID','journalEntryAccountsDebitAmount','journalEntryAccountsCreditAmount','journalEntryAccountsMemo'), 'left')
                ->join('financeAccounts', 'journalEntryAccounts.financeAccountsID = financeAccounts.financeAccountsID', array('financeAccountsCode','financeAccountsName'), 'left')
                ->where(array('entity.deleted'=>0,'journalEntry.openingBalanceFlag' => 1));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryDataByJournalEntryIDs($journalEntryIDs = [])
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=> 0));
        $select->where->in('journalEntry.journalEntryID', $journalEntryIDs);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID($documentTypeID, $documentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryDimensions', 'journalEntry.journalEntryID = journalEntryDimensions.journalEntryID' , array('*'),'left')
                ->join('dimension', 'dimension.dimensionId = journalEntryDimensions.dimensionType' , array('dimensionName'),'left')
                ->join('dimensionValues', 'dimensionValues.dimensionValueId = journalEntryDimensions.dimensionValueID' , array('dimensionValue'),'left')
                ->where(array('entity.deleted'=>0, 'journalEntry.documentTypeID' => $documentTypeID, 'journalEntry.journalEntryDocumentID' => $documentID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getDailyTransactions($date)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=> 0, 'journalEntry.journalEntryDate' => $date));
        $select->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
