<?php

namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class IncomeProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveIncomeProduct($incomeProduct)
    {
        $data = array(
            'incomeID' => $incomeProduct->incomeID,
            'productID' => $incomeProduct->productID,
            'incomeProductPrice' => $incomeProduct->incomeProductPrice,
            'incomeProductDiscount' => $incomeProduct->incomeProductDiscount,
            'incomeProductDiscountType' => $incomeProduct->incomeProductDiscountType,
            'incomeProductTotal' => $incomeProduct->incomeProductTotal,
            'incomeProductQuantity' => $incomeProduct->incomeProductQuantity,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }    
}
