<?php 

namespace Accounting\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Income
{

    public $incomeID;
    public $incomeCode;
    public $customerID;
    public $incomeAccountID;
    public $incomeLocationID;
    public $incomeDate;
    public $incomeTotal;
    public $incomeComment;
    public $incomeItemFlag;
    public $status;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->incomeID = (!empty($data['incomeID'])) ? $data['incomeID'] : null;
        $this->incomeCode = (!empty($data['incomeCode'])) ? $data['incomeCode'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->incomeAccountID = (!empty($data['incomeAccountID'])) ? $data['incomeAccountID'] : null;
        $this->incomeLocationID = (!empty($data['incomeLocationID'])) ? $data['incomeLocationID'] : null;
        $this->incomeDate = (!empty($data['incomeDate'])) ? $data['incomeDate'] : null;
        $this->incomeTotal = (!empty($data['incomeTotal'])) ? $data['incomeTotal'] : 0;
        $this->incomeComment = (!empty($data['incomeComment'])) ? $data['incomeComment'] : null;
        $this->incomeItemFlag = (!empty($data['incomeItemFlag'])) ? $data['incomeItemFlag'] : 0;
        $this->status = (!empty($data['status'])) ? $data['status'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : 0;
    }

}