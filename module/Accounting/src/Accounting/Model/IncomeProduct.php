<?php

namespace Accounting\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomeProduct
{

    public $incomeProductID;
    public $incomeID;
    public $productID;
    public $incomeProductPrice;
    public $incomeProductDiscount;
    public $incomeProductDiscountType;
    public $incomeProductTotal;
    public $incomeProductQuantity;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->incomeProductID = (!empty($data['incomeProductID'])) ? $data['incomeProductID'] : null;
        $this->incomeID = (!empty($data['incomeID'])) ? $data['incomeID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->incomeProductPrice = (!empty($data['incomeProductPrice'])) ? $data['incomeProductPrice'] : null;
        $this->incomeProductDiscount = (!empty($data['incomeProductDiscount'])) ? $data['incomeProductDiscount'] : null;
        $this->incomeProductDiscountType = (!empty($data['incomeProductDiscountType'])) ? $data['incomeProductDiscountType'] : null;
        $this->incomeProductTotal = (!empty($data['incomeProductTotal'])) ? $data['incomeProductTotal'] : null;
        $this->incomeProductQuantity = (!empty($data['incomeProductQuantity'])) ? $data['incomeProductQuantity'] : null;
    }
}
