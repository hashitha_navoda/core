<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

/**
 * Class GlAccountSetupTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains GlAccountSetupTable Functions
 */
class GlAccountSetupTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * GlAccountSetupTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveGlAccountSetup(GlAccountSetup $glAccountSetup)
    {   
        $data = array(
            'glAccountSetupItemDefaultSalesAccountID' => $glAccountSetup->glAccountSetupItemDefaultSalesAccountID,
            'glAccountSetupItemDefaultInventoryAccountID' => $glAccountSetup->glAccountSetupItemDefaultInventoryAccountID,
            'glAccountSetupItemDefaultCOGSAccountID' => $glAccountSetup->glAccountSetupItemDefaultCOGSAccountID,
            'glAccountSetupItemDefaultAdjusmentAccountID' => $glAccountSetup->glAccountSetupItemDefaultAdjusmentAccountID,
            'glAccountSetupSalesAndCustomerReceivableAccountID' => $glAccountSetup->glAccountSetupSalesAndCustomerReceivableAccountID,
            'glAccountSetupSalesAndCustomerSalesAccountID' => $glAccountSetup->glAccountSetupSalesAndCustomerSalesAccountID,
            'glAccountSetupSalesAndCustomerSalesDiscountAccountID' => $glAccountSetup->glAccountSetupSalesAndCustomerSalesDiscountAccountID,
            'glAccountSetupSalesAndCustomerAdvancePaymentAccountID' => $glAccountSetup->glAccountSetupSalesAndCustomerAdvancePaymentAccountID,
            'glAccountSetupPurchasingAndSupplierPayableAccountID' => $glAccountSetup->glAccountSetupPurchasingAndSupplierPayableAccountID,
            'glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID' => $glAccountSetup->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID,
            'glAccountSetupPurchasingAndSupplierGrnClearingAccountID' => $glAccountSetup->glAccountSetupPurchasingAndSupplierGrnClearingAccountID,
            'glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID' => $glAccountSetup->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID,
            'glAccountSetupGeneralExchangeVarianceAccountID' => $glAccountSetup->glAccountSetupGeneralExchangeVarianceAccountID,
            'glAccountSetupGeneralProfitAndLostYearAccountID' => $glAccountSetup->glAccountSetupGeneralProfitAndLostYearAccountID,
            'glAccountSetupGeneralBankChargersAccountID' => $glAccountSetup->glAccountSetupGeneralBankChargersAccountID,
            'glAccountSetupGeneralDeliveryChargersAccountID' => $glAccountSetup->glAccountSetupGeneralDeliveryChargersAccountID,
            'glAccountSetupGeneralLoyaltyExpenseAccountID' => $glAccountSetup->glAccountSetupGeneralLoyaltyExpenseAccountID,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function fetchAll()
    {
    	$resultSet = $this->tableGateway->select();
        return $resultSet;	
    }

    public function updateGlAccountSetup(GlAccountSetup $glAccountSetup, $glAccountSetupID)
    {
    	$data = array(
            'glAccountSetupItemDefaultSalesAccountID' => $glAccountSetup->glAccountSetupItemDefaultSalesAccountID,
            'glAccountSetupItemDefaultInventoryAccountID' => $glAccountSetup->glAccountSetupItemDefaultInventoryAccountID,
            'glAccountSetupItemDefaultCOGSAccountID' => $glAccountSetup->glAccountSetupItemDefaultCOGSAccountID,
            'glAccountSetupItemDefaultAdjusmentAccountID' => $glAccountSetup->glAccountSetupItemDefaultAdjusmentAccountID,
            'glAccountSetupSalesAndCustomerReceivableAccountID' => $glAccountSetup->glAccountSetupSalesAndCustomerReceivableAccountID,
            'glAccountSetupSalesAndCustomerSalesAccountID' => $glAccountSetup->glAccountSetupSalesAndCustomerSalesAccountID,
            'glAccountSetupSalesAndCustomerSalesDiscountAccountID' => $glAccountSetup->glAccountSetupSalesAndCustomerSalesDiscountAccountID,
            'glAccountSetupSalesAndCustomerAdvancePaymentAccountID' => $glAccountSetup->glAccountSetupSalesAndCustomerAdvancePaymentAccountID,
            'glAccountSetupPurchasingAndSupplierPayableAccountID' => $glAccountSetup->glAccountSetupPurchasingAndSupplierPayableAccountID,
            'glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID' => $glAccountSetup->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID,
            'glAccountSetupPurchasingAndSupplierGrnClearingAccountID' => $glAccountSetup->glAccountSetupPurchasingAndSupplierGrnClearingAccountID,
            'glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID' => $glAccountSetup->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID,
            'glAccountSetupGeneralExchangeVarianceAccountID' => $glAccountSetup->glAccountSetupGeneralExchangeVarianceAccountID,
            'glAccountSetupGeneralProfitAndLostYearAccountID' => $glAccountSetup->glAccountSetupGeneralProfitAndLostYearAccountID,
            'glAccountSetupGeneralBankChargersAccountID' => $glAccountSetup->glAccountSetupGeneralBankChargersAccountID,
            'glAccountSetupGeneralDeliveryChargersAccountID' => $glAccountSetup->glAccountSetupGeneralDeliveryChargersAccountID,
            'glAccountSetupGeneralLoyaltyExpenseAccountID' => $glAccountSetup->glAccountSetupGeneralLoyaltyExpenseAccountID,
        );

    	try {
    		$this->tableGateway->update($data,array('glAccountSetupID' => $glAccountSetupID));
            return TRUE;
    	} catch (Exception $e) {
            return FALSE;
    	}
    }

    /*
    * get the glaccount setup data by account id
    */
    public function getDefaultAccountByAccountID($accountID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('glAccountSetup')
                ->columns(array('*'))
                ->where(new PredicateSet(array(new Operator('glAccountSetupItemDefaultSalesAccountID', '=',$accountID), 
                    new Operator('glAccountSetupItemDefaultInventoryAccountID', '=',$accountID),
                    new Operator('glAccountSetupItemDefaultCOGSAccountID', '=',$accountID),
                    new Operator('glAccountSetupItemDefaultAdjusmentAccountID', '=',$accountID),
                    new Operator('glAccountSetupSalesAndCustomerReceivableAccountID', '=',$accountID),
                    new Operator('glAccountSetupSalesAndCustomerSalesAccountID', '=',$accountID),
                    new Operator('glAccountSetupSalesAndCustomerSalesDiscountAccountID', '=',$accountID),
                    new Operator('glAccountSetupSalesAndCustomerAdvancePaymentAccountID', '=',$accountID),
                    new Operator('glAccountSetupPurchasingAndSupplierPayableAccountID', '=',$accountID),
                    new Operator('glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID', '=',$accountID),
                    new Operator('glAccountSetupPurchasingAndSupplierGrnClearingAccountID', '=',$accountID),
                    new Operator('glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID', '=',$accountID),
                    new Operator('glAccountSetupGeneralExchangeVarianceAccountID', '=',$accountID),
                    new Operator('glAccountSetupGeneralProfitAndLostYearAccountID', '=',$accountID),
                    new Operator('glAccountSetupGeneralBankChargersAccountID', '=',$accountID),
                    new Operator('glAccountSetupGeneralLoyaltyExpenseAccountID', '=',$accountID),
                    new Operator('glAccountSetupGeneralDeliveryChargersAccountID', '=',$accountID)
                ), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }
}
