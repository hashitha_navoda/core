<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class financeAccountTypesTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Finance Account Types Table Functions
 */
class FinanceAccountTypesTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * FinanceAccountTypesTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

     /**
     * FetchAll finance AccountTypes 
     * @return $resultSet
     */
    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getFinanceAccountTypeWithTheirClasses()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccountTypes')
                ->columns(array('*'))
                ->join('financeAccountClass', 'financeAccountClass.financeAccountTypesID = financeAccountTypes.financeAccountTypesID', array('*'), 'left')
                ->join('entity', 'financeAccountClass.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('financeAccountTypes.financeAccountTypesID ASC'))
                ->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;        
    }

    public function getFinanceAccountTypeByID($financeAccountTypesID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccountTypes')
                ->columns(array('*'))
                ->where(array('financeAccountTypesID'=>$financeAccountTypesID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

}
