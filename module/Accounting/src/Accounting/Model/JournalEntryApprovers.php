<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class JournalEntryApprovers
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class JournalEntryApprovers implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented JournalEntryApproversID. (Primary key)
     */
    public $journalEntryApproversID;
    
    /**
     * @var string journalEntryID.
     */
    public $journalEntryID;

    /**
     * @var date  journalEntryTypeApproversID.
     */
    public $journalEntryTypeApproversID;

    /**
     * @var int journalEntryApproversType.
     */
    public $journalEntryApproversType;
    
    /**
     * @var int journalEntryApproversStatus.
     */
    public $journalEntryApproversStatus;

    /**
     * @var int journalEntryApproversExpire.
     */
    public $journalEntryApproversExpire;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->journalEntryApproversID = (isset($data['journalEntryApproversID'])) ? $data['journalEntryApproversID'] : null;
        $this->journalEntryID = (isset($data['journalEntryID'])) ? $data['journalEntryID'] : null;
        $this->journalEntryTypeApproversID = (isset($data['journalEntryTypeApproversID'])) ? $data['journalEntryTypeApproversID'] : null;
        $this->journalEntryApproversType = (isset($data['journalEntryApproversType'])) ? $data['journalEntryApproversType'] : null;
        $this->journalEntryApproversStatus = (isset($data['journalEntryApproversStatus'])) ? $data['journalEntryApproversStatus'] : 'pending';
        $this->journalEntryApproversExpire = (isset($data['journalEntryApproversExpire'])) ? $data['journalEntryApproversExpire'] : 0;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryID',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryTypeApproversID',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryApproversExpire',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryApproversType',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryApproversStatus',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
