<?php

namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
Class IncomeNonItemProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveNonItemProduct($incDatas)
    {
        $data = array(
            'incomeID' => $incDatas->incomeID,
            'incomeNonItemProductDiscription' => $incDatas->incomeNonItemProductDiscription,
            'incomeNonItemProductQuantity' => $incDatas->incomeNonItemProductQuantity,
            'incomeNonItemProductUnitPrice' => $incDatas->incomeNonItemProductUnitPrice,
            'incomeNonItemProductTotalPrice' => $incDatas->incomeNonItemProductTotalPrice,
            'incomeNonItemProductGLAccountID' => $incDatas->incomeNonItemProductGLAccountID
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }
}