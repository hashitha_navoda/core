<?php 

namespace Accounting\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomeProductTax
{

    public $incomeProductTaxID;
    public $incomeID;
    public $incomeProductIdOrNonItemProductID;
    public $incomeTaxID;
    public $incomeTaxPrecentage;
    public $incomeTaxAmount;
    public $incomeTaxItemOrNonItemProduct;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->incomeProductTaxID = (!empty($data['incomeProductTaxID'])) ? $data['incomeProductTaxID'] : null;
        $this->incomeID = (!empty($data['incomeID'])) ? $data['incomeID'] : null;
        $this->incomeProductIdOrNonItemProductID = (!empty($data['incomeProductIdOrNonItemProductID'])) ? $data['incomeProductIdOrNonItemProductID'] : null;
        $this->incomeTaxID = (!empty($data['incomeTaxID'])) ? $data['incomeTaxID'] : null;
        $this->incomeTaxPrecentage = (!empty($data['incomeTaxPrecentage'])) ? $data['incomeTaxPrecentage'] : null;
        $this->incomeTaxAmount = (!empty($data['incomeTaxAmount'])) ? $data['incomeTaxAmount'] : null;
        $this->incomeTaxItemOrNonItemProduct = (!empty($data['incomeTaxItemOrNonItemProduct'])) ? $data['incomeTaxItemOrNonItemProduct'] : null;
    }

}