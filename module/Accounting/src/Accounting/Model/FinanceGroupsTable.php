<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

/**
 * Class financeGroupsTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Finance Groups Table Functions
 */
class FinanceGroupsTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * FinanceGroupsTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * FetchAll finance Groups 
     * @return $resultSet
     */
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeGroups')
            ->columns(array('*'))
            ->join('entity', 'financeGroups.entityID = entity.entityID', array('deleted'), 'left')
            ->join('financeGroupTypes', 'financeGroups.financeGroupTypesID = financeGroupTypes.financeGroupTypesID', array('financeGroupTypesName'), 'left')
            ->join(array('parentGroup'=>'financeGroups'), 'financeGroups.financeGroupsParentID = parentGroup.financeGroupsID', array( "parentFinanceGroupsName" => new Expression('parentGroup.financeGroupsName'), "parentFinanceGroupsShortName" => new Expression('parentGroup.financeGroupsShortName')), 'left')
            ->order(array('financeGroups.financeGroupsName' => 'DESC'))
            ->where(array('deleted' => '0'));
        
        if ($paginated) {
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * FetchAll finance Groups by group name
     * @return $resultSet
     */
    public function getFinanceGroupsByGroupsName($financeGroupsName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeGroups')
                ->columns(array('*'))
                ->join('entity', 'financeGroups.entityId = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0,'financeGroupsName'=>$financeGroupsName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveFinanceGroups(financeGroups $financeGroups)
    {	
         $data = array(
            'financeGroupsName' => $financeGroups->financeGroupsName,
            'financeGroupsShortName' => $financeGroups->financeGroupsShortName,
            'financeGroupTypesID' => $financeGroups->financeGroupTypesID,
            'financeGroupsAddress' => $financeGroups->financeGroupsAddress,
            'financeGroupsParentID' => $financeGroups->financeGroupsParentID,
            'entityID' => $financeGroups->entityID,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

     public function searchGroupsForDropDown($groupsSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeGroups')
                ->columns(array(
                	'posi' => new Expression('LEAST(IF(POSITION(\'' . $groupsSearchKey . '\' in financeGroupsName )>0,POSITION(\'' . $groupsSearchKey . '\' in financeGroupsName), 9999),'
                            . 'IF(POSITION(\'' . $groupsSearchKey . '\' in financeGroupsShortName )>0,POSITION(\'' . $groupsSearchKey . '\' in financeGroupsShortName), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(financeGroupsName ), CHAR_LENGTH(financeGroupsShortName )) '),
                	'*'
                	))
                ->join('entity', 'financeGroups.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0))
        		->where(new PredicateSet(array(new Operator('financeGroups.financeGroupsName', 'like', '%' . $groupsSearchKey . '%'), new Operator('financeGroups.financeGroupsShortName', 'like', '%' . $groupsSearchKey . '%')), PredicateSet::OP_OR))
        		->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

     public function getGroupsDataByGroupsID($groupsID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeGroups')
                ->columns(array('*'))
                ->join('entity', 'financeGroups.entityID = entity.entityID', array('deleted'), 'left')
                ->join('financeGroupTypes', 'financeGroups.financeGroupTypesID = financeGroupTypes.financeGroupTypesID', array('financeGroupTypesName'), 'left')
                ->join(array('parentGroup'=>'financeGroups'), 'financeGroups.financeGroupsParentID = parentGroup.financeGroupsID', array( "parentFinanceGroupsName" => new Expression('parentGroup.financeGroupsName'), "parentFinanceGroupsShortName" => new Expression('parentGroup.financeGroupsShortName')), 'left')
                ->where(array('entity.deleted'=>0,'financeGroups.financeGroupsID'=>$groupsID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

     public function updateFinanceGroups($data, $financeGroupsID)
    {
        try {
            $this->tableGateway->update($data, array('financeGroupsID'=>$financeGroupsID));   
            return true;
        } catch (Exception $e) {
            return flase;
        }
        
    }

    public function getFinanceGroupsByFinanceGroupParentID($financeGroupsParentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeGroups')
                ->columns(array('*'))
                ->join('entity', 'financeGroups.entityId = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0,'financeGroupsParentID'=>$financeGroupsParentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}
