<?php

namespace Accounting\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomeSubProduct
{

    public $incomeSubProductID;
    public $incomeProductID;
    public $productBatchID;
    public $productSerialID;
    public $incomeProductSubQuantity;
    public $incomeSubProductsWarranty;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->incomeSubProductID = (!empty($data['incomeSubProductID'])) ? $data['incomeSubProductID'] : null;
        $this->incomeProductID = (!empty($data['incomeProductID'])) ? $data['incomeProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->incomeProductSubQuantity = (!empty($data['incomeProductSubQuantity'])) ? $data['incomeProductSubQuantity'] : null;
        $this->incomeSubProductsWarranty = (!empty($data['incomeSubProductsWarranty'])) ? $data['incomeSubProductsWarranty'] : null;
    }
}
