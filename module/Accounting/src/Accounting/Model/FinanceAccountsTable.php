<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;


/**
 * Class financeAccountsTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Finance Accounts Table Functions
 */
class FinanceAccountsTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    static $accountLevels = 6;
    /**
     * FinanceAccountsTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

     /**
     * FetchAll finance Accounts 
     * @return $resultSet
     */
    public function fetchAll($paginated = FALSE, $accountID = NULL, $activeFlag = false, $financeAccountTypes)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array('*'))
                ->join('entity', 'financeAccounts.entityID = entity.entityID', array('deleted'), 'left')
                ->join('financeAccountClass', 'financeAccounts.financeAccountClassID = financeAccountClass.financeAccountClassID', array('financeAccountClassName', 'financeAccountTypesID'), 'left')
                ->join(array('parentAccount'=>'financeAccounts'), 'financeAccounts.financeAccountsParentID = parentAccount.financeAccountsID', array( "parentFinanceAccountsCode" => new Expression('parentAccount.financeAccountsCode'), "parentFinanceAccountsName" => new Expression('parentAccount.financeAccountsName')), 'left')
                ->order(array('financeAccounts.financeAccountsCode' => 'DESC'))
                ->where(array('deleted' => '0'));
        $select->where->notEqualTo('financeAccounts.financeAccountStatusID',20);
        if($accountID != ''){
            $select->where->in('financeAccounts.financeAccountsID', $accountID);
        }
        if($activeFlag){
            $select->where(array('financeAccounts.financeAccountStatusID' => '1'));
        }
        
        if (!empty($financeAccountTypes)) {
            $select->where->in('financeAccountClass.financeAccountTypesID', $financeAccountTypes);    
        }

        if ($paginated) {
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * FetchAll finance Accounts by account code
     * @return $resultSet
     */
    public function getFinanceAccountsByAccountsCode($financeAccountsCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array('*'))
                ->join('entity', 'financeAccounts.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0,'financeAccountsCode'=>$financeAccountsCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveFinanceAccount(financeAccounts $financeAccounts)
    {
         $data = array(
            'financeAccountClassID' => $financeAccounts->financeAccountClassID,
            'financeAccountsCode' => $financeAccounts->financeAccountsCode,
            'financeAccountsName' => $financeAccounts->financeAccountsName,
            'financeAccountsParentID' => $financeAccounts->financeAccountsParentID,
            'financeAccountStatusID' => $financeAccounts->financeAccountStatusID,
            'financeAccountsCreditAmount' => $financeAccounts->financeAccountsCreditAmount,
            'financeAccountsDebitAmount' => $financeAccounts->financeAccountsDebitAmount,
            'entityID' => $financeAccounts->entityID,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function searchAccountsForDropDown($accountsSearchKey,$activeAccountsFlag)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array(
                	'posi' => new Expression('LEAST(IF(POSITION(\'' . $accountsSearchKey . '\' in financeAccountsCode )>0,POSITION(\'' . $accountsSearchKey . '\' in financeAccountsCode), 9999),'
                            . 'IF(POSITION(\'' . $accountsSearchKey . '\' in financeAccountsName )>0,POSITION(\'' . $accountsSearchKey . '\' in financeAccountsName), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(financeAccountsCode ), CHAR_LENGTH(financeAccountsName )) '),
                	'*'
                	))
                ->join('entity', 'financeAccounts.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0))
        		->where(new PredicateSet(array(new Operator('financeAccounts.financeAccountsCode', 'like', '%' . $accountClassSearchKey . '%'), new Operator('financeAccounts.financeAccountsName', 'like', '%' . $accountClassSearchKey . '%')), PredicateSet::OP_OR))
        		->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->where->notEqualTo('financeAccountStatusID',20);
        if($activeAccountsFlag){
            $select->where(array('financeAccounts.financeAccountStatusID'=>1));
        }
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAccountsDataByAccountsID($accountsID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array('*'))
                ->join('entity', 'financeAccounts.entityID = entity.entityID', array('deleted'), 'left')
                ->join('financeAccountClass', 'financeAccounts.financeAccountClassID = financeAccountClass.financeAccountClassID', array('financeAccountClassName'), 'left')
                ->join(array('parentAccount'=>'financeAccounts'), 'financeAccounts.financeAccountsParentID = parentAccount.financeAccountsID', array( "parentFinanceAccountsCode" => new Expression('parentAccount.financeAccountsCode'), "parentFinanceAccountsName" => new Expression('parentAccount.financeAccountsName')), 'left')
                ->order(array('financeAccounts.financeAccountsCode' => 'DESC'))
                ->where(array('deleted' => '0','financeAccounts.financeAccountsID'=>$accountsID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function updateFinanceAccounts($data, $financeAccountsID)
    {
        try {
            $this->tableGateway->update($data, array('financeAccountsID'=>$financeAccountsID));   
            return true;
        } catch (Exception $e) {
            return flase;
        }
        
    }

    public function getAllActiveAccounts()
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array('*'))
                ->join('entity', 'financeAccounts.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0,'financeAccountStatusID'=>1));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAccountsByParentAccountID($parentAccountsID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array('*'))
                ->join('entity', 'financeAccounts.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0,'financeAccountsParentID'=>$parentAccountsID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAllAccountsByAccountClassID($accountClassID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array('*'))
                ->join('entity', 'financeAccounts.entityID = entity.entityID', array('deleted'), 'left')
                ->join('financeAccountClass', 'financeAccounts.financeAccountClassID = financeAccountClass.financeAccountClassID', array('financeAccountClassName','financeAccountTypesID'), 'left')
                ->join('financeAccountTypes', 'financeAccountClass.financeAccountTypesID = financeAccountTypes.financeAccountTypesID', array('financeAccountTypesName'), 'left')
                ->where(array('entity.deleted'=>0,'financeAccounts.financeAccountClassID'=>$accountClassID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAllAccountsByAccountClassTypeID($types)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array('*'))
                ->join('entity', 'financeAccounts.entityID = entity.entityID', array('deleted'), 'left')
                ->join('financeAccountClass', 'financeAccounts.financeAccountClassID = financeAccountClass.financeAccountClassID', array('financeAccountClassName','financeAccountTypesID'), 'left')
                ->join('financeAccountTypes', 'financeAccountClass.financeAccountTypesID = financeAccountTypes.financeAccountTypesID', array('financeAccountTypesName'), 'left')
                ->where(array('entity.deleted'=>0));

        $select->where->in('financeAccountClass.financeAccountTypesID', $types);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultSet = [];
        if (sizeof($results) > 0) {
           foreach ($results as $key => $value) {
                $resultSet[] = $value;
            } 
        }


        return $resultSet;
    }

     /**
     * Generate accounts array nested under each parent
     * @return array
     */
    public function getHierarchyByAccountClassID($accountClassID, $startDate = null, $endDate = null,$activeFlag = false)
    {

        $res = $this->fetchAllHierarchicalByAccountClassID($accountClassID, $activeFlag);
        $all = array();

        foreach ($res as $loo) {
            $loo->totalCreditAmount = 0.00;
            $loo->totalDebitAmount = 0.00;
            
            $data = $this->getJournalEntryTotalCreditAndDebitByAccountID($loo->financeAccountsID, $startDate, $endDate);
            if($data['journalEntryID'] != NULL){
                $loo->totalCreditAmount = $data['totalCreditAmount'];
                $loo->totalDebitAmount = $data['totalDebitAmount'];

            }

            $all[$loo->financeAccountsID] = array("data" => $loo);
        }

        foreach ($all as $key => &$loo) {
            if (isset($loo['data'])) {
                $all[$loo['data']->financeAccountsParentID]['child'][$loo['data']->financeAccountsID] = $loo;
                unset($all[$key]);
            }
        }

        return $all;
    }

    public function fetchAllHierarchicalByAccountClassID($accountClassID, $activeFlag = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = $this->_generateQueryByAccountClassID(self::$accountLevels,$accountClassID, $activeFlag);
        $rowset = $adapter->query($sql, $adapter::QUERY_MODE_EXECUTE);
        return $rowset;
    }

     /**
     * Generates query to get hierarchical data of finance accounts
     * @param int $level accounts level to dig down to
     * @return string SQL Query
     */
    private function _generateQueryByAccountClassID($level,$accountClassID, $activeFlag = false)
    {
        $queryParts = array();

        for ($i = 0; $i < $level; $i++) {
            $queryParts[] = $this->_generateQueryForLevelByAccountClassID($i,$accountClassID, $activeFlag);
        }

        $query = implode(" UNION ", $queryParts);

        $query = <<<SQL
        SELECT *, @row := @row + 1 as row
        FROM
            ({$query}) r,
            (SELECT @row := 0) v
        ORDER BY
            row DESC
SQL;

        return $query;
    }

      /**
     * Generate query to get accounts of a  Accounts level
     * @param int $level
     * @return string SQL Query
     */
    private function _generateQueryForLevelByAccountClassID($level = 0,$accountClassID, $activeFlag = false)
    {
        if($activeFlag){
            $baseQ = "SELECT financeAccounts.* FROM financeAccounts, entity WHERE financeAccounts.financeAccountsParentID = 0 AND financeAccounts.entityID = entity.entityID AND entity.deleted = 0 AND financeAccounts.financeAccountStatusID = 1 AND financeAccounts.financeAccountClassID = ".$accountClassID."";
            $wrapperQ = "SELECT financeAccounts.*  FROM financeAccounts, entity WHERE financeAccounts.financeAccountsParentID IN (__SUBQ__) AND financeAccounts.entityID = entity.entityID AND entity.deleted = 0 AND financeAccounts.financeAccountStatusID = 1 AND financeAccounts.financeAccountClassID = ".$accountClassID."";
            
        } else {
            $baseQ = "SELECT financeAccounts.* FROM financeAccounts, entity WHERE financeAccounts.financeAccountsParentID = 0 AND financeAccounts.entityID = entity.entityID AND entity.deleted = 0 AND financeAccounts.financeAccountClassID = ".$accountClassID."";
            $wrapperQ = "SELECT financeAccounts.*  FROM financeAccounts, entity WHERE financeAccounts.financeAccountsParentID IN (__SUBQ__) AND financeAccounts.entityID = entity.entityID AND entity.deleted = 0 AND financeAccounts.financeAccountClassID = ".$accountClassID."";
        }

        $genQ = $baseQ;
        for ($i = 0; $i < $level; $i++) {
            $genQ = str_replace("*", "financeAccountsID", $genQ);
            $genQ = str_replace("__SUBQ__", $genQ, $wrapperQ);
        }

        return $genQ;
    }

    public function getJournalEntryTotalCreditAndDebitByAccountID($accountID, $startDate, $endDate, $yearClosing = false, $dimensionType = null, $dimensionValue = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCreditAmount' => new Expression('SUM(journalEntryAccountsCreditAmount)'), 'totalDebitAmount' => new Expression('SUM(journalEntryAccountsDebitAmount)'), 'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID, 'journalEntry.journalEntryStatusID' => 12))
                ->order(array('journalEntryAccounts.financeAccountsID'));
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        if ($yearClosing) {
            $select->where->notEqualTo('journalEntryAccounts.yearEndCloseFlag',1);
        }
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }


    public function getJournalEntryTotalCreditAndDebitByAccountIDForAuditSheduleReport($accountID, $startDate, $endDate, $yearClosing = false, $dimensionType, $dimensionValue, $locationIDs = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCreditAmount' => new Expression('SUM(journalEntryAccountsCreditAmount)'), 'totalDebitAmount' => new Expression('SUM(journalEntryAccountsDebitAmount)'), 'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID, 'journalEntry.journalEntryStatusID' => 12))
                ->order(array('journalEntryAccounts.financeAccountsID'))
                ->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        if ($yearClosing) {
            $select->where->notEqualTo('journalEntryAccounts.yearEndCloseFlag',1);
        }
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }

        if ($locationIDs != NULL) {
            $select->where->in('journalEntry.locationID', $locationIDs);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function getJournalEntryTotalCreditAndDebitByAccountIDForTrialBalance($accountID, $startDate, $endDate, $yearClosing = false, $dimensionType = null, $dimensionValue = null, $locationIDs = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCreditAmount' => new Expression('SUM(journalEntryAccountsCreditAmount)'), 'totalDebitAmount' => new Expression('SUM(journalEntryAccountsDebitAmount)'), 'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID, 'journalEntry.journalEntryStatusID' => 12))
                ->order(array('journalEntryAccounts.financeAccountsID'))
                ->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        if ($yearClosing) {
            $select->where->notEqualTo('journalEntryAccounts.yearEndCloseFlag',1);
        }
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }

        if ($locationIDs != NULL) {
            if (is_array($locationIDs)) {
                $select->where->in('journalEntry.locationID', $locationIDs);
            } else {
                $select->where(array('journalEntry.locationID' => $locationIDs));

            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function getFinanceAccountBasicByAccountsID($accountID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array('*'))
                ->join('entity', 'financeAccounts.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0,'financeAccountsID'=>$accountID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getFinanceAccountTypeIdByFinanceAccountId($financeAccountsID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccounts')
                ->columns(array('*'))
                ->join('financeAccountClass', 'financeAccounts.financeAccountClassID = financeAccountClass.financeAccountClassID', array('financeAccountClassName','financeAccountTypesID'), 'left')
                ->where(array('financeAccounts.financeAccountsID'=>$financeAccountsID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getJournalEntryTotalCreditAndDebitByAccountIDForGeneralLedger($accountID, $startDate, $endDate, $yearClosing = false, $dimensionType, $dimensionValue)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCreditAmount' => new Expression('SUM(journalEntryAccountsCreditAmount)'), 'totalDebitAmount' => new Expression('SUM(journalEntryAccountsDebitAmount)'), 'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID, 'journalEntry.journalEntryStatusID' => 12))
                ->order(array('journalEntryAccounts.financeAccountsID'))
                ->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        if ($yearClosing) {
            $select->where->notEqualTo('journalEntryAccounts.yearEndCloseFlag',1);
        }
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }


    public function getJournalEntryTotalCreditAndDebitByAccountIDForBalanceSheet($accountID, $startDate, $endDate, $yearClosing = false, $dimensionType, $dimensionValue, $locationIDs = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCreditAmount' => new Expression('SUM(journalEntryAccountsCreditAmount)'), 'totalDebitAmount' => new Expression('SUM(journalEntryAccountsDebitAmount)'), 'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID, 'journalEntry.journalEntryStatusID' => 12))
                ->order(array('journalEntryAccounts.financeAccountsID'))
                ->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }

        if ($locationIDs != NULL) {
            if (is_array($locationIDs)) {
                $select->where->in('journalEntry.locationID', $locationIDs);
            } else {
                $select->where(array('journalEntry.locationID' => $locationIDs));

            }
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function getJournalEntryTotalCreditAndDebitByAccountIDForBalanceSheetPdfAndCsv($accountID, $startDate, $endDate, $yearClosing = false, $dimensionType = null, $dimensionValue = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCreditAmount' => new Expression('SUM(journalEntryAccountsCreditAmount)'), 'totalDebitAmount' => new Expression('SUM(journalEntryAccountsDebitAmount)'), 'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID, 'journalEntry.journalEntryStatusID' => 12))
                ->order(array('journalEntryAccounts.financeAccountsID'));
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);

        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }
}
