<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class FiscalPeriod
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class FiscalPeriod implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented fiscalPeriodID. (Primary key)
     */
    public $fiscalPeriodID;
    
    /**
     * @var string fiscalPeriodStartDate.
     */
    public $fiscalPeriodStartDate;

    /**
     * @var string  fiscalPeriodEndDate.
     */
    public $fiscalPeriodEndDate;

    /**
     * @var string  fiscalPeriodStatusID.
     */
    public $fiscalPeriodStatusID;

    /**
     * @var string  fiscalPeriodHasJournalEntries.
     */
    public $fiscalPeriodHasJournalEntries;

    /**
     * @var string  fiscalPeriodParentID.
     */
    public $fiscalPeriodParentID;
    
    /**
     * @var int entity id.
     */
    public $entityID;

    /**
     * @var int entity id.
     */
    public $isCompleteStepOne;

    /**
     * @var int entity id.
     */
    public $isCompleteStepTwo;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->fiscalPeriodID = (isset($data['fiscalPeriodID'])) ? $data['fiscalPeriodID'] : null;
        $this->fiscalPeriodStartDate = (isset($data['fiscalPeriodStartDate'])) ? $data['fiscalPeriodStartDate'] : null;
        $this->fiscalPeriodEndDate = (isset($data['fiscalPeriodEndDate'])) ? $data['fiscalPeriodEndDate'] : null;
        $this->fiscalPeriodStatusID = (isset($data['fiscalPeriodStatusID'])) ? $data['fiscalPeriodStatusID'] : null;
        $this->fiscalPeriodHasJournalEntries = (isset($data['fiscalPeriodHasJournalEntries'])) ? $data['fiscalPeriodHasJournalEntries'] : 0;
        $this->fiscalPeriodParentID = (isset($data['fiscalPeriodParentID'])) ? $data['fiscalPeriodParentID'] : null;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : null;
        $this->isCompleteStepOne = (!empty($data['isCompleteStepOne'])) ? $data['isCompleteStepOne'] : 0;
        $this->isCompleteStepTwo = (!empty($data['isCompleteStepTwo'])) ? $data['isCompleteStepTwo'] : 0;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'fiscalPeriodStartDate',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'fiscalPeriodEndDate',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'fiscalPeriodStatusID',
                        'required' => true,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
