<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class FinanceGroups
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class FinanceGroups implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented financeGroupsID. (Primary key)
     */
    public $financeGroupsID;
    
    /**
     * @var string financeGroupsName.
     */
    public $financeGroupsName;

    /**
     * @var string 	financeGroupsShortName.
     */
    public $financeGroupsShortName;

    /**
     * @var string 	financeGroupTypesID.
     */
    public $financeGroupTypesID;

    /**
     * @var string 	financeGroupsAddress.
     */
    public $financeGroupsAddress;

    /**
     * @var string 	financeGroupsParentID.
     */
    public $financeGroupsParentID;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->financeGroupsID = (isset($data['financeGroupsID'])) ? $data['financeGroupsID'] : null;
        $this->financeGroupsName = (isset($data['financeGroupsName'])) ? $data['financeGroupsName'] : null;
        $this->financeGroupsShortName = (isset($data['financeGroupsShortName'])) ? $data['financeGroupsShortName'] : null;
        $this->financeGroupTypesID = (isset($data['financeGroupTypesID'])) ? $data['financeGroupTypesID'] : null;
        $this->financeGroupsAddress = (isset($data['financeGroupsAddress'])) ? $data['financeGroupsAddress'] : null;
        $this->financeGroupsParentID = (isset($data['financeGroupsParentID'])) ? $data['financeGroupsParentID'] : null;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeGroupsName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeGroupsShortName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeGroupTypesID',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeGroupsAddress',
                        'required' => false, 
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeGroupsParentID',
                        'required' => false, 
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
