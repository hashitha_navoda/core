<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;


/**
 * Class FiscalPeriodTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains fiscalPeriod Table Functions
 */
class FiscalPeriodTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * FiscalPeriodTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * FetchAll FiscalPeriod
     * @return $resultSet
     */
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
            ->columns(array('*'))
            ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'), 'left')
            ->where(array('deleted' => '0', 'fiscalPeriodParentID' => NULL ));
        
        if ($paginated) {
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveFiscalPeriod(FiscalPeriod $fiscalPeriod)
    {   
         $data = array(
            'fiscalPeriodStartDate' => $fiscalPeriod->fiscalPeriodStartDate,
            'fiscalPeriodEndDate' => $fiscalPeriod->fiscalPeriodEndDate,
            'fiscalPeriodStatusID' => $fiscalPeriod->fiscalPeriodStatusID,
            'fiscalPeriodHasJournalEntries' => $fiscalPeriod->fiscalPeriodHasJournalEntries,
            'fiscalPeriodParentID' => $fiscalPeriod->fiscalPeriodParentID,
            'entityID' => $fiscalPeriod->entityID,
            'isCompleteStepOne' => $fiscalPeriod->isCompleteStepOne,
            'isCompleteStepTwo' => $fiscalPeriod->isCompleteStepTwo,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function updateFiscalPeriod($data, $fiscalPeriodID)
    {
    	try {
            $this->tableGateway->update($data, array('fiscalPeriodID' => $fiscalPeriodID));   
            return true;
        } catch (Exception $e) {
            return flase;
        }
    }

    public function getFiscalPeriodDataByFiscalPeriodID($fiscalPeriodID)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
                ->columns(array('*'))
                ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0, 'fiscalPeriodID' => $fiscalPeriodID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    public function getFiscalPeriodChildDataByFiscalPeriodParentID($fiscalPeriodID)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
                ->columns(array('*'))
                ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0, 'fiscalPeriodParentID' => $fiscalPeriodID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getAllFiscalPeriods()
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
                ->columns(array('*'))
                ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get valid fiscal period for given date
     * @param  date $date  '2018-01-15'
     * @return array       [
     *                         'fiscalPeriodStartDate' => '2018-01-01',
     *                         'fiscalPeriodEndDate' => '2018-12-31',
     *                         ....
     *                     ]
     */
    public function getValidFiscalPeriod($date)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
                ->columns(array('*'))
                ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'))
                 ->where(array(
                    'fiscalPeriod.fiscalPeriodStatusID' => 14,      // 14 = unlocked
                    'entity.deleted' => 0)
                )
                ->where->greaterThanOrEqualTo('fiscalPeriodEndDate', $date)
                ->AND->lessThanOrEqualTo('fiscalPeriodStartDate', $date);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if ($result->count()) {
            return $result->current();
        }

        return false;
    }
    public function getAllUnlockedFiscalPeriods($unlock = true)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
                ->columns(array('*'))
                ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'))               
                ->where(array('entity.deleted' => 0))
                ->where(array('fiscalPeriod.fiscalPeriodParentID' => null));
        if ($unlock) {
            $select->where->notEqualTo("fiscalPeriod.fiscalPeriodStatusID", 11);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updateFiscalPeriodByParentId($data, $fiscalPeriodID)
    {
        try {
            $this->tableGateway->update($data, array('fiscalPeriodParentID' => $fiscalPeriodID));   
            return true;
        } catch (Exception $e) {
            return flase;
        }
    }

    public function getAllFiscalPeriodsForYearEndClosing()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
            ->columns(array('*'))
            ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'), 'left')
            ->where(array('deleted' => '0', 'fiscalPeriodParentID' => NULL ))
            ->order(array('fiscalPeriod.fiscalPeriodStartDate ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getNextFiscalPeriodsForYearEndClosing($fiscalPeriodEndDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
            ->columns(array('*'))
            ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'), 'left')
            ->where(array('deleted' => '0', 'fiscalPeriodParentID' => NULL, 'fiscalPeriodStatusID' => 14))
            ->order(array('fiscalPeriod.fiscalPeriodStartDate ASC'))
            ->where->greaterThan('fiscalPeriod.fiscalPeriodStartDate',$fiscalPeriodEndDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPreviousFiscalPeriods($date, $parentFlag = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
            ->columns(array('*'))
            ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'), 'left')
            ->where(array('deleted' => '0'))
            ->order(array('fiscalPeriod.fiscalPeriodStartDate DESC'))
            ->where->lessThan('fiscalPeriod.fiscalPeriodEndDate',$date);

        if ($parentFlag) {
            $select->where(array('fiscalPeriodParentID' => NULL));    
        } else {
            $select->where->isNotNull('fiscalPeriodParentID');
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getEarliestFiscalPeriod()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
                ->columns(array('*'))
                ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0, 'fiscalPeriodParentID' => NULL))
                ->order(array('fiscalPeriod.fiscalPeriodStartDate ASC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }
    public function fetchAllByAsc () {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
            ->columns(array('*'))
            ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'), 'left')
            ->where(array('deleted' => '0', 'fiscalPeriodParentID' => NULL ))
            ->order(array('fiscalPeriod.fiscalPeriodStartDate ASC'));
        

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;

    }
    public function getFiscalPeriodsByParentId ($parentId) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('fiscalPeriod')
            ->columns(array('*'))
            ->join('entity', 'fiscalPeriod.entityID = entity.entityID', array('deleted'), 'left')
            ->where(array('deleted' => '0', 'fiscalPeriodParentID' => $parentId ))
            ->order(array('fiscalPeriod.fiscalPeriodStartDate ASC'));
        

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}
