<?php

namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
Class BudgetTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('budget');
        $select->join('fiscalPeriod', 'budget.fiscalPeriodID = fiscalPeriod.fiscalPeriodID', array('fiscalPeriodStartDate','fiscalPeriodEndDate','fiscalPeriodStatusID'), 'left');
        $select->join('dimension', 'budget.dimension = dimension.dimensionId', array('dimensionName'), 'left');
        $select->join('dimensionValues', 'budget.dimensionValue = dimensionValues.dimensionValueId', array('dimensionValueName' => new Expression('dimensionValues.dimensionValue')), 'left');
        $select->join('job', 'budget.dimensionValue = job.jobId', array('jobName','jobReferenceNumber'), 'left');
        $select->join('project', 'budget.dimensionValue = project.projectId', array('projectCode','projectName'), 'left');
        $select->where(array('budget.deleted' => 0));
        if ($paginated) {
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveBudget($budgetData)
    {
        $budgetData = array(
            'budgetName' => $budgetData->budgetName,
            'fiscalPeriodID' => $budgetData->fiscalPeriodID,
            'dimension' => $budgetData->dimension,
            'dimensionValue' => $budgetData->dimensionValue,
            'interval' => $budgetData->interval,
            'deleted' => $budgetData->deleted,
        );
        if ($this->tableGateway->insert($budgetData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateBudgetAsDeleted($budgetId)
    {
        $budgetData = array(
            'deleted' => 1,
        );

        try {
            $this->tableGateway->update($budgetData, array('budgetId' => $budgetId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    public function getBudgetByFiscalPeriodID($fiscalPeriodID, $dimensionType = null, $dimensionValue = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('budget')
                ->columns(array('*'))
                ->where(array('deleted' => 0, 'fiscalPeriodID' => $fiscalPeriodID));

        if ($dimensionType != "" && $dimensionValue != "") {
            $select->where(array('dimension' => $dimensionType, 'dimensionValue' => $dimensionValue));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function budgetSearchByKey($searchKey = null, $fiscalPeriodID = null) {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('budget');
        $select->join('fiscalPeriod', 'budget.fiscalPeriodID = fiscalPeriod.fiscalPeriodID', array('fiscalPeriodStartDate','fiscalPeriodEndDate','fiscalPeriodStatusID'), 'left');
        $select->join('dimension', 'budget.dimension = dimension.dimensionId', array('dimensionName'), 'left');
        $select->join('dimensionValues', 'budget.dimensionValue = dimensionValues.dimensionValueId', array('dimensionValueName' => new Expression('dimensionValues.dimensionValue')), 'left');
        $select->where(array('budget.deleted' => 0));

        if ($searchKey != "") {
            $select->where(new PredicateSet(array(new Operator('budget.budgetName', 'like', '%' . $searchKey . '%'))));
        }

        if ($fiscalPeriodID != "0") {
            $select->where(new PredicateSet(array(new Operator('budget.fiscalPeriodID', '=', $fiscalPeriodID))));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getBudgetByBudgetID($budgetId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('budget')
                ->columns(array('*'))
                ->join('budgetDetails', 'budget.budgetId = budgetDetails.budgetId', array('subFiscalPeriodID' => new Expression('budgetDetails.fiscalPeriodID'),'financeAccountID','budgetValue','budgetDetailsId'), 'left')
                ->where(array('budget.budgetId' => $budgetId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}