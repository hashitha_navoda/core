<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class JournalEntryApproversTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Journal Entry Approvers Table Functions
 */
class JournalEntryApproversTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * JournalEntryApproversTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveJournalEntryApprovers(JournalEntryApprovers $journalEntryApprovers)
    {   
         $data = array(
            'journalEntryID' => $journalEntryApprovers->journalEntryID,
            'journalEntryTypeApproversID' => $journalEntryApprovers->journalEntryTypeApproversID,
            'journalEntryApproversType' => $journalEntryApprovers->journalEntryApproversType,
            'journalEntryApproversStatus' => $journalEntryApprovers->journalEntryApproversStatus,
            'journalEntryApproversExpire' => $journalEntryApprovers->journalEntryApproversExpire,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function getJournalEntryApprovers($journalEntryID, $journalEntryTypeApproversID, $journalEntryApproversType)
    {
    	$rowset = $this->tableGateway->select(array('journalEntryID' => $journalEntryID, 'journalEntryTypeApproversID' => $journalEntryTypeApproversID, 'journalEntryApproversType' => $journalEntryApproversType, 'journalEntryApproversExpire' => 0));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function updateJournalEntryApprovers($data,$journalEntryApproversID)
    {
    	try {
    		$this->tableGateway->update($data,array('journalEntryApproversID' => $journalEntryApproversID));
    		return true;
    	} catch (Exception $e) {
    		return false;	
    	}
        
    }

    public function getJournalEntryApproversByJournalEntryIDAndType($journalEntryID, $journalEntryApproversType)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntryApprovers')
                ->columns(array('*'))
                ->join('journalEntryTypeApprovers', 'journalEntryApprovers.journalEntryTypeApproversID = journalEntryTypeApprovers.journalEntryTypeApproversID', array('employeeID'), 'left')
                ->join('employee', 'journalEntryTypeApprovers.employeeID = employee.employeeID', array('*'), 'left')
                ->where(array('journalEntryApprovers.journalEntryID'=>$journalEntryID, 'journalEntryApprovers.journalEntryApproversType' => $journalEntryApproversType, 'journalEntryApprovers.journalEntryApproversExpire' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
        
    }

    
    
}

