<?php

namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
Class IncomeProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

     public function saveIncomeProductTax($incmeProductTax)
    {
        $data = array(
            'incomeID' => $incmeProductTax->incomeID,
            'incomeProductIdOrNonItemProductID' => $incmeProductTax->incomeProductIdOrNonItemProductID,
            'incomeTaxID' => $incmeProductTax->incomeTaxID,
            'incomeTaxPrecentage' => $incmeProductTax->incomeTaxPrecentage,
            'incomeTaxAmount' => $incmeProductTax->incomeTaxAmount,
            'incomeTaxItemOrNonItemProduct' => $incmeProductTax->incomeTaxItemOrNonItemProduct,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    // public function getPaymentVoucherProductTaxByPVPID($paymentVoucherProductID, $paymentVoucherProductType)
    // {
    //     $sql = new Sql($this->tableGateway->getAdapter());
    //     $select = new Select();
    //     $select->from("paymentVoucherProductTax")
    //             ->columns(array("*"))
    //             ->where(array('paymentVoucherProductTax.paymentVoucherProductIdOrNonItemProductID' => $paymentVoucherProductID, 'paymentVoucherProductTax.paymentVoucherTaxItemOrNonItemProduct' => $paymentVoucherProductType));
    //     $statement = $sql->prepareStatementForSqlObject($select);
    //     $results = $statement->execute();
    //     if (!$results) {
    //         return null;
    //     }
    //     return $results;
    // }
}