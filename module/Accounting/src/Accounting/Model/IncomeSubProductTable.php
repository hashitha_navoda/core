<?php

namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class IncomeSubProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveIncomeSubProduct($incomeSubProduct)
    {
        $data = array(
            'incomeProductID' => $incomeSubProduct->incomeProductID,
            'productBatchID' => $incomeSubProduct->productBatchID,
            'productSerialID' => $incomeSubProduct->productSerialID,
            'incomeProductSubQuantity' => $incomeSubProduct->incomeProductSubQuantity,
            'incomeSubProductsWarranty' => $incomeSubProduct->incomeSubProductsWarranty,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }
}
