<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class GlAccountSetup
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class GlAccountSetup implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented GlAccountSetupD. (Primary key)
     */
    public $glAccountSetupID;
    
    /**
     * @var string glAccountSetupItemDefaultSalesAccountID.
     */
    public $glAccountSetupItemDefaultSalesAccountID;

    /**
     * @var string  glAccountSetupItemDefaultInventoryAccountID.
     */
    public $glAccountSetupItemDefaultInventoryAccountID;

    /**
     * @var string  glAccountSetupItemDefaultCOGSAccountID.
     */
    public $glAccountSetupItemDefaultCOGSAccountID;

    /**
     * @var string  glAccountSetupItemDefaultAdjusmentAccountID.
     */
    public $glAccountSetupItemDefaultAdjusmentAccountID;

    /**
     * @var string  glAccountSetupSalesAndCustomerReceivableAccountID.
     */
    public $glAccountSetupSalesAndCustomerReceivableAccountID;

    /**
     * @var string  glAccountSetupSalesAndCustomerSalesAccountID.
     */
    public $glAccountSetupSalesAndCustomerSalesAccountID;

     /**
     * @var string  glAccountSetupSalesAndCustomerSalesDiscountAccountID.
     */
    public $glAccountSetupSalesAndCustomerSalesDiscountAccountID;

     /**
     * @var string  glAccountSetupSalesAndCustomerAdvancePaymentAccountID.
     */
    public $glAccountSetupSalesAndCustomerAdvancePaymentAccountID;

     /**
     * @var string  glAccountSetupPurchasingAndSupplierPayableAccountID.
     */
    public $glAccountSetupPurchasingAndSupplierPayableAccountID;

    /**
     * @var string  glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID.
     */
    public $glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID;

     /**
     * @var string  glAccountSetupPurchasingAndSupplierGrnClearingAccountID.
     */
    public $glAccountSetupPurchasingAndSupplierGrnClearingAccountID;

     /**
     * @var string  glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID.
     */
    public $glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID;

    /**
     * @var string  glAccountSetupGeneralExchangeVarianceAccountID.
     */
    public $glAccountSetupGeneralExchangeVarianceAccountID;

     /**
     * @var string  glAccountSetupGeneralProfitAndLostYearAccountID.
     */
    public $glAccountSetupGeneralProfitAndLostYearAccountID;

     /**
     * @var string  glAccountSetupGeneralBankChargersAccountID.
     */
    public $glAccountSetupGeneralBankChargersAccountID;

    /**
     * @var string  glAccountSetupGeneralDeliveryChargersAccountID.
     */
    public $glAccountSetupGeneralDeliveryChargersAccountID;

    /**
     * @var string  glAccountSetupGeneralLoyaltyExpenseAccountID.
     */
    public $glAccountSetupGeneralLoyaltyExpenseAccountID;
    public $glAccountSetupItemDefaultDepreciationAccountID;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->glAccountSetupID = (!empty($data['glAccountSetupID'])) ? $data['glAccountSetupID'] : null;
        $this->glAccountSetupItemDefaultSalesAccountID = (!empty($data['glAccountSetupItemDefaultSalesAccountID'])) ? $data['glAccountSetupItemDefaultSalesAccountID'] : null;
        $this->glAccountSetupItemDefaultInventoryAccountID = (!empty($data['glAccountSetupItemDefaultInventoryAccountID'])) ? $data['glAccountSetupItemDefaultInventoryAccountID'] : null;
        $this->glAccountSetupItemDefaultCOGSAccountID = (!empty($data['glAccountSetupItemDefaultCOGSAccountID'])) ? $data['glAccountSetupItemDefaultCOGSAccountID'] : null;
        $this->glAccountSetupItemDefaultAdjusmentAccountID = (!empty($data['glAccountSetupItemDefaultAdjusmentAccountID'])) ? $data['glAccountSetupItemDefaultAdjusmentAccountID'] : null;
        $this->glAccountSetupSalesAndCustomerReceivableAccountID = (!empty($data['glAccountSetupSalesAndCustomerReceivableAccountID'])) ? $data['glAccountSetupSalesAndCustomerReceivableAccountID'] : null;
        $this->glAccountSetupSalesAndCustomerSalesAccountID = (!empty($data['glAccountSetupSalesAndCustomerSalesAccountID'])) ? $data['glAccountSetupSalesAndCustomerSalesAccountID'] : null;
        $this->glAccountSetupSalesAndCustomerSalesDiscountAccountID = (!empty($data['glAccountSetupSalesAndCustomerSalesDiscountAccountID'])) ? $data['glAccountSetupSalesAndCustomerSalesDiscountAccountID'] : null;
        $this->glAccountSetupSalesAndCustomerAdvancePaymentAccountID = (!empty($data['glAccountSetupSalesAndCustomerAdvancePaymentAccountID'])) ? $data['glAccountSetupSalesAndCustomerAdvancePaymentAccountID'] : null;
        $this->glAccountSetupPurchasingAndSupplierPayableAccountID = (!empty($data['glAccountSetupPurchasingAndSupplierPayableAccountID'])) ? $data['glAccountSetupPurchasingAndSupplierPayableAccountID'] : null;
        $this->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID = (!empty($data['glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID'])) ? $data['glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID'] : null;
        $this->glAccountSetupPurchasingAndSupplierGrnClearingAccountID = (!empty($data['glAccountSetupPurchasingAndSupplierGrnClearingAccountID'])) ? $data['glAccountSetupPurchasingAndSupplierGrnClearingAccountID'] : null;
        $this->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID = (!empty($data['glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID'])) ? $data['glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID'] : null;
        $this->glAccountSetupGeneralExchangeVarianceAccountID = (!empty($data['glAccountSetupGeneralExchangeVarianceAccountID'])) ? $data['glAccountSetupGeneralExchangeVarianceAccountID'] : null;
        $this->glAccountSetupGeneralProfitAndLostYearAccountID = (!empty($data['glAccountSetupGeneralProfitAndLostYearAccountID'])) ? $data['glAccountSetupGeneralProfitAndLostYearAccountID'] : null;
        $this->glAccountSetupGeneralBankChargersAccountID = (!empty($data['glAccountSetupGeneralBankChargersAccountID'])) ? $data['glAccountSetupGeneralBankChargersAccountID'] : null;
        $this->glAccountSetupGeneralDeliveryChargersAccountID = (!empty($data['glAccountSetupGeneralDeliveryChargersAccountID'])) ? $data['glAccountSetupGeneralDeliveryChargersAccountID'] : null;
        $this->glAccountSetupGeneralLoyaltyExpenseAccountID = (!empty($data['glAccountSetupGeneralLoyaltyExpenseAccountID'])) ? $data['glAccountSetupGeneralLoyaltyExpenseAccountID'] : null;
        $this->glAccountSetupItemDefaultDepreciationAccountID = (!empty($data['glAccountSetupItemDefaultDepreciationAccountID'])) ? $data['glAccountSetupItemDefaultDepreciationAccountID'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupItemDefaultSalesAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupItemDefaultInventoryAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupItemDefaultCOGSAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupItemDefaultAdjusmentAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupSalesAndCustomerReceivableAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupSalesAndCustomerSalesAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupSalesAndCustomerSalesDiscountAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupSalesAndCustomerAdvancePaymentAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupPurchasingAndSupplierPayableAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupPurchasingAndSupplierGrnClearingAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupGeneralExchangeVarianceAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupGeneralProfitAndLostYearAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupGeneralBankChargersAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupGeneralDeliveryChargersAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupGeneralLoyaltyExpenseAccountID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'glAccountSetupItemDefaultDepreciationAccountID',
                        'required' => false,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
