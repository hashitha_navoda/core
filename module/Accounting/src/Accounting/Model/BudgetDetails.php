<?php 

namespace Accounting\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BudgetDetails
{

    public $budgetDetailsId;
    public $budgetId;
    public $fiscalPeriodID;
    public $financeAccountID;
    public $budgetValue;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->budgetDetailsId = (!empty($data['budgetDetailsId'])) ? $data['budgetDetailsId'] : null;
        $this->budgetId = (!empty($data['budgetId'])) ? $data['budgetId'] : null;
        $this->fiscalPeriodID = (!empty($data['fiscalPeriodID'])) ? $data['fiscalPeriodID'] : null;
        $this->financeAccountID = (!empty($data['financeAccountID'])) ? $data['financeAccountID'] : null;
        $this->budgetValue = (!empty($data['budgetValue'])) ? $data['budgetValue'] : null;
    }

}