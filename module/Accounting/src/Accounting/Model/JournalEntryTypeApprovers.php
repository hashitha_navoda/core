<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class JournalEntryTypeApprovers
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class JournalEntryTypeApprovers implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented JournalEntryTypeApproversID. (Primary key)
     */
    public $journalEntryTypeApproversID;
    
    /**
     * @var string journalEntryTypeID.
     */
    public $journalEntryTypeID;

    /**
     * @var date  employeeID.
     */
    public $employeeID;

    /**
     * @var int journalEntryTypeApproversMinAmount.
     */
    public $journalEntryTypeApproversMinAmount;
    
    /**
     * @var int journalEntryTypeApproversMaxAmount.
     */
    public $journalEntryTypeApproversMaxAmount;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->journalEntryTypeApproversID = (isset($data['journalEntryTypeApproversID'])) ? $data['journalEntryTypeApproversID'] : null;
        $this->journalEntryTypeID = (isset($data['journalEntryTypeID'])) ? $data['journalEntryTypeID'] : null;
        $this->employeeID = (isset($data['employeeID'])) ? $data['employeeID'] : null;
        $this->journalEntryTypeApproversMinAmount = (isset($data['journalEntryTypeApproversMinAmount'])) ? $data['journalEntryTypeApproversMinAmount'] : 0.00;
        $this->journalEntryTypeApproversMaxAmount = (isset($data['journalEntryTypeApproversMaxAmount'])) ? $data['journalEntryTypeApproversMaxAmount'] : 0.00;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'employeeID',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryTypeApproversMinAmount',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryTypeApproversMaxAmount',
                        'required' => false,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
