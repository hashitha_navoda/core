<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class JournalEntryTypeApproversTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Journal Entry Type Approvers Table Functions
 */
class JournalEntryTypeApproversTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * JournalEntryTypeApproversTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveJournalEntryTypeApprovers(JournalEntryTypeApprovers $journalEntryTypeApprovers)
    {   
         $data = array(
            'journalEntryTypeID' => $journalEntryTypeApprovers->journalEntryTypeID,
            'employeeID' => $journalEntryTypeApprovers->employeeID,
            'journalEntryTypeApproversMinAmount' => $journalEntryTypeApprovers->journalEntryTypeApproversMinAmount,
            'journalEntryTypeApproversMaxAmount' => $journalEntryTypeApprovers->journalEntryTypeApproversMaxAmount,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function getJournalEntryTypeApproversByJournalEntryTypeID($journalEntryTypeID)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntryTypeApprovers')
                ->columns(array('*'))
                ->join('approvers', 'journalEntryTypeApprovers.employeeID = approvers.approverID', array('firstName','email'), 'left')
                ->where(array('journalEntryTypeApprovers.journalEntryTypeID' => $journalEntryTypeID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function deleteJournalEntryTypeApproversByJournalEntryTypeID($journalEntryTypeID)
    {
    	try {
    		$this->tableGateway->delete(array('journalEntryTypeID' => $journalEntryTypeID));
    		return true;
    	} catch (Exception $e) {
    		return false;
    	}
    }
}

