<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

/**
 * Class JournalEntryTypeTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Journal Entry Type Table Functions
 */
class JournalEntryTypeTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * JournalEntryTypeTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * FetchAll Journal Entry type
     * @return $resultSet
     */
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntryType')
            ->columns(array('*'))
            ->join('entity', 'journalEntryType.entityID = entity.entityID', array('deleted'), 'left')
            ->order(array('journalEntryType.journalEntryTypeName' => 'DESC'))
            ->where(array('deleted' => '0'));
        
        if ($paginated) {
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveJournalEntryType(JournalEntryType $journalEntryType)
    {   
         $data = array(
            'journalEntryTypeName' => $journalEntryType->journalEntryTypeName,
            'journalEntryTypeApprovedStatus' => $journalEntryType->journalEntryTypeApprovedStatus,
            'entityID' => $journalEntryType->entityID,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function updateJournalEntryType($journalEntryTypeData, $journalEntryTypeID)
    {   
    	try {
			$this->tableGateway->update($journalEntryTypeData,array('journalEntryTypeID' => $journalEntryTypeID));
			return true;    		
    	} catch (Exception $e) {
    		return false;
    	}
    }

    public function getJournalEntryTypeByJournalEntryTypeName($journalEntryTypeName)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntryType')
                ->columns(array('*'))
                ->join('entity', 'journalEntryType.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('journalEntryType.journalEntryTypeName' => $journalEntryTypeName))
                ->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

      public function searchJournalEntryTypeForDropDown($journalEntryTypeSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntryType')
                ->columns(array(
                	'posi' => new Expression('POSITION(\'' . $journalEntryTypeSearchKey . '\' in journalEntryTypeName )>0,POSITION(\'' . $journalEntryTypeSearchKey . '\' in journalEntryTypeName), 9999'),
                    'len' => new Expression('CHAR_LENGTH(journalEntryTypeName )'),
                	'*'
                	))
                ->join('entity', 'journalEntryType.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
        		->where(new PredicateSet(array(new Operator('journalEntryType.journalEntryTypeName', 'like', '%' . $journalEntryTypeSearchKey . '%')), PredicateSet::OP_OR))
        		->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryTypeDataByJournalEntryTypeID($journalEntryTypeID)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntryType')
                ->columns(array('*'))
                ->join('entity', 'journalEntryType.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0,'journalEntryType.journalEntryTypeID' => $journalEntryTypeID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }
}
