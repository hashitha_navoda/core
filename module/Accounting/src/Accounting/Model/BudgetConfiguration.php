<?php 

namespace Accounting\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class BudgetConfiguration
{

    public $budgetConfigurationId;
    public $isNotificationEnabled;
    public $isBlockedEnabled;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->budgetConfigurationId = (!empty($data['budgetConfigurationId'])) ? $data['budgetConfigurationId'] : null;
        $this->isNotificationEnabled = (!empty($data['isNotificationEnabled'])) ? $data['isNotificationEnabled'] : null;
        $this->isBlockedEnabled = (!empty($data['isBlockedEnabled'])) ? $data['isBlockedEnabled'] : null;
    }

}