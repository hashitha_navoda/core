<?php

namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class BudgetConfigurationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('budgetConfiguration');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    public function saveBudgetConfiguration($confData)
    {
        $confData = array(
            'isNotificationEnabled' => ($confData->isNotificationEnabled == "true") ? 1 : 0,
            'isBlockedEnabled' => ($confData->isBlockedEnabled == "true") ? 1 : 0,
        );
        if ($this->tableGateway->insert($confData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateBudgetConfiguration($budgetConfigurationId, $confData)
    {
        $confData = array(
            'isNotificationEnabled' => ($confData->isNotificationEnabled == "true") ? 1 : 0,
            'isBlockedEnabled' => ($confData->isBlockedEnabled == "true") ? 1 : 0,
        );

        try {
            $this->tableGateway->update($confData, array('budgetConfigurationId' => $budgetConfigurationId));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }
}