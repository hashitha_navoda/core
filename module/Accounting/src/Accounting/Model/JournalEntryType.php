<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class JournalEntryType
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class JournalEntryType implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented JournalEntryTypeID. (Primary key)
     */
    public $journalEntryTypeID;
    
    /**
     * @var string journalEntryTypeName.
     */
    public $journalEntryTypeName;

    /**
     * @var date  journalEntryTypeApprovedStatus.
     */
    public $journalEntryTypeApprovedStatus;

    /**
     * @var int entityID.
     */
    public $entityID;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->journalEntryTypeID = (isset($data['journalEntryTypeID'])) ? $data['journalEntryTypeID'] : null;
        $this->journalEntryTypeName = (isset($data['journalEntryTypeName'])) ? $data['journalEntryTypeName'] : null;
        $this->journalEntryTypeApprovedStatus = (isset($data['journalEntryTypeApprovedStatus'])) ? $data['journalEntryTypeApprovedStatus'] : 0;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryTypeName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'journalEntryTypeApprovedStatus',
                        'required' => false,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
