<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\ResultSet\ResultSet;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;


/**
 * Class financeAccountClassTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Finance Account Class Table Functions
 */
class FinanceAccountClassTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * FinanceAccountClassTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

     /**
     * FetchAll finance AccountClass 
     * @return $resultSet
     */
    public function fetchAll($paginated = FALSE)
    {
         if ($paginated) {
            $select = new Select();
            $select->from('financeAccountClass')
                    ->columns(array('*'))
                    ->join('entity', 'financeAccountClass.entityID = entity.entityID', array('deleted'), 'left')
                    ->join('financeAccountTypes', 'financeAccountClass.financeAccountTypesID = financeAccountTypes.financeAccountTypesID', array('financeAccountTypesName'), 'left')
                    ->order(array('financeAccountClassID' => 'DESC'))
                    ->where(array('deleted' => '0'));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * FetchAll finance AccountClass by account class name
     * @return $resultSet
     */
    public function getFinanceAccountClassByClassName($financeAccountClassName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccountClass')
                ->columns(array('*'))
                ->join('entity', 'financeAccountClass.entityId = entity.entityID', array('deleted'))
                ->where(array('entity.deleted' => 0,'financeAccountClassName'=>$financeAccountClassName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveFinanceAccountClass(financeAccountClass $financeAccountClass)
    {
         $data = array(
            'financeAccountClassName' => $financeAccountClass->financeAccountClassName,
            'financeAccountTypesID' => $financeAccountClass->financeAccountTypesID,
            'entityID' => $financeAccountClass->entityID,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function searchAccountClassesForDropDown($accountClassSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccountClass')
                ->columns(array('*'))
                ->join('entity', 'financeAccountClass.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('financeAccountClassID' => 'DESC'))
                ->where(array('entity.deleted'=>0));
        $select->where->like('financeAccountClassName', '%' . $accountClassSearchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAccountClassDataByAccountClassID($accountClassID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccountClass')
                ->columns(array('*'))
                ->join('entity', 'financeAccountClass.entityID = entity.entityID', array('deleted'), 'left')
                ->join('financeAccountTypes', 'financeAccountClass.financeAccountTypesID = financeAccountTypes.financeAccountTypesID', array('financeAccountTypesID','financeAccountTypesName'), 'left')
                ->where(array('entity.deleted'=>0,'financeAccountClassID'=>$accountClassID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function updateFinanceAccountClass($data, $financeAccountClassID)
    {
        try {
            $this->tableGateway->update($data, array('financeAccountClassID'=>$financeAccountClassID));   
            return true;
        } catch (Exception $e) {
            return flase;
        }
        
    }

    public function getAllActiveAccountClasses()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccountClass')
                ->columns(array('*'))
                ->join('financeAccountTypes', 'financeAccountClass.financeAccountTypesID = financeAccountTypes.financeAccountTypesID', array('financeAccountTypesName'), 'left')
                ->join('entity', 'financeAccountClass.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted'=>0))
                ->order(array('financeAccountTypesID'  => 'AESC'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAllActiveAccountClassesByAccountTypeID($financeAccountTypeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccountClass')
                ->columns(array('*'))
                ->join('financeAccountTypes', 'financeAccountClass.financeAccountTypesID = financeAccountTypes.financeAccountTypesID', array('financeAccountTypesName'), 'left')
                ->join('entity', 'financeAccountClass.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0, 'financeAccountClass.financeAccountTypesID' => $financeAccountTypeID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getAllActiveAccountClassesByAccountTypeIDForReportClassification($financeAccountTypeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('financeAccountClass')
                ->columns(array('*'))
                ->join('financeAccountTypes', 'financeAccountClass.financeAccountTypesID = financeAccountTypes.financeAccountTypesID', array('financeAccountTypesName'), 'left')
                ->join('entity', 'financeAccountClass.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0));
        $select->where->in('financeAccountClass.financeAccountTypesID', $financeAccountTypeID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
