<?php 

namespace Accounting\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Budget
{

    public $budgetId;
    public $budgetName;
    public $fiscalPeriodID;
    public $dimension;
    public $dimensionValue;
    public $interval;
    public $deleted;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->budgetId = (!empty($data['budgetId'])) ? $data['budgetId'] : null;
        $this->budgetName = (!empty($data['budgetName'])) ? $data['budgetName'] : null;
        $this->fiscalPeriodID = (!empty($data['fiscalPeriodID'])) ? $data['fiscalPeriodID'] : null;
        $this->dimension = (!empty($data['dimension'])) ? $data['dimension'] : null;
        $this->dimensionValue = (!empty($data['dimensionValue'])) ? $data['dimensionValue'] : null;
        $this->interval = (!empty($data['interval'])) ? $data['interval'] : null;
        $this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : 0;
    }

}