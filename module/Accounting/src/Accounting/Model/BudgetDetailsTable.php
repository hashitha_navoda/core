<?php

namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

Class BudgetDetailsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('budgetDetails');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function saveBudgetDetails($budgetData)
    {
        $budgetData = array(
            'budgetId' => $budgetData->budgetId,
            'fiscalPeriodID' => $budgetData->fiscalPeriodID,
            'financeAccountID' => $budgetData->financeAccountID,
            'budgetValue' => $budgetData->budgetValue
        );
        if ($this->tableGateway->insert($budgetData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getBudgetDetailsByFiscalPeriodIDAndFinanceAcID($fiscalPeriodID, $financeAccountID, $budgetId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('budgetDetails')
                ->columns(array('*'))
                ->where(array('financeAccountID' => $financeAccountID, 'fiscalPeriodID' => $fiscalPeriodID, 'budgetId' => $budgetId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}