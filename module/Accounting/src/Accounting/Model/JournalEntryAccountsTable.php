<?php
namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

/**
 * Class JournalEntryAccountsTable
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 *
 * This file contains Journal Entry Accounts Table Functions
 */
class JournalEntryAccountsTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * JournalEntryAccountsTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveJournalEntryAccounts(JournalEntryAccounts $journalEntryAccounts)
    {   
         $data = array(
            'journalEntryID' => $journalEntryAccounts->journalEntryID,
            'financeAccountsID' => $journalEntryAccounts->financeAccountsID,
            'financeGroupsID' => $journalEntryAccounts->financeGroupsID,
            'journalEntryAccountsDebitAmount' => $journalEntryAccounts->journalEntryAccountsDebitAmount,
            'journalEntryAccountsCreditAmount' => $journalEntryAccounts->journalEntryAccountsCreditAmount,
            'journalEntryAccountsMemo' => $journalEntryAccounts->journalEntryAccountsMemo,
            'yearEndCloseFlag' => $journalEntryAccounts->yearEndCloseFlag,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function getJournalEntryAccountsByJournalEntryID($journalEntryID)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntryAccounts')
                ->columns(array('*'))
                ->join('financeAccounts', 'journalEntryAccounts.financeAccountsID = financeAccounts.financeAccountsID', array('financeAccountsCode','financeAccountsName'), 'left')
                ->join('financeGroups', 'journalEntryAccounts.financeGroupsID = financeGroups.financeGroupsID', array('financeGroupsName'), 'left')
                ->where(array('journalEntryAccounts.journalEntryID'=>$journalEntryID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryAccountsByAccountID($accountID)
    {
    	$adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryTotalCreditAndDebitByAccountID($accountID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCreditAmount' => new Expression('SUM(journalEntryAccountsCreditAmount)'), 'totalDebitAmount' => new Expression('SUM(journalEntryAccountsDebitAmount)'), 'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID))
                ->order(array('journalEntryAccounts.financeAccountsID'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryAccountsByAccountIDAndDateRange($accountID, $startDate = null, $endDate, $yearClosing = false, $dimensionType = null, $dimensionValue = null, $isOrderByDate = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID));
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        if ($yearClosing) {
            $select->where->notEqualTo('journalEntryAccounts.yearEndCloseFlag',1);
        }
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }

        if ($isOrderByDate) {
            $select->order(array('journalEntry.journalEntryDate'));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryAccountsByAccountIDAndDateRangeForTrialBalanceSheet($accountID, $startDate = null, $endDate, $yearClosing = false, $dimensionType = null, $dimensionValue = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID));
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        $select->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
        if ($yearClosing) {
            $select->where->notEqualTo('journalEntryAccounts.yearEndCloseFlag',1);
        }
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateJournalEntryAccounts($data, $journalEntryAccountsID)
    {
        try {
            $this->tableGateway->update($data, array('journalEntryAccountsID'=>$journalEntryAccountsID));   
            return true;
        } catch (Exception $e) {
            return flase;
        }
        
    }

     public function updateJournalEntryAccountByJeAndFinanceAccount($data,$financeAccountsID, $journalEntryID)
    {
        try {
            $this->tableGateway->update($data,array('financeAccountsID' => $financeAccountsID, 'journalEntryID' => $journalEntryID, 'journalEntryAccountsDebitAmount' => 0));
            return true;
        } catch (Exception $e) {
            return false;   
        }
        
    }

    public function getCustomerPaymentJournalEntryAccountsByAccountIDAndDateRange($accountID, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('incomingPayment', 'incomingPayment.incomingPaymentID = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left')
                ->join('incomingPaymentMethodBankTransfer', 'incomingPaymentMethod.incomingPaymentMethodBankTransferId = incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId', array('*'), 'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left')
                ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('*'), 'left')
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                ->join('incomingPaymentMethodGiftCard', 'incomingPaymentMethod.incomingPaymentMethodGiftCardId = incomingPaymentMethodGiftCard.incomingPaymentMethodGiftCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLoyaltyCard', 'incomingPaymentMethod.incomingPaymentMethodLoyaltyCardId = incomingPaymentMethodLoyaltyCard.incomingPaymentMethodLoyaltyCardId', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 7,'journalEntryAccounts.financeAccountsID' => $accountID, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultArr = [];

        foreach ($results as $key => $value) {
            if (!empty($value['incomingPaymentMethodCashId']) || !empty($value['incomingPaymentMethodCreditCardId']) || !empty($value['incomingPaymentMethodGiftCardId']) || !empty($value['incomingPaymentMethodLoyaltyCardId'])) {
                $resultArr[] = $value;
            }
        }

        return $resultArr;
    }

    public function getIncomePaymentJournalEntryAccountsByAccountIDAndDateRange($accountID, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('income', 'income.incomeID = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->join('incomingInvoicePayment', 'income.incomeID = incomingInvoicePayment.incomeID', array('*'), 'left')
                ->join('incomingPayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('*', 'incomingPaymentIDNew' => new Expression('incomingPayment.incomingPaymentID')), 'left')
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left')
                ->join('incomingPaymentMethodBankTransfer', 'incomingPaymentMethod.incomingPaymentMethodBankTransferId = incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId', array('*'), 'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left')
                // ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('*'), 'left')
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                ->join('incomingPaymentMethodGiftCard', 'incomingPaymentMethod.incomingPaymentMethodGiftCardId = incomingPaymentMethodGiftCard.incomingPaymentMethodGiftCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLoyaltyCard', 'incomingPaymentMethod.incomingPaymentMethodLoyaltyCardId = incomingPaymentMethodLoyaltyCard.incomingPaymentMethodLoyaltyCardId', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 43,'journalEntryAccounts.financeAccountsID' => $accountID, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultArr = [];

        foreach ($results as $key => $value) {
            if (!empty($value['incomingPaymentMethodCashId']) || !empty($value['incomingPaymentMethodCreditCardId']) || !empty($value['incomingPaymentMethodGiftCardId']) || !empty($value['incomingPaymentMethodLoyaltyCardId'])) {
                $resultArr[] = $value;
            }
        }

        return $resultArr;

        // return $results;
    }

    public function getDirectWithdrawalByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('accountWithdrawal', 'accountWithdrawal.accountWithdrawalId = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->join('financeAccounts', 'accountWithdrawal.accountWithdrawalIssuedFinanceAccountId = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('account', 'financeAccounts.financeAccountsID = account.financeAccountID', array('*'),'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 30,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsCreditAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getDepositByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('accountWithdrawal', 'accountWithdrawal.accountWithdrawalId = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->join('financeAccounts', 'accountWithdrawal.accountWithdrawalRecivedFinanceAccountId = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('account', 'financeAccounts.financeAccountsID = account.financeAccountID', array('*'),'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 30,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getDirectDepositByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('accountDeposit', 'accountDeposit.accountDepositId = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->join('financeAccounts', 'accountDeposit.accountDepositFinanceAccountID = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('account', 'financeAccounts.financeAccountsID = account.financeAccountID', array('*'),'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 29,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getDirectJEDebitByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => null,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getDirectJECreditByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => null,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsCreditAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getOtherJECreditByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->where(array('entity.deleted' => 0,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsCreditAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        $select->where->in('journalEntry.documentTypeID', array(1, 4, 5, 6, 12, 11, 18, 7, 17, 27, 10, 16, 20,27, 28, 13));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getOtherJEDebitByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->where(array('entity.deleted' => 0,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        $select->where->in('journalEntry.documentTypeID', array(1, 4, 5, 6, 12, 11, 18, 14, 13, 17,27, 10, 16, 20,27, 28));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        return $results;
    }

    public function getWithdrawalByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('accountDeposit', 'accountDeposit.accountDepositId = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->join('financeAccounts', 'accountDeposit.accountDepositFinanceAccountID = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('account', 'financeAccounts.financeAccountsID = account.financeAccountID', array('*'),'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 29,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsCreditAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getOutgoingBankTransfersByAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('accountTransfer', 'accountTransfer.accountTransferId = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->join('financeAccounts', 'accountTransfer.accountTransferIssuedFinanceAccountId = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('account', 'financeAccounts.financeAccountsID = account.financeAccountID', array('*'),'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 31,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsCreditAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getIncomingBankTransfersByAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('accountTransfer', 'accountTransfer.accountTransferId = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->join('financeAccounts', 'accountTransfer.accountTransferRecivedFinanceAccountId = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('account', 'financeAccounts.financeAccountsID = account.financeAccountID', array('*'),'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 31,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getOutgoingAdvancedPaymentByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('outgoingPayment', 'outgoingPayment.outgoingPaymentID = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->join('outGoingPaymentMethodsNumbers', 'outGoingPaymentMethodsNumbers.outGoingPaymentID = outgoingPayment.outgoingPaymentID', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 14,'outGoingPaymentMethodsNumbers.outGoingPaymentMethodFinanceAccountID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsCreditAmount','0');
        $select->where->notEqualto('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 2);
        $select->where->notEqualto('outGoingPaymentMethodsNumbers.outGoingPaymentMethodID', 5);
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getOutgoingPaymentForIncomeByGlAccountId($glAccountId, $startDate = null, $endDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->join('income', 'income.incomeID = journalEntry.journalEntryDocumentID', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntry.documentTypeID' => 43,'journalEntryAccounts.financeAccountsID' => $glAccountId, 'journalEntryAccounts.isReconcile' => false));
        $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsCreditAmount','0');
        if ($startDate && $endDate) {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateReconcileStatus($data,$journalEntryAccountsID)
    {        
        if($journalEntryAccountsID){
            if($this->tableGateway->update($data, array('journalEntryAccountsID'=>$journalEntryAccountsID))){
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function updateJournalEntryByDocumentId($journalEntryData, $journalEntryID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $update = $sql->update();
        $update->table('journalEntryAccounts');
        $update->set($journalEntryData);
        $update->where('journalEntryID = '.$journalEntryID.'');
        $update->where->notEqualTo('journalEntryAccountsDebitAmount', 0);
        $update->where->notEqualTo('journalEntryAccountsCreditAmount', 0);

        $statement = $sql->prepareStatementForSqlObject($update);
        $result = $statement->execute();

        $flag = $result->getAffectedRows();
        if(!($flag)){
            return false;
        }else{
            return true;
        }
    }

    /**
    * this function use to get journalEntry with their account details
    * @param int $journalEntryID
    * @param int $accountID
    * return mix
    **/
    public function getJournalEntryAccountsByJournalEntryIDAndAccountID($journalEntryID, $accountID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntryAccounts')
                ->columns(array('*'))
                ->join('financeAccounts', 'journalEntryAccounts.financeAccountsID = financeAccounts.financeAccountsID', array('*'), 'left')
                ->join('financeGroups', 'journalEntryAccounts.financeGroupsID = financeGroups.financeGroupsID', array('financeGroupsName'), 'left')
                ->where(array('journalEntryAccounts.journalEntryID' => $journalEntryID, 'journalEntryAccounts.financeAccountsID' => $accountID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * this function use to delete JOURNAL ENTRY ACCOUNTS by given id
    * @param $journalEntryAccountID
    * retrun boolen
    **/
    public function deleteJournalEntryAccoutsByID($journalEntryAccountID)
    {
        try {
            $result = $this->tableGateway->delete(array('journalEntryAccountsID' => $journalEntryAccountID));
            return true;
        } catch (\Exception $e) {
            error_log($e);
            return false;
        }
    }

    public function getJournalEntryAccountsByAccountIDAndDateRangeForGeneralLedger($accountID, $startDate = null, $endDate, $yearClosing = false, $dimensionType, $dimensionValue, $isOrderByDate = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID, 'journalEntry.journalEntryTemplateStatus'=>0));
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        $select->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
        if ($yearClosing) {
            $select->where->notEqualTo('journalEntryAccounts.yearEndCloseFlag',1);
        }

        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }
        if ($isOrderByDate) {
            $select->order(array('journalEntry.journalEntryDate'));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryAccountsByAccountIDAndDateRangeForBalanceSheetView($accountID, $startDate = null, $endDate, $yearClosing = false, $dimensionType = null, $dimensionValue = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCredit' => new Expression('journalEntryAccounts.journalEntryAccountsCreditAmount'),'totalDebit' => new Expression('journalEntryAccounts.journalEntryAccountsDebitAmount'),'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID));
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        $select->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
        if ($yearClosing) {
            $select->where->notEqualTo('journalEntryAccounts.yearEndCloseFlag',1);
        }
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }
        $select->group('journalEntryAccounts.financeAccountsID');

        var_dump($select->getSqlString());
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getJournalEntryAccountsByAccountIDAndDateRangeForBalanceSheetPdfAndCsv($accountID, $startDate = null, $endDate, $yearClosing = false, $dimensionType = null, $dimensionValue = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID));
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getJournalEntryAccountsByAccountIDAndDateRangeForBudget($accountID, $startDate = null, $endDate, $yearClosing = false, $dimensionType = null, $dimensionValue = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCredit' => new Expression('SUM(journalEntryAccounts.journalEntryAccountsCreditAmount)'),'totalDebit' => new Expression('SUM(journalEntryAccounts.journalEntryAccountsDebitAmount)'),'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID));
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        $select->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
        if ($yearClosing) {
            $select->where->notEqualTo('journalEntryAccounts.yearEndCloseFlag',1);
        }
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }
        $select->group('journalEntryAccounts.financeAccountsID');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryAccountsByAccountIDAndDateRangeForIncomeExpense($accountID, $startDate, $endDate, $dimensionType = null, $dimensionValue = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCredit' => new Expression('SUM(journalEntryAccounts.journalEntryAccountsCreditAmount)'),'totalDebit' => new Expression('SUM(journalEntryAccounts.journalEntryAccountsDebitAmount)'),'financeAccountsID'), 'left')
                ->where(array('entity.deleted' => 0, 'journalEntryAccounts.financeAccountsID' => $accountID));
        $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
        $select->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
       
        if ($dimensionType != null && $dimensionValue != null) {
            $select->join('journalEntryDimensions','journalEntryDimensions.journalEntryID = journalEntry.journalEntryID',array('journalEntryDimensionID'),'left');
            $select->where(array('journalEntryDimensions.dimensionType' => $dimensionType,'journalEntryDimensions.dimensionValueID' => $dimensionValue));
        }
        $select->group('journalEntryAccounts.financeAccountsID');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getJournalEntryAccountsByAccountTypeIDAndDateRangeForDashbaord($accountTypeID, $startDate = null, $endDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join('entity', 'journalEntry.entityID = entity.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCredit' => new Expression('SUM(journalEntryAccounts.journalEntryAccountsCreditAmount)'),'totalDebit' => new Expression('SUM(journalEntryAccounts.journalEntryAccountsDebitAmount)'),'financeAccountsID'), 'left')
                ->join('financeAccounts', 'journalEntryAccounts.financeAccountsID = financeAccounts.financeAccountsID', array('financeAccountClassID'), 'left')
                ->join('financeAccountClass', 'financeAccounts.financeAccountClassID = financeAccountClass.financeAccountClassID', array('financeAccountTypesID','financeAccountClassName'), 'left')
                ->where(array('entity.deleted' => 0));
        $select->where->in('financeAccountClass.financeAccountTypesID',$accountTypeID);
        if (is_null($startDate)) {
            $select->where->equalTo('journalEntry.journalEntryDate', $endDate);
        } else {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
            
        }
        $select->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
      
        $select->group('financeAccounts.financeAccountClassID');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getJournalEntryAccountsByAccountClassNameAndDateRangeForDashbaord($accountClassName, $startDate = null, $endDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('journalEntry')
                ->columns(array('*'))
                ->join(['entity1' => 'entity'], 'journalEntry.entityID = entity1.entityID', array('deleted'), 'left')
                ->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('totalCredit' => new Expression('SUM(journalEntryAccounts.journalEntryAccountsCreditAmount)'),'totalDebit' => new Expression('SUM(journalEntryAccounts.journalEntryAccountsDebitAmount)'),'financeAccountsID'), 'left')
                ->join('financeAccounts', 'journalEntryAccounts.financeAccountsID = financeAccounts.financeAccountsID', array('financeAccountClassID','financeAccountsCode','financeAccountsName'), 'left')
                ->join('financeAccountClass', 'financeAccounts.financeAccountClassID = financeAccountClass.financeAccountClassID', array('financeAccountTypesID','financeAccountClassName'), 'left')
                ->join(['entity2' => 'entity'], 'financeAccountClass.entityID = entity2.entityID', array('deleted'), 'left')
                ->where(array('entity1.deleted' => 0, 'entity2.deleted' => 0,'financeAccountClass.financeAccountClassName' => $accountClassName));
        if (is_null($startDate)) {
            $select->where->equalTo('journalEntry.journalEntryDate', $endDate);
        } else {
            $select->where->between('journalEntry.journalEntryDate', $startDate, $endDate);
            
        }
        $select->where->notEqualTo('journalEntry.documentStatus',5)
                ->where->notEqualTo('journalEntry.documentStatus',10);
      
        $select->group('financeAccounts.financeAccountsID');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
