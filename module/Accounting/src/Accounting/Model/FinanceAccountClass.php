<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class FinanceAccountClass
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class FinanceAccountClass implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented account class Id. (Primary key)
     */
    public $financeAccountClassID;
    
    /**
     * @var string finance account class name.
     */
    public $financeAccountClassName;
    
    /**
     * @var int finance account types id.
     */
    public $fiananceAccountTypesID;

    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->financeAccountClassID = (isset($data['financeAccountClassID'])) ? $data['financeAccountClassID'] : null;
        $this->financeAccountClassName = (isset($data['financeAccountClassName'])) ? $data['financeAccountClassName'] : null;
        $this->financeAccountTypesID = (isset($data['financeAccountTypesID'])) ? $data['financeAccountTypesID'] : null;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeAccountClassName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeAccountTypesID',
                        'required' => true,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
