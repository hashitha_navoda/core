<?php 

namespace Accounting\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomeNonItemProduct
{

    public $incomeNonItemProductID;
    public $incomeID;
    public $incomeNonItemProductDiscription;
    public $incomeNonItemProductQuantity;
    public $incomeNonItemProductUnitPrice;
    public $incomeNonItemProductTotalPrice;
    public $incomeNonItemProductGLAccountID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->incomeNonItemProductID = (!empty($data['incomeNonItemProductID'])) ? $data['incomeNonItemProductID'] : null;
        $this->incomeID = (!empty($data['incomeID'])) ? $data['incomeID'] : null;
        $this->incomeNonItemProductDiscription = (!empty($data['incomeNonItemProductDiscription'])) ? $data['incomeNonItemProductDiscription'] : null;
        $this->incomeNonItemProductQuantity = (!empty($data['incomeNonItemProductQuantity'])) ? $data['incomeNonItemProductQuantity'] : null;
        $this->incomeNonItemProductUnitPrice = (!empty($data['incomeNonItemProductUnitPrice'])) ? $data['incomeNonItemProductUnitPrice'] : 0.00;
        $this->incomeNonItemProductTotalPrice = (!empty($data['incomeNonItemProductTotalPrice'])) ? $data['incomeNonItemProductTotalPrice'] : 0.00;
        $this->roockID = (!empty($data['roockID'])) ? $data['roockID'] : null;
        $this->incomeNonItemProductGLAccountID = (!empty($data['incomeNonItemProductGLAccountID'])) ? $data['incomeNonItemProductGLAccountID'] : null;
    }
}