<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class FinanceGroupTypes
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class FinanceGroupTypes implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented financeGroupTypesID. (Primary key)
     */
    public $financeGroupTypesID;
    
    /**
     * @var string financeGroupTypesName.
     */
    public $financeGroupTypesName;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->financeGroupTypesID = (isset($data['financeGroupTypesID'])) ? $data['financeGroupTypesID'] : null;
        $this->financeGroupTypesName = (isset($data['financeGroupTypesName'])) ? $data['financeGroupTypesName'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeGroupTypesID',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeGroupTypesName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
