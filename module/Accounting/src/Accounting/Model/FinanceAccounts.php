<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class FinanceAccounts
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class FinanceAccounts implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented accounts Id. (Primary key)
     */
    public $financeAccountsID;
    
    /**
     * @var string finance accounts code.
     */
    public $financeAccountsCode;
    
    /**
     * @var int finance accounts name.
     */
    public $financeAccountsName;

    /**
     * @var int finance accounts parent id.
     */
    public $financeAccountsParentID;

    /**
     * @var int finance accounts class id.
     */
    public $financeAccountClassID;

    /**
     * @var int finance accounts status ID.
     */
    public $financeAccountStatusID;

    /**
     * @var int finance accounts credit amount.
     */
    public $financeAccountsCreditAmount;

    /**
     * @var int finance accounts debit amount.
     */
    public $financeAccountsDebitAmount;

    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->financeAccountsID = (!empty($data['financeAccountsID'])) ? $data['financeAccountsID'] : null;
        $this->financeAccountsCode = (!empty($data['financeAccountsCode'])) ? $data['financeAccountsCode'] : null;
        $this->financeAccountsName = (!empty($data['financeAccountsName'])) ? $data['financeAccountsName'] : null;
        $this->financeAccountsParentID = (!empty($data['financeAccountsParentID'])) ? $data['financeAccountsParentID'] : 0;
        $this->financeAccountClassID = (!empty($data['financeAccountClassID'])) ? $data['financeAccountClassID'] : null;
        $this->financeAccountStatusID = (!empty($data['financeAccountStatusID'])) ? $data['financeAccountStatusID'] : null;
        $this->financeAccountsCreditAmount = (!empty($data['financeAccountsCreditAmount'])) ? $data['financeAccountsCreditAmount'] : 0.00;
        $this->financeAccountsDebitAmount = (!empty($data['financeAccountsDebitAmount'])) ? $data['financeAccountsDebitAmount'] : 0.00;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeAccountsCode',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeAccountsName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeAccountClassID',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeAccountsParentID',
                        'required' => false,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeAccountStatusID',
                        'required' => true,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
