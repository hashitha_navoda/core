<?php

namespace Accounting\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
Class IncomeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveIncome($incomeData)
    {
        $saveData = array(
            'incomeCode' => $incomeData->incomeCode,
            'customerID' => $incomeData->customerID,
            'incomeAccountID' => $incomeData->incomeAccountID,
            'incomeLocationID' => $incomeData->incomeLocationID,
            'incomeDate' => $incomeData->incomeDate,
            'incomeTotal' => $incomeData->incomeTotal,
            'incomeComment' => $incomeData->incomeComment,
            'incomeItemFlag' => $incomeData->incomeItemFlag,
            'status' => 4,
            'entityID' => $incomeData->entityID,
        );
        if ($this->tableGateway->insert($saveData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateIncome($data, $incomeID)
    {
        $this->tableGateway->update($data, array('incomeID' => $incomeID));
        return TRUE;
    }

    public function checkIncomeByCode($incomeCode, $locationID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('income')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "income.entityID = e.entityID")
                    ->where(array('incomeLocationID' => $locationID))
                    ->where(array('e.deleted' => '0'))
                    ->where(array('income.incomeCode' => $incomeCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('income')
                    ->columns(array('*'))
                    ->join('customer', 'income.customerID = customer.customerID', array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->join('status', 'income.status = status.statusID', array('statusName'), 'left')
                    ->order(array('incomeDate' => 'DESC', 'incomeID' => 'DESC'))
                    ->where(array('incomeLocationID' => $locationID));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $rowset = $this->tableGateway->select();
        return $rowset;
    }


    public function getIncomerByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('income')
                ->columns(array('*'))
                ->join('customer', 'customer.customerID = income.customerID', array('*'),'left')
                ->join('status', 'income.status = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'income.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('income.status',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('income.incomeLocationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }
    public function getIncomerByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('income')
                ->columns(array('*'))
                ->join('customer', 'customer.customerID = income.customerID', array('*'),'left')
                ->join('status', 'income.status = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'income.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('income.status',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('income.incomeLocationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCustomerCumulativeIncomeBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('income')
                ->columns(array('customerID','incomeTotal','incomeID'))
                ->join('customer', 'customer.customerID = income.customerID', array(),'left')
                ->join('status', 'income.status = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'income.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('income.status',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('income.incomeLocationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCustomerCumulativeIncomeBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('income')
                ->columns(array('customerID','cumulative_total' => new \Zend\Db\Sql\Expression('SUM(incomeTotal)'),'incomeID'))
                ->join('customer', 'customer.customerID = income.customerID', array(),'left')
                ->join('status', 'income.status = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'income.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('income.status',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('income.incomeLocationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getIncomeByCustomerID($customerID, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("income")
                ->columns(array("*"))
                ->join("customer", "income.customerID = customer.customerID", array("customerName", "customerCode", "customerStatus"), "left")
                ->where(array("income.customerID" => $customerID, "income.incomeLocationID" => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function searchIncomesForDropDown($locationID = false, $searchKey, $customerID = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("income")
                ->columns(array('incomeID', 'incomeCode'))
                ->order(array('incomeDate' => 'DESC'));
        $select->where->like('incomeCode', '%' . $searchKey . '%');
        $select->limit(50);

        if($locationID){
            $select->where(array("income.incomeLocationID" => $locationID));
        }

        if($customerID){
            $select->where(array("income.customerID" => $customerID));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getIncomeforSearch($incomeID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("income")
                ->columns(array("*"))
                ->join("customer", "income.customerID = customer.customerID", array("*"), "left")
                ->where(array("incomeID" => $incomeID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getIncomeByDate($fromdate, $todate, $customerID = NULL, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("income")
                ->columns(array("*"))
                ->join("customer", "income.customerID = customer.customerID", array("customerName", "customerShortName", "customerCode","customerStatus"), "left")
                ->order(array('incomeDate' => 'DESC'))
                ->where(array("income.incomeLocationID" => $locationID));
        if ($customerID != NULL) {
            $select->where(array("income.customerID" => $customerID));
        }
        $select->where->between('incomeDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getIncomeProductByIncomeID($incomeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('income');
        $select->join('incomeProduct', 'income.incomeID = incomeProduct.incomeID', array('incomeProductID', 'productID', 'incomeProductQuantity', 'incomeProductPrice', 'incomeProductTotal', 'incomeProductDiscount','incomeProductDiscountType'));
        $select->where(array('income.incomeID' => $incomeID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getIncomeBasicsByIncomeID($incomeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('income');
        $select->join('entity', 'income.entityID = entity.entityID', array('createdTimeStamp'), 'left');
        $select->where(array('income.incomeID' => $incomeID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }


    public function getIncomeDetailsByIncomeID($incomeID, $locationID, $inventoryFlag = true)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('income');
        $select->join('incomeProduct', 'income.incomeID = incomeProduct.incomeID', array('incomeProductID', 'productID', 'incomeProductQuantity', 'incomeProductPrice', 'incomeProductTotal', 'incomeProductDiscount','incomeProductDiscountType'), 'left');
        $select->join('incomeNonItemProduct', 'income.incomeID = incomeNonItemProduct.incomeID', array('incomeNonItemProductID', 'incomeNonItemProductDiscription', 'incomeNonItemProductQuantity', 'incomeNonItemProductUnitPrice', 'incomeNonItemProductTotalPrice', 'incomeNonItemProductGLAccountID'), 'left');
        $select->join('financeAccounts', 'incomeNonItemProduct.incomeNonItemProductGLAccountID = financeAccounts.financeAccountsID', array('financeAccountsCode','financeAccountsName'), 'left');
        $select->join('product', 'incomeProduct.productID = product.productID', array('productCode', 'productName'), 'left');
        $select->join('locationProduct', 'locationProduct.productID = incomeProduct.productID', array('locationProductID'), 'left');
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion'), 'left');
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'), 'left');
        $select->join("customer", "income.customerID = customer.customerID", array("customerName", "customerShortName", "customerCode","customerStatus","customerTitle"), "left");
        $select->join('location', 'income.incomeLocationID = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('incomeProductTax', 'income.incomeID = incomeProductTax.incomeID', array('incomeProductTaxID', 'incomeProductIdOrNonItemProductID', 'incomeTaxID', 'incomeTaxPrecentage', 'incomeTaxAmount', 'incomeTaxItemOrNonItemProduct'), 'left');
        $select->join('tax', 'incomeProductTax.incomeTaxID = tax.id', array('taxName'), 'left');
        $select->join('entity', 'income.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->where(array('income.incomeID' => $incomeID));

        if ($inventoryFlag) {
            $select->where(array('locationProduct.locationID' => $locationID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}