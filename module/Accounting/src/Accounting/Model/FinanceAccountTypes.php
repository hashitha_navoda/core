<?php

namespace Accounting\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class FinanceAccountTypes
 * @package Accounting\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class FinanceAccountTypes implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented account types Id. (Primary key)
     */
    public $financeAccountTypesID;
    
    /**
     * @var string finance account types name.
     */
    public $financeAccountTypesName;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->financeAccountTypesID = (isset($data['financeAccountTypesID'])) ? $data['financeAccountTypesID'] : null;
        $this->financeAccountTypesName = (isset($data['financeAccountTypesName'])) ? $data['financeAccountTypesName'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'financeAccountTypesName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
