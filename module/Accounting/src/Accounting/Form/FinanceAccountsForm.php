<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class FinanceAccountsForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create finance accounts form
 */
class FinanceAccountsForm extends Form {

    public function __construct($name, $data) {
        $accounts = $data['financeAccounts'];
        $accountClass = $data['financeAccountClass'];
        $accountsStatus = $data['financeAccountsStatus'];
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'financeAccountsID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'financeAccountsID'
            ),
        ));
        $this->add(array(
            'name' => 'fAccountsParentID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'fAccountsParentID'
            ),
        ));


        $this->add(array(
            'name' => 'financeAccountsCode',
            'attributes' => array(
                'type' => 'text',
                'id' => 'financeAccountsCode',
                'class' => 'form-control',
                'placeholder' => '0001',
            )
        ));

        $this->add(array(
            'name' => 'financeAccountsName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'financeAccountsName',
                'class' => 'form-control',
                'placeholder' => 'Sales Accounts',
            )
        ));

        $this->add(array(
            'name' => 'financeAccountsParentID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'financeAccountsParentID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select a account',
                'value_options' => isset($accounts) ? $accounts : NULL
            )
        ));


        $this->add(array(
            'name' => 'financeAccountClassID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'financeAccountClassID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select a account class',
                'value_options' => isset($accountClass) ? $accountClass : NULL
            )
        ));

        $this->add(array(
            'name' => 'financeAccountStatusID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'financeAccountStatusID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'value_options' => isset($accountsStatus) ? $accountsStatus : NULL
            )
        ));

        $this->add(array(
            'name' => 'entityID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'entityID',
            ),
        ));

        $this->add(array(
            'name' => 'createFinanceAccounts',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Create'),
                'id' => 'createFinanceAccounts',
                'class' => 'btn btn-primary'
            ),
        ));

        $this->add(array(
            'name' => 'cancelFinanceAccounts',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Reset'),
                'id' => 'cancelFinanceAccounts',
                'class' => 'btn btn-default'
            ),
        ));
    }
}