<?php

namespace Accounting\Form;

use Zend\Form\Form;

class YearEndClosingForm extends Form {

    public function __construct($name, $data) {
        $fiscalPeriods = $data['fiscalPeriods'];
        $financeAccounts = $data['financeAccounts'];
        $accountsStatus = $data['financeAccountsStatus'];
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'fiscalPeriod',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'fiscalPeriod',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true",
                'required' => "true"

            ),
            'options' => array(
                'empty_option' => 'select a period',
                'value_options' => isset($fiscalPeriods) ? $fiscalPeriods : NULL
            )
        ));

        $this->add(array(
            'name' => 'yearEndClosingAc',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'yearEndClosingAc',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select a account',
                'value_options' => isset($financeAccounts) ? $financeAccounts : NULL
            )
        ));

        $this->add(array(
            'name' => 'retainedEarningAc',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'retainedEarningAc',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select a account',
                'value_options' => isset($financeAccounts) ? $financeAccounts : NULL
            )
        ));

        $this->add(array(
            'name' => 'entityID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'entityID',
            ),
        ));

        $this->add(array(
            'name' => 'proceedYearEndClosing',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Proceed'),
                'id' => 'proceedYearEndClosing',
                'class' => 'btn btn-primary'
            ),
        ));
    }
}