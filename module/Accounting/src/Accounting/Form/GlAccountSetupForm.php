<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class GlAccountSetupForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create gl account setup form
 */
class GlAccountSetupForm extends Form {

    public function __construct($name) {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'glAccountSetupID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'glAccountSetupID'
            ),
        ));

        $this->add(array(
            'name' => 'glAccountSetupItemDefaultSalesAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupItemDefaultSalesAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupItemDefaultInventoryAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupItemDefaultInventoryAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupItemDefaultCOGSAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupItemDefaultCOGSAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupItemDefaultAdjusmentAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupItemDefaultAdjusmentAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupSalesAndCustomerReceivableAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupSalesAndCustomerReceivableAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupSalesAndCustomerSalesAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupSalesAndCustomerSalesAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupSalesAndCustomerSalesDiscountAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupSalesAndCustomerSalesDiscountAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupSalesAndCustomerAdvancePaymentAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupSalesAndCustomerAdvancePaymentAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupPurchasingAndSupplierPayableAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupPurchasingAndSupplierPayableAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupPurchasingAndSupplierGrnClearingAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupPurchasingAndSupplierGrnClearingAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupGeneralExchangeVarianceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupGeneralExchangeVarianceAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupGeneralProfitAndLostYearAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupGeneralProfitAndLostYearAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupGeneralBankChargersAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupGeneralBankChargersAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupGeneralDeliveryChargersAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupGeneralDeliveryChargersAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'glAccountSetupGeneralLoyaltyExpenseAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'glAccountSetupGeneralLoyaltyExpenseAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'saveGlAccountSetup',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'saveGlAccountSetup',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}
