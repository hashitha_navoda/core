<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class FinanceGroupsForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create finance groups form
 */
class FinanceGroupsForm extends Form {

    public function __construct($name, $data) {
        $groups = $data['financeGroups'];
        $groupTypes = $data['financeGroupTypes'];
        $accountsStatus = $data['financeAccountsStatus'];
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'financeGroupsID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'financeGroupsID'
            ),
        ));

        $this->add(array(
            'name' => 'financeGroupsName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'financeGroupsName',
                'class' => 'form-control',
                'placeholder' => 'Project',
            )
        ));

        $this->add(array(
            'name' => 'financeGroupsShortName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'financeGroupsShortName',
                'class' => 'form-control',
                'placeholder' => 'project',
            )
        ));

        $this->add(array(
            'name' => 'financeGroupsParentID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'financeGroupsParentID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select a group',
                'value_options' => isset($groups) ? $groups : NULL
            )
        ));


        $this->add(array(
            'name' => 'financeGroupTypesID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'financeGroupTypesID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select a group type',
                'value_options' => isset($groupTypes) ? $groupTypes : NULL
            )
        ));

        $this->add(array(
            'name' => 'financeGroupsAddress',
            'attributes' => array(
                'type' => 'text',
                'id' => 'financeGroupsAddress',
                'class' => 'form-control',
                'placeholder' => 'NO/312',
            )
        ));

        $this->add(array(
            'name' => 'entityID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'entityID',
            ),
        ));

        $this->add(array(
            'name' => 'createFinanceGroups',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Create'),
                'id' => 'createFinanceGroups',
                'class' => 'btn btn-primary'
            ),
        ));

        $this->add(array(
            'name' => 'cancelFinanceGroups',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Reset'),
                'id' => 'cancelFinanceGroups',
                'class' => 'btn btn-default'
            ),
        ));
    }
}