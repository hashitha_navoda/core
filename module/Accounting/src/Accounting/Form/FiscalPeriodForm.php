<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class FiscalPeriodForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create fiscal period form
 */
class FiscalPeriodForm extends Form {

    public function __construct($name,$data = null) {
    	$status = $data['status'];
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'fiscalPeriodID',
            'attributes' => array(
                'type' => 'hidden',
                'data-hasjournalentries' => 0,
                'id' => 'fiscalPeriodID'
            ),
        ));

        $this->add(array(
            'name' => 'fiscalPeriodStartDate',
            'attributes' => array(
                'type' => 'text',
                'id' => 'fiscalPeriodStartDate',
                'class' => 'form-control pointer_cursor',
                'readOnly' => 'readOnly',
                'style' => "background-color: white",
                'placeholder' => '2000/01/01',
            )
        ));

        $this->add(array(
            'name' => 'fiscalPeriodEndDate',
            'attributes' => array(
                'type' => 'text',
                'id' => 'fiscalPeriodEndDate',
                'class' => 'form-control pointer_cursor',
                'readOnly' => 'readOnly',
                'style' => "background-color: white",
                'placeholder' => '2000/01/01',
            )
        ));

        $this->add(array(
            'name' => 'fiscalPeriodStatusID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'fiscalPeriodStatusID',
                'class' => 'form-control',
            ),
            'options' => array(
                'value_options' => isset($status) ? $status : NULL,
            )
        ));

        $this->add(array(
            'name' => 'entityID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'entityID',
            ),
        ));

         $this->add(array(
            'name' => 'createFiscalPeriod',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'createFiscalPeriod',
                'class' => 'btn btn-primary'
            ),
        ));

        $this->add(array(
            'name' => 'cancelFiscalPeriod',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Cancel'),
                'id' => 'cancelFiscalPeriod',
                'class' => 'btn btn-default',
                'data-dismiss' => "modal"
            ),
        ));
    }

}
