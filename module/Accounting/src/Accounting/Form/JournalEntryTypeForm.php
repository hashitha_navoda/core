<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class JournalEntryTypeForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create journal entry type form
 */
class JournalEntryTypeForm extends Form {

    public function __construct($name) {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'journalEntryTypeID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'journalEntryTypeID'
            ),
        ));

        $this->add(array(
            'name' => 'journalEntryTypeName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'journalEntryTypeName',
                'class' => 'form-control',
                'placeholder' => 'sales',
            )
        ));

        $this->add(array(
            'name' => 'journalEntryTypeApprovedStatus',
            'attributes' => array(
                'type' => 'checkBox',
                'id' => 'journalEntryTypeApprovedStatus'
            )
        ));

        $this->add(array(
            'name' => 'entityID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'entityID',
            ),
        ));

        $this->add(array(
            'name' => 'createJournalEntryType',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'createJournalEntryType',
                'class' => 'btn btn-primary'
            ),
        ));

        $this->add(array(
            'name' => 'cancelJournalEntryType',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Cancel'),
                'id' => 'cancelJournalEntryType',
                'class' => 'btn btn-default',
                'data-dismiss' => "modal"
            ),
        ));
    }

}
