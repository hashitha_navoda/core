<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class JournalEntryAccountsForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create journal entry accounts form
 */
class JournalEntryAccountsForm extends Form {

    public function __construct($name, $data) {
        $fAccounts = $data['financeAccounts'];
        $financeGroups = $data['financeGroups'];
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'financeAccountsID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'financeAccountsID',
                'class' => 'form-control',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select a Account',
                'value_options' => isset($fAccounts) ? $fAccounts : NULL
            )
        ));

         $this->add(array(
            'name' => 'financeAccountsName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'financeAccountsName',
                'class' => 'form-control',
                'placeholder' => 'Sales',
            )
        ));

        $this->add(array(
            'name' => 'financeGroupsID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'financeGroupsID',
                'class' => 'form-control',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Ref',
                'value_options' => isset($financeGroups) ? $financeGroups : NULL
            )
        ));

        $this->add(array(
            'name' => 'journalEntryAccountsDebitAmount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'journalEntryAccountsDebitAmount',
                'class' => 'form-control',
                'placeholder' => '3000',
            )
        ));

        $this->add(array(
            'name' => 'journalEntryAccountsCreditAmount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'journalEntryAccountsCreditAmount',
                'class' => 'form-control',
                'placeholder' => '3000',
            )
        ));

        $this->add(array(
            'name' => 'journalEntryAccountsMemo',
            'attributes' => array(
                'type' => 'text',
                'id' => 'journalEntryAccountsMemo',
                'class' => 'form-control',
                'placeholder' => 'memo',
            )
        ));
    }

}
