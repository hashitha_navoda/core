<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class PaymentMethodSetupForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create Payment Method Setup Form
 */
class PaymentMethodSetupForm extends Form {

    public function __construct($name) {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'salesCashFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'salesCashFinanceAccountID',
                'class' => 'form-control selectpicker salesAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'salesLcFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'salesLcFinanceAccountID',
                'class' => 'form-control selectpicker salesAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'salesTtFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'salesTtFinanceAccountID',
                'class' => 'form-control selectpicker salesAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
        	'name' => 'salesCardFinanceAccountID',
        	'type' => 'Select',
        	'attributes' => array(
        		'id' => 'salesCardFinanceAccountID',
        		'class' => 'form-control selectpicker salesAccountID',
        		'data-live-search'=>"true"
        	),
        	'options' => array(
        		'empty_option' => 'Select an Account',
        	)
        ));

        $this->add(array(
        	'name' => 'salesGiftCardFinanceAccountID',
        	'type' => 'Select',
        	'attributes' => array(
        		'id' => 'salesGiftCardFinanceAccountID',
        		'class' => 'form-control selectpicker salesAccountID',
        		'data-live-search'=>"true"
        	),
        	'options' => array(
        		'empty_option' => 'Select an Account',
        	)
        ));

        $this->add(array(
        	'name' => 'salesChequeFinanceAccountID',
        	'type' => 'Select',
        	'attributes' => array(
        		'id' => 'salesChequeFinanceAccountID',
        		'class' => 'form-control selectpicker salesAccountID',
        		'data-live-search'=>"true"
        	),
        	'options' => array(
        		'empty_option' => 'Select an Account',
        	)
        ));

        $this->add(array(
            'name' => 'salesLoyaltyCardFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'salesLoyaltyCardFinanceAccountID',
                'class' => 'form-control selectpicker salesAccountID' ,
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'salesBankTransferFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'salesBankTransferFinanceAccountID',
                'class' => 'form-control selectpicker salesAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'purchaseCashFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'purchaseCashFinanceAccountID',
                'class' => 'form-control selectpicker purchaseAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'purchaseLcFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'purchaseLcFinanceAccountID',
                'class' => 'form-control selectpicker purchaseAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'purchaseTtFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'purchaseTtFinanceAccountID',
                'class' => 'form-control selectpicker purchaseAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'purchaseCardFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'purchaseCardFinanceAccountID',
                'class' => 'form-control selectpicker purchaseAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'purchaseGiftCardFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'purchaseGiftCardFinanceAccountID',
                'class' => 'form-control selectpicker purchaseAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'purchaseChequeFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'purchaseChequeFinanceAccountID',
                'class' => 'form-control selectpicker purchaseAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'purchaseLoyaltyCardFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'purchaseLoyaltyCardFinanceAccountID',
                'class' => 'form-control selectpicker purchaseAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'purchaseBankTransferFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'purchaseBankTransferFinanceAccountID',
                'class' => 'form-control selectpicker purchaseAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'savePaymentMethodSetup',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'savePaymentMethodSetup',
                'class' => 'btn btn-primary'
            ),
        ));

        $this->add(array(
            'name' => 'salesUniformVoucherFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'salesUniformVoucherFinanceAccountID',
                'class' => 'form-control selectpicker salesAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'purchaseUniformVoucherFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'purchaseUniformVoucherFinanceAccountID',
                'class' => 'form-control selectpicker purchaseAccountID',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));
    }

}
