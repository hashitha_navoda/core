<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class FinanceAccountClassForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create finance account class form
 */
class FinanceAccountClassForm extends Form {

    public function __construct($name, $data) {
    	$accountTypes = $data['financeAccountTypes'];
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'financeAccountClassID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'financeAccountClassID'
            ),
        ));

        $this->add(array(
            'name' => 'financeAccountClassName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'financeAccountClassName',
                'class' => 'form-control',
                'placeholder' => 'Sales Account',
            )
        ));

        $this->add(array(
            'name' => 'financeAccountTypesID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'financeAccountTypesID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select a account type',
                'value_options' => isset($accountTypes) ? $accountTypes : NULL
            )
        ));

        $this->add(array(
            'name' => 'entityId',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'entityId',
            ),
        ));

        $this->add(array(
            'name' => 'createFinanceAccountClass',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Create'),
                'id' => 'createFinanceAccountClass',
                'class' => 'btn btn-primary'
            ),
        ));

        $this->add(array(
            'name' => 'cancelFinanceAccountClass',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Reset'),
                'id' => 'cancelFinanceAccountClass',
                'class' => 'btn btn-default'
            ),
        ));
    }

}
