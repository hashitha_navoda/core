<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class JournalEntryForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create journal entry form
 */
class JournalEntryForm extends Form {

    public function __construct($name) {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'journalEntryID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'journalEntryID'
            ),
        ));

        $this->add(array(
            'name' => 'journalEntryCode',
            'attributes' => array(
                'type' => 'text',
                'id' => 'journalEntryCode',
                'class' => 'form-control',
                'placeholder' => 'JOUR_001',
            )
        ));

        $this->add(array(
            'name' => 'journalEntryDate',
            'attributes' => array(
                'type' => 'text',
                'id' => 'journalEntryDate',
                'class' => 'form-control pointer_cursor',
                'readOnly' => 'readOnly',
                'style' => "background-color: white",
                'placeholder' => '2016/01/01',
            )
        ));

        $this->add(array(
            'name' => 'journalEntryTypeID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'journalEntryTypeID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select a Journal Entry Type',
            )
        ));

        $this->add(array(
            'name' => 'journalEntryStatusID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'journalEntryStatusID'
            ),
        ));

        $this->add(array(
            'name' => 'journalEntryIsReverse',
            'attributes' => array(
                'type' => 'checkBox',
                'id' => 'journalEntryIsReverse'
            )
        ));

        $this->add(array(
            'name' => 'journalEntryTemplateStatus',
            'attributes' => array(
                'type' => 'checkBox',
                'id' => 'journalEntryTemplateStatus'
            )
        ));

        $this->add(array(
            'name' => 'journalEntryForOpeningBalance',
            'attributes' => array(
                'type' => 'checkBox',
                'id' => 'journalEntryForOpeningBalance'
            )
        ));


        $this->add(array(
            'name' => 'journalEntryComment',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'journalEntryComment',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'entityID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'entityID',
            ),
        ));

        $this->add(array(
            'name' => 'locationID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'locationID',
            ),
        ));

        $this->add(array(
            'name' => 'createJournalEntry',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'createJournalEntry',
                'class' => 'btn btn-primary'
            ),
        ));

        $this->add(array(
            'name' => 'createOpeningBalanceJournalEntry',
            'attributes' => array(
                'type' => 'button',
                'value' => _('Save with temporary account'),
                'id' => 'createOpeningBalanceJournalEntry',
                'class' => 'btn btn-primary hidden'
            ),
        ));

        $this->add(array(
            'name' => 'cancelJournalEntry',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Reset'),
                'id' => 'cancelJournalEntry',
                'class' => 'btn btn-default'
            ),
        ));
    }

}
