<?php

namespace Accounting\Form;

use Zend\Form\Form;

/**
 * class JournalEntryTypeApproversForm
 * @author Ashan Madushka <ashan@thinkcube.com>
 * create journal entry type approvers form
 */
class JournalEntryTypeApproversForm extends Form {

    public function __construct($name) {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'journalEntryTypeApproversID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'journalEntryTypeApproversID'
            ),
        ));

        $this->add(array(
            'name' => 'journalEntryTypeApproversMinAmount',
            'attributes' => array(
                'type' => 'number',
                'id' => 'journalEntryTypeApproversMinAmount',
                'class' => 'form-control',
                'placeholder' => '100',
            )
        ));

        $this->add(array(
            'name' => 'journalEntryTypeApproversMaxAmount',
            'attributes' => array(
                'type' => 'number',
                'id' => 'journalEntryTypeApproversMaxAmount',
                'class' => 'form-control',
                'placeholder' => '100',
            )
        ));

        $this->add(array(
            'name' => 'employeeID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'employeeID',
                'class' => 'form-control ',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select a approver',
            )
        ));
    }

}
