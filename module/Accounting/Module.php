<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Accounting;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $this->getConfig();
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'AccountService' => 'Accounting\Service\AccountService',
                'BudgetService' => 'Accounting\Service\BudgetService',
                'IncomeService' => 'Accounting\Service\IncomeService',
            ),
            'shared' => array(
                'AccountService' => true,
                'BudgetService' => true,
                'IncomeService' => true
            ),
            'aliases' => array(
                'GlAccountSetupTable' => 'Accounting\Model\GlAccountSetupTable',
                'FiscalPeriodTable' => 'Accounting\Model\FiscalPeriodTable',
                'FinanceAccountTypesTable' => 'Accounting\Model\FinanceAccountTypesTable',
                'FinanceAccountClassTable' => 'Accounting\Model\FinanceAccountClassTable',
                'FinanceAccountsTable' => 'Accounting\Model\FinanceAccountsTable',
                'JournalEntryAccountsTable' => 'Accounting\Model\JournalEntryAccountsTable',
                'JournalEntryTable' => 'Accounting\Model\JournalEntryTable',
                'BudgetConfigurationTable' => 'Accounting\Model\BudgetConfigurationTable',
                'BudgetDetailsTable' => 'Accounting\Model\BudgetDetailsTable',
                'BudgetTable' => 'Accounting\Model\BudgetTable',
                'IncomeTable' => 'Accounting\Model\IncomeTable',
                'IncomeNonItemProductTable' => 'Accounting\Model\IncomeNonItemProductTable',
                'IncomeProductTaxTable' => 'Accounting\Model\IncomeProductTaxTable',
                'IncomeProductTable' => 'Accounting\Model\IncomeProductTable',
                'IncomeSubProductTable' => 'Accounting\Model\IncomeSubProductTable',
            ),
            'factories' => array(
                'Accounting\Model\FinanceAccountTypesTable' => function($sm) {
                    $tableGateway = $sm->get('FinanceAccountTypesTableGateWay');
                    $table = new Model\FinanceAccountTypesTable($tableGateway);
                    return $table;
                },
                'FinanceAccountTypesTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\FinanceAccountTypes());
                    return new TableGateway('financeAccountTypes', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\FinanceAccountClassTable' => function($sm) {
                    $tableGateway = $sm->get('FinanceAccountClassTableGateWay');
                    $table = new Model\FinanceAccountClassTable($tableGateway);
                    return $table;
                },
                'FinanceAccountClassTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\FinanceAccountClass());
                    return new TableGateway('financeAccountClass', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\FinanceAccountsTable' => function($sm) {
                    $tableGateway = $sm->get('FinanceAccountsTableGateWay');
                    $table = new Model\FinanceAccountsTable($tableGateway);
                    return $table;
                },
                'FinanceAccountsTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\FinanceAccounts());
                    return new TableGateway('financeAccounts', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\FinanceGroupTypesTable' => function($sm) {
                    $tableGateway = $sm->get('FinanceGroupTypesTableGateWay');
                    $table = new Model\FinanceGroupTypesTable($tableGateway);
                    return $table;
                },
                'FinanceGroupTypesTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\FinanceGroupTypes());
                    return new TableGateway('financeGroupTypes', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\FinanceGroupsTable' => function($sm) {
                    $tableGateway = $sm->get('FinanceGroupsTableGateWay');
                    $table = new Model\FinanceGroupsTable($tableGateway);
                    return $table;
                },
                'FinanceGroupsTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\FinanceGroups());
                    return new TableGateway('financeGroups', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\JournalEntryTable' => function($sm) {
                    $tableGateway = $sm->get('JournalEntryTableGateWay');
                    $table = new Model\JournalEntryTable($tableGateway);
                    return $table;
                },
                'JournalEntryTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\JournalEntry());
                    return new TableGateway('journalEntry', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\JournalEntryAccountsTable' => function($sm) {
                    $tableGateway = $sm->get('JournalEntryAccountsTableGateWay');
                    $table = new Model\JournalEntryAccountsTable($tableGateway);
                    return $table;
                },
                'JournalEntryAccountsTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\JournalEntryAccounts());
                    return new TableGateway('journalEntryAccounts', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\JournalEntryTypeTable' => function($sm) {
                    $tableGateway = $sm->get('JournalEntryTypeTableGateWay');
                    $table = new Model\JournalEntryTypeTable($tableGateway);
                    return $table;
                },
                'JournalEntryTypeTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\JournalEntryType());
                    return new TableGateway('journalEntryType', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\JournalEntryTypeApproversTable' => function($sm) {
                    $tableGateway = $sm->get('JournalEntryTypeApproversTableGateWay');
                    $table = new Model\JournalEntryTypeApproversTable($tableGateway);
                    return $table;
                },
                'JournalEntryTypeApproversTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\JournalEntryTypeApprovers());
                    return new TableGateway('journalEntryTypeApprovers', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\JournalEntryApproversTable' => function($sm) {
                    $tableGateway = $sm->get('JournalEntryApproversTableGateWay');
                    $table = new Model\JournalEntryApproversTable($tableGateway);
                    return $table;
                },
                'JournalEntryApproversTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\JournalEntryApprovers());
                    return new TableGateway('journalEntryApprovers', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\FiscalPeriodTable' => function($sm) {
                    $tableGateway = $sm->get('FiscalPeriodTableGateWay');
                    $table = new Model\FiscalPeriodTable($tableGateway);
                    return $table;
                },
                'FiscalPeriodTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\FiscalPeriod());
                    return new TableGateway('fiscalPeriod', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\GlAccountSetupTable' => function($sm) {
                    $tableGateway = $sm->get('GlAccountSetupTableGateWay');
                    $table = new Model\GlAccountSetupTable($tableGateway);
                    return $table;
                },
                'GlAccountSetupTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\GlAccountSetup());
                    return new TableGateway('glAccountSetup', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\BudgetConfigurationTable' => function($sm) {
                    $tableGateway = $sm->get('BudgetConfigurationTableGateWay');
                    $table = new Model\BudgetConfigurationTable($tableGateway);
                    return $table;
                },
                'BudgetConfigurationTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\BudgetConfiguration());
                    return new TableGateway('budgetConfiguration', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\BudgetTable' => function($sm) {
                    $tableGateway = $sm->get('BudgetTableGateWay');
                    $table = new Model\BudgetTable($tableGateway);
                    return $table;
                },
                'BudgetTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Budget());
                    return new TableGateway('budget', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\IncomeTable' => function($sm) {
                    $tableGateway = $sm->get('IncomeTableGateWay');
                    $table = new Model\IncomeTable($tableGateway);
                    return $table;
                },
                'IncomeTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Income());
                    return new TableGateway('income', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\IncomeSubProductTable' => function($sm) {
                    $tableGateway = $sm->get('IncomeSubProductTableGateWay');
                    $table = new Model\IncomeSubProductTable($tableGateway);
                    return $table;
                },
                'IncomeSubProductTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\IncomeSubProduct());
                    return new TableGateway('incomeSubProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\IncomeProductTable' => function($sm) {
                    $tableGateway = $sm->get('IncomeProductTableGateWay');
                    $table = new Model\IncomeProductTable($tableGateway);
                    return $table;
                },
                'IncomeProductTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\IncomeProduct());
                    return new TableGateway('incomeProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\IncomeProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('IncomeProductTaxTableGateWay');
                    $table = new Model\IncomeProductTaxTable($tableGateway);
                    return $table;
                },
                'IncomeProductTaxTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\IncomeProductTax());
                    return new TableGateway('incomeProductTax', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\IncomeNonItemProductTable' => function($sm) {
                    $tableGateway = $sm->get('IncomeNonItemProductTableGateWay');
                    $table = new Model\IncomeNonItemProductTable($tableGateway);
                    return $table;
                },
                'IncomeNonItemProductTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\IncomeNonItemProduct());
                    return new TableGateway('incomeNonItemProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Accounting\Model\BudgetDetailsTable' => function($sm) {
                    $tableGateway = $sm->get('BudgetDetailsTableGateWay');
                    $table = new Model\BudgetDetailsTable($tableGateway);
                    return $table;
                },
                'BudgetDetailsTableGateWay' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\BudgetDetails());
                    return new TableGateway('budgetDetails', $dbAdapter, null, $resultSetPrototype);
                },
            )
        );
    }

}
