<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'account-classes' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account-classes[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\AccountClasses',
                        'action' => 'create',
                    ),
                ),
            ),
            'income' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/income[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\Income',
                        'action' => 'create',
                    ),
                ),
            ),
            'account-dashboard' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account-dashboard[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\AccountDashboard',
                        'action' => 'index',
                    ),
                ),
            ),
            'account-classes-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account-classes-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\AccountClasses',
                        'action' => 'saveAccountClass',
                    ),
                ),
            ),
            'income-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/income-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\Income',
                        'action' => 'saveIncome',
                    ),
                ),
            ),
            'account-dashboard-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account-dashboard-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\AccountDashboard',
                        'action' => 'saveAccountClass',
                    ),
                ),
            ),
            'accounts' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/accounts[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\Accounts',
                        'action' => 'create',
                    ),
                ),
            ),
            'accounts-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/accounts-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\Accounts',
                        'action' => 'saveAccounts',
                    ),
                ),
            ),
            'groups' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/groups[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\Groups',
                        'action' => 'create',
                    ),
                ),
            ),
            'groups-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/groups-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\Groups',
                        'action' => 'saveGroups',
                    ),
                ),
            ),
            'journal-entries' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/journal-entries[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\JournalEntries',
                        'action' => 'create',
                    ),
                ),
            ),
            'journal-entries-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/journal-entries-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\JournalEntries',
                        'action' => 'saveJournalEntry',
                    ),
                ),
            ),
            'journal-entry-type' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/journal-entry-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\JournalEntryType',
                        'action' => 'create',
                    ),
                ),
            ),
            'journal-entry-type-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/journal-entry-type-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\JournalEntryType',
                        'action' => 'saveJournalEntryType',
                    ),
                ),
            ),
            'fiscal-period' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/fiscal-period[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\FiscalPeriod',
                        'action' => 'list',
                    ),
                ),
            ),
            'fiscal-period-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/fiscal-period-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\FiscalPeriod',
                        'action' => 'saveFiscalPeriod',
                    ),
                ),
            ),
            'gl-account-setup' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/gl-account-setup[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\GlAccountSetup',
                        'action' => 'index',
                    ),
                ),
            ),
            'gl-account-setup-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/gl-account-setup-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\GlAccountSetup',
                        'action' => 'saveGlAccountSetup',
                    ),
                ),
            ),
            'year-end-closing' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/year-end-closing[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\YearEndClosing',
                        'action' => 'index',
                    ),
                ),
            ),
            'year-end-closing-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/year-end-closing-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\YearEndClosing',
                        'action' => 'proceedYearEndClosing',
                    ),
                ),
            ),
            'budget' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/budget[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\Budget',
                        'action' => 'index',
                    ),
                ),
            ),
            'budget-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/budget-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Accounting\Controller\API\Budget',
                        'action' => 'proceedYearEndClosing',
                    ),
                ),
            ),
        // The following is a route to simplify getting started creating
        // new controllers and actions without needing to create a new
        // module. Simply drop new controllers in, and you can access them
        // using the path /application/:controller/:action
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
               'Accounting\Controller\AccountClasses' => 'Accounting\Controller\AccountClassesController',
               'Accounting\Controller\Income' => 'Accounting\Controller\IncomeController',
               'Accounting\Controller\AccountDashboard' => 'Accounting\Controller\AccountDashboardController',
               'Accounting\Controller\API\AccountClasses' => 'Accounting\Controller\API\AccountClassesController',
               'Accounting\Controller\API\Income' => 'Accounting\Controller\API\IncomeController',
               'Accounting\Controller\API\AccountDashboard' => 'Accounting\Controller\API\AccountDashboardController',
               'Accounting\Controller\Accounts' => 'Accounting\Controller\AccountsController',
               'Accounting\Controller\API\Accounts' => 'Accounting\Controller\API\AccountsController',
               'Accounting\Controller\Groups' => 'Accounting\Controller\GroupsController',
               'Accounting\Controller\API\Groups' => 'Accounting\Controller\API\GroupsController',
               'Accounting\Controller\YearEndClosing' => 'Accounting\Controller\YearEndClosingController',
               'Accounting\Controller\API\YearEndClosing' => 'Accounting\Controller\API\YearEndClosingController',
               'Accounting\Controller\JournalEntries' => 'Accounting\Controller\JournalEntriesController',
               'Accounting\Controller\API\JournalEntries' => 'Accounting\Controller\API\JournalEntriesController',
               'Accounting\Controller\JournalEntryType' => 'Accounting\Controller\JournalEntryTypeController',
               'Accounting\Controller\API\JournalEntryType' => 'Accounting\Controller\API\JournalEntryTypeController',
               'Accounting\Controller\FiscalPeriod' => 'Accounting\Controller\FiscalPeriodController',
               'Accounting\Controller\API\FiscalPeriod' => 'Accounting\Controller\API\FiscalPeriodController',
               'Accounting\Controller\GlAccountSetup' => 'Accounting\Controller\GlAccountSetupController',
               'Accounting\Controller\API\GlAccountSetup' => 'Accounting\Controller\API\GlAccountSetupController',
               'Accounting\Controller\Budget' => 'Accounting\Controller\BudgetController',
               'Accounting\Controller\API\Budget' => 'Accounting\Controller\API\BudgetController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'accounting' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(

            ),
        ),
    ),
);
