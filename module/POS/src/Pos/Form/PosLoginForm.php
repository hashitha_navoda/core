<?php

namespace Pos\Form;

use Zend\Form\Form;

class PosLoginForm extends Form
{

    public function __construct($params)
    {
        parent::__construct('PosLogin');
        $this->setAttribute('method', 'post');
        $this->setAttribute('data-ng-submit', 'authenticate(\'login\')');

        $this->add(array(
            'name' => 'openingBalance',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'class' => 'form-control currency',
                'placeholder' => 'Enter your opening balance',
                'id' => 'openingBalance',
                'data-ng-model' => 'openingBalance',
                'data-ng-change' => "validate('openingBalance')",
                'required' => 'required'
            ),
            'validators' => array(
                array(
                    'name' => 'Float',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'closingBalance',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'disabled' => 'disabled',
                'class' => 'form-control currency',
                'placeholder' => 'Enter your closing balance',
                'id' => 'openingBalance',
                'data-ng-model' => 'closingBalance',
                'ng-change' => "validate('price')"
//                'required' => 'required'
            ),
            'validators' => array(
                array(
                    'name' => 'Float',
                ),
            ),
        ));

        $this->add(array(
            'name' => 'login',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Login',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary',
//                'data-ng-click' => "authenticate('login')"
            ),
        ));

        $this->add(array(
            'name' => 'logout',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Logout',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary',
//                'data-ng-click' => "authenticate('logout')"
            ),
        ));
    }

}
