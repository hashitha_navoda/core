<?php

namespace Pos\Service;

use Core\Service\BaseService;
use Pos\Model\Pos;

class PosService extends BaseService 
{

	public function createPosInvoice($data, $locationID)
	{
		if($this->validateUnPayedInvoice($data['invoiceData']['invoice']['invoiceId'])){
			$pos = $this->getServiceLocator()->get("PosController");
            $result = $pos->savePosInvoice($data['invoiceData'], true, $locationID, $data['posBalanceData'], $data['userID']);

        	if ($result['status']) {
        		return $this->returnSuccess($result['data'], $result['msg']);
        	} else {
				return $this->returnError($result['msg'],$result['data']);        		
        	}
        }else{
        	return $this->returnError('ERR_POS_PAYMENT_DUPLICATE');
        }

	}

	public function validateUnPayedInvoice($invoicedID)
    {
        $invoicedDetails = $this->getModel('InvoiceTable')->getInvoiceStatusByInvoiceID($invoicedID)->current();
        if(!empty($invoicedDetails) && $invoicedDetails['statusID'] == '4'){
            return false;
        }
        return true;
    }

    public function posAuthenticate($data, $locationID)
    {

        $openingBalance = $data['openingBalance'];
        $closingBalance = $data['closingBalance'];
        $action = $data['action'];

        if (is_null($openingBalance) && is_null($closingBalance)) {
            return $this->returnError('ERR_POS_VALIDAMOU');
        }

        $userID = $data['userID'];
        $locationID = $locationID;

        if ($action == 'login') {
            $result = $this->_storeLogInDetails($openingBalance, $userID, $locationID);
            
            if ($result['status']) {
                $posCurrentID = $result['data']['lastInsertedID'];
                $posLogInDateTime = $result['data']['logInDateTime'];
                $posUserLogin = true;
                $posOpeningBalance = $openingBalance;
            }

            $data = [
                'posCurrentID' => $posCurrentID,
                'posLogInDateTime' => $posLogInDateTime,
                'posUserLogin' => true,
                'posOpeningBalance' => $posOpeningBalance
            ];
        } else if ($action == 'logout') {
            $posCurrentID = $data['posCurrentID'];
            $posLogInDateTime = $data['posLogInDateTime'];
            $this->_storeLogOutDetails($closingBalance, $userID, $posLogInDateTime, $posCurrentID, $locationID, $openingBalance);
        }

        return $this->returnSuccess($data,'SUCC_POS_LOG');
    }

    public function _storeLogInDetails($openingBalance, $userID, $locationID)
    {
        $logInDateTime = $this->getGMTDateTime()['data']['currentTime'];
        $data = array(
            'userID' => $userID,
            'locationID' => $locationID,
            'openingBalance' => $openingBalance,
            'closingBalance' => '',
            'logInDateTime' => $logInDateTime,
            'logOutDateTime' => '',
        );

        if (!is_null($openingBalance)) {
            $posTable = $this->getModel('PosTable');
            $pos = new Pos();
            $pos->exchangeArray($data);
            $lastInsertedID = $posTable->saveLogInPos($pos);
//          Add to location Cash In Hand

            $currentLocationDetails = $this->getModel('LocationTable')->getLocationCashInHandAmount($locationID)->current();
            $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] + $openingBalance;
            $this->getModel('LocationTable')->updateLocationAmount($locationID, $newLocationCashInHand);
            
            $data = [
                'logInDateTime' => $logInDateTime,
                'lastInsertedID' => $lastInsertedID
            ];

            return ['status'=> true,'data'=> $data];
        }

        return ['status'=> false];
    }

    public function _storeLogOutDetails($closingBalance, $userID, $posLogInDateTime, $posCurrentID, $locationID, $openingBalance)
    {
        $logOutDateTime = $this->getGMTDateTime()['data']['currentTime'];

        $data = array(
            'posID' => $posCurrentID,
            'userID' => $userID,
            'locationID' => $locationID,
            'openingBalance' => $openingBalance,
            'closingBalance' => $closingBalance,
            'logInDateTime' => $posLogInDateTime,
            'logOutDateTime' => $logOutDateTime,
        );

        if (!is_null($closingBalance)) {
            $posTable = $this->getModel('PosTable');
            $pos = new Pos();
            $pos->exchangeArray($data);
            $posTable->saveLogOutPos($pos);
        }
    }
}