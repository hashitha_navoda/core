<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 * this for pos login logout details
 */

namespace Pos\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Pos implements InputFilterAwareInterface
{

    public $posID;
    public $userID;
    public $locationID;
    public $openingBalance;
    public $closingBalance;
    public $logInDateTime;
    public $logOutDateTime;
    public $inputFilter;

    public function exchangeArray($data)
    {
        $this->posID = (!empty($data['posID'])) ? $data['posID'] : 0;
        $this->userID = (!empty($data['userID'])) ? $data['userID'] : 0;
        $this->openingBalance = (!empty($data['openingBalance'])) ? $data['openingBalance'] : 0;
        $this->closingBalance = (!empty($data['closingBalance'])) ? $data['closingBalance'] : 0;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->logInDateTime = (!empty($data['logInDateTime'])) ? $data['logInDateTime'] : null;
        $this->logOutDateTime = (!empty($data['logOutDateTime'])) ? $data['logOutDateTime'] : null;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'openingBalance',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
                        'attributes' => array(
                            'required' => 'required'
                        )
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

}
