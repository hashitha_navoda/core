<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 * this for pos login logout details
 */

namespace Pos\Model;

use Pos\Model\Pos;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;

class PosTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getPosBalanceData($fromDate = null, $toDate = null, $userIds = NULL, $locIds = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pos');
        $select->columns(array('*'));
        $select->join('user', 'user.userID = pos.userID', array('*'), 'left');
        $select->order(array('pos.posID' => 'DESC'));
        if ($fromDate != NULL && $toDate != NULL) {
            // $select->where->like('pos.logInDateTime', '%' . $fromDate . '%');
            $select->where->between('pos.logInDateTime', $fromDate, $toDate);
        }
        if ($userIds != NULL) {
            $select->where->in('pos.userID', $userIds);
        }
        if ($locIds != NULL) {
            $select->where->in('pos.locationID', $locIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * This will store the details of pos login and logout
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param \Pos\Model\Pos $pos
     */
    public function saveLogInPos(Pos $pos)
    {
        $data = array(
            'userID' => $pos->userID,
            'locationID' => $pos->locationID,
            'openingBalance' => $pos->openingBalance,
            'closingBalance' => $pos->closingBalance,
            'logInDateTime' => $pos->logInDateTime,
            'logOutDateTime' => $pos->logOutDateTime
        );

        $this->tableGateway->insert($data);
        $lastInsertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $lastInsertedID;
    }

    /**
     * This update the log out details
     * @param \Pos\Model\Pos $pos
     */
    public function saveLogOutPos(Pos $pos)
    {

        $data = array(
            'closingBalance' => $pos->closingBalance,
            'logOutDateTime' => $pos->logOutDateTime
        );

        try {
            $this->tableGateway->update($data, array(
                'posID' => $pos->posID,
                'locationID' => $pos->locationID,
            ));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public function getSessionTotalIncome($from, $to, $user)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('totalIncome' => new Expression('sum(incomingPayment.incomingPaymentAmount)')))
                ->join('entity', 'entity.entityID =  salesInvoice.entityID')
                ->join('incomingInvoicePayment', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID',array('*'),'left')
                ->join('incomingPayment','incomingInvoicePayment.incomingPaymentID = incomingPayment.incomingPaymentID',array('*'),'left')
                ->join('creditNote','salesInvoice.salesInvoiceID = creditNote.invoiceID',array('*'),'left')
                ->join('creditNotePaymentDetails','creditNote.creditNoteID = creditNotePaymentDetails.creditNoteID', 
                        array('totalCreditNotePayedValue' => new Expression('sum(creditNotePaymentDetails.creditNotePaymentDetailsAmount)')),'left')
                ->where(array("entity.createdBy" => $user,"salesInvoice.pos" => 1))
        ->where->between('entity.createdTimeStamp', $from, $to);
        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();
        return $results;
    }

    /**
     * Get Location Wise pos
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @return type
     */
    public function getPosByLocation($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('pos')
                ->columns(['*'])
                ->where(['locationID' => $locationID]);
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getPosClosingBalance($time, $locationID, $user)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('pos')
                ->columns(array('*'))
                ->where(array('userID' => $user,'locationID' => $locationID, 'logInDateTime' => $time));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateClosingBalance($finBalance,$time, $locationID, $user)
    {
        
        $data = array(
            'closingBalance' => $finBalance
        );
        try {
            $this->tableGateway->update($data, array('userID' => $user,'locationID' => $locationID, 'logInDateTime' => $time));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return TRUE;
    }

}
