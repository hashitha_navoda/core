<?php

namespace Pos\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;

/**
 *
 * PosAuthenticateRestfullController
 *
 *
 * Restfull controller for products
 */
class PosAuthenticateRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList()
    {
       
    }

    public function get($id)
    {
        return new JsonModel(['status' => true, 'data' => null, 'msg' => 'successfuly done']);
    }

    public function create($data)
    {
        $this->service = $this->getService('PosService');
        $locationID = $this->getRequest()->getHeaders()->get("Locationid")->getFieldValue();
        //customer add transaction begin
        $this->beginTransaction();
        $posAuthResult = $this->service->posAuthenticate($data, $locationID);
        if ($posAuthResult['status']) {
            $this->commit();
            return $this->returnJsonSuccess($posAuthResult['data'], $posCreateResult['msg']);
        }

        $this->rollback();
        return $this->returnJsonError($this->getMessage($posAuthResult['msg']));
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
