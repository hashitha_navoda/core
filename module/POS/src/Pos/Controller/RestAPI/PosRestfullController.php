<?php

namespace Pos\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;

/**
 *
 * PosRestfullController
 *
 *
 * Restfull controller for products
 */
class PosRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList()
    {
       
    }

    public function get($id)
    {
        return new JsonModel(['status' => true, 'data' => null, 'msg' => 'successfuly done']);
    }

    public function create($data)
    {
        $this->service = $this->getService('PosService');
        $locationID = $this->getRequest()->getHeaders()->get("Locationid")->getFieldValue();
        //customer add transaction begin
        $this->beginTransaction();
        $posCreateResult = $this->service->createPosInvoice($data, $locationID);
        if ($posCreateResult['status']) {
            $this->commit();
            return $this->returnJsonSuccess($posCreateResult['data'], $posCreateResult['msg']);
        }

        $this->rollback();
        return $this->returnJsonError($this->getMessage($posCreateResult['msg']));
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
