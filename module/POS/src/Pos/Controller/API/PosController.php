<?php

/**
 * Description of PosController
 *
 * @author User <prathap@thinkcube.com>
 */

namespace Pos\Controller\API;

use Core\Controller\CoreController;
use Pos\Model\Pos;
use Invoice\Model\Customer;
use Settings\Model\LocationTemplate;
use DateTime;
use DateTimeZone;

class PosController extends CoreController
{

    public $userID;
    public $locationID;
    private $_lastInsertedID;
    private $_logInDateTime;

    private $updatedProductsIds;

    public function getProductsDetailsAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_POS_INVLDREQ');
            return $this->JSONRespond();
        }

        $timestamp = $req->getPost('timestamp');
        $locationID = $this->user_session->userActiveLocation['locationID'];

        $productService = $this->getService('ProductService');
        $productDetails = $productService->getAllProductsForPOS($locationID, $timestamp);

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_POS_PRODUCT_DETAILS');
        $this->data = ['products_details' => $productDetails['data']];
        return $this->JSONRespond();
    }

    public function getUserPermissionAction(){
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_POS_INVLDREQ');
            return $this->JSONRespond();
        }

        $uname = $req->getPost('username');
        $upass = $req->getPost('pass');
        
        // get user
        $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($uname);

        if ($user) {
            // if user not admin or deleted user
            if($user->roleID != 1 || $user->deleted){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_USERAPICONTRO_INCUSRNAME');
                return $this->JSONRespond();
            }

            $passStr = explode(':', $user->userPassword);
            $passHash = $passStr[0];
            $newHash = md5($upass . $passStr[1]);

            if($passHash == $newHash){
                $this->status = true;
                $this->msg = $this->getMessage('INFO_USERAPICONTRO_AUTH');
                return $this->JSONRespond();
            }

        }
        // if user dose not exists
        $this->status = false;
        $this->msg = $this->getMessage('ERR_VIEWCRNOTEPAY_INVALID_USRNAME_PWD');
        return $this->JSONRespond();
    }


    public function changeSettingsAction(){
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_POS_INVLDREQ');
            return $this->JSONRespond();
        }
        
        $settingName = $req->getPost('setting');
        $attributes = $req->getPost('value');
        $locationId = $this->user_session->userActiveLocation['locationID'];
        $userId = $this->user_session->userID;

        $config = $this->getServiceLocator()->get('config');
        $setting = $config['settings'][$settingName];
        $this->CommonTable('SettingTable')->saveSettings($setting, $attributes, $locationId, $userId);

        $this->msg = $this->getMessage('SUCC_POS_STNG_' . $settingName);
        $this->status = true;
        return $this->JSONRespond();
    }

    public function changeLocationTemplateAction(){
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_POS_INVLDREQ');
            return $this->JSONRespond();
        }

        $data = array(
            'locationID' => $this->user_session->userActiveLocation['locationID'],
            'documentTypeID' => $req->getPost('documentTypeID'),
            'templateID' => $req->getPost('templateID')
        );

        $locationTplTable = $this->CommonTable('Settings\Model\LocationTemplateTable');
        $rowTplIDs = $locationTplTable->getLocationTemplate($data['locationID'], $data['documentTypeID']);

        if ($rowTplIDs->count()) {
            $locationTplTable->updateLocationTemplate($data, $data['locationID'], $data['documentTypeID']);
        }else{
            $locationTpl = new LocationTemplate();
            $locationTpl->exchangeArray($data);
            $locationTplTable->saveLocationTemplate($locationTpl);
        }

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_LOC_TPL_CHG');
        return $this->JSONRespond();
    }

    /**
     * This function make user log in or log out acording to the action parameter.
     * Keep updating session openig balance and closing balance.
     * @return state
     * @return msg Description
     * @return data Array {ob : float, logout : bool}
     */
    public function authenticateAction()
    {
        $req = $this->getRequest();

        if ($req->isPost()) {
            $openingBalance = $req->getPost('openingBalance');
            $closingBalance = $req->getPost('closingBalance');
            $action = $req->getPost('action');
            $logout = false;

            if (is_null($openingBalance) && is_null($closingBalance)) {
                $this->msg = $this->getMessage('ERR_POS_VALIDAMOU');
                $this->status = false;
                return $this->JSONRespond();
            }

            $this->userID = $this->user_session->userID;
            $this->locationID = $this->user_session->userActiveLocation['locationID'];

            if ($action == 'login') {
                $this->_storeLogInDetails($openingBalance);
                $this->user_session->posCurrentID = $this->_lastInsertedID;
                $this->user_session->posLogInDateTime = $this->_logInDateTime;
                $this->user_session->posUserLogin = true;
                $this->user_session->posOpeningBalance = $openingBalance;
            } else if ($action == 'logout') {
                $logout = true;
                $this->user_session->posUserLogin = false;
                $this->_storeLogOutDetails($closingBalance);
            }
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_POS_LOG');
            $this->data = array('ob' => $openingBalance, 'logout' => $logout);
            return $this->JSONRespond();
        }
        $this->status = false;
        $this->msg = $this->getMessage('ERR_POS_LOG');
        return $this->JSONRespond();
    }

    /**
     * this store the pos login details
     * @param Int $openingBalance starting balance
     */
    private function _storeLogInDetails($openingBalance)
    {
        $this->_logInDateTime = $this->getGMTDateTime();

        $data = array(
            'userID' => $this->userID,
            'locationID' => $this->locationID,
            'openingBalance' => $openingBalance,
            'closingBalance' => '',
            'logInDateTime' => $this->_logInDateTime,
            'logOutDateTime' => '',
        );

        if (!is_null($openingBalance)) {
            $posTable = $this->CommonTable('Pos\Model\PosTable');
            $pos = new Pos();
            $pos->exchangeArray($data);

            $this->_lastInsertedID = $posTable->saveLogInPos($pos);
//          Add to location Cash In Hand
            $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($this->locationID)->current();
            $newLocationCashInHand = $currentLocationDetails['locationCashInHand'] + $openingBalance;
            $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($this->locationID, $newLocationCashInHand);
        }
    }

    /**
     * this update the pos logout details to correspoding row
     * @param Int $closingBalance starting balance
     */
    private function _storeLogOutDetails($closingBalance)
    {
        $logOutDateTime = $this->getGMTDateTime();

        $data = array(
            'posID' => $this->user_session->posCurrentID,
            'userID' => $this->user_session->userID,
            'locationID' => $this->user_session->userActiveLocation['locationID'],
            'closingBalance' => $closingBalance,
            'logInDateTime' => $this->user_session->posLogInDateTime,
            'logOutDateTime' => $logOutDateTime,
        );

        if (!is_null($closingBalance)) {
            $posTable = $this->CommonTable('Pos\Model\PosTable');
            $pos = new Pos();
            $pos->exchangeArray($data);
            $posTable->saveLogOutPos($pos);
        }
    }

    public function createAction()
    {
        $request = $this->getRequest();
        if ($request->isPost() && $this->params()->fromRoute('param1') == 'bulk') {
            $req = $request->getPost();
            
            $invoice =$this->CommonTable('Invoice\Model\InvoiceTable');

            foreach ($req as $orderID => $orderData) {
                if($invoice->isOfflineInvoiceExists($orderData['tempCode'])){
                    $result['data'][$orderID] = true;
                }else{
                    $res = $this->savePosInvoice($orderData);
                    $result['data'][$orderID] = $res['status'];
                }
            }
        } else if ($request->isPost()) {

            $req = $request->getPost();
            if($this->validateUnPayedInvoice($req['invoice']['invoiceId'])){
                $result = $this->savePosInvoice($req);  
            }else{
                $result = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_POS_PAYMENT_DUPLICATE'),
                );
            }
        } else {
            $result = array(
                'status' => false,
                'msg' => '',
                'data' => ['invID' => null, 'payID' => null]
            );
        }

        $this->status = $result['status'];
        $this->msg = $result['msg'];
        $this->data = $result['data'];

        return $this->JSONRespond();
    }

    public function savePosInvoice($req, $restFlag = false, $locationID, $posBalanceData, $posUserID)
    {
        if ($restFlag) {
            $locationID = $locationID;            
        } else {
            $locationID = $this->user_session->userActiveLocation['locationID'];
        }
        $this->beginTransaction();

        if($req['posMode'] != 'pay'){
            $storeInvoice  = $this->invoiceSave($req, $locationID, $restFlag, $posUserID);
            if (!$storeInvoice['status']) {
                $this->rollback();
                return array(
                    'status' => false,
                    'msg' => $storeInvoice['msg'],
                    'data' => array(
                        'invID' => null,
                        'payID' => null,
                    )
                );
            }
            // update customer current balance.
            $req['cust_balance'] += $req['invoice']['finTotal']; 
            
            // Invoice saving process end here
            // If pos mode is "Invoice Only" (inv)
            //  - do not save payment details
            //  - do not update customer
            //  - do not update closing balance
            //  - terminate the function
            //  - return success message of invoice saving
            //  - commit transaction

            if($req['posMode'] == 'inv'){
                $this->commit();

                $eventParameter = ['productIDs' => $this->updatedProductsIds, 'locationIDs' => [$locationID]];
                $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

                return array(
                    'status' => true,
                    'msg' => $this->getMessage('SUCC_POS_SAVE'),
                    'data' => array(
                        'invID' => $storeInvoice['data']['salesInvoiceID'],
                        'invCode' => $storeInvoice['data']['salesInvoiceCode'],
                        'payID' => $res['id'],
                        'opBal' => $this->user_session->posOpeningBalance,
                        'clBal' => $this->user_session->posOpeningBalance,
                        'income' => $this->user_session->posSessionInconme,
                    )
                );
            }
        }

        if($req['posMode'] == 'pay'){
            $storeInvoice['totalPrice'] = $req['invoice']['finTotal'];
            $storeInvoice['data']['salesInvoiceID'] = $req['invoice']['invoiceId'];
            $storeInvoice['data']['salesInvoiceCode'] = $req['invoice']['invoiceCode'];

        }

        $res = $this->paymentSave($storeInvoice, $req, $locationID, $restFlag, $posUserID);
        if (!$res['state']) {
            $this->rollback();

            return array(
                'status' => false,
                'msg' => $res['msg'],
                'data' => array(
                    'invID' => null,
                    'payID' => null,
                )
            );
        }

        $posTable = $this->CommonTable('Pos\Model\PosTable');
        if (!$restFlag) {
            $invTotal = $posTable->getSessionTotalIncome($this->user_session->posLogInDateTime, $this->getGMTDateTime(), $this->userID);
            $total = $invTotal->current();
            $this->user_session->posSessionInconme = $total['totalIncome'];
            $closingBal = ($total['totalIncome'] + $this->user_session->posOpeningBalance) - $total['totalCreditNotePayedValue'];

            $this->_storeLogOutDetails($closingBal);
        } else {
            $invTotal = $posTable->getSessionTotalIncome($posBalanceData['posLogInDateTime'], $this->getGMTDateTime(), $posUserID);
            $total = $invTotal->current();
            $posSessionInconme = $total['totalIncome'];
            $closingBal = ($total['totalIncome'] + $posBalanceData['posOpeningBalance']) - $total['totalCreditNotePayedValue'];
        }

        $this->commit();

        if($req['posMode'] != 'pay'){
            $eventParameter = ['productIDs' => $this->updatedProductsIds, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);
        }

        if ($restFlag) {
            return array(
                'status' => true,
                'msg' => $this->getMessage('SUCC_POS_SAVE'),
                'data' => array(
                    'invID' => $storeInvoice['data']['salesInvoiceID'],
                    'invCode' => $storeInvoice['data']['salesInvoiceCode'],
                    'payID' => $res['id'],
                    'posSessionInconme' => $posSessionInconme,
                    'clBal' => $closingBal,
                )
            );
        } else {
            return array(
                'status' => true,
                'msg' => $this->getMessage('SUCC_POS_SAVE'),
                'data' => array(
                    'invID' => $storeInvoice['data']['salesInvoiceID'],
                    'invCode' => $storeInvoice['data']['salesInvoiceCode'],
                    'payID' => $res['id'],
                    'opBal' => $this->user_session->posOpeningBalance,
                    'clBal' => $closingBal,
                    'invome' => $this->user_session->posSessionInconme,
                )
            );
        }
    }

    function invoiceSave($invData, $locationID, $restFlag, $posUserID){

        // TODO - refactor getLocationReferenceDetails in CoreCtrler
        $referenceData = $this->getLocationReferenceDetails('3', $locationID);
        if ($referenceData == FALSE) {
            $referenceData = $this->getLocationReferenceDetails('3', NULL);
        }
        
        $refNumber = $this->getReferenceNumber($referenceData->locationReferenceID);
        if ($refNumber == '' || $refNumber == NULL) {
            if ($referenceData->locationReferenceID == null) {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_DELINOTECON_ADD_REF')
                );
            } else {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_DELINOTECON_CHANGE_REF_MAX_REACH')
                );
            }
        }
        $locationRefID = $referenceData->locationReferenceID;
        $custID = $invData['cust_id'];
        $productsForNotific = [];
        $this->updatedProductsIds = [];
        foreach ($invData['invoice']['cart'] as $index => $item) {
            if ($invData['invoice']['inclusiveTax'] == "true") {
                $productPrice = $item['inclusiveTaxpR'] / $item['pUomUc'];
                $inclusiveTaxProductPrice = $item['pR'] / $item['pUomUc'];
            } else {
                $productPrice = $item['pR'] / $item['pUomUc'];
                $inclusiveTaxProductPrice = null;
            }
            $products[] = array(
                'productID' => $item['pID'],
                'productCode' => $item['pC'],
                'deliverQuantity' => array('qty' => $item['qty'] * $item['pUomUc']),
                'productPrice' => $productPrice,
                'productDiscount' => $item['dType'] == 'precentage' ? $item['dAmo'] : $item['disc']/($item['qty'] * $item['pUomUc']),
                'productDiscountType' => $item['dType'],
                'productTotal' => $item['pTotal'],
                'productType' => $item['pT'],
                'pTax' => $item['tax'],
                'stockUpdate' => 'true',
                'giftCard' => $item['giftCard'],
                'locationProductID' => $item['lPID'],
                'locationID' => $locationID,
                'selectedUomId' => $item['uomOptions']['value']['id'],
                'inclusiveTaxProductPrice' => $inclusiveTaxProductPrice
            );
            $productsForNotific[] = $item['pID'];
            $this->updatedProductsIds[] = $item['pID'];
        }
        if ($invData['invoice']['discountSelect'] == 'pre') {
            $discType = 'presentage';
        } else {
            $discType = 'Value';
        }

        $totalDiscountAmount = (!empty($invData['invoice']['discountAmount'])) ? $invData['invoice']['discountAmount'] : 0;
        
        $products['subProduct'] = $invData['subProduct'];
        $invSaveData = array(
            'products' => $products,
            'invoiceCode' => $refNumber,
            'locationOutID' => $locationID,
            'invoiceDate' => $this->getGMTDateTime("Y-m-d"),
            'dueDate' => $this->getGMTDateTime("Y-m-d"),
            'deliveryCharge' => 0,
            'invoiceTotalPrice' => $invData['invoice']['finTotal'],
            'invoiceComment' => $invData['invoice']['comment'],
            'paymentTermID' => 1,
            'showTax' => true,
            'ignoreBudgetLimit' => true,
            'deliveryNoteID' => null,
            'salesOrderID' => null,
            'customerID' => $custID,
            'invoiceTotalDiscount' => $totalDiscountAmount,
            'salesInvoicePayedAmount' => 0,
            'subProducts' => $invData['subProduct'],
            'promotionID' => $invData['promotion']['promoID'],
            'promotionDiscountValue' => $invData['promotion']['totalDiscount'],
            'invTempCode' => $invData['tempCode'],
            'offlineSavedTime' => null,
            'offlineUser' => null,
            'invoiceDiscountType' => $discType,
            'invoiceDiscountRate' => $invData['invoice']['discount'],
            'discountOnlyOnEligible' => filter_var($invData['invoice']['discountOnlyOnEligible'], FILTER_VALIDATE_BOOLEAN),
            'inclusiveTax' => filter_var($invData['invoice']['inclusiveTax'], FILTER_VALIDATE_BOOLEAN)
        );
        if($invData['issueDateTime']){
            $date = new DateTime($invData['issueDateTime'], new DateTimeZone($this->timeZone));

            $invSaveData['invoiceDate'] = $date->format('Y-m-d');
            $invSaveData['dueDate'] = $date->format('Y-m-d');
            $invSaveData['offlineSavedTime'] = $date->format('Y-m-d H:i:s');
            $invSaveData['offlineUser'] = $invData['userID'];
        }
        $invSaveData['posMode'] = $invData['posMode'];

        $invoice = $this->getServiceLocator()->get("InvoiceController");
        $invSaveState = $invoice->storeInvoice($invSaveData, $custID, $pos = true, false, $restFlag, $posUserID);

        if($invSaveState['status']){
            $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($custID);
            $custNewCurrentBalance = $cust->customerCurrentBalance + $invSaveState['totalPrice'];
            $custData = array(
                'customerCurrentBalance' => $custNewCurrentBalance,
                'customerID' => $custID
            );
            $customerAllData = new Customer;
            $customerAllData->exchangeArray($custData);

            $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);
        }


        if ($invSaveState['status']) {
           $invSaveState['data']['invoiceDate'] = $invSaveData['invoiceDate'];
        }
        return $invSaveState;

    }

    function paymentSave($invData, $paymentData, $locationID, $restFlag, $posUserID){
        $customerID = $paymentData['cust_id'];
        if($restFlag == true && $customerID == null) {
            $customerID = 0;
        }
                      
        $paymentRefData = $this->getLocationReferenceDetails('4', $locationID);
        if ($paymentRefData == FALSE) {
            $paymentRefData = $this->getLocationReferenceDetails('4', NULL);
        }

        $locationRefID = $paymentRefData->locationReferenceID;
        $payRefNumber = $this->getReferenceNumber($paymentRefData->locationReferenceID);
        if ($payRefNumber == '' || $payRefNumber == NULL) {
            if ($paymentRefData->locationReferenceID == null) {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_DELINOTECON_ADD_REF')
                );
            } else {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_PAY_CHANGE_REF_MAX_REACH')
                );
            }
        }
        $paymentEntity = $this->createEntity();

        $methods = array();

        $paidAmount = 0;
        if ($paymentData['payment']['cash'] > 0) {
            $paidAmount += $paymentData['payment']['cash'];
            $methods[] = array(
                'methodID' => 1,
                'paidAmount' => $paymentData['payment']['cash'],
            );
        }
        if ($paymentData['payment']['cardPayment'] > 0) {
            $paidAmount += $paymentData['payment']['cardPayment'];

            $methods[] = array(
                'methodID' => 3,
                'paidAmount' => $paymentData['payment']['cardPayment'],
                'reciptnumber' => $paymentData['payment']['cardRef'],
                'cardnumber' => $paymentData['payment']['cardNo'],
                'cardType' => $paymentData['payment']['cardType']
            );
        }
        if ($paymentData['payment']['redeem_value'] > 0) {
            $paidAmount += $paymentData['payment']['redeem_value'];
            $methods[] = array(
                'methodID' => 4,
                'paidAmount' => $paymentData['payment']['redeem_value'],
            );
        }
        if (isset($paymentData['payment']['giftCards'])) {
            foreach ($paymentData['payment']['giftCards'] as $card) {
                if ($restFlag) {
                    $methods[] = array(
                        'methodID' => 6,
                        'paidAmount' => $card['giftCardValue'],
                        'giftCardID' => $card['giftCardId'],
                    );
                    $paidAmount += $card['giftCardValue'];
                } else {
                    $methods[] = array(
                        'methodID' => 6,
                        'paidAmount' => $card['paidAmount'],
                        'giftCardID' => $card['giftCardID'],
                    );
                    $paidAmount += $card['paidAmount'];
                }
            }
        }
        if ($paymentData['payment']['chequePayment'] > 0) {
            $paidAmount += $paymentData['payment']['chequePayment'];
            $methods[] = array(
                'methodID' => 2,
                'paidAmount' => $paymentData['payment']['chequePayment'],
                'checkNumber' => $paymentData['payment']['chequeRef'],
            );
        }
        if ($paymentData['payment']['uniformVoucherPayment'] > 0) {
            $paidAmount += $paymentData['payment']['uniformVoucherPayment'];
            $methods[] = array(
                'methodID' => 9,
                'paidAmount' => $paymentData['payment']['uniformVoucherPayment'],
                'voucherNumber' => $paymentData['payment']['uniformVoucherNo'],
            );
        }
        $creditValue = '';
        //add credit values 
        if(isset($paymentData['payment']['creditNotePayment']) && $paymentData['payment']['creditNotePayment'] > 0)
        {
            if($paymentData['invoice']['finTotal'] < $paymentData['payment']['creditNotePayment']){
                $creditValue = $paymentData['invoice']['finTotal'];
                $creditValueExceed = true;
            }else{
                $creditValue = $paymentData['payment']['creditNotePayment'];
                $creditValueExceed = false;
            }
        }
        
        //check this invoice has any promotion discounts
        $discountAmo;
        if(isset($paymentData['invoice']['promotionDiscount']) && $paymentData['invoice']['promotionDiscount'] != 0){
            $discountAmo = $paymentData['invoice']['promotionDiscount'];
        } else {
            $discountAmo = 0;
        }
        $paymentData = array(
            'customerID' => $customerID,
            'invData' => '{"' . $invData['data']['salesInvoiceID'] . '":"' . $paymentData['invoice']['finTotal'] . '"}',
            'paymentID' => $payRefNumber,
            'date' =>  ($paymentData['posMode'] == 'nor') ? $invData['data']['invoiceDate'] : $this->getGMTDateTime("Y-m-d"),
            'paymentTerm' => '1',
            'amount' => $paymentData['invoice']['finTotal'],
            'discount' => $discountAmo,
            'memo' => '',
            'paymentType' => 'invoice',
            'customerCredit' => $paymentData['cust_credit'],
            'customerBalance' => $paymentData['cust_balance'],
            'locationID' => $locationID,
            'creditAmount' => $creditValue,
            'paymentMethods' => $methods,
            'pos' => true,
            'balance' => $paymentData['payment']['balance'],
            'totalPaidAmount' => $paidAmount,
            'loyaltyData' => $paymentData['loyaltyData'],
            'cardType' => $paymentData['payment']['cardType'],
            'payedCreditNoteID' => $paymentData['payedCreditNoteID'],
            'creditValueExceed' => $creditValueExceed,
            'ignoreBudgetLimit' => true

        );
        $payment = $this->getServiceLocator()->get("PaymentsAPI");
        return $payment->makePayment($paymentData, $restFlag, $posUserID);
        
    }

    // Get loyalty card
    function getCustomerLoyaltyCardAction()
    {
        $req = $this->getRequest();
        if (!$req->isPost()) {
            $this->msg = $this->getMessage('ERR_POS_INVLDREQ');
            $this->status = false;
            return $this->JSONRespond();
        }

        $customerLoyalty = $this->CommonTable('CustomerLoyaltyTable');
        if ($req->getPost('loyaltyCardNo')) {

            $loyalty_card = $customerLoyalty->getLoyaltyCardByCardNo($req->getPost('loyaltyCardNo'));
        } elseif ($req->getPost('phoneNo')) {

            $loyalty_card = $customerLoyalty->getLoyaltyCardByPhoneNo($req->getPost('phoneNo'));
        } else {
            $this->msg = $this->getMessage('ERR_POS_LOYALTYFLDEMPTY');
            $this->status = false;
            return $this->JSONRespond();
        }

        if (!$loyalty_card) {
            $this->msg = $this->getMessage('ERR_POS_NOLOYALTCRD');
            $this->status = false;
            return $this->JSONRespond();
        } else {
            if (!$loyalty_card['loyaltyStatus']) {
                $this->msg = $this->getMessage('ERR_POS_LOYALTYCRDDEACTIVE');
                $this->status = false;
                return $this->JSONRespond();
            }
            
            $this->data = array(
                'lid' => $loyalty_card['loyaltyID'],
                'lcode' => $loyalty_card['customerLoyaltyCode'],
                'lname' => $loyalty_card['loyaltyName'],
                'lactive_min' => $loyalty_card['loyaltyActiveMinVal'],
                'lactive_max' => $loyalty_card['loyaltyActiveMaxVal'],
                'ladd' => $loyalty_card['loyaltyEarningPerPoint'],
                'lredeem' => $loyalty_card['loyaltyRedeemPerPoint'],
                'cust_id' => $loyalty_card['customerID'],
                'cust_lylt_id' => $loyalty_card['customerLoyaltyID'],
                'cust_name' => $loyalty_card['customerTitle'] . ' ' . ucfirst($loyalty_card['customerName']),
                'cust_lpoints' => $loyalty_card['customerLoyaltyPoints']
            );
            $this->status = true;
            return $this->JSONRespond();
        }
    }
    /**
    * use to get POS related invoice details for invoice search 
    *
    */

    function searchDataForPOSCreditNoteAction()
    {
        $req = $this->getRequest();
        if (!$req->isPost()) {
            $this->msg = $this->getMessage('ERR_POS_CRNTE_RETRIVE');
            $this->status = false;
            return $this->JSONRespond();
        }
        $value = ($req->getPost('data')) ?$req->getPost('data'): 'invoice';
        if($value == 'invoice'){
             //if selection type invoice then load all pos invoice ids.
            $posInvoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getAllPOSInvoices();
            $posInvoiceKeys = [];
            foreach($posInvoiceDetails as $details){
                $posInvoiceKeys[] =$details; 
            }
            $this->status = true;
            $this->data = $posInvoiceKeys;
            return $this->JSONRespond();
        }
    
    }
    /**
    * get All related details that given invoice id
    * return JSONRespond
    */
     public function getDataForPOSCreditNoteAction()
    {
        $req = $this->getRequest();
        if (!$req->isPost()) {
            $this->msg = $this->getMessage('ERR_POS_INV_ID');
            $this->status = false;
            return $this->JSONRespond();
        }
        $invoiceID = $req->getPost('data');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $promotionDetails = $this->CommonTable('Settings\Model\PromotionTable')
            ->getPromotionDetailsByInvoiceID($invoiceID);
        $creditNoteProducts = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
            ->getCreditNoteProductsByInvoiceID($invoiceID);
        $posInvoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')
            ->getInvoicedPOSDetails($invoiceID);
                 
        $allPOSInvoiceDetails = [];
        $invoicedProducts = [];
        $invoicedSubProducts = [];
        $invoicedBatchProducts = [];
        $invoicedSerialProducts = [];
        $invoicedSerialBatch = [];
        $relatedCreditNoteDetails = [];
      

        foreach($creditNoteProducts as $CNPro){
                $relatedCreditNoteDetails[$CNPro['productID']]['creditNoteProductQuantity'] += $CNPro['creditNoteProductQuantity'];
                $relatedCreditNoteDetails[$CNPro['productID']]['creditNoteProductTotal'] += $CNPro['creditNoteProductTotal']; 
                $relatedCreditNoteDetails[$CNPro['productID']]['invoiceProductID'] = $CNPro['invoiceProductID']; 
        }

        foreach($posInvoiceDetails as $details) {
            //check that item return or not
            if($details['salesInvoiceProductQuantity'] <= $relatedCreditNoteDetails[$details['productID']]['creditNoteProductQuantity']){
                continue;
            }

            $invoicedSubProducts[] = array(
                'subProID' => $details['salesInvoiceSubProductID'],
                'subProQty' => $detials['salesInvoiceSubProductQuantity'],
            );

            $invoicedProducts[$details['productID']] = array(
                'salesInvoiceProID' => $details['salesInvoiceProductID'], 
                'pID' => $details['productID'],
                'pC' => $details['productCode'],
                'prodDiscription' => $details['salesInvoiceProductDescription'],
                'pR' => $details['salesInvoiceProductPrice'],
                'inclusiveTaxpR' => $details['inclusiveTaxSalesInvoiceProductPrice'],
                // 'pR' => $details['salesInvoiceProductPrice']*$details['productUomConversion'],
                'qty' => ($details['salesInvoiceProductQuantity'] - $relatedCreditNoteDetails[$details['productID']]['creditNoteProductQuantity'])/$details['productUomConversion'],
                'pTotal' => $details['salesInvoiceProductTotal'] - $relatedCreditNoteDetails[$details['productID']]['creditNoteProductTotal'],
                'proDiscount' => $details['salesInvoiceProductDiscount'],
                'proDiscType' => $details['salesInvoiceProductDiscountType'],
                'proTax' => $details['salesInvoiceProductTax'],
                'pN' => $details['productName'],
                'disc' => 100,
                'selectedUom' => $details['salesInvoiceProductSelectedUomId']
            );

            if($details['batchProduct'] == '1'){
                    
                $creditNoteSubProduct = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                    ->getCreditNoteProductAndSubProductDataByInvoiceProductID(
                        $relatedCreditNoteDetails[$details['productID']]['invoiceProductID']);
                $bQty = $details['salesInvoiceSubProductQuantity'];   
                if(count($creditNoteSubProduct)>0){
                    foreach ($creditNoteSubProduct as $key => $value) {
                        if($details['productBatchID'] == $value['productBatchID']){
                            $bQty = $details['salesInvoiceSubProductQuantity']-$value['creditNoteSubProductQuantity'];
                        }
                           
                    } 
                }

                $invoicedBatchProducts[$details['productBatchID']] = array(
                    'proBatchID' => $details['productBatchID'],
                    'PBC' => $details['productBatchCode'],
                    'PID' => $details['productID'],
                    'selected' => 1,
                    'batchQty' => $bQty,
                    'tempBatchQty' => $bQty,
                    'returnQty' =>''
                ); 
               
                       
                $invoicedProducts[$details['productID']]['proType'] = 'b';
                $invoicedProducts[$details['productID']]['products'] = $invoicedBatchProducts; 
            }
            // add serial products to the data array    
            if($details['serialProduct'] == '1'){
                $creditNoteSubProduct = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                    ->getCreditNoteProductAndSubProductDataByInvoiceProductID(
                        $relatedCreditNoteDetails[$details['productID']]['invoiceProductID']);
                $creditNoteSerialSubItemFlag = false;
            
                foreach($creditNoteSubProduct as $CNSubPro){
                    if($CNSubPro['productSerialID'] == $details['productSerialID']){
                        $creditNoteSerialSubItemFlag = true;
                    }
                }
            
                if(!$creditNoteSerialSubItemFlag){
                    $invoicedSerialProducts[$details['productSerialID']] = array(
                        'proSerialID' => $details['productSerialID'],
                        'PSC' => $details['productSerialCode'],
                        'PID' => $details['productID'],
                        'selected' => 1,
                        'warranty' => $details['productSerialWarrantyPeriod']
                    );
                }
                $invoicedProducts[$details['productID']]['proType'] = 's';
                $invoicedProducts[$details['productID']]['products'] = $invoicedSerialProducts;
                    
            }
            // add batch-serial item to the array
            if($details['serialProduct'] == '1' && $details['batchProduct'] == '1'){
                $invoicedProducts[$details['productID']]['proType'] = 'bS';
                $creditNoteSubProduct = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                    ->getCreditNoteProductAndSubProductDataByInvoiceProductID(
                        $relatedCreditNoteDetails[$details['productID']]['invoiceProductID']);
                $creditNoteBatchSerialSubItemFlag = false;
        
                foreach($creditNoteSubProduct as $CNSubPro){
                    if($CNSubPro['productSerialID'] == $details['productSerialID']){
                        $creditNoteBatchSerialSubItemFlag = true;
                    }
                }

                if(!$creditNoteBatchSerialSubItemFlag){
                    $invoicedSerialBatch[$details['productSerialID']] = array(
                        'proBatchID' => $details['productBatchID'],
                        'PBC' => $details['productBatchCode'],
                        'proSerialID' => $details['productSerialID'],
                        'PSC' => $details['productSerialCode'],
                        'PID' => $details['productID'],
                        'selected' => 1,
                        'warranty' => $details['productSerialWarrantyPeriod']
                    );
                }
                $invoicedProducts[$details['productID']]['products'] = $invoicedSerialBatch;
            }
                    
            $allPOSInvoiceDetails = array(
                'invID' => $details['salesInvoiceID'],
                'invCode' => $details['salesInvoiceCode'],
            );
            $allPOSInvoiceDetails['item'] = $invoicedProducts;
            $allPOSInvoiceDetails['totalDiscType'] = $details['salesInvoiceTotalDiscountType'];
            $allPOSInvoiceDetails['totalDiscAmo'] = $details['salesInvoiceDiscountRate'];
            $allPOSInvoiceDetails['remainingDiscValue'] = $details['salesInvoiceRemainingDiscValue'];
            $allPOSInvoiceDetails['promotionDetails'] = $promotionDetails;
            $allPOSInvoiceDetails['remainingPromotionDiscValue'] = $details['salesInvoiceRemainingPromotionDiscValue'];
            $allPOSInvoiceDetails['invTotalValue'] = $details['salesinvoiceTotalAmount'];
         
        }

        if(!count($invoicedProducts) > 0){
            $this->status = false;
            return $this->JSONRespond();
        }
        $this->data = $allPOSInvoiceDetails;
        $this->status = true;
        return $this->JSONRespond();
    }

    /**
    * save POS credit notes 
    * return JSONRespond
    */
    public function savePOSCreditNoteAction()
    {
        $req = $this->getRequest();
        if (!$req->isPost()) {
            $this->msg = $this->getMessage('ERR_POS_CRDNOTE_SAVE');
            $this->status = false;
            return $this->JSONRespond();
        }
        $genaralData = $req->getPost('genaralData');
        $productDetails = $req->getPost('productDetails');
        $invoiceID = $req->getPost('invID');

        $custID = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicedCustomerIdByInvoiceID($invoiceID)->current();
       
        //create data set for save credit note table
        $saveType = $req->getPost('type');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $creditNoteDataSet = array(
            'customerID' => $custID['customerID'],
            'locationID' => $locationID,
            'paymentTermID' => 1,
            'invoiceID' => $invoiceID,
            'creditNoteDate' => $this->convertDateToStandardFormat($this->getGMTDateTime()),
            'creditNoteTotal' => $genaralData['finTotal'],
            'creditNoteComment' => $comment,
            'statusID' => 3,
            'creditNotePaymentEligible' => 1,
            'customCurrencyId' => 0,
            'creditNotePaymentAmount' => $genaralData['finTotal'],
            'creditNoteCustomCurrencyRate' => 1,
            'entityID' => $this->createEntity(),
            'pos' => 1,
        );
        
        $products = $productIds = [];
        $subProducts = [];
    
        foreach($genaralData['cart'] as $pro){
            $quantity = array(
                'qty' => $pro['qty']*$pro['pUomUc'],
            );

            $productIds[] = $pro['pID'];
              
            $products []=array(
                'productID' => $pro['pID'],
                'invoiceproductID' => $req->getPost('productDetails')[$pro['pID']]['salesInvoiceProID'],
                'productCode' => $pro['pC'],
                'productName' => $pro['pN'],
                'productPrice' => $pro['pR']/$pro['pUomUc'],
                'productDiscount' => $pro['dAmo'],
                'productDiscountType' => $pro['dType'],
                'productTotal' => $pro['pTotal'],
                'productType' => $pro['pT'],
                'creditNoteQuantity' => $quantity,
                'pTax' => $pro['tax'],
                'giftCard' => $pro['giftCard'] 
            );
        
            //add batch items to the array
            if($productDetails[$pro['pID']]['proType'] == 'b') {
                foreach($productDetails[$pro['pID']]['products'] as $subPro){
                    if($subPro['PID'] == $pro['pID']){
                        $subProducts[$pro['pID']][] = array(
                            'batchID' => $subPro['proBatchID'],
                            'serialID' => null,
                            'qtyByBase' => (!empty($subPro['batchQty']))? $subPro['batchQty']: 0, 
                        );        
                    }
                }
            }
            //add batch serial items to the array
            else if($productDetails[$pro['pID']]['proType'] == 'bS') {
                foreach($productDetails[$pro['pID']]['products'] as $subPro){
                    if($subPro['selected'] != 0){
                        $subProducts[$pro['pID']][] = array(
                            'batchID' => $subPro['proBatchID'],
                            'serialID' => $subPro['proSerialID'],
                            'qtyByBase' => 1, 
                        );          
                    }
                }
            }
            //add serial items to the array
            else if($productDetails[$pro['pID']]['proType'] == 's') {
                foreach($productDetails[$pro['pID']]['products'] as $subPro){
                    if($subPro['selected'] != 0){
                        $subProducts[$pro['pID']][] = array(
                            'batchID' => null,
                            'serialID' => $subPro['proSerialID'],
                            'qtyByBase' => 1, 
                        );          
                    }
                }
            }
        }
        
        $validate = $this->validateCreditNote($invoiceID,$genaralData,$subProducts);
        if(!$validate){
            $this->msg = $this->getMessage('ERR_POS_DUPLICATE_CRDNOTE_SAVE');
            $this->status = false;
            return $this->JSONRespond();   
        }
       

        $this->beginTransaction();  
        $creditNote = $this->getServiceLocator()->get("CreditNoteController");
        $saveCreditNote = $creditNote->saveCreditNote($creditNoteDataSet,$products,$subProducts,$locationID);       
        if(!$saveCreditNote['status']){
            $this->rollback();
            $this->status = false;
            $this->data = $saveCreditNote;
            $this->msg = $saveCreditNote['msg'];
            return $this->JSONRespond();
        }

        //update customer credit balance
        if($saveType == 'save'){
            $customerID = $custID['customerID'];  
            $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID); 
            $cCurrentCredit = $customer->customerCurrentCredit;
            $newCreditBalance = $cCurrentCredit + $genaralData['finTotal'];

            $customerupdate = array(
                'customerID' => $customerID,
                'customerCurrentCredit' => $newCreditBalance,
            );
            $customerData = new Customer;
            $customerData->exchangeArray($customerupdate);
            $res = $customerUpdateResult = $this->CommonTable('Invoice\Model\CustomerTable')
                ->updateCustomerCredit($customerData);
            if(!$res){
                $this->rollback();
                $this->status = false;
                $this->data = $saveCreditNote;
                $this->msg = $this->getMessage('ERR_CUS_CREDIT_UPDATE');
                return $this->JSONRespond();
            }
        }
        // calcuate invoice discount and update invoice table remainingdiscount column.
        if($genaralData['discountSelect'] == 'val'){
            if(is_null($genaralData['existingDiscValue'])){
                $remValue = $genaralData['realDiscount'] - $genaralData['discount'];    
            }
            else{
                $remValue = $genaralData['existingDiscValue'] - $genaralData['discount'];   
            }
                  
            $updateInvoiceDiscountValue = $this->CommonTable('Invoice\Model\InvoiceTable')
                ->updateInvoiceRemainingDiscountValue($req->getPost('invID'),$remValue);
        }
        //calculate promotion discount and update invoice table remainingpromdiscount column
        if($req->getPost('promoData') && $req->getPost('promoType') == 'val'){
            if($req->getPost('promoRemainingValue') == ''){
                $promRemValue = $req->getPost('promoData')['invoicePromotionDiscountAmount'] - $req->getPost('promotionValue'); 
              
            }
            else{
                $promRemValue = $req->getPost('promoRemainingValue') - $req->getPost('promotionValue');
                  
            }
            $updatePromDiscountValue = $this->CommonTable('Invoice\Model\InvoiceTable')
                ->updatePromotionRemainingDiscountValue($req->getPost('invID'),$promRemValue);
        }
        $saveCreditNote['user'] = $this->user_session->username;
        $this->commit();

        $eventParameter = ['productIDs' => $productIds, 'locationIDs' => [$locationID]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        $this->status = true;
        $this->data = $saveCreditNote;
        return $this->JSONRespond();
       
    }
    /**
    * get all pos credit notes for payment process
    * return JSONRespond
    */
    public function getCreditNoteDetailsAction()
    {
        $req = $this->getRequest();
        if (!$req->isPost()) {
            $this->msg = $this->getMessage('ERR_POS_CRNTE_RETRIVE');
            $this->status = false;
            return $this->JSONRespond();
        }
        $customerID = $req->getPost('cusID');
        $cusIDs = [];
        $creditNoteLists = [];
        //add to default customer id and selected customer id into the array
        if($customerID){
            $cusIDs[] = 0;
            $cusIDs[] = $customerID;
        }
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $creditNoteList = $this->CommonTable('Invoice\Model\CreditNoteTable')
            ->getActiveCreditNote($locationID,1, $cusIDs);
        if(!count($creditNoteList)>0){
            $this->status =false;
            return $this->JSONRespond();
        }
        foreach ($creditNoteList as $key => $value) {
            //calcuate remaining credit value
            $value['creditNoteTotal'] = $value['creditNoteTotal']- $value['creditNoteSettledAmount'];
            $creditNoteLists [$value['creditNoteID']] = $value;
        }
        $this->status = true;
        $this->data = $creditNoteLists;
        return $this->JSONRespond();
    }
    /**
    * update POS closing balance when do credit note payment in pos 
    * return JSONRespond
    */
    
    public function updateClosingBalanceAction()
    {
        $req = $this->getRequest();
        if (!$req->isPost()) {
            $this->msg = $this->getMessage('ERR_POS_CRNTE_PAYMENT');
            $this->status = false;
            return $this->JSONRespond();
        }

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userID = $this->userID;
        $logInDate = $this->user_session->posLogInDateTime;
        $closingBalance = $this->CommonTable('Pos\Model\PosTable')
            ->getPosClosingBalance($logInDate, $locationID, $userID)->current();
        $creditNoteValue = $req->getPost('creditNoteTotal');
        $creditNoteID = $req->getPost('creditNoteID');

        $creditNoteRow = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNoteID);
        $creditNote = $creditNoteRow->current();
        $customerID = $creditNote['customerID'];

        //update creditNote payment table
        $creditNotePayment = $this->getServiceLocator()->get("CreditNotePaymentController");
        $respond = $creditNotePayment->saveCreditNotePaymentByPOS($creditNoteValue, $creditNoteID,$locationID, $customerID);
        if(!$respond['status']){
            $this->status = false;
            $this->msg =$this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
       
        //update credit note table 
        
        $payment = $this->getServiceLocator()->get("PaymentsAPI");
        $res = $payment->updateCreditNoteByPayments($creditNoteValue, $creditNoteID);
       
        //update closing balance
        $finBalance = $closingBalance['closingBalance'] - $creditNoteValue;
         
        $updatePosClosingBalance = $this->CommonTable('Pos\Model\PosTable')
            ->updateClosingBalance($finBalance,$logInDate, $locationID, $userID);
        $this->status = true;
        $this->data = array(
            'code' => $respond['paymentCode'],
            'user' => $this->user_session->username,
            'clblance' => $finBalance
            );
        return $this->JSONRespond();
    }

    /**
     * Check browser's timestamp against server's timestamp
     * @return Array Returns error message if there is a differant (more than 30s) between in timstamps.
     */
    public function checkDateTimeAction()
    {
        $timeStamp = $this->getRequest()->getPost('timeStamp');
        $this->status = true;
        $serverTime = time();

        if(abs($serverTime-$timeStamp) > 30){
            $this->msg = $this->getMessage('ERR_POS_INVLDDATTIME');
            $this->status = false;
        }


        return $this->JSONRespond();
    }


    public function getUnpaidPosInvoicesAction()
    {
        $lastID = $this->getRequest()->getPost('lastID');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $rowInvoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getOpenPosInvoices($lastID, $locationID);

        $invoices = [];
        
        while ($inv = $rowInvoices->current()) {
                $invoices[] = array(
                    'id' => $inv["salesInvoiceID"],
                    'code' => $inv["salesInvoiceCode"],
                    'cust_id' => $inv['customerID'],
                    'issueDate' => $inv["salesInvoiceIssuedDate"],
                    'invTotal' => $inv["salesinvoiceTotalAmount"],
                    'invDiscType' => $inv["salesInvoiceTotalDiscountType"],
                    'invDiscRate' => $inv["salesInvoiceDiscountRate"],
                    'invDiscVal' => $inv["salesInvoiceWiseTotalDiscount"],
                    'invPromoDisc' => $inv["salesInvoicePromotionDiscount"],
                );
        }

        $this->status = $invoices ? true : false;
        $this->data = $invoices;

        return $this->JSONRespond();

    }

    public function getInvoiceProductsAction()
    {
        if(!$this->getRequest()->isPost()){
            $this->status = false;

            return $this->JSONRespond();
        }

        $invId = $this->getRequest()->getPost("invoiceID");
        $validateValue = $this->validateUnPayedInvoice($invId);
        if(!$validateValue){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_POS_PAYMENT_DUPLICATE');
            return $this->JSONRespond();            
        }

        $products = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getProductsByInvoiceID($invId, true);

        while ($product = $products->current()) {
            $proList[] = $product;
        }
        // echo "<pre>" . print_r($proList, true) . "</pre>"; exit;

        $this->status = true;
        $this->data = $proList;
        return $this->JSONRespond();
    }

    /**
    * Validate that given details releted to the invoice id
    * @param int $invoiceID
    * @param array $creditNoteData
    * @param array $subProducts
    * return boolean  
    **/
    public function validateCreditNote($invoiceID,$creditNoteData,$subProducts)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $creditNoteProducts = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
            ->getCreditNoteProductsByInvoiceID($invoiceID);
        $posInvoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')
            ->getInvoicedPOSDetails($invoiceID);
              
      
        $savedCreditNoteDetails = [];
        $currentCreditNoteDetails = [];
        $creditNoteSubPro = [];
        $creditNoteSubProBatchIDs = [];
        $creditNoteSubProSerialIDs = [];
        $invoicedItemQty = [];
       
        foreach ($creditNoteData['cart'] as $creditNoteProduct) {
            $currentCreditNoteDetails[$creditNoteProduct['pID']] = $creditNoteProduct['qty'];
        }
                
        $validateFlag = true;
        foreach($creditNoteProducts as $CNPro){
            if($CNPro['locationID'] == $locationID) {
                $savedCreditNoteDetails[$CNPro['productID']] += $CNPro['creditNoteProductQuantity'];
            }
            //get subproduct details that related to given CreditNoteProductID
            $creditNoteSubProduct = $this->CommonTable('Invoice\Model\CreditNoteSubProductTable')
                                            ->getCreditNoteSubProductByCreditNoteID($CNPro['creditNoteProductID']);
            if(count($creditNoteSubProduct) > 0){
                foreach ($creditNoteSubProduct as $subProduct) {
                    $creditNoteSubProBatchIDs[$subProduct['productBatchID']] = $subProduct['creditNoteSubProductQuantity'];
                    $creditNoteSubProSerialIDs[] = $subProduct['productSerialID'];
                }
                foreach($subProducts[$CNPro['productID']] as $notSavedSubProducts){
                    //validate serial items/ batch-serial items
                    if($notSavedSubProducts['serialID'] != null && 
                            in_array($notSavedSubProducts['serialID'], $creditNoteSubProSerialIDs)){
                        $validateFlag = false;
                    
                    }else if($notSavedSubProducts['batchID'] != null && 
                            array_key_exists($notSavedSubProducts['batchID'], $creditNoteSubProBatchIDs) && 
                            $notSavedSubProducts['serialID'] == null){ 
                        //get credited batch products that related to the given invoiceID
                        $creditNoteExistingsubBatchPro = $this->CommonTable('Invoice\Model\CreditNoteSubProductTable')
                                ->getCreditNoteSubProductQtyByInvoiceIDAndProductID(
                                    $invoiceID,
                                    $notSavedSubProducts['batchID'],
                                    $CNPro['creditNoteProductID']
                                    );
                        //get invoiced batch products that related to the given invoiceID
                        $invoiceExistingSubBAtchPro = $this->CommonTable('Invoice\Model\InvoiceSubProductTable')
                                ->getSalesInvoiceSubProductQtyByInvoiceIDAndProductID(
                                    $invoiceID,
                                    $notSavedSubProducts['batchID'],
                                    $CNPro['invoiceProductID']
                                    );
                        //validate batch items
                        if($invoiceExistingSubBAtchPro[0]['sum'] < $creditNoteExistingsubBatchPro[0]['sum'] + $notSavedSubProducts['qtyByBase']){
                            $validateFlag = false;     
                        }
                        
                    }
                }  
            }
        }

          foreach($posInvoiceDetails as $details) {
            $invoicedItemQty[$details['productID']] = $details['salesInvoiceProductQuantity']; 
            //valiadete normal products
            if($details['salesInvoiceProductQuantity'] < ($savedCreditNoteDetails[$details['productID']]+ $currentCreditNoteDetails[$details['productID']])){
                $validateFlag = false;
            }
        }
        return $validateFlag;
    }

    /**
    * validate UnPayed invoices when selection time
    * @param int $invoiceID
    * return boolean
    **/
    public function validateUnPayedInvoice($invoicedID)
    {
        $invoicedDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceStatusByInvoiceID($invoicedID)->current();
        if(!empty($invoicedDetails) && $invoicedDetails['statusID'] == '4'){
            return false;
        }
        return true;
    }

    /**
    * validate Unpayed invoices when pay
    * return JSONRespond
    **/
    public function validateUnpayedInvoiceSaveAction()
    {
        if(!$this->getRequest()->isPost()){
            $this->status = false;
            return $this->JSONRespond();
        }

        $invId = $this->getRequest()->getPost("invoiceID");
        $validateInvoice = $this->validateUnPayedInvoice($invId);
        if(!$validateInvoice){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_POS_PAYMENT_DUPLICATE');
            return $this->JSONRespond();
        }
        $this->status = true;
        return $this->JSONRespond();
    }
}
