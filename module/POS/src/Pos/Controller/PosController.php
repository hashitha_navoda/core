<?php

/**
 * Description of PosController
 *
 * @author User <prathap@thinkcube.com>
 */

namespace Pos\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Pos\Form\PosLoginForm;
use Inventory\Form\ProductForm;
use Inventory\Form\ProductHandelingForm;
use Inventory\Form\CategoryForm;
use Zend\I18n\Translator\Translator;

class PosController extends CoreController
{
   

    public function createAction()
    {

        $useAccounting = $this->user_session->useAccounting;
        $locationID = $this->user_session->userActiveLocation['locationID'];

        $refData = $this->getReferenceNoForLocation(3, $locationID);
        if ($refData !== NULL) {
            $refData = $this->getReferenceNoForLocation(4, $locationID);
        }

        $isLogin = $this->user_session->posUserLogin == TRUE ? TRUE : FALSE;
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'taxes',
            'getGiftCard',
            'categories',
            'uomByID',
            'customerListWithoutDefaultCustomer',
            'wsport',
            'customerDefaultAccounts'
        ]);

        $this->layout('pos/layouts/pos-layout.phtml');

        $open = isset($this->user_session->posOpeningBalance) ? $this->user_session->posOpeningBalance : 0;
        $income = isset($this->user_session->posSessionInconme) ? $this->user_session->posSessionInconme : 0;

        $loginForm = new PosLoginForm(array('closingBalance' => $open + $income));

        $promotions = $this->CommonTable('Settings\Model\PromotionTable')->getToDayPromotions(date("Y-m-d"), $locationID);

        $location = $this->CommonTable('Core\Model\LocationTable')->getLocation($locationID);

        $cardTypes = $this->CommonTable('Expenses\Model\CardTypeTable')->getAssignedCardTypes();
        $cardTypeList = [];
        foreach ($cardTypes as $cardType) {
            $cardTypeList[$cardType['cardTypeID']] = $cardType['cardTypeName'];
        }

        $tpl = $this->CommonTable('Settings\Model\TemplateTable');
        $templates = $tpl->getTemplateNamesForDocTypes(['POS Printout', 'POS Credit Note', 'POS (invoice only)', 'POS (pay only)']);

        while ($row = $templates->current()) {
            $tpl_list[$row['documentTypeName']][] = array(
                'id' => $row['templateID'],
                'name' => $row['templateName'],
                'doc_id' => $row['documentTypeID'],
                'default' => $row['templateDefault']
            );
        }

        // set location templates
        $locationTemplates = array();
        $locTpl = $this->CommonTable('Settings\Model\LocationTemplateTable');

        // 8 - pos printoutn
        // 24 - pos creditnote
        // 25 - pos (invoice only)
        // 26 - pos (pay only)
        $rowTplIDs = $locTpl->getLocationTemplate($locationID, [8, 24, 25, 26]);
        if($rowTplIDs->count()){
            foreach ($rowTplIDs as $row) {
                $locationTemplates[$row['documentTypeID']] = $row['templateID'];
            }
        }

        // Get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];
        $countries = $config['all_countries'];
        $countries[0] = 'Select a country';

        // Get Loyalty Card list
        $loyalty = $this->CommonTable("LoyaltyTable")->getLoyaltyList();
        $translator = new Translator();
        $loyaltyCardList[0] = $translator->translate("-- Select Loyalty Card --");
        while ($l = $loyalty->current()) {
            $loyaltyCardList[$l->id] = $l->name;
        }

        // Get payment terms list
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        foreach ($paymentTerms as $row) {
            $id = (int) $row->paymentTermID;
            $terms[$id] = $row->paymentTermName;
        }

        // Get payment terms list

        $priceListData[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $priceListData = $priceListData + $existingCustomerPriceLists;

        // Get Active Customer Evnt for customer creation form
        $customerEvents = $this->CommonTable('Settings\Model\CustomerEventTable')->getActiveCutomerEvents();
        $customerEvenList = [];
        $customerEvenList[''] = '';
        foreach ($customerEvents as $cEvent) {
            $customerEvenList[$cEvent['customerEventID']] = $cEvent['customerEventName'];
        }

        // Get currencys list
        $allCurrency = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencyList = array();
        $currencyList['0'] = '';
        foreach ($allCurrency as $row) {
            $currencyList[$row->currencyID] = $row->currencyName;
        }

        //Get customer category list
        $allCustomerCategory = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategoryList = array();
        $customerCategoryList['0'] = '';
        foreach ($allCustomerCategory as $key => $row) {
            $customerCategoryList[$key] = $row;
        }
        
        // discount only on eligible
        $disSetting = $this->getSetting('INV_DISCOUNT');

        // inclusive tax on eligible
        $inclusiveTaxSetting = $this->getSetting('INCLUSIVE_TAX');

        // override item price
        $itemPriceSetting = $this->getSetting('OVERRIDE_ITEM_PRICE');

         // price_list settings
        $priceListSetting = $this->getSetting('ALLOW_PRICE_LIST');


        $priceListState = true;
        $priceListPriceEditState = true;
        if ($this->user_session->roleID != 1) {
            $priceListState = $priceListSetting->allow_price_list_pos;
            $priceListPriceEditState = $priceListSetting->allow_edit_price_list_price;
        }


        // override item discount
        $itemDiscountSetting = $this->getSetting('OVERRIDE_ITEM_DISCOUNT');

        $allAccountsDetails = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAllActiveAccounts();
        $acconts = [];
        foreach ($allAccountsDetails as $acc) {
            $accounts[$acc['financeAccountsID']] = $acc['financeAccountsCode']."_".$acc['financeAccountsName'];
        }

        $tax = array();
        $ctax = array();
        $tax_resource = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
        foreach ($tax_resource as $t) {
            $tax[$t->id] = $t->taxName;
            if ($t->taxType == 'c') {
                $ctax[$t->id] = $t->taxName;
            }
        }
        $locationDetails = array();
        $locationsArr = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
        foreach ($locationsArr as $r) {
            $r = (object) $r;
            $locationDetails[$r->locationID] = $r->locationName;
        }

        $productType = array();
        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $sup = array();
        $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAll();
        foreach ($supplier as $r) {
            $sup[$r->supplierID] = $r->supplierName;
        }
        $productForm = new ProductForm(array(
            'supplier' => $sup,
            'tax' => $tax,
            'productType' => $productType,
            'location' => $locationDetails,
        ));

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $productSalesAccountName = $financeAccountsArray[$productSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productSalesAccountID]['financeAccountsName'];
                $productForm->get('productSalesAccountID')->setAttribute('data-id',$productSalesAccountID);
                $productForm->get('productSalesAccountID')->setAttribute('data-value',$productSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $productInventoryAccountName = $financeAccountsArray[$productInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productInventoryAccountID]['financeAccountsName'];
                $productForm->get('productInventoryAccountID')->setAttribute('data-id',$productInventoryAccountID);
                $productForm->get('productInventoryAccountID')->setAttribute('data-value',$productInventoryAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $productCOGSAccountName = $financeAccountsArray[$productCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productCOGSAccountID]['financeAccountsName'];
                $productForm->get('productCOGSAccountID')->setAttribute('data-id',$productCOGSAccountID);
                $productForm->get('productCOGSAccountID')->setAttribute('data-value',$productCOGSAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $productAdjusmentAccountName = $financeAccountsArray[$productAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productAdjusmentAccountID]['financeAccountsName'];
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-id',$productAdjusmentAccountID);
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-value',$productAdjusmentAccountName);
            }
        }

        $productForm->get('submit')->setValue('Save');
        $productForm->get('productState')->setValue('Active');
        $productForm->setAttribute('id', 'create-product-form');

        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $defaultCategoryID = array_search('General', $allCategoryList);
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        $defaultUomID = $this->CommonTable('Settings\Model\UomTable')->getUomByName('Unit')->uomID;
        $categoryForm = new CategoryForm($allCategoryList);
        $categoryForm->get('categoryParentID')->setAttribute('data-id', $defaultCategoryID);
        $productHandelingForm = new ProductHandelingForm();
        $productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", true);
        return new ViewModel(
                // $refData = NULL means no referance data sets for ither invoice or payment
                array(
            'loginForm' => $loginForm,
            'isLogin' => $isLogin,
            'closeBalance' => $open + $income,
            'requireRef' => $refData == NULL ? TRUE : FALSE,
            'currency' => $this->companyCurrencySymbol,
            'dateFormat' => $this->getUserDateFormat(),
            'timeFormat' => $this->getUserTimeFormat(),
            'promotions' => $promotions,
            'locationTax' => $location->locationTax,
            'userID' => $this->user_session->userID,
            'cardTypes' => $cardTypeList,
            'countries' => $countries,
            'customerTitle' => $customerTitle,
            'loyaltyCardList' => $loyaltyCardList,
            'paymentTerms' => $terms,
            'customerEvenList' => $customerEvenList,
            'currencyList' => $currencyList,
            'customerCategoryList' => $customerCategoryList,
            'tpl_list' => $tpl_list,
            'locationTemplates' => $locationTemplates,
            'dis_on_eligible' => $disSetting->discount_only_eligible_items,
            'inclusive_tax_on_eligible' => $inclusiveTaxSetting->inclusive_tax,
            'override_price' =>  $itemPriceSetting->override_item_price,
            'alow_price_list' =>  $priceListState,
            'allow_edit_price_list_price' => $priceListPriceEditState,
            'override_discount' =>  $itemDiscountSetting->override_item_discount,
            'useAccounting' => $this->user_session->useAccounting,
            'priceListData' => $priceListData,
            'accounts' => $accounts,
            'defaultAcc' => $defaultAcc,
            'productForm' => $productForm,
            'productHandelingForm' => $productHandelingForm,
            'categoryForm' => $categoryForm,
            'uomList' => $uoms,
            'tax' => $tax,
            'ctax' => $ctax,
            'location' => $locationDetails,
            'defaultUom' => $defaultUomID,
            'useAccounting' => $useAccounting,
            'locID' => $locationID
        ));
    }

    public function getSetting($settingName){
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $userID = $this->user_session->userID;

        $config = $this->getServiceLocator()->get('config');
        $setting = $config['settings'][$settingName];

        $res = $this->CommonTable('SettingTable')->getSettings($setting, $locationID, $userID);

        if($res){
            $obj = json_decode($res->settingAttribute);
        }else{
            $obj = json_decode($setting['default']);
        }
        return $obj;
    }
}
