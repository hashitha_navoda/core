<?php

namespace Pos;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Pos\Model\Pos;
use Pos\Model\PosTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;

class Module
{


    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $this->getConfig();

        $defaultLanguage = $config['defaultLanguage'];

        if (session_id() == '')
            @session_start();

        $translator = $e->getApplication()->getServiceManager()->get('translator');

        if (isset($_SESSION['ezBizUser']['lang'])) {
            $translator->setLocale($_SESSION['ezBizUser']['lang']);
        }
        else {
            $translator->setLocale($defaultLanguage);
        }

    }


    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );

    }


    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';

    }


    public function getServiceConfig()
    {
        return array(
            'factories' => array(
                'Pos\Model\PosTable' => function ($sm) {
                                            $tableGateway = $sm->get('PosTableGateway');
                                            $table = new PosTable($tableGateway);
                                            return $table;
                                        },
                'PosTableGateway' => function ($sm) {
                                        $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                                        $resultSetPrototype = new ResultSet();
                                        $resultSetPrototype->setArrayObjectPrototype(new Pos());
                                        return new TableGateway('pos', $dbAdapter, null, $resultSetPrototype);
                                    },
            ),
            'invokables' => array(
                'ProductService' => 'Core\Service\ProductService',
                'PosService' => 'Pos\Service\PosService',
                'PosController' => 'Pos\Controller\API\PosController',
            ),
            'shared' => array(
                'ProductService' => true,
                'PosService' => true
            ),
            'aliases' => array(
                'PosTable' => 'Pos\Model\PosTable',
            ),
        );

    }


}
