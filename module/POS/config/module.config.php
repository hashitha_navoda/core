<?php

return array(
    'controllers' => array(
        'invokables' => array(
            'Pos\Controller\Pos' => 'Pos\Controller\PosController',
            'Pos\Controller\Pos\API' => 'Pos\Controller\API\PosController',
            'Pos\Controller\RestAPI\Pos' => 'Pos\Controller\RestAPI\PosRestfullController',
            'Pos\Controller\RestAPI\PosAuthenticate' => 'Pos\Controller\RestAPI\PosAuthenticateRestfullController',
        ),
    ),
    'router' => array(
        'routes' => array(
            'pos' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Pos\Controller\Pos',
                        'action' => 'create',
                    ),
                ),
            ),
            'pos-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/pos-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param1' => '[a-zA-Z][a-zA-Z0-9_-]*',
                        'param2' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Pos\Controller\Pos\API',
                        'action' => 'authenticate',
                    ),
                ),
            ),
            'pos-invoice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/pos-invoice[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Pos\Controller\RestAPI\Pos'
                    ),
                ),
            ),
            'pos-authenticate' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/rest-api/pos-authenticate[/:id]',
                    'constraints' => array(
                        'id' => '[0-9]+',
                    ),
                    'defaults' => array(
                        'controller' => 'Pos\Controller\RestAPI\PosAuthenticate'
                    ),
                ),
            ),
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'pos' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    'defaultLanguage' => "en_US",
);
