<?php

namespace PosTest\Controller\API;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\Container;

class PosControllerTest extends AbstractHttpControllerTestCase
{


    public function setUp()
    {
        $this->setApplicationConfig(
                include DOC_ROOT . 'config/test.application.config.php'
        );
        parent::setUp();

    }


    public function mockUserLogin()
    {
        //        if (!session_id()) {
        $userSessions = new Container('ezBizUser');
        //        }
        $userSessions->userID = 1;
        $userSessions->roleID = 1;
        $locationarray = array(
            'locationName' => "kirulapana",
            'roleID' => 1,
            'locationID' => 53
        );
        $userSessions->userActiveLocation = $locationarray;

    }


    public function testAuthenticateActionCanBeAccessed()
    {
        //set userSession variables
        $_SERVER["SERVER_PORT"] = "80";
        $_SERVER["SERVER_NAME"] = "inventory";
        $_SERVER["REQUEST_URI"] = "";

        $postData = array("openingBalance" => 500,
            "closingBalance" => 500,
            "action" => "logout"
        );

        $this->mockUserLogin();
        $this->dispatch('/pos-api/authenticate', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('POS');
        $this->assertControllerName('Pos\Controller\Pos\api');
        $this->assertControllerClass('PosController');
        $this->assertMatchedRouteName('pos-api');

    }


    public function testCreateActionCanBeAccessed()
    {
        //set userSession variables
        $_SERVER["SERVER_PORT"] = "80";
        $_SERVER["SERVER_NAME"] = "inventory";
        $_SERVER["REQUEST_URI"] = "";

        $postData = array(
            'invoice' => array(
                'totalTax' => 0,
                'discount' => 0,
                'cart' => array(
                    array(
                        'pID' => '319',
                        'pC' => 'amaple',
                        'pN' => 'sda',
                        'qty' => 1,
                        'pUom' => 'kg',
                        'pR' => 500.00,
                        'disc' => '',
                        'dType' => '',
                        'dAmo' => '',
                        'sign' => '',
                        'tax' => array(
                            'tTA' => '1750',
                            'tL' => array(
                                '2' => array(
                                    'tN' => 'nbt',
                                    'tP' => 5,
                                    'tA' => 1750
                                )
                            )
                        ),
                        'hashKey' => '00I',
                        'pTotal' => 500,
                    )
                ),
                'subTotal' => 500,
                'finTotal' => 500
            ),
            'payment' => array(
                'payType' => array(
                    'name' => 'cash',
                    'id' => 0,
                ),
                'cardPayment' => 0,
                'balance' => 0,
                'cash' => 500
            )
        );

        $this->mockUserLogin();
        $this->dispatch('/pos-api/create', 'POST', $postData);
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('POS');
        $this->assertControllerName('Pos\Controller\Pos\api');
        $this->assertControllerClass('PosController');
        $this->assertMatchedRouteName('pos-api');

    }


}
