<?php

namespace PosTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\Container;

class PosControllerTest extends AbstractHttpControllerTestCase
{


    public function setUp()
    {
        $this->setApplicationConfig(
                include DOC_ROOT . 'config/test.application.config.php'
        );
        parent::setUp();

    }


    public function mockUserLogin()
    {
//        if (!session_id()) {
        $userSessions = new Container('ezBizUser');
//        }
        $userSessions->userID = 1;
        $userSessions->roleID = 1;
        $locationArray = array(
            'locationName' => "kirulapana",
            'roleID' => 1,
            'locationID' => 53
        );
        $userSessions->userActiveLocation = $locationArray;

    }


    public function testCreateActionCanBeAccessed()
    {
        //set userSession variables
        $_SERVER["SERVER_PORT"] = "80";
        $_SERVER["SERVER_NAME"] = "inventory";
        $_SERVER["REQUEST_URI"] = "";

        $this->mockUserLogin();
        $this->dispatch('/pos/create');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('POS');
        $this->assertControllerName('Pos\Controller\Pos');
        $this->assertControllerClass('PosController');
        $this->assertMatchedRouteName('pos');

    }


}
