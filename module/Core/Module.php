<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Core;

use Zend\EventManager\StaticEventManager as StaticEventManager;
use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Mvc\Router; ///////
use Zend\View\Model\ViewModel;
use User\Model\User;
use User\Model\UserTable;
use User\Model\Emailconfig;
use User\Model\EmailconfigTable;
use User\Model\RoleFeature;
use User\Model\RoleFeatureTable;
use Core\Model\Log;
use Core\Model\LogTable;
use Core\Model\LogDetails;
use Core\Model\LogDetailsTable;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Session\Container;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Core\Model\Status;
use Core\Model\StatusTable;
use Core\Model\Product;
use Core\Model\ProductTable;
use Core\Model\ProductType;
use Core\Model\ProductTypeTable;
use Core\Model\PaymentTerm;
use Core\Model\PaymentTermTable;
use Core\Model\Currency;
use Core\Model\CurrencyTable;
use Inventory\Model\Supplier;
use Inventory\Model\SupplierTable;
use Core\Model\Entity;
use Core\Model\EntityTable;
use Core\Model\UserLocation;
use Core\Model\UserLocationTable;
use Core\Model\PaymentMethod;
use Core\Model\PaymentMethodTable;
use Core\Model\Core;
use Core\Model\CoreTable;
use Core\Model\Notification;
use Core\Model\NotificationTable;
use Settings\Model\Tax;
use Settings\Model\TaxTable;
use Settings\Model\Location;
use Settings\Model\LocationTable;
use Settings\Model\DisplaySetup;
use Settings\Model\DisplaySetupTable;
use Inventory\Model\SupplierCategory;
use Inventory\Model\SupplierCategoryTable;
use Inventory\Model\SupplierCategoryComponent;
use Inventory\Model\SupplierCategoryComponentTable;
use Core\Model\Licence;
use Core\Model\LicenceTable;
use Core\Model\Notice;
use Core\Model\NoticeTable;
use Core\Model\UserNotice;
use Core\Model\UserNoticeTable;
use Core\Model\Attachment;
use Core\Model\AttachmentTable;
use Core\Model\DocumentReference;
use Core\Model\DocumentReferenceTable;
use Core\Model\ReportQueue;
use Core\Model\ReportQueueTable;
use Core\Model\JournalEntryDimension;
use Core\Model\JournalEntryDimensionTable;
use Core\Model\DocumentAttachementMapping;
use Core\Model\DocumentAttachementMappingTable;
use Core\Library\RestApiRoutes;
use Symfony\Component\Translation\Translator;
use Illuminate\Container\Container as IlluminateContainer;
use Illuminate\Validation\Factory as ValidationFactory;

class Module
{

    private $apiRequest = null;
    private $userAccessWhiteList = array(
        'Core\Controller\API\NotificationController' => array('updateNotification', 'getNotification', 'updateOverdueInvoiceStatus', 'calculateCashInHand'),
        'User\Controller\User' => array('login', 'logout', 'forgotPassword', 'resetPassword', 'changePassword', 'costingMigrate'),
        'User\Controller\UserApi' => array('userfeedback', 'removeNoticeForUser', 'checkWizardAuth'),
        'Core\Controller\JavascriptVariable' => array('messages'),
        'JobCard\Controller\API\InquiryLog' => array('sendEmailToInquiryLogEmployees'),
        'Expenses\Controller\API\ExpensePurchaseInvoiceAPI' => array('approve'),
        'Inventory\Controller\API\PurchaseRequistionController' => array('approve'),
        'Inventory\Controller\API\PurchaseOrderController' => array('approve'),
        'Inventory\Controller\API\Grn' => array('approve'),
        'Invoice\Controller\API\Invoice' => array('approve'),
        'Invoice\Controller\API\InvoiceReturn' => array('approve'),
        'Pos\Controller\Pos\API' => array('check-date-time'),
        'Core\Controller\API\AppController' => array('view-queued-reports', 'delete-viewed-reports', 'generate-report')
    );
    private $featureList;
    private $featureDetails;

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $sharedManager = $eventManager->getSharedManager();
        $sm = $e->getApplication()->getServiceManager();

        $sharedManager->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', function($e)
                use ($sm) {
            $controller = $e->getTarget();
            $controller->getEventManager()->attachAggregate($sm->get('NotificationListener'));
            $controller->getEventManager()->attachAggregate($sm->get('AccountListener'));
            $controller->getEventManager()->attachAggregate($sm->get('ProductListener'));
        }, 2);


        // skip authentication if it is an api request

        if ($this->isApiRequest($e)) {
            return true;
        }


        $config = $this->getConfig();

        $defaultLanguage = $config['defaultLanguage'];
        if (session_id() == '')
            @session_start();

        $translator = $e->getApplication()->getServiceManager()->get('translator');

        if (isset($_SESSION['ezBizUser']['lang'])) {
            $translator->setLocale($_SESSION['ezBizUser']['lang']);
        } else {
            $translator->setLocale($defaultLanguage);
        }

// pass user session array to view (layout.phtml)
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $viewModel->ezBizUser = (isset($_SESSION['ezBizUser'])) ? $_SESSION['ezBizUser'] : null;

        $e->getApplication()->getEventManager()->attach('route', array($this, 'checkAcl'));

// save log at the end of each request
        $e->getApplication()->getEventManager()->attach('finish', function($e) {
            $log = $e->getApplication()->getServiceManager()->get('Log');
            if($log->getFeatureID() != ''){
                $log->saveLog();
            }
        });
    }

    /*
     * append module name and action to the head title according to page navigation
     */

    public function setTitle(MvcEvent $e, $title)
    {
        $viewHelperManager = $e->getApplication()->getServiceManager()->get('viewHelperManager');
        $headTitleHelper = $viewHelperManager->get('headTitle');
        $headTitleHelper->setSeparator(($title == '') ? '' : ' - ');
        $headTitleHelper->append($title . ' | ezBiz');
//        $headTitleHelper->append();
    }

    /**
     * this for helper function, to use notification
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'CoreHelper' => function($helperPluginManager) {

            $sm = $helperPluginManager->getServiceLocator();
            $notificationController = $sm->get('NotificationController');
            $helper = new View\Helper\CoreHelper($notificationController);
            return $helper;
        }
            )
        );
    }

    public function init()
    {
        $events = StaticEventManager::getInstance();
        $events->attach('Zend\Mvc\Controller\AbstractActionController', 'dispatch', array(
            $this,
            'authPreDispatch'
                ), 110);
    }

    /**
     * Make sure the user is logged in to process the request.
     * Redirects the user if not logged in.
     * @param MvcEvent $e
     * @return Response
     */
    public function authPreDispatch($e)
    {

        // skip authentication if it is an api request
        if ($this->isApiRequest($e)) {
            return true;
        }


        $services = $e->getApplication()->getServiceManager();
        $requestMethod = $services->get('request')->getMethod();
        if ($requestMethod == 'GET') {
            $requestUrl = $services->get('request')->getUri()->getPath();
        } else {
            $requestUrl = '';
        }

        $requestURI = $services->get('request')->getUri()->getPath();
        if (isset($_SESSION['ezBizUser']['userID']) && $requestURI != '/api/notification/getNotification') {
            $userController = $e->getApplication()->getServiceManager()->get('User');
            $userController->checkAndUpdateSingleInstace();
        }

        $controlller = $e->getRouteMatch()->getParam('controller');
        $action = $e->getRouteMatch()->getParam('action');
        // if the user needs to be logged in to process the request but is not logged in, redirect to login page

        if (!isset($_SESSION['ezBizUser']['userID']) && (!isset($this->userAccessWhiteList[$controlller]) || !in_array($action, $this->userAccessWhiteList[$controlller]))) {
            $url = $e->getRouter()->assemble(array(
                "controller" => "user",
                'action' => 'login',
                'param1' => urlencode($requestUrl),
                    ), array(
                'name' => 'user'
            ));

            $response = $e->getResponse();
            $response->setHeaders($response->getHeaders()->addHeaderLine('Location', $url));
            $response->setStatusCode(302);
            $response->sendHeaders();
            return $response;
        }
    }

    public function initAcl(MvcEvent $e)
    {
        $acl = new \Zend\Permissions\Acl\Acl();
        $roles = $this->getDbRoles($e);
        $allFeatures = $this->getAllFeatures($e);
        $this->featureDetails = $allFeatures;
        $featureList = array();
        foreach ($allFeatures as $key => $resource) {
            $featureList[$key] = $resource['combinedResource'];
            $combinedResource = $resource['combinedResource'];
            if (!$acl->hasResource($combinedResource))
                $acl->addResource(new \Zend\Permissions\Acl\Resource\GenericResource($combinedResource));
        }

        $this->featureList = $featureList;
        foreach ($roles as $role => $resources) {
            $role = new \Zend\Permissions\Acl\Role\GenericRole($role);
            $acl->addRole($role);

            foreach ($resources as $resource) {
                $roleCombinedResource = $resource['controller'] . '|' . $resource['action'];
                $acl->allow($role, $roleCombinedResource);
            }
        }
        $e->getViewModel()->acl = $acl;
    }

    public function checkAcl(MvcEvent $e)
    {
// TODO: refactor
        if (!isset($_SESSION['ezBizUser']['userID'])) {
            return true;
        }

        $this->initAcl($e);

// TODO - check if action exists (before going to 404, shows error saying No Permission)
        $pageNotFoundFlag = FALSE;
        $route = $e->getRouteMatch()->getMatchedRouteName();
        $controlller = $e->getRouteMatch()->getParam('controller');
        $action = $e->getRouteMatch()->getParam('action');
        $checkCombinedResource = $controlller . '|' . $action;

        $lwrCaseAction = lcfirst(preg_replace_callback('/(^|-)([a-z])/', function($m) {
                    return strtoupper($m[2]);
                }, $action));
        $lwrCasecheckCombinedResource = $controlller . '|' . $lwrCaseAction;
        $userRole = $_SESSION['ezBizUser']['roleID'];
        $_SESSION['ezBizUser']['permissionForAction'] = false;
        $userHasPermissions = FALSE;
        if ($e->getViewModel()->acl->hasResource($checkCombinedResource)) {
            if ($e->getViewModel()->acl->isAllowed($userRole, $checkCombinedResource)) {
                $userHasPermissions = TRUE;
                $_SESSION['ezBizUser']['permissionForAction'] = true;
            }
        } elseif ($e->getViewModel()->acl->hasResource($lwrCasecheckCombinedResource)) {
            if ($e->getViewModel()->acl->isAllowed($userRole, $lwrCasecheckCombinedResource)) {
                $userHasPermissions = TRUE;
                $_SESSION['ezBizUser']['permissionForAction'] = true;
            }
        } elseif ($checkCombinedResource == 'User\\Controller\\User|logout') {
            $userHasPermissions = TRUE;
            $_SESSION['ezBizUser']['permissionForAction'] = true;
        } else {

            /* IF feature doesn't exist in DB what happen to user permissions */
            /* For testing purposes uncomment $userHasPermissions and comment $pageNotFoundFlag */
            $pageNotFoundFlag = FALSE;
            // $_SESSION['ezBizUser']['permissionForAction'] = true;
           // $userHasPermissions = TRUE;

        }

        $userApi = $e->getApplication()->getServiceManager()->get('UserApi');

        if ($route == "materialRequisition" && $action == "index" && $userHasPermissions == FALSE) {
            $userApi->setJobRole('jobAdmin', false);
            $jobStoreKeeper = false;
            if ($e->getViewModel()->acl->hasResource("Jobs\Controller\MaterialRequisition|materialIssue")) {
                if ($e->getViewModel()->acl->isAllowed($userRole, "Jobs\Controller\MaterialRequisition|materialIssue")) {
                    $jobStoreKeeper = TRUE;
                }
            }

            if ($jobStoreKeeper) {
                header("Location: /material-requisition/materialIssue");
                exit();
            }
        }

        if ($route == "materialRequisition" && $action == "materialIssue" && $userHasPermissions == FALSE) {
            $userApi->setJobRole('jobStoreKeeper', false);
            header("Location: /material-requisition/index");
            exit();
        }

        if ($route == "invoice-return") {
            if ($_SESSION['ezBizUser']->displaySettings->isUseSalesInvoiceReturn != 1) {
                header("Location: /sales-dashboard");
                exit();
            }
        }

        $isDashBoardController = ($controlller == 'User\Controller\DashBoard') ? true : false;
        $isDashBoardIndexAction = ($action == 'index') ? true : false;
        if ($userHasPermissions == FALSE && $isDashBoardController && $isDashBoardIndexAction) {
            $userHasPermissions = true;
            $_SESSION['ezBizUser']['permissionForAction'] = true;
        }

        // If first login then force user to change password
        if ($userApi->isFirstLogin()){
            if ($action != 'force-password-reset' && $action != 'resetPassword') {
                header("Location: /user/force-password-reset");
                exit();
            }
            else if ($action != 'resetPassword') {
                array_push($this->userAccessWhiteList[$controlller], 'force-password-reset');
            }
        }
        else if (!$userApi->isFirstLogin() && $action == 'force-password-reset'){
            $userHasPermissions = FALSE;
            $_SESSION['ezBizUser']['permissionForAction'] = false;
        }

        $wizardUpdateWhiteList = array('Settings\Controller\CompanyAPI', 'Settings\Controller\TaxAPI', 'Core\\Controller\\CoreController', 'Accounting\Controller\API\Accounts','Accounting\Controller\API\GlAccountSetup');
        if ($userHasPermissions && $controlller != "Settings\Controller\Wizard" && $controlller != "Settings\Controller\API\Wizard" && !in_array($controlller, $wizardUpdateWhiteList)) {
            if (!$_SESSION['ezBizUser']['wizardComplete']) {
                $wizardUrl = $userApi->checkWizardAuthAction();
                $redirectUrl = "/wizard/" . $wizardUrl;
                header("Location:" . $redirectUrl);
                exit();
            }
        }


        if ($e->getRequest()->isPost() == true && $userHasPermissions == FALSE && (!isset($this->userAccessWhiteList[$controlller]) || !in_array($action, $this->userAccessWhiteList[$controlller]))) {
            $response = $e->getResponse();
            $response->setStatusCode(403);
            $noPermssionView = new ViewModel();
            if ($pageNotFoundFlag == FALSE) {
                $noPermssionView->setTemplate('error/permission');
            } else {
                $noPermssionView->setTemplate('error/page-not-found');
            }

            $permissionErrorHtmlContent = $e->getApplication()->getServiceManager()
                    ->get('viewrenderer')
                    ->render($noPermssionView);

            $viewmodel = new ViewModel(array('content' => $permissionErrorHtmlContent));
            $viewmodel->setTemplate('layout/layout');

            $layoutHtmlContent = $e->getApplication()->getServiceManager()
                    ->get('viewrenderer')
                    ->render($viewmodel);

            $response->setContent($layoutHtmlContent);
            return $response;
        }

// continue if user has permission
        $log = $e->getApplication()->getServiceManager()->get('Log');
        if (in_array($checkCombinedResource, $this->featureList)) {
            $featureID = array_search($checkCombinedResource, $this->featureList);
        } elseif (in_array($lwrCasecheckCombinedResource, $this->featureList)) {
            $featureID = array_search($lwrCasecheckCombinedResource, $this->featureList);
        }
        if (isset($featureID)) {
            $log->setFeatureID($featureID);
            $log->setFeatureType($this->featureDetails[$featureID]['featureType']);
        }
    }

    public function getDbRoles(MvcEvent $e)
    {

        $dbAdapter = $e->getApplication()->getServiceManager()->get('Zend\Db\Adapter\Adapter');
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('roleFeature');
        $select->join('feature', 'roleFeature.featureID = feature.featureID', array('featureController', 'featureAction'));
        $select->where(array('roleID' => $_SESSION['ezBizUser']['roleID']));
        $select->where(array('roleFeatureEnabled' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $roles = array();
        while ($row = $result->current()) {
            $roles[$row['roleID']][] = array(
                'controller' => $row['featureController'],
                'action' => $row['featureAction']);
        }
        return $roles;
    }

    public function getAllFeatures(MvcEvent $e)
    {

        $dbAdapter = $e->getApplication()->getServiceManager()->get('Zend\Db\Adapter\Adapter');
        $sql = new Sql($dbAdapter);
        $select = $sql->select();
        $select->from('feature');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $features = array();


        foreach ($result as $row) {
            $features[$row['featureID']] = array(
                'combinedResource' => $row['featureController'] . '|' . $row['featureAction'],
                'featureType' => $row['featureType']);

            // get feature name from table for current controller and action
            $this->setTitleFromFeature($e, $row);
        }
        return $features;
    }

    /*
     * pass the action name and module name to the setTitle method
     */

    public function setTitleFromFeature($e, $feature)
    {
        $controller = $e->getRouteMatch()->getParam('controller');
        $lwrcaseController = lcfirst(preg_replace_callback('/(^|-)([a-z])/', function($m) {
                    return strtoupper($m[2]);
                }, $controller));

        $action = $e->getRouteMatch()->getParam('action');
        $lwrcaseAction = lcfirst(preg_replace_callback('/(^|-)([a-z])/', function($m) {
                    return strtoupper($m[2]);
                }, $action));

        $fController = lcfirst(preg_replace_callback('/(^|-)([a-z])/', function($m) {
                    return strtoupper($m[2]);
                }, $feature['featureController']));
        $fAction = lcfirst(preg_replace_callback('/(^|-)([a-z])/', function($m) {
                    return strtoupper($m[2]);
                }, $feature['featureAction']));

        if ($fController == $lwrcaseController && $fAction == $lwrcaseAction) {
            if ($feature['featureCategory'] != "Application" && $feature['featureName'] != "Default" && $feature['featureType'] == 0) {
                if ($feature['featureCategory'] != $feature['featureName']) {

                    $this->setTitle($e, $feature['featureCategory'] . ' - ' . $feature['featureName']);
                } else {
                    $this->setTitle($e, $feature['featureCategory']);
                }
            } else {
                $this->setTitle($e, "");
            }
        }
    }

    /**
     * Check if the request is a valid API request
     * @param \Zend\Mvc\MvcEvent $e
     * @return bool
     */
    public function isApiRequest(MvcEvent $e)
    {
// act as a singleton

        if ($this->apiRequest !== null) {
            return $this->apiRequest;
        }

        $services = $e->getApplication()->getServiceManager();
        $requestHeaders = $services->get('request')->getHeaders();

        if (strtoupper($services->get('request')->getMethod()) === 'OPTIONS') {
            // If need access control headers can be set here
            // Currently access control headers are set in .htaccess file
            exit();
        }

        if ($requestHeaders->has('Api')) {

// validate the api request
            $auth = $requestHeaders->get('Authorization') ?
                            $requestHeaders->get('Authorization')->getFieldValue() : null;
            $authData = explode(':', $auth);
            $hash = $authData[0];
            $salt = $authData[1];

            $apiKey = $services->get('config')['apiKey'];
            $mobileApiKey = $services->get('config')['mobileApiKey'];
            $posApiKey = $services->get('config')['mobilePosApiKey'];
            $apiRequestType = '';
            $apiRequestValidation = false;

            // check portal request
            if (crypt($apiKey, $salt) == $hash) {
                $this->apiRequest = true;

            // check ezbiz_mobile request
            } else if (md5($mobileApiKey.$salt) == $hash) {
                $apiRequestType = 'mobile_dashboard';
                $apiRequestValidation = true;

            // check ezbiz_pos request
            } else {
                // if (crypt($posApiKey, $salt) == $hash) {
                $apiRequestType = 'mobile_pos';
                $apiRequestValidation = true;

            }

            if ($apiRequestValidation && !($requestHeaders->get('AUTH-TOKEN'))) {
                // check exact login request
                $respond = RestApiRoutes::validateLoginRequest($services);
                $this->apiRequest = true;

            }  else if ($apiRequestValidation && $requestHeaders->get('AUTH-TOKEN')) {

                // validate request
                $respond = RestApiRoutes::validateRestApiRequest($services, $requestHeaders, $apiRequestType);
                $this->apiRequest = true;
            }
        }
        return $this->apiRequest;
    }

    public function getConfig()
    {
        $config = array();
        $configFiles = array(
            include __DIR__ . '/config/module.config.php',
            include __DIR__ . '/config/template.config.php',
            include __DIR__ . '/config/ezbiz.config.php',
            include __DIR__ . '/config/notification.config.php',
            include __DIR__ . '/config/personTitle.config.php',
            include __DIR__ . '/config/licence.config.php',
            include __DIR__ . '/config/message.config.php',
            include __DIR__ . '/config/menus.config.php',
            include __DIR__ . '/config/timezones.config.php',
            include __DIR__ . '/config/languages.config.php',
            include __DIR__ . '/config/dateformat.config.php',
            include __DIR__ . '/config/defaultReferencePrefix.config.php',
            include __DIR__ . '/config/countries.config.php',
            include __DIR__ . '/config/currency.config.php',
            include __DIR__ . '/config/toolTip.config.php',
            include __DIR__ . '/config/template-data-table.config.php',
            include __DIR__ . '/config/itemImport.config.php',
            include __DIR__ . '/config/settings.config.php',
            include __DIR__ . '/config/accounts.config.php',
            include __DIR__ . '/config/reports.config.php',
            include __DIR__ . '/config/apiRoutes.config.php'
        );
        foreach ($configFiles as $file) {
            $config = \Zend\Stdlib\ ArrayUtils::merge($config, $file);
        }

        return $config;
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'invokables' => array(
                'Modal' => 'Core\Controller\ModalController',
                'User' => 'User\Controller\UserController',
                'UserApi' => 'User\Controller\UserApiController',
                'Template' => 'Settings\Controller\TemplateController',
                'NotificationController' => 'Core\Controller\API\NotificationController',
                'Log' => 'Core\Controller\LogController',
                'JavascriptVariableController' => 'Core\Controller\JavascriptVariableController',
                'ReferenceService' => 'Core\Service\ReferenceService',
                'TaxService' => 'Core\Service\TaxService',
                'CurrencyService' => 'Core\Service\CurrencyService',
                'PaymentTermService' => 'Core\Service\PaymentTermService',
                'ProductService' => 'Core\Service\ProductService',
                'DisplaySetupService' => 'Core\Service\DisplaySetupService',
                'EntityService' => 'Core\Service\EntityService',
                'ReportService' => 'Core\Service\ReportService',
                'PusherService' => 'Core\Service\PusherService',
                'CategoryService' => 'Core\Service\CategoryService',
                'PromotionService' => 'Core\Service\PromotionService',
                'SmsService' => 'Core\Service\SmsService',
            ),
            'shared' => array(
                'ReferenceService' => true,
                'TaxService' => true,
                'CurrencyService' => true,
                'PaymentTermService' => true,
                'ProductService' => true,
                'PromotionService' => true,
                'DisplaySetupService' => true,
                'EntityService' => true,
                'ReportService' => true,
                'PusherService' => true,
                'CategoryService' => true,
                'SmsService' => true,
            ),
            'aliases' => array(
                'CurrencyTable' => 'Core\Model\CurrencyTable',
                'PaymentTermTable' => 'Core\Model\PaymentTermTable',
                'TaxTable' => 'Core\Model\TaxTable',
                'EntityTable' => 'Core\Model\EntityTable',
                'NotificationTable' => 'Core\Model\NotificationTable',
                'PaymentMethodTable' => 'Core\Model\PaymentMethodTable',
                'DocumentAttachementMappingTable' => 'Core\Model\DocumentAttachementMappingTable',
                'StatusTable' => 'Core\Model\StatusTable'
            ),
            'factories' => array(
                'Validator' => function($sm) {
                    $translator = new Translator('en');
                    $cont = new IlluminateContainer;
                    return new ValidationFactory($translator, $cont);
                },
                'Core\Model\StatusTable' => function($sm) {
            $tableGateway = $sm->get('StatusTableGateway');
            $table = new StatusTable($tableGateway);
            return $table;
        },
                'StatusTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Status());
            return new TableGateway('status', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\ProductTable' => function ($sm) {
            $tableGateway = $sm->get('ProductTableGateway');
            $table = new ProductTable($tableGateway);
            return $table;
        },
                'ProductTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Product());
            return new TableGateway('product', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\ProductTypeTable' => function($sm) {
            $tableGateway = $sm->get('ProductTypeTableGateway');
            $table = new ProductTypeTable($tableGateway);
            return $table;
        },
                'ProductTypeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ProductType());
            return new TableGateway('productType', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\PaymentTermTable' => function($sm) {
            $tableGateway = $sm->get('PaymentTermTableGateway');
            $table = new PaymentTermTable($tableGateway);
            return $table;
        },
                'PaymentTermTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new PaymentTerm());
            return new TableGateway('paymentTerm', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\CurrencyTable' => function($sm) {
            $tableGateway = $sm->get('CurrencyTableGateway');
            $table = new CurrencyTable($tableGateway);
            return $table;
        },
                'CurrencyTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Currency());
            return new TableGateway('currency', $dbAdapter, null, $resultSetPrototype);
        },
                'Inventory\Model\SupplierTable' => function($sm) {
            $tableGateway = $sm->get('SupplierTableGateway');
            $table = new SupplierTable($tableGateway);
            return $table;
        },
                'SupplierTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Supplier());
            return new TableGateway('supplier', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\EntityTable' => function($sm) {
            $tableGateway = $sm->get('EntityTableGateway');
            $table = new EntityTable($tableGateway);
            return $table;
        },
                'EntityTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Entity());
            return new TableGateway('entity', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\UserLocationTable' => function($sm) {
            $tableGateway = $sm->get('UserLocationTableGateway');
            $table = new UserLocationTable($tableGateway);
            return $table;
        },
                'UserLocationTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new UserLocation());
            return new TableGateway('userLocation', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\ReferenceTable' => function($sm) {
            $tableGateway = $sm->get('ReferenceTableGateway');
            $table = new ReferenceTable($tableGateway);
            return $table;
        },
                'ReferenceTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Reference());
            return new TableGateway('reference', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\PaymentMethodTable' => function($sm) {
            $tableGateway = $sm->get('PaymentMethodTableGateway');
            $table = new PaymentMethodTable($tableGateway);
            return $table;
        },
                'PaymentMethodTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new PaymentMethod());
            return new TableGateway('paymentMethod', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\CoreTable' => function($sm) {
            $tableGateway = $sm->get('CoreTableGateway');
            $table = new CoreTable($tableGateway);
            return $table;
        },
                'CoreTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Core());
            return new TableGateway('', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\NotificationTable' => function($sm) {
            $tableGateway = $sm->get('NotificationTableGateway');
            $table = new NotificationTable($tableGateway);
            return $table;
        },
                'NotificationTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Notification());
            return new TableGateway('notification', $dbAdapter, null, $resultSetPrototype);
        },
                'Inventory\Model\SupplierCategoryTable' => function($sm) {
            $tableGateway = $sm->get('SupplierCategoryTableGateway');
            $table = new SupplierCategoryTable($tableGateway);
            return $table;
        },
                'SupplierCategoryTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new SupplierCategory());
            return new TableGateway('supplierCategory', $dbAdapter, null, $resultSetPrototype);
        },
                'Inventory\Model\SupplierCategoryComponentTable' => function($sm) {
            $tableGateway = $sm->get('SupplierCategoryComponentTableGateway');
            $table = new SupplierCategoryComponentTable($tableGateway);
            return $table;
        },
                'SupplierCategoryComponentTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new SupplierCategoryComponent());
            return new TableGateway('supplierCategoryComponent', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\TaxTable' => function($sm) {
            $tableGateway = $sm->get('TaxTableGateway');
            $table = new TaxTable($tableGateway);
            return $table;
        },
                'TaxTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Tax());
            return new TableGateway('tax', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\Model\LocationTable' => function($sm) {
            $tableGateway = $sm->get('LocationTableGateway');
            $table = new LocationTable($tableGateway);
            return $table;
        },
                'LocationTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Location());
            return new TableGateway('location', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\model\DisplaySetupTable' => function($sm) {
            $tableGateway = $sm->get('TemplateTableGateway');
            $table = new DisplaySetupTable($tableGateway);
            return $table;
        },
                'DisplaySetupTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DisplaySetup());
            return new TableGateway('displaySetup', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\model\LogTable' => function($sm) {
            $tableGateway = $sm->get('LogTableGateway');
            $table = new LogTable($tableGateway);
            return $table;
        },
                'LogTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Log());
            return new TableGateway('log', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\model\LicenceTable' => function($sm) {
            $tableGateway = $sm->get('LicenceTableGateway');
            $table = new LicenceTable($tableGateway);
            return $table;
        },
                'LicenceTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Licence());
            return new TableGateway('licence', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\model\NoticeTable' => function($sm) {
            $tableGateway = $sm->get('NoticeTableGateway');
            $table = new NoticeTable($tableGateway);
            return $table;
        },
                'NoticeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Notice());
            return new TableGateway('notice', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\model\UserNoticeTable' => function($sm) {
            $tableGateway = $sm->get('UserNoticeTableGateway');
            $table = new UserNoticeTable($tableGateway);
            return $table;
        },
                'UserNoticeTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new UserNotice());
            return new TableGateway('userNotice', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\model\AttachmentTable' => function($sm) {
            $tableGateway = $sm->get('AttachmentTableGateway');
            $table = new AttachmentTable($tableGateway);
            return $table;
        },
                'AttachmentTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Attachment());
            return new TableGateway('attachment', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\model\DocumentReferenceTable' => function($sm) {
            $tableGateway = $sm->get('DocumentReferenceTableGateway');
            $table = new DocumentReferenceTable($tableGateway);
            return $table;
        },
                'DocumentReferenceTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DocumentReference());
            return new TableGateway('documentReference', $dbAdapter, null, $resultSetPrototype);
        },
                'Core\model\LogDetailsTable' => function($sm) {
            $tableGateway = $sm->get('LogDetailsTableGateway');
            $table = new LogDetailsTable($tableGateway);
            return $table;
        },
                'LogDetailsTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new LogDetails());
            return new TableGateway('logDetails', $dbAdapter, null, $resultSetPrototype);
        },
        'Core\model\ReportQueueTable' => function($sm) {
            $tableGateway = $sm->get('ReportQueueTableGateway');
            $table = new ReportQueueTable($tableGateway);
            return $table;
        },
                'ReportQueueTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new ReportQueue());
            return new TableGateway('reportQueue', $dbAdapter, null, $resultSetPrototype);
        },
        'Core\model\JournalEntryDimensionTable' => function($sm) {
            $tableGateway = $sm->get('JournalEntryDimensionTableGateway');
            $table = new JournalEntryDimensionTable($tableGateway);
            return $table;
        },
                'JournalEntryDimensionTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new JournalEntryDimension());
            return new TableGateway('journalEntryDimensions', $dbAdapter, null, $resultSetPrototype);
        },
        'Core\model\DocumentAttachementMappingTable' => function($sm) {
            $tableGateway = $sm->get('DocumentAttachementMappingTableGateway');
            $table = new DocumentAttachementMappingTable($tableGateway);
            return $table;
        },
                'DocumentAttachementMappingTableGateway' => function ($sm) {
            $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new DocumentAttachementMapping());
            return new TableGateway('documentAttachementMapping', $dbAdapter, null, $resultSetPrototype);
        },
        
            )
        );
    }

}
