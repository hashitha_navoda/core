<?php

namespace Core\Contracts;

interface SearchableFromCode {

    public function codeExists($code, $locationId);

}