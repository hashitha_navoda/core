<?php

namespace Core\Library;

use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;
use Core\Library\Jwt\BeforeValidException;
use Core\Library\Jwt\ExpiredException;
use Core\Library\Jwt\SignatureInvalidException;
use Core\Library\Jwt\WrongRouteException;
use Core\Library\Jwt\ValidationFailException;
use Core\Library\Jwt\JWT;
// use \Firebase\JWT\JWT;


class RestApiRoutes {

	/**
	* this function use to validate user login request
	**/
	public static function validateLoginRequest($service)
	{
		$loginRoutes = $service->get('config')['default_login_routes'];
		// current route
		$currentRoute = $service->get('request')->getUri()->getPath();
		if (!in_array($currentRoute, $loginRoutes)) {
			throw new WrongRouteException("Wrong login route");
		}

		return true;

	}

	/**
	* this function use to genarate Jwt Token
	**/
	public static function createJwtToken($config,$userName, $appType, $userFirstName,$currencySymbol, $userLocations, $userdefaultLocations, $company, $userID)
	{

		$token = array(
	       "iss" => $config[$appType]['issuer'],
	       "aud" => $config[$appType]['audience'],
	       "iat" => time(),
	       "exp" => time() + 86400,
	       "data" => array(
	           "userName" => $userName,
	           "userFirstName" => $userFirstName,
	           "currencySymbol" => $currencySymbol,
	           "userLocations" => $userLocations,
	           "userdefaultLocations" => $userdefaultLocations,
	           "company" => $company,
	           "userID" => $userID,
	       )
    	);
    	$key = $config[$appType]['key'];
    	try {
    		$jwt = JWT::encode($token, $key);
    		return $jwt;
    	} catch (Exception $e) {
    		error_log($e->getMessage());
    		return false;
    	}

	}

	/**
	* this function use to validate current request
	**/
	public static function validateRestApiRequest($service, $reqHeader, $appType)
	{
		$type = $appType.'_routes';
		$jwtTypes = $appType.'_app_jwt_details';

		// get app routes
		$appRoutes = $service->get('config')[$type];

		// requested url
		$currentUrl = $service->get('request')->getUri()->getPath();

		// validate token
		$token = $reqHeader->get('AUTH-TOKEN')->getFieldValue();
		$key = $service->get('config')[$jwtTypes]['key'];
		try {

			if (!in_array($currentUrl, $appRoutes)) {
				throw new WrongRouteException('Wrong routes');
			}

		    // decode jwt
		    $decoded = JWT::decode($token, $key, array('HS256'));
		    return ['data' => $decoded->data->userName];

	    } catch (Exception $e) {

	    	throw new ValidationFailException('Route validation fails');
	    }

	}
}
