<?php

namespace Core\Library;

use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;

class Util
{
    /**
     * Send HTTP request
     *
     * @param string $url
     * @param string $method
     * @param array $params
     * @param array $headers
     * @return Response
     */
    public static function httpRequest($url, $method, $params = [], $headers = [], $options = [])
    {
        $method = (strtolower($method) == "post") ? 1 : 0;

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, $method);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_exec($ch);

        if(curl_errno($ch)){
            error_log(curl_error($ch));
            return false;
        }

        return true;
    }

    /**
     * Send  HTTP request (Alt)
     *
     * @param string $url
     * @param string $method
     * @param array $params
     * @param array $headers
     * @return Response
     */
    public static function httpAlterRequest($url, $method, $params = [], $headers = [], $options = [])
    {
        if ($headers['Content-Type'] == 'application/x-www-form-urlencoded') {
            $data = http_build_query($data);
        }

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);  //Post Fields
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3600);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $res = curl_exec($ch);
        if(curl_errno($ch)){
            error_log(curl_error($ch));
            return false;
        }
        if((int) $res->resultCode != 0){
            error_log($res.resultDesc);
            return false;
        }

        return true;
    }

    /**
     * Get base url
     *
     * @return string
     */
    public static function getBaseUrl()
    {

        $schema = (isset($_SERVER['HTTP_ORIGIN'])) ? explode("://",$_SERVER['HTTP_ORIGIN'])[0] : 'https';

        return $schema .'://'. $_SERVER['SERVER_NAME'];

        //return sprintf("%s://%s", isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https' : 'http', $_SERVER['SERVER_NAME']);
    }

    /**
     * Get report callback data
     *
     * @return array
     */
    public static function getReportJobCallbackData($data, $type)
    {
        return ['service' => $data['service'], 'method' => $data[$type]];
    }

}
