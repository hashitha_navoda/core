<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This is the Document Reference Model
 */

namespace Core\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DocumentReference implements InputFilterAwareInterface
{

    public $documentReferenceId;
    public $sourceDocumentTypeId;
    public $sourceDocumentId;
    public $referenceDocumentTypeId;
    public $referenceDocumentId;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->documentReferenceId = (isset($data['documentReferenceId'])) ? $data['documentReferenceId'] : 0;
        $this->sourceDocumentTypeId = (isset($data['sourceDocumentTypeId'])) ? $data['sourceDocumentTypeId'] : 0;
        $this->sourceDocumentId = (isset($data['sourceDocumentId'])) ? $data['sourceDocumentId'] : 0;
        $this->referenceDocumentTypeId = (isset($data['referenceDocumentTypeId'])) ? $data['referenceDocumentTypeId'] : 0;
        $this->referenceDocumentId = (isset($data['referenceDocumentId'])) ? $data['referenceDocumentId'] : 0;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
