<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 */

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class LicenceTable {

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway) {
        $this->tableGateway = $tableGateway;
    }

    public function saveLicence(Licence $licence) {
        $data = array(
            'licenceName' => $licence->licenceName,
            'licenceExpireDate' => $licence->licenceExpireDate,
            'licenceStartingDate' => $licence->licenceStartingDate,
            'UserID' => $licence->userID,
        );

        $this->tableGateway->insert($data);
        $licenceID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $licenceID;
    }

    public function getLicenceByUserID($userID, $unlimitedUser = FALSE) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('licence');
        $select->where(array('userID' => $userID));
        if ($unlimitedUser) {
            $select->where->greaterThan('licenceExpireDate', new Expression('CURDATE()'));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getLicenseDetailsByUserID($userID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('licence');
        $select->where(array('userID' => $userID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    public function makeAllEmptyLicence() {
        $data = array(
            'userID' => NULL
        );
        return $this->tableGateway->update($data);
    }

    public function makeEmptyLicenceByUserID($userID) {
        $data = array(
            'userID' => NULL
        );
        return $this->tableGateway->update($data, array('userID' => $userID));
    }

    public function assignLicenceForUserID($licenceID, $userID) {
        $data = array(
            'userID' => $userID
        );
        return $this->tableGateway->update($data, array('licenceID' => $licenceID));
    }

    public function getEmptyLicence() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('licence');
        $select->where(array('userID' => NULL));
        $select->order(array('licenceExpireDate' => 'DESC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultArray = [];
        foreach ($resultSet as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getEmptyAndAssignedLicenceByUserID($userID) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('licence');
        $select->where(array('userID' => NULL));
        $select->where->OR->equalTo('userID', $userID);
        $select->order(array('licenceExpireDate' => 'DESC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultArray = [];
        foreach ($resultSet as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getAssignedLicence() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('licence');
        $select->where->notEqualTo('userID', '');
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultArray = [];
        foreach ($resultSet as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getCurrentLicence() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('licence');
        $select->where(array('licenceExpired' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultArray = [];
        foreach ($resultSet as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function fetchAll() {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('licence');
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        $resultArray = [];
        foreach ($resultSet as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getCurrentLicenceCount() {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('licence');
        $select->where->greaterThan('licenceExpireDate', new Expression('CURDATE()'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();
        return $resultSet->count();
    }

    public function deleteLicenceByLicenceID($licenceID) {
        return $this->tableGateway->delete(array('licenceID' => $licenceID));
    }

    public function updateLicenceStatusByDate() {
        $data = array(
            'licenceExpired' => 1
        );
        $predicate = new \Zend\Db\Sql\Where();
        $this->tableGateway->update($data, $predicate->lessThan('licenceExpireDate', new Expression('CURDATE()')));

        $data = array(
            'licenceExpired' => 0
        );
        $predicate = new \Zend\Db\Sql\Where();
        $this->tableGateway->update($data, $predicate->greaterThanOrEqualTo('licenceExpireDate', new Expression('CURDATE()')));
    }

    public function updateLicenceByName($data, $licenceName) {
        $this->tableGateway->update($data, array('licenceName' => $licenceName));
        return true;
    }

    public function getLicenceByLicenseName($licenceName) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('licence');
        $select->where(array('licenceName' => $licenceName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

}
