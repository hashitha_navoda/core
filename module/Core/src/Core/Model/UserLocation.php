<?php

namespace Core\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class UserLocation implements InputFilterAwareInterface
{

    public $userLocationID;
    public $userID;
    public $locationID;
    public $entityID;

    public function exchangeArray($data)
    {
        $this->userLocationID = (isset($data['userLocationID'])) ? $data['userLocationID'] : NULL;
        $this->userID = (isset($data['userID'])) ? $data['userID'] : NULL;
        $this->locationID = (isset($data['locationID'])) ? $data['locationID'] : 0;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : NULL;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}

