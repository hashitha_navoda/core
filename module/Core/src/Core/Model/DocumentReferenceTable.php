<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 */

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DocumentReferenceTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDocumentReference(DocumentReference $documentReference)
    {
        $data = array(
            'sourceDocumentTypeId' => $documentReference->sourceDocumentTypeId,
            'sourceDocumentId' => $documentReference->sourceDocumentId,
            'referenceDocumentTypeId' => $documentReference->referenceDocumentTypeId,
            'referenceDocumentId' => $documentReference->referenceDocumentId,
        );

        $this->tableGateway->insert($data);
        $documentReferenceID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $documentReferenceID;
    }
    
    //get document reference by Source Data
    public function getDocumentReferenceBySourceData($sourceDocumentTypeId,$sourceDocumentId){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('documentReference');
        $select->where(array('sourceDocumentTypeId' => $sourceDocumentTypeId, 'sourceDocumentId' => $sourceDocumentId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get document reference by Source Data
    public function getDocumentReferenceBySourceDocIDAndRefDocDetails($sourceDocumentTypeId,$referenceDocumentId, $referenceDocumentTypeId){

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('documentReference');
        $select->where(array('sourceDocumentTypeId' => $sourceDocumentTypeId, 'referenceDocumentTypeId' => $referenceDocumentTypeId, 'referenceDocumentId' => $referenceDocumentId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $resultSet = [];

        if (sizeof($result) > 0) {
            foreach ($result as $key => $value) {
                $resultSet[] = $value['sourceDocumentId'];
            }
        }

        return $resultSet;
    }

    /**
    * Delete document reference data depending on po, pi, grn details
    * @param string $sourceDocumentTypeId
    * @param string $sourceDocumentId
    * @return mixed
    */
    public function deleteDocumentReference($sourceDocumentTypeId, $sourceDocumentId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $delete = $sql->delete();
        $delete->from('documentReference');
        $delete->where(array('sourceDocumentTypeId' => $sourceDocumentTypeId, 'sourceDocumentId' => $sourceDocumentId));
        $statement = $sql->prepareStatementForSqlObject($delete);
        $result = $statement->execute();
        return $result;
    }


    //get document reference by Source Data
    public function getDocumentReferenceBySourceDocIDAndRefTypeDetails($sourceDocumentTypeId,$referenceDocumentTypeId, $sourceDocumentId){

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('documentReference');
        $select->where(array('sourceDocumentTypeId' => $sourceDocumentTypeId, 'referenceDocumentTypeId' => $referenceDocumentTypeId, 'sourceDocumentId' => $sourceDocumentId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;
    }
}
