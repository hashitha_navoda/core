<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This is the Notice Model
 */

namespace Core\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Notice implements InputFilterAwareInterface
{

    public $noticeID;
    public $noticeName;
    public $notice;
    public $noticeLink;
    public $noticeType;
    public $noticeExpireDate;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->noticeID = (isset($data['noticeID'])) ? $data['noticeID'] : null;
        $this->noticeName = (isset($data['noticeName'])) ? $data['noticeName'] : null;
        $this->notice = (isset($data['notice'])) ? $data['notice'] : null;
        $this->noticeLink = (isset($data['noticeLink'])) ? $data['noticeLink'] : null;
        $this->noticeType = (isset($data['noticeType'])) ? $data['noticeType'] : null;
        $this->noticeExpireDate = (isset($data['noticeExpireDate'])) ? $data['noticeExpireDate'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
