<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains PaymentTerm Model Functions
 */

namespace Core\Model;

class PaymentTerm
{

    public $paymentTermID;
    public $paymentTermName;
    public $paymentTermDescription;

    public function exchangeArray($data)
    {
        $this->paymentTermID = (!empty($data['paymentTermID'])) ? $data['paymentTermID'] : NULL;
        $this->paymentTermName = (!empty($data['paymentTermName'])) ? $data['paymentTermName'] : null;
        $this->paymentTermDescription = (!empty($data['paymentTermDescription'])) ? $data['paymentTermDescription'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

