<?php

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;

class PaymentTermTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getPaymentTerm($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('paymentTermID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getPaymentTermByName($paymentTermName)
    {
        $rowset = $this->tableGateway->select(array('paymentTermName' => $paymentTermName));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

}

?>
