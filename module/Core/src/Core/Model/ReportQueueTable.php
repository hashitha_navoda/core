<?php

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ReportQueueTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveReportQueue(ReportQueue $reportQueue)
    {
        $data = array(
            'reportQueueUrl' => $reportQueue->reportQueueUrl,
            'reportQueueReportName' => $reportQueue->reportQueueReportName,
            'reportQueueCategory' => $reportQueue->reportQueueCategory,
            'reportQueueStatus' => $reportQueue->reportQueueStatus,
            'reportQueueCreatedDateTime' => $reportQueue->reportQueueCreatedDateTime,
        );
        try {
            $this->tableGateway->insert($data);
            return $this->tableGateway->lastInsertValue;
        } catch (\Exception $e) {
            return false;
        }
    }


    public function getQueuedReportsByCategory($reportQueueCategory)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('reportQueue');
        $select->columns(array('*'));
        $select->where(array('reportQueueCategory' => $reportQueueCategory));
        $select->where(array('reportQueueStatus' => true));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    public function deleteViewedReportById($deletedID)
    {
        $data = array(
            'reportQueueStatus' => 0,
        );
        $this->tableGateway->update($data, array('reportQueueID' => $deletedID));
        return TRUE;
    }


    
}
