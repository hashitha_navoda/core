<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 * This file for contains Notification model
 */

namespace Core\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Notification implements InputFilterAwareInterface
{

    public $notificationID;
    public $notificationReferID;            // (sales / purchase) invoiceID, locationProductID
    public $locationID;
    public $notificationType;   // 1,2,3,4 invoice,payaments,minimum level, reorderlevel
    public $notificationTitle;
    public $notificationBody;
    public $notificationBody_si;
    public $notificationBody_tl;
    public $notificationUpdatedDateTime;
    public $status; // 0, 1
    public $notificationShowID; // 0, 1
    public $userID;

    /**
     * check input data fields if empty,
     *      add default value for each and assign to global variable
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->notificationID = (!empty($data['notificationID'])) ? $data['notificationID'] : null;
        $this->notificationReferID = (!empty($data['notificationReferID'])) ? $data['notificationReferID'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->notificationType = (!empty($data['notificationType'])) ? $data['notificationType'] : null;
        $this->notificationTitle = (!empty($data['notificationTitle'])) ? $data['notificationTitle'] : null;
        $this->notificationBody = (!empty($data['notificationBody'])) ? $data['notificationBody'] : null;
        $this->notificationBody_si = (!empty($data['notificationBody_si'])) ? $data['notificationBody_si'] : null;
        $this->notificationBody_tl = (!empty($data['notificationBody_tl'])) ? $data['notificationBody_tl'] : null;
        $this->notificationUpdatedDateTime = (!empty($data['notificationUpdatedDateTime'])) ? $data['notificationUpdatedDateTime'] : null;
        $this->status = (!empty($data['status'])) ? $data['status'] : 0;
        $this->notificationShowID = (!empty($data['notificationShowID'])) ? $data['notificationShowID'] : 0;
        $this->userID = (!empty($data['userID'])) ? $data['userID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'notificationID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'notificationReferID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'locationID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'notificationType',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'notificationTitle',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'notificationBody',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 150,
                                ),
                            ),
                        ),
            )));

            // 'updated date time' validater not applyed

            $inputFilter->add($factory->createInput(array(
                        'name' => 'status',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
