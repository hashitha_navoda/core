<?php

namespace Core\Model;

class LogDetails
{

    public $logID;
    public $previousData;
    public $newData;

    function exchangeArray($data)
    {
        $this->logID = (isset($data['logID'])) ? $data['logID'] : NULL;
        $this->previousData = (isset($data['previousData'])) ? $data['previousData'] : NULL;
        $this->newData = (isset($data['newData'])) ? $data['newData'] : NULL;
    }

    function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

?>
