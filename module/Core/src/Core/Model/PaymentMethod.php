<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Product Model Functions
 */

namespace Core\Model;

class PaymentMethod
{

    public $paymentMethodID;
    public $paymentMethodName;
    public $paymentMethodDescription;
    public $paymentMethodSalesFinanceAccountID;
    public $paymentMethodPurchaseFinanceAccountID;
    public $paymentMethodInSales;
    public $paymentMethodInPurchase;

    public function exchangeArray($data)
    {
        $this->paymentMethodID = (!empty($data['paymentMethodID'])) ? $data['paymentMethodID'] : null;
        $this->paymentMethodName = (!empty($data['paymentMethodName'])) ? $data['paymentMethodName'] : null;
        $this->paymentMethodDescription = (!empty($data['paymentMethodDescription'])) ? $data['paymentMethodDescription'] : null;
        $this->paymentMethodSalesFinanceAccountID = (!empty($data['paymentMethodSalesFinanceAccountID'])) ? $data['paymentMethodSalesFinanceAccountID'] : null;
        $this->paymentMethodPurchaseFinanceAccountID = (!empty($data['paymentMethodPurchaseFinanceAccountID'])) ? $data['paymentMethodPurchaseFinanceAccountID'] : null;
        $this->paymentMethodInSales = (!empty($data['paymentMethodInSales'])) ? $data['paymentMethodInSales'] : null;
        $this->paymentMethodInPurchase = (!empty($data['paymentMethodInPurchase'])) ? $data['paymentMethodInPurchase'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
