<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class NoticeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

//Modify the function
    public function saveNotice(Notice $notice)
    {
        $data = array(
            'noticeName' => $notice->noticeName,
            'notice' => $notice->notice,
            'noticeLink' => $notice->noticeLink,
            'noticeType' => $notice->noticeType,
            'noticeExpireDate' => $notice->noticeExpireDate,
        );

        $this->tableGateway->insert($data);
        $noticeID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $noticeID;
    }

    public function deleteNoticeByName($noticeName)
    {
        try {
            $result = $this->tableGateway->delete(array('noticeName' => $noticeName));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function deleteNoticeByID($noticeID)
    {
        try {
            $result = $this->tableGateway->delete(array('noticeID' => $noticeID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getNoticeIDByName($noticeName)
    {
        $result = $this->tableGateway->select(array('noticeName' => $noticeName));
        return $result->current();
    }

    public function getNoticeByDateAndTitle($noticeExpireDate, $noticeName)
    {
        $result = $this->tableGateway->select(array(
                        'noticeName' => $noticeName,
                        'noticeExpireDate' => $noticeExpireDate
                    ));
        return $result->current();
    }

}
