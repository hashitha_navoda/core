<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains Currency Model Functions
 */

namespace Core\Model;

class Currency
{

    public $currencyID;
    public $currencyName;
    public $currencySymbol;
    public $currencyRate;

    public function exchangeArray($data)
    {
        $this->currencyID = (!empty($data['currencyID'])) ? $data['currencyID'] : NULL;
        $this->currencyName = (!empty($data['currencyName'])) ? $data['currencyName'] : null;
        $this->currencySymbol = (!empty($data['currencySymbol'])) ? $data['currencySymbol'] : null;
        $this->currencyRate = (!empty($data['currencyRate'])) ? $data['currencyRate'] : 0.00;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
