<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 */

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Core\Model\Attachment;
use Zend\Db\Sql\Sql;

class AttachmentTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     *
     * @param \Core\Model\Attachment $attchment
     * @return int
     */
    public function save(Attachment $attchment)
    {
        $data = array(
            'attachmentName' => $attchment->attachmentName,
            'attachmentPath' => $attchment->attachmentPath,
            'attachmentSize' => $attchment->attachmentSize,
            'entityID' => $attchment->entityID,
            'entitySubTypeID' => $attchment->entitySubTypeID,
            'userID' => $attchment->userID,
        );

        $this->tableGateway->insert($data);
        $attachmentID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $attachmentID;
    }

    public function deleteAttachmentByEntityID($entityID)
    {
        try {
            $result = $this->tableGateway->delete(array('entityID' => $entityID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

    public function getAttachmentByEntityID($entityID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('attachment');
        $select->columns(array('*'));
        $select->where(array('entityID' => $entityID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
