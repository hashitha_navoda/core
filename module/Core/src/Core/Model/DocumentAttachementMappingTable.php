<?php

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class DocumentAttachementMappingTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveUploadFileMapData($fileData)
    {
        $fData = array(
            'documentID' => $fileData->documentID,
            'documentTypeID' => $fileData->documentTypeID,
            'documentRealName' => $fileData->documentRealName,
            'documentName' => $fileData->documentName,
        );
        if ($this->tableGateway->insert($fData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($documentID, $documentType)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('documentAttachementMapping')
                ->columns(array('*'))
                ->where(array('documentID' => $documentID, 'documentTypeID' => $documentType, 'deleted' => 0));
       
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultsList = Array();
        foreach ($results as $v) {
            $resultsList[] = $v;
        }
        return $resultsList;
    }

    public function updateDeletedAttachemnts($documentID, $documentTypeID)
    {
        $data = array(
            'deleted' => 1,
        );
        $this->tableGateway->update($data, array('documentID' => $documentID, 'documentTypeID' => $documentTypeID));
        return TRUE;
    }
}

?>
