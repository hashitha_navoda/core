<?php

namespace Core\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ReportQueue implements InputFilterAwareInterface
{

    public $reportQueueID;
    public $reportQueueUrl;
    public $reportQueueCategory;
    public $reportQueueStatus;
    
    /**
     * check input data fields if empty,
     *      add default value for each and assign to global variable
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->reportQueueID = (!empty($data['reportQueueID'])) ? $data['reportQueueID'] : null;
        $this->reportQueueReportName = (!empty($data['reportQueueReportName'])) ? $data['reportQueueReportName'] : null;
        $this->reportQueueUrl = (!empty($data['reportQueueUrl'])) ? $data['reportQueueUrl'] : null;
        $this->reportQueueCategory = (!empty($data['reportQueueCategory'])) ? $data['reportQueueCategory'] : null;
        $this->reportQueueStatus = (!empty($data['reportQueueStatus'])) ? $data['reportQueueStatus'] : null;
        $this->reportQueueCreatedDateTime = (!empty($data['reportQueueCreatedDateTime'])) ? $data['reportQueueCreatedDateTime'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function getInputFilter()
    {

    }

}

