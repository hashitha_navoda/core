<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains the Entity Model
 */

namespace Core\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Entity implements InputFilterAwareInterface
{

    public $entityID;
    public $createdBy;
    public $createdTimeStamp;
    public $updatedBy;
    public $updatedTimeStamp;
    public $deleted;
    public $deletedBy;
    public $deletedTimeStamp;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->createdBy = (!empty($data['createdBy'])) ? $data['createdBy'] : null;
        $this->createdTimeStamp = (!empty($data['createdTimeStamp'])) ? $data['createdTimeStamp'] : null;
        $this->updatedBy = (!empty($data['updatedBy'])) ? $data['updatedBy'] : null;
        $this->updatedTimeStamp = (!empty($data['updatedTimeStamp'])) ? $data['updatedTimeStamp'] : null;
        $this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : 0;
        $this->deletedBy = (!empty($data['deletedBy'])) ? $data['deletedBy'] : null;
        $this->deletedTimeStamp = (!empty($data['deletedTimeStamp'])) ? $data['deletedTimeStamp'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                'name' => 'entityID',
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'entityID',
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'updatedBy',
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            )));
            $inputFilter->add($factory->createInput(array(
                'name' => 'deletedBy',
                'required' => false,
                'filters' => array(
                    array('name' => 'Int'),
                ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
