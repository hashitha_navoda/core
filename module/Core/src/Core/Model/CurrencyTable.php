<?php

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class CurrencyTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('currency');
        $select->order('currencyName DESC');
        $select->where->notEqualTo('currencyRate', 0);
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $rowset = $this->tableGateway->select();
            return $rowset;
        }
    }

    public function getCurrency($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('currencyID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getCurrencyByName($currencyName)
    {

        $rowset = $this->tableGateway->select(array('currencyName' => $currencyName));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    //update function of currency
    public function updateCurrency(currency $currency)
    {
        $id = (int) $currency->currencyID;
        $data = array(
            'currencyRate' => $currency->currencyRate,
        );
        try {
            $this->tableGateway->update($data, array('currencyID' => $id));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    //get currency for search
    public function getCurrencyforSearch($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('currency');
        $select->where(new PredicateSet(array(new Operator('currency.currencyName', 'like', '%' . $keyword . '%'))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }
    
    /**
     * Get all active currencies 
     * @return mixed
     */
    public function getActiveCurrencies() 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('currency')
            ->columns(array('*'))
            ->where->notEqualTo('currency.currencyRate', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * this function returns currency list as an array
     * @return Array
     */
    public function getActiveCurrencyListForDropDown() 
    {
        $currencyList = array();
        
        foreach ($this->getActiveCurrencies() as $currency){
            $currencyList[$currency['currencyID']] = $currency['currencyName'].' ('.$currency['currencySymbol'].')';
        }
        return $currencyList;
    }
}

?>
