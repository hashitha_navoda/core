<?php

namespace Core\Model;

class Log
{

    public $featureID;
    public $logMessage;
    public $logTimeStamp;
    public $logUserID;
    public $logIP;
    public $logUserAgent;

    function exchangeArray($data)
    {
        $this->featureID = (!empty($data['featureID'])) ? $data['featureID'] : 0;
        $this->logMessage = (!empty($data['logMessage'])) ? $data['logMessage'] : NULL;
        $this->logTimeStamp = (!empty($data['logTimeStamp'])) ? $data['logTimeStamp'] : NULL;
        $this->logUserID = (!empty($data['logUserID'])) ? $data['logUserID'] : NULL;
        $this->logIP = (!empty($data['logIP'])) ? $data['logIP'] : NULL;
        $this->logUserAgent = (!empty($data['logUserAgent'])) ? $data['logUserAgent'] : NULL;
    }

    function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

?>
