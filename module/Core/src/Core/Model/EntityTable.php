<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains Entity Table Functions
 */

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;

class EntityTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    /**
     * @param Entity Model Variable
     * @author Damith Thamara <damith@thinkcube.com>
     * @return INT - (last Entered Entity ID)
     */
    public function saveEntity(Entity $entity)
    {
        $data = array(
            'createdBy' => $entity->createdBy,
            'createdTimeStamp' => $entity->createdTimeStamp,
            'updatedBy' => $entity->updatedBy,
            'updatedTimeStamp' => $entity->updatedTimeStamp,
            'deleted' => $entity->deleted,
            'deletedBy' => $entity->deletedBy,
            'deletedTimeStamp' => $entity->deletedTimeStamp
        );
        $this->tableGateway->insert($data);
        $insertedEntityID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedEntityID;
    }

    /**
     * @param Data arry with data to uodate
     * @author Damith Thamara <damith@thinkcube.com>
     * @return update result 1 - if success  0 if failed
     */
    public function updateEntity($data, $entityID)
    {
        $result = $this->tableGateway->update($data, array('entityID' => $entityID));
        return $result;
    }

    /**
     * Marks the entity as deleted but doesn't remove the record (sets deleted flag to 1)
     * @param int entityID
     * @author Malitta Nanayakkara <malitta@thinkcube.com>
     * @return bool
     */
    public function softDeleteEntity($entityID)
    {
        $result = $this->tableGateway->update(array('deleted' => 1), array('entityID' => $entityID));
        return $result;
    }

    public function getEntityByEntityID($entityID)
    {
        $result = $this->tableGateway->select(array('entityID' => $entityID));
        $row = $result->current();
        return $row;
    }

    public function beginTransaction()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = "START TRANSACTION;";
        $results = $adapter->query($sql);
        $results->execute();

        return true;
    }

    public function rollback()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = "ROLLBACK;";
        $results = $adapter->query($sql);
        $results->execute();

        return true;
    }

    public function commit()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = "COMMIT;";
        $results = $adapter->query($sql);
        $results->execute();

        return true;
    }

}
