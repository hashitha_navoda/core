<?php

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;

class JournalEntryDimensionTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveJournalEntryDimension($dimensionData)
    {
        $dimensionJEData = array(
            'journalEntryID' => $dimensionData->journalEntryID,
            'dimensionType' => $dimensionData->dimensionType,
            'dimensionValueID' => $dimensionData->dimensionValueID,
        );
        if ($this->tableGateway->insert($dimensionJEData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

}

?>
