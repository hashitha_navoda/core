<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 * This file for contains Notification model
 */

namespace Core\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Attachment implements InputFilterAwareInterface
{

    public $attachementID;
    public $attachmentName;
    public $attachmentPath;
    public $attachmentSize;     // in Bytes
    public $entityID;   // attachment related to the document entityID
    public $entitySubTypeID;
    public $userID;

    /**
     * check input data fields if empty,
     *      add default value for each and assign to global variable
     *
     * @param array $data
     */
    public function exchangeArray($data)
    {
        $this->attachementID = (!empty($data['attachementID'])) ? $data['attachementID'] : null;
        $this->attachmentName = (!empty($data['attachmentName'])) ? $data['attachmentName'] : null;
        $this->attachmentPath = (!empty($data['attachmentPath'])) ? $data['attachmentPath'] : null;
        $this->attachmentSize = (!empty($data['attachmentSize'])) ? $data['attachmentSize'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->entitySubTypeID = (!empty($data['entitySubTypeID'])) ? $data['entitySubTypeID'] : null;
        $this->userID = (!empty($data['userID'])) ? $data['userID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'attachementID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'attachmentName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'attachmentPath',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'attachmentSize',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'entityID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'entitySubTypeID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'userID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
