<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 * This file for user licence
 */

namespace Core\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Licence implements InputFilterAwareInterface
{

    public $licenceID;
    public $licenceName;
    public $licenceStartingDate;
    public $licenceExpireDate;
    public $userID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->licenceID = (isset($data['licenceID'])) ? $data['licenceID'] : null;
        $this->licenceName = (isset($data['licenceName'])) ? $data['licenceName'] : null;
        $this->licenceStartingDate = (isset($data['licenceStartingDate'])) ? $data['licenceStartingDate'] : null;
        $this->licenceExpireDate = (isset($data['licenceExpireDate'])) ? $data['licenceExpireDate'] : null;
        $this->userID = (isset($data['userID'])) ? $data['userID'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
