<?php

namespace Core\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class LogTable
{

    protected $table = 'log';
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('log')
                    ->columns(array('*'))
                    ->join('logDetails', 'log.logID = logDetails.logID', array('logDetailsID','previousData','newData'), 'left')
                    ->join('user', 'log.logUserID = user.userID', array('*'), 'left')
                    ->order(array('log.logID DESC'));
           
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveLog(Log $logger)
    {
        $data = array(
            'featureID' => $logger->featureID,
            'logMessage' => $logger->logMessage,
            'logTimeStamp' => $logger->logTimeStamp,
            'logUserID' => $logger->logUserID,
            'logIP' => $logger->logIP,
            'logUserAgent' => $logger->logUserAgent,
        );

        if ($this->tableGateway->insert($data)) {
            $logID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $logID;
        } else {
            return FALSE;
        }
    }

    public function getCompleteLogDetails()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('log');
        $select->join('user', 'log.logUserID = user.userID', array('userUsername', 'userFirstName', 'userEmail1'));
        $select->join('feature', 'log.featureID = feature.featureID', array('featureName', 'featureController', 'featureAction', 'featureCategory'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resulstArray = array();
        foreach ($results as $result) {
            $resulstArray[] = $result;
        }
        return $resulstArray;
    }

    public function getLogUntillToday($fromDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('log');
        $select->columns(array('logMessage', 'logTimeStamp'));
        $select->join('user', 'log.logUserID = user.userID', array('userUsername', 'userFirstName'));
        if ($fromDate != NULL) {
            $select->where("logTimeStamp >  '" . $fromDate . "'");
        }
        $select->where->like('logMessage', "%Log%");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resulstArray = array();
        foreach ($results as $result) {
            $resulstArray[] = $result;
        }
        return $resulstArray;
    }

    public function getLastActivity()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('log');
        $select->columns(array('logMessage', 'logTimeStamp'));
        $select->join('user', 'log.logUserID = user.userID', array('userUsername', 'userFirstName'));
        $select->order('logID DESC')->limit(1);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resulstArray = array();
        foreach ($results as $result) {
            $resulstArray[] = $result;
        }
        return $resulstArray;
    }

    public function getAuditLogByDateFilter($fromDate,$toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("log")
                ->columns(array('*'))
                ->join('logDetails', 'log.logID = logDetails.logID', array('logDetailsID','previousData','newData'), 'left')
                ->join('user', 'log.logUserID = user.userID', array('*'), 'left')
                ->order(array('log.logID DESC'));
        
        $select->where->between('logTimeStamp', $fromDate, $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

}
