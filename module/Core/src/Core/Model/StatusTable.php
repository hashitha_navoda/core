<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Unit Measure Table Functions
 */

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;

class StatusTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {

    }

    public function getStatusByStatusName($name)
    {
        $rowset = $this->tableGateway->select(array('statusName' => $name));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $name");
        }
        return $row;
    }

}
