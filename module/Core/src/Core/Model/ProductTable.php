<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

//use Zend\Db\Sql\Select;
//use Zend\Paginator\Adapter\DbSelect;
//use Zend\Paginator\Paginator;
//use Zend\Db\ResultSet\ResultSet;
//use Inventory\Model\Product;
//use Zend\Db\Adapter\Adapter;

class ProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @author Damith Thamara<damith@thinkcube.com>
     * @param INT $locationID
     * @return Resultset
     */
    public function getActiveProductListByLocation($locationID)
    { 
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array('*'));
        $select->join('productTax', 'product.productID = productTax.productID', array('taxID'), 'left');
        $select->join('tax', 'productTax.taxID = tax.id', array('*'), 'left');
        $select->join('productSerial', 'locationProduct.locationProductID = productSerial.locationProductID', array('productSerialID', 'serialProductBatchID' => new Expression('productSerial.productBatchID'), 'productSerialCode', 'productSerialSold', 'productSerialReturned','productSerialWarrantyPeriod', 'productSerialExpireDate','productSerialWarrantyPeriodType'), 'left');
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID'), 'left');
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomDecimalPlace', 'uomAbbr', 'uomState'), 'left');
        $select->join('productBatch', 'locationProduct.locationProductID = productBatch.locationProductID', array('productBatchID', 'productBatchCode', 'productBatchQuantity', 'productBatchExpiryDate', 'productBatchWarrantyPeriod'), 'left');
        $select->where(array('locationID' => $locationID));
        $select->where(array('productState' => 1));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * @author Prathap Weerasinhge <prathap@thinkcube.com>
     * @param INT $locationID
     * @return Resultset
     */
    public function getActiveProductDetailsByLocation($locationID, $productId = null, $updated_at = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array(
            'lp_id' => "locationProductID",
            'loc_id' => "locationID",
            'l_qty' => 'locationProductQuantity',
            'l_dic_pre' => 'locationDiscountPercentage',
            'l_dic_val' => 'locationDiscountValue',
            'dflt_pr' => 'defaultSellingPrice',
        ));

        $select->join(array('locEntity' => 'entity'), 'locationProduct.entityID = locEntity.entityID', array('deleted'));
        $select->join('productTax', 'product.productID = productTax.productID', array('taxID'), 'left');
        $select->join('tax', 'productTax.taxID = tax.id', array('*'), 'left');
        $select->join('productSerial', 'locationProduct.locationProductID = productSerial.locationProductID', array('productSerialID', 'serialProductBatchID' => new Expression('productSerial.productBatchID'), 'productSerialCode', 'productSerialWarrantyPeriod', 'productSerialSold', 'productSerialReturned','productSerialExpireDate'), 'left');
        $select->join('productHandeling', 'product.productID = productHandeling.productID', array('productHandelingID', 'productHandelingManufactureProduct', 'productHandelingPurchaseProduct', 'productHandelingSalesProduct', 'productHandelingConsumables', 'productHandelingFixedAssets', 'productHandelingGiftCard'), 'left');
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomID', 'productUomDisplay'), 'left');
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomDecimalPlace', 'uomName', 'uomAbbr', 'uomState'), 'left');
        $select->join('productBatch', 'locationProduct.locationProductID = productBatch.locationProductID', array('productBatchID', 'productBatchCode', 'productBatchQuantity', 'productBatchWarrantyPeriod','productBatchExpiryDate','productBatchPrice'), 'left');
        $select->join(array('entity' => 'entity'), 'entity.entityID = product.entityID', array(
            'deleted'), 'left');

        if (is_array($locationID)) {
            $select->where->in('locationID', $locationID);
        } else {
            $select->where(array('locationID' => $locationID));
        }


        if ($productId) {
            $select->where->in('product.productID', $productId);
        }

        $select->where(new PredicateSet(array(
            new Operator('productHandeling.productHandelingManufactureProduct', '=', 1),
            new Operator('productHandeling.productHandelingSalesProduct', '=', 1),
            new Operator('productHandeling.productHandelingGiftCard', '=', 1)
                ), PredicateSet::OP_OR));
        $select->where(array('productState' => 1));
        $select->where(array('entity.deleted' => 0));
        $select->where(array('locEntity.deleted' => 0));

        if ($updated_at) {
            $select->where->greaterThanOrEqualTo('locEntity.updatedTimeStamp', $updated_at);
        }

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    /**
     * @param  int $locationId
     * @param  int $productid
     * @return array
     */
    public function getProcessedActiveProducts($locationId, $productId = null)
    {
        $productsList = $this->getActiveProductDetailsByLocation($locationId, $productId);

        $products = array();
        while ($row = $productsList->current()) {
            $tempP = array();
            $tempP['pID'] = $row['productID'];
            $tempP['locID'] = $row['loc_id'];
            $tempP['pC'] = $row['productCode'];
            $tempP['pN'] = $row['productName'];
            $tempP['pT'] = $row['productTypeID'];
            $tempP['pBC'] = $row['productBarcode'];
            $tempP['lPID'] = $row['lp_id'];
            $tempP['pR'] = $row['dflt_pr'];
            $tempP['LPQ'] = $row['l_qty'];
            $tempP['dE'] = $row['productDiscountEligible'];
            $tempP['dP'] = $row['l_dic_pre'];
            $tempP['dV'] = $row['l_dic_val'];
            $tempP['bP'] = $row['batchProduct'];
            $tempP['sP'] = $row['serialProduct'];
            $tempP['sales'] = false;
            $tempP['purchase'] = false;
            $tempP['giftCard'] = $row['productHandelingGiftCard'];
            $tempP['rackID'] = $row['rackID'];
            $tempP['dtl'] = $row['itemDetail'];

            $tax = (isset($products[$row['productID']]['tax'])) ? $products[$row['productID']]['tax'] : array();
            if ($row['productTaxEligible'] == 1) {
                if ($row['taxID'] != NULL) {
                    $tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state']);
                }
            }

            $uom = (isset($products[$row['productID']]['uom'])) ? $products[$row['productID']]['uom'] : array();
            if ($row['uomID'] != NULL) {
                $uom[$row['uomID']] = array(
                    'uA' => $row['uomAbbr'],
                    'uDP' => $row['uomDecimalPlace'],
                    'uC' => $row['productUomConversion'],
                    'uS' => $row['uomState'],
                    'disp' => $row['productUomDisplay']
                );
            }

            $batchSerial = (isset($products[$row['productID']]['batchSerial'])) ? $products[$row['productID']]['batchSerial'] : array();
            $batchIDsInBatchSerial = (isset($products[$row['productID']]['productIDs'])) ? $products[$row['productID']]['productIDs'] : array();
            if ($row['serialProductBatchID'] != NULL && $row['serialProductBatchID'] == $row['productBatchID'] && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {
                $batchSerial[$row['productSerialID']] = array(
                    'PBID' => $row['serialProductBatchID'],
                    'PBC' => $row['productBatchCode'],
                    'PBW' => $row['productBatchWarrantyPeriod'],
                    'PSID' => $row['productSerialID'],
                    'PSC' => $row['productSerialCode'],
                    'PSW' => $row['productSerialWarrantyPeriod'],
                    'PSS' => $row['productSerialSold']
                );
                if ($row['productBatchID'] != NULL) {
                    $batchIDsInBatchSerial[$row['productBatchID']] = array('PBID' => $row['productBatchID']);
                }
            }

            $batch = (isset($products[$row['productID']]['batch'])) ? $products[$row['productID']]['batch'] : array();
            if ($row['productBatchID'] != NULL && $row['serialProductBatchID'] != $row['productBatchID']) {
                $batch[$row['productBatchID']] = array(
                    'PBID' => $row['productBatchID'],
                    'PBC' => $row['productBatchCode'],
                    'PBQ' => $row['productBatchQuantity'],
                    'PBW' => $row['productBatchWarrantyPeriod'],
                    'PSID' => $row['productSerialID']
                );
            }

            $serial = (isset($products[$row['productID']]['serial'])) ? $products[$row['productID']]['serial'] : array();
            if ($row['productSerialID'] != NULL && $row['serialProductBatchID'] == NULL && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {
                $serial[$row['productSerialID']] = array(
                    'PBID' => $row['serialProductBatchID'],
                    'PBC' => $row['productBatchCode'],
                    'PSID' => $row['productSerialID'],
                    'PSC' => $row['productSerialCode'],
                    'PSW' => $row['productSerialWarrantyPeriod'],
                    'PSS' => $row['productSerialSold']
                );
            }

            $tempP['tax'] = $tax;
            $tempP['uom'] = $uom;
            $tempP['batch'] = $batch;
            $tempP['serial'] = $serial;
            $tempP['batchSerial'] = $batchSerial;
            $tempP['productIDs'] = $batchIDsInBatchSerial;
            $products[$row['productID']] = $tempP;
        }

        return $products;
    }

    public function getActiveProductsNames($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array('locationID'));
        $select->join('entity', 'product.entityID = entity.entityID', array('deleted'));
        $select->where(array('locationID' => $locationID));
        $select->where(array('productState' => 1));
        $select->where(array('deleted' => 0));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function getActiveProductsIds($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("*"));
        $select->from('product');
        $select->join('locationProduct', 'product.productID = locationProduct.productID');
        $select->join('productHandeling', 'product.productID = productHandeling.productID');
        $select->join('entity', 'product.entityID = entity.entityID', array('deleted'));
        $select->where(array('locationID' => $locationID));
        $select->where(array('productState' => 1));
        $select->where(array('deleted' => 0));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function fetchAll($paginated = FALSE, $locationID = NULL)
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $locationProductID
     * @return type
     */
    public function getLocationProductDetailsByLPID($locationID, $productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array('*'));
        $select->join(array('locEntity' => 'entity'), 'locationProduct.entityID = locEntity.entityID', array('deleted'));
        $select->join('productTax', 'product.productID = productTax.productID', array('taxID'), 'left');
        $select->join('tax', 'productTax.taxID = tax.id', array('*'), 'left');
        $select->join('productSerial', 'locationProduct.locationProductID = productSerial.locationProductID', array('productSerialID', 'serialProductBatchID' => new Expression('productSerial.productBatchID'), 'productSerialCode', 'productSerialSold', 'productSerialReturned', 'productSerialWarrantyPeriod', 'productSerialExpireDate','productSerialWarrantyPeriodType'), 'left');
        $select->join('productHandeling', 'product.productID = productHandeling.productID', array('productHandelingID', 'productHandelingManufactureProduct', 'productHandelingPurchaseProduct', 'productHandelingSalesProduct', 'productHandelingConsumables', 'productHandelingFixedAssets', 'productHandelingGiftCard'), 'left');
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomID', 'productUomDisplay','productUomBase'), 'left');
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomDecimalPlace', 'uomAbbr', 'uomState', 'uomName'), 'left');
        $select->join('productBatch', 'locationProduct.locationProductID = productBatch.locationProductID', array('productBatchID', 'productBatchCode', 'productBatchQuantity', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchPrice'), 'left');
        $select->join(array('entity' => 'entity'), 'entity.entityID = product.entityID', array(
            'deleted'), 'left');
        $select->where(array('product.productID' => $productID, 'locationID' => $locationID));
        $select->where(array('productState' => 1));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function getLocationProductsByLocationProductID($locationProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('locationProduct');
        $select->where(array('locationProductID' => $locationProductID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

     //get products wise sales item details
    public function getItemWiseSalesReport($products, $isAllProducts, $locations, $isAllLocation, $formDate, $endDate)
    {
        $isAllProducts = filter_var($isAllProducts, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('product')
                ->columns(array('productID', 'productCode', 'productName',))
                ->join('salesInvoiceProduct', 'product.productID = salesInvoiceProduct.productID', array('*'), 'left')
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('*'), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('*'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order('product.productName ASC')
                ->where(array('entity.deleted' => '0'));
        if (!$isAllProducts) {
            $select->where->in('product.productID', $products);
        }

        if (!$isAllLocation) {
            $select->where->in('salesInvoice.locationID', $locations);
        }

        if($formDate != null && $endDate != null ){
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $formDate, $endDate);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    // get product related transaction
    public function getProductTransactionByPID($productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product')
               ->columns(array('*'));
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array('*'));
        $select->join('itemIn', 'itemIn.itemInLocationProductID = locationProduct.locationProductID', array('itemInID'), 'left');
        $select->join('itemOut', 'itemOut.itemOutLocationProductID = locationProduct.locationProductID', array('itemOutID'), 'left');
        $select->where(array('product.productID' => $productID));
        $select->group('locationProduct.locationID');
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }
    
    /** 
    * this function use to get all details about item(active or inactive) for given product ID
    * @param int $productID
    * return array
    **/
    public function getProductDetailsByProductID ($productID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product')
               ->columns(array('*'));
        $select->where(array('product.productID' => $productID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute()->current();
        return $rowset;   
    }

    /**
    * this function use to get all active products
    * return mix
    */
    public function getAllActiveProducts()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join('entity', 'product.entityID = entity.entityID', array('deleted'));
        $select->where(array('productState' => 1));
        $select->where(array('deleted' => 0));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }


    public function getActiveProductDetailsByLocationForPos($locationID, $productId = null, $updated_at = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('product');
        $select->join('locationProduct', 'product.productID = locationProduct.productID', array(
            'lp_id' => "locationProductID",
            'l_qty' => 'locationProductQuantity',
            'l_dic_pre' => 'locationDiscountPercentage',
            'l_dic_val' => 'locationDiscountValue',
            'dflt_pr' => 'defaultSellingPrice',
        ));

        $select->join(array('locEntity' => 'entity'), 'locationProduct.entityID = locEntity.entityID', array('deleted'));
        $select->join(array('productEntity' => 'entity'), 'product.entityID = productEntity.entityID', array('deleted'));
        $select->join('productTax', 'product.productID = productTax.productID', array('taxID'), 'left');
        $select->join('tax', 'productTax.taxID = tax.id', array('*'), 'left');
        $select->join('productSerial', 'locationProduct.locationProductID = productSerial.locationProductID', array('productSerialID', 'serialProductBatchID' => new Expression('productSerial.productBatchID'), 'productSerialCode', 'productSerialWarrantyPeriod', 'productSerialSold', 'productSerialReturned'), 'left');
        $select->join('productHandeling', 'product.productID = productHandeling.productID', array('productHandelingID', 'productHandelingManufactureProduct', 'productHandelingPurchaseProduct', 'productHandelingSalesProduct', 'productHandelingConsumables', 'productHandelingFixedAssets', 'productHandelingGiftCard'), 'left');
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomID', 'productUomDisplay'), 'left');
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomDecimalPlace', 'uomName', 'uomAbbr', 'uomState'), 'left');
        $select->join('productBatch', 'locationProduct.locationProductID = productBatch.locationProductID', array('productBatchID', 'productBatchCode', 'productBatchQuantity', 'productBatchWarrantyPeriod'), 'left');
        $select->join(array('entity' => 'entity'), 'entity.entityID = product.entityID', array(
            'deleted'), 'left');

        $select->where(array('locationID' => $locationID));
        if ($productId) {
            $select->where->in('product.productID', $productId);
        }

        $select->where(new PredicateSet(array(
            new Operator('productHandeling.productHandelingManufactureProduct', '=', 1),
            new Operator('productHandeling.productHandelingSalesProduct', '=', 1),
            new Operator('productHandeling.productHandelingGiftCard', '=', 1)
                ), PredicateSet::OP_OR));
        $select->where(array('productState' => 1));
        $select->where(array('entity.deleted' => 0));
        $select->where(array('locEntity.deleted' => 0));

        if ($updated_at) {
            $select->where(new PredicateSet(array(
            new Operator('locEntity.updatedTimeStamp', '>=', $updated_at),
            new Operator('productEntity.updatedTimeStamp', '>=', $updated_at),
                ), PredicateSet::OP_OR));
        }

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        // var_dump($query);
        return $rowset;
    }
}
