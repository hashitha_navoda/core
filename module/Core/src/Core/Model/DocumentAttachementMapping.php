<?php
namespace Core\Model;

class DocumentAttachementMapping
{

    public $documentAttachemntMapID;
    public $documentID;
    public $documentTypeID;
    public $documentRealName;
    public $documentName;
    public $deleted;

    public function exchangeArray($data)
    {
        $this->documentAttachemntMapID = (!empty($data['documentAttachemntMapID'])) ? $data['documentAttachemntMapID'] : NULL;
        $this->documentID = (!empty($data['documentID'])) ? $data['documentID'] : null;
        $this->documentTypeID = (!empty($data['documentTypeID'])) ? $data['documentTypeID'] : null;
        $this->documentRealName = (!empty($data['documentRealName'])) ? $data['documentRealName'] : null;
        $this->documentName = (!empty($data['documentName'])) ? $data['documentName'] : null;
        $this->deleted = (!empty($data['deleted'])) ? $data['deleted'] : 0;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
