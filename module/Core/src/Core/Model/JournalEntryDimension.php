<?php

namespace Core\Model;

class JournalEntryDimension
{

    public $journalEntryDimensionID;
    public $journalEntryID;
    public $dimensionType;
    public $dimensionValueID;

    public function exchangeArray($data)
    {
        $this->journalEntryDimensionID = (!empty($data['journalEntryDimensionID'])) ? $data['journalEntryDimensionID'] : NULL;
        $this->journalEntryID = (!empty($data['journalEntryID'])) ? $data['journalEntryID'] : null;
        $this->dimensionType = (!empty($data['dimensionType'])) ? $data['dimensionType'] : null;
        $this->dimensionValueID = (!empty($data['dimensionValueID'])) ? $data['dimensionValueID'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

