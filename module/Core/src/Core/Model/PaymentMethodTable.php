<?php

namespace Core\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PaymentMethodTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("paymentMethod")
                ->columns(array("*"))
                ->join("financeAccounts", "paymentMethod.paymentMethodSalesFinanceAccountID = financeAccounts.financeAccountsID", array("salesFinanceAccountsCode" => new Expression('financeAccounts.financeAccountsCode'),"salesFinanceAccountsName" => new Expression('financeAccounts.financeAccountsName')), "left")
                ->join(array('purchaseFinanceAccounts'=>'financeAccounts'), "paymentMethod.paymentMethodPurchaseFinanceAccountID = purchaseFinanceAccounts.financeAccountsID", array("purchaseFinanceAccountsCode" => new Expression('purchaseFinanceAccounts.financeAccountsCode'),"purchaseFinanceAccountsName" => new Expression('purchaseFinanceAccounts.financeAccountsName')), "left");
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }

        return $results;
    }

    public function getPaymentMethod($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('paymentMethodID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getPaymentMethodByName($paymentterm)
    {
        $rowset = $this->tableGateway->select(array('paymentMethodName' => $paymentterm));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return List of PaymentMethods Array
     *
     */
    public function getPaymentMethods()
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('paymentMethods');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val['method'];
        }

        return $countArray;
    }


    public function updatePaymentMethods($data, $paymentMethodID)
    {
        try {
            $this->tableGateway->update($data, array('paymentMethodID' => $paymentMethodID));
            return true;
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    public function getPaymentMethodById($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("paymentMethod")
                ->columns(array("*"))
                ->join("financeAccounts", "paymentMethod.paymentMethodSalesFinanceAccountID = financeAccounts.financeAccountsID", array("salesFinanceAccountsCode" => new Expression('financeAccounts.financeAccountsCode'),"salesFinanceAccountsName" => new Expression('financeAccounts.financeAccountsName')), "left")
                ->join(array('purchaseFinanceAccounts'=>'financeAccounts'), "paymentMethod.paymentMethodPurchaseFinanceAccountID = purchaseFinanceAccounts.financeAccountsID", array("purchaseFinanceAccountsCode" => new Expression('purchaseFinanceAccounts.financeAccountsCode'),"purchaseFinanceAccountsName" => new Expression('purchaseFinanceAccounts.financeAccountsName')), "left");
        $select->where(array('paymentMethodID' =>$id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results->current();
    }


    /*
    * get the payment method data by gl account id
    */
    public function getPaymentMethodByGlAccountID($glAccountID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('paymentMethod')
                ->columns(array('*'))
                ->where(new PredicateSet(array(new Operator('paymentMethodSalesFinanceAccountID', '=',$glAccountID), 
                    new Operator('paymentMethodPurchaseFinanceAccountID', '=',$glAccountID)
                ), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }
}

?>
