<?php

namespace Core\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class LogDetailsTable
{

    protected $table = 'log';
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveLogDetails(LogDetails $logDetails)
    {
        $data = array(
            'logID' => $logDetails->logID,
            'previousData' => $logDetails->previousData,
            'newData' => $logDetails->newData,
        );

        if ($this->tableGateway->insert($data)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function getLogDetailsByLogID($logID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("logDetails")
                ->columns(array('*'))
                ->join('log', 'logDetails.logID = log.logID', array('featureID'), 'left')
                ->join('feature', 'log.featureID = feature.featureID', array('featureAction'), 'left')
                ->order(array('logDetails.logDetailsID ASEC'))
                ->where(array("logDetails.logID" => $logID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }
}
