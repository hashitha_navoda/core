<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 */

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Core\Model\Notification;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Sql;

class NotificationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * get whole details for notification list from Notification table
     * @return type
     */
    public function fetchAll($locationID)
    {
        $adpater = $this->tableGateway->getAdapter();
        $notification = new TableGateway('notification', $adpater);
        $rowset = $notification->select(function (Select $select) use ($locationID) {
            $select->order('notificationUpdatedDateTime DESC');
            $select->where(array('locationID' => $locationID)
            );
        });
        return $rowset;
    }

    /**
     * get whole details of open status notification list from Notification table
     * @return type
     */
    public function fetchAllOpenStatus($locationID, $filterCategory = null)
    {
        $adpater = $this->tableGateway->getAdapter();
        $notification = new TableGateway('notification', $adpater);
        $rowset = $notification->select(function (Select $select) use ($locationID, $filterCategory) {
            $select->order('notificationUpdatedDateTime DESC');
            $select->where(array('status' => 0,
                'locationID' => $locationID,
                'notificationShowID' => 0,
                    )
            );
            if ($filterCategory != null) {
                $select->where(array('notificationType' => $filterCategory,)
                );
            }
        });
        return $rowset;
    }

    /**
     * store the new notification on Notification table
     * @param array $array data to store on Notification Table
     */
    public function saveNotification(Notification $notification)
    {
        $data = array(
            'notificationReferID' => $notification->notificationReferID,
            'locationID' => $notification->locationID,
            'notificationType' => $notification->notificationType,
            'notificationTitle' => $notification->notificationTitle,
            'notificationBody' => $notification->notificationBody,
            'notificationBody_si' => $notification->notificationBody_si,
            'notificationBody_tl' => $notification->notificationBody_tl,
            'notificationUpdatedDateTime' => $notification->notificationUpdatedDateTime,
            'status' => $notification->status,
            'userID' => $notification->userID,
        );

        $this->tableGateway->insert($data);
        $notificationID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $notificationID;
    }

    /**
     * check all nodification for their validatation
     */
    public function changeStatus(Notification $notification)
    {
        $data = array(
            'status' => $notification->status
        );

        try {
            $this->tableGateway->update($data, array(
                'notificationID' => $notification->notificationID,
                'locationID' => $notification->locationID,
            ));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }

        echo 'the data ' . $notification->notificationID . ' status going to change \'close\' <br />';
        exit;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Change Status of notification by userID, locationID,ReferenceID
     * @param \Core\Model\Notification $notification
     * @return boolean
     */
    public function changeStatusByRefIDAndUserID(Notification $notification)
    {
        if ($notification->notificationReferID && $notification->userID) {
            $data = array(
                'status' => $notification->status
            );
            try {
                $this->tableGateway->update($data, array(
                    'userID' => $notification->userID,
                    'locationID' => $notification->locationID,
                    'notificationReferID' => $notification->notificationReferID
                ));
                return true;
            } catch (\Exception $e) {
                return FALSE;
            }
        }
    }

    /**
     * @author Madawa Chandrarathne <madawa@thinkcube.com>
     *
     * Update Notification
     * @param array $data
     * @param string $incomingPaymentMethodCheque
     * @return boolean
     */
    public function updateNotification($data, $referId, $type)
    {
        try {
            return $this->tableGateway->update($data, array('notificationReferID' => $referId, 'notificationType' => $type));
        } catch (\Exception $ex) {
            return FALSE;
        }
    }

    /**
     * Get Currently available notifications in open state
     * @author sharmilan <sharmilan@thincube.com>
     * @param type $referenceIDs
     * @param type $locations
     * @return array
     */
    public function getOpenNotification($referenceIDs, $locations, $notificationType)
    {
        $adpater = $this->tableGateway->getAdapter();
        $sql = new Sql($adpater);
        $select = $sql->select();
        $select->from('notification');
        $select->where(['status' => 0, 'notificationType' => $notificationType]);
        $select->where->in('notificationReferID', $referenceIDs)->AND
                ->in('locationID', $locations);

        $statment = $sql->prepareStatementForSqlObject($select);
        $result = $statment->execute();
        if (count($result) > 0) {
            return iterator_to_array($result);
        } else {
            return[];
        }
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to update showState value
     * @param type $notificationID
     * @return boolean
     */
    public function updateShowState($notificationID)
    {
        $data = array(
            'notificationShowID' => 1 // if show state is 1 then not show in the view
        );

        try {
            $this->tableGateway->update($data, array(
                'notificationID' => $notificationID,
            ));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public function getNotificationCountForMobileApp()
    {
        try {
            $adpater = $this->tableGateway->getAdapter();
            $sql = new Sql($adpater);
            $select = $sql->select();
            $select->from('notification');
            $select->columns(array('notificCount'=> new Expression('COUNT(notificationID)')));
            $select->where(['status' => 0, 'mobileNotificationShow' => 0, 'notificationShowID' => 0]);
            

            $statment = $sql->prepareStatementForSqlObject($select);
            $result = $statment->execute()->current();
            return $result;
            
        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }

    public function getAllOpenNotificationToTheMobile()
    {
        $adpater = $this->tableGateway->getAdapter();
        $sql = new Sql($adpater);
        $select = $sql->select();
        $select->from('notification');
        $select->columns(array('*'));
        $select->where(['status' => 0, 'mobileNotificationShow' => 0,'notificationShowID' => 0]);
        $select->order('notificationID DESC');

        $statment = $sql->prepareStatementForSqlObject($select);
        $result = $statment->execute();
        if (!$result) {
            return null;
        } else {
            return $result;
            
        }
    }

    public function getNotificationByID($notificationID)
    {
        $adpater = $this->tableGateway->getAdapter();
        $sql = new Sql($adpater);
        $select = $sql->select();
        $select->from('notification');
        $select->columns(array('*'));
        $select->where(['status' => 0, 'mobileNotificationShow' => 0, 'notificationID' => $notificationID]);
        

        $statment = $sql->prepareStatementForSqlObject($select);
        $result = $statment->execute();
        return $result;
    }

}
