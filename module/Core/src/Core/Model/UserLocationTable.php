<?php

namespace Core\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

class UserLocationTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select('userLocation');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new UserLocation());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getUserLocation($userLocationID)
    {
        $userLocationID = (int) $userLocationID;
        $rowset = $this->tableGateway->select(array('userLocationID' => $userLocationID));
        $row = $rowset->current();
        if (!$row) {
            throw new \Exception("Could not find row $userLocationID");
        }
        return $row;
    }

    public function saveUserLocation(UserLocation $userLocation)
    {
        $data = array(
            'userLocationID' => $userLocation->userLocationID,
            'userID' => $userLocation->userID,
            'locationID' => $userLocation->locationID,
        );

        $userLocationID = (int) $userLocation->userLocationID;

        if ($userLocationID == 0) {
            if ($this->tableGateway->insert($data)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            if ($this->getUserLocation($userLocationID)) {
                if ($this->tableGateway->update($data, array('userLocationID' => $userLocationID))) {
                    return TRUE;
                } else {
                    return FALSE;
                }
            } else {
                throw new \Exception('Form id does not exist');
                return FALSE;
            }
        }
    }

    public function deleteLocation($userLocationID)
    {
        try {
            $this->tableGateway->delete(array('userLocationID' => $userLocationID));
            return array('status' => TRUE, 'exception' => NULL);
        } catch (\Exception $e) {
            return array('status' => FALSE, 'exception' => $e);
        }
    }

    public function getUserLocationsByID($userID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('userLocation');
        $select->join('location', 'userLocation.locationID = location.locationID', array('locationID', 'locationName', 'locationCode'));
        $select->where(array('userID' => $userID, 'location.locationStatus' => 1));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        $locations = array();
        while ($row = $rowset->current()) {
            $locations[$row['locationID']] = array('locationName' => $row['locationName'], 'locationCode' => $row['locationCode']);
        }
        return $locations;
    }

    public function deleteUserLocation($locationID, $userID)
    {
        return $this->tableGateway->delete(array(
                    'userID' => $userID,
                    'locationID' => $locationID
        ));
    }

    public function insertUserLocation($locationID, $userID)
    {
        return $this->tableGateway->insert(array(
                    'userID' => $userID,
                    'locationID' => $locationID
        ));
    }

}
