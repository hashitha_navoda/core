<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */

namespace Core\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Product
{

    public $productID;
    public $productCode;
    public $productName;
    public $productTypeID;
    public $productDescription;
    public $productDiscountEligible;
    public $productDiscountPercentage;
    public $productDiscountValue;
    public $productImageURL;
    public $productState;
    public $categoryID;
    public $productGlobalProduct;
    public $batchProduct;
    public $serialProduct;
    public $hsCode;
    public $customDutyValue;
    public $customDutyPercentage;
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->productCode = (!empty($data['productCode'])) ? $data['productCode'] : null;
        $this->productName = (!empty($data['productName'])) ? $data['productName'] : null;
        $this->productTypeID = (!empty($data['productTypeID'])) ? $data['productTypeID'] : null;
        $this->productDescription = (!empty($data['productDescription'])) ? $data['productDescription'] : null;
        $this->productDiscountEligible = (!empty($data['productDiscountEligible'])) ? (int) $data['productDiscountEligible'] : 0;
        $this->productDiscountPercentage = (!empty($data['productDiscountPercentage'])) ? $data['productDiscountPercentage'] : 0.00;
        $this->productDiscountValue = (!empty($data['productDiscountValue'])) ? $data['productDiscountValue'] : 0.00;
        $this->productTaxEligible = (!empty($data['productTaxEligible'])) ? $data['productTaxEligible'] : 0;
        $this->productImageURL = (!empty($data['productImageURL'])) ? $data['productImageURL'] : null;
        $this->productState = (!empty($data['productState'])) ? $data['productState'] : 0;
        $this->categoryID = (!empty($data['categoryID'])) ? $data['categoryID'] : null;
        $this->productGlobalProduct = (!empty($data['productGlobalProduct'])) ? $data['productGlobalProduct'] : 0;
        $this->batchProduct = (!empty($data['batchProduct'])) ? $data['batchProduct'] : 0;
        $this->serialProduct = (!empty($data['serialProduct'])) ? $data['serialProduct'] : 0;
        $this->hsCode = (!empty($data['hsCode'])) ? $data['hsCode'] : null;
        $this->customDutyValue = (!empty($data['customDutyValue'])) ? $data['customDutyValue'] : 0;
        $this->customDutyPercentage = (!empty($data['customDutyPercentage'])) ? $data['customDutyPercentage'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'productID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'productCode',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 50,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'productName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'productCategoryID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'digits'
                            ),
                        ),
            )));

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
