<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class UserNoticeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveUserNotice(UserNotice $userNotice)
    {
        $data = array(
            'userID' => $userNotice->userID,
            'noticeID' => $userNotice->noticeID,
        );

        $this->tableGateway->insert($data);
        $userNoticeID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $userNoticeID;
    }

    public function deleteUserNoticeByID($noticeID)
    {
        try {
            $result = $this->tableGateway->delete(array('noticeID' => $noticeID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

}
