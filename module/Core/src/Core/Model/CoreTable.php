<?php

namespace Core\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class CoreTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //this function is use for all tabele for details of defined table,
    //use in company/updateReferenceAction
    public function checkReferenceNumber($tname, $tcolum, $locationColum, $Code, $branch)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from($tname)
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "$tname.entityID = e.entityID");
        if ($branch) {
            $select->join('location', "$tname.$locationColum = location.locationID", array('*'), 'left');
//            Edited following where clause by @auther Sandun Dissanayake(Because of does not save referece when editing)
//            $select->where(array($locationColum => $branch));//previews where caluse
            $select->where(array('location.locationID' => $branch));
        }
        $select->where(array('e.deleted' => '0'))
                ->where(array("$tname.$tcolum" => $Code));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute()->current();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    //this function is usefor check reference number of purchase returns
    public function checkReferenceNumberForPurchaseReturns($Code, $branch)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('purchaseReturn')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), 'e.entityID = purchaseReturn.purchaseReturnEntityID', array('deleted'), 'left');
        if ($branch) {
            $select->join('grn', 'grn.grnID = purchaseReturn.purchaseReturnGrnID', array('*'), 'left');
            $select->join('location', "location.locationID = grn.grnRetrieveLocation", array('*'), 'left');
            $select->where(array('location.locationID' => $branch));
        }
        $select->where(array('e.deleted' => '0'))
                ->where(array("purchaseReturn.purchaseReturnCode" => $Code));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute()->current();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    //this function is usefor get Document Type
    public function getDocumentType()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('documentType')
                ->columns(array('*'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

}
