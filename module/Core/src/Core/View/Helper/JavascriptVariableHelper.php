<?php

/**
 * @author sharmilan <sharmilan@thinkcube.com>
 */

namespace Core\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class JavascriptVariableHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    protected $serviceLocator;
    protected $vars;

    public function __invoke()
    {
        $data = $this->getServiceLocator()->getServiceLocator()->get('JavascriptVariableController')->indexAction();
        if (is_array($this->vars)) {
            foreach ($this->vars as $var) {
                $actionName = $var . 'Action';
                $temp = $this->getServiceLocator()->getServiceLocator()->get('JavascriptVariableController')->$actionName();
                $data = array_merge($data, $temp);
            }
        }
        return $this->getView()->render('core/javascript-variable/index.phtml', array('data' => $data));
    }

    public function setvars(array $vars)
    {
        $this->vars = $vars;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
