<?php

namespace Core\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class UserDateFormatHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    //protected $notificationController;
    protected $serviceLocator;
   // protected $placeholder;

    public function __invoke($date,$dateformat)
    {
        //$dateformat = $_SESSION['ezBizUser']['dateFormat'];
switch ($dateformat) {
            case 'yyyy/mm/dd':
                return date('Y/m/d', strtotime($date));
                break;
            case 'yyyy-mm-dd':
                return date('Y-m-d', strtotime($date));
                break;
            case 'yyyy.mm.dd':
                return date('Y.m.d', strtotime($date));
                break;
            case 'dd/mm/yyyy':
                return date('d/m/Y', strtotime($date));
                break;
            case 'dd-mm-yyyy':
                return date('d-m-Y', strtotime($date));
                break;
            case 'dd.mm.yyyy':
                return date('d.m.Y', strtotime($date));
                break;
            case 'mm/dd/yyyy':
               // $date = preg_replace("/([0-9]{4})([0-9]{4})([0-9]{4})/", "$2$3$1", $date);
            return date('m/d/Y', strtotime($date));
            case 'mm-dd-yyyy':
                $date = date('m/d/Y', strtotime($date));
                return str_replace('/','-',$date);;
            case 'mm.dd.yyyy':
                return date('m.d.Y', strtotime($date));
        }
       //$date =  $this->convertDateToUserFormat($dateformat,$date);
      return $date;
    }
    
    public function getPlaceHolder()
    {
        return $placeholder;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
