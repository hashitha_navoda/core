<?php

namespace Core\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Db\Sql\Sql;

class NoticeHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    public function __invoke()
    {
        $userID = $_SESSION['ezBizUser']['userID'];
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();
        $dbAdapter = $serviceLocator->get('Zend\Db\Adapter\Adapter');
        if (!isset($userID) || $userID == NULL || $userID == '') {
            $statement = $dbAdapter->query("SELECT userID FROM `user` LIMIT 1");
            $result = $statement->execute();
            $userID = $result->current()['userID'];
        }
        $gmDate = gmdate('Y-m-d H:i:s');
        $statement = $dbAdapter->query("SELECT * FROM `notice` WHERE noticeID NOT IN (SELECT noticeID from userNotice WHERE userID = $userID) AND noticeExpireDate > '$gmDate';");
        $results = $statement->execute();
        $resultArray = [];
        $noticeStatuses = $serviceLocator->get('config')['noticeStatus'];
        foreach ($results as $result) {
            $result['noticeStatusLable'] = 'notice_panel_' . $noticeStatuses[$result['noticeType']];
            $resultArray[] = $result;
        }
        return $this->getView()->render('core/notification/notice.phtml', array('notices' => $resultArray));
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
