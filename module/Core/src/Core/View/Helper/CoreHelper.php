<?php

namespace Core\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class CoreHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    protected $notificationController;
    protected $serviceLocator;

    public function __construct($notificationController)
    {
        $this->notificationController = $notificationController;
    }

    public function __invoke()
    {
        $viewVariables = $this->notificationController->setViewVariables();

        return $this->getView()->render('core/notification/index.phtml', array('data' => $viewVariables));
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
