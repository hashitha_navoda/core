<?php

namespace Core\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;

class ToolTipHelper extends AbstractHelper implements ServiceLocatorAwareInterface
{

    protected $serviceLocator;

    public function __invoke($key)
    {
        $serviceLocator = $this->getServiceLocator()->getServiceLocator();
        $Totaltips = $serviceLocator->get('config')['tooltips'];
        $tip = $Totaltips[$key];
        return $tip;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }

    public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
        return $this;
    }

}
