<?php

namespace Core\BackgroundJobs;

class Job
{
    public $currentJob;

    const QUEUE = 'default';

    public function setUp()
    {
        $this->currentJob = $this->job;
    }

    public function perform() {}

    public function tearDown()
    {
        try {
            return \Resque::dequeue(self::QUEUE, [ $this->currentJob->payload['class'] => $this->currentJob->payload['id']]);
        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return false;
        }
    }

    public static function add($data)
    {
        try {
            // TODO :: use vhost configurations
            $redisHost = 'localhost:6379';
            \Resque::setBackend($redisHost); // set backend

            //\Resque::setBackend(getenv('REDISHOST') . ':' . getenv('REDISPORT')); // set backend

            return \Resque::enqueue(static::QUEUE, get_called_class(), $data, true); // add to queue

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return false;
        }
    }

}
