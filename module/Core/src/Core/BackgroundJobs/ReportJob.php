<?php

namespace Core\BackgroundJobs;

use Core\Library\Util;

class ReportJob extends Job
{
    const QUEUE = 'reporting';

    public function perform()
    {
        try {

            $url = $this->args['meta']['url'] . '/api/app/generate-report';

            $headers = [
                'api' => true,
                'Authorization' => '42QeDIojjqD3g:42d6c7d61481d1c21bd1635f59edae05'
            ];
            $options = ['timeout' => 3600];

            $response = Util::httpRequest($url, 'POST', $this->args, $headers, $options);

            if ($response) {
                return true;
            }
            return false;

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return false;
        }
    }

}
