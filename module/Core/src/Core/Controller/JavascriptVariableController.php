<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * This class can be used to generate dynamic JS files which could contain global variables
 */

namespace Core\Controller;

use Zend\Session\Container;
use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class JavascriptVariableController extends CoreController
{

    protected $viewModel;
    protected $allLocations;
    protected $location;

    public function __construct()
    {
        parent::__construct();
        $this->viewModel = new ViewModel();
        $this->viewModel->setTerminal(true);
        $this->viewModel->setTemplate('core/javascript-variable/index.phtml');
        $this->viewModel->data = array();

        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->allLocations = $this->user_session->userAllLocations;
        $this->userActiveLocation = $this->user_session->userActiveLocation;
        $this->location = $this->user_session->userActiveLocation['locationID'];
    }

    public function indexAction()
    {
// set base path
        $uri = $this->getServiceLocator()->get('Request')->getUri();

// assume SSL if ENV is live
        $scheme = (in_array(getenv('ENV'), array("live", "demo"))) ? 'https' : $uri->getScheme();
        $base = sprintf('%s://%s', $scheme, $uri->getHost());

// set user details
        $user_session = new Container('ezBizUser');

        return [
            'BASE_URL' => $base,
            'HELP_TOOL_URL' => $this->getServiceLocator()->get('Config')['helptool_url'],
            'USER_NAME' => $user_session->username,
            'COMPANY_NAME' => @$user_session->companyDetails->companyName,
            'U_DATE_R_OVERRIDE' => $user_session->UniversalDateRestrictionOverride,
        ];
    }

    /**
     * function to get javascript messages
     * @return messages array
     */
    public function messagesAction()
    {
        $totalMessages = $this->getServiceLocator()->get('config')['messages'];
        $this->addVariable('messages', $totalMessages);

        header("Expires: Sat, 28 Jul 2040 05:00:00 GMT");
        header("Cache-Control: private, max-age=6000, pre-check=6000");
        header("Pragma: private");

        return $this->viewModel;
    }

    /**
     * @author Damith Thamara  <damith@thinkcube.com>
     * Function to send supplier to frontEnd
     * @return supplier Array
     */
    public function suppliersAction()
    {
        $suppliersList = $this->CommonTable('Inventory\Model\SupplierTable')->getSuppliers();
        $suppliers = array();
        while ($row = $suppliersList->current()) {
            $suppliers[$row->supplierID] = $row->supplierCode . '-' . $row->supplierName;
        }
        return ['Suppliers' => $suppliers];
    }

    public function taxesAction()
    {
        $taxDetail = $this->CommonTable('Core\Model\TaxTable')->getAllTax();
        $taxes = [];
        $taxDetails = ($taxDetail == null) ? array() : $taxDetail;
        foreach ($taxDetails as $row) {
            $taxes[$row['id']] = (isset($taxes[$row['id']])) ? $taxes[$row['id']] : [
                'tID' => $row['id'],
                'tN' => $row['taxName'],
                'tT' => $row['taxType'],
                'tP' => $row['taxPrecentage'],
                'tS' => $row['state'],
                'susTax'=>$row['taxSuspendable'],
                'sT' => []
            ];

            if ($row['simpleTaxID']) {
                $taxes[$row['id']]['sT'][$row['simpleTaxID']] = [
                    'stN' => $row['sTaxName'],
                    'stP' => $row['sTaxPrecentage'],
                    'stS' => $row['sState']
                ];
            }
        }
        $tr_number = $this->CommonTable('CompanyTable')->getTRNumber();
        $taxStatus = isset($tr_number->trNumber) ? TRUE : FALSE;

        return [
            'TAXES' => $taxes,
            'TAXSTATUS' => $taxStatus
        ];
    }

    public function categoriesAction()
    {

        $allCategoryPaths = $this->CommonTable('Inventory/Model/CategoryTable')->getHierarchyPaths();
        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();

        return [
            'CATEGORIES_PATH_LIST' => $allCategoryPaths,
            'CATEGORIES_LIST' => $allCategoryList
        ];
    }

    /**
     * @author Prathap Weerasinghe <Prathap@thinkcube.com>
     * @return Products names in an array
     */
    public function allProductNamesAction()
    {
        $data = array();
        $result = $this->CommonTable('Core\Model\ProductTable')->getActiveProductsNames($this->location);
        while ($row = $result->current()) {
            $data[] = $row["productName"];
        }

        return [
            "products_names" => $data
        ];
    }

    public function allCustomerNamesAction()
    {
        $result = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        $data = array();
        $data1 = array();
        while ($row = $result->current()) {
            $data[$row["customerID"]] = $row["customerName"] . '-' . $row["customerCode"];
            $data1[$row["customerID"]] = $row;
        }

        return [
            "customers_names" => $data,
        ];
    }

    public function customerListWithoutDefaultCustomerAction()
    {
        $result = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        $data = array();
        $data1 = array();
        while ($row = $result->current()) {
            $data[$row["customerID"]] = $row["customerName"] . '-' . $row["customerCode"] . ' - ' . $row['customerTelephoneNumber'];
            $data1[$row["customerID"]] = $row;
        }

        unset($data[0]);
        unset($data1[0]);
        if ($this->getRequest()->isPost()) {
            $this->status = ture;
            $this->data = array("CUSTOMERS_NAMES" => $data);
            return $this->JSONRespond();
        }else{
            return [
            "customers_names" => $data,
            ];
        }

    }

    public function deliveryNoteAction()
    {
        $result = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getLocationRelatedDeliveryNotes($this->location);
        $data = array();
        while ($row = $result->current()) {
            $row = (array) $row;
            $data[$row["deliveryNoteID"]] = $row["deliveryNoteCode"];
        }
        return ["delivery_note" => $data];
    }

    /**
     * @author Prathap Weerasinghe <Prathap@thinkcube.com>
     * @return Product codes as an array
     */
    public function allProductIdsAction()
    {
        $data = array();
        $dataName = array();
        $dataNameCode = array();

        $result = $this->CommonTable('Core\Model\ProductTable')->getActiveProductsIds($this->location);
        foreach ($result as $row) {
            if ($row['productHandelingManufactureProduct'] == 1 || $row['productHandelingSalesProduct'] == 1) {
                $data[$row["productID"]] = $row["productCode"];
                $dataName[$row["productID"]] = $row["productName"];
                $dataNameCode[$row["productID"]] = $row["productCode"] . ' - ' . $row["productName"];
            }
        }

        return [
            "products_ids" => $data,
            "products_names" => $dataName,
            "products_code_and_name" => $dataNameCode
        ];
    }

    /**
     * @author Prathap Weerasinghe <prathap@thinkcube.com>
     * @return array product details
     * @param param1 from route param1=locationID
     * get all location related active product
     */
    public function allActiveProductsByLocationAction()
    {
        $productsList = $this->CommonTable('Core\Model\ProductTable')->getActiveProductDetailsByLocation($this->location);
        $productUom = array();
        $products = array();
        foreach ($productsList as $row) {
            $tempP = array();
            $tempP['pC'] = $row['productCode'];
            $tempP['pID'] = $row['productID'];
            $tempP['lPID'] = $row['lp_id'];
            $tempP['pN'] = $row['productName'];
            $tempP['pR'] = $row['dflt_pr'];
            $tempP['LPQ'] = $row['l_qty'];
            $tempP['dE'] = $row['productDiscountEligible'];
            $tempP['dP'] = $row['l_dic_pre'];
            $tempP['dV'] = $row['l_dic_val'];
            $tempP['pT'] = $row['productTypeID'];

            $tax = (isset($products[$row['productCode']]['tax'])) ? $products[$row['productCode']]['tax'] : array();
            if ($row['productTaxEligible'] == 1) {
                if ($row['taxID'] != NULL) {
                    $tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state']);
                }
            }

            $uom = (isset($products[$row['productCode']]['uom'])) ? $products[$row['productCode']]['uom'] : array();
            if ($row['uomID'] != NULL) {
                $uom[$row['uomID']] = array('uA' => $row['uomAbbr'], 'uN' => $row['uomName'], 'uDP' => $row['uomDecimalPlace'], 'uC' => $row['productUomConversion'], 'uS' => $row['uomState'], 'pUI' => $row['productUomID'], 'uomID' => $row['uomID']);
            }
            $productUom[$row['productID']][$row['uomID']] = $uom[$row['uomID']];

            $tempP['tax'] = $tax;
            $tempP['uom'] = $productUom[$row['productID']];
            $products[$row['productID']] = $tempP;
        }

        return ["products_details" => $products];
    }

    /**
     * @author Prathap Weerasinghe <prathap@thinkcube.com>
     * @return Product details as JSON
     */
    public function getAllProductsForPOSAction()
    {
        $products = $this->CommonTable('Core\Model\ProductTable')->getProcessedActiveProducts($this->location);
        return ['products_details' => $products];
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return location
     */
    public function locationAction()
    {
        $userID = $this->user_session->userID;
        $respond = $this->CommonTable('Core\Model\LocationTable')->gerActiveUserLocation($userID);
        $locations = array();
        $locationsNameCode = array();
        foreach ($respond as $key => $location) {
            $locations[$location['locationID']] = $location['locationCode'] . ' - ' . $location['locationName'];
            $locationsNameCode[$location['locationCode'] . '-' . $location['locationName']] = array('id' => $key);
        }

        return [
            'Location' => $locations,
            'LocationNameWithCode' => $locationsNameCode
        ];
    }

    public function getDisplaySetupAction()
    {
        $userID = $this->user_session->userID;
        $respond = $this->CommonTable('Core\Model\DisplaySetupTable')->fetchAllDetails()->current();
        
        return [
            'displaySetupDetails' => $respond
        ];
    }

//    /**
//     * @author Sandun <sandun@thinkcube.com>
//     * @return location
//     */
//    public function locationNameWithCodeAction()
//    {
//        $userAvailableLocations = $this->allLocations;
//        $locations = array();
//        foreach ($userAvailableLocations as $key => $location) {
//            $locations[$location['locationCode'] . '-' . $location['locationName']] = array('id' => $key);
//        }
//
//        $this->addVariable('LocationNameWithCode', $locations);
//        return $this->viewModel;
//    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @return locations available to current user
     */
    public function userLocationsAction()
    {
        $userAvailableLocations = $this->allLocations;
        $userLocations = array();
        foreach ($userAvailableLocations as $key => $location) {
            $userLocations[$key] = $location['locationCode'] . '-' . $location['locationName'];
        }
        return ['Location' => $userLocations];
    }

    public function invoiceAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $result = $this->CommonTable('Invoice\Model\InvoiceTable')->getLocationRelatedInvoices($locationID);
        $data = array();
        while ($rows = $result->current()) {
            $row = (object) $rows;
            $data[$row->salesInvoiceID] = $row->salesInvoiceCode;
        }
        return ['Invoice' => $data];
    }

    public function halfInvoiceAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $result = $this->CommonTable('Invoice\Model\InvoiceTable')->getLocationRelatedHalfInvoices($locationID);
        $data = array();
        while ($rows = $result->current()) {
            $row = (object) $rows;
            if ($row->salesinvoiceTotalAmount != $row->salesInvoicePayedAmount) {
                $data[$row->salesInvoiceID] = $row->salesInvoiceCode;
            }
        }

        return ['Invoice' => $data];
    }

    public function halfPurchaseInvoiceAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $result = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getLocationRelatedHalfPurchaseInvoices($locationID);
        $data = array();
        while ($rows = $result->current()) {
            $row = (object) $rows;
            if ($row->purchaseInvoiceTotal != $row->purchaseInvoicePayedAmount) {
                $data[$row->purchaseInvoiceID] = $row->purchaseInvoiceCode;
            }
        }

        return ['Invoice' => $data];
    }

    public function salesOrderAction()
    {
        $location = $this->user_session->userActiveLocation["locationID"];
        $result = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll($location, false);
        $salesData = array();
        while ($row = $result->current()) {
            $salesData[$row->soID] = $row->soCode;
        }

        return ['Salesorders' => $salesData];
    }

    public function openQuotationAction()
    {
        $location = $this->user_session->userActiveLocation["locationID"];
        $result = $this->CommonTable('Invoice\Model\QuotationTable')->fetchAllByLocationAndStatus($location);
        $quotations = array();
        while ($row = $result->current()) {
            $row = (object) $row;
            $quotations[$row->quotationID] = $row->quotationCode;
        }

        return ['Quotations' => $quotations];
    }

    public function quotationAction()
    {
        $location = $this->user_session->userActiveLocation["locationID"];
        $result = $this->CommonTable('Invoice\Model\QuotationTable')->fetchAllByLocation($location, false);
        $quotations = array();
        while ($row = $result->current()) {
            $row = (object) $row;
            $quotations[$row->quotationID] = $row->quotationCode;
        }

        return ['Quotations' => $quotations];
    }

    public function getPaymentCodeAction()
    {
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $result = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByLocatioID($locationID);
        $data = array();
        while ($row = $result->current()) {
            $row = (object) $row;
            $data[$row->incomingPaymentID] = $row->incomingPaymentCode;
        }

        return ['Paymentcode' => $data];
    }

    public function getCreditNotePaymentCodeAction()
    {
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $result = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentsByLocatioID($locationID);
        $data = array();
        foreach ($result as $row) {
            $row = (object) $row;
            $data[$row->creditNotePaymentID] = $row->creditNotePaymentCode;
        }

        return ['Creditnotepaymentcode' => $data];
    }

    public function getSupplierPaymentCodeAction()
    {
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $result = $this->CommonTable('SupplierPaymentsTable')->getPaymentsByLocatioID($locationID);
        $data = array();
        foreach ($result as $row) {
            $row = (object) $row;
            $data[$row->outgoingPaymentID] = $row->outgoingPaymentCode;
        }

        return ['Supplierpaymentcode' => $data];
    }

    public function locationReferenceAction()
    {
        $respond = $this->CommonTable('Core\Model\LocationTable')->activeFetchAll();
        $locations = array();
        while ($row = $respond->current()) {
            $locations[$row->locationID] = $row->locationCode . ' - ' . $row->locationName;
        }

        return ['Location' => $locations];
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * Purchase Order code list depend on user's available locations
     */
    public function POCodesAction()
    {
        $poRawSet = $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrders(FALSE, NULL, $this->getStatusID('open'), TRUE);
        $poList = array();
        $userActiveLocation = $this->userActiveLocation;
        while ($row = $poRawSet->current()) {
            if ($row['locationID'] == $userActiveLocation['locationID']) {
                $poList[$row['purchaseOrderID']] = $row['purchaseOrderCode'] . '-' . $row['locationCode'];
            }
        }

        return ['poList' => $poList];
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * Grn list depend on user's available locations
     */
    public function GrnCodeListAction()
    {
        $grnRawSet = $this->CommonTable('Inventory\Model\GrnTable')->getGrns(FALSE, NULL, $this->getStatusID('open'));
        $grnList = array();
        $userActiveLocation = $this->userActiveLocation;
        while ($row = $grnRawSet->current()) {
            if ($row['locationID'] == $userActiveLocation['locationID']) {
                $grnList[$row['grnID']] = $row['grnCode'] . '-' . $row['locationCode'];
            }
        }

        return ['grnList' => $grnList];
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return product array
     */
    public function productsByCodeAction()
    {
        $positiveProByCode = array();
        $proDetailsByCode = array();
        $proList = $this->CommonTable('Inventory\Model\ProductTable')->getProductLists(FALSE, FALSE);
        foreach ($proList as $p) {
            $proDetailsByCode[$p['productCode'] . $p['locationID']] = $p;
        }

        $positiveProList = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getProductLists();
        foreach ($positiveProList as $pP) {
            $positiveProByCode[$pP['productCode'] . $pP['locationID']] = $pP;
        }

        return [
            'ProductListsByCodeByPositiveAdjst' => $positiveProByCode,
            'ProductListsByCode' => $proDetailsByCode
        ];
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return product array
     */
    public function productsByNameAction()
    {
        $positiveProByCode = array();
        $proDetailsByName = array();

        $proList = $this->CommonTable('Inventory\Model\ProductTable')->getProductLists(FALSE, FALSE);
        foreach ($proList as $p) {
            $proDetailsByName[$p['locationID'] . $p['productName']] = $p;
        }

        $positiveProList = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getProductLists();
        foreach ($positiveProList as $pP) {
            $positiveProByCode[$pP['locationID'] . $pP['productName']] = $pP;
        }

        return [
            'ProductListsByNameByPositiveAdjst' => $positiveProByCode,
            'ProductListsByName' => $proDetailsByName
        ];
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return product array
     */
    public function productListAction()
    {
        $proObject = $this->CommonTable('Inventory\Model\ProductTable')->fetchAll();
        $proDetails = array();
        while ($row = $proObject->current()) {
            $proDetails[$row->productID] = $row;
        }

        return ['ProductListAll' => $proDetails];
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return product array
     */
    public function uomByIDAction()
    {
        $uom_object = $this->CommonTable('Settings/Model/UomTable')->activeFetchAll();
        $uom = array();
        foreach ($uom_object as $row) {
            $uom[$row->uomID] = array('uomAbbr'=>$row->uomAbbr,'uomID'=>$row->uomID,'uomName'=>$row->uomName,'uomDecimalPlace'=>$row->uomDecimalPlace);
        }

        return ['uomListByID' => $uom];
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @return product array
     */
    public function uomByAbbrAction()
    {
        $uom_object = $this->CommonTable('Settings/Model/UomTable')->activeFetchAll();
        $uom = array();
        foreach ($uom_object as $row) {
            $uom[$row->uomAbbr] = array('uomAbbr'=>$row->uomAbbr);
        }

        return ['uomListByAbbr' => $uom];
    }

    public function uomByProductIDAction()
    {
        $proUomObject = $this->CommonTable('Inventory\Model\ProductUomTable')->getAll();
        $proUomList = array();
        foreach ($proUomObject as $row) {
            $proUomList[$row['productID'] . $row['uomID']] = array('productUomConversion'=>$row['productUomConversion']);
        }

        return ['ProductUomList' => $proUomList];
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * @return js file,Active locations of User in session
     */
    public function userActiveLocationsAction()
    {
        return ['UserActiveLocations' => $this->allLocations];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get All active returns
     */
    public function getReturnsAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $returnObject = $this->CommonTable('SalesReturnsTable')->fetchAll(null, $locationID);
        $returnList = array();
        foreach ($returnObject as $row) {
            $returnList[$row['salesReturnID']] = $row['salesReturnCode'];
        }

        return ['Returnscode' => $returnList];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get All credit Notes
     */
    public function getCreditNotesAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $creditNoteObject = $this->CommonTable('Invoice\Model\CreditNoteTable')->fetchAll(null, $locationID);
        $creditNoteList = array();
        foreach ($creditNoteObject as $row) {
            $creditNoteList[$row['creditNoteID']] = $row['creditNoteCode'];
        }

        return ['CreditNotes' => $creditNoteList];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get All active and payment eligible credit Notes
     */
    public function getActiveCreditNotesAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $creditNoteObject = $this->CommonTable('Invoice\Model\CreditNoteTable')->getActiveCreditNote($locationID);
        $creditNoteList = array();
        foreach ($creditNoteObject as $row) {
            $creditNoteList[$row['creditNoteID']] = $row['creditNoteCode'];
        }

        return ['ActiveCreditNotes' => $creditNoteList];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get All purchase Voucher
     */
    public function purchaseVoucherAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $result = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getLocationRelatedPV($locationID);
        $data = array();
        while ($rows = $result->current()) {
            $row = (object) $rows;
            $data[$row->purchaseInvoiceID] = $row->purchaseInvoiceCode;
        }

        return ['PurchaseVouchers' => $data];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get All debit Notes
     */
    public function getDebitNotesAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $debitNoteObject = $this->CommonTable('Inventory\Model\DebitNoteTable')->fetchAll(null, $locationID);
        $debitNoteList = array();
        foreach ($debitNoteObject as $row) {
            $debitNoteList[$row['debitNoteID']] = $row['debitNoteCode'];
        }

        return ['DebitNotes' => $debitNoteList];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get All active and payment eligible debit Notes
     */
    public function getActiveDebitNotesAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $debitNoteObject = $this->CommonTable('Inventory\Model\DebitNoteTable')->getActiveDebitNote($locationID);
        $debitNoteList = array();
        foreach ($debitNoteObject as $row) {
            $debitNoteList[$row['debitNoteID']] = $row['debitNoteCode'];
        }

        return ['ActiveDebitNotes' => $debitNoteList];
    }

    public function getDebitNotePaymentCodeAction()
    {
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $result = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentsByLocatioID($locationID);
        $data = array();
        foreach ($result as $row) {
            $row = (object) $row;
            $data[$row->debitNotePaymentID] = $row->debitNotePaymentCode;
        }

        return ['DebitNotePaymentCode' => $data];
    }

    public function getDebitNotePaymentCodeWithDeletedAction()
    {
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $result = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentsByLocatioIDWithDeleted($locationID);
        $data = array();
        foreach ($result as $row) {
            $row = (object) $row;
            $data[$row->debitNotePaymentID] = $row->debitNotePaymentCode;
        }

        return ['DebitNotePaymentCode' => $data];
    }

    public function getCreditNotePaymentCodeWithDeletedAction()
    {
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $result = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentsByLocatioID($locationID, true);
        $data = array();
        foreach ($result as $row) {
            $row = (object) $row;
            $data[$row->creditNotePaymentID] = $row->creditNotePaymentCode;
        }

        return ['Creditnotepaymentcode' => $data];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get All Active noneInventory items
     */
    public function getAllActiveNonInventoryItemsAction()
    {
        $proObject = $this->CommonTable('Inventory\Model\ProductTable')->getActiveNonInventoryItems();
        $proDetails = array();
        while ($row = $proObject->current()) {
            $row = (object) $row;
            $proDetails[$row->productID] = $row->productCode . '-' . $row->productName;
        }

        return ['NIProductList' => $proDetails];
    }

    public function getAllJobTypesAction()
    {
        $jobType = $this->CommonTable('JobCard\Model\JobTypeTable')->fetchAll();
        $jobTypeList = array();
        foreach ($jobType as $jT) {
            $jT = (object) $jT;
            $jobTypeList[$jT->jobTypeId] = $jT->jobTypeName;
        }

        return ['job_types' => $jobTypeList];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get Active Project types
     */
    public function getProjectTypesAction()
    {
        $projectTypes = $this->CommonTable('JobCard\Model\ProjectTypeTable')->fetchAll();
        $projectDetails = array();
        foreach ($projectTypes as $row) {
            $row = (object) $row;
            $projectDetails[$row->projectTypeID] = $row->projectTypeCode . ' - ' . $row->projectTypeName;
        }
        return ['Project_type_list' => $projectDetails];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get  employees
     */
    public function getEmployeesAction()
    {
        $employees = $this->CommonTable('JobCard\Model\EmployeeTable')->fetchAll();
        $employeesDetails = array();
        foreach ($employees as $row) {
            $row = (object) $row;
            $employeesDetails[$row->employeeID] = $row->employeeFirstName . " - " . $row->employeeSecondName;
        }

        return ['Employe_list' => $employeesDetails];
    }

    /**
     * @author ashan  <ashan@thinkcube.com>
     * @return get  Divisions
     */
    public function getDivisionsAction()
    {
        $divisions = $this->CommonTable('JobCard\Model\DivisionTable')->fetchAll();
        $divisionDetails = array();
        foreach ($divisions as $row) {
            $row = (object) $row;
            $divisionDetails[$row->divisionID] = $row->divisionName;
        }

        return ['Divisions' => $divisionDetails];
    }

    public function getAllJobStationAction()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $jobStation = $this->CommonTable('JobCard\Model\JobStationTable')->fetchAll($locationID);
        $jobStationList = array();
        foreach ($jobStation as $js) {
            $jStation = (object) $js;
            $jobStationList[$jStation->jobStationID] = $jStation->jobStationName;
        }

        return ['Job_station' => $jobStationList];
    }

    /**
     * Method to introduce a new global variable into the JS file
     * Variable names will be converted to uppercase automatically
     * @param string $var Javascript variable name
     * @param mixed $data Data to be json encoded or prepared for Javascript
     * @return boolean
     */
    protected function addVariable($var, $data)
    {
// variable names cannot contain a space
        $var = preg_replace("/[^A-Za-z_]/", '', $var);
        $this->viewModel->data = array_merge($this->viewModel->data, array($var => $data));
        return true;
    }

    public function getAllProjectAction()
    {
        $project = $this->CommonTable('JobCard\Model\ProjectTable')->fetchAll();
        $projectDetails = array();
        foreach ($project as $row) {
            $row = (object) $row;
            $projectDetails[$row->projectId] = $row->projectCode;
        }

        return ['Project_list' => $projectDetails];
    }

    public function getAllProjectWithProgressAction()
    {
        $project = $this->CommonTable('JobCard\Model\ProjectTable')->fetchAll();
        $projectDetails = array();
        foreach ($project as $row) {
            $row = (object) $row;
            $progress = ($row->projectProgress != '') ? $row->projectProgress : '0.00';
            $projectDetails[$row->projectId] = $row->projectCode . ' (' . $progress . '%)';
        }

        return ['Project_list' => $projectDetails];
    }

    public function activityTypesAction()
    {
        $activityTypes = $this->CommonTable('JobCard\Model\ActivityTypeTable')->fetchAll();
        $activityTypesDetails = array();
        foreach ($activityTypes as $v) {
            $activityTypesDetails[$v['activityTypeID']] = $v['activityTypeName'];
        }

        return ['ActivityTypeList' => $activityTypesDetails];
    }

    public function divisionAction()
    {
        $division = $this->CommonTable('JobCard\Model\DivisionTable')->fetchAll();
        $divisionDetails = array();
        foreach ($division as $v) {
            $divisionDetails[$v['divisionID']] = $v['divisionName'];
        }

        return ['DivisionList' => $divisionDetails];
    }

    public function contractorsAction()
    {
        $contractors = $this->CommonTable('JobCard\Model\ContractorTable')->fetchAll();
        $contractorDetails = array();
        foreach ($contractors as $c) {
            $contractorDetails[$c['contractorID']] = $c['contractorFirstName'] . '-' . $c['contractorSecondName'];
        }

        return ['ContractorsList' => $contractorDetails];
    }

    public function temporaryContractorsAction()
    {
        $contractors = $this->CommonTable('JobCard\Model\TemporaryContractorTable')->fetchAll();
        $contractorDetails = array();
        foreach ($contractors as $c) {
            $contractorDetails[$c['temporaryContractorID']] = $c['temporaryContractorFirstName'] . '-' . $c['temporaryContractorSecondName'];
        }

        return ['TemporaryContractorsList' => $contractorDetails];
    }

    public function rawMaterialProductsAction()
    {
        $rowMaterialProductData = $this->CommonTable('Inventory\Model\ProductTable')->getRawMaterialItems();
        $rawMaterialProductDetails = array();
        foreach ($rowMaterialProductData as $r) {
            $rawMaterialProductDetails[$r['productID']] = $r['productName'] . '-' . $r['productCode'];
        }

        return ['RawMaterialProductList' => $rawMaterialProductDetails];
    }

    public function fixedAssetsProductsAction()
    {
        $fixedAssestsProductData = $this->CommonTable('Inventory\Model\ProductTable')->getFixedAssetsItems(NULL);
        $fixedAssetsProductDetails = array();
        foreach ($fixedAssestsProductData as $r) {
            $fixedAssetsProductDetails[$r['productID']] = $r['productName'] . '-' . $r['productCode'];
        }

        return ['FixedAssetsProductList' => $fixedAssetsProductDetails];
    }

    public function temporaryProductsAction()
    {
        $tempProductData = $this->CommonTable('JobCard\Model\TemporaryProductTable')->fetchAll();
        $tempProductDetails = array();
        foreach ($tempProductData as $r) {
            $tempProductDetails[$r['temporaryProductID']] = array(
                'temporaryProductCode'=>$r['temporaryProductCode'],
                'temporaryProductName'=>$r['temporaryProductName'],
                'temporaryProductQuantity'=>$r['temporaryProductQuantity'],
                'temporaryProductDescription'=>$r['temporaryProductDescription'],
                'uomID'=>$r['uomID'],
                'temporaryProductBatch'=>$r['temporaryProductBatch'],
                'temporaryProductPrice'=>$r['temporaryProductPrice'],
                'temporaryProductSerial'=>$r['temporaryProductSerial'],
                'temporaryProductImageURL'=>$r['temporaryProductImageURL'],

                );
        }

        return ['TemporaryProductList' => $tempProductDetails];
    }

//this function use to get all Job reference that related to Location ID
    public function jobReferenceAction()
    {

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $jobReferenceData = $this->CommonTable('JobCard\Model\JobTable')->getAllJobDetails($locationID);
        $jobReferenceDetails = array();
        foreach ($jobReferenceData as $j) {
            $jobReferenceDetails[$j['jobId']] = $j['jobReferenceNumber'];
        }

        return ['JobReferenceList' => $jobReferenceDetails];
    }

    public function costTypesAction()
    {
        $costTypes = $this->CommonTable('JobCard\Model\CostTypeTable')->fetchAll();
        $costTypesDetails = array();
        foreach ($costTypes as $v) {
            $costTypesDetails[$v['costTypeID']] = $v['costTypeName'];
        }

        return ['CostTypeList' => $costTypesDetails];
    }

//this function use to get all inquiry status data.
    public function getAllInquiryStatusNamesAction()
    {
        $allData = $this->CommonTable('JobCard\Model\InquiryStatusTable')->fetchAll();
        foreach ($allData as $inqStaName) {
            $inqStatusName[$inqStaName['inquiryStatusID']] = $inqStaName['inquiryStatusName'];
        }

        return ['AllInquiryStatusNames' => $inqStatusName];
    }

//this function use to get all Activity Reference Numbers.
    public function getAllActivityReferenceNumbersAction($projectId = null)
    {
        $allActNum = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByProjectID($projectId);
        foreach ($allActNum as $aCN) {
            $activityReferenceNumbers[$aCN[activityId]] = $aCN[activityCode];
        }

        return ['AllActivityReference' => $activityReferenceNumbers];
    }

//this function use to get all inquiry type data.
    public function getAllInquiryTypesAction()
    {
        $allInquiryTypes = $this->CommonTable('JobCard\Model\InquiryTypeTable')->fetchAll();
        foreach ($allInquiryTypes as $inqType) {
            $allInquiryTypeList[$inqType[inquiryTypeID]] = $inqType[inquiryTypeCode];
        }

        return ['AllInquiryTypes' => $allInquiryTypeList];
    }

    public function uomAbbrByProductIDAction()
    {
        $uomAbbrData = $this->CommonTable('Inventory\Model\ProductUomTable')->getUomAbbrByProductID();
        $uomAbbrDetails = array();
        foreach ($uomAbbrData as $u) {
            $uomAbbrDetails[$u['productID']] = $u['uomAbbr'];
        }

        return ['uomAbbrByProductList' => $uomAbbrDetails];
    }

//get gift card
    public function getGiftCardAction()
    {
        $giftCardData = $this->CommonTable('Inventory\Model\GiftCardTable')->getAllOpenGiftCards();
        $giftCardDetails = array();
        foreach ($giftCardData as $g) {
            $giftCardDetails[$g->giftCardId] = array(
                'giftCardId' => $g->giftCardId,
                'giftCardCode' => $g->giftCardCode,
                'giftCardExpireDate' => $g->giftCardExpireDate,
                'giftCardValue' => $g->giftCardValue
            );
        }

        return ['giftCardList' => $giftCardDetails];
    }
    
    public function itemImportConfigAction() 
    {
        $fields = $this->getServiceLocator()->get('config')['fields'];
        return ['fields' => $fields];
    }
    
    public function productDetailsAction()
    {
        $categoryList = $typeList = $uomList = [];
        //get categories
        $categories = $this->CommonTable('Inventory/Model/CategoryTable')->fetchAll();
        foreach ($categories as $category) {
            $categoryList[] = array(
                'categoryId'=>$category['categoryID'],
                'categoryName'=>$category['categoryName'],
                'categoryParentId'=>$category['categoryParentID']
            );
        }
        //product types
        $types = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($types as $type) {
            $typeList[] = array('typeId'=>$type->productTypeID,'typeName'=>$type->productTypeName);
        }
        //get uoms
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        foreach ($uoms as $uom) {
            $uomList[] = array('uomId'=>$uom->uomID,'uomName'=>$uom->uomName);
        }
        return [
            'categoryList' => $categoryList,
            'typeList' => $typeList,
            'uomList' => $uomList
        ];
    }

    public function wsportAction()
    {
        return [
            'wsport' => $pusherPort = $this->getServiceLocator()->get('config')['wsport']
        ];
    }

    public function customerDefaultAccountsAction()
    {
        $defaultAcc = [];

        // set customer default accounts
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->user_session->useAccounting) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $defaultAcc['customerReceviable'] =  $customerReceviableAccountID;
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $defaultAcc['customerSales'] = $customerSalesAccountID;
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $defaultAcc['customerSalesDiscount'] = $customerSalesDiscountAccountID;
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $defaultAcc['customerAdvancePayment'] = $customerAdvancePaymentAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $defaultAcc['productSales'] = $productSalesAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $defaultAcc['productInventory'] = $productInventoryAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $defaultAcc['productCOGSA'] = $productCOGSAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $defaultAcc['productAdjusment'] = $productAdjusmentAccountID;
            }
        }

        return [
            'customer_accounts' => $defaultAcc
        ];
    }

    public function jobSettingsAction()
    {
        $jobSettings = (object) $this->CommonTable('Jobs\Model\JobGeneralSettingsTable')->fetchAll();
        return ['jobSettings' => $jobSettings];
    }

}
