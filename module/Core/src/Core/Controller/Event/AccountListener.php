<?php

namespace Core\Controller\Event;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Core\Controller\CoreController;

class AccountListener extends CoreController implements ListenerAggregateInterface
{

    protected $listeners = [];

    public function attach(EventManagerInterface $events)
    {

        $accountService = $this->getServiceLocator()->get('AccountService');

        $this->listeners[] = $events->attach('bankBalanceUpdated', [
            $accountService, 'updateBankAccountBalance'
        ]);
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

}
