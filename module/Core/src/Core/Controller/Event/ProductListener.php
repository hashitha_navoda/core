<?php

namespace Core\Controller\Event;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Core\Controller\CoreController;

class ProductListener extends CoreController implements ListenerAggregateInterface
{

    protected $listeners = [];

    public function attach(EventManagerInterface $events)
    {

        $productService = $this->getServiceLocator()->get('ProductService');

        $this->listeners[] = $events->attach('productQuantityUpdated', [
            $productService, 'updateEntityRecord'
        ]);
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

}
