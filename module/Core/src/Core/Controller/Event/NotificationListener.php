<?php

namespace Core\Controller\Event;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Core\Controller\CoreController;

/**
 * Description of EzbizListener
 *
 * @author sharmilan <sharmilan@thinkcube.com>
 */
class NotificationListener extends CoreController implements ListenerAggregateInterface
{

    protected $listeners = [];

    public function attach(EventManagerInterface $events)
    {

        $notificationController = $this->getServiceLocator()->get('NotificationController');

        $this->listeners[] = $events->attach('productQuantityUpdated', [
            $notificationController, 'checkMinimumQunatityOfProduct'
        ]);
        $this->listeners[] = $events->attach('productQuantityUpdated', [
            $notificationController, 'checkReorederLevelOfProduct'
        ]);
        $this->listeners[] = $events->attach('productQuantityUpdated', [
            $notificationController, 'updatePosClientInventory'
        ]);
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

}
