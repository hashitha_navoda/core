<?php

namespace Core\Controller\Event;

use Zend\EventManager\ListenerAggregateInterface;
use Zend\EventManager\EventManagerInterface;
use Core\Controller\CoreController;

class CategoryListener extends CoreController implements ListenerAggregateInterface
{

    protected $listeners = [];

    public function attach(EventManagerInterface $events)
    {

        $categoryService = $this->getServiceLocator()->get('CategoryService');

        $this->listeners[] = $events->attach('categoryQuantityUpdated', [
            $categoryService, 'updateEntityRecord'
        ]);
    }

    public function detach(EventManagerInterface $events)
    {
        foreach ($this->listeners as $index => $listener) {
            if ($events->detach($listener)) {
                unset($this->listeners[$index]);
            }
        }
    }

}
