<?php

namespace Core\Controller;

use Zend\View\Model\ViewModel;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use Core\Model\Entity;
use Core\Model\JournalEntryDimension;
use Core\Model\DocumentAttachementMapping;
use JobCard\Model\Activity;
use JobCard\Model\Job;
use JobCard\Model\Project;
use Core\Model\Attachment;
use Zend\I18n\Translator\Translator;
use Core\Library\Util;

use Accounting\Form\JournalEntryForm;
use Accounting\Form\JournalEntryAccountsForm;
use Accounting\Model\JournalEntry;
use Accounting\Model\JournalEntryAccounts;
use Accounting\Model\JournalEntryApprovers;

use User\Controller\Plugin\SendMail;

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */
class CoreController extends AbstractActionController
{

    protected $sideMenus = '';
    protected $upperMenus = '';
    protected $downMenus = '';
    protected $entityTable;
    protected $customerTable;
    protected $referenceTable;
    protected $statusTable;
    protected $status;
    protected $msg;
    protected $data;
    protected $html;
    protected $user_session;
    protected $userID;
    protected $username;
    protected $categoryTable;
    protected $companyTable;
    protected $displaySetupTable;
    protected $coreModel;
    protected $uomTable;
    protected $invoiceTable;
    protected $purchaseInvoiceTable;
    protected $locationProductTable;
    protected $companyCurrencySymbol;
    protected $companyCurrencyId;
    protected $timeZone;
    protected $SupplierCategoryTable;
    protected $SupplierCategoryComponentTable;
    protected $productUomTable;
    protected $templateDetails;
    protected $templateItemAtrributeDetails;
    protected $company;

    CONST NUMBER_OF_DEFAULT_DECIMAL_POINTS = 2;
    CONST NUMBER_OF_DEFAULT_DECIMAL_TO_ROUND = 10;

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->allLocations = $this->user_session->userAllLocations;
        $this->companyCurrencySymbol = $this->user_session->companyCurrencySymbol;
        $this->companyCurrencyId = $this->user_session->companyCurrencyId;
        $this->timeZone = (isset($this->user_session->timeZone) && $this->user_session->timeZone) ? $this->user_session->timeZone : 'UTC';
        $this->company = $this->user_session->companyDetails;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param string $message
     * @return boolean true/false
     * Usage - Use in action which need to be saved to the audit trail
     */
    public function setLogMessage($message)
    {
        $log = $this->getServiceLocator()->get('Log');
        return $log->setMessage($message);
    }

     public function setLogDetailsArray($previousData,$newData)
    {
        $log = $this->getServiceLocator()->get('Log');
        return $log->setLogDetailsArray($previousData,$newData);
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * Set Header of All Excel files
     * @param type $name
     * @param type $size
     * @param type $size_f
     * @param type $title
     * @param type $header
     * @param type $arr
     */
    public function csvHeader($name, $size, $size_f, $title, $header, $arr)
    {
        ob_start();
        header("Content-type: application/csv");
        header("Accept-Ranges: bytes");
        header("Content-length: {$size_f}");
        header("Content-Disposition: attachment; filename={$name}");
        header("Pragma:public");
        header("Cache-Control:max-age=0");
        header("Expires: 0");

        print $title . $header . "\n" . $arr;
        ob_flush();
        exit();
    }

    public function csvContent($title, $header, $arr)
    {
        return $title . $header . "\n" . $arr;
    }

    public function getTodayNotificationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = array('invoice' => '', 'purchaseInvocie' => '');
            $locationID = $this->user_session->userActiveLocation['locationID'];
//            $date = date('Y-m-d', strtotime($request->getpost('date')));
//            $ReInvoice = $this->getRecurrentInvoiceTable()->getRecurrentInvoiceByNextDate($date);
//            echo '<pre>', print_r($ReInvoice, true);
//            exit;
//            while ($row = $ReInvoice->current()) {
//                $res[] = $row;
//            }
            $overdueInvoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getOverduenvoices($locationID);
            while ($row2 = $overdueInvoice->current()) {
//                if ($row2['state'] != 'Overdue')
//                    $this->getInvoiceTable()->updateInvoiceState($row2['id'], 'Overdue');
                $row2["Type"] = "Overdue";
                $res[] = $row2;
            }
            $data['invoice'] = $res;

            $overduePurchaseInvoice = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getOverduePurchaseInvoices($locationID);
            while ($row3 = $overduePurchaseInvoice->current()) {
//                if ($row2['state'] != 'Overdue')
//                    $this->getInvoiceTable()->updateInvoiceState($row2['id'], 'Overdue');
                $row3["Type"] = "Overdue";
                $opi[] = $row3;
            }
            $data['purchaseInvocie'] = $opi;

            $overdueItemReorderLevel = $this->CommonTable('Inventory/Model/LocationProductTable')->getOverdueReorderLevelItem($locationID);
            while ($row4 = $overdueItemReorderLevel->current()) {
//                if ($row2['state'] != 'Overdue')
//                    $this->getInvoiceTable()->updateInvoiceState($row2['id'], 'Overdue');
                $row4["Type"] = "Overdue";
                $irol[] = $row4;
            }
            $data['reorderLevel'] = $irol;

            $overdueItemInventoryLevel = $this->CommonTable('Inventory/Model/LocationProductTable')->getOverdueInventoryLevelItem($locationID);
            while ($row5 = $overdueItemInventoryLevel->current()) {
//                if ($row2['state'] != 'Overdue')
//                    $this->getInvoiceTable()->updateInvoiceState($row2['id'], 'Overdue');
                $row5["Type"] = "Overdue";
                $ilev[] = $row5;
            }
            $data['inventoryLevel'] = $ilev;

            if (!empty($data)) {
                return new JsonModel($data);
            } else {
                return new JsonModel(array(false));
            }
        }
    }

    /**
     * Get side,upper and down menus for layout
     * @param type $sideMenuSelected
     * @param type $upperMenuSelected
     * @param type $downMenuSelected
     */
    protected function getSideAndUpperMenus($sideMenuSelected = null, $upperMenuSelected = NULL, $headermenuSelected = NULL, $downmenuSelected = NULL)
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $sideMenus = ($this->sideMenus) ? $globaldata[$this->sideMenus] : '';
        $upperMenus = ($this->upperMenus) ? $globaldata[$this->upperMenus] : '';
        $downMenus = ($this->downMenus) ? $globaldata[$this->downMenus] : '';

        if (!is_null($sideMenuSelected)) {
            $this->getEvent()->getViewModel()->sidemenu = $sideMenus;
            $this->getEvent()->getViewModel()->sidemenuselected = $sideMenuSelected;
        }
        if (!is_null($upperMenuSelected)) {
            $this->getEvent()->getViewModel()->uppermenu = $upperMenus;
            $this->getEvent()->getViewModel()->uppermenuselected = $upperMenuSelected;
        }
        if (!is_null($headermenuSelected)) {
            $this->getEvent()->getViewModel()->headermenuselected = $headermenuSelected;
        }
        if (!is_null($downmenuSelected)) {
            $this->getEvent()->getViewModel()->downmenu = $downMenus;
            $this->getEvent()->getViewModel()->downmenuselected = $downmenuSelected;
        }

        if (!$_SESSION['ezBizUser']['permissionForAction']) {
            $view = $this->getEvent()->getViewModel();

            $errorPage = new ViewModel();
            $errorPage->setTemplate('error/permission');

            $viewRender = $this->getServiceLocator()->get('ViewRenderer');
            $errorHtml = $viewRender->render($errorPage);

            $this->getEvent()->getViewModel()->content = $errorHtml;

            $html = $viewRender->render($view);

            echo $html;
            exit();
        }
    }

    /**
     * Return status ID
     * @param type $statusCode
     */
    public function getStatusID($statusCode)
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $statuses = $globaldata['statuses'];
        return array_search($statusCode, $statuses);
    }

    /**
     * Return status Code
     * @param type $statusCode
     */
    public function getStatusCode($statusID)
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $statuses = $globaldata['statuses'];
        return $statuses[$statusID];
    }

    protected function getViewHelper($helperName)
    {
        return $this->getServiceLocator()->get('viewhelpermanager')->get($helperName);
    }

    /**
     * @author Damith Thamara  <damith@thinkcube.com>
     * Create entity and return the created ID
     * @return int entityID
     */
    protected function createEntity()
    {
//        $currentTime = date('Y-m-d H:i:s');
        $currentTime = $this->getGMTDateTime('Y-m-d H:i:s');
        $entitydata = array(
            'createdBy' => $this->userID,
            'createdTimeStamp' => $currentTime,
            'updatedBy' => $this->userID,
            'updatedTimeStamp' => $currentTime
        );
        $entityModel = new Entity();
        $entityModel->exchangeArray($entitydata);
        $entityID = $this->CommonTable('Core\Model\EntityTable')->saveEntity($entityModel);
        return $entityID;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * Update entity details
     * @param INT $entityID
     */
    protected function updateEntity($entityID)
    {
//        $currentTime = date('Y-m-d H:i:s');
        $currentTime = $this->getGMTDateTime('Y-m-d H:i:s');
        $entitydata = array(
            'updatedBy' => $this->userID,
            'updatedTimeStamp' => $currentTime
        );
        $this->CommonTable('Core\Model\EntityTable')->updateEntity($entitydata, $entityID);
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * Update entity details
     * @param INT $entityID
     */
    protected function updateDeleteInfoEntity($entityID)
    {
//        $currentTime = date('Y-m-d H:i:s');
        $currentTime = $this->getGMTDateTime('Y-m-d H:i:s');
        $entitydata = array(
            'deleted' => 1,
            'deletedBy' => $this->userID,
            'deletedTimeStamp' => $currentTime
        );
        $return = $this->CommonTable('Core\Model\EntityTable')->updateEntity($entitydata, $entityID);
        return $return;
    }

    protected function revertSoftDelete($entityID)
    {
        $entitydata = array(
            'deleted' => 0,
            'deletedBy' => NULL,
            'deletedTimeStamp' => NULL
        );
        $return = $this->CommonTable('Core\Model\EntityTable')->updateEntity($entitydata, $entityID);

        return $return;
    }

    protected function getEntityByID($entityID)
    {
        $return = $this->CommonTable('Core\Model\EntityTable')->getEntityByEntityID($entityID);
        return $return;
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * Generatte JSON Object from given data
     * @return \Zend\View\Model\JsonModel
     */
    protected function JSONRespondHtml()
    {
        $htmlOutput = $this->getServiceLocator()
                ->get('viewrenderer')
                ->render($this->html);

        return new JsonModel(array(
            'status' => $this->status,
            'msg' => _($this->msg),
            'data' => $this->data,
            'html' => $htmlOutput,
        ));
    }

    /**
     * @author PRASA7  <prasanna@thinkcube.com>
     * Generatte JSON Object from given data
     * @return \Zend\View\Model\JsonModel
     */
    protected function JSONRespond()
    {
        return new JsonModel(array(
            'status' => $this->status,
            'msg' => _($this->msg),
            'data' => $this->data,
        ));
    }

    /**
     * Return success message of type JSON
     *
     * @param mixed $data
     * @param  string|array $msg
     * @return JsonModel
     */
    protected function returnJsonSuccess($data = null, $msg = '')
    {
        $msg = (is_array($msg)) ? $this->getMessage($msg[0], $msg[1]) : $this->getMessage($msg);

        return new JsonModel(array(
            'status' => true,
            'msg' => $msg,
            'data' => $data,
        ));
    }

    /**
     * Return error message of type JSON
     *
     * @param  string|array $msg
     * @param mixed $data
     * @return JsonModel
     */
    protected function returnJsonError($msg, $data = null)
    {
        $msg = (is_array($msg)) ? $this->getMessage($msg[0], $msg[1]) : $this->getMessage($msg);

        return new JsonModel(array(
            'status' => false,
            'msg' => $msg,
            'data' => $data,
        ));
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param int $referenceID
     * @param int $locationID
     */
    protected function getLocationReferenceDetails($referenceID, $locationID)
    {
        $result = $this->CommonTable('Settings\Model\ReferencePrefixTable')->getReference($referenceID, $locationID);
        return $result;
    }

    /**
     * get reference number by using location reference id
     */
    protected function getReferenceNumber($referencePrefixID)
    {
        $reference = $this->CommonTable('Settings\Model\ReferencePrefixTable')->getReferenceByLocationReferenceID($referencePrefixID);
        $numberLength = strlen($reference->currentReference);
        if ($numberLength <= $reference->numberOfDigits) {
            $string = sprintf("%0" . $reference->numberOfDigits . "d", $reference->currentReference);
            $referenceNumber = $reference->referenceType . $string;
        } else {
            $referenceNumber = '';
        }

        return $referenceNumber;
    }

    /**
     * update current reference by using location reference id
     */
    protected function updateReferenceNumber($LocationReferenceID)
    {
        $reference = $this->CommonTable('Settings\Model\ReferencePrefixTable')->getReferenceByLocationReferenceID($LocationReferenceID);
        $data = array(
            'referencePrefixCurrentReference' => $reference->currentReference + 1,
        );
        $value = $this->CommonTable('Settings\Model\ReferencePrefixTable')->updateReferenceByID($data, $LocationReferenceID);

        return $value;
    }

    public function defaultValueForReference()
    {
        $config = $this->getServiceLocator()->get('config');
        $referenceName = $config['referencePrefix'];
        for ($i = 1; $i < sizeof($referenceName) + 1; $i++) {
            $checkDefultValue = $this->CommonTable('Settings\Model\ReferenceTable')->selectAllForDefaultCheck($i);
            //if selected reference type not in database then insert default reference
            if (!($checkDefultValue->current())) {
                $refID = $i;
                $refName = $referenceName[$i];
                $refType = '0';
                $refPrefix = $referenceName[$i];
                $digits = 3;
                $current = 1;
                $result = $this->CommonTable('Settings\Model\ReferenceTable')->saveDefaultReference($refName, $refType, $refID);
                $resul2 = $this->CommonTable('Settings\Model\ReferencePrefixTable')->saveDefaultReference($refID, $refPrefix, $digits, $current);
            }
        }
    }

    public function getDocumentFileName($documentType, $entityID, $templateID = 0)
    {
        return md5(strtolower(preg_replace('/([^a-zA-Z0-9])*/', '', $documentType)) . '-' . $entityID . '-' .
                $templateID);
    }

    public function getSubdomain()
    {
        $serverName = $this->getServiceLocator()->get('Config')['servername'];
        $sitePath = $_SERVER['HTTP_HOST']; // getRequest cannot be used because of context restrictions ($this)

        $companyName = '';
        if (strpos($sitePath, $serverName) > 0) {
// get unique company name from subdomain
            $companyName = end(explode('.', trim(str_replace($serverName, '', $sitePath), '.')));
        } else if ($sitePath == $serverName) {
// no subdomains are available (local enviroment)
            $companyName = 'ezbiz';
        } else {
            die('config.sitepath not set');
        }
        return $companyName;
    }

    public function getCompanyDataFolder()
    {

        $companyName = $this->getSubdomain();

        $companyFolder = ROOT_PATH . '/public/userfiles/' . md5($companyName);

// make directory if it doesnt exist
        if (!is_dir($companyFolder)) {
            mkdir($companyFolder, 0777, true);
        }

        return $companyFolder;
    }

    public function getCompanyImageFolderName()
    {
        $serverName = $this->getServiceLocator()->get('Config')['servername'];
        $sitePath = $_SERVER['HTTP_HOST']; // getRequest cannot be used because of context restrictions ($this)

        $companyName = '';
        if (strpos($sitePath, $serverName) > 0) {
// get unique company name from subdomain
            $companyName = end(explode('.', trim(str_replace($serverName, '', $sitePath), '.')));
        } else if ($sitePath == $serverName) {
// no subdomains are available (local enviroment)
            $companyName = 'ezbiz';
        } else {
            die('config.sitepath not set');
        }

        $companyFolder = md5($companyName);

        return $companyFolder;
    }

    public function getReportFolderUrl()
    {
        $companyName = $this->getSubdomain();

        return '/userfiles/' . md5($companyName) . '/documents/reports/';
    }

    /**
     * Generates a document for a given entity to be printed or emailed.
     * If template ID is not passed, the default template for the document type is used.
     * @author Malitta Nanayakkara <malitta@thinkcube.com>
     * @param int $itemID
     * @param string $documentType
     * @param int $templateID
     * @return string HTML of the generated document
     */
    public function generateDocument($itemID, $documentType, $templateID = 0)
    {

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

// If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

// if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $itemID, $templateID);
//        if ($document = $tpl->getDocumentFile($filename, $documentType)) {
//            echo $document;
//            exit;
//        }
// Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);

        $this->templateItemAtrributeDetails = $this->CommonTable('Settings\Model\TemplateItemAttributeMapTable')->getTemplateItemAttributeByTemplateID($templateID);
        
        $data = $this->getDataForDocument($itemID);

// Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);

        switch (strtolower($this->templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
            case 'pos':
                $data['data_table'] = $this->getDocumentDataTable($itemID, strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName']))));
                break;
        }


        return $tpl->renderTemplateByID($templateID, $data, $filename);
    }



    /**
     * Generates a document for a given entity to be printed or emailed.
     * If template ID is not passed, the default template for the document type is used.
     * @author Malitta Nanayakkara <malitta@thinkcube.com>
     * @param int $itemID
     * @param string $documentType
     * @param int $templateID
     * @return string HTML of the generated document
     */
    public function generateDocumentWithPaymentDetails($itemID, $documentType, $templateID = 0, $withPayment = false)
    {

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

// If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

// if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $itemID, $templateID);
//        if ($document = $tpl->getDocumentFile($filename, $documentType)) {
//            echo $document;
//            exit;
//        }
// Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);

        $this->templateItemAtrributeDetails = $this->CommonTable('Settings\Model\TemplateItemAttributeMapTable')->getTemplateItemAttributeByTemplateID($templateID);
        
        $data = $this->getDataForDocument($itemID);

// Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);


        if ($withPayment) {

            $paymentData=$this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentIDByPurchaseInvoiceID($itemID);

            $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getDeliveryNoteRelatedInvoicePaymentsDataByQuotationId(null, null, null, $itemID);
            $paymentIDs = array();

            foreach($paymentData as $key => $value) {
                $paymentIDs[]=$value['incomingPaymentID'];
            }

            switch (strtolower($this->templateDetails['documentSizeName'])) {
                case 'a4 (portrait)':
                case 'a4 (landscape)':
                case 'a5 (portrait)':
                case 'a5 (landscape)':
                    $data['data_table'] = $this->getDocumentDataTable($itemID, strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName']))));
                    $data['payment_table'] = $this->_getDocumentPaymentTable($paymentIDs, strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])).'-payment'));
                    break;
                case 'pos':
                    $data['data_table'] = $this->getDocumentDataTable($itemID, strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName']))));
                    break;
            }

        } else {
            switch (strtolower($this->templateDetails['documentSizeName'])) {
                case 'a4 (portrait)':
                case 'a4 (landscape)':
                case 'a5 (portrait)':
                case 'a5 (landscape)':
                case 'pos':
                    $data['data_table'] = $this->getDocumentDataTable($itemID, strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName']))));
                    break;
            }

        }

        
        return $tpl->renderTemplateByID($templateID, $data, $filename);
    }

    /**
     * @author Malitta Nanayakkara <malitta@thinkcube.com>
     * Get general data needed for document preview and generating document
     * Includes company data, date/time, currency symbol etc.
     * @return array
     */
    public function getDataForDocumentView()
    {
// get company details
        $data = (array) $this->CommonTable('CompanyTable')->fetchAll()->current();
        $data['companyPostalCode'] = $data['postalCode'];
        $data['companyTelephoneNumber'] = $data['telephoneNumber'];
        $data['companyTelephoneNumber2'] = $data['telephoneNumber2'];
        $data['companyTelephoneNumber3'] = $data['telephoneNumber3'];
        $data['companyFaxNumber'] = $data['faxNumber'];
        $data['companyEmail'] = $data['email'];
        $data['companyCountry'] = $data['country'];
        $data['companyWebsite'] = $data['website'];
        $data['companyBrNumber'] = $data['brNumber'];
        $data['companyTaxRegNumber'] = $data['trNumber'];
        $data['companyLogoID'] = $data['logoID'];

// get company logo
        $companyFolder = $this->getCompanyImageFolderName();
        $baseURL = 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . $_SERVER['SERVER_NAME'];

        if ($data['logoID'] != null) {
            $data['companyLogo'] = $baseURL . '/userfiles/' . $companyFolder . '/company/thumbs/' . $data['logoID'];
//            $data['companyLogo'] .= ($data['logoID']) ? $data['logoID'] : 'default.jpeg';
        } else {
            $data['companyLogo'] = $baseURL . '/assets/img/defaultPreview.png';
        }

// todo - get location data
        $gmttime = $this->getGMTDateTime('H:i:s');
        $data['today_date'] = gmdate('Y-m-d');
        $data['current_time'] = $this->getUserDateTime($gmttime, 'h:i:s A'); //gmdate('H:i:s A');
        $data['current_user'] = $this->username;
        $data['current_location'] = $this->user_session->userActiveLocation['locationName'];

        $displaySetup = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();
        $data['currencySymbol'] = $displaySetup['currencySymbol'];
        $data['companyCurrencySymbol'] = $displaySetup['currencySymbol'];

        return $data;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>
     * return company details data
     * @return array $company
     */
    public function getCompanyDetails()
    {
        $companyDetails = $this->CommonTable('CompanyTable')->fetchAll();

        foreach ($companyDetails as $c) {
            $company[] = $c;
        }
        return $company;
    }

    public function getDefaultCurrency()
    {
        $countryName = $this->CommonTable('CompanyTable')->fetchAll();
        $config = $this->getServiceLocator()->get('config');
        $country_currency_name = ($config['currency'][$countryName->current()->country]);
        if ($country_currency_name) {
            $CurrencyName = $country_currency_name;
        } else {
            $CurrencyName = 'USD';
        }
        $currencyValue = $this->CommonTable('Core\Model\CurrencyTable')->getCurrencyByName($CurrencyName);
        return $currencyValue->currencyID;
    }

    public function getLanguageForSelectedCountry()
    {
        $Country = $this->CommonTable('CompanyTable')->fetchAll();
        $country_name = preg_replace('(\s)', '_', strtolower($Country->current()->country));
        $config = $this->getServiceLocator()->get('config');
        $languages = isset($config['countries'][$country_name]) ? $config['countries'][$country_name] : $config['countries']['international'];
        return $languages;
    }

    /**
     * @author sandun  <sandun@thinkcube.com>     *
     * @return $customer
     */
    public function getCustomerData()
    {
        $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        foreach ($customerData as $c) {
            $customer[] = $c;
        }
        return $customer;
    }

    protected function getStatusIDByStatusName($statusName)
    {
        $reference = $this->CommonTable('Core\Model\StatusTable')->getStatusByStatusName($statusName);
        $statusID = $reference->statusID;
        return $statusID;
    }

    public function getReferenceNoForLocation($referenceID, $locationID)
    {
        $refNumber = NULL;
        $locationRefID = NULL;
        $referenceType = $this->CommonTable('Settings\Model\ReferenceTable')->getReferenceType($referenceID);

        if ($referenceType == 0) {
            $referenceData = $this->getLocationReferenceDetails($referenceID, NULL);
        } else if ($referenceType == 1) {
            $referenceData = $this->getLocationReferenceDetails($referenceID, $locationID);
        } else {
            return FALSE;
        }
        if ($referenceData) {
            $locationRefID = $referenceData->locationReferenceID;
            $refNumber = $this->getReferenceNumber($referenceData->locationReferenceID);
        } else {
            $refNumber = NULL;
            $locationRefID = NULL;
        }
        return array(
            'refNo' => $refNumber,
            'locRefID' => $locationRefID,
        );
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * For current date time according to user specified time zone
     * @param type $fromTime
     * @param type $format
     * @return type
     */
    public function getUserDateTime($fromTime = false, $format = 'Y-m-d H:i:s')
    {
        try {

            $fromTime = (!$fromTime) ? gmdate($format) : $fromTime;
            $from = new \DateTimeZone('GMT');
            $to = new \DateTimeZone($this->timeZone);
            $orgTime = new \DateTime($fromTime, $from);
            $toTime = new \DateTime($orgTime->format("c"));
            $toTime->setTimezone($to);

            return $toTime->format($format);
        } catch (Exception $e) {
            return '-';
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * For current GMT date time
     * @param type $format : default for DB access
     * @return datatime
     */
    public function getGMTDateTime($format = 'Y-m-d H:i:s', $timestamp = false)
    {
        if ($timestamp) {
            $userDateFormat = $this->convertUserDateFormatToPhpFormat();
            $date = \DateTime::createFromFormat($userDateFormat . " H:i", $timestamp);
            return gmdate('Y-m-d H:i:s', strtotime($date->format('Y-m-d H:i:s')));
        } else {
            return gmdate($format);
        }
    }

    /**
     * this function return the appropiate Uom and abberviation
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $productID
     * @param type $amount
     * @return String amount and uom symbol
     */
    public function getAppropriateUomForProduct($productID, $amount)
    {
        $result = $this->CommonTable('Inventory\Model\ProductUomTable')->getOrderedProductUomByProductID($productID);

        $noOfUom = count($result);
        if ($noOfUom == 0) {
            return $amount;
        } else if ($noOfUom == 1) {
            return $amount . " " . $result->current()['uomAbbr'];
        } else if ($noOfUom > 1) {
            $val = 0;
            $uomAbbr = null;
            while ($row = $result->current()) {
                if ($amount >= $row['productUomConversion']) {
                    $val = $amount / $row['productUomConversion'];
                    $uomAbbr = $row['uomAbbr'];
                    break;
                }
            }
            return $val . " " . $uomAbbr;
        }
    }

    /**
     * return http response
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function httpRespond($code, $msg = NULL)
    {
        if ($msg != NULL) {
            header("Content-Description: " . $msg, TRUE, $code);
        } else {
            header(" ", TRUE, $code);
        }
        exit;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * This will check user have permission to access the paricular page
     * @param String $controller
     * @param String $action
     * @param string $userID
     * @return \Zend\View\Model\ViewModel
     */
    public function checkFeatureEnableByUserID($controller, $action, $userID)
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID($controller, $action, $userID);
        if (!$isEnable) {
            $view = new ViewModel();
            $view->setTemplate('error/dashboard');
            return $view;
        }
    }

    public function headerViewTemplate($name, $period)
    {
        $translator = new Translator();

        $headerView = new ViewModel(array(
            'period' => $period,
            'reportName' => $translator->translate($name),
            'cD' => $this->getcompanyDetails(),
            'dateTime' => $this->getUserDateTime(FALSE, 'Y-M-d h:i:s A'),
                )
        );
        return $headerView;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * this will render a view model object.
     * @param type $view
     * @return renderdView this can be use in parent viewmodel
     */
    public function htmlRender($view)
    {
        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    protected function _dependingQueryError($msg)
    {
        $this->rollback();
        $this->status = false;
        $this->msg = $msg;
        return $this->JSONRespond();
    }

    protected function beginTransaction()
    {
        if (!$this->entityTable) {
            $sm = $this->getServiceLocator();
            $this->entityTable = $sm->get('Core\Model\EntityTable');
        }
        $this->entityTable->beginTransaction();
        return true;
    }

    protected function rollback()
    {
        if (!$this->entityTable) {
            $sm = $this->getServiceLocator();
            $this->entityTable = $sm->get('Core\Model\EntityTable');
        }
        $this->entityTable->rollback();
        return true;
    }

    protected function commit()
    {
        if (!$this->entityTable) {
            $sm = $this->getServiceLocator();
            $this->entityTable = $sm->get('Core\Model\EntityTable');
        }
        $this->entityTable->commit();
        return true;
    }

    public function sendEmail($to, $subject, $body, $files = [], $fromName = "ezBiz", $from = null)
    {
        if ($from == null) {
            $from = 'accounts@ezbizapp.com';

            $companyEmail = $this->CommonTable('CompanyTable')->fetchAll()->current()->email;
            if ($companyEmail) {
                $from = $companyEmail;
            }
        }

        $bcc = null;
        $cc = null;

        // if $body is an array it would contain both HTML and plain text email bodies
        $plainTextBody = (is_array($body)) ? $body[1] : null;
        $htmlBody = (is_array($body)) ? $body[0] : $body;

        $mailConfig = $this->_getMailConfig();

        $sendMail = new SendMail($mailConfig);

        return $sendMail->sendMailUsingGmail($to, $from, $fromName, $subject, $bcc, $cc, $plainTextBody, $htmlBody, $files);
    }


    protected function _getMailConfig()
    {
        $sm = $this->getServiceLocator();
        $conf = $sm->get('User\Model\EmailconfigTable')->getConfiguration();

        if ($conf) {
            $mailConfig = [
                'host' => $conf->host,
                'username' => $conf->email,
                'password' => $conf->password,
            ];
            if ($conf->port) {
                $mailConfig['port'] = $conf->port;
            }
        } else {

            $mailConfig = $this->getServiceLocator()->get('Config')['mail'];
        }

        return $mailConfig;
    }


    public function convertToPDF($sourceFile, $destinationFile, $landscapeOrientation)
    {
        if ($landscapeOrientation) {
            exec('wkhtmltopdf -O landscape ' . $sourceFile . ' ' . $destinationFile);
        } else {
            exec('wkhtmltopdf ' . $sourceFile . ' ' . $destinationFile);
        }

        return (file_exists($destinationFile));
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $htmlFile
     * @return $htmlContent
     */
    public function viewRendererHtmlContent($htmlFile)
    {
        $viewRender = $this->getServiceLocator()->get('ViewRenderer');
        $htmlContent = $viewRender->render($htmlFile);

        return $htmlContent;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param string $reportName
     * @param html content $htmlContent
     * @return true or $browserPath
     */
    public function downloadPDF($reportName, $htmlContent, $returnPath = false, $landscapeOrientation = false)
    {
        $uniqueName = $reportName . preg_replace("/[^0-9]/", "", microtime(true)) . uniqid();

        $docPath = $this->getCompanyDataFolder() . '/documents/reports/pdf';

        if (!is_dir($docPath)) {
            mkdir($docPath, 0777, true);
        }

//string put in to html file and save it in local hard
        file_put_contents($docPath . '/' . $uniqueName . '.html', $htmlContent);

// create unique file name and convert html to pdf using wkhtmltopdf
        $this->convertToPDF($docPath . '/' . $uniqueName . '.html', $docPath . '/' . $uniqueName . '.pdf', $landscapeOrientation);

//get path where file has been saved in local machine
        $browserPath = str_replace(getcwd() . '/public/', '', $docPath) . '/' . $uniqueName . '.pdf';

        if ($returnPath) {
            return $browserPath;
        } else {
            header('Location: /' . $browserPath);

            return true;
        }
    }

    /**
     * @author sandun <sandun@thinkcube.com>
     * @param string $reportName
     * @param string $csvFile
     * @return string
     */
    public function generateCSVFile($reportName, $csvFile)
    {
        $uniqueName = $reportName . preg_replace("/[^0-9]/", "", microtime(true)) . uniqid();

        $docPath = $this->getCompanyDataFolder() . '/documents/reports/csv';

        if (!is_dir($docPath)) {
            mkdir($docPath, 0777, true);
        }

        //string put in to csv file and save it in local hard in specific location
        file_put_contents($docPath . '/' . $uniqueName . '.csv', $csvFile);

        //get path where file has been saved in local machine
        $browserPath = str_replace(getcwd() . '/public/', '', $docPath) . '/' . $uniqueName . '.csv';


        return $browserPath;
    }

    public function sendDocumentEmail($to, $subject, $body, $documentType, $documentID, $templateID = null, $attachCsv = false, $isDraft = false)
    {
        $companyDetails = (array) $this->CommonTable('CompanyTable')->fetchAll()->current();

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

        if ($templateID == null) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        if ($documentType == "Goods Received Note") {
            if ($isDraft) {
                $file = $this->getDocumentFileName("Draft Goods Received Note", $documentID, $templateID);
            } else {
                $file = $this->getDocumentFileName($documentType, $documentID, $templateID);
            } 
        } elseif ($documentType == "Sales Invoice") {
            if ($isDraft) {
                $file = $this->getDocumentFileName("Draft Sales Invoice", $documentID, $templateID);
            } else {
                $file = $this->getDocumentFileName($documentType, $documentID, $templateID);
            } 
        } elseif ($documentType == "Sales Invoice Return") {
            if ($isDraft) {
                $file = $this->getDocumentFileName("Draft Invoice Return", $documentID, $templateID);
            } else {
                $file = $this->getDocumentFileName($documentType, $documentID, $templateID);
            } 
        } else {
            $file = $this->getDocumentFileName($documentType, $documentID, $templateID);
        }

        
        $arr = str_split($file, 2);
        $path = $tpl->getDocumentPath($documentType, $documentID, $templateID) . "/" . $arr[0] . "/" . $arr[1];
        $filename = substr($file, 4);

        $html_file = $path . '/' . $filename . '.html';
        $pdf_file = $path . '/' . $filename . '.pdf';
        $csv_file = $path . '/' . $filename . '.csv';

        exec('wkhtmltopdf.sh ' . $html_file . ' ' . $pdf_file);
//create files array
        $files = [
            ['file' => $pdf_file, 'fileName' => $filename . '.pdf']
        ];
        if ($attachCsv) {
            array_push($files, ['file' => $csv_file, 'fileName' => $filename . '.csv']);
        }

        $status = $this->sendEmail($to, $subject, $body, $files, $companyDetails['companyName']);

// remove PDF/CSV file after emailing
        unlink($pdf_file);
        file_exists($csv_file) ? unlink($csv_file) : '';

        if ($status) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * list of array which all active locations
     * @author Sandun  <sandun@thinkcube.com>
     * @return $userLocations
     */
    public function userLocations()
    {
        $userAvailableLocations = $this->allLocations;
        $userLocations = array();
        foreach ($userAvailableLocations as $key => $location) {
            $userLocations[$key] = $location ['locationCode'] . '-' . $location['locationName'];
        }
        return $userLocations;
    }

    /**
     * active customer list
     * @author Sandun  <sandun@thinkcube.com>
     * @return array $cusList
     */
    public function getActiveCustomerList($withoutDefaultCustomer = FALSE)
    {
        $cusData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerNameList();
        foreach ($cusData as $k => $v) {
            $cusList[$v['customerID']] = $v['cust_name'];
        }
        if ($withoutDefaultCustomer) {
            unset($cusList[0]);
        }
        return $cusList;
    }

    public function _generateRandomString($length = 6)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    /**
     * Pass messages to classes
     *
     */
    public function getMessage($key, array $params = [])
    {

        $messages = $this->getServiceLocator()->get('config')['messages'];

        // if message is not defined in config, return as it is
        if (!isset($messages[$key])) {
            return $key;
        }

        return vsprintf($messages[$key], $params);
    }

    public function CommonTable($table)
    {
        $sm = $this->getServiceLocator();
        $tableObject = $sm->get($table);
        return $tableObject;
    }

    /*
     * get template data to preview
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * @return common view model
     */

    public function getCommonPreview($data, $path, $createNew, $documentType, $code, $createPath, $enableCsvAttachment = false, $isInitialView = false)
    {
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $defaultTpl = $tpl->getDefaultTemplate($documentType);
        $data['documentSizeName'] = $defaultTpl['documentSizeName'];
        $toggleEnabledDocuments = $this->getServiceLocator()->get('config')['toggle_document_types'];

// get template list for dropdown
        $templates = $tpl->getTemplatesForDocument($documentType);
        $data['documentTemplates'] = $templates;
        $subPathArr = explode('/', $path);

        $view = new ViewModel(array(
            'data' => $data,
            'path' => $path,
            'documentPath' => $subPathArr[1],
            'createNew' => $createNew,
            'code' => $code,
            'createPath' => $createPath,
            'enableCsvAttachment' => $enableCsvAttachment,
            'isInitialView' => $isInitialView,
            'enableToggle' => in_array($documentType, array_keys($toggleEnabledDocuments)),
            'documentNames' => isset($toggleEnabledDocuments[$documentType]) ? $toggleEnabledDocuments[$documentType] : [],
        ));

        $view->setTemplate('common-document-preview');
        $view->setTerminal(TRUE);

        return $view;
    }

    /**
     * send email function for document view
     *  @author Yashora Jayamanne <yashora@thinkcube.com>
     *
     */
    public function mailDocumentAsTemplate($request, $documentType, $attachCsv = false)
    {
        $documentID = $request->getPost('documentID');
        $to = $request->getPost('to_email');
        $to = array_unique(explode(',', str_replace(' ', '', $to)));
        $subject = $request->getPost('subject');
        $body = $request->getPost('body');
        $templateID = $request->getPost('templateID');
        return $this->sendDocumentEmail($to, $subject, $body, $documentType, $documentID, $templateID, $attachCsv);
    }

    /**
     * @author Peushani Jayasekara <peushani@thinkcube.com>
     * upload image and save both original and preview image
     *
     * @param String $folder
     * @param String $dataID
     * @param boolean $keepOriginal (keep original image)
     *
     */
    public function previewImage($folder, $dataID, $keepOriginal = true, $desired_width = 170, $desired_height = 170)
    {
        $request = $this->getRequest(); //create http request

        if ($request->isPost()) {
            $path = $this->getCompanyDataFolder(); //create md5($companyname) folder
            $newPath = $path . $folder;
            $copyPath = $newPath . "/thumbs"; //create path to save preview images

            if (!is_dir($newPath)) {
                mkdir($newPath, 0777, true);
            }
            define('PATH', $newPath);

            /* Configuration part */
            $rd = extract($_POST);

            $img_quality = 100;

            if ($_FILES) {
                $file = $_FILES['images'];

                if (!$file ['error'] && $file['size'] < (1 * 1024 * 1024)) {
                    if (is_uploaded_file($file['tmp_name'])) {

                        $ext = substr($file['name'], strrpos($file['name'], '.') + 1); //Grab file extension from uploaded file
                        $img_name = $dataID . "." . $ext; //set original image name
                        $temp_file = PATH . "/" . $img_name; //set original image path


                        if ($ext == 'jpeg' || $ext == 'jpg' || $ext == 'png') {
                            move_uploaded_file($file['tmp_name'], $temp_file); // upload the file in appropriate folder
                        } else {
                            move_uploaded_file($file['tmp_name'], $temp_file);
                            @unlink($temp_file);
                        }
//check if the file was uploaded properly without any error
                        if (file_exists($temp_file) && filesize($temp_file) > 0) {

                            $newfile = $dataID . "_preview." . $ext; //set preview image name
                            $prev_file = $copyPath . "/" . $newfile; //set preview image path

                            if (!is_dir($copyPath)) {
                                mkdir($copyPath, 0777, true);
                            }
                            copy($temp_file, $prev_file); //create copy of $temp_file and move to temp folder
                            $cropimg = $this->createCropImage($temp_file, $prev_file, $ext, $_POST['w'], $_POST['h'], $_POST['x1'], $_POST['y1']);

                            if ($keepOriginal === false) {
                                @unlink($temp_file);
                            }
                        }
                        return $newfile;
                    }
                }
            }
        }
    }

    /**
     * @author Peushani Jayasekara <peushani@thinkcube.com>
     * @param String $temp_file -original image path
     * @param String $prev_file -preview image path
     * @param String $ext -extension of original image
     * @param type $w
     * @param type $h
     * @param type $x1
     * @param type $y1
     * @return
     *
     */
    public function createCropImage($temp_file, $prev_file, $ext, $w, $h, $x1, $y1)
    {
        $size = getimagesize($temp_file);
        $file_size_arr = getimagesize($temp_file); // get the image detail

        if (!$file_size_arr) {
            @unlink($prev_file); //if file size array not exits then delete it
            return;
        }

        $desired_width = 170;
        $desired_height = 170;
        $jpeg_quality = 100;

//change the width size in the preview image
        $desired_width = $w;
        $desired_height = $h;

// create a new true color image
        $true_color_img = @imagecreatetruecolor($desired_width, $desired_height);

        switch ($ext) {
            case 'jpg':
                $img = @imagecreatefromjpeg($temp_file);
// copy and resize part of an image with resampling
                imagecopyresampled($true_color_img, $img, 0, 0, (int) $x1, (int) $y1, $desired_width, $desired_height, (int) $w, (int) $h);
// upload resultant file to the folder
                $cropImg = imagejpeg($true_color_img, $prev_file, $jpeg_quality);
                break;

            case 'jpeg':
                $img = @imagecreatefromjpeg($temp_file);
                imagecopyresampled($true_color_img, $img, 0, 0, (int) $x1, (int) $y1, $desired_width, $desired_height, (int) $w, (int) $h);
                $cropImg = imagejpeg($true_color_img, $prev_file, $jpeg_quality);
                break;

            case 'png':
                imagealphablending($true_color_img, false);
                imagesavealpha($true_color_img, true);
                imagecolortransparent($true_color_img, imagecolorallocate($true_color_img, 0, 0, 0));
                $img = @imagecreatefrompng($temp_file);
                imagecopyresampled($true_color_img, $img, 0, 0, (int) $x1, (int) $y1, $desired_width, $desired_height, (int) $w, (int) $h);
                $cropImg = imagepng($true_color_img, $prev_file, 8);
                break;

            case 'gif':
                $img = @imagecreatefromgif($temp_file);
                imagecopyresampled($true_color_img, $img, 0, 0, (int) $x1, (int) $y1, $desired_width, $desired_height, (int) $w, (int) $h);
                $cropImg = imagegif($true_color_img, $prev_file, $jpeg_quality);
                break;

            default:
                $cropImg = null;
                return;
        }

        return $cropImg;
    }

    /**
     * @author Peushani Jayasekara <peushani@thinkcube.com>
     *
     * save preview image in temp folder
     *
     * @return \Zend\View\Model\JsonModel
     */
    public function savePreviewImageAction()
    {
        $request = $this->getRequest(); //create http request
        $post = $request->getPost();
        $name = $_FILES['images']['tmp_name'];

        if (is_uploaded_file($name)) {
            $ext = substr($_FILES['images']['name'], strrpos($_FILES['images']['name'], '.') + 1);

            if ($request->isPost()) {
                $path = $this->getCompanyDataFolder() . "/temp";
//create temp folder
                if (!is_dir($path)) {
                    mkdir($path, 0777, true);
                }
//set unique name for image
                $imageName = md5(microtime() . rand(500000)) . "." . $ext;
                $imagePath = $path . "/" . $imageName;
//get image path
                $filename = substr($imagePath, strpos($imagePath, '/userfiles') + 0); //get position of ':'

                move_uploaded_file($name, $imagePath); // upload the file in appropriate folder
                $cropimg = $this->createCropImage($imagePath, $imagePath, $ext, $_POST['w'], $_POST['h'], $_POST['x1'], $_POST['y1']);


                $this->status = true;
            }
        }
        return new JsonModel(array($filename));
    }

    /**
     * Get current Status List on config
     * @author Sandun  <sandun@thinkcube.com>
     * @return $statuses
     */
    public function getStatusesList()
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $statuses = $globaldata['statuses'];
        return $statuses;
    }

    /**
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * @param type $dateformat
     * @param type $date
     * @return date
     */
    function convertDateToStandardFormat($date)
    {
        if ($date) {
            $dateformat = $this->getUserDateFormat();
            if ($dateformat === 'yyyy/mm/dd' || $dateformat === 'yyyy-mm-dd' || $dateformat === 'yyyy.mm.dd') {
                $symbols = array('/', '.');
                $date = str_replace($symbols, '-', $date);
                return date("Y-m-d", strtotime($date));
            } else if ($dateformat == 'dd/mm/yyyy' || $dateformat == 'dd-mm-yyyy' || $dateformat == 'dd.mm.yyyy') {
                $symbols = array('/', '.');
                $date = str_replace($symbols, '-', $date);
                return date("Y-m-d", strtotime($date));
            } else {
                $symbols = array('-', '.');
                $date = str_replace($symbols, '/', $date);
//            $date = preg_replace("/([0-9]{4})([0-9]{4})/", "$2$1", $date);
                return date('Y-m-d', strtotime($date));
            }
        }
///......
    }

    function convertDateToGivenFormat($date, $format)
    {
        if (!empty($date)) {
            $newdate = str_replace('-', '/', $date);
            $dd=date_create($newdate);
            return date_format($dd,$format);
        }        
    }

    /**
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * @param type $dateformat
     * @param type $date
     * @return date
     */
    public function convertDateToUserFormat($date)
    {
        if ($date) {
            switch ($this->getUserDateFormat()) {
                case 'yyyy/mm/dd':
                    return date('Y/m/d', strtotime($date));
                    break;
                case 'yyyy-mm-dd':
                    return date('Y-m-d', strtotime($date));
                    break;
                case 'yyyy.mm.dd':
                    return date('Y.m.d', strtotime($date));
                    break;
                case 'dd/mm/yyyy':
                    return date('d/m/Y', strtotime($date));
                    break;
                case 'dd-mm-yyyy':
                    return date('d-m-Y', strtotime($date));
                    break;
                case 'dd.mm.yyyy':
                    return date('d.m.Y', strtotime($date));
                    break;
                case 'mm/dd/yyyy':
// $date = preg_replace("/([0-9]{4})([0-9]{4})([0-9]{4})/", "$2$3$1", $date);
                    return date('m/d/Y', strtotime($date));
                case 'mm-dd-yyyy':
                    $date = date('m/d/Y', strtotime($date));
                    return str_replace('/', '-', $date);
                    ;
                case 'mm.dd.yyyy':
                    return date('m.d.Y', strtotime($date));
            }
        }
    }

    /**
     * @author ashan madushka <ashan@thinkcube.com>
     * convert dateformat to php format
     * @return dateformat
     */
    public function convertUserDateFormatToPhpFormat()
    {
        $display = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll();
        $displaysetup = $display->current();

        switch ($displaysetup->dateFormat) {
            case 'yyyy/mm/dd':
                return'Y/m/d';
                break;
            case 'yyyy-mm-dd':
                return 'Y-m-d';
                break;
            case 'yyyy.mm.dd':
                return 'Y.m.d';
                break;
            case 'dd/mm/yyyy':
                return 'd/m/Y';
                break;
            case 'dd-mm-yyyy':
                return 'd-m-Y';
                break;
            case 'dd.mm.yyyy':
                return 'd.m.Y';
                break;
            case 'mm/dd/yyyy':
                return 'm/d/Y';
            case 'mm-dd-yyyy':
                return 'm-d-Y';
                break;
            case 'mm.dd.yyyy':
                return 'm.d.Y';
                break;
        }
    }

    /**
     * @author Yashora Jayamanne <yashora@thinkcube.com>
     * @param type $dateformat
     * @return dateformat
     */
    public function getUserDateFormat()
    {
        $display = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll();
        $displaysetup = $display->current();

        return $displaysetup->dateFormat;
    }

    public function getUserTimeFormat()
    {
        $display = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll();
        $displaysetup = $display->current();

        return $displaysetup->timeFormat;
    }

    public function getLanguage()
    {
        $config = $this->getServiceLocator()->get('config')['countries'];
    }

    public function getActiveAllLocationsIds()
    {
        $allLocations = $this->allLocations;
        $locIDs = array();
        foreach ($allLocations as $key => $value) {
            $locIDs[] = $key;
        }
        return $locIDs;
    }

    public function getActiveAllLocationsNames()
    {
        $allLocations = $this->allLocations;
        $locNames = array();
        foreach ($allLocations as $key => $value) {
            $locNames[$key] = $value['locationName'] . '-' . $value['locationCode'];
        }
        return $locNames;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $customerID
     * @return object
     */
    public function getCustomerDetailsByCustomerID($customerID)
    {
        $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerAndPrimaryProfileByID($customerID);
        $customerProfiles = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerID($customerID);
        $custProfiles = [];
        foreach ($customerProfiles as $value) {
            $custProfiles[$value['customerProfileID']] = $value['customerProfileName'];
        }
        $addressArray = array(
            trim($customer->customerProfileLocationNo, ','),
            trim($customer->customerProfileLocationRoadName1, ','),
            trim($customer->customerProfileLocationRoadName2, ','),
            trim($customer->customerProfileLocationRoadName3, ','),
            trim($customer->customerProfileLocationSubTown, ','),
            trim($customer->customerProfileLocationTown, ','),
            trim($customer->customerProfileLocationPostalCode, ','),
            trim($customer->customerProfileLocationCountry, ','),
        );

        $address = implode(', ', array_filter($addressArray));
        $customer->customerAddress = ($address != '') ? $address . '.' : $address;
        $customer->customerEmail = $customer->customerProfileEmail;
        $customer->customerProfiles = $custProfiles;

        return $customer;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $productID
     * @return type
     */
    public function getLocationProductDetails($locationID, $productID)
    {

        $locationProduct = $this->CommonTable('Inventory\Model\ProductTable')->getLocationProductDetailsByLPID($locationID, $productID);
        $products = array();
        foreach ($locationProduct as $row) {
            $tempP = array();
            $tempP['pC'] = $row['productCode'];
            $tempP['pID'] = $row['productID'];
            $tempP['lPID'] = $row['locationProductID'];
            $tempP['pN'] = $row['productName'];
            $tempP['LPQ'] = $row['locationProductQuantity'];
            $tempP['bP'] = $row['batchProduct'];
            $tempP['sP'] = $row['serialProduct'];
            $tempP['dSP'] = $row['defaultSellingPrice'];
            $tempP['dPP'] = $row['defaultPurchasePrice'];
            $tempP['dEL'] = $row['productDiscountEligible'];
            $tempP['dPR'] = $row['locationDiscountPercentage'];
            $tempP['dV'] = $row['locationDiscountValue'];
            $tempP['pT'] = $row['productTypeID'];
            $tempP['dPEL'] = $row['productPurchaseDiscountEligible'];
            //product default purchase discount percentage
            $tempP['pPDP'] = $row['productPurchaseDiscountPercentage'];
            //product default purchase discount value
            $tempP['pPDV'] = $row['productPurchaseDiscountValue'];
            $tempP['sales'] = false;
            $tempP['purchase'] = false;
            $tempP['giftCard'] = $row['productHandelingGiftCard'];
            //product tax eligible flag
            $tempP['proTaxFlag'] = $row['productTaxEligible'];
            $tempP['mrpType'] = $row['mrpType'];
            $tempP['mrpValue'] = $row['mrpValue'];
            $tempP['mrpPercentageValue'] = $row['mrpPercentageValue'];
            $tempP['useDisScheme'] = $row['isUseDiscountScheme'];


            $checkIsBomItem = $this->CommonTable('Inventory\Model\BomTable')->getBomDataByProductID($productID)->current();

            if ($checkIsBomItem) {
                $tempP['isBom'] = true;
                $tempP['bomData'] = $checkIsBomItem;
            } else {
                $tempP['isBom'] = false;
                $tempP['bomData'] = [];
            }


            if ($row['productHandelingManufactureProduct'] == 1 || $row['productHandelingSalesProduct'] == 1) {
                $tempP['sales'] = true;
            }

            if ($row['productHandelingManufactureProduct'] == 1 || $row['productHandelingPurchaseProduct'] == 1 || $row['productHandelingConsumables'] == 1 || $row['productHandelingFixedAssets'] == 1) {
                $tempP['purchase'] = true;
            }

            $tax = (isset($products[$row['productID']]['tax'])) ? $products[$row['productID']]['tax'] : array();
            if ($row['productTaxEligible'] == 1) {
                if ($row['taxID'] != NULL) {
                    $tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state']);
                }
            }
            $uom = (isset($products[$row['productID']]['uom'])) ? $products[$row['productID']]['uom'] : array();
            if ($row['uomID'] != NULL) {
                $uom[$row['uomID']] = array('uA' => $row['uomAbbr'], 'uN' => $row['uomName'], 'uDP' => $row['uomDecimalPlace'], 'uS' => $row['uomState'], 'uC' => $row['productUomConversion'], 'pUI' => $row['productUomID'], 'uomID' => $row['uomID'], 'pUDisplay' => $row['productUomDisplay'], 'pUBase' => $row['productUomBase']);
            }

            $batchSerial = (isset($products[$row['productID']]['batchSerial'])) ? $products[$row['productID']]['batchSerial'] : array();
            $batchIDsInBatchSerial = (isset($products[$row['productID']]['productIDs'])) ? $products[$row['productID']]['productIDs'] : array();
            if ($row['serialProductBatchID'] != NULL && $row['serialProductBatchID'] == $row['productBatchID'] && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {
                $batchSerial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'], 'PBSWoD' => $row['productSerialWarrantyPeriod'], 'PBSExpD' => $row['productSerialExpireDate'], 'PBUP' => $row['productBatchPrice']);
                if ($row['productBatchID'] != NULL) {
                    $batchIDsInBatchSerial[$row['productBatchID']] = array('PBID' => $row['productBatchID']);
                }
            }

            $batch = (isset($products[$row['productID']]['batch'])) ? $products[$row['productID']]['batch'] : array();
            if ($row['productBatchID'] != NULL && $row['serialProductBatchID'] != $row['productBatchID']) {
                $batch[$row['productBatchID']] = array('PBID' => $row['productBatchID'], 'PBC' => $row['productBatchCode'], 'PBQ' => $row['productBatchQuantity'], 'PSID' => $row['productSerialID'], 'PBExpD' => $row['productBatchExpiryDate'], 'PBWoD' => $row['productBatchWarrantyPeriod'], 'PBUP' => $row['productBatchPrice']);
            }

            $serial = (isset($products[$row['productID']]['serial'])) ? $products[$row['productID']]['serial'] : array();
            if ($row['productSerialID'] != NULL && $row['serialProductBatchID'] == NULL && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {

                $serial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'], 'PSWoD' => $row['productSerialWarrantyPeriod'], 'PSExpD' => $row['productSerialExpireDate'],'PSWoT' => $row['productSerialWarrantyPeriodType']);
            }

            $schemeConditions = [];
            if ($row['isUseDiscountScheme'] == 1) {
                $schemeConditions = $this->CommonTable('DiscountSchemeConditionTable')->getSchemeConditionsBySchemeID($row['discountSchemID']);
            }



            $tempP['tax'] = $tax;
            $tempP['schemeConditions'] = $schemeConditions;
            $tempP['uom'] = $uom;
            $tempP['batch'] = $batch;
            $tempP['serial'] = $serial;
            $tempP['batchSerial'] = $batchSerial;
            $tempP['productIDs'] = $batchIDsInBatchSerial;
            $productID = $row['productID'];
            $products[$row['productID']] = $tempP;
        } 
        return $products[$productID];
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * @param type $activityID
     * @param type $activityCode
     * @param type $locationID
     * get activity related all products
     * @return Data
     */
    public function getActivityRelatedProductByActivityID($activityID, $locationID)
    {
        $products = array(
            'productID' => '',
            'unitPrice' => 0.00,
            'discount' => 0.00,
            'discountType' => '',
            'quantity' => 0.00,
            'productCode' => '',
            'productName' => '',
            'productType' => '',
            'subProduct' => ''
        );

        $subProduct = array(
            'activitySubProductID' => '',
            'productBatchID' => '',
            'productBatchCode' => '',
            'productSerialID' => '',
            'productSerialCode' => '',
            'activitySubProductQuantity' => '',
        );

        $costypeArray = array();

//get customer details
        $JobData = $this->CommonTable('JobCard\Model\JobTable')->getJobDetilsByActivityID($activityID)->current();
        $customerID = $JobData['customerID'];
        $customer = $this->getCustomerDetailsByCustomerID($customerID);

//get cost type related product details
        $activityCostTypeData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityCostTypesByActivityID($activityID, $locationID);
        foreach ($activityCostTypeData as $values) {
            $values = (object) $values;

            if ($values->costTypeID) {
                if ($costypeArray[$values->productID] == $values->productID) {
                    $activityProduct[$values->productID]->unitPrice = $activityProduct[$values->productID]->unitPrice + $values->activityCostTypeActualCost;
                } else {
                    $activityProduct[$values->productID] = (object) $products;
                    $costypeArray[$values->productID] = $values->productID;
                    $activityProduct[$values->productID]->unitPrice = $values->activityCostTypeActualCost;
                }

                $activityProduct[$values->productID]->productID = $values->productID;
                $activityProduct[$values->productID]->discount = 0.00;
                $activityProduct[$values->productID]->productType = $values->productTypeID;
                $activityProduct[$values->productID]->quantity = 0;
                $activityProduct[$values->productID]->productCode = $values->productCode;
                $activityProduct[$values->productID]->productName = $values->productName;
                $activityProduct[$values->productID]->locationProductID = $values->locationProductId;

                if ($values->locationDiscountValue != 0) {
                    $activityProduct [$values->productID]->discountType = 'value';
                } else {
                    $activityProduct[$values->productID]->discountType = 'precentage';
                }
                $locationProducts[$values->productID] = $this->getLocationProductDetails($locationID, $values->productID);
            }
        }


//get activity raw materials details
        $activityRawData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityRawMaterialsByActivityID($activityID);
        foreach ($activityRawData as $values) {
            $values = (object) $values;
            if ($values->activityRawMaterialId != '') {
                $activityProduct[$values->productID] = (object) $products;
                $activityProduct[$values->productID]->productID = $values->productID;
                $activityProduct[$values->productID]->unitPrice = $values->activityRawMaterialCost;
                $activityProduct[$values->productID]->quantity = $values->activityRawMaterialQuantity;
                if ($values->locationDiscountValue != 0) {
                    $activityProduct [$values->productID]->discountType = 'value';
                    $activityProduct [$values->productID]->discount = $values->locationDiscountValue;
                } else {
                    $activityProduct[$values->productID]->discountType = 'precentage';
                    $activityProduct [$values->productID]->discount = $values->locationDiscountPercentage;
                }

                $activityProduct[$values->productID]->productCode = $values->productCode;
                $activityProduct[$values->productID]->productName = $values->productName;
                $activityProduct[$values->productID]->productType = $values->productTypeID;
                $activityProduct[$values->productID]->locationProductID = $values->locationProductId;

                if ($values->activityRawMaterialSubProductID != '') {
                    $activitySubProduct[$values->productID][$values->activityRawMaterialSubProductID] = (object) $subProduct;
                    $activitySubProduct[$values->productID][$values->activityRawMaterialSubProductID]->activitySubProductID = $values->activityRawMaterialSubProductID;
                    $activitySubProduct[$values->productID][$values->activityRawMaterialSubProductID]->productBatchID = $values->productBatchID;
                    $activitySubProduct[$values->productID][$values->activityRawMaterialSubProductID]->productSerialID = $values->productSerialID;
                    $activitySubProduct[$values->productID][$values->activityRawMaterialSubProductID]->productBatchCode = $values->productBatchCode;
                    $activitySubProduct[$values->productID][$values->activityRawMaterialSubProductID]->productSerialCode = $values->productSerialCode;
                    $activitySubProduct[$values->productID][$values->activityRawMaterialSubProductID]->activitySubProductQuantity = $values->activityRawMaterialSubProductQuantity;
                }
                if ($activitySubProduct) {
                    $activityProduct[$values->productID]->subProduct = $activitySubProduct[$values->productID];
                }
                $locationProducts[$values->productID] = $this->getLocationProductDetails($locationID, $values->productID);
            }
        }

//get invoice produts of activity
        $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDetailsByActivityID($activityID);
        foreach ($invoiceData as $values) {
            $values = (object) $values;
            $invoiceProduct[$values->productID] = $values;
        }

        if ($activityProduct) {
//remove invoiced product from activity
            foreach ($activityProduct as $key => $values) {
                if ($invoiceProduct[$key]) {
                    unset($activityProduct[$key]);
                }
            }
            if ($activityProduct) {
                $flag = 'hasProducts';
            } else {
                $flag = 'invoiced';
            }
        } else {
            $flag = 'notProducts';
        }
//pass activity produts data,customer details and location product details
        $data = array(
            'activityProducts' => $activityProduct,
            'customer' => $customer,
            'locationProducts' => $locationProducts,
            'flag' => $flag,
        );
        return $data;
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * @param type $jobID
     * @param type $locationID
     * get job related all activity products
     * @return Data
     */
    public function getAllActivitiesProductsByJobId($jobID, $locationID)
    {
//get all activities by jobID
        $activityArray = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID);
        $activityProducts = array();
        $activityProductsPrice = array();
        $locationProducts = array();
        $customer = '';

        if ($activityArray->count() != 0) {
            foreach ($activityArray as $values) {
                $values = (object) $values;
//get activity related product details by activityid and locationid
                $data = $this->getActivityRelatedProductByActivityID($values->activityId, $locationID);
                if ($data['flag'] == 'hasProducts') {
                    foreach ($data['activityProducts'] as $pro) {
                        $pro = (object) $pro;
//check whether product is inventory or non Inventory
                        if ($pro->productType != 2) {
                            if ($activityProducts[$pro->productID]) {
//if product is alredy added to array then calculate totla quantity and average unit price for that product
                                $activityProductsPrice[$pro->productID]['productPrice'] = $activityProductsPrice[$pro->productID]->productPrice + ($pro->unitPrice * $pro->quantity);
                                foreach ($pro->subProduct as $subPro) {
                                    $subPro = (object) $subPro;
                                    $activityProducts[$pro->productID]->subProduct[$subPro->activitySubProductID] = $subPro;
                                }
                                $activityProducts[$pro->productID]->quantity = $activityProducts[$pro->productID]->quantity + $pro->quantity;
                                $activityProducts[$pro->productID]->unitPrice = $activityProductsPrice[$pro->productID]->productPrice / $activityProducts[$pro->productID]->quantity;
                            } else {
                                $activityProducts[$pro->productID] = (object) $pro;
                                $activityProductsPrice[$pro->productID]['productPrice'] = $pro->unitPrice * $pro->quantity;
                            }
                        } else {
//if the product is non inventory then calculate unit price of each product
                            if ($activityProducts[$pro->productID]) {
                                $activityProducts[$pro->productID]->unitPrice = $activityProducts[$pro->productID]->unitPrice + $pro->unitPrice;
                            } else {
                                $activityProducts[$pro->productID] = (object) $pro;
                            }
                        }
                    }
//get location product details to an array
                    foreach ($data['locationProducts'] as $locPro) {
                        $locPro = (object) $locPro;
                        $locationProducts[$locPro->pID] = $locPro;
                    }
                }
                $customer = $data['customer'];
            }

//get invoice produts by jobID
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDetailsByjobID($jobID);
            foreach ($invoiceData as $values) {
                $values = (object) $values;
                $invoiceProduct[$values->productID] = $values;
            }
//remove invoiced product from job
            foreach ($activityProducts as $key => $values) {
                if ($invoiceProduct[$key]) {
                    if ($activityProducts[$key]) {
                        unset($activityProducts[$key]);
                    }
                }
            }
            if ($activityProducts) {
                $flag = 'hasProducts';
            } else {
                $flag = 'invoiced';
            }
        } else {
            $flag = 'noActivity';
        }

        $jobProductsData = array(
            'activityProducts' => $activityProducts,
            'customer' => $customer,
            'locationProducts' => $locationProducts,
            'flag' => $flag
        );
        return $jobProductsData;
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * check and update actvity status
     */
    public function updateActivityStatus($activityID, $products)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
//get cost type related product details
        $activityCostTypeData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityCostTypesByActivityID($activityID, $locationID);
        foreach ($activityCostTypeData as $values) {
            $values = (object) $values;
            $activityProduct[$values->productID] = $values;
        }

//get activity raw materials details
        $activityRawData = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityRawMaterialsByActivityID($activityID);

        foreach ($activityRawData as $values) {
            $values = (object) $values;
            if ($values->activityRawMaterialId != '') {
                $activityProduct[$values->productID] = $values;
            }
        }

        /* check whether all activity products were invoiced or not.
          this is done by getting two arrays of products called activity all products and invoice products,
          compare those two arrays and all invoice products were removed from activity produts array
         */
        foreach ($products as $key => $values) {
            $values = (object) $values;
            $pkey = split("_", $key)[0];
            if ($activityProduct[$pkey]) {
                unset($activityProduct[$pkey]);
            }
        }

        /*  check activity produts array.
          if any produts were not found from activity products array then update the actiity status as close.
         */
        if (!$activityProduct) {
            $data = array(
                'activityId' => $activityID,
                'activityStatus' => 4
            );
            $activityData = new Activity();
            $activityData->exchangeArray($data);
            $result = $this->CommonTable('JobCard\Model\ActivityTable')->updateActivityStatus($activityData);
        } else {
            $result = true;
        }
        return $result;
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * check and update job status
     */
    public function updateJobStatus($jobID, $products)
    {
//get all activity related product details by job id
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $data = $this->getAllActivitiesProductsByJobId($jobID, $locationID);
        $activityProduct = $data['activityProducts'];

        /* check whether job related products were invoiced or not.
          this is done by getting two arrays of products called job all products and invoice products,
          compare those two arrays and all invoice products were removed from job produts array
         */
        foreach ($products as $key => $values) {
            $values = (object) $values;
            if ($activityProduct[$key]) {
                unset($activityProduct[$key]);
            }
        }

        /*  check job produts array.
          if any produts were not found from job products array then update the job status as close.
         */
        if (!$activityProduct) {
//get all activity by job id
            $activityArray = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID);
//update all activity status as close
            foreach ($activityArray as $values) {
                $values = (object) $values;
                $data = array(
                    'activityId' => $values->activityId,
                    'activityStatus' => 4
                );
                $activityData = new Activity();
                $activityData->exchangeArray($data);
                $result = $this->CommonTable('JobCard\Model\ActivityTable')->updateActivityStatus($activityData);
            }
            $jobStatus = array(
                'jobId' => $jobID,
                'jobStatus' => 4
            );
            $jobData = new Job();
            $jobData->exchangeArray($jobStatus);
            $result = $this->CommonTable('JobCard\Model\JobTable')->updateJobStatus($jobData);
        } else {
            $result = true;
        }
        return $result;
    }

    /* @author Sharmilan <sharmilan@thinkcube.com>
     * Get Location Product Details For Return by locationID and ProductID
     * @param type $locationID
     * @param type $productID
     * @return type
     */

    public function getLocationProductDetailsForReturn($locationID, $productID, $invalidFlag = false)
    {

        $productsList = $this->CommonTable('Inventory\Model\ProductTable')->getActiveProductListByLocation($locationID, $productID, false,$invalidFlag);
        $products = array();
        foreach ($productsList as $row) {
            if ($row['productSerialReturned'] == 0) {
                $tempP = array();
                $tempP['pC'] = $row['productCode'];
                $tempP['pID'] = $row['productID'];
                $tempP['lPID'] = $row['locationProductID'];
                $tempP['pN'] = $row['productName'];
                $tempP['LPQ'] = $row['locationProductQuantity'];
                $tempP['bP'] = $row['batchProduct'];
                $tempP['sP'] = $row['serialProduct'];
                $tempP['dSP'] = $row['defaultSellingPrice'];
                $tempP['dPR'] = $row['locationDiscountPercentage'];
                $tempP['dEL'] = $row['productDiscountEligible'];
                $tempP['dV'] = $row['locationDiscountValue'];
                $tempP['giftCard'] = $row['productHandelingGiftCard'];
                $tempP['mrpType'] = $row['mrpType'];
                $tempP['mrpValue'] = $row['mrpValue'];
                $tempP['mrpPercentageValue'] = $row['mrpPercentageValue'];
                $tempP['useDisScheme'] = $row['isUseDiscountScheme'];

                $tax = (isset($products[$row['productID']]['tax'])) ? $products[$row['productID']]['tax'] : array();
                if ($row['productTaxEligible'] == 1) {
                    if ($row['taxID'] != NULL) {
                        $tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state']);
                    }
                }
                $uom = (isset($products[$row['productID']]['uom'])) ? $products[$row['productID']]['uom'] : array();
                if ($row['uomID'] != NULL) {
                    $uom[$row['uomID']] = array('uA' => $row['uomAbbr'], 'uN' => $row['uomName'], 'uDP' => $row['uomDecimalPlace'], 'uS' => $row['uomState'], 'uC' => $row['productUomConversion'], 'pUI' => $row['productUomID'], 'pUDisplay' => $row['productUomDisplay'], 'pUBase' => $row['productUomBase'], 'uomID' => $row['uomID']);
                }

                $batchSerial = (isset($products[$row['productID']]['batchSerial'])) ? $products[$row['productID']]['batchSerial'] : array();
                $batchIDsInBatchSerial = (isset($products[$row['productID']]['productIDs'])) ? $products[$row['productID']]['productIDs'] : array();
                if ($row['serialProductBatchID'] != NULL && $row['serialProductBatchID'] == $row['productBatchID']) {
                    $batchSerial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'], 'PBSWoD' => $row['productSerialWarrantyPeriod'], 'PBSExpD' => $row['productSerialExpireDate'], 'PBUP' => $row['productBatchPrice']);
                    if ($row['productBatchID'] != NULL) {
                        $batchIDsInBatchSerial[$row['productBatchID']] = array('PBID' => $row['productBatchID']);
                    }
                }

                $batch = (isset($products[$row['productID']]['batch'])) ? $products[$row['productID']]['batch'] : array();
                if ($row['productBatchID'] != NULL && $row['serialProductBatchID'] != $row['productBatchID']) {
                    $batch[$row['productBatchID']] = array('PBID' => $row['productBatchID'], 'PBC' => $row['productBatchCode'], 'PBQ' => $row['productBatchQuantity'], 'PSID' => $row['productSerialID'], 'PBExpD' => $row['productBatchExpiryDate'], 'PBWoD' => $row['productBatchWarrantyPeriod'], 'PBUP' => $row['productBatchPrice']);
                }

                $serial = (isset($products[$row['productID']]['serial'])) ? $products[$row['productID']]['serial'] : array();
                if ($row['productSerialID'] != NULL && $row['serialProductBatchID'] == NULL) {

                    $serial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'], 'PSWoD' => $row['productSerialWarrantyPeriod'], 'PSExpD' => $row['productSerialExpireDate'],'PSWoT' => $row['productSerialWarrantyPeriodType']);
                }


                $schemeConditions = [];
                if ($row['isUseDiscountScheme'] == 1) {
                    $schemeConditions = $this->CommonTable('DiscountSchemeConditionTable')->getSchemeConditionsBySchemeID($row['discountSchemID']);
                }

                $tempP['tax'] = $tax;
                $tempP['uom'] = $uom;
                $tempP['schemeConditions'] = $schemeConditions;
                $tempP['batch'] = $batch;
                $tempP['serial'] = $serial;
                $tempP['batchSerial'] = $batchSerial;
                $tempP['productIDs'] = $batchIDsInBatchSerial;
                $products[$row['productID']] = $tempP;
            }
        }
        return $products[$productID];
    }

    /**
     * @author Ashan <ashan@thinkcube.com>
     * @param type $projectID
     * @param type $locationID
     * get job related all activity products
     * @return Data
     */
    public function getAllJobProductsByProjectId($projectID, $locationID)
    {
//get all jobs by projectID
        $jobArray = $this->CommonTable('JobCard\Model\JobTable')->getJobListByProjectID($projectID, $locationID);
        $jobProducts = array();
        $jobProductsPrice = array();
        $locationProducts = array();
        $customer = '';

        if ($jobArray->count() != 0) {
            foreach ($jobArray as $values) {
                $values = (object) $values;
//get Job related product details by jobID and locationid
                $data = $this->getAllActivitiesProductsByJobId($values->jobId, $locationID);
                if ($data['flag'] == 'hasProducts') {
                    foreach ($data['activityProducts'] as $pro) {
                        $pro = (object) $pro;
//check whether product is inventory or non Inventory
                        if ($pro->productType != 2) {
                            if ($jobProducts[$pro->productID]) {
//if product is alredy added to array then calculate totla quantity and average unit price for that product
                                $jobProductsPrice[$pro->productID]->productPrice = $jobProductsPrice[$pro->productID]->productPrice + ($pro->unitPrice * $pro->quantity);
                                foreach ($pro->subProduct as $subPro) {
                                    $subPro = (object) $subPro;
                                    $jobProducts[$pro->productID]->subProduct[$subPro->activitySubProductID] = $subPro;
                                }
                                $jobProducts[$pro->productID]->quantity = $jobProducts[$pro->productID]->quantity + $pro->quantity;
                                $jobProducts[$pro->productID]->unitPrice = $jobProductsPrice[$pro->productID]->productPrice / $jobProducts[$pro->productID]->quantity;
                            } else {
                                $jobProducts[$pro->productID] = (object) $pro;
                                $jobProductsPrice[$pro->productID]->productPrice = $pro->unitPrice * $pro->quantity;
                            }
                        } else {
//if the product is non inventory then calculate unit price of each product
                            if ($jobProducts[$pro->productID]) {
                                $jobProducts[$pro->productID]->unitPrice = $jobProducts[$pro->productID]->unitPrice + $pro->unitPrice;
                            } else {
                                $jobProducts[$pro->productID] = (object) $pro;
                            }
                        }
                    }
//get location product details to an array
                    foreach ($data['locationProducts'] as $locPro) {
                        $locPro = (object) $locPro;
                        $locationProducts[$locPro->pID] = $locPro;
                    }
                }
                $customer = $data['customer'];
            }

//get invoice produts by projectID
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDetailsByProjectID($projectID);
            foreach ($invoiceData as $values) {
                $values = (object) $values;
                $invoiceProduct[$values->productID] = $values;
            }
//remove invoiced product from job
            foreach ($jobProducts as $key => $values) {
                if ($invoiceProduct[$key]) {
                    if ($jobProducts[$key]) {
                        unset($jobProducts[$key]);
                    }
                }
            }
            if ($jobProducts) {
                $flag = 'hasProducts';
            } else {
                $flag = 'invoiced';
            }
        } else {
            $flag = 'noJobs';
        }

        $jobProductsData = array(
            'activityProducts' => $jobProducts,
            'customer' => $customer,
            'locationProducts' => $locationProducts,
            'flag' => $flag
        );
        return $jobProductsData;
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * check and update project status
     */
    public function updateProjectStatus($projectID, $products)
    {
//get all jobs related product details by project id
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $data = $this->getAllJobProductsByProjectId($projectID, $locationID);
        $activityProduct = $data['activityProducts'];

        /* check whether project related products were invoiced or not.
          this is done by getting two arrays of products called project all products and invoice products,
          compare those two arrays and all invoice products were removed from project produts array
         */
        foreach ($products as $key => $values) {
            $values = (object) $values;
            if ($activityProduct[$key]) {
                unset($activityProduct[$key]);
            }
        }

        /*  check project produts array.
          if any produts were not found from project products array then update the job status as close.
         */
        if (!$activityProduct) {
//get all jobs by projectid
            $jobArray = $this->CommonTable('JobCard\Model\JobTable')->getJobListByProjectID($projectID, $locationID);
//update all activity status as close
            foreach ($jobArray as $values) {
                $values = (object) $values;
                $data = array(
                    'jobId' => $values->jobId,
                    'jobStatus' => 4
                );
                $projectData = new Job();
                $projectData->exchangeArray($data);
                $result = $this->CommonTable('JobCard\Model\JobTable')->updateJobStatus($projectData);
//get all activity by job id
                $activityArray = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($values->jobId);
//update all activity status as close
                foreach ($activityArray as $values) {
                    $values = (object) $values;
                    $data = array(
                        'activityId' => $values->activityId,
                        'activityStatus' => 4
                    );
                    $activityData = new Activity();
                    $activityData->exchangeArray($data);
                    $result = $this->CommonTable('JobCard\Model\ActivityTable')->updateActivityStatus($activityData);
                }
            }
            $projectStatus = array(
                'projectId' => $projectID,
                'projectStatus' => 4
            );
            $projectData = new Project();
            $projectData->exchangeArray($projectStatus);
            $result = $this->CommonTable('JobCard\Model\ProjectTable')->updateProjectStatus($projectData);
        } else {
            $result = true;
        }
        return $result;
    }

    public function timeCalcutator($time)
    {

        $days = floor($time / 1440);
        $temp1 = $time % 1440;
        $hours = floor($temp1 / 60);
        $temp2 = $temp1 % 60;
        $minutes = $temp2;
        $string = "$days day(s), $hours hour(s) and $minutes minute(s) estimated.";
        return $string;
    }

    /**
     * Get Physical path for temporary user wise attachments
     * @author Sharmilan <sharmilan@thinkcube.com>
     * return String
     */
    public function getTemporaryAttachmentDirectoryPath($clear = false)
    {
        $docPath = $this->getCompanyDataFolder() . '/temporary-attachments/' . md5($this->userID) . '/images/';

        if (!is_dir($docPath)) {
            mkdir($docPath, 0777, true);
        }
//        clear inside of folder
        if ($clear) {
            $this->_deleteDirectory($docPath);
        }
        return $docPath;
    }

    /**
     * Get Image Attachments Directory path
     *  - This Directory will store the image attachments by the user
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return string
     */
    public function getImageAttachmentsDirectoryPath($isBrowserPath = false)
    {
        $docPath = $this->getCompanyDataFolder() . '/attachments/images/';

        if (!is_dir($docPath)) {
            mkdir($docPath, 0777, true);
        }

        if ($isBrowserPath) {
            $docPath = '/userfiles/' . $this->getCompanyImageFolderName() . '/attachments/images/';
        }

        return $docPath;
    }

    /**
     * Save attachment details to the table
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $attachmentName file name with extention
     * @param type $attachmentPath physical file path
     * @param type $attachmentSize in Byte
     * @param type $entityID related entityID for particular file
     * @param type $entitySubTypeID
     *
     * @return boolean
     */
    public function saveAttachment($attachmentName, $attachmentPath, $attachmentSize, $entityID, $entitySubTypeID)
    {
        $attchment = new Attachment();
        $data = array(
            'attachmentName' => $attachmentName,
            'attachmentPath' => $attachmentPath,
            'attachmentSize' => $attachmentSize,
            'entityID' => $entityID,
            'entitySubTypeID' => $entitySubTypeID,
            'userID' => $this->userID,
        );
        $attchment->exchangeArray($data);
        $attachmentID = $this->CommonTable('Core\model\AttachmentTable')->save($attchment);
        if ($attachmentID) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * This will delete whole items and folder inside the the $dir path
     * @author sharmilan <sharmilan@thincube.com>
     * @param type $dir
     */
    private function _deleteDirectory($dir)
    {
        if (is_dir($dir)) {
            $objects = scandir($dir);
            foreach ($objects as $object) {
                if ($object != "." && $object != "..") {
                    if (filetype($dir . "/" . $object) == "dir") {
                        $this->_deleteDirectory($dir . "/" . $object);
                    } else {
                        unlink($dir . "/" . $object);
                    }
                }
            }
            reset($objects);
            rmdir($dir);
        }
    }

    /** @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * Compare logged user with super admin
     * @return Boolean value
     */
    public function isSuperAdmin()
    {
        $userID = $this->user_session->userID;
        $userRole = $this->CommonTable("User\Model\RoleTable")->getRoleIdByName('Super Admin');
        $userData = $this->CommonTable("User\Model\UserTable")->getUser($userID);

        return ((int) $userRole === (int) $userData->roleID);
    }

    /** @author sharmilan <sharmilan@thinkcube.com>
     * get product quantity for display
     * @pram int quantity of the product via base uom
     * @pram array Uom list of the product
     * @return array quantity of the product via display Uom
     */
    public function getProductQuantityViaDisplayUom($quantity, $uomList)
    {
        $noOfUoms = count($uomList);
        if ($noOfUoms == 0) {
            $displayUomDetails['quantity'] = $quantity;
            $displayUomDetails['uomAbbr'] = Null;
        } else if ($noOfUoms == 1) {
//if only having
            $displayUomDetails['quantity'] = $quantity / $uomList[0]['productUomConversion'];
            $displayUomDetails = array_merge($displayUomDetails, $uomList[0]);
        } else if ($noOfUoms > 1) {

//get index of display uom with have true value
            $displayUomKey = array_search(1, array_column($uomList, 'productUomDisplay'));

            if (is_int($displayUomKey)) {
                $displayUomDetails['quantity'] = $quantity / $uomList[$displayUomKey]['productUomConversion'];
                $displayUomDetails = array_merge($displayUomDetails, $uomList[$displayUomKey]);
            } else {
//if not set display uom get base uom details
                $baseUomKey = array_search('1', array_column($uomList, 'productUomConversion'));
                $displayUomDetails['quantity'] = $quantity;
                $displayUomDetails = array_merge($displayUomDetails, $uomList[$baseUomKey]);
            }
        }
        return $displayUomDetails;
    }

    /** @author Ashan Madushka <ashan@thinkcube.com>
     * get product unitPrice via display uom type
     * $pram1=>price of the product via base uom
     * $pram2=>Uom list of the product
     * $return=>price of the product via display Uom
     */
    public function getProductUnitPriceViaDisplayUom($unitPrice, $uomList)
    {
        $conversionRate = 0.00;
        foreach ($uomList as $value) {
            $value = (object) $value;
            if ($value->productUomDisplay == 1) {
                $conversionRate = $value->productUomConversion;
                break;
            } else {
                $conversionRate = ($conversionRate < $value->productUomConversion) ? $value->productUomConversion : $conversionRate;
            }
        }
        $pUnitPrice = round(($unitPrice * $conversionRate), self::NUMBER_OF_DEFAULT_DECIMAL_TO_ROUND);
        $pUnitPrice = number_format($pUnitPrice, $this->getNumberOfDecimalPoints($pUnitPrice), '.', '');
        return $pUnitPrice;
    }

    /**
     * get number of decimal points
     * @param string $number
     * @return int
     */
    public function getNumberOfDecimalPoints($number)
    {
        $decimals = self::NUMBER_OF_DEFAULT_DECIMAL_POINTS;

        $length = 0;
        $string = strrchr((float)$number, ".");
        if ($string != false) {
            $length = strlen(substr($string, 1));
        }
        return ($length < $decimals) ? $decimals : $length;
    }

    //this will return the product uom data for uom plugins
    public function getProductRelatedUomForUomPlugin($productId)
    {
        $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productId);

        foreach ($productUom as $value) {
            $uom[] = ['uomID' => $value['uomID'], 'us' => $value['uomState'], 'uA' => $value['uomAbbr'], 'uDP' => $value['uomDecimalPlace'], 'uC' => $value['productUomConversion'], 'pUDisplay' => $value['productUomDisplay'], 'uN' => $value['uomName']];
        }

        return $uom;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * @param array $inquiryLogList
     * @param boolean $paginate
     * @return \Zend\Paginator\Paginator | array
     */
    public function setInquiryTypeAndComplainType($inquiryLogList, $paginate = true)
    {
        $inqCompData = array();
        foreach ($inquiryLogList as $inquiryComplainDetails) {

            if ($inquiryComplainDetails['inquiryComplainType'] == 'Inquiry') {
                $documentType = 'Job';
                $inquiryComplainDetails['documentType'] = $documentType;
                $inqComplainDetailsJob = $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->getDocumentForGivenInqID($inquiryComplainDetails['inquiryLogID'], '22')->current();
                $inqComplainDetailsProject = $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->getDocumentForGivenInqID($inquiryComplainDetails['inquiryLogID'], '21')->current();
                $inqComplainDetailsActivity = $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->getDocumentForGivenInqID($inquiryComplainDetails['inquiryLogID'], '23')->current();
                if ($inqComplainDetailsJob) {
                    $jobDetails = $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($inqComplainDetailsJob['inquiryComplainRelationDocumentID'])->current();
                    $inquiryComplainDetails['documentID'] = $jobDetails['jobReferenceNumber'];
                } else if ($inqComplainDetailsProject) {
                    $proDetails = $this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByProjectID($inqComplainDetailsProject['inquiryComplainRelationDocumentID'])->current();
                    $inquiryComplainDetails['documentID'] = $proDetails['projectCode'];
                }
            } else {
                $documentList = $this->CommonTable('JobCard\Model\InquiryComplainRelationTable')->getDocumentForGivenInqID($inquiryComplainDetails['inquiryLogID'])->current();
                switch ($documentList['documentTypeID']) {
                    case '1':
                        $quotationDetails = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByID($documentList['inquiryComplainRelationDocumentID']);
                        $inquiryComplainDetails['documentType'] = 'Quotation';
                        $inquiryComplainDetails['documentID'] = $quotationDetails->quotationCode;
                        break;
                    case '2':
                        $salesOrder = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($documentList['inquiryComplainRelationDocumentID']);
                        $inquiryComplainDetails['documentType'] = 'Sales Order';
                        $inquiryComplainDetails['documentID'] = $salesOrder->soCode;
                        break;
                    case '3':
                        $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByInvoiceID($documentList['inquiryComplainRelationDocumentID'])->current();
                        $inquiryComplainDetails['documentType'] = 'Invoice';
                        $inquiryComplainDetails['documentID'] = $invoice->salesInvoiceCode;
                        break;
                    case '21':
                        $project = (object) $this->CommonTable('JobCard\Model\ProjectTable')->getProjectDetailsByProjectID($documentList['inquiryComplainRelationDocumentID'])->current();
                        $inquiryComplainDetails['documentType'] = 'Project';
                        $inquiryComplainDetails['documentID'] = $project->projectCode;
                        break;
                    case '22':
                        $job = (object) $this->CommonTable('JobCard\Model\JobTable')->getJobDetails($documentList['inquiryComplainRelationDocumentID'])->current();
                        $inquiryComplainDetails['documentType'] = 'Job';
                        $inquiryComplainDetails['documentID'] = $job->jobReferenceNumber;
                        break;
                    case '23':
                        $activity = (object) $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByActivityID($documentList['inquiryComplainRelationDocumentID'])->current();
                        $inquiryComplainDetails['documentType'] = 'Activity';
                        $inquiryComplainDetails['documentID'] = $activity->activityCode;
                        break;
                    case '24':
                        $inquiryLog = (object) $this->CommonTable('JobCard\Model\InquiryLogTable')->getInquiryLogDetailsByInquiryLogID($documentList['inquiryComplainRelationDocumentID'])->current();
                        $inquiryComplainDetails['documentType'] = 'Inquiry Log';
                        $inquiryComplainDetails['documentID'] = $inquiryLog->inquiryLogReference;
                        break;
                }
            }
            $inqCompData[] = $inquiryComplainDetails;
        }

        if ($paginate == false) {
            return $inqCompData;
        }

        $paginator = new \Zend\Paginator\Paginator(new \Zend\Paginator\Adapter\ArrayAdapter($inqCompData));
        $paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $paginator->setItemCountPerPage(10);
        return $paginator;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $reportContentData
     * render html content with related data
     * @return \Zend\View\Model\ViewModel $reportView
     */
    public function reportHtmlContentPrepare($reportContentData)
    {
        $companyDetails = $this->getCompanyDetails();

        $headerView = $this->headerViewTemplate($reportContentData['name'], $reportContentData['period']);
        $headerView->setTemplate('reporting/template/headerTemplateNormal');
        $headerViewRender = $this->htmlRender($headerView);

        $reportView = new ViewModel(array(
            'cD' => $companyDetails,
            'reportData' => $reportContentData['reportData'],
            'headerTemplate' => $headerViewRender,
        ));
        $reportView->setTemplate($reportContentData['htmlFilePath']);

        return $reportView;
    }

    //getCustomCurrencyByCurrencyId
    public function getCustomCurrencyByCustomCurrencyIdAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customCurrencyId = $request->getPost('customCurrencyId');
            if ($customCurrencyId != '') {
                $customCurrency = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            } else {
                $customCurrency = (object) array('currencySymbol' => $this->companyCurrencySymbol, 'currencyRate' => 1);
            }
            $this->data = $customCurrency;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    public function getCurrencySymbolAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currencyId = $request->getPost('currencyId');
            $value = $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($currencyId);
            $this->data = $value;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    //get Currency list with rate
    public function getCurrencyListWithRates()
    {
        //get custom currency list
        $cCurrency = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currency = [];
        foreach ($cCurrency as $c) {
            $c = (object) $c;
            if ($c->currencyRate > 0) {
                $currency[$c->currencyID] = $c->currencyName . '(' . $c->currencyRate . ')';
            }
        }
        return $currency;
    }

    //get Price list
    public function getActivePriceList()
    {
        //get price list
        $priceL = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAll();
        $priceList = [];
        foreach ($priceL as $pl) {
            $pl = (object) $pl;
            if ($pl->priceListState == 1) {
                $priceList[$pl->priceListId] = $pl->priceListName;
            }
        }
        return $priceList;
    }

    //get Price list Items
    public function getPriceListItemsAction()
    {
        //get price list
        $request = $this->getRequest();
        if ($request->isPost()) {
            $priceListId = $request->getPost('priceListId');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $priceLI = $this->CommonTable('Inventory\Model\PriceListItemsTable')->getPriceListItemsByPriceListIdAndLocationId($priceListId, $locationID);
            $priceListItems = [];
            foreach ($priceLI as $pl) {
                $pl = (object) $pl;
                $priceListItems[$pl->productId] = $pl->priceListItemsPrice;
            }
            if (count($priceListItems) > 0) {
                $this->data = $priceListItems;
                $this->status = true;
            } else {
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    public function getPriceListWithDiscountAction()
    {
        //get price list
        $request = $this->getRequest();
        if ($request->isPost()) {
            $priceListId = $request->getPost('priceListId');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $priceLI = $this->CommonTable('Inventory\Model\PriceListItemsTable')->getPriceListItemsByPriceListIdAndLocationId($priceListId, $locationID);
            $priceListItems = [];
            foreach ($priceLI as $pl) {
                $pl = (object) $pl;
                $priceListItems[$pl->productId] = [
                    'itemPrice' => $pl->priceListItemsPrice,
                    'itemDiscountType' => $pl->priceListItemsDiscountType,
                    'itemDiscount' => $pl->priceListItemsDiscount
                ];
            }
            if (count($priceListItems) > 0) {
                $this->data = $priceListItems;
                $this->status = true;
            } else {
                $this->status = false;
            }
            return $this->JSONRespond();
        }
    }

    protected function getDocumentTypes()
    {
        $documents = $this->CommonTable('Core\Model\CoreTable')->getDocumentType();
        $documentTypes = [];
        foreach ($documents as $dt) {
            $dt = (object) $dt;
            $documentTypes[$dt->documentTypeID] = $dt->documentTypeName;
        }
        return $documentTypes;
    }

    //get document reference by source document data
    protected function getDocumentReferenceBySourceData($sourceDocumentTypeId, $sourceDocumentId)
    {
        $documentReference = $this->CommonTable('Core\model\DocumentReferenceTable')->getDocumentReferenceBySourceData($sourceDocumentTypeId, $sourceDocumentId);
        $referenceData = [];
        foreach ($documentReference as $value) {
            $value = (object) $value;
            $referenceDocumentTypeId = $value->referenceDocumentTypeId;
            $referenceDocumentId = $value->referenceDocumentId;
            switch ($referenceDocumentTypeId) {
                case 1:
                    $invoicesdata = (object) $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByInvoiceID($referenceDocumentId)->current();
                    $invoice[] = ['id' => $referenceDocumentId, 'code' => $invoicesdata->salesInvoiceCode];
                    $referenceData[1] = $invoice;
                    break;
                case 2:
                    $quotationData = (object) $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByID($referenceDocumentId);
                    $quotation[] = ['id' => $referenceDocumentId, 'code' => $quotationData->quotationCode];
                    $referenceData[2] = $quotation;
                    break;
                case 3:
                    $salesOrderData = (object) $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($referenceDocumentId);
                    $salesOrder[] = ['id' => $referenceDocumentId, 'code' => $salesOrderData->soCode];
                    $referenceData[3] = $salesOrder;
                    break;
                case 4:
                    $deliveryNoteData = (object) $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteByID($referenceDocumentId);
                    $deliveryNote[] = ['id' => $referenceDocumentId, 'code' => $deliveryNoteData->deliveryNoteCode];
                    $referenceData[4] = $deliveryNote;
                    break;
                case 7:
                    $salesPaymentsData = (object) $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentByID($referenceDocumentId);
                    $salesPayments[] = ['id' => $referenceDocumentId, 'code' => $salesPaymentsData->incomingPaymentCode];
                    $referenceData[7] = $salesPayments;
                    break;
                case 9:
                    $purchaseOrderData = (object) $this->CommonTable('Inventory\Model\PurchaseOrderTable')->getPurchaseOrderByID($referenceDocumentId);
                    $purchaseOrder[] = ['id' => $referenceDocumentId, 'code' => $purchaseOrderData->purchaseOrderCode];
                    $referenceData[9] = $purchaseOrder;
                    break;
                case 10:
                    $grnData = (object) $this->CommonTable('Inventory\Model\GrnTable')->getGrnDetailsByGrnId($referenceDocumentId);
                    $grn[] = ['id' => $referenceDocumentId, 'code' => $grnData->grnCode];
                    $referenceData[10] = $grn;
                    break;
                case 12:
                    $purchaseInvoiceData = (object) $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getPurchaseInvoiceByID($referenceDocumentId)->current();
                    $purchaseInvoice[] = ['id' => $referenceDocumentId, 'code' => $purchaseInvoiceData->purchaseInvoiceCode];
                    $referenceData[12] = $purchaseInvoice;
                    break;
                case 15:
                    $transferData = (object) $this->CommonTable('Inventory\Model\TransferTable')->getTransferByID($referenceDocumentId);
                    $transfer[] = ['id' => $referenceDocumentId, 'code' => $transferData->transferCode];
                    $referenceData[15] = $transfer;
                    break;
                case 19:
                    $jobData = (object) $this->CommonTable('JobCard\Model\JobTable')->getJobsByJobID($referenceDocumentId);
                    $job[] = ['id' => $referenceDocumentId, 'code' => $jobData->jobReferenceNumber];
                    $referenceData[19] = $job;
                    break;
                case 20:
                    $paymentVoucherData = (object) $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherByPaymentVoucherID($referenceDocumentId);
                    $paymentVoucher[] = ['id' => $referenceDocumentId, 'code' => $paymentVoucherData->paymentVoucherCode];
                    $referenceData[20] = $paymentVoucher;
                    break;
                case 22:
                    $projectData = (object) $this->CommonTable('JobCard/Model/ProjectTable')->getProjectByProjectId($referenceDocumentId);
                    $project[] = ['id' => $referenceDocumentId, 'code' => $projectData->projectCode];
                    $referenceData[22] = $project;
                    break;
                case 23:
                    $activityData = (object) $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByActivityID($referenceDocumentId)->current();
                    $activity[] = ['id' => $referenceDocumentId, 'code' => $activityData->activityCode];
                    $referenceData[23] = $activity;
                    break;
                case 11:
                    $prData = (object) $this->CommonTable('Inventory\Model\PurchaseReturnTable')->getPRDetailsByPrId($referenceDocumentId);
                    $pr[] = ['id' => $referenceDocumentId, 'code' => $prData->purchaseReturnCode];
                    $referenceData[11] = $pr;
                    break;
                case 46:
                    $prData = (object) $this->CommonTable('Invoice\Model\CustomerOrderTable')->retriveCustomerOrder($referenceDocumentId)->current();
                    $co[] = ['id' => $referenceDocumentId, 'code' => $prData->customerOrderCode];
                    $referenceData[46] = $co;
                    break;
                default:
                    break;
            }
        }
        return $referenceData;
    }

    /**
     * get value of tax regarding to document type
     * @param int $documentID
     * @param string $documentType
     * @param int $locationProductID
     * @return double $taxValue
     */
    protected function getTaxValue($documentID, $documentType, $locationProductID)
    {
        $taxValue = 0;
        $productID = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProductByLocationProductID($locationProductID)->productID;
        switch ($documentType) {
            case "Goods Received Note":
                $grnTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($documentID, $locationProductID);

                foreach ($grnTax as $g) {
                    $quantity = $g['grnProductTotalQty'];
                    //get quantity and unit price according to dispaly uom
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productID);
                    $unitPrice = $this->getProductUnitPriceViaDisplayUom($g['grnTaxAmount'], $productUom);
                    $thisqty = $this->getProductQuantityViaDisplayUom($quantity, $productUom);
                    $grnQty = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                    $taxValue += $unitPrice / $grnQty;
                }
                break;

            case "Payment Voucher":
                $paymentVoucherTax = $this->CommonTable("Inventory\Model\PurchaseInvoiceProductTaxTable")->getPurchaseInvoiceProductTax($documentID, $locationProductID);

                foreach ($paymentVoucherTax as $pmntV) {
                    $quantity = $pmntV['purchaseInvoiceProductQuantity'];
                    //get quantity and unit price according to dispaly uom
                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productID);
                    $unitPrice = $this->getProductUnitPriceViaDisplayUom($pmntV['purchaseInvoiceTaxAmount'], $productUom);
                    $thisqty = $this->getProductQuantityViaDisplayUom($quantity, $productUom);
                    $paymentVoucherQty = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                    $taxValue += $unitPrice / $paymentVoucherQty;
                }
                break;

            default:
                break;
        }

        return $taxValue;
    }
/**
 * this function use to divide warranty period as months days and years.
 * @param int $days
 * @return string
 */
    public function calculateWarrantyPeriod($days,$type)
    {
     if($type==4){
        if ($days>1) {
            $warrantyPeriod[] = $days . ' years';
        } else if ($days== 1) {
            $warrantyPeriod[] =$days . ' year';
        } 
     }else if($type==3){
        if ($days>1) {
            $warrantyPeriod[] = $days . ' months';
        } else if ($days == 1) {
            $warrantyPeriod[] = $days . ' month';
        }
     }else if($type==2){
        if ($days>1) {
            $warrantyPeriod[] = $days . ' week';
        } else if ($days == 1) {
            $warrantyPeriod[] = $days . ' weeks';
        }
     }else{
        $years = floor($days/365);
        $months = floor(($days%365)/30);
        $days = floor((($days%365)%30));
        $warrantyPeriod = array();

        if ($years>1) {
            $warrantyPeriod[] = $years . ' years';
        } else if ($years == 1) {
            $warrantyPeriod[] =$years . ' year';
        }

        if ($months>1) {
            $warrantyPeriod[] = $months . ' months';
        } else if ($months == 1) {
            $warrantyPeriod[] = $months . ' month';
        }

        if ($days>1) {
            $warrantyPeriod[] = $days . ' days';
        } else if ($days == 1) {
            $warrantyPeriod[] = $days . ' day';
        }
    }
        return implode(" ", $warrantyPeriod);
    }

    protected function getService($service)
    {
        return $this->getServiceLocator()->get($service);
    }

    public function getFinanceAccounts()
    {
        $financeAccountsArray = [];
        $financeAccounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAll();
        foreach ($financeAccounts as $key => $value) {
            $financeAccountsArray[$value['financeAccountsID']] = $value;
        }
        return $financeAccountsArray;
    }


    /**
    * follwing function has optional parameter call $specialLocID -  that use only expense purchase Invoices aproval process.
    **/
    public function saveJournalEntry($JEData, $cancelJEDocumentTypeID = null, $cancelJEDocumentID = null, $restFlag = false, $restLocationID = null, $specialLocID = null, $isYearEnd = false)
    {
        $journalEntryCode = $JEData['journalEntryCode'];
        $JEData['journalEntryDate'] = $this->convertDateToStandardFormat($JEData['journalEntryDate']);
        $journalEntryDate = $JEData['journalEntryDate'];
        $ignoreBudgetLimit = (isset($JEData['ignoreBudgetLimit'])) ? $JEData['ignoreBudgetLimit'] : true;
        $journalEntryTemplateStatus = (isset($JEData['journalEntryTemplateStatus']))? filter_var($JEData['journalEntryTemplateStatus'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE): false;
        $journalEntryForOpeningBalance = (isset($JEData['journalEntryForOpeningBalance']))? filter_var($JEData['journalEntryForOpeningBalance'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE): false;

        if ($restFlag) {
            $locationID = $restLocationID;
        } elseif ($specialLocID) {
            $locationID = $specialLocID;
        } else {
            $locationID = $this->user_session->userActiveLocation['locationID'];
        }

        if ($journalEntryForOpeningBalance) {
            $journalEntryTemplateStatus = true;
        }

        if ($cancelJEDocumentTypeID != null && $cancelJEDocumentID != null) {
            $journalEntryReverseData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID($cancelJEDocumentTypeID, $cancelJEDocumentID);
            if (!$journalEntryReverseData) {
                $status = false;
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_FOR_REVERSE');
                return array('status' => $status, 'msg' => $msg);
            }
        }

        $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();
        if(!$journalEntryTemplateStatus){
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];

            $checkFiscalPeriod = false;
            $isAlreadyCompleteStageOne = false;
            if(count($fiscalPeriod) > 0){
                foreach ($fiscalPeriod as $key => $value) {
                    if($value['fiscalPeriodStatusID'] == 14){
                        if(($value['fiscalPeriodStartDate'] <= $journalEntryDate && $journalEntryDate <= $value['fiscalPeriodEndDate'])){
                            $checkFiscalPeriod = true;
                            if ($value['isCompleteStepOne'] == 1 && !$isYearEnd) {
                                // $isAlreadyCompleteStageOne = true;
                                $status = false;
                                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DATE_IS_IN_PROCESSING_YEAR_END_FISCAL_PERIOD');
                                return array('status' => $status, 'msg' => $msg);
                            } 
                        }
                    }
                }
            } else {
               $status = false;
               $msg = $this->getMessage('ERR_JOURNAL_ENTRY_FISCALPERIOD_NOT_CREATE');
               return array('status' => $status, 'msg' => $msg);
            }

            if(!$checkFiscalPeriod){
                $status = false;
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DATE_IS_NOTIN_FISCALPERIOD_RANGE');
                return array('status' => $status, 'msg' => $msg);
            }

            if($journalEntryCode == ''){
                $status = false;
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_CODE_LIMIT_EXEED');
                return array('status' => $status, 'msg' => $msg);
            }

            while ($journalEntryCode) {
                if ($this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryByJournalEntryCode($journalEntryCode)) {
                    if ($jelocationReferenceID) {
                        $newJournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);
                        if ($newJournalEntryCode == $journalEntryCode) {
                            $this->updateReferenceNumber($jelocationReferenceID);
                            $journalEntryCode = $this->getReferenceNumber($jelocationReferenceID);
                        } else {
                            $journalEntryCode = $newJournalEntryCode;
                        }
                    } else {
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_JOURNAL_ENTRY_CODE_EXISTS')
                            );
                    }
                } else {
                    break;
                }
            }
        }else{

            if($journalEntryCode == ''){
                $status = false;
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_TEMPLATE_CODE_LIMIT_EXEED');
                return array('status' => $status, 'msg' => $msg);
            }

            $jeresult = $this->getReferenceNoForLocation('31', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];

            while ($journalEntryCode) {
                if ($this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryByJournalEntryCode($journalEntryCode)) {
                    if ($jelocationReferenceID) {
                        $newJournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);
                        if ($newJournalEntryCode == $journalEntryCode) {
                            $this->updateReferenceNumber($jelocationReferenceID);
                            $journalEntryCode = $this->getReferenceNumber($jelocationReferenceID);
                        } else {
                            $journalEntryCode = $newJournalEntryCode;
                        }
                    } else {
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_JOURNAL_ENTRY_CODE_EXISTS')
                            );
                    }
                } else {
                    break;
                }
            }

            if ($journalEntryForOpeningBalance) {
                $JEData['openingBalanceFlag'] = 1;
            }
        }



        $JEData['journalEntryCode'] = $journalEntryCode;
        $JEData['locationID'] = $locationID;
        $JEData['journalEntryReverseRefID'] = null;
        $journalEntryForm = new JournalEntryForm('CreateJournalEntry');


        $journalEntryAccounts = $JEData['journalEntryAccounts'];
        $resJournalEntryStatus = $this->checkJournalEntryAccountStatus($JEData['journalEntryAccounts']);

        if (!$resJournalEntryStatus['status']) {
            return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_JOURNAL_ENTRY_ACCOUNT_INACTIVE', [$resJournalEntryStatus['msg']])
                    );
        }
        $journalEntryTypeID = (int) $JEData['journalEntryTypeID'];
        $journalEntryStatusID = 12;

        if(!$journalEntryTemplateStatus){
            $journalEntryApprovers = [];
            if($journalEntryTypeID){
                $approversData = $this->checkJournalEntryTypeApproversExists($journalEntryAccounts, $journalEntryTypeID);
                if($approversData['journalEntryApproversExist']){
                    $journalEntryStatusID = 7;
                    $journalEntryApprovers = $approversData['journalEntryApprovers'];
                }
            }
        }else{
            $journalEntryStatusID = 7;
        }

        $JEData['journalEntryStatusID'] = $journalEntryStatusID;
        $JEData['journalEntryTemplateStatus'] = $journalEntryTemplateStatus;
        $JEData['journalEntryHashValue'] = hash('md5', $journalEntryCode);

        //set journel entry data and inputfilers for vallidation
        $journalEntry = new JournalEntry();
        $journalEntryForm->setInputFilter($journalEntry->getInputFilter());
        $journalEntryForm->setData($JEData);

        if (!$journalEntryForm->isValid()) {
            $status = false;
            $msg = $this->getMessage('ERR_JOURNAL_ENTRY_CREATE');
            return array('status' => $status, 'msg' => $msg);
        }

        $JEData['entityID'] = $this->createEntity();
        $journalEntry->exchangeArray($JEData);
        $journalEntryID = $this->CommonTable('Accounting\Model\JournalEntryTable')->saveJournalEntry($journalEntry);

        if (!$journalEntryID) {
            $status = false;
            $msg = $this->getMessage('ERR_JOURNAL_ENTRY_CREATE');
            return array('status' => $status, 'msg' => $msg);
        }


        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        $budgetConf = (object) $this->CommonTable('Accounting\Model\BudgetConfigurationTable')->fetchAll();
        
        if ($displaySettings->isBudgetEnabled == 1 && $ignoreBudgetLimit == "false") {
            $budgetExceedMsg = "";
            $budgetCheck = $this->checkBudgetForAccounts($JEData['journalEntryAccounts'], $journalEntryDate);
            if (!$budgetCheck['status']) {
                if ($budgetConf->isNotificationEnabled == "1") {
                    foreach ($budgetCheck['data'] as $value) {
                        $financeAcData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($value)->current();
                        $budgetExceedMsg = ($budgetExceedMsg == "") ? $financeAcData['financeAccountsCode'].'-'.$financeAcData['financeAccountsName'] : $budgetExceedMsg.' ,'.$financeAcData['financeAccountsCode'].'-'.$financeAcData['financeAccountsName'];
                    }

                    if ($budgetConf->isBlockedEnabled == "1") {
                        $bData = "BlockBudgetLimit";                        
                    } else {
                        $bData = "NotifyBudgetLimit";                        
                    }

                    return array(
                        'status' => false,
                        'data' => $bData,
                        'msg' => $this->getMessage('ERR_JOURNAL_ENTRY_ACCOUNT_BUDGET_EXCEEDED', [$budgetExceedMsg])
                        );
                }
            }
        }
        //save journal entry accounts data
        $status = $this->saveJournalEntryAccounts($journalEntryAccounts,$journalEntryID,$journalEntryStatusID);
        if(!$status){
            $status = false;
            $msg = $this->getMessage('ERR_JOURNAL_ENTRY_CREATE');
            return array('status' => $status, 'msg' => $msg);
        }

        $this->updateReferenceNumber($jelocationReferenceID);
        if(!$journalEntryTemplateStatus){
            if (count($journalEntryApprovers) > 0) {

                $JEAstatus = $this->saveJournalEntryApprovers($journalEntryApprovers, $journalEntryID, 'Create');
                if(!$JEAstatus){
                    $status = false;
                    $msg = $this->getMessage('ERR_JOURNAL_ENTRY_CREATE');
                    return array('status' => $status, 'msg' => $msg);
                }

                $emailStatus = $this->sendApproversEmail($journalEntryID, $journalEntryCode, $journalEntryApprovers, $JEData['journalEntryHashValue'], 'Create');
            }
        }

        $status = true;
        $msg = $this->getMessage('SUCC_JOURNAL_ENTRY_CREATE');
        $data = array('journalEntryID' => $journalEntryID, 'journalEntryCode' => $journalEntryCode);
        return array('status' => $status, 'msg' => $msg, 'data' => $data);
    }

    /*
     * This function is used to validate the journal entry account status.
     * @param journalentry acounts
     */
    public function checkJournalEntryAccountStatus($journalEntryAccounts)
    {
        $inActiveAccountsArray = Array();
        foreach ($journalEntryAccounts as $key => $value) {
            $res = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($value['financeAccountsID'])->current();
            if ($res['financeAccountStatusID'] == 2) {
                $inActiveAccountsArray[] = $res['financeAccountsCode'].'_'.$res['financeAccountsName'];
            }
        }

        $inActiveAccounts = implode(",",$inActiveAccountsArray);

        if (!empty($inActiveAccountsArray)) {
            return ['status' => false, 'msg' => $inActiveAccounts];
        } else {
            return ['status' => true];
        }
    }


    public function checkBudgetForAccounts($journalEntryAccounts, $journalEntryDate)
    {
        $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();
        foreach ($fiscalPeriod as $key => $value) {
            if($value['fiscalPeriodStatusID'] == 14){
                if(($value['fiscalPeriodStartDate'] <= $journalEntryDate && $journalEntryDate <= $value['fiscalPeriodEndDate'])){
                    if (is_null($value['fiscalPeriodParentID'])) {
                        $searchBudgetRes = $this->CommonTable('Accounting\Model\BudgetTable')->getBudgetByFiscalPeriodID($value['fiscalPeriodID'])->current();                        
                        if ($searchBudgetRes) {
                            if ($searchBudgetRes['dimension'] == null && $searchBudgetRes['dimensionValue'] == null) {
                                if ($searchBudgetRes['interval'] == "monthly") {
                                    return $this->checkMonthlyBudget($value, $searchBudgetRes['budgetId'],$journalEntryAccounts, $journalEntryDate);
                                } else {
                                    return $this->checkAnnualBudget($value, $searchBudgetRes['budgetId'],$journalEntryAccounts, $journalEntryDate);
                                }                           
                            } else {
                                return ['status' => true];
                            }
                        } else {
                            return ['status' => true];
                        }
                    }
                }
            }
        }
    }


    public function checkBudgetForAccountsByDimension($journalEntryAccounts, $journalEntryDate, $dimensionData)
    {
        $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();
        foreach ($fiscalPeriod as $key => $value) {
            if($value['fiscalPeriodStatusID'] == 14){
                if(($value['fiscalPeriodStartDate'] <= $journalEntryDate && $journalEntryDate <= $value['fiscalPeriodEndDate'])){
                    if (is_null($value['fiscalPeriodParentID'])) {
                        $searchBudgetRes = $this->CommonTable('Accounting\Model\BudgetTable')->getBudgetByFiscalPeriodID($value['fiscalPeriodID'])->current();                        
                        if ($searchBudgetRes) {
                            if ($searchBudgetRes['dimension'] != null && $searchBudgetRes['dimensionValue'] != null) {
                                if ($dimensionData['dimensionTypeId'] == $searchBudgetRes['dimension'] && $dimensionData['dimensionValueId'] == $searchBudgetRes['dimensionValue']) {
                                    if ($searchBudgetRes['interval'] == "monthly") {
                                        return $this->checkMonthlyBudget($value, $searchBudgetRes['budgetId'],$journalEntryAccounts, $journalEntryDate, $$searchBudgetRes['dimension'], $searchBudgetRes['dimensionValue']);
                                    } else {
                                        return $this->checkAnnualBudget($value, $searchBudgetRes['budgetId'],$journalEntryAccounts, $journalEntryDate, $$searchBudgetRes['dimension'], $searchBudgetRes['dimensionValue']);
                                    }    
                                } else {
                                    return ['status' => true];
                                }
                            } else {
                                return ['status' => true];    
                            }
                        } else {
                            return ['status' => true];
                        }
                    }
                }
            }
        }
    }

    public function checkMonthlyBudget($parentFiscalPeriod, $budgetID ,$journalEntryAccounts, $journalEntryDate, $dimensionType = null, $dimensionValue = null) 
    {
        $budgetExceedAcIDs = [];
        //get child fiscal periods
        $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getFiscalPeriodChildDataByFiscalPeriodParentID($parentFiscalPeriod['fiscalPeriodID']);
        if (count($fiscalPeriod) == 0) {
            foreach ($journalEntryAccounts as $val) {
                $checkBudgetValue = $this->CommonTable('Accounting\Model\BudgetDetailsTable')->getBudgetDetailsByFiscalPeriodIDAndFinanceAcID($parentFiscalPeriod['fiscalPeriodID'], $val['financeAccountsID'], $budgetID)->current();   
                if ($checkBudgetValue) {
                    $currentAccValue = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRangeForBudget($val['financeAccountsID'],$parentFiscalPeriod['fiscalPeriodStartDate'],$parentFiscalPeriod['fiscalPeriodEndDate'], false, $dimensionType, $dimensionValue)->current();

                    if ($currentAccValue) {
                        $currentDebit = $currentAccValue['totalDebit'];                        
                        $currentCredit = $currentAccValue['totalCredit'];                        
                         if ($checkBudgetValue['budgetValue'] > 0) {
                            $newValue = $currentDebit + $val['journalEntryAccountsDebitAmount'];
                            if ($newValue > floatval($checkBudgetValue['budgetValue'])) {
                                $budgetExceedAcIDs[] = $val['financeAccountsID'];
                            }
                        } else {
                            $newValue = $currentCredit + $val['journalEntryAccountsCreditAmount'];
                            if ($newValue > abs(floatval($checkBudgetValue['budgetValue']))) {
                                $budgetExceedAcIDs[] = $val['financeAccountsID'];
                            }
                        }         
                    } else {
                        $currentAccValue = 0; 
                        if ($checkBudgetValue['budgetValue'] > 0) {
                            $newValue = $currentAccValue + $val['journalEntryAccountsDebitAmount'];
                            if ($newValue > floatval($checkBudgetValue['budgetValue'])) {
                                $budgetExceedAcIDs[] = $val['financeAccountsID'];
                            }
                        } else {
                            $newValue = $currentAccValue + $val['journalEntryAccountsCreditAmount'];
                            if ($newValue > abs(floatval($checkBudgetValue['budgetValue']))) {
                                $budgetExceedAcIDs[] = $val['financeAccountsID'];
                            }
                        }                                
                    }
                }
            }
        } else {
            foreach ($fiscalPeriod as $key => $value) {
                if($value['fiscalPeriodStatusID'] == 14){
                    if(($value['fiscalPeriodStartDate'] <= $journalEntryDate && $journalEntryDate <= $value['fiscalPeriodEndDate'])){
                        $fiscalPeriodID = $value['fiscalPeriodID'];
                        foreach ($journalEntryAccounts as $val) {
                            $checkBudgetValue = $this->CommonTable('Accounting\Model\BudgetDetailsTable')->getBudgetDetailsByFiscalPeriodIDAndFinanceAcID($fiscalPeriodID, $val['financeAccountsID'], $budgetID)->current();   
                            if ($checkBudgetValue) {
                                $currentAccValue = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRangeForBudget($val['financeAccountsID'],$value['fiscalPeriodStartDate'],$value['fiscalPeriodEndDate'], false, $dimensionType, $dimensionValue)->current();
                                if ($currentAccValue) {
                                    $currentDebit = $currentAccValue['totalDebit'];                        
                                    $currentCredit = $currentAccValue['totalCredit'];                        
                                     if ($checkBudgetValue['budgetValue'] > 0) {
                                        $newValue = $currentDebit + $val['journalEntryAccountsDebitAmount'];
                                        if ($newValue > floatval($checkBudgetValue['budgetValue'])) {
                                            $budgetExceedAcIDs[] = $val['financeAccountsID'];
                                        }
                                    } else {
                                        $newValue = $currentCredit + $val['journalEntryAccountsCreditAmount'];
                                        if ($newValue > abs(floatval($checkBudgetValue['budgetValue']))) {
                                            $budgetExceedAcIDs[] = $val['financeAccountsID'];
                                        }
                                    }  
                                } else {
                                    $currentAccValue = 0; 
                                    if ($checkBudgetValue['budgetValue'] > 0) {
                                        $newValue = $currentAccValue + $val['journalEntryAccountsDebitAmount'];
                                        if ($newValue > floatval($checkBudgetValue['budgetValue'])) {
                                            $budgetExceedAcIDs[] = $val['financeAccountsID'];
                                        }
                                    } else {
                                        $newValue = $currentAccValue + $val['journalEntryAccountsCreditAmount'];
                                        if ($newValue > abs(floatval($checkBudgetValue['budgetValue']))) {
                                            $budgetExceedAcIDs[] = $val['financeAccountsID'];
                                        }
                                    }                                
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!empty($budgetExceedAcIDs)) {
            return ['status' => false, 'data' => $budgetExceedAcIDs];
        } else {
            return ['status' => true];
        }

    }

    public function checkAnnualBudget($parentFiscalPeriod, $budgetID ,$journalEntryAccounts, $journalEntryDate, $dimensionType = null, $dimensionValue = null)
    {
        $budgetExceedAcIDs = [];
        foreach ($journalEntryAccounts as $val) {
            $checkBudgetValue = $this->CommonTable('Accounting\Model\BudgetDetailsTable')->getBudgetDetailsByFiscalPeriodIDAndFinanceAcID($parentFiscalPeriod['fiscalPeriodID'], $val['financeAccountsID'], $budgetID)->current();   
            if ($checkBudgetValue) {
                $currentAccValue = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByAccountIDAndDateRangeForBudget($val['financeAccountsID'],$parentFiscalPeriod['fiscalPeriodStartDate'],$parentFiscalPeriod['fiscalPeriodEndDate'], false, $dimensionType, $dimensionValue)->current();
                if ($currentAccValue) {
                    $currentDebit = $currentAccValue['totalDebit'];                        
                    $currentCredit = $currentAccValue['totalCredit'];                        
                     if ($checkBudgetValue['budgetValue'] > 0) {
                        $newValue = $currentDebit + $val['journalEntryAccountsDebitAmount'];
                        if ($newValue > floatval($checkBudgetValue['budgetValue'])) {
                            $budgetExceedAcIDs[] = $val['financeAccountsID'];
                        }
                    } else {
                        $newValue = $currentCredit + $val['journalEntryAccountsCreditAmount'];
                        if ($newValue > abs(floatval($checkBudgetValue['budgetValue']))) {
                            $budgetExceedAcIDs[] = $val['financeAccountsID'];
                        }
                    }  
                } else {
                    $currentAccValue = 0; 
                    if ($checkBudgetValue['budgetValue'] > 0) {
                        $newValue = $currentAccValue + $val['journalEntryAccountsDebitAmount'];
                        if ($newValue > floatval($checkBudgetValue['budgetValue'])) {
                            $budgetExceedAcIDs[] = $val['financeAccountsID'];
                        }
                    } else {
                        $newValue = $currentAccValue + $val['journalEntryAccountsCreditAmount'];
                        if ($newValue > abs(floatval($checkBudgetValue['budgetValue']))) {
                            $budgetExceedAcIDs[] = $val['financeAccountsID'];
                        }
                    }                                
                }
            }
        }

        if (!empty($budgetExceedAcIDs)) {
            return ['status' => false, 'data' => $budgetExceedAcIDs];
        } else {
            return ['status' => true];
        }
    }


    public function deleteJournalEntry($journalEntryID)
    {
        $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryID($journalEntryID);
        $journalEntryTypeID = $journalEntries['journalEntryTypeID'];

        $journalEntryApproversData = iterator_to_array($this->CommonTable('Accounting\Model\JournalEntryTypeApproversTable')->getJournalEntryTypeApproversByJournalEntryTypeID($journalEntryTypeID));

        $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryID($journalEntryID);
        $journalEntryCode =  $journalEntries['journalEntryCode'];
        $token = $journalEntries['journalEntryHashValue'];
        $entityID = $journalEntries['entityID'];

        if(count($journalEntryApproversData) > 0){

            $journalEntryDeleteApproversData = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->getJournalEntryApproversByJournalEntryIDAndType($journalEntryID, 'Delete');
            $rejected = false;
            if(count($journalEntryDeleteApproversData) > 0){
                $status = false;
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE_APPROVE_ALREDY_SENT',array($journalEntryCode));
                return array('status' => $status, 'msg' =>$msg);
            }else{
                $rejected = true;
                $saveJEAPPROVERS = $this->saveJournalEntryApprovers($journalEntryApproversData, $journalEntryID, 'Delete');
                if(!$saveJEAPPROVERS){
                    $status = false;
                    $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE');
                    return array('status' => $status, 'msg' =>$msg);
                }else{
                    $status = true;
                    $msg = $this->getMessage('SUCC_JOURNAL_ENTRY_DELETE_APPROVE_SENT',array($journalEntryCode));
                    return array('status' => $status, 'msg' =>$msg);
                }
            }
            if($rejected){
                $emailStatus = $this->sendApproversEmail($journalEntryID, $journalEntryCode, $journalEntryApproversData, $token, "Delete");
            }

        }else{

            $financeStatus = $this->downgrateFinanceAccountsAmounts($journalEntryID);
            if(!$financeStatus){
                $status = false;
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE',array($journalEntryCode));
                return array('status' => $status, 'msg' =>$msg);
            }

            $status = $this->updateDeleteInfoEntity($entityID);
            $journalEntryReveresData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryByJournalEntryReverseRefID($journalEntryID);
            if($status && count($journalEntryReveresData) > 0){
                $JEReverseRefData = $journalEntryReveresData->current();
                $JERJournalEntryID = $JEReverseRefData['journalEntryID'];

                $financeStatus = $this->downgrateFinanceAccountsAmounts($JERJournalEntryID);
                if(!$financeStatus){
                    $status = false;
                    $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE',array($journalEntryCode));
                    return array('status' => $status, 'msg' =>$msg);
                }

                $status = $this->updateDeleteInfoEntity($JEReverseRefData['entityID']);
            }

            if ($status) {
                $status = true;
                $msg = $this->getMessage('SUCC_JOURNAL_ENTRY_DELETE');
                return array('status' => $status, 'msg' =>$msg);
            } else {
                $status = false;
                $msg = $this->getMessage('ERR_JOURNAL_ENTRY_DELETE');
                return array('status' => $status, 'msg' =>$msg);
            }
        }
    }

    protected function downgrateFinanceAccountsAmounts($journalEntryID)
    {
        $journalEntryAccountsData = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);
        foreach ($journalEntryAccountsData as $key => $JEAccounts) {
            $financeAccountsID = $JEAccounts['financeAccountsID'];

            $financeAccountsValues = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($financeAccountsID);
            $financeCreditAmount = $financeAccountsValues['financeAccountsCreditAmount'] - $JEAccounts['journalEntryAccountsCreditAmount'];
            $financeDebitAmount = $financeAccountsValues['financeAccountsDebitAmount'] - $JEAccounts['journalEntryAccountsDebitAmount'];

            $financeAccountsData = array(
                'financeAccountsCreditAmount' => $financeCreditAmount,
                'financeAccountsDebitAmount' => $financeDebitAmount,
                );
            $financeStaus = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->updateFinanceAccounts($financeAccountsData, $financeAccountsID);

            if(!$financeStaus){
                return false;
            }
        }
        return true;
    }

    protected function checkJournalEntryTypeApproversExists($journalEntryAccounts, $journalEntryTypeID)
    {
        $creditamount = 0;
        foreach ($journalEntryAccounts as $key => $value) {
            $creditamount+= $value['journalEntryAccountsCreditAmount'];
        }

        $JETApprovers = $this->CommonTable('Accounting\Model\JournalEntryTypeApproversTable')->getJournalEntryTypeApproversByJournalEntryTypeID($journalEntryTypeID);

        $journalEntryApprovers = [];
        $journalEntryApproversExist = false;
        if(count($JETApprovers) > 0){
            foreach ($JETApprovers as $key => $value) {
                $checkApprover = false;
                if($value['journalEntryTypeApproversMinAmount'] == 0 && $value['journalEntryTypeApproversMaxAmount'] == 0 ){
                    $checkApprover = true;
                }else if($value['journalEntryTypeApproversMinAmount'] == 0){
                    if( $creditamount <= $value['journalEntryTypeApproversMaxAmount'] ){
                        $checkApprover = true;
                    }
                }else if($value['journalEntryTypeApproversMaxAmount'] == 0){
                    if( $value['journalEntryTypeApproversMinAmount'] <= $creditamount ){
                        $checkApprover = true;
                    }
                }else if( $value['journalEntryTypeApproversMinAmount'] <= $creditamount && $creditamount <= $value['journalEntryTypeApproversMaxAmount']){
                    $checkApprover = true;
                }

                if($checkApprover){
                    $journalEntryApprovers[$value['employeeID']] = $value;
                    $journalEntryApproversExist = true;
                }
            }
        }
        return array('journalEntryApproversExist' => $journalEntryApproversExist, 'journalEntryApprovers'=> $journalEntryApprovers);
    }

    protected function saveJournalEntryAccounts($journalEntryAccounts, $journalEntryID, $journalEntryStatusID)
    {
        $creditamount = 0;
        $debitamount = 0;
        foreach ($journalEntryAccounts as $key => $value) {
            $debitamount+= $value['journalEntryAccountsDebitAmount'];
            $creditamount+= $value['journalEntryAccountsCreditAmount'];
            $value['journalEntryID'] = $journalEntryID;

            $journalEntryAccountsForm = $journalEntryAccountsForm = new JournalEntryAccountsForm('CreateJournalEntryAccounts',
                array(
                    'financeAccounts' => array(''),
                    'financeGroups' => array(''),
                ));
            //set journel entry Accounts data and inputfilers for vallidation
            $journalEntryAccounts= new JournalEntryAccounts();
            $journalEntryAccountsForm->setInputFilter($journalEntryAccounts->getInputFilter());
            $journalEntryAccountsForm->setData($value);

            if (!$journalEntryAccountsForm->isValid()) {
                return false;
            }

            $journalEntryAccounts->exchangeArray($value);
            $journalEntryAccountsID = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->saveJournalEntryAccounts($journalEntryAccounts);

            if(!$journalEntryAccountsID){
                return false;
            }

            if($journalEntryStatusID != 7){
            //update finance account balances
                $financeAccountsID = $value['financeAccountsID'];
                $financeAccountsValues = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getAccountsDataByAccountsID($financeAccountsID);
                $financeCreditAmount = $financeAccountsValues['financeAccountsCreditAmount'] + $value['journalEntryAccountsCreditAmount'];
                $financeDebitAmount = $financeAccountsValues['financeAccountsDebitAmount'] + $value['journalEntryAccountsDebitAmount'];

                $financeAccountsData = array(
                    'financeAccountsCreditAmount' => $financeCreditAmount,
                    'financeAccountsDebitAmount' => $financeDebitAmount,
                    );
                $financeStaus = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->updateFinanceAccounts($financeAccountsData, $financeAccountsID);

                if(!$financeStaus){
                    return false;
                }
            }
            $eventParameter = ['glAccountID' => $value['financeAccountsID']];
            $this->getEventManager()->trigger('bankBalanceUpdated', $this, $eventParameter);
        }
        if(round($creditamount, 0) != round($debitamount, 0)){
            return false;
        }

        return true;
    }

     protected function saveJournalEntryApprovers($journalEntryApprovers, $journalEntryID, $journalEntryApproversType)
    {
        foreach ($journalEntryApprovers as $key => $value) {

            $data = array(
                'journalEntryID' => $journalEntryID,
                'journalEntryTypeApproversID' => $value['journalEntryTypeApproversID'],
                'journalEntryApproversType' => $journalEntryApproversType,
                'journalEntryApproversStatus' => 'Pending',
                'journalEntryApproversExpire' => 0,
            );
            $journalEntryApprovers = new JournalEntryApprovers();
            $journalEntryApprovers->exchangeArray($data);
            $journalEntryApproversID = $this->CommonTable('Accounting\Model\JournalEntryApproversTable')->saveJournalEntryApprovers($journalEntryApprovers);

            if(!$journalEntryApproversID){
                return false;
            }

        }
        return true;
    }

     protected function sendApproversEmail($journalEntryID, $journalEntryCode, $journalEntryApprovers, $token, $JournalEntryApproversType)
    {
        $companyDetails = (array) $this->CommonTable('CompanyTable')->fetchAll()->current();
        $checkSaved = true;
        foreach ($journalEntryApprovers as $journalEntryApprover) {
            $JETAPPID = $journalEntryApprover['journalEntryTypeApproversID'];
            $approveUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/journal-entries-api/approve/?journalEntryID=' . $journalEntryID . '&action=approve&token=' . $token.'&journalEntryTypeApproverID='.$JETAPPID.'&actionType='.$JournalEntryApproversType;
            $rejectUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/journal-entries-api/approve/?journalEntryID=' . $journalEntryID . '&action=reject&token=' . $token.'&journalEntryTypeApproverID='.$JETAPPID.'&actionType='.$JournalEntryApproversType;
            $approveJEEmailView = new ViewModel(array(
                'name' => $journalEntryApprover['employeeFirstName'] . ' ' . $journalEntryApprover['employeeSecondName'],
                'journalEntryCode' => $journalEntryCode,
                'approveLink' => $approveUrl,
                'rejectLink' => $rejectUrl,
            ));
            if($JournalEntryApproversType == 'Create'){
                $approveJEEmailView->setTemplate('/core/email/journal-entry-approve');
                $msg = "Request for Approve - Journal Entry - $journalEntryCode";
            }else{
                $approveJEEmailView->setTemplate('/core/email/journal-entry-delete-approve');
                $msg = "Request for Approve - Journal Entry Delete - $journalEntryCode";
            }
            $renderedEmailHtml = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($approveJEEmailView);
            $documentType = 'Journal Entry';
            $status = $this->sendEmail($journalEntryApprover['employeeEmail'], $msg, $renderedEmailHtml, '', $companyDetails['companyName'], '');
            if(!$status){
                $checkSaved = false;
            }
        }
        return $checkSaved;
    }

    /**
     * Calculate product unit tax
     * @param string $unitPrice
     * @param int $productId
     * @return string
     */
    public function calculateProductUnitTax($unitPrice, $productId)
    {
        $productTax = 0;
        $productTaxData = $this->CommonTable("Inventory\Model\ProductTaxTable")->getProTaxDetailsByProductID($productId);
        foreach ($productTaxData as $tax) {
            if ($tax['state'] == 1) {
                if ($tax['taxType'] == 'v') {
                    $vTaxValue = number_format(($unitPrice * $tax['taxPrecentage'] / 100), 2, '.', '');
                    $productTax += $vTaxValue;
                } else {
                    $simpleTaxAmount = 0;
                    $simpleTaxes = $this->CommonTable('Settings\Model\TaxCompoundTable')->getSimpleTaxByCompoundTaxIdAndProductId($tax['taxID'], $productId);
                    foreach ($simpleTaxes as $sT) {
                        if ($sT['state'] == 1) {
                            $simpleTaxAmount = $simpleTaxAmount + number_format(($unitPrice * $sT['taxPrecentage'] / 100), 2, '.', '');
                        }
                    }
                    $compuntTaxAmount = $simpleTaxAmount + number_format(($unitPrice * $tax['taxPrecentage'] / 100), 2, '.', ',');
                    $productTax = $productTax + $compuntTaxAmount;
                }
            }
        }
        return $productTax;
    }

    protected function getJournalEntryDataByJournalEntryID($journalEntryID)
    {
        $journalEntry = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByJournalEntryID($journalEntryID);

        $journalEntries = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataForViewByJournalEntryCode($journalEntry['journalEntryCode']);

        $journalEntry = [];
        $journalEntryAccounts = [];

        foreach ($journalEntries as $key => $value) {
            if(!isset($JournalEntry['journalEntryCode'])){
                $journalEntry['journalEntryID'] = $value['journalEntryID'];
                $journalEntry['journalEntryCode'] = $value['journalEntryCode'];
                $journalEntry['journalEntryDate'] = $value['journalEntryDate'];
                $journalEntry['journalEntryIsReverse'] = $value['journalEntryIsReverse'];
                $journalEntry['journalEntryComment'] = $value['journalEntryComment'];
            }
            $journalEntryAccounts[$value['journalEntryAccountsID']]['financeAccountsCode']=$value['financeAccountsCode'];
            $journalEntryAccounts[$value['journalEntryAccountsID']]['financeAccountsName']=$value['financeAccountsName'];
            $journalEntryAccounts[$value['journalEntryAccountsID']]['journalEntryAccountsDebitAmount']=$value['journalEntryAccountsDebitAmount'];
            $journalEntryAccounts[$value['journalEntryAccountsID']]['journalEntryAccountsCreditAmount']=$value['journalEntryAccountsCreditAmount'];
            $journalEntryAccounts[$value['journalEntryAccountsID']]['journalEntryAccountsMemo']=$value['journalEntryAccountsMemo'];
        }

        $journalEntry['journalEntryAccounts'] =$journalEntryAccounts;

        return array('JEDATA' => $journalEntry);
    }

    /**
     * Calculate product single tax values
     * @param array tax
     * @return string
     */
    public function caluculateProductSingleTax($unitPrice, $tax)
    {
        if ($tax['state'] == 1) {
            if ($tax['taxType'] == 'v') {
                $vTaxValue = number_format(($unitPrice * $tax['taxPrecentage'] / 100), 2, '.', '');
                $productTax += $vTaxValue;
            } else {
                $simpleTaxAmount = 0;
                $simpleTaxes = $this->CommonTable('Settings\Model\TaxCompoundTable')->getSimpleTaxByCompoundTaxIdAndProductId($tax['taxID'], $productId);
                foreach ($simpleTaxes as $sT) {
                    if ($sT['state'] == 1) {
                        $simpleTaxAmount = $simpleTaxAmount + number_format(($unitPrice * $sT['taxPrecentage'] / 100), 2, '.', '');
                    }
                }
                $compuntTaxAmount = $simpleTaxAmount + number_format(($unitPrice * $tax['taxPrecentage'] / 100), 2, '.', ',');
                $productTax = $productTax + $compuntTaxAmount;
            }
        }

        return $productTax;
    }

    protected function getReportJobCallbackData($data, $reportType)
    {
        return [
            'url' => Util::getBaseUrl(),
            'service' => $data['service'],
            'category' => $data['category'],
            'method' => $data[$reportType],
            'requestUserId' => $this->userID,
            'requestLocationId' => $this->user_session->userActiveLocation['locationID'],
            'requestDateTime' => $this->getGMTDateTime(),
            'companyCurrencySymbol' => $this->companyCurrencySymbol
        ];
    }

    public function getCompanyName()
    {
        $serverName = $this->getServiceLocator()->get('Config')['servername'];
        $sitePath = $_SERVER['HTTP_HOST']; // getRequest cannot be used because of context restrictions ($this)

        $companyName = '';
        if (strpos($sitePath, $serverName) > 0) {
// get unique company name from subdomain
            $companyName = end(explode('.', trim(str_replace($serverName, '', $sitePath), '.')));
        } else if ($sitePath == $serverName) {
// no subdomains are available (local enviroment)
            $companyName = 'ezbiz';
        } else {
            die('config.sitepath not set');
        }

        return $companyName;
    }

    public function getPdfGenerationCommand($options, $path)
    {
        $header = $path . '/header.html';
        $body = $path . '/body.html';
        $footer = $path . '/footer.html';
        $pdfFileWithPath = $path . '-print.pdf';

        $command = 'wkhtmltopdf.sh';

        $command .= (($options['isPortrait'])) ? ' --orientation Portrait' : ' --orientation Landscape'; // set orientation

        if ($options['zoomFactor'] != 0) { // zoom factor

            $command .= ' --zoom ' . $options['zoomFactor'];

        }

        $command .= ' --margin-top ' . $options['marginTop'] . ' --margin-bottom ' . $options['marginBottom'] . ' --margin-left ' . $options['marginLeft'] . ' --margin-right ' . $options['marginRight']; // set margins

        $command .= ($options['isCustomPage']) ? ' --page-height ' . $options['pageHeight'] . ' --page-width ' . $options['pageWidth'] : ' --page-size ' . $options['pageSize']; // set page size

        //$command .= ($options['grayScale']) ? ' -g' : ''; // PDF will be generated in grayscale

        if ($options['repeatHeader'] && $options['repeatFooter']) {

            $command .= ' --header-html ' . $header . ' --header-spacing ' . $options['headerSpacing']; // adds a html header

            $command .= ' --footer-html ' . $footer . ' --footer-spacing ' . $options['footerSpacing']; // adds a html footer

        } else if ($options['repeatHeader'] && !$options['repeatFooter']) {

            $command .= ' --header-html ' . $header . ' --header-spacing ' . $options['headerSpacing']; // adds a html header

            $command .= ' --footer-html ' . $footer . ' --footer-spacing ' . $options['footerSpacing']; // adds a html footer

        } else if (!$options['repeatHeader'] && $options['repeatFooter']) {

            $command .= ' --footer-html ' . $footer . ' --footer-spacing ' . $options['footerSpacing']; // adds a html footer

        } else {

            $command .= ' --footer-html ' . $footer . ' --footer-spacing ' . $options['footerSpacing']; // adds a html footer

        }

        $command .= ' ' . $body . ' ' . $pdfFileWithPath;

        return $command;
    }

    public function rendererDocumentDataTable($pathToDocument, $documentData)
    {
        $view = new ViewModel($documentData);
        $view->setTemplate($pathToDocument);
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    public function generateDocumentPdf($itemID, $documentType, $documentData, $templateID)
    {
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

        $result = $this->CommonTable('Settings\Model\TemplateTable')->isAdvancedTemplateEnabled($templateID);
        if (!filter_var($result, FILTER_VALIDATE_BOOLEAN)) {
            return new ViewModel();
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $itemID, $templateID);

        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);

        // get file name
        $filenameParts = str_split($filename, 2);
        $filenameWithoutPrefix = substr($filename, 4);
        $path = $tpl->getDocumentPath($this->templateDetails['documentTypeName']) . "/" . $filenameParts[0] . "/" . $filenameParts[1] . '/' . $filenameWithoutPrefix;

        $tpl->renderTemplateByID($templateID, $documentData, $filename, true);

        $options = (!empty($this->templateDetails['templateAdvancedOptions'])) ? json_decode($this->templateDetails['templateAdvancedOptions'], true) : [];

        $command = $this->getPdfGenerationCommand($options, $path);

        error_log($command);

        exec($command);

        $browserPath = str_replace(getcwd() . '/public/', '', $path) . '-print.pdf';
        header('Location: /' . $browserPath);

        exit();
    }

    public function getBaseUrl()
    {
        return 'http' . (empty($_SERVER['HTTPS']) ? '' : 's') . '://' . $_SERVER['SERVER_NAME'];
    }

    /**
     * get product default accounts
     * @return array of default accounts
     */
    public function getProductDefaultAccounts()
    {
        $defaultAcc = [];

        // set product default accounts
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->user_session->useAccounting) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $defaultAcc['productSales'] = $productSalesAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $defaultAcc['productInventory'] = $productInventoryAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $defaultAcc['productCOGSA'] = $productCOGSAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $defaultAcc['productAdjusment'] = $productAdjusmentAccountID;
            }
        }

        return [
            'product_accounts' => $defaultAcc
        ];
    }

    /**
    * this function use to validate enterd user name and password and check it is admin user or not
    * @param string password
    * @param string userName
    * return boolean
    **/
    public function validateAdminUser($password, $userName)
    {
        // get user details by given username
        $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($userName);
            if ($user) {
                if ($user->roleID == '1') {
                    $passwordData = explode(':', $user->userPassword);
                    $storedPassword = $passwordData[0];
                    $checkPassword = md5($password . $passwordData[1]);

                    if ($storedPassword == $checkPassword) {
                        return ['status' => true, 'msg' => 'success'];
                    } else {
                        return ['status' => false, 'msg' => $this->getMessage('ERR_USER_CREDE')];
                    }
                } else {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_ADMIN_USER_CREDE')];
                }
            } else {
                return ['status' => false, 'msg' => $this->getMessage('ERR_NO_USER')];

            }
    }

    protected function createEntityForRestApi($userID)
    {
//        $currentTime = date('Y-m-d H:i:s');
        $currentTime = $this->getGMTDateTime('Y-m-d H:i:s');
        $entitydata = array(
            'createdBy' => $userID,
            'createdTimeStamp' => $currentTime,
            'updatedBy' => $userID,
            'updatedTimeStamp' => $currentTime
        );
        $entityModel = new Entity();
        $entityModel->exchangeArray($entitydata);
        $entityID = $this->CommonTable('Core\Model\EntityTable')->saveEntity($entityModel);
        return $entityID;
    }

    /**
     * This function is used to save journal entry dimension 
     * @param $dimension data, $journal entry id
     * @return $resultset
     */
    public function saveDimensionsForJournalEntry($dimensionData, $journalEntryID, $ignoreBudgetLimit, $journalEntryAccounts, $journalEntryDate)
    {
        if(!is_array($dimensionData)) {
            $dimensionData = (array) $dimensionData;
        }
        foreach ($dimensionData as $value) {
            if(!is_array($value)) {
                $value = (array) $value;
            }
            
            $data = array(
                'journalEntryID' => $journalEntryID,
                'dimensionType' => $value['dimensionTypeId'],
                'dimensionValueID' => $value['dimensionValueId'],
            );

            $dimensionData = new JournalEntryDimension();
            $dimensionData->exchangeArray($data);
            $saveJEDimension = $this->CommonTable('Core\Model\JournalEntryDimensionTable')->saveJournalEntryDimension($dimensionData);

            if (!$saveJEDimension) {
                return ['status' => false, 'msg' => $this->getMessage('ERR_SAVE_JE_DIMENSION')];
            }

            $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
            $budgetConf = (object) $this->CommonTable('Accounting\Model\BudgetConfigurationTable')->fetchAll();
            
            if ($displaySettings->isBudgetEnabled == 1 && $ignoreBudgetLimit == "false") {
                $budgetExceedMsg = "";
                $budgetCheck = $this->checkBudgetForAccountsByDimension($journalEntryAccounts, $journalEntryDate, $value);
                if (!$budgetCheck['status']) {
                    if ($budgetConf->isNotificationEnabled == "1") {
                        foreach ($budgetCheck['data'] as $val) {
                            $financeAcData = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getFinanceAccountBasicByAccountsID($val)->current();
                            $budgetExceedMsg = ($budgetExceedMsg == "") ? $financeAcData['financeAccountsCode'].'-'.$financeAcData['financeAccountsName'] : $budgetExceedMsg.' ,'.$financeAcData['financeAccountsCode'].'-'.$financeAcData['financeAccountsName'];
                        }

                        if ($budgetConf->isBlockedEnabled == "1") {
                            $bData = "BlockBudgetLimit";                        
                        } else {
                            $bData = "NotifyBudgetLimit";                        
                        }

                        return array(
                            'status' => false,
                            'data' => $bData,
                            'msg' => $this->getMessage('ERR_JOURNAL_ENTRY_ACCOUNT_BUDGET_EXCEEDED_WITH_DIMENSION', [$budgetExceedMsg])
                            );
                    }
                }
            }
        }

        return ['status' => true, 'msg' => 'success'];
    }


    /**
     * This function is used update document status in journal entry table 
     * @param $documentID, $status
     * @return $resultSet
     */
    public function updateDocumentStatusInJournalEntry($documentID,$documentTypeID, $status)
    {
        $jdata = array(
            'documentStatus' => $status,
        );
        
        $updateStatus = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntryDocumentStatus($jdata, $documentTypeID,$documentID);
         
        if (!$updateStatus) {
            return ['status' => false, 'msg' => $this->getMessage('ERR_JOURNAL_ENTRY_UPDATE_DOCUMENT_STATUS')];
        }

        return ['status' => true, 'msg' => 'success'];
    }

    /**
     * This function is used to save document related files
     */
    public function storeFilesAction()
    {
        $request = $this->getRequest(); //create http request
        $post = $request->getPost();
        $files = $_FILES['files'];

        if ($post['updateFlag']) {
            $previousData = $this->updateDocumentRelatedAttachments($post);
        }


        if ($post['documentID'] == "same") {
            $documentID = $post['editedDocumentID'];
        } else {
            $documentID = $post['documentID'];
        }

        if ($post['epttAttachemntFlag'] == "true") {
            $tempInvoiceData = $this->CommonTable('Invoice\Model\DraftInvoiceTable')->getTempInvoiceAttachemnt($post['draftInvoiceID']);
            if ($tempInvoiceData) {
                $data = array(
                    'documentID' => $documentID,
                    'documentTypeID' => $post['documentTypeID'],
                    'documentRealName' => "Booking vocher Detail.csv",
                    'documentName' => explode("/", $tempInvoiceData['attachment'])[3],
                );
                $docFile = new DocumentAttachementMapping();
                $docFile->exchangeArray($data);
                $insertedID = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->saveUploadFileMapData($docFile);
                if(!$insertedID) {
                    $this->status = false;
                    return $this->JSONRespond();
                }
            }
        }

        if (isset($_FILES['files']) && !empty($_FILES['files'])) {
            $no_files = count($_FILES["files"]['name']);
            for ($i = 0; $i < $no_files; $i++) {
                if ($request->isPost()) {
                    $realFileName = $_FILES['files']['name'][$i];
                    $fileName = md5(microtime() . rand(500000)) . "_" . $documentID."_".$post['documentTypeID']."_".str_replace(' ', '_', $_FILES['files']['name'][$i]);
                    $fileTempName = $_FILES['files']['tmp_name'][$i];
                    $path = $this->getCompanyDataFolder() . "/attachments";
                    // create attachements folder
                    if (!is_dir($path)) {
                        mkdir($path, 0777, true);
                    }
                    $filePath = $path . "/" . $fileName;
                    move_uploaded_file($fileTempName, $filePath); // upload the file in appropriate folder
                    
                    $data = array(
                        'documentID' => $documentID,
                        'documentTypeID' => $post['documentTypeID'],
                        'documentRealName' => $realFileName,
                        'documentName' => $fileName,
                    );
                    $docFile = new DocumentAttachementMapping();
                    $docFile->exchangeArray($data);
                    $insertedID = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->saveUploadFileMapData($docFile);
                    if(!$insertedID) {
                        $this->status = false;
                        return $this->JSONRespond();
                    }
                }
            }
        }

        if ($post['updateFlag']) {
            $companyName = $this->getSubdomain();
            $pathOfDoc = '/userfiles/' . md5($companyName) . "/attachments";

            $newUploadedData = [];
            $newAttachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($documentID, $post['documentTypeID']);
            foreach ($newAttachmentData as $value) {
                $temp = [];
                $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
                $temp['documentRealName'] = $value['documentRealName'];
                $temp['documentName'] = $value['documentName'];
                $temp['docLink'] = $pathOfDoc . "/" .$value['documentName'];
                $newUploadedData[$value['documentAttachemntMapID']] = $temp;
            }

            $newData = json_encode($newUploadedData);
            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("Attachemnts of ".$post['documentCode']." updated.");
        }

        $this->status = true;
        return $this->JSONRespond();
    }

    public function updateDocumentRelatedAttachments($postData) 
    {
        if ($postData->documentID == "same") {
            $documentID = $postData->editedDocumentID;
        } else {
            $documentID = $postData->documentID;
        }

        $uploadedAttachments = [];
        $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($postData->editedDocumentID, $postData->documentTypeID);
        $companyName = $this->getSubdomain();
        $path = '/userfiles/' . md5($companyName) . "/attachments";

        foreach ($attachmentData as $value) {
            $temp = [];
            $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
            $temp['documentRealName'] = $value['documentRealName'];
            $temp['documentName'] = $value['documentName'];
            $temp['docLink'] = $path . "/" .$value['documentName'];
            $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
        }

        $deletedAttachemntIds = explode(',',$postData->deletedAttachmentIds);

        foreach ($uploadedAttachments as $key => $value) {
            if(in_array($key, $deletedAttachemntIds)) {
                $deletedAttachments[$key] = $value;                
            } else {
                $remaingAttachemnts[$key] = $value;                
            }
        }

        if ($postData->documentID == "same") {
            $updateExistingFilesAsDeletd = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->updateDeletedAttachemnts($documentID, $postData->documentTypeID);
        }

        foreach ($remaingAttachemnts as $value) {
            $data = array(
                'documentID' => $documentID,
                'documentTypeID' => $postData->documentTypeID,
                'documentRealName' => $value['documentRealName'],
                'documentName' => $value['documentName'],
            );
            $docFile = new DocumentAttachementMapping();
            $docFile->exchangeArray($data);
            $insertedID = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->saveUploadFileMapData($docFile);
        }

        //set log details
        $previousData = json_encode($uploadedAttachments);
        return $previousData;
    }


    public function getDocumentRelatedAttachementAction()
    {
        $request = $this->getRequest(); //create http request
        if ($request->isPost()) {
            $post = $request->getPost();
            $files = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($post['documentID'],$post['documentTypeID']);
            $companyName = $this->getSubdomain();
            $path = '/userfiles/' . md5($companyName) . "/attachments";

            if(!empty($files)){
                $fileData = [];
                foreach ($files as $value) {
                    $temp['docName'] = $value['documentRealName'];
                    $temp['link'] = $path . "/" .$value['documentName'];

                    $fileData[] = $temp;
                }

                $this->data = $fileData;
                $this->status = true;
                return $this->JSONRespond();
            } else {
                $this->status = false;
                return $this->JSONRespond();
            }

        }
    }

    public function getDocumentRelatedLinkedCusPoAction()
    {
        $request = $this->getRequest(); 
        if ($request->isPost()) {
            $post = $request->getPost();

            $refCustomerOrderData = $this->CommonTable('Core\Model\DocumentReferenceTable')->getDocumentReferenceBySourceDocIDAndRefTypeDetails(3,$post['documentTypeID'], $post['documentID'])->current();

            $customerOrderData = $this->CommonTable('Invoice\Model\CustomerOrderTable')->retriveCustomerOrder($refCustomerOrderData['referenceDocumentId'])->current();

            $files = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($refCustomerOrderData['referenceDocumentId'],$post['documentTypeID']);
            $companyName = $this->getSubdomain();
            $path = '/userfiles/' . md5($companyName) . "/attachments";

            $fileData = [];
            foreach ($files as $value) {
                $temp['docName'] = $value['documentRealName'];
                $temp['link'] = $path . "/" .$value['documentName'];

                $fileData[] = $temp;
            }


            $dataSet = [
                'fileData' => $fileData,
                'customerOrderData' => $customerOrderData
            ];

            $this->data = $dataSet;
            $this->status = true;
            return $this->JSONRespond();
        }
    }

    /**
     * This message is used to send sms
     * @param $telephone number, $message
     */
    public function sendSms($telephoneNo, $message) 
    {
        $checkTele = !!preg_match('/^94\d{9}$/', "$telephoneNo");
        
        if (!$checkTele) {
            if (!!preg_match('/^\+94\d{9}$/', "$telephoneNo")) {
                $telephoneNo = ltrim($telephoneNo,'+');
            } else if (!!preg_match('/^0\d{9}$/', "$telephoneNo")) {
                $telephoneNo = ltrim($telephoneNo,'0');
                $telephoneNo = "94".$telephoneNo;
            } else if (!!preg_match('/\d{9}$/', "$telephoneNo")) {
                $telephoneNo = "94".$telephoneNo;
            } 
        }
        $checkTele = !!preg_match('/^94\d{9}$/', "$telephoneNo");
        
        if ($checkTele) {
            require_once('public/ESMSWS.php');
            $smsConfs = (object) $this->CommonTable('Settings\Model\SmsConfigurationTable')->fetchAll();
            
            if ($smsConfs) {
                // Check the service provider
                if((int) $smsConfs->serviceProvider == 1){
                    $session = createSession('',$smsConfs->userName,$smsConfs->password,'');
                    $session_validity = isSession($session);
                    if ($session_validity) {
                        $msgRes = sendMessages($session,$smsConfs->alias,$message,array($telephoneNo),0); 
                        closeSession($session);
                        return $msgRes;
                    }
                }else if((int) $smsConfs->serviceProvider == 2){
                    $sms = $this->getServiceLocator()->get('SmsService');
                    $respond = $sms->sendMessage($smsConfs, $telephoneNo, $message);
                    return $respond;
                }
            }
        }
        return false;
    }

    public function generateCSVFileForEPTTInvoice($reportName, $csvFile)
    {
        $uniqueName = $reportName . preg_replace("/[^0-9]/", "", microtime(true)) . uniqid();

        $docPath = $this->getCompanyDataFolder() . '/attachments';

        if (!is_dir($docPath)) {
            mkdir($docPath, 0777, true);
        }

        //string put in to csv file and save it in local hard in specific location
        file_put_contents($docPath . '/' . $uniqueName . '.csv', $csvFile);

        //get path where file has been saved in local machine
        $browserPath = str_replace(getcwd() . '/public/', '', $docPath) . '/' . $uniqueName . '.csv';


        return $browserPath;
    }
}

?>
