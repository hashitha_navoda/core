<?php

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * This class can be used to generate generic modal boxes for entity controllers
 * Eg: Delete confirmation box for Product
 */

namespace Core\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class ModalController extends AbstractActionController
{

    protected $viewModel;

    public function __construct() {
//        $this->viewModel->setTerminal(true);
    }
    
    /**
     * Returns a dialog box for deleting an item
     * @param string $entity Eg: "Product"
     * @param string $btn_id Eg: "delete-product-btn"
     * @return ViewModel View for modal dialog box
     */
    public function deleteDialog($entity = "Item", $btn_id = "delete-item-btn")
    {
        $this->viewModel = new ViewModel();
        
        $this->viewModel->setTemplate('core/modal/delete.phtml');
        $this->viewModel->entity = $entity;
        $this->viewModel->btn_id = $btn_id;
        
        return $this->viewModel;
    }
    
    
    /**
     * Returns a dialog box for activating an item
     * @param string $entity Eg: "Product"
     * @param string $btn_id Eg: "delete-product-btn"
     * @return ViewModel View for modal dialog box
     */
    public function activateDialog($entity = "Item", $btn_id = "active-item-btn")
    {
        $this->viewModel = new ViewModel();
        
        $this->viewModel->setTemplate('core/modal/statechange.phtml');
        $this->viewModel->entity = $entity;
        $this->viewModel->btn_id = $btn_id;
        $this->viewModel->state = 'Activate';

        return $this->viewModel;
    }
    
    
    /**
     * Returns a dialog box for deactivating an item
     * @param string $entity Eg: "Product"
     * @param string $btn_id Eg: "delete-product-btn"
     * @return ViewModel View for modal dialog box
     */
    public function deactivateDialog($entity = "Item", $btn_id = "deactive-item-btn")
    {
        $this->viewModel = new ViewModel();
        
        $this->viewModel->setTemplate('core/modal/statechange.phtml');
        $this->viewModel->entity = $entity;
        $this->viewModel->btn_id = $btn_id;
        $this->viewModel->state = 'Deactivate';
        
        return $this->viewModel;
    }

}