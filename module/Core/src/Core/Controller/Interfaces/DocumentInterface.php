<?php

namespace Core\Controller\Interfaces;

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 * Methods needed to implement a document view
 */
interface DocumentInterface
{

    public function documentAction();

    public function getDataForDocument($invoiceID);

    public function getDocumentDataTable($invoiceID, $documentSize);

    public function documentPreviewAction();
}
