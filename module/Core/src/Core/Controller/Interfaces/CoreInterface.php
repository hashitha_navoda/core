<?php

namespace Core\Controller\Interfaces;

interface CoreInterface
{

    public function indexAction();

    public function createAction();

    public function editAction();

    public function listAction();

    public function viewAction();
}
