<?php

namespace Core\Controller\RestAPI;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;


class CoreRestfullController extends AbstractRestfulController
{
    protected $entityTable;
    protected function getService($service)
    {
        return $this->getServiceLocator()->get($service);
    }

    /**
     * Return success message of type JSON
     *
     * @param mixed $data
     * @param  string|array $msg
     * @return JsonModel
     */
    protected function returnJsonSuccess($data = null, $msg = '')
    {
        $msg = (is_array($msg)) ? $this->getMessage($msg[0], $msg[1]) : $this->getMessage($msg);

        return new JsonModel(array(
            'status' => true,
            'msg' => $msg,
            'data' => $data,
        ));
    }

    /**
     * Return error message of type JSON
     *
     * @param  string|array $msg
     * @param mixed $data
     * @return JsonModel
     */
    protected function returnJsonError($msg, $data = null)
    {
        $msg = (is_array($msg)) ? $this->getMessage($msg[0], $msg[1]) : $this->getMessage($msg);

        return new JsonModel(array(
            'status' => false,
            'msg' => $msg,
            'data' => $data,
        ));
    }

    /**
     * Pass messages to classes
     *
     */
    public function getMessage($key, array $params = [])
    {

        $messages = $this->getServiceLocator()->get('config')['messages'];

        // if message is not defined in config, return as it is
        if (!isset($messages[$key])) {
            return $key;
        }

        return vsprintf($messages[$key], $params);
    }

    protected function beginTransaction()
    {
        if (!$this->entityTable) {
            $sm = $this->getServiceLocator();
            $this->entityTable = $sm->get('EntityTable');
        }
        $this->entityTable->beginTransaction();
        return true;
    }

    protected function rollback()
    {
        if (!$this->entityTable) {
            $sm = $this->getServiceLocator();
            $this->entityTable = $sm->get('EntityTable');
        }
        $this->entityTable->rollback();
        return true;
    }

    protected function commit()
    {
        if (!$this->entityTable) {
            $sm = $this->getServiceLocator();
            $this->entityTable = $sm->get('EntityTable');
        }
        $this->entityTable->commit();
        return true;
    }

}
