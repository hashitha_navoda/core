<?php

namespace Core\Controller\API;

use Zend\Mvc\Controller\AbstractRestfulController;

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */
class CoreController extends \Core\Controller\CoreController
{

    public function __construct()
    {
        parent::__construct();
    }

}

?>
