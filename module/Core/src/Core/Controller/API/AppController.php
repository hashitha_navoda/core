<?php

/**
 * Description of Ezbiz API Controller
 *
 * @author Damith Thamara <damith@thinkcube.com>
 */

namespace Core\Controller\API;

use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Core\Model\ReportQueue;
use Core\Model\Licence;
use User\Model\Emailconfig;

class AppController extends \Core\Controller\API\CoreController
{

    protected $user_session;
    private $locationID;
    private $licenceTable;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->allLocations = $this->user_session->userAllLocations;
        $this->locationID = $this->user_session->userActiveLocation['locationID'];
    }

    /**
     * function for the first time inserting licence
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function saveLicencesAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            // set default values get from config
            $config = $this->getServiceLocator()->get('config');
            $defaultLicenceCount = $config['licence']['licenceCount'];
            $defaultLicencePeriod = $config['licence']['licencePeriod'];
            $defaultLicenceType = $config['licence']['licenceType'];

            $licenceCount = !is_null($request->getPost('licenceCount')) ? $request->getPost('licenceCount') : $defaultLicenceCount;
            $licencePeriod = !is_null($request->getPost('licencePeriod')) ? $request->getPost('licencePeriod') : $defaultLicencePeriod;
            $userID = !is_null($request->getPost('userID')) ? $request->getPost('userID') : null;
            $licenceStartingDate = $this->getGMTDateTime();
            $licenceExpireDate = date('Y-m-d H:i:s', strtotime("+$licencePeriod day", strtotime($licenceStartingDate)));

            $data = array(
                'licenceName' => !is_null($request->getPost('licenceName')) ? $request->getPost('licenceName') : null,
                'licenceType' => !is_null($request->getPost('licenceType')) ? $request->getPost('licenceType') : $defaultLicenceType,
                'licenceStartingDate' => $licenceStartingDate,
                'licenceExpireDate' => $licenceExpireDate,
                'userID' => $userID,
                'licenceExpired' => 0
            );

//           insert super Admin licence
            $licence = new Licence();
            $licence->exchangeArray($data);
            $insertedIDs[] = $this->CommonTable('Core\Model\LicenceTable')->saveLicence($licence);
            $data['userID'] = null;

//          create empty licence for other users
            for ($i = 0; $i < $licenceCount - 1; $i++) {
                $licence = new Licence();
                $licence->exchangeArray($data);
                $insertedIDs[] = $this->CommonTable('Core\Model\LicenceTable')->saveLicence($licence);
            }

            if (count($insertedIDs) != $licenceCount) {
                $this->msg = 'Couldn\'t insert licence.';
                $this->status = FALSE;
                return $this->JSONRespond();
            }
            $licenceCount = $this->CommonTable('Core\Model\LicenceTable')->getCurrentLicenceCount();
            $this->CommonTable('CompanyTable')->updateLicenceCount($licenceCount, $this->getCompanyDetails()[0]->id);
            $this->data = $insertedIDs;
            $this->msg = 'Successfully licence inserted.';
            $this->status = TRUE;
            return $this->JSONRespond();
        }
    }

    /**
     * this function will modify licenceExpire status if
     *      expiredate less than current date
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function updateLicenceStatusByDateAction()
    {
        $this->CommonTable('Core\Model\LicenceTable')->updateLicenceStatusByDate();
        $licenceCount = $this->CommonTable('Core\Model\LicenceTable')->getCurrentLicenceCount();
        $this->CommonTable('CompanyTable')->updateLicenceCount($licenceCount, $this->getCompanyDetails()[0]->id);
        return $this->JSONRespond();
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function assignUserForLicenceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $licenceID = $request->getPost('licenceID');
            $userID = $request->getPost('userID');
            if (!$licenceID) {
                $this->msg = 'Licence ID Couldn\'t be null';
                $this->status = FALSE;
                return $this->JSONRespond();
            } else if (!$userID) {
                $this->msg = 'User ID Couldn\'t be null';
                $this->status = FALSE;
                return $this->JSONRespond();
            }

            $modifiedID = $this->CommonTable('Core\Model\LicenceTable')->assignLicenceForUserID($licenceID, $userID);
            if ($modifiedID) {
                $this->msg = 'Successfully licence added to user';
                $this->status = TRUE;
                return $this->JSONRespond();
            } else {
                $this->msg = 'Couldn\'t add licence for user';
                $this->status = FALSE;
                return $this->JSONRespond();
            }
        }
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function deactivateLicenceForUserAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $userID = $request->getPost('userID');
            if (!$userID) {
                $this->msg = 'User ID Couldn\'t be null';
                $this->status = FALSE;
                return $this->JSONRespond();
            }

            $modifiedID = $this->CommonTable('Core\Model\LicenceTable')->makeEmptyLicenceByUserID($userID);
            if ($modifiedID) {
                $this->msg = 'Successfully licence added to user';
                $this->status = TRUE;
                return $this->JSONRespond();
            } else {
                $this->msg = 'Couldn\'t add licence for user';
                $this->status = FALSE;
                return $this->JSONRespond();
            }
        }
    }

    public function deleteLicenceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $licenceID = $request->getPost('licenceID');
            if (!$licenceID) {
                $this->msg = 'Licence ID Couldn\'t be null';
                $this->status = FALSE;
                return $this->JSONRespond();
            }
            $deletedID = $this->CommonTable('Core\Model\LicenceTable')->deleteLicenceByLicenceID($licenceID);
            if ($deletedID) {
                $licenceCount = $this->CommonTable('Core\Model\LicenceTable')->getCurrentLicenceCount();
                $this->CommonTable('CompanyTable')->updateLicenceCount($licenceCount, $this->getCompanyDetails()[0]->id);
                $this->msg = 'Successfully licence deleted';
                $this->status = TRUE;
                return $this->JSONRespond();
            } else {
                $this->msg = 'Couldn\'t delete licence';
                $this->status = FALSE;
                return $this->JSONRespond();
            }
        }
    }

//    TODO Should update in company table licence count
    public function updateLicenceAction()
    {
//
//        $request = $this->getRequest();
//        if ($request->isPost()) {
//            $licenceCount = $request->getPost('licenceCount');
//            echo $request->getPost('licenceID');
//        }
        exit;
    }

    public function generateReportAction()
    {
        try {

            set_time_limit(0); // set unlimited execution time

            $request = $this->getRequest();
            if (!$request->isPost()) {
                return $this->returnJsonError('Invalid request');
            }
            $requestData = $request->getPost()->toArray();// get report request data

            $service = $requestData['meta']['service'];
            $reportQueueCategory = $requestData['meta']['category'];
            $method = $requestData['meta']['method'];
            $reportData = $requestData['data'];
            $reportData['companyCurrencySymbol'] = $requestData['meta']['companyCurrencySymbol'];

            // generate report
            $result = $this->getService($service)->$method($reportData);

            if (!$result['status']) {
                return $this->returnJsonError('Error occured while generating queued report.');
            }

            $data =[];
            $data['reportQueueCategory'] = $reportQueueCategory;
            $data['reportQueueReportName'] = $result['data']['reportName'];
            $data['reportQueueUrl'] = $this->getReportFolderUrl().strtolower($result['data']['fileType']).'/'.$result['data']['fileName'];
            $data['reportQueueStatus'] = TRUE;
            $data['reportQueueCreatedDateTime'] = $requestData['meta']['requestDateTime'];

            $reportQueue = new ReportQueue();
            $reportQueue->exchangeArray($data);

            if (!$this->CommonTable('Core\Model\ReportQueueTable')->saveReportQueue($reportQueue)) {
                return $this->returnJsonError('Error occured while inserting queued report data to database.');
            }

            // send notification
            $result = $this->getService('ReportService')->sendNotification($result['data']['reportName'], $result['data']['fileType'], $result['data']['fileName'], $requestData['meta']);

            if (!$result['status']) {
                return $this->returnJsonError('Report has been generated. But Error occured while sending notification.');
            }
            return $this->returnJsonSuccess(null, 'Report has been generated.');

        } catch (\Exception $ex) {
            error_log($ex->getMessage());
            return $this->returnJsonError('Error occured while generating queued report.');
        }
    }


    public function viewQueuedReportsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->returnJsonError('Invalid request');
        }
        $requestData = $request->getPost()->toArray();

        $queuedReportData = $this->CommonTable('Core\Model\ReportQueueTable')->getQueuedReportsByCategory($requestData['reportQueuecategory']);

        return $this->returnJsonSuccess($queuedReportData, 'Queued Reports are succefully retrived.');


    }

    public function deleteViewedReportsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->returnJsonError('Invalid request');
        }
        $requestData = $request->getPost()->toArray();

        $deleteStatus = $this->CommonTable('Core\Model\ReportQueueTable')->deleteViewedReportById($requestData['deleteID']);

        return $this->returnJsonSuccess($deleteStatus, 'Queued Report viewed.');

    }


    public function saveEmailConfigAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->returnJsonError($this->getMessage('ERR_VALID_MAIL_CONFIG'));
        }

        $data = $request->getPost()->toArray();

        if ( !(isset($data['email']) && $data['email'])
                || !(isset($data['password']) && $data['password'])
                || !(isset($data['host']) && $data['host'])) {
            return $this->returnJsonError($this->getMessage('ERR_VALID_MAIL_CONFIG'));
        }

        $mailConf = new Emailconfig();
        $mailConf->exchangeArray($data);

        $this->CommonTable('User\Model\EmailconfigTable')->saveConfiguration($mailConf);

        return $this->returnJsonSuccess($data, $this->getMessage('SUCC_MAIL_CONFIG'));
    }

    public function removeEmailConfigAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            return $this->returnJsonError('Invalid request');
        }

        $this->CommonTable('User\Model\EmailconfigTable')->removeConfiguration();

        return $this->returnJsonSuccess([], 'e-mail configurations succefully removed.');
    }

}
