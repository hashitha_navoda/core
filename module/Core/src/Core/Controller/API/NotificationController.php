<?php

/**
 * Description of NotificationController
 *
 * @author shamilan <sharmilan@thinkcube.com>
 */

namespace Core\Controller\API;

use Zend\Session\Container;
use Core\Model\Notification;
use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\EventManager\EventInterface;
use Core\Library\WsHandler;
use ZfrPusher\Client\Credentials;
use ZfrPusher\Client\PusherClient;
use ZfrPusher\Service\PusherService;
use Pusher;

class NotificationController extends CoreController
{

    protected $user_session;
    private $locationID;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->allLocations = $this->user_session->userAllLocations;
        $this->locationID = $this->user_session->userActiveLocation['locationID'];
    }

    /**
     * get all 'open notifications' with their actual values from Notification table
     */
    public function getNotificationAction()
    {

        $noti_data = $this->setViewVariables();
        $noti_list = new ViewModel(array('data' => $noti_data));
        $noti_list->setTemplate('core/notification/index')->setTerminal(true);
        return $noti_list;
    }

    /**
     * setting variables for view site
     *
     * @param array $notifications having no of notifications and variables for list
     */
    public function setViewVariables($filterType = null)
    {

        // if user is not currently logged in, skip executing this method
        // TODO - find better way to do this. put in place to bypass ssoprovider bug
        if (empty($this->userID)) {
            return true;
        }

//        $notifications = $this->_getOpenNotificationFromTable($this->locationID);
        $notificationTable = $this->CommonTable('Core\Model\NotificationTable');
        $notificationResult = $notificationTable->fetchAllOpenStatus($this->locationID, $filterType);
        foreach ($notificationResult as $noti) {
            if (isset($noti['userID']) && ($noti['userID'] != $this->userID)) {
                continue;
            }
            $notifications[] = $noti;
        }

        if (!empty($notifications)) {
            $noOfNotification = count($notifications);
            if ($noOfNotification < 10) {
                $noOfNotification = '0' . $noOfNotification;
            }

            foreach ($notifications as $notification) {

                $config = $this->getServiceLocator()->get('config');
                $title = $notification['notificationTitle'];

                if ($this->user_session->lang == 'si_LK' && !empty($config['notification-title-sl'][$notification['notificationTitle']])) {
                    $title = $config['notification-title-sl'][$notification['notificationTitle']];
                } elseif ($this->user_session->lang == 'tl_LK' && !empty($config['notification-title-tl'][$notification['notificationTitle']])) {
                    $title = $config['notification-title-tl'][$notification['notificationTitle']];
                }

                $body = $notification['notificationBody'];
                if ($this->user_session->lang == 'si_LK' && !empty(json_decode($notification['notificationBody_si']))) {
                    $body = json_decode($notification['notificationBody_si']);                    
                } elseif ($this->user_session->lang == 'tl_LK' && !empty(json_decode($notification['notificationBody_tl']))) {
                    $body = json_decode($notification['notificationBody_tl']);
                }
                // $body = ($this->user_session->lang == 'si_LK') ? json_decode($notification['notificationBody_si']) : $notification['notificationBody'];
                $notificationType = $this->_numericToText($notification['notificationType'], 'notification-types');
                $notificationUpdatedDateTime = $this->getUserDateTime($notification['notificationUpdatedDateTime'], 'Y-m-d H:i:s');
                $ID = $notification['notificationReferID'];
                $iconClass = $this->_selectIcon($notificationType);
                $link = '';
                if ($notificationType === 'Overdue Sales Invoice') {
                    $link = "/invoice/invoiceView/" . $ID;
                    $notificationType .= " over_due_inv";
                } else if ($notificationType === 'Overdue Purchase Invoice') {
                    $link = "/pi/preview/" . $ID;
                    $notificationType .= " over_due_purchase_inv";
                } else if ($notificationType === 'Report Notification') {
                    $link = $this->getReportFolderUrl() . $ID;
                } else if ($notificationType === 'User Message') {
                    $link = "/inquiry-log/view/" . $ID;
                } else if ($notificationType === 'Next Invoice Date') {
                    $link = "/invoice/invoiceView/" . $ID;
                    $notificationType .= " over_due_inv";
                } else {
                    $link = "#";
                }
                $notificationArray[] = array(
                    'notificationID' => $notification['notificationID'],
                    'notificationTitle' => $title,
                    'notificationBody' => $body,
                    'notificationType' => $notificationType,
                    'notificationUpdatedTime' => $notificationUpdatedDateTime,
                    'notificationFormatedUpdateTime' => $this->_formatDate($notificationUpdatedDateTime),
                    'iconClass' => $iconClass,
                    'link' => $link
                );
            }
            return array(
                'noOfNotification' => $noOfNotification,
                'notifications' => $notificationArray
            );
        } else {

            $notificationArray[] = array(
                'notificationTitle' => 'No messages',
                'notificationBody' => 'No messges to display',
                'notificationType' => 'Overdue Purchase Invoice',
                'iconClass' => '',
                'notificationUpdatedTime' => '',
                'notificationFormatedUpdateTime' => '',
                'link' => '#'
            );
            return array(
                'noOfNotification' => '00',
                'notifications' => $notificationArray
            );
        }
    }

    public function displayAllNotificationsAction()
    {

//        $globaldata = $this->getServiceLocator()->get('config');
//        $this->getEvent()->getViewModel()->sidemenu = $globaldata['home_menu'];
//        $this->getEvent()->getViewModel()->sidemenuselected = 'Home';

        $notificationType = $this->params()->fromRoute('param2');
        $notificationType = ($notificationType) ? $notificationType : null;
        $notificationArray = $this->setViewVariables($notificationType)['notifications'];
        $noOfNotification = $this->setViewVariables($notificationType)['noOfNotification'];
//      Setting paginaor to notification list
        $paginator = new \Zend\Paginator\Paginator(new
                \Zend\Paginator\Adapter\ArrayAdapter($notificationArray)
        );
        $paginator->setItemCountPerPage(6);
        $paginator->setCurrentPageNumber($this->params()->fromRoute('param1'));

        $allNotification = new ViewModel(array(
            'notifications' => $paginator,
            'notificationType' => $notificationType,
            'noOfNotification' => $noOfNotification
        ));
        $allNotification->setTemplate('core/notification/display-all-notifications');
        return $allNotification;
    }

    /**
     * In Cron usage.
     * This will update the Notification Table
     *      if new notification will add
     *      if notification disabled change status in notification Table
     */
    public function updateNotificationAction()
    {
        echo $_SERVER['HTTP_HOST'] . " \n";

        $location = $this->CommonTable('Core\Model\LocationTable');
        $result = $location->getActiveLocations();

        while ($row = $result->current()) {
            $this->_storeToNotification($row['locationID']);
            $this->_updateNotificationStatus($row['locationID']);
        }

        $this->checkFiscalPeriod();

        echo "Successfully updated notifications. \n----------- \n\n";
        exit;
    }


    /**
     * Push banner like notice to users if fiscal period ends within 7 days
     */
    public function checkFiscalPeriod()
    {
        $company = $this->CommonTable('CompanyTable')->fetchAll()->current();
        if (!$company->companyUseAccounting) {
            return;
        }

        $inWeekDate = date('Y-m-d', strtotime("+7 day"));
        $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getValidFiscalPeriod($inWeekDate);

        if (!$fiscalPeriod) {
            $msg = $this->getMessage('ERR_FICAL_PERIOD_ABOUT_TO_EXPIRE');
            $noticeName = 'Ending phiscal period';
            $expDate = date("Y-m-d", strtotime('tomorrow'));

            $data = [
                'notice' => $msg,
                'noticeName' => $noticeName,
                'noticeType' => 'danger',
                'noticeExpireDate' => $expDate,
                'noticeLink' => ''
            ];

            $noticeService = $this->getService('NoticeService');
            $getCurNotice = $noticeService->getNoticeByDateAndTitle($expDate, $noticeName);

            if (!$getCurNotice['status']) {
                $res = $noticeService->createNotice($data);
            }
        }
    }

    /**
     * get new notifications and to store NotificationTable
     */
    private function _getNewNotification($locationID = NULL)
    {
        if (is_null($locationID)) {
            $locationID = $this->locationID;
        }

        $membershipNoti = $this->_getMemberShipNotifications($locationID);
        $salesNoti = $this->_getSalesInvoiceNoti($locationID);
        $purchaseNoti = $this->_getPurchaseInvoiceNoti($locationID);

//        $reOrderNoti = $this->_getInventeryReorderNoti($locationID);
//        $minimumNoti = $this->_getInventeryMinStockLevelNoti($locationID);

        $incomingChequeNoti = $this->_getIncomingChequeNoti($locationID);
        $outgoingChequeNoti = $this->_getOutgoingChequeNoti($locationID);

//       item expire date notification
        $itemExpiryAlert = $this->_getExpireItemNoti($locationID);

        //post days cheque expire notification
        $postDayChequeExpireAlert = $this->_getPostDaysChequeExpireItemNoti($locationID);

        $notificationArray = array_merge($salesNoti, $purchaseNoti, $incomingChequeNoti, $outgoingChequeNoti, $itemExpiryAlert, $postDayChequeExpireAlert, $membershipNoti);
//        $notificationArray = array_merge($salesNoti, $purchaseNoti, $reOrderNoti, $minimumNoti, $incomingChequeNoti, $outgoingChequeNoti);

        return $notificationArray;
    }

    /**
     * If there any overdue notifications in Invoice add to new array ($notificationArray)
     */
    private function _getSalesInvoiceNoti($locationID = NULL)
    {
        $overdueInvoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getOverduenvoices($locationID);
        while ($row2 = $overdueInvoice->current()) {
            $data['notificationReferID'] = $row2['salesInvoiceID'];
            $data['locationID'] = $locationID;
            $data['notificationType'] = 1;
            $data['notificationTitle'] = 'An Overdue Sales invoice';
            $data['notificationBody'] = "Invoice \"{$row2['salesInvoiceCode']}\" issued to"
                    . " \"{$row2['customerName']}\" has reached due Date.";
            $data['notificationBody_si'] = json_encode("\"{$row2['customerName']}\" වෙත නිකුත් කරනු ලැබූ \"{$row2['salesInvoiceCode']}\" යන ඉන්වොයිසිය නියමිත දිනට ලගාවී ඇත.");
            $data['notificationBody_tl'] = json_encode("\"{$row2['customerName']}\" க்கு வழங்கப்பட்ட விலைப்பட்டியல் "
                    . " \"{$row2['salesInvoiceCode']}\" தவணைத் தேதியை  அடைந்துள்ளது.");
            $date = new \DateTime();
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime(); //$date->format(\DateTime::ATOM);
            $data['status'] = 0;
            $notificationArray[] = $data;
        }

        if (!empty($notificationArray)) {
            return $notificationArray;
        } else {
            return array();
        }
    }

     private function _getMemberShipNotifications($locationID = NULL)
    {
        $today = $this->convertDateToStandardFormat(date('Y-m-d'));

        $memberShipInvoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getMembershipInvoices($locationID, $today);
        while ($row2 = $memberShipInvoice->current()) {
            $data['notificationReferID'] = $row2['salesInvoiceID'];
            $data['locationID'] = $locationID;
            $data['notificationType'] = 11;
            $data['notificationTitle'] = 'Next Invoice Date For Sales Invoice';
            if ($row2['membershipComment'] != null) {
                $data['notificationBody'] = "Invoice \"{$row2['salesInvoiceCode']}\" issued to"
                        . " \"{$row2['customerName']}\" has reached next Invoice Date. Comment: ".$row2['membershipComment'];
                $data['notificationBody_si'] =  json_encode("\"{$row2['customerName']}\" හට නිකුත් කරනු ලැබූ ". " \"{$row2['salesInvoiceCode']}\" දරන ඉන්වොයිසිය මීළඟ ඉන්වොයිස් දිනය කරා ලඟා වී ඇත. අදහස්: ".$row2['membershipComment']);

                $data['notificationBody_tl'] =  json_encode("\"{$row2['customerName']}\" க்கு வழங்கப்பட்ட விலைப்பட்டியல் ". " \"{$row2['salesInvoiceCode']}\"  அடுத்த விலைப்பட்டியல் தேதியை  அடைந்துள்ளது. கருத்துகள்: ".$row2['membershipComment']);

                
            } else {
                $data['notificationBody'] = "Invoice \"{$row2['salesInvoiceCode']}\" issued to"
                        . " \"{$row2['customerName']}\" has reached next Invoice Date.";
                $data['notificationBody_si'] =  json_encode("\"{$row2['customerName']}\" හට නිකුත් කරනු ලැබූ ". " \"{$row2['salesInvoiceCode']}\" දරන ඉන්වොයිසිය මීළඟ ඉන්වොයිස් දිනය කරා ලඟා වී ඇත.");

                $data['notificationBody_tl'] =  json_encode("\"{$row2['customerName']}\" க்கு வழங்கப்பட்ட விலைப்பட்டியல் ". " \"{$row2['salesInvoiceCode']}\"  அடுத்த விலைப்பட்டியல் தேதியை  அடைந்துள்ளது.");
            }
            $date = new \DateTime();
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime(); //$date->format(\DateTime::ATOM);
            $data['status'] = 0;
            $notificationArray[] = $data;
        }

        if (!empty($notificationArray)) {
            return $notificationArray;
        } else {
            return array();
        }
    }

    /**
     * If there any overdue notifications in purchaseinvoice add to
     *  new array ($notificationArray)
     */
    private function _getPurchaseInvoiceNoti($locationID = NULL)
    {
        $overduePurchaseInvoice = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getOverduePurchaseInvoices($locationID);
        while ($row3 = $overduePurchaseInvoice->current()) {
            $data['notificationReferID'] = $row3['purchaseInvoiceID'];
            $data['locationID'] = $locationID;
            $data['notificationType'] = 2;
            $data['notificationTitle'] = 'An Overdue Purchase Invoice';
            $data['notificationBody'] = "Purchase Invoice \"{$row3['purchaseInvoiceCode']}\" received from"
                    . " \"{$row3['supplierName']}\" has reached due Date.";
            $data['notificationBody_si'] = json_encode("\"{$row3['supplierName']}\" ගෙන් ලද \"{$row3['purchaseInvoiceCode']}\" ගැනුම් ඉන්වොයිසිය නියමිත දිනට ලඟා වී  ඇත.");
            $data['notificationBody_tl'] = json_encode("\"{$row3['supplierName']}\" இடம் பெறப்பட்ட கொள்முதல் விலைப்பட்டியல் "
                    . " \"{$row3['purchaseInvoiceCode']}\" தவணைத்தேதியை அடைந்துள்ளது.") ;
            $date = new \DateTime();
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime(); //$date->format(\DateTime::ATOM);
            $data['status'] = 0;
            $notificationArray[] = $data;
        }

        if (!empty($notificationArray)) {
            return $notificationArray;
        } else {
            return array();
        }
    }

    /**
     * If there any Re-order level notifications add to
     *  new array ($notificationArray)
     */
    private function _getInventeryReorderNoti($locationID = NULL)
    {
        $overdueItemReorderLevel = $this->CommonTable('Inventory\Model\LocationProductTable')->getOverdueReorderLevelItem($locationID);
        while ($row4 = $overdueItemReorderLevel->current()) {

            $data['notificationReferID'] = $row4['locationProductID'];
            $data['locationID'] = $locationID;
            $data['notificationType'] = 3;
            $data['notificationTitle'] = 'Have Reached Reorder Level';
            $data['notificationBody'] = "Item \"{$row4['productName']}\" "
                    . " has reached Reorder level.";
            $date = new \DateTime();
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime(); //$date->format(\DateTime::ATOM);
            $data['status'] = 0;
            $notificationArray[] = $data;
        }

        if (!empty($notificationArray)) {
            return $notificationArray;
        } else {
            return array();
        }
    }

    /**
     * If there any Minimum Inventory level notifications add to
     *  new array ($notificationArray)
     */
    private function _getInventeryMinStockLevelNoti($locationID = NULL)
    {
        $overdueItemInventoryLevel = $this->CommonTable('Inventory\Model\LocationProductTable')->getOverdueInventoryLevelItem($locationID);
        while ($row5 = $overdueItemInventoryLevel->current()) {

            $data['notificationReferID'] = $row5['locationProductID'];
            $data['locationID'] = $locationID;
            $data['notificationType'] = 4;
            $data['notificationTitle'] = 'Have Reached Minimum Inventory Level';
            $data['notificationBody'] = "Item \"{$row5['productName']}\""
                    . " has reached Minimum Inventory Level.";
            $date = new \DateTime();
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime(); // $date->format(\DateTime::ATOM);
            $data['status'] = 0;
            $notificationArray[] = $data;
        }

        if (!empty($notificationArray)) {
            return $notificationArray;
        } else {
            return array();
        }
    }

    /**
     * this function use to get all expire item details.
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * @param type $locationID
     * @return array
     */
    private function _getExpireItemNoti($locationID)
    {
        $value = $this->CommonTable('Inventory\Model\ProductTable')->getAllProductForExpiryAlert($locationID);
        foreach ($value as $row5) {
            $data['locationID'] = $locationID;
            $data['notificationType'] = 8; // product item expire alert notification
            //if item expire current day then run this condition
            if ($row5['numberOfDays'] == 0) {

                //if item is serial,then run below part
                if ($row5['productSerialCode'] != '' || $row5['productSerialCode'] != null) {
                    $serialBatchID = $row5['productSerialID'] . '_' . $row5['numberOfDays'] . 'S';
                    $data['notificationBody'] = "Serial No \"{$row5['productSerialCode']}\""
                            . " has expired in product \"{$row5['productCode']} - {$row5['productName']}\".";
                    $data['notificationBody_si'] = json_encode("\"{$row5['productSerialCode']}\" යන අනුක්රමික අංකය දරන \"{$row5['productCode']} - {$row5['productName']}\" නමැති නිෂ්පාදනය කල් ඉකුත් වී ඇත.");
                    $data['notificationBody_tl'] = json_encode("\"{$row5['productCode']} - {$row5['productName']}\" ல் வரிசை எண்". " \"{$row5['productSerialCode']}\" காலாவதியானது.");
                }
                //if item type is batch then run below part
                else if ($row5['productBatchCode'] != '' || $row5['productBatchCode'] != null) {
                    $serialBatchID = $row5['productBatchID'] . '_' . $row5['numberOfDays'] . 'B';
                    $quantity = number_format($row5['productBatchQuantity'], 2);
                    $data['notificationBody'] = "\"{$quantity}  {$row5['uomAbbr']}\" quantity of Batch No \"{$row5['productBatchCode']}\""
                            . " has expired in product \"{$row5['productCode']} - {$row5['productName']}\".";
                    $data['notificationBody_si'] = json_encode("\"{$row5['productBatchCode']}\" යන කණ්ඩායම් අංකය දරන \"{$row5['productCode']} - {$row5['productName']}\" යන නිෂ්පාදනයේ \"{$quantity}  {$row5['uomAbbr']}\" කල් ඉකුත් වී ඇත.");
                    $data['notificationBody_tl'] = json_encode("\"{$row5['productCode']} - {$row5['productName']}\" தொகுதி எண் \"{$row5['productBatchCode']}\" இல் \"{$quantity}  {$row5['uomAbbr']}\" அளவு காலாவதியானது");
                            
                }
            } else {

                if ($row5['productSerialCode'] != '' || $row5['productSerialCode'] != null) {
                    $serialBatchID = $row5['productSerialID'] . '_' . $row5['numberOfDays'] . 'S';
                    $data['notificationBody'] = "Serial No \"{$row5['productSerialCode']}\""
                            . " has remaining \"{$row5['numberOfDays']}\" days for expire in product \"{$row5['productCode']} - {$row5['productName']}\".";

                    $data['notificationBody_si'] = json_encode("\"{$row5['productSerialCode']}\" යන අනුක්රමික අංකය දරන \"{$row5['productCode']} - {$row5['productName']}\" නමැති නිෂ්පාදනය කල් ඉකුත් වීම සඳහා තව දින \"{$row5['numberOfDays']}\" ක් ඇත.");

                    $data['notificationBody_tl'] = json_encode("\"{$row5['productCode']} - {$row5['productName']}\"  இல் வரிசை எண் \"{$row5['productSerialCode']}\" காலாவதியாக  இன்னும் \"{$row5['numberOfDays']}\" நாட்கள்  உள்ளன.");

                } else if ($row5['productBatchCode'] != '' || $row5['productBatchCode'] != null) {
                    $quantity = number_format($row5['productBatchQuantity'], 2);
                    $serialBatchID = $row5['productBatchID'] . '_' . $row5['numberOfDays'] . 'B';
                    $data['notificationBody'] = "\"{$quantity}  {$row5['uomAbbr']}\" quantity of Batch No \"{$row5['productBatchCode']}\""
                            . " has remaining \"{$row5['numberOfDays']}\" days for expire in product \"{$row5['productCode']} - {$row5['productName']}\".";

                    $data['notificationBody_si'] = json_encode("\"{$row5['productBatchCode']}\" යන කණ්ඩායම් අංකය දරන \"{$row5['productCode']} - {$row5['productName']}\" යන නිෂ්පාදනයේ \"{$quantity}  {$row5['uomAbbr']}\" කල් ඉකුත් වීම සඳහා තව දින \"{$row5['numberOfDays']}\" ක් ඇත.");

                    $data['notificationBody_tl'] = json_encode("\"{$row5['productCode']} - {$row5['productName']}\" தொகுதி எண் \"{$row5['productBatchCode']}\" இல் \"{$quantity}  {$row5['uomAbbr']}\" அளவு காலாவதியாக இன்னும் \"{$row5['numberOfDays']}\" நாட்கள் உள்ளன");
                }
            }
            $data['notificationReferID'] = $serialBatchID;
            $data['notificationTitle'] = 'Product Expiry Alert Notification';

            $date = new \DateTime();
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime(); // $date->format(\DateTime::ATOM);
            $data['status'] = 0;
            $notificationArray[] = $data;
        }
        if (!empty($notificationArray)) {
            return $notificationArray;
        } else {
            return array();
        }
    }

    /**
     * this function use to get post days cheque expire notification alerts.
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * @param type $locationID
     * @return int
     */
    private function _getPostDaysChequeExpireItemNoti($locationID)
    {
        $value = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getPostDatedChequesForAlertNotification($locationID);
        foreach ($value as $row5) {
            $paidAmount = number_format($row5['incomingPaymentPaidAmount'], 2);
            $data['notificationReferID'] = $row5['incomingPaymentMethodChequeId'];
            $data['locationID'] = $locationID;
            $data['notificationType'] = 9; // Post Days Cheque Expire Alerts
            $data['notificationBody'] = "Post Dated Cheque for payment no \"{$row5['incomingPaymentCode']}\""
                    . " has remaining 7 days for expire. "
                    . " Customer - \" {$row5['customerName']}\" "
                    . " Paid Amount - {$row5['currencySymbol']} {$paidAmount} ";
            $data['notificationBody_si'] = json_encode("ගෙවීම් අංක \"{$row5['incomingPaymentCode']}\" සදහා වූ කල් ඉකුත්වන චෙක් පත"
                    . " කල් ඉකුත්  වීම සඳහා තව දින 7ක් ඇත. "
                    . " පාරිභෝගිකයා - \" {$row5['customerName']}\" "
                    . " ගෙවූ මුදල - {$row5['currencySymbol']} {$paidAmount} ");

            $data['notificationBody_tl'] = json_encode("கொடுப்பனவு எண் \"{$row5['incomingPaymentCode']}\" "
                    . " இற்கு வழங்கப்பட்டிருந்த பின் தேதியிட்ட காசோலை காலாவதியாக 7 நாட்கள் உள்ளன. "
                    . " வாடிக்கையாளர் - \" {$row5['customerName']}\" "
                    . " செலுத்தப்பட்ட தொகை - {$row5['currencySymbol']} {$paidAmount} ");

            $data['notificationTitle'] = 'Postdated Cheque Expire Alert Notification';

            $date = new \DateTime();
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime(); // $date->format(\DateTime::ATOM);
            $data['status'] = 0;
            $notificationArray[] = $data;
            $extraNumber++;
        }
        if (!empty($notificationArray)) {
            return $notificationArray;
        } else {
            return array();
        }
    }

    /**
     * for crontap works
     * @author sharmilan <sharmilan@thinkcube.com>
     *  update the overdue invoices open status to overdue status
     */
    public function updateOverdueInvoiceStatusAction()
    {
        echo $_SERVER['HTTP_HOST'] . " \n";
        $locations = $this->CommonTable('Core\Model\LocationTable')->getActiveLocations();
        while ($row = $locations->current()) {
            $this->_updateOverduePurchaseInvoice($row['locationID']);
            $this->_updateOverdueSalesInvoiceState($row['locationID']);
            $this->_updateOverdueExpensePaymentInvoice($row['locationID']);
        }
        echo 'Overdue invoices\' status sucessfully updated........';
        exit;
    }

    /**
     * update the open status to overDue status
     * @param type $locationID
     */
    private function _updateOverdueSalesInvoiceState($locationID = null)
    {
        if (is_null($locationID)) {
            $locationID = $this->locationID;
        }
        $overdueStatusID = $this->getStatusID("overdue");
        $overdueInvoiceIDs = $this->CommonTable('Invoice\Model\InvoiceTable')->getOverdueInvoiceIDs($locationID);
        while ($row = $overdueInvoiceIDs->current()) {
            $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceStatusID($row['salesInvoiceID'], $overdueStatusID);
        }
    }

    /**
     * update the open status to overdue status
     * @param type $locationID
     */
    private function _updateOverduePurchaseInvoice($locationID = NULL)
    {
        if (is_null($locationID)) {
            $locationID = $this->locationID;
        }
        $overdueStatusID = $this->getStatusID("overdue");
        $overdueStatusIDs = $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->getOverdueinvoiceIDs($locationID);
        while ($row = $overdueStatusIDs->current()) {
            $this->CommonTable('Inventory\Model\PurchaseInvoiceTable')->updateOverDueInvoiceStatus($row['purchaseInvoiceID'], $overdueStatusID);
        }
    }

    /**
     * update the open status to overdue status
     * @param type $locationID
     */
    private function _updateOverdueExpensePaymentInvoice($locationID = NULL)
    {
        if (is_null($locationID)) {
            $locationID = $this->locationID;
        }
        $overdueStatusID = $this->getStatusID("overdue");
        $overdueStatusIDs = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getOverduePaymentVoucherIDs($locationID);
        while ($row = $overdueStatusIDs->current()) {
            $this->CommonTable('Expenses\Model\PaymentVoucherTable')->updatePaymentVoucherStatus($row['paymentVoucherID'], $overdueStatusID);
        }
    }

    /**
     * get whole notification of 'open' 'status' dataes
     * @return array data from table
     */
    private function _getOpenNotificationFromTable($locationID = NULL)
    {
        if (is_null($locationID)) {
            $locationID = $this->locationID;
        }

        $notification = $this->CommonTable('Core\Model\NotificationTable');
        $oldNotificationArray = array();
        $result = $notification->fetchAllOpenStatus($locationID);
        while ($row = $result->current()) {
            $oldNotificationArray[] = $row;
        }

        if ($oldNotificationArray != NULL) {
            return $oldNotificationArray;
        } else {
            return array();
        }
    }

    /**
     * check collected new notification according to 'locationID' with Notification table contents
     *  if exists ignore it
     *  or add new notification to Notification table
     */
    private function _storeToNotification($locationID = NULL)
    {
        if (is_null($locationID)) {
            $locationID = $this->locationID;
        }

        $newNotificationArray = $this->_getNewNotification($locationID);
        $notification = new Notification();

        foreach ($newNotificationArray as $newNotification) {
            $oldNotificationArray = $this->_getOpenNotificationFromTable($locationID);

            foreach ($oldNotificationArray as $oldNotification) {
                $result = $this->_isExists($oldNotification, $newNotification);
                if ($result) {
                    break;
                }
            }

            if (!$result) {
                $notification->exchangeArray($newNotification);
                $notificationID = $this->CommonTable('Core\Model\NotificationTable')->saveNotification($notification);
                 if ($newNotification['notificationType'] == 1 || $newNotification['notificationType'] == 2 || $newNotification['notificationType'] == 6 || $newNotification['notificationType'] == 7 || $newNotification['notificationType'] == 9 ) {
                    // call mobile push notification function
                    $this->pushNotificationsToMobile($notificationID);
                 }
                // echo 'this data copied to database <br />';
            } else {
//                echo 'this data already exixts <br />';
            }
        }
    }

    /**
     * check old notification according to the 'locationID' still exists in new Notification array
     *  if not it's 'status' change column to 1 (close)
     */
    private function _updateNotificationStatus($locationID = NULL)
    {
        if (is_null($locationID)) {
            $locationID = $this->locationID;
        }

        $oldNotificationArray = $this->_getOpenNotificationFromTable($locationID);
        $newNotificationArray = $this->_getNewNotification($locationID);

        foreach ($oldNotificationArray as $oldNotification) {
            if ($oldNotification['notificationType'] == '5' || $oldNotification['notificationType'] == '4' || $oldNotification['notificationType'] == '3') {
                continue;
            }
            $result = FALSE;

            $newNotification = new Notification();
            $newNotification->exchangeArray($oldNotification);

            foreach ($newNotificationArray as $singleData) {
                $result = $this->_isExists($oldNotification, $singleData);
                if ($result) {
                    break;
                }
            }
            if (!$result) {
                $notificationTable = $this->CommonTable('Core\Model\NotificationTable');
                $newNotification->status = 1;
                $notificationTable->changeStatus($newNotification);
//                echo 'changing status of ' . $newNotification->notificationID . ' <br />';
            } else {
//                echo 'this data still exixts <br />';
            }
        }
    }

    /**
     * This will convert Timestamp to needy date format for notification
     * @param type $timestamp
     *
     * @return string date format to print in notification list
     */
    private function _formatDate($dateTime)
    {
        if (!empty($dateTime)) {
            $timestamp = strtotime($dateTime);
            $date = date('d', $timestamp);
            $month = date('M', $timestamp);
            $hour = date('h', $timestamp);
            $mins = date('i', $timestamp);
            $ampm = date('a', $timestamp);
            return $month . '-' . $date . ' ' . $hour . ':' . $mins . ' ' . $ampm;
        }
    }

    /**
     * To notification List
     * select appropriate icon for each notification
     *
     * @param string $notificationType
     * @return string notification class name for Icon
     */
    private function _selectIcon($notificationType)
    {
        switch ($notificationType) {
            case "Overdue Purchase Invoice":
                return "fa fa-angle-down";

            case "Overdue Sales Invoice":
                return "fa fa-angle-down";

            case "Inventory Reorder":
                return "fa fa-angle-down";

            case "Inventory Minimum Level":
                return "fa fa-angle-down error";

            case "warning":
                return "alert-warning alert-danger";

            case "success":
                return "alert-success";

            default:
                return "alert-info";
        }
    }

    /**
     * To convert numeric to text as defined in Core/config/notification.config.php
     *
     * @param int $num which is selection number
     * @param string $category which is type of text eg: notification-types or notification-status
     * @return int number for corresponding input text
     */
    private function _numericToText($num, $category)
    {
        $config = $this->getServiceLocator()->get('config');

        if ($category == 'notification-types') {
            return $config[$category][$num][0];
        } elseif ($category == 'notification-status') {
            return $config[$category][$num][0];
        }

        return NULL;
    }

    /**
     * check two Notification object have same details
     *
     * @param array $oldNotification already exists data in Notification table
     * @param array $notification fresh notification collected form whole tables
     * @return boolean TRUE if same data already exists if not FALSE
     */
    private function _isExists($oldNotification, $notification)
    {
        if (($oldNotification['locationID'] == $notification['locationID'] ) &&
                ($oldNotification['notificationReferID'] == $notification['notificationReferID'] ) &&
                ($oldNotification['status'] == $notification['status'] ) &&
                ($oldNotification['notificationType'] == $notification['notificationType'])) {
            return TRUE;
        }

        return FALSE;
    }

    /**
     * incoming cheque postdated notifications
     * @return array
     */
    private function _getIncomingChequeNoti($locationId)
    {
        $incomingCheques = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getPostdatedCheques(date("Y-m-d"),$locationId);
        foreach ($incomingCheques as $incomingCheque) {
            $data['notificationReferID'] = $incomingCheque['incomingPaymentMethodChequeId'];
            $data['locationID'] = $incomingCheque['locationID'];
            $data['notificationType'] = 6;
            $data['notificationTitle'] = 'Postdated Incoming Cheque';
            $data['notificationBody'] = $this->generateChequeNotificationMsg( null, $incomingCheque['postdatedChequeDate'], $incomingCheque['incomingPaymentMethodChequeBankName'], $incomingCheque['incomingPaymentMethodChequeNumber'], 'incoming');
            $data['notificationBody_si'] = json_encode($this->generateChequeNotificationMsg( null, $incomingCheque['postdatedChequeDate'], $incomingCheque['incomingPaymentMethodChequeBankName'], $incomingCheque['incomingPaymentMethodChequeNumber'], 'incoming', 'si'));
            $data['notificationBody_tl'] = json_encode($this->generateChequeNotificationMsg( null, $incomingCheque['postdatedChequeDate'], $incomingCheque['incomingPaymentMethodChequeBankName'], $incomingCheque['incomingPaymentMethodChequeNumber'], 'incoming', 'tl'));
            $date = new \DateTime();
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime(); //$date->format(\DateTime::ATOM);
            $data['status'] = 0;
            $notificationArray[] = $data;
        }
        if (!empty($notificationArray)) {
            return $notificationArray;
        } else {
            return array();
        }
    }

    /**
     * outgoing cheque postdated notifications
     * @return array
     */
    private function _getOutgoingChequeNoti($locationId)
    {
        $tomorrow = new \DateTime();
        $tomorrow->modify('+1 day');

        $outgoingCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPostdatedCheques($tomorrow->format('Y-m-d'),$locationId);
        foreach ($outgoingCheques as $outgoingCheque) {
            $data['notificationReferID'] = $outgoingCheque['outGoingPaymentMethodsNumbersID'];
            $data['locationID'] = $outgoingCheque['locationID'];
            $data['notificationType'] = 7;
            $data['notificationTitle'] = 'Postdated Outgoing Cheque';
            $data['notificationBody'] = $this->generateChequeNotificationMsg( null, $outgoingCheque['postdatedChequeDate'], $outgoingCheque['bankName'], $outgoingCheque['outGoingPaymentMethodReferenceNumber'], 'outgoing');
            $data['notificationBody_si'] = json_encode($this->generateChequeNotificationMsg( null, $outgoingCheque['postdatedChequeDate'], $outgoingCheque['bankName'], $outgoingCheque['outGoingPaymentMethodReferenceNumber'], 'outgoing', 'si'));
            $data['notificationBody_tl'] = json_encode($this->generateChequeNotificationMsg( null, $outgoingCheque['postdatedChequeDate'], $outgoingCheque['bankName'], $outgoingCheque['outGoingPaymentMethodReferenceNumber'], 'outgoing', 'tl'));
            $date = new \DateTime();
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime(); //$date->format(\DateTime::ATOM);
            $data['status'] = 0;
            $notificationArray[] = $data;
        }
        if (!empty($notificationArray)) {
            return $notificationArray;
        } else {
            return array();
        }
    }

    /**
     * Calculate Cash In Hand loaction wise
     * @author sharmilan <sharmilan@thinkcube.com>
     */
    public function calculateCashInHandAction()
    {
        $locations = $this->CommonTable('Settings\Model\LocationTable')->fetchAll();
        foreach ($locations as $location) {
            $location = (object) $location;
            $locationCashInHand = 0.00;

//          get Sales, POS and Advance cash Payments
            $salesPaymentsCash = 0.00;
            $salesPayments = $this->CommonTable('Invoice\Model\PaymentsTable')->getCashPaymentsByLocatioID($location->locationID);

            foreach ($salesPayments as $salesPayment) {
                $salesPaymentsCash += $salesPayment['incomingPaymentMethodAmount'];
            }

//          get Pos opening amount
            $posOpeningBalance = 0.00;
            $pos = $this->CommonTable('Pos\Model\PosTable')->getPosByLocation($location->locationID);
            foreach ($pos as $posRow) {
                $posOpeningBalance += $posRow['openingBalance'];
            }

//          get credit note cash payments
            $creditNoteCash = 0.00;
            $creditNotePayments = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCashPaymentsByLocationID($location->locationID);
            foreach ($creditNotePayments as $creditNotePayment) {
                $creditNoteCash += $creditNotePayment['creditNotePaymentAmount'];
            }

//          get purchaseing cash payments
            $purchasingCash = 0.00;
            $purchasingCashPayments = $this->CommonTable('SupplierPaymentsTable')->getCashPurchaseInvoicePaymentsByLocation($location->locationID);
            foreach ($purchasingCashPayments as $purchasingCashPayment) {
                $purchasingCash += $purchasingCashPayment['outgoingInvoiceCashAmount'];
            }

//          purchasing advance payments
            $advancePurchasingCash = 0.00;
            $advancePurchasingCashPayments = $this->CommonTable('SupplierPaymentsTable')->getCashAdvancePaymentsByLocation($location->locationID);
            foreach ($advancePurchasingCashPayments as $advancePurchasingCashPayment) {
                $advancePurchasingCash += $advancePurchasingCashPayment['outgoingPaymentAmount'];
            }

//          get debit note cash Payments
            $debitNoteCash = 0.00;
            $debitnotePayments = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getCashDebitNotePaymentsByLocation($location->locationID);
            foreach ($debitnotePayments as $debitNotePayment) {
                $debitNoteCash += $debitNotePayment['debitNotePaymentDetailsAmount'];
            }

//          get petty cash float cash in hand payments
            $pettyCashFloatAmount = 0.00;
            $pettyCashFloats = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getActivePettyCashFloatByAccountID();
            foreach ($pettyCashFloats as $pettyCashFloat) {
                $pettyCashFloatAmount += $pettyCashFloat->pettyCashFloatAmount;
            }

            $withdrawalAmount = 0.00;
            $withdrawals = $this->CommonTable('Expenses\Model\CashInHandWithdrawalTable')->getCashInHandWithdrawalsByLocationId($location->locationID);
            foreach ($withdrawals as $withdrawal) {
                $withdrawalAmount += $withdrawal['cashInHandWithdrawalAmount'];
            }

            $depositAmount = 0.00;
            $deposits = $this->CommonTable('Expenses\Model\CashInHandDepositTable')->getCashInHandDepositsByLocationId($location->locationID);
            foreach ($deposits as $deposit) {
                $depositAmount += $deposit['cashInHandDepositAmount'];
            }

//          calculating the net location cash amount
            $locationCashInHand = (double) $salesPaymentsCash +
                    (double) $posOpeningBalance -
                    (double) $creditNoteCash -
                    (double) $purchasingCash -
                    (double) $advancePurchasingCash +
                    (double) $debitNoteCash -
                    (double) $pettyCashFloatAmount -
                    (double) $withdrawalAmount +
                    (double) $depositAmount;

//          Store the net Cash amount to location
            $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($location->locationID, $locationCashInHand);
        }
        print_r("<pre>");
        print_r(' ---Successfully location wise cash in hand is updated--- ');
        exit;
    }

    /**
     * Check givens products quantity has been reached their minimum Inventory level
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param EventInterface $event
     *  - Event should have product IDs and LocationIDs in an array
     */
    public function checkMinimumQunatityOfProduct(EventInterface $event)
    {
        $productIDs = $event->getParam('productIDs');
        $locationIDs = $event->getParam('locationIDs');

        $minimumInvetoryLevelProducts = $this->CommonTable('Inventory\Model\LocationProductTable')->getInventoryLevelProducts($productIDs, $locationIDs, true);
        $notificationType = "4";
        $existingNotifications = $this->CommonTable('Core\Model\NotificationTable')->getOpenNotification($productIDs, $locationIDs, $notificationType);

//      Inserting new Notificaitons
        $newNotificaitons = [];
        foreach ($minimumInvetoryLevelProducts as $product) {
            $data['notificationReferID'] = $product['productID'];
            $data['locationID'] = $product['locationID'];
            $data['notificationType'] = "4";
            $data['notificationTitle'] = 'Have Reached Minimum Inventory Level';
            $data['notificationBody'] = "Item \"{$product['productName']}\""
                    . " has reached Minimum Inventory Level.";
            $data['notificationBody_si'] = json_encode("\"{$product['productName']}\"". " නැමති  නිෂ්පාදනය අවම ඉන්වෙන්ටරි මට්ටමට ළඟා වී ඇත.");
            $data['notificationBody_tl'] = json_encode("உருப்படி \"{$product['productName']}\"". " குறைந்தபட்ச அளவு நிலையை அடைந்துள்ளது.");
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime();
            $data['status'] = "0";
            $newNotificaitons[] = $data;

            $filter_keys = ['notificationReferID', 'locationID', 'notificationType', 'status'];

//          check the same notificaiton is already exists
            $matching = array_filter($existingNotifications, function($curr) use ($data, $filter_keys) {
                $prep = array_intersect_key($curr, array_flip($filter_keys));
                $prepVal = array_intersect_key($data, array_flip($filter_keys));
                ksort($prep);
                ksort($prepVal);

                return json_encode($prep) == json_encode($prepVal);
            });

            if (!count($matching)) {
                $notification = new Notification();
                $notification->exchangeArray($data);
                $notificationID =  $this->CommonTable('Core\Model\NotificationTable')->saveNotification($notification);

            }
        }

//      Update the existing notificaitons
        foreach ($existingNotifications as $existingNotification) {
            $filter_keys = ['notificationReferID', 'locationID', 'notificationType', 'status'];

//          check the same notificaiton is already exists
            $matching = array_filter($newNotificaitons, function($curr) use ($existingNotification, $filter_keys) {
                $prep = array_intersect_key($curr, array_flip($filter_keys));
                $prepVal = array_intersect_key($existingNotification, array_flip($filter_keys));
                ksort($prep);
                ksort($prepVal);
                return json_encode($prep) == json_encode($prepVal);
            });

            if (!count($matching)) {
                $notification = new Notification();
                $notification->exchangeArray($existingNotification);
                $notification->status = 1;
                $this->CommonTable('Core\Model\NotificationTable')->changeStatus($notification);
            }
        }
    }

    /**
     * Check given Products quantity has been reached to location product inventory reorder level
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param EventInterface $event
     *  - Event should have product IDs and LocationIDs in an array
     */
    public function checkReorederLevelOfProduct(EventInterface $event)
    {
        $productIDs = $event->getParam('productIDs');
        $locationIDs = $event->getParam('locationIDs');

        $reorderLevelProducts = $this->CommonTable('Inventory\Model\LocationProductTable')->getInventoryLevelProducts($productIDs, $locationIDs, false, true);
        $notificationType = "3";
        $existingNotifications = $this->CommonTable('Core\Model\NotificationTable')->getOpenNotification($productIDs, $locationIDs, $notificationType);

//        Inserting new Notificaitons
        $newNotificaitons = [];
        foreach ($reorderLevelProducts as $product) {
            $data['notificationReferID'] = $product['productID'];
            $data['locationID'] = $product['locationID'];
            $data['notificationType'] = "3";
            $data['notificationTitle'] = 'Have Reached Reorder Level';
            $data['notificationBody'] = "Item \"{$product['productName']}\" "
                    . " has reached Reorder level.";
            $data['notificationBody_si'] = json_encode("\"{$product['productName']}\"". " නැමති  නිෂ්පාදනය නැවත ඇනවුම් කිරීමේ මට්ටමට ළඟාවි ඇත.");
            $data['notificationBody_tl'] = json_encode("உருப்படி \"{$product['productName']}\""." மறு ஒழுங்கு நிலையை அடைந்துள்ளது.");
            $data['notificationUpdatedDateTime'] = $this->getGMTDateTime();
            $data['status'] = "0";
            $newNotificaitons[] = $data;

            $filter_keys = ['notificationReferID', 'locationID', 'notificationType', 'status'];

//          check the same notificaiton is already exists
            $matching = array_filter($existingNotifications, function($curr) use ($data, $filter_keys) {
                $prep = array_intersect_key($curr, array_flip($filter_keys));
                $prepVal = array_intersect_key($data, array_flip($filter_keys));
                ksort($prep);
                ksort($prepVal);

                return json_encode($prep) == json_encode($prepVal);
            });

            if (!count($matching)) {
                $notification = new Notification();
                $notification->exchangeArray($data);
                $this->CommonTable('Core\Model\NotificationTable')->saveNotification($notification);
            }
        }

//      Update the existing notificaitons
        foreach ($existingNotifications as $existingNotification) {
            $filter_keys = ['notificationReferID', 'locationID', 'notificationType', 'status'];

//          check the same notificaiton is already exists
            $matching = array_filter($newNotificaitons, function($curr) use ($existingNotification, $filter_keys) {
                $prep = array_intersect_key($curr, array_flip($filter_keys));
                $prepVal = array_intersect_key($existingNotification, array_flip($filter_keys));
                ksort($prep);
                ksort($prepVal);
                return json_encode($prep) == json_encode($prepVal);
            });

            if (!count($matching)) {
                $notification = new Notification();
                $notification->exchangeArray($existingNotification);
                $notification->status = 1;
                $this->CommonTable('Core\Model\NotificationTable')->changeStatus($notification);
            }
        }
    }

    private function generateChequeNotificationMsg( $amount, $date, $bank, $reference, $type, $lang = 'en')
    {
        $msg = null;
        switch ($type) {
            case 'incoming':

                if ($lang == 'en') {
                    $msg = "You have an incoming cheque, it need to deposit on {$date}";
                    if($date && $bank && $reference){
                        $msg = "Cheque \"{$reference}\" of \"{$bank}\" need to deposit on \"{$date}\".";
                    }

                } elseif ($lang == 'si') {
                    $msg = "ඔබට චෙක්පතක් ලැබී තිබෙ එය \"{$date}\" දින තැන්පත් කල යුතුය.";
                    if($date && $bank && $reference){
                        $msg = "ඔබට \"{$bank}\" යන බැංකුවෙන් \"{$reference}\" අංකය දරන චෙක්පතක් ලැබී තිබෙ එය \"{$date}\" දින තැන්පත් කල යුතුය.";
                    }
                } elseif ($lang == 'tl') {
                    $msg = "உள்வந்த காசோலை ஒன்று உம்மிடம் உள்ளது. இது \"{$date}\" அன்று டெபாசிட் செய்யப்பட வேண்டும்";
                    if($date && $bank && $reference){
                        $msg = "\"{$bank}\" காசோலை \"{reference}\" {$date} ன்று வைப்பில் இட வேண்டும்.";
                    }
                }

                break;


            case 'outgoing':
                if ($lang == 'en') {
                    $msg = "You have an outgoing cheque, it will present on {$date}";
                    if($date && $bank && $reference){
                        $msg = "Cheque \"{$reference}\" of \"{$bank}\" will present on \"{$date}\".";
                    }
                } elseif ($lang == 'si') {
                    $msg = "ඔබ සතුව පිටතට යන චෙක්පතක් තිබේ, එය \"{$date}\" යන දින ප්‍රධානය කල යුතුය.";
                    if($date && $bank && $reference){
                        $msg = "ඔබ සතුව \"{$bank}\" යන බැංකුවෙන් යන චෙක්පතක් තිබේ, එය \"{$date}\" යන දින ප්‍රධානය කල යුතුය.";
                    }
                } elseif ($lang == 'tl') {
                    $msg = "உங்களிடம் வெளிச்செல்லும் காசோலை உள்ளது, இது \"{$date}\" அன்று வழங்கப்பட வேண்டும்";
                    if($date && $bank && $reference){
                        $msg = "Cheque \"{$reference}\" of \"{$bank}\" will present on \"{$date}\".";
                    }
                }
                break;

            default:
            error_log('invalid request');
        }
        return $msg;
    }


    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * this function use to update show state value
     * @return type
     */
    public function updateShowStateAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = "error";
            return $this->JSONRespond();
        } else {
            $notificationID = $request->getPost('notificID');
            $updateShowState = $this->CommonTable('Core\Model\NotificationTable')->updateShowState($notificationID);
            if ($updateShowState) {
                $this->status = true;
                $this->msg = $this->getMessage("SUCESS_DELETE_NOTIFIC");
                return $this->JSONRespond();
            }
        }
    }

    public function pushNotificationsToMobile($notificationID)
    {
        try {
            $notifications = $this->CommonTable('Core\Model\NotificationTable')->getNotificationByID($notificationID)->current();
            $location = $this->CommonTable('Settings\Model\LocationTable')->getLocationByID($notifications['locationID']);
            $domain = $this->getSubdomain();
            $currentNotification = [
                'title' => $notifications['notificationTitle'],
                'body' => 'In '.$location->locationName.' location , '. $notifications['notificationBody'],
                'date' => 'now',
                'id' =>  $notifications['notificationID']
            ];

            $options = array(
                'cluster' => 'ap2',
                'useTLS' => true
              );
            $pusher = new Pusher\Pusher(
                'a32520c41be7d5dc59cf',
                'c1ccf3eb50bcb494002a',
                '734542',
                $options
            );
        
            $pusher->trigger($domain, 'my-event', $currentNotification);
                    

        } catch (\Exception $e) {
            error_log($e->getMessage());
        }
    }

    /**
     * @param  EventInterface
     */
    public function updatePosClientInventory(EventInterface $event)
    {
        try {

            $productIDs = $event->getParam('productIDs');

            $locationIDs = $event->getParam('locationIDs');

            $action = $event->getParam('action');

            if ($action && $action == 'deleted') {
                foreach ($productIDs as $id) {
                    $products[$id]['action'] = 'deleted';
                }
            }else {
                $products = $this->CommonTable('Core\Model\ProductTable')->getProcessedActiveProducts($locationIDs, $productIDs);

                foreach ($products as $key => $value) {
                    $barcodes = $this->CommonTable('Inventory\Model\itemBarcodeTable')->getRelatedBarCodesByProductID($value['pID'], true);
                    $products[$key]['barcodes'] = $barcodes;
                }
            }

            $pusher = $this->getService('PusherService');

            $pusher->push($products);

        } catch (\Exception $e) {
            error_log($e->getMessage());
        }

    }
}
