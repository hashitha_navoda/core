<?php

namespace Core\Controller;

use Core\Controller\CoreController;
use Core\Model\Log;
use Core\Model\LogTable;
use Core\Model\LogDetails;
use Core\Model\LogDetailsTable;

/**
 * @author Malitta Nanayakkara <malitta@thinkcube.com>
 */
class LogController extends CoreController
{

    private $message = null;
    private $featureID = "";
    private $featureType = "";
    private $logDetailsArray = [];

    public function setMessage($message)
    {
        $this->message = $message;
    }

    public function setFeatureType($featureType)
    {
        $this->featureType = $featureType;
    }

    public function setFeatureID($featureID)
    {
        $this->featureID = $featureID;
    }

    public function getFeatureID()
    {
        return $this->featureID;
    }

     public function setLogDetailsArray($previousData,$newData)
    {
        $this->logDetailsArray[] = array('previousData' =>$previousData, 'newData' => $newData);
    }


    public function saveLog()
    {

        if ($this->userID) {

            $data = array(
                'featureID' => $this->featureID,
                'logMessage' => $this->message,
                'logTimeStamp' => $this->getGMTDateTime(),
                'logUserID' => $this->userID,
                'logIP' => isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : NULL,
                'logUserAgent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : NULL,
            );

            $logModel = new Log();
            $logModel->exchangeArray($data);
            $logID = $this->CommonTable('Core\Model\LogTable')->saveLog($logModel);

            if($logID){
                foreach ($this->logDetailsArray as $key => $value) {
                    $logDetailsData = array(
                        'logID' => $logID,
                        'previousData' => $value['previousData'],
                        'newData' => $value['newData'],
                    );
                    $logDetailsModel = new LogDetails();
                    $logDetailsModel->exchangeArray($logDetailsData);
                    $this->CommonTable('Core\Model\LogDetailsTable')->saveLogDetails($logDetailsModel);
                }
            }

            //feature validation is cancelled - login/logout doesn't have features
//            if ($this->featureType == '0' || ($this->featureType == '1' && $this->message != '')) {
//            }
        }
    }

}

?>
