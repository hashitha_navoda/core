<?php

namespace Core\Service;

use Zend\I18n\Translator\Translator;
use Zend\View\Model\ViewModel;
use Core\Model\Notification;

class ReportService extends BaseService
{
    const PDF_REPORT = 'PDF';

    const CSV_REPORT = 'CSV';

    /**
     * Generate CSV file
     */
    public function generateCSVFile($reportName, $csvFile)
    {
        $uniqueName = $reportName . preg_replace("/[^0-9]/", "", microtime(true)) . uniqid();

        $docPath = $this->getCompanyDataFolder() . '/documents/reports/csv';

        if (!is_dir($docPath)) {
            mkdir($docPath, 0777, true);
        }

        //string put in to csv file and save it in local hard in specific location
        file_put_contents($docPath . '/' . $uniqueName . '.csv', $csvFile);

        //get path where file has been saved in local machine
        $browserPath = str_replace(getcwd() . '/public/', '', $docPath) . '/' . $uniqueName . '.csv';

        return $uniqueName . '.csv';
    }

    /**
     * Download html content as PDF
     */
    public function downloadPDF($reportName, $htmlContent, $returnPath = false)
    {
        $uniqueName = $reportName . preg_replace("/[^0-9]/", "", microtime(true)) . uniqid();

        $docPath = $this->getCompanyDataFolder() . '/documents/reports/pdf';

        if (!is_dir($docPath)) {
            mkdir($docPath, 0777, true);
        }

        //string put in to html file and save it in local hard
        file_put_contents($docPath . '/' . $uniqueName . '.html', $htmlContent);

        // create unique file name and convert html to pdf using wkhtmltopdf
        $this->convertToPDF($docPath . '/' . $uniqueName . '.html', $docPath . '/' . $uniqueName . '.pdf');

        //get path where file has been saved in local machine
        $browserPath = str_replace(getcwd() . '/public/', '', $docPath) . '/' . $uniqueName . '.pdf';

        return $uniqueName . '.pdf';
    }

    /**
     * Get csv content
     */
    public function csvContent($title, $header, $arr)
    {
        return $title . $header . "\n" . $arr;
    }
    
    /**
     * Render report header html
     */
    public function headerViewTemplate($name, $period)
    {
        $translator = new Translator();

        $headerView = new ViewModel([
            'period' => $period,
            'reportName' => $translator->translate($name),
            'cD' => $this->getcompanyDetails(),
            'dateTime' => $this->getUserDateTime(false, 'Y-M-d h:i:s A'),
        ]);
        
        return $headerView;
    }

    /**
     * Render a view model object.
     */
    public function htmlRender($view)
    {
        return $this->getServiceLocator()->get('viewrenderer')->render($view);
    }

    /**
     * Render html content
     */
    public function viewRendererHtmlContent($htmlFile)
    {
        $viewRender = $this->getServiceLocator()->get('ViewRenderer');
        $htmlContent = $viewRender->render($htmlFile);

        return $htmlContent;
    }

    /**
     * Convert HTML file to PDF
     */
    public function convertToPDF($sourceFile, $destinationFile)
    {
        exec('wkhtmltopdf.sh ' . $sourceFile . ' ' . $destinationFile);

        return (file_exists($destinationFile));
    }

    /**
     * Send report notification
     */
    public function sendNotification($reportName, $reportType, $referenceId, $requestData)
    {
        try {
            $notification = new Notification();
            $notification->notificationReferID = strtolower($reportType) . "/" . $referenceId;
            $notification->locationID = $requestData['requestLocationId'];
            $notification->notificationType = 10;
            $notification->status = 0;
            $notification->notificationTitle = 'Report has been generated';
            $notification->notificationBody = "{$reportName} {$reportType} report has been generated.";
            $notification->notificationBody_si = json_encode("{$reportName} යන {$reportType} වාර්තාව ජනනය කරන ලදී .");
            $notification->notificationBody_tl = json_encode("{$reportName} {$reportType} அறிக்கை உருவாக்கப்பட்டது");
            $notification->userID = $requestData['requestUserId'];
            $notification->notificationUpdatedDateTime = $requestData['requestDateTime'];
            $notificationId = $this->getModel('NotificationTable')->saveNotification($notification);
            if ($notificationId) {
                return true;
            }
            return false;
        } catch (\Exception $ex) {
            return false;
        }
    }
}
