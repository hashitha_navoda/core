<?php
/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Reference related services
 */
namespace Core\Service;

use Core\Contracts\SearchableFromCode;
use Core\Service\BaseService;

class ReferenceService extends BaseService
{

    public function getReferenceNumber($referenceID, $locationID = NULL)
    {
        $refNumber = NULL;
        $locationRefID = NULL;

        $referenceType = $this->getModel('ReferenceTable')->getReferenceType($referenceID);
        //if reference type equla it mean the reference number is global then assignn locationID as null
        if ($referenceType == 0) {
            $locationID = NULL;
        }
        //get location reference prefix data by documentReferenceID and location ID
        $referenceData = $this->getLocationReferenceDetails($referenceID, $locationID);
        if ($referenceData) {
            //get document reference number
            $refNumber = $this->getReferencePrefixNumber($referenceData->locationReferenceID);
            $locationRefID = $referenceData->locationReferenceID;
        }
        return $this->returnSuccess(['referenceNo' => $refNumber, 'locationReferenceID' => $locationRefID]);
    }

    public function checkAndGetNextRef($referenceCode, $locationID, $locationReferenceID, SearchableFromCode $service)
    {        
        if (!$locationReferenceID) {
            return $this->returnError('CODE_EXIST');
        }

        while ($service->codeExists($referenceCode, $locationID)['data']['exists']) {
            // get next reference number             
            $newReferenceCode = $this->getReferencePrefixNumber($locationReferenceID);
            //check the reference code match whith loaded document reference code
            if ($referenceCode == $newReferenceCode) {
                //update the reference sequense by 1
                $res = $this->updateReferenceNumber($locationReferenceID);
                
                if($res['status'] == false){
                    return $this->returnError('UPDATE_ERROR');
                }
                //get new reference number
                $referenceCode = $this->getReferencePrefixNumber($locationReferenceID);
            } else {
                $referenceCode = $newReferenceCode;
            }
             
        }
        return $this->returnSuccess(['referenceCode' => $referenceCode]);
    }

    public function updateReferenceNumber($LocationReferenceID)
    {   
        //get document reference details by location reference id
        $reference =  $this->getModel('ReferencePrefixTable')->getReferenceByLocationReferenceID($LocationReferenceID);
        $data = array(
            'referencePrefixCurrentReference' => $reference->currentReference + 1,
            );
        //update the document reference id by location reference id
        $res = $this->getModel('ReferencePrefixTable')->updateReferenceByID($data, $LocationReferenceID);

        if($res == false){
             return $this->returnError('UPDATE_ERROR');
        }

        return $this->returnSuccess($res);

    }

    public function getReferencePrefixNumber($locationReferenceID)
    {
        $referenceNumber = '';
        $reference = $this->getModel('ReferencePrefixTable')->getReferenceByLocationReferenceID($locationReferenceID);
        $numberLength = strlen($reference->currentReference);
        if ($numberLength <= $reference->numberOfDigits) {
            $string = sprintf("%0" . $reference->numberOfDigits . "d", $reference->currentReference);
            $referenceNumber = $reference->referenceType . $string;
        }
        return $referenceNumber;
    }

    protected function getLocationReferenceDetails($referenceID, $locationID)
    {
        return $this->getModel('ReferencePrefixTable')->getReference($referenceID, $locationID);
    }

    public function setDefaultReferences($documetReferenceData)
    {
        try {
            // get current references
            $existingReferenceIds = $this->getModel('ReferenceTable')->getExistingReferenceIds();

            foreach ($documetReferenceData as $referenceId => $reference) {

                if (in_array($referenceId, $existingReferenceIds)) {
                    continue;
                }

                $result = $this->getModel('ReferenceTable')->saveDefaultReference($reference['referenceName'], $reference['referenceTypeID'], $referenceId);
                if (!$result) {
                    return $this->returnError('Error occurred while saving default reference.');
                }

                $result = $this->getModel('ReferencePrefixTable')->saveDefaultReference($referenceId, $reference['referencePrefixCHAR'], $reference['referencePrefixNumberOfDigits'], $reference['referencePrefixCurrentReference']);
                if (!$result) {
                    return $this->returnError('Error occurred while saving default reference.');
                }

            }
            return $this->returnSuccess(null, 'Default references has been saved.');

        } catch (\Exception $ex) {
            return $this->returnError('Error occurred while saving default reference.');
        }
    }

}
