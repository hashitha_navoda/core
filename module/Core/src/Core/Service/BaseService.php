<?php

namespace Core\Service;

use Zend\Di\Di;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

class BaseService implements ServiceLocatorAwareInterface {
	protected $sm;

	protected $userSession;

	protected $companyCurrencySymbol;

	CONST NUMBER_OF_DEFAULT_DECIMAL_TO_ROUND = 10;
	CONST NUMBER_OF_DEFAULT_DECIMAL_POINTS = 2;

	public function __construct() {
		$this->user_session = new Container('ezBizUser');
		$this->timeZone = (isset($this->user_session->timeZone) && $this->user_session->timeZone) ? $this->user_session->timeZone : 'UTC';
		$this->locationID  = $this->user_session->userActiveLocation['locationID'];
	}

	/**
	 * Set serviceManager instance
	 *
	 * @param  ServiceLocatorInterface $serviceLocator
	 * @return void
	 */
	public function setServiceLocator(ServiceLocatorInterface $serviceLocator) {
		$this->sm = $serviceLocator;
	}

	public function setLogMessage($message)
    {
        $log = $this->getServiceLocator()->get('Log');
        return $log->setMessage($message);
    }

     public function setLogDetailsArray($previousData,$newData)
    {
        $log = $this->getServiceLocator()->get('Log');
        return $log->setLogDetailsArray($previousData,$newData);
    }

	/**
	 * Retrieve serviceManager instance
	 *
	 * @return ServiceLocatorInterface
	 */
	public function getServiceLocator() {
		return $this->sm;
	}

	/**
     * Return status Code
     * @param type $statusCode
     */
    public function getStatusCode($statusID)
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $statuses = $globaldata['statuses'];
        return $statuses[$statusID];
    }

	protected function returnSuccess($data = null, $msg = '') {
		return [
			'status' => true,
			'data' => $data,
			'msg' => $msg,
		];
	}

	protected function returnError($msg, $data = null) {
		return [
			'status' => false,
			'data' => $data,
			'msg' => $msg,
		];
	}

	
    protected function getLocationReferenceDetails($referenceID, $locationID)
    {
        $result = $this->getModel('ReferencePrefixTable')->getReference($referenceID, $locationID);
        return $result;
    }

	public function getReferenceNoForLocation($referenceID, $locationID)
    {
        $refNumber = NULL;
        $locationRefID = NULL;
        $referenceType = $this->getModel('ReferenceTable')->getReferenceType($referenceID);

        if ($referenceType == 0) {
            $referenceData = $this->getLocationReferenceDetails($referenceID, NULL);
        } else if ($referenceType == 1) {
            $referenceData = $this->getLocationReferenceDetails($referenceID, $locationID);
        } else {
            return FALSE;
        }
        if ($referenceData) {
            $locationRefID = $referenceData->locationReferenceID;
            $refNumber = $this->getReferenceNumber($referenceData->locationReferenceID);
        } else {
            $refNumber = NULL;
            $locationRefID = NULL;
        }
        return array(
            'refNo' => $refNumber,
            'locRefID' => $locationRefID,
        );
    }

    /**
     * get reference number by using location reference id
     */
    protected function getReferenceNumber($referencePrefixID)
    {
        $reference = $this->getModel('ReferencePrefixTable')->getReferenceByLocationReferenceID($referencePrefixID);
        $numberLength = strlen($reference->currentReference);
        if ($numberLength <= $reference->numberOfDigits) {
            $string = sprintf("%0" . $reference->numberOfDigits . "d", $reference->currentReference);
            $referenceNumber = $reference->referenceType . $string;
        } else {
            $referenceNumber = '';
        }

        return $referenceNumber;
    }

	protected function getModel($table) {

		if (!isset($models[$table])) {
			$models[$table] = $this->getServiceLocator()->get($table);
		}

		return $models[$table];
	}

	protected function getService($service) {
		return $this->getServiceLocator()->get($service);
	}

	protected function convertDateToStandardFormat($date) {
		if ($date) {
			$dateformat = $this->getUserDateFormat()['data']['userDateFormat'];
			if ($dateformat === 'yyyy/mm/dd' || $dateformat === 'yyyy-mm-dd' || $dateformat === 'yyyy.mm.dd') {
				$symbols = array('/', '.');
				$date = str_replace($symbols, '-', $date);
				return date("Y-m-d", strtotime($date));
			} else if ($dateformat == 'dd/mm/yyyy' || $dateformat == 'dd-mm-yyyy' || $dateformat == 'dd.mm.yyyy') {
				$symbols = array('/', '.');
				$date = str_replace($symbols, '-', $date);
				return date("Y-m-d", strtotime($date));
			} else {
				$symbols = array('-', '.');
				$date = str_replace($symbols, '/', $date);
				return date('Y-m-d', strtotime($date));
			}
		}
	}

	protected function getUserDateFormat() {
		$displaySetupService = $this->getService('DisplaySetupService');
		$res = $displaySetupService->getDisplaySetupData();
		return $this->returnSuccess(['userDateFormat' => $res['data']['displaySetupData']->dateFormat]);
	}

	public function getGMTDateTime($format = 'Y-m-d H:i:s', $timestamp = false) {
		if ($timestamp) {
			$userDateFormat = $this->convertUserDateFormatToPhpFormat()['data']['dateformat'];
			$date = \DateTime::createFromFormat($userDateFormat . " H:i", $timestamp);
			$currentTime = gmdate('Y-m-d H:i:s', strtotime($date->format('Y-m-d H:i:s')));
		} else {
			$currentTime = gmdate($format);
		}
		return $this->returnSuccess(['currentTime' => $currentTime]);
	}

	protected function convertUserDateFormatToPhpFormat() {
		$displaySetupService = $this->getService('DisplaySetupService');
		$res = $displaySetupService->getDisplaySetupData();

		switch ($res['data']['displaySetupData']->dateFormat) {
		case 'yyyy/mm/dd':
			$format = 'Y/m/d';
			break;
		case 'yyyy-mm-dd':
			$format = 'Y-m-d';
			break;
		case 'yyyy.mm.dd':
			$format = 'Y.m.d';
			break;
		case 'dd/mm/yyyy':
			$format = 'd/m/Y';
			break;
		case 'dd-mm-yyyy':
			$format = 'd-m-Y';
			break;
		case 'dd.mm.yyyy':
			$format = 'd.m.Y';
			break;
		case 'mm/dd/yyyy':
			$format = 'm/d/Y';
			break;
		case 'mm-dd-yyyy':
			$format = 'm-d-Y';
			break;
		case 'mm.dd.yyyy':
			$format = 'm.d.Y';
			break;
		}

		return $this->returnSuccess(['dateformat' => $format]);
	}

	/**
	 * Get sub domain
	 */
	public function getSubdomain() {
		$serverName = $this->getServiceLocator()->get('Config')['servername'];
		$sitePath = $_SERVER['HTTP_HOST']; // getRequest cannot be used because of context restrictions ($this)

		$companyName = '';
		if (strpos($sitePath, $serverName) > 0) {
			// get unique company name from subdomain
			$companyName = end(explode('.', trim(str_replace($serverName, '', $sitePath), '.')));
		} else if ($sitePath == $serverName) {
			// no subdomains are available (local enviroment)
			$companyName = 'ezbiz';
		} else {
			$companyName = 'ezbiz';
			//die('config.sitepath not set');
		}
		return $companyName;
	}

	/**
	 * Get company data folder
	 */
	public function getCompanyDataFolder() {
		$companyName = $this->getSubdomain();

		$companyFolder = ROOT_PATH . '/public/userfiles/' . md5($companyName);

		// make directory if it doesnt exist
		if (!is_dir($companyFolder)) {
			mkdir($companyFolder, 0777, true);
		}

		return $companyFolder;
	}

	/**
	 * Get company details
	 */
	public function getCompanyDetails() {
		$companyDetails = $this->getModel('CompanyTable')->fetchAll();

		foreach ($companyDetails as $c) {
			$company[] = $c;
		}
		return $company;
	}

	/**
	 * For current date time according to user specified time zone
	 *
	 * @param type $fromTime
	 * @param type $format
	 * @return string
	 */
	public function getUserDateTime($fromTime = false, $format = 'Y-m-d H:i:s') {
		try {
			$fromTime = (!$fromTime) ? gmdate($format) : $fromTime;
            $from = new \DateTimeZone('GMT');
            $to = new \DateTimeZone($this->timeZone);
            $orgTime = new \DateTime($fromTime, $from);
            $toTime = new \DateTime($orgTime->format("c"));
            $toTime->setTimezone($to);

			return $toTime->format($format);
		} catch (\Exception $e) {
			return '-';
		}
	}

	public function getProductQuantityViaDisplayUom($quantity, $uomList) {
		$noOfUoms = count($uomList);
		if ($noOfUoms == 0) {
			$displayUomDetails['quantity'] = $quantity;
			$displayUomDetails['uomAbbr'] = Null;
		} else if ($noOfUoms == 1) {
//if only having
			$displayUomDetails['quantity'] = $quantity / $uomList[0]['productUomConversion'];
			$displayUomDetails = array_merge($displayUomDetails, $uomList[0]);
		} else if ($noOfUoms > 1) {

//get index of display uom with have true value
			$displayUomKey = array_search(1, array_column($uomList, 'productUomDisplay'));

			if (is_int($displayUomKey)) {
				$displayUomDetails['quantity'] = $quantity / $uomList[$displayUomKey]['productUomConversion'];
				$displayUomDetails = array_merge($displayUomDetails, $uomList[$displayUomKey]);
			} else {
//if not set display uom get base uom details
				$baseUomKey = array_search('1', array_column($uomList, 'productUomConversion'));
				$displayUomDetails['quantity'] = $quantity;
				$displayUomDetails = array_merge($displayUomDetails, $uomList[$baseUomKey]);
			}
		}
		return $displayUomDetails;
	}

	public function getProductUnitPriceViaDisplayUom($unitPrice, $uomList) {
		$conversionRate = 0.00;
		foreach ($uomList as $value) {
			$value = (object) $value;
			if ($value->productUomDisplay == 1) {
				$conversionRate = $value->productUomConversion;
				break;
			} else {
				$conversionRate = ($conversionRate < $value->productUomConversion) ? $value->productUomConversion : $conversionRate;
			}
		}
		$pUnitPrice = round(($unitPrice * $conversionRate), self::NUMBER_OF_DEFAULT_DECIMAL_TO_ROUND);
		$pUnitPrice = number_format($pUnitPrice, $this->getNumberOfDecimalPoints($pUnitPrice), '.', '');
		return $pUnitPrice;
	}

	private function getNumberOfDecimalPoints($number) {
		$decimals = self::NUMBER_OF_DEFAULT_DECIMAL_POINTS;

		$length = 0;
		$string = strrchr((float) $number, ".");
		if ($string != false) {
			$length = strlen(substr($string, 1));
		}
		return ($length < $decimals) ? $decimals : $length;
	}

	/**
	 * This function is used to get location product details of product
	 * @param $locationID
	 * @param $productID
	 * @return $location productDta
	 */
	public function getLocationProductDetails($locationID, $productID) {

		$locationProduct = $this->getModel('ProductTable')->getLocationProductDetailsByLPID($locationID, $productID);
		$products = array();
		foreach ($locationProduct as $row) {
			$tempP = array();
			$tempP['pC'] = $row['productCode'];
			$tempP['pID'] = $row['productID'];
			$tempP['lPID'] = $row['locationProductID'];
			$tempP['pN'] = $row['productName'];
			$tempP['LPQ'] = $row['locationProductQuantity'];
			$tempP['bP'] = $row['batchProduct'];
			$tempP['sP'] = $row['serialProduct'];
			$tempP['dSP'] = $row['defaultSellingPrice'];
			$tempP['dPP'] = $row['defaultPurchasePrice'];
			$tempP['dEL'] = $row['productDiscountEligible'];
			$tempP['dPR'] = $row['locationDiscountPercentage'];
			$tempP['dV'] = $row['locationDiscountValue'];
			$tempP['pT'] = $row['productTypeID'];
			$tempP['dPEL'] = $row['productPurchaseDiscountEligible'];
			//product default purchase discount percentage
			$tempP['pPDP'] = $row['productPurchaseDiscountPercentage'];
			//product default purchase discount value
			$tempP['pPDV'] = $row['productPurchaseDiscountValue'];
			$tempP['sales'] = false;
			$tempP['purchase'] = false;
			$tempP['giftCard'] = $row['productHandelingGiftCard'];
			//product tax eligible flag
			$tempP['proTaxFlag'] = $row['productTaxEligible'];

			if ($row['productHandelingManufactureProduct'] == 1 || $row['productHandelingSalesProduct'] == 1) {
				$tempP['sales'] = true;
			}

			if ($row['productHandelingManufactureProduct'] == 1 || $row['productHandelingPurchaseProduct'] == 1 || $row['productHandelingConsumables'] == 1 || $row['productHandelingFixedAssets'] == 1) {
				$tempP['purchase'] = true;
			}

			$tax = (isset($products[$row['productID']]['tax'])) ? $products[$row['productID']]['tax'] : array();
			if ($row['productTaxEligible'] == 1) {
				if ($row['taxID'] != NULL) {
					$tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state']);
				}
			}
			$uom = (isset($products[$row['productID']]['uom'])) ? $products[$row['productID']]['uom'] : array();
			if ($row['uomID'] != NULL) {
				$uom[$row['uomID']] = array('uA' => $row['uomAbbr'], 'uN' => $row['uomName'], 'uDP' => $row['uomDecimalPlace'], 'uS' => $row['uomState'], 'uC' => $row['productUomConversion'], 'pUI' => $row['productUomID'], 'uomID' => $row['uomID'], 'pUDisplay' => $row['productUomDisplay']);
			}

			$batchSerial = (isset($products[$row['productID']]['batchSerial'])) ? $products[$row['productID']]['batchSerial'] : array();
			$batchIDsInBatchSerial = (isset($products[$row['productID']]['productIDs'])) ? $products[$row['productID']]['productIDs'] : array();
			if ($row['serialProductBatchID'] != NULL && $row['serialProductBatchID'] == $row['productBatchID'] && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {
				$batchSerial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'], 'PBSWoD' => $row['productSerialWarrantyPeriod'], 'PBSExpD' => $row['productSerialExpireDate'], 'PBUP' => $row['productBatchPrice']);
				if ($row['productBatchID'] != NULL) {
					$batchIDsInBatchSerial[$row['productBatchID']] = array('PBID' => $row['productBatchID']);
				}
			}

			$batch = (isset($products[$row['productID']]['batch'])) ? $products[$row['productID']]['batch'] : array();
			if ($row['productBatchID'] != NULL && $row['serialProductBatchID'] != $row['productBatchID']) {
				$batch[$row['productBatchID']] = array('PBID' => $row['productBatchID'], 'PBC' => $row['productBatchCode'], 'PBQ' => $row['productBatchQuantity'], 'PSID' => $row['productSerialID'], 'PBExpD' => $row['productBatchExpiryDate'], 'PBWoD' => $row['productBatchWarrantyPeriod'], 'PBUP' => $row['productBatchPrice']);
			}

			$serial = (isset($products[$row['productID']]['serial'])) ? $products[$row['productID']]['serial'] : array();
			if ($row['productSerialID'] != NULL && $row['serialProductBatchID'] == NULL && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {

				$serial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'], 'PSWoD' => $row['productSerialWarrantyPeriod'], 'PSExpD' => $row['productSerialExpireDate']);
			}

			$tempP['tax'] = $tax;
			$tempP['uom'] = $uom;
			$tempP['batch'] = $batch;
			$tempP['serial'] = $serial;
			$tempP['batchSerial'] = $batchSerial;
			$tempP['productIDs'] = $batchIDsInBatchSerial;
			$productID = $row['productID'];
			$products[$row['productID']] = $tempP;
		}
		return $products[$productID];
	}

    public function getLocationProductDetailsForReturn($locationID, $productID, $invalidFlag = false)
    {
    
		$productsList = $this->getModel('ProductTable')->getActiveProductListByLocation($locationID, $productID, false,$invalidFlag);
        $products = array();
        foreach ($productsList as $row) {
            if ($row['productSerialReturned'] == 0) {
                $tempP = array();
                $tempP['pC'] = $row['productCode'];
                $tempP['pID'] = $row['productID'];
                $tempP['lPID'] = $row['locationProductID'];
                $tempP['pN'] = $row['productName'];
                $tempP['LPQ'] = $row['locationProductQuantity'];
                $tempP['bP'] = $row['batchProduct'];
                $tempP['sP'] = $row['serialProduct'];
                $tempP['dSP'] = $row['defaultSellingPrice'];
                $tempP['dPR'] = $row['locationDiscountPercentage'];
                $tempP['dEL'] = $row['productDiscountEligible'];
                $tempP['dV'] = $row['locationDiscountValue'];
                $tempP['giftCard'] = $row['productHandelingGiftCard'];

                $tax = (isset($products[$row['productID']]['tax'])) ? $products[$row['productID']]['tax'] : array();
                if ($row['productTaxEligible'] == 1) {
                    if ($row['taxID'] != NULL) {
                        $tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state']);
                    }
                }
                $uom = (isset($products[$row['productID']]['uom'])) ? $products[$row['productID']]['uom'] : array();
                if ($row['uomID'] != NULL) {
                    $uom[$row['uomID']] = array('uA' => $row['uomAbbr'], 'uN' => $row['uomName'], 'uDP' => $row['uomDecimalPlace'], 'uS' => $row['uomState'], 'uC' => $row['productUomConversion'], 'pUI' => $row['productUomID'], 'pUDisplay' => $row['productUomDisplay']);
                }

                $batchSerial = (isset($products[$row['productID']]['batchSerial'])) ? $products[$row['productID']]['batchSerial'] : array();
                $batchIDsInBatchSerial = (isset($products[$row['productID']]['productIDs'])) ? $products[$row['productID']]['productIDs'] : array();
                if ($row['serialProductBatchID'] != NULL && $row['serialProductBatchID'] == $row['productBatchID']) {
                    $batchSerial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'], 'PBSWoD' => $row['productSerialWarrantyPeriod'], 'PBSExpD' => $row['productSerialExpireDate'], 'PBUP' => $row['productBatchPrice']);
                    if ($row['productBatchID'] != NULL) {
                        $batchIDsInBatchSerial[$row['productBatchID']] = array('PBID' => $row['productBatchID']);
                    }
                }

                $batch = (isset($products[$row['productID']]['batch'])) ? $products[$row['productID']]['batch'] : array();
                if ($row['productBatchID'] != NULL && $row['serialProductBatchID'] != $row['productBatchID']) {
                    $batch[$row['productBatchID']] = array('PBID' => $row['productBatchID'], 'PBC' => $row['productBatchCode'], 'PBQ' => $row['productBatchQuantity'], 'PSID' => $row['productSerialID'], 'PBExpD' => $row['productBatchExpiryDate'], 'PBWoD' => $row['productBatchWarrantyPeriod'], 'PBUP' => $row['productBatchPrice']);
                }

                $serial = (isset($products[$row['productID']]['serial'])) ? $products[$row['productID']]['serial'] : array();
                if ($row['productSerialID'] != NULL && $row['serialProductBatchID'] == NULL) {

                    $serial[$row['productSerialID']] = array('PBID' => $row['serialProductBatchID'], 'PBC' => $row['productBatchCode'], 'PSID' => $row['productSerialID'], 'PSC' => $row['productSerialCode'], 'PSS' => $row['productSerialSold'], 'PSWoD' => $row['productSerialWarrantyPeriod'], 'PSExpD' => $row['productSerialExpireDate'], 'PSWoT' => $row['productSerialWarrantyPeriodType'],);
                }

                $tempP['tax'] = $tax;
                $tempP['uom'] = $uom;
                $tempP['batch'] = $batch;
                $tempP['serial'] = $serial;
                $tempP['batchSerial'] = $batchSerial;
                $tempP['productIDs'] = $batchIDsInBatchSerial;
                $products[$row['productID']] = $tempP;
            }
        }
        return $products[$productID];
    }

    protected function getController($controller)
    {
        return $this->getServiceLocator()->get($controller);
    }
	public function getStatusIDByStatusName($statusName) {
		$reference = $this->getModel('StatusTable')->getStatusByStatusName($statusName);
		$statusID = $reference->statusID;
		return $statusID;
	}

	public function getMaterialTypeID($materialTypeName) {
		$globaldata = $this->getServiceLocator()->get('config');
		$materialTypes = $globaldata['material_type'];
		return array_search($materialTypeName, $materialTypes);
	}

	public function getJobUserRoleID($userRoleName) {
		$globaldata = $this->getServiceLocator()->get('config');
		$jobUserRoles = $globaldata['job_user_roles'];
		return array_search($userRoleName, $jobUserRoles);
	}

	/**
     * Get current Status List on config
     * @author Sandun  <sandun@thinkcube.com>
     * @return $statuses
     */
    public function getStatusesList()
    {
        $globaldata = $this->getServiceLocator()->get('config');
        $statuses = $globaldata['statuses'];
        return $statuses;
    }
    
	 /**
     * @param type $dateformat
     * @param type $date
     * @return date
     */
    public function convertDateToUserFormat($date)
    {
        if ($date) {
            switch ($this->getUserDateFormat()['data']['userDateFormat']) {
                case 'yyyy/mm/dd':
                    return date('Y/m/d', strtotime($date));
                    break;
                case 'yyyy-mm-dd':
                    return date('Y-m-d', strtotime($date));
                    break;
                case 'yyyy.mm.dd':
                    return date('Y.m.d', strtotime($date));
                    break;
                case 'dd/mm/yyyy':
                    return date('d/m/Y', strtotime($date));
                    break;
                case 'dd-mm-yyyy':
                    return date('d-m-Y', strtotime($date));
                    break;
                case 'dd.mm.yyyy':
                    return date('d.m.Y', strtotime($date));
                    break;
                case 'mm/dd/yyyy':
                    return date('m/d/Y', strtotime($date));
                case 'mm-dd-yyyy':
                    $date = date('m/d/Y', strtotime($date));
                    return str_replace('/', '-', $date);
                    ;
                case 'mm.dd.yyyy':
                    return date('m.d.Y', strtotime($date));
            }
        }
    }

    /**
    * this function use to calculate item Tax
    * @param float $unitPrice
    * @param array $productTaxes
    * @param float $productQty
    * return array
    **/
    public function calculateItemCustomTax($unitPrice, $productId, $productQty) 
    {
	    if ($unitPrice == null) {
	        $unitPrice = 0;
	    }
	    $taxResults = [];
	    $Ptaxes = [];
	    $productTax = 0;
        $productTaxData =$this->getModel('ProductTaxTable')->getProTaxDetailsByProductID($productId);
        foreach ($productTaxData as $tax) {
        	$taxID = $tax['taxID'];
            if ($tax['state'] == 1) {
                if ($tax['taxType'] == 'v') {
                    $vTaxValue = (floatval($unitPrice) * floatval($tax['taxPrecentage']) / 100);
                    $totalItemTaxValue = floatval($vTaxValue) * floatval($productQty); 
                    $productTax += $totalItemTaxValue;
                    $Ptaxes[$taxID] = [
	                	'tN' => $tax['taxName'],
	                	'tP' => $tax['taxPrecentage'],
	                	'tA' => $totalItemTaxValue,
	                	'susTax' => $tax['taxSuspendable']
	                ];
                } else {
                    $simpleTaxAmount = 0;
                    $simpleTaxes = $this->getModel('TaxCompoundTable')->getSimpleTaxByCompoundTaxIdAndProductId($tax['taxID'], $productId);
                    foreach ($simpleTaxes as $sT) {
                        if ($sT['state'] == 1) {
                            $simpleTaxAmount = floatval($simpleTaxAmount) + (floatval($unitPrice) * floatval($sT['taxPrecentage']) / 100);
                        }
                    }
                    $compoundUnitPrice = floatval($simpleTaxAmount) + floatval($unitPrice); 
                    $compuntTaxAmount = floatval($compoundUnitPrice)* floatval($tax['taxPrecentage']) / 100;
                    $totalItemCompTax = $compuntTaxAmount * floatval($productQty); 
                    
                     $Ptaxes[$taxID] = [
	                	'tN' => $tax['taxName'],
	                	'tP' => $tax['taxPrecentage'],
	                	'tA' => $totalItemCompTax,
	                	'susTax' => $tax['taxSuspendable']
	                ];

                    $productTax = $productTax + $totalItemCompTax;
                }
            }
            $taxResults['tTA'] = $productTax;
	        $taxResults['tL'] = $Ptaxes;
        }
        return $taxResults;
	}

	public function timeAgo($time)
	{
		$diff = time() - strtotime($time);
			
		if ($diff < 1) {
			return 'now';
		} else if ($diff < 60) {
			return $diff.'seconds ago.';
		} else if ($diff < 3600) {
			$tmpTime = round($diff/60);
			return $tmpTime. ' minutes ago.';
		} else if ($diff < 86400) {
			$tmpTime = round($diff/3600);
			return $tmpTime.' hours ago.';
		} else if ($diff < 86400*30) {
			$tmpTime = round($diff/86400);
			return $tmpTime.' days ago.';
		} else {
			$time = split(" ", $time);
			return $time[0];
		}
		
	}

	/**
     * This function is used to send sms
     * @param $telephone number, $message
     */
    public function sendSms($telephoneNo, $message) 
    {
        $checkTele = !!preg_match('/^94\d{9}$/', "$telephoneNo");
        
        if (!$checkTele) {
            if (!!preg_match('/^\+94\d{9}$/', "$telephoneNo")) {
                $telephoneNo = ltrim($telephoneNo,'+');
            } else if (!!preg_match('/^0\d{9}$/', "$telephoneNo")) {
                $telephoneNo = ltrim($telephoneNo,'0');
                $telephoneNo = "94".$telephoneNo;
            } else if (!!preg_match('/\d{9}$/', "$telephoneNo")) {
                $telephoneNo = "94".$telephoneNo;
            } 
        }
        $checkTele = !!preg_match('/^94\d{9}$/', "$telephoneNo");
        
        if ($checkTele) {
            require_once('public/ESMSWS.php');
            $smsConfs = (object) $this->getModel('SmsConfigurationTable')->fetchAll();
            
            if ($smsConfs) {
                // Check the service provider
                if((int) $smsConfs->serviceProvider == 1){
                    $session = createSession('',$smsConfs->userName,$smsConfs->password,'');
                    $session_validity = isSession($session);
                    if ($session_validity) {
                        $msgRes = sendMessages($session,$smsConfs->alias,$message,array($telephoneNo),0); 
                        closeSession($session);
                        return $msgRes;
                    }
                }else if((int) $smsConfs->serviceProvider == 2){
                    $sms = $this->getServiceLocator()->get('SmsService');
                    $respond = $sms->sendMessage($smsConfs, $telephoneNo, $message);
                    return $respond;
                }
            }
        }
        return false;
    }
}