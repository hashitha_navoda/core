<?php

namespace Core\Service;

use Core\Service\BaseService;
use Core\Model\Entity;

class EntityService extends BaseService
{

    public function createEntity($createdBy)
    {  
    	$createdTimeStamp = $this->getGMTDateTime('Y-m-d H:i:s')['data']['currentTime'];
    	$entityData = array(
            'createdBy' => $createdBy,
            'createdTimeStamp' => $createdTimeStamp,
            'updatedBy' => $createdBy,
            'updatedTimeStamp' => $createdTimeStamp
        );
        $entityModel = new Entity();
        $entityModel->exchangeArray($entityData);
        $entityID = $this->getModel('EntityTable')->saveEntity($entityModel);
        return $this->returnSuccess(['entityID' => $entityID]);
    }

    public function updateDeleteInfoEntity($entityID, $createdBy)
    {
        $currentTime = $this->getGMTDateTime('Y-m-d H:i:s')['data']['currentTime'];
        $entitydata = array(
            'deleted' => 1,
            'deletedBy' => $createdBy,
            'deletedTimeStamp' => $currentTime
        );
        $return = $this->getModel('EntityTable')->updateEntity($entitydata, $entityID);
        return $return;
    }

    public function updateEntity($entityID, $createdBy)
    {
        $currentTime = $this->getGMTDateTime('Y-m-d H:i:s')['data']['currentTime'];
        $entitydata = array(
            'updatedBy' => $createdBy,
            'updatedTimeStamp' => $currentTime
        );
        $this->getModel('EntityTable')->updateEntity($entitydata, $entityID);
    }

}
