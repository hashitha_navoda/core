<?php
/**
 * Service class to handle notification related functions.
 *
 * @package     Core\Service
 * @link        git@code.thinkcube.net:project-leapx/leapx-portal.git
 * @copyright   Copyright (C) 2017 thinkCube Systems (Pvt) Ltd. All rights reserved.
 * @license     thinkCube Proprietary License
 * @author      Sahan Siriwardhan <sahan@thinkcube.com>
 *
 */


namespace Core\Service;

use Core\Service\BaseService;
use Core\Library\Util;

class SmsService extends BaseService
{

    public function sendMessage($smsConfs, $telephoneNo, $message)
    {
        $url = 'https://richcommunication.dialog.lk/api/sms/send';
        
        date_default_timezone_set('Asia/Colombo');
        $now = date("Y-m-d\TH:i:s");
        
        $username = $smsConfs->userName;
        $password = $smsConfs->password;
        $digest = md5($password);

        $headers = [
            'Content-Type: application/json',
            'USER: ' . $username,
            'DIGEST: ' . $digest,
            'CREATED: ' . $now
        ];
        
        $message = preg_replace( "/(\r\n)/", "\\n", $message );
        $message = preg_replace( "/(\n)/", "\\n", $message );
$messageBody  =<<<EOT
    {
            "messages": [
                {
                    "clientRef": "0934345",
                    "number": "$telephoneNo",
                    "mask": "$smsConfs->alias",
                    "text": "$message",
                    "campaignName":"ezBizPromo"
                }
            ]
        }
EOT;
        $response = Util::httpAlterRequest($url, 'POST', $messageBody, $headers, $options);
        
        if ($response) {
            return true;
        }
        return false;
    }


}