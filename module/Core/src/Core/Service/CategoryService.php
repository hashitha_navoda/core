<?php

namespace Core\Service;

use Core\Service\BaseService;
use Zend\EventManager\EventInterface;
use Zend\Session\Container;


class CategoryService extends BaseService
{ 


    public function getAllCategoriesForDesktopPOS()
    {
        $updated_at = $timestamp ? date('Y-m-d h:i:s', $timestamp) : null;
        $categoriesList = $this->getModel('CategoryTable')->fetchAll();
        $categories = array();
        while ($row = $categoriesList->current()) {
            $tempP = array();
            $tempP['cID'] = $row['categoryID'];
            $tempP['cN'] = $row['categoryName'];
            $tempP['cParentID'] = $row['categoryParentID'];
            $tempP['cLevel'] = $row['categoryLevel'];
            $tempP['cPrefix'] = $row['categoryPrefix'];
            $tempP['cState'] = $row['categoryState'];
            $categories[$row['categoryID']] = $tempP;
        }
        error_log(count($categories));

        return $this->returnSuccess($categories);
    }
}