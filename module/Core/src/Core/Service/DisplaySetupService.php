<?php

namespace Core\Service;

use Core\Service\BaseService;

class DisplaySetupService extends BaseService
{

    public function getDisplaySetupData()
    {  
        $displaySetupData = $this->getModel('DisplaySetupTable')->fetchAll()->current();
        
        return $this->returnSuccess(['displaySetupData' => $displaySetupData]);
    }

}
