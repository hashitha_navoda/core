<?php

namespace Core\Service;

use Core\Service\BaseService;

class CurrencyService extends BaseService 
{

    public function getCurrencyByCurrencyId($currencyId, $companyCurrencySymbol)
    {   
    	$currencyRate = 1;
    	$currencySymbol = $companyCurrencySymbol;

    	if(!$currencyId == null){
     		$res = $this->getModel('CurrencyTable')->getCurrency($currencyId);
     		$currencyRate = $res->currencyRate;
     		$currencySymbol = $res->currencySymbol;
    	}

    	return $this->returnSuccess([
    		'currencyRate' => $currencyRate, 
    		'currencySymbol' => $currencySymbol
		]);
    }

    public function getAllCurrency()
    {
    	$currencies = [];
    	foreach ($this->getModel('CurrencyTable')->fetchAll() as $currency) {
            $currencies[$currency->currencyID] = $currency->currencyName;
        }

        return $this->returnSuccess(['currencies'=>$currencies]);
    	
    }
}

