<?php

namespace Core\Service;

use Core\Service\BaseService;

class PaymentTermService extends BaseService
{

    public function getPaymentTerms()
    {  
        $paymentTerms = $this->getModel('PaymentTermTable')->fetchAll();

        $terms = [];
        foreach ($paymentTerms as $value) {
            $id = ($value->paymentTermID == null) ? 0 : $value->paymentTermID;
            $terms[$id] = $value->paymentTermName;
        }

        return $this->returnSuccess(['paymentTerms' => $terms]);
    }
       
}