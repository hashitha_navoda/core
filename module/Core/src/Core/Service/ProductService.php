<?php

namespace Core\Service;

use Core\Service\BaseService;
use Zend\EventManager\EventInterface;
use Zend\Session\Container;


class ProductService extends BaseService
{

    public function updateEntityRecord(EventInterface $event)
    {
        $productIDs = $event->getParam('productIDs');

        $locationIDs = $event->getParam('locationIDs');

        $locProducts = $this->getModel('LocationProductTable')->getLocationProductsByIds($productIDs, $locationIDs);

        $user_session = new Container('ezBizUser');
        $userID = $user_session->userID;


        $currentTime = $this->getGMTDateTime('Y-m-d H:i:s');
        $entitydata = array(
            'updatedBy' => $userID,
            'updatedTimeStamp' => $currentTime['data']['currentTime']
        );

        foreach ($locProducts as $product) {
            $this->getModel('EntityTable')->updateEntity($entitydata, $product['entityID']);
        }

        return $this->returnSuccess([]);
    }

    public function getProductByProductCode($productCode)
    {  
        $res = $this->getModel('ProductTable')->getProductByCode($productCode);
        
        return $this->returnSuccess(['product' => $res]);
    }

    public function getLocationProductByProductIdAndLocationID($productID, $locatiionID)
    {  
        $res = $this->getModel('LocationProductTable')->getLocationProduct($productID, $locatiionID);
        
        return $this->returnSuccess(['locationProduct' => $res]);
    }

    public function getAllProductsForPOS($location, $timestamp = null)
    {

        $updated_at = $timestamp ? date('Y-m-d h:i:s', $timestamp) : null;


        $productsList = $this->getModel('ProductTable')->getActiveProductDetailsByLocation($location, null, $updated_at);
        $products = array();
        while ($row = $productsList->current()) {
            $tempP = array();
            $tempP['pID'] = $row['productID'];
            $tempP['locID'] = $row['loc_id'];
            $tempP['pC'] = $row['productCode'];
            $tempP['pN'] = $row['productName'];
            $tempP['pT'] = $row['productTypeID'];
            $tempP['pBC'] = $row['productBarcode'];
            $tempP['lPID'] = $row['lp_id'];
            $tempP['pR'] = $row['dflt_pr'];
            $tempP['LPQ'] = $row['l_qty'];
            $tempP['dE'] = $row['productDiscountEligible'];
            $tempP['dP'] = $row['l_dic_pre'];
            $tempP['dV'] = $row['l_dic_val'];
            $tempP['bP'] = $row['batchProduct'];
            $tempP['sP'] = $row['serialProduct'];
            $tempP['sales'] = false;
            $tempP['purchase'] = false;
            $tempP['giftCard'] = $row['productHandelingGiftCard'];
            $tempP['rackID'] = $row['rackID'];
            $tempP['dtl'] = $row['itemDetail'];

            $getRelatedBarcodes = $this->getModel('itemBarcodeTable')->getRelatedBarCodesByProductID($row['productID'], true);
            $tempP['barcodes'] = $getRelatedBarcodes;

            $tax = (isset($products[$row['productID']]['tax'])) ? $products[$row['productID']]['tax'] : array();
            if ($row['productTaxEligible'] == 1) {
                if ($row['taxID'] != NULL) {
                    $simpleTaxes = [];
                    $taxDetail = $this->getModel('TaxTable')->getAllTax($row['taxID']);
                    $taxDetails = ($taxDetail == null) ? array() : $taxDetail;
                    foreach ($taxDetails as $val) {
                        if ($val['simpleTaxID']) {
                            $simpleTaxes[$val['simpleTaxID']] = [
                                'stN' => $val['sTaxName'],
                                'stP' => $val['sTaxPrecentage'],
                                'stS' => $val['sState']
                            ];
                        }
                    }
                    $tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state'],'tT'=> $row['taxType'], 'sT' => $simpleTaxes);
                }
            }

            $uom = (isset($products[$row['productID']]['uom'])) ? $products[$row['productID']]['uom'] : array();
            if ($row['uomID'] != NULL) {
                $uom[$row['uomID']] = array(
                    'uA' => $row['uomAbbr'],
                    'uDP' => $row['uomDecimalPlace'],
                    'uC' => $row['productUomConversion'],
                    'uS' => $row['uomState'],
                    'disp' => $row['productUomDisplay']
                );
            }

            $batchSerial = (isset($products[$row['productID']]['batchSerial'])) ? $products[$row['productID']]['batchSerial'] : array();
            $batchIDsInBatchSerial = (isset($products[$row['productID']]['productIDs'])) ? $products[$row['productID']]['productIDs'] : array();
            if ($row['serialProductBatchID'] != NULL && $row['serialProductBatchID'] == $row['productBatchID'] && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {
                $batchSerial[$row['productSerialID']] = array(
                    'PBID' => $row['serialProductBatchID'],
                    'PBC' => $row['productBatchCode'],
                    'PBW' => $row['productBatchWarrantyPeriod'],
                    'PSID' => $row['productSerialID'],
                    'PSC' => $row['productSerialCode'],
                    'PSW' => $row['productSerialWarrantyPeriod'],
                    'PSED' => $row['productSerialExpireDate'],
                    'PSS' => $row['productSerialSold']
                );
                if ($row['productBatchID'] != NULL) {
                    $batchIDsInBatchSerial[$row['productBatchID']] = array('PBID' => $row['productBatchID']);
                }
            }

            $batch = (isset($products[$row['productID']]['batch'])) ? $products[$row['productID']]['batch'] : array();
            if ($row['productBatchID'] != NULL && $row['serialProductBatchID'] != $row['productBatchID']) {
                $batch[$row['productBatchID']] = array(
                    'PBID' => $row['productBatchID'],
                    'PBC' => $row['productBatchCode'],
                    'PBQ' => $row['productBatchQuantity'],
                    'PBW' => $row['productBatchWarrantyPeriod'],
                    'PBED' => $row['productBatchExpiryDate'],
                    'PBPRICE' => $row['productBatchPrice'],
                    'PSID' => $row['productSerialID']
                );
            }

            $serial = (isset($products[$row['productID']]['serial'])) ? $products[$row['productID']]['serial'] : array();
            if ($row['productSerialID'] != NULL && $row['serialProductBatchID'] == NULL && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {
                $serial[$row['productSerialID']] = array(
                    'PBID' => $row['serialProductBatchID'],
                    'PBC' => $row['productBatchCode'],
                    'PSID' => $row['productSerialID'],
                    'PSC' => $row['productSerialCode'],
                    'PSW' => $row['productSerialWarrantyPeriod'],
                    'PSED' => $row['productSerialExpireDate'],
                    'PSS' => $row['productSerialSold']
                );
            }

            $tempP['tax'] = $tax;
            $tempP['uom'] = $uom;
            $tempP['batch'] = $batch;
            $tempP['serial'] = $serial;
            $tempP['batchSerial'] = $batchSerial;
            $tempP['productIDs'] = $batchIDsInBatchSerial;
            $products[$row['productID']] = $tempP;
        }

        $display = $this->getModel('DisplaySetupTable')->fetchAll();
        $displaySetup = $display->current();
        $posPrintoutImageConversionStatus = $displaySetup->posPrintoutImageConversionStatus;
        $data =[
            'products' => $products,
            'posPrintoutImageConversionStatus' => $posPrintoutImageConversionStatus
        ];

        return $this->returnSuccess($data);
    }

    public function getGiftCardList()
    {
        // Get giftcard data
        $giftCardData = $this->getModel('GiftCardTable')->getAllOpenGiftCards();
        $giftCardDetails = array();
        foreach ($giftCardData as $g) {
            $giftCardDetails[$g->giftCardId] = array(
                'giftCardId' => $g->giftCardId,
                'giftCardCode' => $g->giftCardCode,
                'giftCardExpireDate' => $g->giftCardExpireDate,
                'giftCardValue' => $g->giftCardValue
            );
        }

        // Get card type data
        $cardTypeData = $this->getModel('CardTypeTable')->fetchAll();
        $cardDetails = array();
        foreach ($cardTypeData as $g) {
            $cardDetails[$g['cardTypeID']] = array(
                'cardTypeID' => $g['cardTypeID'],
                'cardTypeName' => $g['cardTypeName'],
                'accountID' => $g['accountID']
            );
        }

        // Get glAccount data
        $glAccountSetupData = $this->getModel('GlAccountSetupTable')->fetchAll();
        $glAccountSetupDetails = $glAccountSetupData->current();

        // Get Location data
        $locationData = $this->getModel('LocationTable')->fetchAll();
        $locationDetails = array();
         foreach ($locationData as $g) {
           $locationDetails[$g['locationID']] = array(
                'locationID' => $g['locationID'],
                'locationCode' => $g['locationCode'],
                'locationName' => $g['locationName'],
                'locationAddressLine1' => $g['locationAddressLine1'],
                'locationAddressLine2' => $g['locationAddressLine2'],
                'locationAddressLine3' => $g['locationAddressLine3'],
                'locationAddressLine4' => $g['locationAddressLine4'],
                'locationEmail' => $g['locationEmail'],
                'locationTelephone' => $g['locationTelephone'],
                'locationFax' => $g['locationFax'],
                'locationTax' => $g['locationTax'],
                'locationStatus' => $g['locationStatus'],
                'locationCashInHand' => $g['locationCashInHand'],
                'entityID' => $g['entityID'],
                'deleted' => $g['deleted']
            );
        }

        // Get supplier data
        $supplierData = $this->getModel('SupplierTable')->fetchAll();
        $supplierDetails = array();
        foreach ($supplierData as $g) {
            $supplierDetails[$g['supplierID']] = array(
                'supplierID' => $g['supplierID'],
                'supplierCode' => $g['supplierCode'],
                'supplierTitle' => $g['supplierTitle'],
                'supplierName' => $g['supplierName'],
                'supplierAddress' => $g['supplierAddress']
            );
        }

        // Get product supplier data
        $productSupplierData = $this->getModel('ProductSupplierTable')->fetchAll();
        $productSupplierDetails = array();
        foreach ($productSupplierData as $key => $row) {
            $productSupplierDetails[$key] = array(
                'productID' => $row->productID,
                'supplierID' => $row->supplierID,
            );
        }

        // Get POS template data
        $posTemplateDetailsTableData = $this->getModel('PosTemplateDetailsTable')->fetchAllData();
        $posTemplateDetailsTableDetails = array();
        foreach ($posTemplateDetailsTableData as $key => $row) {
            $posTemplateDetailsTableDetails[$key] = array(
                'posTemplateDetailsID' => $row->posTemplateDetailsID,
                'posTemplateDetailsAttribute' => $row->posTemplateDetailsAttribute,
                'posTemplateDetailsIsSelected' => $row->posTemplateDetailsIsSelected,
                'posTemplateDetailsByDefaultSelected' => $row->posTemplateDetailsByDefaultSelected,
            );
        }

        // Get company data
        $companyTableData = $this->getModel('CompanyTable')->getCompany();
        $companyDetails = array(
            'id' => $companyTableData->id,
            'companyName' => $companyTableData->companyName,
            'companyAddress' => $companyTableData->companyAddress,
            'postalCode' => $companyTableData->postalCode,
            'telephoneNumber' => $companyTableData->telephoneNumber,
            'email' => $companyTableData->email,
            'country' => $companyTableData->country,
            'website' => $companyTableData->website,
            'brNumber' => $companyTableData->brNumber,
            'logoID' => $companyTableData->logoID,
            'taxRegNumber' => $companyTableData->taxRegNumber,
            'oldTaxRegNumber' => $companyTableData->oldTaxRegNumber,
            'companyLicenseExpireDate' => $companyTableData->companyLicenseExpireDate,
            'telephoneNumber2' => $companyTableData->telephoneNumber2,
            'telephoneNumber3' => $companyTableData->telephoneNumber3,
            'faxNumber' => $companyTableData->faxNumber,
            'additionalTextForPos' => $companyTableData->additionalTextForPos,
        );

        $companyFolder = $this->getCompanyDataFolder();
        $companyImage = "/assets/img/default.jpeg";

        if (isset($companyTableData->logoID) && $companyTableData->logoID != NULL) {
            $companyImage =   $companyFolder . '/company/thumbs/' . $companyTableData->logoID;
        }
        
        // Convert image to base64 encode
        $image = file_get_contents($companyImage);
        $type = pathinfo($companyImage, PATHINFO_EXTENSION);
        $b64image = "";
        if ($image !== false){
            $b64image = 'data:image/'.$type.';base64,'.base64_encode($image);
        }   
        $companyDetails['companyLogo'] = $b64image;

        $data = [
            'giftCardDetails'=>$giftCardDetails, 
            'cardDetails' => $cardDetails,
            'glAccountSetupDetails' => $glAccountSetupDetails,
            'locationDetails' => $locationDetails,
            'supplierDetails' => $supplierDetails,
            'productSupplierDetails' => $productSupplierDetails,
            'posTemplateDetailsTableData' => $posTemplateDetailsTableDetails,
            'companyDetails' => $companyDetails,
        ];

        return $this->returnSuccess($data);
    }


    public function getAllProductsForDesktopPOS($location, $timestamp)
    {

        $updated_at = $timestamp ? $timestamp : null;

        $productsList = $this->getModel('ProductTable')->getActiveProductDetailsByLocationForPos($location, null, $updated_at);
        $products = array();
        while ($row = $productsList->current()) {
            $tempP = array();
            $tempP['pID'] = $row['productID'];
            $tempP['pC'] = $row['productCode'];
            $tempP['pN'] = $row['productName'];
            $tempP['pT'] = $row['productTypeID'];
            $tempP['pBC'] = $row['productBarcode'];
            $tempP['lPID'] = $row['lp_id'];
            $tempP['pR'] = $row['dflt_pr'];
            $tempP['LPQ'] = $row['l_qty'];
            $tempP['dE'] = $row['productDiscountEligible'];
            $tempP['dP'] = $row['l_dic_pre'];
            $tempP['dV'] = $row['l_dic_val'];
            $tempP['bP'] = $row['batchProduct'];
            $tempP['sP'] = $row['serialProduct'];
            $tempP['sales'] = false;
            $tempP['purchase'] = false;
            $tempP['giftCard'] = $row['productHandelingGiftCard'];
            $tempP['rackID'] = $row['rackID'];
            $tempP['dtl'] = $row['itemDetail'];
            $tempP['cID'] = $row['categoryID'];

            $tempP['productSalesAccountID'] = $row['productSalesAccountID'];
            $tempP['productInventoryAccountID'] = $row['productInventoryAccountID'];
            $tempP['productCOGSAccountID'] = $row['productCOGSAccountID'];
            $tempP['productAdjusmentAccountID'] = $row['productAdjusmentAccountID'];

            $getRelatedBarcodes = $this->getModel('itemBarcodeTable')->getRelatedBarCodesByProductID($row['productID'], true);
            $tempP['barcodes'] = $getRelatedBarcodes;

            $tax = (isset($products[$row['productID']]['taxDetails'])) ? $products[$row['productID']]['taxDetails'] : array();
            if ($row['productTaxEligible'] == 1) {
                if ($row['taxID'] != NULL) {
                    $simpleTaxes = [];
                    $taxDetail = $this->getModel('TaxTable')->getAllTax($row['taxID']);
                    $taxDetails = ($taxDetail == null) ? array() : $taxDetail;
                    foreach ($taxDetails as $val) {
                        if ($val['simpleTaxID']) {
                            $simpleTaxes[$val['simpleTaxID']] = [
                                'stN' => $val['sTaxName'],
                                'stP' => $val['sTaxPrecentage'],
                                'stS' => $val['sState']
                            ];
                        }
                    }
                    $tax[$row['taxID']] = array('tN' => $row['taxName'], 'tP' => $row['taxPrecentage'], 'tS' => $row['state'],'tT'=> $row['taxType'], 'sT' => $simpleTaxes);
                }
            }

            $uom = (isset($products[$row['productID']]['uom'])) ? $products[$row['productID']]['uom'] : array();
            if ($row['uomID'] != NULL) {
                $uom[$row['uomID']] = array(
                    'uA' => $row['uomAbbr'],
                    'uDP' => $row['uomDecimalPlace'],
                    'uC' => $row['productUomConversion'],
                    'uS' => $row['uomState'],
                    'disp' => $row['productUomDisplay']
                );
            }

            $batchSerial = (isset($products[$row['productID']]['batchSerial'])) ? $products[$row['productID']]['batchSerial'] : array();
            $batchIDsInBatchSerial = (isset($products[$row['productID']]['productIDs'])) ? $products[$row['productID']]['productIDs'] : array();
            if ($row['serialProductBatchID'] != NULL && $row['serialProductBatchID'] == $row['productBatchID'] && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {
                $batchSerial[$row['productSerialID']] = array(
                    'PBID' => $row['serialProductBatchID'],
                    'PBC' => $row['productBatchCode'],
                    'PBW' => $row['productBatchWarrantyPeriod'],
                    'PSID' => $row['productSerialID'],
                    'PSC' => $row['productSerialCode'],
                    'PSW' => $row['productSerialWarrantyPeriod'],
                    'PSS' => $row['productSerialSold']
                );
                if ($row['productBatchID'] != NULL) {
                    $batchIDsInBatchSerial[$row['productBatchID']] = array('PBID' => $row['productBatchID']);
                }
            }

            $batch = (isset($products[$row['productID']]['batch'])) ? $products[$row['productID']]['batch'] : array();
            if ($row['productBatchID'] != NULL && $row['serialProductBatchID'] != $row['productBatchID']) {
                $batch[$row['productBatchID']] = array(
                    'PBID' => $row['productBatchID'],
                    'PBC' => $row['productBatchCode'],
                    'PBQ' => $row['productBatchQuantity'],
                    'PBW' => $row['productBatchWarrantyPeriod'],
                    'PSID' => $row['productSerialID']
                );
            }

            $serial = (isset($products[$row['productID']]['serial'])) ? $products[$row['productID']]['serial'] : array();
            if ($row['productSerialID'] != NULL && $row['serialProductBatchID'] == NULL && $row['productSerialSold'] != 1 && $row['productSerialReturned'] == 0) {
                $serial[$row['productSerialID']] = array(
                    'PBID' => $row['serialProductBatchID'],
                    'PBC' => $row['productBatchCode'],
                    'PSID' => $row['productSerialID'],
                    'PSC' => $row['productSerialCode'],
                    'PSW' => $row['productSerialWarrantyPeriod'],
                    'PSS' => $row['productSerialSold']
                );
            }

            $tempP['taxDetails'] = $tax;
            $tempP['uom'] = $uom;
            $tempP['batch'] = $batch;
            $tempP['serial'] = $serial;
            $tempP['batchSerial'] = $batchSerial;
            $tempP['productIDs'] = $batchIDsInBatchSerial;
            $products[$row['productID']] = $tempP;
        }
        
        foreach ($products as $key => $value) {
            $docTypes = ['Goods Received Note','Payment Voucher'];
            $locIds = [$location];
            $proIds = [$value['pID']];
            $dataForCosting = $this->getModel('ItemInTable')->getLastInsertedItemValuesByDocumentTypeLocationIDAndProductID($docTypes, $locIds, $proIds, false)->current();
            $cost = floatval($dataForCosting['itemInPrice']) - floatval($dataForCosting['itemInDiscount']);

            $products[$key]['cost'] = number_format($cost, 2);
            
            if (floatval($cost) != 0) {
                $extraDis =  (floatval($cost) * 5 ) / 100;
                $cost = $cost + $extraDis;
            }

            $products[$key]['dummyCost'] = number_format($cost, 2);
        }
        return $this->returnSuccess($products);
    }
}