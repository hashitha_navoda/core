<?php

namespace Core\Service;

use Core\Service\BaseService;

class PusherService extends BaseService 
{
	public function push($data = [])
	{
		try {
			
            $client = $this->getSubdomain();

            $pusherPort = $this->getServiceLocator()->get('config')['pusher_server'];

            $entryData = array(
                'client' => $client,
                'products' => $data,
                'ts_' => time()
            );

            $context = new \ZMQContext();

            $socket = $context->getSocket(\ZMQ::SOCKET_PUSH, 'my pusher');

            $socket->connect("tcp://" . $pusherPort);

            $socket->send(json_encode($entryData));
		} catch (\Exception $e) {
            error_log($e->getMessage());
		}
	}
}