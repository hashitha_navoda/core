<?php

namespace Core\Service;

use Core\Service\BaseService;
use Zend\EventManager\EventInterface;
use Zend\Session\Container;


class PromotionService extends BaseService
{ 


    public function getPromotionDetailsForDesktopPOS($locationID)
    {
        // $updated_at = $timestamp ? date('Y-m-d h:i:s', $timestamp) : null;
        // $categoriesList = $this->getModel('CategoryTable')->fetchAll();
        // $categories = array();
        // while ($row = $categoriesList->current()) {
        //     $tempP = array();
        //     $tempP['cID'] = $row['categoryID'];
        //     $tempP['cN'] = $row['categoryName'];
        //     $categories[$row['categoryID']] = $tempP;
        // }
        // error_log(count($categories));

        $promotions = $this->getModel('PromotionTable')->getToDayPromotions(date("Y-m-d"), $locationID);

        foreach ($promotions as $key => $value) {
           $promo = $this->getPromotionDetails($value['promotionID']);
           $promotionData[] = $promo[$value['promotionID']];
           // error_log(json_encode($promo));
        }

        return $this->returnSuccess($promotionData);
    }

    public function getPromotionDetails($promotionID)
    {

        $promotionType = $this->getModel('PromotionTable')->getPromotion($promotionID)['promotionType'];
        $promotionData = $this->getModel('PromotionTable')->getPromotionWithDetails($promotionID, $promotionType);
        $invoiceData = $this->getModel('InvoiceTable')->getRelatedInvoices($promotionID)->current();

        if ($promotionType == 1 || $promotionType == 3) {
            $promotionDetails = array();
            foreach ($promotionData as $promo) {
                $tempPromo['promoID'] = $promo['promotionID'];
                $productUoms = $this->getModel('ProductUomTable')->getProductUomsListByProductID($promo['promoProductID']);
                $tempPromo['promoName'] = $promo['promotionName'];
                $tempPromo['promoFromDate'] = $promo['promotionFromDate'];
                $tempPromo['promoToDate'] = $promo['promotionToDate'];
                $tempPromo['promoType'] = $promo['promotionType'];
                $tempPromo['promoDesc'] = $promo['promotionDescription'];
                $tempPromo['promoLocationID'] = $promo['promotionLocation'];
                if ($promo['promotionLocation'] != NULL) {
                    $tempPromo['promoLocationName'] = $this->getModel('LocationTable')->getLocationByID($promo['promotionLocation'])->locationName;
                } else {
                    $tempPromo['promoLocationName'] = 'Apply for every location';
                }
                $tempPromo['promoStatusID'] = $promo['promotionStatus'];
                $tempPromo['promoStatus'] = $this->getStatusCode($promo['promotionStatus']);
                $tempPromo['promoStatus'] = $this->getStatusCode($promo['promotionStatus']);
                $tempPromo['isUsed'] = ($invoiceData) ? true : false;

                $relatedCombinations = $this->getModel('PromotionCombinationTable')->getRelatedCombinationsByPromotionId($promo['promotionID']);


                $combinations = [];
                foreach ($relatedCombinations as $key => $combination) {
                    $relatedProductIds = [];

                    $rules = [];

                    if ($promo['promotionType'] == 1) {
                        $relatedRules = $this->getModel('ItemBaseCombinationRuleTable')->getRelatedRulesByCombinationId($combination['promotionCombinationID']);
                        $relatedProductIds = [];
                        foreach ($relatedRules as $key => $rule) {
                            $rule['productName'] = $rule['productCode'].'-'.$rule['productName'];
                            $rules[] = $rule;
                            $relatedProductIds[] = $rule['productID'];
                        }
                    } elseif ($promo['promotionType'] == 3) {
                        $relatedRules = $this->getModel('AttributeBaseCombinationRuleTable')->getRelatedRulesByCombinationId($combination['promotionCombinationID']);

                        $ruleRelatedProductsData = [];
                        foreach ($relatedRules as $key => $rule) {

                            if (!empty($rule['attributeID'])) {
                                $itemAttrData = $this->getModel('ItemAttributeTable')->getDataByItemAttrID($rule['attributeID'])->current();
                                $rule['attributeTypeName'] = $itemAttrData['itemAttributeCode'].'-'.$itemAttrData['itemAttributeName'];
                                $rule['attributeType'] = $itemAttrData['itemAttributeID'];
                                $ruleRelatedProductArr = $this->getModel('productTable')->getProductsByAttributeID($rule['attributeID'],$rule['attributeValueID']);

                                $ruleRelatedProductsData[$rule['attributeBaseCombinationRuleID']] = $ruleRelatedProductArr;
                                
                            } elseif ($rule['attributeID'] == 0) {
                                $rule['attributeTypeName'] = "Quantity";
                            }

                            if (!empty($rule['attributeValueID'])) {
                                $itemAttrValData = $this->getModel('ItemAttributeValueTable')->getValueByItemAtributeValueID($rule['attributeValueID'])->current();
                                $rule['attributeValName'] = $itemAttrValData['itemAttributeValueDescription'];
                                $rule['attributeValID'] = $itemAttrValData['itemAttributeValueID'];
                            }
                            $rules[] = $rule;
                        }

                        if ($combination['conditionType'] == 'AND') {
                            if (sizeOf($ruleRelatedProductsData) == 1) {
                                $firstEle = array_shift($ruleRelatedProductsData);
                                $relatedItem = $firstEle;
                            }

                            if (sizeOf($ruleRelatedProductsData) > 1) {
                                $relatedItem = call_user_func_array('array_intersect', $ruleRelatedProductsData);
                            }

                            foreach ($relatedItem as $key => $value) {
                                $relatedProductIds[] = $value;
                            }
                        } else {

                            $relatedItem = [];
                            foreach ($ruleRelatedProductsData as $key => $arr) {
                                $relatedItem = array_merge($arr, $relatedItem);
                            }

                            $relatedProductIds = array_unique($relatedItem);
                        }


                    }

                    $combination['ruleSet'] = $rules;
                    $combination['relatedProductIds'] = ($relatedProductIds) ?  $relatedProductIds: [];
                    $combinations [] = $combination;
                }
                $tempPromo['combinations'] = $combinations;
                $promotionDetails[$promo['promotionID']] = $tempPromo;
            }
        } else if ($promotionType == 2) {
            $promotionDetails = array();
            $promoArray = $promotionData[0];
            $tempPromo['promoID'] = $promoArray['promotionID'];
            $tempPromo['promoName'] = $promoArray['promotionName'];
            $tempPromo['promoFromDate'] = $promoArray['promotionFromDate'];
            $tempPromo['promoToDate'] = $promoArray['promotionToDate'];
            $tempPromo['promoType'] = $promoArray['promotionType'];
            $tempPromo['promoDesc'] = $promo['promotionDescription'];
            $tempPromo['promoLocationID'] = $promoArray['promotionLocation'];
            if ($promoArray['promotionLocation'] != NULL) {
                $tempPromo['promoLocationName'] = $this->getModel('LocationTable')->getLocationByID($promoArray['promotionLocation'])->locationName;
            } else {
                $tempPromo['promoLocationName'] = 'Apply for every location';
            }
            $tempPromo['promoStatusID'] = $promoArray['promotionStatus'];
            $tempPromo['promoStatus'] = $this->getStatusCode($promoArray['promotionStatus']);
            $tempPromo['invoicePromotionID'] = $promoArray['invoicePromotionID'];
            $tempPromo['minValue'] = $promoArray['invoicePromotionMinValue'];
            $tempPromo['maxValue'] = $promoArray['invoicePromotionMaxValue'];
            $tempPromo['discountType'] = $promoArray['invoicePromotionDiscountType'];
            $tempPromo['discountAmount'] = $promoArray['invoicePromotionDiscountAmount'];
            $tempPromo['isUsed'] = ($invoiceData) ? true : false;
            $promotionDetails[$promoArray['promotionID']] = $tempPromo;
        }
        return $promotionDetails;
    }
}