<?php

namespace Core\Service;

use Core\Service\BaseService;

class TaxService extends BaseService
{

    public function getTaxes($taxes)
    {  
        $taxes = (count((array) $taxes) > 0 ) ? ($this->getModel('TaxTable')->getTaxes((array) $taxes)) ? : [] : [];
        
        return $this->returnSuccess(['taxes' => $taxes]);
    }

}






