<?php

namespace CoreTest\Controller;

use Zend\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;
use Zend\Session\Container;

class CoreControllerTest extends AbstractHttpControllerTestCase
{

    protected $traceError = true;

    public function setUp()
    {
        $this->setApplicationConfig(
                array_merge(include DOC_ROOT . 'config/test.application.config.php', include DOC_ROOT . 'config/autoload/local.test.php')
        );

        parent::setUp();
    }

    public function setServerVariables()
    {

        $_SERVER['HTTP_HOST'] = $this->getApplicationConfig()['servername'];
        $_SERVER['SERVER_NAME'] = $this->getApplicationConfig()['servername'];
    }

    public function mockUserLogin()
    {
        $userSessions = new Container('ezBizUser');
        $userSessions->userID = 1;
        $userSessions->roleID = 1;
        $userSessions->timeZone = 'Asia/Colombo';
        $locationArray = array(
            'locationName' => "Test Location",
            'roleID' => 1,
            'locationID' => 1
        );
        $userSessions->userActiveLocation = $locationArray;
    }

    public function testIndexActionCanBeAccessed()
    {
        //set user Session variables
        $this->mockUserLogin();

        $this->dispatch('/core/index');
        $this->assertResponseStatusCode(200);
        $this->assertModuleName('Core');
        $this->assertControllerName('Core\Controller\CoreController');
        $this->assertControllerClass('CoreController');
        $this->assertMatchedRouteName('core');
    }

}
