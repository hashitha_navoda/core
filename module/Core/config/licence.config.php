<?php

/**
 * Having some common variables for user Licence
 *
 * @author shamilan <sharmilan@thinkcube.com>
 */
return array(
    'licence' => array(
        'licenceCount' => '3',
        'maxDeActivatedUserCount' => '3',
        'licencePeriod' => '30',
        'licenceType' => 'paid',
        'portalURL' => 'https://www.google.com'
    ),
);

