<?php

return [
    'report' => [
        'representative-wise-item' => [
            'service' => 'SalesReportService',
            'category' => 'daily-sales-items-report',
            'csv' => 'representativeWiseItemCsv',
            'pdf' => 'representativeWiseItemPdf'
        ],
        'representative-wise-item-with-invoice' => [
            'service' => 'SalesReportService',
            'category' => 'daily-sales-items-report',
            'csv' => 'representativeWiseItemWithInvoceCsv',
            'pdf' => 'representativeWiseItemWithInvocePdf'
        ],
        'global-item-moving' => [
            'service' => 'StockInHandReportService',
            'category' => 'stock-in-hand-report',
            'csv' => 'globalItemMovingSheet',
            'pdf' => 'globalItemMovingPdf'
        ],
        'balance-sheet' => [
            'service' => 'FinanceReportService',
            'category' => 'finance-report',
            'csv' => 'balanceSheetCsv',
            'pdf' => 'balanceSheetPdf'
        ],
        'trial-balance' => [
            'service' => 'FinanceReportService',
            'category' => 'finance-report',
            'csv' => 'trialBalanceSheet',
            'pdf' => 'trialBalancePdf'
        ],
        'profit-and-loss' => [
            'service' => 'FinanceReportService',
            'category' => 'finance-report',
            'csv' => 'profitAndLossSheet',
            'pdf' => 'profitAndLossPdf'
        ],
        'general-ledger' => [
            'service' => 'FinanceReportService',
            'category' => 'finance-report',
            'csv' => 'generalLedgerSheet',
            'pdf' => 'generalLedgerPdf'
        ],
        'global-stock-value' => [
            'service' => 'StockInHandReportService',
            'category' => 'stock-in-hand-report',
            'csv' => 'globalStockValueSheet',
            'pdf' => 'globalStockValuePdf'
        ],
        'location-wise-stock-value' => [
            'service' => 'StockInHandReportService',
            'category' => 'stock-in-hand-report',
            'csv' => 'locationWiseStockValueSheet',
            'pdf' => 'locationWiseStockValuePdf'
        ],
        'representative-wise-category' => [
            'service' => 'SalesReportService',
            'category' => 'daily-sales-items-report',
            'csv' => 'representativeWiseCategorySheet',
            'pdf' => 'representativeWiseCategoryPdf'
        ],
        'gross-profit-report' => [
            'service' => 'SalesReportService',
            'category' => 'sales-report',
            'csv' => 'grossProfitReportSheet',
            'pdf' => 'grossProfitReportPdf'
        ],
        'serial-item-moving' => [
            'service' => 'StockInHandReportService',
            'category' => 'stock-in-hand-report',
            'csv' => 'serialItemMovementSheet',
            'pdf' => 'serialItemMovementPdf'
        ],
        'cash-flow' => [
            'service' => 'FinanceReportService',
            'category' => 'finance-report',
            'csv' => 'cashFlowSheet',
            'pdf' => 'cashFlowPdf'
        ]
    ]
];

