<?php

return array(
    'statuses' => array(
        1 => 'active',
        2 => 'inactive',
        3 => 'open',
        4 => 'closed',
        5 => 'cancelled',
        6 => 'overdue',
        7 => 'draft',
        8 => 'in progress',
        9 => 'completed',
        10 => 'replace',
        11 => 'locked',
        12 => 'posted',
        13 => 'rejected',
        14 => 'unlocked',
        15 => 'open',
        16 => 'approved',
        17 => 'hold',
		18 => 'ended',
		19 => 'requested',
        20 => 'invoiced',
        21 => 'paid',
    ),
    'timeInterval' => array(
        'updateInterval' => 5,
        'logoutInterval' => '00:30:00',
        'cookiePeriod' => '1800',
    ),
    'wizardState' => array(
        1 => 'company',
        2 => 'tax',
        3 => 'finish-setup',
    ),
    'wizardCompleteState' => 4,
    'apiKey' => 'EFqbDA_kff0XyBOMqOn%ni6ON%TSRZ06Vw9M3a',
    'mobileApiKey' => '0ddfba99aed66b8d1064ea5cc180c244a11a7315',
    'mobilePosApiKey' => 'c2c6491d251752facbe291817497432b4e4e8425',
    'noticeStatus' => array(
        0 => 'info',
        1 => 'success',
        2 => 'warning',
        3 => 'danger'
    ),
    'chatUrl' => "https://livechat.ezbizapp.com",
    'version' => 'live',
    'jobCardModel' => array(
        1 => 'project',
        2 => 'job',
        3 => 'activity',
    ),
    'promotionTypes' => array(
        1 => 'Item based',
        2 => 'Invoice based',
        3 => 'Attribute based'
    ),
    'entitySubType' => array(
        1 => 'image',
        2 => 'docment'
    ),
    'PettyCashFloatCodePrefix' => 'PCF',
    'pettyCashVoucherTypes' => array(
        1 => 'Cash Outgoing',
        2 => 'Cash Incoming',
    ),
    'cashType' => array(
        1 => 'Cash In Hand',
        2 => 'Cash In Bank',
    ),
    'support_details' => array(
        'Telephone' => '+94 114331188',
        'Email' => 'support@ezbizapp.com',
    ),
    'business_types' => array(
        1 => 'Engineering Services',
        2 => 'Repairing Services',
        3 => 'Tournament',
    ),
    'material_type' => array(
        1 => 'Trading Items',
        2 => 'Fixed Assets',
        3 => 'Raw Materials',
    ),
    'job_user_roles' => array(
        1 => 'Admin',
        2 => 'Store Keeper',
    ),
    'incentive_type' => array(
        1 => 'Per completion',
    ),
    'service_job_material_type' => array(
        1 => 'Service Materials',
        2 => 'Trade Materials',
    ),
    'sms_service_provider' => array(
        1 => 'Mobitel',
        2 => 'Dialog',
    ),
    'SMS_SEND_TYPE' => array(
        1 => 'Automatic',
        2 => 'Manual',
    ),
);
