<?php

/**
 * Having some common variables for Notification
 *
 * @author shamilan <sharmilan@thinkcube.com>
 */
return array(
    'notification-types' => array(
        1 => array('Overdue Sales Invoice'),
        2 => array('Overdue Purchase Invoice'),
        3 => array('Inventory Reorder'),
        4 => array('Inventory Minimum Level'),
        5 => array('User Message'),
        6 => array('Incoming Cheque'),
        7 => array('Outgoing Cheque'),
        8 => array('Product Expiry Alerts'),
        9 => array('Postdated Cheque Expire Alerts'),
        10 => array('Report Notification'),
        11 => array('Next Invoice Date')
    ),
    'notification-status' => array(
        0 => array('open'),
        1 => array('close'),
    ),
    'notification-title-sl' => array(
        'Have Reached Reorder Level' => 'නැවත ඇනවුම්  කිරීමේ මට්ටමට ළඟාවීම',
        'An Overdue Sales invoice' => 'කල් ඉකුත් වූ විකුණුම් ඉන්වොයිසියක්',
        'Next Invoice Date For Sales Invoice' => 'විකුණුම් ඉන්වොයිසිය සඳහා ඊළඟ ඉන්වොයිසි දිනය',
        'An Overdue Purchase Invoice' => 'කල් ඉකුත් වූ මිලදී ගැනීමේ ඉන්වොයිසියක්',
        'Have Reached Minimum Inventory Level' => 'අවම ඉන්වෙන්ටරි මට්ටමට ළඟාවීම',
        'Product Expiry Alert Notification' => 'නිෂ්පාදන කල් ඉකුත් වීමේ දැනුම්දීම',
        'Postdated Incoming Cheque' => 'කල් ඉකුත් වූ චෙක්පත්',
        'Postdated Outgoing Cheque' => 'කල් ඉකුත් වූ පිටතට යන චෙක්පත',
        'Report has been generated' => 'වාර්තාව ජනනය කර ඇත'
    ),
    'notification-title-tl' => array(
        'Have Reached Reorder Level' => 'மீள் நிரப்புவதற்கான நிலையை அடைந்துள்ளது',
        'An Overdue Sales invoice' => 'தவணைக்காலம் கடந்த விற்பனை விலைப்பட்டியல்',
        'Next Invoice Date For Sales Invoice' => 'அடுத்த விற்பனை விலைப்பட்டியலுக்கான  தேதி',
        'An Overdue Purchase Invoice' => 'தவணைக் காலம் கடந்த கொள்முதல் விலைப் பட்டியல்',
        'Have Reached Minimum Inventory Level' => 'குறைந்தபட்ச சரக்கு நிலையை அடைந்துள்ளது',
        'Product Expiry Alert Notification' => 'காலாவதித் தேதிக்கான அறிவிப்பு',
        'Postdated Incoming Cheque' => 'பின் தேதி இடப்பட்ட உள்வரும் காசோலை',
        'Postdated Outgoing Cheque' => 'பின் தேதி இடப்பட்ட வெளிச்செல்லும்  காசோலை',
        'Report has been generated' => 'அறிக்கை தயார்செய்யப்பட்டது'
    )
);
