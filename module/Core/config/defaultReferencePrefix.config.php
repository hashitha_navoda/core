<?php

return array(
    'referencePrefix' => array(
        '1' => 'QT',
        '2' => 'SO',
        '3' => 'INV',
        '4' => 'PAY',
        '5' => 'ADPAY',
        '6' => 'DLN',
        '7' => 'RT',
        '8' => 'CR',
        '9' => 'PO',
        '10' => 'GRN',
        '11' => 'DN',
        '12' => 'ADJ',
        '13' => 'SP',
        '14' => 'PV',
        '15' => 'PRT',
        '16' => 'TRF',
        '17' => 'CNP',
        '18' => 'DNP',
        '19' => 'DEFAULT',
        '20' => 'CUS',
        '21' => 'PROJ',
        '22' => 'JOB',
        '23' => 'ACTY',
        '24' => 'INQLOG',
        '25' => 'INVST',
        '26' => 'EXPV',
        '27' => 'PVOU',
        '28' => 'AADJ',
        '29' => 'DPN',
        '30' => 'JE',
        '31' => 'JET',
        '32' => 'PODFT',
    ),
    'documentTypeName' => array(
        '' => 'Select Document Type',
        '1' => 'Quotation',
        '2' => 'Sales Order',
        '3' => 'Invoice',
//        '4' => 'Payment',
//        '5' => 'Advance Payment',
//        '6' => 'Delivery Note',
//        '7' => 'Sales Returns',
//        '8' => 'Credit Note',
//        '9' => 'Purchase Order',
//        '10' => 'Grn',
//        '11' => 'Debit Note',
//        '12' => 'Adjustment',
//        '13' => 'Supplier Payments',
//        '14' => 'Purchase Invoice',
//        '15' => 'Purchase Returns',
//        '16' => 'Transfer',
//        '17' => 'Credit Note Payments',
//        '18' => 'Debit Note Payments',
////        '19' => 'DEFAULT',
//        '20' => 'Customer',
        '21' => 'Project',
        '22' => 'Job',
        '23' => 'Activity',
        '24' => 'Inquiry Log'
    ),
    'documentReference' => [
        1 => [
            'referenceName' => 'QT',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'QT',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        2 => [
            'referenceName' => 'SO',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'SO',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        3 => [
            'referenceName' => 'INV',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'INV',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        4 => [
            'referenceName' => 'PAY',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'PAY',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        5 => [
            'referenceName' => 'ADPAY',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'ADPAY',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        6 => [
            'referenceName' => 'DLN',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'DLN',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        7 => [
            'referenceName' => 'RT',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'RT',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        8 => [
            'referenceName' => 'CR',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'CR',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        9 => [
            'referenceName' => 'PO',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'PO',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        10 => [
            'referenceName' => 'GRN',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'GRN',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        11 => [
            'referenceName' => 'DN',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'DN',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        12 => [
            'referenceName' => 'ADJ',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'ADJ',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        13 => [
            'referenceName' => 'SP',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'SP',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        14 => [
            'referenceName' => 'PV',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'PV',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        15 => [
            'referenceName' => 'PRT',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'PRT',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        16 => [
            'referenceName' => 'TRF',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'TRF',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        17 => [
            'referenceName' => 'CNP',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'CNP',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        18 => [
            'referenceName' => 'DNP',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'DNP',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        19 => [
            'referenceName' => 'DEFAULT',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'DEFAULT',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        20 => [
            'referenceName' => 'CUS',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'CUS',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        21 => [
            'referenceName' => 'PROJ',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'PROJ',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        22 => [
            'referenceName' => 'JOB',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'JOB',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        23 => [
            'referenceName' => 'ACTY',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'ACTY',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        24 => [
            'referenceName' => 'INQLOG',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'INQLOG',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        25 => [
            'referenceName' => 'INVST',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'INVST',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        26 => [
            'referenceName' => 'EXPV',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'EXPV',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        27 => [
            'referenceName' => 'Petty Cash Voucher',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'PVOU',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        28 => [
            'referenceName' => 'Auto Ajusment',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'AADJ',
            'referencePrefixNumberOfDigits' => 6,
            'referencePrefixCurrentReference' => 1
        ],
        29 => [
            'referenceName' => 'Dispatch Note',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'DISPNOTE',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        30 => [
            'referenceName' => 'JE',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'JE',
            'referencePrefixNumberOfDigits' => 9,
            'referencePrefixCurrentReference' => 1
        ],
        31 => [
            'referenceName' => 'JET',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'JET',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ],
        32 => [
            'referenceName' => 'PODRFT',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'PODFT',
            'referencePrefixNumberOfDigits' => 4,
            'referencePrefixCurrentReference' => 1
        ],
        33 => [
            'referenceName' => 'Item',
            'referenceTypeID' => 0,
            'referencePrefixCHAR' => 'ITM',
            'referencePrefixNumberOfDigits' => 3,
            'referencePrefixCurrentReference' => 1
        ]
    ]
);