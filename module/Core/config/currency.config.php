<?php

return array(
    'currency' => array(
        "Australia" => "AUD",
        "Canada" => "CAD",
        "Hong Kong" => "HKD",
        "Japan" => "JPY",
        "Malaysia" => "MYR",
        "New Zealand" => "NZD",
        "Singapore" => "SGD",
        "Switzerland" => "CHF",
        "Sri Lanka" => "LKR",
        "Philippines" => "Peso",
        "United Kingdom" => "GBP",
        "United States" => "USD",
        "India"=> "INR",
    ),
    'currencyAndSymbols' => array(
        'USD' => '$',
        'AUD' => '$',
        'SGD' => '$',
        'EURO' => '€',
        'LKR' => 'Rs',
        'GBP' => '£',
        'MYR' => 'RM',
        'HKD' => 'HK$',
        'JPY' => '¥',
        'Peso' => 'P',
        'NZD' => '$',
        'CHF' => 'CHF',
        'CAD' => '$',
        'INR' => '₹'
    ),
);

