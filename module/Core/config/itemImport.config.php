<?php

return array(
    'fields' => array(
        'itemCode' => array(
            'id' => 'itemCode',
            'placeholder' => 'Item Code',
            'mandatory' => true,
            'key_words'=> ['item code','item_code','code']
        ),
        'itemName' => array(
            'id' => 'itemName',
            'placeholder' => 'Item Name',
            'mandatory' => true,
            'key_words'=> ['item name','item_name','name']
        ),
        'itemCategory' => array(
            'id' => 'itemCategory',
            'placeholder' => 'Item Category',
            'mandatory' => true,
            'key_words'=> ['item category','item_category','category']
        ),
        'itemType' => array(
            'id' => 'itemType',
            'placeholder' => 'Item Type',
            'mandatory' => true,
            'key_words'=> ['item type','item_type','type']
        ),
        'uomId' => array(
            'id' => 'uomId',
            'placeholder' => 'UOM',
            'mandatory' => true,
            'key_words'=> ['item uom','item_uom','uom']
        ),
        'itemBarcode' => array(
            'id' => 'itemBarcode',
            'placeholder' => 'Item Barcode',
            'mandatory' => false,
            'key_words'=> ['item barcode','item_barcode','barcode']
        ),
        'discountEligible' => array(
            'id' => 'discountEligible',
            'placeholder' => 'Discount Eligible',
            'mandatory' => false,
            'key_words'=> []
        ),
        'discountVal' => array(
            'id' => 'discountVal',
            'placeholder' => 'Discount Value',
            'mandatory' => false,
            'key_words'=> ['Discount']
        ),
        'discountPer' => array(
            'id' => 'discountPer',
            'placeholder' => 'Discount Precentage',
            'mandatory' => false,
            'key_words'=> ['Discount']
        ),
        'sellPrice' => array(
            'id' => 'sellPrice',
            'placeholder' => 'Selling Price',
            'mandatory' => false,
            'key_words'=> ['item selling price','selling price','price']
        ),
        'minInventoryLevel' => array(
            'id' => 'minInventoryLevel',
            'placeholder' => 'Min.Inventory Level',
            'mandatory' => false,
            'key_words'=> ['mil','min.inventory level','minumum inventory levevl','min inventory levevl']
        ),
         'rackId' => array(
            'id' => 'rackId',
            'placeholder' => 'Rack Id',
            'mandatory' => false,
            'key_words'=> ['rack id','rackid']
        ),
        'reoderLevel' => array(
            'id' => 'reoderLevel',
            'placeholder' => 'Re-order Level',
            'mandatory' => false,
            'key_words'=> ['reoder level','re-order level','reoder','re-order']
        ),
        'purPrice' => array(
            'id' => 'purPrice',
            'placeholder' => 'Purchasing Price',
            'mandatory' => false,
            'key_words'=> ['item purchasing price','purchasing price', 'purchase price']
        ),
        'taxEligible' => array(
            'id' => 'taxEligible',
            'placeholder' => 'Tax Eligible',
            'mandatory' => false,
            'key_words'=> []
        ),
        'batchProduct' => array(
            'id' => 'batchProduct',
            'placeholder' => 'Batch Product',
            'mandatory' => false,
            'list' => array(
                '1' => 'Yes',
                '2' => 'No'
            ),
            'key_words'=> ['batch product','batch_product','batch']
        ),
        'salesDiscount' => array(
            'id' => 'salesDiscount',
            'placeholder' => 'Sales Discount Value',
            'mandatory' => false,
            'list' => array(
                '1' => 'Precentage',
                '2' => 'Fixed Value'
            ),
            'key_words'=> ['sales discount','discount','sales_discount']
        ),
        'purchaseDiscount' => array(
            'id' => 'purchaseDiscount',
            'placeholder' => 'Purchase Discount Value',
            'mandatory' => false,
            'list' => array(
                '1' => 'Precentage',
                '2' => 'Fixed Value'
            ),
            'key_words'=> ['purchase discount','discount','purchase_discount','purchasing discount']
        ),
        'serialProduct' => array(
            'id' => 'serialProduct',
            'placeholder' => 'Serial Product',
            'mandatory' => false,
            'list' => array(
                '1' => 'Yes',
                '2' => 'No'
            ),
            'key_words'=> ['serial product','serial_product','serial']
        ),
        'hsCode' => array(
            'id' => 'hsCode',
            'placeholder' => 'Hs Code',
            'mandatory' => false,
            'key_words'=> ['hs code','hs_code','hscode']
        ),
        'productDescription' => array(
            'id' => 'productDescription',
            'placeholder' => 'Item Description',
            'mandatory' => false,
            'key_words'=> ['item description','product description','description']
        ),
        'productHandelingType' => array(
            'id' => 'productHandelingType',
            'placeholder' => 'Product Handeling Type',
            'mandatory' => true,
            'list' => array(
                'productHandelingManufactureProduct' => 'Trading Items',
                'productHandelingPurchaseProduct' => 'Raw Materials',
                'productHandelingSalesProduct' => 'Finished Goods',
                'productHandelingConsumables' => 'Consumables',
                'productHandelingFixedAssets' => 'Fixed Assets'
            ),
            'subList' => array(
                '1' => 'Yes',
                '2' => 'No'
            ),
            'key_words'=> []
        ),
        'stockInHand' => array(
            'id' => 'stockInHand',
            'placeholder' => 'Stock In Hand',
            'mandatory' => false,
            'key_words'=> ['stock','quantity','stock in hand']
        ),
    )
);

