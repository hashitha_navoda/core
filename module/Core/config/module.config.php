<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2013 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'controllers' => array(
        'invokables' => array(
            'Core\Controller\CoreController' => 'Core\Controller\CoreController',
            'Core\Controller\API\CoreController' => 'Core\Controller\API\CoreController',
            'Core\Controller\JavascriptVariable' => 'Core\Controller\JavascriptVariableController',
            'Core\Controller\API\AppController' => 'Core\Controller\API\AppController',
            'Core\Controller\API\NotificationController' => 'Core\Controller\API\NotificationController',
        ),
    ),
    'view_helpers' => array(
        'invokables' => array(
            'noticeHelper' => 'Core\View\Helper\NoticeHelper',
            'configHelper' => 'Core\View\Helper\ConfigHelper',
            'userDateFormatHelper' => 'Core\View\Helper\UserDateFormatHelper',
            'tool_TipHelper' => 'Core\View\Helper\ToolTipHelper',
            'jsvarsHelper' => 'Core\View\Helper\JavascriptVariableHelper'
        )
    ),
    'controller_plugins' => array(
        'invokables' => array(
            'UploadHandler' => 'Core\Controller\Plugin\UploadHandler',
        ),
    ),
    'router' => array(
        'routes' => array(
            'notifications' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/notification[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Core\Controller\API\NotificationController',
                        'action' => 'index',
                    ),
                ),
            ),
            'notification' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/notification[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Core\Controller\API\NotificationController',
                        'action' => 'index',
                    ),
                ),
            ),
            'app' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/app[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Core\Controller\API\AppController',
                        'action' => 'index',
                    ),
                ),
            ),
            'jsvar' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/vars.js[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Core\Controller\JavascriptVariable',
                        'action' => 'index',
                    ),
                ),
            ),
            'currencySymbol' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/currencySymbol[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Core\Controller\CoreController',
                        'action' => 'index',
                    ),
                ),
            ),
            'imagePreview' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/image-preview[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Core\Controller\CoreController',
                        'action' => 'savePreviewImage',
                    ),
                ),
            ),
            'storeFiles' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/store-files[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Core\Controller\CoreController',
                        'action' => 'storeFiles',
                    ),
                ),
            ),
            'getDocumentRelatedAttachement' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-document-related-attachement[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Core\Controller\CoreController',
                        'action' => 'getDocumentRelatedAttachement',
                    ),
                ),
            ),
            'getDocumentRelatedCusPO' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/get-document-related-linked-cus-po[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Core\Controller\CoreController',
                        'action' => 'getDocumentRelatedLinkedCusPo',
                    ),
                ),
            ),
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route' => '/',
                    'defaults' => array(
                        'controller' => 'User\Controller\DashBoard',
                        'action' => 'index',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'core' => array(
                'type' => 'Literal',
                'options' => array(
                    'route' => '/core',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Core\Controller',
                        'controller' => 'Index',
                        'action' => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type' => 'Segment',
                        'options' => array(
                            'route' => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
        'invokables' => array(
            'NotificationListener' => 'Core\Controller\Event\NotificationListener',
            'AccountListener' => 'Core\Controller\Event\AccountListener',
            'ProductListener' => 'Core\Controller\Event\ProductListener',
            'CategoryListener' => 'Core\Controller\Event\CategoryListener',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions' => true,
        'doctype' => 'HTML5',
        'not_found_template' => 'error/404',
        'exception_template' => 'error/index',
        'template_map' => array(
            'layout/layout' => __DIR__ . '/../view/layout/layout.phtml',
            'core/index/index' => __DIR__ . '/../view/core/index/index.phtml',
            'error/404' => __DIR__ . '/../view/error/404.phtml',
            'error/index' => __DIR__ . '/../view/error/index.phtml',
            'error/dashboard' => __DIR__ . '/../view/error/dashboard-permission.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
                'cronroute' => array(
                    'options' => array(
                        'route' => 'scron',
                        'defaults' => array(
                            'controller' => 'Core\Controller\API\NotificationController',
                            'action' => 'getNotification',
                        )
                    )
                )
            ),
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'strategies' => array(
        'ViewJsonStrategy',
    ),
    'defaultLanguage' => "en_US",
    'defaultCountry' => 'international', // languages will be change according to the selceted country, refer languages.config to select a country
//    'defaultCountry' => 'sri_lanka', // languages will be change according to the selceted country, refer languages.config to select a country
);
