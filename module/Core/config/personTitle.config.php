<?php

/**
 * Having some common variables for Person's Title
 *
 * @author shamilan <sharmilan@thinkcube.com>
 */
return array(
    'personTitle' => array(
        '' => '',
        'Mr.' => 'Mr.',
        'Ms' => 'Ms.',
        'Dr.' => 'Dr.',
//        'Miss' => 'Miss',
//        'Mrs.' => 'Mrs.',
    ),
);

