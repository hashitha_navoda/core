<?php
/**
 * type => G-global L-location U-user
 */
return array(
	"settings" => array(
		// invoice total discount application method setting.
		'INV_DISCOUNT' => array(
			'name' => 'INV_DISCOUNT',
			'type' => 'L',
			'default' => '{"discount_only_eligible_items":"false"}'
		),

		// invoice inclusive tax application method setting.
		'INCLUSIVE_TAX' => array(
			'name' => 'INCLUSIVE_TAX',
			'type' => 'L',
			'default' => '{"inclusive_tax":"false"}'
		),

		// give permission to specific user to override item price
		'OVERRIDE_ITEM_PRICE' => array(
			'name' => 'OVERRIDE_ITEM_PRICE',
			'type' => 'U',
			'default' => '{"override_item_price":"false"}'
		),

		// give permission to specific user to override item discount
		'OVERRIDE_ITEM_DISCOUNT' => array(
			'name' => 'OVERRIDE_ITEM_DISCOUNT',
			'type' => 'U',
			'default' => '{"override_item_discount":"false"}'
		),

		'ALLOW_PRICE_LIST' => array(
			'name' => 'ALLOW_PRICE_LIST',
			'type' => 'U',
			'default' => '{"allow_price_list_pos":"false", "allow_edit_price_list_price": "false"}'
		),
	)
);
