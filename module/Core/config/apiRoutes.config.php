<?php

return [
    'default_login_routes' => [
        0 => '/mobileDashBoard/defaultSet',
        1 => '/user/loginMobile'
    ],
    'mobile_dashboard_routes' => [
        0 => '/mobileDashBoard/getInvoiceAndPaidAmountsForDonut',
        1 => '/mobileDashBoard/getInvoicePaymentDetailsForDonut',
        2 => '/mobileDashBoard/getPurchaseInvoiceAndPurchasePaymentsForDonut',
        3 => '/mobileDashBoard/getPurchaseInvoicePaymentDetailsForDonut',
        4 => '/mobileDashBoard/getDataForSalesDashBoard',
        5 => '/mobileDashBoard/getDataForPurchaseDashBoard',
        6 => '/mobileDashBoard/getDataForSalesInvoiceBarChart',
        7 => '/mobileDashBoard/getDataForSalesPaymentBarChart',
        8 => '/mobileDashBoard/getDataForPurchaseInvoiceBarChart',
        9 => '/mobileDashBoard/getDataForPurchasePaymentBarChart',
        10 => '/mobileDashBoard/getNotificationCount',
        11 => '/mobileDashBoard/getNotificationForMobile',
        12 => '/mobileDashBoard/getDailyPurchaseInvoiceDetails',
        13 => '/mobileDashBoard/getDailySalesInvoiceDetails',
        14 => '/mobileDashBoard/getCompanyDetails',
    ],
    'mobile_dashboard_app_jwt_details' => [
        'key' => 'thinkcube@systems.mithila.shermila_rushanAlmeda%prashan&dhanushasilvaanKumarageAnuradhapuraJethawanaramaya@katugasthotaGAMperaliya',
        'issuer' => 'ezbiz_pro_back_end',
        'audience' => 'ezbiz_mobile_dash_board'
    ],
    'mobile_pos_app_jwt_details' => [
        'key' => 'thinkcube@systems.mithila.shermila_rushanAlmeda%prashan&dhanushasilvaanKumarageAnuradhapuraJethawanaramaya@katugasthotaGAMperaliya',
        'issuer' => 'ezbiz_pro_back_end',
        'audience' => 'ezbiz_mobile_dash_board'
    ],
    'mobile_pos_routes' => [
        0 => '/rest-api/product',
        1 => '/rest-api/customer',
        2 => '/rest-api/pos-dashboard',
        3 => '/rest-api/gift-card',
        4 => '/rest-api/pos-invoice',
        5 => '/rest-api/pos-authenticate',
        6 => '/rest-api/category',
        7 => '/rest-api/promotions',
        8 => '/rest-api/pos-users',
        9 => '/rest-api/user-roles',
        10 => '/rest-api/sms-configuration',
        11 => '/rest-api/sms-send'
    ]
    /**
    * The purpose of creating this file : for storing mobile dash board app and mobile pos app related specific data
    **/
];

