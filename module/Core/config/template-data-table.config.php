<?php

return array(
    'template-data-table' => array(
        'invoice' => array(
            'hide_table_header' => false,
            'hide_table_borders' => false,
            'columns' => array(
                array('name' => 'product_index', 'show' => true, 'width' => 5, 'order' => 0),
                array('name' => 'product_code', 'show' => true, 'width' => 10, 'order' => 1),
                array('name' => 'product_name', 'show' => true, 'width' => 28, 'order' => 2),
                array('name' => 'product_code_and_name', 'show' => false, 'width' => 35, 'order' => 3),
                array('name' => 'quantity', 'show' => true, 'width' => 12, 'order' => 4),
                array('name' => 'price', 'show' => true, 'width' => 15, 'order' => 5),
                array('name' => 'price_w_tax', 'show' => false, 'width' => 15, 'order' => 5),
                array('name' => 'discount', 'show' => true, 'width' => 15, 'order' => 6),
                array('name' => 'discount_per_or_val', 'show' => false, 'width' => 15, 'order' => 7),
                array('name' => 'total', 'show' => true, 'width' => 15, 'order' => 8),
                array('name' => 'total_wo_tx', 'show' => false, 'width' => 15, 'order' => 9),
                array('name' => 'total_wo_discounts', 'show' => false, 'width' => 15, 'order' => 10),
                array('name' => 'attribute_01', 'show' => false, 'width' => 7, 'order' => 11),
                array('name' => 'attribute_02', 'show' => false, 'width' => 7, 'order' => 12),
                array('name' => 'attribute_03', 'show' => false, 'width' => 7, 'order' => 13),
            ),
            'rows' => array(
                array('name' => 'sub_total', 'show' => true, 'order' => 0),
                array('name' => 'sub_total_wo_tax', 'show' => false, 'order' => 1),
                array('name' => 'sub_total_wo_discounts', 'show' => false, 'order' => 2),
                array('name' => 'discount', 'show' => true, 'order' => 3),
                array('name' => 'vat', 'show' => true, 'order' => 4),
                array('name' => 'total', 'show' => true, 'order' => 5)
            )
        ),
        'quotation' => array(
            'hide_table_header' => false,
            'hide_table_borders' => false,
            'columns' => array(
                array('name' => 'product_index', 'show' => true, 'width' => 5, 'order' => 0),
                array('name' => 'product_code', 'show' => true, 'width' => 10, 'order' => 1),
                array('name' => 'product_name', 'show' => true, 'width' => 28, 'order' => 2),
                array('name' => 'product_code_and_name', 'show' => false, 'width' => 35, 'order' => 3),
                array('name' => 'quantity', 'show' => true, 'width' => 12, 'order' => 4),
                array('name' => 'price', 'show' => true, 'width' => 15, 'order' => 5),
                array('name' => 'price_w_tax', 'show' => false, 'width' => 15, 'order' => 5),
                array('name' => 'discount', 'show' => true, 'width' => 15, 'order' => 6),
                array('name' => 'discount_per_or_val', 'show' => false, 'width' => 15, 'order' => 7),
                array('name' => 'total', 'show' => true, 'width' => 15, 'order' => 8),
                array('name' => 'total_wo_tx', 'show' => false, 'width' => 15, 'order' => 9),
                array('name' => 'total_wo_discounts', 'show' => false, 'width' => 15, 'order' => 10),
                array('name' => 'attribute_01', 'show' => false, 'width' => 7, 'order' => 11),
                array('name' => 'attribute_02', 'show' => false, 'width' => 7, 'order' => 12),
                array('name' => 'attribute_03', 'show' => false, 'width' => 7, 'order' => 13),
            ),
            'rows' => array(
                array('name' => 'sub_total', 'show' => true, 'order' => 0),
                array('name' => 'sub_total_wo_tax', 'show' => false, 'order' => 1),
                array('name' => 'sub_total_wo_discounts', 'show' => false, 'order' => 2),
                array('name' => 'discount', 'show' => true, 'order' => 3),
                array('name' => 'vat', 'show' => true, 'order' => 4),
                array('name' => 'total', 'show' => true, 'order' => 5)
            )
        ),
        'sales_order' => array(
            'hide_table_header' => false,
            'hide_table_borders' => false,
            'columns' => array(
                array('name' => 'product_index', 'show' => true, 'width' => 5, 'order' => 0),
                array('name' => 'product_code', 'show' => true, 'width' => 10, 'order' => 1),
                array('name' => 'product_name', 'show' => true, 'width' => 28, 'order' => 2),
                array('name' => 'product_code_and_name', 'show' => false, 'width' => 35, 'order' => 3),
                array('name' => 'quantity', 'show' => true, 'width' => 12, 'order' => 4),
                array('name' => 'price', 'show' => true, 'width' => 15, 'order' => 5),
                array('name' => 'price_w_tax', 'show' => false, 'width' => 15, 'order' => 5),
                array('name' => 'discount', 'show' => false, 'width' => 15, 'order' => 6),
                array('name' => 'discount_per_or_val', 'show' => false, 'width' => 15, 'order' => 7),
                array('name' => 'total', 'show' => true, 'width' => 15, 'order' => 8),
                array('name' => 'total_wo_tx', 'show' => false, 'width' => 15, 'order' => 9),
                array('name' => 'total_wo_discounts', 'show' => false, 'width' => 15, 'order' => 10),
                array('name' => 'attribute_01', 'show' => false, 'width' => 7, 'order' => 11),
                array('name' => 'attribute_02', 'show' => false, 'width' => 7, 'order' => 12),
                array('name' => 'attribute_03', 'show' => false, 'width' => 7, 'order' => 13),
            ),
            'rows' => array(
                array('name' => 'sub_total', 'show' => true, 'order' => 0),
                array('name' => 'sub_total_wo_tax', 'show' => false, 'order' => 1),
                array('name' => 'sub_total_wo_discounts', 'show' => false, 'order' => 2),
                array('name' => 'discount', 'show' => true, 'order' => 3),
                array('name' => 'vat', 'show' => false, 'order' => 4),
                array('name' => 'total', 'show' => true, 'order' => 5)
            )
        ),
        'delivery_note' => array(
            'hide_table_header' => false,
            'hide_table_borders' => false,
            'columns' => array(
                array('name' => 'product_index', 'show' => true, 'width' => 5, 'order' => 0),
                array('name' => 'product_code', 'show' => true, 'width' => 10, 'order' => 1),
                array('name' => 'product_name', 'show' => true, 'width' => 28, 'order' => 2),
                array('name' => 'product_code_and_name', 'show' => false, 'width' => 35, 'order' => 3),
                array('name' => 'quantity', 'show' => true, 'width' => 12, 'order' => 4),
                array('name' => 'price', 'show' => false, 'width' => 15, 'order' => 5),
                array('name' => 'price_w_tax', 'show' => false, 'width' => 15, 'order' => 5),
                array('name' => 'discount', 'show' => false, 'width' => 15, 'order' => 6),
                array('name' => 'discount_per_or_val', 'show' => false, 'width' => 15, 'order' => 7),
                array('name' => 'total', 'show' => false, 'width' => 15, 'order' => 8),
                array('name' => 'total_wo_tx', 'show' => false, 'width' => 15, 'order' => 9),
                array('name' => 'total_wo_discounts', 'show' => false, 'width' => 15, 'order' => 10),
                array('name' => 'attribute_01', 'show' => false, 'width' => 7, 'order' => 11),
                array('name' => 'attribute_02', 'show' => false, 'width' => 7, 'order' => 12),
                array('name' => 'attribute_03', 'show' => false, 'width' => 7, 'order' => 13),
            ),
            'rows' => array(
                array('name' => 'sub_total', 'show' => false, 'order' => 0),
                array('name' => 'sub_total_wo_tax', 'show' => false, 'order' => 1),
                array('name' => 'sub_total_wo_discounts', 'show' => false, 'order' => 2),
                array('name' => 'discount', 'show' => false, 'order' => 3),
                array('name' => 'vat', 'show' => false, 'order' => 4),
                array('name' => 'total', 'show' => false, 'order' => 5)
            )
        ),
    )
);
