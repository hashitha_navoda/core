<?php
return array(
    'countries'=>array(
        'sri_lanka'=>array(
            'si_LK'=>'සිංහල',
            'tl_LK'=>'தமிழ்',
            'en_US'=>'English'
        ),
        'international' => array(
            'en_US'=>'English'
        )
    )
);