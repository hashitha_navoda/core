<?php

return array(
    'tooltips' => array(
        /////////////////////////////////////////settings
        ///company--->>
        'SETTINGS_COMPANY_BR' => 'Business Registration Number',
        'SETTINGS_COMPANY_BROWSE' => 'Click to upload an image',
        ////tax--->>
        'SETTINGS_TAX_CHARGE' => 'Select Yes: <br>if you charge tax from your customer or if you need to display taxes seperately in Sales invoices. <br><br>Select No: <br>if you do not want to track taxes in the system.',
        //user --->>
        'SETTINGS_USER_VIEW_LOCATION' => 'Add or remove locations to the user',
        'SETTINGS_USER_VIEW_STATUS' => 'Activate/deactivate users',
        'SETTINGS_USER_ROLE_ADD' => 'Enter a name for the user role.',
        'SETTINGS_USER_ROLE_FEATURE' => 'Select the module you want to enable for the user role. Select relevant features for that user role.
                   Click \'Add Role\' once you are done with allll.',
        'SETTINGS_CATEGORY_ADD' => 'Do you want to add multiple categories?<br>Enter the category and parent category. Enter the prefix if you need to track items based on the category.<br>Ex: Car -> Toyota -> Allion -> Head lights.<br>To maintain category levels for inventory, enter category and parent category',
        //settings-> user-> manage license
        'SETTINGS_USER_MANAGELICENSE_USERCOUNT' => 'Number of users in the system',
        'SETTINGS_USER_MANAGELICENSE_LICENSECOUNT' => 'Number of licences assigned to users',
        'SETTINGS_USER_MANAGELICENSE_FREE_LICENSECOUNT' => 'Number of free user licences',
        'SETTINGS_USER_MANAGELICENSE_REMOVE_LICENSE' => 'Remove all licences assigned to users',
        'SETTINGS_USER_MANAGELICENSE_ASSIGN_LICENSE' => 'Automatically assign licences to users',
        //settings-> user -> edit user roles
        'SETTINGS_USER_EDITROLE' => 'Click here to enable / disable features for the users ',
        'SETTINGS_USER_EDITROLE_INVENTORY' => 'Enable / disable features in the Inventory module ',
        'SETTINGS_USER_EDITROLE_POS' => 'Enable / disable features in the POS module ',
        'SETTINGS_USER_EDITROLE_REPORTING' => 'Enable / disable features in the Reporting module ',
        'SETTINGS_USER_EDITROLE_SALES' => 'Enable / disable features in the Sales module ',
        'SETTINGS_USER_EDITROLE_SETTINGS' => 'Enable / disable features in Settings ',
        'SETTINGS_USER_EDITROLE_USERS_AND_SETTINGS' => 'Enable / disable features in ',
        'SETTINGS_USER_EDITROLE_OTHER_MODULES' => 'Enable / disable features in the ',
        'SETTINGS_USER_EDITROLE_SELECT_ALL' => 'Enable all features for this user role ',
        //settings---> displaysetup
        'SETTINGS_DIPLAYSETUP_DATEFORMAT' => 'Select the date format you want to print in your documents',
        'SETTINGS_DIPLAYSETUP_TIMEFORMAT' => 'Select the time format you want to print in your documents',
        /////invoice>customer->add
        'INVOICE_CUST_ADD_SNAME' => 'Enter an unique code or name. This will be used to identify this customer. ',
        'INVOICE_PAYMENTS_PAYTERM' => 'How long do you give your customers to settle payments, Immediately (Cash Only) or after certain number of days (within 7 Days, withing 14 Days, etc) ?',
        ////invoice->invoice-> cretae
        'INVOICE_INVO_CREATE_CUST' => 'Enter an existing customer',
        'INVOICE_INVO_CREATE_CUSTBUTTON' => 'Add a new customer',
        'INVOICE_INVO_CREATE_DUEDATE' => 'Enter Invoice Due Date',
        'INVOICE_INVO_CREATE_PAYTERM' => 'Select payment term',
        'INVOICE_INVO_CREATE_OUTSTANDING' => 'Amount the customer has to pay',
        'INVOICE_INVO_CREATE_CREDIT' => 'Credit balance of the customer',
        'INVOICE_INVO_CREATE_SALEPERSON' => 'Select the sales person name',
        'INVOICE_INVO_CREATE_QUANTITY' => 'Invoice quantity',
        'INVOICE_INVO_CREATE_PRICE' => 'Unit price',
        'INVOICE_INVO_CREATE_DISC_ITEM' => 'Discount per Item',
        'INVOICE_INVO_CREATE_TAX' => 'applicable taxes',
        'INVOICE_INVO_CREATE_ADDBTN' => 'Add Record',
        'INVOICE_INVO_CREATE_DISPLAY_TAX' => 'Tick to print taxes on the invoice',
        'INVOICE_INVO_CREATE_DELI_CHARGE' => 'Tick to add delivery charges',
        'INVOICE_INVO_CREATE_ENTER_DELI_CHARGE' => 'Enter delivery charges',
        'INVOICE_INVO_CREATE_ADD_COMMENT' => 'Add a comment to the invoice. This will be printed on the Invoice',
        //////////////Make payment for this invoice
        //INVOICE->CUSTOMER-> ADD
        'INVOICE_ADDCUSTOMER_SEARCH_CUST' => 'Select a Customer to view details',
        //CRM->Contacts-> Contact lists
        'CONTACT_LIST_SEARCH_CUST' => 'Select a Customer to Customer List',
        //INVOICE->CUSTOMER-> IMPORT
        'INVOICE_IMPORTCUSTOMER_FIELD_DATA' => 'The first row of your data sheet',
        'INVOICE_IMPORTCUSTOMER_FIELD_NAME' => 'Select the relevant field to add data',
        //SALES->QUOTATION->CREATE
        'QUOTATION_CREATE_VALID_TILL_DATE' => 'Quotation expiry date',
        'QUOTATION_DEFAULT_CUS_CHANGE' => 'Select Customer Profile',
        //SALES->SALESORDER->CREATE
        'SALESORDER_CREATE_COMPLETION_DATE' => 'Sales Order completion date',
        //SALES->SR->CREATE
        'SALESRETURN_CREATE_SR_NUM' => 'Sales Return Number',
        //////inventory -> supplier -> add
        'INVENTORY_SUPP_ADD_CODE' => 'Enter an unique code or name. This will be used to identify this supplier.',
        'INVENTORY_SUPP_ADD_CATEGORY' => 'Add a category to your suppliers to identify them easily. E.g.: Permanent suppliers, Temperory suppliers, Raw Material Suppliers',
        'INVENTORY_SUPP_ADD_CRLIMIT' => 'This is the credit limit given to you by the supplier.',
        //inventory -> purchase order -> create
        'INVENTORY_PO_CREATE_SUPPREF' => 'Enter the Supplier Reference Number from the source document <br>e.g.: Quotation from Supplier or any document',
        'INVENTORY_PO_CREATE_PONUM' => 'Purchase Order Number',
        //invrntory-> pv->cretae
        'INVENTORY_PV_CREATE_PVNUM' => 'Payment Voucher Number',
        //inventory -> item ->add
        'INVENTORY_ITEM_ADD_CODE' => 'Enter an unique code or name. This will be used to identify this item. ',
        //inventory-> payments->create
        'INVENTORY_PAYMENTS_PAYTERM' => 'What is the period your supplier set for you, to settle payments: Immediately (Cash Only) or after certain number of days (within 7 Days, withing 14 Days, etc) ?',
        //General
        //document->view
        'DOCUMENT_TEMPLATE' => 'Select Template of the document',
        'DOCUMENT_PRINT' => 'Print document',
        'DOCUMENT_EMAIL' => 'Email document',
        'DOCUMENT_ADDNEW' => 'Add new document',
        //wizard-account
        'SETTINGS_ACCOUNTING_ENABLE' => 'Additional charges will be added',
        ////sms--->>
        'SETTINGS_SMS_ENABLE' => 'Enable switch: <br>if you want to send notification via sms to your customer. <br><br>Disable switch: <br>if you do not want to send notification via sms to your customer.',
        'SMS_NOTIFICATION_AFTER_INVOICE_ENABLE' => 'Enable switch: <br>if you want to send notification sms to your customer after invoice creation. <br><br>Disable switch: <br>if you do not want to send notification via sms to your customer after invoice creation',
    )
);
