<?php
return array(
  'dateformats'=> array(
    'yyyy/mm/dd' => 'YYYY/MM/DD',
      
    //'YYYY/MM/DD' => ['Y/m/d', 'yyyy/mm/dd'],  
    'yyyy-mm-dd' => 'YYYY-MM-DD',  
    'yyyy.mm.dd' => 'YYYY.MM.DD',  
    'mm/dd/yyyy' => 'MM/DD/YYYY',  
    'mm-dd-yyyy' => 'MM-DD-YYYY',  
    'mm.dd.yyyy' => 'MM.DD.YYYY',  
    'dd/mm/yyyy' => 'DD/MM/YYYY',  
    'dd-mm-yyyy' => 'DD-MM-YYYY',  
    'dd.mm.yyyy' => 'DD.MM.YYYY',
    //'dd.mm.yy' => 'YYYY-MM-DD',
            )
);

//array_keys(Config::get('dateformats'))