<?php

return array(
    'home_menu' => array(
        'customer' => array(
            'controller' => 'customer',
            'action' => 'index',
            'menu_name' => 'Customers'
        ),
        'company' => array(
            'controller' => 'company',
            'action' => 'index',
            'menu_name' => 'Company'
        ),
        'invoice' => array(
            'controller' => 'invoice',
            'action' => 'index',
            'menu_name' => 'Invoices'
        ),
        'product' => array(
            'controller' => 'product',
            'action' => 'index',
            'menu_name' => 'Products'
        )
    ),
    'customer_upper_menu' => array(
        'customerlist' => array(
            'controller' => 'customer',
            'action' => 'index',
            'menu_name' => 'View Customers'
        ),
        'customerAdd' => array(
            'controller' => 'customer',
            'action' => 'add',
            'menu_name' => 'Add Customer'
        ),
        'customerImport' => array(
            'controller' => 'customer',
            'action' => 'import',
            'menu_name' => 'Customer Import'
        ),
        'customerMassedit' => array(
            'controller' => 'customer',
            'action' => 'massedit',
            'menu_name' => 'Customer Mass Edit'
        ),
        'customerCategory' => array(
            'controller' => 'customerCategory',
            'action' => 'index',
            'menu_name' => 'Customer Category'
        ),
        'ratingTypes' => array(
            'controller' => 'ratingTypes',
            'action' => 'index',
            'menu_name' => 'Rating Types'
        ),
    ),
    'sales_customer_upper_menu' => array(
        'customerlist' => array(
            'controller' => 'customer',
            'action' => 'sales-customer-index',
            'menu_name' => 'View Customers'
        ),
        'customerAdd' => array(
            'controller' => 'customer',
            'action' => 'sales-customer-add',
            'menu_name' => 'Add Customer'
        ),
    ),
    'sales_customer_update_upper_menu' => array(
        'customerlist' => array(
            'controller' => 'customer',
            'action' => 'sales-customer-index',
            'menu_name' => 'View Customers'
        ),
        'customerUpdate' => array(
            'controller' => 'customer',
            'action' => 'sales-customer-index',
            'menu_name' => 'Update Customer'
        ),
    ),
//    'product_upper_menu' => array(
//        'productlist' => array(
//            'controller' => 'product',
//            'action' => 'edit',
//            'menu_name' => 'Product List'
//        ),
//        'productadd' => array(
//            'controller' => 'product',
//            'action' => 'index',
//            'menu_name' => 'Add Product'
//        ),
//        'productimport' => array(
//            'controller' => 'product',
//            'action' => 'import',
//            'menu_name' => 'Product Import'
//        ),
//        'productMassedit' => array(
//            'controller' => 'product',
//            'action' => 'massedit',
//            'menu_name' => 'Product Mass Edit'
//        ),
//    ),
    'quotation_upper_menu' => array(
        'create_quotation' => array(
            'controller' => 'quotation',
            'action' => 'create',
            'menu_name' => 'Create Quotation'
        ),
        'view_quotation' => array(
            'controller' => 'quotation',
            'action' => 'list',
            'menu_name' => 'View Quotations'
        )
    ),
    'customer_order_upper_menu' => array(
        'create_quotation' => array(
            'controller' => 'customerOrder',
            'action' => 'create',
            'menu_name' => 'Create Customer Order'
        ),
        'view_quotation' => array(
            'controller' => 'customerOrder',
            'action' => 'list',
            'menu_name' => 'View Customer Orders'
        )
    ),
    'invoice_upper_menu' => array(
        'invoice' => array(
            'controller' => 'invoice',
            'action' => 'create',
            'menu_name' => 'Create Invoice'
        ),
        'view invoice' => array(
            'controller' => 'invoice',
            'action' => 'view',
            'menu_name' => 'View Invoices'
        ),
        'view draft invoice' => array(
            'controller' => 'invoice',
            'action' => 'draftView',
            'menu_name' => 'View Draft Invoices'
        ),
        'view Pos invoice' => array(
            'controller' => 'invoice',
            'action' => 'viewPosList',
            'menu_name' => 'View POS Invoices'
        ),
        'Create Dispatch Note' => array(
            'controller' => 'dispatch-note',
            'action' => 'create',
            'menu_name' => 'Create Dispatch Note'
        ),
        'View Dispatch Note' => array(
            'controller' => 'dispatch-note',
            'action' => 'view',
            'menu_name' => 'View Dispatch Note'
        )
    ),
    'salesorder_upper_menu' => array(
        'Create salesOrder' => array(
            'controller' => 'salesOrders',
            'action' => 'create',
            'menu_name' => 'Create Sales Order'
        ),
        'View Sales Order' => array(
            'controller' => 'salesOrders',
            'action' => 'list',
            'menu_name' => 'View Sales Orders'
        )
    ),
    'customerpayments_upper_menu' => array(
        'Customerpayments' => array(
            'controller' => 'customerPayments',
            'action' => 'index',
            'menu_name' => 'Create Payment'
        ),
        'CustomerPaymentsView' => array(
            'controller' => 'customerPayments',
            'action' => 'view',
            'menu_name' => 'View Payments'
        ),
        'CustomerChequePaymentsView' => array(
            'controller' => 'customerPayments',
            'action' => 'viewCheques',
            'menu_name' => 'View Cheques'
        ),
        'CustomerPaymentPosView' => array(
            'controller' => 'customerPayments',
            'action' => 'viewPos',
            'menu_name' => 'View POS Payments'
        ),
    ),
    'deliverynote_upper_menu' => array(
        'Deliverynotes' => array(
            'controller' => 'delivery-note',
            'action' => 'create',
            'menu_name' => 'Create Delivery Note'
        ),
        'DeliverynotesView' => array(
            'controller' => 'delivery-note',
            'action' => 'view',
            'menu_name' => 'View Delivery Notes'
        ),
    ),
    'sales_returns_upper_menu' => array(
        'Returns' => array(
            'controller' => 'return',
            'action' => 'create',
            'menu_name' => 'Create Return'
        ),
        'ReturnsView' => array(
            'controller' => 'return',
            'action' => 'view',
            'menu_name' => 'View Returns'
        ),
    ),
    'invoice_side_menu' => array(
        'dashboard' => array(
            'controller' => 'sales-dashboard',
            'action' => 'index',
            'menu_name' => 'Sales Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'customer' => array(
            'controller' => 'customer',
            'action' => 'salesCustomerIndex',
            'menu_name' => 'Customers',
            'icon' => 'fa fa-user'
        ),
        'quotation' => array(
            'controller' => 'quotation',
            'action' => 'create',
            'menu_name' => 'Quotations',
            'icon' => 'fa fa-credit-card'
        ),
        'customerOrder' => array(
            'controller' => 'customerOrder',
            'action' => 'create',
            'menu_name' => 'Customer Orders',
            'icon' => 'fa fa-credit-card'
        ),
        'salesOrder' => array(
            'controller' => 'salesOrders',
            'action' => 'create',
            'menu_name' => 'Sales Orders',
            'icon' => 'fa fa-gavel'
        ),
        'deliverynotes' => array(
            'controller' => 'delivery-note',
            'action' => 'create',
            'menu_name' => 'Delivery Notes',
            'icon' => 'fa fa-file-text-o'
        ),
        'invoice' => array(
            'controller' => 'invoice',
            'action' => 'create',
            'menu_name' => 'Invoices',
            'icon' => 'fa fa-file-o'
        ),
        'customerpayments' => array(
            'controller' => 'customerPayments',
            'action' => 'index',
            'menu_name' => 'Payments',
            'icon' => 'fa fa-usd'
        ),
        'return' => array(
            'controller' => 'return',
            'action' => 'create',
            'menu_name' => 'Returns',
            'icon' => 'fa fa-sign-in'
        ),
        'creditnote' => array(
            'controller' => 'creditNote',
            'action' => 'create',
            'menu_name' => 'Credit Note',
            'icon' => 'fa fa-sign-out'
        ),
        'creditnotepayments' => array(
            'controller' => 'creditNotePayments',
            'action' => 'create',
            'menu_name' => 'Credit Note Payments',
            'icon' => 'fa fa-usd'
        ),
        'report' => array(
            'controller' => 'salesReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        )
    ),
    'invoice_side_menu1' => array(
        'dashboard' => array(
            'controller' => 'sales-dashboard',
            'action' => 'index',
            'menu_name' => 'Sales Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'customer' => array(
            'controller' => 'customer',
            'action' => 'salesCustomerIndex',
            'menu_name' => 'Customers',
            'icon' => 'fa fa-user'
        ),
        'invoice' => array(
            'controller' => 'invoice',
            'action' => 'create',
            'menu_name' => 'Invoices',
            'icon' => 'fa fa-file-o'
        ),
        'customerpayments' => array(
            'controller' => 'customerPayments',
            'action' => 'index',
            'menu_name' => 'Payments',
            'icon' => 'fa fa-usd'
        ),
        'creditnote' => array(
            'controller' => 'creditNote',
            'action' => 'create',
            'menu_name' => 'Credit Note',
            'icon' => 'fa fa-sign-out'
        ),
        'report' => array(
            'controller' => 'salesReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        )
    ),
    'invoice_side_menu2' => array(
        'dashboard' => array(
            'controller' => 'sales-dashboard',
            'action' => 'index',
            'menu_name' => 'Sales Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'customer' => array(
            'controller' => 'customer',
            'action' => 'salesCustomerIndex',
            'menu_name' => 'Customers',
            'icon' => 'fa fa-user'
        ),
        'quotation' => array(
            'controller' => 'quotation',
            'action' => 'create',
            'menu_name' => 'Quotations',
            'icon' => 'fa fa-credit-card'
        ),
        'deliverynotes' => array(
            'controller' => 'delivery-note',
            'action' => 'create',
            'menu_name' => 'Delivery Notes',
            'icon' => 'fa fa-file-text-o'
        ),
        'invoice' => array(
            'controller' => 'invoice',
            'action' => 'create',
            'menu_name' => 'Invoices',
            'icon' => 'fa fa-file-o'
        ),
        'customerpayments' => array(
            'controller' => 'customerPayments',
            'action' => 'index',
            'menu_name' => 'Payments',
            'icon' => 'fa fa-usd'
        ),
        'return' => array(
            'controller' => 'return',
            'action' => 'create',
            'menu_name' => 'Returns',
            'icon' => 'fa fa-sign-in'
        ),
        'creditnote' => array(
            'controller' => 'creditNote',
            'action' => 'create',
            'menu_name' => 'Credit Note',
            'icon' => 'fa fa-sign-out'
        ),
        'report' => array(
            'controller' => 'salesReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        )
    ),
    'company_side_menu' => array(
        'company' => array(
            'controller' => 'company',
            'action' => 'index',
            'menu_name' => 'Organization',
            'icon' => 'fa fa-building-o'
        ),
        'Organization Structure' => array(
            'controller' => 'company',
            'action' => 'administraionOfficial',
            'menu_name' => 'Organization Structure',
            'icon' => 'fa fa-sitemap'
        ),
        'tax' => array(
            'controller' => 'tax',
            'action' => 'index',
            'menu_name' => 'Tax',
            'icon' => 'fa fa-money'
        ),
         'approval-workflow' => array(
            'controller' => 'expense-category',
            'action' => 'create',
            'menu_name' => 'Approval Workflow',
            'icon' => 'fa fa-sitemap'
        ),
        'user' => array(
            'controller' => 'user',
            'action' => 'index',
            'menu_name' => 'User',
            'icon' => 'fa fa-user'
        ),
        'Display setup' => array(
            'controller' => 'company',
            'action' => 'display',
            'menu_name' => 'Display Setup',
            'icon' => 'fa fa-desktop'
        ),
        'Add Reference' => array(
            'controller' => 'company',
            'action' => 'reference',
            'menu_name' => 'Add Reference',
            'icon' => 'fa fa-tags'
        ),
        'Product setup' => array(
            'controller' => 'category',
            'action' => 'index',
            'menu_name' => 'Product Setup',
            'icon' => 'fa fa-th-large'
        ),
        'location' => array(
            'controller' => 'location',
            'action' => 'index',
            'menu_name' => 'Location',
            'icon' => 'fa fa-map-marker'
        ),
        'custom-currency' => array(
            'controller' => 'company',
            'action' => 'customCurrency',
            'menu_name' => 'Custom Currency',
            'icon' => 'fa fa-money'
        ),
         'audit-trail' => array(
            'controller' => 'audit-log',
            'action' => 'auditLog',
            'menu_name' => 'Audit Trial',
            'icon' => 'fa fa-history'
        ),
        'dimensions' => array(
            'controller' => 'dimension',
            'action' => 'index',
            'menu_name' => 'Dimensions',
            'icon' => 'fa fa-sitemap'
        ),
        'Signatures' => array(
            'controller' => 'signature',
            'action' => 'index',
            'menu_name' => 'User Signatures',
            'icon' => 'fa fa-sitemap'
        ),
        'notifications' => array(
            'controller' => 'smsSettings',
            'action' => 'index',
            'menu_name' => 'Notification Settings',
            'icon' => 'fa fa-bell-o'
        ),
        'General settings' => array(
            'controller' => 'display-settings',
            'action' => 'index',
            'menu_name' => 'General settings',
            'icon' => 'fa fa-lock'
        ),
    ),
    'organization_structure_down_menu' => array(
        'Administraion Officials' => array(
            'controller' => 'company',
            'action' => 'administraionOfficial',
            'menu_name' => 'Administraion Officials'
        ),
        'Additional Details' => array(
            'controller' => 'company',
            'action' => 'additionalDetail',
            'menu_name' => 'Additional Details'
        ),
    ),
    'approval_workflow_down_menu' => array(
        'Approvers' => array(
            'controller' => 'approver',
            'action' => 'create',
            'menu_name' => 'Approvers',
        ),
        'Approval Workflows' => array(
            'controller' => 'approval-workflow',
            'action' => 'create',
            'menu_name' => 'Approval Workflows',
        ),
        'Approval Workflow Settings' => array(
            'controller' => 'approval-workflow-settings',
            'action' => 'index',
            'menu_name' => 'Workflow Settings',
        ),
        'Expence Categories' => array(
            'controller' => 'expense-category',
            'action' => 'create',
            'menu_name' => 'Voucher Categories',
        ),
        'Expence Types' => array(
            'controller' => 'expense-type',
            'action' => 'create',
            'menu_name' => 'Voucher Types',
        ),
        'Purchase Requisition Categories' => array(
            'controller' => 'purchase-requisition-category',
            'action' => 'create',
            'menu_name' => 'Purchase Requisition Categories',
        ),
        'Purchase Requisition Types' => array(
            'controller' => 'purchase-requisition-type',
            'action' => 'create',
            'menu_name' => 'Purchase Requisition Types',
        ),
    ),
    'portal_payments_upper_menu' => array(
        'account-license' => array(
            'controller' => 'user',
            'action' => 'account-license',
            'menu_name' => 'Account License'
        ),
        'user-licenses' => array(
            'controller' => 'user',
            'action' => 'user-licenses',
            'menu_name' => 'User Licenses'
        ),
        'payment-method' => array(
            'controller' => 'user',
            'action' => 'change-payment-method',
            'menu_name' => 'Payment Methods'
        ),
    ),
    'general_settings_upper_menu' => array(
        'display-settings' => array(
            'controller' => 'display-settings',
            'action' => 'index',
            'menu_name' => 'Display Settings'
        )
    ),
    'settings_products_upper_menu' => array(
        'Category' => array(
            'controller' => 'category',
            'action' => 'index',
            'menu_name' => 'Category Setup'
        ),
        'Unit Of Measure' => array(
            'controller' => 'uom',
            'action' => 'index',
            'menu_name' => 'Unit of Measure Setup'
        ),
        'Item Attributes' => array(
            'controller' => 'item-attributes',
            'action' => 'index',
            'menu_name' => 'Item Attributes'
        ),
        'Landing Cost Type' => array(
            'controller' => 'landing-cost-type',
            'action' => 'index',
            'menu_name' => 'Landing Cost Type'
        ),
        'ServiceChargeType' => array(
            'controller' => 'service-charge-type',
            'action' => 'index',
            'menu_name' => 'Service Charge Type'
        ),
        'MrpSettings' => array(
            'controller' => 'mrp-setting',
            'action' => 'index',
            'menu_name' => 'MRP Settings'
        ),
        'ViewMrpSettings' => array(
            'controller' => 'mrp-setting',
            'action' => 'list',
            'menu_name' => 'View MRP Details'
        ),
        'Discount Schemes' => array(
            'controller' => 'discount-schemes',
            'action' => 'index',
            'menu_name' => 'Discount Schemes'
        )
    ),
    'settings_promotions_upper_menu' => array(
        'addPromotion' => array(
            'controller' => 'promotions',
            'action' => 'create',
            'menu_name' => 'Add Promotion'
        ),
        'viewPromotion' => array(
            'controller' => 'promotions',
            'action' => 'list',
            'menu_name' => 'View Promotion'
        )
    ),
    'credit_note_upper_menu' => array(
        'CreateInvoiceReturn' => array(
            'controller' => 'invoice-return',
            'action' => 'create',
            'menu_name' => 'Create Invoice Return'
        ),
        'ViewInvoiceReturn' => array(
            'controller' => 'invoice-return',
            'action' => 'view',
            'menu_name' => 'View Invoice Return'
        ),
        'ViewDraftInvoiceReturn' => array(
            'controller' => 'invoice-return',
            'action' => 'viewDraft',
            'menu_name' => 'View Draft Invoice Return'
        ),
        'CreateCreditNote' => array(
            'controller' => 'creditNote',
            'action' => 'create',
            'menu_name' => 'Create Credit Note'
        ),
        'ViewCreditNote' => array(
            'controller' => 'creditNote',
            'action' => 'view',
            'menu_name' => 'View Credit Note'
        ),
    ),
    'credit_note_payments_upper_menu' => array(
        'CreateCreditNotePayments' => array(
            'controller' => 'creditNotePayments',
            'action' => 'create',
            'menu_name' => 'Create Payments'
        ),
        'ViewCreditNotePayments' => array(
            'controller' => 'creditNotePayments',
            'action' => 'view',
            'menu_name' => 'View Payments'
        ),
    ),
    'user_upper_menu' => array(
        'userView' => array(
            'controller' => 'user',
            'action' => 'index',
            'menu_name' => 'Users'
        ),
        'UserEditRoles' => array(
            'controller' => 'user',
            'action' => 'get-user-role-permissions',
            'menu_name' => 'Edit User Roles'
        ),
        'UserAddRoles' => array(
            'controller' => 'user',
            'action' => 'add-user-role-permissions',
            'menu_name' => 'Add User Roles'
        ),
        'SalesPerson' => array(
            'controller' => 'SalesPerson',
            'action' => 'index',
            'menu_name' => 'Sales Person'
        ),
        'ManageLicense' => array(
            'controller' => 'user',
            'action' => 'manageLicense',
            'menu_name' => 'Manage Licenses'
        ),
    ),
     'notification_settings_upper_menu' => array(
        'smsSettings' => array(
            'controller' => 'smsSettings',
            'action' => 'index',
            'menu_name' => 'SMS Setting'
        ),
    ),
    'inventory_side_menu' => array(
        'dashboard' => array(
            'controller' => 'inventory-dashboard',
            'action' => 'index',
            'menu_name' => 'Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'item' => array(
            'controller' => 'product',
            'action' => 'list',
            'menu_name' => 'Item',
            'icon' => 'fa fa-th-large'
        ),
        'grn' => array(
            'controller' => 'grn',
            'action' => 'create',
            'menu_name' => 'GRN',
            'icon' => 'fa fa-file-text-o'
        ),
        'transfer' => array(
            'controller' => 'transfer',
            'action' => 'create',
            'menu_name' => 'Transfer',
            'icon' => 'fa fa-exchange'
        ),
        'inv_adjustment' => array(
            'controller' => 'inventoryAdjustment',
            'action' => 'index',
            'menu_name' => 'Adjustment',
            'icon' => 'fa fa-adjust'
        ),
        'bills_of_material' => array(
            'controller' => 'bom',
            'action' => 'create',
            'menu_name' => 'Bill of Material',
            'icon' => 'fa fa-sitemap'
        ),
        'compound_costing' => array(
            'controller' => 'compoundCosting',
            'action' => 'create',
            'menu_name' => 'Landing Cost',
            'icon' => 'fa fa-sitemap'
        ),
        'Fixed Asset Management' => array(
            'controller' => 'fixedAssetManagement',
            'action' => 'listFixedAsset',
            'menu_name' => 'Fixed Asset Management',
            'icon' => 'fa fa-truck'
        ),
        'report' => array(
            'controller' => 'stockInHandReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        ),
    ),
    'fixed_asset_upper_menu' => array(
        'FixedAssetList' => array(
            'controller' => 'fixedAssetManagement',
            'action' => 'listFixedAsset',
            'menu_name' => 'View Fixed Assets'
        ),
    ),
    'inventory_side_menu1' => array(
        'dashboard' => array(
            'controller' => 'inventory-dashboard',
            'action' => 'index',
            'menu_name' => 'Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'item' => array(
            'controller' => 'product',
            'action' => 'list',
            'menu_name' => 'Item',
            'icon' => 'fa fa-th-large'
        ),
        'grn' => array(
            'controller' => 'grn',
            'action' => 'create',
            'menu_name' => 'GRN',
            'icon' => 'fa fa-file-text-o'
        ),
        'inv_adjustment' => array(
            'controller' => 'inventoryAdjustment',
            'action' => 'index',
            'menu_name' => 'Adjustment',
            'icon' => 'fa fa-adjust'
        ),
        'report' => array(
            'controller' => 'stockInHandReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        ),
    ),
    'inventory_side_menu2' => array(
        'dashboard' => array(
            'controller' => 'inventory-dashboard',
            'action' => 'index',
            'menu_name' => 'Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'item' => array(
            'controller' => 'product',
            'action' => 'list',
            'menu_name' => 'Item',
            'icon' => 'fa fa-th-large'
        ),
        'grn' => array(
            'controller' => 'grn',
            'action' => 'create',
            'menu_name' => 'GRN',
            'icon' => 'fa fa-file-text-o'
        ),
        'transfer' => array(
            'controller' => 'transfer',
            'action' => 'create',
            'menu_name' => 'Transfer',
            'icon' => 'fa fa-exchange'
        ),
        'inv_adjustment' => array(
            'controller' => 'inventoryAdjustment',
            'action' => 'index',
            'menu_name' => 'Adjustment',
            'icon' => 'fa fa-adjust'
        ),
        'report' => array(
            'controller' => 'stockInHandReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        ),
    ),
    'purchasing_side_menu' => array(
        'dashboard' => array(
            'controller' => 'purchasing-dashboard',
            'action' => 'index',
            'menu_name' => 'Purchase Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'supplier' => array(
            'controller' => 'supplier',
            'action' => 'add',
            'menu_name' => 'Suppliers',
            'icon' => 'fa fa-truck'
        ),
        'Purchase Requisition' => array(
            'controller' => 'purchaseRequistion',
            'action' => 'create',
            'menu_name' => 'Purchase Requisition',
            'icon' => ' fa fa-cubes'
        ),
        'po' => array(
            'controller' => 'purchaseOrder',
            'action' => 'create',
            'menu_name' => 'Purchase Order',
            'icon' => 'fa fa-gavel'
        ),
        'pi' => array(
            'controller' => 'purchaseInvoice',
            'action' => 'create',
            'menu_name' => 'Purchase Invoice',
            'icon' => 'fa fa-file-text-o'
        ),
        'Supplierpayments' => array(
            'controller' => 'outGoingPayment',
            'action' => 'create',
            'menu_name' => 'Payments',
            'icon' => 'fa fa-usd'
        ),
        'purchaseReturns' => array(
            'controller' => 'purchaseReturns',
            'action' => 'create',
            'menu_name' => 'Returns',
            'icon' => 'fa fa-sign-in'
        ),
        'debitNote' => array(
            'controller' => 'debitNote',
            'action' => 'create',
            'menu_name' => 'Debit Note',
            'icon' => 'fa fa-sign-out'
        ),
        'debitNotePayment' => array(
            'controller' => 'debitNotePayments',
            'action' => 'create',
            'menu_name' => 'Debit Note Payments',
            'icon' => 'fa fa-usd'
        ),
        'inventoryMaterialRequisition' => array(
            'controller' => 'inventoryMaterialRequisition',
            'action' => 'create',
            'menu_name' => 'Material Requisition',
            'icon' => 'glyphicon glyphicon-list-alt'
        ),
        'report' => array(
            'controller' => 'supplierReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        )
    ),
    'purchasing_side_menu1' => array(
        'dashboard' => array(
            'controller' => 'purchasing-dashboard',
            'action' => 'index',
            'menu_name' => 'Purchase Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'supplier' => array(
            'controller' => 'supplier',
            'action' => 'add',
            'menu_name' => 'Suppliers',
            'icon' => 'fa fa-truck'
        ),
        'pi' => array(
            'controller' => 'purchaseInvoice',
            'action' => 'create',
            'menu_name' => 'Purchase Invoice',
            'icon' => 'fa fa-file-text-o'
        ),
        'Supplierpayments' => array(
            'controller' => 'outGoingPayment',
            'action' => 'create',
            'menu_name' => 'Payments',
            'icon' => 'fa fa-usd'
        ),
        'debitNote' => array(
            'controller' => 'debitNote',
            'action' => 'create',
            'menu_name' => 'Debit Note',
            'icon' => 'fa fa-sign-out'
        ),
        'report' => array(
            'controller' => 'supplierReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        )
    ),
    'purchasing_side_menu2' => array(
        'dashboard' => array(
            'controller' => 'purchasing-dashboard',
            'action' => 'index',
            'menu_name' => 'Purchase Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'supplier' => array(
            'controller' => 'supplier',
            'action' => 'add',
            'menu_name' => 'Suppliers',
            'icon' => 'fa fa-truck'
        ),
        'pi' => array(
            'controller' => 'purchaseInvoice',
            'action' => 'create',
            'menu_name' => 'Purchase Invoice',
            'icon' => 'fa fa-file-text-o'
        ),
        'Supplierpayments' => array(
            'controller' => 'outGoingPayment',
            'action' => 'create',
            'menu_name' => 'Payments',
            'icon' => 'fa fa-usd'
        ),
        'purchaseReturns' => array(
            'controller' => 'purchaseReturns',
            'action' => 'create',
            'menu_name' => 'Returns',
            'icon' => 'fa fa-sign-in'
        ),
        'debitNote' => array(
            'controller' => 'debitNote',
            'action' => 'create',
            'menu_name' => 'Debit Note',
            'icon' => 'fa fa-sign-out'
        ),
        'report' => array(
            'controller' => 'supplierReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        )
    ),
    'purchaseReturns_upper_menu' => array(
        'createPR' => array(
            'controller' => 'purchaseReturns',
            'action' => 'create',
            'menu_name' => 'Create PR'
        ),
        'viewPR' => array(
            'controller' => 'purchaseReturns',
            'action' => 'list',
            'menu_name' => 'View PRs'
        )
    ),
    'grn_upper_menu' => array(
        'createGrn' => array(
            'controller' => 'grn',
            'action' => 'create',
            'menu_name' => 'Create GRN'
        ),
        'viewGrn' => array(
            'controller' => 'grn',
            'action' => 'list',
            'menu_name' => 'View GRNs'
        ),
        'viewDraftGrn' => array(
            'controller' => 'grn',
            'action' => 'draftGrnList',
            'menu_name' => 'View Draft GRNs'
        )
    ),
    'inv_adjustment_upper_menu' => array(
        'createAdjustment' => array(
            'controller' => 'inventoryAdjustment',
            'action' => 'index',
            'menu_name' => 'Create Adjustment'
        ),
        'viewAdjustment' => array(
            'controller' => 'inventoryAdjustment',
            'action' => 'view',
            'menu_name' => 'View Adjustments'
        ),
    ),
    'supplier_upper_menu' => array(
        'addSupplier' => array(
            'controller' => 'supplier',
            'action' => 'add',
            'menu_name' => 'Add Supplier'
        ),
        'viewSupplier' => array(
            'controller' => 'supplier',
            'action' => 'view',
            'menu_name' => 'View Suppliers'
        ),
        'supplierCategory' => array(
            'controller' => 'supplier',
            'action' => 'supplierCategory',
            'menu_name' => 'Supplier Category'
        ),
        'supplierImport' => array(
            'controller' => 'supplier',
            'action' => 'import',
            'menu_name' => 'Supplier Import'
        )
    ),
    'supplierpayments_upper_menu' => array(
        'SupplierPayemnts' => array(
            'controller' => 'outGoingPayment',
            'action' => 'create',
            'menu_name' => 'Create Payment'
        ),
        'SupplierPayemntsView' => array(
            'controller' => 'outGoingPayment',
            'action' => 'view',
            'menu_name' => 'View PI Payments'
        ),
        'PaymentVoucherPayemntsView' => array(
            'controller' => 'outGoingPayment',
            'action' => 'viewPVPayments',
            'menu_name' => 'View PV Payments'
        ),
        'PaymentVoucherAdvancePayemntsView' => array(
            'controller' => 'outGoingPayment',
            'action' => 'viewPVAdvancePayments',
            'menu_name' => 'Advance Payments'
        )

    ),
    'po_upper_menu' => array(
        'createPO' => array(
            'controller' => 'purchaseOrder',
            'action' => 'create',
            'menu_name' => 'Create PO'
        ),
        'viewPO' => array(
            'controller' => 'purchaseOrder',
            'action' => 'list',
            'menu_name' => 'View POs'
        )
    ),
    'purchaseRequistion_upper_menu' => array(
        'createPurchaseRequistion' => array(
            'controller' => 'purchaseRequistion',
            'action' => 'create',
            'menu_name' => 'Create Purchase Requisition'
        ),
        'viewPurchaseRequistion' => array(
            'controller' => 'purchaseRequistion',
            'action' => 'list',
            'menu_name' => 'View Purchase Requisition'
        )
    ),
    'pi_upper_menu' => array(
        'createPI' => array(
            'controller' => 'purchaseInvoice',
            'action' => 'create',
            'menu_name' => 'Create PI'
        ),
        'viewPI' => array(
            'controller' => 'purchaseInvoice',
            'action' => 'list',
            'menu_name' => 'View PIs'
        )
    ),
    'product_upper_menu' => array(
        'addItem' => array(
            'controller' => 'product',
            'action' => 'index',
            'menu_name' => 'Add Item'
        ),
        'ItemList' => array(
            'controller' => 'product',
            'action' => 'list',
            'menu_name' => 'View Items'
        ),
        'locationItem' => array(
            'controller' => 'product',
            'action' => 'edit-products-by-location',
            'menu_name' => 'Edit Items By Location'
        ),
        'ItemImport' => array(
            'controller' => 'product',
            'action' => 'item-import',
            'menu_name' => 'Item Import'
        ),
        'GiftCard' => array(
            'controller' => 'giftCard',
            'action' => 'create',
            'menu_name' => 'Add Gift Card'
        ),
        'GiftCardView' => array(
            'controller' => 'giftCard',
            'action' => 'list',
            'menu_name' => 'View Gift Card'
        ),
        'ItemPriceList' => array(
            'controller' => 'product',
            'action' => 'item-price-list',
            'menu_name' => 'Items Price List'
        ),
        'FreeIssueItems' => array(
            'controller' => 'product',
            'action' => 'free-item-issue-list',
            'menu_name' => 'Free Issue Items'
        ),
    ),
    'transfer_upper_menu' => array(
        'transfer' => array(
            'controller' => 'transfer',
            'action' => 'create',
            'menu_name' => 'Add Transfer'
        ),
        'transferList' => array(
            'controller' => 'transfer',
            'action' => 'list',
            'menu_name' => 'View Transfers'
        )
    ),
    'compound_costing_upper_menu' => array(
        'costing_create' => array(
            'controller' => 'compoundCosting',
            'action' => 'create',
            'menu_name' => 'Costing'
        ),
        'costing_list' => array(
            'controller' => 'compoundCosting',
            'action' => 'list',
            'menu_name' => 'List'
        )    
    ),
    'bill_of_material_upper_menu' => array(
        'bom-create' => array(
            'controller' => 'bom',
            'action' => 'create',
            'menu_name' => 'BOM Create'
        ),
        'bom-list' => array(
            'controller' => 'bom',
            'action' => 'list',
            'menu_name' => 'BOM List'
        ),
        'bom-ad' => array(
            'controller' => 'bom',
            'action' => 'ad',
            'menu_name' => 'BOM A/D'
        ),
        'bom-ad-view' => array(
            'controller' => 'bom',
            'action' => 'ad-list',
            'menu_name' => 'BOM A/D LIST'
        )
    ),
    'invoice_report_upper_menu' => array(
        'salesReport' => array(
            'controller' => 'salesReport',
            'action' => 'index',
            'menu_name' => 'Sales'
        ),
        'customerReport' => array(
            'controller' => 'customerReport',
            'action' => 'index',
            'menu_name' => 'Customer'
        ),
        'dailySalesReport' => array(
            'controller' => 'dailySalesReport',
            'action' => 'index',
            'menu_name' => 'Items'
        ),
        'salesOrder' => array(
            'controller' => 'sales-order-report',
            'action' => 'index',
            'menu_name' => 'Sales Order'
        ),
        'posReport' => array(
            'controller' => 'posReport',
            'action' => 'index',
            'menu_name' => 'POS'
        ),
        'dailyDelivery' => array(
            'controller' => 'dailyDelivery',
            'action' => 'index',
            'menu_name' => 'Delivery Notes'
        ),
        'invoiceReport' => array(
            'controller' => 'invoiceReport',
            'action' => 'index',
            'menu_name' => 'Invoice'
        ),
        'salesPayment' => array(
            'controller' => 'salesPayment',
            'action' => 'index',
            'menu_name' => 'Payments'
        ),
        'quotationReport' => array(
            'controller' => 'quotationReport',
            'action' => 'index',
            'menu_name' => 'Quotation',
        ),
    ),
    'invoice_report_upper_menu1' => array(
        'salesReport' => array(
            'controller' => 'salesReport',
            'action' => 'index',
            'menu_name' => 'Sales'
        ),
        'customerReport' => array(
            'controller' => 'customerReport',
            'action' => 'index',
            'menu_name' => 'Customer'
        ),
        'dailySalesReport' => array(
            'controller' => 'dailySalesReport',
            'action' => 'index',
            'menu_name' => 'Items'
        ),
        'posReport' => array(
            'controller' => 'posReport',
            'action' => 'index',
            'menu_name' => 'POS'
        ),
        'invoiceReport' => array(
            'controller' => 'invoiceReport',
            'action' => 'index',
            'menu_name' => 'Invoice'
        ),
        'salesPayment' => array(
            'controller' => 'salesPayment',
            'action' => 'index',
            'menu_name' => 'Payments'
        ),
    ),
    'invoice_report_upper_menu2' => array(
        'salesReport' => array(
            'controller' => 'salesReport',
            'action' => 'index',
            'menu_name' => 'Sales'
        ),
        'customerReport' => array(
            'controller' => 'customerReport',
            'action' => 'index',
            'menu_name' => 'Customer'
        ),
        'dailySalesReport' => array(
            'controller' => 'dailySalesReport',
            'action' => 'index',
            'menu_name' => 'Items'
        ),
        'posReport' => array(
            'controller' => 'posReport',
            'action' => 'index',
            'menu_name' => 'POS'
        ),
        'dailyDelivery' => array(
            'controller' => 'dailyDelivery',
            'action' => 'index',
            'menu_name' => 'Delivery Notes'
        ),
        'invoiceReport' => array(
            'controller' => 'invoiceReport',
            'action' => 'index',
            'menu_name' => 'Invoice'
        ),
        'salesPayment' => array(
            'controller' => 'salesPayment',
            'action' => 'index',
            'menu_name' => 'Payments'
        ),
        'quotationReport' => array(
            'controller' => 'quotationReport',
            'action' => 'index',
            'menu_name' => 'Quotation',
        ),
    ),
    'purchasing_report_upper_menu' => array(
        'supplier' => array(
            'controller' => 'supplierReport',
            'action' => 'index',
            'menu_name' => 'Supplier'
        ),
        'purchase' => array(
            'controller' => 'purchaseReport',
            'action' => 'index',
            'menu_name' => 'Purchase'
        ),
        'payments' => array(
            'controller' => 'paymentReport',
            'action' => 'index',
            'menu_name' => 'Payments'
        ),
        'paymentVoucher' => array(
            'controller' => 'paymentVoucherReport',
            'action' => 'index',
            'menu_name' => 'Purchase Invoice'
        ),
    ),
    'purchasing_report_upper_menu1' => array(
        'supplier' => array(
            'controller' => 'supplierReport',
            'action' => 'index',
            'menu_name' => 'Supplier'
        ),
        'payments' => array(
            'controller' => 'paymentReport',
            'action' => 'index',
            'menu_name' => 'Payments'
        ),
        'paymentVoucher' => array(
            'controller' => 'paymentVoucherReport',
            'action' => 'index',
            'menu_name' => 'Purchase Invoice'
        ),
    ),
    'purchasing_report_upper_menu2' => array(
        'supplier' => array(
            'controller' => 'supplierReport',
            'action' => 'index',
            'menu_name' => 'Supplier'
        ),
        'payments' => array(
            'controller' => 'paymentReport',
            'action' => 'index',
            'menu_name' => 'Payments'
        ),
        'paymentVoucher' => array(
            'controller' => 'paymentVoucherReport',
            'action' => 'index',
            'menu_name' => 'Purchase Invoice'
        ),
    ),
    'inve_report_upper_menu' => array(
        'stock' => array(
            'controller' => 'stockInHandReport',
            'action' => 'index',
            'menu_name' => 'Stock In Hand'
        ),
        'itemTransfer' => array(
            'controller' => 'itemTransferReport',
            'action' => 'index',
            'menu_name' => 'Item Transfer'
        ),
        'grn' => array(
            'controller' => 'grnReport',
            'action' => 'index',
            'menu_name' => 'GRN'
        ),
    ),
    'inve_report_upper_menu1' => array(
        'stock' => array(
            'controller' => 'stockInHandReport',
            'action' => 'index',
            'menu_name' => 'Stock In Hand'
        ),
        'grn' => array(
            'controller' => 'grnReport',
            'action' => 'index',
            'menu_name' => 'GRN'
        ),
    ),
    'inve_report_upper_menu2' => array(
        'stock' => array(
            'controller' => 'stockInHandReport',
            'action' => 'index',
            'menu_name' => 'Stock In Hand'
        ),
        'itemTransfer' => array(
            'controller' => 'itemTransferReport',
            'action' => 'index',
            'menu_name' => 'Item Transfer'
        ),
        'grn' => array(
            'controller' => 'grnReport',
            'action' => 'index',
            'menu_name' => 'GRN'
        ),
    ),
    'debit_note_upper_menu' => array(
        'CreateDebitNote' => array(
            'controller' => 'debitNote',
            'action' => 'create',
            'menu_name' => 'Create Debit Note'
        ),
        'ViewDebitNote' => array(
            'controller' => 'debitNote',
            'action' => 'view',
            'menu_name' => 'View Debit Note'
        ),
    ),
    'debit_note_payment_upper_menu' => array(
        'CreateDebitNotePayment' => array(
            'controller' => 'debitNotePayments',
            'action' => 'create',
            'menu_name' => 'Create Payments'
        ),
        'ViewDebitNotePayment' => array(
            'controller' => 'debitNotePayments',
            'action' => 'view',
            'menu_name' => 'View Payment'
        ),
    ),
    'material_requisition_upper_menu' => array(
        'CreateMaterialRequisition' => array(
            'controller' => 'inventoryMaterialRequisition',
            'action' => 'create',
            'menu_name' => 'Check Materials'
        ),
    ),
    'job_card_side_menu' => array(
        'JobCardSetup' => array(
            'controller' => 'projectType',
            'action' => 'create',
            'menu_name' => 'Job Card Setup',
            'icon' => 'fa fa-cogs'
        ),
        'Inquiry_log' => array(
            'controller' => 'inquiryLog',
            'action' => 'createInquiry',
            'menu_name' => 'Inquiry Log',
            'icon' => 'fa fa-book'
        ),
        'Project' => array(
            'controller' => 'project',
            'action' => 'create',
            'menu_name' => 'Projects',
            'icon' => 'fa fa-suitcase'
        ),
        'Job' => array(
            'controller' => 'job',
            'action' => 'create',
            'menu_name' => 'Job',
            'icon' => 'fa fa-file-text-o',
        ),
        'Activity' => array(
            'controller' => 'activity',
            'action' => 'create',
            'menu_name' => 'Activity',
            'icon' => 'fa fa-gavel'
        ),
        'Progress' => array(
            'controller' => 'progress',
            'action' => 'create',
            'menu_name' => 'Progress Update',
            'icon' => 'fa fa-area-chart'
        ),
        'UniversalSearch' => array(
            'controller' => 'universalSearch',
            'action' => 'search',
            'menu_name' => 'Universal Search',
            'icon' => 'fa fa-globe'
        ),
        'JobDispatch' => array(
            'controller' => 'jobDispatch',
            'action' => 'index',
            'menu_name' => 'Job Dispatch',
            'icon' => 'fa fa-headphones'
        ),
        'Reports' => array(
            'controller' => 'jobCardReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o'
        ),
    ),
    'job_card_set_up_down_menu' => array(
        'CategorySetUp' => array(
            'controller' => 'projectType',
            'action' => 'create',
            'menu_name' => 'Category Setup',
        ),
        'EmployeeSetUp' => array(
            'controller' => 'designations',
            'action' => 'create',
            'menu_name' => 'Employee Setup',
        ),
        'GenaralSetUp' => array(
            'controller' => 'costType',
            'action' => 'create',
            'menu_name' => 'General Setup',
        ),
        'ContractorSetUp' => array(
            'controller' => 'contractor',
            'action' => 'create',
            'menu_name' => 'Contractor Setup',
        ),
    ),
    'job_card_category_set_up_upper_menu' => array(
        'ProjectType' => array(
            'controller' => 'projectType',
            'action' => 'create',
            'menu_name' => 'Project Types',
        ),
        'JobType' => array(
            'controller' => 'jobType',
            'action' => 'create',
            'menu_name' => 'Job Types',
        ),
        'ActivityType' => array(
            'controller' => 'activityType',
            'action' => 'create',
            'menu_name' => 'Activity Types',
        ),
    ),
    'job_card_employee_set_up_upper_menu' => array(
        'Designations' => array(
            'controller' => 'designations',
            'action' => 'create',
            'menu_name' => 'Designations',
        ),
        'Divisions' => array(
            'controller' => 'divisions',
            'action' => 'create',
            'menu_name' => 'Divisions',
        ),
        'Employee' => array(
            'controller' => 'employees',
            'action' => 'create',
            'menu_name' => 'Employees',
        ),
    ),
    'job_card_general_set_up_upper_menu' => array(
        'CostType' => array(
            'controller' => 'costType',
            'action' => 'create',
            'menu_name' => 'Cost Types',
        ),
        'InquiryType' => array(
            'controller' => 'inquiryType',
            'action' => 'create',
            'menu_name' => 'Inquiry Types',
        ),
        'InquiryStatus' => array(
            'controller' => 'inquiryStatus',
            'action' => 'create',
            'menu_name' => 'Inquiry Status',
        ),
        'JobStation' => array(
            'controller' => 'jobStation',
            'action' => 'create',
            'menu_name' => 'Job Stations',
        ),
        'InquirySetup' => array(
            'controller' => 'inquirySetup',
            'action' => 'index',
            'menu_name' => 'Inquiry Setup',
        ),
        'VehicleSetup' => array(
            'controller' => 'vehicle',
            'action' => 'index',
            'menu_name' => 'Vehicle Setup',
        )
    ),
    'job_card_job_upper_menu' => array(
        'JobCreate' => array(
            'controller' => 'job',
            'action' => 'create',
            'menu_name' => 'Create',
        ),
        'JobView' => array(
            'controller' => 'job',
            'action' => 'list',
            'menu_name' => 'View',
        ),
    ),
    'job_card_project_upper_menu' => array(
        'Project' => array(
            'controller' => 'project',
            'action' => 'create',
            'menu_name' => 'Create Project',
        ),
        'ProjectList' => array(
            'controller' => 'project',
            'action' => 'list',
            'menu_name' => 'Project List',
        ),
    ),
    'activity_upper_menu' => array(
        'CreateActivity' => array(
            'controller' => 'activity',
            'action' => 'create',
            'menu_name' => 'Create'
        ),
        'ViewActivity' => array(
            'controller' => 'activity',
            'action' => 'list',
            'menu_name' => 'View'
        ),
    ),
    'progress_upper_menu' => array(
        'UpdateProgress' => array(
            'controller' => 'progress',
            'action' => 'list',
            'menu_name' => 'Update Progress'
        ),
    ),
    'score_update_upper_menu' => array(
        'UpdateScore' => array(
            'controller' => 'scoreUpdate',
            'action' => 'list',
            'menu_name' => 'Update Score'
        ),
    ),
    'department_station_upper_menu' => array(
        'DepartmentCreate' => array(
            'controller' => 'departmentStation',
            'action' => 'create',
            'menu_name' => 'Department & Station'
        ),
    ),
    'employee_management_upper_menu' => array(
        'EmployeePerformance' => array(
            'controller' => 'employeeManagement',
            'action' => 'index',
            'menu_name' => 'Employee Performance'
        ),
    ),
    'cost_setting_upper_menu' => array(
        // 'RateCard' => array(
        //     'controller' => 'rateCard',
        //     'action' => 'create',
        //     'menu_name' => 'Rate Cards'
        // ),
        'Incentives' => array(
            'controller' => 'incentives',
            'action' => 'index',
            'menu_name' => 'Rate Cards and Incentives'
        ),
    ),
    'inquiry_upper_menu' => array(
        'CreateLog' => array(
            'controller' => 'inquiryLog',
            'action' => 'create',
            'menu_name' => 'Create',
        ),
        'ListView' => array(
            'controller' => 'inquiryLog',
            'action' => 'list',
            'menu_name' => 'View',
        ),
    ),
    'inquiry_setting_upper_menu' => array(
        'inquiryStatus' => array(
            'controller' => 'inquiryStatus',
            'action' => 'create',
            'menu_name' => 'Inquiry Status',
        ),
        'inquiryType' => array(
            'controller' => 'inquiryType',
            'action' => 'create',
            'menu_name' => 'Inquiry Type',
        )
    ),
    'inquiry_upper_menu_job_card' => array(
        'CreateLog' => array(
            'controller' => 'inquiryLog',
            'action' => 'createInquiry',
            'menu_name' => 'Create',
        ),
        'ListView' => array(
            'controller' => 'inquiryLog',
            'action' => 'listInquiry',
            'menu_name' => 'View',
        ),
    ),
    'universal_search_upper_menu' => array(
        'CreateLog' => array(
            'controller' => 'universalSearch',
            'action' => 'search',
            'menu_name' => 'Search',
        ),
    ),
    'job_dispatch_upper_menu' => array(
        'JobDispatch' => array(
            'controller' => 'jobDispatch',
            'action' => 'index',
            'menu_name' => 'View',
        ),
    ),
    'job_card_contractor_set_up_upper_menu' => array(
        'Contractor' => array(
            'controller' => 'contractor',
            'action' => 'create',
            'menu_name' => 'Contractor',
        ),
        'TemporaryContractor' => array(
            'controller' => 'temporaryContractor',
            'action' => 'index',
            'menu_name' => 'Temporary Contractor',
        ),
    ),
    'job_card_reports_upper_menu' => array(
        'JobCard' => array(
            'controller' => 'jobCardReport',
            'action' => 'index',
            'menu_name' => 'Job Card'
        )
    ),
    'job_reports_upper_menu' => array(
        'Employee' => array(
            'controller' => 'jobReport',
            'action' => 'index',
            'menu_name' => 'Employee'
        )
    ),
    'crm_side_menu' => array(
        'dashboard' => array(
            'controller' => 'crm-dashboard',
            'action' => 'index',
            'menu_name' => 'CRM Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'Promotions' => array(
            'controller' => 'promotions',
            'action' => 'create',
            'menu_name' => 'Promotions',
            'icon' => 'fa fa-gift',
        ),
        'Contacts' => array(
            'controller' => 'contacts',
            'action' => 'create',
            'menu_name' => 'Contacts',
            'icon' => 'fa fa-phone',
        ),
        'customer' => array(
            'controller' => 'customer',
            'action' => 'index',
            'menu_name' => 'Customers',
            'icon' => 'fa fa-user'
        ),
        'loyalty' => array(
            'controller' => 'loyalty',
            'action' => 'index',
            'menu_name' => 'Loyalty Card',
            'icon' => 'fa fa-credit-card'
        ),
        'Inquiry_log' => array(
            'controller' => 'inquiryLog',
            'action' => 'create',
            'menu_name' => 'Inquiry Log',
            'icon' => 'fa fa-book'
        ),
        'CustomerEvent' => array(
            'controller' => 'customer-event',
            'action' => 'index',
            'menu_name' => 'Customer Event',
            'icon' => 'fa fa-newspaper-o',
        ),
        'sms_notification' => array(
            'controller' => 'sms-notification',
            'action' => 'after-invoice',
            'menu_name' => 'Sms Notification',
            'icon' => 'fa fa-bell-o',
        ),
        'reports' => array(
            'controller' => 'loyalty-card-report',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o',
        ),
        'settings' => array(
            'controller' => 'inquiryStatus',
            'action' => 'create',
            'menu_name' => 'Settings',
            'icon' => 'fa fa-cog'
        ),
    ),
    'sms_notification_upper_menu' => array(
        'After_Invoice' => array(
            'controller' => 'sms-notification',
            'action' => 'after-invoice',
            'menu_name' => 'After Invoice',
        ),
        'After_Receipt' => array(
            'controller' => 'sms-notification',
            'action' => 'after-receipt',
            'menu_name' => 'After Receipt',
        ),
        'Due_Balance' => array(
            'controller' => 'sms-notification',
            'action' => 'due-balance',
            'menu_name' => 'Due Balance',
        ),
        'Due_Invoice' => array(
            'controller' => 'sms-notification',
            'action' => 'due-invoice',
            'menu_name' => 'Due Invoice',
        ),
        'Service_Reminder' => array(
            'controller' => 'sms-notification',
            'action' => 'service-reminder',
            'menu_name' => 'Service Reminder',
        ),
        'Post_Dated_Cheque' => array(
            'controller' => 'sms-notification',
            'action' => 'post-dated-cheque',
            'menu_name' => 'Post Dated Cheque',
        ),
    ),
    'crm_report_upper_menu' => array(
        'Create' => array(
            'controller' => 'loyalty-card-report',
            'action' => 'index',
            'menu_\name' => 'Loyalty Card',
        ),
    ),
    'promotions_up_down_menu' => array(
        'Creations' => array(
            'controller' => 'promotions',
            'action' => 'create',
            'menu_name' => 'Creations',
        ),
        'Emails' => array(
            'controller' => 'promotions',
            'action' => 'emails',
            'menu_name' => 'Campaigns',
        ),
    ),
    'settings_promotion_emails_upper_menu' => array(
        'Products' => array(
            'controller' => 'promotions',
            'action' => 'emails',
            'menu_name' => 'Products & Services'
        ),
        'directPromotions' => array(
            'controller' => 'promotions',
            'action' => 'directPromotion',
            'menu_name' => 'Direct Promotions'
        ),
        'customerEvents' => array(
            'controller' => 'promotions',
            'action' => 'customerEvents',
            'menu_name' => 'Customer Events'
        ),
//        'customerCategory' => array(
//            'controller' => 'promotions',
//            'action' => 'customerCategories',
//            'menu_name' => 'Customer Categories'
//        ),
        'viewEmail' => array(
            'controller' => 'promotions',
            'action' => 'viewEmails',
            'menu_name' => 'View Email'
        )
    ),
    'contacts_upper_menu' => array(
        'Contacts' => array(
            'controller' => 'contacts',
            'action' => 'create',
            'menu_name' => 'Contact Master'
        ),
        'Contacts-list' => array(
            'controller' => 'contacts',
            'action' => 'contact-list',
            'menu_name' => 'Contact List'
        )
    ),
    'accounting_side_menu' => array(
        'dashboard' => array(
            'controller' => 'account-dashboard',
            'action' => 'index',
            'menu_name' => 'Dashboard',
            'icon' => 'fa fa-bar-chart'
        ),
        'settings' => array(
            'controller' => 'bank',
            'action' => 'index',
            'menu_name' => 'Settings',
            'icon' => 'fa fa-cog'
        ),
        'expenses' => array(
            'controller' => 'expense-purchase-invoice',
            'action' => 'create',
            'menu_name' => 'Expenses',
            'icon' => 'fa fa-money'
        ),
        'cash_bank' => array(
            'controller' => 'cheque-deposit',
            'action' => 'index',
            'menu_name' => 'Cash & Bank',
            'icon' => 'fa fa-money'
        ),
        'income' => array(
            'controller' => 'income',
            'action' => 'create',
            'menu_name' => 'Incomes',
            'icon' => 'fa fa-money'
        ),
        'accounts' => array(
            'controller' => 'accounts',
            'action' => 'create',
            'menu_name' => 'Accounts',
            'icon' => 'fa fa-pencil '
        ),
        'chart_of_accounts' => array(
            'controller' => 'accounts',
            'action' => 'chartOfAccounts',
            'menu_name' => 'Chart Of Accounts',
            'icon' => 'fa fa-bar-chart'
        ),
        'journal_entries' => array(
            'controller' => 'journal-entries',
            'action' => 'create',
            'menu_name' => 'Journal Entries',
            'icon' => 'fa fa-folder-open'
        ),
        'reports' => array(
            'controller' => 'reconciliation-report',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-bar-chart-o',
        ),
    ),
    'expense_up_down_menu' => array(
        'payment_voucher' => array(
            'controller' => 'expense-purchase-invoice',
            'action' => 'create',
            'menu_name' => 'Payment Voucher',
        ),
        // 'cheques' => array(
        //     'controller' => 'cheque-deposit',
        //     'action' => 'index',
        //     'menu_name' => 'Cheques',
        // ),
        // 'reconciliation' => array(
        //     'controller' => 'reconciliation',
        //     'action' => 'index',
        //     'menu_name' => 'Reconciliation',
        // ),
        'petty-cash-float' => array(
            'controller' => 'petty-cash-float',
            'action' => 'index',
            'menu_name' => 'Petty Cash Float',
        ),
        'petty-cash-voucher' => array(
            'controller' => 'petty-cash-voucher',
            'action' => 'index',
            'menu_name' => 'Petty Cash Voucher',
        ),
        // 'transaction' => array(
        //     'controller' => 'transaction',
        //     'action' => 'index',
        //     'menu_name' => 'Transaction',
        // ),
    ),
    'cash_bank_up_down_menu' => array(
        'cheques' => array(
            'controller' => 'cheque-deposit',
            'action' => 'index',
            'menu_name' => 'Cheques',
        ),
        'reconciliation' => array(
            'controller' => 'reconciliation',
            'action' => 'index',
            'menu_name' => 'Reconciliation',
        ),
        'transaction' => array(
            'controller' => 'transaction',
            'action' => 'index',
            'menu_name' => 'Transaction',
        ),
    ),
    'crm_settings_up_down_menu' => array(
        'crm_inquiry_settings' => array(
            'controller' => 'inquiryStatus',
            'action' => 'create',
            'menu_name' => 'CRM Inquiry Settings',
        )
    ),
    'expense_up_down_menu1' => array(
        'payment_voucher' => array(
            'controller' => 'expense-purchase-invoice',
            'action' => 'create',
            'menu_name' => 'Payment Voucher',
        ),
        // 'cheques' => array(
        //     'controller' => 'cheque-deposit',
        //     'action' => 'index',
        //     'menu_name' => 'Cheques',
        // ),
        // 'reconciliation' => array(
        //     'controller' => 'reconciliation',
        //     'action' => 'index',
        //     'menu_name' => 'Reconciliation',
        // ),
        // 'transaction' => array(
        //     'controller' => 'transaction',
        //     'action' => 'index',
        //     'menu_name' => 'Transaction',
        // ),
    ),
    'expense_up_down_menu2' => array(
        'payment_voucher' => array(
            'controller' => 'expense-purchase-invoice',
            'action' => 'create',
            'menu_name' => 'Payment Voucher',
        ),
        // 'cheques' => array(
        //     'controller' => 'cheque-deposit',
        //     'action' => 'index',
        //     'menu_name' => 'Cheques',
        // ),
        // 'reconciliation' => array(
        //     'controller' => 'reconciliation',
        //     'action' => 'index',
        //     'menu_name' => 'Reconciliation',
        // ),
        // 'transaction' => array(
        //     'controller' => 'transaction',
        //     'action' => 'index',
        //     'menu_name' => 'Transaction',
        // ),
    ),
    'account_settings_bank_upper_menu' => array(
        'Bank' => array(
            'controller' => 'bank',
            'action' => 'index',
            'menu_name' => 'Bank'
        ),
        'AccountType' => array(
            'controller' => 'account-type',
            'action' => 'index',
            'menu_name' => 'Account Type'
        ),
        'Account' => array(
            'controller' => 'account',
            'action' => 'index',
            'menu_name' => 'Account'
        ),
        'PettyCashExpenseType' => array(
            'controller' => 'petty-cash-expense-type',
            'action' => 'index',
            'menu_name' => 'Petty Cash Expense Type'
        ),
        'CardType' => array(
            'controller' => 'card-type',
            'action' => 'index',
            'menu_name' => 'Card Type'
        ),
    ),
    'payment_voucher-uper-menu' => array(
        'createPI' => array(
            'controller' => 'expense-purchase-invoice',
            'action' => 'create',
            'menu_name' => 'Create PV'
        ),
        'viewPI' => array(
            'controller' => 'expense-purchase-invoice',
            'action' => 'list',
            'menu_name' => 'View PVs'
        ),
    ),
    'cheque_deposit_upper_menu' => array(
        'UndepositedCheques' => array(
            'controller' => 'cheque-deposit',
            'action' => 'index',
            'menu_name' => 'Undeposited Cheques'
        ),
        'Deposits' => array(
            'controller' => 'cheque-deposit',
            'action' => 'deposits',
            'menu_name' => 'Deposits'
        ),
        'BouncedCheques' => array(
            'controller' => 'cheque-deposit',
            'action' => 'bounced-cheques',
            'menu_name' => 'Bounced Cheques'
        ),
        'ChequePrinting' => array(
            'controller' => 'cheque-print',
            'action' => 'index',
            'menu_name' => 'Cheque Printing'
        ),
    ),
    'reconciliation_upper_menu' => array(
        'Transactions' => array(
            'controller' => 'reconciliation',
            'action' => 'index',
            'menu_name' => 'Transactions'
        ),
        'Reconciliation' => array(
            'controller' => 'reconciliation',
            'action' => 'reconciliation',
            'menu_name' => 'Reconciliation'
        )
    ),
    'petty_cash_float_upper_menu' => array(
        'PettyCashFloat' => array(
            'controller' => 'petty-cash-float',
            'action' => 'index',
            'menu_name' => 'Create'
        ),
        'PettyCashFloatView' => array(
            'controller' => 'petty-cash-float',
            'action' => 'view',
            'menu_name' => 'View'
        ),
    ),
    'petty_cash_voucher_upper_menu' => array(
        'pettyCashVoucherCreate' => array(
            'controller' => 'petty-cash-voucher',
            'action' => 'index',
            'menu_name' => 'Create'
        ),
        'pettyCashVoucherView' => array(
            'controller' => 'petty-cash-voucher',
            'action' => 'view',
            'menu_name' => 'View',
        ),
    ),
    'account_settings_up_down_menu' => array(
        'Bank Details' => array(
            'controller' => 'bank',
            'action' => 'index',
            'menu_name' => 'Bank Details',
        ),
        'Accounts Settings' => array(
            'controller' => 'journal-entry-type',
            'action' => 'create',
            'menu_name' => 'Accounts Settings',
        ),
        'Account Classes' => array(
            'controller' => 'account-classes',
            'action' => 'create',
            'menu_name' => 'Account Classes',
        ),
        // 'Groups' => array(
        //     'controller' => 'groups',
        //     'action' => 'create',
        //     'menu_name' => 'Groups',
        // ),
        'Year end closing' => array(
            'controller' => 'year-end-closing',
            'action' => 'index',
            'menu_name' => 'Year End Closing',
        ),
        'Budgeting' => array(
            'controller' => 'budget',
            'action' => 'index',
            'menu_name' => 'Budgeting',
        ),

    ),
    'account_settings_accounts_upper_menu' => array(
        'Journal Entry Type' => array(
            'controller' => 'journal-entry-type',
            'action' => 'create',
            'menu_name' => 'Journal Entry Type'
        ),
        'Fiscal Period' => array(
            'controller' => 'fiscal-period',
            'action' => 'list',
            'menu_name' => 'Fiscal Period'
        ),
        'GL Account Setup' => array(
            'controller' => 'gl-account-setup',
            'action' => 'index',
            'menu_name' => 'GL Account Setup'
        ),
        'Payment Method Setup' => array(
            'controller' => 'gl-account-setup',
            'action' => 'paymentMethod',
            'menu_name' => 'Payment Method Setup'
        ),
    ),
    'expenses_types_upper_menu' => array(
        'Create' => array(
            'controller' => 'expense-type',
            'action' => 'create',
            'menu_name' => 'Create'
        ),
        'View' => array(
            'controller' => 'expense-type',
            'action' => 'list',
            'menu_name' => 'View'
        )
    ),
    'pr_types_upper_menu' => array(
        'Create' => array(
            'controller' => 'purchase-requisition-type',
            'action' => 'create',
            'menu_name' => 'Create'
        ),
        'View' => array(
            'controller' => 'purchase-requisition-type',
            'action' => 'list',
            'menu_name' => 'View'
        )
    ),
    'approval_workflow_upper_menu' => array(
        'Create' => array(
            'controller' => 'approval-workflow',
            'action' => 'create',
            'menu_name' => 'Create'
        ),
        'View' => array(
            'controller' => 'approval-workflow',
            'action' => 'list',
            'menu_name' => 'View'
        )
    ),
    'expenses_reports_upper_menu' => array(
        'Reconciliation Report' => array(
            'controller' => 'reconciliation-report',
            'action' => 'index',
            'menu_name' => 'Reconciliation'
        ),
        'Cheque Report' => array(
            'controller' => 'cheque-report',
            'action' => 'index',
            'menu_name' => 'Cheque'
        ),
        'PettyCashVoucher' => array(
            'controller' => 'pettyCashVoucherReport',
            'action' => 'index',
            'menu_name' => 'Petty Cash',
        ),
        'ExpensePaymentVoucherReport' => array(
            'controller' => 'expensePaymentVoucherReport',
            'action' => 'index',
            'menu_name' => 'Payment Voucher'
        ),
        'Account Report' => array(
            'controller' => 'account-report',
            'action' => 'index',
            'menu_name' => 'Account'
        ),
        'Finance_Report' => array(
            'controller' => 'finance-report',
            'action' => 'index',
            'menu_name' => 'Finance'
        ),
        'Budget_Report' => array(
            'controller' => 'budget-report',
            'action' => 'index',
            'menu_name' => 'Budget'
        ),
        'IncomeExpense_Report' => array(
            'controller' => 'income-expense-report',
            'action' => 'index',
            'menu_name' => 'Income & Expenses'
        ),
    ),
    'expenses_reports_upper_menu1' => array(
        'Reconciliation Report' => array(
            'controller' => 'reconciliation-report',
            'action' => 'index',
            'menu_name' => 'Reconciliation'
        ),
        'Cheque Report' => array(
            'controller' => 'cheque-report',
            'action' => 'index',
            'menu_name' => 'Cheque'
        ),
        'ExpensePaymentVoucherReport' => array(
            'controller' => 'expensePaymentVoucherReport',
            'action' => 'index',
            'menu_name' => 'Payment Voucher'
        ),
        'Account Report' => array(
            'controller' => 'account-report',
            'action' => 'index',
            'menu_name' => 'Account'
        ),
        'Finance_Report' => array(
            'controller' => 'finance-report',
            'action' => 'index',
            'menu_name' => 'Finance'
        ),
    ),
    'expenses_reports_upper_menu2' => array(
        'Reconciliation Report' => array(
            'controller' => 'reconciliation-report',
            'action' => 'index',
            'menu_name' => 'Reconciliation'
        ),
        'Cheque Report' => array(
            'controller' => 'cheque-report',
            'action' => 'index',
            'menu_name' => 'Cheque'
        ),
        'ExpensePaymentVoucherReport' => array(
            'controller' => 'expensePaymentVoucherReport',
            'action' => 'index',
            'menu_name' => 'Payment Voucher'
        ),
        'Account Report' => array(
            'controller' => 'account-report',
            'action' => 'index',
            'menu_name' => 'Account'
        ),
        'Finance_Report' => array(
            'controller' => 'finance-report',
            'action' => 'index',
            'menu_name' => 'Finance'
        ),
    ),
    'expenses_category_upper_menu' => array(
        'Create' => array(
            'controller' => 'expense-category',
            'action' => 'create',
            'menu_name' => 'Create'
        )
    ),
    'pr_category_upper_menu' => array(
        'Create' => array(
            'controller' => 'purchase-requisition-category',
            'action' => 'create',
            'menu_name' => 'Create'
        )
    ),
    'customer_event_upper_menu' => array(
        'Create' => array(
            'controller' => 'customer-event',
            'action' => 'index',
            'menu_name' => 'Create',
        ),
    ),
    'transaction_upper_menu' => array(
        'TransactionCreate' => array(
            'controller' => 'transaction',
            'action' => 'index',
            'menu_name' => 'Create'
        ),
        'TransactionView' => array(
            'controller' => 'transaction',
            'action' => 'view',
            'menu_name' => 'View'
        )
    ),
    'account_classes_upper_menu' => array(
        'AccountClassesCreate' => array(
            'controller' => 'account-classes',
            'action' => 'create',
            'menu_name' => 'Create'
        ),
        'AccountClassesList' => array(
            'controller' => 'account-classes',
            'action' => 'list',
            'menu_name' => 'View Account Classes'
        )
    ),
     'budget_upper_menu' => array(
        'BudgetList' => array(
            'controller' => 'budget',
            'action' => 'list',
            'menu_name' => 'View Budgets'
        ),
        'BudgetCreate' => array(
            'controller' => 'budget',
            'action' => 'create',
            'menu_name' => 'Create Budget'
        ),
        'BudgetSetting' => array(
            'controller' => 'budget',
            'action' => 'setting',
            'menu_name' => 'Budget Setting'
        )
    ),
    'accounts_upper_menu' => array(
        'AccountsCreate' => array(
            'controller' => 'accounts',
            'action' => 'create',
            'menu_name' => 'Create'
        ),
        'AccountsList' => array(
            'controller' => 'accounts',
            'action' => 'list',
            'menu_name' => 'View Accounts'
        )
    ),
    'groups_upper_menu' => array(
        'GroupsCreate' => array(
            'controller' => 'groups',
            'action' => 'create',
            'menu_name' => 'Create'
        ),
        'GroupsList' => array(
            'controller' => 'groups',
            'action' => 'list',
            'menu_name' => 'View Groups'
        )
    ),
    'incomes_upper_menu' => array(
        'IncomeCreate' => array(
            'controller' => 'income',
            'action' => 'create',
            'menu_name' => 'Create Income'
        ),
        'IncomeView' => array(
            'controller' => 'income',
            'action' => 'list',
            'menu_name' => 'View Income'
        )
    ),
    'journal_entries_upper_menu' => array(
        'JournalEntriesCreate' => array(
            'controller' => 'journal-entries',
            'action' => 'create',
            'menu_name' => 'Create'
        ),
        'JournalEntryList' => array(
            'controller' => 'journal-entries',
            'action' => 'list',
            'menu_name' => 'View Journal Entries'
        ),
        'JournalEntryTemplateList' => array(
            'controller' => 'journal-entries',
            'action' => 'journalEntryTemplateList',
            'menu_name' => 'View Journal Entries Template'
        )
    ),
    'jobs_side_menu' => array(
        'job_setup' => array(
            'controller' => 'projectSetup',
            'action' => 'index',
            'menu_name' => 'Jobs Settings',
            'icon' => 'fa fa-cogs'
        ),
        'Project' => array(
            'controller' => 'project',
            'action' => 'list',
            'menu_name' => 'Projects',
            'icon' => 'fa fa-suitcase'
        ),
        'Jobs' => array(
            'controller' => 'jobs',
            'action' => 'list',
            'menu_name' => 'Jobs',
            'icon' => 'fa fa-file-text-o',
        ),
        'Progress' => array(
            'controller' => 'progress',
            'action' => 'list',
            'menu_name' => 'Progress Update',
            'icon' => 'fa fa-area-chart'
        ),
        'Material_requisition' => array(
            'controller' => 'materialRequisition',
            'action' => 'index',
            'menu_name' => 'Material Requisition',
            'icon' => 'glyphicon glyphicon-compressed'
        ),
        'Resource_management' => array(
            'controller' => 'resourceManagement',
            'action' => 'index',
            'menu_name' => 'Resource Management',
            'icon' => 'fa fa-gavel'
        ),
        'Costs' => array(
            'controller' => 'cost',
            'action' => 'index',
            'menu_name' => 'Costs',
            'icon' => 'glyphicon glyphicon-usd'
        ),
        // 'UniversalSearch' => array(
        //     'controller' => 'universalSearch',
        //     'action' => 'search',
        //     'menu_name' => 'Universal Search',
        //     'icon' => 'fa fa-globe'
        // ),
        // 'Reports' => array(
        //     'controller' => 'jobsReport',
        //     'action' => 'index',
        //     'menu_name' => 'Reports',
        //     'icon' => 'fa fa-bar-chart-o'
        // ),
    ),
    'jobs_project_upper_menu' => array(
        'ProjectList' => array(
            'controller' => 'project',
            'action' => 'list',
            'menu_name' => 'Project List',
        ),
        'Project' => array(
            'controller' => 'project',
            'action' => 'index',
            'menu_name' => 'Create Project',
        )
    ),
    'tournament_upper_menu' => array(
        'TournamentList' => array(
            'controller' => 'tournament',
            'action' => 'list',
            'menu_name' => 'Tournament List',
        ),
        'Tournament' => array(
            'controller' => 'tournament',
            'action' => 'index',
            'menu_name' => 'Create Tournament',
        )
    ),
    'tournament_side_menu' => array(
        'tournament_setup' => array(
            'controller' => 'tournamentSetup',
            'action' => 'index',
            'menu_name' => 'Tournament Settings',
            'icon' => 'fa fa-cogs'
        ),
        'Tournament' => array(
            'controller' => 'tournament',
            'action' => 'list',
            'menu_name' => 'Tournaments',
            'icon' => 'fa fa-suitcase'
        ),
        'Events' => array(
            'controller' => 'event',
            'action' => 'list',
            'menu_name' => 'Events',
            'icon' => 'fa fa-file-text-o',
        ),
        'Score_update' => array(
            'controller' => 'scoreUpdate',
            'action' => 'list',
            'menu_name' => 'Score Update',
            'icon' => 'fa fa-area-chart'
        ),
    ),
    'job_setup_down_menu' => array(
        'generalSetting' => array(
            'controller' => 'generalSetup',
            'action' => 'index',
            'menu_name' => 'General Settings'
        ),
        'categorySetting' => array(
            'controller' => 'projectSetup',
            'action' => 'index',
            'menu_name' => 'Category Settings'
        ),
        'EmployeeSetting' => array(
            'controller' => 'designation',
            'action' => 'create',
            'menu_name' => 'Employee Settings',
        ),
        // 'VehicleSetting' => array(
        //     'controller' => 'vehicle',
        //     'action' => 'create',
        //     'menu_name' => 'Vehicles Settings',
        // ),
        'ContractorSetting' => array(
            'controller' => 'contractor',
            'action' => 'create',
            'menu_name' => 'Contractor Settings',
        ),
        'taskSetting' => array(
            'controller' => 'taskSetup',
            'action' => 'index',
            'menu_name' => 'Task Settings'
        ),
    ),
    'tournament_setup_down_menu' => array(
        'generalSetting' => array(
            'controller' => 'tournamentSetup',
            'action' => 'index',
            'menu_name' => 'General Settings'
        ),
        'participants' => array(
            'controller' => 'participantType',
            'action' => 'create',
            'menu_name' => 'Participants',
        ),
    ),
    'category_setting_upper_menu' => array(
        'Project_setup' => array(
            'controller' => 'projectSetup',
            'action' => 'index',
            'menu_name' => 'Project Setup',
        ),
        'Job_setup' => array(
            'controller' => 'jobSetup',
            'action' => 'index',
            'menu_name' => 'Job Setup',
        ), 
        'Cost Type setup' => array(
            'controller' => 'costType',
            'action' => 'index',
            'menu_name' => 'Cost Type',
        ),
    ),
    'general_setting_upper_menu_of_tournament' => array(
        'Project_setup' => array(
            'controller' => 'tournamentSetup',
            'action' => 'index',
            'menu_name' => 'Tournament Setup',
        ),
        'Job_setup' => array(
            'controller' => 'eventSetup',
            'action' => 'index',
            'menu_name' => 'Event Setup',
        ),
    ),
    'participants_upper_menu' => array(
        'Participants_type' => array(
            'controller' => 'participantType',
            'action' => 'create',
            'menu_name' => 'Participants Type',
        ),
        'Participants_sub_type' => array(
            'controller' => 'participantSubType',
            'action' => 'create',
            'menu_name' => 'Participants Sub Type',
        ),
        'Participants' => array(
            'controller' => 'participant',
            'action' => 'create',
            'menu_name' => 'Participants',
        ),
        'Teams' => array(
            'controller' => 'tournamentTeam',
            'action' => 'create',
            'menu_name' => 'Teams',
        ),
    ),
    'jobs_employee_upper_menu' => array(
        'Designations' => array(
            'controller' => 'designation',
            'action' => 'create',
            'menu_name' => 'Designations',
        ),
        'Department' => array(
            'controller' => 'department',
            'action' => 'create',
            'menu_name' => 'Department',
        ),
        'Employees' => array(
            'controller' => 'employees',
            'action' => 'create',
            'menu_name' => 'Employees',
        ),
        'Teams' => array(
            'controller' => 'teams',
            'action' => 'create',
            'menu_name' => 'Teams',
        ),
    ),
    'task_settings_upper_menu' => array(
        'Task' => array(
            'controller' => 'taskSetup',
            'action' => 'index',
            'menu_name' => 'Create Task',
        ),
        'TaskCard' => array(
            'controller' => 'taskCard',
            'action' => 'create',
            'menu_name' => 'Create Task Card',
        ),
        'TaskCardList' => array(
            'controller' => 'taskCard',
            'action' => 'list',
            'menu_name' => 'View Task Cards',
        ),
    ),
    'jobs_job_upper_menu' => array(
        'JobView' => array(
            'controller' => 'jobs',
            'action' => 'list',
            'menu_name' => 'View Job',
        ),
        'JobCreate' => array(
            'controller' => 'jobs',
            'action' => 'create',
            'menu_name' => 'Create Job',
        )
    ),
    'events_upper_menu' => array(
        'EventView' => array(
            'controller' => 'event',
            'action' => 'list',
            'menu_name' => 'View Event',
        ),
        'EventCreate' => array(
            'controller' => 'event',
            'action' => 'create',
            'menu_name' => 'Create Event',
        )
    ),
    'resource_setting_upper_menu' => array(
        'Vehicles_setup' => array(
            'controller' => 'vehicle',
            'action' => 'create',
            'menu_name' => 'Vehicles Setup',
        ),
    ),
    'resource_management_upper_menu' => array(
        'Resource_management' => array(
            'controller' => 'resourceManagement',
            'action' => 'index',
            'menu_name' => 'Resource Management',
        ),
    ),
    'contractor_setting_upper_menu' => array(
        'Contractor_setup' => array(
            'controller' => 'contractor',
            'action' => 'create',
            'menu_name' => 'Contractor Setup',
        ),
    ),
    'general_setting_upper_menu' => array(
        'Basic_setup' => array(
            'controller' => 'generalSetup',
            'action' => 'index',
            'menu_name' => 'Basic Setup',
        ),
    ),
    'cost_upper_menu' => array(
        'Cost' => array(
            'controller' => 'cost',
            'action' => 'index',
            'menu_name' => 'Costs',
        ),
    ),
    'resources_settings_upper_menu' => array(
        'Vehicles_setup' => array(
            'controller' => 'resources',
            'action' => 'index',
            'menu_name' => 'Standard Materials',
        ),
    ),
    'jobs_material_requisition_upper_menu' => array(
        'ApproveMaterial' => array(
            'controller' => 'materialRequisition',
            'action' => 'index',
            'menu_name' => 'Approve Material',
        ),
        'IssueMaterial' => array(
            'controller' => 'materialRequisition',
            'action' => 'materialIssue',
            'menu_name' => 'Issue Material',
        )
    ),

    'jobs_side_menu_of_service' => array(
        'job_setup' => array(
            'controller' => 'departmentStation',
            'action' => 'create',
            'menu_name' => 'Jobs Settings',
            'icon' => 'fa fa-cogs'
        ),
        'Service_Job' => array(
            'controller' => 'serviceJobDashboard',
            'action' => 'index',
            'menu_name' => 'Job',
            'icon' => 'fa fa-file-text-o',
        ),
        'Service_Progress_Update' => array(
            'controller' => 'serviceProgressUpdate',
            'action' => 'index',
            'menu_name' => 'Progress Updates',
            'icon' => 'fa fa-area-chart',
        ),
        'Material_management' => array(
            'controller' => 'materialManagement',
            'action' => 'index',
            'menu_name' => 'Material Management',
            'icon' => 'fa fa-gavel'
        ),
        'Costing' => array(
            'controller' => 'serviceCost',
            'action' => 'index',
            'menu_name' => 'Costing',
            'icon' => 'glyphicon glyphicon-usd'
        ),
        'Vehical_release' => array(
            'controller' => 'vehicleRelease',
            'action' => 'index',
            'menu_name' => 'Vehicle Release',
            'icon' => 'fa fa-road',
        ),
        'Employee_Management' => array(
            'controller' => 'employeeManagement',
            'action' => 'index',
            'menu_name' => 'Employee Management',
            'icon' => 'fa fa-users',
        ),
        'Vehicle_History' => array(
            'controller' => 'vehicleHistory',
            'action' => 'index',
            'menu_name' => 'History',
            'icon' => 'fa fa-sitemap',
        ),
        'Job_Reports' => array(
            'controller' => 'jobReport',
            'action' => 'index',
            'menu_name' => 'Reports',
            'icon' => 'fa fa-sitemap',
        )
    ),
    'job_setup_down_menu_of_service' => array(
        'Vehicles' => array(
            'controller' => 'resources',
            'action' => 'vehicles',
            'menu_name' => 'Vehicle Settings',
        ),
       'departmentStation' => array(
            'controller' => 'departmentStation',
            'action' => 'create',
            'menu_name' => 'Department & Stations'
        ),
        'categorySetting' => array(
            'controller' => 'serviceSetup',
            'action' => 'index',
            'menu_name' => 'Category Settings'
        ),
        'EmployeeSetting' => array(
            'controller' => 'serviceEmployee',
            'action' => 'create',
            'menu_name' => 'Employee Settings',
        ),
        'ResourcesSetting' => array(
            'controller' => 'resources',
            'action' => 'index',
            'menu_name' => 'Resources Settings',
        ),
        'costSetting' => array(
            'controller' => 'incentives',
            'action' => 'index',
            'menu_name' => 'Cost Settings'
        ),
    ),
    'category_setting_upper_menu_of_service' => array(
        'Services_setup' => array(
            'controller' => 'serviceSetup',
            'action' => 'index',
            'menu_name' => 'Services Setup',
        ), 
        'Job_Card_setup' => array(
            'controller' => 'jobCardSetup',
            'action' => 'create',
            'menu_name' => 'Job Card Setup',
        )
    ),
    'jobs_employee_upper_menu_of_service' => array(
        'Service_Employees' => array(
            'controller' => 'serviceEmployee',
            'action' => 'create',
            'menu_name' => 'Service Employee',
        ),

    ),
    'jobs_job_closed_list_upper_menu_of_service' => array(
        'ClosedJob' => array(
            'controller' => 'serviceJobs',
            'action' => 'closedList',
            'menu_name' => 'Closed Jobs',
        ),
        'DeletedJob' => array(
            'controller' => 'serviceJobs',
            'action' => 'deletedList',
            'menu_name' => 'Deleted Jobs',
        )
    ),
);
