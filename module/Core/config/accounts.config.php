<?php

return [
    'default_accounts_settings' => [
        'costing_method' => [
            'FIFOCostingFlag' => 1,
            'averageCostingFlag' => 0
        ]
    ],
    'default_gl_accounts' => [
        'glAccountSetupID' => 1,
        'glAccountSetupItemDefaultSalesAccountID' => 30,
        'glAccountSetupItemDefaultInventoryAccountID' => 8,
        'glAccountSetupItemDefaultCOGSAccountID' => 86,
        'glAccountSetupItemDefaultAdjusmentAccountID' => 87,
        'glAccountSetupSalesAndCustomerReceivableAccountID' => 6,
        'glAccountSetupSalesAndCustomerSalesAccountID' => 30,
        'glAccountSetupSalesAndCustomerSalesDiscountAccountID' => 32,
        'glAccountSetupSalesAndCustomerAdvancePaymentAccountID' => 20,
        'glAccountSetupPurchasingAndSupplierPayableAccountID' => 16,
        'glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID' => 35,
        'glAccountSetupPurchasingAndSupplierGrnClearingAccountID' => 82,
        'glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID' => 9,
        'glAccountSetupGeneralExchangeVarianceAccountID' => 84,
        'glAccountSetupGeneralProfitAndLostYearAccountID' => 29,
        'glAccountSetupGeneralBankChargersAccountID' => 78,
        'glAccountSetupGeneralDeliveryChargersAccountID' => 85,
        'glAccountSetupGeneralLoyaltyExpenseAccountID' => 83
    ],
    'default_customer_gl_accounts' => [
        'customerReceviableAccountID' => 6,
        'customerSalesAccountID' => 30,
        'customerSalesDiscountAccountID' => 32,
        'customerAdvancePaymentAccountID' => 20
    ],
    //payment method finance accounts
    'default_payment_method_finance_accounts' => [
        1 => [
            'paymentMethodInSales' => true,
            'paymentMethodSalesFinanceAccountID' => 4,
            'paymentMethodInPurchase' => true,
            'paymentMethodPurchaseFinanceAccountID' => 4
        ],
        2 => [
            'paymentMethodInSales' => true,
            'paymentMethodSalesFinanceAccountID' => 1,
            'paymentMethodInPurchase' => true,
            'paymentMethodPurchaseFinanceAccountID' => 1
        ],
        3 => [
            'paymentMethodInSales' => true,
            'paymentMethodSalesFinanceAccountID' => 1,
            'paymentMethodInPurchase' => true,
            'paymentMethodPurchaseFinanceAccountID' => 1
        ],
        4 => [
            'paymentMethodInSales' => 0,
            'paymentMethodSalesFinanceAccountID' => 0,
            'paymentMethodInPurchase' => 0,
            'paymentMethodPurchaseFinanceAccountID' => 0
        ],
        5 => [
            'paymentMethodInSales' => 0,
            'paymentMethodSalesFinanceAccountID' => 0,
            'paymentMethodInPurchase' => 0,
            'paymentMethodPurchaseFinanceAccountID' => 0
        ],
        6 => [
            'paymentMethodInSales' => 0,
            'paymentMethodSalesFinanceAccountID' => 0,
            'paymentMethodInPurchase' => 0,
            'paymentMethodPurchaseFinanceAccountID' => 0
        ],
        7 => [
            'paymentMethodInSales' => 0,
            'paymentMethodSalesFinanceAccountID' => 0,
            'paymentMethodInPurchase' => 0,
            'paymentMethodPurchaseFinanceAccountID' => 0
        ],
        8 => [
            'paymentMethodInSales' => 0,
            'paymentMethodSalesFinanceAccountID' => 0,
            'paymentMethodInPurchase' => 0,
            'paymentMethodPurchaseFinanceAccountID' => 0
        ]
    ],
];

