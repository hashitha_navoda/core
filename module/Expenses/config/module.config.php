<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */
return array(
    'router' => array(
        'routes' => array(
            'bank' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/bank[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\Bank',
                        'action' => 'index',
                    ),
                ),
            ),
            'bank-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/bank-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\BankAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'account-type' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\AccountType',
                        'action' => 'index',
                    ),
                ),
            ),
            'account-type-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account-type-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\AccountTypeAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'account' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\Account',
                        'action' => 'index',
                    ),
                ),
            ),
            'account-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/account-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\AccountAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'expense-purchase-invoice' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/expense-purchase-invoice[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\ExpensePurchaseInvoice',
                        'action' => 'index',
                    ),
                ),
            ),
            'cheque-deposit' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cheque-deposit[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\ChequeDeposit',
                        'action' => 'index',
                    ),
                ),
            ),
            'cheque-print' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cheque-print[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\ChequePrint',
                        'action' => 'index',
                    ),
                ),
            ),
            'cheque-deposit-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/cheque-deposit-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\ChequeDepositAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'petty-cash-expense-type' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/petty-cash-expense-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\PettyCashExpenseType',
                        'action' => 'index',
                    ),
                ),
            ),
            'expense-type' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/expense-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\ExpenseType',
                        'action' => 'index',
                    ),
                ),
            ),
            'petty-cash-expense-type-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/petty-cash-expense-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\PettyCashExpenseType',
                        'action' => 'create',
                    ),
                ),
            ),
            'expense-purchase-invoice-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/expense-purchase-invoice-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\ExpensePurchaseInvoiceAPI',
                        'action' => 'add',
                    ),
                ),
            ),
            'expense-type-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/expense-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\ExpenseType',
                        'action' => 'add',
                    ),
                ),
            ),
            'expense-category' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/expense-category[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\ExpenseCategory',
                        'action' => 'index',
                    ),
                ),
            ),
            'petty-cash-float' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/petty-cash-float[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\PettyCashFloat',
                        'action' => 'create',
                    ),
                ),
            ),
            'reconciliation' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reconciliation[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\Reconciliation',
                        'action' => 'index',
                    ),
                ),
            ),
            'petty-cash-float-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/petty-cash-float-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*'
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\PettyCashFloatAPI',
                        'action' => 'index',
                    )
                ),
            ),
            'petty-cash-voucher' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/petty-cash-voucher[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\PettyCashVoucher',
                        'action' => 'index',
                    ),
                ),
            ),
            'expense-category-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/expense-category[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\ExpenseCategory',
                        'action' => 'index',
                    ),
                ),
            ),
            'petty-cash-voucher-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => "/api/petty-cash-voucher[/:action][/:param1][/:param2]",
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\PettyCashVoucher',
                        'action' => 'index',
                    ),
                ),
            ),
            'reconciliation-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/reconciliation-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\ReconciliationAPI',
                        'action' => 'index',
                    ),
                ),
            ),
            'transaction' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transaction[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\Transaction',
                        'action' => 'index',
                    ),
                ),
            ),
            'transaction-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/transaction-api[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\Transaction',
                        'action' => 'index',
                    ),
                ),
            ),
            'card-type' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/card-type[/:action][/:param1]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\CardType',
                        'action' => 'index',
                    ),
                ),
            ),
            'card-type-api' => array(
                'type' => 'segment',
                'options' => array(
                    'route' => '/api/card-type[/:action][/:param1][/:param2]',
                    'constraints' => array(
                        'action' => '[a-zA-Z][a-zA-Z0-9_-]*',
                    ),
                    'defaults' => array(
                        'controller' => 'Expenses\Controller\API\CardType',
                        'action' => 'save',
                    ),
                ),
            ),
        // The following is a route to simplify getting started creating
        // new controllers and actions without needing to create a new
        // module. Simply drop new controllers in, and you can access them
        // using the path /application/:controller/:action
        ),
    ),
    'translator' => array(
        'translation_file_patterns' => array(
            array(
                'type' => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern' => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Expenses\Controller\Bank' => 'Expenses\Controller\BankController',
            'Expenses\Controller\AccountType' => 'Expenses\Controller\AccountTypeController',
            'Expenses\Controller\Account' => 'Expenses\Controller\AccountController',
            'Expenses\Controller\API\BankAPI' => 'Expenses\Controller\API\BankAPIController',
            'Expenses\Controller\API\AccountTypeAPI' => 'Expenses\Controller\API\AccountTypeAPIController',
            'Expenses\Controller\API\AccountAPI' => 'Expenses\Controller\API\AccountAPIController',
            'Expenses\Controller\ExpensePurchaseInvoice' => 'Expenses\Controller\ExpensePurchaseInvoiceController',
            'Expenses\Controller\API\ExpensePurchaseInvoiceAPI' => 'Expenses\Controller\API\ExpensePurchaseInvoiceAPIController',
            'Expenses\Controller\ChequeDeposit' => 'Expenses\Controller\ChequeDepositController',
            'Expenses\Controller\ChequePrint' => 'Expenses\Controller\ChequePrintController',
            'Expenses\Controller\API\ChequeDepositAPI' => 'Expenses\Controller\API\ChequeDepositAPIController',
            'Expenses\Controller\PettyCashExpenseType' => 'Expenses\Controller\PettyCashExpenseTypeController',
            'Expenses\Controller\API\PettyCashExpenseType' => 'Expenses\Controller\API\PettyCashExpenseTypeController',
            'Expenses\Controller\PettyCashFloat' => 'Expenses\Controller\PettyCashFloatController',
            'Expenses\Controller\API\PettyCashFloatAPI' => 'Expenses\Controller\API\PettyCashFloatAPIController',
            'Expenses\Controller\PettyCashVoucher' => 'Expenses\Controller\PettyCashVoucherController',
            'Expenses\Controller\API\PettyCashVoucher' => 'Expenses\Controller\API\PettyCashVoucherController',
            'Expenses\Controller\Reconciliation' => 'Expenses\Controller\ReconciliationController',
            'Expenses\Controller\API\ReconciliationAPI' => 'Expenses\Controller\API\ReconciliationAPIController',
            'Expenses\Controller\ExpenseType' => 'Expenses\Controller\ExpenseTypeController',
            'Expenses\Controller\API\ExpenseType' => 'Expenses\Controller\API\ExpenseTypeController',
            'Expenses\Controller\ExpenseCategory' => 'Expenses\Controller\ExpenseCategoryController',
            'Expenses\Controller\API\ExpenseCategory' => 'Expenses\Controller\API\ExpenseCategoryController',
            'Expenses\Controller\Transaction' => 'Expenses\Controller\TransactionController',
            'Expenses\Controller\API\Transaction' => 'Expenses\Controller\API\TransactionController',
            'Expenses\Controller\CardType' => 'Expenses\Controller\CardTypeController',
            'Expenses\Controller\API\CardType' => 'Expenses\Controller\API\CardTypeController',
        ),
    ),
    'view_manager' => array(
        'template_path_stack' => array(
            'expenses' => __DIR__ . '/../view',
        ),
        'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
