<?php

namespace Expenses\Form;

use Zend\Form\Form;

/**
 * Description of PettyCashVoucherForm
 *
 * @author shermilan <sharmilan@thinkcune.com>
 */
class PettyCashVoucherForm extends Form {

    public function __construct($options = []) {
        $users = ($options['users']) ? $options['users'] : [];
        $pettyCashExpenseTypes = ($options['pettyCashExpenseTypes']) ? $options['pettyCashExpenseTypes'] : [];
        $outgoingPettyCashVouchers = ($options['outgoingPettyCashVoucherList']) ? $options['outgoingPettyCashVoucherList'] : [];
        $pettyCashVoucherTypes = ($options['pettyCashVoucherTypes']) ? $options['pettyCashVoucherTypes'] : [];
        $pettyCashVoucherNo = ($options['pettyCashVoucherNo']) ? $options['pettyCashVoucherNo'] : '';

        parent::__construct($name = null, $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('name', 'create-petty-cash-voucher');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'pettyCashVoucherID',
            'type' => 'hidden',
            'attributes' => array(
                'id' => 'pettyCashVoucherID',
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashFloatBalance',
            'type' => 'text',
            'attributes' => array(
                'id' => 'pettyCashFloatBalance',
                'class' => 'form-control',
                'readonly' => true,
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashVoucherAmount',
            'type' => 'text',
            'attributes' => array(
                'id' => 'pettyCashVoucherAmount',
                'class' => 'form-control',
                'placeholder' => _('1000'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashVoucherComment',
            'type' => 'textarea',
            'attributes' => array(
                'id' => 'pettyCashVoucherComment',
                'class' => 'form-control',
                'placeholder' => _('comment'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashVoucherNo',
            'type' => 'text',
            'attributes' => array(
                'id' => 'pettyCashVoucherNo',
                'class' => 'form-control',
                'readonly' => TRUE,
                'value' => $pettyCashVoucherNo,
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashVoucherDate',
            'type' => 'text',
            'attributes' => array(
                'id' => 'pettyCashVoucherDate',
                'class' => 'form-control curser-pointer',
                'style' => 'background-color: white',
                'readonly' => TRUE,
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashVoucherType',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'pettyCashVoucherType',
                'class' => 'form-control selectpicker',
            ),
            'options' => array(
                'value_options' => $pettyCashVoucherTypes,
            ),
        ));

        $this->add(array(
            'name' => 'userID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'userID',
                'class' => 'form-control selectpicker'
            ),
            'options' => array(
                'empty_option' => ' ',
                'value_options' => $users
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeID',
                'class' => 'form-control selectpicker'
            ),
            'options' => array(
                'value_options' => $pettyCashExpenseTypes
            ),
        ));

        $this->add(array(
            'name' => 'outgoingPettyCashVoucherID',
            'type' => 'select',
            'attributes' => array(
                'id' => 'outgoingPettyCashVoucherID',
                'class' => 'form-control selectpicker'
            ),
            'options' => array(
                'value_options' => $outgoingPettyCashVouchers
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashVoucherFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'pettyCashVoucherFinanceAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'pettyCashVoucherSubmit',
            'type' => 'submit',
            'attributes' => array(
                'id' => 'pettyCashVoucherSubmit',
                'class' => 'btn btn-primary make-full-width',
                'value' => _('Save'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashVoucherReset',
            'type' => 'submit',
            'attributes' => array(
                'id' => 'pettyCashVoucherReset',
                'class' => 'btn btn-default make-full-width',
                'value' => _('Reset'),
            ),
        ));
    }

}
