<?php

namespace Expenses\Form;

use Zend\Form\Form;

/**
 * class AccountForm
 * @author madawa chandrarathne <madawa@thinkcube.com>
 * create account form
 */
class AccountForm extends Form {

    public function __construct($name, $banks, $accountTypes, $currencyList) {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'accountId',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'accountId'
            ),
        ));

        $this->add(array(
            'name' => 'accountName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'accountName',
                'class' => 'form-control',
                'placeholder' => 'Eg: JohnDoe',
            )
        ));

        $this->add(array(
            'name' => 'accountNumber',
            'attributes' => array(
                'type' => 'text',
                'id' => 'accountNumber',
                'class' => 'form-control',
                'placeholder' => 'Eg: xxx-xxx-xxx',
            )
        ));

        $this->add(array(
            'name' => 'accountStatus',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'accountStatus',
                'class' => 'form-control'
            ),
            'options' => array(
                'value_options' => array(
                    '1' => 'unlock',
                    '0' => 'lock'
                ),
            )
        ));

        $this->add(array(
            'name' => 'bankId',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'bankId',
                'class' => 'form-control'
            ),
            'options' => array(
                'empty_option' => 'select a bank',
                'value_options' => isset($banks) ? $banks : NULL
            )
        ));

        $this->add(array(
            'name' => 'bankBranchId',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'bankBranchId',
                'class' => 'form-control'
            ),
            'options' => array(
                'empty_option' => 'select branch',
                'value_options' => NULL
            )
        ));

        $this->add(array(
            'name' => 'accountTypeId',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'accountTypeId',
                'class' => 'form-control'
            ),
            'options' => array(
                'empty_option' => 'select an account type',
                'value_options' => isset($accountTypes) ? $accountTypes : NULL
            )
        ));

        $this->add(array(
            'name' => 'currencyId',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'currencyId',
                'class' => 'form-control'
            ),
            'options' => array(
                'empty_option' => 'select account currency',
                'value_options' => isset($currencyList) ? $currencyList : NULL
            )
        ));

        $this->add(array(
            'name' => 'accountBalance',
            'attributes' => array(
                'type' => 'text',
                'id' => 'accountBalance',
                'class' => 'form-control',
                'placeholder' => 'Eg: xxxx',
            )
        ));

        $this->add(array(
            'name' => 'financeAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'financeAccountID',
                'class' => 'form-control',
                'data-liver-serch' => true,
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));

        $this->add(array(
            'name' => 'entityId',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'entityId',
                'class' => '',
            ),
        ));

        $this->add(array(
            'type' => 'Zend\Form\Element\Checkbox',
            'name' => 'isCardPaymentAccount',
            'attributes' => array(
                'id' => 'isCardPaymentAccount',
            )
        ));
    }

}
