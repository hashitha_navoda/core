<?php

namespace Expenses\Form;

use Zend\Form\Form;

/**
 * Description of ExpenseTypeForm
 *
 * @author shermilan
 */
class PettyCashExpenseTypeForm extends Form {

    public function __construct($name = null, $options = array()) {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'POST');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute('role', "form");


        $this->add(array(
            'name' => 'pettyCashExpenseTypeID',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeID',
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeName',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeName',
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('Salary'),
                'autofocus' => true,
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeDescription',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeDescription',
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('Employee\'s Salary'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeMinAmount',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeMinAmount',
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('100'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeMaxAmount',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeMaxAmount',
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('10000'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeSave',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeSave',
                'type' => 'submit',
                'class' => 'btn btn-primary make-full-width',
                'value' => _('Save'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeCancel',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeCancel',
                'type' => 'reset',
                'class' => 'btn btn-default make-full-width',
                'value' => _('Cancel'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeUpdate',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeUpdate',
                'type' => 'submit',
                'class' => 'btn btn-primary make-full-width',
                'value' => _('Update')
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashExpenseTypeReset',
            'attributes' => array(
                'id' => 'pettyCashExpenseTypeReset',
                'type' => 'reset',
                'class' => 'btn btn-default make-full-width',
                'value' => _('Reset'),
            ),
        ));
    }

}
