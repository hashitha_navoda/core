<?php

namespace Expenses\Form;

use Zend\Form\Form;

/**
 * class BankBranchForm
 * @author madawa chandrarathne <madawa@thinkcube.com>
 * create bank branch form
 */
class BankBranchForm extends Form {

    public function __construct($name) {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'bankBranchId',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'bankBranchId'
            ),
        ));

        $this->add(array(
            'name' => 'bankId',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'bankId'
            ),
        ));

        $this->add(array(
            'name' => 'bankBranchName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'bankBranchName',
                'class' => 'form-control',
                'placeholder' => 'Eg: Common Branch',
            )
        ));

        $this->add(array(
            'name' => 'bankBranchAddress',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'bankBranchAddress',
                'class' => 'form-control',
                'placeholder' => 'Eg: No xx, Main street, Nowhere.',
            )
        ));

        $this->add(array(
            'name' => 'bankBranchCode',
            'attributes' => array(
                'type' => 'text',
                'id' => 'bankBranchCode',
                'class' => 'form-control',
                'placeholder' => 'Eg: Common Bank',
            )
        ));
    }

}
