<?php

namespace Expenses\Form;

use Zend\Form\Form;

/**
 * Description of PettyCashFloatForm
 *
 * @author shermilan
 */
class PettyCashFloatForm extends Form {

    public function __construct($options = array()) {
        parent::__construct($$options);
        $accounts = $options['accounts'];
        $this->setAttribute('class', 'form-horizontal');
        $this->setAttribute('role', 'form');
        $this->setAttribute('method', 'POST');

        $this->add(array(
            'name' => 'pettyCashFloatID',
            'attributes' => array(
                'id' => 'pettyCashFloatID',
                'type' => 'hidden',
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashFloatAmount',
            'attributes' => array(
                'id' => 'pettyCashFloatAmount',
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('1000'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashFloatDate',
            'attributes' => array(
                'id' => 'pettyCashFloatDate',
                'type' => 'text',
                'class' => 'datepicker form-control pointer_cursor',
                'readonly' => TRUE,
                'style' => 'background-color: white',
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashFloatBalance',
            'attributes' => array(
                'id' => 'pettyCashFloatBalance',
                'type' => 'text',
                'readonly' => TRUE,
                'class' => 'form-control',
                'placeholder' => _('1000')
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashFloatFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'pettyCashFloatFinanceAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'pettyCashFloatIssueFinanceAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'pettyCashFloatIssueFinanceAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'Select an Account',
            )
        ));

        $this->add(array(
            'name' => 'pettyCashFloatIssueBalance',
            'attributes' => array(
                'id' => 'pettyCashFloatIssueBalance',
                'type' => 'text',
                'readonly' => TRUE,
                'class' => 'form-control',
                'placeholder' => _('1000')
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashFloatSave',
            'attributes' => array(
                'id' => 'pettyCashFloatSave',
                'type' => 'submit',
                'class' => 'btn btn-primary make-full-width',
                'value' => _('Save'),
            ),
        ));

        $this->add(array(
            'name' => 'pettyCashFloatCancel',
            'attributes' => array(
                'id' => 'pettyCashFloatCancel',
                'type' => 'reset',
                'class' => 'btn btn-default make-full-width',
                'value' => _('Cancel'),
            ),
        ));
    }

}
