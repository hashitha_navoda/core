<?php

namespace Expenses\Form;

use Zend\Form\Form;

/**
 * class TransactionForm
 * @author madawa chandrarathne <madawa@thinkcube.com>
 * create transaction form
 */
class TransactionForm extends Form {

    public function __construct($name, $accountList) {
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        $this->add(array(
            'name' => 'transaction',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'transaction',
                'class' => 'form-control'
            ),
            'options' => array(
                'value_options' => array(
                    'withdrawal' => 'Withdrawal',
                    'deposit' => 'Deposit',
                    'transfer' => 'Transfer'
                ),
            )
        ));

        $this->add(array(
            'name' => 'transactionBankAccountId',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'transactionBankAccountId',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => _('Select an Account'),
                'value_options' => isset($accountList) ? $accountList : NULL,
            )
        ));

        $this->add(array(
            'name' => 'transactionIssuedFinanceAccountId',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'transactionIssuedFinanceAccountId',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => _('Select an Account'),
            )
        ));

        $this->add(array(
            'name' => 'transactionRecivedFinanceAccountId',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'transactionRecivedFinanceAccountId',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => _('Select an Account'),
            )
        ));
        
        $this->add(array(
            'name' => 'transactionAmount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transactionAmount',
                'class' => 'form-control',
                'placeholder' => 'Eg: xxxx',
            )
        ));
        
        $this->add(array(
            'name' => 'transactionDate',
            'attributes' => array(
                'type' => 'text',
                'id' => 'transactionDate',
                'class' => 'form-control',
                'placeholder' => _('Eg: Date'),
                'readonly' => true,
            )
        ));
        
        $this->add(array(
            'name' => 'transactionComment',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'transactionComment',
                'class' => 'form-control',
            )
        ));       
        
    }

}
