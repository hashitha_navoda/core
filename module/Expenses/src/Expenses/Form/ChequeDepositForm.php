<?php

namespace Expenses\Form;

use Zend\Form\Form;

/**
 * class ChequeDepositForm
 * @author madawa chandrarathne <madawa@thinkcube.com>
 * Cheque Deposit form
 */
class ChequeDepositForm extends Form{
    
    public function __construct( $name, $banks){
        parent::__construct( $name);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');
        
        $this->add(array(
            'name' => 'chequeDepositId',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'chequeDepositId'
            ),
        ));
        
        $this->add(array(
            'name' => 'chequeDepositDate',
            'attributes' => array(
                'type' => 'text',
                'id' => 'chequeDepositDate',
                'class' => 'form-control datepicker pointer_cursor',
            )
        ));
        
        $this->add(array(
            'name' => 'bankId',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'bankId',
                'class' => 'form-control'
            ),
            'options' => array(
                'empty_option' => _('select a bank'),
                'value_options' => isset($banks) ? $banks : NULL
            )
        ));
        
        $this->add(array(
            'name' => 'accountId',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'accountId',
                'class' => 'form-control'
            ),
            'options' => array(
                'empty_option' => _('select account'),
                'value_options' => NULL
            )
        ));
        
        $this->add(array(
            'name' => 'chequeDepositAmount',
            'attributes' => array(
                'type' => 'text',
                'id' => 'chequeDepositAmount',
                'class' => 'form-control',
                'disabled' => 'disabled'
            )
        ));
        
        $this->add(array(
            'name' => 'chequeDepositDescription',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'chequeDepositDescription',
                'class' => 'form-control',
            )
        ));
    }
}
