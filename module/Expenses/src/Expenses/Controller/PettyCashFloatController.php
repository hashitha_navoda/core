<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Form\PettyCashFloatForm;

/**
 * Description of PettyCashFloat
 *
 * @author shermilan <sharmilan@thinkcube.com>
 */
class PettyCashFloatController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'petty_cash_float_upper_menu';
    protected $downMenus = 'expense_up_down_menu';
    public $paginator;

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Expenses', 'Create', 'Accounting', 'Petty Cash Float');

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $companyCurrencySymbol = $this->user_session->companyCurrencySymbol;

        $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($locationID)->current();

        $pettyCashFloatForm = new PettyCashFloatForm();

        $PFDAData = $this->CommonTable('Expenses\Model\PettyCashFloatDefaultAccountsTable')->fetchAll();

        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";


        if(count($PFDAData > 0)){
            $data = $PFDAData->current();            
            $financeAccountsArray = $this->getFinanceAccounts();

            if($data->pettyCashFloatFinanceAccountID != ''){
                $pettyCashFloatFinanceAccountID = $data->pettyCashFloatFinanceAccountID;
                $pettyCashFloatFinanceAccountName = $financeAccountsArray[$pettyCashFloatFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$pettyCashFloatFinanceAccountID]['financeAccountsName'];
                $pettyCashFloatBalance = $financeAccountsArray[$pettyCashFloatFinanceAccountID]['financeAccountsDebitAmount'] - $financeAccountsArray[$pettyCashFloatFinanceAccountID]['financeAccountsCreditAmount'];
                $pettyCashFloatForm->get('pettyCashFloatFinanceAccountID')->setAttribute('data-id',$pettyCashFloatFinanceAccountID);
                $pettyCashFloatForm->get('pettyCashFloatFinanceAccountID')->setAttribute('data-value',$pettyCashFloatFinanceAccountName);
                $pettyCashFloatForm->get('pettyCashFloatBalance')->setAttribute('data-value',$pettyCashFloatBalance);
            }

            if($data->pettyCashFloatIssueFinanceAccountID != ''){
                $pettyCashFloatIssueFinanceAccountID = $data->pettyCashFloatIssueFinanceAccountID;
                $pettyCashFloatIssueFinanceAccountName = $financeAccountsArray[$pettyCashFloatIssueFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$pettyCashFloatIssueFinanceAccountID]['financeAccountsName'];
                $pettyCashFloatIssueBalance = $financeAccountsArray[$pettyCashFloatIssueFinanceAccountID]['financeAccountsDebitAmount'] - $financeAccountsArray[$pettyCashFloatIssueFinanceAccountID]['financeAccountsCreditAmount'];
                $pettyCashFloatForm->get('pettyCashFloatIssueFinanceAccountID')->setAttribute('data-id',$pettyCashFloatIssueFinanceAccountID);
                $pettyCashFloatForm->get('pettyCashFloatIssueFinanceAccountID')->setAttribute('data-value',$pettyCashFloatIssueFinanceAccountName);
                $pettyCashFloatForm->get('pettyCashFloatIssueBalance')->setAttribute('data-value',$pettyCashFloatIssueBalance);
            }
        }

        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/petty-cash-float.js');
        $PCFView = new ViewModel(['pettyCashFloatForm' => $pettyCashFloatForm, 'companyCurrencySymbol' => $companyCurrencySymbol]);

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $PCFView->addChild($dimensionAddView, 'dimensionAddView');

        return $PCFView;
    }

    public function ViewAction()
    {
        $this->getSideAndUpperMenus('Expenses', 'View', 'Accounting', 'Petty Cash Float');
        $this->getPaginatedPettyCashFloats();
//      Petty Cash Float prefix
        $config = $this->getServiceLocator()->get('config');
        $PettyCashFloatCodePrefix = $config['PettyCashFloatCodePrefix'];
        $cashTypes = $this->getServiceLocator()->get('config')['cashType'];
        $companyCurrencySymbol = $this->user_session->companyCurrencySymbol;

        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/petty-cash-float-view.js');
        return new ViewModel(array(
            'pettyCashFloats' => $this->paginator,
            'PettyCashFloatCodePrefix' => $PettyCashFloatCodePrefix,
            'cashTypes' => $cashTypes,
            'paginated' => true,
            'companyCurrencySymbol' => $companyCurrencySymbol,
        ));
    }

    public function getPaginatedPettyCashFloats($pageCount = 6)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->fetchAll($locationID, true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($pageCount);
    }

}
