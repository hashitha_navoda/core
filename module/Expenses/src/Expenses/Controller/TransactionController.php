<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Form\TransactionForm;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;
use Zend\Session\Container;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Account Transaction related actions
 */
class TransactionController extends CoreController
{

    protected $sideMenus  = 'accounting_side_menu';
    protected $upperMenus = 'transaction_upper_menu';
    protected $downMenus  = 'cash_bank_up_down_menu';

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->downMenus = 'expense_up_down_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus( 'Cash & Bank', 'Create', 'Accounting', 'Transaction');
        //get location id
        $locationId = $this->user_session->userActiveLocation['locationID'];
        $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($locationId)->current();
                
        //get existing Accounts
        $accountSData = $this->CommonTable('Expenses\Model\AccountTable')->getActiveAccounts();
        
        $accountList = array();
        foreach ($accountSData as $key => $value) {
            $accountList[$value['accountId']] = $value['accountName']." - ".$value['accountNumber'];
        }

        //create account form
        $form = new TransactionForm('createTransaction', $accountList);
                
        return new ViewModel(array('form'=>$form));
    }    
    
    public function viewAction()
    {
        $this->getSideAndUpperMenus( 'Cash & Bank', 'View', 'Accounting', 'Transaction');
        
        $page = $this->params('param1',1);
        $locationId = $this->user_session->userActiveLocation['locationID'];
        $dateFormat = $this->getUserDateFormat();
        
        //get account withdrawals
        $withdrawals = array();
        $withdrawals = $this->CommonTable('Expenses\Model\AccountWithdrawalTable')->getWithdrawalsByAccountId();
        $withdrawals = array_map(function($transaction) {
            $transaction['id'] = $transaction['accountWithdrawalId'];
            $transaction['reference'] = "WDL".$transaction['accountWithdrawalId'];
            $transaction['issuedAccount'] = $transaction['issuedFinanceAccountsCode'].'_'.$transaction['issuedFinanceAccountsName'];
            $transaction['recivedAccount'] = $transaction['recivedFinanceAccountsCode'].'_'.$transaction['recivedFinanceAccountsName'];
            $transaction['date'] = $transaction['accountWithdrawalDate'];
            $transaction['type'] = 'withdrawal';
            $transaction['amount'] = $transaction['accountWithdrawalAmount'];
            
            return $transaction;
        }, iterator_to_array($withdrawals));
        
        //get account deposit
        $deposits = array();
        $deposits = $this->CommonTable('Expenses\Model\AccountDepositTable')->getDepositsByAccountId();
        $deposits = array_map(function($transaction) {
            $transaction['id'] = $transaction['accountDepositId'];
            $transaction['reference'] = "DEP".$transaction['accountDepositId'];
            $transaction['bankAccount'] = $transaction['accountName'].' ('.$transaction['accountNumber'].')';
            $transaction['issuedAccount'] = $transaction['issuedFinanceAccountsCode'].'_'.$transaction['issuedFinanceAccountsName'];
            $transaction['date'] = $transaction['accountDepositDate'];
            $transaction['type'] = 'deposit';
            $transaction['amount'] = $transaction['accountDepositAmount'];
            return $transaction;
        }, iterator_to_array($deposits));
        
        //get account incoming account transfer
        $transfer = array();
        $transfer = $this->CommonTable('Expenses\Model\AccountTransferTable')->getAccountTransfersByAccountId();
        $transfer = array_map(function($transaction) {
            $transaction['id'] = $transaction['accountTransferId'];
            $transaction['reference'] = "TRF".$transaction['accountTransferId'];
            $transaction['issuedAccount'] = $transaction['issuedFinanceAccountsCode'].'_'.$transaction['issuedFinanceAccountsName'];
            $transaction['recivedAccount'] = $transaction['recivedFinanceAccountsCode'].'_'.$transaction['recivedFinanceAccountsName'];
            $transaction['type'] = 'transfer';
            $transaction['date'] = $transaction['accountTransferDate'];
            $transaction['amount'] = $transaction['accountTransferAmount'];
            return $transaction;
        }, iterator_to_array($transfer));      
        
        $transactions = array_merge($withdrawals, $deposits, $transfer);
             
        $items     = new ArrayAdapter($transactions);
        $paginator = new Paginator($items);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(8);
        
        return new ViewModel( array('transactions' => $paginator, 'userdateFormat'=>$dateFormat, 'paginated' => true));
    }
}
