<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Form\PettyCashExpenseTypeForm;

/**
 * This file contains Expense Type Detils
 * @author sharmilan <sharmilan@thinkcube.com>
 */
class PettyCashExpenseTypeController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'account_settings_bank_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';
    protected $paginator;

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Petty Cash Expense Type', 'Accounting', 'Bank Details');

        $pettyCashExpenseTypeForm = new PettyCashExpenseTypeForm();

        $this->getpaginater();

//      For display toggle purpose of add and edit view
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/petty-cash-expense-type.js');
        return new ViewModel(array(
            'pettyCashExpenseTypeForm' => $pettyCashExpenseTypeForm,
            'pettyCashExpenseTypes' => $this->paginator,
            'paginated' => true,
        ));
    }

    public function getpaginater($size = 6)
    {
        $this->paginator = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($size);
    }

}
