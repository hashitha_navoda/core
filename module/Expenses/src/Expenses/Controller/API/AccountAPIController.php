<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Expenses\Form\AccountForm;
use Expenses\Model\Account;
use Expenses\Model\CardType;
use Zend\View\Model\ViewModel;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Account API related actions
 */
class AccountAPIController extends CoreController
{

    /**
     * Create Account
     * @return JsonModel
     */
    public function createAccountAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {

            $postData = $this->getRequest()->getPost()->toArray();
            
            if ($postData['financeAccountID'] == '0') {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVALID_ACCOUNT');
                return $this->JSONRespond();
            }
            //get existing banks
            $bankList = $this->CommonTable('Expenses\Model\BankTable')->getBankListForDropDown();

            //get banks account types
            $accountTypeList = $this->CommonTable('Expenses\Model\AccountTypeTable')->getAccountTypeListForDropDown();

            //get currency list for dropdown
            $currencyList = $this->CommonTable('Core\Model\CurrencyTable')->getActiveCurrencyListForDropDown();

            //create account form
            $form = new AccountForm('createAccount', $bankList, $accountTypeList, $currencyList);

            $account = new Account();
            $form->setInputFilter($account->getInputFilter());
            $form->setData($postData);

            // validate card types array

            if ($postData['cardTypes'] != NULL) {
                $cardTypeIDs = array_filter($postData['cardTypes'], 'ctype_digit');
            }
            //this part use to find exists user account numbers.
            $accountExists = $this->CommonTable('Expenses\Model\AccountTable')->checkAccountNumberIsExists($postData['accountNumber'], $postData['bankId']);
            if (count($accountExists) > 0) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_ACCOUNT_NO_EXISTS');
            } else if ($form->isValid()) {
                $postData['entityId'] = $this->createEntity();
                $account->exchangeArray($postData);
                $result = $this->CommonTable('Expenses\Model\AccountTable')->saveAccount($account);

                // Add to card types
                $isAdded = $this->CommonTable('Expenses\Model\CardTypeTable')->assignAccount($result, $cardTypeIDs);

                if ($result) {
                    $this->setLogMessage('Bank account '.$postData['accountName'].' is created.');
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_ACCOUNT_CREATE');
                    $this->data = $result;
                } else {
                    $this->msg = $this->getMessage('ERR_ACCOUNT_CREATE');
                }
            } else {
                $this->msg = $this->getMessage('ERR_ACCOUNT_VALIDATION');
            }
        }
        return $this->JSONRespond();
    }

    /**
     * Update Account
     * @return JsonModel
     */
    public function updateAccountAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $newData = $postData;

        $bankList = $this->CommonTable('Expenses\Model\BankTable')->getBankListForDropDown();

        $accountTypeList = $this->CommonTable('Expenses\Model\AccountTypeTable')->getAccountTypeListForDropDown();

        $currencyList = $this->CommonTable('Core\Model\CurrencyTable')->getActiveCurrencyListForDropDown();

        $form = new AccountForm('UpdateAccount', $bankList, $accountTypeList, $currencyList);

        $account = new Account();
        $form->setInputFilter($account->getInputFilter());
        $form->setData($postData);
        if (!$form->isValid()) {
            $this->msg = $this->getMessage('ERR_ACCOUNT_VALIDATION');
            return $this->JSONRespond();
        }
        //this part use to find exists user account numbers.
        $accountExists = $this->CommonTable('Expenses\Model\AccountTable')->checkAccountNumberIsExists($postData['accountNumber'], $postData['bankId'], $postData['accountId']);
        if (count($accountExists) > 0) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_ACCOUNT_NO_EXISTS');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        try {

            $oldData = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($postData['accountId']);
            $formData = $form->getData();

            // validate card types array
            if ($postData['cardTypes'] != null) {
                $cardTypeIDs = array_filter($postData['cardTypes'], 'ctype_digit');
            }

            unset($postData['accountBalance']);
            unset($postData['currencyId']);
            unset($postData['cardTypes']);

            $this->CommonTable('Expenses\Model\AccountTable')->updateAccount($postData, $postData['accountId']);
            $this->updateEntity($postData['entityId']);

            // clear card type of this account
            $this->CommonTable('Expenses\Model\CardTypeTable')->clearAccount($formData['accountId']);

            // Add to card types
            $this->CommonTable('Expenses\Model\CardTypeTable')->assignAccount($formData['accountId'], $cardTypeIDs);

            $eventParameter = ['glAccountID' => $formData['financeAccountID']];
            $this->getEventManager()->trigger('bankBalanceUpdated', $this, $eventParameter);
            $changeArray = $this->getBankAccountChageDetails($oldData, $newData);
            //set log details
            $previousData = json_encode($changeArray['previousData']);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("Bank Account ".$oldData['accountName']." is updated.");
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_ACCOUNT_UPDATE');
        } catch (\Exception $e) {
            $this->rollback();
            $this->msg = $this->getMessage('ERR_ACCOUNT_UPDATE_FAIL');
        }

        return $this->JSONRespond();
    }

    public function getBankAccountChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Account name'] = $row['accountName'];
        $previousData['Account number'] = $row['accountNumber'];
        $previousData['Account status'] = ($row['accountStatus'] == 1) ? 'Unlock' : 'Lock';

        $newData = [];
        $newData['Account name'] = $data['accountName'];
        $newData['Account number'] = $data['accountNumber'];
        $newData['Account status'] =  ($data['accountStatus'] == 1) ? 'Unlock' : 'Lock';
        return array('previousData' => $previousData, 'newData' => $newData);
    }


    /**
     * Delete account
     * @return JsonModel
     */
    public function deleteAccountAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {

            $accountId = $this->getRequest()->getPost('accountId');

            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
            if ($account) {
                $this->beginTransaction();
                //this use to check that given accountID has any transactions.
                $checkLinkedPayments = $this->checkLinkedPaymentDetails($accountId);
                //this use to remove car types when account delete
                $removeCardType = $this->removeAccountFromCardType($account);
                if (!$checkLinkedPayments) {
                    $this->msg = $this->getMessage('ERR_ACCOUNT_LINK_WITH_PAYMENTS');
                } else if ($removeCardType) {
                    if ($this->updateDeleteInfoEntity($account['entityId'])) {
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_ACCOUNT_DELETE');
                        $this->setLogMessage('Bank Account '.$account['accountName'].' is deleted.');
                        $this->commit();
                    } else {
                        $this->msg = $this->getMessage('ERR_ACCOUNT_DELETE_FAILED');
                        $this->rollback();
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_ACCOUNT_DELETE_FAILED');
                    $this->rollback();
                }
            } else {
                $this->msg = $this->getMessage('ERR_ACCOUNT_NOT_FOUND');
            }
        }

        return $this->JSONRespond();
    }

    /**
     * Get Account
     * @return JsonModel
     */
    public function getAccountAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {

            $accountId = $this->getRequest()->getPost('accountId');

            $data['accountDetails'] = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
            $financeAccountID = $data['accountDetails']['financeAccountID'];
            $financeAccountsArray = $this->getFinanceAccounts();
            $data['accountDetails']['financeAccountName'] = $financeAccountsArray[$financeAccountID]['financeAccountsCode']."_".$financeAccountsArray[$financeAccountID]['financeAccountsName'];
            $data['cardTypes'] = $this->CommonTable('Expenses\Model\CardTypeTable')->getUnAssignedCardTypes($accountId);
            $this->data = $data;
            if ($this->data) {
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_ACCOUNT_RETRIEVED');
            } else {
                $this->msg = $this->getMessage('ERR_ACCOUNT_NOT_FOUND');
            }
        }

        return $this->JSONRespond();
    }

    /**
     * get all AccountTypes
     * @return ViewModel
     */
    public function getAccountsAction()
    {
        //get page number
        $page = $this->params('param1', 1);
        //get accounts list
        $paginator = $this->CommonTable('Expenses\Model\AccountTable')->fetchAll(true, array('account.accountId DESC'));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $view = new ViewModel(array('accounts' => $paginator, 'paginated' => true));
        $view->setTerminal(true);
        $view->setTemplate('expenses/account-api/get-accounts');
        return $view;
    }

    /**
     * Search Account
     * @return ViewModel
     */
    public function searchAction()
    {

        if ($this->getRequest()->isPost()) {

            $searchField = $this->getRequest()->getPost('searchField');
            $searchValue = $this->getRequest()->getPost('searchValue');

            switch ($searchField) {
                case 'account-name':
                    $resultSet = $this->CommonTable('Expenses\Model\AccountTable')->getAccountsByTableField('accountId', $searchValue);
                    break;
                case 'account-number':
                    $resultSet = $this->CommonTable('Expenses\Model\AccountTable')->getAccountsByTableField('accountId', $searchValue);
                    break;
                case 'bank-name':
                    $resultSet = $this->CommonTable('Expenses\Model\AccountTable')->getAccountsByTableField('bankId', $searchValue);
                    break;
                default:
                    error_log('invalid field');
            }

            $view = new ViewModel(array('accounts' => $resultSet, $paginated = false));
            $view->setTerminal(true);
            $view->setTemplate('expenses/account-api/get-accounts');
            return $view;
        }
    }

    /**
     * get account name list for dropdown
     * @return JsonModel
     */
    public function accountNameListForDropdownAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = array('list' => array());

        if ($this->getRequest()->isPost()) {

            $searchKey = $this->getRequest()->getPost('searchKey');

            $accounts = $this->CommonTable('Expenses\Model\AccountTable')->searchAccountByAccountName($searchKey, array('account.accountName ASC'));

            $accountNameList = array();
            foreach ($accounts as $account) {
                $accountNameList[] = array('value' => $account['accountId'], 'text' => $account['accountName']);
            }
            $this->data = array('list' => $accountNameList);
        }

        return $this->JSONRespond();
    }

    /**
     * get account number list for dropdown
     * @return JsonModel
     */
    public function accountNumberListForDropdownAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = array('list' => array());

        if ($this->getRequest()->isPost()) {

            $searchKey = $this->getRequest()->getPost('searchKey');

            $accounts = $this->CommonTable('Expenses\Model\AccountTable')->searchAccountByAccountNumber($searchKey, array('account.accountNumber ASC'));

            $accountNumberList = array();
            foreach ($accounts as $account) {
                $accountNumberList[] = array('value' => $account['accountId'], 'text' => $account['accountNumber']);
            }
            $this->data = array('list' => $accountNumberList);
        }

        return $this->JSONRespond();
    }

    /**
     * get bank account list for dropdown
     * @return JsonModel
     */
    public function bankAccountListForDropdownAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = array('list' => array());

        if ($this->getRequest()->isPost()) {
            //get bank id
            $bankId = $this->getRequest()->getPost('bankId');
            $withGLAccount = (!empty($this->getRequest()->getPost('withGLAccount'))) ? true : false;

            $accounts = $this->CommonTable('Expenses\Model\AccountTable')->getAccountsByBankId($bankId);

            $accountNumberList = array();
            foreach ($accounts as $account) {
                if($withGLAccount) {

                $accountNumberList[] = array('glAccountID' => $account['financeAccountID'],'value' => $account['accountId'], 'text' => $account['accountName'] . '-' . $account['accountNumber']);
            } else {
                $accountNumberList[] = array('value' => $account['accountId'], 'text' => $account['accountName'] . '-' . $account['accountNumber']);

            }
            }
            $this->status = true;
            $this->msg = 'account list for bankId : ' . $bankId;
            $this->data = array('list' => $accountNumberList);
        }

        return $this->JSONRespond();
    }

    /**
     * get account currency details
     * @return JsonModel
     */
    public function getAccountCurrencyAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {
            //get account id
            $accountId = $this->getRequest()->getPost('accountId');
            //get account currency
            $currency = $this->CommonTable('Expenses\Model\AccountTable')->getAccountCurrencyDetailsByAccountId($accountId);
            $this->status = true;
            $this->msg = 'account currency details';
            $this->data = $currency;
        }

        return $this->JSONRespond();
    }

    /**
     * get account name and number list for dropdown
     * @return JsonModel
     */
    public function accountListForDropdownAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = array('list' => array());

        if ($this->getRequest()->isPost()) {

            $searchKey = $this->getRequest()->getPost('searchKey');

            $accounts = $this->CommonTable('Expenses\Model\AccountTable')->searchAccountByAccountName($searchKey, array('account.accountName ASC'));

            $accountNameList = array();
            foreach ($accounts as $account) {
                $accountNameList[] = array('value' => $account['accountId'], 'text' => $account['accountName'] . ' - ' . $account['accountNumber']);
            }
            $this->data = array('list' => $accountNameList);
        }

        return $this->JSONRespond();
    }

    /**
     * this function use to remove account from card types when user delete account
     * @param array $account
     * @return boolean
     */
    public function removeAccountFromCardType($account)
    {
        if ($account['isCardPaymentAccount'] == false) {
            return true;
        }
        $updated = $this->CommonTable('Expenses\Model\CardTypeTable')->clearAccount($account['accountId']);
        if (!$updated) {
            return false;
        }
        return true;
    }

    /**
     * this function use to find any related transactions about given accountID
     * @param int $accountId
     * @return boolean
     */
    public function checkLinkedPaymentDetails($accountId)
    {
        $cardPaymentDetails = $this->CommonTable('Invoice\Model\IncomingPaymentMethodCreditCardTable')->getByAccountID($accountId);
        $bankTransferDetails = $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->getBankTransfersByAccountId($accountId);
        if (count($cardPaymentDetails) > 0 || count($bankTransferDetails) > 0) {
            return false;
        }
        return true;
    }

}
