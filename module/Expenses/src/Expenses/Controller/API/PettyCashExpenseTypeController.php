<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Expenses\Model\PettyCashExpenseType;
use Zend\View\Model\ViewModel;

/**
 * Description of ExpenseTypeAPIController
 *
 * @author sharmilan <sharmilan@thinkcube.com>
 */
class PettyCashExpenseTypeController extends CoreController
{

    protected $paginator;

    /**
     * Add New Expense Type
     *
     * @return type
     */
    public function saveExpenseTypeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pettyCashExpenseTypeName = $request->getPost('pettyCashExpenseTypeName');
//            check the expense type name already exists
            $result = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getDetailsByName($pettyCashExpenseTypeName);

            if (count($result) > 0) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_NAME_EXISTS');
                return $this->JSONRespond();
            }

            $pettyCashExpenseType = new PettyCashExpenseType();
            $pettyCashExpenseType->exchangeArray($request->getPost());
            $insertedID = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->save($pettyCashExpenseType);

            if ($insertedID) {
                $this->getpaginater();
                $listview = new ViewModel(array('pettyCashExpenseTypes' => $this->paginator, 'paginated' => true));
                $listview->setTerminal(true);
                $listview->setTemplate('expenses/petty-cash-expense-type/list');
                $this->status = TRUE;
                $this->setLogMessage('Petty cash expense type '.$request->getPost('pettyCashExpenseTypeName').' is created.');
                $this->msg = $this->getMessage('SUCC_PETTY_CASH_EXPENSE_TYPE_SAVE');
                $this->html = $listview;
            } else {
                $this->status = FALSE;
                $this->msg = '';
            }
            return $this->JSONRespondHtml();
        }
    }

    /**
     * Update the Expense Type
     *
     * @return type
     */
    public function updateExpenseTypeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $expenseTypeID = $request->getPost('pettyCashExpenseTypeID');
            $expenseTypeName = $request->getPost('pettyCashExpenseTypeName');

            // check the expense type name already exists
            $result = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getDetailsByName($expenseTypeName);
            $oldData = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getDataByID($expenseTypeID)->current();

            $expenseType = $result->current();

            if (count($result) > 0 && $expenseType['pettyCashExpenseTypeID'] != $expenseTypeID) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_NAME_EXISTS');
                return $this->JSONRespond();
            } else {
                $pettyCashExpenseType = new PettyCashExpenseType();
                $pettyCashExpenseType->exchangeArray($request->getPost());
                $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->update($pettyCashExpenseType);
                $this->getpaginater();
                $listview = new ViewModel(array('pettyCashExpenseTypes' => $this->paginator, 'paginated' => true));
                $listview->setTerminal(true);
                $listview->setTemplate('expenses/petty-cash-expense-type/list');
                $this->status = TRUE;
                $changeArray = $this->getPettyCashAccountTypeChageDetails($oldData, $request->getPost());
                //set log details
                $previousData = json_encode($changeArray['previousData']);
                $newData = json_encode($changeArray['newData']);

                $this->setLogDetailsArray($previousData,$newData);
                $this->setLogMessage("Petty cash expense type ".$oldData->pettyCashExpenseTypeName." is updated.");
                $this->msg = $this->getMessage('SUCC_PETTY_CASH_EXPENSE_TYPE_UPDATE');
                $this->html = $listview;
            }
            return $this->JSONRespondHtml();
        }
    }

    public function getPettyCashAccountTypeChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Petty Cash Expense Type Name'] = $row->pettyCashExpenseTypeName;
        $previousData['Petty Cash Expense Type Description'] = $row->pettyCashExpenseTypeDescription;
        $previousData['Petty Cash Expense Type Min Amount'] = $row->pettyCashExpenseTypeMinAmount;
        $previousData['Petty Cash Expense Type Max Amount'] = $row->pettyCashExpenseTypeMaxAmount;

        $newData = [];
        $newData['Petty Cash Expense Type Name'] = $data['pettyCashExpenseTypeName'];
        $newData['Petty Cash Expense Type Description'] = $data['pettyCashExpenseTypeDescription'];
        $newData['Petty Cash Expense Type Min Amount'] = $data['pettyCashExpenseTypeMinAmount'];
        $newData['Petty Cash Expense Type Max Amount'] = $data['pettyCashExpenseTypeMaxAmount'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }

    /**
     * Get Expense Type Details by expense Type ID
     * @return type
     */
    public function getPettyCashExpenseTypeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pettyCashExpenseTypeID = $request->getPost('pettyCashExpenseTypeID');
            $result = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getDataByID($pettyCashExpenseTypeID);
            $expenseTypeData = $result->current();

            $financeAccountsArray = $this->getFinanceAccounts();
            $expenseTypeData->pettyCashExpenseTypeAccountName = $financeAccountsArray[$expenseTypeData->pettyCashExpenseTypeAccountID]['financeAccountsCode']."_".$financeAccountsArray[$expenseTypeData->pettyCashExpenseTypeAccountID]['financeAccountsName'];

            if ($expenseTypeData) {
                $this->status = TRUE;
                $this->data = $expenseTypeData;
            } else {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_NOT_EXISTS');
            }
            return $this->JSONRespond();
        }
    }

    /**
     * Delete the Expense Type
     *
     * @return view Model
     */
    public function deletePettyCashExpenseTypeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pettyCashExpenseTypeID = $request->getPost('pettyCashExpenseTypeID');
            $oldData = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getDataByID($pettyCashExpenseTypeID)->current();
            $pettyCashExpenseTypes = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getByPettyCashExpenseTypeID($pettyCashExpenseTypeID);
            if (count($pettyCashExpenseTypes) > 0) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_USED_PETTY_CASH_EXPENSE_TYPE_DELETE');
                return $this->JSONRespond();
            }

            $result = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->delete($pettyCashExpenseTypeID);

            if ($result) {
                $this->getpaginater();
                $listview = new ViewModel(array('pettyCashExpenseTypes' => $this->paginator, 'paginated' => TRUE));
                $listview->setTerminal(true);
                $listview->setTemplate('expenses/petty-cash-expense-type/list');
                $this->setLogMessage('Petty cash expense type '.$oldData->pettyCashExpenseTypeName.' is deleted.');
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_PETTY_CASH_EXPENSE_TYPE_DELETED');
                $this->html = $listview;
            } else {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_DELETE');
            }
            return $this->JSONRespondHtml();
        }
    }

    /**
     * Change Status of Expense Type
     *
     * @return view Model
     */
    public function updatePettyCashExpenseTypeStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pettyCashExpenseTypeID = $request->getPost('pettyCashExpenseTypeID');

            $result = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getDataByID($pettyCashExpenseTypeID);
            $expenseTypeData = $result->current();

            if (!$expenseTypeData) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_EXPENSE_TYPE_NOT_EXISTS');
            } else {
                $currentStatus = $expenseTypeData->pettyCashExpenseTypeStatus;

                if ($currentStatus) {
                    $newStatus = '0';
                } else {
                    $newStatus = '1';
                }

                $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->changeStatusByExpenseTypeID($pettyCashExpenseTypeID, $newStatus);
                $this->getpaginater();
                $listview = new ViewModel(['pettyCashExpenseTypes' => $this->paginator, 'paginated' => true]);
                $listview->setTerminal(true);
                $listview->setTemplate('expenses/petty-cash-expense-type/list');
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_PETTY_CASH_EXPENSE_TYPE_UPDATE_STATUS');
                $this->html = $listview;
            }

            return $this->JSONRespondHtml();
        }
    }

    public function searchExpenseTypeAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $searchKey = trim($request->getPost('searchKey'));

            if ($searchKey != '') {
                $pettyCashExpenseTypes = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->searchExpenseTypeList($searchKey);
                $expenseTypeListView = new ViewModel(['pettyCashExpenseTypes' => $pettyCashExpenseTypes]);
            } else {
                $this->getpaginater();
                $pettyCashExpenseTypes = $this->paginator;
                $expenseTypeListView = new ViewModel(['pettyCashExpenseTypes' => $pettyCashExpenseTypes, 'paginated' => true]);
            }

            $expenseTypeListView->setTerminal(true);
            $expenseTypeListView->setTemplate('expenses/petty-cash-expense-type/list');
            $this->status = true;
            $this->html = $expenseTypeListView;
            return $this->JSONRespondHtml();
        }
    }

    public function getpaginater($size = 6)
    {
        $this->paginator = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($size);
    }

}
