<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Form\ChequeDepositForm;
use Expenses\Model\ChequeDeposit;
use Expenses\Model\ChequeDepositCancellation;
use Expenses\Model\ChequeDepositCancellationCheque;
use Expenses\Model\ChequeDepositCheque;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Cheque deposit related actions
 */
class ChequeDepositAPIController extends CoreController
{    
    /**
     * Save deposit
     * @return JsonModel
     */
    public function saveAction()
    {   
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){

            $postData = $this->getRequest()->getPost()->toArray();

            $locationID = $this->user_session->userActiveLocation['locationID'];
            $date =  $this->convertDateToStandardFormat($postData['chequeDepositDate']);
            
            $accountCheque = array();
            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountFlag = (count($glAccountExist) > 0)? true :false;

            $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
            $payMethod = array();
            foreach ($paymentMethos as $key => $t) {
                $payMethod[$t['paymentMethodID']] = $t;
            }
            
            $chequePaymentMethodInSales = ($payMethod[2]['paymentMethodInSales'] == '1') ? true : false; 
            $chequePaymentMethodSalesAccountID = $payMethod[2]['paymentMethodSalesFinanceAccountID']; 

            //get bank list
            $bankList = array();
            $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll();
            foreach ($banks as $bank){
                $bankList[$bank['bankId']] = $bank['bankName'];
            }
        
            //create cheque deposit form
            $form = new ChequeDepositForm('chequeDeposit',$bankList);
            
            $chequeDeposit = new ChequeDeposit();
            $form->setInputFilter($chequeDeposit->getInputFilter());
            $form->setData($postData);
            
            if($form->isValid()){                
                try{
                    //begin transaction
                    $this->beginTransaction();                    
                    //get account details
                    $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($postData['accountId']);
                    if($account){
                        if($glAccountFlag && empty($account['financeAccountID'])){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_GL_ACCOUNT_IS_NOT_SET_FOR_BANK_ACCOUNT', array($account['accountName']));
                            return $this->JSONRespond();
                        }

                        if($glAccountFlag && (!$chequePaymentMethodInSales || empty($chequePaymentMethodSalesAccountID))){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_GL_ACCOUNT_IS_NOT_SET_FOR_IN_CHEQUE_PAYMENT_METHOD');
                            return $this->JSONRespond();   
                        }
                        //get deposit currency rate
                        $dCurrency = $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($postData['chequeDepositCurrencyId']);
                        $chequeDeposit->exchangeArray($postData);
                        $chequeDeposit->chequeDepositCurrencyRate = $dCurrency->currencyRate;
                        $chequeDeposit->chequeDepositAccountCurrencyRate = $account['currencyRate'];
                        $chequeDeposit->entityId = $this->createEntity();
                        //save cheque deposit
                        $chequeDepositId = $this->CommonTable('Expenses\Model\ChequeDepositTable')->saveChequeDeposit($chequeDeposit);
                        if($chequeDepositId){
                            //save deosited cheques
                            $cheque = new ChequeDepositCheque();
                            $cheque->chequeDepositId = $chequeDepositId;
                            foreach($postData['cheques'] as $chequeId){
                                $cheque->incomingPaymentMethodChequeId = $chequeId;
                                $cheque->entityId = $this->createEntity();
                                $exitsingCheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($chequeId);
                                if (is_null($exitsingCheque)) {
                                    $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->saveChequeDeposit($cheque);
                                } else {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = "some of cheque are already deposited";
                                    return $this->JSONRespond();
                                }
                                //updated postdated cheque notification status
                                $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->updateIncomingPaymentMethodCheque(array('postdatedChequeStatus'=>0),$chequeId);
                                $this->CommonTable('Core\Model\NotificationTable')->updateNotification(array('status'=>'1'),$chequeId,'6');
                            }      
                            //calculate deposit amount
                            $depositAmount = $postData['chequeDepositAmount'] * $dCurrency->currencyRate ;
                            
                            if($glAccountFlag){
                            //set gl accounts for the journal entry
                                if(isset($accountCheque[$account['financeAccountID']]['debitTotal'])){
                                    $accountCheque[$account['financeAccountID']]['debitTotal'] += $depositAmount;
                                }else{
                                    $accountCheque[$account['financeAccountID']]['debitTotal'] = $depositAmount;
                                    $accountCheque[$account['financeAccountID']]['creditTotal'] = 0.00;
                                    $accountCheque[$account['financeAccountID']]['accountID'] = $account['financeAccountID'];
                                }

                                foreach($postData['cheques'] as $chequeId){
                                    $chequePaymentSalesAccountID = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getChequeDetailsByChequeId($chequeId)->current();

                                    if(isset($accountCheque[$chequePaymentSalesAccountID['incomingPaymentMethodChequeAccNo'].'_'.'cheque']['creditTotal'])){
                                        $accountCheque[$chequePaymentSalesAccountID['incomingPaymentMethodChequeAccNo'].'_'.'cheque']['creditTotal'] += $chequePaymentSalesAccountID['incomingPaymentMethodAmount'];
                                    }else{
                                        $accountCheque[$chequePaymentSalesAccountID['incomingPaymentMethodChequeAccNo'].'_'.'cheque']['creditTotal'] = $chequePaymentSalesAccountID['incomingPaymentMethodAmount'];
                                        $accountCheque[$chequePaymentSalesAccountID['incomingPaymentMethodChequeAccNo'].'_'.'cheque']['debitTotal'] = 0.00;
                                        $accountCheque[$chequePaymentSalesAccountID['incomingPaymentMethodChequeAccNo'].'_'.'cheque']['accountID'] = $chequePaymentSalesAccountID['incomingPaymentMethodChequeAccNo'];
                                    }
                                }

                                $j=0;
                                $journalEntryAccounts = array();

                                foreach ($accountCheque as $akey => $avalue) {
                                    $journalEntryAccounts[$j]['fAccountsIncID'] = $j;
                                    $journalEntryAccounts[$j]['financeAccountsID'] = $avalue['accountID'];
                                    $journalEntryAccounts[$j]['financeGroupsID'] = '';
                                    $journalEntryAccounts[$j]['journalEntryAccountsDebitAmount'] = $avalue['debitTotal'];
                                    $journalEntryAccounts[$j]['journalEntryAccountsCreditAmount'] = $avalue['creditTotal'];
                                    $journalEntryAccounts[$j]['journalEntryAccountsMemo'] = 'Created By Cheque Deposit '.$chequeDepositId.'.';
                                    $j++;
                                }

                                //get journal entry reference number.
                                $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                                $jelocationReferenceID = $jeresult['locRefID'];
                                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                                $journalEntryData = array(
                                    'journalEntryAccounts' => $journalEntryAccounts,
                                    'journalEntryDate' => $date,
                                    'journalEntryCode' => $JournalEntryCode,
                                    'journalEntryTypeID' => '',
                                    'journalEntryIsReverse' => 0,
                                    'journalEntryComment' => 'Journal Entry is posted when Deposit the cheques '.$chequeDepositId.'.',
                                    'documentTypeID' => 32,
                                    'journalEntryDocumentID' => $chequeDepositId,
                                    'ignoreBudgetLimit' => $postData['ignoreBudgetLimit'],
                                );
                                $resultData = $this->saveJournalEntry($journalEntryData);
                                if(!$resultData['status']){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $resultData['msg'];
                                    $this->data = $resultData['data'];
                                    return $this->JSONRespond();
                                }
                            }

                            $depositAmount = $depositAmount / $account['currencyRate'];
                            //calculate new account balance                            
                            $accountBalance = $account['accountBalance'] + $depositAmount;
                            //update account balance
                            $updateStatus = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount(array('accountBalance' => $accountBalance), $postData['accountId']);
                            if($updateStatus){
                                //commit transaction
                                $this->commit();
                                $this->status = true;
                                $this->msg = $this->getMessage('SUCC_CHEQUEDEPOSIT_SAVE');
                            } else {
                                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_SAVE');
                            }
                        } else {
                            $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_SAVE');
                        }
                    } else {
                        $this->msg = $this->getMessage('ERR_ACCOUNT_NOT_FOUND');
                    }
                } catch (\Exception $e){
                    //rollback transaction
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_SAVE');
                }
                
            } else {
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_VALIDATION');
            }
        }
        
        return $this->JSONRespond();
    }
    
    /**
     * Cancel deposit
     * @return JsonModel
     */
    public function cancelAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){
            $depositId = $this->getRequest()->getPost('chequeDepositId');
            $ignoreBudgetLimit = $this->getRequest()->getPost('ignoreBudgetLimit');
            $locationID = $this->user_session->userActiveLocation['locationID'];

            $accountCheque = array();
            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountFlag = (count($glAccountExist) > 0)? true :false;
            $glAccountData = $glAccountExist->current();

            $paymentMethodChequeAmount = array();
            $cheques = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getIncomingPaymentMethodChequeByChequeDepositId($depositId);
            foreach ($cheques as $ckey => $cvalue) {
                $paymentMethodChequeAmount[$cvalue['incomingPaymentMethodChequeId']]= $cvalue['incomingPaymentMethodAmount'] / $cvalue['incomingPaymentCustomCurrencyRate'];
            }

            $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
            $payMethod = array();
            foreach ($paymentMethos as $key => $t) {
                $payMethod[$t['paymentMethodID']] = $t;
            }

            $chequePaymentMethodInSales = ($payMethod[2]['paymentMethodInSales'] == '1') ? true : false; 
            $chequePaymentMethodSalesAccountID = $payMethod[2]['paymentMethodSalesFinanceAccountID']; 

            $deposit   = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getChequeDepositByChequeDeposiId($depositId);
            if($deposit){
                //get cheques related to deposit id
                $cheques = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getIncomingPaymentMethodChequeByChequeDepositId($depositId);
                
                $chequeList = array();
                foreach ($cheques as $cheque){
                    $chequeList[] = $cheque['incomingPaymentMethodChequeId'];
                }

                $reconcileStatus = false;
                if(count($chequeList) > 0){
                    //check whether deposit reconcile
                    $reconcileStatus = $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->isIncomingChequeReconciled($chequeList);
                }
                if(!$reconcileStatus){
                    try{
                        //begin transaction
                        $this->beginTransaction();


                        //for delete deposit
                        $this->updateDeleteInfoEntity($deposit['entityId']);
                        //deposit cancellation
                        $cancellation = new ChequeDepositCancellation();
                        $cancellation->chequeDepositId = $depositId;
                        $cancellation->chequeDepositCancelationCharge = 0;
                        $cancellation->chequeDepositCancellationComment = 'Deposit delete';
                        $cancellationId = $this->CommonTable('Expenses\Model\ChequeDepositCancellationTable')->save($cancellation);
                        //cheque cancellation
                        $cancellationCheque = new ChequeDepositCancellationCheque();
                        $cancellationCheque->chequeDepositCancelationId = $cancellationId;
                        //for delete deposit cheques
                        $chequeRecivedAmount = 0;
                        foreach ($chequeList as $chequeId){
                            $chequeRecivedAmount += $paymentMethodChequeAmount[$chequeId];
                            //get cheque
                            $cheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($chequeId);
                            //delete cheque
                            $this->updateDeleteInfoEntity($cheque['entityId']);
                            //save to cancellation cheque
                            $cancellationCheque->incomingPaymentMethodChequeId = $chequeId;
                            $this->CommonTable('Expenses\Model\ChequeDepositCancellationChequeTable')->save($cancellationCheque);
                        }
                        //get account details
                        $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($deposit['accountId']);
                        if($account){
                            //calculate deposit amount
                            $depositedAmount = $deposit['chequeDepositAmount'] * $deposit['chequeDepositCurrencyRate'] / $deposit['chequeDepositAccountCurrencyRate'];
                            $accountBalance = $account['accountBalance'] - $depositedAmount;
                            //update account balance
                            $this->CommonTable('Expenses\Model\AccountTable')->updateAccount(array('accountBalance' => $accountBalance), $deposit['accountId']);
                            
                            if ($glAccountFlag && count($chequeList) > 0) {

                                if(empty($account['financeAccountID'])){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_GL_ACCOUNT_IS_NOT_SET_FOR_BANK_ACCOUNT', array($account['accountName']));
                                    return $this->JSONRespond();
                                }

                                if(!$chequePaymentMethodInSales || empty($chequePaymentMethodSalesAccountID)){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_GL_ACCOUNT_IS_NOT_SET_FOR_IN_CHEQUE_PAYMENT_METHOD');
                                    return $this->JSONRespond();   
                                }
                                $chequeRecivedAmount = $chequeRecivedAmount*$deposit['chequeDepositCurrencyRate'];
                                if(isset($accountCheque[$chequePaymentMethodSalesAccountID]['debitTotal'])){
                                    $accountCheque[$chequePaymentMethodSalesAccountID]['debitTotal'] += $chequeRecivedAmount;
                                }else{
                                    $accountCheque[$chequePaymentMethodSalesAccountID]['debitTotal'] = $chequeRecivedAmount;
                                    $accountCheque[$chequePaymentMethodSalesAccountID]['creditTotal'] = 0.00;
                                    $accountCheque[$chequePaymentMethodSalesAccountID]['accountID'] = $chequePaymentMethodSalesAccountID;
                                }

                                if(isset($accountCheque[$account['financeAccountID']]['creditTotal'])){
                                    $accountCheque[$account['financeAccountID']]['creditTotal'] += $chequeRecivedAmount;
                                }else{
                                    $accountCheque[$account['financeAccountID']]['creditTotal'] = $chequeRecivedAmount;
                                    $accountCheque[$account['financeAccountID']]['debitTotal'] = 0.00;
                                    $accountCheque[$account['financeAccountID']]['accountID'] = $account['financeAccountID'];
                                }
                                
                                if(count($accountCheque) > 0){
                                    $i=0;
                                    $journalEntryAccounts = array();
                                    foreach ($accountCheque as $key => $value) {
                                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Cheque Deposit cancellation '.$depositId.'.';
                                        $i++;
                                    }

                    //get journal entry reference number.
                                    $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                                    $jelocationReferenceID = $jeresult['locRefID'];
                                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                                    $journalEntryData = array(
                                        'journalEntryAccounts' => $journalEntryAccounts,
                                        'journalEntryDate' => $this->convertDateToStandardFormat($this->getUserDateTime()),
                                        'journalEntryCode' => $JournalEntryCode,
                                        'journalEntryTypeID' => '',
                                        'journalEntryIsReverse' => 0,
                                        'journalEntryComment' => 'Journal Entry is posted when cancel the cheque deposits '.$depositId.'.',
                                        'documentTypeID' => 32,
                                        'journalEntryDocumentID' => $depositId,
                                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                                        );

                                    $resultData = $this->saveJournalEntry($journalEntryData);

                                    if(!$resultData['status']){
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $resultData['msg'];
                                        $this->data = $resultData['data'];
                                        return $this->JSONRespond();
                                    }

                                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($depositId,32, 5);
                                    if(!$jEDocStatusUpdate['status']){
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $jEDocStatusUpdate['msg'];
                                        return $this->JSONRespond();
                                    }

                                }
                            }

                            //commit transaction
                            $this->commit();
                            $this->status = true;
                            $this->msg = $this->getMessage('SUCC_CHEQUEDEPOSIT_CANCEL');
                        } else {
                            $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_CANCEL');
                        }
                    } catch (\Exception $e){
                        //rollback transaction
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_CANCEL');
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_CANCEL_RECONCILIATION');
                }
                
            } else {
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_NOT_FOUND');
            }
        }
        
        return $this->JSONRespond();
    }
    
    /**
     * Update deposit
     * @return JsonModel
     */
    function updateAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if (!$this->getRequest()->isPost()) {
            return $this->JSONRespond();
        }
        
        $postData = $this->getRequest()->getPost()->toArray();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $postData['bouncedCheques'] = empty($postData['bouncedCheques']) ? [] : $postData['bouncedCheques'];
                
        $accountCheque = array();
        $totalIncomingPayment = 0;
        $totalPaidInvoiceAmount = 0;
        $chequeRecivedAmount = 0;
        $paymentId = '';
        $paymentCode = '';
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountFlag = (count($glAccountExist) > 0)? true :false;
        $glAccountData = $glAccountExist->current();
        $bankChargesGlAccountID = $glAccountData->glAccountSetupGeneralBankChargersAccountID;

        $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        $payMethod = array();
        foreach ($paymentMethos as $key => $t) {
            $payMethod[$t['paymentMethodID']] = $t;
        }

        $chequePaymentMethodInSales = ($payMethod[2]['paymentMethodInSales'] == '1') ? true : false; 
        $chequePaymentMethodSalesAccountID = $payMethod[2]['paymentMethodSalesFinanceAccountID']; 

        $deposit = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getChequeDepositByChequeDeposiId($postData['chequeDepositId']);
        
        if (!$deposit) {
            $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_NOT_FOUND');
            return $this->JSONRespond();
        }        
        //check whether deposit reconcile
        $reconcileStatus = $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->isIncomingChequeReconciled($postData['chequeList']);
        if ($reconcileStatus) {
            $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE_RECONCILIATION');
            return $this->JSONRespond();
        }

        try {
            //begin transaction
            $this->beginTransaction();

            $chequeDepositCancelationCharge = $postData['chequeDepositCancelationCharge']* $deposit['chequeDepositCurrencyRate'];
            //get account details
            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($deposit['accountId']);
            if (!$account) {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                return $this->JSONRespond();
            }

            if($glAccountFlag && empty($account['financeAccountID'])){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_GL_ACCOUNT_IS_NOT_SET_FOR_BANK_ACCOUNT', array($account['accountName']));
                return $this->JSONRespond();
            }

            if($glAccountFlag && (!$chequePaymentMethodInSales || empty($chequePaymentMethodSalesAccountID))){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_GL_ACCOUNT_IS_NOT_SET_FOR_IN_CHEQUE_PAYMENT_METHOD');
                return $this->JSONRespond();   
            }

            if($glAccountFlag && $chequeDepositCancelationCharge != 0 && empty($bankChargesGlAccountID)){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_GL_ACCOUNT_IS_NOT_SET_FOR_BANK_CHARGERS_ACCOUNT');
                return $this->JSONRespond();  
            }

            $this->updateEntity($deposit['entityId']);
            //update chequeDeposit table
            $chequeDepositUpdateStatus = $this->CommonTable('Expenses\Model\ChequeDepositTable')->updateChequeDeposit(array('chequeDepositAmount'=>$postData['chequeDepositAmount']),$postData['chequeDepositId']);
            if (!$chequeDepositUpdateStatus) {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                return $this->JSONRespond();
            }
            foreach ($postData['chequeList'] as $chequeData) {
                $chequeId = $chequeData['chequeID'];
                $chequeAmount = $chequeData['amount'];
                //get related payment
                $invoicePayments = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getInvoicePaymentDeatailsByChequeId($chequeId);
                $paymentMethodData = iterator_to_array($invoicePayments);
                //get payment meta data
                $paymentId = $paymentMethodData[0]['incomingPaymentID'];
                $paymentMethodAmount = $paymentMethodData[0]['incomingPaymentMethodAmount'];
                $chequeRecivedAmount+= $chequeAmount*$deposit['chequeDepositCurrencyRate'];
                
                if (in_array($chequeId, $postData['bouncedCheques'])) {
                    $paymentReversedAmount = 0;
                    $restPaymentAmount = $paymentMethodAmount;
                    $invoicePaymentReverseArr = [];

                    foreach ($paymentMethodData as $invoicePayment) {
                        if ($paymentMethodAmount > $paymentReversedAmount) {
                            //delete payment method
                            $this->updateDeleteInfoEntity($invoicePayment['entityId']);
                            if ($restPaymentAmount <= $invoicePayment['incomingInvoicePaymentAmount']) {                            
                                $invoicePaymentReverseArr[$invoicePayment['salesInvoiceID']] = (float) $restPaymentAmount;
                                $restPaymentAmount = 0;
                                break;
                            } else {
                                $invoicePaymentReverseArr[$invoicePayment['salesInvoiceID']] = (float) $invoicePayment['incomingInvoicePaymentAmount'];
                                $restPaymentAmount -= $invoicePayment['incomingInvoicePaymentAmount'];
                            }
                        }
                    }

                    $invoiceAmount = 0;
                    $totalReversedAmount = 0;
                    foreach ($invoicePaymentReverseArr as $invoiceId => $reverseAmount) {
                        $invoicePaymentDetails = $this->CommonTable("Invoice\Model\InvoicePaymentsTable")->getInvoicePaymentByInvoiceIdAndPaymentId($invoiceId, $paymentId);
                        $invoicePaymentData = [
                            'incomingInvoicePaymentAmount' => ($invoicePaymentDetails['incomingInvoicePaymentAmount'] - $reverseAmount)
                        ];

                        $InvoicePaymentUpadteStatus = $this->CommonTable("Invoice\Model\InvoicePaymentsTable")->updateInvoicePaymentByInvoiceIdAndPaymentId($invoicePaymentData, $invoiceId, $paymentId);
                        if (!$InvoicePaymentUpadteStatus) {
                            $this->rollback();
                            $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                            return $this->JSONRespond();
                        }

                        //get invoice
                        $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceId);
                        $invoiceData = [
                            'salesInvoicePayedAmount' => ($invoice->salesInvoicePayedAmount - $reverseAmount),
                            'statusID' => 3
                        ];
                        $invoiceUpdateStatus = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoice($invoiceData, $invoiceId);
                        if (!$invoiceUpdateStatus) {
                            $this->rollback();
                            $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                            return $this->JSONRespond();
                        }
                    }

                    $paymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentByID($paymentId);

                    $paymentCode = $paymentDetails->incomingPaymentCode;
                    $existingPaymentAmount = $paymentDetails->incomingPaymentAmount;
                    $existingPaymentPaidAmount = $paymentDetails->incomingPaymentPaidAmount;
                    $existingPaymentBalance = (is_null($paymentDetails->incomingPaymentBalance)) ? 0 : $paymentDetails->incomingPaymentBalance;

                    //get remaing payment details
                    $remainingPayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentData($paymentId);
                    foreach ($remainingPayment as $key => $value) {
                        $totalIncomingPayment += $value['incomingPaymentMethodAmount'];
                    }

                    $checkCreditPayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentByID($paymentId);
                    if (!is_null($checkCreditPayment->incomingPaymentCreditAmount)) {
                        $totalIncomingPayment += $checkCreditPayment->incomingPaymentCreditAmount;
                    }

                    //get remaing invoice payment details
                    $remainingInvoiceAmount = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getInvoicePaymentByPaymentId($paymentId);
                    foreach ($remainingInvoiceAmount as $key => $value) {
                        $totalPaidInvoiceAmount += $value['incomingInvoicePaymentAmount'];
                    }


                    $invoiceRemaingAmount = $totalIncomingPayment - $totalPaidInvoiceAmount;
                    $paymentData = [
                        'incomingPaymentPaidAmount' => $totalIncomingPayment,
                        'incomingPaymentAmount' => $totalPaidInvoiceAmount,
                        'incomingPaymentBalance' => $invoiceRemaingAmount
                    ];
                    $paymentUpadateStatus = $this->CommonTable('Invoice\Model\PaymentsTable')->update($paymentData, $paymentId);
                    if (!$paymentUpadateStatus) {
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                        return $this->JSONRespond();
                    }


                    //get customer
                    $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($invoice->customerID);
                    
                    $customerCurrentBalance =  $customer->customerCurrentBalance + $existingPaymentAmount - $totalPaidInvoiceAmount;
                    $customerCurrentCredit =  $customer->customerCurrentCredit - $existingPaymentBalance + $invoiceRemaingAmount;
                    
                    $customerData = [
                        'customerCurrentBalance' => $customerCurrentBalance,
                        'customerCurrentCredit' => $customerCurrentCredit
                    ];
                    
                    $customerUpdateStatus = $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerByArray($customerData, $paymentDetails->customerID);
                    if (!$customerUpdateStatus) {
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                        return $this->JSONRespond();
                    }

                }   

                $cheque = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getChequeDepositChequeByIncomingPaymentMethodChequeId($chequeId);
                //delete cheque
                $this->updateDeleteInfoEntity($cheque['entityId']);
            }

            if($glAccountFlag){
                    //set gl accounts for the journal entry
                if(isset($accountCheque[$chequePaymentMethodSalesAccountID]['debitTotal'])){
                    $accountCheque[$chequePaymentMethodSalesAccountID]['debitTotal'] += $chequeRecivedAmount;
                }else{
                    $accountCheque[$chequePaymentMethodSalesAccountID]['debitTotal'] = $chequeRecivedAmount;
                    $accountCheque[$chequePaymentMethodSalesAccountID]['creditTotal'] = 0.00;
                    $accountCheque[$chequePaymentMethodSalesAccountID]['accountID'] = $chequePaymentMethodSalesAccountID;
                }

                if(!empty($chequeDepositCancelationCharge)){
                    if(isset($accountCheque[$bankChargesGlAccountID]['debitTotal'])){
                        $accountCheque[$bankChargesGlAccountID]['debitTotal'] += $chequeDepositCancelationCharge;
                    }else{
                        $accountCheque[$bankChargesGlAccountID]['debitTotal'] = $chequeDepositCancelationCharge;
                        $accountCheque[$bankChargesGlAccountID]['creditTotal'] = 0.00;
                        $accountCheque[$bankChargesGlAccountID]['accountID'] = $bankChargesGlAccountID;
                    }
                    $chequeRecivedAmount +=$chequeDepositCancelationCharge;
                }

                if(isset($accountCheque[$account['financeAccountID']]['creditTotal'])){
                    $accountCheque[$account['financeAccountID']]['creditTotal'] += $chequeRecivedAmount;
                }else{
                    $accountCheque[$account['financeAccountID']]['creditTotal'] = $chequeRecivedAmount;
                    $accountCheque[$account['financeAccountID']]['debitTotal'] = 0.00;
                    $accountCheque[$account['financeAccountID']]['accountID'] = $account['financeAccountID'];
                }

                $j=0;
                $journalEntryAccounts = array();

                foreach ($accountCheque as $akey => $avalue) {
                    $journalEntryAccounts[$j]['fAccountsIncID'] = $j;
                    $journalEntryAccounts[$j]['financeAccountsID'] = $avalue['accountID'];
                    $journalEntryAccounts[$j]['financeGroupsID'] = '';
                    $journalEntryAccounts[$j]['journalEntryAccountsDebitAmount'] = $avalue['debitTotal'];
                    $journalEntryAccounts[$j]['journalEntryAccountsCreditAmount'] = $avalue['creditTotal'];
                    $journalEntryAccounts[$j]['journalEntryAccountsMemo'] = 'Created By Cheque Deposit cancellation '.$postData['chequeDepositId'].'.';
                    $j++;
                }

                //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $this->convertDateToStandardFormat($this->getUserDateTime()),
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when cancel the Cheque Deposit '.$postData['chequeDepositId'].'.',
                    'documentTypeID' => 32,
                    'journalEntryDocumentID' => $postData['chequeDepositId'],
                    'ignoreBudgetLimit' => $postData['ignoreBudgetLimit'],
                );

                $resultData = $this->saveJournalEntry($journalEntryData);
                if(!$resultData['status']){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $resultData['msg'];
                    $this->data = $resultData['data'];
                    return $this->JSONRespond();
                }

                $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($postData['chequeDepositId'],32, 5);
                if(!$jEDocStatusUpdate['status']){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $jEDocStatusUpdate['msg'];
                    return $this->JSONRespond();
                }

                //revers incoming cheque payments journal entry
                if ($postData['bouncedCheques']) {
                    $paymentJEDetails = $this->reverseIncomingChequePaymentJEByBouncedCheques($postData['chequeDepositId'], $postData['bouncedCheques'], $locationID, $totalIncomingPayment, $totalPaidInvoiceAmount, $paymentId, $paymentCode, $postData['ignoreBudgetLimit']);
                    if(!$paymentJEDetails['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $paymentJEDetails['msg'];
                        $this->data = $paymentJEDetails['data'];
                        return $this->JSONRespond();
                    }
                    
                }
                
            }
            //get deposit diff
            $diffAmount = $deposit['chequeDepositAmount'] - $postData['chequeDepositAmount'];
            $accountBalance = $account['accountBalance'] - ($diffAmount * $deposit['chequeDepositCurrencyRate'] / $deposit['chequeDepositAccountCurrencyRate']);
            //apply bank charge
            if (is_numeric($postData['chequeDepositCancelationCharge'])) {
                $accountBalance = $accountBalance - ($postData['chequeDepositCancelationCharge'] * $deposit['chequeDepositCurrencyRate'] / $deposit['chequeDepositAccountCurrencyRate']);
            }
            //update account balance
            $updateStatus = $this->CommonTable('Expenses\Model\AccountTable')->updateAccount(array('accountBalance' => $accountBalance), $deposit['accountId']);

            if (!$updateStatus) {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                return $this->JSONRespond();
            }
            //update chequeDepositCancellation table
            $chequeDepositCancellation = new ChequeDepositCancellation();
            $chequeDepositCancellation->exchangeArray($postData);
            $cancellationId = $this->CommonTable('Expenses\Model\ChequeDepositCancellationTable')->save($chequeDepositCancellation);
            if (!$cancellationId) {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                return $this->JSONRespond();
            }            
            //update chequeDepositCancellationCheque table
            $cancellationCheque = new ChequeDepositCancellationCheque();
            $cancellationCheque->chequeDepositCancelationId = $cancellationId;
            $cancellationArr = [];
            foreach ($postData['chequeList'] as $chequeId) {
                $cancellationCheque->incomingPaymentMethodChequeId = $chequeId;
                $cancellationArr[] = $this->CommonTable('Expenses\Model\ChequeDepositCancellationChequeTable')->save($cancellationCheque);
            }
            if (in_array( FALSE, $cancellationArr, true)) {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                return $this->JSONRespond();
            }            
            //update bounced status
            $incomingPayChequeArr = [
                'incomingPaymentMethodChequeBouncedStatus' => 1
            ];
            $bouncedStatus = true;
            if (!empty($postData['bouncedCheques'])) {
                $bouncedStatus = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')
                            ->updateMultipleIncomingPaymentMethodCheque($incomingPayChequeArr, $postData['bouncedCheques']);
            }
            
            if (!$bouncedStatus) {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
                return $this->JSONRespond();
            }
            //commit transaction
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_CHEQUEDEPOSIT_UPDATE');
            return $this->JSONRespond();
        } catch (\Exception $e) {
            //rollback transaction
            $this->rollback();
            $this->msg = $this->getMessage('ERR_CHEQUEDEPOSIT_UPDATE');
            return $this->JSONRespond();
        }        
    }
    
    /**
     * get all undeposited cheques
     * @return ViewModel
     */
    public function getUndepositedChequesAction()
    {   
        //get customer cheques
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $paginator = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getUndepositedCheques( false, $locationID);

        $view = new ViewModel(array('cheques' => $paginator));
        $view->setTerminal(true);
        $view->setTemplate('expenses/cheque-deposit-api/get-undeposited-cheques');
        return $view;
    }
    
    /**
     * get all cheque deposits
     * @return ViewModel
     */
    public function getDepositsAction()
    {       
        //get page number
        $page = $this->params('param1',1);
        
        //get cheque deposits
        $paginator = $this->CommonTable('Expenses\Model\ChequeDepositTable')->fetchAll( true, array('chequeDeposit.chequeDepositId DESC'));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $view = new ViewModel(array('deposits' => $paginator));
        $view->setTerminal(true);
        $view->setTemplate('expenses/cheque-deposit-api/get-deposits');
        return $view;
    }
    
    /**
     * get all cheque within the single deposit
     * @return ViewModel
     */
    public function getDepositAction()
    {   
        $cheques      = array();
        
        if($this->getRequest()->isPost()){
            $depositId = $this->getRequest()->getPost('chequeDepositId');
            $deposit   = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getChequeDepositByChequeDeposiId($depositId);
            if($deposit){
                $cheques = $this->CommonTable('Expenses\Model\ChequeDepositChequeTable')->getIncomingPaymentMethodChequeByChequeDepositId($depositId);
            }
        }
        
        $view = new ViewModel(array('cheques' => $cheques));
        $view->setTerminal(true);
        $view->setTemplate('expenses/cheque-deposit-api/get-deposit');
        return $view;
    }

    /**
    * this function use to create a reverse journal entry for sales cheque payment when create a bounced cheque
    * @param int $chequeDepositID
    * @param array $bounceChequeIDs
    * @param int $locationID
    * return array
    **/
    public function reverseIncomingChequePaymentJEByBouncedCheques($chequeDepositID, $bounceChequeIDs, $locationID, $newpaidAmount, $newPaymentAmount, $paymentId, $paymentCode, $ignoreBudgetLimit)
    {
        $accoutArray = [];
        $docType = 7; //customer payment
        $journalEntry = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithAccountDetailsByDocumentTypeIDAndDocumentID($docType, $paymentId);

        $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('7', $paymentId);
        $journalEntryID = $journalEntryData['journalEntryID'];
        $journalEntryDate = $journalEntryData['journalEntryDate'];
       
        $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);


        $i=0;
        $journalEntryAccounts = array();
        foreach ($jEAccounts as $key => $value) {
            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
            $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Cheque Deposit cancellation '.$chequeDepositID.' for payment ('.$paymentCode.') reverse.';
            $i++;
        }

        //get journal entry reference number.
        $jeresult = $this->getReferenceNoForLocation('30', $locationID);
        $jelocationReferenceID = $jeresult['locRefID'];
        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

        $journalEntryData = array(
            'journalEntryAccounts' => $journalEntryAccounts,
            'journalEntryDate' => $this->convertDateToStandardFormat($journalEntryData['journalEntryDate']),
            'journalEntryCode' => $JournalEntryCode,
            'journalEntryTypeID' => '',
            'journalEntryIsReverse' => 0,
            'journalEntryComment' => 'Journal Entry is posted when bounce the Cheque Deposit '.$chequeDepositID.' for reverse payment ' .$paymentCode.'.',
            'documentTypeID' => 32,
            'journalEntryDocumentID' => $chequeDepositID,
            'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );


        $resultData = $this->saveJournalEntry($journalEntryData);
        if(!$resultData['status']){
           return $resultData;
        }

        $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($chequeDepositID,32, 5);
        if(!$jEDocStatusUpdate['status']){
            return $jEDocStatusUpdate;
        }

        $journalEntryCodeForComment = $JournalEntryCode;

        $paymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentByID($paymentId);
        
        $remainingPayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByPaymentIDForJE($paymentId, true);

        //get gl accounts
        $chequePayID = '2'; // becasue cheque paymentID = 2
        $chequePayData = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethod($chequePayID);  
    
        $cashPayID = '1'; // becasue cheque paymentID = 2
        $cashPayData = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethod($cashPayID);  
    
        $cardPayID = '3'; // becasue cheque paymentID = 2
        $cardPayData = $this->CommonTable('Core\Model\PaymentMethodTable')->getPaymentMethod($cardPayID);  
    
        $overPayAccount = 0;
        $cusRecivbleAcc = 0;
        foreach ($remainingPayment as $key => $value) {
            $overPayAccount = $value['customerAdvancePaymentAccountID'];
            $cusRecivbleAcc = $value['customerReceviableAccountID'];
            if (!is_null($value['incomingPaymentMethodChequeNumber'])) {
                $chequePayAcc = ($value['incomingPaymentMethodChequeAccNo'] == 0) ? $chequePayData->paymentMethodSalesFinanceAccountID : $value['incomingPaymentMethodChequeAccNo'];
                $accoutArray[$chequePayAcc]['debitAmount'] += $value['incomingPaymentMethodAmount'];
            } else if (!is_null($value['incomingPaymentMethodCashId'])) {
                $cashPayAcc = (is_null($value['cashAccountID'])) ? $cashPayData->paymentMethodSalesFinanceAccountID : $value['cashAccountID'];
                $accoutArray[$cashPayAcc]['debitAmount'] += $value['incomingPaymentMethodAmount'];
            } else if (!is_null($value['incomingPaymentMethodCreditReceiptNumber'])) {
                $cardPayAcc = ($value['cardAccountID'] == 0) ? $cardPayID->paymentMethodSalesFinanceAccountID : $value['cardAccountID'];
                $accoutArray[$cardPayAcc]['debitAmount'] += $value['incomingPaymentMethodAmount'];
            } 
        }

        //checkcreditPayment
        $remainingCreditPayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getCreditPaymentsByPaymentIDForJE($paymentId);
        foreach ($remainingCreditPayment as $key => $value) {
            $overPayAccount = $value['customerAdvancePaymentAccountID'];
            $cusRecivbleAcc = $value['customerReceviableAccountID'];
            if (!is_null($value['incomingPaymentCreditAmount'])) {
                $creditPayAcc = $value['customerAdvancePaymentAccountID'];
                $accoutArray[$creditPayAcc]['debitAmount'] += $value['incomingPaymentCreditAmount'];
            } 
        }

        if ($newPaymentAmount != 0 || $newpaidAmount != 0) {
            $accoutArray[$overPayAccount]['creditAmount'] = $newpaidAmount - $newPaymentAmount;
            $accoutArray[$cusRecivbleAcc]['creditAmount'] = $newPaymentAmount;
        }

        $subJEmsg = 'Created By Sales Payment '.$paymentCode.'.';
        $mainJEMsg = 'Journal Entry is posted when create Sales Payment '.$paymentCode.' by Cheque bounced of '.$chequeDepositID.'.';
        $docID = $chequeDepositID;
        $docTypeID = 32;

        $createdData = $this->setDataToJournalEntrySaveArray($accoutArray, $locationID, $subJEmsg, $mainJEMsg, $docID, $docTypeID, $journalEntryDate, $ignoreBudgetLimit);
        if (!empty($accoutArray)) {
            $result = $this->saveJournalEntry($createdData);

            if(!$result['status']){
               return $result;
            }
            $journalEntryCodeForComment = $createdData['journalEntryCode'];

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($docID,$docTypeID, 5);
            if(!$jEDocStatusUpdate['status']){
                return $jEDocStatusUpdate;
            }

        }

        if (is_null($paymentDetails->incomingPaymentMemo)) {
            $comment = "Journal entry of this payment is cancel by the journal entry ".$journalEntryCodeForComment." due to bounced cheque";
        } else {
            $comment = $paymentDetails->incomingPaymentMemo .", Journal entry of this payment is cancel by the journal entry ".$journalEntryCodeForComment." due to bounced cheque";
        }

        $commentData = array(
            'incomingPaymentMemo' => $comment
        );
        $paymentUpdateResult = $this->CommonTable('Invoice\Model\PaymentsTable')->update($commentData, $paymentId);

        return ['status' => true];
    }
    
    /**
    * this function use to arrange data for journal entry
    * @param array $accountsSet
    * @param int $locationID
    * @param string $subJEmsg
    * @param string $mainJEMsg
    * @param int $docID
    * @param int $docTypeID
    * return array
    **/
    public function setDataToJournalEntrySaveArray($accountsSet, $locationID, $subJEmsg, $mainJEMsg, $docID, $docTypeID, $journalEntryDate, $ignoreBudgetLimit)
    {
        //set jornal entry data set
        $increment = 0;
        $journalEntryAccounts = array();

        foreach ($accountsSet as $accID => $accValues) {
            $journalEntryAccounts[$increment]['fAccountsIncID'] = $increment;
            $journalEntryAccounts[$increment]['financeAccountsID'] = $accID;
            $journalEntryAccounts[$increment]['financeGroupsID'] = '';
            $journalEntryAccounts[$increment]['journalEntryAccountsDebitAmount'] = ($accValues['debitAmount']) ? $accValues['debitAmount'] : 0;
            $journalEntryAccounts[$increment]['journalEntryAccountsCreditAmount'] = ($accValues['creditAmount']) ? $accValues['creditAmount'] : 0;
            $journalEntryAccounts[$increment]['journalEntryAccountsMemo'] = $subJEmsg;
            $increment++;
        }

        //get journal entry reference number.
        $jeresult = $this->getReferenceNoForLocation('30', $locationID);
        $jelocationReferenceID = $jeresult['locRefID'];
        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

        $journalEntryData = array(
            'journalEntryAccounts' => $journalEntryAccounts,
            'journalEntryDate' => $this->convertDateToStandardFormat($journalEntryDate),
            'journalEntryCode' => $JournalEntryCode,
            'journalEntryTypeID' => '',
            'journalEntryIsReverse' => 0,
            'journalEntryComment' => $mainJEMsg,
            'documentTypeID' => $docTypeID,
            'journalEntryDocumentID' => $docID,
            'ignoreBudgetLimit' => $ignoreBudgetLimit,
        );
        return $journalEntryData;
    }



    public function getAllRelatedDocumentDetailsByPaymentIDAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $paymentID = $request->getPost('paymentID');
            $getpayment = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByPaymentID($paymentID)->current();
            $getpaymentInvoice = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getInvoicePaymentByPaymentId($paymentID);
       
            $dataExistsFlag = false;
            if ($getpayment) {
                $paymentData = array(
                    'type' => 'CustomerPayment',
                    'documentID' => $getpayment['incomingPaymentID'],
                    'code' => $getpayment['incomingPaymentCode'],
                    'amount' => number_format($getpayment['incomingPaymentAmount'], 2),
                    'issuedDate' => $getpayment['incomingPaymentDate'],
                    'created' => $getpayment['createdTimeStamp'],
                );
                $paymentsDetails[] = $paymentData;
                $dataExistsFlag = true;
            }

            if (isset($getpaymentInvoice)) {
                foreach ($getpaymentInvoice as $invDta) {
                    if (empty($invDta['incomeID']) && !empty($invDta['salesInvoiceID'])) {
                        $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBasicDetailsByInvoiceId($invDta['salesInvoiceID']);

                        $payInvDta = array(
                            'type' => 'SalesInvoice',
                            'documentID' => $invoiceData['salesInvoiceID'],
                            'code' => $invoiceData['salesInvoiceCode'],
                            'amount' => number_format($invoiceData['salesinvoiceTotalAmount'], 2),
                            'issuedDate' => $invoiceData['salesInvoiceIssuedDate'],
                            'created' => $invoiceData['createdTimeStamp'],
                        );

                    } elseif (!empty($invDta['incomeID']) && empty($invDta['salesInvoiceID'])) {
                        
                        $incomeData = $this->CommonTable('Accounting\Model\IncomeTable')->getIncomeBasicsByIncomeID($invDta['incomeID']);
                        $payInvDta = array(
                            'type' => 'Income',
                            'documentID' => $incomeData['incomeID'],
                            'code' => $incomeData['incomeCode'],
                            'amount' => number_format($incomeData['incomeTotal'], 2),
                            'issuedDate' => $incomeData['incomeDate'],
                            'created' => $incomeData['createdTimeStamp'],
                        );
                    }
                    $paymentsDetails[] = $payInvDta;
                }
            }
             
            $sortData = Array();
            foreach ($paymentsDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $paymentsDetails);

            $documentDetails = array(
                'paymentsDetails' => $paymentsDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }
    
    public function retriveCustomerWiseUndepositChequesAction() {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $customerID = $request->getPost('customerID');
            $data = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getUndepositedChequesByCustomer($customerID, $locationID);


            $view = new ViewModel(array('cheques' => $data, 'companyCurrencyId' => $this->companyCurrencyId, 'companyCurrencySymbol' => $this->companyCurrencySymbol, 'form'=>$form));

            $view->setTerminal(TRUE);
            $view->setTemplate('expenses/cheque-deposit/view-list');

            return $view;
        }
    }

    public function retrivePaymentWiseUndepositChequesAction() {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $paymentID = $request->getPost('paymentID');
            $data = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getUndepositedChequesByPayment($paymentID, $locationID);


            $view = new ViewModel(array('cheques' => $data, 'companyCurrencyId' => $this->companyCurrencyId, 'companyCurrencySymbol' => $this->companyCurrencySymbol, 'form'=>$form));

            $view->setTerminal(TRUE);
            $view->setTemplate('expenses/cheque-deposit/view-list');

            return $view;
        }
    }

    public function retriveInvoiceWiseUndepositChequesAction() {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $invoiceID = $request->getPost('invoiceID');
            $data = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getUndepositedChequesByInvoice($invoiceID, $locationID);


            $view = new ViewModel(array('cheques' => $data, 'companyCurrencyId' => $this->companyCurrencyId, 'companyCurrencySymbol' => $this->companyCurrencySymbol, 'form'=>$form));

            $view->setTerminal(TRUE);
            $view->setTemplate('expenses/cheque-deposit/view-list');

            return $view;
        }
    }
    public function retriveIncomeWiseUndepositChequesAction() {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $incomeID = $request->getPost('incomeID');
            $data = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getUndepositedChequesByIncome($incomeID, $locationID);


            $view = new ViewModel(array('cheques' => $data, 'companyCurrencyId' => $this->companyCurrencyId, 'companyCurrencySymbol' => $this->companyCurrencySymbol, 'form'=>$form));

            $view->setTerminal(TRUE);
            $view->setTemplate('expenses/cheque-deposit/view-list');

            return $view;
        }
    }

    public function retriveUndepositChequesByRefAction() {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $searchString = trim($request->getPost('searchString'));
            $data = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getUndepositedChequesByRef($searchString, $locationID);


            $view = new ViewModel(array('cheques' => $data, 'companyCurrencyId' => $this->companyCurrencyId, 'companyCurrencySymbol' => $this->companyCurrencySymbol, 'form'=>$form));

            $view->setTerminal(TRUE);
            $view->setTemplate('expenses/cheque-deposit/view-list');

            return $view;
        }
    }
}
