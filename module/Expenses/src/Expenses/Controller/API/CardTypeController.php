<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Expenses\Model\CardType;
use Zend\View\Model\ViewModel;

/**
 * Description of CardTypeController
 *
 * @author shermilan
 */
class CardTypeController extends CoreController
{

    public function saveAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->status = false;
            return $this->JSONRespond();
        }
        

        $postData = $this->getRequest()->getPost()->toArray();

        // validate postdata
        if (!$postData['cardTypeName'] || $postData['cardTypeName'] == '') {
            $this->msg = $this->getMessage('ERR_CARD_TYPE_NAME_EMPTY');
            $this->status = false;
            return $this->JSONRespond();
        }

        $existingCardType = $this->CommonTable('Expenses\Model\CardTypeTable')->getCardTypeByName($postData['cardTypeName']);

        // check duplication
        if ($existingCardType->current()) {
            $this->msg = $this->getMessage('ERR_CARD_TYPE_NAME_DUPLICATE');
            $this->status = false;
            return $this->JSONRespond();
        } 

        // add new Card Type
        $cardType = new CardType();
        $cardType->exchangeArray([
            'cardTypeName' => $postData['cardTypeName'],
            'entityID' => $this->createEntity()
        ]);

        $lastInsertedID = $this->CommonTable('Expenses\Model\CardTypeTable')->save($cardType);
        if (!$lastInsertedID) {
            $this->msg = $this->getMessage('ERR_CARD_TYPE_SAVE');
            $this->status = false;
            return $this->JSONRespond();
        } 

        $this->setLogMessage('Card type '.$postData['cardTypeName'].' is created.');
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_CARD_TYPE_SAVE');
        $this->html = $this->getCardTypeListHtml();
        
        return $this->JSONRespondHtml();
    }

    /**
     * Update the Card Type
     * @return view modal
     */
    public function updateAction()
    {


        if (!$this->getRequest()->isPost()) {
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->status = false;
            return $this->JSONRespond();
        }
        $postData = $this->getRequest()->getPost()->toArray();

        // validate postdata
        if (!$postData['cardTypeName'] || $postData['cardTypeName'] == '') {
            $this->msg = $this->getMessage('ERR_CARD_TYPE_NAME_EMPTY');
            $this->status = false;
            return $this->JSONRespond();
        }

        $existingCardType = $this->CommonTable('Expenses\Model\CardTypeTable')->getCardTypeByID($postData['cardTypeID'])->current();

        if (!$existingCardType) {
            $this->msg = $this->getMessage('ERR_CARD_TYPE_NOT_FOUND');
            $this->status = false;
            return $this->JSONRespond();
        } 

        // update card Type
        $cardType = new CardType();
        $cardType->exchangeArray([
            'cardTypeName' => $postData['cardTypeName'],
            'cardTypeID' => $existingCardType['cardTypeID']
        ]);

        try {
            $this->CommonTable('Expenses\Model\CardTypeTable')->update($cardType);
        } catch (Exception $e) {
            $this->msg = $this->getMessage('ERR_CARD_TYPE_UPDATE');
            $this->status = false;
            return $this->JSONRespond();              
        }
        $this->setLogMessage('Card type '.$existingCardType['cardTypeName'].' is updated to '.$postData['cardTypeName']);
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_CARD_TYPE_UPDATE');
        $this->html = $this->getCardTypeListHtml();

        return $this->JSONRespondHtml();
    }

    public function deleteAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->status = false;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();

        // check requested ID exists and used any where
        $existingCardType = $this->CommonTable('Expenses\Model\CardTypeTable')->getCardTypeByID($postData['cardTypeID'])->current();

        if (!(!isset($existingCardType['accountID']) || $existingCardType['accountID'] == 0)) {
            $this->msg = $this->getMessage('ERR_CARD_TYPE_DELETE_USED');
            $this->status = false;
            return $this->JSONRespond();
        }

        $isDeleted = $this->CommonTable('Expenses\Model\CardTypeTable')->delete($postData['cardTypeID']);
        
        if (!$isDeleted) {
            $this->msg = $this->getMessage('ERR_CARD_TYPE_DELETE');
            $this->status = false;
            return $this->JSONRespond();    
        }
        $this->setLogMessage('Card type '.$existingCardType['cardTypeName'].' is deleted.');
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_CARD_TYPE_DELETE');
        $this->html = $this->getCardTypeListHtml();

        return $this->JSONRespondHtml();
    }

    public function searchAction()
    {

        if (!$this->getRequest()->isPost()) {
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->status = false;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();

        if (!$postData['searchKey'] || $postData['searchKey'] == '') {
            $this->html = $this->getCardTypeListHtml();
            $this->status = TRUE;
            return $this->JSONRespondHtml();
        } 
          
        $cardTypes = $this->CommonTable('Expenses\Model\CardTypeTable')->search($postData['searchKey']);
        $view = new ViewModel(['cardTypes' => $cardTypes, 'paginated' => FALSE]);
        $view->setTerminal(true);
        $view->setTemplate('expenses/card-type/list');

        $this->html = $view;
        $this->status = TRUE;
        
        return $this->JSONRespondHtml();
    }

    /**
     * Get Card Type List Html
     * @return ViewModel
     */
    private function getCardTypeListHtml()
    {
        //get page number
        $page = $this->params('param1', 1);

        //get existing card types
        $paginator = $this->CommonTable('Expenses\Model\CardTypeTable')->fetchAll(true);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $view = new ViewModel(['cardTypes' => $paginator, 'paginated' => TRUE]);
        $view->setTerminal(true);
        $view->setTemplate('expenses/card-type/list');

        return $view;
    }

    public function getUnAssignedCardListAction()
    {

        if (!$this->getRequest()->isPost()) {
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->status = false;
            return $this->JSONRespond();
        }

        $cardTypes = $this->CommonTable('Expenses\Model\CardTypeTable')->getUnAssignedCardTypes();
        foreach ($cardTypes as $cardType) {
            $cardTypeList[$cardType['cardTypeID']] = $cardType['cardTypeName'];
        }

        $this->data = ($cardTypeList) ? $cardTypeList : [];
        $this->status = TRUE;

        return $this->JSONRespond();
    }

}
