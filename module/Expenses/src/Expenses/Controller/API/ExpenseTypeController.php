<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class ExpenseTypeController extends CoreController
{

    public function addAction()
    {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost()['expTypePostData'];
            $this->beginTransaction();
            $entityId = $this->createEntity();
            $expenseTypeData = array(
                'expenseTypeName' => $post['expTypeName'],
                'expenseTypeCategory' => $post['expTypeCat'],
                'expenseTypeApproverEnabled' => $post['approverEnabled'],
                'expenseTypeStatus' => $this->getStatusID('active'),
                'expenseTypeAccountID' => $post['expenseTypeAccountID'],
                'entityId' => $entityId
            );
            $expenseTypeModel = new \Expenses\Model\ExpenseType();
            $expenseTypeModel->exchangeArray($expenseTypeData);
            $savedExpenseTypeId = $this->CommonTable('Expenses\Model\ExpenseTypeTable')->create($expenseTypeModel);
            if ($savedExpenseTypeId) {
                if ($post['approverEnabled']) {
                    foreach ($post['expValueLimits'] as $expValueLimit) {
                        $expenseTypeLimitData = array(
                            'expenseTypeId' => $savedExpenseTypeId,
                            'expenseTypeLimitMin' => $expValueLimit['min'],
                            'expenseTypeLimitMax' => $expValueLimit['max']
                        );
                        $expensetypeLimitModel = new \Expenses\Model\ExpenseTypeLimit();
                        $expensetypeLimitModel->exchangeArray($expenseTypeLimitData);
                        $savedExpenseTypeLimitId = $this->CommonTable('Expenses\Model\ExpenseTypeLimitTable')->create($expensetypeLimitModel);
                        foreach ($expValueLimit['approvers'] as $approver) {
                            $expenseTypeLimitApproverData = array(
                                'expenseTypeLimitId' => $savedExpenseTypeLimitId,
                                'expenseTypeLimitApprover' => $approver,
                            );
                            $expenseTypeLimitApproverModel = new \Expenses\Model\ExpenseTypeLimitApprover();
                            $expenseTypeLimitApproverModel->exchangeArray($expenseTypeLimitApproverData);
                            $savedExpenseTypeLimitApproverId = $this->CommonTable('Expenses\Model\ExpenseTypeLimitApproverTable')->create($expenseTypeLimitApproverModel);
                        }
                    }
                    $this->commit();
                    $this->status = TRUE;
                    $this->msg = $this->getMessage('SUCC_EXPTYP_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                } else {
                    $this->commit();
                    $this->status = TRUE;
                    $this->msg = $this->getMessage('SUCC_EXPTYP_SAVE');
                    $this->flashMessenger()->addMessage($this->msg);
                }
            } else {
                $this->rollback();
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_EXPTYP_SAVE');
            }
            return $this->JSONRespond();
        }
    }

    public function editAction()
    {
        if ($this->getRequest()->isPost()) {
            $post = $this->getRequest()->getPost()['expTypePostData'];
            $this->beginTransaction();
            $editExpTypeId = $post['editId'];
            $expenseTypeData = array(
                'expenseTypeName' => $post['expTypeName'],
                'expenseTypeCategory' => $post['expTypeCat'],
                'expenseTypeApproverEnabled' => $post['approverEnabled'],
                'expenseTypeAccountID' => $post['expenseTypeAccountID'],
            );
            $isUpdated = $this->CommonTable('Expenses\Model\ExpenseTypeTable')->update($expenseTypeData, $editExpTypeId);

            if (!$isUpdated) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_EXPTYP_UPD');
                return $this->JSONRespond();
            }
            $expTypeData = $this->CommonTable('Expenses\Model\ExpenseTypeTable')->getExpenseTypeByID($editExpTypeId);
            $this->updateEntity($expTypeData['entityId']);
            //delete all approver limits and approvers belongs to old expTypeId and reinsert new data
            if ($post['approverEnabled']) {
                //deleting process
                $oldApproverLimits = $this->CommonTable("Expenses\Model\ExpenseTypeLimitTable")->getLimitsByTypeId($editExpTypeId);
                foreach ($oldApproverLimits as $limit) {
                    //deletes approvers for current limit
                    $approversDeleted = $this->CommonTable("Expenses\Model\ExpenseTypeLimitApproverTable")->deleteByExpTypeLimitId($limit['expenseTypeLimitId']);
                }
                //delete approve limits
                $limitsDeleted = $this->CommonTable("Expenses\Model\ExpenseTypeLimitTable")->deleteByExpTypeId($editExpTypeId);

                foreach ($post['expValueLimits'] as $expValueLimit) {
                    $expenseTypeLimitData = array(
                        'expenseTypeId' => $editExpTypeId,
                        'expenseTypeLimitMin' => $expValueLimit['min'],
                        'expenseTypeLimitMax' => $expValueLimit['max']
                    );
                    $expensetypeLimitModel = new \Expenses\Model\ExpenseTypeLimit();
                    $expensetypeLimitModel->exchangeArray($expenseTypeLimitData);
                    $savedExpenseTypeLimitId = $this->CommonTable('Expenses\Model\ExpenseTypeLimitTable')->create($expensetypeLimitModel);
                    foreach ($expValueLimit['approvers'] as $approver) {
                        $expenseTypeLimitApproverData = array(
                            'expenseTypeLimitId' => $savedExpenseTypeLimitId,
                            'expenseTypeLimitApprover' => $approver,
                        );
                        $expenseTypeLimitApproverModel = new \Expenses\Model\ExpenseTypeLimitApprover();
                        $expenseTypeLimitApproverModel->exchangeArray($expenseTypeLimitApproverData);
                        $savedExpenseTypeLimitApproverId = $this->CommonTable('Expenses\Model\ExpenseTypeLimitApproverTable')->create($expenseTypeLimitApproverModel);
                    }
                }
                $this->commit();
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_EXPTYP_UPD');
                $this->flashMessenger()->addMessage($this->msg);
            } else {

                //deleting process
                $oldApproverLimits = $this->CommonTable("Expenses\Model\ExpenseTypeLimitTable")->getLimitsByTypeId($editExpTypeId);
                foreach ($oldApproverLimits as $limit) {
                    //deletes approvers for current limit
                    $approversDeleted = $this->CommonTable("Expenses\Model\ExpenseTypeLimitApproverTable")->deleteByExpTypeLimitId($limit['expenseTypeLimitId']);
                }
                //delete approve limits
                $limitsDeleted = $this->CommonTable("Expenses\Model\ExpenseTypeLimitTable")->deleteByExpTypeId($editExpTypeId);

                $this->commit();
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_EXPTYP_UPD');
                $this->flashMessenger()->addMessage($this->msg);
            }

            return $this->JSONRespond();
        }
    }

    public function deleteAction()
    {
        if ($this->getRequest()->getPost()) {
            $expTypeId = $this->request->getPost('expTypeId');
            $expTypeData = $this->CommonTable('Expenses\Model\ExpenseTypeTable')->getExpenseTypeByID($expTypeId);
            if ($this->updateDeleteInfoEntity($expTypeData['entityId'])) {
                $this->msg = $this->getMessage('SUCC_EXPTYP_DEL');
                $this->status = TRUE;
            } else {
                $this->msg = $this->getMessage('ERR_EXPTYP_DEL');
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function changeExpenseTypeStatusAction()
    {
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $expenseTypeId = $postData['expenseTypeId'];
            $changeStatusTo = $this->getStatusID($postData['changeStatusTo']);
            if ($this->CommonTable('Expenses\Model\ExpenseTypeTable')->changeStatus($changeStatusTo, $expenseTypeId)) {
                $this->status = TRUE;
            } else {
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function getExpenseTypeBySearchAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $expenseTypeID = $searchrequest->getPost('searchKey');
            $expenseTypes = $this->CommonTable('Expenses/Model/ExpenseTypeTable')->searchExpenseTypeForDropDown($expenseTypeID);

            $expenseTypeList = array();
            if ($searchrequest->getPost('addFlag') == "expenseType") {
                // Merge GLAccounts to expenses list
                $accounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->searchAccountsForDropDown($searchrequest->getPost('searchKey'),true);
                foreach ($accounts as $acc) {
                    array_push($expenseTypeList, array(
                        'value' => "acc_" . $acc['financeAccountsID'],
                        'text' => $acc['financeAccountsCode'] . "_" . $acc['financeAccountsName']
                        )
                    );
                }
            } else {
                foreach ($expenseTypes as $expenseType) {
                    array_push($expenseTypeList, array(
                        'value' => "exp_" . $expenseType['expenseTypeId'],
                        'text' => $expenseType['expenseTypeName']
                        )
                    );
                }
            }

            $this->data = array('list' => $expenseTypeList);
            return $this->JSONRespond();
        }
    }
    public function getExpenseTypeWithFinanceAccountsBySearchAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $expenseTypeID = $searchrequest->getPost('searchKey');
            $expenseTypes = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->searchPettyCashExpenseTypeForDropDown($expenseTypeID);

            $expenseTypeList = array();
            foreach ($expenseTypes as $expenseType) {
                array_push($expenseTypeList, array(
                    'value' => "exp_" . $expenseType['pettyCashExpenseTypeID'],
                    'text' => $expenseType['pettyCashExpenseTypeName']
                    )
                );
            }

            // Merge GLAccounts to expenses list
            $accounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->searchAccountsForDropDown($searchrequest->getPost('searchKey'),true);
            foreach ($accounts as $acc) {
                array_push($expenseTypeList, array(
                    'value' => "acc_" . $acc['financeAccountsID'],
                    'text' => $acc['financeAccountsCode'] . "_" . $acc['financeAccountsName']
                    )
                );
            }

            $this->data = array('list' => $expenseTypeList);
            return $this->JSONRespond();
        }
    }

}
