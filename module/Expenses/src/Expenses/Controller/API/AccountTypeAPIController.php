<?php

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains AccountType API related actions
 */

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Expenses\Model\AccountType;
use Zend\View\Model\ViewModel;

class AccountTypeAPIController extends CoreController
{
    
    /**
     * Save AccoutType
     * @return JsonModel
     */
    public function addAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->html   = null;
        
        if($this->getRequest()->isPost()){
            
            $postData = $this->getRequest()->getPost()->toArray();
            
            if($postData['accountTypeName']){
                $accoutType = $this->CommonTable('Expenses\Model\AccountTypeTable')->getAccountTypeByAccountTypeName($postData['accountTypeName']);
                
                if(!$accoutType){
                    $accoutType = new AccountType();
                    $accoutType->exchangeArray($postData);                    
                    $result = $this->CommonTable('Expenses\Model\AccountTypeTable')->saveAccountType($accoutType,$this->createEntity());
                    
                    if($result){
                        $this->status = true;
                        $this->setLogMessage('Account Type '.$postData['accountTypeName'].' is created.');
                        $this->msg = $this->getMessage('SUCC_ACCOUNT_TYPE_ADD');
                        
                    } else {
                        $this->msg = $this->getMessage('ERR_ACCOUNT_TYPE_ADD');
                    }
                    
                } else {
                    $this->msg = $this->getMessage('ERR_ACCOUNT_TYPE_EXIST');
                }
            } else {
                $this->msg = $this->getMessage('ERR_ACCOUNT_TYPE_VALIDATION');
            }            
        }
        
        return $this->JSONRespond();
    }
    
    /**
     * Update AccountType
     * @return JsonModel
     */
    public function updateAccountTypeAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){
            
            $postData = $this->getRequest()->getPost()->toArray();
            
            if($postData['accountTypeId']){                
                $accountType = $this->CommonTable('Expenses\Model\AccountTypeTable')->getAccountTypeByAccountTypeId($postData['accountTypeId']);
                if($accountType){
                    //for check accounttype name already exist
                    $result = $this->CommonTable('Expenses\Model\AccountTypeTable')->checkAccountTypeNameValid($postData['accountTypeId'],$postData['accountTypeName']);
                    if($result){
                        $this->msg = $this->getMessage('ERR_ACCOUNT_TYPE_EXIST');
                    } else {
                        try {
                            $this->CommonTable('Expenses\Model\AccountTypeTable')->updateAccountType($postData,$postData['accountTypeId']);
                            $this->updateEntity($postData['entityId']);
                            $this->status = true;
                            $this->setLogMessage('Account Type '.$accountType['accountTypeName'].' is updated to '.$postData['accountTypeName'].'.');
                            $this->msg = $this->getMessage('SUCC_ACCOUNT_TYPE_UPDATE');
                        } catch (\Exception $exc) {
                            $this->msg = $exc->getMessage();
                        }
                    }                    
                } else {
                    $this->msg = $this->getMessage('ERR_ACCOUNT_TYPE_NOT_FOUND');
                }
            }
        }        
        return $this->JSONRespond();
    }
    
    /**
     * search AccoutType
     * @return ViewModel
     */
    public function searchAction()
    {
        if($this->getRequest()->isPost()){
            
            $key = $this->getRequest()->getPost('searchKey');
            
            $accountTypes = $this->CommonTable('Expenses\Model\AccountTypeTable')->searchAccountTypeByAccountTypeName($key,array('accountType.accountTypeName ASC'));
            
            $view = new ViewModel(array('accountTypes'=>$accountTypes));
            $view->setTerminal(true);
            $view->setTemplate('expenses/account-type-api/search');
            return $view;
        }
    }
    
    /**
     * get all AccountTypes
     * @return ViewModel
     */
    public function getAccountTypesAction()
    {       
        //get page number
        $page = $this->params('param1',1);
        //get bank list
        $paginator = $this->CommonTable('Expenses\Model\AccountTypeTable')->fetchAll(true,array('accountType.accountTypeId DESC'));        
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $view = new ViewModel(array('accountTypes'=>$paginator));
        $view->setTerminal(true);
        $view->setTemplate('expenses/account-type-api/get-account-types');
        return $view;
    }
    
    /**
     * Delete AccountType
     * @return JsonModel
     */
    public function deleteAccountTypeAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){    
            
            $accountTypeId = $this->getRequest()->getPost('accountTypeId');
            
            $accountType = $this->CommonTable('Expenses\Model\AccountTypeTable')->getAccountTypeByAccountTypeId($accountTypeId);
            if($accountType){
                //for check whether account type used for accounts
                $result = $this->CommonTable('Expenses\Model\AccountTable')->getAccountsByAccountTypeId($accountTypeId);
                if($result->count() == 0){
                    if($this->updateDeleteInfoEntity($accountType['entityId'])){
                        $this->status = true;
                        $this->setLogMessage('Account type '.$accountType['accountTypeName'].'is deleted.');
                        $this->msg = $this->getMessage('SUCC_ACCOUNT_TYPE_DELETE');
                    } else {
                        $this->msg = $this->getMessage('ERR_ACCOUNT_TYPE_DELETE_FAILED');
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_ACCOUNT_TYPE_DELETE');
                }
            } else {
                $this->msg = $this->getMessage('ERR_ACCOUNT_TYPE_NOT_FOUND');
            }            
        }
        
        return $this->JSONRespond();
    }
    
}
