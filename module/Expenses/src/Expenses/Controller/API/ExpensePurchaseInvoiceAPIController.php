<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Inventory\Form\SupplierForm;
use Inventory\Model\ProductBatch;
use Inventory\Model\ProductSerial;
use Expenses\Model\PaymentVoucherProduct;
use Expenses\Model\PaymentVoucherNonItemProduct;
use Expenses\Model\PaymentVoucher;
use Expenses\Model\PaymentVoucherProductTax;
use Inventory\Model\ItemIn;
use Inventory\Model\Supplier;
use Accounting\Model\JournalEntryAccounts;
use Inventory\Model\ItemOut;
use Accounting\Model\JournalEntry;

class ExpensePurchaseInvoiceAPIController extends CoreController
{
    private $productIds = [];

    /**
     * Use to save payment voucher data
     *  */
    public function savePiAction()
    { 
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $post = $request->getPost();
            $paymentVoucherCode = $post['piC'];
            $locationReferenceID = $post['lRID'];
            $locationID = $post['rL'];
            $dimensionData = $post['dimensionData'];
            $ignoreBudgetLimit = $post['ignoreBudgetLimit'];
            // check if a payment voucher code from the same payment voucher code exists if exist add next number to payment voucher code
            while ($paymentVoucherCode) {
                if ($this->CommonTable('Expenses\Model\PaymentVoucherTable')->checkPaymentVoucherByCode($paymentVoucherCode, $locationID)->current()) {
                    if ($locationReferenceID) {
                        $newPaymentVoucherCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newPaymentVoucherCode == $paymentVoucherCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $paymentVoucherCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $paymentVoucherCode = $newPaymentVoucherCode;
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('PAVOUCH_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }
            $itemFlag = false;
            $nonItemFlag = false;
            $itemFlagValue = 0;
            $entityID = $this->createEntity();
            if (count($post['pr']) > 0 && count($post['nIpr']) > 0) {
                $nonItemFlag = true;
                $itemFlag = true;
                $itemFlagValue = 3;
            } else if (count($post['nIpr']) > 0) {
                $nonItemFlag = true;
                $itemFlagValue = 2;
            } else if (count($post['pr']) > 0) {
                $itemFlag = true;
                $itemFlagValue = 1;
            }

            $this->beginTransaction();
            $hashValue = hash('md5', $paymentVoucherCode);
            $paymentVaucherData = array(
                'paymentVoucherCode' => $paymentVoucherCode,
                'paymentVoucherSupplierID' => (int)$post['sID'],
                'paymentVoucherSupplierReference' => $post['sR'],
                'paymentVoucherAccountID' => (int)$post['accountID'],
                'paymentVoucherLocationID' => $locationID,
                'paymentVoucherDueDate' => $this->convertDateToStandardFormat($post['dD']),
                'paymentVoucherIssuedDate' => $this->convertDateToStandardFormat($post['iD']),
                'paymentVoucherComment' => $post['cm'],
                'paymentVoucherDeliveryCharge' => $post['dC'],
                'paymentVoucherShowTax' => $post['sT'],
                'paymentVoucherTotal' => $post['fT'],
                'paymentTermID' => $post['pT'],
                'entityID' => $entityID,
                'paymentVoucherItemNotItemFlag' => $itemFlagValue,
                'paymentVoucherExpenseType' => $post['exTy'],
                'paymentVoucherHashValue' => $hashValue
            );
            $payVoucher = new PaymentVoucher();
            $payVoucher->exchangeArray($paymentVaucherData);
            $paymentVoucherID = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->savePaymentVoucher($payVoucher);
            
            if ($paymentVoucherID && $post['sID']) {
                $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($post['sID']);
                $tmpOutStanding = $supplierData->supplierOutstandingBalance + $post['fT'];
                $supplierDataArray = array(
                    'supplierOutstandingBalance' => $tmpOutStanding,
                    'supplierID' => $post['sID'],
                );
                $supplierDetails = new Supplier();
                $supplierDetails->exchangeArray($supplierDataArray);
                $updateCusOutstanding = $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplierOutStandingBalance($supplierDetails);
            }

            if ($paymentVoucherID) {
                 if ($nonItemFlag) {
                    for ($i = 0; $i < count($post['nIpr']); $i++) {
                        $paymentVoucherNonItemdetails = Array(
                            'paymentVoucherID' => $paymentVoucherID,
                            'paymentVoucherNonItemProductDiscription' => $post['nIpr'][$i]['description'],
                            'paymentVoucherNonItemProductQuantity' => $post['nIpr'][$i]['qty'],
                            'paymentVoucherNonItemProductUnitPrice' => $post['nIpr'][$i]['unitPrice'],
                            'paymentVoucherNonItemProductTotalPrice' => $post['nIpr'][$i]['totalPrice'],
                            'paymentVoucherNonItemProductExpenseTypeId' => $post['nIpr'][$i]['exType'],
                            'paymentVoucherNonItemProductExpenseTypeFormat' => $post['nIpr'][$i]['exFormat']
                        );
                        $paymentVoucherNonItem = new PaymentVoucherNonItemProduct();
                        $paymentVoucherNonItem->exchangeArray($paymentVoucherNonItemdetails);
                        $paymentVoucherNonItemID = $this->CommonTable('Expenses\Model\PaymentVoucherNonItemProductTable')->saveNonItemPayment($paymentVoucherNonItem);

                        if (sizeof($post['nIpr'][$i]['tax']) > 0) {
                            $addedTaxValue = 0;
                            foreach ($post['nIpr'][$i]['tax'] as $nonItmTxKy => $nonItmTxVlu) {
                                $taxData = $this->CommonTable('settings\Model\TaxTable')->getSimpleTax($nonItmTxVlu);
                               
                                $itemPrice = $post['nIpr'][$i]['qty'] * $post['nIpr'][$i]['unitPrice'];
                                if($taxData['taxType'] == 'v'){
                                    $taxValue = ($itemPrice * $taxData['taxPrecentage']) / 100;    
                                } else {
                                    $taxValue = (($itemPrice + $addedTaxValue) * $taxData['taxPrecentage']) / 100;
                                }

                                $nonItemTaxData = array(
                                    'paymentVoucherID' => $paymentVoucherID,
                                    'paymentVoucherProductIdOrNonItemProductID' => $paymentVoucherNonItemID,
                                    'paymentVoucherTaxID' => $nonItmTxVlu,
                                    'paymentVoucherTaxPrecentage' => $taxData['taxPrecentage'],
                                    'paymentVoucherTaxAmount' => $taxValue,
                                    'paymentVoucherTaxItemOrNonItemProduct' => 'nonItem',
                                );

                                $paymentVoucherTaxData = New PaymentVoucherProductTax();
                                $paymentVoucherTaxData->exchangeArray($nonItemTaxData);
                                $paymentVoucherProductTaxID = $this->CommonTable('Expenses\Model\PaymentVoucherProductTaxTable')->savePaymentVoucherProductTax($paymentVoucherTaxData);
                                $addedTaxValue = $taxValue;
                            }
                        }
                    }
                }

                $this->productIds = [];

                if ($itemFlag) {
                    for ($i = 0; $i < count($post['pr']); $i++) {
                        $this->productIds[] = $post['pr'][$i]['pID'];

                        $itemDiscount = 0;
                        if ($post['pr'][$i]['pDiscount']) {
                            $itemDiscount = ($post['pr'][$i]['pUnitPrice'] / 100) * $post['pr'][$i]['pDiscount'] * $post['pr'][$i]['pQuantity'];
                        }
                        $productType = $post['pr'][$i]['productType'];
                        $paymentVoucherProductID = array();
                        $paymentVoucherProductData = Array(
                            'paymentVoucherID' => $paymentVoucherID,
                            'locationProductID' => $post['pr'][$i]['locationPID'],
                            'paymentVoucherProductQuantity' => $post['pr'][$i]['pQuantity'],
                            'paymentVoucherProductUnitPrice' => $post['pr'][$i]['pUnitPrice'],
                            'paymentVoucherProductTotalPrice' => $post['pr'][$i]['pTotal'],
                            'paymentVoucherProductDiscount' => $itemDiscount,
                            'paymentVoucherProductExpenseTypeId' => $post['pr'][$i]['productExType'],
                            'uomID' => $post['pr'][$i]['pUom'],
                        );
                        if (sizeof($post['pr'][$i]['bProducts']) > 0) {
                            foreach ($post['pr'][$i]['bProducts'] as $key => $value) {
                                $paymentVoucherProductData['productBatch'] = 1;
                                $paymentVoucherProductData['paymentVoucherProductQuantityByBatch'] = $post['pr'][$i]['bProducts'][$key]['bQty'];
                                $batchSerialArray = array();
                                $batchSerialArray['batchData'] = $value;
                                if (sizeof($post['pr'][$i]['sProducts']) > 0) {
                                    $paymentVoucherProductData['productSerial'] = 1;
                                    foreach ($post['pr'][$i]['sProducts'] as $sKey => $sValue) {
                                        if ($sValue['sBCode'] == $key) {
                                            $batchSerialArray['serialData'][$sKey] = $sValue;
                                        }
                                    }
                                }
                                $btSealData = json_encode($batchSerialArray);
                                $paymentVoucherProductData['batchSerialData'] = $btSealData;

                                $paymentVoucherProduct = new PaymentVoucherProduct();
                                $paymentVoucherProduct->exchangeArray($paymentVoucherProductData);
                                $paymentVoucherProductID[] = $this->CommonTable('Expenses\Model\PaymentVoucherProductTable')->savePaymentVoucherProduct($paymentVoucherProduct);
                            }
                        } else {
                            $serialDataArray = array();
                            if (sizeof($post['pr'][$i]['sProducts']) > 0) {
                            
                                $paymentVoucherProductData['productSerial'] = 1;
                                $paymentVoucherProductData['productBatch'] = 0;
                                foreach ($post['pr'][$i]['sProducts'] as $sKey => $sValue) {
                                    $serialDataArray['serialData'][$sKey] = $sValue;
                                }
                                $serialData = json_encode($serialDataArray);
                            } else {
                                $paymentVoucherProductData['productSerial'] = 0;
                                $paymentVoucherProductData['productBatch'] = 0;
                                $serialData = 'No batch serial data';
                            }
                            $paymentVoucherProductData['batchSerialData'] = $serialData;

                            $paymentVoucherProduct = new PaymentVoucherProduct();
                            $paymentVoucherProduct->exchangeArray($paymentVoucherProductData);
                            $paymentVoucherProductID[] = $this->CommonTable('Expenses\Model\PaymentVoucherProductTable')->savePaymentVoucherProduct($paymentVoucherProduct);
                        }
                        if (isset($post['pr'][$i]['pTax']['tL']) && sizeof($post['pr'][$i]['pTax']['tL']) > 0) {
                            foreach ($post['pr'][$i]['pTax']['tL'] as $taxKey => $taxValue) {
                                foreach ($paymentVoucherProductID as $proIDKey => $proIDValue) {
                                    $taxData = array(
                                        'paymentVoucherID' => $paymentVoucherID,
                                        'paymentVoucherProductIdOrNonItemProductID' => $proIDValue,
                                        'paymentVoucherTaxID' => $taxKey,
                                        'paymentVoucherTaxPrecentage' => $taxValue['tP'],
                                        'paymentVoucherTaxAmount' => $taxValue['tA'],
                                        'paymentVoucherTaxItemOrNonItemProduct' => 'item',
                                    );

                                    $paymentVoucherTaxData = New PaymentVoucherProductTax();
                                    $paymentVoucherTaxData->exchangeArray($taxData);
                                    $paymentVoucherProductTaxID = $this->CommonTable('Expenses\Model\PaymentVoucherProductTaxTable')->savePaymentVoucherProductTax($paymentVoucherTaxData);
                                }
                            }
                        }
                    }
                }
                
                $approvers = $this->getApprovers($post['exTy'], $post['fT']);
                if (!empty($approvers)) {
                    $approversFlag = true;
                } else {
                    $result = $this->setDraftToTheItemIn($paymentVoucherID, $dimensionData, $ignoreBudgetLimit);
                    if(!$result['status']){
                        $this->rollback();
                        $this->status = FALSE;
                        $this->msg = $result['msg'];
                        $this->data = $result['data'];
                        return $this->JSONRespond();
                    }
                    $approversFlag = false;
                }
               
                $this->updateReferenceNumber($locationReferenceID);
                $this->commit();
                $this->setLogMessage('Expense payment voucher '.$paymentVoucherCode.' is created.');
                if (!empty($this->productIds)) {
                    $eventParameter = ['productIDs' => array_unique($this->productIds), 'locationIDs' => [$locationID]];
                    $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);
                }

                $this->status = true;
                $this->msg = $this->getMessage('EXPENSE_PAY_SUCCSESS');
                $this->data = array('pVID' => $paymentVoucherID, 'approversFlag' => $approversFlag, 'paymentVoucherCode' => $paymentVoucherCode, 'approver' => $approvers, 'hashValue' => $hashValue);
            } else {
                $this->rollback();
                $this->status = FALSE;
                $this->data = null;
            }

            return $this->JSONRespond();
        }
    }

    /**
     * use to find reference numbers that related to the payment voucher
     */
    public function getExpensePurchaseInvoiceReferenceForLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $refData = $this->getReferenceNoForLocation(26, $locationID);
            $rid = $refData["refNo"];
            $lrefID = $refData["locRefID"];
            if ($rid == '' || $rid == NULL) {
                if ($lrefID == null) {
                    $this->msg = $this->getMessage('ERR_PURINV_REFLOCATION');
                } else {
                    $this->msg = $this->getMessage('ERR_PURINV_REFMAX');
                }
                $this->data = FALSE;
                $this->status = false;
            } else {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $rid,
                    'locRefID' => $lrefID,
                );
            }
            return $this->JSONRespond();
        }
    }

    public function getPVsForSearchAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $dateFormat = $this->getUserDateFormat();
            $statuses = $globaldata[
                    'statuses'];
            if ($request->getPost('searchKey') != '') {
                $pvList = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchersforSearch($request->getPost('searchKey'));
                $pvListView = new ViewModel(array('paymentVoucherList' => $pvList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
            } else {
                $this->paginator = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchers(true);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $pvListView = new ViewModel(array('paymentVoucherList' => $this->paginator, 'statuses' => $statuses));
            }
            $pvListView->setTemplate('expenses/expense-purchase-invoice/payment-voucher-list');
            $this->html = $pvListView;
            return $this->JSONRespondHtml();
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param string toEmail,string subject,string body,string documentID
     * This function send a email with attached PI to the passed address
     */
    public function sendPVEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Expense Payment Voucher';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_EXP_PV_EMAIL');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_EXP_PV_EMAIL', array($documentType));
        return $this->JSONRespond();
    }

    public function sendApproverEmailAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pvId = $request->getPost('pvId');
            $pvCode = $request->getPost('pvCode');
            $approvers = $request->getPost('approvers');
            $token = $request->getPost('token');
            if ($pvId && $pvCode && $approvers && $token) {
                $this->sendPaymentVoucherApproversEmail($pvId, $pvCode, $approvers, $token);
                $this->msg = $this->getMessage('SUCC_EXP_SEND_FOR_WF');
                $this->status = true;
            } else {
                $this->msg = $this->getMessage('ERR_EXP_SEND_FOR_WF');
                $this->status = false;
            }
        }
        return $this->JSONRespond();
    }

    public function getApprovers($typeID, $totalPrice)
    {
        $approvers = $this->CommonTable('Expenses\Model\ExpenseTypeTable')->getApproversByTypeIDAndPriceRange($typeID, $totalPrice);
        $approversArray = array();
        $i = 0;
        foreach ($approvers as $apr) {
            $approversArray[$i]['expenseTypeId'] = $apr['expenseTypeId'];
            $approversArray[$i]['expenseTypeName'] = $apr['expenseTypeName'];
            $approversArray[$i]['expenseTypeCategory'] = $apr['expenseTypeCategory'];
            $approversArray[$i]['expenseTypeLimitApproverID'] = $apr['expenseTypeLimitApprover'];
            $approversArray[$i]['ApproverFirstName'] = $apr['firstName'];
            $approversArray[$i]['ApproverLastName'] = $apr['lastName'];
            $approversArray[$i]['ApproverEmail'] = $apr['email'];
            $i++;
        }
        return $approversArray;
    }

    private function setDraftToTheItemIn($paymentVoucherID, $dimensionData, $ignoreBudgetLimit = false)
    {
        $paymentVoucherData = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherDetailsByPVID($paymentVoucherID);

        $pavmentVoucherDetails = array();
        $pavmentVoucherNonItemTaxDetails = array();
        $pavmentVoucherItemTaxDetails = array();
        $productType = NULL;

        $accountProduct = array();
        $accountsPayableAmount = 0;
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountFlag = (count($glAccountExist) > 0)? true :false;
        $glAccountsData = $glAccountExist->current();
        $deliveryCharge = 0;
        foreach ($paymentVoucherData as $row) {
            if ($row['paymentVoucherDeliveryCharge'] != 0 && $deliveryCharge == 0) {
                $deliveryCharge = $row['paymentVoucherDeliveryCharge'];
            }
            
            $supplierID = $row['paymentVoucherSupplierID'];
            $glAccountID = $row['paymentVoucherAccountID'];
            $locationID = $row['paymentVoucherLocationID'];
            $date = $row['paymentVoucherIssuedDate'];
            $uomConversionValue = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($row['productID'],$row['paymentVoucherProductUomID']);
            $productData = $this->CommonTable('Inventory\Model\LocationProductTable')->getProductsByLocationProductID($row['locationProductID']);

            $pavmentVoucherDetails['pVID'] = $paymentVoucherID;
            $pavmentVoucherDetails['pVCode'] = $row['paymentVoucherCode'];

            if ($pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductID'] != $row['paymentVoucherProductID']) {
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductID'] = $row['paymentVoucherProductID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['locationProductID'] = $row['locationProductID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductQuantity'] = $row['paymentVoucherProductQuantity'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductDiscount'] = $row['paymentVoucherProductDiscount']/$row['paymentVoucherProductQuantity'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductUnitPrice'] = $row['paymentVoucherProductUnitPrice'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductTotalPrice'] = $row['paymentVoucherProductTotalPrice'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductQuantityByBatch'] = $row['paymentVoucherProductQuantityByBatch'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['batchSerialData'] = $row['batchSerialData'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productBatch'] = $row['productBatch'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productSerial'] = $row['productSerial'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productID'] = $row['productID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productCode'] = $row['productCode'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productName'] = $row['productName'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['uomID'] = $row['uomID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productUomConversion'] = $uomConversionValue;
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['uomAbbr'] = $row['uomAbbr'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productType'] = $productData->productTypeID;

                $paymentVoucherPTaxData = $this->CommonTable('Expenses\Model\PaymentVoucherProductTaxTable')->getPaymentVoucherProductTaxByPVPID($row['paymentVoucherProductID'],'item');
                if(count($paymentVoucherPTaxData) > 0){
                    foreach ($paymentVoucherPTaxData as $tkey => $tvalue) {
                      $pavmentVoucherItemTaxDetails[$row['paymentVoucherProductID']][$tvalue['paymentVoucherProductTaxID']]['paymentVoucherProductTaxID'] = $tvalue['paymentVoucherProductTaxID'];
                      $pavmentVoucherItemTaxDetails[$row['paymentVoucherProductID']][$tvalue['paymentVoucherProductTaxID']]['taxID'] = $tvalue['paymentVoucherTaxID'];
                      $pavmentVoucherItemTaxDetails[$row['paymentVoucherProductID']][$tvalue['paymentVoucherProductTaxID']]['taxPrecentage'] = $tvalue['paymentVoucherTaxPrecentage'];
                      $pavmentVoucherItemTaxDetails[$row['paymentVoucherProductID']][$tvalue['paymentVoucherProductTaxID']]['taxAmount'] = $tvalue['paymentVoucherTaxAmount'];
                    }
                    $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['tax'] = $pavmentVoucherItemTaxDetails[$row['paymentVoucherProductID']];                      
                }
            }

            if($pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductID'] != $row['paymentVoucherNonItemProductID']){

                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductID'] = $row['paymentVoucherNonItemProductID'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductQuantity'] = $row['paymentVoucherNonItemProductQuantity'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductUnitPrice'] = $row['paymentVoucherNonItemProductUnitPrice'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductTotalPrice'] = $row['paymentVoucherNonItemProductTotalPrice'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductExpenseTypeId'] = $row['paymentVoucherNonItemProductExpenseTypeId'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductExpenseTypeFormat'] = $row['paymentVoucherNonItemProductExpenseTypeFormat'];
                
                $paymentVoucherNonPTaxData = $this->CommonTable('Expenses\Model\PaymentVoucherProductTaxTable')->getPaymentVoucherProductTaxByPVPID($row['paymentVoucherNonItemProductID'], 'nonItem');
                
                if(count($paymentVoucherNonPTaxData) > 0){
                    foreach ($paymentVoucherNonPTaxData as $ntkey => $ntvalue) {
                        
                      $pavmentVoucherNonItemTaxDetails[$row['paymentVoucherNonItemProductID']][$ntvalue['paymentVoucherProductTaxID']]['paymentVoucherProductTaxID'] = $ntvalue['paymentVoucherProductTaxID'];
                      $pavmentVoucherNonItemTaxDetails[$row['paymentVoucherNonItemProductID']][$ntvalue['paymentVoucherProductTaxID']]['taxID'] = $ntvalue['paymentVoucherTaxID'];
                      $pavmentVoucherNonItemTaxDetails[$row['paymentVoucherNonItemProductID']][$ntvalue['paymentVoucherProductTaxID']]['taxPrecentage'] = $ntvalue['paymentVoucherTaxPrecentage'];
                      $pavmentVoucherNonItemTaxDetails[$row['paymentVoucherNonItemProductID']][$ntvalue['paymentVoucherProductTaxID']]['taxAmount'] = $ntvalue['paymentVoucherTaxAmount'];
                    }
                    
                    $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['tax'] = $pavmentVoucherNonItemTaxDetails[$row['paymentVoucherNonItemProductID']];                      
                }

            }
        }

        $supplierPayableAccountIDorGLAccountID = $glAccountID;
        if($glAccountFlag && !empty($supplierID)){
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($supplierID);
            if(empty($supplierData->supplierPayableAccountID)){
                    $status = false;
                    $msg = $this->getMessage('ERR_PURINV_SUPPLIER_PAYABLE_ACCOUNT', array($supplierData->supplierName.' - '.$supplierData->supplierCode));
                    return array('status' => $status, 'msg' => $msg);
            }
            $supplierPayableAccountIDorGLAccountID = $supplierData->supplierPayableAccountID;
        }else if($glAccountFlag && empty($glAccountID)){
            $status = false;
            $msg = $this->getMessage('ERR_SELECT_PI_ACCOUNT');
            return array('status' => $status, 'msg' => $msg);
        }
        
        if ($pavmentVoucherDetails['itemProduct']) {
            foreach ($pavmentVoucherDetails['itemProduct'] as $key => $value) {
                if($value['productType'] != 2){
                    $productBatchId = null;
                    $productSerialId = null;
                    $locationPID = $value['locationProductID'];
                    //get location product data for update average costing.
                    $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                    $locationProductQuantity = $locationProductData->locationProductQuantity;
                    $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

                    $itemInArray = array(
                        'itemInDocumentType' => 'Expense Payment Voucher',
                        'itemInDocumentID' => $pavmentVoucherDetails['pVID'],
                        'itemInLocationProductID' => $locationPID,
                        'itemInPrice' => $value['paymentVoucherProductUnitPrice']/$value['productUomConversion'],
                        'itemInDiscount' => $value['paymentVoucherProductDiscount']/$value['productUomConversion'],
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                        );

                    if ($value['productBatch'] == 1 || $value['productSerial'] == 1) {
                        $batchSerialData = json_decode($value['batchSerialData']);
                        if ($value['productBatch'] == 1) {
                            $temp = $batchSerialData->batchData;
                            $batchArray = array(
                                'locationProductID' => $locationPID,
                                'productBatchCode' => $temp->bCode,
                                'productBatchExpiryDate' => $this->convertDateToStandardFormat($temp->eDate),
                                'productBatchWarrantyPeriod' => $temp->warnty,
                                'productBatchManufactureDate' => $this->convertDateToStandardFormat($temp->mDate),
                                'productBatchQuantity' => $value['paymentVoucherProductQuantityByBatch'],
                                );
                            $batchData = new ProductBatch();
                            $batchData->exchangeArray($batchArray);
                            $productBatchId = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchData);
                        }
                        if ($value['productSerial'] == 1) {
                            $temp = $batchSerialData->serialData;
                            foreach ($temp as $value2) {
                                $serialArray = array(
                                    'locationProductID' => $locationPID,
                                    'productBatchID' => $productBatchId,
                                    'productSerialCode' => $value2->sCode,
                                    'productSerialExpireDate' => $this->convertDateToStandardFormat($value2->sEdate),
                                    'productSerialWarrantyPeriod' => $value2->sWarranty,
                                    'productSerialWarrantyPeriodType' => $value2->sWarrantyType,
                                    );
                                $serialData = new ProductSerial();
                                $serialData->exchangeArray($serialArray);
                                $productSerialId = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialData);
                                $itemInArray['itemInBatchID'] = $productBatchId;
                                $itemInArray['itemInSerialID'] = $productSerialId;
                                $itemInArray['itemInQty'] = '1';

                                $itemInData = new ItemIn();
                                $itemInData->exchangeArray($itemInArray);
                                $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInData);

                                //update location product
                                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                $newPQtyData = array(
                                    'locationProductQuantity' => $newPQty
                                    );
                                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                            }
                        } else {
                            $itemInArray['itemInBatchID'] = $productBatchId;
                            $itemInArray['itemInQty'] = $value['paymentVoucherProductQuantityByBatch'];

                            $itemInData = new ItemIn();
                            $itemInData->exchangeArray($itemInArray);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInData);

                            //update location product
                            $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                            $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($value['paymentVoucherProductQuantityByBatch']);
                            $newPQtyData = array(
                                'locationProductQuantity' => $newPQty
                                );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        }
                    } else {
                        $itemInArray['itemInQty'] = $value['paymentVoucherProductQuantity'];
                        $itemInData = new ItemIn();
                        $itemInData->exchangeArray($itemInArray);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInData);

                        //update location product
                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($value['paymentVoucherProductQuantity']);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                            );
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                    }

                    $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
                    $locationPrTotalQty = 0;
                    $locationPrTotalPrice = 0;
                    $newItemInIds = array();
                    if(count($itemInData) > 0){
                        foreach ($itemInData as $inkey => $invalue) {
                            if($invalue->itemInID > $locationProductLastItemInID){
                                $newItemInIds[] = $invalue->itemInID; 
                                $remainQty = $invalue->itemInQty - $invalue->itemInSoldQty;
                                if($remainQty > 0){
                                    $itemInPrice = $remainQty*$invalue->itemInPrice;
                                    $itemInDiscount = $remainQty*$invalue->itemInDiscount;
                                    $locationPrTotalQty += $remainQty;
                                    $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                                }
                                $locationProductLastItemInID = $invalue->itemInID; 
                            }
                        }
                    }
                    $locationPrTotalQty += $locationProductQuantity;
                    $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                    if($locationPrTotalQty > 0){
                        $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                        $lpdata = array(
                            'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                            'locationProductLastItemInID' => $locationProductLastItemInID,
                            );
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                        foreach ($newItemInIds as $inID) {
                            $intemInData = array(
                                'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                );
                            $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                        }
                    }
                }
                if($glAccountFlag){
                //check whether Product Inventory Gl account id set or not
                    $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($value['productID']);
                    if(empty($pData['productInventoryAccountID'])){
                        $status = false;
                        $msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                        return array('status' => $status, 'msg' => $msg, 'data' => '');
                    }

                    $punitPrice = $value['paymentVoucherProductUnitPrice']/$value['productUomConversion'];
                    $punitDiscount = $value['paymentVoucherProductDiscount']/$value['productUomConversion'];
                    $productTotal = $value['paymentVoucherProductQuantity']*($punitPrice - $punitDiscount);
                    $accountsPayableAmount += $productTotal;

                    //set gl accounts for the journal entry
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                    }

                    if(isset($value['tax'])){
                        foreach ($value['tax'] as $tkey => $tvalue) {
                            $taxID = $tvalue['taxID']; 
                            $taxAmount = $tvalue['taxAmount']; 
                            $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxID);
                            if(empty($taxData['taxPurchaseAccountID'])){
                                $status = false;
                                $msg = $this->getMessage('ERR_TAX_PURCHASE_ACCOUNT', array($taxData['taxName']));
                                return array('status' => $status, 'msg' => $msg, 'data' => '');
                            }

                            //set gl accounts for the journal entry
                            if(isset($accountProduct[$taxData['taxPurchaseAccountID']]['debitTotal'])){
                                $accountProduct[$taxData['taxPurchaseAccountID']]['debitTotal'] += $taxAmount;
                            }else{
                                $accountProduct[$taxData['taxPurchaseAccountID']]['debitTotal'] = $taxAmount;
                                $accountProduct[$taxData['taxPurchaseAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$taxData['taxPurchaseAccountID']]['accountID'] = $taxData['taxPurchaseAccountID'];
                            }
                            $accountsPayableAmount += $taxAmount;
                        }
                    }
                }
            }
        } 

        if ($pavmentVoucherDetails['nonItemProduct'] && $glAccountFlag) {
            foreach ($pavmentVoucherDetails['nonItemProduct'] as $nikey => $nivalue) {
                $pTotal = $nivalue['paymentVoucherNonItemProductUnitPrice'] *$nivalue['paymentVoucherNonItemProductQuantity'];
                             
                if($nivalue['paymentVoucherNonItemProductExpenseTypeFormat'] == 'acc'){
                    // If paymentVoucherNonItemProductExpenseTypeFormat is acc then expenseTypeAccountID is the paymentVoucherNonItemProductExpenseTypeId
                    $expenseTypeData['expenseTypeAccountID'] = $nivalue['paymentVoucherNonItemProductExpenseTypeId'];
                }else{
                    $expenseTypeData = $this->CommonTable('Expenses\Model\ExpenseTypeTable')->getExpenseTypeByID($nivalue['paymentVoucherNonItemProductExpenseTypeId']);
                    if(empty($expenseTypeData['expenseTypeAccountID'])){
                        $status = false;
                        $msg = $this->getMessage('ERR_EXPENSE_TYPE_GL_ACCOUNT_NOT_SET', array($expenseTypeData['expenseTypeName']));
                        return array('status' => $status, 'msg' => $msg, 'data' => '');
                    }
                }


                // Set gl accounts for the journal entry
                if(isset($accountProduct[$expenseTypeData['expenseTypeAccountID']]['debitTotal'])){
                    $accountProduct[$expenseTypeData['expenseTypeAccountID']]['debitTotal'] += $pTotal;
                }else{
                    $accountProduct[$expenseTypeData['expenseTypeAccountID']]['debitTotal'] = $pTotal;
                    $accountProduct[$expenseTypeData['expenseTypeAccountID']]['creditTotal'] = 0.00;
                    $accountProduct[$expenseTypeData['expenseTypeAccountID']]['accountID'] = $expenseTypeData['expenseTypeAccountID'];
                }
                $accountsPayableAmount += $pTotal;

                if(isset($nivalue['tax'])){
                    foreach ($nivalue['tax'] as $nitkey => $nitvalue) {
                        $taxID = $nitvalue['taxID']; 
                        $taxAmount = $nitvalue['taxAmount']; 
                        $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxID);
                        if(empty($taxData['taxPurchaseAccountID'])){
                            $status = false;
                            $msg = $this->getMessage('ERR_TAX_PURCHASE_ACCOUNT', array($taxData['taxName']));
                            return array('status' => $status, 'msg' => $msg, 'data' => '');
                        }

                        //set gl accounts for the journal entry
                        if(isset($accountProduct[$taxData['taxPurchaseAccountID']]['debitTotal'])){
                            $accountProduct[$taxData['taxPurchaseAccountID']]['debitTotal'] += $taxAmount;
                        }else{
                            $accountProduct[$taxData['taxPurchaseAccountID']]['debitTotal'] = $taxAmount;
                            $accountProduct[$taxData['taxPurchaseAccountID']]['creditTotal'] = 0.00;
                            $accountProduct[$taxData['taxPurchaseAccountID']]['accountID'] = $taxData['taxPurchaseAccountID'];
                        }
                        $accountsPayableAmount += $taxAmount;
                    }
                }
            }
        }

        if($glAccountFlag){

            // set delivery charge
            if ($deliveryCharge > 0) {
                
                if(empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)){ 
                    $status = false;
                    $msg = $this->getMessage('ERR_DELIVARY_ACC_NOT_SET');
                    return array('status' => $status, 'msg' => $msg);
                }
                // set delivery chargers to the accounts
                if(isset($accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID])){
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['debitTotal'] += $deliveryCharge;
                }else{
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['debitTotal'] = $deliveryCharge;
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] = 0.00;
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['accountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                }

                $accountsPayableAmount += $deliveryCharge;

            }
             

            //set gl accounts for the journal entry
            if(isset($accountProduct[$supplierPayableAccountIDorGLAccountID]['creditTotal'])){
                $accountProduct[$supplierPayableAccountIDorGLAccountID]['creditTotal'] += $accountsPayableAmount;
            }else{
                $accountProduct[$supplierPayableAccountIDorGLAccountID]['creditTotal'] = $accountsPayableAmount;
                $accountProduct[$supplierPayableAccountIDorGLAccountID]['debitTotal'] = 0.00;
                $accountProduct[$supplierPayableAccountIDorGLAccountID]['accountID'] = $supplierPayableAccountIDorGLAccountID;
            }

            $j=0;
            $journalEntryAccounts = array();

            foreach ($accountProduct as $akey => $avalue) {
                $journalEntryAccounts[$j]['fAccountsIncID'] = $j;
                $journalEntryAccounts[$j]['financeAccountsID'] = $avalue['accountID'];
                $journalEntryAccounts[$j]['financeGroupsID'] = '';
                $journalEntryAccounts[$j]['journalEntryAccountsDebitAmount'] = $avalue['debitTotal'];
                $journalEntryAccounts[$j]['journalEntryAccountsCreditAmount'] = $avalue['creditTotal'];
                $journalEntryAccounts[$j]['journalEntryAccountsMemo'] = 'Created By Payment Voucher Create '.$pavmentVoucherDetails['pVCode'].'.';
                $j++;
            }

                //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $date,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Payment Voucher '.$pavmentVoucherDetails['pVCode'].'.',
                'documentTypeID' => 20,
                'journalEntryDocumentID' => $pavmentVoucherDetails['pVID'],
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );
            $resultData = $this->saveJournalEntry($journalEntryData,null,null,false,null, $locationID);
            if(!$resultData['status']){
                return array('status' => false, 'msg' => $resultData['msg'], 'data' => $resultData['data']);
            }

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($pavmentVoucherDetails['pVID'],20, 3);
            if(!$jEDocStatusUpdate['status']){
                return array(
                    'status'=> false,
                    'msg'=>$jEDocStatusUpdate['msg'],
                    );
            }

            if (!empty($dimensionData)) {
                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$pavmentVoucherDetails['pVCode']], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                if(!$saveRes['status']){
                    return array('status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']);
                }   
            } 
        }
        //update expense payment state
        $stateID = '3'; //this is open state
        $updateState = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->updatePaymentVoucherStatus($paymentVoucherID, $stateID);
        return array('status' => true, 'msg' => '');
    }

    public function getPaymentVoucherForDropDownAction()
    {
        $searchRequest = $this->getRequest();
        if ($searchRequest->isPost()) {
            $keyWord = $searchRequest->getPost('searchKey');
            $locationID = $searchRequest->getPost('locationID');
            if ($locationID == "" || $locationID == null) {
                $locationID = FALSE;
            }
            $paymentVoucherDetails = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchersforSearch($keyWord, $locationID);
            $pvList = array();
            foreach ($paymentVoucherDetails as $paymentVoucherDetail) {
                $temp['value'] = $paymentVoucherDetail['paymentVoucherID'];
                $temp['text'] = $paymentVoucherDetail['paymentVoucherCode'];
                $pvList[] = $temp;
            }

            $this->setLogMessage("Retrive payment Voucher list for dropdown.");
            $this->data = array('list' => $pvList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     * check Payment voucher
     */
    public function checkPaymentVoucherAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pVID = $request->getPost('pVID');
            $check = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherDetailsByPVID($pVID)->current();
            if ($check != '') {
                $this->status = TRUE;
            } else {
                $this->status = false;
            }
            $this->data = $check;
            $this->setLogMessage("Check payment voucher ".$check['paymentVoucherCode'].' status.');
            return $this->JSONRespond();
        }
    }

    public function approveAction()
    {
        $action = $_GET['action'];
        $pvId = $_GET['pvId'];
        $token = $_GET['token'];
        $pvDetails = $this->CommonTable("Expenses\Model\PaymentVoucherTable")->getPaymentVoucherDetailsByPVID($pvId)->current();
        $pvCode = $pvDetails['paymentVoucherCode'];
        $this->beginTransaction();
        if ($pvDetails['paymentVoucherHashValue'] == $token) {
            if ($action == 'approve') {
                $currentApproveStatus = $pvDetails['paymentVoucherApproved'];
                if ($currentApproveStatus == NULL) {
                    $result = $this->setDraftToTheItemIn($pvId);
                    if(!$result['status']){
                        $this->rollback();
                        echo $result['msg'];
                        exit();
                    }else{
                        $this->CommonTable("Expenses\Model\PaymentVoucherTable")->updateApprovedStatus($pvId, 1);
                        $this->commit();
                        echo "Payment Voucher $pvCode approved..!";
                        exit();
                    }
                } else {
                    if ($currentApproveStatus == "0") {
                        $this->rollback();
                        echo 'This expense payment voucher workflow is already rejected one you can not approve it again';
                        exit();
                    } else {
                        $this->rollback();
                        echo 'This expense payment voucher workflow is already aprroved one';
                        exit();
                    }
                }
            } else {
                $currentApproveStatus = $pvDetails['paymentVoucherApproved'];

                if ($currentApproveStatus == NULL) {
                   $this->CommonTable("Expenses\Model\PaymentVoucherTable")->updateApprovedStatus($pvId, 0, true);
               
                    $this->commit();
                    echo "Payment Voucher $pvCode rejected..!";
                    exit();
                } else {
                    if ($currentApproveStatus == "1") {
                        $this->rollback();
                        echo 'This expense payment voucher workflow is already approved one you can not reject it again';
                        exit();
                    } else {
                        $this->rollback();
                        echo 'This expense payment voucher workflow is already rjected one';
                        exit();
                    }
                }
            }
        } else {
            $this->rollback();
            echo 'Token Mismatch';
            exit();
        }
    }

    private function sendPaymentVoucherApproversEmail($pvId, $pvCode, $approvers, $token)
    {
        foreach ($approvers as $approver) {
            $approveUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/expense-purchase-invoice-api/approve/?pvId=' . $pvId . '&action=approve&token=' . $token;
            $rejectUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/expense-purchase-invoice-api/approve/?pvId=' . $pvId . '&action=reject&token=' . $token;
            $approvePvEmailView = new ViewModel(array(
                'name' => $approver['ApproverFirstName'] . ' ' . $approver['ApproverLastName'],
                'pvCode' => $pvCode,
                'approveLink' => $approveUrl,
                'rejectLink' => $rejectUrl,
            ));
            $approvePvEmailView->setTemplate('/core/email/exp-pi-approve');
            $renderedEmailHtml = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($approvePvEmailView);
            $documentType = 'Expense Payment Voucher';
            $this->sendDocumentEmail($approver['ApproverEmail'], "Request for Approval - Payment Voucher - $pvCode", $renderedEmailHtml, $documentType, $pvId);
        }
    }

    public function getPurchaseInvoiceViewByFilterAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $globaldata = $this->getServiceLocator()->get('config');
            $dateFormat = $this->getUserDateFormat();
            $statuses = $globaldata[
                    'statuses'];

            $supplierID = $request->getPost('supplierID');
            $pvID = $request->getPost('pvID');

            if ($supplierID != '' || $pvID != '') {
                $pvList = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchersView($supplierID, $pvID);
                $pvListView = new ViewModel(array('paymentVoucherList' => $pvList, 'statuses' => $statuses, 'paginated' => false, 'dateFormat' => $dateFormat));
            } else {
                $this->paginator = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchers(true);
                $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
                $this->paginator->setItemCountPerPage(8);
                $pvListView = new ViewModel(array('paymentVoucherList' => $this->paginator, 'statuses' => $statuses));
            }
            $pvListView->setTemplate('expenses/expense-purchase-invoice/payment-voucher-list');
            $this->html = $pvListView;
            return $this->JSONRespondHtml();
        }
    }

    public function deletePaymentVoucherAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        
        $paymentVoucherID = $request->getPost('pVID');
        $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
        
        //get payment voucher details that related to the given payment voucher ID
        $paymentVoucherDetails = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherById($paymentVoucherID);
        $paymentVoucherDetail = iterator_to_array($paymentVoucherDetails);

        $locationId = $paymentVoucherDetail[0]['paymentVoucherLocationID'];

        //before do any oparations check the status of this PV
        if(!($paymentVoucherDetail[0]['paymentVoucherStatus'] == 3 || $paymentVoucherDetail[0]['paymentVoucherStatus'] == 6)){
            $this->msg = $this->getMessage('ERR_PV_STATUS_DELETE');
            $this->status = false;
            return $this->JSONRespond();
        }
        
       
        /**
        * check this is inventory or non inventory pv
        * then paymentVoucherItemNotItemFlag = 2 mean non inventory pv
        */ 
        
        $this->beginTransaction();
        if($paymentVoucherDetail[0]['paymentVoucherItemNotItemFlag'] != '2'){
            $reverseInventoryItems = $this->reverseInventroryItemByPV($paymentVoucherID);
            
            if(!$reverseInventoryItems['status']){
                $this->rollback();
                $this->msg = $reverseInventoryItems['msg'];
                $this->status = false;
                return $this->JSONRespond();
           }
        }
        
        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(20,$paymentVoucherID);
        $dimensionData = [];
        foreach ($jEDimensionData as $value) {
            if (!is_null($value['journalEntryID'])) {
                $temp = [];
                $temp['dimensionTypeId'] = $value['dimensionType'];
                $temp['dimensionValueId'] = $value['dimensionValueID'];
                $dimensionData[$paymentVoucherDetail[0]['paymentVoucherCode']][] = $temp;
            }
        }

        // check, accounting use or not 
        $glAccountData = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if(sizeof($glAccountData) > 0 ){
            $updateJournalAccount = $this->journalEntryreverseByPV(
                    $paymentVoucherID, 
                    $paymentVoucherDetail[0]['paymentVoucherCode'],
                    $paymentVoucherDetail[0]['paymentVoucherLocationID'],
                    $dimensionData,
                    $ignoreBudgetLimit
                    );
            if(!$updateJournalAccount['status']){
                $this->rollback();
                $this->status =  false;
                $this->data = $updateJournalAccount['data'];
                $this->msg =  $this->getMessage('ERR_UPDATE_ACCOUNTS');
                return $this->JSONRespond();
            }

        }
        // update supplier outstanding when use supplier
        if($paymentVoucherDetail[0]['paymentVoucherSupplierID'] != ''){
            
            $supplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($paymentVoucherDetail[0]['paymentVoucherSupplierID']);
            
            $currentOutStanding = floatval($supplierDetails->supplierOutstandingBalance) - floatval($paymentVoucherDetail[0]['paymentVoucherTotal']);
            
            //update supplier
            $data = array(
                'supplierOutstandingBalance' => $currentOutStanding,
                );
            $updateSupplier = $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplier($data, $paymentVoucherDetail[0]['paymentVoucherSupplierID']);
        }
        
        // update pv status as a canceled. add 5 as a status id
        $statusID = 5;
        $updatePv = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->updatePaymentVoucherStatus($paymentVoucherID, $statusID);

        $this->commit();
        $this->setLogMessage('Expense payment voucher '.$paymentVoucherDetail[0]['paymentVoucherCode'].' is deleted.');
        if (!empty($this->productIds)) {
            $eventParameter = ['productIDs' => array_unique($this->productIds), 'locationIDs' => [$locationId]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);
        }

        $this->status = true;
        $this->msg = $this->getMessage('SUCC_DEL_PV');
        return $this->JSONRespond();
    }

    private function journalEntryreverseByPV($paymentVoucherID, $pvCode, $locationID, $dimensionData, $ignoreBudgetLimit)
    {
        /**
        * get journal entry details by given id and document id =20
        * because payment voucher document id is 20
        */
         
        $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('20', $paymentVoucherID);
        $journalEntryID = $journalEntryData['journalEntryID'];
        $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);                   
         
        $i=0;
        $journalEntryAccounts = array();
        foreach ($jEAccounts as $key => $value) {
            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
            $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Payment Voucher Delete '.$pvCode.'.';
            $i++;
        }

        //get journal entry reference number.
        $jeresult = $this->getReferenceNoForLocation('30', $locationID);
        $jelocationReferenceID = $jeresult['locRefID'];
        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);
        $pvDetailsforReverse = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherByPaymentVoucherID($paymentVoucherID);
        
        $journalEntryData = array(
            'journalEntryAccounts' => $journalEntryAccounts,
            'journalEntryDate' => $this->convertDateToStandardFormat($pvDetailsforReverse->paymentVoucherIssuedDate),
            'journalEntryCode' => $JournalEntryCode,
            'journalEntryTypeID' => '',
            'journalEntryIsReverse' => 0,
            'journalEntryComment' => 'Journal Entry is posted when cancel Payment Voucher '.$pvCode.'.',
            'documentTypeID' => 20,
            'journalEntryDocumentID' => $paymentVoucherID,
            'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

        $resultData = $this->saveJournalEntry($journalEntryData);

        if(!$resultData['status']){
           return $resultData;
           
        }

        $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($paymentVoucherID,20, 5);
        if(!$jEDocStatusUpdate['status']){
            return $jEDocStatusUpdate;
        }

        foreach ($dimensionData as $value) {
            if (!empty($value)) {
                $saveRes = $this->saveDimensionsForJournalEntry($value, $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                if(!$saveRes['status']){
                    return $saveRes;
                }   
            }
        }

        return ['status' => true];
    }

    private function reverseInventroryItemByPV($paymentVoucherID)
    {
           
        //check selected pv product sold or not
        $docType = "Expense Payment Voucher";
        $soldQty = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInSoldDetailsByDocumentTypeAndDocumentID($paymentVoucherID, $docType, true);
        
        if(sizeof($soldQty) > 0){
            foreach ($soldQty as $key => $itemInRecords) {
                
                if($itemInRecords['itemInBatchID'] != ''){
                    $validateValue = $this->validateBatchSerialForPVReverse($soldQty,$itemInRecords['itemInBatchID'], NULL);
                                            
                } else if($itemInRecords['itemInSerialID'] != ''){
                    $validateValue = $this->validateBatchSerialForPVReverse($soldQty, NULL, $itemInRecords['itemInSerialID']);
                } else {
                    $returnArray = array(
                        'msg' => $this->getMessage('ERR_PRODUCT_IN_USE'),
                        'status' => false,
                    );
                    $validateValue =  $returnArray;
                }
            }
        } else {
            $returnArray = array(
                'status' => true,
            );
            $validateValue =  $returnArray;
        }
        
        if(!$validateValue['status']){
            
            return $validateValue;
        }
        //update itemIn table itemOut table and locationProduct Tables    
        $state = $this->updateItemInOutAndLocationProductByGivenPV($paymentVoucherID, $docType);

        if(!$state){
           $returnArray = array(
            'status' => false,
            ); 
           return $returnArray;
        }
        
        $returnArray = array(
            'status' => true,
            );
        return $returnArray;
        
    }

    private function validateBatchSerialForPVReverse($soldQty, $batchID = NULL, $serialID = NULL)
    {
                 
        // get all sold qty by given batch/Serial ID in itemIn Table
        $soldQty = $this->CommonTable('Inventory\Model\ItemInTable')
            ->getSoldQtyByBatchOrSerialID($batchID, $serialID)->current();
       
        // get all return qty by given batch/Serial ID in ItemOut table
        $retunQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getReturnQtyByBatchOrSerialID($batchID, $serialID)->current();
      
        if(floatval($soldQty['totalQty']) > floatval($retunQty['totalQty'])){
            $returnArray = array(
                'msg' => $this->getMessage('ERR_PRODUCT_IN_USE'),
                'status' => false,
            );
            return $returnArray;
            break;
        }
        
        $returnArray = array(
            'status' => true,
        );
        
        return $returnArray;

    }

    private function updateItemInOutAndLocationProductByGivenPV($paymentVoucherID, $docType)
    {
        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInSoldDetailsByDocumentTypeAndDocumentID($paymentVoucherID, $docType, false);

        $this->productIds = [];

        foreach ($itemInDetails as $key => $singleInValue) {
            $this->productIds[] = $singleInValue['productID'];

            if($singleInValue['itemInBatchID'] != NULL && $singleInValue['itemInSerialID'] == NULL){
                $itemInBatchDetails = $this->CommonTable('Inventory\Model\ItemInTable')
                    ->getItemInDetailsByBatchIDOrSerialID($singleInValue['itemInBatchID']);

                foreach ($itemInBatchDetails as $key => $singleBatchDetails) {
                    $updateStatus = $this->addRecordToItemOutAndUpdateItemInAndLocationProduct($singleBatchDetails);
                    //update batch table for given batchID
                    if($updateStatus){
                        $remainValue = floatval($singleBatchDetails['itemInSoldQty']);
                        $data = array(
                            'productBatchQuantity' => $remainValue,
                            );
                        $updateBatchTable = $this->CommonTable('Inventory\Model\ProductBatchTable')
                                ->updateProductBatchQuantity($singleInValue['itemInBatchID'], $data);
                    }
                }
                
            } else if($singleInValue['itemInSerialID']){
                $itemInSerialDetails = $this->CommonTable('Inventory\Model\ItemInTable')
                    ->getItemInDetailsByBatchIDOrSerialID(null, $singleInValue['itemInSerialID']);
                $batchSerialQty = [];
                foreach ($itemInSerialDetails as $key => $singleSerialDetails) {
                    if($singleSerialDetails['itemInBatchID'] != ''){
                        $batchSerialQty[$singleSerialDetails['itemInBatchID']] ++;
                    }
                    $updateStatus = $this->addRecordToItemOutAndUpdateItemInAndLocationProduct($singleSerialDetails);
                    //update serial table for given batchID
                    if($updateStatus){
                        $data = array(
                            'productSerialSold' => 1,
                            );
                        $updateSerialTable = $this->CommonTable('Inventory\Model\ProductSerialTable')
                                ->updateProductSerialData($data, $singleInValue['itemInSerialID']);
                    }
                }

                if(!empty($batchSerialQty)){
                    foreach ($batchSerialQty as $key => $value) {
                        $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($key);
                        $newBatchQty = floatval($batchDetails->productBatchQuantity) - floatval($value);

                        //update batchTable
                        $data = array(
                            'productBatchQuantity' => $newBatchQty,
                            );
                        $update = $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($key, $data);
                    }
                }


            } else {
                $updateStatus = $this->addRecordToItemOutAndUpdateItemInAndLocationProduct($singleInValue);
            }
        }

        return true;
      
    }

    private function addRecordToItemOutAndUpdateItemInAndLocationProduct($dataSet)
    {
        $returnValue = floatval($dataSet['itemInQty']) - floatval($dataSet['itemInSoldQty']);
        if($returnValue != 0){
            $itemOutM = new ItemOut();
            $itemOutData = array(
                'itemOutDocumentType' => 'Payment Voucher Cancel',
                'itemOutDocumentID' => $dataSet['itemInDocumentID'],
                'itemOutLocationProductID' => $dataSet['itemInLocationProductID'],
                'itemOutBatchID' => $dataSet['itemInBatchID'],
                'itemOutSerialID' => $dataSet['itemInSerialID'],
                'itemOutItemInID' => $dataSet['itemInID'],
                'itemOutQty' => $returnValue,
                'itemOutPrice' => $dataSet['itemInPrice'],
                'itemOutAverageCostingPrice' => $dataSet['itemInPrice'],
                'itemOutDiscount' => $dataSet['itemInDiscount'],
                'itemOutDateAndTime' => $this->getGMTDateTime(),
                'itemOutDeletedFlag' => 1,
            );
            $itemOutM->exchangeArray($itemOutData);
            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
            
            //update itemIn table as all items sold.
            $updatedItemInDetails = array(
                'itemInSoldQty' => $dataSet['itemInQty'],
                'itemInDeletedFlag' => 1,
            );
            $itemInUpdateStatue = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($updatedItemInDetails, $dataSet['itemInID']);

            //calculate new avarageCosting
            $locationProDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($dataSet['itemInLocationProductID']);

            $existItemTotal = floatval($locationProDetails->locationProductQuantity) 
                    - floatval($returnValue);

            //update locationProductAvgCosting
            $data = array(
                'locationProductQuantity' => $existItemTotal
            );
            
            $udateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')
                        ->updateAvgCostingByLocationProductID($data, $dataSet['itemInLocationProductID']);  
            
            $updateAverageCosting = $this->calculateAvgCostAndOtherDetails($locationProDetails, $dataSet);
            if(!$updateAverageCosting['status']){
                $this->rollback();
                $returnBatchSerialState = array(
                    'msg' => $this->getMessage('ERR_UPDATE_AVERAGE_COSTING'),
                    'status' => false, 
                );
                return $returnBatchSerialState;
               
            }  
            
            return true; 
        }
        return false;   

        
    }

    public function getDataForEditAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }
        
        $paymentVoucherID = $request->getPost('pVID');
        $details = $this->getPaymentVoucherDetails($paymentVoucherID);

        $invoicePaymentAccountData = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentIDByPaymentVoucherID($paymentVoucherID);

        $hasLinkedPayments = false;

        if (sizeof($invoicePaymentAccountData) > 0) {
            $hasLinkedPayments = true;
        }

        $details['hasLinkedPayments'] = $hasLinkedPayments;


        if($details['pvTypeFlag'] != 2){
            $this->msg = $this->getMessage('ERR_PV_TYPE');
            $this->flashMessenger()->addMessage(['status'=>false,'msg'=>$this->msg]);
            $this->status = false;
            return $this->JSONRespond();
        } else if(!($details['pVSt'] == 3 || $details['pVSt'] == 6)) {
            $this->msg = $this->getMessage('ERR_PV_STATUS');
            $this->flashMessenger()->addMessage(['status'=>false,'msg'=>$this->msg]);
            $this->status = false;
            return $this->JSONRespond();
        } else {
            $this->status = true;
            $this->data = $details;
            return $this->JSONRespond();
        }

    }

    private function getPaymentVoucherDetails($pvID)
    {
        $data = $this->getDataForDocumentView(); // get comapny details
        
        $paymentVoucherData = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherDetailsByPVID($pvID);
        $pavmentVoucherDetails = array();
        $accountID = null;
        $pavmentVoucherCode = null;
        foreach ($paymentVoucherData as $row) {
            $accountID = $row['paymentVoucherAccountID'];
            $pavmentVoucherDetails['pVID'] = $row['paymentVoucherID'];
            $pavmentVoucherDetails['pVCd'] = $row['paymentVoucherCode'];
            $pavmentVoucherCode = $row['paymentVoucherCode'];
            $pavmentVoucherDetails['pVSName'] = $row['supplierTitle'] . ' ' . $row['supplierName'];
            $pavmentVoucherDetails['pVSAddress'] = $row['supplierAddress'];
            $pavmentVoucherDetails['pVSEmail'] = $row['supplierEmail'];
            $pavmentVoucherDetails['pVSID'] = $row['paymentVoucherSupplierID'];
            $pavmentVoucherDetails['pVSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
            $pavmentVoucherDetails['pVSR'] = $row['paymentVoucherSupplierReference'];
            $pavmentVoucherDetails['pVLocID'] = $row['paymentVoucherLocationID'];
            $pavmentVoucherDetails['pVRL'] = $row['locationCode'] . '-' . $row['locationName'];
            $pavmentVoucherDetails['pVIssueD'] = $this->convertDateToUserFormat($row['paymentVoucherIssuedDate']);
            $pavmentVoucherDetails['pVDD'] = $this->convertDateToUserFormat($row['paymentVoucherDueDate']);
            $pavmentVoucherDetails['pVDC'] = $row['paymentVoucherDeliveryCharge'];
            $pavmentVoucherDetails['pVT'] = $row['paymentVoucherTotal'];
            $pavmentVoucherDetails['pVPaidAmo'] = $row['paymentVoucherPaidAmount'];
            $pavmentVoucherDetails['pVSTax'] = $row['paymentVoucherShowTax'];
            $pavmentVoucherDetails['pVC'] = $row['paymentVoucherComment'];
            $pavmentVoucherDetails['paymentVoucherExpenseType'] = $row['paymentVoucherExpenseType'];
            $pavmentVoucherDetails['pVSt'] = $row['paymentVoucherStatus'];
            $pavmentVoucherDetails['userUsername'] = $row['userUsername'];
            $pavmentVoucherDetails['pvTypeFlag'] = $row['paymentVoucherItemNotItemFlag'];
            $pavmentVoucherDetails['createdTimeStamp'] = $this->getUserDateTime($row['createdTimeStamp']);
            if ($pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productID'] != $row['paymentVoucherProductID']) {
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductID'] = $row['paymentVoucherProductID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['locationProductID'] = $row['locationProductID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductQuantity'] = $row['paymentVoucherProductQuantity'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductUnitPrice'] = $row['paymentVoucherProductUnitPrice'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductTotalPrice'] = $row['paymentVoucherProductTotalPrice'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductQuantityByBatch'] = $row['paymentVoucherProductQuantityByBatch'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['batchSerialData'] = $row['batchSerialData'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productBatch'] = $row['productBatch'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productSerial'] = $row['productSerial'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productID'] = $row['productID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productCode'] = $row['productCode'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productName'] = $row['productName'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['uomID'] = $row['uomID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productUomConversion'] = $row['productUomConversion'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['uomAbbr'] = $row['uomAbbr'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['itemDiscount'] = $row['paymentVoucherProductDiscount'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductExpenseType'] = ($row['paymentVoucherProductExpenseType']) ? $row['paymentVoucherProductExpenseType'] : '-';
                if ($row['paymentVoucherTaxItemOrNonItemProduct'] == 'item' && $row['paymentVoucherProductIdOrNonItemProductID'] == $row['paymentVoucherProductID']) {
                    $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxID'] = $row['paymentVoucherTaxID'];
                    $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxPrecentage'] = $row['paymentVoucherTaxPrecentage'];
                    $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxAmount'] = $row['paymentVoucherTaxAmount'];
                    $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['tax'][$row['paymentVoucherTaxID']]['taxName'] = $row['taxName'];
                }
            }
            if ($pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['productID'] != $row['paymentVoucherNonItemProductID']) {
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['productID'] = $row['paymentVoucherNonItemProductID'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductDiscription'] = $row['paymentVoucherNonItemProductDiscription'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductQuantity'] = $row['paymentVoucherNonItemProductQuantity'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductUnitPrice'] = $row['paymentVoucherNonItemProductUnitPrice'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductTotalPrice'] = $row['paymentVoucherNonItemProductTotalPrice'];

                // if paymentVoucherNonItemProductFormat is "acc" then GLAccount name should be displayed
                if($row['paymentVoucherNonItemProductExpenseTypeFormat'] == 'acc'){
                    $acc = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAll(false, [$row['paymentVoucherNonItemProductExpenseTypeId']]);
                    $acc_data = $acc->current();
                    $exp_type = $acc_data['financeAccountsCode'] . '_' . $acc_data['financeAccountsName'];
                }else{
                    $exp_type = $row['paymentVoucherNonItemProductExpenseType'];
                }
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductExpenseType'] = ($exp_type) ? $exp_type : '-';
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductExpenseTypeID'] = $row['paymentVoucherNonItemProductExpenseTypeId'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductExpenseTypeFormat'] = $row['paymentVoucherNonItemProductExpenseTypeFormat'];
            }
            if ($row['paymentVoucherTaxItemOrNonItemProduct'] != 'item' && $row['paymentVoucherProductIdOrNonItemProductID'] == $row['paymentVoucherNonItemProductID']) {
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxID'] = $row['paymentVoucherTaxID'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxPrecentage'] = $row['paymentVoucherTaxPrecentage'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxAmount'] = $row['paymentVoucherTaxAmount'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['tax'][$row['paymentVoucherTaxID']]['taxName'] = $row['taxName'];
            }
        }
        $dataa = array_merge($data, $pavmentVoucherDetails);
        $paymentVoucherMethodData = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentsDetailsByPaymentVoucherID($pvID);
        $pvMethod="";
        foreach ($paymentVoucherMethodData as $key => $value) {
            if($value['paymentMethodID']==2){
                $pvMethod.=$value['outGoingPaymentMethodReferenceNumber']."(".$value['bankName'].")"."\r\n";
            }
        }
        //load account name to the data array
        if($accountID != null){
            $accountDetails = $this->CommonTable('Accounting\Model\FinanceAccountsTable')
                    ->getFinanceAccountBasicByAccountsID($accountID)->current();
            $dataa['supAccountID'] = $accountID;
            $dataa['supAccName'] = $accountDetails['financeAccountsCode'] . '_' . $accountDetails['financeAccountsName'];        
        }


        $dataa['check_and_bank'] = $pvMethod;
        $dataa['today_date'] = gmdate('Y-m-d');
        $dataa['currencySymbol'] = $this->companyCurrencySymbol;


        $dimensionData = [];
        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(20,$pvID);
        foreach ($jEDimensionData as $value) {
            if (!is_null($value['journalEntryID'])) {
                $temp = [];
                if($value['dimensionType'] == "job" || $value['dimensionType'] == "project") {
                    if ($value['dimensionType'] == "job") {
                        $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($value['dimensionValueID']);
                        $valueTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                        $dimensionTxt = "Job";
                    } else if ($value['dimensionType'] == "project") {
                        $proData = $this->CommonTable('Jobs\Model\ProjectTable')->getProjectByProjectId($value['dimensionValueID']);
                        $valueTxt = $proData->projectCode.'-'.$proData->projectName;
                        $dimensionTxt = "Project";
                    }

                    $temp['dimensionTypeId'] = $value['dimensionType'];
                    $temp['dimensionTypeTxt'] = $dimensionTxt;
                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                    $temp['dimensionValueTxt'] = $valueTxt;
                } else {
                    $temp['dimensionTypeId'] = $value['dimensionType'];
                    $temp['dimensionTypeTxt'] = $value['dimensionName'];
                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                    $temp['dimensionValueTxt'] = $value['dimensionValue'];
                }
                $dimensionData[$pavmentVoucherCode][$value['dimensionType']] = $temp;
            }
        }

        $uploadedAttachments = [];
        $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($pvID, 20);
        $companyName = $this->getSubdomain();
        $path = '/userfiles/' . md5($companyName) . "/attachments";

        foreach ($attachmentData as $value) {
            $temp = [];
            $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
            $temp['documentRealName'] = $value['documentRealName'];
            $temp['docLink'] = $path . "/" .$value['documentName'];
            $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
        }

        $dataa['dimensionData'] = $dimensionData;
        $dataa['uploadedAttachments'] = $uploadedAttachments;

        return $dataa;
    }

    public function updateAction()
    {
        $request = $this->getRequest();
        $post = $request->getPost();
        $this->beginTransaction();

        $existPvDetails = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherById($post['pVID'])->current();

        if ($post['linkedPayments'] == "true") {
            

            $eariliestPayment = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentIDByPaymentVoucherIDASC($_POST['pVID'])->current();

            if($eariliestPayment['outgoingPaymentDate'] < $this->convertDateToStandardFormat($post['iD'])){
                $this->rollback();
                $this->status =  false;
                $this->msg =  $this->getMessage('CANNOT_ENTER_BEFORE_DATE_FOR_ID');
                return $this->JSONRespond();
            }


            if ($existPvDetails['paymentVoucherIssuedDate'] != $this->convertDateToStandardFormat($post['iD'])) {
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('20', $post['pVID']);

                $jeData = [
                    'journalEntryDate' => $this->convertDateToStandardFormat($post['iD'])
                ];

                $updateJournalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntry($jeData, $journalEntryData['journalEntryID']);
                
            }



            $updateData = [
                'paymentVoucherComment' => $post['cm'],
                'paymentVoucherIssuedDate' => $this->convertDateToStandardFormat($post['iD']),
                'paymentVoucherDueDate' => $this->convertDateToStandardFormat($post['dD'])

            ];

            $updateDateAndComment = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->updatePaymentVoucherCommentAndDates($post['pVID'], $updateData);


            $updateData = [
                'pVID' => $post['pVID']
            ];
           
        } else {
            // check, accounting use or not 
            $glAccountData = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            if(sizeof($glAccountData) > 0 ){
                $updateJournalAccount = $this->journalEntryreverseByPV(
                        $post['pVID'], 
                        $post['piC'],
                        $post['rL'],
                        $post['dimensionData'],
                        $post['ignoreBudgetLimit']
                        );
                if(!$updateJournalAccount['status']){
                    $this->rollback();
                    $this->status =  false;
                    $this->data = $updateJournalAccount['data'];
                    $this->msg =  $this->getMessage('ERR_UPDATE_ACCOUNTS');
                    return $this->JSONRespond();
                }

            }

            if ($existPvDetails['paymentVoucherStatus'] == "4" || $existPvDetails['paymentVoucherStatus'] == "10") {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PV_STATUS');
                return $this->JSONRespond();
            }

            // update supplier outstanding when use supplier
            if($existPvDetails['paymentVoucherSupplierID'] != ''){
                
                $supplierDetails = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($existPvDetails['paymentVoucherSupplierID']);
                
                $currentOutStanding = floatval($supplierDetails->supplierOutstandingBalance) - floatval($existPvDetails['paymentVoucherTotal']);
                
                //update supplier
                $data = array(
                    'supplierOutstandingBalance' => $currentOutStanding,
                    );
                $updateSupplier = $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplier($data, $existPvDetails['paymentVoucherSupplierID']);
            }
            
            // update pv status as a replaced. add 10 as a status id
            $statusID = 10;
            $replasePvStatus = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->updatePaymentVoucherStatus($post['pVID'], $statusID);
            

            $updatePV = $this->updatePV($post, $existPvDetails);
            if(!$updatePV['status']){
                $this->rollback();
                $this->status =  false;
                $this->msg =  $updatePV['msg'];
                return $this->JSONRespond();  
            }

            $updateData = $updatePV['data'];
        }

        $changeArray = $this->getEXPVChageDetails($existPvDetails, $post);
        //set log details
        $previousData = json_encode($changeArray['previousData']);
        $newData = json_encode($changeArray['newData']);

        $this->setLogDetailsArray($previousData,$newData);
        $this->setLogMessage('Expense Payment Voucher '.$existPvDetails['paymentVoucherCode'].' is updated.');
        $this->commit();
        $this->data = $updateData;
        $this->status = true;
        $this->msg = $this->getMessage('EXPENSE_PAY_UPDATE_SUCCSESS');
        return $this->JSONRespond();



    }

    public function getEXPVChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Payment Voucher Code'] = $row['paymentVoucherCode'];
        $previousData['Payment Voucher Supplier Reference'] = $row['paymentVoucherSupplierReference'];
        $previousData['Payment Voucher Issued Date'] = $this->convertDateToStandardFormat($row['paymentVoucherIssuedDate']);
        $previousData['Payment Voucher Due Date'] = $this->convertDateToStandardFormat($row['paymentVoucherDueDate']);
        $previousData['Payment Voucher Comment'] = $row['paymentVoucherComment'];
        $previousData['Payment Voucher Delivery Charge'] = $row['paymentVoucherDeliveryCharge'];
        $previousData['Payment Voucher Total'] = number_format($row['paymentVoucherTotal'],2);
        $previousData['Location Name'] = $row['locationName'];

        $newData = [];
        $newData['Payment Voucher Code'] = $data['piC'];
        $newData['Payment Voucher Supplier Reference'] = $this->convertDateToStandardFormat($data['sR']);
        $newData['Payment Voucher Issued Date'] = $this->convertDateToStandardFormat($data['iD']);
        $newData['Payment Voucher Due Date'] = $data['dD'];
        $newData['Payment Voucher Comment'] = $data['cm'];
        $newData['Payment Voucher Delivery Charge'] = $data['dC'];
        $newData['Payment Voucher Total'] = number_format($data['fT'],2);
        $newData['Location Name'] = $row['locationName'];

        return array('previousData' => $previousData, 'newData' => $newData);
    }

    public function updatePV($pvData, $existPvDetails)
    {
        $paymentVoucherCode = $pvData['piC'];
        $locationReferenceID = $pvData['lRID'];
        $locationID = $pvData['rL'];
       
        $itemFlag = false;
        $nonItemFlag = false;
        $itemFlagValue = 0;
        $entityID = $this->createEntity();
        if (count($pvData['pr']) > 0 && count($pvData['nIpr']) > 0) {
            $nonItemFlag = true;
            $itemFlag = true;
            $itemFlagValue = 3;
        } else if (count($pvData['nIpr']) > 0) {
            $nonItemFlag = true;
            $itemFlagValue = 2;
        } else if (count($pvData['pr']) > 0) {
            $itemFlag = true;
            $itemFlagValue = 1;
        }

        $hashValue = hash('md5', $paymentVoucherCode);
        $paymentVaucherData = array(
            'paymentVoucherCode' => $paymentVoucherCode,
            'paymentVoucherSupplierID' => (int)$pvData['sID'],
            'paymentVoucherSupplierReference' => $pvData['sR'],
            'paymentVoucherAccountID' => (int)$pvData['accountID'],
            'paymentVoucherLocationID' => $locationID,
            'paymentVoucherDueDate' => $this->convertDateToStandardFormat($pvData['dD']),
            'paymentVoucherIssuedDate' => $this->convertDateToStandardFormat($pvData['iD']),
            'paymentVoucherComment' => $pvData['cm'],
            'paymentVoucherDeliveryCharge' => $pvData['dC'],
            'paymentVoucherShowTax' => $pvData['sT'],
            'paymentVoucherTotal' => $pvData['fT'],
            'paymentTermID' => $pvData['pT'],
            'entityID' => $entityID,
            'paymentVoucherItemNotItemFlag' => $itemFlagValue,
            'paymentVoucherExpenseType' => $pvData['exTy'],
            'paymentVoucherPaidAmount' => $existPvDetails['paymentVoucherPaidAmount'],
            'paymentVoucherHashValue' => $hashValue
        );
        $payVoucher = new PaymentVoucher();
        $payVoucher->exchangeArray($paymentVaucherData);
        $paymentVoucherID = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->savePaymentVoucher($payVoucher);
        
        if ($paymentVoucherID && $pvData['sID']) {
            $supplierData = $this->CommonTable('Inventory\Model\SupplierTable')->getSupplierFromID($pvData['sID']);
            $tmpOutStanding = $supplierData->supplierOutstandingBalance + $pvData['fT'];
            $supplierDataArray = array(
                'supplierOutstandingBalance' => $tmpOutStanding,
                'supplierID' => $pvData['sID'],
            );
            $supplierDetails = new Supplier();
            $supplierDetails->exchangeArray($supplierDataArray);
            $updateCusOutstanding = $this->CommonTable('Inventory\Model\SupplierTable')->updateSupplierOutStandingBalance($supplierDetails);
        }

        //if this payment voucher had any payments, then need to update that payments with that new PV
        if($existPvDetails['paymentVoucherPaidAmount'] != '' && $existPvDetails['paymentVoucherPaidAmount'] != 0){
            $pvPaymentsDetails = array(
                'paymentVoucherId' => $paymentVoucherID,
                );
            $updatePaymentTable = $this->CommonTable('SupplierInvoicePaymentsTable')->updateInvoicePaymentByPaymentVoucherId($pvPaymentsDetails, $existPvDetails['paymentVoucherID']);
        }
                
        if ($paymentVoucherID) {
             if ($nonItemFlag) {
                for ($i = 0; $i < count($pvData['nIpr']); $i++) {
                    $paymentVoucherNonItemdetails = Array(
                        'paymentVoucherID' => $paymentVoucherID,
                        'paymentVoucherNonItemProductDiscription' => $pvData['nIpr'][$i]['description'],
                        'paymentVoucherNonItemProductQuantity' => $pvData['nIpr'][$i]['qty'],
                        'paymentVoucherNonItemProductUnitPrice' => $pvData['nIpr'][$i]['unitPrice'],
                        'paymentVoucherNonItemProductTotalPrice' => $pvData['nIpr'][$i]['totalPrice'],
                        'paymentVoucherNonItemProductExpenseTypeId' => $pvData['nIpr'][$i]['exType'],
                        'paymentVoucherNonItemProductExpenseTypeFormat' => $pvData['nIpr'][$i]['exFormat']
                    );
                    $paymentVoucherNonItem = new PaymentVoucherNonItemProduct();
                    $paymentVoucherNonItem->exchangeArray($paymentVoucherNonItemdetails);
                    $paymentVoucherNonItemID = $this->CommonTable('Expenses\Model\PaymentVoucherNonItemProductTable')->saveNonItemPayment($paymentVoucherNonItem);

                    if (sizeof($pvData['nIpr'][$i]['tax']) > 0) {
                        $addedTaxValue = 0;

                        foreach ($pvData['nIpr'][$i]['tax'] as $nonItmTxKy => $nonItmTxVlu) {
                            $taxData = $this->CommonTable('settings\Model\TaxTable')->getSimpleTax($nonItmTxVlu);
                            $itemPrice = $pvData['nIpr'][$i]['qty'] * $pvData['nIpr'][$i]['unitPrice'];
                            if($taxData['taxType'] == 'v'){
                                $taxValue = ($itemPrice * $taxData['taxPrecentage']) / 100;    
                            } else {
                                $taxValue = (($itemPrice + $addedTaxValue) * $taxData['taxPrecentage']) / 100;
                            }

                            $nonItemTaxData = array(
                                'paymentVoucherID' => $paymentVoucherID,
                                'paymentVoucherProductIdOrNonItemProductID' => $paymentVoucherNonItemID,
                                'paymentVoucherTaxID' => $nonItmTxVlu,
                                'paymentVoucherTaxPrecentage' => $taxData['taxPrecentage'],
                                'paymentVoucherTaxAmount' => $taxValue,
                                'paymentVoucherTaxItemOrNonItemProduct' => 'nonItem',
                            );

                            $paymentVoucherTaxData = New PaymentVoucherProductTax();
                            $paymentVoucherTaxData->exchangeArray($nonItemTaxData);
                            $paymentVoucherProductTaxID = $this->CommonTable('Expenses\Model\PaymentVoucherProductTaxTable')->savePaymentVoucherProductTax($paymentVoucherTaxData);
                            $addedTaxValue = $taxValue;
                        }
                    }
                }
            }

            if ($itemFlag) {
                for ($i = 0; $i < count($pvData['pr']); $i++) {
                    $itemDiscount = 0;
                    if ($post['pr'][$i]['pDiscount']) {
                        $itemDiscount = ($pvData['pr'][$i]['pUnitPrice'] / 100) * $pvData['pr'][$i]['pDiscount'] * $pvData['pr'][$i]['pQuantity'];
                    }
                    $productType = $pvData['pr'][$i]['productType'];
                    $paymentVoucherProductID = array();
                    $paymentVoucherProductData = Array(
                        'paymentVoucherID' => $paymentVoucherID,
                        'locationProductID' => $pvData['pr'][$i]['locationPID'],
                        'paymentVoucherProductQuantity' => $pvData['pr'][$i]['pQuantity'],
                        'paymentVoucherProductUnitPrice' => $pvData['pr'][$i]['pUnitPrice'],
                        'paymentVoucherProductTotalPrice' => $pvData['pr'][$i]['pTotal'],
                        'paymentVoucherProductDiscount' => $itemDiscount,
                        'paymentVoucherProductExpenseTypeId' => $pvData['pr'][$i]['productExType'],
                        'uomID' => $pvData['pr'][$i]['pUom'],
                    );
                    if (sizeof($pvData['pr'][$i]['bProducts']) > 0) {
                        foreach ($pvData['pr'][$i]['bProducts'] as $key => $value) {
                            $paymentVoucherProductData['productBatch'] = 1;
                            $paymentVoucherProductData['paymentVoucherProductQuantityByBatch'] = $pvData['pr'][$i]['bProducts'][$key]['bQty'];
                            $batchSerialArray = array();
                            $batchSerialArray['batchData'] = $value;
                            if (sizeof($pvData['pr'][$i]['sProducts']) > 0) {
                                $paymentVoucherProductData['productSerial'] = 1;
                                foreach ($pvData['pr'][$i]['sProducts'] as $sKey => $sValue) {
                                    if ($sValue['sBCode'] == $key) {
                                        $batchSerialArray['serialData'][$sKey] = $sValue;
                                    }
                                }
                            }
                            $btSealData = json_encode($batchSerialArray);
                            $paymentVoucherProductData['batchSerialData'] = $btSealData;

                            $paymentVoucherProduct = new PaymentVoucherProduct();
                            $paymentVoucherProduct->exchangeArray($paymentVoucherProductData);
                            $paymentVoucherProductID[] = $this->CommonTable('Expenses\Model\PaymentVoucherProductTable')->savePaymentVoucherProduct($paymentVoucherProduct);
                        }
                    } else {
                        $serialDataArray = array();
                        if (sizeof($pvData['pr'][$i]['sProducts']) > 0) {
                        
                            $paymentVoucherProductData['productSerial'] = 1;
                            $paymentVoucherProductData['productBatch'] = 0;
                            foreach ($pvData['pr'][$i]['sProducts'] as $sKey => $sValue) {
                                $serialDataArray['serialData'][$sKey] = $sValue;
                            }
                            $serialData = json_encode($serialDataArray);
                        } else {
                            $paymentVoucherProductData['productSerial'] = 0;
                            $paymentVoucherProductData['productBatch'] = 0;
                            $serialData = 'No batch serial data';
                        }
                        $paymentVoucherProductData['batchSerialData'] = $serialData;

                        $paymentVoucherProduct = new PaymentVoucherProduct();
                        $paymentVoucherProduct->exchangeArray($paymentVoucherProductData);
                        $paymentVoucherProductID[] = $this->CommonTable('Expenses\Model\PaymentVoucherProductTable')->savePaymentVoucherProduct($paymentVoucherProduct);
                    }
                    if (isset($pvData['pr'][$i]['pTax']['tL']) && sizeof($pvData['pr'][$i]['pTax']['tL']) > 0) {
                        foreach ($pvData['pr'][$i]['pTax']['tL'] as $taxKey => $taxValue) {
                            foreach ($paymentVoucherProductID as $proIDKey => $proIDValue) {
                                $taxData = array(
                                    'paymentVoucherID' => $paymentVoucherID,
                                    'paymentVoucherProductIdOrNonItemProductID' => $proIDValue,
                                    'paymentVoucherTaxID' => $taxKey,
                                    'paymentVoucherTaxPrecentage' => $taxValue['tP'],
                                    'paymentVoucherTaxAmount' => $taxValue['tA'],
                                    'paymentVoucherTaxItemOrNonItemProduct' => 'item',
                                );

                                $paymentVoucherTaxData = New PaymentVoucherProductTax();
                                $paymentVoucherTaxData->exchangeArray($taxData);
                                $paymentVoucherProductTaxID = $this->CommonTable('Expenses\Model\PaymentVoucherProductTaxTable')->savePaymentVoucherProductTax($paymentVoucherTaxData);
                            }
                        }
                    }
                }
            }
            
            $approvers = $this->getApprovers($pvData['exTy'], $pvData['fT']);
            if (!empty($approvers)) {
                $approversFlag = true;
            } else {
                $result = $this->setDraftToTheItemIn($paymentVoucherID, $pvData['dimensionData'], $pvData['ignoreBudgetLimit']);
              
                if(!$result['status']){
                    $returnArray = array(
                        'status' => false,
                        'msg' => $result['msg'],
                        'data' => $result['data']
                        );
                    return $returnArray;
                }
                $approversFlag = false;
            }
                      
            $returnArray = array(
                'status' => true,
                'msg' => $this->getMessage('EXPENSE_PAY_UPDATE_SUCCSESS'),
                'data' => array('pVID' => $paymentVoucherID, 'approversFlag' => $approversFlag, 'paymentVoucherCode' => $paymentVoucherCode, 'approver' => $approvers, 'hashValue' => $hashValue),
            );
            
            return $returnArray;

        } else {
            $returnArray = array(
                'status' => false,
                );
            return $returnArray;
        }
    }


    private function calculateAvgCostAndOtherDetails($locationProductDetails, $productDataSet = null)
    {
        //get all iteminDetails
        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductIDInDecendingOrder($productDataSet['itemInLocationProductID'],$productDataSet['itemInID']);
        $initialState = 0;
        $locationProductQuantity = floatval($locationProductDetails->locationProductQuantity);

        foreach ($itemInDetails as $key => $itemInSingleValue) {
            if ($initialState == 0) {
                //get sum of qty from item out for calculate costing
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
            } else {
                $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();

                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();

            }
            $initialState++;

            $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQty['totalQty'])) - floatval($itemInSingleValue['itemInQty']);

        }
        $nextInRecordFromCancelPi = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();

        $outDetailsQtyForPi = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($productDataSet['itemInLocationProductID'], $productDataSet['itemInDateAndTime'], $nextInRecordFromCancelPi['itemInDateAndTime'])->current();  


        $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQtyForPi['totalQty'])) - floatval($productDataSet['itemInQty']);

        $previousItemInFromCancelPi = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryPreviousRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();


        $outDetailsQtyForPreviousDoc = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($previousItemInFromCancelPi['itemInLocationProductID'], $previousItemInFromCancelPi['itemInDateAndTime'], $productDataSet['itemInDateAndTime'])->current();

        $locationProductQuantity = floatval($locationProductQuantity) + floatval($outDetailsQtyForPreviousDoc['totalQty']);

        //get all iteminDetails after cancel
        $itemInDetailsAfterCancel = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductID($previousItemInFromCancelPi['itemInLocationProductID'],$previousItemInFromCancelPi['itemInID']);

        $avarageCosting = floatval($previousItemInFromCancelPi['itemInAverageCostingPrice']);
        $initialState = 0;
        foreach ($itemInDetailsAfterCancel as $key => $itemInSingleValue) {
            //get very Next record in itemIn Table
            $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();
            
            if($nextInRecord){
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();
                
            } else {
                if (sizeof($itemInDetails) != 0) {
                    $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
                }
            }
            
            if ($initialState == 0) {
                $newAvgCosting = $avarageCosting;
            } else {
                $newAvgCosting = (($locationProductQuantity * $avarageCosting) + (floatval($itemInSingleValue['itemInQty']) * floatval($itemInSingleValue['itemInPrice'])))/(floatval($itemInSingleValue['itemInQty']) + $locationProductQuantity);
                $avarageCosting = $newAvgCosting;
                $locationProductQuantity = $locationProductQuantity + floatval($itemInSingleValue['itemInQty']);
            }
            
            //set data to update itemIn Table
            $itemInUpdatedDataSet = array(
                'itemInAverageCostingPrice' => $newAvgCosting,
            );

            //update item in table
            $itemInUpdateFlag = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($itemInUpdatedDataSet, $itemInSingleValue['itemInID']);
            if(!$itemInUpdateFlag){
                $returnCostingDetails = array(
                    'status' => false,
                    );
                return $returnCostingDetails;
            }
            
            $locationProductQuantity = $locationProductQuantity - floatval($outDetailsQty['totalQty']);
            //set data to update locationProduct
            $locationProductUpdatedData = array(
                'locationProductAverageCostingPrice' => $newAvgCosting
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $itemInSingleValue['itemInLocationProductID']);


            if($nextInRecord){
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime'],$nextInRecord['itemInDateAndTime']);
        
            } else {
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime']);

            }

           
            foreach ($itemOutDetails as $key => $itemOutSingleValue) {
                //set Data to update itemOut table
                $updateItemOutCValue = array(
                    'itemOutAverageCostingPrice' => $newAvgCosting,
                );

                //update costing value in itemOutRecords
                $updateItemOut = $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutTable($itemOutSingleValue['itemOutID'],$updateItemOutCValue);

                $itemOutIds = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDataByItemOutID($itemOutSingleValue['itemOutID'])->current();

                $journalEntryData = array(
                    'journalEntryAccountsDebitAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    'journalEntryAccountsCreditAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    );

                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryByDocumentId($journalEntryData, $itemOutIds['journalEntryID']);
                
                if(!$updateItemOut){
                    
                    $returnCalculateData = array(
                        'status' => false,
                    );
                
                    return $returnCalculateData;
                }
            }
            $initialState++;
        }

        $returnCostingDetails = array(
            'status' => true,
            );
        return $returnCostingDetails;
    }

    public function getAllRelatedDocumentDetailsByPaymentVoucherIDAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $paymentVoucherID = $request->getPost('paymentVoucherID');
            $getpaymentVoucherData = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherDetailsByPVID($paymentVoucherID)->current();
            if (isset($getpaymentVoucherData)) {
                $pvData = array(
                    'type' => 'PaymentVoucher',
                    'documentID' => $getpaymentVoucherData['paymentVoucherID'],
                    'code' => $getpaymentVoucherData['paymentVoucherCode'],
                    'amount' => number_format($getpaymentVoucherData['paymentVoucherTotal'], 2),
                    'issuedDate' => $getpaymentVoucherData['paymentVoucherDueDate'],
                    'created' => $getpaymentVoucherData['createdTimeStamp'],
                );
                $historyDetails[] = $pvData;
                if (isset($getpaymentVoucherData)) {
                    $dataExistsFlag = true;
                }
            }

            $getPaymentData = $this->CommonTable('SupplierPaymentsTable')->getPaymentDetailsByPIIdOrPVId($paymentVoucherID, "paymentVoucher");

            if (isset($getPaymentData)) {
                foreach ($getPaymentData as $piPayDta) {
                    $piPaymentData = array(
                        'type' => 'SupplierPayment',
                        'documentID' => $piPayDta['outgoingPaymentID'],
                        'code' => $piPayDta['outgoingPaymentCode'],
                        'amount' => number_format($piPayDta['outgoingPaymentAmount'], 2),
                        'issuedDate' => $piPayDta['outgoingPaymentDate'],
                        'created' => $piPayDta['createdTimeStamp'],
                    );
                    $historyDetails[] = $piPaymentData;
                    if (isset($piPayDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($historyDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $historyDetails);

            $documentDetails = array(
                'historyDetails' => $historyDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }
}
