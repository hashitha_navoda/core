<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Expenses\Model\PettyCashVoucher;
use Zend\View\Model\ViewModel;
use Expenses\Model\PettyCashVoucherDefaultAccounts;

/**
 * Description of PettyCashVoucherController
 *
 * @author shermilan
 */
class PettyCashVoucherController extends CoreController
{

    public $paginator;

    public function saveAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pettyCashVoucherInputs = $request->getPost();
            $pettyCashVoucherType = $pettyCashVoucherInputs['pettyCashVoucherType'];

            $pettyCashVoucherAmount = $pettyCashVoucherInputs['pettyCashVoucherAmount'];
            $pettyCashFloatBalance = $pettyCashVoucherInputs['pettyCashFloatBalance'];
            $pettyCashVoucherFinanceAccountID = $pettyCashVoucherInputs['pettyCashVoucherFinanceAccountID'];
            $ignoreBudgetLimit = $pettyCashVoucherInputs['ignoreBudgetLimit'];
            $dimensionData = $pettyCashVoucherInputs['dimensionData'];
            $locationID = $this->user_session->userActiveLocation['locationID'];
            // this flag use to find that cashVoucher come from expense or finance account
            $fromExpenseFormat = true;

//          check petty cash enough for this voucher amount
            if ($pettyCashFloatBalance < $pettyCashVoucherAmount) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_VOUCHER_AMOUNT_NOT_ENOUGH');
                return $this->JSONRespond();
            }

//          If voucher type is Expense Type
            if ($pettyCashVoucherType == '1') {
                unset($pettyCashVoucherInputs['outgoingPettyCashVoucherID']);
                // first check this expenseTypeID is finance account or expense type
                $expenseTypeComingDetails = split('_', $pettyCashVoucherInputs['pettyCashExpenseTypeID']);
                $pettyCashVoucherInputs['pettyCashExpenseTypeID'] = $expenseTypeComingDetails[1];

                //if this is finance account
                if($expenseTypeComingDetails[0] == 'acc'){
                    $fromExpenseFormat = false;
                    $debitAccountID = $expenseTypeComingDetails[1];
                } else {
                    //  Get selected expense Type details
                    $expenseType = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getDataByID($expenseTypeComingDetails[1])->current();
                    //  Check the voucher amount if grreater than the minimum amount of expense type
                    if ($expenseType->pettyCashExpenseTypeMinAmount && ($pettyCashVoucherAmount < $expenseType->pettyCashExpenseTypeMinAmount)) {
                        $this->msg = $this->getMessage('ERR_PETTY_CASH_VOUCHER_AMOUNT_NOT_ENOUGH_FOR_EXPENSE');
                        $this->status = false;
                        return $this->JSONRespond();
                    }

                    //  Check the voucher amount is less than maximum amount of selected expense type
                    if ($expenseType->pettyCashExpenseTypeMaxAmount && ($pettyCashVoucherAmount > $expenseType->pettyCashExpenseTypeMaxAmount)) {
                        $this->msg = $this->getMessage('ERR_PETTY_CASH_VOUCHER_AMOUNT_EXCESS_FOR_EXPENSE');
                        $this->status = false;
                        return $this->JSONRespond();
                    }

                    $debitAccountID = $expenseType->pettyCashExpenseTypeAccountID;
                }


                $creditAccountID = $pettyCashVoucherFinanceAccountID;

            } else if ($pettyCashVoucherType == '2') {
                unset($pettyCashVoucherInputs['pettyCashExpenseTypeID']);

                $outgoingPettyCashVoucherID = $pettyCashVoucherInputs['outgoingPettyCashVoucherID'];
                $insertedPettyCashVoucher = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getActivePettyCashVoucherByID($outgoingPettyCashVoucherID);

                $returnedPettyCashVouchers = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getReturnedPettyCashVoucher($outgoingPettyCashVoucherID);

                $returnedAmount = 0;
                foreach ($returnedPettyCashVouchers as $returnedPettyCashVoucher) {
                    $returnedAmount += $returnedPettyCashVoucher['pettyCashVoucherAmount'];
                }

                $availableAmountForReturn = $insertedPettyCashVoucher->pettyCashVoucherAmount - $returnedAmount;

                // Check selected voucher amount is enough for current voucher amount
                if ($availableAmountForReturn < $pettyCashVoucherAmount) {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PETTY_CASH_VOUCHER_AMOUNT_NOT_ENOUGH_IN_VOUCHER');
                    return $this->JSONRespond();
                }

                if($insertedPettyCashVoucher->pettyCashVoucherExpenseTypeFormat == 'acc'){
                    $fromExpenseFormat = false;
                    $creditAccountID = $insertedPettyCashVoucher->pettyCashExpenseTypeID;
                } else {

                    $pettyCashExpenceTypeID = $insertedPettyCashVoucher->pettyCashExpenseTypeID;

                    //  Get selected expense Type details
                    $expenseType = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getDataByID($pettyCashExpenceTypeID)->current();
                    $creditAccountID = $expenseType->pettyCashExpenseTypeAccountID;

                }
                $debitAccountID = $pettyCashVoucherFinanceAccountID;
            }

            //check the expence type have a finance Account
            if(empty($expenseType->pettyCashExpenseTypeAccountID) && $fromExpenseFormat){
                $this->msg = $this->getMessage('ERR_PETTY_CASH_VOUCHER_TYPE_ACCOUNT_SHOULD_SET',array($expenseType->pettyCashExpenseTypeName));
                $this->status = false;
                return $this->JSONRespond();
            }else if(empty($pettyCashVoucherFinanceAccountID)){
                $this->msg = $this->getMessage('ERR_PETTY_CASH_VOUCHER_ACCOUNT_SHOULD_SET');
                $this->status = false;
                return $this->JSONRespond();
            }
//          get voucher reference no

            $refData = $this->getReferenceNoForLocation(27, $locationID);
            $rid = $refData["refNo"];
            $locationReferenceID = $refData["locRefID"];
            $pettyCashVoucherNo = $this->getReferenceNumber($locationReferenceID);

            $pettyCashVoucherInputs['locationID'] = $locationID;
            $pettyCashVoucherInputs['pettyCashVoucherNo'] = $pettyCashVoucherNo;
            $pettyCashVoucherInputs['entityID'] = $this->createEntity();
            if(!$fromExpenseFormat){
                $pettyCashVoucherInputs['pettyCashVoucherExpenseTypeFormat'] = 'acc';
            } else {
                $pettyCashVoucherInputs['pettyCashVoucherExpenseTypeFormat'] = 'exp';
            }
            $pettyCashVoucher = new PettyCashVoucher();
            $pettyCashVoucher->exchangeArray($pettyCashVoucherInputs);

            $debitRow = array(
                'fAccountsIncID' =>1,
                'financeAccountsID' => $debitAccountID,
                'financeGroupsID' => '',
                'journalEntryAccountsDebitAmount' => $pettyCashVoucherAmount,
                'journalEntryAccountsCreditAmount' => 0.00,
                'journalEntryAccountsMemo' => '',
            );

            $creditRow = array(
                'fAccountsIncID' =>2,
                'financeAccountsID' => $creditAccountID,
                'financeGroupsID' => '',
                'journalEntryAccountsDebitAmount' => 0.00,
                'journalEntryAccountsCreditAmount' => $pettyCashVoucherAmount,
                'journalEntryAccountsMemo' => '',
            );

            $journalEntryAccounts[1] = $debitRow;
            $journalEntryAccounts[2] = $creditRow;

            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $this->beginTransaction();

            $insertedID = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->save($pettyCashVoucher);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $pettyCashVoucherInputs['pettyCashVoucherDate'],
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create the Petty Cash Voucher '.$pettyCashVoucherNo.'.',
                'documentTypeID' => 28,
                'journalEntryDocumentID' => $insertedID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

            $resultData = $this->saveJournalEntry($journalEntryData);

            if ($resultData['status']) {
                if (!empty($dimensionData)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$pettyCashVoucherNo], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);

                    if(!$saveRes['status']){
                        return array('status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']);
                    }   
                }

            }

            $pettyCashVoucherDefaultAccountsData = array(
                "pettyCashVoucherFinanceAccountID" => $pettyCashVoucherFinanceAccountID,
            );

            $PVDAData = $this->CommonTable('Expenses\Model\PettyCashVoucherDefaultAccountsTable')->fetchAll();
            if(count($PVDAData) > 0){
                $pettyCashVoucherDefaultAccountsID = $PVDAData->current()->pettyCashVoucherDefaultAccountsID;
                $this->CommonTable('Expenses\Model\PettyCashVoucherDefaultAccountsTable')->update($pettyCashVoucherDefaultAccountsData, $pettyCashVoucherDefaultAccountsID);
            }else{
                $PettyCashVoucherDefaultAccounts = new PettyCashVoucherDefaultAccounts();
                $PettyCashVoucherDefaultAccounts->exchangeArray($pettyCashVoucherDefaultAccountsData);
                $this->CommonTable('Expenses\Model\PettyCashVoucherDefaultAccountsTable')->save($PettyCashVoucherDefaultAccounts);
            }

            if ($insertedID && $resultData['status']){
                $this->commit();
                $this->updateReferenceNumber($locationReferenceID);
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PETTY_CASH_VOUCHER_SAVE');
                $this->flashMessenger()->addMessage($this->getMessage('SUCC_PETTY_CASH_VOUCHER_SAVE'));
            } else {
                $this->rollback();
                $this->status = false;
                if($resultData['status']){
                    $this->msg = $this->getMessage('ERR_PRTTY_CASH_VOUCHER_SAVE');
                }else{
                    $this->msg = $resultData['msg'];
                    $this->data = $resultData['data'];
                }
            }
            return $this->JSONRespond();
        }
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $voucherID = $request->getPost('voucherID');
            $locationID = $this->user_session->userActiveLocation['locationID'];

            $pettyCashVoucher = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getActivePettyCashVoucherByID($voucherID);
            if (count($pettyCashVoucher) <= 0) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_VOUCHER_NOT_AVAILABLE');
                return $this->JSONRespond();
            }
            $pettyCashFloatBalance = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getCurrentPettyCashFloatBalance($locationID);

            $this->beginTransaction();
            if ($pettyCashVoucher->pettyCashVoucherType == '1') {
//          if cash outgoing type
//                check is already used for incoming
                $returnedPettyCashVouchers = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getReturnedPettyCashVoucher($voucherID);
                if (count($returnedPettyCashVouchers) > 0) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_PETTY_CASH_VOUCHER_USED_DELETE');
                    return $this->JSONRespond();
                }
            }

            $deleted = $this->updateDeleteInfoEntity($pettyCashVoucher->entityID);

            $documentTypeID = 28;
            $JEdata = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID($documentTypeID, $voucherID);

            if(isset($JEdata['journalEntryID'])){
                $journalEntryID = $JEdata['journalEntryID'];
                $JEDeletStatus = $this->deleteJournalEntry($journalEntryID);
            }

            if ($deleted) {
                $checkStatus = false;
                if($JEDeletStatus == null ){
                    $checkStatus = true;
                }else if($JEDeletStatus['status']){
                    $checkStatus = true;
                }

                if($checkStatus){
                    $this->commit();
                // get petty cash types for drop down
                    $pettyCashVoucherTypes = $this->getServiceLocator()->get('config')['pettyCashVoucherTypes'];
                    $this->pettyCashVoucherPaginator();
                    $listView = new ViewModel([
                        'pettyCashVouchers' => $this->paginator,
                        'pettyCashVoucherTypes' => $pettyCashVoucherTypes,
                        'isPaginated' => TRUE,
                        ]);
                    $listView->setTerminal(true);
                    $listView->setTemplate('expenses/petty-cash-voucher/list');
                    $this->html = $listView;
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_PETTY_CASH_VOUCHER_DELETE');
                }else{
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $JEDeletStatus['msg'];
                }
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_VOUCHER_DELETE');
            }
            return $this->JSONRespondHtml();
        }
    }

    public function pettyCashVoucherPaginator($pageCount = 6)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->fetchAll($locationID, TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($pageCount);
    }

    public function sendPettyCashVoucherEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Petty Cash Voucher';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_EXP_PV_EMAIL');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_EXP_PV_EMAIL', array($documentType));
        return $this->JSONRespond();
    }

    public function searchPettyCashVoucherDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $searchKey = $searchrequest->getPost('searchKey');
            $pettyCashVouchers = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCashVoucherByLocationId($locationID,$searchKey);
            $voucherList = array();
            foreach ($pettyCashVouchers as $voucher) {
                $temp['value'] = $voucher['pettyCashVoucherID'];
                $temp['text'] = $voucher['pettyCashVoucherNo'];
                $voucherList[] = $temp;
            }

            $this->data = array('list' => $voucherList);
            $this->setLogMessage("Retrive petty cash voucher list for dropdown.");
            return $this->JSONRespond();
        }
    }

    public function retrivePettyCashVoucherAction()
    {
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $pettyCashVoucherTypes = $this->getServiceLocator()->get('config')['pettyCashVoucherTypes'];

        if ($request->isPost()) {
            $fromDate = $this->convertDateToStandardFormat(trim($request->getPost('fromdate')));
            $toDate = $this->convertDateToStandardFormat(trim($request->getPost('todate')));
            $PettyCashID = trim($request->getPost('PettyCashID'));
            $PettyCashNo = trim($request->getPost('PettyCashNo'));
            if($fromDate && $toDate){
                $pettyCashVoucher = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCashVoucherDetailsByDateRange($fromDate,$toDate,$locationID);
            }else{
            $pettyCashVoucher = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCashVoucherByLocationId($locationID,false,$PettyCashID);
            }
            $pettyCashView = new ViewModel(array(
                'pettyCashVouchers' => $pettyCashVoucher,
                'pettyCashVoucherTypes' => $pettyCashVoucherTypes,
                'isPaginated' => FALSE,
                'dateFormat' => $dateFormat
                    )
            );
            $pettyCashView->setTerminal(true);
            $pettyCashView->setTemplate('expenses/petty-cash-voucher/list');
            $this->setLogMessage('Retrive petty cash voucher '.$PettyCashNo.' for list view');
            return $pettyCashView;
        }
        $this->setLogMessage("Retrive petty cash voucher ".$PettyCashNo." for list view request is not post request");
        return false;
    }

}
