<?php
namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Expenses\Form\TransactionForm;
use Expenses\Model\AccountWithdrawal;
use Expenses\Model\AccountDeposit;
use Expenses\Model\AccountTransfer;
use Expenses\Model\CashInHandWithdrawal;
use Expenses\Model\CashInHandDeposit;
use Zend\View\Model\ViewModel;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Transaction api related actions
 */
class TransactionController extends CoreController
{
    public function createAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){
            $postData = $this->getRequest()->getPost()->toArray();            

            //get existing Accounts
            $accountSData = $this->CommonTable('Expenses\Model\AccountTable')->getActiveAccounts();
            $accountList = array();
            foreach ($accountSData as $key => $value) {
                $accountList[$value['accountId']] = $value['accountName']." - ".$value['accountNumber'];
            }

            //get location id
            $locationId = $this->user_session->userActiveLocation['locationID'];

            //create transaction form
            $form = new TransactionForm( 'createTransaction', $accountList);
            
            $this->beginTransaction();

            $transactionAmount = $postData['transactionAmount'];
            $transactionDate = $postData['transactionDate'];
            switch ($postData['transaction']){                
                case 'withdrawal'://for withdrawal
                    $documentName = 'Withdrawal';
                    $withdrawal = new AccountWithdrawal();
                    
                    $form->setInputFilter($withdrawal->getInputFilter());
                    $form->setData($postData);
                    //for validate form
                    if($form->isValid()){
                        $withdrawal->exchangeArray($postData);                        
                        $resultData = $this->saveWithdrawal( $withdrawal, $locationId);                        
                        if($resultData['status']){
                            $creditAccountID = $postData['transactionIssuedFinanceAccountId'];
                            $debitAccountID = $postData['transactionRecivedFinanceAccountId'];
                            $documentTypeID = 30;
                            $documentID = $resultData['data'];
                        }else{
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $resultData['msg'];
                            return $this->JSONRespond();
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_FORM_VALIDATION');
                        return $this->JSONRespond();
                    }                    
                    break;

                case 'deposit'://for deposits
                    $documentName = 'Deposit';                
                    $deposit = new AccountDeposit();
                    $form->setInputFilter($deposit->getInputFilter());
                    $form->setData($postData);
                    //for validate form
                    if($form->isValid()){                        
                        $deposit->exchangeArray($postData);
                        $resultData = $this->saveDeposit($deposit, $locationId );
                        if($resultData['status']){
                            $creditAccountID = $postData['transactionIssuedFinanceAccountId'];
                            $accountID = $postData['transactionBankAccountId'];
                            $accountResult = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountID);
                            if($accountResult['financeAccountID'] == ''){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_SELECT_BANK_ACCOUNT_DOESNOT_HAVE_GL_ACCOUNT');
                                return $this->JSONRespond();
                            }
                            $debitAccountID = $accountResult['financeAccountID'];
                            $documentTypeID = 29;
                            $documentID = $resultData['data'];
                        }else{
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $resultData['msg'];
                            return $this->JSONRespond();
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_FORM_VALIDATION');
                        return $this->JSONRespond();
                    }
                    break;                    
                
                case 'transfer'://for transfer
                    $documentName = 'Transfer';                    
                    $transfer = new AccountTransfer();
                    $form->setInputFilter($transfer->getInputFilter());
                    $form->setData($postData);                    
                    //for validate form
                    if($form->isValid()){
                        $transfer->exchangeArray($postData);
                        $resultData = $this->saveTransfer($transfer);
                        if($resultData['status']){
                            $creditAccountID = $postData['transactionIssuedFinanceAccountId'];
                            $debitAccountID = $postData['transactionRecivedFinanceAccountId'];
                            $documentTypeID = 31;
                            $documentID = $resultData['data'];
                        }else{
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $resultData['msg'];
                            return $this->JSONRespond();
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_FORM_VALIDATION');
                        return $this->JSONRespond();
                    }                    
                    break;

                default :
                    error_log('invalid request');
            }

            if(empty($creditAccountID)){
                $this->rollback();
                $this->status = false;
                $this->msg =$this->getMessage('ERR_ISSUED_ACCOUNT_NOT_SET');
                return $this->JSONRespond();
            }else if(empty($debitAccountID)){
                $this->rollback();
                $this->status = false;
                $this->msg =$this->getMessage('ERR_RECIVED_ACCOUNT_NOT_SET');
                return $this->JSONRespond();

            }

            $debitRow = array(
                'fAccountsIncID' =>1,
                'financeAccountsID' => $debitAccountID,
                'financeGroupsID' => '',
                'journalEntryAccountsDebitAmount' => $transactionAmount,
                'journalEntryAccountsCreditAmount' => 0.00,
                'journalEntryAccountsMemo' => '',
            );

            $creditRow = array(
                'fAccountsIncID' =>2,
                'financeAccountsID' => $creditAccountID,
                'financeGroupsID' => '',
                'journalEntryAccountsDebitAmount' => 0.00,
                'journalEntryAccountsCreditAmount' => $transactionAmount,
                'journalEntryAccountsMemo' => '',
            );

            $journalEntryAccounts[1] = $debitRow; 
            $journalEntryAccounts[2] = $creditRow;

            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $transactionDate,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry posted based on Transaction '.$documentName.'.',
                'documentTypeID' => $documentTypeID,
                'journalEntryDocumentID' => $documentID,
                'ignoreBudgetLimit' => $postData['ignoreBudgetLimit'],
            );
            
            $resultData = $this->saveJournalEntry($journalEntryData);
            if($resultData['status']){
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_TRANSACTION_SAVED');
                // $this->flashMessenger()->addMessage($this->msg);
            }else{
                $this->rollback();
                $this->status = false;
                $this->msg = $resultData['msg'];
                $this->data = $resultData['data'];
            }
        }
        
        return $this->JSONRespond();
    }
    
    /**
     * Save withdrawal
     * @param Obj $withdrawal
     * @param int $locationId
     * @param decimal $locationCashInHand
     */
    private function saveWithdrawal($withdrawal, $locationId ) 
    {
        $data = '';
        try{
            //save account withdrawal
            $withdrawal->entityId = $this->createEntity();
            $result = $this->CommonTable('Expenses\Model\AccountWithdrawalTable')->saveWithdrawal($withdrawal);
            if($result){
                $status = true;
                $msg    = $this->getMessage('SUCC_WITHDRAWAL_SAVE');
                $data   = $result;
            } else {
                $status = false;
                $this->msg = $this->getMessage('ERR_WITHDRAWAL_SAVE');
            }
        } catch (\Exception $ex) {
            $status = flase;
            $msg = $ex->getMessage();
        }
        return  array('status' => $status, 'msg' => $msg, 'data' => $data);
    }
    
    /**
     * Save deposit
     * @param Obj $deposit
     * @param int $locationId
     * @param decimal $locationCashInHand
     */
    private function saveDeposit( $deposit, $locationId ) 
    {
        try{
            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($deposit->accountDepositAccountId);
            if($account){
                $accountBalance = $account['accountBalance'] + $deposit->accountDepositAmount;
                    //update account balance
                $this->CommonTable('Expenses\Model\AccountTable')->updateAccount(array('accountBalance' => $accountBalance), $deposit->accountDepositAccountId);
                    //update entity
                $this->updateEntity($account->entityId);
                    //save deposit
                $deposit->entityId = $this->createEntity();
                $result = $this->CommonTable('Expenses\Model\AccountDepositTable')->saveDeposit($deposit);
                if($result){
                    $status = true;
                    $msg    = $this->getMessage('SUCC_DEPOSIT_SAVE');
                    $data   = $result;
                    return array('status' => $status, 'msg' => $msg, 'data' => $data );
                } else {
                    $status = false;
                    $msg = $this->getMessage('ERR_DEPOSIT_SAVE');
                    return array('status' => $status, 'msg' => $msg, 'data' => '');
                }
            } else {
                $status = false;
                $msg = $this->getMessage('ERR_ACCOUNT_NOT_FOUND');
                return array('status' => $status, 'msg' => $msg, 'data' => '');
            }                
        } catch (\Exception $ex) {
            $status = false;
            $msg = $ex->getMessage();
            return array('status' => $status, 'msg' => $msg, 'data' => '');
        }
    }

    /**
     * Save transfer
     * @param Obj $transfer
     */
    private function saveTransfer(AccountTransfer $transfer)
    {
        $data = '';
        try {
            //save transfer
            $transfer->entityId = $this->createEntity();
            $result = $this->CommonTable('Expenses\Model\AccountTransferTable')->saveTransfer($transfer);
            if($result){
                $status = true;
                $msg    = $this->getMessage('SUCC_TRANSFER_SAVE');
                $data   = $result;
            } else {
                $status = false;
                $msg = $this->getMessage('ERR_TRANSFER_SAVE');
            }

        } catch (\Exception $ex) {
            $status = false;
            $msg = $ex->getMessage();
        }

        return array('status' => $status, 'msg' => $msg, 'data' => $data);
    }
    
    /**
     * Cancel transaction
     */
    public function cancelAction() 
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){
            $postData = $this->getRequest()->getPost()->toArray();
            //get location id
            $locationId = $this->user_session->userActiveLocation['locationID'];
            
            try {
                $this->beginTransaction();
                switch ($postData['transactionType']){
                    case 'withdrawal':
                        $transaction = $this->CommonTable('Expenses\Model\AccountWithdrawalTable')->getWithdrawalByWithdrawalId($postData['transactionId']);
                        $documentTypeID = 30;
                        break;
                    case 'deposit':
                        $transaction = $this->CommonTable('Expenses\Model\AccountDepositTable')->getDepositByDepositId($postData['transactionId']);
                        $documentTypeID = 29;
                        break;
                    case 'transfer':
                        $transaction = $this->CommonTable('Expenses\Model\AccountTransferTable')->getTransferByTransferId($postData['transactionId']);
                        $documentTypeID = 31;
                        break;
                    default :
                        error_log('invalid option');
                }

                if($transaction){
                    $result = $this->updateDeleteInfoEntity($transaction['entityId']);
                    
                    $JEdata = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID($documentTypeID, $postData['transactionId']);

                    if(isset($JEdata['journalEntryID'])){
                        $journalEntryID = $JEdata['journalEntryID'];
                        $JEDeletStatus = $this->deleteJournalEntry($journalEntryID);
                    }

                    if($result){
                        $checkStatus = false;
                        if($JEDeletStatus == null ){
                            $checkStatus = true;
                        }else if($JEDeletStatus['status']){
                            $checkStatus = true;
                        }
                        
                        if($checkStatus){
                            $this->commit();
                            $this->status = true;
                            $this->msg = $this->getMessage('SUCC_TRANSACTION_CANCEL');
                        }else{
                            $this->rollback();
                            $this->status = false;    
                            $this->msg = $JEDeletStatus['msg'];
                        }
                    } else {
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_TRANSACTION_CANCEL');
                    }

                } else{
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_TRANSACTION_EXIST');
                }
            } catch (\Exception $ex) {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_TRANSACTION_CANCEL');
            }                            
        }
        
        return $this->JSONRespond();
    }
    
    /**
     * Search transaction 
     */
    public function searchAction() 
    {        
        if($this->getRequest()->isPost()){
            
            $postData = $this->getRequest()->getPost()->toArray();
            
            $locationId = $this->user_session->userActiveLocation['locationID'];
            $transactions = array();
            if(!empty($postData['transactionType'])){
                
                $postData['fromDate'] = (!empty($postData['fromDate'])) ? $this->getGMTDateTime('Y-m-d H:i:s', $postData['fromDate'] . ' 00:00') : null;
                $postData['toDate']   = (!empty($postData['toDate'])) ? $this->getGMTDateTime('Y-m-d H:i:s', $postData['toDate'] . ' 23:59') : null;
                
                //get withdrawals
                $withdrawals = array();
                if (in_array('withdrawal', $postData['transactionType'])) {
                    $withdrawals = $this->CommonTable('Expenses\Model\AccountWithdrawalTable')->getWithdrawalsByAccountId( $postData['accountId'], $postData['fromDate'], $postData['toDate']);
                    $withdrawals = array_map(function($transaction) {
                        $transaction['id'] = $transaction['accountWithdrawalId'];
                        $transaction['issuedAccount'] = $transaction['issuedFinanceAccountsCode'].'_'.$transaction['issuedFinanceAccountsName'];
                        $transaction['reference'] = "WDL".$transaction['accountWithdrawalId'];
                        $transaction['recivedAccount'] = $transaction['recivedFinanceAccountsCode'].'_'.$transaction['recivedFinanceAccountsName'];
                        $transaction['date'] = $transaction['accountWithdrawalDate'];
                        $transaction['type'] = 'withdrawal';
                        $transaction['amount'] = $transaction['accountWithdrawalAmount'];
                        return $transaction;
                    }, iterator_to_array($withdrawals));
                }

                //get deposit
                $deposits = array();
                if (in_array('deposit', $postData['transactionType'])) {
                    $deposits = $this->CommonTable('Expenses\Model\AccountDepositTable')->getDepositsByAccountId( $postData['accountId'], $postData['fromDate'], $postData['toDate']);
                    $deposits = array_map(function($transaction) {
                        $transaction['id'] = $transaction['accountDepositId'];
                        $transaction['bankAccount'] = $transaction['accountName'].' ('.$transaction['accountNumber'].')';
                        $transaction['issuedAccount'] = $transaction['issuedFinanceAccountsCode'].'_'.$transaction['issuedFinanceAccountsName'];
                        $transaction['reference'] = "DEP".$transaction['accountDepositId'];
                        $transaction['date'] = $transaction['accountDepositDate'];
                        $transaction['type'] = 'deposit';
                        $transaction['amount'] = $transaction['accountDepositAmount'];
                        return $transaction;
                    }, iterator_to_array($deposits));
                }

                //get incoming account transfer
                $transfer = array();
                if (in_array('transfer', $postData['transactionType'])) {
                    $transfer = $this->CommonTable('Expenses\Model\AccountTransferTable')->getAccountTransfersByAccountId( $postData['accountId'], $postData['fromDate'], $postData['toDate']);
                    $transfer = array_map(function($transaction) {
                        $transaction['id'] = $transaction['accountTransferId'];
                        $transaction['issuedAccount'] = $transaction['issuedFinanceAccountsCode'].'_'.$transaction['issuedFinanceAccountsName'];
                        $transaction['recivedAccount'] = $transaction['recivedFinanceAccountsCode'].'_'.$transaction['recivedFinanceAccountsName'];
                        $transaction['reference'] = "TRF".$transaction['accountTransferId'];
                        $transaction['type'] = 'transfer';
                        $transaction['date'] = $transaction['accountTransferDate'];
                        $transaction['amount'] = $transaction['accountTransferAmount'];
                        return $transaction;
                    }, iterator_to_array($transfer));
                }       

                $transactions = array_merge($withdrawals, $deposits, $transfer);
            }
            $view = new ViewModel(array( 'transactions'=>$transactions, 'paginated' => false));
            $view->setTerminal(true);
            $view->setTemplate('expenses/transaction/transaction-list');
            return $view;
        }            
    }

    public function viewTransactionAction()
    {
        if (!$this->getRequest()->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data = null;
            return $this->JSONRespond();
        }

        $postData = $this->getRequest()->getPost()->toArray();
        $transaction = array();
        switch ($postData['transactionType']) {
            case 'withdrawal':
                    $withdrawals = $this->CommonTable('Expenses\Model\AccountWithdrawalTable')->getWithdrawalByWithdrawalId($postData['transactionId']);
                    $transaction['id'] = $withdrawals['accountWithdrawalId'];
                    $transaction['issuedAccount'] = $withdrawals['issuedFinanceAccountsCode'].'_'.$withdrawals['issuedFinanceAccountsName'];
                    $transaction['recivedAccount'] = $withdrawals['recivedFinanceAccountsCode'].'_'.$withdrawals['recivedFinanceAccountsName'];
                    $transaction['date'] = $withdrawals['accountWithdrawalDate'];
                    $transaction['type'] = 'Withdrawal';
                    $transaction['amount'] = $withdrawals['accountWithdrawalAmount'];
                    $transaction['comment'] = $withdrawals['accountWithdrawalComment'];
                    
                break;

             case 'deposit':
                    $deposits = $this->CommonTable('Expenses\Model\AccountDepositTable')->getDepositByDepositId($postData['transactionId']);
                    $transaction['id'] = $deposits['accountDepositId'];
                    $transaction['bankAccount'] = $deposits['accountName'].' ('.$deposits['accountNumber'].')';
                    $transaction['issuedAccount'] = $deposits['issuedFinanceAccountsCode'].'_'.$deposits['issuedFinanceAccountsName'];
                    $transaction['date'] = $deposits['accountDepositDate'];
                    $transaction['type'] = 'Deposit';
                    $transaction['amount'] = $deposits['accountDepositAmount'];
                    $transaction['comment'] = $deposits['accountDepositComment'];
                    
                break;

            case 'transfer':
                    $transfer = $this->CommonTable('Expenses\Model\AccountTransferTable')->getTransferByTransferId($postData['transactionId']);
                    $transaction['id'] = $transfer['accountTransferId'];
                    $transaction['issuedAccount'] = $transfer['issuedFinanceAccountsCode'].'_'.$transfer['issuedFinanceAccountsName'];
                    $transaction['recivedAccount'] = $transfer['recivedFinanceAccountsCode'].'_'.$transfer['recivedFinanceAccountsName'];
                    $transaction['type'] = 'Transfer';
                    $transaction['date'] = $transfer['accountTransferDate'];
                    $transaction['amount'] = $transfer['accountTransferAmount'];
                    $transaction['comment'] = $transfer['accountTransferComment'];

                break;
        }

        $view = new ViewModel(array('transaction'=>$transaction));
        $view->setTerminal(true);
        $view->setTemplate('expenses/transaction/transaction-view');
        return $view;
    }
}
