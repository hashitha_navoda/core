<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

/**
 * @author Damith Karunathilaka <damith@thinkcube.com>
 * This file contains Expense Category API related actions
 */
class ExpenseCategoryController extends CoreController
{

    /**
     * Create Expense Category
     * @return JsonModel
     */
    public function addAction()
    {

        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $this->beginTransaction();
            $entityID = $this->createEntity();
            $expenseCategoryData = array(
                'expenseCategoryName' => $postData['expenseCategoryName'],
                'expenseCategoryParentId' => ($postData['expenseCategoryParent'] == 0) ? NULL : $postData['expenseCategoryParent'],
                'expenseCategoryStatus' => $this->getStatusID('active'),
                'entityId' => $entityID
            );
            $expenseCategory = new \Expenses\Model\ExpenseCategory();
            $expenseCategory->exchangeArray($expenseCategoryData);
            $savedExpenseCategoryID = $this->CommonTable('Expenses\Model\ExpenseCategoryTable')->create($expenseCategory);
            if ($savedExpenseCategoryID) {
                $this->commit();
                $this->msg = $this->getMessage('SUCC_EXPCAT_SAVE');
                $this->status = TRUE;
                $listView = $this->getExpensesCategoryListView();
                $this->data = $this->htmlRender($listView);
            } else {
                $this->rollback();
                $this->msg = $this->getMessage('ERR_EXPCAT_SAVE');
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function updateAction()
    {
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $expenseCategoryId = $postData['expenseCategoryId'];
            $expenseCategoryData = array(
                'expenseCategoryName' => $postData['expenseCategoryName'],
                'expenseCategoryParentId' => ($postData['expenseCategoryParent'] == 0) ? NULL : $postData['expenseCategoryParent'],
            );
            $dataUpdated = $this->CommonTable('Expenses\Model\ExpenseCategoryTable')->update($expenseCategoryData, $expenseCategoryId);
            if ($dataUpdated) {
                $this->msg = $this->getMessage('SUCC_EXPCAT_UPD');
                $this->status = TRUE;
                $listView = $this->getExpensesCategoryListView();
                $this->data = $this->htmlRender($listView);
            } else {
                $this->msg = $this->getMessage('ERR_EXPCAT_UPD');
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function changeExpenseCategoryStatusAction()
    {
        if ($this->getRequest()->isPost()) {
            $postData = $this->getRequest()->getPost();
            $expenseCategoryID = $postData['expenseCategoryId'];
            $changeStatusTo = $this->getStatusID($postData['changeStatusTo']);
            if ($this->CommonTable('Expenses\Model\ExpenseCategoryTable')->changeStatus($changeStatusTo, $expenseCategoryID)) {
                $this->status = TRUE;
            } else {
                $this->status = FALSE;
            }
            return $this->JSONRespond();
        }
    }

    public function deleteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $expenseCategoryId = $request->getPost('expenseCategoryId');
            //check category used for expense types
            $expenseTypes = $this->CommonTable('Expenses\Model\ExpenseTypeTable')->getExpenseTypesByCategory($expenseCategoryId);
            if (count($expenseTypes) > 0) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_EXPCAT_DEL');
            } else {
                $expenseCategoryData = $this->CommonTable('Expenses\Model\ExpenseCategoryTable')->getExpenseCategoryByID($expenseCategoryId);
                $entityID = $expenseCategoryData['entityId'];
                $this->updateDeleteInfoEntity($entityID);
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_EXPCAT_DEL');
            }

            return $this->JSONRespond();
        }
    }

    private function getExpensesCategoryListView()
    {
        $paginator = $this->CommonTable('Expenses\Model\ExpenseCategoryTable')->fetchAll(TRUE);
        $paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $paginator->setItemCountPerPage(6);
        $expCategoryListView = new ViewModel(array(
            'categories' => $paginator,
            'paginated' => TRUE));
        $expCategoryListView->setTemplate('expenses/expense-category/list');
        $expCategoryListView->setTerminal(FALSE);
        return $expCategoryListView;
    }

}
