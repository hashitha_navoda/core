<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Expenses\Model\PettyCashFloat;
use Expenses\Form\PettyCashFloatForm;
use Zend\View\Model\ViewModel;
use Expenses\Model\PettyCashFloatDefaultAccounts;

/**
 * Description of PettyCashFloatAPIController
 *
 * @author sharmilan <sharmilan@thinkcube.com>
 */
class PettyCashFloatAPIController extends CoreController
{

    public $paginator;

    public function saveAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $pettyCashFloatData = $request->getPost();

            $dimensionData = $pettyCashFloatData['dimensionData'];
            unset($pettyCashFloatData['dimensionData']);

            $locationID = $this->user_session->userActiveLocation['locationID'];
            $pettyCashFloatData['locationID'] = $locationID;
            $pettyCashFloatData['pettyCashFloatStatus'] = 4;
            $pettyCashFloatData['entityID'] = $this->createEntity();

            $pettyCashFloat = new PettyCashFloat();
            $pettyCashFloat->exchangeArray($pettyCashFloatData);

            if(empty($pettyCashFloatData['pettyCashFloatFinanceAccountID'])){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_ACCOUNT_NOT_SET');
                return $this->JSONRespond();
            }else if(empty($pettyCashFloatData['pettyCashFloatIssueFinanceAccountID'])){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_ISSUE_ACCOUNT_NOT_SET');
                return $this->JSONRespond();
            }
           
            $this->beginTransaction();
            $insertedID = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->save($pettyCashFloat);

            $debitRow = array(
                'fAccountsIncID' =>1,
                'financeAccountsID' => $pettyCashFloatData['pettyCashFloatFinanceAccountID'],
                'financeGroupsID' => '',
                'journalEntryAccountsDebitAmount' => $pettyCashFloatData['pettyCashFloatAmount'],
                'journalEntryAccountsCreditAmount' => 0.00,
                'journalEntryAccountsMemo' => '',
            );

            $creditRow = array(
                'fAccountsIncID' =>2,
                'financeAccountsID' => $pettyCashFloatData['pettyCashFloatIssueFinanceAccountID'],
                'financeGroupsID' => '',
                'journalEntryAccountsDebitAmount' => 0.00,
                'journalEntryAccountsCreditAmount' => $pettyCashFloatData['pettyCashFloatAmount'],
                'journalEntryAccountsMemo' => '',
            );
            $journalEntryAccounts[1] = $debitRow; 
            $journalEntryAccounts[2] = $creditRow;

            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $pettyCashFloatData['pettyCashFloatDate'],
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create the Petty Cash Float',
                'documentTypeID' => 27,
                'journalEntryDocumentID' => $insertedID,
                'ignoreBudgetLimit' => $pettyCashFloatData['ignoreBudgetLimit'],
            );

            $resultData = $this->saveJournalEntry($journalEntryData);

            if ($resultData['status']) {
                if (!empty($dimensionData)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($dimensionData['data'], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);

                    if(!$saveRes['status']){
                        return array('status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']);
                    }   
                }

            }

            $pettyCashFloatDefaultAccountsData = array(
                "pettyCashFloatFinanceAccountID" => $pettyCashFloatData['pettyCashFloatFinanceAccountID'],
                "pettyCashFloatIssueFinanceAccountID" => $pettyCashFloatData['pettyCashFloatIssueFinanceAccountID'],
            );

            $PFDAData = $this->CommonTable('Expenses\Model\PettyCashFloatDefaultAccountsTable')->fetchAll();
            if(count($PFDAData) > 0){
                $pettyCashFloatDefaultAccountsID = $PFDAData->current()->pettyCashFloatDefaultAccountsID; 
                $this->CommonTable('Expenses\Model\PettyCashFloatDefaultAccountsTable')->update($pettyCashFloatDefaultAccountsData, $pettyCashFloatDefaultAccountsID);
            }else{
                $PettyCashFloatDefaultAccounts = new PettyCashFloatDefaultAccounts();
                $PettyCashFloatDefaultAccounts->exchangeArray($pettyCashFloatDefaultAccountsData);
                $this->CommonTable('Expenses\Model\PettyCashFloatDefaultAccountsTable')->save($PettyCashFloatDefaultAccounts);
            }

            if ($insertedID && $resultData['status']) {
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_PETTY_CASH_FLOAT_SAVE');
                $this->flashMessenger()->addMessage(['status' => true,'msg'=>$this->msg]);
            } else {
                $this->rollback();
                $this->status = false;
                if($resultData['status']){
                    $this->msg = $this->getMessage('ERR_PETTY_CASH_FLOAT_SAVE');
                }else{
                    $this->msg = $resultData['msg'];
                    $this->data = $resultData['data'];
                }
            }
            return $this->JSONRespond();
        }
    }

    public function updateStatusAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $pettyCashFloatID = $request->getPost('pettyCashFloatID');
            $locationID = $this->user_session->userActiveLocation['locationID'];

            $result = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getPettyCashFloatByID($pettyCashFloatID);
            $pettyCashFloat = $result->current();
            $entityID = $pettyCashFloat->entityID;
            
            $this->beginTransaction();

            $status = $this->updateDeleteInfoEntity($entityID);
            
            $documentTypeID = 27;
            $JEdata = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID($documentTypeID, $pettyCashFloatID);

            if(isset($JEdata['journalEntryID'])){
                $journalEntryID = $JEdata['journalEntryID'];
                $JEDeletStatus = $this->deleteJournalEntry($journalEntryID);
            }

            if($status){
                $checkStatus = false;
                if($JEDeletStatus == null ){
                    $checkStatus = true;
                }else if($JEDeletStatus['status']){
                    $checkStatus = true;
                }
                if($checkStatus){
                    $this->getPaginatedPettyCashFloats();
                     $companyCurrencySymbol = $this->user_session->companyCurrencySymbol;
                    // Petty Cash Float prefix
                    $PettyCashFloatCodePrefix = $this->getServiceLocator()->get('config')['PettyCashFloatCodePrefix'];
                    $listview = new ViewModel([
                        'pettyCashFloats' => $this->paginator,
                        'PettyCashFloatCodePrefix' => $PettyCashFloatCodePrefix,
                        'cashTypes' => $cashTypes,
                        'paginated' => true,
                        'companyCurrencySymbol' => $companyCurrencySymbol,
                        ]);
                    $listview->setTerminal(true);
                    $listview->setTemplate('expenses/petty-cash-float/list');
                    $this->html = $listview;
                    $this->commit();
                    $this->status = TRUE;
                    $this->msg = $this->getMessage('SUCC_PETTY_CASH_FLOAT_STATUS_UPADTE');
                }else{
                    $this->rollback();
                    $this->status = false;    
                    $this->msg = $JEDeletStatus['msg'];
                }
            }else{
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_PETTY_CASH_FLOAT_STATUS_UPADTE');
            }

            return $this->JSONRespondHtml();
        }
    }

    public function getPaginatedPettyCashFloats($pageCount = 6)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->fetchAll($locationID, true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($pageCount);
    }

}
