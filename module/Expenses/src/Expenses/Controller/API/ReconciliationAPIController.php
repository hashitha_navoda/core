<?php

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Model\Reconciliation;
use Expenses\Model\ReconciliationTransaction;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Reconciliation API related actions
 */
class ReconciliationAPIController extends CoreController {

    /**
     * Get account transaction list 
     * @return ViewModel
     */
    public function getAccountTransactionsAction() {
        if ($this->getRequest()->isPost()) {
            $transaction = array();
            $accountId = $this->getRequest()->getPost('accountId');
            $startDate = $this->getRequest()->getPost('startDate');
            $endDate = $this->getRequest()->getPost('endDate');

            $reconcileEditData = $this->CommonTable('Expenses\Model\ReconciliationTable')->checkReconcilation($accountId, $startDate, $endDate);

            if (!is_null($reconcileEditData)) {
                $view = new ViewModel(array('transactions' => [], 'account' => [], 'validate' => false));
                $view->setTerminal(true);
                $view->setTemplate('expenses/reconciliation-api/transaction-list');
                return $view;
            }

            $startDate = null;

            //get account by account id
            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
            $glAccountId = $account['financeAccountID'];
            $newDate = date('Y-m-d', strtotime($endDate . ' +1 day'));

            $jEDataForOpening = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($glAccountId, '', $endDate);

            $incomingBankTransferData = $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->getBankTransfersByAccountId($accountId,null,$startDate,$endDate);

            $outgoingBankTransferData = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentOutgoingBankTransfersByAccountId( $accountId, null, $startDate, $endDate);

            $issedChequesData = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId($accountId,null,$startDate,$endDate);

            $adPayissuedChequesData = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId( $accountId, null, $startDate, $endDate);

            $issuedChequesAmount = 0;
            foreach ($issedChequesData as $key22 => $value22) {
                if ($value22['outGoingPaymentMethodFinanceAccountID'] != $glAccountId) {
                    $issuedChequesAmount += floatval($value22['outGoingPaymentMethodPaidAmount']);
                }
            }

            $adissuedChequesAmount = 0;
            foreach ($adPayissuedChequesData as $key33 => $value33) {
                if ($value33['outGoingPaymentMethodFinanceAccountID'] != $glAccountId) {
                    $adissuedChequesAmount += floatval($value33['outGoingPaymentMethodPaidAmount']);
                }
            }


            $outgoingTransferAmount = 0;
            foreach ($outgoingBankTransferData as $key77 => $value77) {
                if ($value77['outGoingPaymentMethodFinanceAccountID'] != $glAccountId) {
                    $outgoingTransferAmount += floatval($value77['outGoingPaymentMethodPaidAmount']);
                }
            }


            $incomingTranserAmount = 0;
            foreach ($incomingBankTransferData as $key66 => $value66) {

                $incomeData = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getInvoicePaymentByPaymentId($value66['incomingPaymentId'])->current();

                if ($incomeData && !empty($incomeData['incomeID'])) {

                    $journalEntryDataForIncome = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('43', $incomeData['incomeID']);

                    $journalEntryAccountDataForIncome = $this->CommonTable('JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryIDAndAccountID( $journalEntryDataForIncome['journalEntryID'], $glAccountId)->current();

                    if (!$journalEntryAccountDataForIncome) {
                        $incomingTranserAmount += floatval($value66['incomingPaymentMethodAmount']);
                    }
                } else {
                    $journalEntryDataForInvoice = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('7', $value66['incomingPaymentId']);

                    $journalEntryAccountDataForInvoice = $this->CommonTable('JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryIDAndAccountID( $journalEntryDataForInvoice['journalEntryID'], $glAccountId)->current();

                    if (!$journalEntryAccountDataForInvoice) {
                        $incomingTranserAmount += floatval($value66['incomingPaymentMethodAmount']);
                    }
                }
            }

            $account['actualAccBalance'] = (floatval($jEDataForOpening['totalDebitAmount']) + $incomingTranserAmount) - (floatval($jEDataForOpening['totalCreditAmount']) +  $issuedChequesAmount + $adissuedChequesAmount + $outgoingTransferAmount);

            //get deposited cheques
            $depositedCheques = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getDepositedChequeByAccountId( $accountId, 0, $startDate, $endDate);
            $depositedCheques = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['incomingPaymentMethodChequeId'];
                $transaction['type'] = 'cheque-deposit';
                $transaction['amount'] = $transaction['incomingPaymentMethodAmount']/$transaction['chequeDepositAccountCurrencyRate'];
                return $transaction;
            }, iterator_to_array($depositedCheques));
            
            //get invoice payment issued cheques
            $issuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId( $accountId, 0, $startDate, $endDate);
            $issuedCheques = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['outGoingPaymentMethodsNumbersID'];
                $transaction['journalEntryDocumentID'] = $transaction['outGoingPaymentID'];
                $transaction['type'] = 'cheque-issue';
                $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                return $transaction;
            }, iterator_to_array($issuedCheques));
            
            //get advanced payment issued cheques
            $adPayissuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId( $accountId, 0, $startDate, $endDate);
            $adPayissuedCheques = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['outGoingPaymentMethodsNumbersID'];
                $transaction['journalEntryDocumentID'] = $transaction['outGoingPaymentID'];
                $transaction['type'] = 'cheque-issue';
                $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                return $transaction;
            }, iterator_to_array($adPayissuedCheques));
            
            //get invoice outgoing bank transfer
            $bankOutgoingTransfer = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getOutgoingBankTransfersByAccountId( $accountId, 0, $startDate, $endDate);
            $bankOutgoingTransfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['outGoingPaymentMethodsNumbersID'];
                $transaction['type'] = 'outgoing-bank-transfer';
                $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                return $transaction;
            }, iterator_to_array($bankOutgoingTransfer));
            
            //get advanced payment bank transfer
            $adPaybankOutgoingTransfer = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentOutgoingBankTransfersByAccountId( $accountId, 0, $startDate, $endDate);
            $adPaybankOutgoingTransfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['outGoingPaymentMethodsNumbersID'];
                $transaction['type'] = 'outgoing-bank-transfer';
                $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                return $transaction;
            }, iterator_to_array($adPaybankOutgoingTransfer));
                        
            //get incoming bank transfer
            $bankIncomingTransfer = $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->getBankTransfersByAccountId($accountId,0,$startDate,$endDate);
            $bankIncomingTransfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['incomingPaymentMethodBankTransferId'];
                $transaction['incomingPaymentID'] = $transaction['incomingPaymentId'];
                $transaction['type'] = 'incoming-bank-transfer';
                $transaction['amount'] = $transaction['incomingPaymentMethodAmount'];
                return $transaction;
            }, iterator_to_array($bankIncomingTransfer));

            //get account direct withrawal
            $bankWithdrawal = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDirectWithdrawalByGlAccountId($glAccountId,$startDate, $endDate);
            $bankWithdrawal = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'bank-withrawal';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($bankWithdrawal));

            //get account direct deposit je
            $directDebitJE = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDirectJEDebitByGlAccountId($glAccountId,$startDate, $endDate);
            $directDebitJE = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'direct-journal-entry-deposit';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($directDebitJE));

            //get account direct withdrawal je
            $directCreditJE = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDirectJECreditByGlAccountId($glAccountId,$startDate, $endDate);
            $directCreditJE = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'direct-journal-entry-withrawal';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($directCreditJE));

        
            //get account direct deposit
            $bankdeposit = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDirectDepositByGlAccountId($glAccountId,$startDate, $endDate);
            $bankdeposit = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'bank-deposit';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($bankdeposit));
           
            //get account deposit by a withdrwal
            $deposit = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDepositByGlAccountId($glAccountId,$startDate, $endDate);
            $deposit = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'deposit';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($deposit));

            //get account withdrwal by a deposit
            $withdrwal = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getWithdrawalByGlAccountId($glAccountId,$startDate, $endDate);
            $withdrwal = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'withdrwal';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($withdrwal));
           
            //get account incoming transfer
            $accountIncomingTranfer = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getIncomingBankTransfersByAccountId($glAccountId,$startDate, $endDate);
            $accountIncomingTranfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'account-incoming-transfer';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($accountIncomingTranfer));

            //get account outgoing transfer
            $accountOutgoingTransfer = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOutgoingBankTransfersByAccountId($glAccountId,$startDate, $endDate);
            $accountOutgoingTransfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'account-outgoing-transfer';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($accountOutgoingTransfer));
            

            //get outgoing glaccount advanced payments
            $outgoingAdvancedPayments = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOutgoingAdvancedPaymentByGlAccountId($glAccountId,$startDate, $endDate);
            $outgoingAdvancedPayments = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'outgoing-advanced-payments';
                $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                return $transaction;
            }, iterator_to_array($outgoingAdvancedPayments));

           //get incoming glaccount advanced payments
            $incomingAdvancedPayments = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getCustomerPaymentJournalEntryAccountsByAccountIDAndDateRange( $glAccountId,$startDate, $endDate);

            foreach ($incomingAdvancedPayments as $key88 => $value88) {
                $incomingAdvancedPayments[$key88]['transactionId'] = $value88['journalEntryAccountsID'];
                $incomingAdvancedPayments[$key88]['incomingPaymentID'] = $value88['incomingPaymentId'];
                $incomingAdvancedPayments[$key88]['type'] = 'incoming-advanced-payments';
                $incomingAdvancedPayments[$key88]['amount'] = $value88['incomingPaymentMethodAmount'];
            }

            //get incoming glaccount advanced income payments
            $incomingPayments = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getIncomePaymentJournalEntryAccountsByAccountIDAndDateRange( $glAccountId,$startDate, $endDate);

            foreach ($incomingPayments as $key99 => $value99) {
                $incomingPayments[$key99]['transactionId'] = $value99['journalEntryAccountsID'];
                $incomingPayments[$key99]['incomingPaymentID'] = $value99['incomingPaymentIDNew'];
                $incomingPayments[$key99]['type'] = 'incoming-advanced-income-payments';
                $incomingPayments[$key99]['amount'] = $value99['incomingPaymentMethodAmount'];
            }


            //get outgoing glaccount advanced income payments
            $outgoingIncomePayments = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOutgoingPaymentForIncomeByGlAccountId($glAccountId,$startDate, $endDate);
            $outgoingIncomePayments = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'outgoing-advanced-income-payments';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($outgoingIncomePayments));

            //get account direct deposit je
            $otherDebitJE = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOtherJEDebitByGlAccountId($glAccountId,$startDate, $endDate);

            $otherDebitJE = array_map(function($transaction){

                $transaction['ref'] = "-";
                $transaction['bank'] = "-";
                $transaction['relatedCode'] = "-";
                if ($transaction['documentTypeID'] == '18') {
                    $debitNotePaymentData = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentIDForRec($transaction['journalEntryDocumentID'])->current();
                    // var_dump($debitNotePaymentData['debitNotePaymentCode']).die();
                    if ($debitNotePaymentData) {

                        switch ($debitNotePaymentData['paymentMethodID']) {
                            case '1':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '2':
                                $transaction['ref'] = (!empty($debitNotePaymentData['debitNotePaymentMethodReferenceNumber'])) ? $debitNotePaymentData['debitNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($debitNotePaymentData['debitNotePaymentMethodBank'])) ? $debitNotePaymentData['debitNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '4':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '5':
                                $transaction['ref'] = (!empty($debitNotePaymentData['debitNotePaymentMethodReferenceNumber'])) ? $debitNotePaymentData['debitNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($debitNotePaymentData['debitNotePaymentMethodBank'])) ? $debitNotePaymentData['debitNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '3':
                                $transaction['ref'] = (!empty($debitNotePaymentData['debitNotePaymentMethodReferenceNumber'])) ? $debitNotePaymentData['debitNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($debitNotePaymentData['debitNotePaymentMethodBank'])) ? $debitNotePaymentData['debitNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '6':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '7':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '8':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            
                            default:
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                # code...
                                break;
                        }
                    }
                }

                if ($transaction['documentTypeID'] == '10') {
                    $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($grnData) {
                       $transaction['relatedCode'] = (!empty($grnData['grnCode'])) ? $grnData['grnCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '16') {
                    $adjustmentDataData = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getAdjustmentByAdjustmentIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($adjustmentDataData) {
                       $transaction['relatedCode'] = (!empty($adjustmentDataData['goodsIssueCode'])) ? $adjustmentDataData['goodsIssueCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '13') {
                    $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($debitNoteData) {
                       $transaction['relatedCode'] = (!empty($debitNoteData['debitNoteCode'])) ? $debitNoteData['debitNoteCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '20') {
                    $pvData = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPVByPvIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($pvData) {
                       $transaction['relatedCode'] = (!empty($pvData['paymentVoucherCode'])) ? $pvData['paymentVoucherCode'] : '-';
                    }
                }

                // if ($transaction['documentTypeID'] == '27') {
                //     $pettyCashFloatData = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getPettyCasshFloatByPettyCasshFloatIDForRec($transaction['journalEntryDocumentID'])->current();
                //     if ($pettyCashFloatData) {
                //         $config = $this->getServiceLocator()->get('config');
                //         $PettyCashFloatCodePrefix = $config['PettyCashFloatCodePrefix'];
                //         $transaction['relatedCode'] = (!empty($pettyCashFloatData['pettyCashFloatID'])) ? $PettyCashFloatCodePrefix.$pettyCashFloatData['pettyCashFloatID'] : '-';
                //     }
                // }

                if ($transaction['documentTypeID'] == '28') {
                    $pettyCashVoucherData = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCasshVoucherByPettyCasshVoucherIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($pettyCashVoucherData) {
                       $transaction['relatedCode'] = (!empty($pettyCashVoucherData['pettyCashVoucherNo'])) ? $pettyCashVoucherData['pettyCashVoucherNo'] : '-';
                    }
                }

                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'other-journal-entry-deposit';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($otherDebitJE));

            //get account direct withdrawal je
            $otherCreditJE = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOtherJECreditByGlAccountId($glAccountId,$startDate, $endDate);
            $otherCreditJE = array_map(function($transaction){
                $transaction['ref'] = "-";
                $transaction['bank'] = "-";
                $transaction['relatedCode'] = "-";
                if ($transaction['documentTypeID'] == '17') {
                    $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentIDForRec($transaction['journalEntryDocumentID'])->current();

                    if ($creditNotePaymentData) {

                        switch ($creditNotePaymentData['paymentMethodID']) {
                            case '1':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '2':
                                $transaction['ref'] = (!empty($creditNotePaymentData['creditNotePaymentMethodReferenceNumber'])) ? $creditNotePaymentData['creditNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($creditNotePaymentData['creditNotePaymentMethodBank'])) ? $creditNotePaymentData['creditNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '3':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '4':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '5':
                                $transaction['ref'] = (!empty($creditNotePaymentData['creditNotePaymentMethodReferenceNumber'])) ? $creditNotePaymentData['creditNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($creditNotePaymentData['creditNotePaymentMethodBank'])) ? $creditNotePaymentData['creditNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '6':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '7':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '8':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            default:
                                # code...
                                break;
                        }
                    }
                }

               if ($transaction['documentTypeID'] == '10') {
                    $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($grnData) {
                       $transaction['relatedCode'] = (!empty($grnData['grnCode'])) ? $grnData['grnCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '16') {
                    $adjustmentDataData = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getAdjustmentByAdjustmentIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($adjustmentDataData) {
                       $transaction['relatedCode'] = (!empty($adjustmentDataData['goodsIssueCode'])) ? $adjustmentDataData['goodsIssueCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '13') {
                    $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($debitNoteData) {
                       $transaction['relatedCode'] = (!empty($debitNoteData['debitNoteCode'])) ? $debitNoteData['debitNoteCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '20') {
                    $pvData = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPVByPvIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($pvData) {
                       $transaction['relatedCode'] = (!empty($pvData['paymentVoucherCode'])) ? $pvData['paymentVoucherCode'] : '-';
                    }
                }

                // if ($transaction['documentTypeID'] == '27') {
                //     $pettyCashFloatData = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getPettyCasshFloatByPettyCasshFloatIDForRec($transaction['journalEntryDocumentID'])->current();
                //     if ($pettyCashFloatData) {
                //         $config = $this->getServiceLocator()->get('config');
                //         $PettyCashFloatCodePrefix = $config['PettyCashFloatCodePrefix'];
                //         $transaction['relatedCode'] = (!empty($pettyCashFloatData['pettyCashFloatID'])) ? $PettyCashFloatCodePrefix.$pettyCashFloatData['pettyCashFloatID'] : '-';
                //     }
                // }

                if ($transaction['documentTypeID'] == '28') {
                    $pettyCashVoucherData = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCasshVoucherByPettyCasshVoucherIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($pettyCashVoucherData) {
                       $transaction['relatedCode'] = (!empty($pettyCashVoucherData['pettyCashVoucherNo'])) ? $pettyCashVoucherData['pettyCashVoucherNo'] : '-';
                    }
                }

                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'other-journal-entry-withrawal';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($otherCreditJE));

            //merge arrays
            $result = array_merge( $transaction, $depositedCheques, $issuedCheques, $adPayissuedCheques, $bankOutgoingTransfer, $adPaybankOutgoingTransfer, $bankIncomingTransfer, $bankWithdrawal, $bankdeposit, $accountIncomingTranfer, $accountOutgoingTransfer, $outgoingAdvancedPayments,$incomingAdvancedPayments, $deposit, $withdrwal, $directDebitJE, $directCreditJE, $incomingPayments, $outgoingIncomePayments, $otherDebitJE, $otherCreditJE);
            
            $view = new ViewModel(array('transactions' => $result, 'account' => $account, 'validate' => true));
            $view->setTerminal(true);
            $view->setTemplate('expenses/reconciliation-api/transaction-list');
            return $view;
        }
    }

    /**
     * Update Cheque ReconciliationStatus
     * @return JsonModel
     */
    public function changeReconciliationStatusAction() {

        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {

            $chequeList = $this->getRequest()->getPost('chequeList');
            $status = $this->getRequest()->getPost('status');
            $accountId = $this->getRequest()->getPost('accountId');
            $accountBalance = $this->getRequest()->getPost('accountBalance');

            try {
                //begin transaction
                $this->beginTransaction();

                $result = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')
                        ->updateMultipleIncomingPaymentMethodCheque(array('incomingPaymentMethodChequeReconciliationStatus' => $status), $chequeList);

                if ($result) {
                    //update account balance
                    $this->CommonTable('Expenses\Model\AccountTable')->updateAccount(array('accountBalance' => $accountBalance), $accountId);
                    $this->updateEntity($postData['entityId']);
                    //commit transaction
                    $this->commit();

                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_RECONCILIATION');
                }
            } catch (\Exception $e) {
                //rollback transaction
                $this->rollback();
                $this->msg = $this->getMessage('ERR_RECONCILIATION');
            }
        }

        return $this->JSONRespond();
    }
    
    /**
     * Create Reconciliation
     * @return JsonModel
     */
    function createAction() {
        
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {

            $postData = $this->getRequest()->getPost()->toArray();
            try {
                //begin transaction
                $this->beginTransaction();


                if (isset($postData['reconciliationEditID']) && $postData['reconciliationEditID'] > 0) {
                    $reconcileEditData = $this->CommonTable('Expenses\Model\ReconciliationTable')->getReconciliationWithAccountByReconciliationId($postData['reconciliationEditID']);     

                    $postData['startDate'] = $reconcileEditData['reconciliationStartDate'];             
                    $postData['endDate'] = $reconcileEditData['reconciliationEndDate'];             
                    $postData['accountId'] = $reconcileEditData['accountId'];   


                    $reconcileEditData = $this->CommonTable('Expenses\Model\ReconciliationTable')->deleteReconciliationByID($postData['reconciliationEditID']);   

                    $reconcileEditData = $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->deleteReconciliationDetailByID($postData['reconciliationEditID']);  
          
                }

                $reconciliation = new Reconciliation();
                $reconciliation->reconciliationAmount = $postData['amount'];
                $reconciliation->reconciliationComment = $postData['comment'];
                $reconciliation->reconciliationStatementBalance = $postData['statementBalance'];
                $reconciliation->reconciliationStartDate = $postData['startDate'];
                $reconciliation->reconciliationEndDate = $postData['endDate'];
                $reconciliation->reconciliationStatus = 1;
                $reconciliation->accountId = $postData['accountId'];
                $reconciliation->entityId = $this->createEntity();
                $result = $this->CommonTable('Expenses\Model\ReconciliationTable')->save($reconciliation);
                if($result){
                    $transactions = $postData['transactions'];//get all the transaction inside the reconsolation
                    if (empty($transactions)) {
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_RECONCILIATION_SELECT_TRANSACTIONS');
                        return $this->JSONRespond();
                    }                            
                    foreach ($transactions as $key => $type) {//get transaction types
                        foreach ($type as $transactionData) {//get single transaction
                            $transaction = array();
                            $transaction['reconciliationId'] = $result;
                            switch ($key) {
                                case 'chequeDeposit':
                                    $transaction['incomingPaymentMethodChequeId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'cheque-deposit';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->updateIncomingPaymentMethodCheque(array('incomingPaymentMethodChequeReconciliationStatus'=>1),$transactionData['id']);
                                    break;
                                
                                case 'chequeIssue':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = abs($transactionData['amount']);
                                    $transaction['reconciliationTransactionType'] = 'cheque-issue';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('SupplierPaymentMethodNumbersTable')->updatePaymentMethodsNumbers(array('outGoingPaymentMethodReconciliationStatus'=>1,'postdatedChequeStatus'=>0),$transactionData['id']);
                                    //updated postdated cheque notification status
                                    $this->CommonTable('Core\Model\NotificationTable')->updateNotification(array('status'=>'1'),$transactionData['id'],'7');
                                    break;
                                
                                case 'bankOutgoingTransfer':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = abs($transactionData['amount']);
                                    $transaction['reconciliationTransactionType'] = 'outgoing-bank-transfer';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('SupplierPaymentMethodNumbersTable')->updatePaymentMethodsNumbers(array('outGoingPaymentMethodReconciliationStatus'=>1),$transactionData['id']);
                                    break;
                                
                                case 'bankIncomingTransfer':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'incoming-bank-transfer';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->updateBankTransfer(array('incomingPaymentMethodBankTransferReconciliationStatus'=>1),$transactionData['id']);
                                    break;
                                
                                case 'bankWithdrawal':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'bank-withrawal';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'bankDeposit':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'bank-deposit';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                case 'jeWithdrawal':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'direct-journal-entry-withrawal';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'jeDiposit':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'direct-journal-entry-deposit';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                case 'otherJeWithdrawal':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'other-journal-entry-withrawal';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'otherJeDiposit':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'other-journal-entry-deposit';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                case 'outgoingAdvancedIncomePayments':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'outgoing-advanced-income-payments';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'incomingAdvancedIncomePayments':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'incoming-advanced-income-payments';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'withdrawal':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'withdrwal';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'deposit':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'deposit';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'accountIncomingTranfer':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'account-incoming-transfer';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'accountOutgoingTransfer':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'account-outgoing-transfer';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'incomingAdvancedPayments':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'incoming-payments';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                case 'outgoingAdvancedPayments':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'outgoing-payments';
                                    //update transaction type reconciliation status
                                    $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>true),$transactionData['id']);
                                    break;
                                
                                default:
                                    error_log('invalid option');
                            }
                            //save transaction
                            $transaction['entityId'] = $this->createEntity();
                            $recTrans = new ReconciliationTransaction();
                            $recTrans->exchangeArray($transaction);
                            $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->save($recTrans);
                        }
                    }
                }
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_RECONCILIATION_SAVE');
                //commit transaction
                $this->commit();
                
            } catch (\Exception $e) {
                //rollback transaction
                $this->rollback();
                $this->msg = $this->getMessage('ERR_RECONCILIATION');
            }
        }
        return $this->JSONRespond();
    }
    
    /**
     * Get all reconciliations 
     * @return ViewModel
     */
    function getReconciliationsAction()
    {   
        if ($this->getRequest()->isPost())
        {
            $accountId = $this->getRequest()->getPost('accountId');
            $startDate = $this->getRequest()->getPost('startDate');
            $endDate = $this->getRequest()->getPost('endDate');
            
            $result = $this->CommonTable('Expenses\Model\ReconciliationTable')
                            ->getReconciliationsByAccountId( $accountId, null, $startDate, $endDate, false, array('reconciliation.reconciliationId DESC'));
            
            $view = new ViewModel(array('reconciliations' => $result));
            $view->setTerminal(true);
            $view->setTemplate('expenses/reconciliation-api/reconciliation-list');
            return $view;
        }
    }
    
    /**
     * Cancel Reconciliation
     * @return JsonModel
     */
    public function cancelAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()){
            $reconciliationId = $this->getRequest()->getPost('reconciliationId');
            
            if($reconciliationId){
                
                $reconciliation = $this->CommonTable('Expenses\Model\ReconciliationTable')->getReconciliationByReconciliationId($reconciliationId);
                try {
                    //begin transaction
                    $this->beginTransaction();
                    //delete reconciliation
                    $this->updateDeleteInfoEntity($reconciliation['entityId']);
                    //get transactions
                    $transactions = $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->getNormalReconciliationTransactionByReconciliationId($reconciliationId);

                    foreach ($transactions as $transaction){
                        
                        $transactionId = $transaction['reconciliationTransactionId'];

                        //get transaction
                        $transaction = $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->getReconciliationTransactionByTransactionId($transactionId);
                        //delete transaction
                        $this->updateDeleteInfoEntity($transaction['entityId']);
                        switch ($transaction['reconciliationTransactionType']) {
                            case 'cheque-deposit':
                                //update transaction type reconciliation status
                                $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->updateIncomingPaymentMethodCheque(array('incomingPaymentMethodChequeReconciliationStatus'=>0),$transaction['incomingPaymentMethodChequeId']);
                                break;
                            case 'cheque-issue' :
                                //update transaction type reconciliation status
                                $this->CommonTable('SupplierPaymentMethodNumbersTable')->updatePaymentMethodsNumbers(array('outGoingPaymentMethodReconciliationStatus'=>0),$transaction['outGoingPaymentMethodsNumbersId']);
                                break;
                            case 'outgoing-bank-transfer':
                                //update transaction type reconciliation status
                                $this->CommonTable('SupplierPaymentMethodNumbersTable')->updatePaymentMethodsNumbers(array('outGoingPaymentMethodReconciliationStatus'=>0),$transaction['outGoingPaymentMethodsNumbersId']);
                                break;
                            case 'incoming-bank-transfer' :
                                //update transaction type reconciliation status
                                $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->updateBankTransfer(array('incomingPaymentMethodBankTransferReconciliationStatus'=>0),$transaction['incomingPaymentMethodBankTransferId']);
                                break;
                            case 'direct-journal-entry-deposit' :
                                //update transaction type reconciliation status
                                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['incomingPaymentMethodBankTransferId']);
                                break;
                            case 'direct-journal-entry-withrawal' :
                                //update transaction type reconciliation status
                                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['outGoingPaymentMethodsNumbersId']);
                                break;
                            case 'bank-deposit' :
                                //update transaction type reconciliation status
                                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['incomingPaymentMethodBankTransferId']);
                                break;
                            case 'withdrwal' :
                                //update transaction type reconciliation status
                                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['outGoingPaymentMethodsNumbersId']);
                                break;
                            case 'bank-withrawal' :
                                //update transaction type reconciliation status
                                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['outGoingPaymentMethodsNumbersId']);
                                break;
                            case 'other-journal-entry-deposit' :
                                //update transaction type reconciliation status
                                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['incomingPaymentMethodBankTransferId']);
                                break;
                            case 'other-journal-entry-withrawal' :
                                //update transaction type reconciliation status
                                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['outGoingPaymentMethodsNumbersId']);
                                break;
                            case 'incoming-payments' :
                                //update transaction type reconciliation status
                                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['incomingPaymentMethodBankTransferId']);
                                break;
                            default:
                                error_log('invalid option');
                        }    
                    }
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_RECONCILIATION_CANCEL');
                    //commit transaction
                    $this->commit();
                    
                } catch (\Exception $exc) {
                    //rollback transaction
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_RECONCILIATION_CANCEL');
                }
            }
        }
        
        return $this->JSONRespond();
    }
    
    /**
     * Get transaction list of given reconciliation 
     * @return ViewModel
     */
    function getTransactionsAction()
    {   
        if ($this->getRequest()->isPost())
        {
            $reconciliationId = $this->getRequest()->getPost('reconciliationId');
            $filter = $this->getRequest()->getPost('filter');
            $filter = (strlen($filter) == 0? null : $filter);
            
            if($reconciliationId){
                $transactions = $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->getTransactionsByReconciliationId( $reconciliationId,$filter);
            }
            $view = new ViewModel(array('transactions' => $transactions));
            $view->setTerminal(true);
            $view->setTemplate('expenses/reconciliation-api/reconciliation-transaction-list');
            return $view;
        }
    }
    
    /**
     * Update Reconciliation
     * @return JsonModel
     */
    public function updateAction()
    {
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;
        
        if ($this->getRequest()->isPost()){
            
            $postData = $this->getRequest()->getPost()->toArray();
            
            try {
                //begin transaction
                $this->beginTransaction();
                
                foreach ($postData['canceledTransactions'] as $transactionId){
                    //get transaction
                    $transaction = $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->getReconciliationTransactionByTransactionId($transactionId);
                    //delete transaction
                    $this->updateDeleteInfoEntity($transaction['entityId']);
                    switch ($transaction['reconciliationTransactionType']) {
                        case 'cheque-deposit':
                            //update transaction type reconciliation status
                            $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->updateIncomingPaymentMethodCheque(array('incomingPaymentMethodChequeReconciliationStatus'=>0),$transaction['incomingPaymentMethodChequeId']);
                            break;

                        case 'cheque-issue' :
                            //update transaction type reconciliation status
                            $this->CommonTable('SupplierPaymentMethodNumbersTable')->updatePaymentMethodsNumbers(array('outGoingPaymentMethodReconciliationStatus'=>0),$transaction['outGoingPaymentMethodsNumbersId']);
                            break;

                        case 'outgoing-bank-transfer':
                            //update transaction type reconciliation status
                            $this->CommonTable('SupplierPaymentMethodNumbersTable')->updatePaymentMethodsNumbers(array('outGoingPaymentMethodReconciliationStatus'=>0),$transaction['outGoingPaymentMethodsNumbersId']);
                            break;

                        case 'incoming-bank-transfer' :
                            //update transaction type reconciliation status
                            $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->updateBankTransfer(array('incomingPaymentMethodBankTransferReconciliationStatus'=>0),$transaction['incomingPaymentMethodBankTransferId']);
                            break;
                        case 'direct-journal-entry-deposit' :
                            //update transaction type reconciliation status
                            $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['incomingPaymentMethodBankTransferId']);
                            break;
                        case 'direct-journal-entry-withrawal' :
                            //update transaction type reconciliation status
                            $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['outGoingPaymentMethodsNumbersId']);
                            break;
                        case 'bank-deposit' :
                            //update transaction type reconciliation status
                            $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['incomingPaymentMethodBankTransferId']);
                            break;
                        case 'withdrwal' :
                            //update transaction type reconciliation status
                            $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['outGoingPaymentMethodsNumbersId']);
                            break;
                        case 'bank-withrawal' :
                            //update transaction type reconciliation status
                            $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['outGoingPaymentMethodsNumbersId']);
                            break;
                        case 'other-journal-entry-deposit' :
                            //update transaction type reconciliation status
                            $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['incomingPaymentMethodBankTransferId']);
                            break;
                        case 'other-journal-entry-withrawal' :
                            //update transaction type reconciliation status
                            $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['outGoingPaymentMethodsNumbersId']);
                            break;
                        case 'incoming-payments' :
                            //update transaction type reconciliation status
                            $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateReconcileStatus(array('isReconcile'=>0), $transaction['incomingPaymentMethodBankTransferId']);
                            break;
                        default:
                            error_log('invalid option');
                    }   
                }
                //get reconciliation
                $reconciliation = $this->CommonTable('Expenses\Model\ReconciliationTable')->getReconciliationByReconciliationId($postData['reconciliationId']);
                //update reconciliation amount
                $result = $this->CommonTable('Expenses\Model\ReconciliationTable')->updateReconciliation(array('reconciliationAmount'=>$postData['reconciliationAmount']),$postData['reconciliationId']);
                if($result){
                    $this->updateEntity($reconciliation['entityId']);
                    $this->commit();
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_RECONCILIATION_UPDATE');
                } else {
                    //rollback transaction
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_RECONCILIATION_UPDATE');
                }
                
            } catch (\Exception $exc) {
                //rollback transaction
                $this->rollback();
                $this->msg = $this->getMessage('ERR_RECONCILIATION_UPDATE');
            }
        }
        return $this->JSONRespond();
    }
    
    public function calculateTransactionAmountAction() {
        
        $this->status = false;
        $this->msg    = 'invalid request';
        $this->data   = null;
        
        if ($this->getRequest()->isPost()){
            
            $accountId = $this->getRequest()->getPost('accountId');
            $startDate = $this->getRequest()->getPost('date');
            $transaction = array();
            
            //get account by account id
            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
            
            if($account){
                try{             
                    //get reconciliation
                    $reconciliation = $this->CommonTable('Expenses\Model\ReconciliationTable')->getLastReconciliationByAccountId($accountId);
                    $statementBalance = ($reconciliation) ? $reconciliation['reconciliationStatementBalance']: 0.00;
                    
                    //get deposited cheques
                    $depositedCheques = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getDepositedChequeByAccountId( $accountId, null, $startDate);
                    $depositedCheques = array_map(function($transaction){
                        $transaction['type'] = 'cheque-deposit';
                        $transaction['amount'] = $transaction['incomingPaymentMethodAmount'];
                        return $transaction;
                    }, iterator_to_array($depositedCheques));

                    //get issued cheques
                    $issuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId( $accountId, null, $startDate);
                    $issuedCheques = array_map(function($transaction){
                        $transaction['type'] = 'cheque-issue';
                        $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                        return $transaction;
                    }, iterator_to_array($issuedCheques));
                    
                    //get advanced payment issued cheques
                    $adPayissuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId( $accountId, null, $startDate);
                    $adPayissuedCheques = array_map(function($transaction){
                        $transaction['type'] = 'cheque-issue';
                        $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                        return $transaction;
                    }, iterator_to_array($adPayissuedCheques));
                    
                    //get outgoing bank transfer
                    $bankOutgoingTransfer = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getOutgoingBankTransfersByAccountId( $accountId, null, $startDate);
                    $bankOutgoingTransfer = array_map(function($transaction){
                        $transaction['type'] = 'outgoing-bank-transfer';
                        $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                        return $transaction;
                    }, iterator_to_array($bankOutgoingTransfer));
                    
                    //get advanced payment bank transfer
                    $adPaybankOutgoingTransfer = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentOutgoingBankTransfersByAccountId( $accountId, null, $startDate);
                    $adPaybankOutgoingTransfer = array_map(function($transaction){
                        $transaction['type'] = 'outgoing-bank-transfer';
                        $transaction['amount'] = $transaction['outgoingPaymentAmount'];
                        return $transaction;
                    }, iterator_to_array($adPaybankOutgoingTransfer));

                    //get incoming bank transfer
                    $bankIncomingTransfer = $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->getBankTransfersByAccountId( $accountId, null, $startDate);
                    $bankIncomingTransfer = array_map(function($transaction){
                        $transaction['type'] = 'incoming-bank-transfer';
                        $transaction['amount'] = $transaction['incomingPaymentMethodAmount'];
                        return $transaction;
                    }, iterator_to_array($bankIncomingTransfer));

                    //merge arrays
                    $results = array_merge( $transaction, $depositedCheques, $issuedCheques, $adPayissuedCheques, $bankOutgoingTransfer, $adPaybankOutgoingTransfer, $bankIncomingTransfer);
                    $amount = 0;
                    foreach ($results as $r){
                        switch ($r['type']){
                            case 'cheque-deposit':
                                $amount = $amount + $r['amount'];
                                break;
                            case 'cheque-issue':
                                $amount = $amount - $r['amount'];
                                break;
                            case 'outgoing-bank-transfer':
                                $amount = $amount - $r['amount'];
                                break;
                            case 'incoming-bank-transfer':
                                $amount = $amount + $r['amount'];
                                break;
                            default :
                                error_log('invalid option');
                        }
                    }
                    $accountBalance = $account['accountBalance'] - $amount;                  
                    
                    $this->status = true;
                    $this->msg = 'transaction amount calculated';
                    $this->data = array('amount'=>$accountBalance,'statementBalance'=>$statementBalance);

                } catch (\Exception $ex){
                    $this->msg = $ex->getMessage();
                }
            }
            
        }
        return $this->JSONRespond();
        
    }

    public function createDraftAction() {
        
        $this->status = false;
        $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data = null;

        if ($this->getRequest()->isPost()) {

            $postData = $this->getRequest()->getPost()->toArray();
            try {
                //begin transaction
                $this->beginTransaction();

                if (isset($postData['reconciliationEditID']) && $postData['reconciliationEditID'] > 0) {
                    $reconcileEditData = $this->CommonTable('Expenses\Model\ReconciliationTable')->getReconciliationWithAccountByReconciliationId($postData['reconciliationEditID']);     

                    $postData['startDate'] = $reconcileEditData['reconciliationStartDate'];             
                    $postData['endDate'] = $reconcileEditData['reconciliationEndDate'];             
                    $postData['accountId'] = $reconcileEditData['accountId'];   


                    $reconcileEditData = $this->CommonTable('Expenses\Model\ReconciliationTable')->deleteReconciliationByID($postData['reconciliationEditID']);   

                    $reconcileEditData = $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->deleteReconciliationDetailByID($postData['reconciliationEditID']);  
          
                }

                $reconciliation = new Reconciliation();
                $reconciliation->reconciliationAmount = $postData['amount'];
                $reconciliation->reconciliationComment = $postData['comment'];
                $reconciliation->reconciliationStatementBalance = $postData['statementBalance'];
                $reconciliation->accountId = $postData['accountId'];
                $reconciliation->reconciliationStartDate = $this->convertDateToStandardFormat($postData['startDate']);
                $reconciliation->reconciliationEndDate = $this->convertDateToStandardFormat($postData['endDate']);
                $reconciliation->reconciliationStatus = 0;
                $reconciliation->entityId = $this->createEntity();
                $result = $this->CommonTable('Expenses\Model\ReconciliationTable')->save($reconciliation);
                if($result){
                    $transactions = $postData['transactions'];//get all the transaction inside the reconsolation
                    if (empty($transactions)) {
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_RECONCILIATION_SELECT_TRANSACTIONS');
                        return $this->JSONRespond();
                    }                            
                    foreach ($transactions as $key => $type) {//get transaction types
                        foreach ($type as $transactionData) {//get single transaction
                            $transaction = array();
                            $transaction['reconciliationId'] = $result;
                            switch ($key) {
                                case 'chequeDeposit':
                                    $transaction['incomingPaymentMethodChequeId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'cheque-deposit';
                                    break;
                                
                                case 'chequeIssue':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = abs($transactionData['amount']);
                                    $transaction['reconciliationTransactionType'] = 'cheque-issue';
                                    break;
                                
                                case 'bankOutgoingTransfer':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = abs($transactionData['amount']);
                                    $transaction['reconciliationTransactionType'] = 'outgoing-bank-transfer';
                                    break;
                                
                                case 'bankIncomingTransfer':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'incoming-bank-transfer';
                                    break;
                                
                                case 'bankWithdrawal':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'bank-withrawal';
                                    break;
                                
                                case 'bankDeposit':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'bank-deposit';
                                    break;
                                case 'jeWithdrawal':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'direct-journal-entry-withrawal';
                                    break;
                                
                                case 'jeDiposit':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'direct-journal-entry-deposit';
                                    break;
                                case 'otherJeWithdrawal':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'other-journal-entry-withrawal';
                                    break;
                                
                                case 'otherJeDiposit':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'other-journal-entry-deposit';
                                    break;
                                case 'outgoingAdvancedIncomePayments':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'outgoing-advanced-income-payments';
                                    break;
                                
                                case 'incomingAdvancedIncomePayments':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'incoming-advanced-income-payments';
                                    break;
                                
                                case 'withdrawal':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'withdrwal';
                                    break;
                                
                                case 'deposit':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'deposit';
                                    break;
                                
                                case 'accountIncomingTranfer':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'account-incoming-transfer';
                                    break;
                                
                                case 'accountOutgoingTransfer':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'account-outgoing-transfer';
                                    break;
                                
                                case 'incomingAdvancedPayments':
                                    $transaction['incomingPaymentMethodBankTransferId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionCreditAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'incoming-payments';
                                    break;
                                
                                case 'outgoingAdvancedPayments':
                                    $transaction['outGoingPaymentMethodsNumbersId'] = $transactionData['id'];
                                    $transaction['reconciliationTransactionDebitAmount'] = $transactionData['amount'];
                                    $transaction['reconciliationTransactionType'] = 'outgoing-payments';
                                    break;
                                
                                default:
                                    error_log('invalid option');
                            }
                            //save transaction
                            $transaction['entityId'] = $this->createEntity();
                            $recTrans = new ReconciliationTransaction();
                            $recTrans->exchangeArray($transaction);
                            $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->save($recTrans);
                        }
                    }
                }
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_RECONCILIATION_DRAFT_SAVE');
                //commit transaction
                $this->commit();
                
            } catch (\Exception $e) {
                //rollback transaction
                $this->rollback();
                $this->msg = $this->getMessage('ERR_RECONCILIATION');
            }
        }
        return $this->JSONRespond();
    }

     public function getAccountTransactionsByDraftAction() {
        if ($this->getRequest()->isPost()) {
            $transaction = array();
            $reconciliationId = $this->getRequest()->getPost('reconciliationId');

            $result = $this->CommonTable('Expenses\Model\ReconciliationTable')->getReconciliationWithAccountByReconciliationId($reconciliationId);

            $accountId = $result['accountId'];
            $startDate = null;
            $endDate = $result['reconciliationEndDate'];
            //get account by account id
            $account = $this->CommonTable('Expenses\Model\AccountTable')->getAccountByAccountId($accountId);
            $glAccountId = $account['financeAccountID'];
            $newDate = date('Y-m-d', strtotime($endDate . ' +1 day'));

            $jEDataForOpening = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->getJournalEntryTotalCreditAndDebitByAccountID($glAccountId, '', $endDate);

            $incomingBankTransferData = $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->getBankTransfersByAccountId($accountId,null,$startDate,$endDate);

            $outgoingBankTransferData = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentOutgoingBankTransfersByAccountId( $accountId, null, $startDate, $endDate);

            $issedChequesData = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId($accountId,null,$startDate,$endDate);

            $adPayissuedChequesData = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId( $accountId, null, $startDate, $endDate);

            $issuedChequesAmount = 0;
            foreach ($issedChequesData as $key22 => $value22) {
                if ($value22['outGoingPaymentMethodFinanceAccountID'] != $glAccountId) {
                    $issuedChequesAmount += floatval($value22['outGoingPaymentMethodPaidAmount']);
                }
            }

            $adissuedChequesAmount = 0;
            foreach ($adPayissuedChequesData as $key33 => $value33) {
                if ($value33['outGoingPaymentMethodFinanceAccountID'] != $glAccountId) {
                    $adissuedChequesAmount += floatval($value33['outGoingPaymentMethodPaidAmount']);
                }
            }


            $outgoingTransferAmount = 0;
            foreach ($outgoingBankTransferData as $key77 => $value77) {
                if ($value77['outGoingPaymentMethodFinanceAccountID'] != $glAccountId) {
                    $outgoingTransferAmount += floatval($value77['outGoingPaymentMethodPaidAmount']);
                }
            }


            $incomingTranserAmount = 0;
            foreach ($incomingBankTransferData as $key66 => $value66) {

                $incomeData = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getInvoicePaymentByPaymentId($value66['incomingPaymentId'])->current();

                if ($incomeData && !empty($incomeData['incomeID'])) {

                    $journalEntryDataForIncome = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('43', $incomeData['incomeID']);

                    $journalEntryAccountDataForIncome = $this->CommonTable('JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryIDAndAccountID( $journalEntryDataForIncome['journalEntryID'], $glAccountId)->current();

                    if (!$journalEntryAccountDataForIncome) {
                        $incomingTranserAmount += floatval($value66['incomingPaymentMethodAmount']);
                    }
                } else {
                    $journalEntryDataForInvoice = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('7', $value66['incomingPaymentId']);

                    $journalEntryAccountDataForInvoice = $this->CommonTable('JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryIDAndAccountID( $journalEntryDataForInvoice['journalEntryID'], $glAccountId)->current();

                    if (!$journalEntryAccountDataForInvoice) {
                        $incomingTranserAmount += floatval($value66['incomingPaymentMethodAmount']);
                    }
                }
            }

            $account['actualAccBalance'] = (floatval($jEDataForOpening['totalDebitAmount']) + $incomingTranserAmount) - (floatval($jEDataForOpening['totalCreditAmount']) +  $issuedChequesAmount + $adissuedChequesAmount + $outgoingTransferAmount);

            //get deposited cheques
            $depositedCheques = $this->CommonTable('Expenses\Model\ChequeDepositTable')->getDepositedChequeByAccountId( $accountId, 0, $startDate, $endDate);
            $depositedCheques = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['incomingPaymentMethodChequeId'];
                $transaction['type'] = 'cheque-deposit';
                $transaction['amount'] = $transaction['incomingPaymentMethodAmount']/$transaction['chequeDepositAccountCurrencyRate'];
                return $transaction;
            }, iterator_to_array($depositedCheques));
            
            //get invoice payment issued cheques
            $issuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getIssuedChequesByAccountId( $accountId, 0, $startDate, $endDate);
            $issuedCheques = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['outGoingPaymentMethodsNumbersID'];
                $transaction['type'] = 'cheque-issue';
                $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                return $transaction;
            }, iterator_to_array($issuedCheques));
            
            //get advanced payment issued cheques
            $adPayissuedCheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentIssuedChequesByAccountId( $accountId, 0, $startDate, $endDate);
            $adPayissuedCheques = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['outGoingPaymentMethodsNumbersID'];
                $transaction['type'] = 'cheque-issue';
                $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                return $transaction;
            }, iterator_to_array($adPayissuedCheques));
            
            //get invoice outgoing bank transfer
            $bankOutgoingTransfer = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getOutgoingBankTransfersByAccountId( $accountId, 0, $startDate, $endDate);
            $bankOutgoingTransfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['outGoingPaymentMethodsNumbersID'];
                $transaction['type'] = 'outgoing-bank-transfer';
                $transaction['amount'] = $transaction['outgoingInvoiceCashAmount'];
                return $transaction;
            }, iterator_to_array($bankOutgoingTransfer));
            
            //get advanced payment bank transfer
            $adPaybankOutgoingTransfer = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getAdvancedPaymentOutgoingBankTransfersByAccountId( $accountId, 0, $startDate, $endDate);
            $adPaybankOutgoingTransfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['outGoingPaymentMethodsNumbersID'];
                $transaction['type'] = 'outgoing-bank-transfer';
                $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                return $transaction;
            }, iterator_to_array($adPaybankOutgoingTransfer));
                        
            //get incoming bank transfer
            $bankIncomingTransfer = $this->CommonTable('Invoice\Model\IncomingPaymentMethodBankTransferTable')->getBankTransfersByAccountId($accountId,0,$startDate,$endDate);
            $bankIncomingTransfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['incomingPaymentMethodBankTransferId'];
                $transaction['type'] = 'incoming-bank-transfer';
                $transaction['amount'] = $transaction['incomingPaymentMethodAmount'];
                return $transaction;
            }, iterator_to_array($bankIncomingTransfer));

            //get account direct withrawal
            $bankWithdrawal = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDirectWithdrawalByGlAccountId($glAccountId,$startDate, $endDate);
            $bankWithdrawal = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'bank-withrawal';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($bankWithdrawal));

            //get account direct deposit je
            $directDebitJE = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDirectJEDebitByGlAccountId($glAccountId,$startDate, $endDate);
            $directDebitJE = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'direct-journal-entry-deposit';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($directDebitJE));

            //get account direct withdrawal je
            $directCreditJE = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDirectJECreditByGlAccountId($glAccountId,$startDate, $endDate);
            $directCreditJE = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'direct-journal-entry-withrawal';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($directCreditJE));

        
            //get account direct deposit
            $bankdeposit = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDirectDepositByGlAccountId($glAccountId,$startDate, $endDate);
            $bankdeposit = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'bank-deposit';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($bankdeposit));
           
            //get account deposit by a withdrwal
            $deposit = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getDepositByGlAccountId($glAccountId,$startDate, $endDate);
            $deposit = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'deposit';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($deposit));

            //get account withdrwal by a deposit
            $withdrwal = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getWithdrawalByGlAccountId($glAccountId,$startDate, $endDate);
            $withdrwal = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'withdrwal';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($withdrwal));
           
            //get account incoming transfer
            $accountIncomingTranfer = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getIncomingBankTransfersByAccountId($glAccountId,$startDate, $endDate);
            $accountIncomingTranfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'account-incoming-transfer';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($accountIncomingTranfer));

            //get account outgoing transfer
            $accountOutgoingTransfer = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOutgoingBankTransfersByAccountId($glAccountId,$startDate, $endDate);
            $accountOutgoingTransfer = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'account-outgoing-transfer';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($accountOutgoingTransfer));
            

            //get outgoing glaccount advanced payments
            $outgoingAdvancedPayments = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOutgoingAdvancedPaymentByGlAccountId($glAccountId,$startDate, $endDate);
            $outgoingAdvancedPayments = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'outgoing-advanced-payments';
                $transaction['amount'] = $transaction['outGoingPaymentMethodPaidAmount'];
                return $transaction;
            }, iterator_to_array($outgoingAdvancedPayments));

           //get incoming glaccount advanced payments
            $incomingAdvancedPayments = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getCustomerPaymentJournalEntryAccountsByAccountIDAndDateRange( $glAccountId,$startDate, $endDate);

            foreach ($incomingAdvancedPayments as $key88 => $value88) {
                $incomingAdvancedPayments[$key88]['transactionId'] = $value88['journalEntryAccountsID'];
                $incomingAdvancedPayments[$key88]['type'] = 'incoming-advanced-payments';
                $incomingAdvancedPayments[$key88]['amount'] = $value88['incomingPaymentMethodAmount'];
            }


            //get incoming glaccount advanced income payments
            $incomingPayments = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getIncomePaymentJournalEntryAccountsByAccountIDAndDateRange( $glAccountId,$startDate, $endDate);

            foreach ($incomingPayments as $key99 => $value99) {
                $incomingPayments[$key99]['transactionId'] = $value99['journalEntryAccountsID'];
                $incomingPayments[$key99]['type'] = 'incoming-advanced-income-payments';
                $incomingPayments[$key99]['amount'] = $value99['incomingPaymentMethodAmount'];
            }


            //get outgoing glaccount advanced income payments
            $outgoingIncomePayments = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOutgoingPaymentForIncomeByGlAccountId($glAccountId,$startDate, $endDate);
            $outgoingIncomePayments = array_map(function($transaction){
                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'outgoing-advanced-income-payments';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($outgoingIncomePayments));

            //get account direct deposit je
            $otherDebitJE = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOtherJEDebitByGlAccountId($glAccountId,$startDate, $endDate);

            $otherDebitJE = array_map(function($transaction){

                $transaction['ref'] = "-";
                $transaction['bank'] = "-";
                $transaction['relatedCode'] = "-";
                if ($transaction['documentTypeID'] == '18') {
                    $debitNotePaymentData = $this->CommonTable('Inventory\Model\DebitNotePaymentTable')->getDebitNotePaymentByDebitNotePaymentIDForRec($transaction['journalEntryDocumentID'])->current();
                    // var_dump($debitNotePaymentData['debitNotePaymentCode']).die();
                    if ($debitNotePaymentData) {

                        switch ($debitNotePaymentData['paymentMethodID']) {
                            case '1':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '2':
                                $transaction['ref'] = (!empty($debitNotePaymentData['debitNotePaymentMethodReferenceNumber'])) ? $debitNotePaymentData['debitNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($debitNotePaymentData['debitNotePaymentMethodBank'])) ? $debitNotePaymentData['debitNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '4':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '5':
                                $transaction['ref'] = (!empty($debitNotePaymentData['debitNotePaymentMethodReferenceNumber'])) ? $debitNotePaymentData['debitNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($debitNotePaymentData['debitNotePaymentMethodBank'])) ? $debitNotePaymentData['debitNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '3':
                                $transaction['ref'] = (!empty($debitNotePaymentData['debitNotePaymentMethodReferenceNumber'])) ? $debitNotePaymentData['debitNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($debitNotePaymentData['debitNotePaymentMethodBank'])) ? $debitNotePaymentData['debitNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '6':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '7':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            case '8':
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                break;
                            
                            default:
                                $transaction['relatedCode'] = (!empty($debitNotePaymentData['debitNotePaymentCode'])) ? $debitNotePaymentData['debitNotePaymentCode'] : '-';
                                # code...
                                break;
                        }
                    }
                }

                if ($transaction['documentTypeID'] == '10') {
                    $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($grnData) {
                       $transaction['relatedCode'] = (!empty($grnData['grnCode'])) ? $grnData['grnCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '16') {
                    $adjustmentDataData = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getAdjustmentByAdjustmentIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($adjustmentDataData) {
                       $transaction['relatedCode'] = (!empty($adjustmentDataData['goodsIssueCode'])) ? $adjustmentDataData['goodsIssueCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '13') {
                    $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($debitNoteData) {
                       $transaction['relatedCode'] = (!empty($debitNoteData['debitNoteCode'])) ? $debitNoteData['debitNoteCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '20') {
                    $pvData = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPVByPvIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($pvData) {
                       $transaction['relatedCode'] = (!empty($pvData['paymentVoucherCode'])) ? $pvData['paymentVoucherCode'] : '-';
                    }
                }

                // if ($transaction['documentTypeID'] == '27') {
                //     $pettyCashFloatData = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getPettyCasshFloatByPettyCasshFloatIDForRec($transaction['journalEntryDocumentID'])->current();
                //     if ($pettyCashFloatData) {
                //         $config = $this->getServiceLocator()->get('config');
                //         $PettyCashFloatCodePrefix = $config['PettyCashFloatCodePrefix'];
                //         $transaction['relatedCode'] = (!empty($pettyCashFloatData['pettyCashFloatID'])) ? $PettyCashFloatCodePrefix.$pettyCashFloatData['pettyCashFloatID'] : '-';
                //     }
                // }

                if ($transaction['documentTypeID'] == '28') {
                    $pettyCashVoucherData = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCasshVoucherByPettyCasshVoucherIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($pettyCashVoucherData) {
                       $transaction['relatedCode'] = (!empty($pettyCashVoucherData['pettyCashVoucherNo'])) ? $pettyCashVoucherData['pettyCashVoucherNo'] : '-';
                    }
                }

                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'other-journal-entry-deposit';
                $transaction['amount'] = $transaction['journalEntryAccountsDebitAmount'];
                return $transaction;
            }, iterator_to_array($otherDebitJE));

            //get account direct withdrawal je
            $otherCreditJE = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getOtherJECreditByGlAccountId($glAccountId,$startDate, $endDate);
            $otherCreditJE = array_map(function($transaction){
                $transaction['ref'] = "-";
                $transaction['bank'] = "-";
                $transaction['relatedCode'] = "-";
                if ($transaction['documentTypeID'] == '17') {
                    $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentIDForRec($transaction['journalEntryDocumentID'])->current();

                    if ($creditNotePaymentData) {

                        switch ($creditNotePaymentData['paymentMethodID']) {
                            case '1':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '2':
                                $transaction['ref'] = (!empty($creditNotePaymentData['creditNotePaymentMethodReferenceNumber'])) ? $creditNotePaymentData['creditNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($creditNotePaymentData['creditNotePaymentMethodBank'])) ? $creditNotePaymentData['creditNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '3':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '4':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '5':
                                $transaction['ref'] = (!empty($creditNotePaymentData['creditNotePaymentMethodReferenceNumber'])) ? $creditNotePaymentData['creditNotePaymentMethodReferenceNumber'] : '-';
                                $transaction['bank'] = (!empty($creditNotePaymentData['creditNotePaymentMethodBank'])) ? $creditNotePaymentData['creditNotePaymentMethodBank'] : '-';
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '6':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '7':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            case '8':
                                $transaction['relatedCode'] = (!empty($creditNotePaymentData['creditNotePaymentCode'])) ? $creditNotePaymentData['creditNotePaymentCode'] : '-';
                                break;
                            default:
                                # code...
                                break;
                        }
                    }
                }

               if ($transaction['documentTypeID'] == '10') {
                    $grnData = $this->CommonTable('Inventory\Model\GrnTable')->getGrnByGrnIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($grnData) {
                       $transaction['relatedCode'] = (!empty($grnData['grnCode'])) ? $grnData['grnCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '16') {
                    $adjustmentDataData = $this->CommonTable('Inventory\Model\GoodsIssueTable')->getAdjustmentByAdjustmentIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($adjustmentDataData) {
                       $transaction['relatedCode'] = (!empty($adjustmentDataData['goodsIssueCode'])) ? $adjustmentDataData['goodsIssueCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '13') {
                    $debitNoteData = $this->CommonTable('Inventory\Model\DebitNoteTable')->getDebitNoteByDebitNoteIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($debitNoteData) {
                       $transaction['relatedCode'] = (!empty($debitNoteData['debitNoteCode'])) ? $debitNoteData['debitNoteCode'] : '-';
                    }
                }

                if ($transaction['documentTypeID'] == '20') {
                    $pvData = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPVByPvIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($pvData) {
                       $transaction['relatedCode'] = (!empty($pvData['paymentVoucherCode'])) ? $pvData['paymentVoucherCode'] : '-';
                    }
                }

                // if ($transaction['documentTypeID'] == '27') {
                //     $pettyCashFloatData = $this->CommonTable('Expenses\Model\PettyCashFloatTable')->getPettyCasshFloatByPettyCasshFloatIDForRec($transaction['journalEntryDocumentID'])->current();
                //     if ($pettyCashFloatData) {
                //         $config = $this->getServiceLocator()->get('config');
                //         $PettyCashFloatCodePrefix = $config['PettyCashFloatCodePrefix'];
                //         $transaction['relatedCode'] = (!empty($pettyCashFloatData['pettyCashFloatID'])) ? $PettyCashFloatCodePrefix.$pettyCashFloatData['pettyCashFloatID'] : '-';
                //     }
                // }

                if ($transaction['documentTypeID'] == '28') {
                    $pettyCashVoucherData = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCasshVoucherByPettyCasshVoucherIDForRec($transaction['journalEntryDocumentID'])->current();
                    if ($pettyCashVoucherData) {
                       $transaction['relatedCode'] = (!empty($pettyCashVoucherData['pettyCashVoucherNo'])) ? $pettyCashVoucherData['pettyCashVoucherNo'] : '-';
                    }
                }

                $transaction['transactionId'] = $transaction['journalEntryAccountsID'];
                $transaction['type'] = 'other-journal-entry-withrawal';
                $transaction['amount'] = $transaction['journalEntryAccountsCreditAmount'];
                return $transaction;
            }, iterator_to_array($otherCreditJE));

            //merge arrays
            $result = array_merge( $transaction, $depositedCheques, $issuedCheques, $adPayissuedCheques, $bankOutgoingTransfer, $adPaybankOutgoingTransfer, $bankIncomingTransfer, $bankWithdrawal, $bankdeposit, $accountIncomingTranfer, $accountOutgoingTransfer, $outgoingAdvancedPayments,$incomingAdvancedPayments, $deposit, $withdrwal, $directDebitJE, $directCreditJE, $incomingPayments, $outgoingIncomePayments, $otherDebitJE, $otherCreditJE);
            
            $draftTranscation = $this->CommonTable('Expenses\Model\ReconciliationTransactionTable')->getNormalReconciliationTransactionByReconciliationId($reconciliationId);

            $draftTranscationIds = [];
            foreach ($draftTranscation as $key => $value) {
                if (!is_null($value['incomingPaymentMethodChequeId'])) {
                    $draftTranscationIds[] = $value['reconciliationTransactionType']."-".$value['incomingPaymentMethodChequeId'];
                }

                if (!is_null($value['outGoingPaymentMethodsNumbersId'])) {
                    $draftTranscationIds[] = $value['reconciliationTransactionType']."-".$value['outGoingPaymentMethodsNumbersId'];
                }

                if (!is_null($value['incomingPaymentMethodBankTransferId'])) {
                    $draftTranscationIds[] = $value['reconciliationTransactionType']."-".$value['incomingPaymentMethodBankTransferId'];
                }
            }

            $view = new ViewModel(array('transactions' => $result, 'account' => $account, 'draftTranscationIds' => $draftTranscationIds));
            $view->setTerminal(true);
            $view->setTemplate('expenses/reconciliation-api/draft-transaction-list');
            return $view;
        }
    }
}
