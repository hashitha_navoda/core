<?php

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Bank API related actions
 */

namespace Expenses\Controller\API;

use Core\Controller\CoreController;
use Expenses\Model\Bank;
use Expenses\Model\BankBranch;
use Zend\View\Model\ViewModel;

class BankAPIController extends CoreController
{
    
    /**
     * register new bank
     * @return JsonModel
     */
    public function registerAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){            
            $postData = $this->getRequest()->getPost()->toArray();            
            if($postData['bankCode'] && $postData['bankName']){                
                $result = $this->CommonTable('Expenses\Model\BankTable')->getBankByBankName($postData['bankName']);
                if($result){
                    $this->msg = $this->getMessage('ERR_BANK_NAME_EXIST');
                } else {
                    $result = $this->CommonTable('Expenses\Model\BankTable')->getBankByBankCode($postData['bankCode']);
                    if($result){
                        $this->msg = $this->getMessage('ERR_BANK_CODE_EXIST');
                    } else {
                        $bank = new Bank();
                        $bank->exchangeArray($postData);
                        $this->data = $this->CommonTable('Expenses\Model\BankTable')->saveBank($bank,$this->createEntity());
                        if($this->data){
                            $this->setLogMessage('Bank '.$postData['bankCode'].' is created.');
                            $this->status = true;
                            $this->msg = $this->getMessage('SUCC_BANK_REGISTER');
                        } else {
                            $this->msg = $this->getMessage('ERR_BANK_REGISTER');
                        }
                    }
                }
            } else {
                $this->msg = $this->getMessage('ERR_BANK_VALIDATION');
            }
        }
        
        return $this->JSONRespond();
    }
    
    /**
     * Update bank
     * @return JsonModel
     */
    public function updateAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){
            
            $postData = $this->getRequest()->getPost()->toArray();
            
            if($postData['bankId']){                
                $bank = $this->CommonTable('Expenses\Model\BankTable')->getBankByBankId($postData['bankId']);
                if($bank){ 
                    $result = $this->CommonTable('Expenses\Model\BankTable')->checkBankNameAndBankCodeValid($postData['bankId'],$postData['bankName'],$postData['bankCode']);
                    if($result){
                        if($result['bankName'] == $postData['bankName']){
                            $this->msg = $this->getMessage('ERR_BANK_NAME_EXIST');
                        } else {
                            $this->msg = $this->getMessage('ERR_BANK_CODE_EXIST');
                        }
                    } else {
                        try {
                            $this->CommonTable('Expenses\Model\BankTable')->updateBank($postData,$postData['bankId']);
                            $this->updateEntity($postData['entityId']);
                            $this->status = true;
                            $changeArray = $this->getBankChageDetails($bank, $postData);
                            //set log details
                            $previousData = json_encode($changeArray['previousData']);
                            $newData = json_encode($changeArray['newData']);

                            $this->setLogDetailsArray($previousData,$newData);
                            $this->setLogMessage("Bank ".$bank['bankCode']." is updated.");
                            $this->msg = $this->getMessage('SUCC_BANK_UPDATE');
                        } catch (\Exception $exc) {
                            $this->msg = $exc->getMessage();
                        }
                    }                    
                } else {
                    $this->msg = $this->getMessage('ERR_BANK_NOT_FOUND');
                }
            }
        }        
        return $this->JSONRespond();
    }

    public function getBankChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Bank code'] = $row['bankCode'];
        $previousData['Bank name'] = $row['bankName'];

        $newData = [];
        $newData['Bank code'] = $data['bankCode'];
        $newData['Bank name'] = $data['bankName'];
        return array('previousData' => $previousData, 'newData' => $newData);
    }
    
    /**
     * Get bank
     * @return JsonModel
     */
    public function getBankAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){            
            $postData = $this->getRequest()->getPost()->toArray();            
            if($postData['bankId']){
                $this->data = $this->CommonTable('Expenses\Model\BankTable')->getBankByBankId($postData['bankId']);
                if($this->data){
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_BANK_RETRIEVED');
                } else {
                    $this->msg = $this->getMessage('ERR_BANK_NOT_FOUND');
                }
            }
        }        
        return $this->JSONRespond();
    }
    
    /**
     * search banks
     * @return ViewModel
     */
    public function searchAction()
    {        
        if($this->getRequest()->isPost()){            
            $key = $this->getRequest()->getPost('searchKey');            
            $banks = $this->CommonTable('Expenses\Model\BankTable')->searchBankByName($key,array('bank.bankName ASC'));
            $view = new ViewModel(array('banks'=>$banks));
            $view->setTerminal(true);
            $view->setTemplate('expenses/bank-api/search');
            return $view;
        }
    }

    /**
     * get all banks
     * @return ViewModel
     */
    public function getBanksAction()
    {       
        //get page number
        $page = $this->params('param1',1);
        //get bank list
        $paginator = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(true,array('bank.bankId DESC'));        
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $view = new ViewModel(array('banks'=>$paginator));
        $view->setTerminal(true);
        $view->setTemplate('expenses/bank-api/get-banks');
        return $view;
    }
    
    /**
     * Delete bank
     * @return JsonModel
     */
    public function deleteBankAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){            
            $bankId   = $this->getRequest()->getPost('bankId');            
            $bank = $this->CommonTable('Expenses\Model\BankTable')->getBankByBankId($bankId);
            if($bank){
                $result = $this->CommonTable('Expenses\Model\AccountTable')->getAccountsByBankId($bankId);
                if($result->count() == 0){
                    if($this->updateDeleteInfoEntity($bank['entityId'])){
                        $this->status = true;
                        $this->setLogMessage('Bank '.$bank['bankCode'].' is deleted.');
                        $this->msg = $this->getMessage('SUCC_BANK_DELETE');
                    } else {
                        $this->msg = $this->getMessage('ERR_BANK_DELETE_FAILED');
                    }                        
                } else {
                    $this->msg = $this->getMessage('ERR_BANK_DELETE');
                }
            } else {
                $this->msg = $this->getMessage('ERR_BANK_NOT_FOUND');
            }
        }        
        return $this->JSONRespond();
    }
    
    /**
     * get bank list for dropdown
     * @return JsonModel
     */
    public function bankNameListForDropdownAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = array('list' => array());
        
        if ($this->getRequest()->isPost()) {            
            $searchKey = $this->getRequest()->getPost('searchKey');            
            $banks = $this->CommonTable('Expenses\Model\BankTable')->searchBankByName($searchKey);

            $bankList = array();
            foreach ($banks as $bank) {
                $bankList[]    = array('value'=>$bank['bankId'],'text'=>$bank['bankName']);
            }
            $this->data = array('list' => $bankList);            
        }        
        return $this->JSONRespond();
    }
    
    /**
     * add new bank branch
     * @return JsonModel
     */
    public function addBankBranchAction() {
        
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if ($this->getRequest()->isPost()){
            
            $postData = $this->getRequest()->getPost()->toArray();
            
            $bankBranch = new BankBranch();
            
            $form = new \Expenses\Form\BankBranchForm('create bank branch');
            $form->setInputFilter($bankBranch->getInputFilter());
            $form->setData($postData);
            
            if($form->isValid()){
                $bankBranch->exchangeArray($postData);
                $branch = $this->CommonTable('Expenses\Model\BankBranchTable')->getBankBranchByBankIdAndBranchCode($bankBranch->bankId,$bankBranch->bankBranchCode);
                if(!$branch){
                    $result = $this->CommonTable('Expenses\Model\BankBranchTable')->saveBranch($bankBranch,$this->createEntity());
                    if($result){
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_BRANCH_ADD');
                        $this->data = $result;
                    } else {
                        $this->msg = $this->getMessage('ERR_BRANCH_ADD');
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_BRANCH_CODE_EXIST');
                } 
            } else {
                $this->msg = $this->getMessage('ERR_BRANCH_VALIDATION');
            }
        }
        
        return $this->JSONRespond();
    }
    
    /**
     * Get bank branch
     * @return JsonModel
     */
    public function getBankBranchAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){            
            $postData = $this->getRequest()->getPost()->toArray();            
            if($postData['bankBranchId']){
                $this->data = $this->CommonTable('Expenses\Model\BankBranchTable')->getBankBranchByBankBranchId($postData['bankBranchId']);
                if($this->data){
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_BRANCH_RETRIEVED');
                } else {
                    $this->msg = $this->getMessage('ERR_BRANCH_NOT_FOUND');
                }
            }
        }        
        return $this->JSONRespond();
    }
    
    /**
     * Update bank branch
     * @return JsonModel
     */
    public function updateBankBranchAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){            
            $postData = $this->getRequest()->getPost()->toArray();
            $bankBranch = new BankBranch();
            
            $form = new \Expenses\Form\BankBranchForm('update bank branch');
            $form->setInputFilter($bankBranch->getInputFilter());
            $form->setData($postData);
            
            if($form->isValid()){
                $branch = $this->CommonTable('Expenses\Model\BankBranchTable')->getBankBranchByBankBranchId($postData['bankBranchId']);
                if($branch){
                    $result = $this->CommonTable('Expenses\Model\BankBranchTable')->checkBranchCodeValid($branch['bankId'],$branch['bankBranchId'],$postData['bankBranchCode']);
                    if(empty($result)){
                        try{
                            $this->CommonTable('Expenses\Model\BankBranchTable')->updateBankBranch($postData,$postData['bankBranchId']);
                            $this->updateEntity($branch['entityId']);
                            $this->status = true;
                        $this->msg = $this->getMessage('SUCC_BRANCH_UPDATE');
                        } catch (\Exception $e){
                            $this->msg = $e->getMessage();
                        }
                    } else {
                        $this->msg = $this->getMessage('ERR_BRANCH_CODE_EXIST');
                    }
                } else {
                    $this->msg = $this->getMessage('ERR_BRANCH_NOT_FOUND');
                }
                
            } else {
                $this->msg = $this->getMessage('ERR_BRANCH_VALIDATION');
            }            
        }        
        return $this->JSONRespond();
    }
    
    /**
     * Delete bank branch
     * @return JsonModel
     */
    public function deleteBankBranchAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;
        
        if($this->getRequest()->isPost()){            
            $branchId   = $this->getRequest()->getPost('bankBranchId');            
            $branch = $this->CommonTable('Expenses\Model\BankBranchTable')->getBankBranchByBankBranchId($branchId);
            if($branch){
                $result = $this->CommonTable('Expenses\Model\AccountTable')->getAccountsByBankBranchId($branchId);
                if($result->count() == 0){
                    if($this->updateDeleteInfoEntity($branch['entityId'])){
                        $this->status = true;
                        $this->msg = $this->getMessage('SUCC_BRANCH_DELETE');
                    } else {
                        $this->msg = $this->getMessage('ERR_BRANCH_DELETE_FAILED');
                    }                        
                } else {
                    $this->msg = $this->getMessage('ERR_BRANCH_DELETE');
                }
            } else {
                $this->msg = $this->getMessage('ERR_BRANCH_NOT_FOUND');
            }
        }        
        return $this->JSONRespond();
    }
    
    /**
     * search bank branches
     * @return ViewModel
     */
    public function searchBankBranchesAction()
    {        
        if($this->getRequest()->isPost()){            
            $postData = $this->getRequest()->getPost()->toArray();
            $branches = $this->CommonTable('Expenses\Model\BankBranchTable')->searchBankBranchesByBranchName( $postData['searchKey'], $postData['bankId'], array('bankBranch.bankBranchName ASE'),true);
            $view = new ViewModel(array('branches'=>$branches,'pagination'=> $postData['pagination']));
            $view->setTerminal(true);
            $view->setTemplate('expenses/bank-api/search-bank-branches');
            return $view;
        }
    }
    
    /**
     * get all branches of the given bank
     * @return ViewModel
     */
    public function getBankBranchesAction()
    {   
        //get bank id
        $bankId = $this->params('param1',null);
        //get page number
        $page = $this->params('param2',1);
        //get bank branch list
        $paginator = $this->CommonTable('Expenses\Model\BankBranchTable')->getBankBranchesByBankId($bankId,true,array('bankBranch.bankBranchId DESC'));        
        $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('param2', $page));
        $paginator->setItemCountPerPage(15);

        $view = new ViewModel(array('branches'=>$paginator,'bankId'=>$bankId));
        $view->setTerminal(true);
        $view->setTemplate('expenses/bank-api/get-bank-branches');
        return $view;
    }
    
    /**
     * get bank branch list for dropdown
     * @return JsonModel
     */
    public function bankBranchListForDropdownAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = array();
        
        if ($this->getRequest()->isPost()) {            
            $bankId = $this->getRequest()->getPost('bankId');            
            $branches = $this->CommonTable('Expenses\Model\BankBranchTable')->getBankBranchesByBankId($bankId,false,array('bankBranch.bankBranchName ASE'));
            if($branches){
                $branchList = array();
                foreach ($branches as $branch) {
                    $branchList[] = array('branchId'=>$branch['bankBranchId'],'branchName'=>$branch['bankBranchName']);
                }
                $this->status = true;
                $this->msg = $branches->count().' branch(s) found';
                $this->data = $branchList;
            }         
        }        
        return $this->JSONRespond();
    }
    
}
