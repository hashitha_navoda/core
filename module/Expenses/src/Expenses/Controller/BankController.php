<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Form\BankBranchForm;

class BankController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'account_settings_bank_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Bank', 'Accounting', 'Bank Details');
        //get page number
        $page = $this->params('param1', 1);
        //get bank list
        $paginator = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(true, array('bank.bankId DESC'));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        return new ViewModel(array('banks' => $paginator));
    }

    public function branchAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Bank', 'Accounting', 'Bank Details');

        //get bank id
        $bankId = $this->params('param1', null);
        //get page number
        $page = $this->params('param2', 1);

        //get bank details
        $bank = $this->CommonTable('Expenses\Model\BankTable')->getBankByBankId($bankId);
        if ($bank) {
            //create form
            $form = new BankBranchForm('create bank branch');

            //get branch list
            $paginator = $this->CommonTable('Expenses\Model\BankBranchTable')->getBankBranchesByBankId($bankId, true, array('bankBranch.bankBranchId DESC'));
            $paginator->setCurrentPageNumber((int) $this->params()->fromQuery('param2', $page));
            $paginator->setItemCountPerPage(15);
        } else {
            //TODO::Fix me
            //return to permission denied page
            return $this->redirect()->toRoute('core', array(
                        'action' => 'error'
            ));
        }

        return new ViewModel(array('form' => $form, 'bank' => $bank, 'branches' => $paginator, 'params' => ['param1' => $bankId, 'param2' => $page]));
    }

}
