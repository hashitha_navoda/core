<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Form\ChequeDepositForm;
use Zend\Session\Container;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Cheque deposit related actions
 */
class ChequeDepositController extends CoreController
{    
     
    protected $sideMenus  = 'accounting_side_menu';
    protected $upperMenus = 'cheque_deposit_upper_menu';
    protected $downMenus = 'cash_bank_up_down_menu';
    
    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->downMenus = 'expense_up_down_menu'.$this->packageID;
        }
    }
    
    public function indexAction()
    {   
        $this->getSideAndUpperMenus('Cash & Bank', 'Undeposited Cheques', 'Accounting', 'Cheques');
        
        //get bank list
        $bankList = array();
        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll();
        foreach ($banks as $bank){
            $bankList[$bank[bankId]] = $bank['bankName'];
        }
        
        //create cheque deposit form
        $form = new ChequeDepositForm('chequeDeposit',$bankList);
        $locationID = $this->user_session->userActiveLocation['locationID'];
        
        //get customer cheques
        $paginator = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getUndepositedCheques( false, $locationID);


        $relatedDocsView = new ViewModel();
        $relatedDocsView->setTemplate('expenses/cheque-deposit/related-docs');

        $view = new ViewModel(array('cheques' => $paginator, 'companyCurrencyId' => $this->companyCurrencyId, 'companyCurrencySymbol' => $this->companyCurrencySymbol, 'form'=>$form));
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/credit-note/document-view');

        $view->addChild($relatedDocsView, 'relatedDocsView');
        $view->addChild($documentView, 'documentView');

        
        return $view;
    }
    
    public function depositsAction() 
    {
        $this->getSideAndUpperMenus('Cash & Bank', 'Deposits', 'Accounting', 'Cheques');
        
        //get page number
        $page = $this->params('param1',1);
        
        //get customer cheques
        $paginator = $this->CommonTable('Expenses\Model\ChequeDepositTable')->fetchAll( true, array('chequeDeposit.chequeDepositId DESC'));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        
        return new ViewModel(array('deposits'=>$paginator));
    }
    
    public function bouncedChequesAction() 
    {
        $this->getSideAndUpperMenus('Cash & Bank', 'Bounced Cheques', 'Accounting', 'Cheques');
        
        //get page number
        $page = $this->params('param1',1);
        
        //get customer cheques
        $paginator = $this->CommonTable('Invoice\Model\IncomingPaymentMethodChequeTable')->getBouncedCheques( true, array('incomingPayment.incomingPaymentID DESC'));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        
        return new ViewModel(array('cheques' => $paginator, 'companyCurrencyId' => $this->companyCurrencyId, 'companyCurrencySymbol' => $this->companyCurrencySymbol));
    }
    
}
