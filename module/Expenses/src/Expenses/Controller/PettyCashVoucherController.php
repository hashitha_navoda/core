<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Form\PettyCashVoucherForm;
use Zend\I18n\Translator\Translator;
use Zend\Session\Container;

/**
 * Description of PettyCashVoucherController
 *
 * @author shermilan <sharmilan@thinkcube.com>
 */
class PettyCashVoucherController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'petty_cash_voucher_upper_menu';
    protected $downMenus = 'expense_up_down_menu';
    protected $userID;
    protected $user_session;
    protected $company;
    protected $useAccounting;
    public $paginator;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Expenses', 'Create', 'Accounting', 'Petty Cash Voucher');

        // get user list for drop down
        $activeUsers = $this->CommonTable('User\Model\UserTable')->getActiveUsers();
        $users = array();
        foreach ($activeUsers as $activeUser) {
            $users[$activeUser['userID']] = $activeUser['userUsername'];
        }

        // get eaxpense type list for dropdown
        $pettyCashExpenseTypes = $this->CommonTable('Expenses\Model\PettyCashExpenseTypeTable')->getActiveExpenseTypes();
        foreach ($pettyCashExpenseTypes as $pettyCashExpenseType) {
            $range = '';
            if ($pettyCashExpenseType->pettyCashExpenseTypeMinAmount) {
                $range .= '(' . number_format($pettyCashExpenseType->pettyCashExpenseTypeMinAmount, 0) . ' < amount ';
            }
            if ($pettyCashExpenseType->pettyCashExpenseTypeMaxAmount) {
                $range.= '< ' . number_format($pettyCashExpenseType->pettyCashExpenseTypeMaxAmount);
            }
            $range.= ($pettyCashExpenseType->pettyCashExpenseTypeMinAmount || $pettyCashExpenseType->pettyCashExpenseTypeMaxAmount) ? ')' : '';
            $activePettyCashExpenseTypes[$pettyCashExpenseType->pettyCashExpenseTypeID] = $pettyCashExpenseType->pettyCashExpenseTypeName . ' ' . $range;
        }

        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";


        // get petty cash types for drop down
        $pettyCashVoucherTypes = $this->getServiceLocator()->get('config')['pettyCashVoucherTypes'];

        $locationID = $this->user_session->userActiveLocation['locationID'];
        // get outgoing inserted petty cash voucher list
        $outgoingVouchers = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getActiveOutGoingPettyCashVouchers($locationID);
        foreach ($outgoingVouchers as $outgoingVoucher) {
            $outgoingVoucher = (object) $outgoingVoucher;
            $outgoingVoucherList[$outgoingVoucher->pettyCashVoucherID] = $outgoingVoucher->pettyCashVoucherNo;
        }

        $refData = $this->getReferenceNoForLocation(27, $locationID);
        $rid = $refData["refNo"];
        $locationReferenceID = $refData["locRefID"];
        $pettyCashVoucherNo = $this->getReferenceNumber($locationReferenceID);


        $pettyCashVoucherForm = new PettyCashVoucherForm([
            'users' => $users,
            'pettyCashExpenseTypes' => $activePettyCashExpenseTypes,
            'pettyCashVoucherTypes' => $pettyCashVoucherTypes,
            'outgoingPettyCashVoucherList' => $outgoingVoucherList,
            'pettyCashVoucherNo' => $pettyCashVoucherNo,
        ]);

        $PVDAData = $this->CommonTable('Expenses\Model\PettyCashVoucherDefaultAccountsTable')->fetchAll();

        if(count($PVDAData) > 0){
            $data = $PVDAData->current();
            $financeAccountsArray = $this->getFinanceAccounts();

            if($data->pettyCashVoucherFinanceAccountID != ''){
                $pettyCashVoucherFinanceAccountID = $data->pettyCashVoucherFinanceAccountID;
                $pettyCashVoucherFinanceAccountName = $financeAccountsArray[$pettyCashVoucherFinanceAccountID]['financeAccountsCode']."_".$financeAccountsArray[$pettyCashVoucherFinanceAccountID]['financeAccountsName'];
                $pettyCashFloatBalance = $financeAccountsArray[$pettyCashVoucherFinanceAccountID]['financeAccountsDebitAmount'] - $financeAccountsArray[$pettyCashVoucherFinanceAccountID]['financeAccountsCreditAmount'];
                $pettyCashVoucherForm->get('pettyCashVoucherFinanceAccountID')->setAttribute('data-id',$pettyCashVoucherFinanceAccountID);
                $pettyCashVoucherForm->get('pettyCashVoucherFinanceAccountID')->setAttribute('data-value',$pettyCashVoucherFinanceAccountName);
                $pettyCashVoucherForm->get('pettyCashFloatBalance')->setAttribute('data-value',$pettyCashFloatBalance);
            }
        }

        $pettyCashVoucherView = new ViewModel(['pettyCashVoucherForm' => $pettyCashVoucherForm]);

        //  if reference not set
        if ($rid == '' || $rid == NULL) {
            if ($locationReferenceID == null) {
                $title = 'Petty Cash Voucher Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Petty Cash Voucher Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $pettyCashVoucherView->addChild($refNotSet, 'refNotSet');
        }

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $pettyCashVoucherView->addChild($dimensionAddView, 'dimensionAddView');


        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/petty-cash-voucher.js');
        return $pettyCashVoucherView;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Expenses', 'View', 'Accounting', 'Petty Cash Voucher');

        // get petty cash types for drop down
        $pettyCashVoucherTypes = $this->getServiceLocator()->get('config')['pettyCashVoucherTypes'];

        $this->pettyCashVoucherPaginator();
        $pettyCashVoucher = $this->paginator;
        $dateFormat = $this->getUserDateFormat();
        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/petty-cash-voucher.js');
        return new ViewModel([
            'pettyCashVouchers' => $pettyCashVoucher,
            'pettyCashVoucherTypes' => $pettyCashVoucherTypes,
            'isPaginated' => TRUE,
            'dateFormat' => $dateFormat
        ]);
    }

    public function listsAction()
    {
        $this->getSideAndUpperMenus('Petty Cash Voucher', 'View', 'Expenses');

        $pvId = $this->params()->fromRoute('param1');
        if (!$pvId) {
            return $this->redirect()->toRoute('petty-cash-voucher', array('action' => 'view'));
        }
        $data = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCashVoucherDetails($pvId);

        $data['pettyCashVoucherTypeName'] = ($data['pettyCashVoucherType'] == 1) ? 'Cash Outgoing' : 'Cash Incoming';
        $data['pettyCashExpenseType'] = ($data['pettyCashVoucherType'] == 1 && $data['pettyCashVoucherExpenseTypeFormat'] == 'acc') ? $data['financeAccountsCode'].' - '.$data['financeAccountsName'] : $data['pettyCashExpenseType'];

        return new ViewModel(['data' => $data]);
    }

    public function getPettyCashVoucherListAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->pettyCashVoucherPaginator();
            $pettyCashVoucherTypes = $this->getServiceLocator()->get('config')['pettyCashVoucherTypes'];
            $pettyCashVoucher = $this->paginator;
            $dateFormat = $this->getUserDateFormat();
            $pettyCashView = new ViewModel(array(
                'pettyCashVouchers' => $pettyCashVoucher,
                'pettyCashVoucherTypes' => $pettyCashVoucherTypes,
                'isPaginated' => TRUE,
                'dateFormat' => $dateFormat
                    )
            );
            $pettyCashView->setTerminal(true);
            $pettyCashView->setTemplate('expenses/petty-cash-voucher/list');
            $this->setLogMessage('Retrive petty cash voucher '.$PettyCashNo.' for list view');
            return $pettyCashView;
        }else{
           $this->status = false;
           $this->msg=$this->getMessage('ERR_REQUEST');



           return $this->JSONRespond();
        }
    }

    public function pettyCashVoucherPaginator($pageCount = 6)
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->fetchAll($locationID, TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($pageCount);
    }

    public function previewAction()
    {
        $pettyCashVoucherID = $this->params()->fromRoute('param1');
        $data = $this->_getDataForPettyCashVoucherPreview($pettyCashVoucherID);
        $data['pettyCashVoucherID'] = $pettyCashVoucherID;
        $path = "/petty-cash-voucher/document/"; //.$paymentID;
        $translator = new Translator();
        $createNew = $translator->translate('New Petty Cash Voucher');
        $createPath = '/petty-cash-voucher';
        $data['total'] = number_format($data['total'], 2);
        $data["email"] = array(
                "to" => "",
                "subject" => "Petty Cash Voucher from " . $data['companyName'],
                "body" => <<<EMAILBODY

Dear , <br /><br />

Thank you for your inquiry. <br /><br />

A Petty Cash Voucher has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>Petty Cash Voucher Number:</strong> {$data['pettyCashVoucherNo']} <br />
<strong>Petty Cash Voucher Amount:</strong> {$data['currencySymbol']} {$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
            );

         $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountFlag = (count($glAccountExist) > 0)? true :false;
            $journalEntryValues = [];
            if($glAccountFlag){
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('28',$pettyCashVoucherID);
                $journalEntryID = $journalEntryData['journalEntryID'];
                $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
                $journalEntryValues = $jeResult['JEDATA'];
                $journalEntryValues['journalEntryDocumentID'] = 28;
            }

            $JEntry = new ViewModel(
                array(
                 'journalEntry' => $journalEntryValues,
                 'closeHidden' => true
                 )
                );
            $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $documentType = 'Petty Cash Voucher';
        $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $pettyCashVoucherID, $createPath);
        $preview->addChild($JEntry, 'JEntry');
        $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
        $additionalButton->setTemplate('additionalButton');
        $preview->addChild($additionalButton, 'additionalButton');

        //$this->setLogMessage("Preview supplier payment ".$data['outgoingPaymentCode']);
        return $preview;
    }

    public function documentPdfAction()
    {
        $documentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Petty Cash Voucher';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/expenses/petty-cash-voucher/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->_getDataForPettyCashVoucherPreview($documentID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($documentID, $documentType, $documentData, $templateID);

        return;
    }

    public function documentAction()
    {
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');

        $documentType = 'Petty Cash Voucher';
        $pettyCashVoucherID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }


        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $pettyCashVoucherID, $templateID);

        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForPettyCashVoucherPreview($pettyCashVoucherID);

        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);

        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($pettyCashVoucherID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    private function _getDataForPettyCashVoucherPreview($pettyCashVoucherID){
        if (!empty($this->_paymentViewData)) {
            return $this->_paymentViewData;
        }

        // Get company details
        $data = $this->getDataForDocumentView();
        $locationID = $this->user_session->userActiveLocation["locationID"];

        //$pettyCashVoucherDetails = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCashVoucherByLocationId($locationID,false,$pettyCashVoucherID)->current();

        $pettyCashVoucherDetails = $this->CommonTable('Expenses\Model\PettyCashVoucherTable')->getPettyCashVoucherDetails($pettyCashVoucherID);

        $pettyCashVoucherDetails['pettyCashVoucherTypeName'] = ($pettyCashVoucherDetails['pettyCashVoucherType'] == 1) ? 'Cash Outgoing' : 'Cash Incoming';
        $pettyCashVoucherDetails['pettyCashExpenseType'] = ($pettyCashVoucherDetails['pettyCashVoucherType'] == 1 && $pettyCashVoucherDetails['pettyCashVoucherExpenseTypeFormat'] == 'acc') ? $pettyCashVoucherDetails['financeAccountsCode'].' - '.$pettyCashVoucherDetails['financeAccountsName'] : $pettyCashVoucherDetails['pettyCashExpenseType'];


        $pettyCashVoucherDetails['pettyCashVoucherDate'] = $this->convertDateToUserFormat($pettyCashVoucherDetails['pettyCashVoucherDate']);
        $data = array_merge($data, $pettyCashVoucherDetails);

        // to be used by below functions - convert to object
        $pettyCashVoucherDetails = (object) $pettyCashVoucherDetails;
        $pettyCashVoucherTypes = $this->getServiceLocator()->get('config')['pettyCashVoucherTypes'];
        // Use core date function
        $data['today_date'] = gmdate('Y-m-d');

        $displaySetup = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();
        $data['currencySymbol'] = $displaySetup['currencySymbol'];

        $data['type'] = "receipt";
        $data['pettyCashVoucherDate'] = $pettyCashVoucherDetails->pettyCashVoucherDate;
        $data['pettyCashVoucherNo'] = $pettyCashVoucherDetails->pettyCashVoucherNo;
        $data['pettyCashVoucherId'] = $pettyCashVoucherDetails->pettyCashVoucherID;
        if($pettyCashVoucherDetails->pettyCashVoucherType == 1){
            $pettyType = 'Cash Outgoing';
        }else{
            $pettyType = 'Cash Incoming';
        }

        if($pettyCashVoucherDetails->pettyCashVoucherType == '1'){
            $rec = array(
                $pettyCashVoucherDetails->pettyCashVoucherNo,
                $pettyType,
                $pettyCashVoucherDetails->pettyCashExpenseType,
                $pettyCashVoucherDetails->pettyCashVoucherAmount,
                );
            $data['table'] = array(
                'col_size' => array("10", "10", "10", "10"),
                'headers' => array(
                    "Voucher No." => 1,
                    "Voucher Type" => 1,
                    "Petty cash </br>expense type" => 1,
                    "Voucher Amount </br>" . "(" . $displaySetup['currencySymbol'] . ")" => 1,
                ),
                'records' => $rec,
            );
        }else{
            $rec = array(
                $pettyCashVoucherDetails->pettyCashVoucherNo,
                $pettyType,
                $pettyCashVoucherDetails->outGoingPettyCashVoucher,
                $pettyCashVoucherDetails->pettyCashVoucherAmount,
                );
            $data['table'] = array(
                'col_size' => array("10", "10", "10", "10"),
                'headers' => array(
                    "Voucher No." => 1,
                    "Voucher Type" => 1,
                    "Outgoing petty </br>cash voucher" => 1,
                    "Voucher Amount </br>" . "(" . $displaySetup['currencySymbol'] . ")" => 1,
                ),
                'records' => $rec,
            );

        }



        $data['comment'] = $pettyCashVoucherDetails->pettyCashVoucherComment;
        $data['sub_total'] = $pettyCashVoucherDetails->pettyCashVoucherAmount;
        $data['total'] = $pettyCashVoucherDetails->pettyCashVoucherAmount;
        $data['discount'] = null;

        $data['cdnUrl'] = $this->cdnUrl;

        return $this->_paymentViewData = $data;
    }

    private function _getDocumentDataTable($pettyCashVoucherID, $documentSize = 'A4')
    {

        $data_table_vars = $this->_getDataForPettyCashVoucherPreview($pettyCashVoucherID);

        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/expenses/petty-cash-voucher/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

}
