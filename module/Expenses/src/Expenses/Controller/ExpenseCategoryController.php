<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class ExpenseCategoryController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'expenses_category_upper_menu';
    protected $downMenus = 'approval_workflow_down_menu';
    protected $paginator;

    public function createAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'Create', null, 'Voucher Categories');
        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/expenseType.js');
        $categories = $this->CommonTable('Expenses\Model\ExpenseCategoryTable')->fetchAll();
        $this->getPaginatedExpenseCategories();
        $expCategoryListView = new ViewModel(array(
            'categories' => $this->paginator,
            'paginated' => TRUE));
        $expCategoryListView->setTemplate('expenses/expense-category/list');
        $expCategoryListView->setTerminal(FALSE);
        $expenseCategoryCreateView = new ViewModel(array(
            'categories' => $categories,
        ));
        $expenseCategoryCreateView->addChild($expCategoryListView, 'categoryList');
        return $expenseCategoryCreateView;
    }

    private function getPaginatedExpenseCategories()
    {

        $this->paginator = $this->CommonTable('Expenses\Model\ExpenseCategoryTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(6);
    }

}
