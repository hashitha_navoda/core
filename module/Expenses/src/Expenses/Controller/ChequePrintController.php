<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Form\ChequeDepositForm;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Paginator\Paginator;

class ChequePrintController extends CoreController
{    
     
    protected $sideMenus  = 'accounting_side_menu';
    protected $upperMenus = 'cheque_deposit_upper_menu';
    protected $downMenus = 'cash_bank_up_down_menu';
    
    public function indexAction()
    {   
        $this->getSideAndUpperMenus('Cash & Bank', 'Cheque Printing', 'Accounting', 'Cheques');
        
        $cheques = $this->CommonTable('SupplierPaymentMethodNumbersTable')
                ->getIssuedChequesList();

        $printedChequeRes = $this->CommonTable('Expenses\Model\ChequePrintTable')->fetchAll();
        $page = $this->params()->fromRoute('param1');
        $checkList = array();
        foreach ($cheques as $cheque) {
            $checkList[] = $cheque;
        }
        $items = new ArrayAdapter($checkList);
        $paginator = new Paginator($items);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/print-cheques.js');

        $chequePrintView =  new ViewModel(array(
            'cheques' => $paginator,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'printedCheques' => $printedChequeRes,
        ));

        $confirmView = new ViewModel();
        $confirmView->setTemplate('expenses/cheque-print/print-confirmation-modal');
        $chequePrintView->addChild($confirmView, 'confirmView');

        return $chequePrintView;
    }
}
