<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

/**
 * Description of CardTypeController
 *
 * @author shermilan
 */
class CardTypeController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'account_settings_bank_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Card Type', 'Accounting', 'Bank Details');

        //get page number
        $page = $this->params('param1', 1);

        //get existing card types
        $paginator = $this->CommonTable('Expenses\Model\CardTypeTable')->fetchAll(true);
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        // Add js file to view page
        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/card-type.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return new ViewModel(['cardTypes' => $paginator, 'paginated' => TRUE]);
    }

}
