<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Inventory\Form\GrnForm;
use Inventory\Form\SupplierForm;
use Zend\I18n\Translator\Translator;
use Zend\Session\Container;

class ExpensePurchaseInvoiceController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'payment_voucher-uper-menu';
    protected $downMenus = 'expense_up_down_menu';
    protected $_piViewDetails;
    protected $paginator;
    protected $useAccounting;

     public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->downMenus = 'expense_up_down_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Expenses', 'Create PV', 'Accounting', 'Payment Voucher');
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        while ($row = $currencyResultSet->current()) {
            $currencies[$row->currencyID] = $row->currencyName;
        }

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        while ($row = $paymentTermsResultSet->current()) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }

        // get person Title (Mr, Miss ..) from config file
        $config = $this->getServiceLocator()->get('config');
        $supplierTitle = $config['personTitle'];

        $currencyValue = $this->getDefaultCurrency();
        $supplierCategory = $this->CommonTable('Inventory\Model\SupplierCategoryTable')->fetchAllAsArray();

        $supplierForm = new SupplierForm(
                array(
            'paymentTerms' => $paymentTerms,
            'currencies' => $currencies,
            'supplierTitle' => $supplierTitle,
            'supplierCategory' => $supplierCategory,
            'currencyCountry' => $currencyValue,
        ));

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID != ''){
                $supplierPayableAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPayableAccountID;
                $supplierPayableAccountName = $financeAccountsArray[$supplierPayableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPayableAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-id',$supplierPayableAccountID);
                $supplierForm->get('supplierPayableAccountID')->setAttribute('data-value',$supplierPayableAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID != ''){
                $supplierPurchaseDiscountAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierPurchaseDiscountAccountID;
                $supplierPurchaseDiscountAccountName = $financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierPurchaseDiscountAccountID]['financeAccountsName'];
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-id',$supplierPurchaseDiscountAccountID);
                $supplierForm->get('supplierPurchaseDiscountAccountID')->setAttribute('data-value',$supplierPurchaseDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID != ''){
                $supplierGrnClearingAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierGrnClearingAccountID;
                $supplierGrnClearingAccountName = $financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierGrnClearingAccountID]['financeAccountsName'];
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-id',$supplierGrnClearingAccountID);
                $supplierForm->get('supplierGrnClearingAccountID')->setAttribute('data-value',$supplierGrnClearingAccountName);
            }

            if($glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID != ''){
                $supplierAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupPurchasingAndSupplierAdvancePaymentAccountID;
                $supplierAdvancePaymentAccountName = $financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$supplierAdvancePaymentAccountID]['financeAccountsName'];
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-id',$supplierAdvancePaymentAccountID);
                $supplierForm->get('supplierAdvancePaymentAccountID')->setAttribute('data-value',$supplierAdvancePaymentAccountName);
            }
        }

        $supplierAddView = new ViewModel(array('supplierForm' => $supplierForm,'useAccounting'=>$this->useAccounting));
        $supplierAddView->setTemplate('inventory/supplier/supplier-add-modal');
        $piForm = new GrnForm(array('paymentTerms' => $paymentTerms));
        $piForm->get('discount')->setAttribute('id', 'piDiscount');
        $piForm->get('grnNo')->setAttribute('id', 'piNo')->setAttribute('name', 'piNo');
        $piForm->get('grnSaveButton')->setAttribute('id', 'piSave')->setAttribute('name', 'piSaveButton');
        $piForm->get('grnCancelButton')->setAttribute('id', 'piCancel')->setAttribute('name', 'piCancelButton');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'userLocations',
            'taxes'
        ]);

        $this->voucherType = $this->CommonTable('Expenses/Model/ExpenseTypeTable')->getExpeneTypesForApprover();

        $voucherTypes = array();
        foreach ($this->voucherType as $pTyp) {
            $voucherTypes[$pTyp['expenseTypeId']] = $pTyp['expenseTypeName'];
        }

        $dateFormat = $this->getUserDateFormat();
        $piAddView = new ViewModel(
                array(
            'piForm' => $piForm,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'dateFormat' => $dateFormat,
            'voucherTypes' => $voucherTypes
                )
        );
        $piAddView->addChild($supplierAddView, 'supplierAddView');
        $autoFillView = new ViewModel(array('dateFormat' => $dateFormat));
        $autoFillView->setTemplate('inventory/serialNumberAutoFill');
        $piProductsView = new ViewModel(array('dateFormat' => $dateFormat));
        $piProductsView->setTemplate('inventory/purchase-invoice/pi-add-products');
        $piProductsView->addChild($autoFillView, 'SerialNumberAutoFill');
        $piAddView->addChild($piProductsView, 'piAddProducts');
        $refNotSet = new ViewModel(array(
            'title' => 'Reference Number not set',
            'msg' => $this->getMessage('REQ_OUTPAY_ADD'),
            'controller' => 'company',
            'action' => 'reference'
        ));
        $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
        $piAddView->addChild($refNotSet, 'refNotSet');

         // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $piAddView->addChild($dimensionAddView, 'dimensionAddView');
        
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $piAddView->addChild($attachmentsView, 'attachmentsView');

        return $piAddView;
    }

    /**
     * use to list payment vouchers
     * @return list View Model
     */
    public function listAction()
    {
        $this->getSideAndUpperMenus('Expenses', 'View PVs', 'Accounting', 'Payment Voucher');
        $pvSearchView = new ViewModel();
        $pvSearchView->setTemplate('expenses/expense-purchase-invoice/search-header');
        $userActiveLocation = $this->user_session->userActiveLocation;
        $paymentVoucherList = $this->getPaginatedPaymentVouchers($userActiveLocation['locationID']);
        $dateFormat = $this->getUserDateFormat();
        $piViewList = new ViewModel(
                array(
            'paymentVoucherList' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat
        ));
        $piViewList->setTemplate('expenses/expense-purchase-invoice/payment-voucher-list');
        $piView = new ViewModel();
        $piView->addChild($piViewList, 'paymentVoucherList');
        $piView->addChild($pvSearchView, 'paymentVoucherSearchHeader');


        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/debit-note/doc-history');        
        $piView->addChild($docHistoryView, 'docHistoryView');

        $documentView = new ViewModel();
        $documentView->setTemplate('inventory/debit-note/document-view');
        $piView->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $piView->addChild($attachmentsView, 'attachmentsView');

        return $piView;
    }

    /**
     *
     * get payment voucher view
     */
    public function viewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $this->getSideAndUpperMenus('Payment Voucher', 'View PVs', 'Accounting');
            $data = $this->_getDataForPVView($paramIn);
            if ($data['itemProduct'] > 0) {
                foreach ($data['itemProduct'] as $value) {
                    $batchData[] = array();
                    $serialData[] = array();
                    if ($value['productBatch'] == 1 || $value['productSerial'] == 1) {
                        $batchSerialData = json_decode($value['batchSerialData']);
                        if ($value['productBatch'] == 1) {
                            $temp = $batchSerialData->batchData;
                            $batchData[$value['locationProductID']][$temp->bCode]['bQty'] = $temp->bQty;
                            $batchData[$value['locationProductID']][$temp->bCode]['mDate'] = $temp->mDate;
                            $batchData[$value['locationProductID']][$temp->bCode]['eDate'] = $temp->eDate;
                            $batchData[$value['locationProductID']][$temp->bCode]['warnty'] = $temp->warnty;
                        }
                        if ($value['productSerial'] == 1) {
                            $temp = $batchSerialData->serialData;
                            foreach ($temp as $value2) {
                                $serialData[$value['locationProductID']][$value2->sCode]['bCode'] = $value2->sBCode;
                                $serialData[$value['locationProductID']][$value2->sCode]['eDate'] = $value2->sEdate;
                                $serialData[$value['locationProductID']][$value2->sCode]['warnty'] = $value2->sWarranty;
                            }
                        }
                    }
                }
            }
            $pVSingleView = new ViewModel(
                    array(
                'data' => $data,
                'batchData' => $batchData,
                'serialData' => $serialData,
            ));
            $pVSingleView->setTemplate('expenses/expense-purchase-invoice/payment-voucher-view');
            return $pVSingleView;
        } else {
            $this->redirect()->toRoute('expense-purchase-invoice', array('action' => 'list'));
        }
    }

    public function previewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $data = $this->_getDataForPVView($paramIn);
            $data['pvCode'] = $data['pVCd'];
            $data['total'] = number_format($data['pVT'], 2);
            $path = "/expense-purchase-invoice/document/"; //.$paramIn;
            $translator = new Translator();
            $createNew = $translator->translate('New Payment Voucher');
            $createPath = "/expense-purchase-invoice/create";

            $globaldata = $this->getServiceLocator()->get('config');

            $data["email"] = array(
                "to" => $data['piSEmail'],
                "subject" => "Payment Voucher from " . $data['companyName'],
                "body" => <<<EMAILBODY

Dear {$data['piSName']}, <br /><br />

Thank you for your inquiry. <br /><br />

An Payment Voucher has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>Payment Voucher Number:</strong> {$data['pvCode']} <br />
<strong>Payment Voucher Amount:</strong> {$data['currencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
            );

            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountFlag = (count($glAccountExist) > 0)? true :false;
            $journalEntryValues = [];
            if($glAccountFlag){
                $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('20',$paramIn);
                $journalEntryID = $journalEntryData['journalEntryID'];
                $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
                $journalEntryValues = $jeResult['JEDATA'];
            }

            $JEntry = new ViewModel(
                array(
                 'journalEntry' => $journalEntryValues,
                 'closeHidden' => true
                 )
                );
            $JEntry->setTemplate('accounting/journal-entries/view-modal');

            $documentType = 'Expense Payment Voucher';
            $state = $globaldata['statuses'][$data['piSt']];

            $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paramIn, $createPath);
            $preview->addChild($JEntry, 'JEntry');
            $additionalButton = new ViewModel(array('state' => $state, 'piCode' => $data['pVID'], 'data' => $data,'glAccountFlag' => $glAccountFlag));
            $additionalButton->setTemplate('inventory/purchase-invoice/additionalButton');
            $preview->addChild($additionalButton, 'additionalButton');

            $preview->setTerminal(true);

            return $preview;
        }
    }

    public function documentPdfAction()
    {
        $documentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Expense Payment Voucher';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/expenses/expense-purchase-invoice/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->_getDataForPVView($documentID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($documentID, $documentType, $documentData, $templateID);

        return;
    }

    public function documentAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = 'Expense Payment Voucher';
        $pvID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        $paymentData=$this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentIDByPaymentVoucherID($pvID);
        $paymentIDs = array();

        foreach($paymentData as $key => $value) {
            $paymentIDs[]=$value['paymentID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName($documentType, $pvID, $templateID);
//        if ($document = $tpl->getDocumentFile($filename, $documentType)) {
//            echo $document;
//            exit;
//        }
        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->_getDataForPVView($pvID);
        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);
        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->_getDocumentDataTable($pvID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                $data['data_table_indetail'] = $this->_getDocumentDataTable($pvID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])) . '-indetail'));
                $data['payment_table'] = $this->_getDocumentPaymentTable($paymentIDs, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])).'-payment'));
                // $data['dimension_table'] = $this->_getDocumentDimensionTable($pvID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName'])).'-dimension'));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }

    public function _getDocumentDataTable($pvID, $documentSize = 'A4')
    {
        $data_table_vars = $this->_getDataForPVView($pvID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/expenses/expense-purchase-invoice/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    public function _getDataForPVView($pvID)
    {
        if (!empty($this->_pVViewDetails)) {
            return $this->_pVViewDetails;
        }
        $data = $this->getDataForDocumentView(); // get comapny details
        $paymentVoucherData = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVoucherDetailsByPVID($pvID);
        $pavmentVoucherDetails = array();
        foreach ($paymentVoucherData as $row) {
            $pavmentVoucherDetails['pVID'] = $row['paymentVoucherID'];
            $pavmentVoucherDetails['pVCd'] = $row['paymentVoucherCode'];
            $pavmentVoucherDetails['pVSName'] = $row['supplierTitle'] . ' ' . $row['supplierName'];
            $pavmentVoucherDetails['pVSAddress'] = $row['supplierAddress'];
            $pavmentVoucherDetails['pVSEmail'] = $row['supplierEmail'];
            $pavmentVoucherDetails['pVSN'] = $row['supplierCode'] . '-' . $row['supplierName'];
            $pavmentVoucherDetails['pVSR'] = $row['paymentVoucherSupplierReference'];
            $pavmentVoucherDetails['pVRL'] = $row['locationCode'] . '-' . $row['locationName'];
            $pavmentVoucherDetails['pVIssueD'] = $this->convertDateToUserFormat($row['paymentVoucherIssuedDate']);
            $pavmentVoucherDetails['pVDD'] = $this->convertDateToUserFormat($row['paymentVoucherDueDate']);
            $pavmentVoucherDetails['pVDC'] = $row['paymentVoucherDeliveryCharge'];
            $pavmentVoucherDetails['pVT'] = $row['paymentVoucherTotal'];
            $pavmentVoucherDetails['pVPaidAmo'] = $row['paymentVoucherPaidAmount'];
            $pavmentVoucherDetails['pVSTax'] = $row['paymentVoucherShowTax'];
            $pavmentVoucherDetails['pVC'] = $row['paymentVoucherComment'];
            $pavmentVoucherDetails['pVSt'] = $row['paymentVoucherStatus'];
            $pavmentVoucherDetails['userUsername'] = $row['userUsername'];
            $pavmentVoucherDetails['createdTimeStamp'] = $this->getUserDateTime($row['createdTimeStamp']);
            if ($pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productID'] != $row['paymentVoucherProductID']) {
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductID'] = $row['paymentVoucherProductID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['locationProductID'] = $row['locationProductID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductQuantity'] = $row['paymentVoucherProductQuantity'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductUnitPrice'] = $row['paymentVoucherProductUnitPrice'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductTotalPrice'] = $row['paymentVoucherProductTotalPrice'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductQuantityByBatch'] = $row['paymentVoucherProductQuantityByBatch'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['batchSerialData'] = $row['batchSerialData'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productBatch'] = $row['productBatch'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productSerial'] = $row['productSerial'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productID'] = $row['productID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productCode'] = $row['productCode'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productName'] = $row['productName'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['uomID'] = $row['uomID'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['productUomConversion'] = $row['productUomConversion'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['uomAbbr'] = $row['uomAbbr'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['itemDiscount'] = $row['paymentVoucherProductDiscount'];
                $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['paymentVoucherProductExpenseType'] = ($row['paymentVoucherProductExpenseType']) ? $row['paymentVoucherProductExpenseType'] : '-';
                if ($row['paymentVoucherTaxItemOrNonItemProduct'] == 'item' && $row['paymentVoucherProductIdOrNonItemProductID'] == $row['paymentVoucherProductID']) {
                    $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxID'] = $row['paymentVoucherTaxID'];
                    $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxPrecentage'] = $row['paymentVoucherTaxPrecentage'];
                    $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxAmount'] = $row['paymentVoucherTaxAmount'];
                    $pavmentVoucherDetails['itemProduct'][$row['paymentVoucherProductID']]['tax'][$row['paymentVoucherTaxID']]['taxName'] = $row['taxName'];
                }
            }
            if ($pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['productID'] != $row['paymentVoucherNonItemProductID']) {
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['productID'] = $row['paymentVoucherNonItemProductID'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductDiscription'] = $row['paymentVoucherNonItemProductDiscription'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductQuantity'] = $row['paymentVoucherNonItemProductQuantity'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductUnitPrice'] = $row['paymentVoucherNonItemProductUnitPrice'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductTotalPrice'] = $row['paymentVoucherNonItemProductTotalPrice'];

                // if paymentVoucherNonItemProductFormat is "acc" then GLAccount name should be displayed
                if($row['paymentVoucherNonItemProductExpenseTypeFormat'] == 'acc'){
                    $acc = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAll(false, [$row['paymentVoucherNonItemProductExpenseTypeId']]);
                    $acc_data = $acc->current();
                    $exp_type = $acc_data['financeAccountsCode'] . '_' . $acc_data['financeAccountsName'];
                }else{
                    $exp_type = $row['paymentVoucherNonItemProductExpenseType'];
                }
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['paymentVoucherNonItemProductExpenseType'] = ($exp_type) ? $exp_type : '-';
            }
            if ($row['paymentVoucherTaxItemOrNonItemProduct'] != 'item' && $row['paymentVoucherProductIdOrNonItemProductID'] == $row['paymentVoucherNonItemProductID']) {
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxID'] = $row['paymentVoucherTaxID'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxPrecentage'] = $row['paymentVoucherTaxPrecentage'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['tax'][$row['paymentVoucherTaxID']]['paymentVoucherTaxAmount'] = $row['paymentVoucherTaxAmount'];
                $pavmentVoucherDetails['nonItemProduct'][$row['paymentVoucherNonItemProductID']]['tax'][$row['paymentVoucherTaxID']]['taxName'] = $row['taxName'];
            }
        }

        $dimensionData = [];
        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(20,$pvID);
        foreach ($jEDimensionData as $value) {
            if (!is_null($value['journalEntryID'])) {
                $temp = [];
                if($value['dimensionType'] == "job" || $value['dimensionType'] == "project") {
                    if ($value['dimensionType'] == "job") {
                        $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($value['dimensionValueID']);
                        $valueTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                        $dimensionTxt = "Job";
                    } else if ($value['dimensionType'] == "project") {
                        $proData = $this->CommonTable('Jobs\Model\ProjectTable')->getProjectByProjectId($value['dimensionValueID']);
                        $valueTxt = $proData->projectCode.'-'.$proData->projectName;
                        $dimensionTxt = "Project";
                    }

                    $temp['dimensionText'] = $dimensionTxt.'-'.$valueTxt;
                } else {
                    $temp['dimensionText'] = $value['dimensionName'].'-'.$value['dimensionValue'];
                }
                $dimensionData[] = $temp;
            }
        }
        if (sizeof($dimensionData) == 0) {
            $dimensionTxtDetails = "";
        } else {
            $dimensionTxtDetails = "";
        }

        foreach ($dimensionData as $key => $value) {
            if (sizeOf($dimensionData) == 1) {
                $dimensionTxtDetails = $value['dimensionText'];
            } else {
                $dimensionTxtDetails = ($key == 0) ? $value['dimensionText']: $dimensionTxtDetails.' / '.$value['dimensionText'];
            }
        }
        $pavmentVoucherDetails['pVD'] = $dimensionTxtDetails;

        $dataa = array_merge($data, $pavmentVoucherDetails);
        $paymentVoucherMethodData = $this->CommonTable('SupplierInvoicePaymentsTable')->getPaymentsDetailsByPaymentVoucherID($pvID);
        $pvMethod="";
        foreach ($paymentVoucherMethodData as $key => $value) {
            if($value['paymentMethodID']==2){
                $pvMethod.=$value['outGoingPaymentMethodReferenceNumber']."(".$value['bankName'].")"."\r\n";
            }
        }
        $dataa['check_and_bank'] = $pvMethod;
        $dataa['today_date'] = gmdate('Y-m-d');
        $dataa['currencySymbol'] = $this->companyCurrencySymbol;
        return $this->_pVViewDetails = $dataa;
    }

    private function _getDocumentPaymentTable($paymentIDs, $documentSize = 'A4') {
        $data_table_vars = array();
        $currencySymbol = null;
        $total = 0.0;
        $discountTot = 0.0;
        $i = 0;
        if(count($paymentIDs) > 0 ){
            foreach ($paymentIDs as $pi){


                $data_table_vars[] = $this->_getDataForPaymentView($pi);
                $currencySymbol = $data_table_vars[0]['currencySymbol'];
                $leftToPay = $data_table_vars[$i]['leftToPay'];
                $total += $data_table_vars[$i]['total'];
                $discountTot += $data_table_vars[$i]['cr_discount'];
                $i++;
            }
        }

        $view = new ViewModel(
            array(
                'paymentDetails' => $data_table_vars,
                'currencySymbol' => $currencySymbol,
                'settledTotal' => $total,
                'discountTotal' => $discountTot,
                'leftToPay' => $leftToPay
            ));
        $view->setTemplate('/inventory/purchase-invoice/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    protected function _getDataForPaymentView($paymentID) {

        $data = array();
        $paymentDetails = $this->CommonTable('SupplierPaymentsTable')->getAllPaymentDetailsByID($paymentID)->current();

        $paymentMethodDetails = $this->CommonTable('SupplierPaymentMethodNumbersTable')->getPaymentMethodDetailsByPaymentId($paymentID)->current();

        $invoiceDetails = $this->CommonTable('SupplierInvoicePaymentsTable')->getAllSupplierInvoiceDetailsByPaymentID($paymentID)->current();

        $paymentDetails['outgoingPaymentDate'] = $this->convertDateToUserFormat($paymentDetails['outgoingPaymentDate']);
        $paymentDetails['createdTimeStamp'] = $this->getUserDateTime($paymentDetails['createdTimeStamp']);
        $data = array_merge($data, $paymentDetails,$paymentMethodDetails);

        // to be used by below functions - convert to object
        $paymentDetails = (object) $paymentDetails;
        $paymentMethodDetails = (object) $paymentMethodDetails;
        $invoiceDetails = (object) $invoiceDetails;

        // Use core date function
        $data['today_date'] = gmdate('Y-m-d');

        $displaySetup = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();
        $data['currencySymbol'] = $displaySetup['currencySymbol'];


        $paymentmethods = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();


        while ($t = $paymentmethods->current()) {
            $t = (object) $t;
            $paymethod[] = $t;
        }


        if($invoiceDetails->paymentMethodID == NULL){
            $data['paymentMethodName'] = 'Credit Payment';
        }else{
            $data['paymentMethodName'] = $paymethod[$invoiceDetails->paymentMethodID - 1]->paymentMethodName;
        }
        $data['paymentMethodReference'] = $paymentMethodDetails->outGoingPaymentMethodReferenceNumber;
        $data['type'] = "receipt";
        $data['outgoingPaymentDate'] = $paymentDetails->outgoingPaymentDate;
        $data['paymentTermName'] = $paymentDetails->paymentTermName;
        $data['outgoingPaymentCode'] = $paymentDetails->outgoingPaymentCode;
        $data['outgoingPaymentID'] = $paymentDetails->outgoingPaymentID;
        $data['cr_discount'] = $paymentDetails->outgoingPaymentDiscount;
        $data['total'] = $paymentDetails->outgoingPaymentAmount - $paymentDetails->outgoingPaymentDiscount;
        $data['cashPayment'] = $invoiceDetails->outgoingInvoiceCashAmount;
        $data['creditPayment'] = $invoiceDetails->outgoingInvoiceCreditAmount;
        $data['leftToPay'] = $invoiceDetails->left_to_pay_pv;
        $data['cdnUrl'] = $this->cdnUrl;

        return $data;
    }

    private function getPaginatedPaymentVouchers($userActiveLocationID = NULL)
    {
        $this->paginator = $this->CommonTable('Expenses\Model\PaymentVoucherTable')->getPaymentVouchers(true, $userActiveLocationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Expenses', 'Create PV', 'Accounting', 'Payment Voucher');
        $pVID = $this->params()->fromRoute('param1');

        $pVEdit = $this->createAction();
        $pVEdit->setTemplate('expenses/expense-purchase-invoice/create');
        $index = new ViewModel(
            array(
                'editMode' => true,
                'pVID' => $pVID,
                ));
        $index->addChild($pVEdit, 'pVEditDetails');
        $invoiceProductsView = new ViewModel();
        $invoiceProductsView->setTemplate('inventory/purchase-invoice/purchase-invoice-add-products');
        $index->addChild($invoiceProductsView, 'invoiceProducts');

        return $index;
    }


}

?>
