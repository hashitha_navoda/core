<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class AccountTypeController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'account_settings_bank_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Account Type', 'Accounting', 'Bank Details');
        //get page number
        $page = $this->params('param1', 1);
        //get bank list
        $paginator = $this->CommonTable('Expenses\Model\AccountTypeTable')->fetchAll(true, array('accountType.accountTypeId DESC'));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        return new ViewModel(array('accountTypes' => $paginator));
    }

}
