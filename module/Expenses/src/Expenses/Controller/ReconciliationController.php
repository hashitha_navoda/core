<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Reconciliation related actions
 */
class ReconciliationController extends CoreController
{    
     
    protected $sideMenus  = 'accounting_side_menu';
    protected $upperMenus = 'reconciliation_upper_menu';
    protected $downMenus = 'cash_bank_up_down_menu';
    
    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->downMenus = 'expense_up_down_menu'.$this->packageID;
        }
    }

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Cash & Bank', 'Transactions', 'Accounting', 'Reconciliation');
        
        //get existing banks
        $bankList = array();
        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll();
        foreach ($banks as $bank){
            $bankList[$bank[bankId]] = $bank['bankName'];
        }


        $relatedDocsView = new ViewModel();
        $relatedDocsView->setTemplate('expenses/cheque-deposit/related-docs');

        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/credit-note/document-view');

        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('inventory/out-going-payment/doc-history');
        
        $view = new ViewModel(array('banks'=>$bankList));
        $view->addChild($relatedDocsView, 'relatedDocsView');
        $view->addChild($documentView, 'documentView');
        $view->addChild($docHistoryView, 'docHistoryView');

        return $view;
    }


    public function editAction()
    {
        $this->getSideAndUpperMenus('Cash & Bank', 'Transactions', 'Accounting', 'Reconciliation');
        
        $reconciliationID = $this->params()->fromRoute('param1');

        $result = $this->CommonTable('Expenses\Model\ReconciliationTable')->getReconciliationWithAccountByReconciliationId($reconciliationID);

        return new ViewModel(array('reconciliationData'=>$result));
    }
    
    public function reconciliationAction()
    {
        $this->getSideAndUpperMenus('Cash & Bank', 'Reconciliation', 'Accounting', 'Reconciliation');
        
        //get existing banks
        $bankList = array();
        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll();
        foreach ($banks as $bank){
            $bankList[$bank[bankId]] = $bank['bankName'];
        }
        
        return new ViewModel(array('banks'=>$bankList));
    }
    
}
