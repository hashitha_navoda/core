<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;

class ExpenseTypeController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'expenses_types_upper_menu';
    protected $downMenus = 'approval_workflow_down_menu';
    protected $paginator;

    public function createAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'Create', null, 'Voucher Types');
        $expenseCategories = $this->CommonTable('Expenses\Model\ExpenseCategoryTable')->fetchAll();
        $employees = $this->CommonTable('Settings\Model\ApproverTable')->fetchAll();
        $employeeList = [];
        foreach ($employees as $employee) {
            $employeeList[] = $employee;
        }
        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/expenseType.js');
        return new ViewModel(array(
            'expenseCategories' => $expenseCategories,
            'employees' => $employeeList
        ));
    }

    public function editAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'View', null, 'Voucher Types');
        $editExpTypeId = $paramIn = $this->params()->fromRoute('param1');
        $expTypeData = $this->CommonTable("Expenses\Model\ExpenseTypeTable")->getExpenseTypeDetailsByID($editExpTypeId);
        $expTypeDetails = array();
        $financeAccountsArray = $this->getFinanceAccounts();
        foreach ($expTypeData as $expType) {
            $expTmp = array();
            $expTmp['expenseTypeId'] = $expType['expenseTypeId'];
            $expTmp['expenseTypeName'] = $expType['expenseTypeName'];
            $expTmp['expenseTypeCategory'] = $expType['expenseTypeCategory'];
            $expTmp['expenseTypeApproverEnabled'] = $expType['expenseTypeApproverEnabled'];
            $expTmp['expenseTypeStatus'] = $expType['expenseTypeStatus'];
            $expTmp['expenseTypeAccountID'] = $expType['expenseTypeAccountID'];
            $expTmp['expenseTypeAccountName'] = $financeAccountsArray[$expType['expenseTypeAccountID']]['financeAccountsCode']."_".$financeAccountsArray[$expType['expenseTypeAccountID']]['financeAccountsName'];
            $expTypeLimitsArray = isset($expTypeDetails['expTypeLimits']) ? $expTypeDetails['expTypeLimits'] : array();
            $expTypeLimitApprovers = isset($expTypeLimitsArray[$expType['expenseTypeLimitId']]['expTypeLimitApprovers']) ? $expTypeLimitsArray[$expType['expenseTypeLimitId']]['expTypeLimitApprovers'] : array();
            if ($expType['expenseTypeLimitId'] != NULL) {
                $expTypeLimitsArray[$expType['expenseTypeLimitId']] = array(
                    'expenseTypeLimitId' => $expType['expenseTypeLimitId'],
                    'expenseTypeLimitMin' => $expType['expenseTypeLimitMin'],
                    'expenseTypeLimitMax' => $expType['expenseTypeLimitMax'],
                );
                $expTypeLimitApprovers[$expType['expenseTypeLimitApproverId']] = $expType['expenseTypeLimitApprover'];
                $expTypeLimitsArray[$expType['expenseTypeLimitId']]['expTypeLimitApprovers'] = $expTypeLimitApprovers;
            }
            $expTmp['expTypeLimits'] = $expTypeLimitsArray;
            $expTypeDetails = $expTmp;
        }

        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/expenseType.js');
        $expenseCategories = $this->CommonTable('Expenses\Model\ExpenseCategoryTable')->fetchAll();
        $employees = $this->CommonTable('Settings\Model\ApproverTable')->fetchAll();
        $employeeList = [];
        foreach ($employees as $employee) {
            $employeeList[] = $employee;
        }
        return new ViewModel(array(
            'data' => $expTypeDetails,
            'expenseCategories' => $expenseCategories,
            'employees' => $employeeList,
            'editExpTypeId' => $editExpTypeId
        ));
    }

    public function listAction()
    {
        $this->getSideAndUpperMenus('Approval Workflow', 'View', null, 'Voucher Types');
        $this->getViewHelper('HeadScript')->prependFile('/js/expenses/expenseType.js');
        $this->getPaginatedExpenseTypes();
        return new ViewModel(array(
            'expTypes' => $this->paginator,
            'paginated' => TRUE));
    }

    private function getPaginatedExpenseTypes()
    {

        $this->paginator = $this->CommonTable('Expenses\Model\ExpenseTypeTable')->fetchAll(TRUE);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(6);
    }

}
