<?php

namespace Expenses\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Expenses\Form\AccountForm;

/**
 * @author Madawa Chandrarathne <madawa@thinkcube.com>
 * This file contains Account related actions
 */
class AccountController extends CoreController
{

    protected $sideMenus = 'accounting_side_menu';
    protected $upperMenus = 'account_settings_bank_upper_menu';
    protected $downMenus = 'account_settings_up_down_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Settings', 'Account', 'Accounting', 'Bank Details');

        //get page number
        $page = $this->params('param1', 1);

        //get existing banks
        $bankList = array();
        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll();
        foreach ($banks as $bank) {
            $bankList[$bank[bankId]] = $bank['bankName'];
        }

        //get banks account types
        $accountTypeList = array();
        $accountTypes = $this->CommonTable('Expenses\Model\AccountTypeTable')->fetchAll();
        foreach ($accountTypes as $accountType) {
            $accountTypeList[$accountType['accountTypeId']] = $accountType['accountTypeName'];
        }

        //get currency list for dropdown
        $currencyList = $this->CommonTable('Core\Model\CurrencyTable')->getActiveCurrencyListForDropDown();

        //create account form
        $form = new AccountForm('createAccount', $bankList, $accountTypeList, $currencyList);

        //get all accounts
        $paginator = $this->CommonTable('Expenses\Model\AccountTable')->fetchAll(true, array('account.accountId DESC'));
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);

        return new ViewModel(array('form' => $form, 'accounts' => $paginator, 'paginated' => true));
    }

}
