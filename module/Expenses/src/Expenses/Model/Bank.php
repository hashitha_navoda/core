<?php

namespace Expenses\Model;

/**
 * Class Bank
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class Bank {
    
    /**
     * @var int Auto incremented bank Id. (Primary key)
     */
    public $bankId;
    
    /**
     * @var string bank code.
     */
    public $bankCode;
    
    /**
     * @var string bank name.
     */
    public $bankName;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->bankId = (isset($data['bankId'])) ? $data['bankId'] : null;
        $this->bankCode = (isset($data['bankCode'])) ? $data['bankCode'] : null;
        $this->bankName = (isset($data['bankName'])) ? $data['bankName'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
}
