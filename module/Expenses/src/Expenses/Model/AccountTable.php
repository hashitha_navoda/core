<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
/**
 * Class AccountTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 *
 * This file contains Account Table Functions
 */
class AccountTable
{

    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Return all the accounts in the table.
     * @param boolean $paginated
     * @param array $order
     * @return mixed
     */
    public function fetchAll($paginated = FALSE, $order = array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
                ->join('bank', 'account.bankId = bank.bankId', array('bankName'))
                ->join('accountType', 'account.accountTypeId = accountType.accountTypeId', array('accountTypeName'))
                ->join('currency', 'account.currencyId = currency.currencyID', array('currencyName', 'currencySymbol'))
                ->order($order)
        ->where->equalTo('entity.deleted', 0);
        if ($paginated) {
            $paginatorAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * Return account according to the given id.
     * @param string $accountId
     * @return mixed
     */
    public function getAccountByAccountId($accountId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('currency', 'account.currencyId = currency.currencyId', array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('account.accountId', $accountId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

    /**
     * Return accounts which having numbers match to the given query string.
     * @param string $query
     * @param array $order
     * @param boolean $paginated
     * @return mixed
     */
    public function searchAccountByAccountNumber($query, $order = array(), $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0)
                ->like('account.accountNumber', '%' . $query . '%');
        if ($paginated) {
            $paginatorAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * Return accounts which having names match to the given query string.
     * @param string $query
     * @param array $order
     * @param boolean $paginated
     * @return mixed
     */
    public function searchAccountByAccountName($query, $order = array(), $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0)
                ->like('account.accountName', '%' . $query . '%');
        if ($paginated) {
            $paginatorAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * Return accounts according to the given accountTypeId.
     * @param string $accountTypeId
     * @return mixed
     */
    public function getAccountsByAccountTypeId($accountTypeId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('account.accountTypeId', $accountTypeId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Return accounts according to the given bankId.
     * @param string $bankId
     * @return mixed
     */
    public function getAccountsByBankId($bankId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('account.accountStatus', 1)
        ->where->equalTo('account.bankId', $bankId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Create new account
     * @param \Expenses\Model\Account $account
     * @return mixed
     */
    public function saveAccount(Account $account)
    {
        $data = array(
            'accountName' => $account->accountName,
            'accountNumber' => $account->accountNumber,
            'accountBalance' => $account->accountBalance,
            'accountStatus' => $account->accountStatus,
            'accountTypeId' => $account->accountTypeId,
            'currencyId' => $account->currencyId,
            'bankId' => $account->bankId,
            'bankBranchId' => $account->bankBranchId,
            'entityId' => $account->entityId,
            'isCardPaymentAccount' => $account->isCardPaymentAccount,
            'financeAccountID' => $account->financeAccountID
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    /**
     * Update existing account
     * @param array $data
     * @param string $accountId
     * @return boolean
     */
    public function updateAccount($data, $accountId)
    {

        if (!$accountId) {
            return false;
        }

        try {
            $this->tableGateway->update($data, array('accountId' => $accountId));
            return true;
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Get accounts by given field and value
     * @param string $field
     * @param string $value
     * @param array $order
     * @return mixed
     */
    public function getAccountsByTableField($field, $value, $order = array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
                ->join('bank', 'account.bankId = bank.bankId', array('bankName'))
                ->join('accountType', 'account.accountTypeId = accountType.accountTypeId', array('accountTypeName'))
                ->join('currency', 'account.currencyId = currency.currencyID', array('currencyName', 'currencySymbol'))
                ->order($order)
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('account.' . $field, $value);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Return accounts according to the given bank branch id.
     * @param string $branchId
     * @return mixed
     */
    public function getAccountsByBankBranchId($branchId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('account.bankBranchId', $branchId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get active accounts
     * @return type
     */
    public function getActiveAccounts()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
                ->join('currency', 'account.currencyId = currency.currencyID', array('currencyName', 'currencySymbol'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('account.accountStatus', '1');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get account currency details by account id
     * @param int $accountId
     * @return mixed
     */
    public function getAccountCurrencyDetailsByAccountId($accountId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('currencyId'))
                ->join('currency', 'account.currencyId = currency.currencyID', array('*'))
        ->where->equalTo('account.accountId', $accountId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

    /**
     * this function use to get details that related to given account number and bank
     * @param int $accountNo
     * @param int $bankID
     * @param int $accountID
     * @return mix
     */
    public function checkAccountNumberIsExists($accountNo, $bankID, $accountID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('account.accountNumber', $accountNo)
        ->where->equalTo('account.bankId', $bankID);
        if ($accountID) {
            $select->where->notEqualTo('account.accountId', $accountID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSets = $statement->execute();
        return $resultSets;
    }

    public function getAccountDetailsByFinanceAccountId($fAccountId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('*'))
        ->where->equalTo('account.financeAccountID', $fAccountId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get Bank Account Balance for dashboard.
     */
    public function getBankAccountBalanceForDashboard($paginated = FALSE, $order = array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('account')
                ->columns(array('totalBalance' => new Expression('SUM(accountBalance)')))
                ->join('entity', 'account.entityId = entity.entityID', array('deleted'))
                ->join('bank', 'account.bankId = bank.bankId', array('bankName'))
                ->join('accountType', 'account.accountTypeId = accountType.accountTypeId', array('accountTypeName'))
                ->join('currency', 'account.currencyId = currency.currencyID', array('currencyName', 'currencySymbol'))
                // ->order($order)
        ->where->equalTo('entity.deleted', 0);
        if ($paginated) {
            $paginatorAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
}
