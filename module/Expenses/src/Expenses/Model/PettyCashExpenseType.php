<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Description of ExpenseType
 *
 * @author shermilan
 */
class PettyCashExpenseType implements InputFilterAwareInterface
{

    public $pettyCashExpenseTypeID;
    public $pettyCashExpenseTypeName;
    public $pettyCashExpenseTypeDescription;
    public $pettyCashExpenseTypeMinAmount;
    public $pettyCashExpenseTypeMaxAmount;
    public $pettyCashExpenseTypeStatus;
    public $pettyCashExpenseTypeAccountID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->pettyCashExpenseTypeID = (isset($data['pettyCashExpenseTypeID'])) ? $data['pettyCashExpenseTypeID'] : null;
        $this->pettyCashExpenseTypeName = (isset($data['pettyCashExpenseTypeName'])) ? $data['pettyCashExpenseTypeName'] : null;
        $this->pettyCashExpenseTypeDescription = (isset($data['pettyCashExpenseTypeDescription'])) ? $data['pettyCashExpenseTypeDescription'] : null;
        $this->pettyCashExpenseTypeMinAmount = (!empty($data['pettyCashExpenseTypeMinAmount'])) ? $data['pettyCashExpenseTypeMinAmount'] : null;
        $this->pettyCashExpenseTypeMaxAmount = (!empty($data['pettyCashExpenseTypeMaxAmount'])) ? $data['pettyCashExpenseTypeMaxAmount'] : null;
        $this->pettyCashExpenseTypeStatus = (isset($data['pettyCashExpenseTypeStatus'])) ? $data['pettyCashExpenseTypeStatus'] : 1;
        $this->pettyCashExpenseTypeAccountID = ($data['pettyCashExpenseTypeAccountID'] != "") ? $data['pettyCashExpenseTypeAccountID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashExpenseTypeID',
                                'required' => false,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Int',
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashExpenseTypeName',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'StringLength',
                                        'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min' => 1,
                                            'max' => 100,
                                        ),
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashExpenseTypeDescription',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'StringLength',
                                        'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min' => 1,
                                            'max' => 100,
                                        ),
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashExpenseTypeMinAmount',
                                'required' => false,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Float',
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashExpenseTypeMaxAmount',
                                'required' => false,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Float',
                                    ),
                                ),
                            )
                    )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

}
