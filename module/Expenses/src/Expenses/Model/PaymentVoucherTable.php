<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PaymentVoucherTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * @param Paginator true or false
     * @return paginator object if true, else resultset
     */
    public function getPaymentVouchers($paginated = FALSE, $userActiveLocationID = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('paymentVoucher');
        $select->order('paymentVoucherID DESC');
        $select->join('entity', 'paymentVoucher.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'paymentVoucher.paymentVoucherLocationID = location.locationID', array('locationName', 'locationID'));
        $select->join('supplier', 'paymentVoucher.paymentVoucherSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'), left);
        $select->where(array('deleted' => '0'));
        $select->where->notEqualTo('paymentVoucher.paymentVoucherStatus', 10);
        if ($userActiveLocationID != NULL) {
            $select->where(array('locationID' => $userActiveLocationID));
        }

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getPaymentVouchersforSearch($pVSearchKey, $locationID = False)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('paymentVoucher');
        $select->columns(array(
            'posi' => new Expression('LEAST(IF(POSITION(\'' . $pVSearchKey . '\' in paymentVoucher.paymentVoucherCode )>0,POSITION(\'' . $pVSearchKey . '\' in paymentVoucher.paymentVoucherCode), 9999),'
                    . 'IF(POSITION(\'' . $pVSearchKey . '\' in supplier.supplierName )>0,POSITION(\'' . $pVSearchKey . '\' in supplier.supplierName), 9999)) '),
            'len' => new Expression('LEAST(CHAR_LENGTH(paymentVoucher.paymentVoucherCode ), CHAR_LENGTH(supplier.supplierName )) '),
            '*',
        ));
        $select->join('entity', 'paymentVoucher.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'paymentVoucher.paymentVoucherLocationID = location.locationID', array('locationName'), 'left');
        $select->join('supplier', 'paymentVoucher.paymentVoucherSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'), 'left');
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', '0'))));
        $select->where(new PredicateSet(array(new Operator('paymentVoucher.paymentVoucherCode', 'like', '%' . $pVSearchKey . '%'), new Operator('supplier.supplierName', 'like', '%' . $pVSearchKey . '%')), PredicateSet::OP_OR));
        if ($locationID) {
            $select->where(array('paymentVoucher.paymentVoucherLocationID' => $locationID));
        }
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC'));
        $select->where->notEqualTo('paymentVoucher.paymentVoucherStatus', 10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getPIsByPICode($piCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('purchaseInvoice');
        $select->join('purchaseInvoiceProduct', 'purchaseInvoice.purchaseInvoiceID = purchaseInvoiceProduct.purchaseInvoiceID', array('*'));
        $select->join('locationProduct', 'purchaseInvoiceProduct.locationProductID = locationProduct.locationProductID', array('productID'));
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'));
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'));
        $select->join('productBatch', 'purchaseInvoiceProduct.productBatchID = productBatch.productBatchID', array('productBatchCode', 'productBatchExpiryDate', 'productBatchWarrantyPeriod', 'productBatchManufactureDate', 'productBatchQuantity'), 'left');
        $select->join('productSerial', 'purchaseInvoiceProduct.productSerialID = productSerial.productSerialID', array('productSerialCode', 'productSerialWarrantyPeriod', 'productSerialExpireDate'), 'left');
        $select->join('supplier', 'purchaseInvoice.purchaseInvoiceSupplierID = supplier.supplierID', array('supplierCode', 'supplierName'), 'left');
        $select->join('location', 'purchaseInvoice.purchaseInvoiceRetrieveLocation = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('purchaseInvoiceProductTax', 'purchaseInvoiceProduct.purchaseInvoiceProductID = purchaseInvoiceProductTax.purchaseInvoiceProductID', array('purchaseInvoiceTaxID', 'purchaseInvoiceTaxPrecentage', 'purchaseInvoiceTaxAmount'), 'left');
        $select->join('tax', 'purchaseInvoiceProductTax.purchaseInvoiceTaxID = tax.id', array('taxName'), 'left');
        $select->where(array('purchaseInvoiceCode' => $piCode));
        $select->where(array('productUomBase' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function checkPaymentVoucherByCode($paymentVoucherCode, $locationID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('paymentVoucher')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "paymentVoucher.entityID = e.entityID")
                    ->where(array('paymentVoucherLocationID' => $locationID))
                    ->where(array('e.deleted' => '0'))
                    ->where(array('paymentVoucher.paymentVoucherCode' => $paymentVoucherCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getPaymentVoucherDetailsByPVID($pVID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('paymentVoucher');
        $select->join('paymentVoucherProduct', 'paymentVoucher.paymentVoucherID = paymentVoucherProduct.paymentVoucherID', array('paymentVoucherProductID', 'locationProductID', 'paymentVoucherProductQuantity', 'paymentVoucherProductUnitPrice', 'paymentVoucherProductTotalPrice', 'paymentVoucherProductQuantityByBatch', 'productBatch', 'productSerial', 'batchSerialData', 'roockID', 'paymentVoucherProductDiscount', 'paymentVoucherProductExpenseTypeId', 'paymentVoucherProductUomID' => 'uomID'), 'left');
        $select->join(['pvpet' => 'expenseType'], 'paymentVoucherProduct.paymentVoucherProductExpenseTypeId = pvpet.expenseTypeId', array('paymentVoucherProductExpenseType' => 'expenseTypeName'), 'left');
        $select->join('paymentVoucherNonItemProduct', 'paymentVoucher.paymentVoucherID = paymentVoucherNonItemProduct.paymentVoucherID', array('paymentVoucherNonItemProductID', 'paymentVoucherNonItemProductDiscription', 'paymentVoucherNonItemProductQuantity', 'paymentVoucherNonItemProductUnitPrice', 'paymentVoucherNonItemProductTotalPrice', 'roockID', 'paymentVoucherNonItemProductExpenseTypeId', 'paymentVoucherNonItemProductExpenseTypeFormat'), 'left');
        $select->join(['pvipet' => 'expenseType'], 'paymentVoucherNonItemProduct.paymentVoucherNonItemProductExpenseTypeId = pvipet.expenseTypeId', array('paymentVoucherNonItemProductExpenseType' => 'expenseTypeName'), 'left');
        $select->join('locationProduct', 'paymentVoucherProduct.locationProductID = locationProduct.locationProductID', array('productID'), 'left');
        $select->join('product', 'locationProduct.productID = product.productID', array('productCode', 'productName'), 'left');
        $select->join('productUom', 'locationProduct.productID = productUom.productID', array('uomID', 'productUomConversion'), 'left');
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'), 'left');
        $select->join('supplier', 'paymentVoucher.paymentVoucherSupplierID = supplier.supplierID', array('supplierCode', 'supplierName', 'supplierEmail', 'supplierAddress', 'supplierTitle'), 'left');
        $select->join('location', 'paymentVoucher.paymentVoucherLocationID = location.locationID', array('locationCode', 'locationName'), 'left');
        $select->join('paymentVoucherProductTax', 'paymentVoucher.paymentVoucherID = paymentVoucherProductTax.paymentVoucherID', array('paymentVoucherProductTaxID', 'paymentVoucherProductIdOrNonItemProductID', 'paymentVoucherTaxID', 'paymentVoucherTaxPrecentage', 'paymentVoucherTaxAmount', 'paymentVoucherTaxItemOrNonItemProduct'), 'left');
        $select->join('tax', 'paymentVoucherProductTax.paymentVoucherTaxID = tax.id', array('taxName'), 'left');
        $select->join('entity', 'paymentVoucher.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->where(array('paymentVoucher.paymentVoucherID' => $pVID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function savePaymentVoucher(PaymentVoucher $pavVaucher)
    {
        $data = array(
            'paymentVoucherCode' => $pavVaucher->paymentVoucherCode,
            'paymentVoucherSupplierID' => $pavVaucher->paymentVoucherSupplierID,
            'paymentVoucherSupplierReference' => $pavVaucher->paymentVoucherSupplierReference,
            'paymentVoucherAccountID' => $pavVaucher->paymentVoucherAccountID,
            'paymentVoucherLocationID' => $pavVaucher->paymentVoucherLocationID,
            'paymentVoucherDueDate' => $pavVaucher->paymentVoucherDueDate,
            'paymentVoucherIssuedDate' => $pavVaucher->paymentVoucherIssuedDate,
            'paymentVoucherComment' => $pavVaucher->paymentVoucherComment,
            'paymentVoucherDeliveryCharge' => $pavVaucher->paymentVoucherDeliveryCharge,
            'paymentVoucherShowTax' => $pavVaucher->paymentVoucherShowTax,
            'paymentVoucherTotal' => $pavVaucher->paymentVoucherTotal,
            'paymentVoucherPaidAmount' => $pavVaucher->paymentVoucherPaidAmount,
            'entityID' => $pavVaucher->entityID,
            'paymentVoucherItemNotItemFlag' => $pavVaucher->paymentVoucherItemNotItemFlag,
            'paymentVoucherStatus' => 7, //this is Draft state
            'paymentVoucherExpenseType' => $pavVaucher->paymentVoucherExpenseType,
            'paymentVoucherHashValue' => $pavVaucher->paymentVoucherHashValue
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function updatePaymentVoucherStatus($paymentVoucherID, $statusID)
    {
        $data = array('paymentVoucherStatus' => $statusID);
        $this->tableGateway->update($data, array('paymentVoucherID' => $paymentVoucherID));
    }

    //get location related half payment voucher
    public function getLocationRelatedHalfPaymentVouchers($locationID, $searchKey = FALSE)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("paymentVoucher")
                ->columns(array("*"))
                ->join('entity', 'paymentVoucher.entityID=entity.entityID', array('*'), 'left')
                ->order(array('paymentVoucherIssuedDate' => 'DESC'))
                ->where(array('deleted' => '0', 'paymentVoucherLocationID' => $locationID))
        ->where->notEqualTo('paymentVoucherTotal', 'paymentVoucherPaidAmount');
         $select->where->notEqualTo('paymentVoucher.paymentVoucherStatus', 10);
        if ($searchKey) {
            $select->where->like('paymentVoucher.paymentVoucherCode', '%' . $searchKey . '%');
            $select->limit(50);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    //get payment  voucher details by supplier ID
    public function getPaymentVouchersBySupplierID($id, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("paymentVoucher")
                ->columns(array("*"))
                ->join("supplier", "paymentVoucher.paymentVoucherSupplierID = supplier.supplierID", array("*"), "left")
                ->join("status", "paymentVoucher.paymentVoucherStatus= status.statusID", array("*"), "left")
                ->order(array('paymentVoucherIssuedDate' => 'DESC'))
                ->where(array('paymentVoucher.paymentVoucherSupplierID' => $id, 'paymentVoucher.paymentVoucherLocationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    //update payment of Invoice
    public function updatePaymentOfPaymentVoucher(PaymentVoucher $paymentVoucher)
    {
        try {
            $data = array(
                'paymentVoucherPaidAmount' => $paymentVoucher->paymentVoucherPaidAmount,
                'paymentVoucherStatus' => $paymentVoucher->paymentVoucherStatus,
            );
            $this->tableGateway->update($data, array('paymentVoucherID' => $paymentVoucher->paymentVoucherID));
            return TRUE;
            
        } catch (Exception $e) {
            error_log($e);
            return false;
        }
    }

    public function updateApprovedStatus($paymentVoucherId, $status, $isReject = false)
    {
        if ($isReject) {
            $data = array(
                'paymentVoucherApproved' => $status,
                'paymentVoucherStatus' => 13
            );
        } else {
            $data = array(
                'paymentVoucherApproved' => $status
            );
        }
        $this->tableGateway->update($data, array('paymentVoucherID' => $paymentVoucherId));
    }

    public function getPaymentVouchersForReport($expenseTypes = [], $fromDate = false, $toDate = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('paymentVoucher');
        $select->order('paymentVoucherCode');
        $select->join('expenseType', 'paymentVoucher.paymentVoucherExpenseType = expenseType.expenseTypeId', ['expenseTypeName'], 'left');
        $select->where->notEqualTo('paymentVoucher.paymentVoucherStatus', 10);

        if ($expenseTypes != null && count($expenseTypes) > 0) {
            $select->where->in('paymentVoucher.paymentVoucherExpenseType', $expenseTypes);
        }
        if ($fromDate && $toDate) {
            $select->where->between('paymentVoucher.paymentVoucherIssuedDate', $fromDate, $toDate);
        }

        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getPaymentVouchersView($supplierID = false, $pvID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('paymentVoucher');
        $select->join('entity', 'paymentVoucher.entityID = entity.entityID', array('deleted'));
        $select->join('location', 'paymentVoucher.paymentVoucherLocationID = location.locationID', array('locationName', 'locationID'));
        $select->join('supplier', 'paymentVoucher.paymentVoucherSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle'), 'left');
        $select->where(['entity.deleted' => '0']);
        $select->where->notEqualTo('paymentVoucher.paymentVoucherStatus', 10);
        if ($supplierID) {
            $select->where(['supplier.supplierID' => $supplierID]);
        }
        if ($pvID) {
            $select->where(['paymentVoucher.paymentVoucherID' => $pvID]);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * get overdue payment Voucher IDs
     * @param type $locationID
     * @param type $statusID
     * @return type
     */
    public function getOverduePaymentVoucherIDs($locationID)
    {
        $openStatusID = '3';
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("paymentVoucherID", "paymentVoucherStatus", "paymentVoucherLocationID"));
        $select->from('paymentVoucher');
        $select->where->lessThan("paymentVoucherDueDate", new Expression('CURDATE()'))->AND->equalTo("paymentVoucherStatus", $openStatusID)->AND->equalTo('paymentVoucherLocationID', $locationID);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    public function getPaymentVoucherByPaymentVoucherID($pvID)
    {
        $rowset = $this->tableGateway->select(array('paymentVoucherID' => $pvID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getPaymentVoucherByPaymentVoucherIDForJE($pvID)
    {
        
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('paymentVoucher');
        $select->join("supplier", "paymentVoucher.paymentVoucherSupplierID = supplier.supplierID", array("*"), "left");
        $select->join('financeAccounts','paymentVoucher.paymentVoucherAccountID = financeAccounts.financeAccountsID', array('financeAccountsCode','financeAccountsName'),'left');        
        $select->where(['paymentVoucher.paymentVoucherID' => $pvID]);
        $select->where->notEqualTo('paymentVoucher.paymentVoucherStatus', 5);
        $select->where->notEqualTo('paymentVoucher.paymentVoucherStatus', 10);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }


    public function getPaymentVoucherById($pvID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('paymentVoucher');
        $select->join('location', 'paymentVoucher.paymentVoucherLocationID = location.locationID', array('locationName', 'locationID'));
        $select->where(['paymentVoucher.paymentVoucherID' => $pvID]);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    /**
    * get all payment vouche details by given supllier 
    *
    **/    
    public function getPaymentVouchersBySupplierIDs($supplierIDs = [], $status, $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("paymentVoucher")
                ->columns(array("*"))
                ->join("supplier", "paymentVoucher.paymentVoucherSupplierID = supplier.supplierID", array("*"), "left")
                ->join("status", "paymentVoucher.paymentVoucherStatus= status.statusID", array("*"), "left")
                ->join('entity', ' paymentVoucher.entityID = entity.entityID', array('deleted','createdTimeStamp'), 'left')
                ->order(array('paymentVoucherIssuedDate' => 'DESC'))
                ->where(array('deleted' => 0));
        $select->where->in('paymentVoucher.paymentVoucherSupplierID', $supplierIDs);
        $select->where->in('paymentVoucher.paymentVoucherStatus', $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('paymentVoucher.paymentVoucherIssuedDate', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
    * this function use to get supplier outstanding by given date range
    * @param array $supplierIds
    * @param date $endDate
    * @param boolean $isAllSuppliers
    * return array
    **/
    public function getAgedSupplierPaymentVoucherData($supplierIds, $endDate, $isAllSuppliers = false)
    {
        $agedAnalysisData = [];
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('paymentVoucher');
        $select->columns(array('paymentVoucherIssuedDate',
            'paymentVoucherCode', 'paymentVoucherID', 'paymentVoucherSupplierID','paymentVoucherIssuedDate','paymentVoucherTotal'
        ));
        $select->join('supplier', 'paymentVoucher.paymentVoucherSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle', 'supplierCode'), 'left');
        
        $select->join('entity', 'paymentVoucher.entityID = entity.entityID', array('deleted'), 'left');
        $select->order(array('paymentVoucher.paymentVoucherCode'));
        
        $select->where->lessThanOrEqualTo('paymentVoucherIssuedDate', $endDate);
        if ($supplierIds && !$isAllSuppliers) {
            $select->where->in('paymentVoucher.paymentVoucherSupplierID', $supplierIds);
        }
        $select->where('paymentVoucher.paymentVoucherSupplierID IS NOT NULL');
        $select->where->in('paymentVoucher.paymentVoucherStatus', ['3','6', '4']);
        
        $select->where(array('entity.deleted' => '0'));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $agedAnalysisData[] = $val;
        }
        
        return $agedAnalysisData;
    }

    /**
    * this function use to get advance supplier outstanding by given date range
    * @param array $supplierIds
    * @param date $endDate
    * @param boolean $isAllSuppliers
    * return array
    **/
    public function getAgedSupplierPaymentVoucherAdvanceData($supplierIds, $endDate, $groupBy, $isAllSuppliers = false)
    {
        $agedAnalysisData = [];
        $isAllSuppliers = filter_var($isAllSuppliers, FILTER_VALIDATE_BOOLEAN);
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('paymentVoucher');
        $select->columns(array('paymentVoucherIssuedDate',
            'paymentVoucherCode', 'paymentVoucherID', 'paymentVoucherSupplierID','paymentVoucherIssuedDate','paymentVoucherTotal'
        ));
        $select->join('supplier', 'paymentVoucher.paymentVoucherSupplierID = supplier.supplierID', array('supplierName', 'supplierTitle', 'supplierCode'), 'left');
        
        $select->join('entity', 'paymentVoucher.entityID = entity.entityID', array('deleted'), 'left');
        $select->order(array('paymentVoucher.paymentVoucherCode'));
        
        $select->where->lessThanOrEqualTo('paymentVoucherIssuedDate', $endDate);
        if ($supplierIds && !$isAllSuppliers) {
            $select->where->in('paymentVoucher.paymentVoucherSupplierID', $supplierIds);
        }
        $select->where('paymentVoucher.paymentVoucherSupplierID IS NOT NULL');
        $select->where->in('paymentVoucher.paymentVoucherStatus', ['3','6','4']);
        
        $select->where(array('entity.deleted' => '0'));
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $agedAnalysisData[] = $val;
        }
                
        return $agedAnalysisData;
    } 
    public function getPaymentVouchersBySupplierIDsForSupplierBalance($supplierIDs = [], $status, $fromdate = null, $todate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("paymentVoucher")
                ->columns(array("*"))
                ->join("supplier", "paymentVoucher.paymentVoucherSupplierID = supplier.supplierID", array("*"), "left")
                ->join("status", "paymentVoucher.paymentVoucherStatus= status.statusID", array("*"), "left")
                ->join('entity', ' paymentVoucher.entityID = entity.entityID', array('deleted','createdTimeStamp'), 'left')
                ->order(array('createdTimeStamp' => 'DESC'))
                ->where(array('deleted' => 0));
        $select->where->in('paymentVoucher.paymentVoucherSupplierID', $supplierIDs);
        $select->where->in('paymentVoucher.paymentVoucherStatus', $status);
        if($fromdate != '' && $todate != ''){
            $select->where->between('entity.createdTimeStamp', $fromdate, $todate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function updatePaymentVoucherCommentAndDates($paymentVoucherID, $data)
    {
        $this->tableGateway->update($data, array('paymentVoucherID' => $paymentVoucherID));
    }

     public function getPVByPvIDForRec($paymentVoucherID, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('paymentVoucher')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "paymentVoucher.entityID = e.entityID")
                    ->where(array('paymentVoucher.paymentVoucherID' => $paymentVoucherID));
                $select->where(array('e.deleted' => '0'));
            
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

}
