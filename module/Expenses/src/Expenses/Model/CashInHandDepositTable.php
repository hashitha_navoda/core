<?php
namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class CashInHandDepositTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 *
 * This file contains CashInHand Deposit Table Functions
 */
class CashInHandDepositTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * CashInHandDepositTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Create new cash in hand deposit
     * @param \Expenses\Model\CashInHandDeposit $cashInHandDeposit
     * @return mixed
     */
    public function saveDeposit(CashInHandDeposit $cashInHandDeposit)
    {
        $data = array(
            'cashInHandDepositAmount' => $cashInHandDeposit->cashInHandDepositAmount,
            'cashInHandDepositDate' => $cashInHandDeposit->cashInHandDepositDate,
            'cashInHandDepositComment' => $cashInHandDeposit->cashInHandDepositComment,
            'locationId' => $cashInHandDeposit->locationId,
            'entityId' => $cashInHandDeposit->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Get All available cash in hand deposits by location
     * @param int $locationId
     * @return mixed
     */
    public function getCashInHandDepositsByLocationId( $locationId) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cashInHandDeposit')
                ->columns(array('*'))
                ->join('entity', 'cashInHandDeposit.entityId = entity.entityID', array('deleted'), "left")
                ->where(array('cashInHandDeposit.locationId' => $locationId, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }
    
    /**
     * Get deposits by location id
     * @param int $locationId
     * @param date $fromDate
     * @param date $toDate
     * @return mixed
     */
    public function getAllDepositsByLocationId( $locationId, $fromDate = null, $toDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cashInHandDeposit')
                ->columns(array('*'))
                ->join('entity', 'cashInHandDeposit.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('cashInHandDeposit.locationId', $locationId)
                ->where->equalTo('entity.deleted', 0);
        if ($fromDate && $toDate) {
            $select->where->between('cashInHandDeposit.cashInHandDepositDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * Get cash in hand deposit details by cashInHandDepositId
     * @param int $depositId
     * @return mixed
     */
    public function getDepositByDepositId( $depositId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cashInHandDeposit')
                ->columns(array('*'))
                ->where->equalTo('cashInHandDeposit.cashInHandDepositId', $depositId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if($result->count() == 0){
            return NULL;
        } else {
            return $result->current();
        }
    }
}
