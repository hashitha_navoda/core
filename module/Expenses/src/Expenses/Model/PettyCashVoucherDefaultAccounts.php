<?php

namespace Expenses\Model;

/**
 * Class Petty Cash Voucher Default Accounts
 * @package Expenses\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class PettyCashVoucherDefaultAccounts {
    
    /**
     * @var int Auto incremented pettyCashVoucherDefaultAccountsID. (Primary key)
     */
    public $pettyCashVoucherDefaultAccountsID;
    
    /**
     * @var int pettyCashVoucherFinanceAccountID.
     */
    public $pettyCashVoucherFinanceAccountID;
    
    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->pettyCashVoucherDefaultAccountsID = (isset($data['pettyCashVoucherDefaultAccountsID'])) ? $data['pettyCashVoucherDefaultAccountsID'] : null;
        $this->pettyCashVoucherFinanceAccountID = (isset($data['pettyCashVoucherFinanceAccountID'])) ? $data['pettyCashVoucherFinanceAccountID'] : null;
    }
}