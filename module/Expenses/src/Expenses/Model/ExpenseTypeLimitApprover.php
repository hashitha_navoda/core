<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class Expense Type Limit Approver
 * @package Expenses\Model
 * @author Damith Thamara <damith@thinkcube.com>
 */
class ExpenseTypeLimitApprover implements InputFilterAwareInterface
{

    public $expenseTypeLimitApproverId;
    public $expenseTypeLimitId;
    public $expenseTypeLimitApprover;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * Job employee ID mapped to expenseTypeLimitApprover
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->expenseTypeLimitApproverId = (isset($data['expenseTypeLimitApproverId'])) ? $data['expenseTypeLimitApproverId'] : null;
        $this->expenseTypeLimitId = (isset($data['expenseTypeLimitId'])) ? $data['expenseTypeLimitId'] : null;
        $this->expenseTypeLimitApprover = (isset($data['expenseTypeLimitApprover'])) ? $data['expenseTypeLimitApprover'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
