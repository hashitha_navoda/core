<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * Class ChequeDepositTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 * 
 * This file contains Cheque Deposit Table Functions
 */
class ChequeDepositTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * ChequeDepositTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Return all the cheque deposits in the table.
     * @param boolean $paginated
     * @param array $order
     * @return mixed
     */
    public function fetchAll($paginated = FALSE,$order=array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('chequeDeposit')
                ->columns(array('*'))
                ->join('bank', 'chequeDeposit.bankId = bank.bankId', array('bankName'))
                ->join('account', 'chequeDeposit.accountId = account.accountId', array('accountNumber','accountName'))
                ->join('currency', 'chequeDeposit.chequeDepositCurrencyId = currency.currencyId', array('currencyName','currencySymbol'))
                ->join('entity', 'chequeDeposit.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0);
        if ($paginated){
            $paginatorAdapter = new DbSelect($select,$this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    /**
     * Save cheque deposit
     * @param \Expenses\Model\ChecqueDeposit $checqueDeposit
     * @return mixed
     */
    public function saveChequeDeposit($checqueDeposit)
    {
        $data = array(
            'chequeDepositAmount' => $checqueDeposit->chequeDepositAmount,
            'chequeDepositDate' =>$checqueDeposit->chequeDepositDate,
            'chequeDepositDescription' => $checqueDeposit->chequeDepositDescription,
            'chequeDepositCurrencyId' => $checqueDeposit->chequeDepositCurrencyId,
            'chequeDepositCurrencyRate' => $checqueDeposit->chequeDepositCurrencyRate,
            'chequeDepositAccountCurrencyRate' => $checqueDeposit->chequeDepositAccountCurrencyRate,
            'bankId' => $checqueDeposit->bankId,
            'accountId' => $checqueDeposit->accountId,
            'entityId' => $checqueDeposit->entityId
        );
        
        if($this->tableGateway->insert($data)){
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Update existing cheque deposit
     * @param array $data
     * @param string $depositId
     * @return boolean
     */
    public function updateChequeDeposit($data, $depositId)
    {        
        if($depositId){
            if($this->tableGateway->update($data, array('chequeDepositId'=>$depositId))){
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    /**
     * Return cheque deposit according to the given cheque deposit id.
     * @param string $depositId
     * @return mixed
     */
    public function getChequeDepositByChequeDeposiId($depositId) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('chequeDeposit')
                ->columns(array('*'))
                ->join('entity', 'chequeDeposit.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('chequeDeposit.chequeDepositId',$depositId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Return deposited cheques to the given account id.
     * @param string $accountId
     * @return mixed
     */
    public function getDepositedChequeByAccountId( $accountId, $recStatus = null, $startDate = null, $endDate = null) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('chequeDeposit')
                ->columns(array('*'))
                ->join('chequeDepositCheque', 'chequeDeposit.chequeDepositId = chequeDepositCheque.chequeDepositId', array('incomingPaymentMethodChequeId'))
                ->join('incomingPaymentMethodCheque', 'chequeDepositCheque.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId=incomingPaymentMethod.incomingPaymentMethodChequeId', array('incomingPaymentMethodAmount'))
                ->join('incomingPayment', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentID', array('*'), 'left')
                ->join('entity', 'chequeDepositCheque.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('chequeDeposit.accountId',$accountId);
        if(!is_null($recStatus)){
            $select->where->equalTo('incomingPaymentMethodCheque.incomingPaymentMethodChequeReconciliationStatus', $recStatus);
        }
        if($startDate && !$endDate){
            $select->where->greaterThan('chequeDeposit.chequeDepositDate', $startDate);
        }
        if($startDate && $endDate){
            $select->where->between('chequeDeposit.chequeDepositDate', $startDate, $endDate);            
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getChequeDepositByChequeDeposiIdForJE($depositId) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('chequeDeposit')
                ->columns(array('*'))
                ->join('chequeDepositCancellation', 'chequeDeposit.chequeDepositId = chequeDepositCancellation.chequeDepositId')
                ->where->equalTo('chequeDeposit.chequeDepositId',$depositId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
}
