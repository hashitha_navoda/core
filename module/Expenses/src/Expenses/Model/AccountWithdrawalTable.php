<?php
namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class AccountWithdrawalTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 *
 * This file contains Account Withdrawal Table Functions
 */
class AccountWithdrawalTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountWithdrawalTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Create new account withdrawal
     * @param \Expenses\Model\AccountWithdrawal $accountWithdrawal
     * @return mixed
     */
    public function saveWithdrawal(AccountWithdrawal $accountWithdrawal)
    {
        $data = array(
            // 'accountWithdrawalAccountId' => $accountWithdrawal->accountWithdrawalAccountId,
            'accountWithdrawalAmount' => $accountWithdrawal->accountWithdrawalAmount,
            'accountWithdrawalDate' => $accountWithdrawal->accountWithdrawalDate,
            'accountWithdrawalComment' => $accountWithdrawal->accountWithdrawalComment,
            'accountWithdrawalIssuedFinanceAccountId' => $accountWithdrawal->accountWithdrawalIssuedFinanceAccountId,
            'accountWithdrawalRecivedFinanceAccountId' => $accountWithdrawal->accountWithdrawalRecivedFinanceAccountId,
            'entityId' => $accountWithdrawal->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Get withdrawals by account id
     * @param int $accountId
     * @param date $fromDate
     * @param date $toDate
     * @return mixed
     */
    public function getWithdrawalsByAccountId( $accountId = null, $fromDate = null, $toDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountWithdrawal')
                ->columns(array('*'))                
                ->join('entity', 'accountWithdrawal.entityId = entity.entityID', array('deleted'))
                ->join('financeAccounts', 'accountWithdrawal.accountWithdrawalIssuedFinanceAccountId = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join(array('RfinanceAccounts'=>'financeAccounts'), 'accountWithdrawal.accountWithdrawalRecivedFinanceAccountId = RfinanceAccounts.financeAccountsID', array('recivedFinanceAccountsCode'=>'financeAccountsCode','recivedFinanceAccountsName'=>'financeAccountsName'))
                ->where->equalTo('entity.deleted', 0);
        if($accountId){
            $select->where->equalTo('account.financeAccountID', $accountId);
            $select->join('account', 'financeAccounts.financeAccountsID = account.financeAccountID', array('*'),'left');
        }        
        if ($fromDate && $toDate) {
            $select->where->between('accountWithdrawal.accountWithdrawalDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
             
        return $result;
    }
    
    /**
     * Get account withdrawal details by accountWithdrawalId
     * @param int $withdrawalId
     * @return mixed
     */
    public function getWithdrawalByWithdrawalId( $withdrawalId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountWithdrawal')
                ->columns(array('*'))
                ->join('entity', 'accountWithdrawal.entityId = entity.entityID', array('deleted'))
                ->join('financeAccounts', 'accountWithdrawal.accountWithdrawalIssuedFinanceAccountId = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join(array('RfinanceAccounts'=>'financeAccounts'), 'accountWithdrawal.accountWithdrawalRecivedFinanceAccountId = RfinanceAccounts.financeAccountsID', array('recivedFinanceAccountsCode'=>'financeAccountsCode','recivedFinanceAccountsName'=>'financeAccountsName'))
                ->where(array('accountWithdrawal.accountWithdrawalId' => $withdrawalId, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if($result->count() == 0){
            return NULL;
        } else {
            return $result->current();
        }
    }

    public function getWithdrawalsByGlAccountId( $financeAccountId = null, $fromDate = null, $toDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountWithdrawal')
                ->columns(array('*'))                
                ->join('entity', 'accountWithdrawal.entityId = entity.entityID', array('deleted'))
                ->join('financeAccounts', 'accountWithdrawal.accountWithdrawalIssuedFinanceAccountId = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('account', 'financeAccounts.financeAccountsID = account.financeAccountID', array('*'),'left')
                ->join(array('RfinanceAccounts'=>'financeAccounts'), 'accountWithdrawal.accountWithdrawalRecivedFinanceAccountId = RfinanceAccounts.financeAccountsID', array('recivedFinanceAccountsCode'=>'financeAccountsCode','recivedFinanceAccountsName'=>'financeAccountsName'))
                ->where->equalTo('entity.deleted', 0);
        if($financeAccountId){
            $select->where->equalTo('accountWithdrawal.accountWithdrawalIssuedFinanceAccountId', $financeAccountId);
        }        
        if ($fromDate && $toDate) {
            $select->where->between('accountWithdrawal.accountWithdrawalDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
             
        return $result;
    }
}
