<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * Class AccountTypeTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 * 
 * This file contains AccountType Table Functions
 */
class AccountTypeTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * AccountTypeTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Return all the account types in the table.
     * @param boolean $paginated
     * @param array $order
     * @return mixed
     */
    public function fetchAll($paginated = FALSE,$order=  array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountType')
                ->columns(array('*'))
                ->join('entity', 'accountType.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0);
        if ($paginated){
            $paginatorAdapter = new DbSelect($select,  $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    /**
     * Return account types which name match to the given query string.
     * @param string $searchKey
     * @param array $order
     * @param boolean $paginated
     * @return mixed
     */
    public function searchAccountTypeByAccountTypeName( $searchKey, $order = array(), $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountType')
                ->columns(array('*'))
                ->join('entity', 'accountType.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0)
                ->like('accountType.accountTypeName', '%' . $searchKey . '%');
        
        if ($paginated){
            $paginatorAdapter = new DbSelect($select,$this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    /**
     * Return account type according to the given account type id.
     * @param string $accountTypeId
     * @return mixed
     */
    public function getAccountTypeByAccountTypeId($accountTypeId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountType')
                ->columns(array('*'))
                ->join('entity', 'accountType.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('accountType.accountTypeId', $accountTypeId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Return account type according to the given account type name.
     * @param string $accountTypeName
     * @return mixed
     */
    public function getAccountTypeByAccountTypeName($accountTypeName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountType')
                ->columns(array('*'))
                ->join('entity', 'accountType.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('accountType.accountTypeName', $accountTypeName);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Return account type according to the given account type name and not equal 
     * to the given account type id
     * @param string $accountTypeId
     * @param string $accountTypeName
     * @return mixed
     */
    public function checkAccountTypeNameValid($accountTypeId,$accountTypeName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountType')
                ->columns(array('*'))
                ->join('entity', 'accountType.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('accountType.accountTypeName', $accountTypeName)
                ->where->notEqualTo('accountType.accountTypeId', $accountTypeId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Create new account type
     * @param \Expenses\Model\AccountType $accountType
     * @param string $entityId
     * @return boolean
     */
    public function saveAccountType(AccountType $accountType,$entityId)
    {
        $data = array(
            'accountTypeName' => $accountType->accountTypeName,
            'entityId' => $entityId
        );
        
        if($this->tableGateway->insert($data)){
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Update existing account type
     * @param array $data
     * @param string $accountTypeId
     * @return boolean
     */
    public function updateAccountType($data,$accountTypeId)
    {        
        if($accountTypeId){
            if($this->tableGateway->update($data, array('accountTypeId'=>$accountTypeId))){
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    /**
     * Returns account type list as an array
     * @return Array
     */
    public function getAccountTypeListForDropDown() {
        $accountTypeList = array();
        
        foreach ($this->fetchAll() as $accountType){
            $accountTypeList[$accountType[accountTypeId]] = $accountType['accountTypeName'];
        }
        return $accountTypeList;
    }
}
