<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * Class ReconciliationTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 * 
 * This file contains Reconciliation Table Functions
 */
class ReconciliationTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * ReconciliationTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Create new reconciliation
     * @param \Expenses\Model\Reconciliation $reconciliation
     * @param string $entityId
     * @return mixed
     */
    public function save(Reconciliation $reconciliation)
    {        
        $data = array(
            'reconciliationAmount' => $reconciliation->reconciliationAmount,
            'reconciliationComment' => $reconciliation->reconciliationComment,
            'reconciliationDate' => date('Y-m-d'),
            'reconciliationStatementBalance' => $reconciliation->reconciliationStatementBalance,
            'accountId' => $reconciliation->accountId,
            'reconciliationStartDate' => isset($reconciliation->reconciliationStartDate) ? $reconciliation->reconciliationStartDate : null,
            'reconciliationEndDate' => isset($reconciliation->reconciliationEndDate) ? $reconciliation->reconciliationEndDate : null,
            'reconciliationStatus' => isset($reconciliation->reconciliationStatus) ? $reconciliation->reconciliationStatus : 1,
            'entityId' => $reconciliation->entityId
        );
        
        if($this->tableGateway->insert($data)){
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Return all the reconciliations for given account id.
     * @param int $accountId
     * @param int $status
     * @param string $startDate
     * @param string $endDate
     * @param boolean $paginated
     * @param array $order
     * @return mixed
     */
    public function getReconciliationsByAccountId( $accountId = null, $status = null, $startDate = null, $endDate = null, $paginated = FALSE, $order=array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('reconciliation')
                ->columns(array('*'))
                ->join('entity', 'reconciliation.entityId = entity.entityID', array('deleted'))
                ->order($order);
        if($accountId){
            $select->where->equalTo('reconciliation.accountId',$accountId);
        }
        if(!is_null($status)){
            $select->where->equalTo('entity.deleted', $status);
        }
        if($startDate && $endDate){
            $select->where->between('reconciliation.reconciliationDate', $startDate, $endDate);            
        }
        if ($paginated){
            $paginatorAdapter = new DbSelect($select,$this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    /**
     * Get reconciliation by reconciliation id
     * @param int $reconciliationId
     * @return mixed
     */
    public function getReconciliationByReconciliationId($reconciliationId) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('reconciliation')
                ->columns(array('*'))
                ->join('entity', 'reconciliation.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('reconciliation.reconciliationId',$reconciliationId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Update existing reconciliation
     * @param array $data
     * @param string $reconciliationId
     * @return boolean
     */
    public function updateReconciliation($data,$reconciliationId)
    {        
        if($reconciliationId){
            if($this->tableGateway->update($data, array('reconciliationId'=>$reconciliationId))){
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    /**
     * Get last reconciliation by account id
     * @param string $accountId
     * @return mixed
     */
    public function getLastReconciliationByAccountId( $accountId) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('reconciliation')
                ->columns(array('*'))
                ->join('entity', 'reconciliation.entityId = entity.entityID', array('deleted'))
                ->order('reconciliationId DESC')
                ->limit(1)
                ->where->equalTo('reconciliation.accountId', $accountId)
                ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

    public function getReconciliationWithAccountByReconciliationId($reconciliationId) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('reconciliation')
                ->columns(array('*'))
                ->join('entity', 'reconciliation.entityId = entity.entityID', array('deleted'))
                ->join('account', 'reconciliation.accountId = account.accountId', array('accountName', 'accountNumber', 'bankId'))
                ->join('bank', 'bank.bankId = account.bankId', array('bankCode', 'bankName'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('reconciliation.reconciliationId',$reconciliationId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

    public function checkReconcilation($accountId, $startDate, $endDate) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('reconciliation')
                ->columns(array('*'))
                ->join('entity', 'reconciliation.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('reconciliation.accountId',$accountId)
                ->where->greaterThanOrEqualTo('reconciliationEndDate', $startDate);;
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

    public function deleteReconciliationByID($reconciliationId)
    {
        try {
            $result = $this->tableGateway->delete(array('reconciliationId' => $reconciliationId));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }
}
