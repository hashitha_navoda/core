<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class AccountWithdrawal
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */

class AccountWithdrawal implements InputFilterAwareInterface{
    
    /**
     * @var int Auto incremented account withdrawal Id. (Primary key)
     */
    public $accountWithdrawalId;
    
    /**
     * @var int account Id.
     */
    public $accountWithdrawalAccountId;
    
    /**
     * @var float withdrawal amount.
     */
    public $accountWithdrawalAmount;
    
    /**
     * @var date withdrawal date.
     */
    public $accountWithdrawalDate;
    
    /**
     * @var string withdrawal comment.
     */
    public $accountWithdrawalComment;

    /**
     * @var int accountWithdrawalIssuedFinanceAccountId.
     */
    public $accountWithdrawalIssuedFinanceAccountId;

    /**
     * @var int accountWithdrawalRecivedFinanceAccountId
     */
    public $accountWithdrawalRecivedFinanceAccountId;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;


    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->accountWithdrawalId = (isset($data['accountWithdrawalId'])) ? $data['accountWithdrawalId'] : null;
        $this->accountWithdrawalAccountId = (isset($data['transactionIssuedAccountId'])) ? $data['transactionIssuedAccountId'] : null;
        $this->accountWithdrawalAmount = (isset($data['transactionAmount'])) ? $data['transactionAmount'] : 0.00;
        $this->accountWithdrawalDate = (isset($data['transactionDate'])) ? $data['transactionDate'] : null;
        $this->accountWithdrawalComment = (isset($data['transactionComment'])) ? $data['transactionComment'] : null;
        $this->accountWithdrawalIssuedFinanceAccountId = (isset($data['transactionIssuedFinanceAccountId'])) ? $data['transactionIssuedFinanceAccountId'] : null;
        $this->accountWithdrawalRecivedFinanceAccountId = (isset($data['transactionRecivedFinanceAccountId'])) ? $data['transactionRecivedFinanceAccountId'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transaction',
                        'required' => true,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionIssuedFinanceAccountId',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionRecivedFinanceAccountId',
                        'required' => true,                        
                    )
                )
            );

             $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionBankAccountId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionAmount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Float',
                            ),
                        ),
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionDate',
                        'required' => true,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
}
