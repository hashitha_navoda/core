<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

/**
 * Class ExpenseCategoryTable
 * @package Expenses\Model
 * @author Damith Karunathilaka <damith@thinkcube.com>
 *
 * This file contains expense category Table Functions
 */
class ExpenseCategoryTable
{

    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('expenseCategory')
                    ->columns(array('*'))
                    ->join(array('parentExpCat' => 'expenseCategory'), 'expenseCategory.expenseCategoryParentId = parentExpCat.expenseCategoryId', array('expenseCategoryParentName' => new Expression('parentExpCat.expenseCategoryName')), 'left')
                    ->join('entity', 'expenseCategory.entityId = entity.entityID', array('deleted'))
                    ->where(array('entity.deleted' => '0'));
            $paginatorAdapter = new DbSelect($select, $adapter);
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        } else {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('expenseCategory')
                    ->columns(array('*'))
                    ->join(array('parentExpCat' => 'expenseCategory'), 'expenseCategory.expenseCategoryParentId = parentExpCat.expenseCategoryId', array('expenseCategoryParentName' => new Expression('parentExpCat.expenseCategoryName')), 'left')
                    ->join('entity', 'expenseCategory.entityId = entity.entityID', array('deleted'))
                    ->where(array('entity.deleted' => '0'));
            $select->where->notEqualto('expenseCategory.expenseCategoryStatus',2);
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            $resultsArray = [];
            foreach ($results as $result) {
                $resultsArray[] = $result;
            }
            return $resultsArray;
        }
    }

    public function getExpenseCategoryByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('expenseCategory')
                ->columns(array('*'))
                ->where(array('expenseCategory.expenseCategoryId' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    /**
     * Create new account
     * @param \Expenses\Model\Account $account
     * @param string $entityId
     * @return mixed
     */
    public function create(ExpenseCategory $expenseCategory)
    {
        $data = array(
            'expenseCategoryId' => $expenseCategory->expenseCategoryId,
            'expenseCategoryName' => $expenseCategory->expenseCategoryName,
            'expenseCategoryParentId' => $expenseCategory->expenseCategoryParentId,
            'expenseCategoryStatus' => $expenseCategory->expenseCategoryStatus,
            'entityId' => $expenseCategory->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function update($data, $expenseCategoryId)
    {
        if ($this->tableGateway->update($data, array('expenseCategoryId' => $expenseCategoryId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function changeStatus($status, $expenseCategoryId)
    {
        if ($this->tableGateway->update(array('expenseCategoryStatus' => $status), array('expenseCategoryId' => $expenseCategoryId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

}
