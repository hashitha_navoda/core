<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

/**
 * Class ExpenseCategoryTable
 * @package Expenses\Model
 * @author Damith Karunathilaka <damith@thinkcube.com>
 *
 * This file contains expense type limit approver Table Functions
 */
class ExpenseTypeLimitApproverTable
{

    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function create(ExpenseTypeLimitApprover $ExpenseTypeLimitApprover)
    {
        $data = array(
            'expenseTypeLimitApproverId' => $ExpenseTypeLimitApprover->expenseTypeLimitApproverId,
            'expenseTypeLimitId' => $ExpenseTypeLimitApprover->expenseTypeLimitId,
            'expenseTypeLimitApprover' => $ExpenseTypeLimitApprover->expenseTypeLimitApprover,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function deleteByExpTypeLimitId($expTypeLimit)
    {
        if ($this->tableGateway->delete(array('expenseTypeLimitId' => $expTypeLimit))) {
            return true;
        } else {
            return false;
        }
    }

}
