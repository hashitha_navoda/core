<?php

namespace Expenses\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PaymentVoucherProduct implements InputFilterAwareInterface
{

    public $paymentVoucherID;
    public $paymentVoucherProductID;
    public $locationProductID;
    public $paymentVoucherProductQuantity;
    public $paymentVoucherProductQuantityByBatch;
    public $paymentVoucherProductUnitPrice;
    public $paymentVoucherProductTotalPrice;
    public $productSerial;
    public $productBatch;
    public $batchSerialData;
    public $roockID;
    public $paymentVoucherProductExpenseTypeId;
    public $uomID;
    
    /**
     * expense type id payment voucher product
     * @var int 
     */
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->paymentVoucherProductID = (!empty($data['paymentVoucherProductID'])) ? $data['paymentVoucherProductID'] : null;
        $this->paymentVoucherID = (!empty($data['paymentVoucherID'])) ? $data['paymentVoucherID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->paymentVoucherProductQuantity = (!empty($data['paymentVoucherProductQuantity'])) ? $data['paymentVoucherProductQuantity'] : null;
        $this->paymentVoucherProductQuantityByBatch = (!empty($data['paymentVoucherProductQuantityByBatch'])) ? $data['paymentVoucherProductQuantityByBatch'] : null;
        $this->paymentVoucherProductUnitPrice = (!empty($data['paymentVoucherProductUnitPrice'])) ? $data['paymentVoucherProductUnitPrice'] : 0.00;
        $this->paymentVoucherProductTotalPrice = (!empty($data['paymentVoucherProductTotalPrice'])) ? $data['paymentVoucherProductTotalPrice'] : 0.00;
        $this->paymentVoucherProductDiscount = (!empty($data['paymentVoucherProductDiscount'])) ? $data['paymentVoucherProductDiscount'] : 0.00;
        $this->productSerial = (!empty($data['productSerial'])) ? $data['productSerial'] : null;
        $this->productBatch = (!empty($data['productBatch'])) ? $data['productBatch'] : null;
        $this->batchSerialData = (!empty($data['batchSerialData'])) ? $data['batchSerialData'] : null;
        $this->roockID = (!empty($data['roockID'])) ? $data['roockID'] : null;
        $this->paymentVoucherProductExpenseTypeId = (!empty($data['paymentVoucherProductExpenseTypeId'])) ? $data['paymentVoucherProductExpenseTypeId'] : null;
        $this->uomID = (!empty($data['uomID'])) ? $data['uomID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
