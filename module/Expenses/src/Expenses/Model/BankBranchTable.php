<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * Class BankBranchTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 * 
 * This file contains Bank Branch Table Functions
 */
class BankBranchTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * BankTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Return all the bank branches in the table.
     * @param boolean $paginated
     * @param array $order
     * @return mixed
     */
    public function fetchAll($paginated = FALSE,$order=array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bankBranch')
                ->columns(array('*'))
                ->join('entity', 'bankBranch.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0);
        if ($paginated){
            $paginatorAdapter = new DbSelect($select,$this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    /**
     * Create new branch
     * @param \Expenses\Model\BankBranch $branch
     * @param type $entityId
     * @return boolean
     */
    public function saveBranch(BankBranch $branch, $entityId)
    {
        $data = array(
            'bankId' => $branch->bankId,
            'bankBranchCode' => $branch->bankBranchCode,
            'bankBranchName' => $branch->bankBranchName,
            'bankBranchAddress' => $branch->bankBranchAddress,
            'entityId' => $entityId
        );
        
        if($this->tableGateway->insert($data)){
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Return bank branch according to the given bank branch id.
     * @param string $branchId
     * @return mixed
     */
    public function getBankBranchByBankBranchId($branchId){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bankBranch')
                ->columns(array('*'))
                ->join('entity', 'bankBranch.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('bankBranch.bankBranchId', $branchId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Return bank branch according to the given bank id and branch code.
     * @param string $bankId
     * @param string $branchCode
     * @return mixed
     */
    public function getBankBranchByBankIdAndBranchCode($bankId,$branchCode){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bankBranch')
                ->columns(array('*'))
                ->join('entity', 'bankBranch.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('bankBranch.bankId', $bankId)
                ->where->equalTo('bankBranch.bankBranchCode', $branchCode);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Return bank branch according to the given bank branch code and not equal 
     * to the given branch id
     * @param string $bankId
     * @param string $branchId
     * @param string $branchCode
     * @return mixed
     */
    public function checkBranchCodeValid($bankId,$branchId,$branchCode){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bankBranch')
                ->columns(array('*'))
                ->join('entity', 'bankBranch.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('bankBranch.bankId', $bankId)
                ->where->equalTo('bankBranch.bankBranchCode', $branchCode)
                ->where->notEqualTo('bankBranch.bankBranchId',$branchId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Update bank branch
     * @param array $data
     * @param string $branchId
     * @return boolean
     */
    public function updateBankBranch($data,$branchId)
    {
        $bankData = array(
            'bankBranchName' => $data['bankBranchName'],
            'bankBranchCode' => $data['bankBranchCode'],
            'bankBranchAddress' => $data['bankBranchAddress']
        );        
        if($branchId){
            if($this->tableGateway->update($bankData, array('bankBranchId'=>$branchId))){
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }
    
    /**
     * Return all the bank branches according to the given bank id.
     * @param boolean $paginated
     * @param array $order
     * @return mixed
     */
    public function getBankBranchesByBankId( $bankId, $paginated = FALSE,$order=array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bankBranch')
                ->columns(array('*'))
                ->join('entity', 'bankBranch.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('bankBranch.bankId', $bankId);
        if ($paginated){
            $paginatorAdapter = new DbSelect($select,$this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    /**
     * Return bank branches which having names match to the given query string.
     * @param string $searchKey
     * @param string $bankId
     * @param array $order
     * @param boolean $paginated
     * @return mixed
     */
    public function searchBankBranchesByBranchName( $searchKey, $bankId, $order = array(), $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bankBranch')
                ->columns(array('*'))
                ->join('entity', 'bankBranch.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('bankBranch.bankId', $bankId)
                ->like('bankBranch.bankBranchName', '%' . $searchKey . '%');
        
        if ($paginated){
            $paginatorAdapter = new DbSelect($select,$this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
}
