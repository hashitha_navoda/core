<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Description of PettyCashFloat
 *
 * @author sharmilan <sharmilan@thinkcube.com>
 */
class PettyCashFloat implements InputFilterAwareInterface
{

    public $pettyCashFloatID;
    public $pettyCashFloatAmount;
    public $pettyCashFloatDate;
    public $accountID;
    public $cashType;
    public $pettyCashFloatBalance;
    public $pettyCashFloatStatus;
    public $locationID;
    public $pettyCashFloatFinanceAccountID;
    public $pettyCashFloatIssueFinanceAccountID;
    public $entityID;
    protected $inputfilter;

    public function exchangeArray($data)
    {
        $this->pettyCashFloatID = (isset($data['pettyCashFloatID'])) ? $data['pettyCashFloatID'] : null;
        $this->pettyCashFloatAmount = (isset($data['pettyCashFloatAmount'])) ? $data['pettyCashFloatAmount'] : null;
        $this->pettyCashFloatDate = (isset($data['pettyCashFloatDate'])) ? $data['pettyCashFloatDate'] : null;
        $this->accountID = (isset($data['accountID'])) ? $data['accountID'] : null;
        $this->cashType = (isset($data['cashType'])) ? $data['cashType'] : null;
        $this->pettyCashFloatBalance = (isset($data['pettyCashFloatBalance'])) ? $data['pettyCashFloatBalance'] : null;
        $this->pettyCashFloatStatus = (isset($data['pettyCashFloatStatus'])) ? $data['pettyCashFloatStatus'] : 1;
        $this->locationID = (isset($data['locationID'])) ? $data['locationID'] : null;
        $this->pettyCashFloatFinanceAccountID = (isset($data['pettyCashFloatFinanceAccountID'])) ? $data['pettyCashFloatFinanceAccountID'] : null;
        $this->pettyCashFloatIssueFinanceAccountID = (isset($data['pettyCashFloatIssueFinanceAccountID'])) ? $data['pettyCashFloatIssueFinanceAccountID'] : null;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : null;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashFloatID',
                                'required' => FALSE,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Int'
                                    ),
                                ),
                    ))
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashFloatAmount',
                                'required' => TRUE,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim')
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Int'
                                    )
                                )
                    ))
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'accountID',
                                'required' => TRUE,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim')
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Int'
                                    )
                                )
                    ))
            );
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

}
