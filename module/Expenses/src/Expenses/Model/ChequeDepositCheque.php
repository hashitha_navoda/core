<?php
namespace Expenses\Model;

/**
 * Class ChequeDepositCheque
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class ChequeDepositCheque {
    
    /**
     * @var int Auto incremented cheque deposit cheque id. (Primary key)
     */
    public $chequeDepositChequeId;

    /**
     * @var int incoming payment method cheque id.
     */
    public $incomingPaymentMethodChequeId;
    
    /**
     * @var int cheque deposit id.
     */
    public $chequeDepositId;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->chequeDepositChequeId = (isset($data['chequeDepositChequeId'])) ? $data['chequeDepositChequeId'] : null;
        $this->incomingPaymentMethodChequeId = (isset($data['incomingPaymentMethodChequeId'])) ? $data['incomingPaymentMethodChequeId'] : null;
        $this->chequeDepositId = (isset($data['chequeDepositId'])) ? $data['chequeDepositId'] : null;
        $this->entityId  = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
}
