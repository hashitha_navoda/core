<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

/**
 * Class ExpenseTypeLimitTable
 * @package Expenses\Model
 * @author Damith Karunathilaka <damith@thinkcube.com>
 *
 * This file contains expense type limit  Table Functions
 */
class ExpenseTypeLimitTable
{

    /**
     * @var TableGateway
     */
    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function create(ExpenseTypeLimit $expenseTypeLimit)
    {
        $data = array(
            'expenseTypeLimitId' => $expenseTypeLimit->expenseTypeLimitId,
            'expenseTypeId' => $expenseTypeLimit->expenseTypeId,
            'expenseTypeLimitMin' => $expenseTypeLimit->expenseTypeLimitMin,
            'expenseTypeLimitMax' => $expenseTypeLimit->expenseTypeLimitMax,
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function getLimitsByTypeId($expTypeId)
    {
        $results = $this->tableGateway->select(array('expenseTypeId' => $expTypeId));
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = (array) $result;
        }
        return $resultsArray;
    }

    public function deleteByExpTypeId($expTypeId)
    {
        if ($this->tableGateway->delete(array('expenseTypeId' => $expTypeId))) {
            return true;
        } else {
            return false;
        }
    }

}
