<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PaymentVoucherProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePaymentVoucherProductTax(PaymentVoucherProductTax $pavVaucherTax)
    {
        $data = array(
            'paymentVoucherID' => $pavVaucherTax->paymentVoucherID,
            'paymentVoucherProductIdOrNonItemProductID' => $pavVaucherTax->paymentVoucherProductIdOrNonItemProductID,
            'paymentVoucherTaxID' => $pavVaucherTax->paymentVoucherTaxID,
            'paymentVoucherTaxPrecentage' => $pavVaucherTax->paymentVoucherTaxPrecentage,
            'paymentVoucherTaxAmount' => $pavVaucherTax->paymentVoucherTaxAmount,
            'paymentVoucherTaxItemOrNonItemProduct' => $pavVaucherTax->paymentVoucherTaxItemOrNonItemProduct,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('paymentVoucherProductTaxID');
        return $insertedID;
    }

    public function getPaymentVoucherProductTaxByPVPID($paymentVoucherProductID, $paymentVoucherProductType)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("paymentVoucherProductTax")
                ->columns(array("*"))
                ->where(array('paymentVoucherProductTax.paymentVoucherProductIdOrNonItemProductID' => $paymentVoucherProductID, 'paymentVoucherProductTax.paymentVoucherTaxItemOrNonItemProduct' => $paymentVoucherProductType));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

}
