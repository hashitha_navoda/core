<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class AccountDeposit
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class AccountDeposit  implements InputFilterAwareInterface{
    
    /**
     * @var int Auto incremented account deposit Id. (Primary key)
     */
    public $accountDepositId;
    
    /**
     * @var int account Id.
     */
    public $accountDepositAccountId;
    
    /**
     * @var float deposit amount.
     */
    public $accountDepositAmount;
    
    /**
     * @var date deposit date.
     */
    public $accountDepositDate;
    
    /**
     * @var string deposit comment.
     */
    public $accountDepositComment;

    /**
     * @var int accountDepositFinanceAccountID.
     */
    public $accountDepositFinanceAccountID;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->accountDepositId = (isset($data['accountDepositId'])) ? $data['accountDepositId'] : null;
        $this->accountDepositAccountId = (isset($data['transactionBankAccountId'])) ? $data['transactionBankAccountId'] : null;
        $this->accountDepositAmount = (isset($data['transactionAmount'])) ? $data['transactionAmount'] : 0.00;
        $this->accountDepositDate = (isset($data['transactionDate'])) ? $data['transactionDate'] : null;
        $this->accountDepositComment = (isset($data['transactionComment'])) ? $data['transactionComment'] : null;
        $this->accountDepositFinanceAccountID = (isset($data['transactionIssuedFinanceAccountId'])) ? $data['transactionIssuedFinanceAccountId'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transaction',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionIssuedFinanceAccountId',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionBankAccountId',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionRecivedFinanceAccountId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionAmount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Float',
                            ),
                        ),
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionDate',
                        'required' => true,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
