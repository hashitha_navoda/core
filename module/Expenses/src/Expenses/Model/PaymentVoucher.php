<?php

namespace Expenses\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PaymentVoucher implements InputFilterAwareInterface
{

    public $paymentVoucherID;
    public $paymentVoucherCode;
    public $paymentVoucherSupplierID;
    public $paymentVoucherSupplierReference;
    public $paymentVoucherAccountID;
    public $paymentVoucherLocationID;
    public $paymentVoucherDueDate;
    public $paymentVoucherIssuedDate;
    public $paymentVoucherComment;
    public $paymentVoucherDeliveryCharge;
    public $paymentVoucherShowTax;
    public $paymentVoucherStatus;
    public $paymentVoucherTotal;
    public $paymentVoucherPaidAmount;
    public $paymentTermID;
    public $entityID;
    public $paymentVoucherEmployeeID;
    public $paymentVoucherItemNotItemFlag;
    public $paymentVoucherExpenseType;
    public $paymentVoucherApproved;
    public $paymentVoucherHashValue;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->paymentVoucherID = (!empty($data['paymentVoucherID'])) ? $data['paymentVoucherID'] : null;
        $this->paymentVoucherCode = (!empty($data['paymentVoucherCode'])) ? $data['paymentVoucherCode'] : null;
        $this->paymentVoucherSupplierID = (!empty($data['paymentVoucherSupplierID'])) ? $data['paymentVoucherSupplierID'] : null;
        $this->paymentVoucherSupplierReference = (!empty($data['paymentVoucherSupplierReference'])) ? $data['paymentVoucherSupplierReference'] : null;
        $this->paymentVoucherAccountID = (!empty($data['paymentVoucherAccountID'])) ? $data['paymentVoucherAccountID'] : null;
        $this->paymentVoucherLocationID = (!empty($data['paymentVoucherLocationID'])) ? $data['paymentVoucherLocationID'] : null;
        $this->paymentVoucherDueDate = (!empty($data['paymentVoucherDueDate'])) ? $data['paymentVoucherDueDate'] : null;
        $this->paymentVoucherIssuedDate = (!empty($data['paymentVoucherIssuedDate'])) ? $data['paymentVoucherIssuedDate'] : null;
        $this->paymentVoucherComment = (!empty($data['paymentVoucherComment'])) ? $data['paymentVoucherComment'] : null;
        $this->paymentVoucherDeliveryCharge = (!empty($data['paymentVoucherDeliveryCharge'])) ? $data['paymentVoucherDeliveryCharge'] : 0.00;
        $this->paymentVoucherShowTax = (!empty($data['paymentVoucherShowTax'])) ? $data['paymentVoucherShowTax'] : 0;
        $this->paymentVoucherStatus = (!empty($data['paymentVoucherStatus'])) ? $data['paymentVoucherStatus'] : 3;
        $this->paymentVoucherTotal = (!empty($data['paymentVoucherTotal'])) ? $data['paymentVoucherTotal'] : 0.00;
        $this->paymentVoucherPaidAmount = (!empty($data['paymentVoucherPaidAmount'])) ? $data['paymentVoucherPaidAmount'] : 0.00;
        $this->paymentVoucherEmployeeID = (!empty($data['paymentVoucherEmployeeID'])) ? $data['paymentVoucherEmployeeID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->paymentVoucherItemNotItemFlag = (!empty($data['paymentVoucherItemNotItemFlag'])) ? $data['paymentVoucherItemNotItemFlag'] : 0;
        $this->paymentVoucherExpenseType = (!empty($data['paymentVoucherExpenseType'])) ? $data['paymentVoucherExpenseType'] : null;
        $this->paymentVoucherApproved = (!empty($data['paymentVoucherApproved'])) ? $data['paymentVoucherApproved'] : null;
        $this->paymentVoucherHashValue = (!empty($data['paymentVoucherHashValue'])) ? $data['paymentVoucherHashValue'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
