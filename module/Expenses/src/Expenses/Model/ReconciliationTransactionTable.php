<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class ReconciliationTransaction
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 * 
 * This file contains Account Table Functions
 */
class ReconciliationTransactionTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * ReconciliationTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Create new reconciliation
     * @param \Expenses\Model\Reconciliation $reconciliation
     * @param string $entityId
     * @return mixed
     */
    public function save(ReconciliationTransaction $reconciliationTransaction)
    {
        $data = array(
            'reconciliationTransactionCreditAmount' => $reconciliationTransaction->reconciliationTransactionCreditAmount,
            'reconciliationTransactionDebitAmount' => $reconciliationTransaction->reconciliationTransactionDebitAmount,
            'reconciliationTransactionType' => $reconciliationTransaction->reconciliationTransactionType,
            'incomingPaymentMethodChequeId' => $reconciliationTransaction->incomingPaymentMethodChequeId,
            'outGoingPaymentMethodsNumbersId' => $reconciliationTransaction->outGoingPaymentMethodsNumbersId,
            'incomingPaymentMethodBankTransferId' => $reconciliationTransaction->incomingPaymentMethodBankTransferId,
            'reconciliationId' => $reconciliationTransaction->reconciliationId,
            'entityId' => $reconciliationTransaction->entityId
        );
        
        if($this->tableGateway->insert($data)){
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Get Reconciliation transactions
     * @param int $reconciliationId
     * @param int $filter for filter exist or deleted transactions [ 0 - exist, 1 - deleted ]
     * @return mixed
     */
    public function getTransactionsByReconciliationId( $reconciliationId, $filter = null) {
        
        $sql = 'SELECT * FROM reconciliationTransaction rt 
                LEFT JOIN incomingPaymentMethod ipm ON rt.incomingPaymentMethodChequeId = ipm.incomingPaymentMethodChequeId OR rt.incomingPaymentMethodBankTransferId = ipm.incomingPaymentMethodBankTransferId OR rt.incomingPaymentMethodBankTransferId = ipm.incomingPaymentMethodCashId
                LEFT JOIN incomingPayment ip ON ipm.incomingPaymentId = ip.incomingPaymentID 
                LEFT JOIN incomingPaymentMethodCheque ipmc ON ipm.incomingPaymentMethodChequeId = ipmc.incomingPaymentMethodChequeId
                LEFT JOIN incomingPaymentMethodCash ipmca ON ipm.incomingPaymentMethodCashId = ipmca.incomingPaymentMethodCashId
                LEFT JOIN outGoingPaymentMethodsNumbers ogpmn ON rt.outGoingPaymentMethodsNumbersId = ogpmn.outGoingPaymentMethodsNumbersID
                LEFT JOIN outgoingPayment op ON ogpmn.outGoingPaymentID = op.outgoingPaymentID
                LEFT JOIN incomingPaymentMethodBankTransfer ipmbt ON ipm.incomingPaymentMethodBankTransferId = ipmbt.incomingPaymentMethodBankTransferId
                LEFT JOIN journalEntryAccounts jac ON rt.incomingPaymentMethodBankTransferId = jac.journalEntryAccountsID OR rt.outGoingPaymentMethodsNumbersId = jac.journalEntryAccountsID
                LEFT JOIN journalEntry je ON jac.journalEntryID = je.journalEntryID
                -- LEFT JOIN accountDeposit ad ON je.journalEntryDocumentID = ad.accountDepositId 
                -- LEFT JOIN accountWithdrawal aw ON je.journalEntryDocumentID = aw.accountWithdrawalId 
                LEFT JOIN entity e ON rt.entityId = e.entityID
                WHERE rt.reconciliationId = "'.$reconciliationId.'"';
        
        if(strlen($filter)>0){
            $sql = $sql.'AND e.deleted = "'.$filter.'"';
        }
        
        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);
        return $resultSet;
    }
    
    /**
     * Get reconciliation transaction by reconciliation transaction id
     * @param int $reconciliationId
     * @return mixed
     */
    public function getReconciliationTransactionByTransactionId($transactionId) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('reconciliationTransaction')
                ->columns(array('*'))
                ->join('entity', 'reconciliationTransaction.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('reconciliationTransaction.reconciliationTransactionId',$transactionId)
                ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }


    /**
     * Get reconciliation transaction by reconciliation transaction id
     * @param int $reconciliationId
     * @return mixed
     */
    public function getNormalReconciliationTransactionByReconciliationId($reconciliationId) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('reconciliationTransaction')
                ->columns(array('*'))
                ->join('entity', 'reconciliationTransaction.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('reconciliationTransaction.reconciliationId',$reconciliationId)
                ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result;
    }
    
    /**
     * Check whether given cheque list reconciled
     * @param array $chequeIdList
     * @return boolean
     */
    public function isIncomingChequeReconciled($chequeIdList) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('reconciliationTransaction')
                ->columns(array('*'))
                ->join('entity', 'reconciliationTransaction.entityId = entity.entityID', array('deleted'))
                ->where->in('reconciliationTransaction.incomingPaymentMethodChequeId',$chequeIdList)
                ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return false;
        }
        return true;
    }

    public function deleteReconciliationDetailByID($reconciliationId)
    {
        try {
            $result = $this->tableGateway->delete(array('reconciliationId' => $reconciliationId));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }
}
