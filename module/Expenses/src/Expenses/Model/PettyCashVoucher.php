<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Description of PettyCashVoucher
 *
 * @author shermilan
 */
class PettyCashVoucher implements InputFilterAwareInterface
{

    /**
     * Primary Key
     * @var type
     */
    public $pettyCashVoucherID;

    /**
     * Reference No
     * @var type
     */
    public $pettyCashVoucherNo;

    /**
     * Cash Amount of tranction
     * @var type
     */
    public $pettyCashVoucherAmount;

    /**
     * Entry Date
     * @var type
     */
    public $pettyCashVoucherDate;

    /**
     *  Incoming or outgoing amount
     * @var type
     */
    public $pettyCashVoucherType;

    /**
     * Canbe Assign a user for current tranction
     * @var type
     */
    public $userID;

    /**
     *   expense type table ID
     * @var type
     */
    public $pettyCashExpenseTypeID;

    /**
     * Early inserted petty cash voucher ID with expenses type
     */
    public $outgoingPettyCashVoucherID;

     /**
     * Early inserted petty cash voucher finance account ID 
     */
    public $pettyCashVoucherFinanceAccountID;

    /**
     * The voucher is enabled or not
     * @var type
     */
    public $entityID;
    public $locationID;

    /**
     * petty cash voucher comment
     * @var string
     */
    public $pettyCashVoucherComment;


    protected $inputfilter;

    public function exchangeArray($data)
    {
        $this->pettyCashVoucherID = (!empty($data['pettyCashVoucherID'])) ? $data['pettyCashVoucherID'] : null;
        $this->pettyCashVoucherNo = (!empty($data['pettyCashVoucherNo'])) ? $data['pettyCashVoucherNo'] : null;
        $this->pettyCashVoucherAmount = (!empty($data['pettyCashVoucherAmount'])) ? $data['pettyCashVoucherAmount'] : null;
        $this->pettyCashVoucherDate = (!empty($data['pettyCashVoucherDate'])) ? $data['pettyCashVoucherDate'] : null;
        $this->pettyCashVoucherType = (!empty($data['pettyCashVoucherType'])) ? $data['pettyCashVoucherType'] : null;
        $this->pettyCashExpenseTypeID = (!empty($data['pettyCashExpenseTypeID'])) ? $data['pettyCashExpenseTypeID'] : null;
        $this->outgoingPettyCashVoucherID = (!empty($data['outgoingPettyCashVoucherID'])) ? $data['outgoingPettyCashVoucherID'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->userID = (!empty($data['userID'])) ? $data['userID'] : null;
        $this->pettyCashVoucherFinanceAccountID = (!empty($data['pettyCashVoucherFinanceAccountID'])) ? $data['pettyCashVoucherFinanceAccountID'] : null;
        $this->pettyCashVoucherComment = (!empty($data['pettyCashVoucherComment'])) ? $data['pettyCashVoucherComment'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->pettyCashVoucherExpenseTypeFormat = (!empty($data['pettyCashVoucherExpenseTypeFormat'])) ? $data['pettyCashVoucherExpenseTypeFormat'] : null;
    }

    public function getInputFilter()
    {
        if (!$this->inputfilter) {
            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashVoucherID',
                                'required' => false,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Int'
                                    ),
                                ),
                    ))
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashVoucherNo',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'StringLength',
                                        'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min' => 1,
                                            'max' => 100,
                                        ),
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashVoucherAmount',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Int'
                                    ),
                                ),
                    ))
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashVoucherDate',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'StringLength',
                                        'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min' => 1,
                                            'max' => 100,
                                        ),
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'pettyCashVoucherType',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Int'
                                    ),
                                ),
                    ))
            );

            $this->inputfilter = $inputFilter;
        }
        return $this->inputfilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

}
