<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

/**
 * Description of PettyCashVoucherTable
 *
 * @author shermilan
 */
class PettyCashVoucherTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($locationID, $paginated = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashVoucher');
        $select->where(['pettyCashVoucher.locationID' => $locationID]);
        $select->order(array('pettyCashVoucherID' => 'DESC'));
        $select->join('entity', 'pettyCashVoucher.entityID = entity.entityID', array('deleted'), 'left');
        $select->join('pettyCashExpenseType', 'pettyCashVoucher.pettyCashExpenseTypeID = pettyCashExpenseType.pettyCashExpenseTypeID', ['pettyCashExpenseTypeName'], 'left');
        $select->join(['returnedPettyCashVoucher' => 'pettyCashVoucher'], 'pettyCashVoucher.outgoingPettyCashVoucherID = returnedPettyCashVoucher.pettyCashVoucherID', ['returnedPettyCashVoucherNo' => 'pettyCashVoucherNo'], 'left');

        if ($paginated) {
            $paginateAdapter = new DbSelect($select, $adapter);
            return new Paginator($paginateAdapter);
        }
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getPettyCashVoucherByLocationId($locationID, $searchKey = false,$voucherID = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashVoucher');
        $select->where(['pettyCashVoucher.locationID' => $locationID]);
        if($searchKey){
            $select->where->like('pettyCashVoucher.pettyCashVoucherNo', '%' . $searchKey . '%');
            $select->limit(50);
        }
        if($voucherID){
            $select->where(['pettyCashVoucher.pettyCashVoucherID' => $voucherID]);
        }
        $select->order(array('pettyCashVoucherID' => 'DESC'));
        $select->join('entity', 'pettyCashVoucher.entityID = entity.entityID', array('deleted'), 'left');
        $select->join('pettyCashExpenseType', 'pettyCashVoucher.pettyCashExpenseTypeID = pettyCashExpenseType.pettyCashExpenseTypeID', ['pettyCashExpenseTypeName'], 'left');
        $select->join(['returnedPettyCashVoucher' => 'pettyCashVoucher'], 'pettyCashVoucher.outgoingPettyCashVoucherID = returnedPettyCashVoucher.pettyCashVoucherID', ['returnedPettyCashVoucherNo' => 'pettyCashVoucherNo'], 'left');
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getPettyCashVoucherDetailsByDateRange($fromDate,$toDate,$locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashVoucher');
        $select->where(['pettyCashVoucher.locationID' => $locationID]);
        $select->where(array('pettyCashVoucher.pettyCashVoucherDate >= ?' => $fromDate))
            ->where(array('pettyCashVoucher.pettyCashVoucherDate <= ?' => $toDate));
        $select->order(array('pettyCashVoucherID' => 'DESC'));
        $select->join('entity', 'pettyCashVoucher.entityID = entity.entityID', array('deleted'), 'left');
        $select->join('pettyCashExpenseType', 'pettyCashVoucher.pettyCashExpenseTypeID = pettyCashExpenseType.pettyCashExpenseTypeID', ['pettyCashExpenseTypeName'], 'left');
        $select->join(['returnedPettyCashVoucher' => 'pettyCashVoucher'], 'pettyCashVoucher.outgoingPettyCashVoucherID = returnedPettyCashVoucher.pettyCashVoucherID', ['returnedPettyCashVoucherNo' => 'pettyCashVoucherNo'], 'left');
        $statment = $sql->prepareStatementForSqlObject($select);
        $rowset = $statment->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }



    public function save(PettyCashVoucher $pettyCashVoucher)
    {

        $data = [
            'pettyCashVoucherNo' => $pettyCashVoucher->pettyCashVoucherNo,
            'pettyCashVoucherAmount' => $pettyCashVoucher->pettyCashVoucherAmount,
            'pettyCashVoucherDate' => $pettyCashVoucher->pettyCashVoucherDate,
            'pettyCashVoucherType' => $pettyCashVoucher->pettyCashVoucherType,
            'pettyCashExpenseTypeID' => $pettyCashVoucher->pettyCashExpenseTypeID,
            'outgoingPettyCashVoucherID' => $pettyCashVoucher->outgoingPettyCashVoucherID,
            'userID' => $pettyCashVoucher->userID,
            'locationID' => $pettyCashVoucher->locationID,
            'pettyCashVoucherFinanceAccountID' => $pettyCashVoucher->pettyCashVoucherFinanceAccountID,
            'pettyCashVoucherComment' => $pettyCashVoucher->pettyCashVoucherComment,
            'pettyCashVoucherExpenseTypeFormat' => $pettyCashVoucher->pettyCashVoucherExpenseTypeFormat,
            'entityID' => $pettyCashVoucher->entityID
        ];

        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    /**
     * Get active petty cash voucher list which having expense type
     * @return type
     */
    public function getActiveOutGoingPettyCashVouchers($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashVoucher');
        $select->join('entity', 'pettyCashVoucher.entityID = entity.entityID', [ 'deleted'], 'left');
        $select->where([ 'deleted' => 0, 'pettyCashVoucherType' => '1', 'pettyCashVoucher.locationID' => $locationID]);

        $statment = $sql->
                prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getReturnedPettyCashVoucher($returnedPettyCashVoucherID)
    {
        $adaptor = $this->tableGateway->getAdapter();
        $sql = new Sql($adaptor);
        $select = $sql->select();
        $select->from('pettyCashVoucher');
        $select->join('entity', 'pettyCashVoucher.entityID = entity.entityID', [ 'deleted'], 'left');
        $select->where([ 'outgoingPettyCashVoucherID' => $returnedPettyCashVoucherID, 'deleted' => '0']);

        $statment = $sql->
                prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getActivePettyCashVoucherByID($pettyCashVoucherID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashVoucher');
        $select->join('entity', 'pettyCashVoucher.entityID = entity.entityID', [ 'deleted'], 'left');
        $select->where([ 'deleted' => '0', 'pettyCashVoucherID' => $pettyCashVoucherID]);
        $statment = $sql->prepareStatementForSqlObject($select);
        return (object) $statment->execute()->current();
    }

    public function getByPettyCashExpenseTypeID($pettyCashExpenseTypeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashVoucher');
        $select->where(['pettyCashExpenseTypeID' => $pettyCashExpenseTypeID]);
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getDataForReport($locationID, $pVoucherType = FALSE, $pExpenseType = false, $fromDate = false, $toDate = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashVoucher');
        $select->join('entity', 'pettyCashVoucher.entityID = entity.entityID', [ 'deleted'], 'left');
        $select->join('pettyCashExpenseType', 'pettyCashVoucher.pettyCashExpenseTypeID = pettyCashExpenseType.pettyCashExpenseTypeID', [ 'pettyCashExpenseTypeName'], 'left');
        $select->join('user', 'pettyCashVoucher.userID = user.userID', [ 'userUsername', 'userFirstName', 'userLastName'], 'left');
        $select->where([ 'deleted' => '0', 'pettyCashVoucher.locationID' => $locationID]);
        if ($pVoucherType) {
            $select->where->in('pettyCashVoucher.pettyCashVoucherType', $pVoucherType);
        }
        if ($pExpenseType) {
            $select->where->in('pettyCashVoucher.pettyCashExpenseTypeID', $pExpenseType);
        }
        if ($fromDate && $toDate) {
            $select->where->between('pettyCashVoucherDate', $fromDate, $toDate);
        }
        if ($toDate) {
            $select->where->lessThanOrEqualTo('pettyCashVoucherDate', $toDate);
        }
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getPettyCashVoucherDetails($pettyCashVoucherId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from(['pcv' => 'pettyCashVoucher']);
        $select->join('user', 'pcv.userID = user.userID', [ 'userUsername'], 'left');
        $select->join('entity', 'pcv.entityID = entity.entityID', [ 'deleted'], 'left');
        $select->join('pettyCashVoucher', new Expression('pcv.outgoingPettyCashVoucherID = pettyCashVoucher.pettyCashVoucherID AND pcv.pettyCashVoucherType = 2'), [
            'outGoingPettyCashVoucher' => 'pettyCashVoucherNo'
            ], 'left');
        $select->join('pettyCashExpenseType', new Expression('pcv.pettyCashExpenseTypeID = pettyCashExpenseType.pettyCashExpenseTypeID AND pcv.pettyCashVoucherType = 1'), [
            'pettyCashExpenseType' => 'pettyCashExpenseTypeName'
            ], 'left');
        $select->join('financeAccounts', new Expression("pcv.pettyCashExpenseTypeID = financeAccounts.financeAccountsID AND pcv.pettyCashVoucherType = 1 "), array('financeAccountsCode', 'financeAccountsName'), 'left');
        $select->where(['pcv.pettyCashVoucherID' => $pettyCashVoucherId]);
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute()->current();
    }

    public function getPettyCasshVoucherByPettyCasshVoucherIDForRec($pettyCashVoucherID, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('pettyCashVoucher')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "pettyCashVoucher.entityID = e.entityID", array('deleted', 'createdTimeStamp'), 'left')
                    ->where(array('pettyCashVoucher.pettyCashVoucherID' => $pettyCashVoucherID));
                $select->where(array('e.deleted' => '0'));
            
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

}
