<?php

namespace Expenses\Model;

/**
 * Class ChequeDepositCancellationCheque
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class ChequeDepositCancellationCheque {
    
    /**
     * @var int Auto incremented cheque deposit cancellation cheque id (Primary key)
     */
    public $chequeDepositCancelationChequeId;
    
    /**
     * @var int cheque deposit cancellation id
     */
    public $chequeDepositCancelationId;
    
    /**
     * @var int payment method number id
     */
    public $incomingPaymentMethodChequeId;
    
    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->chequeDepositCancelationChequeId = (isset($data['chequeDepositCancelationChequeId'])) ? $data['chequeDepositCancelationChequeId'] : null;
        $this->chequeDepositCancelationId = (isset($data['chequeDepositCancelationId'])) ? $data['chequeDepositCancelationId'] : null;
        $this->incomingPaymentMethodChequeId = (isset($data['incomingPaymentMethodChequeId'])) ? $data['incomingPaymentMethodChequeId'] : null;
    }
}
