<?php
namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class CashInHandWithdrawalTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 *
 * This file contains CashInHand Withdrawal Table Functions
 */
class CashInHandWithdrawalTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * CashInHandWithdrawalTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Create new cash in hand withdrawal
     * @param \Expenses\Model\CashInHandWithdrawal $cashInHandWithdrawal
     * @return mixed
     */
    public function saveWithdrawal(CashInHandWithdrawal $cashInHandWithdrawal)
    {
        $data = array(
            'cashInHandWithdrawalAmount' => $cashInHandWithdrawal->cashInHandWithdrawalAmount,
            'cashInHandWithdrawalDate' => $cashInHandWithdrawal->cashInHandWithdrawalDate,
            'cashInHandWithdrawalComment' => $cashInHandWithdrawal->cashInHandWithdrawalComment,
            'locationId' => $cashInHandWithdrawal->locationId,
            'entityId' => $cashInHandWithdrawal->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Get All available cash in hand withdrawals by location
     * @param type $locationId
     * @return type
     */
    public function getCashInHandWithdrawalsByLocationId( $locationId) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cashInHandWithdrawal')
                ->columns(array('*'))
                ->join( 'entity', 'cashInHandWithdrawal.entityId = entity.entityID', array('deleted'), "left")
                ->where(array('cashInHandWithdrawal.locationId' => $locationId, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }
    
    /**
     * Get withdrawals by location id
     * @param int $locationId
     * @param date $fromDate
     * @param date $toDate
     * @return mixed
     */
    public function getAllWithdrawalsByLocationId( $locationId, $fromDate = null, $toDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cashInHandWithdrawal')
                ->columns(array('*'))
                ->join('entity', 'cashInHandWithdrawal.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('cashInHandWithdrawal.locationId', $locationId)
                ->where->equalTo('entity.deleted', 0);
        if ($fromDate && $toDate) {
            $select->where->between('cashInHandWithdrawal.cashInHandWithdrawalDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * Get cash in hand withdrawal details by cashInHandWithdrawalId
     * @param int $withdrawalId
     * @return mixed
     */
    public function getWithdrawalByWithdrawalId( $withdrawalId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cashInHandWithdrawal')
                ->columns(array('*'))
                ->where->equalTo('cashInHandWithdrawal.cashInHandWithdrawalId', $withdrawalId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if($result->count() == 0){
            return NULL;
        } else {
            return $result->current();
        }
    }
}
