<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * Class ChequeDepositCancellationChequeTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 * 
 * This file contains ChequeDepositCancellationCheque Table Functions
 */
class ChequeDepositCancellationChequeTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * ChequeDepositCancellationChequeTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Save ChequeDepositCancellationCheque
     * @param \Expenses\Model\ChequeDepositCancellation $chequeDepositCancellationCheque
     * @return mixed
     */
    public function save(ChequeDepositCancellationCheque $chequeDepositCancellationCheque)
    {        
        $data = array(
            'chequeDepositCancelationChequeId' => $chequeDepositCancellationCheque->chequeDepositCancelationChequeId,
            'chequeDepositCancelationId' => $chequeDepositCancellationCheque->chequeDepositCancelationId,
            'incomingPaymentMethodChequeId' => $chequeDepositCancellationCheque->incomingPaymentMethodChequeId
        );
        
        if($this->tableGateway->insert($data)){
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
}
