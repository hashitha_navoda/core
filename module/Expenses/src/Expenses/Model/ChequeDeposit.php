<?php
namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class ChequeDeposit
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class ChequeDeposit implements InputFilterAwareInterface{
    
    /**
     * @var int Auto incremented cheque deposit id. (Primary key)
     */
    public $chequeDepositId;

    /**
     * @var string cheque deposit amount.
     */
    public $chequeDepositAmount;
    
    /**
     * @var string cheque deposit date.
     */
    public $chequeDepositDate;
    
    /**
     * @var string cheque deposit date.
     */
    public $chequeDepositDescription;
    
    /**
     * @var int currency id.
     */
    public $chequeDepositCurrencyId;
    
    /**
     * @var decimal deposit currency currency rate.
     */
    public $chequeDepositCurrencyRate;
    
    /**
     * @var decimal deposited account currency rate.
     */
    public $chequeDepositAccountCurrencyRate;
    
    /**
     * @var int bank id.
     */
    public $bankId;
    
    /**
     * @var int account Id.
     */
    public $accountId;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;
    
    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->chequeDepositId = (isset($data['chequeDepositId'])) ? $data['chequeDepositId'] : null;
        $this->chequeDepositAmount = (isset($data['chequeDepositAmount'])) ? $data['chequeDepositAmount'] : null;
        $this->chequeDepositDate = (isset($data['chequeDepositDate'])) ? $data['chequeDepositDate'] : null;
        $this->chequeDepositDescription = (isset($data['chequeDepositDescription'])) ? $data['chequeDepositDescription'] : null;
        $this->chequeDepositCurrencyId = (isset($data['chequeDepositCurrencyId'])) ? $data['chequeDepositCurrencyId'] : null;
        $this->chequeDepositCurrencyRate = (isset($data['chequeDepositCurrencyRate'])) ? $data['chequeDepositCurrencyRate'] : null;
        $this->chequeDepositAccountCurrencyRate = (isset($data['chequeDepositAccountCurrencyRate'])) ? $data['chequeDepositAccountCurrencyRate'] : null;
        $this->accountId = (isset($data['accountId'])) ? $data['accountId'] : null;
        $this->bankId    = (isset($data['bankId'])) ? $data['bankId'] : null;
        $this->entityId  = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'chequeDepositAmount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Float',
                            ),
                        ),
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'accountId',
                        'required' => true,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'bankId',
                        'required' => true,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
