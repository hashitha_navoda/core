<?php

namespace Expenses\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PaymentVoucherNonItemProduct implements InputFilterAwareInterface
{

    public $paymentVoucherID;
    public $paymentVoucherNonItemProductID;
    public $paymentVoucherNonItemProductDiscription;
    public $paymentVoucherNonItemProductQuantity;
    public $paymentVoucherNonItemProductUnitPrice;
    public $paymentVoucherNonItemProductTotalPrice;
    public $roockID;
    
    /**
     * expense type id of payment voucher non product
     * @var int 
     */
    public $paymentVoucherNonItemProductExpenseTypeId;
    public $paymentVoucherNonItemProductExpenseTypeFormat;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->paymentVoucherNonItemProductID = (!empty($data['paymentVoucherNonItemProductID'])) ? $data['paymentVoucherNonItemProductID'] : null;
        $this->paymentVoucherID = (!empty($data['paymentVoucherID'])) ? $data['paymentVoucherID'] : null;
        $this->paymentVoucherNonItemProductDiscription = (!empty($data['paymentVoucherNonItemProductDiscription'])) ? $data['paymentVoucherNonItemProductDiscription'] : null;
        $this->paymentVoucherNonItemProductQuantity = (!empty($data['paymentVoucherNonItemProductQuantity'])) ? $data['paymentVoucherNonItemProductQuantity'] : null;
        $this->paymentVoucherNonItemProductUnitPrice = (!empty($data['paymentVoucherNonItemProductUnitPrice'])) ? $data['paymentVoucherNonItemProductUnitPrice'] : 0.00;
        $this->paymentVoucherNonItemProductTotalPrice = (!empty($data['paymentVoucherNonItemProductTotalPrice'])) ? $data['paymentVoucherNonItemProductTotalPrice'] : 0.00;
        $this->roockID = (!empty($data['roockID'])) ? $data['roockID'] : null;
        $this->paymentVoucherNonItemProductExpenseTypeId = (!empty($data['paymentVoucherNonItemProductExpenseTypeId'])) ? $data['paymentVoucherNonItemProductExpenseTypeId'] : null;
        $this->paymentVoucherNonItemProductExpenseTypeFormat = (!empty($data['paymentVoucherNonItemProductExpenseTypeFormat'])) ? $data['paymentVoucherNonItemProductExpenseTypeFormat'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
