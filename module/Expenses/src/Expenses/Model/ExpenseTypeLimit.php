<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class Expense Type Limit
 * @package Expenses\Model
 * @author Damith Thamara <damith@thinkcube.com>
 */
class ExpenseTypeLimit implements InputFilterAwareInterface
{

    public $expenseTypeLimitId;
    public $expenseTypeId;
    public $expenseTypeLimitMin;
    public $expenseTypeLimitMax;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->expenseTypeLimitId = (isset($data['expenseTypeLimitId'])) ? $data['expenseTypeLimitId'] : null;
        $this->expenseTypeId = (isset($data['expenseTypeId'])) ? $data['expenseTypeId'] : null;
        $this->expenseTypeLimitMin = (isset($data['expenseTypeLimitMin'])) ? $data['expenseTypeLimitMin'] : null;
        $this->expenseTypeLimitMax = (isset($data['expenseTypeLimitMax'])) ? $data['expenseTypeLimitMax'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
