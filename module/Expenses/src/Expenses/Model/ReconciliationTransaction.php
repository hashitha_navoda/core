<?php
namespace Expenses\Model;

/**
 * Class ReconciliationTransaction
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class ReconciliationTransaction {
    
    /**
     * @var int Auto incremented reconciliation transaction Id. (Primary key)
     */
    public $reconciliationTransactionId;
    
    /**
     * @var decimal reconciliation credit amount.
     */
    public $reconciliationTransactionCreditAmount;

    /**
     * @var decimal reconciliation debit amount.
     */
    public $reconciliationTransactionDebitAmount;
    
    /**
     * @var string reconciliation transaction type.
     * 
     * cheque-deposit
     * cheque-issue
     */
    public $reconciliationTransactionType;
    
    /**
     * @var int incomingPaymentMethodCheque Id.
     */
    public $incomingPaymentMethodChequeId;
    
    /**
     * @var int outGoingPaymentMethodsNumbers Id.
     */
    public $outGoingPaymentMethodsNumbersId;
    
    /**
     * @var int outGoingPaymentMethodsNumbers Id.
     */
    public $incomingPaymentMethodBankTransferId;
    
    /**
     * @var int reconciliation Id
     */
    public $reconciliationId;
    
    /**
     * @var int entity id.
     */
    public $entityId;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->reconciliationTransactionId = (isset($data['reconciliationTransactionId'])) ? $data['reconciliationTransactionId'] : null;
        $this->reconciliationTransactionCreditAmount = (isset($data['reconciliationTransactionCreditAmount'])) ? $data['reconciliationTransactionCreditAmount'] : null;
        $this->reconciliationTransactionDebitAmount = (isset($data['reconciliationTransactionDebitAmount'])) ? $data['reconciliationTransactionDebitAmount'] : null;
        $this->reconciliationTransactionType = (isset($data['reconciliationTransactionType'])) ? $data['reconciliationTransactionType'] : null;
        $this->incomingPaymentMethodChequeId = (isset($data['incomingPaymentMethodChequeId'])) ? $data['incomingPaymentMethodChequeId'] : null;
        $this->outGoingPaymentMethodsNumbersId = (isset($data['outGoingPaymentMethodsNumbersId'])) ? $data['outGoingPaymentMethodsNumbersId'] : null;
        $this->incomingPaymentMethodBankTransferId = (isset($data['incomingPaymentMethodBankTransferId'])) ? $data['incomingPaymentMethodBankTransferId'] : null;
        $this->reconciliationId = (isset($data['reconciliationId'])) ? $data['reconciliationId'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
}
