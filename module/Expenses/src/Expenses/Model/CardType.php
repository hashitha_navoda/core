<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Description of CardType
 *
 * @author shermilan
 */
class CardType implements InputFilterAwareInterface
{

    /**
     * Auto increment Primary ID
     * @var int
     */
    public $cardTypeID;

    /**
     * Identifying name, unique
     * @var string
     */
    public $cardTypeName;

    /**
     * entity ID
     * @var int
     */
    public $entityID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->cardTypeID = (isset($data['cardTypeID'])) ? $data['cardTypeID'] : null;
        $this->cardTypeName = (isset($data['cardTypeName'])) ? $data['cardTypeName'] : null;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : null;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'cardTypeName',
                                'required' => TRUE,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'StringLength',
                                        'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min' => 1,
                                            'max' => 100,
                                        ),
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'entityID',
                                'required' => true,
                            )
                    )
            );
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

}
