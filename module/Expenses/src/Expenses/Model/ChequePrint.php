<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

class ChequePrint implements InputFilterAwareInterface
{

    public $chequePrintID;
    public $paymentID;
    public $comment;
    public $entityID;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->chequePrintID = (isset($data['chequePrintID'])) ? $data['chequePrintID'] : null;
        $this->paymentID = (isset($data['paymentID'])) ? $data['paymentID'] : null;
        $this->comment = (isset($data['comment'])) ? $data['comment'] : null;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
