<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * Description of CardTypeTable
 *
 * @author shermilan
 */
class CardTypeTable
{

    protected $tableGateway;

    /**
     *
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Get card type list
     * @param bool $paginated
     * @return Paginator  or Mixed
     */
    public function fetchAll($paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cardType')
                ->join('entity', 'cardType.entityID = entity.entityID', ['deleted'], 'left')
                ->order(['cardType.cardTypeName'])
        ->where->equalTo('entity.deleted', 0);
        if ($paginated) {
            $paginatorAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * save
     * @param \Expenses\Model\CardType $cardType
     * @return boolean or Last inserted ID
     */
    public function save(CardType $cardType)
    {
        $data = [
            'cardTypeName' => $cardType->cardTypeName,
            'entityID' => $cardType->entityID
        ];

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->getLastInsertValue();
        } else {
            return FALSE;
        }
    }

    /**
     *  get Card type by name
     * @param int $cardTypeName
     * @return object
     */
    public function getCardTypeByName($cardTypeName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cardType')
                ->join('entity', 'cardType.cardTypeID = entity.entityID', ['deleted'], 'left')
                ->where(['deleted' => 0])
        ->where->like('cardType.cardTypeName', $cardTypeName);

        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    /**
     * get card type by ID
     * @param int $cardTypeID
     * @return object
     */
    public function getCardTypeByID($cardTypeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cardType')
                ->join('entity', 'cardType.entityID = entity.entityID', ['deleted'], 'left')
                ->where(['deleted' => 0, 'cardTypeID' => $cardTypeID]);

        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    /**
     * update
     * @param \Expenses\Model\CardType $cardType
     * @return boolean
     */
    public function update(CardType $cardType)
    {
        $data = [
            'cardTypeName' => $cardType->cardTypeName,
        ];

        return $this->tableGateway->update($data, ['cardTypeID' => $cardType->cardTypeID]);
    }

    /**
     *
     * @param type $cardTypeID
     * @return type
     */
    public function delete($cardTypeID)
    {
        return $this->tableGateway->delete(['cardTypeID' => $cardTypeID]);
    }

    /**
     *
     * @param string $searchKey
     * @return object array
     */
    public function search($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cardType')
                ->join('entity', 'cardType.entityID = entity.entityID', ['deleted'], 'left')
                ->where(['deleted' => 0])
        ->where->like('cardTypeName', '%' . $searchKey . '%');

        $statment = $sql->prepareStatementForSqlObject($select);

        return $statment->execute();
    }

    /**
     * Get UnAssigned to any account card types
     * also get card types assigned given account Id
     * @return type
     */
    public function getUnAssignedCardTypes($accountID = false)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cardType')
                ->join('entity', 'cardType.entityID = entity.entityID', ['deleted'], 'left')
                ->where(['deleted' => 0]);
        if (!$accountID) {
            $select->where->isNull('accountID')->OR->equalTo('accountID', 0);
        } else {
            $select->where->isNull('accountID')->OR->equalTo('accountID', 0)->OR->equalTo('accountID', $accountID);
        }

        $statment = $sql->prepareStatementForSqlObject($select);

        $results = $statment->execute();

//        converting to Array
        return iterator_to_array($results);
    }

    public function assignAccount($accountID, $cardTypes)
    {
        return $this->tableGateway->update(['accountID' => $accountID], ['cardTypeID' => $cardTypes]);
    }

    public function clearAccount($accountID)
    {
        return $this->tableGateway->update(['accountID' => ''], ['accountID' => $accountID]);
    }

    /**
     * @return type
     */
    public function getAssignedCardTypes()
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cardType')
                ->join('entity', 'cardType.entityID = entity.entityID', ['deleted'], 'left')
                ->where(['deleted' => 0])
        ->where->notEqualTo('accountID', 0)->isNotNull('accountID');

        $statment = $sql->prepareStatementForSqlObject($select);

        return $statment->execute();
    }

    /**
     *
     * @param int $cardTypeID
     * @return mixed
     */
    public function getAccountIDByID($cardTypeID)
    {
        $adapter = $this->tableGateway->getAdapter();

        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('cardType')
                ->join('entity', 'cardType.entityID = entity.entityID', ['deleted'], 'left')
                ->where(['deleted' => 0, 'cardTypeID' => $cardTypeID]);

        $statment = $sql->prepareStatementForSqlObject($select);

        return $statment->execute();
    }

}
