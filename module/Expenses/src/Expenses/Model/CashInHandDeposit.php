<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class CashInHandDeposit
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class CashInHandDeposit  implements InputFilterAwareInterface{
    
    /**
     * @var int Auto incremented cash in hand deposit Id. (Primary key)
     */
    public $cashInHandDepositId;
    
    /**
     * @var float deposit amount.
     */
    public $cashInHandDepositAmount;
    
    /**
     * @var date deposit date.
     */
    public $cashInHandDepositDate;
    
    /**
     * @var string deposit comment.
     */
    public $cashInHandDepositComment;
    
    /**
     * int location id
     * @var type 
     */
    public $locationId;


    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->cashInHandDepositId = (isset($data['cashInHandDepositId'])) ? $data['cashInHandDepositId'] : null;
        $this->cashInHandDepositAmount = (isset($data['transactionAmount'])) ? $data['transactionAmount'] : 0.00;
        $this->cashInHandDepositDate = (isset($data['transactionDate'])) ? $data['transactionDate'] : null;
        $this->cashInHandDepositComment = (isset($data['transactionComment'])) ? $data['transactionComment'] : null;
        $this->locationId = (isset($data['locationId'])) ? $data['locationId'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transaction',
                        'required' => true,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionType',
                        'required' => true,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionIssuedBankId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionIssuedAccountId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionReceivedBankId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionReceivedAccountId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionAmount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Float',
                            ),
                        ),
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionDate',
                        'required' => true,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }
    
    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
