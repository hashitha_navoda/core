<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * Class ChequeDepositCancellationTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 * 
 * This file contains ChequeDepositCancellation Table Functions
 */
class ChequeDepositCancellationTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * ChequeDepositCancellationTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Save ChequeDepositCancellation
     * @param \Expenses\Model\ChequeDepositCancellation $chequeDepositCancellation
     * @return mixed
     */
    public function save(ChequeDepositCancellation $chequeDepositCancellation)
    {        
        $data = array(
            'chequeDepositCancelationId' => $chequeDepositCancellation->chequeDepositCancelationId,
            'chequeDepositCancelationCharge' => $chequeDepositCancellation->chequeDepositCancelationCharge,
            'chequeDepositCancelationDate' => $chequeDepositCancellation->chequeDepositCancelationDate,
            'chequeDepositCancellationComment' => $chequeDepositCancellation->chequeDepositCancellationComment,
            'chequeDepositId' => $chequeDepositCancellation->chequeDepositId
        );
        
        if($this->tableGateway->insert($data)){
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
}
