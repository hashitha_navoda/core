<?php
namespace Expenses\Model;

/**
 * Class ChequeDepositCancellation
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class ChequeDepositCancellation {
    
    /**
     * @var int Auto incremented cheque deposit cancellation id (Primary key)
     */
    public $chequeDepositCancelationId;
    
    /**
     * @var decimal cheque deposit cancelation charge
     */
    public $chequeDepositCancelationCharge;
    
    /**
     * @var String cheque deposit cancelation date
     */
    public $chequeDepositCancelationDate;
    
    /**
     * @var String cheque deposit cancelation comment
     */
    public $chequeDepositCancellationComment;
    
    /**
     * @var int cheque deposit id
     */
    public $chequeDepositId;
    
    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->chequeDepositCancelationId = (isset($data['chequeDepositCancelationId'])) ? $data['chequeDepositCancelationId'] : null;
        $this->chequeDepositCancelationCharge = (isset($data['chequeDepositCancelationCharge'])) ? $data['chequeDepositCancelationCharge'] : null;
        $this->chequeDepositCancelationDate = (isset($data['chequeDepositCancelationDate'])) ? $data['chequeDepositCancelationDate'] : null;
        $this->chequeDepositCancellationComment = (isset($data['chequeDepositCancellationComment'])) ? $data['chequeDepositCancellationComment'] : null;
        $this->chequeDepositId = (isset($data['chequeDepositId'])) ? $data['chequeDepositId'] : null;
    }
}
