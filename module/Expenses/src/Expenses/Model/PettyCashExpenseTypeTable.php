<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

/**
 * Description of ExpenseTypeTable
 *
 * @author shermilan
 */
class PettyCashExpenseTypeTable
{

    protected $tableGateway;

    /**
     * ExpenseTypeTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Featch all from Expense Type
     *
     * @param type $isPagination
     * @return Paginator
     */
    public function fetchAll($isPagination = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashExpenseType');
        $select->order(array('pettyCashExpenseTypeName'));

        if ($isPagination) {
            $paginatorAdaptor = new DbSelect($select, $adapter);
            return new Paginator($paginatorAdaptor);
        } else {
            $statment = $sql->prepareStatementForSqlObject($select);
            return $statment->execute();
        }
    }

    /**
     * Save the data to Petty Cash Expense Type
     * @param \Expenses\Model\PettyCashExpenseType $pettyCashxpenseType
     * @return boolean
     */
    public function save(PettyCashExpenseType $pettyCashxpenseType)
    {
        $data = [
            'pettyCashExpenseTypeName' => $pettyCashxpenseType->pettyCashExpenseTypeName,
            'pettyCashExpenseTypeDescription' => $pettyCashxpenseType->pettyCashExpenseTypeDescription,
            'pettyCashExpenseTypeMinAmount' => $pettyCashxpenseType->pettyCashExpenseTypeMinAmount,
            'pettyCashExpenseTypeMaxAmount' => $pettyCashxpenseType->pettyCashExpenseTypeMaxAmount,
            'pettyCashExpenseTypeStatus' => $pettyCashxpenseType->pettyCashExpenseTypeStatus,
            'pettyCashExpenseTypeAccountID' => $pettyCashxpenseType->pettyCashExpenseTypeAccountID,
        ];
        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function getDetailsByName($pettyCashExpenseTypeName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashExpenseType');
        $select->where(['pettyCashExpenseTypeName' => $pettyCashExpenseTypeName]);
        $statment = $sql->prepareStatementForSqlObject($select);

        return $statment->execute();
    }

    public function getDataByID($pettyCashExpenseTypeID)
    {
        return $this->tableGateway->select(['pettyCashExpenseTypeID' => $pettyCashExpenseTypeID]);
    }

    /**
     * Save the data to Petty Cash Expense Type
     * @param \Expenses\Model\PettyCashExpenseType $pettyCashExpenseType
     * @return boolean
     */
    public function update(PettyCashExpenseType $pettyCashExpenseType)
    {
        $data = [
            'pettyCashExpenseTypeName' => $pettyCashExpenseType->pettyCashExpenseTypeName,
            'pettyCashExpenseTypeDescription' => $pettyCashExpenseType->pettyCashExpenseTypeDescription,
            'pettyCashExpenseTypeMinAmount' => $pettyCashExpenseType->pettyCashExpenseTypeMinAmount,
            'pettyCashExpenseTypeMaxAmount' => $pettyCashExpenseType->pettyCashExpenseTypeMaxAmount,
            'pettyCashExpenseTypeStatus' => $pettyCashExpenseType->pettyCashExpenseTypeStatus,
            'pettyCashExpenseTypeAccountID' => $pettyCashExpenseType->pettyCashExpenseTypeAccountID,
        ];
        return $this->tableGateway->update($data, ['pettyCashExpenseTypeID' => $pettyCashExpenseType->pettyCashExpenseTypeID]);
    }

    public function delete($pettyCashExpenseTypeID)
    {
        return $this->tableGateway->delete(['pettyCashExpenseTypeID' => $pettyCashExpenseTypeID]);
    }

    public function changeStatusByExpenseTypeID($pettyCashExpenseTypeID, $status)
    {
        return $this->tableGateway->update([ 'pettyCashExpenseTypeStatus' => $status], ['pettyCashExpenseTypeID' => $pettyCashExpenseTypeID]);
    }

    public function searchExpenseTypeList($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashExpenseType');
        $select->where(new PredicateSet(array(new Operator('pettyCashExpenseTypeName', 'like', '%' . $searchKey . '%'))));

        $statment = $sql->prepareStatementForSqlObject($select);
        $result = $statment->execute();

        return $result;
    }

    public function getActiveExpenseTypes()
    {
        return $this->tableGateway->select(['pettyCashExpenseTypeStatus' => '1']);
    }

    public function searchPettyCashExpenseTypeForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashExpenseType');
        $select->where->like('pettyCashExpenseTypeName', '%' . $searchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

}
