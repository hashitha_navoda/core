<?php

namespace Expenses\Model;

/**
 * Class Petty Cash Float Default Accounts
 * @package Expenses\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 */
class PettyCashFloatDefaultAccounts {
    
    /**
     * @var int Auto incremented bank Id. (Primary key)
     */
    public $pettyCashFloatDefaultAccountsID;
    
    /**
     * @var int pettyCashFloatFinanceAccountID.
     */
    public $pettyCashFloatFinanceAccountID;
    
    /**
     * @var int pettyCashFloatIssueFinanceAccountID.
     */
    public $pettyCashFloatIssueFinanceAccountID;
    
    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->pettyCashFloatDefaultAccountsID = (isset($data['pettyCashFloatDefaultAccountsID'])) ? $data['pettyCashFloatDefaultAccountsID'] : null;
        $this->pettyCashFloatFinanceAccountID = (isset($data['pettyCashFloatFinanceAccountID'])) ? $data['pettyCashFloatFinanceAccountID'] : null;
        $this->pettyCashFloatIssueFinanceAccountID = (isset($data['pettyCashFloatIssueFinanceAccountID'])) ? $data['pettyCashFloatIssueFinanceAccountID'] : null;
    }
}