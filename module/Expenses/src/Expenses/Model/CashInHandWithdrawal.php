<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class CashInHandWithdrawal
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */

class CashInHandWithdrawal implements InputFilterAwareInterface{
    
    /**
     * @var int Auto incremented cash in hand withdrawal Id. (Primary key)
     */
    public $cashInHandWithdrawalId;
    
    /**
     * @var float withdrawal amount.
     */
    public $cashInHandWithdrawalAmount;
    
    /**
     * @var date withdrawal date.
     */
    public $cashInHandWithdrawalDate;
    
    /**
     * @var string withdrawal comment.
     */
    public $cashInHandWithdrawalComment;
    
    /**
     * @var int locationId.
     */
    public $locationId;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->cashInHandWithdrawalId = (isset($data['cashInHandWithdrawalId'])) ? $data['cashInHandWithdrawalId'] : null;
        $this->cashInHandWithdrawalAmount = (isset($data['transactionAmount'])) ? $data['transactionAmount'] : 0.00;
        $this->cashInHandWithdrawalDate = (isset($data['transactionDate'])) ? $data['transactionDate'] : null;
        $this->cashInHandWithdrawalComment = (isset($data['transactionComment'])) ? $data['transactionComment'] : null;
        $this->locationId = (isset($data['locationId'])) ? $data['locationId'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transaction',
                        'required' => true,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionType',
                        'required' => true,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionIssuedBankId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionIssuedAccountId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionReceivedBankId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionReceivedAccountId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionAmount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Float',
                            ),
                        ),
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionDate',
                        'required' => true,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
}
