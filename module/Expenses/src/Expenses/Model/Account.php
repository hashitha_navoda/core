<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class Account
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class Account implements InputFilterAwareInterface
{

    /**
     * @var int Auto incremented account Id. (Primary key)
     */
    public $accountId;

    /**
     * @var string account name.
     */
    public $accountName;

    /**
     * @var string account number.
     */
    public $accountNumber;

    /**
     * @var decimal account balance.
     */
    public $accountBalance;

    /**
     * @var int account status.
     *
     * 0 - Lock
     * 1 - Unlock
     */
    public $accountStatus;

    /**
     * @var int account type id.
     */
    public $accountTypeId;

    /**
     * @var int account currency id.
     */
    public $currencyId;

    /**
     * @var int bank id.
     */
    public $bankId;

    /**
     * @var int bank branch id.
     */
    public $bankBranchId;

    /**
     * @var int financeAccountID.
     */
    public $financeAccountID;

    /**
     * @var int entity id.
     */
    public $entityId;

    /**
     *   @var bool is this card payment account
     */
    public $isCardPaymentAccount;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->accountId = (isset($data['accountId'])) ? $data['accountId'] : null;
        $this->accountName = (isset($data['accountName'])) ? $data['accountName'] : null;
        $this->accountNumber = (isset($data['accountNumber'])) ? $data['accountNumber'] : null;
        $this->accountBalance = (isset($data['accountBalance'])) ? $data['accountBalance'] : null;
        $this->accountStatus = (isset($data['accountStatus'])) ? $data['accountStatus'] : null;
        $this->accountTypeId = (isset($data['accountTypeId'])) ? $data['accountTypeId'] : null;
        $this->currencyId = (isset($data['currencyId'])) ? $data['currencyId'] : null;
        $this->bankId = (isset($data['bankId'])) ? $data['bankId'] : null;
        $this->bankBranchId = (isset($data['bankBranchId'])) ? $data['bankBranchId'] : null;
        $this->financeAccountID = (isset($data['financeAccountID'])) ? $data['financeAccountID'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
        $this->isCardPaymentAccount = (isset($data['isCardPaymentAccount'])) ? $data['isCardPaymentAccount'] : 0;
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'accountName',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'StringLength',
                                        'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min' => 1,
                                            'max' => 100,
                                        ),
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'accountNumber',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'StringLength',
                                        'options' => array(
                                            'encoding' => 'UTF-8',
                                            'min' => 1,
                                            'max' => 100,
                                        ),
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'accountStatus',
                                'required' => true,
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'bankId',
                                'required' => true,
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'bankBranchId',
                                'required' => true,
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'accountTypeId',
                                'required' => true,
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'currencyId',
                                'required' => true,
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'accountBalance',
                                'required' => true,
                                'filters' => array(
                                    array('name' => 'StripTags'),
                                    array('name' => 'StringTrim'),
                                ),
                                'validators' => array(
                                    array(
                                        'name' => 'Float',
                                    ),
                                    array(
                                        'name' => 'Between',
                                        'options' => array(
                                            'min' => pow(10, 15) * -1,
                                            'max' => pow(10, 15),
                                            'inclusive' => false
                                        ),
                                    ),
                                ),
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'accountStatus',
                                'required' => true
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'isCardPaymentAccount',
                                'required' => false
                            )
                    )
            );

            $inputFilter->add(
                    $factory->createInput(
                            array(
                                'name' => 'financeAccountID',
                                'required' => false
                            )
                    )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

}
