<?php

namespace Expenses\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PaymentVoucherProductTax implements InputFilterAwareInterface
{

    public $paymentVoucherProductTaxID;
    public $paymentVoucherID;
    public $paymentVoucherProductIdOrNonItemProductID;
    public $paymentVoucherTaxID;
    public $paymentVoucherTaxPrecentage;
    public $paymentVoucherTaxAmount;
    public $paymentVoucherTaxItemOrNonItemProduct;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->paymentVoucherProductTaxID = (!empty($data['paymentVoucherProductTaxID'])) ? $data['paymentVoucherProductTaxID'] : null;
        $this->paymentVoucherID = (!empty($data['paymentVoucherID'])) ? $data['paymentVoucherID'] : null;
        $this->paymentVoucherProductIdOrNonItemProductID = (!empty($data['paymentVoucherProductIdOrNonItemProductID'])) ? $data['paymentVoucherProductIdOrNonItemProductID'] : null;
        $this->paymentVoucherTaxID = (!empty($data['paymentVoucherTaxID'])) ? $data['paymentVoucherTaxID'] : null;
        $this->paymentVoucherTaxPrecentage = (!empty($data['paymentVoucherTaxPrecentage'])) ? $data['paymentVoucherTaxPrecentage'] : null;
        $this->paymentVoucherTaxAmount = (!empty($data['paymentVoucherTaxAmount'])) ? $data['paymentVoucherTaxAmount'] : null;
        $this->paymentVoucherTaxItemOrNonItemProduct = (!empty($data['paymentVoucherTaxItemOrNonItemProduct'])) ? $data['paymentVoucherTaxItemOrNonItemProduct'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
