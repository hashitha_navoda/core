<?php
namespace Expenses\Model;

/**
 * Class Reconciliation
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class Reconciliation {
    
    /**
     * @var int Auto incremented reconciliation Id. (Primary key)
     */
    public $reconciliationId;

    /**
     * @var decimal reconciliation amount.
     */
    public $reconciliationAmount;
    
    /**
     * @var string reconciliation comment.
     */
    public $reconciliationComment;
    
    /**
     * @var date reconciliation date
     */
    public $reconciliationDate;
    
    /**
     * @var decimal reconciliation statement balance
     */
    public $reconciliationStatementBalance;

    /**
     * @var int account id.
     */
    public $accountId;
    public $reconciliationStatus;
    public $reconciliationStartDate;
    public $reconciliationEndDate;
    
    /**
     * @var int entity id.
     */
    public $entityId;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->reconciliationId = (isset($data['reconciliationId'])) ? $data['reconciliationId'] : null;
        $this->reconciliationAmount = (isset($data['reconciliationAmount'])) ? $data['reconciliationAmount'] : null;
        $this->reconciliationComment = (isset($data['reconciliationComment'])) ? $data['reconciliationComment'] : null;
        $this->reconciliationDate = (isset($data['reconciliationDate'])) ? $data['reconciliationDate'] : null;
        $this->reconciliationStatementBalance = (isset($data['reconciliationStatementBalance'])) ? $data['reconciliationStatementBalance'] : null;
        $this->accountId = (isset($data['accountId'])) ? $data['accountId'] : null;
        $this->reconciliationStatus = (isset($data['reconciliationStatus'])) ? $data['reconciliationStatus'] : null;
        $this->reconciliationStartDate = (isset($data['reconciliationStartDate'])) ? $data['reconciliationStartDate'] : null;
        $this->reconciliationEndDate = (isset($data['reconciliationEndDate'])) ? $data['reconciliationEndDate'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
}
