<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

/**
 * Class ExpenseCategoryTable
 * @package Expenses\Model
 * @author Damith Karunathilaka <damith@thinkcube.com>
 *
 * This file contains expense type Table Functions
 */
class ExpenseTypeTable
{

    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('expenseType')
                    ->columns(array('*'))
                    ->join('expenseCategory', 'expenseType.expenseTypeCategory = expenseCategory.expenseCategoryId', array('expenseCategoryName'))
                    ->join('entity', 'expenseType.entityId = entity.entityID', array('deleted'))
                    ->where(array('entity.deleted' => '0'));
            $paginatorAdapter = new DbSelect($select, $adapter);
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        } else {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('expenseType')
                    ->columns(array('*'))
                    ->join('expenseTypeLimit', 'expenseType.expenseTypeId = expenseTypeLimit.expenseTypeId', array('*'), 'left')
                    ->join('expenseTypeLimitApprover', 'expenseTypeLimit.expenseTypeLimitId = expenseTypeLimitApprover.expenseTypeLimitId', array('*'), 'left');
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            $resultsArray = [];
            foreach ($results as $result) {
                $resultsArray[] = $result;
            }
            return $resultsArray;
        }
    }

    public function getExpeneTypesForApprover()
    {
        $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('expenseType')
                    ->columns(array('*'))
                    ->join('entity', 'expenseType.entityId = entity.entityID', array('deleted'))
                    ->where(array('entity.deleted' => '0'))
                    ->where(array('expenseType.expenseTypeStatus' => '1'));
            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();
            $resultsArray = [];
            foreach ($results as $result) {
                $resultsArray[] = $result;
            }
            return $resultsArray;
    }


    public function getExpenseTypeByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('expenseType')
                ->columns(array('*'))
                ->where(array('expenseType.expenseTypeId' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
    }

    public function getExpenseTypeDetailsByID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('expenseType')
                ->columns(array('*'))
                ->join('expenseTypeLimit', 'expenseType.expenseTypeId = expenseTypeLimit.expenseTypeId', array('*'), 'left')
                ->join('expenseTypeLimitApprover', 'expenseTypeLimit.expenseTypeLimitId = expenseTypeLimitApprover.expenseTypeLimitId', array('*'), 'left')
                ->where(array('expenseType.expenseTypeId' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function create(ExpenseType $expenseType)
    {
        $data = array(
            'expenseTypeId' => $expenseType->expenseTypeId,
            'expenseTypeName' => $expenseType->expenseTypeName,
            'expenseTypeCategory' => $expenseType->expenseTypeCategory,
            'expenseTypeApproverEnabled' => $expenseType->expenseTypeApproverEnabled,
            'expenseTypeStatus' => $expenseType->expenseTypeStatus,
            'expenseTypeAccountID' => $expenseType->expenseTypeAccountID,
            'entityId' => $expenseType->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    public function update($data, $expenseTypeId)
    {
        try {
            $this->tableGateway->update($data, array('expenseTypeId' => $expenseTypeId));
            return TRUE;
        } catch (\Exception $e) {
            return FALSE;
        }
    }

    public function changeStatus($status, $expenseTypeId)
    {
        if ($this->tableGateway->update(array('expenseTypeStatus' => $status), array('expenseTypeId' => $expenseTypeId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function searchExpenseTypeForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('expenseType');
        $select->join('entity', 'expenseType.entityID=  entity.entityID', array("*"), "left");
        $select->where(array("entity.deleted" => 0));
        $select->where(array("expenseType.expenseTypeStatus" => 1));
        $select->where->like('expenseTypeName', '%' . $searchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getApproversByTypeIDAndPriceRange($typeID, $totalPrice)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('expenseType');
        $select->join('entity', 'expenseType.entityID=  entity.entityID', array("*"), "left");
        $select->join('expenseTypeLimit', 'expenseType.expenseTypeId=  expenseTypeLimit.expenseTypeId', array("*"), "left");
        $select->join('expenseTypeLimitApprover', 'expenseTypeLimit.expenseTypeLimitId=  expenseTypeLimitApprover.expenseTypeLimitId', array("*"), "left");
        $select->join('approvers', 'expenseTypeLimitApprover.expenseTypeLimitApprover=  approvers.approverID', array('firstName', 'lastName', 'email'), "left");
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $select->where(new PredicateSet(array(new Operator('expenseType.expenseTypeId', '=', $typeID))));
        $select->where(new PredicateSet(array(new Operator('expenseTypeLimit.expenseTypeLimitMin', '<=', $totalPrice))));
        $select->where(new PredicateSet(array(new Operator('expenseTypeLimit.expenseTypeLimitMax', '>=', $totalPrice))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get All Expense Type List
     * @author sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function getExpeneTypes()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('expenseType');
        $select->columns(['expenseTypeName', 'expenseTypeId']);
        $select->join('entity', 'expenseType.entityID=  entity.entityID', ['deleted'], "left");
        $select->where(array("entity.deleted" => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getExpenseTypesByCategory($expenseCategoryId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('expenseType');
        $select->columns(array('*'));
        $select->join('entity', 'expenseType.entityID=  entity.entityID', ['deleted'], "left");
        $select->where(array("entity.deleted" => 0));
        $select->where(array("expenseType.expenseTypeCategory" => $expenseCategoryId));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

}
