<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class AccountTransfer
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class AccountTransfer implements InputFilterAwareInterface {
    
    /**
     * @var int Auto incremented account transfer Id. (Primary key)
     */
    public $accountTransferId;
    
    /**
     * @var int issued account Id.
     */
    public $accountTransferIssuedAccountId;
    
    /**
     * @var int received account Id.
     */
    public $accountTransferReceivedAccountId;
    
    /**
     * @var float transfer amount.
     */
    public $accountTransferAmount;
    
    /**
     * @var float issued account currency rate.
     */
    public $accountTransferIssuedAccountCurrencyRate;
    
    /**
     * @var float received account currency rate.
     */
    public $accountTransferReceivedAccountCurrencyRate;
    
    /**
     * @var date transfer date.
     */
    public $accountTransferDate;
    
    /**
     * @var string transfer comment.
     */
    public $accountTransferComment;

     /**
     * @var int accountTransferIssuedFinanceAccountId.
     */
    public $accountTransferIssuedFinanceAccountId;

     /**
     * @var int accountTransferRecivedFinanceAccountId.
     */
    public $accountTransferRecivedFinanceAccountId;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->accountTransferId = (isset($data['accountTransferId'])) ? $data['accountTransferId'] : null;
        $this->accountTransferIssuedAccountId = (isset($data['transactionIssuedAccountId'])) ? $data['transactionIssuedAccountId'] : null;
        $this->accountTransferReceivedAccountId = (isset($data['transactionReceivedAccountId'])) ? $data['transactionReceivedAccountId'] : null;
        $this->accountTransferDate = (isset($data['transactionDate'])) ? $data['transactionDate'] : null;
        $this->accountTransferAmount = (isset($data['transactionAmount'])) ? $data['transactionAmount'] : 0.00;
        $this->accountTransferIssuedAccountCurrencyRate = (isset($data['accountTransferIssuedAccountCurrencyRate'])) ? $data['accountTransferIssuedAccountCurrencyRate'] : null;
        $this->accountTransferReceivedAccountCurrencyRate = (isset($data['accountTransferReceivedAccountCurrencyRate'])) ? $data['accountTransferReceivedAccountCurrencyRate'] : null;
        $this->accountTransferComment = (isset($data['transactionComment'])) ? $data['transactionComment'] : null;
        $this->accountTransferIssuedFinanceAccountId = (isset($data['transactionIssuedFinanceAccountId'])) ? $data['transactionIssuedFinanceAccountId'] : null;
        $this->accountTransferRecivedFinanceAccountId = (isset($data['transactionRecivedFinanceAccountId'])) ? $data['transactionRecivedFinanceAccountId'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
    
    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transaction',
                        'required' => true,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionIssuedFinanceAccountId',
                        'required' => true,                        
                    )
                )
            );

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionRecivedFinanceAccountId',
                        'required' => true,                        
                    )
                )
            );

             $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionBankAccountId',
                        'required' => false,                        
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionAmount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'Float',
                            ),
                        ),
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'transactionDate',
                        'required' => true,                        
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }
    
}
