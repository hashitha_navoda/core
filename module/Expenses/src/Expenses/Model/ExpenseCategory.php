<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class Expense Category
 * @package Expenses\Model
 * @author Damith Thamara <damith@thinkcube.com>
 */
class ExpenseCategory implements InputFilterAwareInterface
{

    public $expenseCategoryId;
    public $expenseCategoryName;
    public $expenseCategoryParentId;
    public $expenseCategoryStatus;
    public $entityId;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->expenseCategoryId = (isset($data['expenseCategoryId'])) ? $data['accountId'] : null;
        $this->expenseCategoryName = (isset($data['expenseCategoryName'])) ? $data['expenseCategoryName'] : null;
        $this->expenseCategoryParentId = (isset($data['expenseCategoryParentId'])) ? $data['expenseCategoryParentId'] : null;
        $this->expenseCategoryStatus = (isset($data['expenseCategoryStatus'])) ? $data['expenseCategoryStatus'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
