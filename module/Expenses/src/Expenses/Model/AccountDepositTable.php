<?php
namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class AccountDepositTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 *
 * This file contains Account Deposit Table Functions
 */
class AccountDepositTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountDepositTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Create new account deposit
     * @param \Expenses\Model\AccountDeposit $accountDeposit
     * @return mixed
     */
    public function saveDeposit(AccountDeposit $accountDeposit)
    {
        $data = array(
            'accountDepositAccountId' => $accountDeposit->accountDepositAccountId,
            'accountDepositAmount' => $accountDeposit->accountDepositAmount,
            'accountDepositDate' => $accountDeposit->accountDepositDate,
            'accountDepositComment' => $accountDeposit->accountDepositComment,
            'accountDepositFinanceAccountID' => $accountDeposit->accountDepositFinanceAccountID,
            'entityId' => $accountDeposit->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Get deposits by account id
     * @param int $accountId
     * @param date $fromDate
     * @param date $toDate
     * @return mixed
     */
    public function getDepositsByAccountId( $accountId = null, $fromDate = null, $toDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountDeposit')
                ->columns(array('*'))
                ->join('entity', 'accountDeposit.entityId = entity.entityID', array('deleted'))
                ->join('account', 'accountDeposit.accountDepositAccountId = account.accountId', array('accountName','accountNumber','bankId'))
                ->join('financeAccounts', 'accountDeposit.accountDepositFinanceAccountID = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('bank', 'account.bankId = bank.bankId', array('bankName'))
                ->where->equalTo('entity.deleted', 0);
        if($accountId){
            $select->where->equalTo('accountDeposit.accountDepositAccountId', $accountId);
        }
        if ($fromDate && $toDate) {
            $select->where->between('accountDeposit.accountDepositDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * Get account deposit details by accountDepositId
     * @param int $depositId
     * @return mixed
     */
    public function getDepositByDepositId( $depositId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountDeposit')
                ->columns(array('*'))
                ->join('entity', 'accountDeposit.entityId = entity.entityID', array('deleted'))
                ->join('account', 'accountDeposit.accountDepositAccountId = account.accountId', array('accountName','accountNumber','bankId'))
                ->join('financeAccounts', 'accountDeposit.accountDepositFinanceAccountID = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('bank', 'account.bankId = bank.bankId', array('bankName'))
                ->where(array('entity.deleted' => 0,'accountDeposit.accountDepositId' => $depositId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if($result->count() == 0){
            return NULL;
        } else {
            return $result->current();
        }
    }

    public function getDepositsByGlAccountId( $financeAccountId = null, $fromDate = null, $toDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountDeposit')
                ->columns(array('*'))
                ->join('entity', 'accountDeposit.entityId = entity.entityID', array('deleted'))
                ->join('account', 'accountDeposit.accountDepositAccountId = account.accountId', array('accountName','accountNumber','bankId'))
                ->join('financeAccounts', 'accountDeposit.accountDepositFinanceAccountID = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join('bank', 'account.bankId = bank.bankId', array('bankName'))
                ->where->equalTo('entity.deleted', 0);
        if($financeAccountId){
            $select->where->equalTo('accountDeposit.accountDepositFinanceAccountID', $financeAccountId);
        }
        if ($fromDate && $toDate) {
            $select->where->between('accountDeposit.accountDepositDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}
