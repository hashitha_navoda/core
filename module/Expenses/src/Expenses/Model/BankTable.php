<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * Class BankTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 * 
 * This file contains Bank Table Functions
 */
class BankTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * BankTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Return all the banks in the table.
     * @param boolean $paginated
     * @param array $order
     * @return mixed
     */
    public function fetchAll($paginated = FALSE,$order=array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bank')
                ->columns(array('*'))
                ->join('entity', 'bank.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0);
        if ($paginated){
            $paginatorAdapter = new DbSelect($select,$this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    /**
     * Return banks which having names match to the given query string.
     * @param string $searchKey
     * @param array $order
     * @param boolean $paginated
     * @return mixed
     */
    public function searchBankByName( $searchKey, $order = array(), $paginated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bank')
                ->columns(array('*'))
                ->join('entity', 'bank.entityId = entity.entityID', array('deleted'))
                ->order($order)
                ->where->equalTo('entity.deleted', 0)
                ->like('bank.bankName', '%' . $searchKey . '%');
        
        if ($paginated){
            $paginatorAdapter = new DbSelect($select,$this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }
    
    /**
     * Return banks according to the given bank id.
     * @param string $bankId
     * @return mixed
     */
    public function getBankByBankId($bankId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bank')
                ->columns(array('*'))
                ->join('entity', 'bank.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('bank.bankId', $bankId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Return banks according to the given bank name.
     * @param string $bankName
     * @return mixed
     */
    public function getBankByBankName($bankName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bank')
                ->columns(array('*'))
                ->join('entity', 'bank.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('bank.bankName', $bankName);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Return banks according to the given bank code.
     * @param string $bankCode
     * @return mixed
     */
    public function getBankByBankCode($bankCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bank')
                ->columns(array('*'))
                ->join('entity', 'bank.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)
                ->where->equalTo('bank.bankCode', $bankCode);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Return bank type according to the given bank name, code and not equal 
     * to the given bank type id
     * @param string $bankId
     * @param string $bankName
     * @param string $bankCode
     * @return mixed
     */
    public function checkBankNameAndBankCodeValid($bankId,$bankName,$bankCode) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('bank')
                ->columns(array('*'))
                ->join('entity', 'bank.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0)                
                ->where->equalTo('bank.bankCode', $bankCode)->where->notEqualTo('bank.bankId',$bankId)
                ->or
                ->where->equalTo('bank.bankName', $bankName)->where->notEqualTo('bank.bankId',$bankId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Create new bank
     * @param \Expenses\Model\Bank $bank
     * @param string $entityId
     * @return boolean
     */
    public function saveBank(Bank $bank,$entityId)
    {
        $data = array(
            'bankCode' => $bank->bankCode,
            'bankName' => $bank->bankName,
            'entityId' => $entityId
        );
        
        if($this->tableGateway->insert($data)){
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Update existing bank
     * @param array $data
     * @param string $bankId
     * @return boolean
     */
    public function updateBank($data,$bankId)
    {
        $bankData = array(
            'bankName' => $data['bankName'],
            'bankCode' => $data['bankCode']
        );        
        if($bankId){
            if($this->tableGateway->update($bankData, array('bankId'=>$bankId))){
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }    
    
    /**
     * this function returns bank list as an array
     * @return Array
     */
    public function getBankListForDropDown() {
        $bankList = array();
        
        foreach ($this->fetchAll() as $bank){
            $bankList[$bank[bankId]] = $bank['bankName'];
        }
        return $bankList;
    }
}
