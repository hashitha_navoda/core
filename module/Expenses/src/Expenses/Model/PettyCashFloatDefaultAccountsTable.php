<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class PettyCashFloatDefaultAccountsTable
 * @package Expenses\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 * 
 * This file contains PettyCashFloatDefaultAccountsTable Functions
 */
class PettyCashFloatDefaultAccountsTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * PettyCashFloatDefaultAccountsTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
    	$resultSet = $this->tableGateway->select();
        return $resultSet;
    }

     public function save(pettyCashFloatDefaultAccounts $pettyCashFloatDefaultAccounts)
    {
    	$data = [
            'pettyCashFloatFinanceAccountID' => $pettyCashFloatDefaultAccounts->pettyCashFloatFinanceAccountID,
            'pettyCashFloatIssueFinanceAccountID' => $pettyCashFloatDefaultAccounts->pettyCashFloatIssueFinanceAccountID,
        ];
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function update($data, $pettyCashFloatDefaultAccountsID)
    {
    	try {
    		$this->tableGateway->update($data, array('pettyCashFloatDefaultAccountsID' => $pettyCashFloatDefaultAccountsID));
        	return true;
    	} catch (Exception $e) {
    		return false;
    	}
    }
    
}
