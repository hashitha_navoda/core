<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class PettyCashVoucherDefaultAccountsTable
 * @package Expenses\Model
 * @author Ashan Madushka <ashan@thinkcube.com>
 * 
 * This file contains PettyCashVoucherDefaultAccountsTable Functions
 */
class PettyCashVoucherDefaultAccountsTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * PettyCashVoucherDefaultAccountsTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

     public function save(pettyCashVoucherDefaultAccounts $pettyCashVoucherDefaultAccounts)
    {
        $data = [
            'pettyCashVoucherFinanceAccountID' => $pettyCashVoucherDefaultAccounts->pettyCashVoucherFinanceAccountID,
        ];
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function update($data, $pettyCashVoucherDefaultAccountsID)
    {
        try {
            $this->tableGateway->update($data, array('pettyCashVoucherDefaultAccountsID' => $pettyCashVoucherDefaultAccountsID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }
    
}
