<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PaymentVoucherProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePaymentVoucherProduct(PaymentVoucherProduct $pavVaucher)
    {
        $data = array(
            'paymentVoucherID' => $pavVaucher->paymentVoucherID,
            'locationProductID' => $pavVaucher->locationProductID,
            'paymentVoucherProductQuantity' => $pavVaucher->paymentVoucherProductQuantity,
            'paymentVoucherProductUnitPrice' => $pavVaucher->paymentVoucherProductUnitPrice,
            'paymentVoucherProductTotalPrice' => $pavVaucher->paymentVoucherProductTotalPrice,
            'paymentVoucherProductQuantityByBatch' => $pavVaucher->paymentVoucherProductQuantityByBatch,
            'productSerial' => $pavVaucher->productSerial,
            'productBatch' => $pavVaucher->productBatch,
            'batchSerialData' => $pavVaucher->batchSerialData,
            'paymentVoucherProductDiscount' => $pavVaucher->paymentVoucherProductDiscount,
            'paymentVoucherProductExpenseTypeId' => $pavVaucher->paymentVoucherProductExpenseTypeId,
            'uomID' => $pavVaucher->uomID,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('paymentVoucherProductID');
        return $insertedID;
    }

    /**
    * get payment voucher product details by given pv id
    *
    */
    public function getPaymentVoucherproductDetailsById($pvID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('paymentVoucherProduct');
        $select->where(['paymentVoucherProduct.paymentVoucherID' => $pvID]);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

}
