<?php

namespace Expenses\Model;

use Zend\Db\Sql\Expression;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;

/**
 * Class ChequeDepositChequeTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 * 
 * This file contains Cheque Deposit Cheque Table Functions
 */
class ChequeDepositChequeTable {
    
    /**
     * @var TableGateway
     */
    protected $tableGateway;
    
    /**
     * ChequeDepositChequeTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
    
    /**
     * Save cheque deposit cheques
     * @param \Expenses\Model\ChecqueDeposit $checqueDepositCheque
     * @return mixed
     */
    public function saveChequeDeposit($checqueDepositCheque)
    {
        $data = array(
            'incomingPaymentMethodChequeId' => $checqueDepositCheque->incomingPaymentMethodChequeId,
            'chequeDepositId' => $checqueDepositCheque->chequeDepositId,
            'entityId' => $checqueDepositCheque->entityId
        );
        
        if($this->tableGateway->insert($data)){
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Return chequeDepositCheque by given chequeId.
     * @param string $chequeId
     * @return mixed
     */
    public function getChequeDepositChequeByIncomingPaymentMethodChequeId($chequeId){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('chequeDepositCheque')
                ->columns(array('*'))
                ->join('entity', 'chequeDepositCheque.entityId = entity.entityID ', array('deleted'))
                ->where->equalTo('chequeDepositCheque.incomingPaymentMethodChequeId',$chequeId)
                ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Return incomingPaymentMethodCheque ids by given chequeDepositId.
     * @param string $chequeDepositId
     * @return mixed
     */
    public function getIncomingPaymentMethodChequeByChequeDepositId($chequeDepositId) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('chequeDepositCheque')
                ->columns(array('*'))
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = chequeDepositCheque.incomingPaymentMethodChequeId',array('*'))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = incomingPaymentMethod.incomingPaymentMethodChequeId',array('incomingPaymentMethodAmount'))
                ->join('incomingPayment','incomingPaymentMethod.incomingPaymentId = incomingPayment.incomingPaymentID',array('incomingPaymentID','incomingPaymentCode','incomingPaymentDate','incomingPaymentAmount','customCurrencyId','incomingPaymentCustomCurrencyRate'))
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID ', array('currencySymbol'))
                ->join('customer', 'incomingPayment.customerID = customer.customerID ', array('customerID','customerName'))
                ->join('entity', 'chequeDepositCheque.entityId = entity.entityID ', array('deleted'))
                ->where->equalTo('chequeDepositCheque.chequeDepositId',$chequeDepositId)
                ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
    * return chequeDetails with customer and payment details for given deposit id and bouncedcheque ids
    * @param int $chequeDepositId
    * @param array $bouncedChequeIds
    * return mix
    **/ 
    public function getIncomingChequeDetailsByChequeDepositIdAndChequeMethodID($chequeDepositId, $bouncedChequeIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('chequeDepositCheque')
                ->columns(array('*'))
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = chequeDepositCheque.incomingPaymentMethodChequeId',array('*'))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = incomingPaymentMethod.incomingPaymentMethodChequeId',array('incomingPaymentMethodAmount'))
                ->join('incomingPayment','incomingPaymentMethod.incomingPaymentId = incomingPayment.incomingPaymentID',array('incomingPaymentID','incomingPaymentCode','incomingPaymentDate','incomingPaymentAmount','customCurrencyId','incomingPaymentCustomCurrencyRate'))
                ->join('customer', 'incomingPayment.customerID = customer.customerID ', array('customerID','customerName','customerReceviableAccountID','customerAdvancePaymentAccountID'))
                ->where->equalTo('chequeDepositCheque.chequeDepositId',$chequeDepositId)
                ->where->in('incomingPaymentMethodCheque.incomingPaymentMethodChequeId',$bouncedChequeIds);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
