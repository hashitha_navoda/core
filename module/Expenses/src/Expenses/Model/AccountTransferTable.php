<?php
namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

/**
 * Class AccountTransferTable
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 *
 * This file contains Account Transfer Table Functions
 */
class AccountTransferTable
{
    /**
     * @var TableGateway
     */
    protected $tableGateway;

    /**
     * AccountTransferTable constructor.
     * @param TableGateway $tableGateway
     */
    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Create new account transfer
     * @param \Expenses\Model\AccountTransfer $accountTransfer
     * @return mixed
     */
    public function saveTransfer(AccountTransfer $accountTransfer)
    {
        $data = array(
            // 'accountTransferIssuedAccountId' => $accountTransfer->accountTransferIssuedAccountId,
            // 'accountTransferReceivedAccountId' => $accountTransfer->accountTransferReceivedAccountId,
            'accountTransferDate' => $accountTransfer->accountTransferDate,
            'accountTransferAmount' => $accountTransfer->accountTransferAmount,
            // 'accountTransferIssuedAccountCurrencyRate' => $accountTransfer->accountTransferIssuedAccountCurrencyRate,
            // 'accountTransferReceivedAccountCurrencyRate' => $accountTransfer->accountTransferReceivedAccountCurrencyRate,
            'accountTransferComment' => $accountTransfer->accountTransferComment,
            'accountTransferIssuedFinanceAccountId' => $accountTransfer->accountTransferIssuedFinanceAccountId,
            'accountTransferRecivedFinanceAccountId' => $accountTransfer->accountTransferRecivedFinanceAccountId,
            'entityId' => $accountTransfer->entityId
        );

        if ($this->tableGateway->insert($data)) {
            return $this->tableGateway->lastInsertValue;
        } else {
            return FALSE;
        }
    }

    /**
     * Get account transfers
     * @param int $accountId
     * @param date $fromDate
     * @param date $toDate
     * @return mixed
     */
    public function getAccountTransfersByAccountId( $accountId = null, $fromDate = null, $toDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountTransfer')
                ->columns(array('*'))
                ->join('entity', 'accountTransfer.entityId = entity.entityID', array('deleted'))
                ->join('financeAccounts', 'accountTransfer.accountTransferIssuedFinanceAccountId = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join(array('RfinanceAccounts'=>'financeAccounts'), 'accountTransfer.accountTransferRecivedFinanceAccountId = RfinanceAccounts.financeAccountsID', array('recivedFinanceAccountsCode'=>'financeAccountsCode','recivedFinanceAccountsName'=>'financeAccountsName'))
                ->where->equalTo('entity.deleted', 0);
        if($accountId){
            $select->where->equalTo('accountTransfer.accountTransferReceivedAccountId', $accountId);
        }        
        if ($fromDate && $toDate) {
            $select->where->between('accountTransfer.accountTransferDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    
    /**
     * Get incoming account transfers
     * @param int $accountId
     * @param date $fromDate
     * @param date $toDate
     * @return mixed
     */
    public function getIncomingBankTransfersByAccountId($accountId = null, $fromDate = null, $toDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountTransfer')
                ->columns(array('*'))
                ->join('entity', 'accountTransfer.entityId = entity.entityID', array('deleted'))->where->equalTo('entity.deleted', 0);
        if($accountId){
            $select->where->equalTo('accountTransfer.accountTransferRecivedFinanceAccountId', $accountId);
        }
        if ($fromDate && $toDate) {
            $select->where->between('accountTransfer.accountTransferDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * Get outgoing account transfers
     * @param int $accountId
     * @param date $fromDate
     * @param date $toDate
     * @return mixed
     */
    public function getOutgoingBankTransfersByAccountId($accountId = null, $fromDate = null, $toDate = null) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountTransfer')
                ->columns(array('*'))
                ->join('entity', 'accountTransfer.entityId = entity.entityID', array('deleted'))
                ->where->equalTo('entity.deleted', 0);
        if($accountId){
            $select->where->equalTo('accountTransfer.accountTransferIssuedFinanceAccountId', $accountId);
        }
        if ($fromDate && $toDate) {
            $select->where->between('accountTransfer.accountTransferDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * Get account transfer details by accountTransferId
     * @param int $transferId
     * @return mixed
     */
    public function getTransferByTransferId($transferId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('accountTransfer')
                ->columns(array('*'))
                ->join('entity', 'accountTransfer.entityId = entity.entityID', array('deleted'))
                ->join('financeAccounts', 'accountTransfer.accountTransferIssuedFinanceAccountId = financeAccounts.financeAccountsID', array('issuedFinanceAccountsCode'=>'financeAccountsCode','issuedFinanceAccountsName'=>'financeAccountsName'))
                ->join(array('RfinanceAccounts'=>'financeAccounts'), 'accountTransfer.accountTransferRecivedFinanceAccountId = RfinanceAccounts.financeAccountsID', array('recivedFinanceAccountsCode'=>'financeAccountsCode','recivedFinanceAccountsName'=>'financeAccountsName'))
                ->where(array('accountTransfer.accountTransferId' => $transferId,'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if($result->count() == 0){
            return NULL;
        } else {
            return $result->current();
        }
    }    
}
