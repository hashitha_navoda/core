<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class PaymentVoucherNonItemProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveNonItemPayment(PaymentVoucherNonItemProduct $pavVaucher)
    {
        $data = array(
            'paymentVoucherID' => $pavVaucher->paymentVoucherID,
            'paymentVoucherNonItemProductDiscription' => $pavVaucher->paymentVoucherNonItemProductDiscription,
            'paymentVoucherNonItemProductQuantity' => $pavVaucher->paymentVoucherNonItemProductQuantity,
            'paymentVoucherNonItemProductUnitPrice' => $pavVaucher->paymentVoucherNonItemProductUnitPrice,
            'paymentVoucherNonItemProductTotalPrice' => $pavVaucher->paymentVoucherNonItemProductTotalPrice,
            'paymentVoucherNonItemProductExpenseTypeId' => $pavVaucher->paymentVoucherNonItemProductExpenseTypeId,
            'paymentVoucherNonItemProductExpenseTypeFormat' => $pavVaucher->paymentVoucherNonItemProductExpenseTypeFormat,
        );
        $this->tableGateway->insert($data);
        $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue('paymentVoucherNonItemProductID');
        return $insertedID;
    }

}
