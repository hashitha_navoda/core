<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class BankBranch
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class BankBranch implements InputFilterAwareInterface{
    
    /**
     * @var int Auto incremented bank branch Id. (Primary key)
     */
    public $bankBranchId;
    
    /**
     * @var string bank id.
     */
    public $bankId;
    
    /**
     * @var string bank branch name.
     */
    public $bankBranchName;
    
    /**
     * @var string bank branch address.
     */
    public $bankBranchAddress;
    
    /**
     * @var string bank branch code.
     */
    public $bankBranchCode;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->bankBranchId = (isset($data['bankBranchId'])) ? $data['bankBranchId'] : null;
        $this->bankId = (isset($data['bankId'])) ? $data['bankId'] : null;
        $this->bankBranchName = (isset($data['bankBranchName'])) ? $data['bankBranchName'] : null;
        $this->bankBranchAddress = (isset($data['bankBranchAddress'])) ? $data['bankBranchAddress'] : null;
        $this->bankBranchCode = (isset($data['bankBranchCode'])) ? $data['bankBranchCode'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }

    public function getInputFilter() {
        if (!$this->inputFilter) {

            $inputFilter = new InputFilter();
            $factory = new Factory();

            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'bankBranchCode',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
                    )
                )
            );
            
            $inputFilter->add(
                $factory->createInput(
                    array(
                        'name' => 'bankBranchName',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
                    )
                )
            );

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function setInputFilter(InputFilterInterface $inputFilter) {
        throw new \Exception("Not used");
    }

}
