<?php
namespace Expenses\Model;

/**
 * Class AccountType
 * @package Expenses\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class AccountType {
    
    /**
     * @var int Auto incremented account type Id. (Primary key)
     */
    public $accountTypeId;
    
    /**
     * @var string name of the account type.
     */
    public $accountTypeName;
    
    /**
     * @var int entity id.
     */
    public $entityId;
    
    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->accountTypeId = (isset($data['accountTypeId'])) ? $data['accountTypeId'] : null;
        $this->accountTypeName = (isset($data['accountTypeName'])) ? $data['accountTypeName'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }
}
