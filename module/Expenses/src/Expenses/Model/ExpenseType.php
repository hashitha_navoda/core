<?php

namespace Expenses\Model;

use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory;

/**
 * Class Expense Type
 * @package Expenses\Model
 * @author Damith Thamara <damith@thinkcube.com>
 */
class ExpenseType implements InputFilterAwareInterface
{

    public $expenseTypeId;
    public $expenseTypeName;
    public $expenseTypeCategory;
    public $expenseTypeApproverEnabled;
    public $expenseTypeStatus;
    public $expenseTypeAccountID;
    public $entityId;
    protected $inputFilter;

    /**
     * Exchange array to map data with the object.
     * @param $data
     */
    public function exchangeArray($data)
    {
        $this->expenseTypeId = (isset($data['expenseTypeId'])) ? $data['expenseTypeId'] : null;
        $this->expenseTypeName = (isset($data['expenseTypeName'])) ? $data['expenseTypeName'] : null;
        $this->expenseTypeCategory = (isset($data['expenseTypeCategory'])) ? $data['expenseTypeCategory'] : null;
        $this->expenseTypeApproverEnabled = (isset($data['expenseTypeApproverEnabled'])) ? $data['expenseTypeApproverEnabled'] : 0;
        $this->expenseTypeStatus = (isset($data['expenseTypeStatus'])) ? $data['expenseTypeStatus'] : null;
        $this->expenseTypeAccountID = (isset($data['expenseTypeAccountID'])) ? $data['expenseTypeAccountID'] : null;
        $this->entityId = (isset($data['entityId'])) ? $data['entityId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

}
