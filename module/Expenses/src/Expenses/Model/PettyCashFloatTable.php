<?php

namespace Expenses\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;

/**
 * Description of PettyCashFloatTable
 *
 * @author sharmilan <sharmilan@thinkcube.com>
 */
class PettyCashFloatTable
{

    protected $tablegateway;

    public function __construct(TableGateway $tablegateway)
    {
        $this->tablegateway = $tablegateway;
    }

    public function fetchAll($locationID, $paginated = false)
    {
        $adapter = $this->tablegateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashFloat');
        $select->order(array('pettyCashFloatID' => 'DESC'));
        $select->join('account', 'pettyCashFloat.accountID = account.accountID', array('accountName'), 'left');
        $select->join('currency', 'account.currencyId = currency.currencyID', array("currencyName", "currencySymbol"), 'left');
        $select->join('entity', 'pettyCashFloat.entityID = entity.entityID', array("deleted"), 'left');
        $select->where(['pettyCashFloat.locationID' => $locationID,'entity.deleted' => 0]);
        if ($paginated) {
            $paginateAdapter = new DbSelect($select, $adapter);
            return new Paginator($paginateAdapter);
        }
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function save(PettyCashFloat $pettyCashFloat)
    {
        $data = [
            'pettyCashFloatAmount' => $pettyCashFloat->pettyCashFloatAmount,
            'pettyCashFloatDate' => $pettyCashFloat->pettyCashFloatDate,
            'pettyCashFloatStatus' => $pettyCashFloat->pettyCashFloatStatus,
            'pettyCashFloatFinanceAccountID' => $pettyCashFloat->pettyCashFloatFinanceAccountID,
            'pettyCashFloatIssueFinanceAccountID' => $pettyCashFloat->pettyCashFloatIssueFinanceAccountID,
            'locationID' => $pettyCashFloat->locationID,
            'entityID' => $pettyCashFloat->entityID,
        ];
        $this->tablegateway->insert($data);
        $insertedID = $this->tablegateway->adapter->getDriver()->getLastGeneratedValue();
        return $insertedID;
    }

    public function updatePettyCashFloatBalance($amount, $locationID)
    {
        $adapter = $this->tablegateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashFloat');
        $select->limit(1);
        $select->order(['pettyCashFloatID' => 'DESC']);
        $select->where(['locationID' => $locationID]);
        $statment = $sql->prepareStatementForSqlObject($select);
        $result = $statment->execute()->current();

        return $this->tablegateway->update(['pettyCashFloatBalance' => $amount], ['pettyCashFloatID' => $result['pettyCashFloatID']]);
    }

    public function getCurrentPettyCashFloatBalance($locationID)
    {
        $adapter = $this->tablegateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashFloat');
        $select->columns(array('pettyCashFloatBalance'));
        $select->limit(1);
        $select->order(['pettyCashFloatID' => 'DESC']);
        $select->where(['locationID' => $locationID]);
        $statment = $sql->prepareStatementForSqlObject($select);
        $result = $statment->execute();

        $pettyCashFloatBalance = $result->current();
        if ($pettyCashFloatBalance) {
            return $pettyCashFloatBalance['pettyCashFloatBalance'];
        } else {
            return 0;
        }
    }

    public function getPettyCashFloatByID($pettyCashFoatID)
    {
        return $this->tablegateway->select(array('pettyCashFloatID' => $pettyCashFoatID));
    }

    public function updateStatus($pettyCashFloatID, $status)
    {
        return $this->tablegateway->update(['pettyCashFloatStatus' => $status], ['pettyCashFloatID' => $pettyCashFloatID]);
    }

    public function getActivePettyCashFloatByAccountID()
    {
        return $this->tablegateway->select(['cashType' => '1', 'pettyCashFloatStatus' => '1']);
    }

    /**
     * Get petty cash by cash type for given account
     * @param int $cashType
     * @param string $fromDate
     * @param string $toDate
     * @return mixed
     */
    public function getPettyCashFloatByCashType($cashType, $accountId, $fromDate = null, $toDate = null)
    {
        $adapter = $this->tablegateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashFloat')
                ->columns(array('*'))
                ->join('entity', 'pettyCashFloat.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('pettyCashFloat.accountID', $accountId)
        ->where->equalTo('pettyCashFloat.cashType', $cashType);
        if ($fromDate && $toDate) {
            $select->where->between('pettyCashFloat.pettyCashFloatDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get Active Petty cash floats for dashboard cashoutflow graph
     * @author sharmilan  <sharmilan@thinkcube.com>
     * @param type $cashType
     * @return type
     */
    public function getActivePettyCashFloats($fromData, $toData, $format = NULL)
    {
        $groupArray = array();
        if ($format == 'year') {
            $groupArray = array('Year');
        } else if ($format == 'month') {
            $groupArray = array('Month', 'Year');
        } else {
            $groupArray = array('Date', 'Month', 'Year');
        }

        $adapter = $this->tablegateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('pettyCashFloat')
                ->columns(array(
                    'Month' => new Expression('DATE_FORMAT(`pettyCashFloatDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`pettyCashFloatDate`, "%M %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`pettyCashFloatDate`, "%d")'),
                    'Year' => new Expression('Year(pettyCashFloatDate)'),
                    'TotalCashOutFlow' => new Expression('SUM(pettyCashFloatAmount)')
                ))
                ->join('entity', 'pettyCashFloat.entityID = entity.entityID', array('deleted'))
                ->group($groupArray)
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('pettyCashFloat.cashType', '1')
        ->where->between('pettyCashFloatDate', $fromData, $toData);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

     public function getPettyCasshFloatByPettyCasshFloatIDForRec($pettyCashFloatID, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('pettyCashFloat')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "pettyCashFloat.entityID = e.entityID")
                    ->where(array('pettyCashFloat.pettyCashFloatID' => $pettyCashFloatID));
                $select->where(array('e.deleted' => '0'));
            
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

}
