<?php

/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Expenses;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\ResultSet\ResultSet;

class Module
{

    public function onBootstrap(MvcEvent $e)
    {
        $eventManager = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

        $config = $this->getConfig();
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                __DIR__ . '/autoload_classmap.php',
            ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

    public function getServiceConfig()
    {
        return array(
            'aliases' => array(
                'PaymentVoucherTable' => 'Expenses\Model\PaymentVoucherTable',
                'ChequeDepositTable' => 'Expenses\Model\ChequeDepositTable',
                'AccountTable' => 'Expenses\Model\AccountTable',
                'CardTypeTable' => 'Expenses\Model\CardTypeTable',
            ),
            'factories' => array(
                'Expenses\Model\BankTable' => function($sm) {
                    $tableGateway = $sm->get('BankTableGateway');
                    $table = new Model\BankTable($tableGateway);
                    return $table;
                },
                'BankTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Bank());
                    return new TableGateway('bank', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\AccountTypeTable' => function($sm) {
                    $tableGateway = $sm->get('AccountTypeTableGateway');
                    $table = new Model\AccountTypeTable($tableGateway);
                    return $table;
                },
                'AccountTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\AccountType());
                    return new TableGateway('accountType', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ChequePrintTable' => function($sm) {
                    $tableGateway = $sm->get('ChequePrintTableGateway');
                    $table = new Model\ChequePrintTable($tableGateway);
                    return $table;
                },
                'ChequePrintTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ChequePrint());
                    return new TableGateway('chequePrint', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\AccountTable' => function($sm) {
                    $tableGateway = $sm->get('AccountTableGateway');
                    $table = new Model\AccountTable($tableGateway);
                    return $table;
                },
                'AccountTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Account());
                    return new TableGateway('account', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\BankBranchTable' => function($sm) {
                    $tableGateway = $sm->get('BankBranchTableGateway');
                    $table = new Model\BankBranchTable($tableGateway);
                    return $table;
                },
                'BankBranchTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\BankBranch());
                    return new TableGateway('bankBranch', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\PaymentVoucherTable' => function($sm) {
                    $tableGateway = $sm->get('PaymentVoucherTableGateway');
                    $table = new Model\PaymentVoucherTable($tableGateway);
                    return $table;
                },
                'PaymentVoucherTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\PaymentVoucher());
                    return new TableGateway('paymentVoucher', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\PaymentVoucherProductTable' => function($sm) {
                    $tableGateway = $sm->get('PaymentVoucherProductTableGateway');
                    $table = new Model\PaymentVoucherProductTable($tableGateway);
                    return $table;
                },
                'PaymentVoucherProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\PaymentVoucherProduct());
                    return new TableGateway('paymentVoucherProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\PaymentVoucherNonItemProductTable' => function($sm) {
                    $tableGateway = $sm->get('PaymentVoucherNonItemProductTableGateway');
                    $table = new Model\PaymentVoucherNonItemProductTable($tableGateway);
                    return $table;
                },
                'PaymentVoucherNonItemProductTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\PaymentVoucherNonItemProduct());
                    return new TableGateway('paymentVoucherNonItemProduct', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\PaymentVoucherProductTaxTable' => function($sm) {
                    $tableGateway = $sm->get('PaymentVoucherProductTaxTableGateway');
                    $table = new Model\PaymentVoucherProductTaxTable($tableGateway);
                    return $table;
                },
                'PaymentVoucherProductTaxTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\PaymentVoucherProductTax());
                    return new TableGateway('paymentVoucherProductTax', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ChequeDepositTable' => function($sm) {
                    $tableGateway = $sm->get('ChequeDepositTableGateway');
                    $table = new Model\ChequeDepositTable($tableGateway);
                    return $table;
                },
                'ChequeDepositTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ChequeDeposit());
                    return new TableGateway('chequeDeposit', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ChequeDepositCancellationTable' => function($sm) {
                    $tableGateway = $sm->get('ChequeDepositCancellationTableGateway');
                    $table = new Model\ChequeDepositCancellationTable($tableGateway);
                    return $table;
                },
                'ChequeDepositCancellationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ChequeDepositCancellation());
                    return new TableGateway('chequeDepositCancellation', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ChequeDepositCancellationChequeTable' => function($sm) {
                    $tableGateway = $sm->get('ChequeDepositCancellationChequeTableGateway');
                    $table = new Model\ChequeDepositCancellationChequeTable($tableGateway);
                    return $table;
                },
                'ChequeDepositCancellationChequeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ChequeDepositCancellationCheque());
                    return new TableGateway('chequeDepositCancellationCheque', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ReconciliationTable' => function($sm) {
                    $tableGateway = $sm->get('ReconciliationTableGateway');
                    $table = new Model\ReconciliationTable($tableGateway);
                    return $table;
                },
                'ReconciliationTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\Reconciliation());
                    return new TableGateway('reconciliation', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ReconciliationTransactionTable' => function($sm) {
                    $tableGateway = $sm->get('ReconciliationTransactionTableGateway');
                    $table = new Model\ReconciliationTransactionTable($tableGateway);
                    return $table;
                },
                'ReconciliationTransactionTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ReconciliationTransaction());
                    return new TableGateway('reconciliationTransaction', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ChequeDepositChequeTable' => function($sm) {
                    $tableGateway = $sm->get('ChequeDepositChequeTableGateway');
                    $table = new Model\ChequeDepositChequeTable($tableGateway);
                    return $table;
                },
                'ChequeDepositChequeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ChequeDepositCheque());
                    return new TableGateway('chequeDepositCheque', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\PettyCashExpenseTypeTable' => function($sm) {
                    $tableGateway = $sm->get('PettyCashExpenseTypeTableGateway');
                    $table = new Model\PettyCashExpenseTypeTable($tableGateway);
                    return $table;
                },
                'PettyCashExpenseTypeTableGateway' => function($sm) {
                    $dbAdaptor = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetProtoType = new ResultSet();
                    $resultSetProtoType->setArrayObjectPrototype(new Model\PettyCashExpenseType());
                    return new TableGateway('pettyCashExpenseType', $dbAdaptor, null, $resultSetProtoType);
                },
                'Expenses\Model\PettyCashFloatTable' => function($sm) {
                    $tableGateway = $sm->get('PettyCashFloatTableGateway');
                    $table = new Model\PettyCashFloatTable($tableGateway);
                    return $table;
                },
                'PettyCashFloatTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetProtoType = new ResultSet();
                    $resultSetProtoType->setArrayObjectPrototype(new Model\PettyCashFloat());
                    return new TableGateway('pettyCashFloat', $dbAdapter, null, $resultSetProtoType);
                },
                'Expenses\Model\PettyCashVoucherTable' => function($sm) {
                    $tableGateway = $sm->get('PettyCashVoucherGateway');
                    return new Model\PettyCashVoucherTable($tableGateway);
                },
                'PettyCashVoucherGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetProtoType = new ResultSet();
                    $resultSetProtoType->setArrayObjectPrototype(new Model\PettyCashVoucher());
                    return new TableGateway('pettyCashVoucher', $dbAdapter, null, $resultSetProtoType);
                },
                'Expenses\Model\ExpenseCategoryTable' => function($sm) {
                    $tableGateway = $sm->get('ExpenseCategoryTableGateway');
                    $table = new Model\ExpenseCategoryTable($tableGateway);
                    return $table;
                },
                'ExpenseCategoryTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ExpenseCategory());
                    return new TableGateway('expenseCategory', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ExpenseTypeTable' => function($sm) {
                    $tableGateway = $sm->get('ExpenseTypeTableGateway');
                    $table = new Model\ExpenseTypeTable($tableGateway);
                    return $table;
                },
                'ExpenseTypeTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ExpenseType());
                    return new TableGateway('expenseType', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ExpenseTypeLimitTable' => function($sm) {
                    $tableGateway = $sm->get('ExpenseTypeLimitTableGateway');
                    $table = new Model\ExpenseTypeLimitTable($tableGateway);
                    return $table;
                },
                'ExpenseTypeLimitTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ExpenseTypeLimit());
                    return new TableGateway('expenseTypeLimit', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\ExpenseTypeLimitApproverTable' => function($sm) {
                    $tableGateway = $sm->get('ExpenseTypeLimitApproverTableGateway');
                    $table = new Model\ExpenseTypeLimitApproverTable($tableGateway);
                    return $table;
                },
                'ExpenseTypeLimitApproverTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\ExpenseTypeLimitApprover());
                    return new TableGateway('expenseTypeLimitApprover', $dbAdapter, null, $resultSetPrototype);
                },
                'AccountWithdrawalTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\AccountWithdrawal());
                    return new TableGateway('accountWithdrawal', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\AccountWithdrawalTable' => function($sm) {
                    $tableGateway = $sm->get('AccountWithdrawalTableGateway');
                    $table = new Model\AccountWithdrawalTable($tableGateway);
                    return $table;
                },
                'AccountDepositTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\AccountDeposit());
                    return new TableGateway('accountDeposit', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\AccountDepositTable' => function($sm) {
                    $tableGateway = $sm->get('AccountDepositTableGateway');
                    $table = new Model\AccountDepositTable($tableGateway);
                    return $table;
                },
                'AccountTransferTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\AccountTransfer());
                    return new TableGateway('accountTransfer', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\AccountTransferTable' => function($sm) {
                    $tableGateway = $sm->get('AccountTransferTableGateway');
                    $table = new Model\AccountTransferTable($tableGateway);
                    return $table;
                },
                'CashInHandWithdrawalTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\CashInHandWithdrawal());
                    return new TableGateway('cashInHandWithdrawal', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\CashInHandWithdrawalTable' => function($sm) {
                    $tableGateway = $sm->get('CashInHandWithdrawalTableGateway');
                    $table = new Model\CashInHandWithdrawalTable($tableGateway);
                    return $table;
                },
                'CashInHandDepositTableGateway' => function ($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetPrototype = new ResultSet();
                    $resultSetPrototype->setArrayObjectPrototype(new Model\CashInHandDeposit());
                    return new TableGateway('cashInHandDeposit', $dbAdapter, null, $resultSetPrototype);
                },
                'Expenses\Model\CashInHandDepositTable' => function($sm) {
                    $tableGateway = $sm->get('CashInHandDepositTableGateway');
                    $table = new Model\CashInHandDepositTable($tableGateway);
                    return $table;
                },
                'CardTypeTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetProtoType = new ResultSet();
                    $resultSetProtoType->setArrayObjectPrototype(new Model\CardType());
                    return new TableGateway('cardType', $dbAdapter, null, $resultSetProtoType);
                },
                'Expenses\Model\CardTypeTable' => function($sm) {
                    $tableGateway = $sm->get('CardTypeTableGateway');
                    return new Model\CardTypeTable($tableGateway);
                },
                'Expenses\Model\PettyCashFloatDefaultAccountsTable' => function($sm) {
                    $tableGateway = $sm->get('PettyCashFloatDefaultAccountsTableGateway');
                    return new Model\PettyCashFloatDefaultAccountsTable($tableGateway);
                },
                 'PettyCashFloatDefaultAccountsTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetProtoType = new ResultSet();
                    $resultSetProtoType->setArrayObjectPrototype(new Model\PettyCashFloatDefaultAccounts());
                    return new TableGateway('pettyCashFloatDefaultAccounts', $dbAdapter, null, $resultSetProtoType);
                },
                'Expenses\Model\PettyCashVoucherDefaultAccountsTable' => function($sm) {
                    $tableGateway = $sm->get('PettyCashVoucherDefaultAccountsTableGateway');
                    return new Model\PettyCashVoucherDefaultAccountsTable($tableGateway);
                },
                 'PettyCashVoucherDefaultAccountsTableGateway' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $resultSetProtoType = new ResultSet();
                    $resultSetProtoType->setArrayObjectPrototype(new Model\PettyCashVoucherDefaultAccounts());
                    return new TableGateway('pettyCashVoucherDefaultAccounts', $dbAdapter, null, $resultSetProtoType);
                },
            )
        );
    }

}
