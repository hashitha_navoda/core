<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * this file contains customer import form
 */

namespace Invoice\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class DataimportForm extends Form
{

    public function __construct($name = null)
    {
        parent::__construct('Dataimport');
        $this->setAttribute('method', 'post');
        $this->setAttribute('enctype', 'multipart/form-data');
        $this->add(array(
            'name' => 'fileupload',
            'attributes' => array(
                'type' => 'file',
                'value' => 'browse',
                'class' => 'file',
                'id' => 'fileupload',
            ),
        ));
        $this->add(array(
            'name' => 'characterencoding',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'characterencoding'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => 'UTF-8',
                    '1' => 'ISO 8859-1'
                ),
            )
        ));
        $this->add(array(
            'name' => 'delimiter',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'delimiter'
            ),
            'options' => array(
                'value_options' => array(
                    '0' => ', (Comma)',
                    '1' => '; (semi-colon)',
                    '2' => '. (full stop)',
                ),
            )
        ));
        $this->add(array(
            'name' => 'header',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => TRUE,
                'checked_value' => '1',
                'unchecked_value' => '0',
            ),
            'attributes' => array(
                'checked' => TRUE,
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'attributes' => array(
                'type' => 'submit',
                'value' => 'Save changes',
                'id' => 'uploadfile',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}

