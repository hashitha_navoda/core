<?php

namespace Invoice\Form;

use Zend\Form\Form;

class DeliveryNoteForm extends Form
{

    public function __construct($data)
    {

        $name = 'deliveryNoteForm';
        $terms = $data['paymentTerms'];
        $salesPersons = $data['salesPersons'];
        $customCurrency = $data['customCurrency'];
        $priceList = $data['priceList'];
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $name);

        $this->add(array(
            'name' => 'deliveryNoteNo',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'deliveryNoteNo',
            ),
        ));

        $this->add(array(
            'name' => 'customer',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customer',
            ),
        ));

        $this->add(array(
            'name' => 'customerReference',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customerReference',
                'autocomplete' => 'off'
            ),
        ));


        $this->add(array(
            'name' => 'paymentTerm',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'paymentTerm'
            ),
            'options' => array(
                'value_options' => $terms,
            )
        ));

        $this->add(array(
            'name' => 'currentLocation',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'currentLocation',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'deliveryDate',
            'type' => 'date',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'deliveryDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));

        $this->add(array(
            'name' => 'itemCode',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemCode',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'itemName',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemName',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'itemDescription',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemDescription'
            ),
        ));

        $this->add(array(
            'name' => 'availableQuantity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'availableQuantity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'deliverQuanity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'deliverQuanity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'unitPrice',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'unitPrice',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'discount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'discount',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'deliveryCharge',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'deliveryCharge',
                'maxlength' => 10,
            ),
        ));

        $this->add(array(
            'name' => 'deliveryChargeEnable',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'deliveryChargeEnable',
            ),
        ));

        $this->add(array(
            'name' => 'showTax',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'showTax',
            ),
        ));

        $this->add(array(
            'name' => 'comment',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'comment',
            ),
        ));

        $this->add(array(
            'name' => 'deliveryNoteAddress',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'deliveryNoteAddress',
            ),
        ));
        $this->add(array(
            'name' => 'salesPersonID',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'salesPersonID',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => ' ',
                'disable_inarray_validator' => true,
                'value_options' => isset($salesPersons) ? $salesPersons : null,
            )
        ));

        $this->add(array(
            'name' => 'customCurrencyId',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'customCurrencyId',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'disable_inarray_validator' => true,
                'value_options' => isset($customCurrency) ? $customCurrency : null,
            )
        ));
        $this->add(array(
            'name' => 'priceListId',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'priceListId',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => ' ',
                'disable_inarray_validator' => true,
                'value_options' => isset($priceList) ? $priceList : null,
            )
        ));
        $this->add(array(
            'name' => 'deliveryNoteSaveButton',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'deliveryNoteSave',
                'class' => 'btn btn-primary'
            ),
        ));
        $this->add(array(
            'name' => 'deliveryNoteCancelButton',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Cancel'),
                'id' => 'deliveryNoteCancel',
                'class' => 'btn btn-default'
            ),
        ));
    }

}

?>
