<?php
namespace Invoice\Form;

use Zend\Form\Form;

class DispatchNoteForm extends Form
{
    public function __construct( $dispatchNoteCode, $salesPersons)
    {
        $name = 'dispatchNoteForm';
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $name);

        $this->add(array(
            'name' => 'dispatchNoteId',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'dispatchNoteId',
            ),
        ));
        
        $this->add(array(
            'name' => 'dispatchNoteCode',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'dispatchNoteCode',
                'disabled' => 'disabled',
                'value' => $dispatchNoteCode
            ),
        ));
        
        $this->add(array(
            'name' => 'dispatchNoteDate',
            'type' => 'date',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'dispatchNoteDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));
        
        $this->add(array(
            'name' => 'dispatchNoteAddress',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'dispatchNoteAddress',
            ),
        ));

        $this->add(array(
            'name' => 'dispatchNoteComment',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'dispatchNoteComment',
            ),
        ));
        
        $this->add(array(
            'name' => 'salesPersonId',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'salesPersonId',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => '',
                'disable_inarray_validator' => true,
                'value_options' => isset($salesPersons) ? $salesPersons : null,
            )
        ));
        
        $this->add(array(
            'name' => 'customer',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customer',
                'disabled' => 'disabled'
            ),
        ));
        
        $this->add(array(
            'name' => 'location',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'location',
                'disabled' => 'disabled'
            ),
        ));
        
        $this->add(array(
            'name' => 'itemCode',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-left reduce_le_ri_padding itemCode',
                'id' => 'itemCode',
                'disabled' => 'disabled',
                'autocomplete' => 'off'
            ),
        ));
        
        $this->add(array(
            'name' => 'invoiceQuantity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding invoiceQuantity',
                'id' => 'invoiceQuantity',
                'disabled' => 'disabled',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'dispatchQuanity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding dispatchQuanity',
                'id' => 'dispatchQuanity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'dispatchNoteSaveButton',
            'attributes' => array(
                'type' => 'button',
                'value' => _('Save'),
                'id' => 'dispatchNoteSaveButton',
                'class' => 'btn btn-primary',
                'disabled' => true
            ),
        ));
        
        $this->add(array(
            'name' => 'dispatchNoteCancelButton',
            'attributes' => array(
                'type' => 'button',
                'value' => _('Cancel'),
                'id' => 'dispatchNoteCancelButton',
                'class' => 'btn btn-default'
            ),
        ));
    }
}
?>
