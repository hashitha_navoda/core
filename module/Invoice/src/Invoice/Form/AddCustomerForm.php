<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * this file contains customer add form.
 */

namespace Invoice\Form;

use Zend\Form\Form;
use Zend\Form\Element;

class AddCustomerForm extends Form
{

    public function __construct($name = NULL, $paymentTerms = NULL, $currency = NULL, $customerTitle = array(), $currencyValue = null, $customerCategories = array(), $userdateFormat = "yyyy-mm-dd", $loyaltyCardList = NULL, $customerEvents = [], $customerPriceLists = [])
    {

        parent::__construct('AddCustomer');
        $this->setAttribute('method', 'post');
        $this->add(array(
            'name' => 'customerID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'customerID'
            ),
        ));
        $this->add(array(
            'type' => 'Select',
            'name' => 'customerTitle',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customerTitle'
            ),
            'options' => array(
                'value_options' => isset($customerTitle) ? $customerTitle : NULL,
            ),
        ));
        $this->add(array(
            'name' => 'customerName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('John Smith'),
                'id' => 'customerName',
                'required' => true
            ),
        ));
        $this->add(array(
            'name' => 'customerShortName',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('jsmith'),
                'id' => 'customerShortName',
                'required' => false
            ),
        ));

        $this->add(array(
            'name' => 'customerCode',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'placeholder' => _('jsmith'),
                'id' => 'customerCode',
                'required' => true,
            ),
        ));

        $this->add(array(
            'name' => 'customerCurrency',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control SelectPicker',
                'id' => 'customerCurrency',
                'style' => '',
            ),
            'options' => array(
                'value_options' => $currency,
            ),
        ));

        $this->add(array(
            'name' => 'customerTelephoneNumber',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'customerTelephoneNumber',
            ),
        ));
        $this->add(array(
            'name' => 'customerAdditionalTelephoneNumber',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'customerAdditionalTelephoneNumber',
            ),
        ));
        $this->add(array(
            'name' => 'customerDateOfBirth',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control datepicker pointer_cursor',
                'id' => 'customerDateOfBirth',
                'readonly' => 'readonly',
                'style' => 'background-color: white',
                'data-date-format' => $userdateFormat,
            ),
        ));
        $this->add(array(
            'name' => 'customerIdentityNumber',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'customerIdentityNumber',
            ),
        ));
        $this->add(array(
            'name' => 'customerCategory',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customerCategory',
            ),
            'options' => array(
                'value_options' => isset($customerCategories) ? $customerCategories : NULL,
            ),
        ));
        $this->add(array(
            'name' => 'customerPriceList',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customerPriceList',
            ),
            'options' => array(
                'value_options' => isset($customerPriceLists) ? $customerPriceLists : NULL,
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'customerLoyaltyNo',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customerLoyaltyNo',
                'value' => 0
            ),
            'options' => array(
                'value_options' => isset($loyaltyCardList) ? $loyaltyCardList : NULL,
            ),
        ));

        $this->add(array(
            'type' => 'Select',
            'name' => 'customerLoyaltyCode',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'customerLoyaltyCode',
                'value' => 0,
                'data-live-search' => true,
            ),
//            'options' => array(
//                'value_options' => isset($loyaltyCardList) ? $loyaltyCardList : NULL,
//            ),
        ));

        $this->add(array(
            'name' => 'customerEmail',
            'attributes' => array(
                'type' => 'email',
                'class' => 'form-control',
                'placeholder' => _('jsmith@example.com'),
                'id' => 'customerEmail'
            ),
        ));
        $this->add(array(
            'name' => 'customerAddress',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => _('Enter current Address'),
                'id' => 'customerAddress'
            ),
        ));
        $this->add(array(
            'name' => 'customerVatNumber',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'customerVatNumber',
                'placeholder' => _('10')
            ),
        ));
        $this->add(array(
            'name' => 'customerSVatNumber',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'customerSVatNumber',
                'placeholder' => _('10')
            ),
        ));
        $this->add(array(
            'name' => 'customerPaymentTerm',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customerPaymentTerm'
            ),
            'options' => array(
                'value_options' => $paymentTerms,
            )
        ));
        $this->add(array(
            'name' => 'customerEvent',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customerEvent'
            ),
            'options' => array(
                'value_options' => $customerEvents,
            )
        ));
        $this->add(array(
            'name' => 'customerCreditLimit',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'customerCreditLimit',
                'placeholder' => _('1000')
            ),
        ));
        $this->add(array(
            'name' => 'customerCurrentBalance',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control customerAdd',
                'id' => 'customerCurrentBalance',
                'placeholder' => _('1000')
            ),
        ));
        $this->add(array(
            'name' => 'customerCurrentCredit',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control customerAdd',
                'id' => 'customerCurrentCredit',
                'placeholder' => _('1000')
            ),
        ));
        $this->add(array(
            'name' => 'customerDiscount',
            'attributes' => array(
                'type' => 'text',
                'class' => 'form-control',
                'id' => 'customerDiscount',
                'placeholder' => _('10')
            ),
        ));
        $this->add(array(
            'name' => 'customerOther',
            'type' => '\Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'placeholder' => _('Enter Further Details'),
                'id' => 'customerOther'
            ),
        ));

        $this->add(array(
            'name' => 'customerFormsubmit',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save changes'),
                'id' => 'customerFormsubmit',
                'class' => 'btn btn-primary'
            ),
        ));
        $this->add(array(
            'name' => 'saveAndAddCustomer',
            'attributes' => array(
                'type' => 'button',
                'value' => _('Save and Add New Customer'),
                'id' => 'saveAndAddCustomer',
                'class' => 'btn btn-primary hidden'
            ),
        ));

        $this->add(array(
            'name' => 'customerFormCancel',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Clear'),
                'id' => 'customerFormCancel',
                'class' => 'btn btn-warning'
            ),
        ));

        $this->add(array(
            'name' => 'customerReceviableAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'customerReceviableAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));

        $this->add(array(
            'name' => 'customerSalesAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'customerSalesAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));

        $this->add(array(
            'name' => 'customerSalesDiscountAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'customerSalesDiscountAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));

        $this->add(array(
            'name' => 'customerAdvancePaymentAccountID',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'customerAdvancePaymentAccountID',
                'class' => 'form-control selectpicker',
                'data-live-search'=>"true"
            ),
            'options' => array(
                'empty_option' => 'select an account',
            )
        ));
    }

    public function customerFormForModal($paymentTerms, $currencies, $customerTitle, $currencyValue, $customerEventList, $customerCode)
    {
       $customerForm = new self(
                                NULL,
                                $paymentTerms, 
                                $currencies, 
                                $customerTitle, 
                                $currencyValue, 
                                [], 
                                "yyyy-mm-dd", 
                                NULL, 
                                $customerEventList
                            );

        $customerForm->get('customerFormsubmit')->setValue(_("Add New Customer"));
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);
        return $customerForm;
    
    }

}
