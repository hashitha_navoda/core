<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains recurrent Sales Order Form.
 */

namespace Invoice\Form;

use Zend\Form\Form;

class RecurrentSalesOrderForm extends Form {

    public function __construct($data = 'null') {

        parent::__construct('company');
        $this->setAttribute('method', 'post');
        $this->setAttribute('onsubmit', 'validate_form();');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'salesOrderID',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control search form-control typeahead',
                'id' => 'salesOrderID',
                'data-provide' => 'typeahead',
                'placeholder' => _('Enter Sales Order No'),
            ),
        ));
        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'name',
                'placeholder' => _('Please Enter Name'),
            ),
        ));
        $this->add(array(
            'name' => 'timeValue',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'timeValue',
                'placeholder' => _('Please Enter Value'),
            ),
        ));
        $this->add(array(
            'name' => 'timeFormat',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'timeFormat',
            ),
            'options' => array(
                'value_options' => array(
                    '' => _('Please select time format'),
                    'days' => 'Days',
                    'weeks' => 'Weeks',
                    'months' => 'Months',
                    'years' => 'Years',
                ),
            )
        ));

        $this->add(array(
            'name' => 'startDate',
            'type' => 'text',
            'attributes' => array(
                'class' => "form-control span2 datepicker form-control",
                'data-date-format' => "yyyy-mm-dd",
                'placeholder' => _("click to select the date"),
                'id' => 'startDate',
            ),
        ));

        $this->add(array(
            'name' => 'endDate',
            'type' => 'text',
            'attributes' => array(
                'class' => "form-control span2 datepicker form-control",
                'data-date-format' => "yyyy-mm-dd",
                'placeholder' => _("click to select the date"),
                'id' => 'endDate',
            ),
        ));
        $this->add(array(
            'name' => 'state',
            'type' => 'text',
            'value' => 'active',
            'attributes' => array(
                'class' => "form-control",
                'id' => 'state',
            ),
        ));
        $this->add(array(
            'name' => 'Type',
            'type' => 'text',
            'attributes' => array(
                'class' => "form-control",
                'id' => 'Type',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submit',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}

?>
