<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains payments related Form fields
 */

namespace Invoice\Form;

use Zend\Form\Form;

class CustomerPaymentsForm extends Form
{

    public function __construct($data = NULL)
    {
        parent::__construct('customerPayments');
        $this->setAttribute('method', 'post');
        $this->setAttribute('onsubmit', 'validate_form();');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        if ($data['customerID']) {
            $custID = $data['customerID'];
            $placeholder = _('Enter Supplier Name');
        } else {
            $custID = "customerID";
            $placeholder = _('Enter Customer Name');
        }
        $customCurrency = $data['customCurrency'];
        $companyCurrencyId = $data['companyCurrencyId'];
        $this->add(array(
            'name' => $custID,
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => $custID,
                'class' => 'form-control customerID',
                'placeholder' => $placeholder,
            ),
        ));
        $this->add(array(
            'name' => 'locationID',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'locationID',
                'class' => 'form-control customerID',
                'placeholder' => _('Please Type location Name'),
            ),
        ));
        $this->add(array(
            'name' => 'paymentTerm',
            'type' => 'select',
            'options' => array(
                'value_options' => $data['paymentTerm'],
            ),
            'attributes' => array(
                'id' => 'paymentTerm',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'currentCredit',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'readonly' => 'readonly',
//                'placeholder' => 'Customer Credit',
                'disable' => 'true',
                'id' => 'currentCredit',
                'class' => 'form-control',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'currentBalance',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'readonly' => 'readonly',
//                'placeholder' => 'Customer Balance',
                'disable' => 'true',
                'id' => 'currentBalance',
                'class' => 'form-control',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'InvoiceID',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => _('Enter Invoice ID'),
                'disable' => 'true',
                'id' => 'InvoiceID',
                'autocomplete' => 'off',
                'class' => 'form-control InvoiceID',
            ),
        ));
        $this->add(array(
            'name' => 'date',
            'type' => 'date',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'date',
            ),
        ));
        $this->add(array(
            'name' => 'discountAmount',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => _('Eg : ' . $data['companyCurrencySymbol'] . ' 100 '),
                'id' => 'discountAmount',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'amount',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'readonly' => 'readonly',
                'placeholder' => _('Eg : ' . $data['companyCurrencySymbol'] . ' 100 '),
                'id' => 'amount',
                'class' => 'form-control',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'memo',
            'type' => 'Textarea',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => _('Click here to add memo'),
                'id' => 'memo',
                'class' => 'form-control',
                'rows' => 3,
            ),
        ));
        $this->add(array(
            'name' => 'paymentMethod',
            'type' => 'select',
            'options' => array(
                'value_options' => $data['paymentMethod'],
            ),
            'attributes' => array(
                'id' => 'paymentMethod',
                'class' => 'form-control paymentMethod',
            ),
        ));
        $this->add(array(
            'name' => 'restToPaid',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => _('Eg : ' . $data['companyCurrencySymbol'] . ' 100 '),
                'id' => 'restToPaid',
                'class' => 'form-control',
                'disabled' => 'disabled',
            ),
        ));
        $this->add(array(
            'name' => 'paidAmount',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => _('Eg : ' . $data['companyCurrencySymbol'] . ' 100 '),
                'id' => 'paidAmount',
                'class' => 'form-control paidAmount',
            ),
        ));
        $this->add(array(
            'name' => 'creditAmount',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => _('Eg : ' . $data['companyCurrencySymbol'] . ' 100 '),
                'id' => 'creditAmount',
                'class' => 'form-control',
            ),
        ));
        $this->add(array(
            'name' => 'bankID',
            'type' => 'select',
            'options' => array(
                'empty_option' => 'Please select the bank',
                'value_options' => $data['banks'],
            ),
            'attributes' => array(
                'id' => 'bankID',
                'class' => 'form-control bankID',
            ),
        ));
        $this->add(array(
            'name' => 'giftCardID',
            'type' => 'select',
            'options' => array(
                'empty_option' => 'Please select the Gift Card',
                'value_options' => $data['giftCards'],
            ),
            'attributes' => array(
                'id' => 'giftCardID',
                'class' => 'form-control giftCardID',
            ),
        ));
        $this->add(array(
            'name' => 'paymentVoucherID',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => _('Enter Payment Voucher ID'),
                'disable' => 'true',
                'id' => 'paymentVoucherID',
                'autocomplete' => 'off',
                'class' => 'form-control paymentVoucherID',
            ),
        ));

        $this->add(array(
            'name' => 'customCurrencyId',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'customCurrencyId',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'disable_inarray_validator' => true,
                'value_options' => isset($customCurrency) ? $customCurrency : null,
            )
        ));
        $this->add(array(
            'name' => 'customCurrencyRate',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'placeholder' => _('Eg : 100 '),
                'id' => 'customCurrencyRate',
                'class' => 'form-control',
            ),
        ));

        $this->add(array(
            'name' => 'save',
            'type' => 'submit',
            'attributes' => array(
                'value' => _('Add Payment'),
                'id' => 'addPayment',
                'class' => 'btn btn-primary',
            ),
        ));
    }

}
