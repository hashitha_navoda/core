<?php

/**
 * @author Prahtap Dileepa <prathap@prathap.com>
 * This file contains Invoice Form. This is the same file use for the Quotaion Form
 */

namespace Invoice\Form;

use Zend\Form\Form;

class QuotationForm extends Form
{

    public function __construct($data)
    {

        $name = isset($data['name']) ? $data['name'] : 'invoice';
        $id = isset($data['id']) ? $data['id'] : 'inv_no';
        $terms = $data['terms'];
        $salesPersons = $data['salesPersons'];
        $jobCardModel = $data['jobCardModel'];
        $customCurrency = $data['customCurrency'];
        $priceList = $data['priceList'];
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $name);
        $this->setAttribute("role", "form");
        $this->add(array(
            'name' => 'inv_no',
            'type' => 'Hidden',
        ));
        $this->add(array(
            'name' => $id,
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => $id,
            ),
        ));

        $this->add(array(
            'name' => 'cust_name',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'cust_name',
                'autocomplete' => false,
                'placeholder' => _('Enter customer name'),
            ),
        ));
        $this->add(array(
            'name' => 'payment_term',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'payment_term'
            ),
            'options' => array(
                'value_options' => $terms,
            )
        ));
        $this->add(array(
            'name' => 'invoicecurrentBalance',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'invoicecurrentBalance',
                'disabled' => 'disabled'
            ),
        ));
        $this->add(array(
            'name' => 'total_discount_rate',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding invoiceTotalDiscountRate',
                'id' => 'total_discount_rate',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'issue_date',
            'type' => 'date',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'issue_date',
            ),
        ));

        $this->add(array(
            'name' => 'salesPersonID',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'salesPersonID',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => ' ',
                'disable_inarray_validator' => true,
                'value_options' => isset($salesPersons) ? $salesPersons : null,
            )
        ));
        $this->add(array(
            'name' => 'jobCardType',
            'type' => 'Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'jobCardType',
                'data-live-search' => true,
                'title' => '',
            ),
            'options' => array(
                'empty_option' => '',
                'value_options' => $jobCardModel,
            ),
        ));
        $this->add(array(
            'name' => 'projectReference',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'projectReference',
            ),
        ));
        $this->add(array(
            'name' => 'jobReference',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'jobReference',
            ),
        ));
        $this->add(array(
            'name' => 'activityReference',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'activityReference',
            ),
        ));


        $this->add(array(
            'name' => $name == "sales_orders_form" ? "completion_date" : 'expire_date',
            'type' => 'date',
            'attributes' => array(
                'class' => 'form-control',
                'id' => $name == "sales_orders_form" ? "completion_date" : 'expire_date',
            ),
        ));

        $this->add(array(
            'name' => 'item_code',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'item_code',
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'item',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'item'
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'qty',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'qty',
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'price',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'price',
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'discount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'discount',
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'deli_amount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'deli_amount',
            //'required' => 'true',
            ),
        ));
        $this->add(array(
            'name' => 'deli_charg',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checked_value' => '1',
                'unchecked_value' => '0',
            ),
            'attributes' => array(
                'id' => 'deli_charg',
            ),
        ));
        $this->add(array(
            'name' => 'show_tax',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checked_value' => '1',
                'unchecked_value' => '0',
            ),
            'attributes' => array(
                'id' => 'show_tax',
            ),
        ));
        $this->add(array(
            'name' => 'deli_address',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'deli_address',
            ),
        ));
        $this->add(array(
            'name' => 'cmnt',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'cmnt',
            ),
        ));
        $this->add(array(
            'name' => 'customCurrencyId',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'customCurrencyId',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'disable_inarray_validator' => true,
                'value_options' => isset($customCurrency) ? $customCurrency : null,
            )
        ));
        $this->add(array(
            'name' => 'priceListId',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'priceListId',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => ' ',
                'disable_inarray_validator' => true,
                'value_options' => isset($priceList) ? $priceList : null,
            )
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submitbutton',
                'class' => 'btn btn-primary'
            ),
        ));
        $this->add(array(
            'name' => 'cash',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right',
                'id' => 'cash',
            //'required' => 'true',
            ),
        ));
    }


    public function getQuotationForm($paymentTerms = null, $salesPersons = null, $jobCardModel = null, $currency = null, $priceList = null, $refNo = null, $currencyValue = null)
    {
        $quot_form = new self(array(
            'name' => 'quatation',
            'id' => 'qot_no',
            'terms' => $paymentTerms,
            'salesPersons' => $salesPersons,
            'jobCardModel' => $jobCardModel,
            'customCurrency' => $currency,
            'priceList' => $priceList
        ));

        if($refNo){
            $quot_form->get('qot_no')->setValue($refNo);
            // $quot_form->get('qot_no')->setAttribute("disabled", TRUE);
        }
        $quot_form->get('customCurrencyId')->setValue($currencyValue);

        return $quot_form;

    }

}

?>
