<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Advance payments related Form fields
 */

namespace Invoice\Form;

use Zend\Form\Form;

class AdvancePaymentsForm extends Form
{

    public function __construct($data = NULL)
    {
        parent::__construct('advancePayments');
        $this->setAttribute('method', 'post');
        $this->setAttribute('onsubmit', 'validate_form();');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        if ($data['custName']) {
            $CustName = $data['custName'];
            $placeholder = _('Please Type Supplier Name');
        } else {
            $CustName = 'advancecustomer';
            $placeholder = _('Please Type Customer Name');
        }
        $customCurrency = $data['customCurrency'];

        $this->add(array(
            'name' => $CustName,
            'type' => 'Text',
            'attributes' => array(
                'id' => $CustName,
                'class' => 'form-control',
                'placeholder' => $placeholder,
            ),
        ));

        $this->add(array(
            'name' => 'advancedate',
            'type' => 'date',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'advancedate',
            ),
        ));
        $this->add(array(
            'name' => 'advancepaymentMethod',
            'type' => 'select',
            'options' => array(
                'empty_option' => 'Select Payment Method',
                'value_options' => $data['paymentMethod'],
            ),
            'attributes' => array(
                'id' => 'advancepaymentMethod',
                'class' => 'form-control advancepaymentMethod',
            ),
        ));
        $this->add(array(
            'name' => 'advancememo',
            'type' => 'Textarea',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'advancememo',
                'class' => 'form-control',
                'placeholder' => _('eg : this is to add memo'),
            ),
        ));
        $this->add(array(
            'name' => 'advanceamount',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'advanceamount',
                'class' => 'form-control advanceamount',
                'placeholder' => _('eg : ' . $data['currencySymbol'] . ' 100 '),
            ),
        ));
        $this->add(array(
            'name' => 'bankID',
            'type' => 'select',
            'options' => array(
                'empty_option' => 'Please select the bank',
                'value_options' => $data['banks'],
            ),
            'attributes' => array(
                'id' => 'bankID',
                'class' => 'form-control bankID',
            ),
        ));
        $this->add(array(
            'name' => 'giftCardID',
            'type' => 'select',
            'options' => array(
                'empty_option' => 'Please select the Gift Card',
                'value_options' => $data['giftCards'],
            ),
            'attributes' => array(
                'id' => 'giftCardID',
                'class' => 'form-control giftCardID',
            ),
        ));
        $this->add(array(
            'name' => 'customCurrencyId',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'customCurrencyId',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => 'Select Currency',
                'disable_inarray_validator' => true,
                'value_options' => isset($customCurrency) ? $customCurrency : null,
            )
        ));
        $this->add(array(
            'name' => 'customCurrencyRate',
            'type' => 'Text',
            'options' => array(
            ),
            'attributes' => array(
                'id' => 'customCurrencyRate',
                'class' => 'form-control',
                'placeholder' => _('eg : 100 '),
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'submit',
            'attributes' => array(
                'value' => 'Add Payment',
                'id' => 'addAdvancePayment',
                'class' => 'btn btn-primary',
            ),
        ));
    }

}
