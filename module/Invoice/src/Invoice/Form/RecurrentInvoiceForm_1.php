<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains recurrent Invoice Form.
 */

namespace Invoice\Form;

use Zend\Form\Form;

class RecurrentInvoiceForm extends Form {

    public function __construct($data = 'null') {

        parent::__construct('company');
        $this->setAttribute('method', 'post');
        $this->setAttribute('onsubmit', 'validate_form();');
        $this->setAttribute('class', "form-horizontal");
        $this->setAttribute('role', "form");
        $this->add(array(
            'name' => 'sid',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control search form-control typeahead',
                'id' => 'sid',
                'data-provide' => 'typeahead',
                'placeholder' => _('Enter SalesOrder ID'),
            ),
        ));

        $this->add(array(
            'name' => 'qoid',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control search form-control typeahead',
                'id' => 'qoid',
                'data-provide' => 'typeahead',
                'placeholder' => _('Enter Quotation ID'),
            ),
        ));
        $this->add(array(
            'name' => 'name',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'name',
            ),
        ));
        $this->add(array(
            'name' => 'timeValue',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'timeValue'
            ),
        ));
        $this->add(array(
            'name' => 'timeFormat',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'timeFormat',
            ),
            'options' => array(
                'value_options' => array(
                    '' => _('please select time format'),
                    'days' => 'days',
                    'weeks' => 'weeks',
                    'months' => 'months',
                    'years' => 'years',
                ),
            )
        ));

        $this->add(array(
            'name' => 'startDate',
            'type' => 'text',
            'attributes' => array(
                'class' => "form-control span2 datepicker form-control",
                'data-date-format' => "yyyy-mm-dd",
                'placeholder' => _("click to select the date"),
                'id' => 'startDate',
            ),
        ));

        $this->add(array(
            'name' => 'endDate',
            'type' => 'text',
            'attributes' => array(
                'class' => "form-control span2 datepicker form-control",
                'data-date-format' => "yyyy-mm-dd",
                'placeholder' => _("click to select the date"),
                'id' => 'endDate',
            ),
        ));
        $this->add(array(
            'name' => 'state',
            'type' => 'text',
            'value' => 'active',
            'attributes' => array(
                'class' => "form-control",
                'id' => 'state',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Go',
                'id' => 'submit',
                'class' => 'btn btn-primary'
            ),
        ));
    }

}

?>
