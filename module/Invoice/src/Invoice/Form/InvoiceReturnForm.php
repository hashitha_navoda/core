<?php

namespace Invoice\Form;

use Zend\Form\Form;

class InvoiceReturnForm extends Form
{

    public function __construct($data)
    {

        $name = 'returnForm';
        $customCurrency = $data['customCurrency'];
        $promotions = $data['promotions'];
        $promo = [];
        foreach ($promotions  as $pro) {
            $checkInvBasePrecentage = false;
            if($pro->promotionType == 2 && $pro->invoicePromotionDiscountType == 2) {
                $checkInvBasePrecentage = true;
            };
            $promo[] = array(
                'label'=> $pro->promotionName,
                'value'=>$pro->promotionID,
                'attributes'=>array(
                    'data-invbaseprecentage'=>$checkInvBasePrecentage,
                ),
            );
        }
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $name);

        $this->add(array(
            'name' => 'invoiceReturnNo',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'invoiceReturnNo',
            ),
        ));

        $this->add(array(
            'name' => 'customer',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customer',
            ),
        ));

        $this->add(array(
            'name' => 'currentLocation',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'currentLocation',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'invoiceReturnDate',
            'type' => 'date',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'invoiceReturnDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));

        $this->add(array(
            'name' => 'itemCode',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemCode',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'itemName',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemName',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'itemDescription',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemDescription'
            ),
        ));

        $this->add(array(
            'name' => 'invoicedQuantity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'invoicedQuantity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'invoiceReturnQuanity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'invoiceReturnQuanity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'unitPrice',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'unitPrice',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'discount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding dicz',
                'id' => 'discount',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'deliveryCharge',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'deliveryCharge',
            ),
        ));

        $this->add(array(
            'name' => 'deliveryChargeEnable',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'deliveryChargeEnable',
            ),
        ));

        $this->add(array(
            'name' => 'showTax',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'showTax',
            ),
        ));

        $this->add(array(
            'name' => 'comment',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'comment',
            ),
        ));

        $this->add(array(
            'name' => 'deliveryNoteAddress',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'deliveryNoteAddress',
            ),
        ));

        $this->add(array(
            'name' => 'customCurrencyId',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'customCurrencyId',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => ' ',
                'disable_inarray_validator' => true,
                'value_options' => isset($customCurrency) ? $customCurrency : null,
            )
        ));

        $this->add(array(
            'name' => 'invoiceReturnSaveButton',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'invoiceReturnSaveButton',
                'class' => 'btn btn-primary'
            ),
        ));

        $this->add(array(
            'name' => 'invoiceReturnCancelButton',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Cancel'),
                'id' => 'invoiceReturnCancelButton',
                'class' => 'btn btn-default'
            ),
        ));

        $this->add(array(
            'name' => 'total_discount_rate',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding creditNoteTotalDiscountRate',
                'id' => 'total_discount_rate',
                'autocomplete' => 'off'
            ),
        ));

         $this->add(array(
            'name' => 'promotion_discount_rate',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding creditNotePromotionDiscountRate',
                'id' => 'promotion_discount_rate',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'promotion',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'promotion',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => ' ',
                'disable_inarray_validator' => true,
                'value_options' => isset($promo) ? $promo : null,
            )
        ));

        $this->add(array(
            'name' => 'directCreditQuanity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'directCreditQuanity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'directUnitPrice',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'directUnitPrice',
                'autocomplete' => 'off'
            ),
        ));

         $this->add(array(
            'name' => 'directDiscount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'directDiscount',
                'autocomplete' => 'off'
            ),
        ));
    }

}

?>
