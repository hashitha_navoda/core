<?php

/**
 * Description of CustomerCategoryForm
 * @author Sharmilan <sharmilan@thinkcube.com>
 */

namespace Invoice\Form;

use Zend\Form\Form;

class CustomerCategoryForm extends Form
{

    public function __construct($name = null, $options = array())
    {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'customerCategoryID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'customerCategoryID',
            )
        ));

        $this->add(array(
            'name' => 'customerCategoryName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerCategoryName',
                'class' => 'form-control',
                'placeholder' => _('eg : Furniture')
            )
        ));

        $this->add(array(
            'name' => 'customerCategorySubmit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'customerCategorySubmit',
                'value' => _('Add Category'),
                'class' => 'btn btn-primary add_customer_category',
            )
        ));

        $this->add(array(
            'name' => 'customerCategoryUpdate',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'customerCategoryUpdate',
                'value' => _('Update Category'),
                'class' => 'btn btn-primary update_customer_category update',
            )
        ));

        $this->add(array(
            'name' => 'customerCategoryReset',
            'attributes' => array(
                'type' => 'reset',
                'id' => 'customerCategoryReset',
                'value' => _('Cancel'),
                'class' => 'btn btn-default reset_customer_category'
            )
        ));
    }

}
