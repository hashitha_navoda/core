<?php

/**
 * Description of Customer Profile Form
 * @author Sharmilan <sharmilan@thinkcube.com>
 */

namespace Invoice\Form;

use Zend\Form\Form;

class CustomerProfileForm extends Form {

    public function __construct($countries = array(), $name = null) {
        parent::__construct($name, $options);
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'customerProfileID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'customerProfileID',
            )
        ));
        $this->add(array(
            'name' => 'customerID',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'customerID',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileName',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileMobileTP1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileMobileTP1',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileMobileTP2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileMobileTP2',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileLandTP1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileLandTP1',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileLandTP2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileLandTP2',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileFaxNo',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileFaxNo',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileEmail',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileEmail',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileContactPersonName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileContactPersonName',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'customerProfileContactPersonNumber',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileContactPersonNumber',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileLocationNo',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileLocationNo',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'customerProfileLocationRoadName1',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileLocationRoadName1',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileLocationRoadName2',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileLocationRoadName2',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileLocationRoadName3',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileLocationRoadName3',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileLocationSubTown',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileLocationSubTown',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'customerProfileLocationTown',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileLocationTown',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'customerProfileLocationPostalCode',
            'attributes' => array(
                'type' => 'text',
                'id' => 'customerProfileLocationPostalCode',
                'class' => 'form-control',
            )
        ));
        $this->add(array(
            'name' => 'customerProfileLocationCountry',
            'type' => 'Select',
            'attributes' => array(
                'id' => 'customerProfileLocationCountry',
                'class' => 'form-control selectpicker',
                'data-live-search' => true,
            ),
            'options' => array(
                'empty_option' => 'Select a country',
                'value_options' => $countries,
            )
        ));

        $this->add(array(
            'name' => 'customerProfileUpdate',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'customerProfileUpdate',
                'value' => _('Update Profile'),
                'class' => 'btn btn-primary update_customer_Profile update',
            )
        ));

        $this->add(array(
            'name' => 'customerProfileReset',
            'attributes' => array(
                'type' => 'reset',
                'id' => 'customerProfileReset',
                'value' => _('Reset'),
                'class' => 'btn btn-default make-full-width reset_customer_profile'
            )
        ));
    }

}
