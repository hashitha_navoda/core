<?php

/**
 * Description of RatingTypesForm
 * @author Ashan <ashan@thinkcube.com>
 */

namespace Invoice\Form;

use Zend\Form\Form;

class RatingTypesForm extends Form
{

    public function __construct()
    {
        parent::__construct();
        $this->setAttribute('method', 'post');
        $this->setAttribute('class', 'form-horizontal');

        $this->add(array(
            'name' => 'ratingTypesId',
            'attributes' => array(
                'type' => 'hidden',
                'id' => 'ratingTypesId',
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesCode',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ratingTypesCode',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesName',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ratingTypesName',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesDescription',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'ratingTypesDescription',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesMethod',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'ratingTypesMethod',
                'data-live-search' => "true",
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => ' ',
                'disable_inarray_validator' => true,
                'value_options' => array(1 => 'Star Ratings', 2 => 'Number Rating'),
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesMethodDefinition',
            'attributes' => array(
                'type' => 'textarea',
                'id' => 'ratingTypesMethodDefinition',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesState',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'ratingTypesState',
                'data-live-search' => "true",
                'title' => ' ',
            ),
            'options' => array(
                'disable_inarray_validator' => true,
                'value_options' => array(1 => 'Active', 0 => 'InActive'),
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesMethodSize',
            'attributes' => array(
                'type' => 'text',
                'id' => 'ratingTypesMethodSize',
                'class' => 'form-control',
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesReset',
            'attributes' => array(
                'type' => 'reset',
                'id' => 'ratingTypesReset',
                'value' => _('Cancel'),
                'class' => 'btn btn-default  ratingTypesReset'
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesSubmit',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'ratingTypesSubmit',
                'value' => _('Add Rating Type'),
                'class' => 'btn btn-primary  ratingTypesSubmit',
            )
        ));

        $this->add(array(
            'name' => 'ratingTypesUpdate',
            'attributes' => array(
                'type' => 'submit',
                'id' => 'ratingTypesUpdate',
                'value' => _('Update Rating Types'),
                'class' => 'btn btn-primary ratingTypesUpdate hidden',
            )
        ));
    }

}
