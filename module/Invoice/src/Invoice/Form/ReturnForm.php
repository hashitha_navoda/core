<?php

namespace Invoice\Form;

use Zend\Form\Form;

class ReturnForm extends Form
{

    public function __construct($data)
    {

        $name = 'returnForm';
        $customCurrency = $data['customCurrency'];
        parent::__construct($name);
        $this->setAttribute('method', 'post');
        $this->setAttribute("class", "form-horizontal");
        $this->setAttribute("id", $name);

        $this->add(array(
            'name' => 'returnNo',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'returnNo',
            ),
        ));

        $this->add(array(
            'name' => 'customer',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'customer',
            ),
        ));

        $this->add(array(
            'name' => 'currentLocation',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'currentLocation',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'returnDate',
            'type' => 'date',
            'attributes' => array(
                'placeholder' => 'Click to Set a Date',
                'class' => 'datepicker form-control',
                'id' => 'returnDate',
                'data-date-format' => 'yyyy-mm-dd',
            ),
        ));

        $this->add(array(
            'name' => 'itemCode',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemCode',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'itemName',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemName',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'itemDescription',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'itemDescription'
            ),
        ));

        $this->add(array(
            'name' => 'deliveredQuantity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'deliveredQuantity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'returnQuanity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'returnQuanity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'directReturnQuanity',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'directReturnQuanity',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'unitPrice',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'unitPrice',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'directUnitPrice',
            'type' => 'text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'directUnitPrice',
                'autocomplete' => 'off'
            ),
        ));

        $this->add(array(
            'name' => 'discount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'discount',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'directDiscount',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control text-right reduce_le_ri_padding',
                'id' => 'directDiscount',
                'autocomplete' => 'off'
            ),
        ));
        $this->add(array(
            'name' => 'deliveryCharge',
            'type' => 'Text',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'deliveryCharge',
            ),
        ));

        $this->add(array(
            'name' => 'deliveryChargeEnable',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'deliveryChargeEnable',
            ),
        ));

        $this->add(array(
            'name' => 'showTax',
            'type' => 'Zend\Form\Element\Checkbox',
            'options' => array(
                'use_hidden_element' => FALSE,
                'checkedValue' => '1',
                'uncheckedValue' => '0',
            ),
            'attributes' => array(
                'id' => 'showTax',
            ),
        ));

        $this->add(array(
            'name' => 'comment',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'comment',
            ),
        ));

        $this->add(array(
            'name' => 'deliveryNoteAddress',
            'type' => 'Zend\Form\Element\Textarea',
            'attributes' => array(
                'class' => 'form-control',
                'id' => 'deliveryNoteAddress',
            ),
        ));

        $this->add(array(
            'name' => 'customCurrencyId',
            'type' => 'Zend\Form\Element\Select',
            'attributes' => array(
                'class' => 'form-control selectpicker',
                'id' => 'customCurrencyId',
                'data-live-search' => "true",
                'required' => false,
                'title' => ' ',
            ),
            'options' => array(
                'empty_option' => ' ',
                'disable_inarray_validator' => true,
                'value_options' => isset($customCurrency) ? $customCurrency : null,
            )
        ));

        $this->add(array(
            'name' => 'returnSaveButton',
            'attributes' => array(
                'type' => 'submit',
                'value' => _('Save'),
                'id' => 'returnSaveButton',
                'class' => 'btn btn-primary'
            ),
        ));
        $this->add(array(
            'name' => 'retrunCancelButton',
            'attributes' => array(
                'type' => 'reset',
                'value' => _('Cancel'),
                'id' => 'retrunCancelButton',
                'class' => 'btn btn-default'
            ),
        ));
    }

}

?>
