<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DeliveryNoteProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveDeliveryNoteProductTax(DeliveryNoteProductTax $deliveryNoteProductTax)
    {
        $data = array(
            'deliveryNoteProductID' => $deliveryNoteProductTax->deliveryNoteProductID,
            'productID' => $deliveryNoteProductTax->productID,
            'deliveryNoteTaxID' => $deliveryNoteProductTax->deliveryNoteTaxID,
            'deliveryNoteTaxPrecentage' => $deliveryNoteProductTax->deliveryNoteTaxPrecentage,
            'deliveryNoteTaxAmount' => $deliveryNoteProductTax->deliveryNoteTaxAmount,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getTaxByDeliveryNoteProductID($deliveryNoteProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('deliveryNoteProductTax')
                ->columns(array('*'))
                ->join('tax', 'tax.id = deliveryNoteProductTax.deliveryNoteTaxID', array('*'), 'left')
                ->where(array('deliveryNoteProductTax.deliveryNoteProductID' => $deliveryNoteProductID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }
    
    /**
     * Get delivery note product total tax
     * @param string $deliveryNoteProductId
     * @return mixed
     */
    public function getDeliveryNoteTotalProductTax($deliveryNoteProductId) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNoteProductTax')
                ->columns(['deliveryNoteProductID',
                    'deliveryNoteProductTaxAmount' => new Expression('SUM(deliveryNoteProductTax.deliveryNoteTaxAmount)')
                ])
                ->where(array('deliveryNoteProductTax.deliveryNoteProductID' => $deliveryNoteProductId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    /**
     * This function is used to get delivery note product tax
     */
    public function getProductTax($deliveryNoteID, $productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProductTax")
                ->columns(array("*"))
                ->join("deliveryNoteProduct", "deliveryNoteProductTax.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", ["deliveryNoteID", "deliveryNoteProductQuantity"], "left")
                ->where(array("deliveryNoteProduct.deliveryNoteID" => $deliveryNoteID))
                ->where(array("deliveryNoteProductTax.productID" => $productID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function getDeliveryNoteTax($deliveryNoteID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProductTax")
                ->columns(array("*"))
                ->join("tax", "deliveryNoteProductTax.deliveryNoteTaxID = tax.id", array("*"), "left")
                ->join("deliveryNoteProduct", "deliveryNoteProductTax.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array("deliveryNoteID"), "left")
                ->where(array("deliveryNoteProduct.deliveryNoteID" => $deliveryNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }
}
