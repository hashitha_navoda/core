<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains return Table Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\ResultSet\ResultSet;

class ReturnsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('salesReturn')
                    ->columns(array('*'))
                    ->join('customer', 'salesReturn.customerID = customer.customerID', array('customerName', 'customerCode'), 'left')
                    ->join('currency', 'salesReturn.customCurrencyId = currency.currencyId', array('currencySymbol'), 'left')
                    ->order(array('salesReturnDate' => 'DESC'))
                    ->where(array('salesReturn.locationID' => $locationID));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesReturn');
        $select->where(array('locationID' => $locationID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getReturnDetailsBySalesReturnCode($salesReturnCode)
    {
        $rowset = $this->tableGateway->select(array('salesReturnCode' => $salesReturnCode));
        $row = $rowset->current();
        return $row;
    }

    public function checkReturnByCode($returnCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('salesReturn')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "salesReturn.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('salesReturn.salesReturnCode' => $returnCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function saveSalesReturn(Returns $return)
    {
        $data = array(
            'salesReturnCode' => $return->salesReturnCode,
            'customerID' => $return->customerID,
            'salesReturnDate' => $return->salesReturnDate,
            'salesReturnTotal' => $return->salesReturnTotal,
            'locationID' => $return->locationID,
            'salesReturnComment' => $return->salesReturnComment,
            'statusID' => $return->statusID,
            'customCurrencyId' => $return->customCurrencyId,
            'salesReturnCustomCurrencyRate' => $return->salesReturnCustomCurrencyRate,
            'entityID' => $return->entityID,
            'directReturnFlag' => $return->directReturnFlag,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getReturnforSearch($returnID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesReturn")
                ->columns(array("*"))
                ->join("customer", "salesReturn.customerID = customer.customerID", array("*"), "left")
                ->join('currency', 'salesReturn.customCurrencyId = currency.currencyId', array('currencySymbol'), 'left')
                ->where(array("salesReturnID" => $returnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getReturnforSearchForJE($returnID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesReturn")
                ->columns(array("*"))
                ->join("customer", "salesReturn.customerID = customer.customerID", array("*"), "left")
                ->join('currency', 'salesReturn.customCurrencyId = currency.currencyId', array('currencySymbol'), 'left')
                ->where(array("salesReturnID" => $returnID));
        $select->where->notEqualTo('salesReturn.statusID', 5);
        $select->where->notEqualTo('salesReturn.statusID', 10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getReturnsByCustomerID($customerID, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesReturn")
                ->columns(array("*"))
                ->join("customer", "salesReturn.customerID = customer.customerID", array("customerName", "customerCode"), "left")
                ->join('currency', 'salesReturn.customCurrencyId = currency.currencyId', array('currencySymbol'), 'left')
                ->where(array("salesReturn.customerID" => $customerID, "salesReturn.locationID" => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);


        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getReturnsByDate($fromdate, $todate, $customerID = NULL, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesReturn")
                ->columns(array("*"))->join("customer", "salesReturn.customerID = customer.customerID", array("customerName", "customerShortName", "customerCode"), "left")
                ->join('currency', 'salesReturn.customCurrencyId = currency.currencyId', array('currencySymbol'), 'left')
                ->order(array('salesReturnDate' => 'DESC'))
                ->where(array("salesReturn.locationID" => $locationID));
        if ($customerID != NULL) {
            $select->where(array("salesReturn.customerID" => $customerID));
        }
        $select->where->between('salesReturnDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);


        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getReturnDetailsByReturnID($returnID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesReturn');
        $select->join('entity', 'salesReturn.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->where(array('salesReturnID' => $returnID));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search Sales Returns For dropdown
     * @param string $locationID
     * @param string $searchKey
     * @return array limited to maximim 50
     */
    public function searchSalesReturnForDropdown($locationID, $searchKey)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesReturn');
        $select->where(array('locationID' => $locationID));
        $select->where->like('salesReturnCode', '%' . $searchKey . '%');
        $select->limit(50);
        $query = $sql->prepareStatementForSqlObject($select);

        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getSalesReturnByCode($salesReturnCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesReturn');
        $select->columns(array('salesReturnID'));
        $select->join('location', "location.locationID = salesReturn.locationID", array('locationName', 'locationCode'), 'left');
        $select->where(array('salesReturnCode' => $salesReturnCode));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getSalesReturnById($salesReturnId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesReturn');
        $select->columns(array('salesReturnID', 'salesReturnCode'));
        $select->join('location', "location.locationID = salesReturn.locationID", array('locationName', 'locationCode'), 'left');
        $select->where(array('salesReturnID' => $salesReturnId));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getSalesReturnByDeliveryNoteID($deliveryNoteID = null, $creditNoteID = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesReturn');
        $select->columns(array('*'));
        $select->join('salesReturnProduct', "salesReturn.salesReturnID = salesReturnProduct.salesReturnID", array('deliveryNoteProductID'), 'left');
        $select->join('deliveryNoteProduct', "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array('deliveryNoteID'), 'left');
        if (!is_null($deliveryNoteID)) {
            $select->where(array('deliveryNoteProduct.deliveryNoteID' => $deliveryNoteID));
        }
        if (!is_null($creditNoteID)) {
            $select->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNoteProduct.deliveryNoteID', array('*'),'left');
            $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
            $select->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
            $select->join('entity', 'salesReturn.entityID = entity.entityID', array('deleted','createdTimeStamp'));
            $select->where(array('creditNote.creditNoteID' => $creditNoteID, 'salesInvoiceProduct.documentTypeID' => 4));
            $select->group("salesReturn.salesReturnID");
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    // get delivery note related sales return by quotation id
    public function getDeliveryNoteRelatedReturnsDataByQuotationId($quotationID = null, $soID = null, $deliveryNoteID = null, $invoiceID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesReturn');
        $select->columns(array('*'));
        $select->join('salesReturnProduct', "salesReturn.salesReturnID = salesReturnProduct.salesReturnID", array('deliveryNoteProductID'), 'left');
        $select->join('deliveryNoteProduct', "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array('deliveryNoteID'), 'left');
        $select->join('deliveryNote', "deliveryNoteProduct.deliveryNoteID = deliveryNote.deliveryNoteID", array('*'), 'left');
        $select->join('entity', 'salesReturn.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->join('salesOrders', 'deliveryNote.salesOrderID = salesOrders.soID', array('*'),'left');
            $select->where(array('salesOrders.quotationID' => $quotationID));
        }
        if (!is_null($soID)) {
            $select->join('salesOrders', 'deliveryNote.salesOrderID = salesOrders.soID', array('*'),'left');
            $select->where(array('salesOrders.soID' => $soID));
        }
        if (!is_null($deliveryNoteID)) {
            $select->where(array('deliveryNote.deliveryNoteID' => $deliveryNoteID));
        }
        if (!is_null($invoiceID)) {
            $select->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
            $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
            $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        $select->group("salesReturn.salesReturnID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get return basic details with time stamp by return id 
    public function getReturnBasicDetailsByReturnId($returnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesReturn');
        $select->columns(array('*'));
        $select->join('entity', 'salesReturn.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesReturn.salesReturnID' => $returnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;
    }
}
