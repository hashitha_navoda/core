<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\NotIn;

class InvoiceProductTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('invoice')
                    ->columns(array('*'))
                    ->order(array('issuedDate' => 'DESC'));
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Invoice());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveInvoiceProduct(InvoiceProduct $invocieProduct)
    {
        $data = array(
            'salesInvoiceID' => $invocieProduct->salesInvoiceID,
            'productID' => $invocieProduct->productID,
            'locationProductID' => $invocieProduct->locationProductID,
            'salesInvoiceProductDescription' => $invocieProduct->salesInvoiceProductDescription,
            'salesInvoiceProductPrice' => $invocieProduct->salesInvoiceProductPrice,
            'salesInvoiceProductQuantity' => $invocieProduct->salesInvoiceProductQuantity,
            'salesInvoiceProductTotal' => $invocieProduct->salesInvoiceProductTotal,
            'salesInvoiceProductDiscount' => $invocieProduct->salesInvoiceProductDiscount,
            'salesInvoiceProductDiscountType' => $invocieProduct->salesInvoiceProductDiscountType,
            'salesInvoiceProductTax' => $invocieProduct->salesInvoiceProductTax,
            'documentTypeID' => $invocieProduct->documentTypeID,
            'salesInvoiceProductDocumentID' => $invocieProduct->salesInvoiceProductDocumentID,
            'salesInvoiceProductSelectedUomId' => $invocieProduct->salesInvoiceProductSelectedUomId,
            'inclusiveTaxSalesInvoiceProductPrice' => $invocieProduct->inclusiveTaxSalesInvoiceProductPrice,
            'itemSalesPersonID' => $invocieProduct->itemSalesPersonID,
            'salesInvoiceProductMrpType' => $invocieProduct->salesInvoiceProductMrpType,
            'salesInvoiceProductMrpValue' => $invocieProduct->salesInvoiceProductMrpValue,
            'salesInvoiceProductMrpPercentage' => $invocieProduct->salesInvoiceProductMrpPercentage,
            'salesInvoiceProductMrpAmount' => $invocieProduct->salesInvoiceProductMrpAmount,
            'isFreeItem' => $invocieProduct->isFreeItem,
            'hasFreeItem' => $invocieProduct->hasFreeItem
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getProductsByInvoiceID($invoiceID, $flag = false)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('product', 'salesInvoiceProduct.productID =  product.productID', array('*'), 'left')
//                ->join('locationProduct', 'locationProduct.productID =  product.productID', array('*'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProductTax.salesInvoiceProductID =  salesInvoiceProduct.salesInvoiceProductID', array('taxTotal' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay', 'productUomBase'))
                ->join('uom', 'salesInvoiceProduct.salesInvoiceProductSelectedUomId = uom.uomID', array('uomAbbr', 'uomDecimalPlace'),'left')
                ->group(array('salesInvoiceProduct.salesInvoiceProductID'))
                ->where(array('salesInvoiceID' => $invoiceID));
        if($flag){
            $select->where(array('productUomBase' => '1'));

        }
                // ->where(array('productUomConversion' => '1'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getInvoiceProductByInvoiceIDForCreditNote($InvocieID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->join("salesInvoice", "salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID", array("salesInvoiceSuspendedTax"), "left")
                ->join("product", "salesInvoiceProduct.productID = product.productID", array("*"), "left")
                ->join("locationProduct", "salesInvoiceProduct.locationProductID = locationProduct.locationProductID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("salesInvoiceProductTax", "salesInvoiceProduct.salesInvoiceProductID= salesInvoiceProductTax.salesInvoiceProductID", array("salesInvoiceProductTaxID", "taxID", "salesInvoiceProductTaxPrecentage", "salesInvoiceProductTaxAmount"), "left")
                ->join("tax", "salesInvoiceProductTax.taxID = tax.id", array("*"), "left")
                ->join("salesInvoiceSubProduct", "salesInvoiceProduct.salesInvoiceProductID = salesInvoiceSubProduct.salesInvocieProductID", array("salesInvoiceSubProductID", "productBatchID", "productSerialID", "salesInvoiceSubProductQuantity","salesInvoiceSubProductWarranty", "salesInvoiceSubProductWarrantyType"), "left")
                ->order(array('salesInvoiceProduct.salesInvoiceProductID','salesInvoiceProduct.productID'))
                ->where(array('productUomBase' => '1'))
                ->where(array("salesInvoiceProduct.salesInvoiceID" => $InvocieID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param int $productID
     * @param int $invoiceID
     */
    public function updateCopiedSalesInvoiceProducts($invoiceProductID)
    {
        $data = array(
            'copied' => 1
        );
        $this->tableGateway->update($data, array('salesInvoiceProductID' => $invoiceProductID));
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * get product list from salesInvoiceProduct table which copied = 0
     * @param int $invoiceID
     * @return $resultList
     */
    public function getUnCopiedSalesOrderProducts($invoiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoiceProduct');
        $select->where(array('salesInvoiceID' => $invoiceID));
        $select->where(array('copied' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultList = [];

        foreach ($results as $result) {
            $resultList[] = $result;
        }
        return $resultList;
    }

    public function getProductDetailsByInvoiceProductID($invoiceProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoiceProduct');
        $select->where(array('salesInvoiceProductID' => $invoiceProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getProductDetailsByProductIDAndInvoiceID($invoiceID, $productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoiceProduct');
        $select->where(array('productID' => $productID, 'salesInvoiceID' => $invoiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param type $productID
     */
    public function getCustomersForProduct($productID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoiceProduct');
        $select->join("salesInvoice", "salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID", array('customerID'));
        $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerID', 'customerTitle', 'customerName', 'customerCode', 'customerShortName', 'customerTelephoneNumber' ,'customerStatus'));
        $select->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left');
        $select->join('customerProfile', 'customer.customerID = customerProfile.customerID', '*', 'left');
        $select->where(array('salesInvoiceProduct.productID' => $productID));
        $select->where(array('entity.deleted' => '0', 'customerProfile.isPrimary' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultList = [];
        foreach ($results as $result) {
            $resultList[] = $result;
        }
        return $resultList;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     *
     */
    public function getCustomersForAllProducts($productList)
    {
        $productListString = implode(",", $productList);
        $productCount = count($productList);
        $adapter = $this->tableGateway->getAdapter();
        $statement = $adapter->query("SELECT customerID,customerName,customerTitle,customerTelephoneNumber,customerStatus FROM customer WHERE customerID IN (SELECT customerID FROM (
                        SELECT customerID, count(*) noOfPurchases FROM (SELECT si.customerID, sip.productID  FROM salesInvoiceProduct as sip, salesInvoice as si, customer as c WHERE sip.salesInvoiceID = si.salesInvoiceID AND si.customerID = c.customerID AND
                        sip.productID IN ($productListString) GROUP BY sip.productID, c.customerID ORDER BY `sip`.`salesInvoiceID` ASC) as uniqueCustomerProducts GROUP BY customerID) as purchases WHERE noOfPurchases = $productCount)");
        $results = $statement->execute();

        $resultsList = [];
        foreach ($results as $result) {
            $resultsList[] = $result;
        }
        return $resultsList;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param int $invoiceID
     * @return $result
     */
    public function getProductsDetailsByInvoiceID($invoiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->where(array(
            'itemOut.itemOutDocumentID' => $invoiceID,
            'itemOut.itemOutDocumentType' => 'Sales Invoice',
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result;
    }

    public function getProductsDetailsByInvoiceIDAndLocationProductID($invoiceID, $locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->where(array(
            'itemOut.itemOutDocumentID' => $invoiceID,
            'itemOut.itemOutDocumentType' => 'Sales Invoice',
            'itemOut.itemOutLocationProductID' => $locationProductID,
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result;
    }

    public function getProductsDetailsByDeliveryNoteIDAndLocationProductID($deliveryNoteID, $locationProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->where(array(
            'itemOut.itemOutDocumentID' => $deliveryNoteID,
            'itemOut.itemOutDocumentType' => 'Delivery Note',
            'itemOut.itemOutLocationProductID' => $locationProductID,
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result;
    }

    public function getProductsForInvoiceCancelByInvoiceID($invoiceID)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('product', 'salesInvoiceProduct.productID =  product.productID', array('*'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProductTax.salesInvoiceProductID =  salesInvoiceProduct.salesInvoiceProductID', array('taxTotal' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->group(array('salesInvoiceProduct.salesInvoiceProductID'))
                ->where(array('productUomBase' => '1'))
                ->where(array('salesInvoiceID' => $invoiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }



    public function getProductsForInvoiceEditByInvoiceID($invoiceID,$locationID)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('product', 'salesInvoiceProduct.productID =  product.productID', array('*'), 'left')
                ->join('locationProduct', 'locationProduct.productID =  product.productID', array('*'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProductTax.salesInvoiceProductID =  salesInvoiceProduct.salesInvoiceProductID', array('taxTotal' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->group(array('salesInvoiceProduct.salesInvoiceProductID'))
                ->where(array('productUomBase' => '1'))
                ->where(array('locationProduct.locationID' => $locationID))
                ->where(array('salesInvoiceID' => $invoiceID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    /**
     *
     * @param type $productId
     * @param type $salesPersonId
     * @param type $fromDate
     * @param type $toDate
     * @return mixed
     */
    public function getInvoiceProductBySalesPersonId($productId, $salesPersonId, $fromDate = null, $toDate = null)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesPersonID', 'salesInvoiceID' ,'salesInvoiceIssuedDate', 'statusID', 'entityID', 'salesInvoiceCode', 'customerID', 'salesinvoiceTotalAmount', 'salesInvoicePayedAmount'))
                ->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left')
                ->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerCode', 'customerName'))
                ->join('product','salesInvoiceProduct.productID = product.productID',array('productName'))
                ->join('creditNoteProduct', 'salesInvoiceProduct.salesInvoiceProductID = creditNoteProduct.invoiceProductID', array(
                     'cNPTotal' => new Expression('SUM(creditNoteProduct.creditNoteProductTotal)'),
                     'cNPQuantity' => new Expression('SUM(creditNoteProduct.creditNoteProductQuantity)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array(
                    'salesInvoiceProduct.productID' => $productId,
                    'invoiceSalesPersons.salesPersonID' => $salesPersonId,
                    'entity.deleted' => 0
                ));
        if ($fromDate && $toDate) {
            $select->where(new Between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate));
        }
        $select->group('salesInvoiceProduct.salesInvoiceProductID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    /**
     *
     * @param type $categoryId
     * @param type $salesPersonId
     * @param type $fromDate
     * @param type $toDate
     * @return mixed
     */
    public function getInvoiceProductByCategoryIdAndSalesPersonId($categoryId, $salesPersonId, $fromDate, $toDate)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesPersonID','salesInvoiceID', 'salesInvoiceIssuedDate', 'statusID'))
                ->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left')
                ->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left')
                ->join('product', 'salesInvoiceProduct.productID = product.productID', array('categoryID'))
                ->join('creditNoteProduct', 'salesInvoiceProduct.salesInvoiceProductID = creditNoteProduct.invoiceProductID', array(
                    'cNPTotal' => new Expression('SUM(creditNoteProduct.creditNoteProductTotal)'),
                    'cNPQuantity' => new Expression('SUM(creditNoteProduct.creditNoteProductQuantity)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array(
                    'product.categoryID' => $categoryId,
                    'invoiceSalesPersons.salesPersonID' => $salesPersonId,
                    'entity.deleted' => 0
                ));
        if ($fromDate && $toDate) {
            $select->where(new Between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate));
        }
        $select->group('salesInvoiceProduct.salesInvoiceProductID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getProductByInvoiceID($invoiceID, $locationID)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('product', 'salesInvoiceProduct.productID =  product.productID', array('*'), 'left')
                ->join('locationProduct', 'locationProduct.productID =  product.productID', array('locationProductID'))
                ->join('salesInvoiceProductTax', 'salesInvoiceProductTax.salesInvoiceProductID =  salesInvoiceProduct.salesInvoiceProductID', array('taxTotal' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->group(array('salesInvoiceProduct.salesInvoiceProductID'))
                ->where(array('productUomConversion' => '1'))
                ->where(array('salesInvoiceID' => $invoiceID))
                ->where(array('locationID' => $locationID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get invoice product details for dispatch note
     * @param int $invocieId
     * @return mixed
     */
    public function getInvoiceProductByInvoiceIdForDispatchNote($invocieId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->join("product", "salesInvoiceProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->join("salesInvoice", "salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID", array('salesInvoiceCode', 'customerID', 'locationID', 'salesInvoiceIssuedDate', 'salesPersonID'), "left")
                ->where(array('productUomBase' => '1'))
                ->where(array("salesInvoice.salesInvoiceID" => $invocieId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get invoice serial products by invoice id
     * @param int $InvocieId
     * @return mixed
     */
    public function getInvoiceSerialProductByInvoiceId($InvocieId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->join("product", "salesInvoiceProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->join("salesInvoiceSubProduct", "salesInvoiceProduct.salesInvoiceProductID = salesInvoiceSubProduct.salesInvocieProductID", array("salesInvoiceSubProductID", "productBatchID", "productSerialID", "salesInvoiceSubProductQuantity", "salesInvoiceSubProductWarranty", "salesInvoiceSubProductWarrantyType"), "left")
                ->join("productSerial", "salesInvoiceSubProduct.productSerialID = productSerial.productSerialID", array('productSerialCode', 'productSerialWarrantyPeriod','productSerialExpireDate'), "left")
                ->where(array('productUomBase' => '1'))
                ->where(array('serialProduct' => '1'))
                ->where(array("salesInvoiceID" => $InvocieId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get invoice batch products by invoice id
     * @param int $InvocieId
     * @return mixed
     */
    public function getInvoiceBatchProductByInvoiceId($InvocieId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->join("product", "salesInvoiceProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("salesInvoiceSubProduct", "salesInvoiceProduct.salesInvoiceProductID = salesInvoiceSubProduct.salesInvocieProductID", array("salesInvoiceSubProductID", "productBatchID", "productSerialID", "salesInvoiceSubProductQuantity"), "left")
                ->join("productBatch", "salesInvoiceSubProduct.productBatchID = productBatch.productBatchID", array('productBatchCode', 'productBatchWarrantyPeriod','productBatchExpiryDate'), "left")
                ->where(array('productUomBase' => '1'))
                ->where(array('batchProduct' => '1'))
                ->where(array("salesInvoiceID" => $InvocieId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get invoice product by invoiceProductId
     * @param int $invoiceProductId
     * @return mixed
     */
    public function getInvoiceProductByInvoiceProductId($invoiceProductId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->where(array("salesInvoiceProductID" => $invoiceProductId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

    public function getInvoiceProductByInvoiceID($InvocieID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->join("product", "salesInvoiceProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("salesInvoiceProductTax", "salesInvoiceProduct.salesInvoiceProductID= salesInvoiceProductTax.salesInvoiceProductID", array("salesInvoiceProductTaxID", "taxID", "salesInvoiceProductTaxPrecentage", "salesInvoiceProductTaxAmount"), "left")
                ->join("tax", "salesInvoiceProductTax.taxID = tax.id", array("*"), "left")
                ->join("salesInvoiceSubProduct", "salesInvoiceProduct.salesInvoiceProductID = salesInvoiceSubProduct.salesInvocieProductID", array("salesInvoiceSubProductID", "productBatchID", "productSerialID", "salesInvoiceSubProductQuantity"), "left")
                ->group("salesInvoiceProduct.salesInvoiceProductID")
                ->where(array('productUomBase' => '1'))
                ->where(array("salesInvoiceID" => $InvocieID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

     public function getInvoiceProductAndDispalyUOMByInvoiceID($InvocieID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->join("product", "salesInvoiceProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("salesInvoiceProductTax", "salesInvoiceProduct.salesInvoiceProductID= salesInvoiceProductTax.salesInvoiceProductID", array("salesInvoiceProductTaxID", "taxID", "salesInvoiceProductTaxPrecentage", "salesInvoiceProductTaxAmount"), "left")
                ->join("tax", "salesInvoiceProductTax.taxID = tax.id", array("*"), "left")
                ->join("salesInvoiceSubProduct", "salesInvoiceProduct.salesInvoiceProductID = salesInvoiceSubProduct.salesInvocieProductID", array("salesInvoiceSubProductID", "productBatchID", "productSerialID", "salesInvoiceSubProductQuantity"), "left")
                ->group("salesInvoiceProduct.salesInvoiceProductID")
                ->where(array('productUomDisplay' => '1'))
                ->where(array("salesInvoiceID" => $InvocieID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    //get products by invoiceID where copied form delvery note.
    public function getDeliveryNoteCopiedProductsByInvoiceID($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->where(array("salesInvoiceID" => $invoiceID, 'documentTypeID' => 4));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function gelSalesInvoiceProductDetailsByInvoiceIDAndLocationProductID($invoiceID, $locationProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->where(array("salesInvoiceID" => $invoiceID, 'locationProductID' => $locationProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getProductsDetailsByInvoiceIDLocationProductIDAndSerialID($invoiceID, $locationProductID, $serialID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->where(array(
            'itemOut.itemOutDocumentID' => $invoiceID,
            'itemOut.itemOutDocumentType' => 'Sales Invoice',
            'itemOut.itemOutLocationProductID' => $locationProductID,
            'itemOut.itemOutSerialID' => $serialID,
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result->current();
    }

    public function getProductsDetailsByInvoiceIDLocationProductIDAndBatchID($invoiceID, $locationProductID, $batchID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->where(array(
            'itemOut.itemOutDocumentID' => $invoiceID,
            'itemOut.itemOutDocumentType' => 'Sales Invoice',
            'itemOut.itemOutLocationProductID' => $locationProductID,
            'itemOut.itemOutBatchID' => $batchID,
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result->current();
    }

    /**
    * this function use to get all item out details that related to the given invoice id location product id and batch id
    * @param int $invoiceID
    * @param int $locationProductID
    * @param int $batchID
    * return array
    **/
    public function getAllProductsDetailsByInvoiceIDLocationProductIDAndBatchID($invoiceID, $locationProductID, $batchID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->columns(array('itemOutQty' => new Expression('sum(itemOutQty)'),'itemOutBatchID','itemOutSerialID','itemOutPrice','itemOutLocationProductID'));
        $select->where(array(
            'itemOut.itemOutDocumentID' => $invoiceID,
            'itemOut.itemOutDocumentType' => 'Sales Invoice',
            'itemOut.itemOutLocationProductID' => $locationProductID,
            'itemOut.itemOutBatchID' => $batchID,
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result->current();
    }

    /**
     * Get invoice product details by sales person ids & customer ids
     * @param string $fromDate
     * @param string $toDate
     * @param array $itemIds
     * @param array $customerIds
     * @param array $salesPersonIds
     * @param boolean $withDefaultSalesPerson
     * @return mixed
     */
    public function getInvoiceProductDetailsBySalesPersonIdsAndCustomerIds($fromDate = NULL, $toDate = NULL, $itemIds = [], $customerIds = [], $salesPersonIds = [], $withDefaultSalesPerson = false, $cusCategory = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(['salesInvoiceID','productID','salesInvoiceProductTotal','salesInvoiceProductID'])
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', ['*'])
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array('taxID','salesInvoiceProductTaxAmount'), 'left')
                ->join('tax', 'tax.id = salesInvoiceProductTax.taxID', 'taxName', 'left')
                ->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left')
                ->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left')
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("entity", "salesInvoice.entityID = entity.entityID", array("deleted"), "left")
                ->where->equalTo('entity.deleted', 0)
                ->where->in('salesInvoice.statusID', [3, 6])
                ->where->in('salesInvoice.customerID', $customerIds);
                if($itemIds){
                    $select->where->in('salesInvoiceProduct.productID', $itemIds);
                }
                if($fromDate && $toDate){
                    $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
                }
                if($cusCategory != ''){
                    $select->join('customerCategory', 'customer.customerCategory = customerCategory.customerCategoryID', array('customerCategoryName'), 'left');
                    $select->where->in('customer.customerCategory', $cusCategory);
                }
                if($withDefaultSalesPerson) {
                    $select->where->AND->NEST
                    ->in('invoiceSalesPersons.salesPersonID', $salesPersonIds)->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                    );
                } else {
                    $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonIds);
                }
                $select->order(['customer.customerName' => 'ASC']);
                //$select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

      /**
     * Get all invoice product details by sales person ids & product ids
     * @param string $fromDate
     * @param string $toDate
     * @param array $itemIds
     * @param array $salesPersonIds
     * @return mixed
     */

    public function getAllInvoiceProductBySalesPersonIdsAndProIDs($productId , $salesPersonId , $fromDate = null, $toDate = null, $withDefaultSalesPerson = false)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesPersonID', 'salesInvoiceID', 'salesInvoiceIssuedDate', 'statusID', 'entityID', 'salesInvoiceCode', 'customerID', 'salesinvoiceTotalAmount', 'salesInvoicePayedAmount'))
                // ->join('salesPerson', 'salesInvoice.salesPersonID = salesPerson.salesPersonID', array('salesPersonSortName','salesPersonFirstName','salesPersonLastName'), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerCode', 'customerName'))
                ->join('product','salesInvoiceProduct.productID = product.productID',array('productName'))
                ->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left')
                ->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left')
                // ->join('creditNoteProduct', 'salesInvoiceProduct.salesInvoiceProductID = creditNoteProduct.invoiceProductID', array('creditNoteProductTotal',
                //      'creditNoteProductQuantity'), 'left')
                // ->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('creditNoteCode','creditNoteTotal'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0 ));
        
        $select->where->in('salesInvoiceProduct.productID', $productId);
        if($withDefaultSalesPerson) {
                $select->where->AND->NEST
                ->in('invoiceSalesPersons.salesPersonID', $salesPersonId)->orPredicate(
                    new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                );
        } else {
            $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonId);
        }

        
        if ($fromDate && $toDate) {
            $select->where(new Between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

      /**
     * Get all invoice product Related Credit Note Details by sales person ids & product ids
     * @param string $fromDate
     * @param string $toDate
     * @param array $itemIds
     * @param array $salesPersonIds
     * @return mixed
     */

    public function getAllInvoiceProductRelatedCreditNoteProductByproIDAndSalesPerson($productId , $salesPersonId , $fromDate = null, $toDate = null, $withDefaultSalesPerson = false)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesPersonID', 'salesInvoiceIssuedDate', 'statusID', 'entityID', 'salesInvoiceCode', 'customerID', 'salesinvoiceTotalAmount', 'salesInvoicePayedAmount'))
                ->join('creditNoteProduct', 'salesInvoiceProduct.salesInvoiceProductID = creditNoteProduct.invoiceProductID', array('creditNoteProductTotal',
                     'creditNoteProductQuantity'), 'left')
                ->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left')
                ->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left')
                // ->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('creditNoteCode','creditNoteTotal'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0 ));
        
        $select->where->in('salesInvoiceProduct.productID', $productId);
        if($withDefaultSalesPerson) {
                $select->where->AND->NEST
                ->in('invoiceSalesPersons.salesPersonID', $salesPersonId)->orPredicate(
                    new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                );
        } else {
            $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonId);
        }

        
        if ($fromDate && $toDate) {
            $select->where(new Between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAllInvoiceProductWithTaxesBySalesPersonIdsAndProIDs($productId , $salesPersonId , $fromDate = null, $toDate = null)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesPersonID','salesInvoiceIssuedDate'))
                ->join('salesPerson', 'salesInvoice.salesPersonID = salesPerson.salesPersonID', array('salesPersonSortName'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID',array('totalTax' => new Expression('SUM(salesInvoiceProductTaxAmount)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0 ))
                ->group('salesInvoiceProductTax.salesInvoiceProductID');
        
        $select->where->in('salesInvoiceProduct.productID', $productId);
        $select->where->in('salesInvoice.salesPersonID', $salesPersonId);
        
        if ($fromDate && $toDate) {
            $select->where(new Between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function checkProductCopiedFromDeliveryNote($invoiceProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoiceProduct');
        $select->where(array('salesInvoiceProductID' => $invoiceProductID));
        $select->where(array('documentTypeID' => 4));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function updateSalesInvoicePoductStaus($data, $invoiceproductID)
    {
        try {
            $this->tableGateway->update($data, array('salesInvoiceProductID' => $invoiceproductID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function checkAnyInvoicesUsedThisProductAfterThisInvoiceID($invoiceID , $serialID, $batchID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
               ->columns(array('salesInvoiceID'))
               ->join('salesInvoice', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceCode'),'left')
               ->join('salesInvoiceSubProduct', 'salesInvoiceSubProduct.salesInvocieProductID = salesInvoiceProduct.salesInvoiceProductID', array('*'),'left');
        $select->where->notEqualTo('salesInvoice.statusID',5);
        $select->where->greaterThan('salesInvoiceProduct.salesInvoiceID', $invoiceID);
        if (!is_null($serialID)) {
            $select->where(array('salesInvoiceSubProduct.productSerialID' => $serialID));
        }
        if (!is_null($batchID)) {
            $select->where(array('salesInvoiceSubProduct.productBatchID' => $batchID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }


    public function getInvoiceItemTaxDetailsByInviceID($invoiceID)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('salesInvoiceProductTax', 'salesInvoiceProductTax.salesInvoiceProductID =  salesInvoiceProduct.salesInvoiceProductID', array('*'), 'left')
                ->where(array('salesInvoiceID' => $invoiceID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getAllInvoiceProductWithTaxeDetailsBySalesPersonIdsAndProIDs($productId , $salesPersonId , $fromDate = null, $toDate = null, $withDefaultSalesPerson = false)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesPersonID','salesInvoiceIssuedDate'))
                ->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left')
                ->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID',array('*'), 'left')
                ->join('tax', 'salesInvoiceProductTax.taxID = tax.id',array('*'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0 ));
        
        $select->where->in('salesInvoiceProduct.productID', $productId);
        if($withDefaultSalesPerson) {
                $select->where->AND->NEST
                ->in('invoiceSalesPersons.salesPersonID', $salesPersonId)->orPredicate(
                    new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                );
        } else {
            $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonId);
        }

        
        if ($fromDate && $toDate) {
            $select->where(new Between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateSalesInvoicePoductBySalesInvoiceID($data, $invoiceID)
    {
        try {
            $this->tableGateway->update($data, array('salesInvoiceID' => $invoiceID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    public function getInvoiceProductTaxDetailsByInvoiceProductId($invoiceProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->join("salesInvoiceProductTax", "salesInvoiceProduct.salesInvoiceProductID= salesInvoiceProductTax.salesInvoiceProductID", array("salesInvoiceProductTaxID", "taxID", "salesInvoiceProductTaxPrecentage", "salesInvoiceProductTaxAmount"), "left")
                ->join("tax", "salesInvoiceProductTax.taxID = tax.id", array("*"), "left")
                ->where(array("salesInvoiceProduct.salesInvoiceProductID" => $invoiceProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function gelSalesInvoiceProductDetailsByInvoiceIDAndLocationProductIDForDashboard($invoiceID, $locationProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("salesInvoiceProductID"))
                ->where(array("salesInvoiceID" => $invoiceID, 'locationProductID' => $locationProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}
