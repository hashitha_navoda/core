<?php

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class DraftInvoiceTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDraftInvoice($data)
    {

        $data = [
            'customerID' => $data->customerID,
            'invoiceValue' => $data->invoiceValue,
            'attachment' => $data->attachment,
            'productID' => $data->productID
        ];

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getTempInvoiceDataByID($draftInvoiceID, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('draftInvoice')
                ->columns(array('*'))
                ->join("customer", "draftInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("product", "draftInvoice.productID =  product.productID", array("*"), "left")
                ->join("locationProduct", "locationProduct.productID =  product.productID", array("*"), "left")
                ->where(array('draftInvoiceID' => $draftInvoiceID, 'locationProduct.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    public function getTempInvoiceAttachemnt($draftInvoiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('draftInvoice')
                ->columns(array('*'))
                ->where(array('draftInvoiceID' => $draftInvoiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }   
}
