<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoiceProduct implements InputFilterAwareInterface
{

    public $salesInvoiceProductID;
    public $salesInvoiceID;
    public $productID;
    public $salesInvoiceProductDescription;
    public $salesInvoiceProductPrice;
    public $inclusiveTaxSalesInvoiceProductPrice;
    public $salesInvoiceProductQuantity;
    public $salesInvoiceProductTotal;
    public $salesInvoiceProductDiscount;
    public $salesInvoiceProductDiscountType;
    public $salesInvoiceProductTax;
    public $salesInvoiceProductSelectedUomId;
    public $salesInvoiceProductMrpType;
    public $salesInvoiceProductMrpValue;
    public $salesInvoiceProductMrpPercentage;
    public $salesInvoiceProductMrpAmount;
    public $itemSalesPersonID;
    public $isFreeItem;
    public $hasFreeItem;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->salesInvoiceProductID = (!empty($data['salesInvoiceProductID'])) ? $data['salesInvoiceProductID'] : null;
        $this->salesInvoiceID = (!empty($data['salesInvoiceID'])) ? $data['salesInvoiceID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->salesInvoiceProductDescription = (!empty($data['salesInvoiceProductDescription'])) ? $data['salesInvoiceProductDescription'] : null;
        $this->salesInvoiceProductPrice = (!empty($data['salesInvoiceProductPrice'])) ? $data['salesInvoiceProductPrice'] : null;
        $this->inclusiveTaxSalesInvoiceProductPrice = (!empty($data['inclusiveTaxSalesInvoiceProductPrice'])) ? $data['inclusiveTaxSalesInvoiceProductPrice'] : null;
        $this->salesInvoiceProductQuantity = (!empty($data['salesInvoiceProductQuantity'])) ? $data['salesInvoiceProductQuantity'] : null;
        $this->salesInvoiceProductTotal = (!empty($data['salesInvoiceProductTotal'])) ? $data['salesInvoiceProductTotal'] : null;
        $this->salesInvoiceProductDiscount = (!empty($data['salesInvoiceProductDiscount'])) ? $data['salesInvoiceProductDiscount'] : null;
        $this->salesInvoiceProductDiscountType = (!empty($data['salesInvoiceProductDiscountType'])) ? $data['salesInvoiceProductDiscountType'] : null;
        $this->salesInvoiceProductTax = (!empty($data['salesInvoiceProductTax'])) ? $data['salesInvoiceProductTax'] : null;
        $this->documentTypeID = (!empty($data['documentTypeID'])) ? $data['documentTypeID'] : null;
        $this->salesInvoiceProductDocumentID = (!empty($data['salesInvoiceProductDocumentID'])) ? $data['salesInvoiceProductDocumentID'] : null;
        $this->salesInvoiceProductSelectedUomId = (!empty($data['salesInvoiceProductSelectedUomId'])) ? $data['salesInvoiceProductSelectedUomId'] : null;
        $this->itemSalesPersonID = (!empty($data['itemSalesPersonID'])) ? $data['itemSalesPersonID'] : null;
        $this->salesInvoiceProductMrpType = (!empty($data['salesInvoiceProductMrpType'])) ? $data['salesInvoiceProductMrpType'] : null;
        $this->salesInvoiceProductMrpValue = (!empty($data['salesInvoiceProductMrpValue'])) ? $data['salesInvoiceProductMrpValue'] : null;
        $this->salesInvoiceProductMrpPercentage = (!empty($data['salesInvoiceProductMrpPercentage'])) ? $data['salesInvoiceProductMrpPercentage'] : null;
        $this->salesInvoiceProductMrpAmount = (!empty($data['salesInvoiceProductMrpAmount'])) ? $data['salesInvoiceProductMrpAmount'] : null;
        $this->isFreeItem = (!empty($data['isFreeItem'])) ? $data['isFreeItem'] : 0;
        $this->hasFreeItem = (!empty($data['hasFreeItem'])) ? $data['hasFreeItem'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
