<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethod related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class IncomingPaymentMethodTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function savePaymentMethod(IncomingPaymentMethod $incomingPaymentMethod)
    {
        $paymentMethodData = array(
            'incomingPaymentId' => $incomingPaymentMethod->incomingPaymentId,
            'incomingPaymentMethodAmount' => $incomingPaymentMethod->incomingPaymentMethodAmount,
            'incomingPaymentMethodCashId' => $incomingPaymentMethod->incomingPaymentMethodCashId,
            'incomingPaymentMethodChequeId' => $incomingPaymentMethod->incomingPaymentMethodChequeId,
            'incomingPaymentMethodCreditCardId' => $incomingPaymentMethod->incomingPaymentMethodCreditCardId,
            'incomingPaymentMethodBankTransferId' => $incomingPaymentMethod->incomingPaymentMethodBankTransferId,
            'incomingPaymentMethodLoyaltyCardId' => $incomingPaymentMethod->incomingPaymentMethodLoyaltyCardId,
            'incomingPaymentMethodGiftCardId' => $incomingPaymentMethod->incomingPaymentMethodGiftCardId,
            'incomingPaymentMethodLCId' => $incomingPaymentMethod->incomingPaymentMethodLCId,
            'incomingPaymentMethodTTId' => $incomingPaymentMethod->incomingPaymentMethodTTId,
            'incomingPaymentMethodUniformVoucherId' => $incomingPaymentMethod->incomingPaymentMethodUniformVoucherId,
            'entityId' => $incomingPaymentMethod->entityId,
        );
        if ($this->tableGateway->insert($paymentMethodData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get incoming payment Method By paymentID
    public function getPaymentMethodByPaymentId($paymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethod')
                ->columns(array('*'))
                ->where(array('incomingPaymentMethod.incomingPaymentId' => $paymentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    //get incoming payment Method all details By paymentID
    public function getPaymentMethodAllDetailsByPaymentId($paymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethod')
                ->columns(array('*'))
                ->join('incomingPaymentMethodBankTransfer', 'incomingPaymentMethod.incomingPaymentMethodBankTransferId=incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId', array('*'), 'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId=incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left')
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId=incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId=incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                ->join('incomingPaymentMethodGiftCard', 'incomingPaymentMethod.incomingPaymentMethodGiftCardId=incomingPaymentMethodGiftCard.incomingPaymentMethodGiftCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLoyaltyCard', 'incomingPaymentMethod.incomingPaymentMethodLoyaltyCardId=incomingPaymentMethodLoyaltyCard.incomingPaymentMethodLoyaltyCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLC', 'incomingPaymentMethod.incomingPaymentMethodLCId=incomingPaymentMethodLC.incomingPaymentMethodLCId', array('*'), 'left')
                ->join('incomingPaymentMethodTT', 'incomingPaymentMethod.incomingPaymentMethodTTId=incomingPaymentMethodTT.incomingPaymentMethodTTId', array('*'), 'left')
                ->where(array('incomingPaymentMethod.incomingPaymentId' => $paymentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    public function getPosCashPayments($fromDate = NULL, $toDate = NULL, $userIds = NULL, $locationIds = NULL, $invoiceStatusIds = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('incomingInvoicePayment', 'incomingInvoicePayment.salesInvoiceID = salesInvoice.salesInvoiceID', array('*'), 'left');
        $select->join('incomingPaymentMethod', 'incomingInvoicePayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left');
        $select->join('incomingPayment', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('*'), 'left');
        $select->join('user', 'user.userID = entity.createdBy', array('*'), 'left');
        $select->where->equalTo('salesInvoice.pos', '1');
        $select->where->notEqualTo('incomingPaymentMethod.incomingPaymentMethodCashId', '0');
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if ($userIds) {
            $select->where->in('entity.createdBy', $userIds);
        }
        if ($invoiceStatusIds) {
            $select->where->in('salesInvoice.statusID', $invoiceStatusIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    public function getPosCardPayments($fromDate = NULL, $toDate = NULL, $userIds = NULL, $locationIds = NULL, $invoiceStatusIds = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('incomingInvoicePayment', 'incomingInvoicePayment.salesInvoiceID = salesInvoice.salesInvoiceID', array('*'), 'left');
        $select->join('incomingPaymentMethod', 'incomingInvoicePayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left');
        $select->join('incomingPayment', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('*'), 'left');
        $select->join('user', 'user.userID = entity.createdBy', array('*'), 'left');
        $select->where->equalTo('salesInvoice.pos', '1');
        $select->where->notEqualTo('incomingPaymentMethod.incomingPaymentMethodCreditCardId', '0');
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if ($userIds) {
            $select->where->in('entity.createdBy', $userIds);
        }
        if ($invoiceStatusIds) {
            $select->where->in('salesInvoice.statusID', $invoiceStatusIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    public function getOtherPosPayments($fromDate = NULL, $toDate = NULL, $userIds = NULL, $locationIds = NULL, $invoiceStatusIds = NULL, $paymentStatus = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('incomingInvoicePayment', 'incomingInvoicePayment.salesInvoiceID = salesInvoice.salesInvoiceID', array('*'), 'left');
        $select->join('incomingPaymentMethod', 'incomingInvoicePayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left');
        $select->join('incomingPayment', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('*'), 'left');
        $select->join('user', 'user.userID = entity.createdBy', array('*'), 'left');
        $select->where->equalTo('salesInvoice.pos', '1');
        $select->where->equalTo('incomingPaymentMethod.incomingPaymentMethodCashId', '0');
        $select->where->equalTo('incomingPaymentMethod.incomingPaymentMethodCreditCardId', '0');
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if ($userIds) {
            $select->where->in('entity.createdBy', $userIds);
        }
        if ($invoiceStatusIds) {
            $select->where->in('salesInvoice.statusID', $invoiceStatusIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    /**
     * Get invoice payment details by cheque id
     * @param int $chequeId
     * @return mixed
     */
    public function getInvoicePaymentDeatailsByChequeId($chequeId) 
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethod')
                ->columns(array('*'))
                ->join('incomingInvoicePayment', 'incomingPaymentMethod.incomingPaymentId = incomingInvoicePayment.incomingPaymentID', array('*'), 'left')
                ->order(['incomingInvoicePayment.salesInvoiceID DESC'])
                ->where->equalTo('incomingPaymentMethod.incomingPaymentMethodChequeId', $chequeId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /*
    * This function is used to update incoming payment method table by incoming paymentID
    */
    public function updateIncomingPaymentAmountByPaymentID($data, $paymentID)
    {
        return $this->tableGateway->update($data, array('incomingPaymentID' => $paymentID));
    }

}
