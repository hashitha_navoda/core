<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethodCreditCard
{

    public $incomingPaymentMethodCreditCardId;
    public $incomingPaymentMethodCreditCardNumber;
    public $incomingPaymentMethodCreditReceiptNumber;
    public $cardAccountID;
    public $cardTypeID;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodCreditCardId = (!empty($data['incomingPaymentMethodCreditCardId'])) ? $data['incomingPaymentMethodCreditCardId'] : null;
        $this->incomingPaymentMethodCreditCardNumber = (!empty($data['incomingPaymentMethodCreditCardNumber'])) ? $data['incomingPaymentMethodCreditCardNumber'] : null;
        $this->incomingPaymentMethodCreditReceiptNumber = (!empty($data['incomingPaymentMethodCreditReceiptNumber'])) ? $data['incomingPaymentMethodCreditReceiptNumber'] : null;
        $this->cardAccountID = (!empty($data['cardAccountID'])) ? $data['cardAccountID'] : null;
        $this->cardTypeID = (!empty($data['cardTypeID'])) ? $data['cardTypeID'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
