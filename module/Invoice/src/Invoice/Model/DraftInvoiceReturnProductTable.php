<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains creditc note product Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DraftInvoiceReturnProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getInvoiceReturnProductsByInvoiceID($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvoiceReturnProduct")
                ->columns(array("*"))
                ->join("draftInvoiceReturns", "draftInvoiceReturnProduct.draftInvoiceReturnID= draftInvoiceReturns.draftInvoiceReturnID", array("*"), "left")
                ->join("salesInvoiceProduct", "draftInvoiceReturnProduct.invoiceProductID= salesInvoiceProduct.salesInvoiceProductID", array("*"), "left")
                ->join('product', 'draftInvoiceReturnProduct.productID =  product.productID', array('*'), 'left')
                ->join('locationProduct', 'locationProduct.locationProductID =  salesInvoiceProduct.locationProductID', array('*'), 'left')
                ->where(array("salesInvoiceProduct.salesInvoiceID" => $invoiceID));
        $select->where->notEqualTo('draftInvoiceReturns.statusID',4);
        $select->where->notEqualTo('draftInvoiceReturns.statusID',5);
        $select->where->notEqualTo('draftInvoiceReturns.statusID',10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoiceReturnProductAndSubProductDataByInvoiceProductID($invoiceProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvoiceReturnProduct")
                ->columns(array("*"))
                ->join('draftInvoiceReturnSubProduct', 'draftInvoiceReturnSubProduct.draftInvoiceReturnProductID=draftInvoiceReturnProduct.draftInvoiceReturnProductID', array('draftInvoiceReturnSubProductID', 'productBatchID', 'productSerialID', 'invoiceReturnSubProductQuantity'), 'left')
                ->join('draftInvoiceReturns', 'draftInvoiceReturnProduct.draftInvoiceReturnID=draftInvoiceReturns.draftInvoiceReturnID', array('*'), 'left')
                ->join('entity', 'draftInvoiceReturns.entityID=entity.entityID', array('*'), 'left')
                ->where(array('draftInvoiceReturnProduct.invoiceProductID' => $invoiceProductID, 'entity.deleted' => 0));
        $select->where->notEqualTo('draftInvoiceReturns.statusID',4);
        $select->where->notEqualTo('draftInvoiceReturns.statusID',5);
        $select->where->notEqualTo('draftInvoiceReturns.statusID',10);
        $select->where->notEqualTo('draftInvoiceReturns.statusID',16);
        $select->where->notEqualTo('draftInvoiceReturns.statusID',13);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function saveInvoiceReturnProducts(DraftInvoiceReturnProduct $draftInvoiceReturnProduct)
    {
        $data = array(
            'draftInvoiceReturnID' => $draftInvoiceReturnProduct->draftInvoiceReturnID,
            'invoiceProductID' => $draftInvoiceReturnProduct->invoiceProductID,
            'productID' => $draftInvoiceReturnProduct->productID,
            'invoiceReturnProductPrice' => $draftInvoiceReturnProduct->invoiceReturnProductPrice,
            'invoiceReturnProductDiscount' => $draftInvoiceReturnProduct->invoiceReturnProductDiscount,
            'invoiceReturnProductDiscountType' => $draftInvoiceReturnProduct->invoiceReturnProductDiscountType,
            'invoiceReturnProductQuantity' => $draftInvoiceReturnProduct->invoiceReturnProductQuantity,
            'invoiceReturnProductTotal' => $draftInvoiceReturnProduct->invoiceReturnProductTotal,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllInvoiceReturnProductDetailsByInvoiceReturnID($draftInvoiceReturnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('draftInvoiceReturnProduct');
        $select->join(
                'product', 'draftInvoiceReturnProduct.productID = product.productID', array(
            '*',
                )
        );
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->where(array('productUomBase' => '1'));
        $select->where(array('draftInvoiceReturnProduct.draftInvoiceReturnID' => $draftInvoiceReturnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

   
    public function getInvoiceReturnDetailsByInvoiceReturnID($creditNoteID , $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('draftInvoiceReturnProduct')
               ->columns(array('*','invoiceReturnProID' => new Expression('draftInvoiceReturnProduct.draftInvoiceReturnProductID')))
               ->join('draftInvoiceReturns', 'draftInvoiceReturns.draftInvoiceReturnID = draftInvoiceReturnProduct.draftInvoiceReturnID', array('locationID','invoiceID'),'left')
               ->join('locationProduct', 'locationProduct.productID = draftInvoiceReturnProduct.productID', array('locationProductID'),'left')
               ->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductID = draftInvoiceReturnProduct.invoiceProductID', array('documentTypeID','salesInvoiceProductDocumentID'),'left')
               ->join('invoiceReturnSubProduct', 'invoiceReturnSubProduct.draftInvoiceReturnProductID = draftInvoiceReturnProduct.draftInvoiceReturnProductID', array('*'),'left');
        $select->where(array('draftInvoiceReturnProduct.draftInvoiceReturnID' => $creditNoteID));
        $select->where(array('locationProduct.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function checkAnyInvoiceReturnUsedThisProductAfterThisInvoiceReturnID($draftInvoiceReturnID , $serialID, $batchID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('draftInvoiceReturnProduct')
               ->columns(array('draftInvoiceReturnID'))
               ->join('draftInvoiceReturns', 'draftInvoiceReturns.draftInvoiceReturnID = draftInvoiceReturnProduct.draftInvoiceReturnID', array('invoiceID'),'left')
               ->join('invoiceReturnSubProduct', 'invoiceReturnSubProduct.draftInvoiceReturnProductID = draftInvoiceReturnProduct.draftInvoiceReturnProductID', array('*'),'left');
        $select->where->notEqualTo('draftInvoiceReturns.statusID',5);
        $select->where->greaterThan('draftInvoiceReturnProduct.draftInvoiceReturnID', $draftInvoiceReturnID);
        if (!is_null($serialID)) {
            $select->where(array('invoiceReturnSubProduct.productSerialID' => $serialID));
        }
        if (!is_null($batchID)) {
            $select->where(array('invoiceReturnSubProduct.productBatchID' => $batchID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
