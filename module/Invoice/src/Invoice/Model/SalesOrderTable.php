<?php

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class SalesOrderTable
{

    protected $tableGateway;

    function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($branch, $paginated = FALSE)
    {


        if ($paginated) {
            $select = new Select();
            $select->from('salesOrders')
//                    ->columns(array('*'))
                    ->columns(array('soID', 'soCode', 'quotationID', 'customerID', 'issuedDate', 'expireDate',
                        'payementTerm', 'taxAmount', 'totalDiscount', 'totalAmount', 'comment', 'branch',
                        'statusID', 'showTax', 'footerID', 'salesPersonID', 'entityID', 'salesOrdersCustomCurrencyRate'))
                    ->join('customer', 'salesOrders.customerID = customer.customerID', array('customerName', 'customerCode','customerStatus'), 'left')
                    ->join('currency', 'salesOrders.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
//                    ->join('status', 'salesOrders.statusID = status.statusID', array("stName" => "statusName"), 'left')
                    ->order(array('issuedDate' => 'DESC', 'soID' => 'DESC'))
                    ->where(array('branch' => $branch));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );

            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrders');
        $select->columns(array('*'));
        $select->where(array('branch' => $branch));
        $statement = $sql->prepareStatementForSqlObject($select);
        $resultSet = $statement->execute();

        return $resultSet;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @param type $salesOrdersSearchKey
     * @return type
     */
    public function searchSalesOrdersForDropDown($locationID, $salesOrdersSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrders');
        $select->columns(array('soCode', 'soID', 'statusID'));
        $select->where(array('salesOrders.branch' => $locationID));
        $select->where->like('soCode', '%' . $salesOrdersSearchKey . '%');
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getSalesOrderforSearch($salesOrderID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesOrders")
                ->columns(array("*"))
                ->join("customer", "salesOrders.customerID = customer.customerID", array("*"), "left")
                ->join("status", "salesOrders.statusID = status.statusID", array("*"), "left")
                ->join("currency", "salesOrders.customCurrencyId = currency.currencyID", array("*"), "left")
                ->where(array("soID" => $salesOrderID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getSalesOrdersByCustomerID($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesOrders")
                ->columns(array("*"))
                ->join("customer", "salesOrders.customerID = customer.customerID", array("*"), "left")
                ->join("status", "salesOrders.statusID = status.statusID", array("*"), "left")
                ->join("currency", "salesOrders.customCurrencyId = currency.currencyID", array("*"), "left")
                ->order(array('issuedDate' => 'DESC'))
                ->where(array('salesOrders.customerID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getSalesOrdersByDate($fromdate, $todate, $branch, $customerID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesOrders")
                ->columns(array("*"))
                ->join("customer", "salesOrders.customerID = customer.customerID", array("*"), "left")
                ->join("status", "salesOrders.statusID = status.statusID", array("*"), "left")
                ->join("currency", "salesOrders.customCurrencyId = currency.currencyID", array("*"), "left")
                ->order(array('issuedDate' => 'DESC'))
                ->where(array("branch" => $branch));
        if ($customerID) {
            $select->where(array('salesOrders.customerID' => $customerID));
        }
        $select->where->between('issuedDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getSalesOrderIds()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->Select();
        $select->from('salesOrder');
        $select->columns(array("id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function updateSalesOrderStateAndComment(SalesOrder $salesorder)
    {
        $data = array(
            'id' => $salesorder->sales_order_no,
            'comment' => $salesorder->comment,
            'state' => $salesorder->statusID
        );
        $this->tableGateway->update($data, array('id' => $salesorder->sales_order_no));
        return TRUE;
    }

    public function getSalesOrdersByID($soID)
    {
        $rowset = $this->tableGateway->select(array('soID' => $soID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getSalesOrdersByCode($soCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesOrders');
        $select->join('customer', 'salesOrders.customerID = customer.customerID', array('*'));
//        $select->join('location', 'salesInvoice.locationID = location.locationID', array('*'));
        $select->join('paymentTerm', 'salesOrders.payementTerm = paymentTerm.paymentTermID', array('*'));
//        $select->join('deliveryNote', 'salesInvoice.deliveryNoteID = deliveryNote.deliveryNoteID', array('*'), 'left');
        $select->where(array('soCode' => $soCode));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getSalesOrderBySoCode($salesOrderCode, $notDeleted = TRUE)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesOrders');
        $select->join('customer', 'salesOrders.customerID = customer.customerID', array('*'));
        $select->join('paymentTerm', 'salesOrders.payementTerm = paymentTerm.paymentTermID', array('*'));
        $select->join('entity', 'salesOrders.entityID = entity.entityID', array('deleted'));
        $select->where(array('soCode' => $salesOrderCode));
        if ($notDeleted) {
            $select->where(array('deleted' => 0));
        }

        $query = $sql->prepareStatementForSqlObject($select);
        $results = $query->execute();
        $resultsArray = $results->current();
        return $resultsArray;
    }

    public function getSalesOrderBySoID($salesOrderID, $notDeleted = FALSE)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesOrders');
        $select->join('customer', 'salesOrders.customerID = customer.customerID', array('*'));
        $select->join('paymentTerm', 'salesOrders.payementTerm = paymentTerm.paymentTermID', array('*'));
        $select->join('entity', 'salesOrders.entityID = entity.entityID', array('deleted','createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->join('customerProfile', 'customerProfile.customerID = customer.customerID', array('*'), 'left');
        $select->join('quotation', 'quotation.quotationID = salesOrders.quotationID', array('quotationCode'), 'left');
        $select->where(array('soID' => $salesOrderID, 'customerProfile.isPrimary' => 1));
        if ($notDeleted) {
            $select->where(array('deleted' => 0));
        }

        $query = $sql->prepareStatementForSqlObject($select);
        $results = $query->execute();
        return $results;
    }

    public function getSalesOrdersByQuotationID($quotationID)
    {

        $rowset = $this->tableGateway->select(array('quotationID' => $quotationID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }

        return $row;
    }

    public function saveSalesOrders(SalesOrder $sales_order)
    {
        $data = array(
            'soCode' => $sales_order->soCode,
            'quotationID' => $sales_order->quotationID,
            'customerName' => $sales_order->customerName,
            'customerID' => $sales_order->customerID,
            'issuedDate' => $sales_order->issuedDate,
            'expireDate' => $sales_order->expireDate,
            'payementTerm' => $sales_order->payementTerm,
            'taxAmount' => $sales_order->taxAmount,
            'totalDiscount' => $sales_order->totalDiscount,
            'totalAmount' => $sales_order->totalAmount,
            'comment' => $sales_order->comment,
            'branch' => $sales_order->branch,
            'statusID' => $sales_order->statusID,
            'showTax' => $sales_order->showTax,
            'footerID' => $sales_order->footerID,
            'entityID' => $sales_order->entityID,
            'salesPersonID' => $sales_order->salesPersonID,
            'customCurrencyId' => $sales_order->customCurrencyId,
            'salesOrdersCustomCurrencyRate' => $sales_order->salesOrdersCustomCurrencyRate,
            'priceListId' => $sales_order->priceListId,
            'customerProfileID' => $sales_order->customerProfileID,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getSalesOrderBySalesOrderCode($id)
    {
        $rowset = $this->tableGateway->select(array('soCode' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function checkSalesOrderByCode($salesOrderCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('salesOrders')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "salesOrders.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('salesOrders.soCode' => $salesOrderCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function updateSalesOrder(SalesOrder $salesorder)
    {
        $data = array(
            'expireDate' => $salesorder->expireDate,
            'payementTerm' => $salesorder->payementTerm,
            'totalDiscount' => $salesorder->totalDiscount,
            'totalAmount' => $salesorder->totalAmount,
            'comment' => $salesorder->comment,
            'showTax' => $salesorder->showTax,
            'salesPersonID' => $salesorder->salesPersonID,
            'priceListId' => $salesorder->priceListId,
        );
        $this->tableGateway->update($data, array('soID' => $salesorder->soID));
        return TRUE;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param type $salesOrder
     * @return type
     */
    public function getSalesOrdersforSearch($salesOrder)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('id LIKE ?' => '%' . $salesOrder . '%'));
        $rowset = $this->tableGateway->selectWith($select);
        return $rowset;
    }

    public function getSalesOrderProductBysalesOrderID($salesOrderID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesOrders")
                ->columns(array("*"))
                ->join("salesOrdersProduct", "salesOrders.soID = salesOrdersProduct.soID", array("*"), "left")
                ->join("locationProduct", "salesOrdersProduct.locationProductID =  locationProduct.locationProductID", array("*"), "left")
                ->join("product", "locationProduct.productID =  product.productID", array("productTypeID"), "left")
                ->where(array("salesOrders.soID" => $salesOrderID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function updateSalesOrderStatus($salesOrderID, $closeStatusID)
    {
        $data = array(
            'statusID' => $closeStatusID
        );
        return $this->tableGateway->update($data, array('soID' => $salesOrderID));
    }

    /**
     * Get sales order details with sales person id
     * @param string $salesPersonId
     * @param string $toDate
     * @param string $fromDate
     * @param array $locationIds
     * @param array $customerIds
     * @param array $isAllCustomers
     * @return mixed
     */
    public function getSalesPersonWiseSalesOrderDetails($salesPersonId, $toDate, $fromDate, $locationIds, $customerIds, $isAllCustomers, $cusCategory = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesOrders")
            ->columns(['*'])
                ->join("customer", "salesOrders.customerID = customer.customerID", ['customerName','customerCode'], "left")
                ->join("entity", "salesOrders.entityID = entity.entityID", ['deleted'], "left")
                ->where->equalTo('salesOrders.salesPersonID', $salesPersonId)
                ->where->equalTo('entity.deleted', 0)
                ->where->in('salesOrders.branch', $locationIds)
                ->where->between('salesOrders.issuedDate', $fromDate, $toDate);
                if(!$isAllCustomers){
                    $select->where->in('salesOrders.customerID', $customerIds);
                }
                if($cusCategory != ''){
                    $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
                    $select->where->in('customer.customerCategory', $cusCategory);
                }
                $select->order(['salesOrders.soID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

     public function getSalesOrderWiseProducts($salesOrderIds = [], $fromDate, $toDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrders')
                ->columns([
                    'soID',
                    'soCode',
                    'issuedDate',
                    'expireDate',
                    ])
                ->join('salesOrdersProduct', 'salesOrders.soID = salesOrdersProduct.soID', [
                    'soTotalQty' => new Expression('sum(quantity)'),
                    'soProductID',
                    'locationProductID'
                    ], 'left')
                ->join('entity', 'salesOrders.entityID = entity.entityID', [
                    'deleted',
                ])
                ->where->equalTo('entity.deleted', 0);
        if ($salesOrderIds) {
            $select->where->in('salesOrders.soID', $salesOrderIds);
        }
        if ($fromDate && $toDate) {
            $select->where->between('issuedDate', $fromDate, $toDate);
        }
        $select->group(['soID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    function updateSalesOrderTotalBysalesOrderID($dataSet = [])
    {
        $this->tableGateway->update($dataSet, array('soID' => $dataSet['soID']));
        return TRUE;
    }

    // get quotation related sales orders by quotation id
    public function getQuotationRelatedSalesOrderDataByQuotationId($quotationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrders');
        $select->columns(array('*'));
        $select->join('entity', 'salesOrders.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesOrders.quotationID' => $quotationID));
        $select->group("salesOrders.soID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get sales order basic details with time stamp by sales order id 
    public function getSalesOrderDetailsBySalesOrderId($soID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrders');
        $select->columns(array('*'));
        $select->join('entity', 'salesOrders.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesOrders.soID' => $soID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;
    }

    // get dln related sales orders by dln id
    public function getDlnRelatedSalesOrderDataBydlnId($deliveryNoteID = null, $invoiceID = null, $returnID = null, $creditNoteID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrders');
        $select->columns(array('*'));
        $select->join('entity', 'salesOrders.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($deliveryNoteID)) {
            $select->join('deliveryNote', 'salesOrders.soID = deliveryNote.salesOrderID', array('*'),'left');
            $select->where(array('deliveryNote.deliveryNoteID' => $deliveryNoteID));
        }
        if (!is_null($invoiceID)) {
            $select->join('deliveryNote', 'salesOrders.soID = deliveryNote.salesOrderID', array('*'),'left');
            $select->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
            $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
            $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($returnID)) {
            $select->join('deliveryNote', 'salesOrders.soID = deliveryNote.salesOrderID', array('*'),'left');
            $select->join('deliveryNoteProduct', "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array('*'), 'left');
            $select->join('salesReturnProduct', "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array('*'), 'left');
            $select->join('salesReturn', "salesReturn.salesReturnID = salesReturnProduct.salesReturnID", array('*'), 'left');
            $select->where(array('salesReturn.salesReturnID' => $returnID));
        }
        if (!is_null($creditNoteID)) {
            $select->join('salesInvoice', 'salesOrders.soID = salesInvoice.salesOrderID', array('*'),'left');
            $select->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
            $select->where(array('creditNote.creditNoteID' => $creditNoteID));
        }
        $select->group("salesOrders.soID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get invoice related sales orders by invoice id
    public function getInvoiceRelatedSalesOrderDataByInvoiceId($invoiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrders');
        $select->columns(array('*'));
        $select->join('salesInvoice', 'salesOrders.soID = salesInvoice.salesOrderID', array('*'),'left');
        $select->join('entity', 'salesOrders.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID));
        $select->group("salesOrders.soID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }


    // get dln related sales orders by credit note id
    public function getDlnRelatedSalesOrderDataBycreditNoteId($creditNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrders');
        $select->columns(array('*'));
        $select->join('entity', 'salesOrders.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->join('deliveryNote', 'salesOrders.soID = deliveryNote.salesOrderID', array('*'),'left');
        $select->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
        $select->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
        $select->where(array('creditNote.creditNoteID' => $creditNoteID, 'salesInvoiceProduct.documentTypeID' => 4));
        $select->group("salesOrders.soID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get sales order and related invoice total
    public function getSalesOrderDataByDueDateRange($fromDate, $toDate, $status)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrders');
        $select->columns(array('*'));
        $select->join('status', 'salesOrders.statusID = status.statusID', array("stName" => "statusName"), 'left');
        $select->join('salesInvoice', 'salesOrders.soID = salesInvoice.salesOrderID', array('salesInvoiceTotalAmount' => new Expression('sum(salesinvoiceTotalAmount)'),'salesInvoiceTotalPaidAmount' => new Expression('sum(salesInvoicePayedAmount)')),'left');
        $select->where->between('expireDate', $fromDate, $toDate);
        $select->group("salesOrders.soID");

        if ($status == "open") {
            $select->where->in('salesOrders.statusID',[3,6]);
        } else if ($status == "close") {
            $select->where(array('salesOrders.statusID' => 4));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }
}
