<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethodBankTransfer
{

    public $incomingPaymentMethodBankTransferId;
    public $incomingPaymentMethodBankTransferBankId;
    public $incomingPaymentMethodBankTransferAccountId;
    public $incomingPaymentMethodBankTransferCustomerBankName;
    public $incomingPaymentMethodBankTransferCustomerAccountNumber;
    public $incomingPaymentMethodBankTransferReconciliationStatus;
    public $incomingPaymentMethodBankTransferRef;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodBankTransferId = (!empty($data['incomingPaymentMethodBankTransferId'])) ? $data['incomingPaymentMethodBankTransferId'] : null;
        $this->incomingPaymentMethodBankTransferBankId = (!empty($data['incomingPaymentMethodBankTransferBankId'])) ? $data['incomingPaymentMethodBankTransferBankId'] : null;
        $this->incomingPaymentMethodBankTransferAccountId = (!empty($data['incomingPaymentMethodBankTransferAccountId'])) ? $data['incomingPaymentMethodBankTransferAccountId'] : null;
        $this->incomingPaymentMethodBankTransferCustomerBankName = (!empty($data['incomingPaymentMethodBankTransferCustomerBankName'])) ? $data['incomingPaymentMethodBankTransferCustomerBankName'] : null;
        $this->incomingPaymentMethodBankTransferCustomerAccountNumber = (!empty($data['incomingPaymentMethodBankTransferCustomerAccountNumber'])) ? $data['incomingPaymentMethodBankTransferCustomerAccountNumber'] : null;
        $this->incomingPaymentMethodBankTransferReconciliationStatus = (!empty($data['incomingPaymentMethodBankTransferReconciliationStatus'])) ? $data['incomingPaymentMethodBankTransferReconciliationStatus'] : null;
        $this->incomingPaymentMethodBankTransferRef = (!empty($data['incomingPaymentMethodBankTransferRef'])) ? $data['incomingPaymentMethodBankTransferRef'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
