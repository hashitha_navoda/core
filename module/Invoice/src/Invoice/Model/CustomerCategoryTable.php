<?php

/**
 * @author Sharmilan <sharmilan@thinkcube.com>
 * This file contains Customer Category related database operations
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Predicate\Like;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class CustomerCategoryTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerCategory')
                ->columns(array('*'))
                ->order('customerCategoryName ASC');

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function fetchAllForDorpDown()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerCategory')
                ->columns(array('*'))
                ->order('customerCategoryName ASC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $data = array();
        foreach ($results as $value) {
            $data[$value['customerCategoryID']] = $value['customerCategoryName'];
        }
        return $data;
    }

    public function saveCustomerCategory(CustomerCategory $customerCategory)
    {
        $data = [
            'customerCategoryName' => $customerCategory->customerCategoryName,
        ];

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateCustomerCategory(CustomerCategory $customerCategory)
    {
        try {
            $data = array(
                'customerCategoryName' => $customerCategory->customerCategoryName,
            );
            $this->tableGateway->update($data, array('customerCategoryID' => $customerCategory->customerCategoryID));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function getByCustomerCategoryName($customerCategoryName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerCategory')
                ->columns(array('*'))
                ->where(array('customerCategoryName' => $customerCategoryName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $data = array();
        foreach ($results as $value) {
            $data[] = $value;
        }
        return $data;
    }

    public function getByCustomerCategoryID($customerCategoryID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerCategory')
                ->columns(array('*'))
                ->where(array('customercategoryID' => $customerCategoryID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $data = array();
        foreach ($results as $value) {
            $data[] = $value;
        }
        return $data;
    }

    public function searchByCustomerCategoryName($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerCategory');
        $select->columns(array('*'));
        $select->where->like('customerCategoryName', '%' . $searchKey . '%');
        $select->order('customerCategoryName ASC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $value) {
            $objectResults[] = (object) $value;
        }
        if (isset($objectResults)) {
            return (object) $objectResults;
        } else {
            return null;
        }
    }

    public function deleteCustomerCategory($customerCategoryID)
    {
        return $this->tableGateway->delete(array('customerCategoryID' => $customerCategoryID));
    }

    public function getCustomerByCategoryID($customerCategoryID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer');
        $select->columns(array('*'));
        $select->where(array('customerCategory' => $customerCategoryID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result->current();
        foreach ($results as $value) {
            $objectResults[] = (object) $value;
        }
        if (isset($objectResults)) {
            return (object) $objectResults;
        } else {
            return null;
        }
    }

    public function getByCustomerCategoryByCusCategoryName($customerCategoryName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerCategory')
                ->columns(array('*'))
                ->where(array('customerCategoryName' => $customerCategoryName));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results->current();
    }

}
