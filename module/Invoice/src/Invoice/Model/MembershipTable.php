<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class MembershipTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveMembership($membershipData)
    {
        $data = array(
            'invoiceID' => $membershipData->invoiceID,
            'nextInvoiceDate' => $membershipData->nextInvoiceDate,
            'comment' => $membershipData->comment
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {

            return FALSE;
        }
    }

    public function deleteMembershipByInvoiceID($invoiceID) {
        try {
            $result = $this->tableGateway->delete(array('invoiceID' => $invoiceID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }
}
