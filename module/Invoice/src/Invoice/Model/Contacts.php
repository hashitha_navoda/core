<?php

namespace Invoice\Model;

/**
 * Class DispatchNote
 * @package Invoice\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class Contacts
{
    
    public $contactID;
    public $firstName;
    public $lastName;
    public $title;
    public $designation;
    public $entityID;
    public $isConvertToCustomer;
    public $relatedCusID;

    public function exchangeArray($data)
    {
        $this->contactID      = (!empty($data['contactID'])) ? $data['contactID'] : null;
        $this->firstName    = (!empty($data['firstName'])) ? $data['firstName'] : null;
        $this->lastName    = (!empty($data['lastName'])) ? $data['lastName'] : null;
        $this->title = (!empty($data['title'])) ? $data['title'] : null;
        $this->designation = (!empty($data['designation'])) ? $data['designation'] : null;
        $this->entityID           = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->isConvertToCustomer       = (!empty($data['isConvertToCustomer'])) ? $data['isConvertToCustomer'] : 0;
        $this->relatedCusID       = (!empty($data['relatedCusID'])) ? $data['relatedCusID'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
