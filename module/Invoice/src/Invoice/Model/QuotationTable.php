<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class QuotationTable
{

    protected $tableGateway;

    function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }
//    public function fetchAll($paginated = FALSE)
//    {
//        if ($paginated) {
//
//            $select = new Select();
//            $select->from('quotation')
//                    ->columns(array('*'))
//                    ->order(array('issuedDate' => 'DESC'));
//            $resultSetPrototype = new ResultSet();
//            $resultSetPrototype->setArrayObjectPrototype(new Quotation());
//            $paginatorAdapter = new DbSelect(
//                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
//            );
//            $paginator = new Paginator($paginatorAdapter);
//            return $paginator;
//        }
//        $resultSet = $this->tableGateway->select();
//        return $resultSet;
//    }


    public function fetchAllByLocation($branch, $paginated = FALSE)
    {
        if ($paginated) {

            $sql = new sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('quotation')
                    ->columns(array('quotationID', 'quotationCode', 'quotationCustomerName', 'quotationIssuedDate', 'quotationExpireDate', 'quotationTotalAmount', 'quotationStatus', 'quotationCustomCurrencyRate'))
                    ->join('customer', "quotation.customerID = customer.customerID", array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->join(array('e' => 'entity'), "quotation.entityID = e.entityID", array('*'), 'left')
                    ->join('status', "status.statusID = quotation.quotationStatus", array('statusName'), 'left')
                    ->join('currency', 'quotation.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->where(array('quotationBranch' => $branch))
                    ->order(array('quotationIssuedDate' => 'DESC'))
                    ->order(array('quotationID' => 'DESC'))
                    ->where->notEqualTo("quotation.quotationStatus", 10);

            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            //$paginatorAdapter->buffer();   /////<--
//$paginatorAdapter->next(); /////// <-----
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "quotation.entityID = e.entityID")
                ->where(array('quotationBranch' => $branch))
                ->where(array('e.deleted' => '0'))
                ->order(array('quotationIssuedDate' => 'DESC'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function fetchAllByLocationAndStatus($branch, $status = 3)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "quotation.entityID = e.entityID")
                ->where(array('quotationBranch' => $branch))
                ->where(array('e.deleted' => '0'))
                ->where(array('quotation.quotationStatus' => $status))
                ->order(array('quotationIssuedDate' => 'DESC'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getQuotaionByCode($quotationCode)
    {

        $rowset = $this->tableGateway->select(array('quotationCode' => $quotationCode));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getQuotaionByID($quotationID)
    {
        $rowset = $this->tableGateway->select(array('quotationID' => $quotationID));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function checkQuotationByID($quotationID, $branch)
    {


        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('quotation')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "quotation.entityID = e.entityID")
                    ->where(array('quotationBranch' => $branch))
                    ->where(array('e.deleted' => '0'))
                    ->where(array('quotation.quotationID' => $quotationID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }


            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function checkQuotationByCode($quotationCode, $branch)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('quotation')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "quotation.entityID = e.entityID")
//                    ->where(array('quotationBranch' => $branch))
                    ->where(array('e.deleted' => '0'))
                    ->where(array('quotation.quotationCode' => $quotationCode))
                    ->where->notEqualTo("quotation.quotationStatus", 10);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            return false;
        }
    }



    public function getAllQuotationDetailsByQuotationCode($quotationCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation');
//        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('*'));
//        $select->join('location', 'salesInvoice.locationID = location.locationID', array('*'));
        $select->join('paymentTerm', 'quotation.quotationPayementTerm = paymentTerm.paymentTermID', array('*'));
        $select->where(array('quotationCode' => $quotationCode));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getAllQuotationDetailsByQuotationID($quotationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation');
//        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('*'));
//        $select->join('location', 'salesInvoice.locationID = location.locationID', array('*'));
        $select->join('paymentTerm', 'quotation.quotationPayementTerm = paymentTerm.paymentTermID', array('*'));
        $select->join('entity', 'quotation.entityID = entity.entityID', array('createdBy','createdTimeStamp'), 'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->join('salesPerson', 'quotation.salesPersonID = salesPerson.salesPersonID', array('salesPersonTitle','salesPersonFirstName','salesPersonLastName'),'left');
        $select->where(array('quotationID' => $quotationID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getQuotationforSearch($QuotationID, $branch)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "quotation.entityID = e.entityID")
                ->join('status', "status.statusID = quotation.quotationStatus", array('statusName'), 'left')
                ->join('customer', "quotation.customerID = customer.customerID", array('customerName', 'customerCode','customerStatus'), 'left')
                ->join('currency', "quotation.customCurrencyId = currency.currencyID", array('*'), 'left')
                ->where(array('quotationBranch' => $branch))
                ->where(array('quotationID' => $QuotationID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getQuotationsByDate($fromdate, $todate, $branch, $customerID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation')
                ->join('customer', "customer.customerID = quotation.customerID")
                ->join('currency', "quotation.customCurrencyId = currency.currencyID", array('*'), 'left')
                ->join('status', "status.statusID = quotation.quotationStatus", array('statusName'), 'left')
                ->join(array('e' => 'entity'), "quotation.entityID = e.entityID");
        $select->order(array('quotationIssuedDate' => "DESC"))
                ->order(array('quotationID' => "DESC"))
                ->where(array('quotationBranch' => $branch));
        if ($customerID != NULL) {
            $select->where(array('quotation.customerID' => $customerID));
        }
        $select->where->between('quotationIssuedDate', $fromdate, $todate);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getQuotaionByCustomerID($customerID, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "quotation.entityID = e.entityID")
                ->join('customer', "customer.customerID = quotation.customerID")
                ->join('currency', "quotation.customCurrencyId = currency.currencyID", array('*'), 'left')
                ->where(array('quotationBranch' => $locationID))
                ->where(array('quotation.customerID' => $customerID))
                ->order(array('quotationIssuedDate' => "DESC"))
                ->order(array('quotationID' => "DESC"));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getPaginatedQuotaionByCustomerID($customerID)
    {
        $select = new \Zend\Db\Sql\Select;
        $select->from('quotation')
                ->columns(array('*'))
                ->where(array('customerID' => $customerID));
        return new \Zend\Paginator\Paginator(
                new \Zend\Paginator\Adapter\DbSelect(
                $select, $this->tableGateway->getAdapter()
                )
        );
    }

    public function deleteQuotation($quotationID)
    {
        $value = $this->tableGateway->delete(array('quotationID' => $quotationID));
        return $value;
    }

    public function getQuotationID($quotationCode)
    {
        $statement = $this->tableGateway->select(array('quotationCode' => $quotationCode));
        return $statement->current()->quotationID;
    }

    public function getEntityID($quotationID)
    {
        $statement = $this->tableGateway->select(array('quotationID' => $quotationID));
        return $statement->current()->entityID;
    }

    public function saveQuotation(Quotation $quotation)
    {
        $data = array(
            'quotationCode' => $quotation->quotationCode,
            'quotationCustomerName' => $quotation->customerName,
            'customerID' => $quotation->customerID,
            'quotationIssuedDate' => $quotation->issuedDate,
            'quotationExpireDate' => $quotation->expireDate,
            'quotationPayementTerm' => $quotation->paymentTerm,
            'quotationTaxAmount' => $quotation->taxAmount,
            'quotationTotalDiscount' => $quotation->totalDiscount,
            'quotationTotalAmount' => $quotation->totalAmount,
            'quotationComment' => $quotation->comment,
            'quotationAdditionalDetail1' => $quotation->quotationAdditionalDetail1,
            'quotationAdditionalDetail2' => $quotation->quotationAdditionalDetail2,
            'quotationBranch' => $quotation->branch,
            'quotationShowTax' => $quotation->showTax,
            'entityID' => $quotation->entityID,
            'salesPersonID' => $quotation->salesPersonID,
            'jobRefType' => $quotation->jobRefType,
            'jobRefTypeID' => $quotation->jobRefTypeID,
            'customCurrencyId' => $quotation->customCurrencyId,
            'quotationCustomCurrencyRate' => $quotation->quotationCustomCurrencyRate,
            'priceListId' => $quotation->priceListId,
            'customerProfileID' => $quotation->customerProfileID,
            'quotationTotalDiscountType' => $quotation->quotationTotalDiscountType,
            'quotationWiseTotalDiscount' => $quotation->quotationWiseTotalDiscount,
            'quotationDiscountRate' => $quotation->quotationDiscountRate,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {

            return FALSE;
        }
    }

    function getQuotationIdList()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation');
        $select->columns(array("id"));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function updateQuotation(Quotation $quotation)
    {
        $data = array(
            'id' => $quotation->quotation_no,
//            'customerName' => $quotation->customer_name,
//            'customerID' => $quotation->customer_id,
            'currentBalance' => $quotation->invoicecurrentBalance,
            'issuedDate' => $quotation->issue_date,
            'expireDate' => $quotation->expire_date,
            'payementTerm' => $quotation->payment_term,
            'taxAmount' => $quotation->tax,
            'totalDiscount' => $quotation->total_discount,
            'totalAmount' => $quotation->total,
            'comment' => $quotation->comment,
            'branch' => $quotation->branch,
            'deliveryAddress' => $quotation->deliveryAddress,
            'deliveryCharge' => $quotation->deliveryCharge,
            'showTax' => $quotation->showTax,
        );
        $this->tableGateway->update($data, array('id' => $quotation->quotation_no));
        return TRUE;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * This file contains Invoice Bar Chart functions
     *
     */
    public function getAnnuvalQuotations($year)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation')
                ->columns(array('totAmountQuo' => new Expression('COUNT(id)'),
                    'Month' => new Expression('DATE_FORMAT(`quotationIssuedDate`, "%b")')))
                ->group(array('Month'))
                ->order(array('quotationIssuedDate'))
        ->where->like('quotationIssuedDate', '%' . $year . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        $quotationCountByMonth = array(
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'May' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Aug' => 0,
            'Sep' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0,
        );

        foreach ($countArray as $element) {
            switch ($element['Month']) {
                case "Jan":
                    $quotationCountByMonth['Jan'] = $element['totAmountQuo'];
                    break;
                case "Feb":
                    $quotationCountByMonth['Feb'] = $element['totAmountQuo'];
                    break;
                case "Mar":
                    $quotationCountByMonth['Mar'] = $element['totAmountQuo'];
                    break;
                case "Apr":
                    $quotationCountByMonth['Apr'] = $element['totAmountQuo'];
                    break;
                case "May":
                    $quotationCountByMonth['May'] = $element['totAmountQuo'];
                    break;
                case "Jun":
                    $quotationCountByMonth['Jun'] = $element['totAmountQuo'];
                    break;
                case "Jul":
                    $quotationCountByMonth['Jul'] = $element['totAmountQuo'];
                    break;
                case "Aug":
                    $quotationCountByMonth['Aug'] = $element['totAmountQuo'];
                    break;
                case "Sep":
                    $quotationCountByMonth['Sep'] = $element['totAmountQuo'];
                    break;
                case "Oct":
                    $quotationCountByMonth['Oct'] = $element['totAmountQuo'];
                    break;
                case "Nov":
                    $quotationCountByMonth['Nov'] = $element['totAmountQuo'];
                    break;
                case "Dec":
                    $quotationCountByMonth['Dec'] = $element['totAmountQuo'];
                    break;
            }
        }
        return $quotationCountByMonth;
    }

    public function updateQuotationStatus($quotationID, $closeStatusID)
    {
        $data = array(
            'quotationStatus' => $closeStatusID
        );
        return $this->tableGateway->update($data, array('quotationID' => $quotationID));
    }

    public function updateQuotationStatusByID($quotationID, $closeStatusID)
    {
        $data = array(
            'quotationStatus' => $closeStatusID
        );
        return $this->tableGateway->update($data, array('quotationID' => $quotationID));
    }

    public function searchQuotationForDropDown($branch, $searchKey, $status = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "quotation.entityID = e.entityID")
                ->where(array('quotationBranch' => $branch))
                ->where(array('e.deleted' => '0'))
                ->order(array('quotationIssuedDate' => 'DESC'));
        if ($status) {
            $select->where(array('quotation.quotationStatus' => $status));
        }
        $select->where->like('quotationCode', '%' . $searchKey . '%');
        $select->limit(50);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getCopiedQuotationProductDetails($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation');
        $select->columns(array('quotationID'));
        $select->join('quotationProduct', 'quotation.quotationID = quotationProduct.quotationID', array('quotationProductCopied'), 'left');
        $select->where(array('quotation.quotationBranch' => $locationID));
        $select->join(array('e' => 'entity'), "quotation.entityID = e.entityID", array('entityID', 'deleted'), 'left');
        $select->where(array('e.deleted' => '0'));
        $select->where(array('quotationProduct.quotationProductCopied' => 1));
        $query = $sql->prepareStatementForSqlObject($select);
        $resultSet = $query->execute();
        return $resultSet;
    }

    public function getQuotaionsForReport($fromDate, $toDate, $customerID = null, $locationID = null, $status = null , $payTerm = null, $salesPerson = null, $cusCategory = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('quotation')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "quotation.entityID = e.entityID")
                ->join('customer', "customer.customerID = quotation.customerID",array('customerName'),'left')
                ->join('currency', "quotation.customCurrencyId = currency.currencyID", array('*'), 'left')
                ->join('location', 'quotation.quotationBranch = location.locationID', array('locationName', 'locationCode'),'left')
                ->join('status','quotation.quotationStatus = status.statusID', array('*'),'left')
                ->join('salesPerson', 'quotation.salesPersonID = salesPerson.salesPersonID', array('salesPersonSortName'),'left')
                ->join('paymentTerm', 'quotation.quotationPayementTerm = paymentTerm.paymentTermID',array('*'),'left')
                ->join('quotationProductTax', 'quotation.quotationID = quotationProductTax.quotationID',array('quotTaxAmount'),'left')
                ->join('tax', 'tax.id = quotationProductTax.quotTaxID',array('*'),'left')
                ->where->between('quotation.quotationIssuedDate', $fromDate, $toDate);

        if($customerID){
            $select->where->in('customer.customerID', $customerID);
        }

        if($locationID){
            $select->where->in('quotation.quotationBranch', $locationID);
        }

        if($status){
            $select->where->in('quotation.quotationStatus', $status);
        }

        if($payTerm){
            $select->where->in('paymentTerm.paymentTermID', $payTerm);
        }

        if($salesPerson){
            $select->where->in('quotation.salesPersonID', $salesPerson);
        }
        if($cusCategory != ''){
            $select->join('customerCategory', 'customer.customerCategory = customerCategory.customerCategoryID', array('customerCategoryName'),
                'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    // get quotation basic details with time stamp by quotation id 
    public function getQuotationDetailsByQuotationId($quotationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('quotation');
        $select->columns(array('*'));
        $select->join('entity', 'quotation.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('quotation.quotationID' => $quotationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;
    }

    // get sales order related quotation by so id
    public function getSalesOrderRelatedQuotationDataBySOId($salesOrderID = null, $invoiceID = null, $returnID = null, $creditNoteID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('quotation');
        $select->columns(array('*'));
        $select->join('entity', 'quotation.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($salesOrderID)) {
            $select->join('salesOrders', 'quotation.quotationID = salesOrders.quotationID', array('*'),'left');
            $select->where(array('salesOrders.soID' => $salesOrderID));
        }
        if (!is_null($invoiceID)) {
            $select->join('salesOrders', 'quotation.quotationID = salesOrders.quotationID', array('*'),'left');
            $select->join('salesInvoice', 'salesOrders.soID = salesInvoice.salesOrderID', array('*'),'left');
            $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID));
        }
        if (!is_null($returnID)) {
            $select->join('salesOrders', 'quotation.quotationID = salesOrders.quotationID', array('*'),'left');
            $select->join('deliveryNote', 'salesOrders.soID = deliveryNote.salesOrderID', array('*'),'left');
            $select->join('deliveryNoteProduct', "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array('*'), 'left');
            $select->join('salesReturnProduct', "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array('*'), 'left');
            $select->join('salesReturn', "salesReturn.salesReturnID = salesReturnProduct.salesReturnID", array('*'), 'left');
            $select->where(array('salesReturn.salesReturnID' => $returnID));
        }
        if (!is_null($creditNoteID)) {
            $select->join('salesInvoice', 'quotation.quotationID = salesInvoice.quotationID', array('*'),'left');
            $select->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
            $select->where(array('creditNote.creditNoteID' => $creditNoteID));
        }
        $select->group("quotation.quotationID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get dln related quotation by dln id
    public function getDlnRelatedQuotationDataBydlnId($deliveryNoteID = null, $invoiceID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('quotation');
        $select->columns(array('*'));
        $select->join('salesOrders', 'quotation.quotationID = salesOrders.quotationID', array('*'),'left');
        $select->join('deliveryNote', 'salesOrders.soID = deliveryNote.salesOrderID', array('*'),'left');
        $select->join('entity', 'salesOrders.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($deliveryNoteID)) {
            $select->where(array('deliveryNote.deliveryNoteID' => $deliveryNoteID));
        }
        if (!is_null($invoiceID)) {
            $select->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
            $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
            $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        $select->group("quotation.quotationID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get invoice related sales invoice by invoice id
    public function getInvoiceRelatedQuotationeDataByInvoiceId($invoiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('quotation');
        $select->columns(array('*'));
        $select->join('salesInvoice', 'quotation.quotationID = salesInvoice.quotationID', array('*'),'left');
        $select->join('entity', 'quotation.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID));
        $select->group("quotation.quotationID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }


    // get sales order related quotation by creditNote id
    public function getSalesOrderRelatedQuotationDataByCreditNoteId($creditNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('quotation');
        $select->columns(array('*'));
        $select->join('entity', 'quotation.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->join('salesOrders', 'quotation.quotationID = salesOrders.quotationID', array('*'),'left');
        $select->join('deliveryNote', 'salesOrders.soID = deliveryNote.salesOrderID', array('*'),'left');
        $select->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
        $select->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
        $select->where(array('creditNote.creditNoteID' => $creditNoteID, 'salesInvoiceProduct.documentTypeID' => 4));
        $select->group("quotation.quotationID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }
}
