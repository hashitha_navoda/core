<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodCreditCard related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class IncomingPaymentMethodCreditCardTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCreditCardPayment(IncomingPaymentMethodCreditCard $incomingPaymentMethodCreditCard)
    {
        $cashData = array(
            'incomingPaymentMethodCreditCardNumber' => $incomingPaymentMethodCreditCard->incomingPaymentMethodCreditCardNumber,
            'incomingPaymentMethodCreditReceiptNumber' => $incomingPaymentMethodCreditCard->incomingPaymentMethodCreditReceiptNumber,
            'cardAccountID' => $incomingPaymentMethodCreditCard->cardAccountID,
            'cardTypeID' => $incomingPaymentMethodCreditCard->cardTypeID
        );
        if ($this->tableGateway->insert($cashData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //update credit card details
    public function updateCreditCardDetails($data, $incomingPaymentMethodCreditCardId)
    {
        if ($incomingPaymentMethodCreditCardId) {
            try {
                $this->tableGateway->update($data, array('incomingPaymentMethodCreditCardId' => $incomingPaymentMethodCreditCardId));
                return true;
            } catch (Exception $exc) {
                echo $exc->getTraceAsString();
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    public function getByID($incomingPaymentMethodCreditCardId)
    {
        return $this->tableGateway->select(['incomingPaymentMethodCreditCardId' => $incomingPaymentMethodCreditCardId]);
    }

    /**
     * this function use to get credt card payment details that give account ID
     * @param int $accountID
     * @return array
     */
    public function getByAccountID($accountID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodCreditCard')
                ->columns(array('*'))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId = incomingPaymentMethod.incomingPaymentMethodCreditCardId', array('incomingPaymentId'))
                ->join('incomingPayment', 'incomingPaymentMethod.incomingPaymentId=incomingPayment.incomingPaymentID', array('entityID'))
                ->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted'))
        ->where->equalTo('incomingPaymentMethodCreditCard.cardAccountID', $accountID)
        ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
