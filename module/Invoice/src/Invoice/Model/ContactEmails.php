<?php

namespace Invoice\Model;

/**
 * Class DispatchNote
 * @package Invoice\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class ContactEmails
{
    
    public $contactID;
    public $contactEmailID;
    public $emailAddress;
    public $emailType;

    public function exchangeArray($data)
    {
        $this->contactEmailID      = (!empty($data['contactEmailID'])) ? $data['contactEmailID'] : null;
        $this->contactID      = (!empty($data['contactID'])) ? $data['contactID'] : null;
        $this->emailAddress    = (!empty($data['emailAddress'])) ? $data['emailAddress'] : null;
        $this->emailType    = (!empty($data['emailType'])) ? $data['emailType'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
