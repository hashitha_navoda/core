<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Invoice Payments Table Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class InvoicePaymentsTable
{

    protected $tableGateway;

    const ENTITY_ACTIVE = 0;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveInvoicePayment(invoicepayments $invoicepayment)
    {
        $invoicePaymentsData = array(
            'incomingPaymentID' => $invoicepayment->incomingPaymentID,
            'salesInvoiceID' => $invoicepayment->salesInvoiceID,
            'incomingInvoicePaymentAmount' => $invoicepayment->incomingInvoicePaymentAmount,
            'incomingInvoicePaymentCurrencyGainOrLoss' => $invoicepayment->incomingInvoicePaymentCurrencyGainOrLoss,
            'incomeID' => $invoicepayment->incomeID,
        );

        if ($this->tableGateway->insert($invoicePaymentsData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllInvoiceDetailsByPaymentID($payId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingInvoicePayment');
        $select->join(
                'salesInvoice', 'incomingInvoicePayment.salesInvoiceID = salesInvoice.salesInvoiceID', array(
            '*',
            'left_to_pay' => new Expression("`salesInvoiceTotalAmount` -  `salesInvoicePayedAmount`")
                )
        );
        $select->where(array('incomingInvoicePayment.incomingPaymentID = ?' => $payId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getInvoiceByPaymentID($payId)
    {
//        $rowset = $this->tableGateway->select(array('paymentID' => $payId));

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingInvoicePayment');
        $select->join(
                'salesInvoice', 'incomingInvoicePayment.salesInvoiceID = salesInvoice.salesInvoiceID', array(
            '*',
            'left_to_pay' => new Expression("`salesInvoiceTotalAmount` -  `salesInvoicePayedAmount`")
                )
        );
        $select->join('incomingPayment','incomingInvoicePayment.incomingPaymentID= incomingPayment.incomingPaymentID', array('incomingPaymentCreditAmount','incomingPaymentPaidAmount'));
        $select->where(array('incomingInvoicePayment.incomingPaymentID = ?' => $payId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     */
    public function getPaymentsDetailsByInvoiceID($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingInvoicePayment')
                ->join('incomingPayment', 'incomingInvoicePayment.incomingPaymentID = incomingPayment.incomingPaymentID')
                ->where(array('salesInvoiceID' => $invoiceID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

//        $rowset = $this->tableGateway->select(array('invoiceID' => $invoiceID));
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Sandun Dissanayake<sandun@thinkcube.com>
     * @param int $invoiceID
     * @return $rowset
     */
    public function isPaymentsForInvoiceID($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingInvoicePayment')
                ->join('incomingPayment', 'incomingInvoicePayment.incomingPaymentID = incomingPayment.incomingPaymentID')
                ->join('entity', 'entity.entityID = incomingPayment.entityID')
                ->where(array('incomingInvoicePayment.salesInvoiceID' => $invoiceID))
                ->where(array('deleted' => self::ENTITY_ACTIVE));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * Get given invoices payment details
     * @param array $invoiceIds
     * @param date $fromDate
     * @param date $toDate
     * @return mixed
     */
    public function getIncomingInvoicePaymentDetails($invoiceIds, $fromDate = NULL, $toDate = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingInvoicePayment')
                ->join('incomingPayment', 'incomingInvoicePayment.incomingPaymentID = incomingPayment.incomingPaymentID', array('*'))
                ->join('salesInvoice', 'incomingInvoicePayment.salesInvoiceID = salesInvoice.salesInvoiceID', array('*'))
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentID', array('*'))
                ->join('incomingPaymentMethodBankTransfer', 'incomingPaymentMethod.incomingPaymentMethodBankTransferId = incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId', array('*'), 'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left')
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                ->join('incomingPaymentMethodGiftCard', 'incomingPaymentMethod.incomingPaymentMethodGiftCardId = incomingPaymentMethodGiftCard.incomingPaymentMethodGiftCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLoyaltyCard', 'incomingPaymentMethod.incomingPaymentMethodLoyaltyCardId = incomingPaymentMethodLoyaltyCard.incomingPaymentMethodLoyaltyCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLC', 'incomingPaymentMethod.incomingPaymentMethodLCId = incomingPaymentMethodLC.incomingPaymentMethodLCId', array('*'), 'left')
                ->join('incomingPaymentMethodTT', 'incomingPaymentMethod.incomingPaymentMethodTTId = incomingPaymentMethodTT.incomingPaymentMethodTTId', array('*'), 'left')
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName'))
                ->join(array('si' => 'entity'), "salesInvoice.entityID = si.entityID", ['*'])
                ->join(array('ipm' => 'entity'), "incomingPaymentMethod.entityID = ipm.entityID", ['*'])
                ->order(array('salesInvoice.salesInvoiceID DESC'))
        ->where->in('incomingInvoicePayment.salesInvoiceID', $invoiceIds)
        ->where->equalTo('si.deleted', self::ENTITY_ACTIVE)
        ->where->equalTo('ipm.deleted', self::ENTITY_ACTIVE);
        $select->where->notEqualTo('incomingPayment.incomingPaymentStatus', 5);
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    public function getCardTypePaymentDetails($fromDate = NULL, $toDate = NULL, $cardTypeID = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingInvoicePayment')
                ->join('incomingPayment', 'incomingInvoicePayment.incomingPaymentID = incomingPayment.incomingPaymentID', array('*'))
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentID')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->join('customer', 'incomingPayment.customerID = customer.customerID', array('customerName'))
                ->join('cardType', 'cardType.cardTypeID = incomingPaymentMethodCreditCard.cardTypeID', array('cardTypeName'))
                ->join('entity', 'incomingPayment.entityID = entity.entityID')
                ->order(array('incomingPayment.incomingPaymentID DESC'))
        ->where->equalTo('entity.deleted', self::ENTITY_ACTIVE);
        if ($fromDate && $toDate) {
            $select->where->between('incomingPayment.incomingPaymentDate', $fromDate, $toDate);
        }
        if (!empty($cardTypeID)) {
            $select->where->in('incomingPaymentMethodCreditCard.cardTypeID', $cardTypeID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }
    
    /**
     * Get invoice payment by invoice id and payment id
     * @param string $invoiceId
     * @param string $paymentId
     * @return mixed
     */
    public function getInvoicePaymentByInvoiceIdAndPaymentId($invoiceId, $paymentId) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingInvoicePayment')
                ->where(array('incomingInvoicePayment.salesInvoiceID' => $invoiceId))
                ->where(array('incomingInvoicePayment.incomingPaymentID' => $paymentId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }
    
    /**
     * Update invoice payment by invoice id and payment id
     * @param array $data
     * @param int $invoiceId
     * @param int $paymentId
     * @return boolean
     */
    public function updateInvoicePaymentByInvoiceIdAndPaymentId($data, $invoiceId, $paymentId)
    {
        if ($this->tableGateway->update($data, array('salesInvoiceID' => $invoiceId, 'incomingPaymentID' => $paymentId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * Update invoice payment by invoice id 
     * @param array $data
     * @param int $invoiceId
     * @return boolean
     */
    public function updateInvoicePaymentByInvoiceId($data, $invoiceId)
    {
        if ($this->tableGateway->update($data, array('salesInvoiceID' => $invoiceId))) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * This function is used to get invoice paymnet details by payment id
     */
    public function getInvoicePaymentByPaymentId($paymentId) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingInvoicePayment')
                ->where(array('incomingInvoicePayment.incomingPaymentID' => $paymentId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
}
