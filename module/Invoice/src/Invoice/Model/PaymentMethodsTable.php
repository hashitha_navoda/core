<?php

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class PaymentMethodsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getPaymentterm($id)
    {
        $id = (int) $id;
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getPaymenttermByName($paymentterm)
    {
        $rowset = $this->tableGateway->select(array('paymentMethod' => $paymentterm));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return List of PaymentMethods Array
     *
     */
    public function getPaymentMethods()
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('paymentMethods');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val['method'];
        }

        return $countArray;
    }

}

?>
