<?php

namespace Invoice\Model;

/**
 * Class DispatchNote
 * @package ContactLists\Model
 * @author Sahan Siriwardhan <sahan@thinkcube.com>
 */
class ContactLists
{
    
    public $contactListsID;
    public $name;
    public $code;
    public $noOfContacts;

    public function exchangeArray($data)
    {
        $this->contactListsID      = (!empty($data['contactListsID'])) ? $data['contactListsID'] : null;
        $this->name    = (!empty($data['name'])) ? $data['name'] : null;
        $this->code    = (!empty($data['code'])) ? $data['code'] : null;
        $this->noOfContacts = (!empty($data['noOfContacts'])) ? $data['noOfContacts'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
