<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CustomerCategory
{

    public $customerCategoryID;
    public $customerCategoryName;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->customerCategoryID = (!empty($data['customerCategoryID'])) ? $data['customerCategoryID'] : null;
        $this->customerCategoryName = (!empty($data['customerCategoryName'])) ? $data['customerCategoryName'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerCategoryName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
