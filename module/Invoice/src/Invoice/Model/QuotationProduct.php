<?php

namespace Invoice\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterInterface;

class QuotationProduct
{

    public $quotProductID;
    public $locationProductID;
    public $productName;
    public $quantity;
    public $unitPrice;
    public $uom;
    public $discount;
    public $discountType;
    public $quotationID;
    public $total;
    public $tax;
    public $copied;
    public $quotationProductCopiedQuantity;
    private $inputFilter;

    public function exchangeArray($data)
    {
        $this->quotProductID = (!empty($data['quotProductID'])) ? $data['quotProductID'] : null;
        $this->locationProductID = (!empty($data['locationProductID'])) ? $data['locationProductID'] : null;
        $this->productName = (!empty($data['productName'])) ? $data['productName'] : null;
        $this->quotationProductDescription = (!empty($data['quotationProductDescription'])) ? $data['quotationProductDescription'] : null;
        $this->quantity = (!empty($data['quantity'])) ? $data['quantity'] : null;
        $this->unitPrice = (!empty($data['unitPrice'])) ? $data['unitPrice'] : null;
        $this->uom = (!empty($data['uom'])) ? $data['uom'] : null;
        $this->discount = (!empty($data['discount'])) ? $data['discount'] : null;
        $this->discountType = (!empty($data['discountType'])) ? $data['discountType'] : null;
        $this->quotationID = (!empty($data['quotationID'])) ? $data['quotationID'] : null;
        $this->total = (!empty($data['total'])) ? $data['total'] : null;
        $this->tax = (!empty($data['tax'])) ? $data['tax'] : null;
        $this->copied = (!empty($data['copied'])) ? $data['copied'] : null;
        $this->quotationProductCopiedQuantity = (!empty($data['quotationProductCopiedQuantity'])) ? $data['quotationProductCopiedQuantity'] : 0.00;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new inputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'item_code',
                        'required' => true,
//                        'filters' => array(
//                            array('name' => 'Int'),
//                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'item',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 50,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'unit_price',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'discount',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'quantity',
                        'required' => true,
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}

?>