<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DispatchNoteProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Save dispatch note product
     * @param \Invoice\Model\DispatchNoteProduct $dispatchNoteProduct
     * @return boolean
     */
    public function saveDispatchNoteProduct(DispatchNoteProduct $dispatchNoteProduct)
    {
        $data = array(
            'dispatchNoteId' => $dispatchNoteProduct->dispatchNoteId,
            'salesInvoiceProductId' => $dispatchNoteProduct->salesInvoiceProductId,
            'dispatchNoteProductQuantity' => $dispatchNoteProduct->dispatchNoteProductQuantity
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Get dispatch note products
     * @param int $dispatchNoteId
     * @return mixed
     */
    public function getDispatchNoteProductsByDispatchNoteId( $dispatchNoteId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNoteProduct')
                ->columns(array('*'))
                ->join("salesInvoiceProduct", "dispatchNoteProduct.salesInvoiceProductId = salesInvoiceProduct.salesInvoiceProductID", "productID")
                ->join('product', 'salesInvoiceProduct.productID = product.productID', array('productCode','productName'))
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr','uomDecimalPlace'))
                ->where(array('productUomBase' => '1'))
                ->where->equalTo('dispatchNoteProduct.dispatchNoteId', $dispatchNoteId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

}
