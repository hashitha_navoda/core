<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contaions invoice reciept model
 */

namespace Invoice\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterInterface;

class DraftInvSalesPersons
{

    public $draftInvSalesPersonsID;
    public $invoiceID;
    public $salesPersonID;
    public $relatedSalesAmount;
    private $inputFilter;

    public function exchangeArray($data)
    {
        $this->draftInvSalesPersonsID = (!empty($data['draftInvSalesPersonsID'])) ? $data['draftInvSalesPersonsID'] : null;
        $this->invoiceID = (!empty($data['invoiceID'])) ? $data['invoiceID'] : null;
        $this->salesPersonID = (!empty($data['salesPersonID'])) ? $data['salesPersonID'] : null;
        $this->relatedSalesAmount = (!empty($data['relatedSalesAmount'])) ? $data['relatedSalesAmount'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

}

?>