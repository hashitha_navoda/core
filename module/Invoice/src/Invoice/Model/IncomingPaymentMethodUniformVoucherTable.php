<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodTT related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;

class IncomingPaymentMethodUniformVoucherTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveUniformVoucherPayment(IncomingPaymentMethodUniformVoucher $incomingPaymentMethodUniformVoucher)
    {
        $data = array(
            'voucherNumber' => $incomingPaymentMethodUniformVoucher->voucherNumber,
            'voucherAccountID' => $incomingPaymentMethodUniformVoucher->voucherAccountID,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function UniformVoucherPaymentMethod($data, $id)
    {
        if ($id) {
            if ($this->tableGateway->update($data, array('incomingPaymentMethodUniformVoucherID' => $id))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}
