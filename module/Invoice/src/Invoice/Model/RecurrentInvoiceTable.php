<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains RecurrentInvoiceTable Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class RecurrentInvoiceTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $adapter = $this->tableGateway->getAdapter();
            $sql = new Sql($adapter);
            $select = $sql->select();
            $select->from('recurrentInvoice')
                    ->join('invoice', 'recurrentInvoice.id = invoice.id', array('salesOrderID' => new Expression('salesOrderID')))
                    ->where(array('Type' => 'Invoice'));
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }


        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function fetchAllSalesOrder($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select('recurrentInvoice');
            $select->where(array('Type' => 'SalesOrder'));
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new RecurrentInvoice());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveRecurrentInvoice(RecurrentInvoice $invoice)
    {
        $data = array(
            'id' => $invoice->id,
            'name' => $invoice->name,
            'timeValue' => $invoice->timeValue,
            'timeFormat' => $invoice->timeFormat,
            'startDate' => $invoice->startDate,
            'endDate' => $invoice->endDate,
            'state' => $invoice->state,
            'nextDate' => $invoice->nextDate,
            'Type' => $invoice->Type,
        );

        $value = $this->tableGateway->insert($data);
        return $value;
    }

    public function updateRecurrentInvoice(RecurrentInvoice $invoice)
    {
        $data = array(
            'name' => $invoice->name,
            'timeValue' => $invoice->timeValue,
            'timeFormat' => $invoice->timeFormat,
            'startDate' => $invoice->startDate,
            'endDate' => $invoice->endDate,
            'state' => $invoice->state,
            'nextDate' => $invoice->nextDate,
            'Type' => $invoice->Type,
        );
        $value = $this->tableGateway->update($data, array('id' => $invoice->id));
        return $value;
    }

    public function updateRecurrentInvoiceNextdate(RecurrentInvoice $invoice)
    {
        $data = array(
            'nextDate' => $invoice->nextDate,
            'state' => $invoice->state,
        );
        $value = $this->tableGateway->update($data, array('id' => $invoice->id));
        return $value;
    }

    public function cancelRecurrentInvoice(RecurrentInvoice $invoice)
    {
        $data = array(
            'state' => $invoice->state,
        );
        $value = $this->tableGateway->update($data, array('id' => $invoice->id));
        return $value;
    }

    public function getRecurrentInvoiceByCustomerID($cust_id)
    {
        $select = new \Zend\Db\Sql\Select;
        $select->from('invoice')
                ->columns(array('customerID', 'salesOrderID'))
                ->JOIN('recurrentInvoice', 'recurrentInvoice.id = invoice.id', array('*'), 'right')
                ->where(array('invoice.customerID' => $cust_id));
        return new \Zend\Paginator\Paginator(
                new \Zend\Paginator\Adapter\DbSelect(
                $select, $this->tableGateway->getAdapter()
                )
        );
    }

    public function getRecurrentInvoice($inv_id)
    {
        $select = new \Zend\Db\Sql\Select;
        $select->from('invoice')
                ->columns(array('customerID', 'salesOrderID'))
                ->JOIN('recurrentInvoice', 'recurrentInvoice.id = invoice.id', array('*'), 'right')
                ->where(array('invoice.id' => $inv_id));
        return new \Zend\Paginator\Paginator(
                new \Zend\Paginator\Adapter\DbSelect(
                $select, $this->tableGateway->getAdapter()
                )
        );
    }

    public function getRecurrentSalesOrderByCustomerID($cust_id)
    {
        $select = new \Zend\Db\Sql\Select;
        $select->from('salesOrder')
                ->columns(array('customerID',))
                ->JOIN('recurrentInvoice', 'recurrentInvoice.id = salesOrder.id', array('*'), 'right')
                ->where(array('salesOrder.customerID' => $cust_id));
        return new \Zend\Paginator\Paginator(
                new \Zend\Paginator\Adapter\DbSelect(
                $select, $this->tableGateway->getAdapter()
                )
        );
    }

    public function getRecurrentSalesOrder($salesOrder)
    {
        $select = new \Zend\Db\Sql\Select;
        $select->from('salesOrder')
                ->columns(array('customerID',))
                ->JOIN('recurrentInvoice', 'recurrentInvoice.id = salesOrder.id', array('*'), 'right')
                ->where(array('salesOrder.id' => $salesOrder));
        return new \Zend\Paginator\Paginator(
                new \Zend\Paginator\Adapter\DbSelect(
                $select, $this->tableGateway->getAdapter()
                )
        );
    }

    public function getRecurrentInvoicesByDate($fromdate, $todate)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('startDate >= ?' => $fromdate));
        $select->where(array('endDate <= ?' => $todate));
        $select->where(array('Type' => 'Invoice'));
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new RecurrentInvoice());
        $paginatorAdapter = new DbSelect(
                $select, $this->tableGateway->getAdapter(), $resultSetPrototype);
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    public function getRecurrentSalesOrderByDate($fromdate, $todate)
    {
        $select = $this->tableGateway->getSql()->select();
        $select->where(array('startDate >= ?' => $fromdate));
        $select->where(array('endDate <= ?' => $todate));
        $select->where(array('Type' => 'SalesOrder'));
        $resultSetPrototype = new ResultSet();
        $resultSetPrototype->setArrayObjectPrototype(new RecurrentInvoice());
        $paginatorAdapter = new DbSelect(
                $select, $this->tableGateway->getAdapter(), $resultSetPrototype);
        $paginator = new Paginator($paginatorAdapter);
        return $paginator;
    }

    /**
     * @author Prathap Weerasinghe <prathap@thinkcube.net>
     * @param type $id
     * @return null
     */
    public function getRecurrentInvoiceByNextDate($date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("id", "name", "Type", "nextDate"));
        $select->from('recurrentInvoice');
        $select->join("logger", "logger.refID = recurrentInvoice.id", array("refID", "userID"), "left outer");
        $select->join("user", "logger.userID = user.userID", array("firstName", "email1"), "left outer");
        $select->join("invoice", "recurrentInvoice.id = invoice.id", array("customerName"), "left outer");
        $select->where->equalTo("nextDate", $date);
        $select->where(array("logger.module = ?" => "recurrent"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getRecurrentInvoiceByID($id)
    {
        $rowset = $this->tableGateway->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getRecurrentInvoiceList($type)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("id"));
        $select->from('recurrentInvoice');
        $select->where(array("Type = ?" => $type));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

}
