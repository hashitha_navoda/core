<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateInterface;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Predicate;

class DeliveryNoteTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('deliveryNote')
                    ->columns(array('*'))
                    ->join('customer', 'deliveryNote.customerID = customer.customerID', array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->join('status', 'deliveryNote.deliveryNoteStatus = status.statusID', array('statusName'), 'left')
                    ->join('currency', 'deliveryNote.customCurrencyId = currency.currencyId', array('currencySymbol'), 'left')
                    ->order(array('deliveryNoteDeliveryDate' => 'DESC', 'deliveryNoteID' => 'DESC'))
                    ->where(array('deliveryNoteLocationID' => $locationID))
                    ->where->notEqualTo('deliveryNote.deliveryNoteStatus',10);
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function getLocationRelatedDeliveryNotes($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNote")
                ->columns(array("*"))
                ->join("customer", "deliveryNote.customerID = customer.customerID", array("customerName", "customerShortName"), "left")
                ->order(array('deliveryNoteDeliveryDate' => 'DESC'))
                ->where(array("deliveryNote.deliveryNoteLocationID" => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function checkDeliveryNoteByCode($deliveryNoteCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('deliveryNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "deliveryNote.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('deliveryNote.deliveryNoteCode' => $deliveryNoteCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getDeliveryNoteByCode($deliveryCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->join('location', 'location.locationID = deliveryNote.deliveryNoteLocationID', array('locationName', 'locationCode'));
        $select->where(array('deliveryNoteCode' => $deliveryCode));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getDeliveryNoteByDeliveryNoteId($deliveryId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->join('location', 'location.locationID = deliveryNote.deliveryNoteLocationID', array('locationName', 'locationCode'));
        $select->where(array('deliveryNoteID' => $deliveryId));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getDeliveryNoteByID($deliveryNoteID)
    {
        $rowset = $this->tableGateway->select(array('deliveryNoteID' => $deliveryNoteID));
        $row = $rowset->current();
        return $row;
    }

    public function getDeliveryNoteByIDForJE($deliveryNoteID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->join('customer', 'deliveryNote.customerID = customer.customerID', array('customerName','customerID','customerCode','customerShortName'));
        $select->where(array('deliveryNoteID' => $deliveryNoteID));
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus', 5);
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus', 10);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getDeliveryNotesByIDs($deliveryNoteIDs)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->where->in('deliveryNoteID', $deliveryNoteIDs);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getDeliveryNoteDetailsByID($deliveryNoteID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->join('customer', 'deliveryNote.customerID = customer.customerID', array('customerName','customerID','customerCode','customerShortName'),'left');
        $select->join('customerProfile', 'deliveryNote.customerProfileID = customerProfile.customerProfileID', array('*'), 'left');
        $select->join('entity', 'deliveryNote.entityID = entity.entityID', array('createdBy', 'createdTimeStamp'), 'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
//        $select->join('location', 'salesInvoice.locationID = location.locationID', array('*'));
//        $select->join('paymentTerm', 'salesOrders.payementTerm = paymentTerm.paymentTermID', array('*'));
//        $select->join('deliveryNote', 'salesInvoice.deliveryNoteID = deliveryNote.deliveryNoteID', array('*'), 'left');
        $select->where(array('deliveryNoteID' => $deliveryNoteID));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getDeliveryNoteforSearch($deliveryNoteID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNote")
                ->columns(array("*"))
                ->join("customer", "deliveryNote.customerID = customer.customerID", array("*"), "left")
                ->join("currency", "deliveryNote.customCurrencyId= currency.currencyID", array("*"), "left")
                ->where(array("deliveryNoteID" => $deliveryNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDeliveryNoteByCustomerID($customerID, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNote")
                ->columns(array("*"))
                ->join("customer", "deliveryNote.customerID = customer.customerID", array("customerName", "customerCode", "customerStatus"), "left")
                ->join("currency", "deliveryNote.customCurrencyId= currency.currencyID", array("*"), "left")
                ->where(array("deliveryNote.customerID" => $customerID, "deliveryNote.deliveryNoteLocationID" => $locationID));
        $select->where->notEqualTo("deliveryNote.deliveryNoteStatus",10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDeliveryNoteByDate($fromdate, $todate, $customerID = NULL, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNote")
                ->columns(array("*"))
                ->join("customer", "deliveryNote.customerID = customer.customerID", array("customerName", "customerShortName", "customerCode","customerStatus"), "left")
                ->join("currency", "deliveryNote.customCurrencyId= currency.currencyID", array("*"), "left")
                ->order(array('deliveryNoteDeliveryDate' => 'DESC'))
                ->where(array("deliveryNote.deliveryNoteLocationID" => $locationID))
                ->where->notEqualTo('deliveryNote.deliveryNoteStatus',10);
        if ($customerID != NULL) {
            $select->where(array("deliveryNote.customerID" => $customerID));
        }
        $select->where->between('deliveryNoteDeliveryDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }
    public function getDlnBySerialCode($searchString, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNote")
                ->columns(array("*"))
                ->join("customer", "deliveryNote.customerID = customer.customerID", array("customerName", "customerShortName", "customerCode","customerStatus"), "left")
                ->join("deliveryNoteProduct", "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array("*"), "left")
                ->join("deliveryNoteSubProducts", "deliveryNoteProduct.deliveryNoteProductID = deliveryNoteSubProducts.deliveryNoteProductID", array("*"), "left")
                ->join('productSerial','deliveryNoteSubProducts.productSerialID = productSerial.productSerialID',array('*'),'left')
                ->where(array("deliveryNote.deliveryNoteLocationID" => $locationID))
                ->where(new PredicateSet(array(new Operator('productSerial.productSerialCode', 'like', '%' . $searchString . '%')), PredicateSet::OP_OR))
                ->where->notEqualTo('deliveryNote.deliveryNoteStatus',10);
                $select->group(array('deliveryNote.deliveryNoteID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function saveDeliveryNote(DeliveryNote $deliveryNote)
    {
        $data = array(
            'deliveryNoteID' => $deliveryNote->deliveryNoteID,
            'deliveryNoteCode' => $deliveryNote->deliveryNoteCode,
            'customerID' => $deliveryNote->customerID,
            'deliveryNoteLocationID' => $deliveryNote->deliveryNoteLocationID,
            'deliveryNoteDeliveryDate' => $deliveryNote->deliveryNoteDeliveryDate,
            'deliveryNoteCharge' => $deliveryNote->deliveryNoteCharge,
            'deliveryNotePriceTotal' => $deliveryNote->deliveryNotePriceTotal,
            'deliveryNoteAddress' => $deliveryNote->deliveryNoteAddress,
            'deliveryNoteComment' => $deliveryNote->deliveryNoteComment,
            'salesOrderID' => $deliveryNote->salesOrderID,
            'entityID' => $deliveryNote->entityID,
            'salesPersonID' => $deliveryNote->salesPersonID,
            'customCurrencyId' => $deliveryNote->customCurrencyId,
            'deliveryNoteCustomCurrencyRate' => $deliveryNote->deliveryNoteCustomCurrencyRate,
            'priceListId' => $deliveryNote->priceListId,
            'customerProfileID' => $deliveryNote->customerProfileID,
            'jobId' => $deliveryNote->jobId,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getIssuedDeliveryNotesIds($fromDate, $toDate, $locIDs)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('deliveryNote')
                ->columns(array('deliveryNoteID'))
                ->order(array('deliveryNoteDeliveryDate' => 'DESC'));
        $select->where->between('deliveryNoteDeliveryDate', $fromDate, $toDate);
        if (!empty($locIDs)) {
            $select->where->in('deliveryNoteLocationID', $locIDs);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        } else {
            $countArray = array();
            foreach ($results as $val) {
                array_push($countArray, $val['deliveryNoteID']);
            }
            return $countArray;
        }
    }

    public function getIssuedDeliveryNotesDetails($fromDate, $toDate, $locIDs)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('deliveryNote')
                ->columns(array('deliveryNoteID', 'deliveryNoteCode',
                    'deliveryNoteDeliveryDate',
                    'locID' => new Expression('deliveryNoteLocationID')))
                ->join('deliveryNoteProduct', 'deliveryNoteProduct.deliveryNoteID=deliveryNote.deliveryNoteID', array(
                    'qty' => new Expression('deliveryNoteProductQuantity'),'deliveryNoteProductID'
                        ), 'left')
                ->join('product', 'product.productID=deliveryNoteProduct.productID', array(
                    'pName' => new Expression('productName'),
                    'pID' => new Expression('product.productID'),
                    'pCD' => new Expression('product.productCode')
                        ), 'left')
                // ->join('salesInvoice', 'salesInvoice.deliveryNoteID = deliveryNote.deliveryNoteID', array(
                    // 'salesInvoiceID'
                        // ), 'left')
                ->join('location', 'location.locationID = deliveryNote.deliveryNoteLocationID', array(
                    'locationName', 'locationCode'
                        ), 'left')
                // ->where('salesInvoice.deliveryNoteID  IS NULL')
                ->order(array('deliveryNoteID' => 'DESC'));
        $select->where(array('product.productState' => 1));
        $select->where->between('deliveryNoteDeliveryDate', $fromDate, $toDate);
        if (!empty($locIDs)) {
            $select->where->in('deliveryNoteLocationID', $locIDs);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDeliveryNoteProductBySalesOrderID($salesOrderID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNote")
                ->columns(array("*"))
                ->join("deliveryNoteProduct", "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array("*"), "left")
                ->where(array("salesOrderID" => $salesOrderID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Peushani Jayasekara <peushani@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @return null|array
     *
     */
    public function getDailyDeliveryValuesByDate($fromDate, $toDate, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('deliveryNote')
                ->columns(array('deliveryNoteID', 'deliveryNoteCode', 'deliveryNoteDeliveryDate', 'deliveryNotePriceTotal'))
                ->order(array('deliveryNoteDeliveryDate' => 'ASC'));
        $select->where->between('deliveryNoteDeliveryDate', $fromDate, $toDate);
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus',10);
        if($cusCategory != ''){
            $select->join('customer', 'deliveryNote.customerID = customer.customerID', array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        } else {
            $countArray = array();
            foreach ($results as $val) {
                array_push($countArray, $val);
            }
            return $countArray;
        }
    }

    /*
     * @author Yashora  <yashora@thinkcube.com>
     * return database table data to get total values for invoiced delivery notes
     * @param date $fromDate
     * @param date $toDate
     * @retturn array sql query data
     */

    public function getInvoicedDeliveryNoteVAluesData($fromDate, $toDate)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote')
                ->columns(array('deliveryDate' => new Expression('deliveryNote.deliveryNoteDeliveryDate'),
                    'deliveryNoteNumber' => new Expression('deliveryNote.deliveryNoteCode'),
                ));
        $select->join('salesInvoiceProduct', 'deliveryNote.deliveryNoteID = salesInvoiceProduct.salesInvoiceProductDocumentID', array('totalAmount' => new Expression('SUM(salesInvoiceProduct.salesInvoiceProductTotal)')), 'left');
        $select->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('*'), 'left');
        $select->join('entity', 'entity.entityID = deliveryNote.entityID', array('deleted'), 'left');
        $select->group('deliveryNote.deliveryNoteID','salesInvoice.salesInvoiceID');
        $select->where(array('entity.deleted' => '0'));
        $select->where(array('salesInvoiceProduct.documentTypeID' => 4));
        $select->where->between('deliveryNote.deliveryNoteDeliveryDate', $fromDate, $toDate);
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus',10);
        $select->where->notEqualTo('salesInvoice.statusID',10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    /**
     * @author Yashora  <yashora@thinkcube.com>
     * return database table data to get total values for not invoiced delivery notes
     * @param date $fromDate
     * @param date $toDate
     * @return array sql query data
     */
    public function getNotInvoicedDeliveryNoteValuesData($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote')
                ->columns(array('deliveryDate' => new Expression('deliveryNote.deliveryNoteDeliveryDate'),
                    'deliveryNoteNumber' => new Expression('deliveryNote.deliveryNoteCode'),
                    'totalAmount' => new Expression('deliveryNote.deliveryNotePriceTotal')
                ))
                ->join("deliveryNoteProduct", "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", ['deliveryNoteCopiedTotal' => new Expression('SUM((deliveryNoteProductCopiedQuantity/deliveryNoteProductQuantity)*deliveryNoteProductTotal)')], "left")
                ->join('customer', 'deliveryNote.customerID = customer.customerID', array('customerCode','customerName'))
                ->order(array('deliveryNoteDeliveryDate' => 'ASC'));
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus',10);
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus',4);
        $select->order('deliveryNoteNumber');
        $select->where->between('deliveryNoteDeliveryDate', $fromDate, $toDate);
        $select->group('deliveryNoteProduct.deliveryNoteID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultArray = [];
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    public function updateDeliveryNoteStatus($deliveryNoteID, $closeStatusID)
    {
        $data = array(
            'deliveryNoteStatus' => $closeStatusID
        );
        return $this->tableGateway->update($data, array('deliveryNoteID' => $deliveryNoteID));
    }

    /**
     *
     * @param string $locationID
     * @param string $deliveryNoteSearchKey
     * @param array $allowedStatus
     * @return type
     */
    public function searchDeliveryNotesForDropDown($locationID = false, $deliveryNoteSearchKey, $allowedStatus = false, $customerID = false, $currencyID = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNote")
                ->columns(array('deliveryNoteID', 'deliveryNoteCode'))
                ->order(array('deliveryNoteDeliveryDate' => 'DESC'));
        $select->where->like('deliveryNoteCode', '%' . $deliveryNoteSearchKey . '%');
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus',10);
        $select->limit(50);
        if ($allowedStatus) {
            $select->where->in('deliveryNoteStatus', $allowedStatus);
        }

        if($locationID){
            $select->where(array("deliveryNote.deliveryNoteLocationID" => $locationID));
        }

        if($customerID){
            $select->where(array("deliveryNote.customerID" => $customerID));
        }

        if($currencyID){
            $select->where(array("deliveryNote.customCurrencyId" => $currencyID));
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoicesRelatedToDeliveryNoteByDeliveryNoteID($deliveryNoteID){
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProduct")
                ->columns(array("*"))
                ->join("salesInvoice", "salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID", array("*"), "left")
                ->where(array("documentTypeID" => 4,"salesInvoiceProductDocumentID" => $deliveryNoteID));
        $select->where->notEqualTo('salesInvoice.statusID',5);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function updateDeliveryNote($data, $deliveryNoteID)
    {
        $this->tableGateway->update($data, array('deliveryNoteID' => $deliveryNoteID));
        return TRUE;
    }

    /**
     * Get delivery note item details
     * @param string $fromDate
     * @param string $toDate
     * @param array $locationIds
     * @param array $customerIds
     * @param array $itemIds
     * @boolean $sort
     * @return mixed
     */
    public function getDeliverNoteItemDetails($fromDate = null, $toDate = null, $locationIds = [], $deliveryNoteIds = null, $customerIds = null, $itemIds = null, $sort = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('deliveryNote')
                ->columns(array("*"))
                ->join("deliveryNoteProduct", "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", ['deliveryNoteProductID','productID','deliveryNoteProductPrice','deliveryNoteProductQuantity', 'deliveryNoteProductTotal'], "left")
                ->join("product", "deliveryNoteProduct.productID = product.productID", ['productCode', 'productName'], "left")
                ->join("salesReturnProduct", "deliveryNoteProduct.deliveryNoteProductID = salesReturnProduct.deliveryNoteProductID",
                        [
                            'deliveryNoteItemReturnQty' => new Expression('SUM(salesReturnProduct.salesReturnProductQuantity)'),
                            'deliveryNoteItemReturnAmount' => new Expression('SUM(salesReturnProduct.salesReturnProductTotal)'),
                        ], "left")
                ->join("customer", "deliveryNote.customerID = customer.customerID", ['customerCode', 'customerName'], "left")
                ->join('customerProfile', 'deliveryNote.customerProfileID = customerProfile.customerProfileID', array('customerProfileName'), 'left')
                ->join('entity', 'entity.entityID=deliveryNote.entityID', ['deleted'], "left")
                ->where->in('deliveryNote.deliveryNoteLocationID', $locationIds)
                ->where->equalTo('entity.deleted', 0);
                if($fromDate && $toDate){
                    $select->where->between('deliveryNote.deliveryNoteDeliveryDate', $fromDate, $toDate);
                }
                if($deliveryNoteIds){
                    $select->where->in('deliveryNote.deliveryNoteID', $deliveryNoteIds);
                }
                if($customerIds){
                    $select->where->in('customer.customerID', $customerIds);
                }
                if($itemIds){
                    $select->where->in('product.productID', $itemIds);
                }
                if ($sort) {
                    $select->order(['customer.customerName' => 'ASE', 'deliveryNote.deliveryNoteCode' => 'ASE']);
                }
        $select->group("deliveryNoteProduct.deliveryNoteProductID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getDeliveryNoteStatusByDeliveryNoteID ($deliveryNoteID)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote')
                ->columns(array('deliveryNoteStatus','deliveryNoteID','deliveryNoteLocationID'))
                ->where(array('deliveryNoteID' => $deliveryNoteID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    /**
     * Get delivery note wise sales invoice
     * @param string $fromDate
     * @param string $toDate
     * @param array $deliverNoteIds
     * @return mixed
     */
    public function getDeliveryNoteWiseInvoices($fromDate = null, $toDate = null, $deliverNoteIds = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote')
                ->columns(['deliveryNoteID','deliveryNoteCode','deliveryNoteDeliveryDate','deliveryNotePriceTotal', 'deliveryNoteStatus','entityID'])
                ->join('salesInvoiceProduct', 'deliveryNote.deliveryNoteID = salesInvoiceProduct.salesInvoiceProductDocumentID', [
                    'copiedAmount' => new Expression('SUM(salesInvoiceProductTotal)'),'documentTypeID','salesInvoiceID'], 'left')
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', ['salesInvoiceCode','salesInvoiceIssuedDate','salesinvoiceTotalAmount','entityID','statusID'], 'left')
                ->join(['dstatus' => 'status'], 'deliveryNote.deliveryNoteStatus = dstatus.statusID', ['dnStatus' => 'statusName'], 'left')
                ->join(['sstatus' => 'status'], 'salesInvoice.statusID = sstatus.statusID', ['siStatus' => 'statusName'], 'left')
                ->join(['sentity' => 'entity'], 'salesInvoice.entityID = sentity.entityID', ['sideleted' => 'deleted'], 'left')
                ->join(['dentity' => 'entity'], 'deliveryNote.entityID = dentity.entityID', ['dndeleted' => 'deleted'], 'left')
                ->where->equalTo('salesInvoiceProduct.documentTypeID', 4)
                ->where->equalTo('sentity.deleted', 0)
                ->where->equalTo('dentity.deleted', 0);
        if ($fromDate && $toDate) {
            $select->where->between('deliveryNote.deliveryNoteDeliveryDate', $fromDate, $toDate);
        }
        if ($deliverNoteIds) {
            $select->where->in('deliveryNote.deliveryNoteID', $deliverNoteIds);
        }
        $select->group(['deliveryNote.deliveryNoteID','salesInvoice.salesInvoiceID']);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    public function updateDeliveryNoteStatusToOpen($deliveryNoteID, $deliveryNoteProData)
    {
        $this->tableGateway->update($deliveryNoteProData, array('deliveryNoteID' => $deliveryNoteID));
    }

    public function getDeliveryNotes()
    {
        $allDlnList = array();
        foreach ($this->fetchAll() as $dln) {
            $dln = (object) $dln;
            $allDlnList[$dln->deliveryNoteID] = $dln->deliveryNoteCode;
        }

        return $allDlnList;
    }

    // get sales order related delivery note by quotation id
    public function getSalesOrderRelatedDeliveryNoteDataByQuotationId($quotationID = null, $soID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->columns(array('*'));
        $select->join('salesOrders', 'deliveryNote.salesOrderID = salesOrders.soID', array('*'),'left');
        $select->join('entity', 'deliveryNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->where(array('salesOrders.quotationID' => $quotationID));
        }
        if (!is_null($soID)) {
            $select->where(array('salesOrders.soID' => $soID));
        }
        $select->group("deliveryNote.deliveryNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    // get delivery note basic details with time stamp by delivery note id
    public function getDeliveryNoteDetailsByDeliveryNoteId($deliveryNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->columns(array('*'));
        $select->join('entity', 'deliveryNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('deliveryNote.deliveryNoteID' => $deliveryNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;
    }

    // get invoice related delivery note by invoice id
    public function getinvoiceRelatedDeliveryNoteDataByInvoiceId($salesInvoiceID = null, $creditNoteID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->columns(array('*', 'dlnID' => new Expression('deliveryNote.deliveryNoteID')));
        $select->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
        $select->join('entity', 'deliveryNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($salesInvoiceID)) {
            $select->where(array('salesInvoice.salesInvoiceID' => $salesInvoiceID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($creditNoteID)) {
            $select->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
            $select->where(array('creditNote.creditNoteID' => $creditNoteID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        $select->group("deliveryNote.deliveryNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    // get return related delivery note by return id
    public function getReturnRelatedDeliveryNoteDataByReturnId($returnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->columns(array('*'));
        $select->join('deliveryNoteProduct', "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array('*'), 'left');
        $select->join('salesReturnProduct', "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array('*'), 'left');
        $select->join('salesReturn', "salesReturn.salesReturnID = salesReturnProduct.salesReturnID", array('*'), 'left');
        $select->join('entity', 'deliveryNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesReturn.salesReturnID' => $returnID));
        $select->group("deliveryNote.deliveryNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        return $rowset;
    }

    public function getDeliveryNoteWiseSalesReturn($fromDate = null, $toDate = null, $deliverNoteIds = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote')
                ->columns(['deliveryNoteID','deliveryNoteCode','deliveryNoteDeliveryDate','deliveryNotePriceTotal', 'deliveryNoteStatus','entityID'])
                ->join('deliveryNoteProduct', 'deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID', array('*'),'left')
                ->join('salesReturnProduct', 'deliveryNoteProduct.deliveryNoteProductID = salesReturnProduct.deliveryNoteProductID', [
                    'copiedAmount' => new Expression('SUM(salesReturnProductTotal)'),'salesReturnID'], 'left')
                ->join('salesReturn', 'salesReturnProduct.salesReturnID = salesReturn.salesReturnID', ['salesReturnCode','salesReturnDate','salesReturnTotal','entityID','statusID'], 'left')
                ->join(['dstatus' => 'status'], 'deliveryNote.deliveryNoteStatus = dstatus.statusID', ['dnStatus' => 'statusName'], 'left')
                ->join(['sstatus' => 'status'], 'salesReturn.statusID = sstatus.statusID', ['siStatus' => 'statusName'], 'left')
                ->join(['sentity' => 'entity'], 'salesReturn.entityID = sentity.entityID', ['sideleted' => 'deleted'], 'left')
                ->join(['dentity' => 'entity'], 'deliveryNote.entityID = dentity.entityID', ['dndeleted' => 'deleted'], 'left')
                ->where->equalTo('sentity.deleted', 0)
                ->where->equalTo('dentity.deleted', 0);
        if ($fromDate && $toDate) {
            $select->where->between('deliveryNote.deliveryNoteDeliveryDate', $fromDate, $toDate);
        }
        if ($deliverNoteIds) {
            $select->where->in('deliveryNote.deliveryNoteID', $deliverNoteIds);
        }
        $select->group(['deliveryNote.deliveryNoteID','salesReturn.salesReturnID']);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset;
    }

    //get the delivery note item details by delivery note id
    public function getDeliveryNoteProductByDeliveryNoteId($deliveryNoteID, $categoryIds=null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNote")
                ->columns(array("*"))
                ->join("deliveryNoteProduct", "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array("*"), "left")
                ->join('status', 'deliveryNote.deliveryNoteStatus = status.statusID', array('statusName'), 'left');
        $select->join('product', 'product.productID = deliveryNoteProduct.productID', array(
            'pName' => new Expression('productName'),
            'pID' => new Expression('product.productID'),
            'pCD' => new Expression('productCode')
                ), 'left');
        $select->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left');
        $select->join('locationProduct','locationProduct.productID = deliveryNoteProduct.productID',array('locationProductID'),'left');
        $select->join('location', 'location.locationID = deliveryNote.deliveryNoteLocationID', array(
            'locationName', 'locationCode',
            'locID' => new Expression('deliveryNote.deliveryNoteLocationID')
                ), 'left');
        $select->where->equalTo('deliveryNote.deliveryNoteID',$deliveryNoteID);
        $select->where('locationProduct.locationID = deliveryNote.deliveryNoteLocationID');
        if (!is_null($categoryIds)) {
            $select->where->in('product.categoryID', $categoryIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    //get issued delivery note item details
    public function getIssuedDeliveryNotesItemDetails($fromDate, $toDate, $locIDs, $categoryIds = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('deliveryNote')
                ->columns(array('deliveryNoteID', 'deliveryNoteCode',
                    'deliveryNoteDeliveryDate',
                    'locID' => new Expression('deliveryNoteLocationID')))
                ->join('deliveryNoteProduct', 'deliveryNoteProduct.deliveryNoteID=deliveryNote.deliveryNoteID', array(
                    'qty' => new Expression('deliveryNoteProductQuantity'),'deliveryNoteProductID'
                        ), 'left')
                ->join(['dproduct' => 'product'], 'dproduct.productID = deliveryNoteProduct.productID', array(
                    'pName' => new Expression('dproduct.productName'),
                    'pID' => new Expression('dproduct.productID'),
                    'pCD' => new Expression('dproduct.productCode')
                        ), 'left')
                ->join('category', 'dproduct.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left')
                ->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'), 'left')
                ->join(['sproduct' => 'product'], 'sproduct.productID = salesInvoiceProduct.productID', array(
                    'psName' => new Expression('sproduct.productName'),
                    'psID' => new Expression('sproduct.productID'),
                    'psCD' => new Expression('sproduct.productCode')
                        ), 'left')
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesInvoiceCode','locationID','salesInvoiceIssuedDate','statusID'), 'left')
                ->join(['dstatus' => 'status'], 'deliveryNote.deliveryNoteStatus = dstatus.statusID', ['dnStatus' => 'statusName'], 'left')
                ->join(['sstatus' => 'status'], 'salesInvoice.statusID = sstatus.statusID', ['siStatus' => 'statusName'], 'left')
                ->join('location', 'location.locationID = deliveryNote.deliveryNoteLocationID', array(
                    'locationName', 'locationCode'
                        ), 'left');
        $select->join('locationProduct','locationProduct.productID = deliveryNoteProduct.productID',array('dlnLocationProductID'=> 'locationProductID'),'left');
        $select->where(array('dproduct.productState' => 1));
        $select->where->notEqualTo('deliveryNoteStatus', 5);
        $select->where->notEqualTo('deliveryNoteStatus', 10);
        $select->where->between('deliveryNoteDeliveryDate', $fromDate, $toDate);
        $select->where('locationProduct.locationID = deliveryNote.deliveryNoteLocationID');
        if (!empty($locIDs)) {
            $select->where->in('deliveryNoteLocationID', $locIDs);
        }
        if (!is_null($categoryIds)) {
            $select->where->in('dproduct.categoryID', $categoryIds);
            $select->where->in('sproduct.categoryID', $categoryIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    // get delivery note basic details with time stamp by job id
    public function getDeliveryNoteDetailsByJobId($jobId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->columns(array('*'));
        $select->join('entity', 'deliveryNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('deliveryNote.jobId' => $jobId));
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus',4);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;

    }

    public function getAnyDeliveryNoteDetailsByJobId($jobId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->columns(array('*'));
        $select->join('entity', 'deliveryNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('deliveryNote.jobId' => $jobId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;

    }
    
    public function getDeliveryNoteProductsByJobID($jobID, $copyFlag = false, $statusFlag = true)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->join('deliveryNoteProduct', 'deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID', array('*'), 'left');
        $select->where(array('deliveryNote.jobId' => $jobID));
        if ($statusFlag) {
            $select->where(array('deliveryNote.deliveryNoteStatus'=> 3));
        }
        if (!$copyFlag) {
            $select->where(array('deliveryNoteProduct.copied' => 0));
        }
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getDeliveryNoteProductCostByDeliveryNoteProductID($deliveryNoteProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNote');
        $select->join('deliveryNoteProduct', 'deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID', array('*'), 'left');
        $select->join('locationProduct','locationProduct.productID = deliveryNoteProduct.productID AND deliveryNote.deliveryNoteLocationID = locationProduct.locationID',array('locationProductID'),'left');
        $select->join('itemOut','deliveryNoteProduct.deliveryNoteID = itemOut.itemOutDocumentID AND locationProduct.locationProductID = itemOut.itemOutLocationProductID',array('itemOutItemInID'),'left');
        $select->join('itemIn','itemIn.itemInID = itemOut.itemOutItemInID',array('itemInPrice','itemInDiscount'),'left');
        $select->where(array('deliveryNoteProduct.deliveryNoteProductID' => $deliveryNoteProductID, 'itemOut.itemOutDocumentType' => 'Delivery Note'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset->current();
    }
}
