<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Customer
{

    public $customerID;
    public $customerTitle;
    public $customerCode;
    public $customerName;
    public $customerShortName;
    public $customerTelephoneNumber;
//    public $customerAdditionalTelephoneNumber;
//    public $customerEmail;
//    public $customerAddress;
    public $customerIdentityNumber;
    public $customerCategory;
    public $customerPriceList;
    public $customerDesignation;
    public $customerDepartment;
    public $customerVatNumber;
    public $customerSVatNumber;
    public $customerPaymentTerm;
    public $customerCreditLimit;
    public $customerCurrentBalance;
    public $customerCurrentCredit;
    public $customerDiscount;
    public $customerOther;
    public $customerCurrency;
    public $customerEvent;
    public $customerGender;
    public $customerReceviableAccountID;
    public $customerSalesAccountID;
    public $customerSalesDiscountAccountID;
    public $customerAdvancePaymentAccountID;
    public $entityID;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->customerTitle = (!empty($data['customerTitle'])) ? $data['customerTitle'] : null;
        $this->customerCode = (!empty($data['customerCode'])) ? $data['customerCode'] : null;
        $this->customerName = (!empty($data['customerName'])) ? $data['customerName'] : null;
        $this->customerShortName = (!empty($data['customerShortName'])) ? $data['customerShortName'] : null;
        $this->customerTelephoneNumber = (!empty($data['customerTelephoneNumber'])) ? $data['customerTelephoneNumber'] : null;
//        $this->customerAdditionalTelephoneNumber = (!empty($data['customerAdditionalTelephoneNumber'])) ? $data['customerAdditionalTelephoneNumber'] : null;
//        $this->customerEmail = (!empty($data['customerEmail'])) ? $data['customerEmail'] : null;
//        $this->customerAddress = (!empty($data['customerAddress'])) ? $data['customerAddress'] : null;
        $this->customerDateOfBirth = (!empty($data['customerDateOfBirth'])) ? $data['customerDateOfBirth'] : null;
        $this->customerIdentityNumber = (!empty($data['customerIdentityNumber'])) ? $data['customerIdentityNumber'] : null;
        $this->customerCategory = (!empty($data['customerCategory'])) ? $data['customerCategory'] : '';
        $this->customerPriceList = (!empty($data['customerPriceList'])) ? $data['customerPriceList'] : null;
        $this->customerDesignation = (!empty($data['customerDesignation'])) ? $data['customerDesignation'] : '';
        $this->customerDepartment = (!empty($data['customerDepartment'])) ? $data['customerDepartment'] : '';
        $this->customerVatNumber = (!empty($data['customerVatNumber'])) ? $data['customerVatNumber'] : null;
        $this->customerSVatNumber = (!empty($data['customerSVatNumber'])) ? $data['customerSVatNumber'] : null;
        $this->customerPaymentTerm = (!empty($data['customerPaymentTerm'])) ? $data['customerPaymentTerm'] : 1;
        $this->customerCreditLimit = (!empty($data['customerCreditLimit'])) ? $data['customerCreditLimit'] : 0;
        $this->customerCurrentBalance = (!empty($data['customerCurrentBalance'])) ? $data['customerCurrentBalance'] : 0;
        $this->customerCurrentCredit = (!empty($data['customerCurrentCredit'])) ? $data['customerCurrentCredit'] : 0;
        $this->customerDiscount = (!empty($data['customerDiscount'])) ? $data['customerDiscount'] : 0;
        $this->customerOther = (!empty($data['customerOther'])) ? $data['customerOther'] : null;
        $this->customerCurrency = (!empty($data['customerCurrency'])) ? $data['customerCurrency'] : null;
        $this->customerEvent = (!empty($data['customerEvent'])) ? $data['customerEvent'] : null;
        $this->customerReceviableAccountID = (!empty($data['customerReceviableAccountID'])) ? $data['customerReceviableAccountID'] : null;
        $this->customerSalesAccountID = (!empty($data['customerSalesAccountID'])) ? $data['customerSalesAccountID'] : null;
        $this->customerSalesDiscountAccountID = (!empty($data['customerSalesDiscountAccountID'])) ? $data['customerSalesDiscountAccountID'] : null;
        $this->customerAdvancePaymentAccountID = (!empty($data['customerAdvancePaymentAccountID'])) ? $data['customerAdvancePaymentAccountID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->customerGender = (!empty($data['customerGender'])) ? $data['customerGender'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 100,
                            ),
                        ),
                    ),
        )));

        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerShortName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 100,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerAddress',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 300,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerOther',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 500,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerTelephoneNumber',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 30,
                            ),
                        ),
                    ),
        )));

        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
