<?php

namespace Invoice\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterInterface;

class SalesOrder
{

    public $soID;
    public $soCode;
    public $quotationID;
    public $customerName;
    public $customerID;
    public $issuedDate;
    public $expireDate;
    public $payementTerm;
    public $taxAmount;
    public $totalDiscount;
    public $totalAmount;
    public $comment;
    public $branch;
    public $showTax;
    public $footerID;
    public $statusID;
    public $entityID;
    public $salesPersonID;
    public $customCurrencyId;
    public $salesOrdersCustomCurrencyRate;
    public $priceListId;
    public $status;
    public $customerProfileID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->soID = (!empty($data['soID'])) ? $data['soID'] : null;
        $this->soCode = (!empty($data['soCode'])) ? $data['soCode'] : null;
        $this->quotationID = (!empty($data['quotationID'])) ? $data['quotationID'] : 0;
        $this->customerName = (!empty($data['customerName'])) ? $data['customerName'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->issuedDate = (!empty($data['issuedDate'])) ? $data['issuedDate'] : null;
        $this->expireDate = (!empty($data['expireDate'])) ? $data['expireDate'] : null;
        $this->payementTerm = (!empty($data['payementTerm'])) ? (int) $data['payementTerm'] : null;
        $this->taxAmount = (!empty($data['taxAmount'])) ? $data['taxAmount'] : null;
        $this->totalDiscount = (!empty($data['totalDiscount'])) ? $data['totalDiscount'] : null;
        $this->totalAmount = (!empty($data['totalAmount'])) ? $data['totalAmount'] : null;
        $this->comment = (!empty($data['comment'])) ? $data['comment'] : null;
        $this->branch = (!empty($data['branch'])) ? $data['branch'] : null;
        $this->showTax = (!empty($data['showTax'])) ? $data['showTax'] : 0;
        $this->footerID = (!empty($data['footer_id'])) ? $data['footer_id'] : 0;
        $this->statusID = (!empty($data['state'])) ? $data['state'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : 0;
        $this->salesPersonID = (!empty($data['salesPersonID'])) ? $data['salesPersonID'] : NULL;
        $this->customCurrencyId = (!empty($data['customCurrencyId'])) ? $data['customCurrencyId'] : NULL;
        $this->salesOrdersCustomCurrencyRate = (!empty($data['salesOrdersCustomCurrencyRate'])) ? $data['salesOrdersCustomCurrencyRate'] : 0;
        $this->priceListId = (!empty($data['priceListId'])) ? $data['priceListId'] : 0;
        $this->status = (!empty($data['status'])) ? $data['status'] : 0;
        $this->customerProfileID = (!empty($data['customerProfileID'])) ? $data['customerProfileID'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new inputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'so_no',
                        'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'cust_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'issue_date',
                        'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'completion_date',
                        'required' => true,
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
