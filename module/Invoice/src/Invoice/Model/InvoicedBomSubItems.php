<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoicedBomSubItems implements InputFilterAwareInterface
{

    public $invoicedBomSubItemID;
    public $salesInvoiceID;
    public $salesInvoiceProductID;
    public $bomID;
    public $subProductID;
    public $subProductBomPrice;
    public $subProductBomQty;
    public $subProductTotalQty;
    public $subProductTotal;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->invoicedBomSubItemID = (!empty($data['invoicedBomSubItemID'])) ? $data['invoicedBomSubItemID'] : null;
        $this->salesInvoiceID = (!empty($data['salesInvoiceID'])) ? $data['salesInvoiceID'] : null;
        $this->salesInvoiceProductID = (!empty($data['salesInvoiceProductID'])) ? $data['salesInvoiceProductID'] : null;
        $this->bomID = (!empty($data['bomID'])) ? $data['bomID'] : null;
        $this->subProductID = (!empty($data['subProductID'])) ? $data['subProductID'] : null;
        $this->subProductBomPrice = (!empty($data['subProductBomPrice'])) ? $data['subProductBomPrice'] : null;
        $this->subProductBomQty = (!empty($data['subProductBomQty'])) ? $data['subProductBomQty'] : null;
        $this->subProductTotalQty = (!empty($data['subProductTotalQty'])) ? $data['subProductTotalQty'] : null;
        $this->subProductTotal = (!empty($data['subProductTotal'])) ? $data['subProductTotal'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
