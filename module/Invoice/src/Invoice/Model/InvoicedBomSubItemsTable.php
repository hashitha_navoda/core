<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\NotIn;

class InvoicedBomSubItemsTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    

    public function saveInvoicedBomSubProduct(InvoicedBomSubItems $data)
    {
        $dataSet = array(
            'salesInvoiceID' => $data->salesInvoiceID,
            'salesInvoiceProductID' => $data->salesInvoiceProductID,
            'bomID' => $data->bomID,
            'subProductID' => $data->subProductID,
            'subProductBomPrice' => $data->subProductBomPrice,
            'subProductBomQty' => $data->subProductBomQty,
            'subProductTotalQty' => $data->subProductTotalQty,
            'subProductTotal' => $data->subProductTotal,
        );

        if ($this->tableGateway->insert($dataSet)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getInvoicedBomSubItemsByBomItemIDAndDateRange($fromDate, $toDate, $bomIDs, $isAllLocation = false, $locations) {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('invoicedBomSubItems')
                ->columns(array('*'))
                ->join('bom', 'invoicedBomSubItems.bomID =  bom.bomID', array('*'), 'left')
                ->join('product', 'bom.productID =  product.productID', array('productCode','productName'), 'left')
                ->join('salesInvoice', 'invoicedBomSubItems.salesInvoiceID =  salesInvoice.salesInvoiceID', array('salesInvoiceIssuedDate'), 'left')
                ->join('location', 'salesInvoice.locationID =  location.locationID', array('*'), 'left')
                ->join("entity", "entity.entityID = salesInvoice.entityID", ['deleted'], 'left')
                ->where->in('invoicedBomSubItems.bomID', $bomIDs);
        $select->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);

        if (!$isAllLocation) {
            $select->where->in('salesInvoice.locationID', $locations);
        }
        
                // ->where(array('productUomConversion' => '1'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }






    public function getProductsByInvoiceID($invoiceID, $flag = false)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceProduct')
                ->columns(array('*'))
                ->join('product', 'salesInvoiceProduct.productID =  product.productID', array('*'), 'left')
//                ->join('locationProduct', 'locationProduct.productID =  product.productID', array('*'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProductTax.salesInvoiceProductID =  salesInvoiceProduct.salesInvoiceProductID', array('taxTotal' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay', 'productUomBase'))
                ->join('uom', 'salesInvoiceProduct.salesInvoiceProductSelectedUomId = uom.uomID', array('uomAbbr', 'uomDecimalPlace'),'left')
                ->group(array('salesInvoiceProduct.salesInvoiceProductID'))
                ->where(array('salesInvoiceID' => $invoiceID));
        if($flag){
            $select->where(array('productUomBase' => '1'));

        }
                // ->where(array('productUomConversion' => '1'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    
}
