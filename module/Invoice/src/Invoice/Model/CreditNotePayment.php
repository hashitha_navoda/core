<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains credit note payment Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CreditNotePayment implements InputFilterAwareInterface
{

    public $creditNotePaymentID;
    public $creditNotePaymentCode;
    public $creditNotePaymentDate;
    public $paymentTermID;
    public $creditNotePaymentAmount;
    public $customerID;
    public $creditNotePaymentDiscount;
    public $creditNotePaymentMemo;
    public $locationID;
    public $statusID;
    public $creditNotePaymentCancelMessage;
    public $customCurrencyId;
    public $creditNotePaymentCustomCurrencyRate;
    public $creditNotePaymentLoyaltyAmount;
    public $entityID;
    protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->creditNotePaymentID = (!empty($data['creditNotePaymentID'])) ? $data['creditNotePaymentID'] : null;
        $this->creditNotePaymentCode = (!empty($data['creditNotePaymentCode'])) ? $data['creditNotePaymentCode'] : null;
        $this->creditNotePaymentDate = (!empty($data['creditNotePaymentDate'])) ? $data['creditNotePaymentDate'] : null;
        $this->paymentTermID = (!empty($data['paymentTermID'])) ? $data['paymentTermID'] : null;
        $this->creditNotePaymentAmount = (!empty($data['creditNotePaymentAmount'])) ? $data['creditNotePaymentAmount'] : 0.00;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->creditNotePaymentDiscount = (!empty($data['creditNotePaymentDiscount'])) ? $data['creditNotePaymentDiscount'] : 0.00;
        $this->creditNotePaymentMemo = (!empty($data['creditNotePaymentMemo'])) ? $data['creditNotePaymentMemo'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->statusID = (!empty($data['statusID'])) ? $data['statusID'] : null;
        $this->creditNotePaymentCancelMessage = (!empty($data['creditNotePaymentCancelMessage'])) ? $data['creditNotePaymentCancelMessage'] : null;
        $this->customCurrencyId = (!empty($data['customCurrencyId'])) ? $data['customCurrencyId'] : null;
        $this->creditNotePaymentCustomCurrencyRate = (!empty($data['creditNotePaymentCustomCurrencyRate'])) ? $data['creditNotePaymentCustomCurrencyRate'] : 0.00;
        $this->creditNotePaymentLoyaltyAmount = (!empty($data['creditNotePaymentLoyaltyAmount'])) ? $data['creditNotePaymentLoyaltyAmount'] : 0.00;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

// Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
