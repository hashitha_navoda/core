<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\CompoundCostType;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class ServiceChargeTypeTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function ServiceChargeTypeSearchByKey($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceChargeType');
        $select->where(array('serviceChargeTypeIsDelete' => '0'));
        $select->where(new PredicateSet(array(new Operator('serviceChargeTypeName', 'like', '%' . $searchKey . '%'), new Operator('serviceChargeTypeCode', 'like', '%' . $searchKey . '%')), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getAllServiceChargeTypes()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceChargeType');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function fetchAll($paginated = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceChargeType');
        $select->where(array('serviceChargeTypeIsDelete' =>0));
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter());
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    public function getServiceChargeTypeDetailsByCode($code)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceChargeType');
        $select->where(array('serviceChargeTypeIsDelete' => 0, 'serviceChargeTypeCode' => $code));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getServiceChargeTypeDetailsById($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('serviceChargeType');
        $select->where(array('serviceChargeTypeIsDelete' => 0, 'serviceChargeTypeID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function save($serviceChargeType)
    {
        try {
            $data = array(
                'serviceChargeTypeName' => $serviceChargeType->serviceChargeTypeName,
                'serviceChargeTypeCode' => $serviceChargeType->serviceChargeTypeCode,
                'serviceChargeTypeGLAccountID' => $serviceChargeType->serviceChargeTypeGLAccountID,
                'locationID' => $serviceChargeType->locationID,
            );
            $this->tableGateway->insert($data);
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;

        } catch (\Exception $exc) {
            return null;
            error_log($exc);

        }
    }

    public function update($data, $srcTypeID)
    {
        try {
            $result = $this->tableGateway->update($data, array('serviceChargeTypeID' => $srcTypeID));
            return $result;
        } catch (\Exception $exc) {
            return null;
            error_log($exc);
        }
    }

}
