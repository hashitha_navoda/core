<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoiceProductDeliveryNoteProduct implements InputFilterAwareInterface
{

    public $invoiceProductDeliveryNoteProductID;
    public $jobID;
    public $jobCostID;
    public $deliveryNoteProductID;
    public $invoiceProductID;
    public $jobProductCostQty;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->invoiceProductDeliveryNoteProductID = (!empty($data['invoiceProductDeliveryNoteProductID'])) ? $data['invoiceProductDeliveryNoteProductID'] : null;
        $this->jobID = (!empty($data['jobID'])) ? $data['jobID'] : null;
        $this->jobCostID = (!empty($data['jobCostID'])) ? $data['jobCostID'] : null;
        $this->deliveryNoteProductID = (!empty($data['deliveryNoteProductID'])) ? $data['deliveryNoteProductID'] : null;
        $this->invoiceProductID = (!empty($data['invoiceProductID'])) ? $data['invoiceProductID'] : null;
        $this->jobProductCostQty = (!empty($data['jobProductCostQty'])) ? $data['jobProductCostQty'] : null;
        
    }

     public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }
}
