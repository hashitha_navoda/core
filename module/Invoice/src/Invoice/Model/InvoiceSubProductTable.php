<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class InvoiceSubProductTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('invoice')
                    ->columns(array('*'))
                    ->order(array('issuedDate' => 'DESC'));
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Invoice());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveInvoiceSubProduct(InvoiceSubProduct $invoiceSubProduct)
    {  
        
        $data = array(
            'salesInvocieProductID' => $invoiceSubProduct->salesInvocieProductID,
            'productBatchID' => $invoiceSubProduct->productBatchID,
            'productSerialID' => $invoiceSubProduct->productSerialID,
            'salesInvoiceSubProductQuantity' => $invoiceSubProduct->salesInvoiceSubProductQuantity,
            'salesInvoiceSubProductWarranty' => $invoiceSubProduct->salesInvoiceSubProductWarranty,
            'salesInvoiceSubProductWarrantyType' => $invoiceSubProduct->salesInvoiceSubProductWarrantyType,
        );
        
        if (!$this->tableGateway->insert($data)) {
            return FALSE;
        }

        $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $result;
    }

    public function getSalesInvoiceSubProductQtyByInvoiceIDAndProductID($invoiceID,$batchID,$salesInvoiceProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoiceSubProduct')
               ->columns(array('sum' => new Expression ('SUM(salesInvoiceSubProductQuantity)')))
               ->join('salesInvoiceProduct','salesInvoiceSubProduct.salesInvocieProductID = salesInvoiceProduct.salesInvoiceProductID',array('salesInvoiceProductID'),'left')
               ->where(array(
                    'salesInvoiceProduct.salesInvoiceID' => $invoiceID,
                    'salesInvoiceSubProduct.productBatchID' => $batchID,
                    'salesInvoiceProduct.salesInvoiceProductID' => $salesInvoiceProductID
                    ));    

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return iterator_to_array($result);
    }

    public function getSalesInvoiceSubProductBySalesInvoiceID($salesInvoiceProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoiceSubProduct')
               ->columns(array('*'))
               ->where(array(
                    'salesInvoiceSubProduct.salesInvocieProductID' => $salesInvoiceProductID
                ));    
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return iterator_to_array($result);
    }


}
