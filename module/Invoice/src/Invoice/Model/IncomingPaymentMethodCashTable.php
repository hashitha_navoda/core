<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodCash related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;

class IncomingPaymentMethodCashTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCashPayment(IncomingPaymentMethodCash $incomingPaymentMethodCash)
    {
        $cashData = array(
            'incomingPaymentMethodCashId' => $incomingPaymentMethodCash->incomingPaymentMethodCashId,
            'cashAccountID' => $incomingPaymentMethodCash->cashAccountID,
        );
        if ($this->tableGateway->insert($cashData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

}
