<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoiceServiceCharge
{

    public $invoiceServiceChargeID;
    public $invoiceID;
    public $serviceChargeTypeID;
    public $serviceChargeAmount;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->invoiceServiceChargeID = (!empty($data['invoiceServiceChargeID'])) ? $data['invoiceServiceChargeID'] : null;
        $this->invoiceID = (!empty($data['invoiceID'])) ? $data['invoiceID'] : null;
        $this->serviceChargeTypeID = (!empty($data['serviceChargeTypeID'])) ? $data['serviceChargeTypeID'] : null;
        $this->serviceChargeAmount = (!empty($data['serviceChargeAmount'])) ? $data['serviceChargeAmount'] : null;
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
