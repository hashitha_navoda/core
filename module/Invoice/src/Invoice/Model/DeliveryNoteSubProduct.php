<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DeliveryNoteSubProduct
{

    public $deliveryNoteSubProductID;
    public $deliveryNoteProductID;
    public $productBatchID;
    public $productSerialID;
    public $deliveryProductSubQuantity;
    public $deliveryNoteSubProductsWarranty;
    public $deliveryNoteSubProductsCopiedQuantity;
    protected $inputFilter;
    public $deliveryNoteSubProductsWarrantyType;

    public function exchangeArray($data)
    {
        $this->deliveryNoteSubProductID = (!empty($data['deliveryNoteSubProductID'])) ? $data['deliveryNoteSubProductID'] : null;
        $this->deliveryNoteProductID = (!empty($data['deliveryNoteProductID'])) ? $data['deliveryNoteProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->deliveryProductSubQuantity = (!empty($data['deliveryProductSubQuantity'])) ? $data['deliveryProductSubQuantity'] : null;
        $this->deliveryNoteSubProductsWarranty = (!empty($data['deliveryNoteSubProductsWarranty'])) ? $data['deliveryNoteSubProductsWarranty'] : null;
        $this->deliveryNoteSubProductsCopiedQuantity = (!empty($data['deliveryNoteSubProductsCopiedQuantity'])) ? $data['deliveryNoteSubProductsCopiedQuantity'] : 0;
        $this->deliveryNoteSubProductsWarrantyType = (!empty($data['deliveryNoteSubProductsWarrantyType'])) ? $data['deliveryNoteSubProductsWarrantyType'] : 1;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
