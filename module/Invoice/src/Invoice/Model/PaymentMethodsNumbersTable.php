<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains PaymentMethodsNumbersTable Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

class PaymentMethodsNumbersTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        if ($paginated) {
            $select = new Select('paymentMethodsNumbers');
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new PaymentMethodsNumbers());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function savePaymentMethodsNumbers(PaymentMethodsNumbers $PaymentMethodsNumbers)
    {
        $data = array(
            'paymentID' => $PaymentMethodsNumbers->paymentID,
            'paymentMethodID' => $PaymentMethodsNumbers->paymentMethodID,
            'paymentMethodReferenceNumber' => $PaymentMethodsNumbers->paymentMethodReferenceNumber,
            'paymentMethodBank' => $PaymentMethodsNumbers->paymentMethodBank,
            'paymentMethodCardID' => $PaymentMethodsNumbers->paymentMethodCardID,
            'paymentMethodAmount' => $PaymentMethodsNumbers->paymentMethodAmount,
        );

        $this->tableGateway->insert($data);
        return true;
    }

    /**
     * Get all the customers cheques within the given date range.
     *
     * @param Srting $customerName Name of the customer
     * @param String $startDate start date
     * @param String $endDate end date
     *
     * @return Result
     */
    public function getCustomerChequesByPaymentDate($customerId = NULL, $paymentId = NULL, $startDate = NULL, $endDate = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('paymentMethodsNumbers')
                ->columns(array('paymentMethodReferenceNumber', 'paymentMethodBank'))
                ->join('incomingPayment', 'paymentMethodsNumbers.paymentID = incomingPayment.incomingPaymentID', array('incomingPaymentID', 'incomingPaymentCode', 'incomingPaymentDate', 'incomingPaymentAmount'))
                ->join('customer', 'incomingPayment.customerID = customer.customerID ', array('customerID', 'customerName'))
        ->where->equalTo('paymentMethodsNumbers.paymentMethodID', 2);
        if ($customerId) {
            $select->where->equalTo('customer.customerID', $customerId);
        }
        if ($paymentId) {
            $select->where->equalTo('incomingPayment.incomingPaymentID', $paymentId);
        }
        if ($startDate && $endDate) {
            $select->where->between('incomingPayment.incomingPaymentDate', $startDate, $endDate);
        }
        $select->order('incomingPayment.incomingPaymentID DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
