<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains creditc note sub product Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class CreditNoteSubProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCreditNoteSubProduct(CreditNoteSubProduct $creditNoteSubProduct)
    {
        $data = array(
            'creditNoteProductID' => $creditNoteSubProduct->creditNoteProductID,
            'productBatchID' => $creditNoteSubProduct->productBatchID,
            'productSerialID' => $creditNoteSubProduct->productSerialID,
            'creditNoteSubProductQuantity' => $creditNoteSubProduct->creditNoteSubProductQuantity,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getCreditNoteSubProductByCreditNoteID($creditNoteProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNoteSubProduct')
               ->where(array('creditNoteProductID' => $creditNoteProductID));    

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return $result;

    }

    public function getCreditNoteSubProductQtyByInvoiceIDAndProductID($invoiceID,$batchID,$creditNoteProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNoteSubProduct')
               ->columns(array('sum' => new Expression ('SUM(creditNoteSubProductQuantity)')))
               ->join('creditNoteProduct','creditNoteSubProduct.creditNoteProductID = creditNoteProduct.creditNoteProductID',array('creditNoteProductID'),'left')
               ->join('creditNote','creditNoteProduct.creditNoteID = creditNote.creditNoteID',array('creditNoteID'),'left')
               ->where(array('creditNote.invoiceID' => $invoiceID,'creditNoteSubProduct.productBatchID' => $batchID,'creditNoteProduct.creditNoteProductID' => $creditNoteProductID));    

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        return iterator_to_array($result);
    }

}
