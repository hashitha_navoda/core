<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class DispatchNoteTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     * Save dispatch note
     * @param \Invoice\Model\DispatchNote $dispatchNote
     * @return mixed
     */
    public function saveDispatchNote(DispatchNote $dispatchNote)
    {
        $data = array(
            'dispatchNoteCode' => $dispatchNote->dispatchNoteCode,
            'dispatchNoteDate' => $dispatchNote->dispatchNoteDate,
            'dispatchNoteAddress' => $dispatchNote->dispatchNoteAddress,
            'dispatchNoteComment' => $dispatchNote->dispatchNoteComment,
            'invoiceId' => $dispatchNote->invoiceId,
            'salesPersonId' => $dispatchNote->salesPersonId,
            'entityId' => $dispatchNote->entityId
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * Get dispatched quantity of the sales invoice product
     * @param int $invoiceId
     * @param int $invoiceProductId
     * @return float
     */
    public function getDispatchedQuantityOfInvoiceProduct($invoiceId, $invoiceProductId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNote')
                ->join('dispatchNoteProduct', 'dispatchNote.dispatchNoteId = dispatchNoteProduct.dispatchNoteId', array('salesInvoiceProductId'))
                ->join('entity', 'dispatchNote.entityId = entity.entityID', array('deleted'))
                ->columns(array(new Expression('SUM(dispatchNoteProductQuantity) as dispatchedQuantity')))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('dispatchNote.invoiceId', $invoiceId)
        ->where->equalTo('dispatchNoteProduct.salesInvoiceProductId', $invoiceProductId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->current();
        if (is_null($result['dispatchedQuantity'])) {
            return 0;
        }
        return $result['dispatchedQuantity'];
    }

    /**
     * Get active (not deleted) dispatch notes
     * @param boolean $paginated
     * @param array $order
     * @return mixed
     */
    public function getDispatchNotes($paginated = FALSE, $order = array())
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNote')
                ->columns(array('*'))
                ->join('salesInvoice', 'dispatchNote.invoiceId = salesInvoice.salesInvoiceID', array('customerID'))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("customerName", "customerStatus"))
                ->join('entity', 'dispatchNote.entityId = entity.entityID', array('deleted'))
                ->order($order)
        ->where->equalTo('entity.deleted', 0);
        if ($paginated) {
            $paginatorAdapter = new DbSelect($select, $this->tableGateway->getAdapter());
            return new Paginator($paginatorAdapter);
        } else {
            $statement = $sql->prepareStatementForSqlObject($select);
            $result = $statement->execute();
            return $result;
        }
    }

    /**
     * Search Dispatch Note for dropdown
     * @param string $searchKey
     * @return mixed
     */
    public function searchDispatchNoteCodeForDropDown($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNote')
                ->columns(array('dispatchNoteId', 'dispatchNoteCode', 'entityId'))
                ->join('entity', 'dispatchNote.entityId = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
                ->where(new PredicateSet(array(new Operator('dispatchNoteCode', 'like', '%' . $searchKey . '%'))));
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get dispatch notes of the given customer
     * @param string $fromDate
     * @param string $toDate
     * @return mixed
     */
    public function getDispatchNotesByCustomerId($customerId, $fromDate = null, $toDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNote')
                ->columns(array('*'))
                ->join('salesInvoice', 'dispatchNote.invoiceId = salesInvoice.salesInvoiceID', array('customerID'))
                ->join("customer", "salesInvoice.customerID = customer.customerID", "customerName")
                ->join('entity', 'dispatchNote.entityId = entity.entityID', array('deleted'))
                ->order(array('dispatchNote.dispatchNoteCode DESC'))
        ->where->equalTo('customer.customerID', $customerId);
        if ($fromDate && $toDate) {
            $select->where->between('dispatchNote.dispatchNoteDate', $fromDate, $toDate);
        }
        $select->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get dispatch note by dispatch note id
     * @param int $dispatchNoteId
     * @param string $fromDate
     * @param string $toDate
     * @return mixed
     */
    public function getDispatchNoteByDispatchNoteId($dispatchNoteId, $fromDate = null, $toDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNote')
                ->columns(array('*'))
                ->join('salesInvoice', 'dispatchNote.invoiceId = salesInvoice.salesInvoiceID', array('customerID'))
                ->join("customer", "salesInvoice.customerID = customer.customerID", "customerName")
                ->join('entity', 'dispatchNote.entityId = entity.entityID', array('deleted'))
                ->order(array('dispatchNote.dispatchNoteCode DESC'))
        ->where->equalTo('dispatchNote.dispatchNoteId', $dispatchNoteId);
        if ($fromDate && $toDate) {
            $select->where->between('dispatchNote.dispatchNoteDate', $fromDate, $toDate);
        }
        $select->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get dispatch note by dispatch note id
     * @param int $dispatchNoteId
     * @return mixed
     */
    public function getDispatchNoteDetailsByDispatchNoteId($dispatchNoteId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNote')
                ->columns(array('*'))
                ->join('salesInvoice', 'dispatchNote.invoiceId = salesInvoice.salesInvoiceID', array('customerID', 'salesInvoiceCode'))
                ->join("customer", "salesInvoice.customerID = customer.customerID", "customerName")
                ->join('entity', 'dispatchNote.entityId = entity.entityID', array('deleted','createdBy','createdTimeStamp'),'left')
                ->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left')
        ->where->equalTo('dispatchNote.dispatchNoteId', $dispatchNoteId);
        $select->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() > 0) {
            return $result->current();
        }
        return NULL;
    }

    public function getDispatchNoteByInvoiceId($invoiceId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNote')
                ->columns(array('*'))
                ->join('entity', 'entity.entityID = dispatchNote.entityId', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('dispatchNote.invoiceId', $invoiceId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if ($result->count() > 0) {
            return $result;
        }

        return NULL;
    }

    /**
     * For search dispatch note
     * @param int $dispatchNoteId
     * @param int $customerId
     * @param date $fromDate
     * @param date $toDate
     * @return mixed
     */
    public function searchDispatchNote( $dispatchNoteId, $customerId, $fromDate = null, $toDate = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNote')
                ->columns(array('*'))
                ->join('salesInvoice', 'dispatchNote.invoiceId = salesInvoice.salesInvoiceID', array('customerID'))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("customerName","customerStatus"))
                ->join('entity', 'dispatchNote.entityId = entity.entityID', array('deleted'))
                ->order(array('dispatchNote.dispatchNoteCode DESC'));
        if($dispatchNoteId) {
            $select->where->equalTo('dispatchNote.dispatchNoteId', $dispatchNoteId);
        }
        if($customerId) {
            $select->where->equalTo('customer.customerID', $customerId);
        }
        if ($fromDate && $toDate) {
            $select->where->between('dispatchNote.dispatchNoteDate', $fromDate, $toDate);
        }
        $select->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * Get dispatch note by dispatch note code
     * @param string $dispatchNoteCode
     * @return mixed
     */
    public function getDispatchNoteByDispatchNoteCode($dispatchNoteCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNote')
                ->columns(array('*'))
                ->join('entity', 'entity.entityID = dispatchNote.entityId', array('deleted'))
        ->where->equalTo('entity.deleted', 0)
        ->where->equalTo('dispatchNote.dispatchNoteCode', $dispatchNoteCode);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if ($result->count() > 0) {
            return $result;
        }

        return NULL;
    }

}
