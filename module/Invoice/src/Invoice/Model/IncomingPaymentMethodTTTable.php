<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodTT related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Db\Sql\Where;

class IncomingPaymentMethodTTTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveTTPaymentMethod(IncomingPaymentMethodTT $incomingPaymentMethodTT)
    {
        $lCData = array(
            'incomingPaymentMethodTTReference' => $incomingPaymentMethodTT->incomingPaymentMethodTTReference,
        );
        if ($this->tableGateway->insert($lCData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateTTPaymentMethod($data, $ttID)
    {
        if ($ttID) {
            if ($this->tableGateway->update($data, array('incomingPaymentMethodTTId' => $ttID))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}
