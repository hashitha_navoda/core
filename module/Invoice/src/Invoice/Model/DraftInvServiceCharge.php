<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DraftInvServiceCharge
{

    public $draftInvServiceChargeID;
    public $invoiceID;
    public $serviceChargeTypeID;
    public $serviceChargeAmount;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->draftInvServiceChargeID = (!empty($data['draftInvServiceChargeID'])) ? $data['draftInvServiceChargeID'] : null;
        $this->invoiceID = (!empty($data['invoiceID'])) ? $data['invoiceID'] : null;
        $this->serviceChargeTypeID = (!empty($data['serviceChargeTypeID'])) ? $data['serviceChargeTypeID'] : null;
        $this->serviceChargeAmount = (!empty($data['serviceChargeAmount'])) ? $data['serviceChargeAmount'] : null;
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
