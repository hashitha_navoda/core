<?php

/**
 * @author Ashan     <ashan@thinkcube.com>
 * This file contains Credit Note Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoiceReturn implements InputFilterAwareInterface
{

    public $invoiceReturnsID;
    public $invoiceReturnCode;
    public $invoiceReturnTotal;
    public $locationID;
    // public $paymentTermID;
    public $invoiceID;
    public $invoiceReturnComment;
    public $invoiceReturnDate;
    public $customerID;
    public $statusID;
    public $creditNotePaymentEligible;
    public $creditNotePaymentAmount;
    public $creditNoteSettledAmount;
    public $customCurrencyId;
    public $invoiceReturnCustomCurrencyRate;
    public $invoiceReturnDiscountType;
    public $invoiceReturnTotalDiscount;
    public $promotionID;
    public $invoiceReturnPromotionDiscount;
    public $creditNoteDirectFlag;
    public $creditNoteBuyBackFlag;
    public $entityID;
    public $isApproved;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->invoiceReturnsID = (!empty($data['invoiceReturnsID'])) ? $data['invoiceReturnsID'] : null;
        $this->invoiceReturnCode = (!empty($data['invoiceReturnCode'])) ? $data['invoiceReturnCode'] : null;
        $this->invoiceReturnTotal = (!empty($data['invoiceReturnTotal'])) ? $data['invoiceReturnTotal'] : 0.00;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->invoiceID = (!empty($data['invoiceID'])) ? $data['invoiceID'] : 0;
        $this->invoiceReturnComment = (!empty($data['invoiceReturnComment'])) ? $data['invoiceReturnComment'] : null;
        $this->invoiceReturnDate = (!empty($data['invoiceReturnDate'])) ? $data['invoiceReturnDate'] : null;
        $this->customerID = $data['customerID'];
        $this->statusID = (!empty($data['statusID'])) ? $data['statusID'] : null;
        $this->customCurrencyId = (!empty($data['customCurrencyId'])) ? $data['customCurrencyId'] : 0.00;
        $this->invoiceReturnCustomCurrencyRate = (!empty($data['invoiceReturnCustomCurrencyRate'])) ? $data['invoiceReturnCustomCurrencyRate'] : 0.00;
        $this->invoiceReturnDiscountType = (!empty($data['invoiceReturnDiscountType'])) ? $data['invoiceReturnDiscountType'] : null;
        $this->invoiceReturnTotalDiscount = (!empty($data['invoiceReturnTotalDiscount'])) ? $data['invoiceReturnTotalDiscount'] : null;
        $this->promotionID = (!empty($data['promotionID'])) ? $data['promotionID'] : null;
        $this->invoiceReturnPromotionDiscount = (!empty($data['invoiceReturnPromotionDiscount'])) ? $data['invoiceReturnPromotionDiscount'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->isApproved = (!empty($data['isApproved'])) ? $data['isApproved'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
