<?php

namespace Invoice\Model;

/**
 * Class DispatchNoteSubProduct
 * @package Invoice\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class DispatchNoteProduct
{
    /**
     * @var int Auto incremented dispatch note product Id. (Primary key)
     */
    public $dispatchNoteProductId;
    
    /**
     * @var int dispatch note id
     */
    public $dispatchNoteId;
    
    /**
     * @var int sales invoice product id
     */
    public $salesInvoiceProductId;
    
    /**
     * @var decimal dispatch note product quantity
     */
    public $dispatchNoteProductQuantity;

    public function exchangeArray($data)
    {
        $this->dispatchNoteProductId       = (!empty($data['dispatchNoteProductId'])) ? $data['dispatchNoteProductId'] : null;
        $this->dispatchNoteId              = (!empty($data['dispatchNoteId'])) ? $data['dispatchNoteId'] : null;
        $this->salesInvoiceProductId       = (!empty($data['salesInvoiceProductId'])) ? $data['salesInvoiceProductId'] : null;
        $this->dispatchNoteProductQuantity = (!empty($data['dispatchNoteProductQuantity'])) ? $data['dispatchNoteProductQuantity'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
