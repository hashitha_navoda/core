<?php

/**
 * @author Ashan <ashan@thinkcube.com>
 * This file contains returns sub product model */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ReturnsSubProduct implements InputFilterAwareInterface
{

    public $salesReturnSubProductID;
    public $salesReturnProductID;
    public $productBatchID;
    public $productSerialID;
    public $salesReturnSubProductQuantity;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->salesReturnSubProductID = (!empty($data['salesReturnSubProductID'])) ? $data['salesReturnSubProductID'] : null;
        $this->salesReturnProductID = (!empty($data['salesReturnProductID'])) ? $data['salesReturnProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->salesReturnSubProductQuantity = (!empty($data['salesReturnSubProductQuantity'])) ? $data['salesReturnSubProductQuantity'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
