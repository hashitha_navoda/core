<?php

/**
 * @author Ashan <ashan@thinkcube.com>
 * This file contains return product model */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ReturnsProduct implements InputFilterAwareInterface
{

    public $salesReturnProductID;
    public $salesReturnID;
    public $deliveryNoteProductID;
    public $productID;
    public $salesReturnProductPrice;
    public $salesReturnProductDiscount;
    public $salesReturnProductDiscountType;
    public $salesReturnProductQuantity;
    public $salesReturnProductTotal;
    public $rackID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->salesReturnProductID = (!empty($data['salesReturnProductID'])) ? $data['salesReturnProductID'] : null;
        $this->salesReturnID = (!empty($data['salesReturnID'])) ? $data['salesReturnID'] : null;
        $this->deliveryNoteProductID = (!empty($data['deliveryNoteProductID'])) ? $data['deliveryNoteProductID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->salesReturnProductPrice = (!empty($data['salesReturnProductPrice'])) ? $data['salesReturnProductPrice'] : 0.00;
        $this->salesReturnProductDiscount = (!empty($data['salesReturnProductDiscount'])) ? $data['salesReturnProductDiscount'] : 0.00;
        $this->salesReturnProductDiscountType = (!empty($data['salesReturnProductDiscountType'])) ? $data['salesReturnProductDiscountType'] : null;
        $this->salesReturnProductQuantity = (!empty($data['salesReturnProductQuantity'])) ? $data['salesReturnProductQuantity'] : null;
        $this->salesReturnProductTotal = (!empty($data['salesReturnProductTotal'])) ? $data['salesReturnProductTotal'] : 0.00;
        $this->rackID = (!empty($data['rackID'])) ? $data['rackID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {

    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
