<?php
namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DeliveryNoteEditLogTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDeliveryNoteEditLog(DeliveryNoteEditLog $deliveryNoteEditLog)
    {
        $data = array(
            'userID' => $deliveryNoteEditLog->userID,
            'deliveryNoteID' => $deliveryNoteEditLog->deliveryNoteID,
            'dateAndTime' => $deliveryNoteEditLog->dateAndTime
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

}
