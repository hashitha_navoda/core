<?php

/**
 * @author Sharmilan <sharmilan@thinkcube.com>
 * This file contains Customer Profile related database operations
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class CustomerProfileTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCustomerProfile(CustomerProfile $customerProfile)
    {

        $data = [
            'customerID' => $customerProfile->customerID,
            'customerProfileName' => $customerProfile->customerProfileName,
            'customerProfileMobileTP1' => $customerProfile->customerProfileMobileTP1,
            'customerProfileMobileTP2' => $customerProfile->customerProfileMobileTP2,
            'customerProfileLandTP1' => $customerProfile->customerProfileLandTP1,
            'customerProfileLandTP2' => $customerProfile->customerProfileLandTP2,
            'customerProfileFaxNo' => $customerProfile->customerProfileFaxNo,
            'customerProfileEmail' => $customerProfile->customerProfileEmail,
            'customerProfileContactPersonName' => $customerProfile->customerProfileContactPersonName,
            'customerProfileContactPersonNumber' => $customerProfile->customerProfileContactPersonNumber,
            'customerProfileLocationNo' => $customerProfile->customerProfileLocationNo,
            'customerProfileLocationRoadName1' => $customerProfile->customerProfileLocationRoadName1,
            'customerProfileLocationRoadName2' => $customerProfile->customerProfileLocationRoadName2,
            'customerProfileLocationRoadName3' => $customerProfile->customerProfileLocationRoadName3,
            'customerProfileLocationSubTown' => $customerProfile->customerProfileLocationSubTown,
            'customerProfileLocationTown' => $customerProfile->customerProfileLocationTown,
            'customerProfileLocationPostalCode' => $customerProfile->customerProfileLocationPostalCode,
            'customerProfileLocationCountry' => $customerProfile->customerProfileLocationCountry,
            'isPrimary' => $customerProfile->isPrimary,
        ];

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateCustomerProfile(CustomerProfile $customerProfile)
    {
        try {
            $data = [
//                'customerID' => $customerProfile->customerID,
                'customerProfileName' => $customerProfile->customerProfileName,
                'customerProfileMobileTP1' => $customerProfile->customerProfileMobileTP1,
                'customerProfileMobileTP2' => $customerProfile->customerProfileMobileTP2,
                'customerProfileLandTP1' => $customerProfile->customerProfileLandTP1,
                'customerProfileLandTP2' => $customerProfile->customerProfileLandTP2,
                'customerProfileFaxNo' => $customerProfile->customerProfileFaxNo,
                'customerProfileEmail' => $customerProfile->customerProfileEmail,
                'customerProfileContactPersonName' => $customerProfile->customerProfileContactPersonName,
                'customerProfileContactPersonNumber' => $customerProfile->customerProfileContactPersonNumber,
                'customerProfileLocationNo' => $customerProfile->customerProfileLocationNo,
                'customerProfileLocationRoadName1' => $customerProfile->customerProfileLocationRoadName1,
                'customerProfileLocationRoadName2' => $customerProfile->customerProfileLocationRoadName2,
                'customerProfileLocationRoadName3' => $customerProfile->customerProfileLocationRoadName3,
                'customerProfileLocationSubTown' => $customerProfile->customerProfileLocationSubTown,
                'customerProfileLocationTown' => $customerProfile->customerProfileLocationTown,
                'customerProfileLocationPostalCode' => $customerProfile->customerProfileLocationPostalCode,
                'customerProfileLocationCountry' => $customerProfile->customerProfileLocationCountry,
                'isPrimary' => $customerProfile->isPrimary,
            ];

            $this->tableGateway->update($data, array('customerProfileID' => $customerProfile->customerProfileID));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

//this function use to update customer default profile when import customer
    public function updateCustomerProfileByCustomerID(CustomerProfile $customerProfile)
    {
        try {
            $data = [
                'customerProfileName' => $customerProfile->customerProfileName,
                'customerProfileMobileTP1' => $customerProfile->customerProfileMobileTP1,
                'customerProfileMobileTP2' => $customerProfile->customerProfileMobileTP2,
                'customerProfileLandTP1' => $customerProfile->customerProfileLandTP1,
                'customerProfileLandTP2' => $customerProfile->customerProfileLandTP2,
                'customerProfileFaxNo' => $customerProfile->customerProfileFaxNo,
                'customerProfileEmail' => $customerProfile->customerProfileEmail,
                'customerProfileContactPersonName' => $customerProfile->customerProfileContactPersonName,
                'customerProfileContactPersonNumber' => $customerProfile->customerProfileContactPersonNumber,
                'customerProfileLocationNo' => $customerProfile->customerProfileLocationNo,
                'customerProfileLocationRoadName1' => $customerProfile->customerProfileLocationRoadName1,
                'customerProfileLocationRoadName2' => $customerProfile->customerProfileLocationRoadName2,
                'customerProfileLocationRoadName3' => $customerProfile->customerProfileLocationRoadName3,
                'customerProfileLocationSubTown' => $customerProfile->customerProfileLocationSubTown,
                'customerProfileLocationTown' => $customerProfile->customerProfileLocationTown,
                'customerProfileLocationPostalCode' => $customerProfile->customerProfileLocationPostalCode,
                'customerProfileLocationCountry' => $customerProfile->customerProfileLocationCountry,
                'isPrimary' => $customerProfile->isPrimary,
            ];
            $this->tableGateway->update($data, array('customerID' => $customerProfile->customerID, 'isPrimary' => '1'));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function updateCustomerPrimaryEmail($customerEmail, $customerID)
    {
        try {
            $data = array(
                'customerProfileEmail' => $customerEmail,
            );
            $this->tableGateway->update($data, array('customerID' => $customerID, 'isPrimary' => '1'));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function updateCustomerPrimaryAddress($cProfileData, $customerID)
    {
        try {
            $data = array(
                'customerProfileLocationNo' => $cProfileData['cMEA_locationNo'],
                'customerProfileLocationRoadName1' => $cProfileData['cMEA_roadName1'],
                'customerProfileLocationRoadName2' => $cProfileData['cMEA_roadName2'],
                'customerProfileLocationRoadName3' => $cProfileData['cMEA_roadName3'],
                'customerProfileLocationSubTown' => $cProfileData['cMEA_subTown'],
                'customerProfileLocationTown' => $cProfileData['cMEA_town'],
                'customerProfileLocationPostalCode' => $cProfileData['cMEA_postalCode'],
                'customerProfileLocationCountry' => $cProfileData['cMEA_country'],
            );

            $this->tableGateway->update($data, array('customerID' => $customerID, 'isPrimary' => '1'));
            return true;
        } catch (Exception $exc) {
            return false;
        }
    }

    public function getProfileByCustomerID($customerID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerProfile')
                ->columns(array('*'))
                ->join('customer', 'customerProfile.customerID = customer.customerID', array('*'), 'left')
                ->where(array('customerProfile.customerID' => $customerID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getProfileByCustomerProfileID($profileID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerProfile')
                ->columns(array('*'))
                ->join('customer', 'customerProfile.customerID=customer.customerID', array('customerTelephoneNumber','customerStatus'), 'left')
                ->where(array('customerProfileID' => $profileID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function deleteCustomerProfile($customerProfileID)
    {
        return $this->tableGateway->delete(array('customerProfileID' => $customerProfileID));
    }

    /**
     * Get customer profile names for drop down
     * @param string $profileName
     * @return mixed
     */
    public function searchCustomerProfileForDropDown($profileName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(['customerID', 'entityID'])
                ->join('entity', 'customer.entityID = entity.entityID', ['deleted'], 'left')
                ->join('customerProfile', 'customer.customerID = customerProfile.customerID', ['customerProfileName'], 'left')
                ->where->equalTo('entity.deleted', 0)
                ->where->like('customerProfile.customerProfileName', '%' . $profileName . '%');
                $select->group('customerProfile.customerProfileName');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

     /**
     * @author Ashan <ashan@thinkcube.com>
     * @param type $customerSearchKey
     * @return type
     */
    public function searchCustomersForDropDown($customerSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerProfile')
                ->columns(array('customerProfileName'))
                ->join('customer', 'customerProfile.customerID = customer.customerID', array('*'), 'left')
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0,'customer.customerStatus' => '1'))
                ->where(new PredicateSet(array(new Operator('customer.customerName', 'like', '%' . $customerSearchKey . '%'), new Operator('customer.customerCode', 'like', '%' . $customerSearchKey . '%') , new Operator('customer.customerTelephoneNumber', 'like', '%' . $customerSearchKey . '%'), new Operator('customerProfileName', 'like', '%' . $customerSearchKey . '%')), PredicateSet::OP_OR))
        ->where->notEqualTo('customer.customerID', '0');
        $select->order(array('customer.customerName' => 'AESC'));
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function getDefaultProfileByCustomerID($customerID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerProfile')
                ->columns(array('*'))
                ->where(array('customerProfile.customerID' => $customerID, 'customerProfile.isPrimary' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updatePrimaryStateByCustomerProfileID($id, $state)
    {
        $data = array(
            'isPrimary' => $state,
            );
        $this->tableGateway->update($data, array('customerProfileID' => $id));
        return TRUE;
    }
}
