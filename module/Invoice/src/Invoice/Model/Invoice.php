<?php

/**
 * @author damith <damith@thinkcube.com>
 * This file contains Invoice Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Invoice implements InputFilterAwareInterface
{

    public $salesInvoiceID;
    public $salesInvoiceCode;
    public $customerID;
    public $locationID;
    public $salesInvoiceIssuedDate;
    public $salesInvoiceOverDueDate;
    public $salesInvoiceTaxAmount;
    public $salesInvoiceTotalDiscount;
    public $salesinvoiceTotalAmount;
    public $templateInvoiceTotal;
    public $salesInvoicePayedAmount;
    public $salesInvoiceComment;
    public $salesInvoiceDeliveryCharge;
    public $paymentTermID;
    public $statusID;
    public $quotationID;
    public $deliveryNoteID;
    public $salesOrderID;
    public $activityID;
    public $jobID;
    public $projectID;
    public $salesInvoiceShowTax;
    public $entityID;
    public $pos;
    public $salesPersonID;
    public $salesInvoiceSuspendedTax;
    public $salesInvoiceDeliveryAddress;
    public $promotionID;
    public $salesInvoicePromotionDiscount;
    public $salesInvoiceWiseTotalDiscount;
    public $salesInvoiceDiscountRate;
    public $salesInvoiceTotalDiscountType;
    public $customCurrencyId;
    public $salesInvoiceCustomCurrencyRate;
    public $priceListId;
    public $salesInvoiceDeliveryChargeEnable;
    public $customerProfileID;
    public $salesInvoiceTemporaryCode;
    public $offlineSavedTime;
    public $offlineUser;
    public $salesInvoiceRemainingDiscValue;
    public $salesInvoiceRemainingPromotionDiscValue;
    public $discountOnlyOnEligible;
    public $customerCurrentOutstanding;
    public $inclusiveTax;
    public $salesInvoiceServiceChargeAmount;
    public $linkedCusOrderNum;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->salesInvoiceID = (!empty($data['salesInvoiceID'])) ? $data['salesInvoiceID'] : null;
        $this->salesInvoiceCode = (!empty($data['salesInvoiceCode'])) ? $data['salesInvoiceCode'] : null;
        $this->customerID = $data['customerID'];
        $this->locationID = (!empty($data['locationID'])) ? (int) $data['locationID'] : null;
        $this->salesInvoiceIssuedDate = (!empty($data['salesInvoiceIssuedDate'])) ? $data['salesInvoiceIssuedDate'] : null;
        $this->salesInvoiceOverDueDate = (!empty($data['salesInvoiceOverDueDate'])) ? $data['salesInvoiceOverDueDate'] : null;
        $this->salesInvoiceTaxAmount = (!empty($data['salesInvoiceTaxAmount'])) ? $data['salesInvoiceTaxAmount'] : 0.00;
        $this->salesInvoiceTotalDiscount = (!empty($data['salesInvoiceTotalDiscount'])) ? $data['salesInvoiceTotalDiscount'] : 0.00;
        $this->salesinvoiceTotalAmount = (!empty($data['salesinvoiceTotalAmount'])) ? (float) $data['salesinvoiceTotalAmount'] : 0.00;
        $this->templateInvoiceTotal = (!empty($data['templateInvoiceTotal'])) ? (float) $data['templateInvoiceTotal'] : 0.00;
        $this->salesInvoicePayedAmount = (!empty($data['salesInvoicePayedAmount'])) ? $data['salesInvoicePayedAmount'] : 0.00;
        $this->salesInvoiceComment = (!empty($data['salesInvoiceComment'])) ? $data['salesInvoiceComment'] : null;
        $this->salesInvoiceDeliveryCharge = (!empty($data['salesInvoiceDeliveryCharge'])) ? $data['salesInvoiceDeliveryCharge'] : 0.00;
        $this->paymentTermID = (!empty($data['paymentTermID'])) ? (int) $data['paymentTermID'] : null;
        $this->statusID = (!empty($data['statusID'])) ? (int) $data['statusID'] : null;
        $this->salesInvoiceShowTax = (!empty($data['salesInvoiceShowTax'])) ? (int) $data['salesInvoiceShowTax'] : 0;
        $this->quotationID = (!empty($data['quotationID'])) ? (int) $data['quotationID'] : null;
        $this->deliveryNoteID = (!empty($data['deliveryNoteID'])) ? (int) $data['deliveryNoteID'] : null;
        $this->salesOrderID = (!empty($data['salesOrderID'])) ? (int) $data['salesOrderID'] : null;
        $this->activityID = (!empty($data['activityID'])) ? (int) $data['activityID'] : null;
        $this->jobID = (!empty($data['jobID'])) ? (int) $data['jobID'] : null;
        $this->projectID = (!empty($data['projectID'])) ? (int) $data['projectID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? (int) $data['entityID'] : null;
        $this->pos = (!empty($data['pos'])) ? $data['pos'] : 0;
        $this->salesPersonID = (!empty($data['salesPersonID'])) ? $data['salesPersonID'] : null;
        $this->salesInvoiceSuspendedTax = (!empty($data['salesInvoiceSuspendedTax'])) ? $data['salesInvoiceSuspendedTax'] : 0;
        $this->salesInvoiceDeliveryAddress = (!empty($data['salesInvoiceDeliveryAddress'])) ? $data['salesInvoiceDeliveryAddress'] : null;
        $this->promotionID = (!empty($data['promotionID'])) ? $data['promotionID'] : null;
        $this->salesInvoicePromotionDiscount = (!empty($data['salesInvoicePromotionDiscount'])) ? $data['salesInvoicePromotionDiscount'] : null;
        $this->salesInvoiceWiseTotalDiscount = (!empty($data['salesInvoiceWiseTotalDiscount'])) ? $data['salesInvoiceWiseTotalDiscount'] : null;
        $this->salesInvoiceTotalDiscountType = (!empty($data['salesInvoiceTotalDiscountType'])) ? $data['salesInvoiceTotalDiscountType'] : 'presentage';
        $this->salesInvoiceDiscountRate = (!empty($data['salesInvoiceDiscountRate'])) ? $data['salesInvoiceDiscountRate'] : 0;
        $this->salesInvoiceRemainingDiscValue = (!empty($data['salesInvoiceRemainingDiscValue'])) ? $data['salesInvoiceRemainingDiscValue'] : null;
        $this->salesInvoiceRemainingPromotionDiscValue = (!empty($data['salesInvoiceRemainingPromotionDiscValue'])) ? $data['salesInvoiceRemainingPromotionDiscValue'] : null;
        $this->customCurrencyId = (!empty($data['customCurrencyId'])) ? $data['customCurrencyId'] : null;
        $this->salesInvoiceCustomCurrencyRate = (!empty($data['salesInvoiceCustomCurrencyRate'])) ? $data['salesInvoiceCustomCurrencyRate'] : 0.00;
        $this->priceListId = (!empty($data['priceListId'])) ? $data['priceListId'] : 0;
        $this->salesInvoiceDeliveryChargeEnable = (!empty($data['salesInvoiceDeliveryChargeEnable'])) ? $data['salesInvoiceDeliveryChargeEnable'] : 0;
        $this->customerProfileID = (!empty($data['customerProfileID'])) ? $data['customerProfileID'] : null;
        $this->salesInvoiceTemporaryCode = (!empty($data['salesInvoiceTemporaryCode'])) ? $data['salesInvoiceTemporaryCode'] : null;
        $this->offlineSavedTime = (!empty($data['offlineSavedTime'])) ? $data['offlineSavedTime'] : null;
        $this->offlineUser = (!empty($data['offlineUser'])) ? $data['offlineUser'] : null;
        $this->discountOnlyOnEligible = (!empty($data['discountOnlyOnEligible'])) ? $data['discountOnlyOnEligible'] : 0;
        $this->customerCurrentOutstanding = (!empty($data['customerCurrentOutstanding'])) ? $data['customerCurrentOutstanding'] : null;
        $this->inclusiveTax = (!empty($data['inclusiveTax'])) ? $data['inclusiveTax'] : 0;
        $this->salesInvoiceServiceChargeAmount = (!empty($data['salesInvoiceServiceChargeAmount'])) ? $data['salesInvoiceServiceChargeAmount'] : 0;
        $this->linkedCusOrderNum = (!empty($data['linkedCusOrderNum'])) ? $data['linkedCusOrderNum'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
