<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Rating Types related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class RatingTypesTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //get all rating types
    public function fetchAll($paginated = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('ratingTypes')
                ->columns(array('*'))
                ->join('entity', 'ratingTypes.entityID= entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
                ->order('ratingTypesName ASC');

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //save function of rating types
    public function saveRatingTypes(RatingTypes $ratingTypes)
    {
        $data = array(
            'ratingTypesCode' => $ratingTypes->ratingTypesCode,
            'ratingTypesName' => $ratingTypes->ratingTypesName,
            'ratingTypesDescription' => $ratingTypes->ratingTypesDescription,
            'ratingTypesMethod' => $ratingTypes->ratingTypesMethod,
            'ratingTypesSize' => $ratingTypes->ratingTypesSize,
            'ratingTypesMethodDefinition' => $ratingTypes->ratingTypesMethodDefinition,
            'ratingTypesState' => $ratingTypes->ratingTypesState,
            'entityID' => $ratingTypes->entityID,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //update function of rating types
    public function updateRatingTypes($data, $ratingTypesId)
    {
        try {
            $this->tableGateway->update($data, array('ratingTypesId' => $ratingTypesId));
            return true;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

    //get un deleted rating types by rating type code
    public function getRatingTypesByRatingTypesCode($ratingTypesCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('ratingTypes')
                ->join('entity', 'ratingTypes.entityID= entity.entityID', array('deleted'), 'left')
                ->where(array('ratingTypes.ratingTypesCode' => $ratingTypesCode, 'entity.deleted' => 0));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    }

    //get rating types by ratingTypesId
    public function getRatingTypesByRatingTypesId($ratingTypesId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('ratingTypes')
                ->where(array('ratingTypes.ratingTypesId' => $ratingTypesId));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    }

    //get rating types by rating types search key
    public function searchRatingTypesBySearchKey($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('ratingTypes');
        $select->columns(array('*'));
        $select->join('entity', 'ratingTypes.entityID= entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0));
        $select->where(new PredicateSet(array(
            new Operator('ratingTypesCode', 'like', '%' . $searchKey . '%'),
            new Operator('ratingTypesName', 'like', '%' . $searchKey . '%'),
                ), PredicateSet::OP_OR));
        $select->order('ratingTypesName ASC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $value) {
            $objectResults[] = (object) $value;
        }
        if (isset($objectResults)) {
            return (object) $objectResults;
        } else {
            return null;
        }
    }

    //get active rating types
    function getActiveRatingTypes()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('ratingTypes')
                ->join('entity', 'ratingTypes.entityID= entity.entityID', array('deleted'), 'left')
                ->where(array('ratingTypes.ratingTypesState' => 1, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
