<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains credit note product tax Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class CreditNoteInvoiceReturnTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCreditNoteInvoiceReturn(CreditNoteInvoiceReturn $creditNoteInvoiceReturn)
    {
        $data = array(
            'creditNoteID' => $creditNoteInvoiceReturn->creditNoteID,
            'invoiceReturnID' => $creditNoteInvoiceReturn->invoiceReturnID,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }
}
