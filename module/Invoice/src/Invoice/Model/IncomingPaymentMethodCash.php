<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethodCash
{

    public $incomingPaymentMethodCashId;
    public $cashAccountID;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodCashId = (!empty($data['incomingPaymentMethodCashId'])) ? $data['incomingPaymentMethodCashId'] : null;
        $this->cashAccountID = (!empty($data['cashAccountID'])) ? $data['cashAccountID'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
