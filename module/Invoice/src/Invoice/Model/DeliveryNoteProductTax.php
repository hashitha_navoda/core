<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DeliveryNoteProductTax
{

    public $deliveryNoteProductTaxID;
    public $deliveryNoteProductID;
    public $productID;
    public $deliveryNoteTaxID;
    public $deliveryNoteTaxPrecentage;
    public $deliveryNoteTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->deliveryNoteProductTaxID = (!empty($data['deliveryNoteProductTaxID'])) ? $data['deliveryNoteProductTaxID'] : null;
        $this->deliveryNoteProductID = (!empty($data['deliveryNoteProductID'])) ? $data['deliveryNoteProductID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->deliveryNoteTaxID = (!empty($data['deliveryNoteTaxID'])) ? $data['deliveryNoteTaxID'] : null;
        $this->deliveryNoteTaxPrecentage = (!empty($data['deliveryNoteTaxPrecentage'])) ? $data['deliveryNoteTaxPrecentage'] : null;
        $this->deliveryNoteTaxAmount = (!empty($data['deliveryNoteTaxAmount'])) ? $data['deliveryNoteTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
