<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodCheque related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Expression;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\Db\Sql\Where;

class IncomingPaymentMethodChequeTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveChequePayment(IncomingPaymentMethodCheque $incomingPaymentMethodCheque)
    {
        $cashData = array(
            'incomingPaymentMethodChequeNumber' => $incomingPaymentMethodCheque->incomingPaymentMethodChequeNumber,
            'incomingPaymentMethodChequeBankName' => $incomingPaymentMethodCheque->incomingPaymentMethodChequeBankName,
            'postdatedChequeStatus' => $incomingPaymentMethodCheque->postdatedChequeStatus,
            'postdatedChequeDate' => $incomingPaymentMethodCheque->postdatedChequeDate,
            'incomingPaymentMethodChequeBouncedStatus' => $incomingPaymentMethodCheque->incomingPaymentMethodChequeBouncedStatus,
            'incomingPaymentMethodChequeAccNo' => $incomingPaymentMethodCheque->incomingPaymentMethodChequeAccNo
        );
        if ($this->tableGateway->insert($cashData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @author Madawa Chandrarathne <madawa@thinkcube.com>
     *
     * Get all the customers cheques within the given date range.
     * @param Srting $customerId customer id
     * @param String $paymentId payment id
     * @param String $startDate start date
     * @param String $endDate end date
     *
     * @return Result
     */
    public function getCustomerCheques($startDate = NULL, $endDate = NULL, $sortBy = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodCheque')
                ->columns(array('*'))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = incomingPaymentMethod.incomingPaymentMethodChequeId', array('*'))
                ->join('incomingPayment', 'incomingPaymentMethod.incomingPaymentId = incomingPayment.incomingPaymentID', array('*'))
                ->join('customer', 'incomingPayment.customerID = customer.customerID ', array('customerID', 'customerName'))
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID ', array('*'), 'left')
                ->join('entity', 'incomingPayment.entityID = entity.entityID ', array('deleted'))
        ->where->equalTo('entity.deleted', 0);
        if ($startDate && $endDate) {
            $select->where->between('incomingPayment.incomingPaymentDate', $startDate, $endDate);
        }
        if ($sortBy == 'Payment No') {
            $select->order('incomingPayment.incomingPaymentID DESC');
        } else if ($sortBy == 'Date') {
            $select->order('incomingPayment.incomingPaymentDate DESC');
        } else if ($sortBy == 'Cheque Date') {
            $select->order('incomingPaymentMethodCheque.postdatedChequeDate DESC');
        } else if ($sortBy == NULL) {
            $select->order('incomingPayment.incomingPaymentID DESC');
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     *
     * Get all the customers cheques by customerID.
     * @param Srting $customerId customer id
     *
     * @return Result
     */
    public function getCustomerChequesByCustomerId($customerId = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodCheque')
                ->columns(array('*'))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = incomingPaymentMethod.incomingPaymentMethodChequeId', array('*'))
                ->join('incomingPayment', 'incomingPaymentMethod.incomingPaymentId = incomingPayment.incomingPaymentID', array('*'))
                ->join('customer', 'incomingPayment.customerID = customer.customerID ', array('customerID', 'customerName'))
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID ', array('*'), 'left')
                ->join('entity', 'incomingPayment.entityID = entity.entityID ', array('deleted'))
        ->where->equalTo('entity.deleted', 0);
        if ($customerId) {
            $select->where->equalTo('customer.customerID', $customerId);
        }

        $select->order('incomingPayment.incomingPaymentID DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     *
     * Get all the customers cheques by customerID.
     * @param Srting $paymentId customer id
     *
     * @return Result
     */
    public function getCustomerChequesByPaymentId($paymentId = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodCheque')
                ->columns(array('*'))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = incomingPaymentMethod.incomingPaymentMethodChequeId', array('*'))
                ->join('incomingPayment', 'incomingPaymentMethod.incomingPaymentId = incomingPayment.incomingPaymentID', array('*'))
                ->join('customer', 'incomingPayment.customerID = customer.customerID ', array('customerID', 'customerName'))
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID ', array('*'), 'left')
                ->join('entity', 'incomingPayment.entityID = entity.entityID ', array('deleted'))
        ->where->equalTo('entity.deleted', 0);
        if ($paymentId) {
            $select->where->equalTo('incomingPayment.incomingPaymentID', $paymentId);
        }

        $select->order('incomingPayment.incomingPaymentID DESC');
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /**
     * @author Madawa Chandrarathne <madawa@thinkcube.com>
     *
     * Search customer cheque payments
     * @param String $searchKey
     * @return Result
     */
    public function searchCustomerChequePayments($searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodCheque')
                ->columns(array("*"))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = incomingPaymentMethod.incomingPaymentMethodChequeId', array('*'))
                ->join('incomingPayment', 'incomingPaymentMethod.incomingPaymentId = incomingPayment.incomingPaymentID', array('incomingPaymentCode', 'incomingPaymentID'))
        ->where->like('incomingPaymentCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Madawa Chandrarathne <madawa@thinkcube.com>
     *
     * Get all the customer cheques
     * @param boolean $paginated
     * @return Mixed
     */
    public function getUndepositedCheques($paginated = FALSE, $locationID = null)
    {

        if ($locationID != null) {
            $sql = 'select * from incomingPaymentMethodCheque ipmc '
                . 'JOIN incomingPaymentMethod ipm ON ipmc.incomingPaymentMethodChequeId = ipm.incomingPaymentMethodChequeId '
                . 'JOIN incomingPayment ip ON ip.incomingPaymentId = ipm.incomingPaymentId '
                . 'JOIN customer c ON ip.customerID = c.customerID '
                . 'JOIN currency cu ON ip.customCurrencyId = cu.currencyID '
                . 'JOIN entity e ON e.entityId = ip.entityId '
                . 'WHERE e.deleted = 0 AND ipmc.incomingPaymentMethodChequeBouncedStatus = 0 AND ip.locationID = '.$locationID.' AND  ipmc.incomingPaymentMethodChequeId NOT IN (select incomingPaymentMethodChequeId from chequeDepositCheque cdc JOIN entity e on e.entityId = cdc.entityId WHERE e.deleted = false)'
                . 'ORDER BY ip.incomingPaymentId DESC';
        } else {
            $sql = 'select * from incomingPaymentMethodCheque ipmc '
                . 'JOIN incomingPaymentMethod ipm ON ipmc.incomingPaymentMethodChequeId = ipm.incomingPaymentMethodChequeId '
                . 'JOIN incomingPayment ip ON ip.incomingPaymentId = ipm.incomingPaymentId '
                . 'JOIN customer c ON ip.customerID = c.customerID '
                . 'JOIN currency cu ON ip.customCurrencyId = cu.currencyID '
                . 'JOIN entity e ON e.entityId = ip.entityId '
                . 'WHERE e.deleted = 0 AND ipmc.incomingPaymentMethodChequeBouncedStatus = 0 AND ipmc.incomingPaymentMethodChequeId NOT IN (select incomingPaymentMethodChequeId from chequeDepositCheque cdc JOIN entity e on e.entityId = cdc.entityId WHERE e.deleted = false)'
                . 'ORDER BY ip.incomingPaymentId DESC';
        }

        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);
        if ($paginated) {
            $items = new ArrayAdapter(iterator_to_array($resultSet));
            $paginator = new Paginator($items);
            return $paginator;
        } else {
            return $resultSet;
        }
    }

    
    public function getUndepositedChequesByCustomer($customerID,$locationID = null)
    {

        $sql = 'select * from incomingPaymentMethodCheque ipmc '
                . 'JOIN incomingPaymentMethod ipm ON ipmc.incomingPaymentMethodChequeId = ipm.incomingPaymentMethodChequeId '
                . 'JOIN incomingPayment ip ON ip.incomingPaymentId = ipm.incomingPaymentId '
                . 'JOIN customer c ON ip.customerID = c.customerID '
                . 'JOIN currency cu ON ip.customCurrencyId = cu.currencyID '
                . 'JOIN entity e ON e.entityId = ip.entityId '
                . 'WHERE e.deleted = 0 AND ipmc.incomingPaymentMethodChequeBouncedStatus = 0 AND ip.locationID = '.$locationID.' AND c.customerID = '.$customerID.' AND  ipmc.incomingPaymentMethodChequeId NOT IN (select incomingPaymentMethodChequeId from chequeDepositCheque cdc JOIN entity e on e.entityId = cdc.entityId WHERE e.deleted = false)'
                . 'ORDER BY ip.incomingPaymentId DESC';

        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);

        return $resultSet;
    } 

    
    public function getUndepositedChequesByPayment($paymentID,$locationID = null)
    {

        $sql = 'select * from incomingPaymentMethodCheque ipmc '
                . 'JOIN incomingPaymentMethod ipm ON ipmc.incomingPaymentMethodChequeId = ipm.incomingPaymentMethodChequeId '
                . 'JOIN incomingPayment ip ON ip.incomingPaymentId = ipm.incomingPaymentId '
                . 'JOIN customer c ON ip.customerID = c.customerID '
                . 'JOIN currency cu ON ip.customCurrencyId = cu.currencyID '
                . 'JOIN entity e ON e.entityId = ip.entityId '
                . 'WHERE e.deleted = 0 AND ipmc.incomingPaymentMethodChequeBouncedStatus = 0 AND ip.locationID = '.$locationID.' AND ip.incomingPaymentId = '.$paymentID.' AND  ipmc.incomingPaymentMethodChequeId NOT IN (select incomingPaymentMethodChequeId from chequeDepositCheque cdc JOIN entity e on e.entityId = cdc.entityId WHERE e.deleted = false)'
                . 'ORDER BY ip.incomingPaymentId DESC';

        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);

        return $resultSet;
    }

    
    public function getUndepositedChequesByInvoice($invoiceID,$locationID = null)
    {

        $sql = 'select * from incomingPaymentMethodCheque ipmc '
                . 'JOIN incomingPaymentMethod ipm ON ipmc.incomingPaymentMethodChequeId = ipm.incomingPaymentMethodChequeId '
                . 'JOIN incomingPayment ip ON ip.incomingPaymentId = ipm.incomingPaymentId '
                . 'JOIN incomingInvoicePayment iip ON iip.incomingPaymentID = ip.incomingPaymentID '
                . 'JOIN customer c ON ip.customerID = c.customerID '
                . 'JOIN currency cu ON ip.customCurrencyId = cu.currencyID '
                . 'JOIN entity e ON e.entityId = ip.entityId '
                . 'WHERE e.deleted = 0 AND ipmc.incomingPaymentMethodChequeBouncedStatus = 0 AND ip.locationID = '.$locationID.' AND iip.salesInvoiceID = '.$invoiceID.' AND  ipmc.incomingPaymentMethodChequeId NOT IN (select incomingPaymentMethodChequeId from chequeDepositCheque cdc JOIN entity e on e.entityId = cdc.entityId WHERE e.deleted = false)'
                . 'ORDER BY ip.incomingPaymentId DESC';

        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);

        return $resultSet;
    }

    
    public function getUndepositedChequesByIncome($incomeID,$locationID = null)
    {

        $sql = 'select * from incomingPaymentMethodCheque ipmc '
                . 'JOIN incomingPaymentMethod ipm ON ipmc.incomingPaymentMethodChequeId = ipm.incomingPaymentMethodChequeId '
                . 'JOIN incomingPayment ip ON ip.incomingPaymentId = ipm.incomingPaymentId '
                . 'JOIN incomingInvoicePayment iip ON iip.incomingPaymentID = ip.incomingPaymentID '
                . 'JOIN customer c ON ip.customerID = c.customerID '
                . 'JOIN currency cu ON ip.customCurrencyId = cu.currencyID '
                . 'JOIN entity e ON e.entityId = ip.entityId '
                . 'WHERE e.deleted = 0 AND ipmc.incomingPaymentMethodChequeBouncedStatus = 0 AND ip.locationID = '.$locationID.' AND iip.incomeID = '.$incomeID.' AND  ipmc.incomingPaymentMethodChequeId NOT IN (select incomingPaymentMethodChequeId from chequeDepositCheque cdc JOIN entity e on e.entityId = cdc.entityId WHERE e.deleted = false)'
                . 'ORDER BY ip.incomingPaymentId DESC';

        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);

        return $resultSet;
    }

    
    public function getUndepositedChequesByRef($refNo,$locationID = null)
    {

        $sql = 'select * from incomingPaymentMethodCheque ipmc '
                . 'JOIN incomingPaymentMethod ipm ON ipmc.incomingPaymentMethodChequeId = ipm.incomingPaymentMethodChequeId '
                . 'JOIN incomingPayment ip ON ip.incomingPaymentId = ipm.incomingPaymentId '
                . 'JOIN incomingInvoicePayment iip ON iip.incomingPaymentID = ip.incomingPaymentID '
                . 'JOIN customer c ON ip.customerID = c.customerID '
                . 'JOIN currency cu ON ip.customCurrencyId = cu.currencyID '
                . 'JOIN entity e ON e.entityId = ip.entityId '
                . 'WHERE e.deleted = 0 AND ipmc.incomingPaymentMethodChequeBouncedStatus = 0 AND ip.locationID = '.$locationID.' AND ipmc.incomingPaymentMethodChequeNumber like "%'.$refNo.'%" AND  ipmc.incomingPaymentMethodChequeId NOT IN (select incomingPaymentMethodChequeId from chequeDepositCheque cdc JOIN entity e on e.entityId = cdc.entityId WHERE e.deleted = false)'
                . 'ORDER BY ip.incomingPaymentId DESC';

        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);

        return $resultSet;
    }

    /**
     * Update IncomingPaymentMethodCheque
     * @param array $data
     * @param string $incomingPaymentMethodCheque
     * @return boolean
     */
    public function updateIncomingPaymentMethodCheque($data, $incomingPaymentMethodCheque)
    {
        if ($incomingPaymentMethodCheque) {
            if ($this->tableGateway->update($data, array('incomingPaymentMethodChequeId' => $incomingPaymentMethodCheque))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * Update Multiple IncomingPaymentMethodCheque
     * @param array $data
     * @param array $incomingPaymentMethodChequeIdList
     * @return boolean
     */
    public function updateMultipleIncomingPaymentMethodCheque($data, $incomingPaymentMethodChequeIdList)
    {
        $where = new Where();
        $where->in('incomingPaymentMethodChequeId', $incomingPaymentMethodChequeIdList);

        if ($incomingPaymentMethodChequeIdList) {
            if ($this->tableGateway->update($data, $where)) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

    /**
     * @author Madawa Chandrarathne <madawa@thinkcube.com>
     *
     * Get all postdated cheques
     * @param string $postdatedDate cheque postdated date
     * @return Mixed
     */
    public function getPostdatedCheques( $postdatedDate, $locationId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodCheque')
                ->columns(array("*"))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = incomingPaymentMethod.incomingPaymentMethodChequeId', array('incomingPaymentId'))
                ->join('incomingPayment', 'incomingPaymentMethod.incomingPaymentId = incomingPayment.incomingPaymentID', array('locationID', 'incomingPaymentAmount'))
        ->where->equalTo('incomingPaymentMethodCheque.postdatedChequeStatus', 1)
        ->where->equalTo('incomingPayment.locationID', $locationId)        
        ->where->equalTo('incomingPaymentMethodCheque.postdatedChequeDate', $postdatedDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    /**
     * get all post dated cheques that only remaining 7 days for expire
     * @author Lahiru Abeysinghe <lahiru@thinkcube.com>
     * @param type $locationID
     * @return type Mixed
     */
    public function getPostDatedChequesForAlertNotification($locationID)
    {
        $sql = "SELECT `incomingPaymentMethodCheque`.`incomingPaymentMethodChequeNumber`,`incomingPayment`.`incomingPaymentAmount`, `customer`.`customerID`,`incomingPayment`.`incomingPaymentCode`,`customer`.`customerName`,`incomingPayment`.`incomingPaymentPaidAmount`,`incomingPaymentMethodCheque`.`incomingPaymentMethodChequeId`,`currency`.`currencySymbol`
FROM `incomingPaymentMethodCheque`
LEFT JOIN `incomingPaymentMethod`
ON `incomingPaymentMethodCheque`.`incomingPaymentMethodChequeId` = `incomingPaymentMethod`.`incomingPaymentMethodChequeId`
LEFT JOIN `incomingPayment`
ON `incomingPaymentMethod`.`incomingPaymentId` = `incomingPayment`.`incomingPaymentID`
LEFT JOIN `customer`
ON `incomingPayment`.`customerID` = `customer`.`customerID`
LEFT JOIN `entity`
ON `incomingPayment`.`entityID` = `entity`.`entityID`
LEFT JOIN `currency`
ON `incomingPayment`.`customCurrencyId`=`currency`.`currencyID`
WHERE `entity`.`deleted` = 0 AND `incomingPaymentMethodCheque`.`postdatedChequeStatus` = 1 AND DATE_ADD(CURDATE(),INTERVAL 7 DAY)=`incomingPaymentMethodCheque`.`postdatedChequeDate` AND `incomingPayment`.`locationID`=$locationID;";
        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);
        return $resultSet;
    }
    
    /**
     * @author Madawa Chandrarathne <madawa@thinkcube.com>
     *
     * Get all the bounced cheques
     * @param boolean $paginated
     * @return Mixed
     */
    public function getBouncedCheques($paginated = FALSE)
    {
        $sql = 'select * from incomingPaymentMethodCheque ipmc '
                . 'JOIN incomingPaymentMethod ipm ON ipmc.incomingPaymentMethodChequeId = ipm.incomingPaymentMethodChequeId '
                . 'JOIN incomingPayment ip ON ip.incomingPaymentId = ipm.incomingPaymentId '
                . 'JOIN customer c ON ip.customerID = c.customerID '
                . 'JOIN currency cu ON ip.customCurrencyId = cu.currencyID '
                . 'JOIN entity e ON e.entityId = ip.entityId '
                . 'WHERE e.deleted = 0 AND ipmc.incomingPaymentMethodChequeBouncedStatus = 1 AND ipmc.incomingPaymentMethodChequeId NOT IN (select incomingPaymentMethodChequeId from chequeDepositCheque cdc JOIN entity e on e.entityId = cdc.entityId WHERE e.deleted = false)'
                . 'ORDER BY ip.incomingPaymentId DESC';

        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);
        if ($paginated) {
            $items = new ArrayAdapter(iterator_to_array($resultSet));
            $paginator = new Paginator($items);
            return $paginator;
        } else {
            return $resultSet;
        }
    }

    /**
     * This function is used to get incoming payment cheque details by cheque id
     */
    public function getChequeDetailsByChequeId($chequeId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodCheque')
                ->columns(array("*"))
                ->join('incomingPaymentMethod', 'incomingPaymentMethodCheque.incomingPaymentMethodChequeId = incomingPaymentMethod.incomingPaymentMethodChequeId', array('incomingPaymentMethodAmount'))
                ->join('incomingPayment', 'incomingPaymentMethod.incomingPaymentId = incomingPayment.incomingPaymentID', array('locationID', 'incomingPaymentAmount'))
        ->where->equalTo('incomingPaymentMethodCheque.incomingPaymentMethodChequeId', $chequeId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    /**
     * @author Sahan Siriwardhan <sahan@thinkcube.com>
     *
     * Get all postdated cheques between date range
     * @param string $fromDate cheque postdated from date
     * @param string $toDate cheque postdated to date
     * @return Mixed
     */
    public function getPostdatedChequesBetweenDateRange( $fromDate, $toDate)
    {
        $sql = "SELECT `incomingPayment`.`incomingPaymentAmount`, `customer`.`customerID`,`customer`.`customerCode`,`customer`.`customerTitle`, `incomingPayment`.`incomingPaymentCode`,`customer`.`customerName`,`customer`.`customerTelephoneNumber`,`incomingPayment`.`incomingPaymentPaidAmount`,`currency`.`currencySymbol`,
                `postdatedChequeDate`,`incomingPaymentMethodChequeNumber` FROM `incomingPaymentMethodCheque`
                LEFT JOIN `incomingPaymentMethod`
                ON `incomingPaymentMethodCheque`.`incomingPaymentMethodChequeId` = `incomingPaymentMethod`.`incomingPaymentMethodChequeId`
                LEFT JOIN `incomingPayment`
                ON `incomingPaymentMethod`.`incomingPaymentId` = `incomingPayment`.`incomingPaymentID`
                LEFT JOIN `customer`
                ON `incomingPayment`.`customerID` = `customer`.`customerID`
                LEFT JOIN `currency`
                ON `incomingPayment`.`customCurrencyId`=`currency`.`currencyID`
                WHERE `incomingPaymentMethodCheque`.`postdatedChequeStatus` = 1 AND `incomingPaymentMethodCheque`.`postdatedChequeDate` BETWEEN  '".$fromDate."' AND '".$toDate."'
                ORDER BY `incomingPaymentMethodCheque`.`postdatedChequeDate` DESC";
        
        $resultSet = $this->tableGateway->getAdapter()->getDriver()->getConnection()->execute($sql);
        return $resultSet;
    }
}
