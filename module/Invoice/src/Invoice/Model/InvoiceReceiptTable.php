<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains invoice table functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;

class InvoiceReceiptTable
{

    protected $tableGateWay;

    function __construct(TableGateway $tableGateWay)
    {
        $this->tableGateWay = $tableGateWay;
    }

    public function saveReceipt(InvoiceReceipt $inv)
    {
        $data = array(
            'productID' => $inv->productID,
            'productName' => $inv->productName,
            'quantity' => $inv->quantity,
            'unitPrice' => $inv->unitPrice,
            'uom' => $inv->uom,
            'discount' => $inv->discount,
            'invoiceID' => $inv->invoiceID,
            'total' => $inv->total,
            'tax' => $inv->tax
        );
        $this->tableGateWay->insert($data);
        return TRUE;
    }

    public function getReceiptByID($id)
    {
        $rowset = $this->tableGateWay->select(array('id' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getProductsByInvoiceID($invoice)
    {
        $rowset = $this->tableGateWay->select(array('invoiceID' => $invoice));
        return $rowset;
    }

    public function getProductByInvoiceIDAndProductId($invoiceID, $productId)
    {
        $rowset = $this->tableGateWay->select(array('invoiceID' => $invoiceID, 'productID' => $productId));
        return $rowset->current();
    }

    public function editReceipt(InvoiceReceipt $inv)
    {
        $data = array(
            'productID' => $inv->productID,
            'productName' => $inv->productName,
            'quantity' => $inv->quantity,
            'unitPrice' => $inv->unitPrice,
            'uom' => $inv->uom,
            'discount' => $inv->discount,
            'invoiceID' => $inv->invoiceID,
            'total' => $inv->total,
        );
        $this->tableGateWay->update($data, array('productID' => $inv->productID));
    }

    public function updateReceiptFromReturns($retunedQuantity, $invReceiptID)
    {
        $data = array(
            'returnedQuantity' => $retunedQuantity,
        );
        $this->tableGateWay->update($data, array('id' => $invReceiptID));
    }

    public function deleteReceipt($invoiceID, $productId)
    {
        $rowset = $this->tableGateWay->delete(array('invoiceID' => $invoiceID, 'productID' => $productId));
        return $rowset;
    }

}

?>
