<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains credit note payments details Table Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class CreditNotePaymentDetailsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCreditNotePaymentDetails($creditNotePaymentDetails)
    {
        $data = array(
            'paymentMethodID' => $creditNotePaymentDetails->paymentMethodID,
            'creditNotePaymentID' => $creditNotePaymentDetails->creditNotePaymentID,
            'creditNoteID' => $creditNotePaymentDetails->creditNoteID,
            'creditNotePaymentDetailsAmount' => $creditNotePaymentDetails->creditNotePaymentDetailsAmount,
            'creditNotePaymentDetailsCurrencyGainOrLoss' => $creditNotePaymentDetails->creditNotePaymentDetailsCurrencyGainOrLoss,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getCreditNotePaymentDetailsByCreditNotePaymentID($creditNotePaymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePaymentDetails')
                ->columns(array('*'))
                ->where(array('creditNotePaymentID' => $creditNotePaymentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getCreditNotePaymentsByCreditNoteID($creditNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePaymentDetails')
                ->columns(array('*'))
                ->where(array('creditNoteID' => $creditNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
