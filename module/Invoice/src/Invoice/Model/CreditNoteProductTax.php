<?php

/**
 * @author Ashan     <ashan@thinkcube.com>
 * This file contains Credit Note product tax Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CreditNoteProductTax implements InputFilterAwareInterface
{

    public $creditNoteProductTaxID;
    public $creditNoteProductID;
    public $taxID;
    public $creditNoteProductTaxPercentage;
    public $creditNoteTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->creditNoteProductTaxID = (!empty($data['creditNoteProductTaxID'])) ? $data['creditNoteProductTaxID'] : null;
        $this->creditNoteProductID = (!empty($data['creditNoteProductID'])) ? $data['creditNoteProductID'] : null;
        $this->taxID = (!empty($data['taxID'])) ? $data['taxID'] : null;
        $this->creditNoteProductTaxPercentage = (!empty($data['creditNoteProductTaxPercentage'])) ? $data['creditNoteProductTaxPercentage'] : null;
        $this->creditNoteTaxAmount = (!empty($data['creditNoteTaxAmount'])) ? $data['creditNoteTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
