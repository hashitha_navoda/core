<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DraftInvProductTax implements InputFilterAwareInterface
{

    public $draftInvProductTaxID;
    public $salesInvoiceProductID;
    public $productID;
    public $taxID;
    public $salesInvoiceProductTaxPrecentage;
    public $salesInvoiceProductTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->draftInvProductTaxID = (!empty($data['draftInvProductTaxID'])) ? $data['draftInvProductTaxID'] : null;
        $this->salesInvoiceProductID = (!empty($data['salesInvoiceProductID'])) ? $data['salesInvoiceProductID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->taxID = (!empty($data['taxID'])) ? $data['taxID'] : null;
        $this->salesInvoiceProductTaxPrecentage = (!empty($data['salesInvoiceProductTaxPrecentage'])) ? $data['salesInvoiceProductTaxPrecentage'] : null;
        $this->salesInvoiceProductTaxAmount = (!empty($data['salesInvoiceProductTaxAmount'])) ? $data['salesInvoiceProductTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
