<?php

/**
 * @author ashan     <ashan@thinkcube.com>
 * This file contains creditc note Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Between;

class InvoiceReturnTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('invoiceReturns')
                    ->columns(array('*'))
                    ->join('customer', 'invoiceReturns.customerID = customer.customerID', array('customerName', 'customerCode'), 'left')
                    ->join('currency', 'invoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->order(array('invoiceReturnID' => 'DESC'))
                    ->where(array('locationID' => $locationID));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('invoiceReturns');
        $select->where(array('locationID' => $locationID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function checkInvoiceReturnByCode($creditNoteCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('invoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "invoiceReturns.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('invoiceReturns.invoiceReturnCode' => $creditNoteCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function saveInvoiceReturn(InvoiceReturn $invoiceReturn)
    {
        $data = array(
            'invoiceReturnCode' => $invoiceReturn->invoiceReturnCode,
            'customerID' => $invoiceReturn->customerID,
            'invoiceReturnDate' => $invoiceReturn->invoiceReturnDate,
            'invoiceReturnTotal' => $invoiceReturn->invoiceReturnTotal,
            'locationID' => $invoiceReturn->locationID,
            'invoiceID' => $invoiceReturn->invoiceID,
            'invoiceReturnComment' => $invoiceReturn->invoiceReturnComment,
            'statusID' => $invoiceReturn->statusID,
            'customCurrencyId' => $invoiceReturn->customCurrencyId,
            'invoiceReturnCustomCurrencyRate' => $invoiceReturn->invoiceReturnCustomCurrencyRate,
            'entityID' => $invoiceReturn->entityID,
            'invoiceReturnDiscountType' => $invoiceReturn->invoiceReturnDiscountType,
            'invoiceReturnTotalDiscount' => $invoiceReturn->invoiceReturnTotalDiscount,
            'promotionID' => $invoiceReturn->promotionID,
            'invoiceReturnPromotionDiscount' => $invoiceReturn->invoiceReturnPromotionDiscount,
            'isApproved' => $invoiceReturn->isApproved,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    

    public function getInvoiceReturnByCustomerIDAndLocationID($customerID, $locationID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('invoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "invoiceReturns.entityID = e.entityID")
                    ->join('customer', "invoiceReturns.customerID = customer.customerID")
                    ->join('currency', 'invoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    // ->where(array('e.deleted' => '0'))
                    ->where(array('invoiceReturns.customerID' => $customerID, 'invoiceReturns.locationID' => $locationID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getInvoiceReturnByInvoiceReturnID($invoiceReturnID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('invoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "invoiceReturns.entityID = e.entityID")
                    ->join('customer', "invoiceReturns.customerID = customer.customerID", array('*'), 'left')
                    ->join('currency', 'invoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    // ->where(array('e.deleted' => '0'))
                    ->where(array('invoiceReturns.invoiceReturnID' => $invoiceReturnID,));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getOpenInvoiceReturnByInvoiceReturnID()
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('invoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "invoiceReturns.entityID = e.entityID")
                    ->join('customer', "invoiceReturns.customerID = customer.customerID", array('*'), 'left')
                    ->join('currency', 'invoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->where(array('e.deleted' => '0'))
                    ->where(array('invoiceReturns.statusID' => 3));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getInvoiceReturnsByInvoiceID($invoiceID, $locationID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('invoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "invoiceReturns.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('invoiceReturns.invoiceID' => $invoiceID,'invoiceReturns.locationID' => $locationID));

            $select->where->notEqualTo('invoiceReturns.statusID', 4);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    } 


    public function getInvoiceReturnByInvoiceIDForEdit($invoiceID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('invoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "invoiceReturns.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('invoiceReturns.invoiceID' => $invoiceID));

            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getInvoiceReturnsByDate($fromdate, $todate, $customerID = NULL, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("invoiceReturns")
                ->columns(array("*"))
                ->join("customer", "invoiceReturns.customerID = customer.customerID", array("customerName", "customerShortName"), "left")
                ->join('currency', 'invoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->order(array('invoiceReturnDate' => 'DESC'))
                ->where(array("invoiceReturns.locationID" => $locationID));
        if ($customerID != NULL) {
            $select->where(array("invoiceReturns.customerID" => $customerID));
        }
        
        $select->where->between('invoiceReturnDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function updateInvoiceReturnStatus($invoiceReturnID, $status)
    {
        $invoiceReturnData = array(
            'statusID' => $status,
        );

        $res = $this->tableGateway->update($invoiceReturnData, array('invoiceReturnID' => $invoiceReturnID));
        return TRUE;
    }

    // /**
    //  * @author Sharmilan <sharmilan@thinkcube.com>
    //  * Search All Credit note for drop down
    //  * @param string $locationID
    //  * @param string $searchKey
    //  * @return Array limited to maximum 50 rows
    //  */
    public function searchAllInvoiceReturnForDropdown($locationID, $searchKey)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("invoiceReturns")
                ->columns(array("invoiceReturnID", "invoiceReturnCode"))
                ->order(array('invoiceReturnDate' => 'DESC'))
                ->where(array("invoiceReturns.locationID" => $locationID));
        $select->where->like('invoiceReturnCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    

    public function getInvoiceReturnByIdForDeleteInvoiceReturn($id)
    {
        $rowset = $this->tableGateway->select(array('invoiceReturnID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function updateInvoiceReturnStatusForCancel($invoiceReturnID, $statusID)
    {
        $data = array(
            'statusID' => $statusID
        );
        return $this->tableGateway->update($data, array('invoiceReturnID' => $invoiceReturnID));
    }

    public function updateInvoiceReturn($data, $invoiceReturnID)
    {
        $this->tableGateway->update($data, array('invoiceReturnID' => $invoiceReturnID));
        return TRUE;
    }

}
