<?php

/**
 * @author
 * This file contains Rating Types related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ContactEmailsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //get all contact emails
    public function fetchAll()
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('contactEmails');

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //save function of contact emails
    public function saveContactEmail($dataSet)
    {
        $data = array(
            'contactID' => $dataSet->contactID,
            'emailAddress' => $dataSet->emailAddress,
            'emailType' => $dataSet->emailType,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get all rating types
    public function getRelatedEmailByContactID($contactID)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('contactEmails')
                ->where(array('contactID' => $contactID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        $resultSet = [];

        if (sizeof($rowset) > 0) {
            foreach ($rowset as $key => $value) {
                $resultSet[$value['emailType']] = $value['emailAddress'];
            }
        }

        return $resultSet;
    }

    public function deleteContactEmails($contactID)
    {
        try {
            $result = $this->tableGateway->delete(array('contactID' => $contactID));
            return $result;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }
}
