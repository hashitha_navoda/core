<?php

/**
 * @author Ashan <ashan@thinkcube.com>
 * This file contains Credit Note sub product Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CreditNoteSubProduct implements InputFilterAwareInterface
{

    public $creditNoteSubProductID;
    public $creditNoteProductID;
    public $productBatchID;
    public $productSerialID;
    public $creditNoteSubProductQuantity;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->creditNoteSubProductID = (!empty($data['creditNoteSubProductID'])) ? $data['creditNoteSubProductID'] : null;
        $this->creditNoteProductID = (!empty($data['creditNoteProductID'])) ? $data['creditNoteProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->creditNoteSubProductQuantity = (!empty($data['creditNoteSubProductQuantity'])) ? $data['creditNoteSubProductQuantity'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
