<?php

/**
 * @author sandun<sandun@thinkcube.com>
 * This file contains InvoiceEditLogDetials Model
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoiceEditLogDetials implements InputFilterAwareInterface
{

    public $invoiceEditLogDetialsID;
    public $invoiceEditLogDetailsOldState;
    public $invoiceEditLogDetailsNewState;
    public $invoiceEditLogDetailsCategory;
    public $invoiceEditLogID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->invoiceEditLogDetialsID = (!empty($data['invoiceEditLogDetialsID'])) ? $data['invoiceEditLogDetialsID'] : 0;
        $this->invoiceEditLogDetailsOldState = (!empty($data['invoiceEditLogDetailsOldState'])) ? $data['invoiceEditLogDetailsOldState'] : null;
        $this->invoiceEditLogDetailsNewState = (!empty($data['invoiceEditLogDetailsNewState'])) ? $data['invoiceEditLogDetailsNewState'] : null;
        $this->invoiceEditLogDetailsCategory = (!empty($data['invoiceEditLogDetailsCategory'])) ? $data['invoiceEditLogDetailsCategory'] : NULL;
        $this->invoiceEditLogID = (!empty($data['invoiceEditLogID'])) ? $data['invoiceEditLogID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
