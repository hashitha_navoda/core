<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains payments Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Payments implements InputFilterAwareInterface
{

    public $incomingPaymentID;
    public $incomingPaymentCode;
    public $incomingPaymentDate;
    public $paymentTermID;
    public $incomingPaymentAmount;
    public $customerID;
    public $incomingPaymentDiscount;
    public $incomingPaymentMemo;
    public $incomingPaymentType;
    public $locationID;
    public $entityID;
    public $pos;
    public $incomingPaymentBalance;
    public $incomingPaymentPaidAmount;
    public $incomingPaymentStatus;
    public $incomingPaymentCancelMessage;
    public $incomingPaymentCreditAmount;
    public $customCurrencyId;
    public $incomingPaymentCustomCurrencyRate;
    protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->incomingPaymentID = (!empty($data['incomingPaymentID'])) ? $data['incomingPaymentID'] : null;
        $this->incomingPaymentCode = (!empty($data['incomingPaymentCode'])) ? $data['incomingPaymentCode'] : null;
        $this->incomingPaymentDate = (!empty($data['incomingPaymentDate'])) ? $data['incomingPaymentDate'] : null;
        $this->paymentTermID = (!empty($data['paymentTermID'])) ? $data['paymentTermID'] : null;
        $this->incomingPaymentAmount = (!empty($data['incomingPaymentAmount'])) ? $data['incomingPaymentAmount'] : 0.00;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : 0;
        $this->incomingPaymentDiscount = (!empty($data['incomingPaymentDiscount'])) ? $data['incomingPaymentDiscount'] : 0.00;
        $this->incomingPaymentMemo = (!empty($data['incomingPaymentMemo'])) ? $data['incomingPaymentMemo'] : null;
        $this->incomingPaymentType = (!empty($data['incomingPaymentType'])) ? $data['incomingPaymentType'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->pos = (!empty($data['pos'])) ? $data['pos'] : 0;
        $this->incomingPaymentBalance = (!empty($data['incomingPaymentBalance'])) ? $data['incomingPaymentBalance'] : 0;
        $this->incomingPaymentPaidAmount = (!empty($data['incomingPaymentPaidAmount'])) ? $data['incomingPaymentPaidAmount'] : 0;
        $this->incomingPaymentStatus = (!empty($data['incomingPaymentStatus'])) ? $data['incomingPaymentStatus'] : 0;
        $this->incomingPaymentCancelMessage = (!empty($data['incomingPaymentCancelMessage'])) ? $data['incomingPaymentCancelMessage'] : null;
        $this->incomingPaymentCreditAmount = (!empty($data['incomingPaymentCreditAmount'])) ? $data['incomingPaymentCreditAmount'] : null;
        $this->customCurrencyId = (!empty($data['customCurrencyId'])) ? $data['customCurrencyId'] : null;
        $this->incomingPaymentCustomCurrencyRate = (!empty($data['incomingPaymentCustomCurrencyRate'])) ? $data['incomingPaymentCustomCurrencyRate'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'incomingPaymentID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'paymentTermID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'customerID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'incomingPaymentMemo',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'incomingPaymentType',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

// Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
