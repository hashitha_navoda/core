<?php

namespace Invoice\Model;

/**
 * Class DispatchNote
 * @package Invoice\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class DispatchNote
{
    /**
     * @var int Auto incremented dispatch note Id. (Primary key)
     */
    public $dispatchNoteId;
    
    /**
     * @var string dispatch note code
     */
    public $dispatchNoteCode;
    
    /**
     * @var date dispatch note date
     */
    public $dispatchNoteDate;
    
    /**
     * @var string dispatch note address
     */
    public $dispatchNoteAddress;
    
    /**
     * @var string dispatch note comment 
     */
    public $dispatchNoteComment;
    
    /**
     * @var int invoice id 
     */
    public $invoiceId;
    
    /**
     * @var int sales person id 
     */
    public $salesPersonId;
    
    /**
     * @var int entity id 
     */
    public $entityId;

    public function exchangeArray($data)
    {
        $this->dispatchNoteId      = (!empty($data['dispatchNoteId'])) ? $data['dispatchNoteId'] : null;
        $this->dispatchNoteCode    = (!empty($data['dispatchNoteCode'])) ? $data['dispatchNoteCode'] : null;
        $this->dispatchNoteDate    = (!empty($data['dispatchNoteDate'])) ? $data['dispatchNoteDate'] : null;
        $this->dispatchNoteAddress = (!empty($data['dispatchNoteAddress'])) ? $data['dispatchNoteAddress'] : null;
        $this->dispatchNoteComment = (!empty($data['dispatchNoteComment'])) ? $data['dispatchNoteComment'] : null;
        $this->invoiceId           = (!empty($data['invoiceId'])) ? $data['invoiceId'] : null;
        $this->salesPersonId       = (!empty($data['salesPersonId'])) ? $data['salesPersonId'] : null;
        $this->entityId            = (!empty($data['entityId'])) ? $data['entityId'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
