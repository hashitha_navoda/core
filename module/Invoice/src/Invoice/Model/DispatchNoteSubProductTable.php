<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DispatchNoteSubProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDispatchNoteSubProduct(DispatchNoteSubProduct $dispatchNoteSubProduct)
    {
        $data = array(
            'dispatchNoteProductId' => $dispatchNoteSubProduct->dispatchNoteProductId,
            'productBatchId' => $dispatchNoteSubProduct->productBatchId,
            'productSerialId' => $dispatchNoteSubProduct->productSerialId,
            'dispatchNoteSubProductQuantity' => $dispatchNoteSubProduct->dispatchNoteSubProductQuantity,
            'dispatchNoteSubProductsWarranty' => $dispatchNoteSubProduct->dispatchNoteSubProductsWarranty
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }
    
    /**
     * Check whether sub product dispatch or not.
     * @param int $invoiceProductId
     * @param int $subProductId
     * @param string $productType
     * @return boolean
     */
    public function isSubProductDispatched( $invoiceProductId, $subProductId, $productType)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNoteSubProduct')
                ->columns(array('*'))
                ->join('dispatchNoteProduct', 'dispatchNoteSubProduct.dispatchNoteProductId = dispatchNoteProduct.dispatchNoteProductId', array('dispatchNoteId','salesInvoiceProductId'))
                ->join('dispatchNote', 'dispatchNoteProduct.dispatchNoteId = dispatchNote.dispatchNoteId', array('entityId'))
                ->join('entity', 'dispatchNote.entityId = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)       
        ->where->equalTo('salesInvoiceProductId', $invoiceProductId);
        if($productType == 'serial'){
            $select->where->equalTo('dispatchNoteSubProduct.productSerialId', $subProductId);
        } else {
            $select->where->equalTo('dispatchNoteSubProduct.productBatchId', $subProductId);
        }        
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return FALSE;
        }
        return TRUE;
    }
    
    /**
     * Get dispatched quantity of the sub product
     * @param type $invoiceProductId
     * @param type $subProductId
     * @param type $productType
     * @return int
     */
    public function getDispatchedQuantityOfSubProduct( $invoiceProductId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('dispatchNoteSubProduct')
                ->columns(array(new Expression('SUM(dispatchNoteSubProductQuantity) as dispatchedQuantity')))
                ->join('dispatchNoteProduct', 'dispatchNoteSubProduct.dispatchNoteProductId = dispatchNoteProduct.dispatchNoteProductId', array('dispatchNoteId','salesInvoiceProductId'))
                ->join('dispatchNote', 'dispatchNoteProduct.dispatchNoteId = dispatchNote.dispatchNoteId', array('entityId'))
                ->join('entity', 'dispatchNote.entityId = entity.entityID', array('deleted'))
        ->where->equalTo('entity.deleted', 0)       
        ->where->equalTo('salesInvoiceProductId', $invoiceProductId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->current();
        if(is_null($result['dispatchedQuantity'])){
            return 0;
        }
        return $result['dispatchedQuantity'];
    }

}
