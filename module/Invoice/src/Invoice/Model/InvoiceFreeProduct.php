<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoiceFreeProduct implements InputFilterAwareInterface
{

    public $salesInvoiceFreeIssueItemsID;
    public $invoiceID;
    public $mainProductID;
    public $mainSalesInvoiceProductID;
    public $freeProductID;
    public $freeQuantity;
    public $salesInvoiceFreeItemSelectedUom;
    public $locationID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->salesInvoiceFreeIssueItemsID = (!empty($data['salesInvoiceFreeIssueItemsID'])) ? $data['salesInvoiceFreeIssueItemsID'] : null;
        $this->invoiceID = (!empty($data['invoiceID'])) ? $data['invoiceID'] : null;
        $this->mainProductID = (!empty($data['mainProductID'])) ? $data['mainProductID'] : null;
        $this->mainSalesInvoiceProductID = (!empty($data['mainSalesInvoiceProductID'])) ? $data['mainSalesInvoiceProductID'] : null;
        $this->freeProductID = (!empty($data['freeProductID'])) ? $data['freeProductID'] : null;
        $this->freeQuantity = (!empty($data['freeQuantity'])) ? $data['freeQuantity'] : null;
        $this->salesInvoiceFreeItemSelectedUom = (!empty($data['salesInvoiceFreeItemSelectedUom'])) ? $data['salesInvoiceFreeItemSelectedUom'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
