<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains credit note payment Table Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class CreditNotePaymentTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new \Zend\Db\Sql\Select;
            $select->from('creditNotePayment')
                    ->columns(array('*'))
                    ->order(array('creditNotePaymentDate' => 'DESC'))
                    ->JOIN('entity', 'creditNotePayment.entityID= entity.entityID ', array('deleted'), 'left')
                    ->JOIN('customer', 'customer.customerID= creditNotePayment.customerID ', array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->join('currency', 'creditNotePayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->where(array('locationID' => $locationID/** , 'deleted' => 0* */));
            return new \Zend\Paginator\Paginator(
                    new \Zend\Paginator\Adapter\DbSelect(
                    $select, $this->tableGateway->getAdapter()
                    )
            );
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveCreditNotePayment($creditNotePayment)
    {
        $data = array(
            'creditNotePaymentID' => $creditNotePayment->creditNotePaymentID,
            'creditNotePaymentCode' => $creditNotePayment->creditNotePaymentCode,
            'creditNotePaymentDate' => $creditNotePayment->creditNotePaymentDate,
            'paymentTermID' => $creditNotePayment->paymentTermID,
            'creditNotePaymentAmount' => $creditNotePayment->creditNotePaymentAmount,
            'customerID' => $creditNotePayment->customerID,
            'creditNotePaymentDiscount' => $creditNotePayment->creditNotePaymentDiscount,
            'creditNotePaymentMemo' => $creditNotePayment->creditNotePaymentMemo,
            'locationID' => $creditNotePayment->locationID,
            'statusID' => $creditNotePayment->statusID,
            'entityID' => $creditNotePayment->entityID,
            'customCurrencyId' => $creditNotePayment->customCurrencyId,
            'creditNotePaymentCustomCurrencyRate' => $creditNotePayment->creditNotePaymentCustomCurrencyRate,
            'creditNotePaymentLoyaltyAmount' => $creditNotePayment->creditNotePaymentLoyaltyAmount,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getCreditNotePaymentsByLocatioID($locationID, $flag = NULL)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment')
                ->columns(array("*"))
                ->join('entity', 'creditNotePayment.entityID = entity.entityID', array("deleted"), "left")
                ->where(array('creditNotePayment.locationID' => $locationID));
        if ($flag == NULL) {
            $select->where(array('deleted' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getCreditNotePaymentsByCustomerIDAndLocationID($customerID, $locationID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNotePayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNotePayment.entityID = e.entityID")
                    ->join('customer', "creditNotePayment.customerID = customer.customerID")
                    ->join('currency', 'creditNotePayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
//->where(array('e.deleted' => '0'))
                    ->where(array('creditNotePayment.customerID' => $customerID, 'creditNotePayment.locationID' => $locationID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNotePaymentByCreditNotePaymentID($creditNotePaymentID, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNotePayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNotePayment.entityID = e.entityID")
                    ->join('customer', "creditNotePayment.customerID = customer.customerID", array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->join('currency', 'creditNotePayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->where(array('creditNotePayment.creditNotePaymentID' => $creditNotePaymentID));
            if ($flag == NULL) {
                $select->where(array('e.deleted' => '0'));
            }
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNotePaymentByCreditNotePaymentIDForRec($creditNotePaymentID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNotePayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNotePayment.entityID = e.entityID")
                    ->join('creditNotePaymentMethodNumbers', 'creditNotePayment.creditNotePaymentID = creditNotePaymentMethodNumbers.creditNotePaymentID', ['*'], 'left')
                    ->join('creditNotePaymentDetails', 'creditNotePayment.creditNotePaymentID = creditNotePaymentDetails.creditNotePaymentID', ['*'], 'left')
                    ->where(array('creditNotePayment.creditNotePaymentID' => $creditNotePaymentID));
                $select->where(array('e.deleted' => '0'));
            
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNotePaymentByCreditNotePaymentIDForJE($creditNotePaymentID, $flag = NULL)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNotePayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNotePayment.entityID = e.entityID")
                    ->join('customer', "creditNotePayment.customerID = customer.customerID", array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->join('currency', 'creditNotePayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->where(array('creditNotePayment.creditNotePaymentID' => $creditNotePaymentID));
            $select->where->notEqualTo('creditNotePayment.statusID', 5);
            $select->where->notEqualTo('creditNotePayment.statusID', 10);
            if ($flag == NULL) {
                $select->where(array('e.deleted' => '0'));
            }
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNotePaymentByCreditNotePaymentCode($id)
    {
        $rowset = $this->tableGateway->select(array('creditNotePaymentCode' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getCreditNotePaymentsByDate($fromdate, $todate, $customerID = NULL, $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment')
                ->columns(array('*'))
                ->join('entity', 'creditNotePayment.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customer', 'customer.customerID = creditNotePayment.customerID', array('customerName', 'customerShortName', 'customerStatus'), 'left')
                ->join('currency', 'creditNotePayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->where(array('creditNotePaymentDate >= ?' => $fromdate))
                ->where(array('creditNotePaymentDate <= ?' => $todate, 'locationID' => $locationID/** , 'deleted' => 0* */));
        if ($customerID) {
            $select->where(array('creditNotePayment.customerID' => $customerID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getCreditNotePaymentByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment')
                ->columns(array('*'))
                ->join('creditNotePaymentDetails', 'creditNotePaymentDetails.creditNotePaymentID = creditNotePayment.creditNotePaymentID', array('creditNoteID'),'left')
                ->join('creditNote', 'creditNote.creditNoteID = creditNotePaymentDetails.creditNoteID', array('creditNoteCode'), 'left')
                ->join('customer', 'customer.customerID = creditNotePayment.customerID', array('*'),'left')
                ->join('status', 'creditNotePayment.statusID = status.statusID',array('statusName'),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('creditNotePaymentDate' => 'DESC'))
                ->where(array('entity.deleted' => '0'));
                $select->where->in('creditNotePayment.statusID',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNotePayment.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('creditNotePaymentDate', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCustomerCumulativeCreditNotePaymentBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment')
                ->columns(array('customerID','cumulative_total' => new \Zend\Db\Sql\Expression('SUM(creditNotePaymentAmount)')))
                ->join('customer', 'customer.customerID = creditNotePayment.customerID', array(),'left')
                ->join('status', 'creditNotePayment.statusID = status.statusID',array(),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array(), 'left')
                ->where(array('entity.deleted' => '0'));
                $select->where->in('creditNotePayment.statusID',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNotePayment.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('creditNotePayment.creditNotePaymentDate', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $select->group(array('creditNotePayment.customerID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function update($data, $paymentID)
    {
        return $this->tableGateway->update($data, array('creditNotePaymentID' => $paymentID));
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search all Credit note Payments for dropdown list
     * @param string $locationID
     * @param string $searchKey
     * @return array limited to maximum 50rows
     */
    public function searchAllCreditNotepaymentsForDropdown($locationID, $searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment')
                ->columns(array("*"))
                ->join('entity', 'creditNotePayment.entityID = entity.entityID', array("deleted"), "left")
                ->where(array('creditNotePayment.locationID' => $locationID));
        $select->where->like('creditNotePaymentCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getCashPaymentsByLocationID($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment')
                ->columns(['*'])
                ->join('creditNotePaymentMethodNumbers', 'creditNotePayment.creditNotePaymentID = creditNotePaymentMethodNumbers.creditNotePaymentID', ['paymentMethodID'], 'left')
                ->join('creditNotePaymentDetails', 'creditNotePayment.creditNotePaymentID = creditNotePaymentDetails.creditNotePaymentID', ['paymentMethodID', 'creditNotePaymentDetailsAmount'], 'left')
                ->join('entity', 'creditNotePayment.entityID = entity.entityID', ['deleted'], 'left')
                ->where(['creditNotePayment.locationID' => $locationID, 'deleted' => 0, 'creditNotePaymentDetails.paymentMethodID' => '1']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * this function returns cashoutflow for current date month and year
     */
    public function getCurrentCashOutFlowBy($type)
    {
        $whereClause;
        if ($type == 'year') {
            $whereClause = date('Y');
        } else if ($type == 'month') {
            $whereClause = date('Y-m');
        } else {
            $whereClause = date('Y-m-d');
        }
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('creditNotePayment')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`creditNotePaymentDate`, "%m")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`creditNotePaymentDate`, "%m %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`creditNotePaymentDate`, "%d")'),
                    'Year' => new Expression('Year(creditNotePaymentDate)'),
                    'TotalCashOutFlow' => new Expression('SUM(creditNotePaymentAmount)')
                ))
                ->join('entity', 'entity.entityID = creditNotePayment.entityID', '*', 'left')
                ->where(array('entity.deleted' => 0))
        ->where->like('creditNotePaymentDate', '%' . $whereClause . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    // get delivery note related credit note payment by quotation id
    public function getDeliveryNoteRelatedCreditNotePaymentDataByQuotationId($quotationID = null, $soID = null, $deliveryNoteID = null, $invoiceID = null, $returnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment');
        $select->columns(array('*'))
                ->join('creditNotePaymentDetails', 'creditNotePayment.creditNotePaymentID = creditNotePaymentDetails.creditNotePaymentID', array('*'), 'left');
        $select->join('creditNote', 'creditNotePaymentDetails.creditNoteID = creditNote.creditNoteID', array('*'), 'left');
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
        $select->join('deliveryNote', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
        $select->join('salesOrders', 'deliveryNote.salesOrderID = salesOrders.soID', array('*'),'left');
        $select->join('entity', 'creditNotePayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->where(array('salesOrders.quotationID' => $quotationID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($soID)) {
            $select->where(array('salesOrders.soID' => $soID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($deliveryNoteID)) {
            $select->where(array('deliveryNote.deliveryNoteID' => $deliveryNoteID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($invoiceID)) {
            $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID));   
        }
        if (!is_null($returnID)) {
            $select->join('deliveryNoteProduct', "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array('*'), 'left');
            $select->join('salesReturnProduct', "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array('*'), 'left');
            $select->join('salesReturn', "salesReturn.salesReturnID = salesReturnProduct.salesReturnID", array('*'), 'left');
            $select->where(array('salesReturn.salesReturnID' => $returnID));
        }
        $select->group("creditNotePayment.creditNotePaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get sales order related credit note payment by quotation id
    public function getSalesOrderRelatedCreditNotePaymentDataByQuotationId($quotationID = null , $soID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment');
        $select->columns(array('*'))
                ->join('creditNotePaymentDetails', 'creditNotePayment.creditNotePaymentID = creditNotePaymentDetails.creditNotePaymentID', array('*'), 'left');
        $select->join('creditNote', 'creditNotePaymentDetails.creditNoteID = creditNote.creditNoteID', array('*'), 'left');
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
        $select->join('salesOrders', 'salesInvoice.salesOrderID = salesOrders.soID', array('*'),'left');
        $select->join('entity', 'creditNotePayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->where(array('salesOrders.quotationID' => $quotationID));
        }
        if (!is_null($soID)) {
            $select->where(array('salesOrders.soID' => $soID));
        }
        $select->group("creditNotePayment.creditNotePaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get quotation related credit note payment by quotation id
    public function getQuotationRelatedCreditNotePaymentDataByQuotationId($quotationID = null , $creditNoteID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment');
        $select->columns(array('*'))
                ->join('creditNotePaymentDetails', 'creditNotePayment.creditNotePaymentID = creditNotePaymentDetails.creditNotePaymentID', array('*'), 'left');
        $select->join('creditNote', 'creditNotePaymentDetails.creditNoteID = creditNote.creditNoteID', array('*'), 'left');
        $select->join('entity', 'creditNotePayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
            $select->where(array('salesInvoice.quotationID' => $quotationID));
        }
        if (!is_null($creditNoteID)) {
            $select->where(array('creditNote.creditNoteID' => $creditNoteID));
        }
        $select->group("creditNotePayment.creditNotePaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    public function getCustomerCumulativeCreditNotePaymentBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment')
                ->columns(array('customerID','creditNotePaymentAmount','creditNotePaymentID'))
                ->join('customer', 'customer.customerID = creditNotePayment.customerID', array(),'left')
                ->join('status', 'creditNotePayment.statusID = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'creditNotePayment.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('creditNotePayment.statusID',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNotePayment.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCreditNotePaymentByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment')
                ->columns(array('*'))
                ->join('creditNotePaymentDetails', 'creditNotePaymentDetails.creditNotePaymentID = creditNotePayment.creditNotePaymentID', array('creditNoteID'),'left')
                ->join('creditNote', 'creditNote.creditNoteID = creditNotePaymentDetails.creditNoteID', array('creditNoteCode'), 'left')
                ->join('customer', 'customer.customerID = creditNotePayment.customerID', array('*'),'left')
                ->join('status', 'creditNotePayment.statusID = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'creditNotePayment.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('creditNotePayment.statusID',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNotePayment.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCreditNotePaymentsByCreditNotePaymentID($creditNotePaymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNotePayment')
                ->columns(array("*"))
                ->join('entity', 'creditNotePayment.entityID = entity.entityID', array("deleted"), "left")
                ->where(array('creditNotePayment.creditNotePaymentID' => $creditNotePaymentID));
        $select->where(array('deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }
}
