<?php

namespace Invoice\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterInterface;

class Quotation
{

    public $quotationID;
    public $quotationCode;
    public $customerName;
    public $customerID;
    public $issuedDate;
    public $expireDate;
    public $paymentTerm;
    public $taxAmount;
    public $totalDiscount;
    public $totalAmount;
    public $comment;
    public $branch;
    public $showTax;
    public $entityID;
    public $salesPersonID;
    public $status;
    public $jobRefType;
    public $jobRefTypeID;
    public $customCurrencyId;
    public $quotationAdditionalDetail2;
    public $quotationAdditionalDetail1;
    public $quotationCustomCurrencyRate;
    public $quotationTotalDiscountType;
    public $quotationDiscountRate;
    public $quotationWiseTotalDiscount;
    public $priceListId;
    public $customerProfileID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->quotationID = (!empty($data['quotationID'])) ? $data['quotationID'] : null;
        $this->quotationCode = (!empty($data['quotationCode'])) ? $data['quotationCode'] : null;
        $this->issuedDate = (!empty($data['quotationIssuedDate'])) ? $data['quotationIssuedDate'] : null;
        $this->expireDate = (!empty($data['quotationExpireDate'])) ? $data['quotationExpireDate'] : null;
        $this->customerName = (!empty($data['quotationCustomerName'])) ? $data['quotationCustomerName'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->paymentTerm = (!empty($data['quotationPayementTerm'])) ? (int) $data['quotationPayementTerm'] : null;
        $this->comment = (!empty($data['quotationComment'])) ? $data['quotationComment'] : null;
        $this->branch = (!empty($data['quotationBranch'])) ? $data['quotationBranch'] : null;
        $this->taxAmount = (!empty($data['quotationTaxAmount'])) ? $data['quotationTaxAmount'] : null;
        $this->totalDiscount = (!empty($data['quotationTotalDiscount'])) ? $data['quotationTotalDiscount'] : null;
        $this->totalAmount = (!empty($data['quotationTotalAmount'])) ? $data['quotationTotalAmount'] : null;
        $this->showTax = (!empty($data['quotationShowTax'])) ? $data['quotationShowTax'] : 0;
        $this->footerID = (!empty($data['quotationFooterID'])) ? $data['quotationFooterID'] : 0;
        $this->salesPersonID = (!empty($data['quotationSalesPersonID'])) ? $data['quotationSalesPersonID'] : null;
        $this->status = (!empty($data['quotationStatus'])) ? $data['quotationStatus'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : 0;
        $this->jobRefType = (!empty($data['jobRefType'])) ? $data['jobRefType'] : 0;
        $this->jobRefTypeID = (!empty($data['jobRefTypeID'])) ? $data['jobRefTypeID'] : 0;
        $this->customCurrencyId = (!empty($data['customCurrencyId'])) ? $data['customCurrencyId'] : null;
        $this->quotationAdditionalDetail2 = (!empty($data['quotationAdditionalDetail2'])) ? $data['quotationAdditionalDetail2'] : null;
        $this->quotationAdditionalDetail1 = (!empty($data['quotationAdditionalDetail1'])) ? $data['quotationAdditionalDetail1'] : null;
        $this->quotationCustomCurrencyRate = (!empty($data['quotationCustomCurrencyRate'])) ? $data['quotationCustomCurrencyRate'] : 0;
        $this->quotationTotalDiscountType = (!empty($data['quotationTotalDiscountType'])) ? $data['quotationTotalDiscountType'] : null;
        $this->quotationDiscountRate = (!empty($data['quotationDiscountRate'])) ? $data['quotationDiscountRate'] : 0;
        $this->quotationWiseTotalDiscount = (!empty($data['quotationWiseTotalDiscount'])) ? $data['quotationWiseTotalDiscount'] : 0;
        $this->priceListId = (!empty($data['priceListId'])) ? $data['priceListId'] : 0;
        $this->customerProfileID = (!empty($data['customerProfileID'])) ? $data['customerProfileID'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new inputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'qot_no',
                        'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'cust_name',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'issue_date',
                        'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'expire_date',
                        'required' => true,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesPersonID',
                        'required' => false,
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'jobCardType',
                        'required' => false,
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}
