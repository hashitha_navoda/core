<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Inventory\Model\CompoundCostType;
use Zend\Db\Sql\Sql;
use Core\Model\ProductTable as CoreProductTable;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\Operator;

class DraftInvServiceChargeTable extends CoreProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }


    public function getInvoiceChargesByInvoiceID($id)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('draftInvServiceCharge');
        $select->join("serviceChargeType", "draftInvServiceCharge.serviceChargeTypeID = serviceChargeType.serviceChargeTypeID", array("*"), "left");
        $select->where(array('invoiceID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function save($serviceCharge)
    {
        try {
            $data = array(
                'invoiceID' => $serviceCharge->invoiceID,
                'serviceChargeTypeID' => $serviceCharge->serviceChargeTypeID,
                'serviceChargeAmount' => $serviceCharge->serviceChargeAmount,
            );
            $this->tableGateway->insert($data);
            $insertedID = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $insertedID;

        } catch (\Exception $exc) {
            return null;
            error_log($exc);

        }
    }

    public function update($data, $srcTypeID)
    {
        try {
            $result = $this->tableGateway->update($data, array('serviceChargeTypeID' => $srcTypeID));
            return $result;
        } catch (\Exception $exc) {
            return null;
            error_log($exc);
        }
    }

    public function deleteServiceChargeByInvoiceID($invoiceID) {
        try {
            $result = $this->tableGateway->delete(array('invoiceID' => $invoiceID));
            return $result;
        } catch (\Exception $e) {
            $error = 0;
            return $error;
        }
    }

}
