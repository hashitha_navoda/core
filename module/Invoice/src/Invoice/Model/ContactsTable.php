<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Rating Types related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\Expression;

class ContactsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //get all rating types
    public function fetchAll($paginated = false)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contacts')
                ->columns(array('*'))
                ->join('entity', 'contacts.entityID= entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
                ->order('contactID DESC');

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //save function of rating types
    public function saveContact($contact)
    {
        $data = array(
            'firstName' => $contact->firstName,
            'lastName' => $contact->lastName,
            'title' => $contact->title,
            'designation' => $contact->designation,
            'entityID' => $contact->entityID,
            'isConvertToCustomer' => $contact->isConvertToCustomer,
            'relatedCusID' => $contact->relatedCusID,
        );


        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateContact($contactData, $contactID)
    {
        $dataSet = array(
            'firstName' => $contactData['firstName'],
            'lastName' => $contactData['lastName'],
            'title' => $contactData['title'],
            'designation' => $contactData['designation'],
        );
        // var_dump($dataSet).die();
        $this->tableGateway->update($dataSet, array('contactID' => $contactID));
        return TRUE;
    }


    public function updateLinkedCusData($cusID)
    {
        $dataSet = array(
            'isConvertToCustomer' => 0,
            'relatedCusID' => null,
        );
        $this->tableGateway->update($dataSet, array('relatedCusID' => $cusID));
        return TRUE;
    }

    public function searchContactsForDropDown($contactSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contacts')
                ->columns(array('*'))
                ->join('contactMobileNumbers', 'contacts.contactID = contactMobileNumbers.contactID', array('*'), 'left')
                ->join('entity', 'contacts.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
                ->where(new PredicateSet(array(new Operator('contacts.firstName', 'like', '%' . $contactSearchKey . '%'),new Operator('contacts.lastName', 'like', '%' . $contactSearchKey . '%'), new Operator('contactMobileNumbers.mobileNumber', 'like', '%' . $contactSearchKey . '%')), PredicateSet::OP_OR));
        $select->group("contacts.contactID");
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function getContactsByContactIDForSearch($contactID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contacts')
                ->columns(array('*'))
                ->join('entity', 'contacts.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'contactID' => $contactID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results;
        return $row;
    }

    public function getCotactDetailsById($contactID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contacts')
                ->columns(array('*'))
                ->join('entity', 'contacts.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'contactID' => $contactID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getCotactDetailsCustomerId($customerID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contacts')
                ->columns(array('*'))
                ->join('entity', 'contacts.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'relatedCusID' => $customerID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getContactById($contactID){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contacts')
                ->columns(array('*'))
                ->join('entity', 'contacts.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0'));
        if($contactID){
            $select->where(array('contacts.contactID' => $contactID));
        }
        $select->order('contacts.contactID DESC');                
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
