<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Rating Types related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ContactMobileNumbersTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //get all rating types
    public function getRecrdByMobileNumber($mobileNumber)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('contactMobileNumbers')
                ->where(array('mobileNumber' => $mobileNumber));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    }

    //get all rating types
    public function getRecrdByMobileNumberByContactID($mobileNumber, $contactID)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('contactMobileNumbers')
                ->where(array('mobileNumber' => $mobileNumber));

        $select->where->notEqualTo('contactMobileNumbers.contactID', $contactID);

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset->current();
    }

    public function fetchAll()
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('contactMobileNumbers');

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    //save function of rating types
    public function saveContactMobileNumber($dataSet)
    {
        $data = array(
            'contactID' => $dataSet->contactID,
            'mobileNumber' => $dataSet->mobileNumber,
            'mobileNumberType' => $dataSet->mobileNumberType,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getRelatedNumbersByContactID($contactID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('contactMobileNumbers')
                ->where(array('contactID' => $contactID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        $resultSet = [];

        if (sizeof($rowset) > 0) {
            foreach ($rowset as $key => $value) {
                $resultSet[$value['mobileNumberType']] = $value['mobileNumber'];
            }
        }

        return $resultSet;
    }

    public function deleteContactMobileNumbers($contactID)
    {
        try {
            $result = $this->tableGateway->delete(array('contactID' => $contactID));
            return $result;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }
}
