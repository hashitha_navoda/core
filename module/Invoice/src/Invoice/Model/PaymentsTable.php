<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Payments Table Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;

class PaymentsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID, $POSFlag = false)
    {
        if ($paginated) {
            $select = new \Zend\Db\Sql\Select;
            $select->from('incomingPayment')
                    ->columns(array('*'))
                    ->order(array('incomingPaymentDate' => 'DESC', 'incomingPaymentCode' => 'DESC'))
                    ->JOIN('entity', 'incomingPayment.entityID= entity.entityID ', array('deleted'), 'left')
                    ->JOIN('customer', 'customer.customerID= incomingPayment.customerID ', array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->JOIN('status', 'status.statusID= incomingPayment.incomingPaymentStatus', array('statusName'), 'left')
                    ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->where(array('incomingPayment.locationID' => $locationID/** ,'deleted' => 0 */));
            $select->where->notEqualto('incomingPaymentType', 'income');
            if ($POSFlag) {
                $select->where(array("pos" => 1));
            } else {

                $select->where(array("pos" => 0));
            }
            return new \Zend\Paginator\Paginator(
                    new \Zend\Paginator\Adapter\DbSelect(
                    $select, $this->tableGateway->getAdapter()
                    )
            );
        }
        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function update($data, $paymentID)
    {
        return $this->tableGateway->update($data, array('incomingPaymentID' => $paymentID));
    }

    public function getPaymentsByDate($fromdate, $todate, $customerID = NULL, $locationID, $payType = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('customerName', 'customerShortName', 'customerCode', 'customerStatus'), 'left')
                ->join('currency', 'incomingPayment.customCurrencyId= currency.currencyID', array('*'), 'left')
                ->where(array('incomingPaymentDate >= ?' => $fromdate))
                ->where(array('incomingPaymentDate <= ?' => $todate, 'locationID' => $locationID/** , 'deleted' => 0* */));
        if ($customerID) {
            $select->where(array('incomingPayment.customerID' => $customerID));
        }
        if ($payType == 'cusPay') {
            $select->where(array('incomingPayment.pos' => 0));
        } else if ($payType == 'POS') {
            $select->where(array('incomingPayment.pos' => 1));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentsByCustomerID($custID, $locationID, $payType = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('*'), 'left')
                ->join('currency', 'incomingPayment.customCurrencyId= currency.currencyID', array('*'), 'left')
                ->where(array('incomingPayment.customerID' => $custID, 'incomingPayment.locationID' => $locationID/** , 'deleted' => 0* */));
        if ($payType == 'cusPay') {
            $select->where(array('incomingPayment.pos' => 0));
        } else if ($payType == 'POS') {
            $select->where(array('incomingPayment.pos' => 1));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentsByPaymentID($paymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array("*"))
                ->join('customer', 'incomingPayment.customerID = customer.customerID', array("*"), "left")
                ->join('status', 'incomingPayment.incomingPaymentStatus= status.statusID', array("statusName"), "left")
                ->join('currency', 'incomingPayment.customCurrencyId= currency.currencyID', array('*'), 'left')
                ->where(array('incomingPayment.incomingPaymentID' => $paymentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getIncomePaymentsByIncomeID($incomeID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array("*"))
                ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID','incomeID'), 'left')
                ->join('customer', 'incomingPayment.customerID = customer.customerID', array("*"), "left")
                ->join('status', 'incomingPayment.incomingPaymentStatus= status.statusID', array("statusName"), "left")
                ->join('currency', 'incomingPayment.customCurrencyId= currency.currencyID', array('*'), 'left')
                ->where(array('incomingInvoicePayment.incomeID' => $incomeID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentsByPaymentIDForJE($paymentID, $forChequeBounced = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array("*"))
                ->join('customer', 'incomingPayment.customerID = customer.customerID', array("*"), "left")
                ->join('status', 'incomingPayment.incomingPaymentStatus= status.statusID', array("statusName"), "left")
                ->join('currency', 'incomingPayment.customCurrencyId= currency.currencyID', array('*'), 'left')
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('incomingPaymentMethodAmount'),'left')
                ->join('incomingPaymentMethodCheque','incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('incomingPaymentMethodChequeNumber','incomingPaymentMethodChequeAccNo'),'left')
                ->join('incomingPaymentMethodCash','incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('incomingPaymentMethodCashId','cashAccountID'),'left')
                ->join('incomingPaymentMethodCreditCard','incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('incomingPaymentMethodCreditReceiptNumber','cardAccountID'),'left')
                ->where(array('incomingPayment.incomingPaymentID' => $paymentID));
        $select->where->notEqualTo('incomingPayment.incomingPaymentStatus', 5);
        $select->where->notEqualTo('incomingPayment.incomingPaymentStatus', 10);

        if ($forChequeBounced) {
            $select->join('entity', "incomingPaymentMethod.entityID = entity.entityID", ['entityID'])
            ->where(['entity.deleted' => 0]);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentsByLocatioID($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array("*"))
                ->join('entity', 'incomingPayment.entityID = entity.entityID', array("deleted"), "left")
                ->where(array('incomingPayment.locationID' => $locationID/** , 'deleted' => 0* */));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getPaymentByID($payid)
    {
        $rowset = $this->tableGateway->select(array('incomingPaymentID' => $payid));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getAllPaymentDetailsByID($payid)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
//        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('*'));
//        $select->join('location', 'salesInvoice.locationID = location.locationID', array('*'));
        $select->join('paymentTerm', 'incomingPayment.paymentTermID = paymentTerm.paymentTermID', array('*'));
        $select->join('entity', 'incomingPayment.entityID = entity.entityID', array('createdBy','createdTimeStamp'),'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->where(array('incomingPaymentID' => $payid));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getPaymentByCode($payCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->join('entity', 'incomingPayment.entityID = entity.entityID', array('*'));
        $select->where(array('incomingPaymentCode' => $payCode, 'entity.deleted' => 0));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        return $rowset->current();
    }

//when save the payment using this function check wheather paymentCode exist or not
    public function checkPaymentByCode($paymentCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('incomingPayment')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "incomingPayment.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('incomingPayment.incomingPaymentCode' => $paymentCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function deletePayment($payid)
    {
        $rowset = $this->tableGateway->delete(array('paymentID' => $payid));
        return true;
    }

    public function savePayments(Payments $payments)
    {
        $paymentdata = array(
            'incomingPaymentCode' => $payments->incomingPaymentCode,
            'incomingPaymentDate' => $payments->incomingPaymentDate,
            'paymentTermID' => $payments->paymentTermID,
            'incomingPaymentAmount' => $payments->incomingPaymentAmount,
            'customerID' => $payments->customerID,
            'incomingPaymentDiscount' => $payments->incomingPaymentDiscount,
            'incomingPaymentMemo' => $payments->incomingPaymentMemo,
            'incomingPaymentType' => $payments->incomingPaymentType,
            'locationID' => $payments->locationID,
            'entityID' => $payments->entityID,
            'incomingPaymentCreditAmount' => $payments->incomingPaymentCreditAmount,
            'pos' => $payments->pos,
            'incomingPaymentBalance' => $payments->incomingPaymentBalance,
            'incomingPaymentPaidAmount' => $payments->incomingPaymentPaidAmount,
            'customCurrencyId' => $payments->customCurrencyId,
            'incomingPaymentCustomCurrencyRate' => $payments->incomingPaymentCustomCurrencyRate,
        );

        if ($this->tableGateway->insert($paymentdata)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function saveAdvancePayment($data, $cudata)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $db = $this->tableGateway->getAdapter()->getDriver()->getConnection();
        $insert = $sql->insert();
        $insert->into('incomingPayment')
                ->values($data);
        $statementPayment = $sql->getSqlStringForSqlObject($insert);

        $updateCustomerCredit = $sql->update();
        $updateCustomerCredit->table('customer')
                ->set($cudata)
                ->where(array('customerID' => $cudata['customerID']));
        $statementCustomerCredit = $sql->getSqlStringForSqlObject($updateCustomerCredit);

        $db->beginTransaction();
        try {
            $incomingPaymentID = $db->execute($statementPayment)->getGeneratedValue();
            $db->execute($statementCustomerCredit);
            $db->commit();
            return array('state' => true, 'data' => $incomingPaymentID);
        } catch (\Exception $e) {
            $db->rollBack();
            return array('state' => false);
        }
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $date
     * @return type
     */
    public function getDailyPaymentsDetailsByOnlyDate($date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->join('customer', 'payement.customerID = customer.customerID', array('customerName'))
                ->where(array('payement.date' => $date));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $customerID
     * @param type $date
     * @return type
     */
    public function getDailyPaymentsDetailsByDate($customerID, $date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->join('customer', 'payement.customerID = customer.customerID', array('customerName'))
                ->where(array('payement.customerID' => $customerID))
                ->where(array('payement.date' => $date));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }
        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * This file contains CustomerTransaction Report related Mysql functions
     * @param type $customerID
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getPaymentsDetailsByCusId($customerID, $fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->where(array('customerID' => $customerID))
        ->where->between('date', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

     public function getPaymentByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID'), 'left')
                ->join('salesInvoice', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID', array('salesInvoiceCode'), 'left')
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('*'),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array('statusName'),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('incomingPaymentDate' => 'DESC'))
                ->where(array('entity.deleted' => '0'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('incomingPaymentDate', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCustomerCumulativePaymentBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('customerID','cumulative_total' => new \Zend\Db\Sql\Expression('SUM(incomingPaymentAmount) - SUM(IFNULL(incomingPaymentCreditAmount,0))')))
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array(),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array(),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array(), 'left')
                ->where(array('entity.deleted' => '0'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('incomingPayment.incomingPaymentDate', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $select->where(array('incomingPayment.incomingPaymentType' => 'invoice'));
        $select->group(array('incomingPayment.customerID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCustomerCumulativeAdvancePaymentBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('customerID','cumulative_total' => new \Zend\Db\Sql\Expression('SUM(incomingPaymentAmount)')))
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array(),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array(),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array(), 'left')
                ->where(array('entity.deleted' => '0'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('incomingPayment.incomingPaymentDate', $fromDate);
        }else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);$resultSet = new ResultSet();
        }

        $select->where(array('incomingPayment.incomingPaymentType' => 'advance'));
        $select->group(array('incomingPayment.customerID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * This file contains Customer Balances Report related Mysql function
     * @param type $customerID
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getPaymentsBalancesData($customerID, $fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->columns(array('amountPay' => new Expression('SUM(amount)'),
                    'setledPay' => new Expression('SUM(amount)')))
                ->where(array('customerID' => $customerID))
        ->where->between('date', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * get payment summary data
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getPaymentsSummeryData($fromDate, $toDate, $sort, $paymentNumber, $locationNumber, $cancelled = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left')
                ->join('incomingPaymentMethodBankTransfer', 'incomingPaymentMethod.incomingPaymentMethodBankTransferId = incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId', array('*'), 'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left')
                ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('*'), 'left')
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                ->join('incomingPaymentMethodGiftCard', 'incomingPaymentMethod.incomingPaymentMethodGiftCardId = incomingPaymentMethodGiftCard.incomingPaymentMethodGiftCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLoyaltyCard', 'incomingPaymentMethod.incomingPaymentMethodLoyaltyCardId = incomingPaymentMethodLoyaltyCard.incomingPaymentMethodLoyaltyCardId', array('*'), 'left')
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('customerName', 'customerTitle'), 'left')
                ->join('entity', 'entity.entityID = incomingPayment.entityID', array('deleted'), 'left')
                ->join('location', 'incomingPayment.locationID = location.locationID', array('locationName'), 'left')
                ->order('locationID')
        ->where->between('incomingPaymentDate', $fromDate, $toDate)
        ->where->in('incomingPayment.locationID', $locationNumber);
        if ($cancelled) {
            $select->where(array('entity.deleted' => 1));
        } else {
            $select->where(array('entity.deleted' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }
        return $countArray;
    }

    public function getPaymentsSummeryDataForPaymentReport($fromDate, $toDate, $sort, $paymentNumber, $locationNumber, $cancelled = false, $accountId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left')
                ->join('incomingPaymentMethodBankTransfer', 'incomingPaymentMethod.incomingPaymentMethodBankTransferId = incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId', array('*'), 'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left')
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                ->join('incomingPaymentMethodGiftCard', 'incomingPaymentMethod.incomingPaymentMethodGiftCardId = incomingPaymentMethodGiftCard.incomingPaymentMethodGiftCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLoyaltyCard', 'incomingPayment.incomingPaymentID = incomingPaymentMethodLoyaltyCard.incomingPaymentID', array('incomingPaymentMethodLoyaltyCId' => new Expression('incomingPaymentMethodLoyaltyCard.incomingPaymentMethodLoyaltyCardId'),'customerLoyaltyID','collectedPoints','redeemedPoints','expired'), 'left')
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('customerName', 'customerTitle'), 'left')
                ->join('entity', 'entity.entityID = incomingPayment.entityID', array('deleted'), 'left')
                ->join(array('ip' => 'entity'), "incomingPayment.entityID = ip.entityID", ['*'], 'left')
                ->join(array('ipm' => 'entity'), "incomingPaymentMethod.entityID = ipm.entityID", ['*'], 'left')
                ->join('location', 'incomingPayment.locationID = location.locationID', array('locationName'), 'left')
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->order('incomingPayment.incomingPaymentID')
        ->where->between('incomingPaymentDate', $fromDate, $toDate)
        ->where->in('incomingPayment.locationID', $locationNumber)
        ->where->equalTo('ipm.deleted', 0);
        if ($cancelled) {
            $select->where(array('ip.deleted' => 1));
        } else {
            $select->where(array('ip.deleted' => 0));
        }

        if (!is_null($accountId)) {
            $select->join('journalEntry','journalEntry.journalEntryDocumentID = incomingPayment.incomingPaymentID', array('journalEntryID'),'left');
            $select->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left');
            $select->where(array('journalEntry.documentTypeID' => 7,'journalEntryAccounts.financeAccountsID' => $accountId));
            $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }
        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * get payment data for dash board graph
     * @param type $current
     * @param type $before
     * @return type
     */
    public function getPaymentGraphDataByDay($current, $before)
    {
        $days_array = $this->getDateArray($current, $before);

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('payement')
                ->columns(array(
                    $days_array[0] => new Expression("SUM(case when datediff(date, '" . $days_array[0] . "') = 0 then amount end)"),
                    $days_array[1] => new Expression("SUM(case when datediff(date, '" . $days_array[1] . "') = 0 then amount end)"),
                    $days_array[2] => new Expression("SUM(case when datediff(date, '" . $days_array[2] . "') = 0 then amount end)"),
                    $days_array[3] => new Expression("SUM(case when datediff(date, '" . $days_array[3] . "') = 0 then amount end)"),
                    $days_array[4] => new Expression("SUM(case when datediff(date, '" . $days_array[4] . "') = 0 then amount end)"),
                    $days_array[5] => new Expression("SUM(case when datediff(date, '" . $days_array[5] . "') = 0 then amount end)"),
                    $days_array[6] => new Expression("SUM(case when datediff(date, '" . $days_array[6] . "') = 0 then amount end)"),
                    $days_array[7] => new Expression("SUM(case when datediff(date, '" . $days_array[7] . "') = 0 then amount end)"),
                    $days_array[8] => new Expression("SUM(case when datediff(date, '" . $days_array[8] . "') = 0 then amount end)"),
                    $days_array[9] => new Expression("SUM(case when datediff(date, '" . $days_array[9] . "') = 0 then amount end)"),
                    $days_array[10] => new Expression("SUM(case when datediff(date, '" . $days_array[10] . "') = 0 then amount end)"),
                    $days_array[11] => new Expression("SUM(case when datediff(date, '" . $days_array[11] . "') = 0 then amount end)"),
                    $days_array[12] => new Expression("SUM(case when datediff(date, '" . $days_array[12] . "') = 0 then amount end)"),
                    $days_array[13] => new Expression("SUM(case when datediff(date, '" . $days_array[13] . "') = 0 then amount end)"),
                ))
        ->where->between('date', $before, $current);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $yesterday
     * @return type
     */
    public function getYesterdayPaymentData($yesterday)
    {

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('payement')
                ->columns(array('date'))
                ->JOIN('invoicePayment', 'payement.paymentID = invoicePayment.paymentID', array('TotalAmount' => new Expression('SUM(invoicePayment.amount)')), 'inner')
                ->JOIN('paymentMethods', 'invoicePayment.paymentMethod = paymentMethods.id', array('Method' => new Expression('paymentMethods.method')), 'left')
                ->group('Method')
                ->where('Method IS NOT NULL')
                ->where(array('payement.date' => $yesterday));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $current
     * @param type $before
     * @return type
     */
    public function getDateArray($current, $before)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($before);
        $endTime = strtotime($current);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();

        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    public function getPaymentIdsList()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('paymentID' => 'incomingPaymentID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function returns CashInflow for daily monthly and yearly
     */
    function getCashInFlowData($fromData, $toData, $format = NULL)
    {
        $groupArray = array();
        if ($format == 'year') {
            $groupArray = array('Year');
        } else if ($format == 'month') {
            $groupArray = array('Month', 'Year');
        } else {
            $groupArray = array('Date', 'Month', 'Year');
        }
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('incomingPayment')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%M %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%d")'),
                    'Year' => new Expression('Year(incomingPaymentDate)'),
                    'TotalCashFlow' => new Expression('SUM(incomingPaymentAmount)')
                ))
                ->join('entity', 'entity.entityID = incomingPayment.entityID', '*', 'left')
                ->group($groupArray)
                ->order(array('incomingPayment.incomingPaymentDate'))
                ->where(array('entity.deleted' => 0))
        ->where->between('incomingPaymentDate', $fromData, $toData);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this function returns cashinflow for current date month and year
     */
    public function getCurrentCashInFlowBy($type)
    {
        $whereClause;
        if ($type == 'year') {
            $whereClause = date('Y');
        } else if ($type == 'month') {
            $whereClause = date('Y-m');
        } else {
            $whereClause = date('Y-m-d');
        }
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('incomingPayment')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%m")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%m %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`incomingPaymentDate`, "%d")'),
                    'Year' => new Expression('Year(incomingPaymentDate)'),
                    'TotalCashInFlow' => new Expression('SUM(incomingPaymentAmount)')
                ))
                //added left join with entity table @Sandun Dissanayake
                ->join('entity', 'entity.entityID = incomingPayment.entityID', '*', 'left')
                ->where(array('entity.deleted' => 0))
        ->where->like('incomingPaymentDate', '%' . $whereClause . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search Customer Payments For Dropdown
     * @param string $locationID
     * @param string $searchKey
     * @return array limited maximum 50 rows
     */
    public function searchCustomerPaymentsForDropDown($locationID, $searchKey, $type = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array("*"))
                ->join('entity', 'incomingPayment.entityID = entity.entityID', array("deleted"), "left");
        if ($type == 'cusPay') {
            $select->where(array('incomingPayment.pos' => 0));
        } else if ($type == 'POS') {
            $select->where(array('incomingPayment.pos' => 1));
        }
        if ($locationID) {
            $select->where(array('incomingPayment.locationID' => $locationID));
        }
        $select->where->like('incomingPaymentCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * @author Madawa Chandrarathne <madawa@thinkcube.com>
     *
     * Search customer cheque payments
     * @param String $locationID
     * @param String $searchKey
     * @return Result
     */
    public function searchCustomerChequePayments($locationID, $searchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array("*"))
                ->join('paymentMethodsNumbers', 'incomingPayment.incomingPaymentID=paymentMethodsNumbers.paymentID', array('paymentMethodID', 'paymentMethodReferenceNumber', 'paymentMethodBank'))
        ->where->equalTo('incomingPayment.locationID', $locationID)
        ->where->equalTo('paymentMethodsNumbers.paymentMethodID', 2)->like('incomingPaymentCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
     * Get Location wise Cash Payments
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $locationID
     * @return type
     */
    public function getCashPaymentsByLocatioID($locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(['*'])
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', ['incomingPaymentMethodId', 'incomingPaymentMethodCashId', 'incomingPaymentMethodAmount'], 'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', ['*'], 'inner')
                ->join('entity', 'incomingPayment.entityID = entity.entityID', ['deleted'], "left")
                ->where(['incomingPayment.locationID' => $locationID, 'deleted' => 0]);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * Get payment details by payment id
     * @param int $paymentId
     * @return mixed
     */
    public function getPaymentData($paymentId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(['*'])
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', ['incomingPaymentMethodId', 'incomingPaymentMethodCashId', 'incomingPaymentMethodAmount'], 'left')
                ->join(array('ipm' => 'entity'), "incomingPaymentMethod.entityID = ipm.entityID", ['ipm.entityID' => 'entityID'])
                ->join(array('ipe' => 'entity'), "incomingPayment.entityID = ipe.entityID",  ['ipe.entityID' => 'entityID'])
                ->where(['incomingPayment.incomingPaymentID' => $paymentId])
                ->where(['ipe.deleted' => 0])
                ->where(['ipm.deleted' => 0]);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    // get dln related invoice payments by quotation id
    public function getDeliveryNoteRelatedInvoicePaymentsDataByQuotationId($quotationID = null, $soID = null, $deliveryNoteID = null, $invoiceID = null, $returnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('*'))
            ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID'), 'left')
            ->join('salesInvoice', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID', array('*'), 'left');
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
        $select->join('deliveryNote', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
        $select->join('salesOrders', 'deliveryNote.salesOrderID = salesOrders.soID', array('*'),'left');
        $select->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->where(array('salesOrders.quotationID' => $quotationID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($soID)) {
            $select->where(array('salesOrders.soID' => $soID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($deliveryNoteID)) {
            $select->where(array('deliveryNote.deliveryNoteID' => $deliveryNoteID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($invoiceID)) {
            $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID));   
        }
        if (!is_null($returnID)) {
            $select->join('deliveryNoteProduct', "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array('*'), 'left');
            $select->join('salesReturnProduct', "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array('*'), 'left');
            $select->join('salesReturn', "salesReturn.salesReturnID = salesReturnProduct.salesReturnID", array('*'), 'left');
            $select->where(array('salesReturn.salesReturnID' => $returnID));
        }
        $select->group("incomingPayment.incomingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    public function getInvoicePaymentsDataByInvoiceId($invoiceID = null, $endDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('*'))
            ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID', 'paidTotalAmount' => new \Zend\Db\Sql\Expression('SUM(incomingInvoicePaymentAmount)')), 'left')
            ->join('salesInvoice', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID', array('*'), 'left');
        $select->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($invoiceID)) {
            $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID, 'deleted' => 0));   
        }
        $select->where->lessThanOrEqualTo('incomingPaymentDate', $endDate);
        $select->group("salesInvoice.salesInvoiceID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get sales order related invoice payments by quotation id
    public function getSalesOrderRelatedInvoicePaymentsDataByQuotationId($quotationID = null, $soID = null, $creditNoteID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('*'))
            ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID'), 'left')
            ->join('salesInvoice', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID', array('*'), 'left');
        $select->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->join('salesOrders', 'salesInvoice.salesOrderID = salesOrders.soID', array('*'),'left');
            $select->where(array('salesOrders.quotationID' => $quotationID));
        }
        if (!is_null($soID)) {
            $select->join('salesOrders', 'salesInvoice.salesOrderID = salesOrders.soID', array('*'),'left');
            $select->where(array('salesOrders.soID' => $soID));
        }
        if (!is_null($creditNoteID)) {
            $select->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
            $select->where(array('creditNote.creditNoteID' => $creditNoteID));
        }
        $select->group("incomingPayment.incomingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get quotation related invoice payments by quotation id
    public function getQuotationRelatedInvoicePaymentsDataByQuotationId($quotationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('*'))
            ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID'), 'left')
            ->join('salesInvoice', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID', array('*'), 'left');
        $select->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesInvoice.quotationID' => $quotationID));
        $select->group("incomingPayment.incomingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

     // get credit note related invoice payments by credit note id
    public function getCreditNoteRelatedInvoicePaymentsDataByCreditNoteId($creditNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('*'))
            ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID'), 'left')
            ->join('salesInvoice', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID', array('*'), 'left');
        $select->join('salesOrders', 'salesInvoice.salesOrderID = salesOrders.soID', array('*'),'left');
        $select->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->where(array('salesOrders.quotationID' => $quotationID));
        }
        if (!is_null($soID)) {
            $select->where(array('salesOrders.soID' => $soID));
        }
        $select->group("incomingPayment.incomingPaymentID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;

    }
    
    public function getCustomerCumulativePaymentBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('customerID','incomingPaymentAmount','incomingPaymentPaidAmount','incomingPaymentBalance','incomingPaymentCreditAmount','incomingPaymentID','incomingPaymentType'))
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array(),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'incomingPayment.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $select->where(array('incomingPayment.incomingPaymentType' => 'invoice'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }


    public function getCustomerCumulativeIncomePaymentBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('customerID','incomingPaymentAmount','incomingPaymentPaidAmount','incomingPaymentBalance','incomingPaymentCreditAmount','incomingPaymentID','incomingPaymentType'))
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array(),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'incomingPayment.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0', 'incomingPayment.incomingPaymentType' => 'income'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $select->where(array('incomingPayment.incomingPaymentType' => 'income'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCustomerCumulativeIncomePaymentBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('customerID','cumulative_total' => new \Zend\Db\Sql\Expression('SUM(incomingPaymentAmount) - SUM(IFNULL(incomingPaymentCreditAmount,0))'),'incomingPaymentAmount','incomingPaymentPaidAmount','incomingPaymentBalance','incomingPaymentCreditAmount','incomingPaymentID','incomingPaymentType'))
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array(),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'incomingPayment.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0', 'incomingPayment.incomingPaymentType' => 'income'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $select->where(array('incomingPayment.incomingPaymentType' => 'income'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCustomerCumulativeAdvancePaymentBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('customerID','incomingPaymentAmount','incomingPaymentID','incomingPaymentType'))
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array(),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'incomingPayment.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        }else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);$resultSet = new ResultSet();
        }

        $select->where(array('incomingPayment.incomingPaymentType' => 'advance'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getPaymentByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID'), 'left')
                ->join('salesInvoice', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID', array('salesInvoiceCode'), 'left')
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('*'),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'incomingPayment.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getIncomePaymentByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID'), 'left')
                ->join('income', 'income.incomeID = incomingInvoicePayment.incomeID', array('incomeCode'), 'left')
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('*'),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'incomingPayment.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0', 'incomingPayment.incomingPaymentType' => 'income'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getIncomePaymentByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('incomingInvoicePayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('salesInvoiceID'), 'left')
                ->join('income', 'income.incomeID = incomingInvoicePayment.incomeID', array('incomeCode'), 'left')
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('*'),'left')
                ->join('status', 'incomingPayment.incomingPaymentStatus = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'incomingPayment.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0', 'incomingPayment.incomingPaymentType' => 'income'));
                $select->where->in('incomingPayment.incomingPaymentStatus',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('incomingPayment.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

     public function getCreditPaymentsSummeryDataForPaymentReport($fromDate, $toDate, $sort, $paymentNumber, $locationNumber, $cancelled = false, $accountId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('customerName', 'customerTitle'), 'left')
                ->join('entity', 'entity.entityID = incomingPayment.entityID', array('deleted'), 'left')
                ->join(array('ip' => 'entity'), "incomingPayment.entityID = ip.entityID", ['*'], 'left')
                ->join('location', 'incomingPayment.locationID = location.locationID', array('locationName'), 'left')
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->order('incomingPayment.incomingPaymentID')
        ->where->between('incomingPaymentDate', $fromDate, $toDate)
        ->where->in('incomingPayment.locationID', $locationNumber);
        if ($cancelled) {
            $select->where(array('ip.deleted' => 1));
        } else {
            $select->where(array('ip.deleted' => 0));
        }

        if (!is_null($accountId)) {
            $select->join('journalEntry','journalEntry.journalEntryDocumentID = incomingPayment.incomingPaymentID', array('journalEntryID'),'left');
            $select->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left');
            $select->where(array('journalEntry.documentTypeID' => 7,'journalEntryAccounts.financeAccountsID' => $accountId));
            $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }
        return $countArray;
    }


    /**
     * get Aged Customer data for done payments
     * @param Array $cusIds
     * @param Array $endDate
     * @param Date $cusCategory
     * @return Array $agedAnalysisData
     */
    public function getAgedCustomerForDonePayments($cusIds, $endDate, $isAllCustomers = false, $cusCategory = false)
    {
        $agedAnalysisData = [];
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('incomingPaymentDate',
            'incomingPaymentCode', 'incomingPaymentID', 'customerID','incomingPaymentCreditAmount',
            "Current" => new Expression("case when datediff(incomingPaymentDate, '" . $endDate . "') = 0 then (incomingPaymentPaidAmount) end "),
            "WithIn7Days" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) BETWEEN 0 AND 7 then (incomingPaymentPaidAmount) end"),
            "WithIn14Days" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) BETWEEN 8 AND 14 then (incomingPaymentPaidAmount) end "),
            "WithIn30Days" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) BETWEEN 15 AND 30 then (incomingPaymentPaidAmount) end"),
            "Over30" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) > 30 then (incomingPaymentPaidAmount) end")
        ));
        $select->join('customer', 'incomingPayment.customerID = customer.customerID', array('customerName', 'customerShortName', 'customerTitle', 'customerCode'), 'left');
        $select->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted'), 'left');
        
        $select->where->lessThanOrEqualTo('incomingPaymentDate', $endDate);
        if ($cusIds && !$isAllCustomers) {
            $select->where->in('incomingPayment.customerID', $cusIds);
        }
        $select->where->notEqualTo('incomingPayment.customerID', 0);
        $select->where(array('incomingPayment.incomingPaymentStatus'=> 4));
        $select->where(array('deleted' => '0'));
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $agedAnalysisData[] = $val;
        }

        return $agedAnalysisData;
    }

    public function getAgedCusForDonePaymentAdvanceData($cusIds, $endDate, $isAllCustomers = false, $cusCategory = false)
    {
        $agedAnalysisData = [];
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('incomingPaymentDate',
            'incomingPaymentCode', 'incomingPaymentID', 'customerID','incomingPaymentCreditAmount',
            "Current" => new Expression("case when datediff(incomingPaymentDate, '" . $endDate . "') = 0 then (incomingPaymentPaidAmount) end "),
            "WithIn30Days" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) BETWEEN 0 AND 30 then (incomingPaymentPaidAmount) end"),
            "WithIn60Days" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) BETWEEN 31 AND 60 then (incomingPaymentPaidAmount) end"),
            "WithIn90Days" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) BETWEEN 61 AND 90 then (incomingPaymentPaidAmount) end"),
            "WithIn180Days" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) BETWEEN 91 AND 180 then (incomingPaymentPaidAmount) end"),
            "WithIn270Days" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) BETWEEN 181 AND 270 then (incomingPaymentPaidAmount) end"),
            "WithIn365Days" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) BETWEEN 271 AND 365 then (incomingPaymentPaidAmount) end"),
            "Over365" => new Expression("case when datediff('" . $endDate . "', incomingPaymentDate) > 365 then (incomingPaymentPaidAmount) end")
        ));
        $select->join('customer', 'incomingPayment.customerID = customer.customerID', array('customerName', 'customerShortName', 'customerTitle', 'customerCode'), 'left');
        $select->join('entity', 'incomingPayment.entityID = entity.entityID', array('deleted'), 'left');

        $select->where->lessThanOrEqualTo('incomingPaymentDate', $endDate);
        if ($cusIds && !$isAllCustomers) {
            $select->where->in('incomingPayment.customerID', $cusIds);
        }
        $select->where->notEqualTo('incomingPayment.customerID', 0);
        $select->where(array('incomingPayment.incomingPaymentStatus'=> 4));
        $select->where(array('deleted' => '0'));
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $agedAnalysisData[] = $val;
        }

        return $agedAnalysisData;
    }

    public function getCreditPaymentsByPaymentIDForJE($paymentID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array("*"))
                ->join('customer', 'incomingPayment.customerID = customer.customerID', array("*"), "left")
                ->join('status', 'incomingPayment.incomingPaymentStatus= status.statusID', array("statusName"), "left")
                ->join('currency', 'incomingPayment.customCurrencyId= currency.currencyID', array('*'), 'left')
                ->where(array('incomingPayment.incomingPaymentID' => $paymentID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    /**
    * this function use to get sales payment total value for daily, monthly and annual 
    * @param date $currentDate
    * @param date $startDate
    * @param string $type
    * return mix
    **/
    public function getPaymentValueByGivenDateRange($currentDate, $startDate, $type)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('totalPayments' => new Expression('SUM(incomingPaymentAmount)')));
        $select->where(array('incomingPaymentType' => 'invoice'));
        $select->where->notEqualto('incomingPaymentStatus', 5);  // 5 mean deleted
        
        if ($type == 'daily') {
            $select->where->equalTo('incomingPaymentDate', $currentDate);
        } else {
            $select->where->between('incomingPaymentDate', $startDate, $currentDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getPaymentDetailsForDonut($currentDate, $startDate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('*'));
        $select->where(array('incomingPaymentType' => 'invoice'));
        $select->where->notEqualto('incomingPaymentStatus', 5);  // 5 mean deleted
        $select->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left');
        if ($startDate != null) {
            $select->where->between('incomingPaymentDate', $startDate, $currentDate);
        } else {
            $select->where(array('incomingPaymentDate', $currentDate));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;      
    }

    public function getPaymentCountByGivenDate($currentDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('totalPaymentCount' => new Expression('COUNT(incomingPaymentID)')));
        $select->where(array('incomingPaymentType' => 'invoice'));
        $select->where->notEqualto('incomingPaymentStatus', 5);  // 5 mean deleted
        $select->where->equalTo('incomingPaymentDate', $currentDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;   
    }

    public function getDateWisePaymentDetails($startDate, $endDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('totalPaymentCount' => new Expression('SUM(incomingPaymentAmount)'), 'incomingPaymentDate'));
        $select->where(array('incomingPaymentType' => 'invoice'));
        $select->where->notEqualto('incomingPaymentStatus', 5);  // 5 mean deleted
        $select->where->between('incomingPaymentDate', $startDate, $endDate);
        $select->group('incomingPaymentDate');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;   
    }

    public function getPaymentsDetailsForCashFlow($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left')
                ->join('journalEntry','journalEntry.journalEntryDocumentID = incomingPayment.incomingPaymentID', array('journalEntryID'),'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left')
                // ->join(array('cashJEAc' => 'journalEntryAccounts'), "cashJEAc.journalEntryID = journalEntry.journalEntryID AND cashJEAc.financeAccountsID = incomingPaymentMethodCash.cashAccountID", ['cashDebit' => new Expression('cashJEAc.journalEntryAccountsDebitAmount'),'cashCredit' => new Expression('cashJEAc.journalEntryAccountsCreditAmount')], 'left')
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left')
                // ->join(array('chequeJEAc' => 'journalEntryAccounts'), "chequeJEAc.journalEntryID = journalEntry.journalEntryID AND chequeJEAc.financeAccountsID = incomingPaymentMethodCheque.incomingPaymentMethodChequeAccNo", ['chequeDebit' => new Expression('chequeJEAc.journalEntryAccountsDebitAmount'),'chequeCredit' => new Expression('chequeJEAc.journalEntryAccountsCreditAmount')], 'left')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                // ->join(array('creditJEAc' => 'journalEntryAccounts'), "creditJEAc.journalEntryID = journalEntry.journalEntryID AND creditJEAc.financeAccountsID = incomingPaymentMethodCreditCard.cardAccountID", ['cardDebit' => new Expression('creditJEAc.journalEntryAccountsDebitAmount'),'cardCredit' => new Expression('creditJEAc.journalEntryAccountsCreditAmount')], 'left')
                ->join('customer', 'customer.customerID = incomingPayment.customerID', array('customerName', 'customerTitle'), 'left')
                ->join('entity', 'entity.entityID = incomingPayment.entityID', array('deleted'), 'left')
                ->join(array('ip' => 'entity'), "incomingPayment.entityID = ip.entityID", ['*'], 'left')
                ->join(array('ipm' => 'entity'), "incomingPaymentMethod.entityID = ipm.entityID", ['*'], 'left')
                ->join('location', 'incomingPayment.locationID = location.locationID', array('locationName'), 'left')
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->order('incomingPayment.incomingPaymentID')
        ->where->between('incomingPaymentDate', $fromDate, $toDate)
        ->where->equalTo('ipm.deleted', 0);
        $select->where(array('journalEntry.documentTypeID' => 7));
        $select->where(array('ip.deleted' => 0))
                ->where->nest()->isNotNull('cashAccountID')
                ->OR->isNotNull('cardAccountID')
                ->OR->notEqualto('incomingPaymentMethodChequeAccNo',0);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $countArray = Array();
        foreach ($results as $val) {
            $countArray[] = $val;
        }
        return $countArray;
    }


    public function getTotalPaymentsByDateRangeAndLocationID($startDate, $endDate, $locationID, $isPos = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        if ($isPos) {
            $select->columns(array('totalPayment' => new Expression('SUM(incomingPaymentPaidAmount - incomingPaymentBalance)'), 'incomingPaymentDate'));
        } else {
            $select->columns(array('totalPayment' => new Expression('SUM(incomingPaymentPaidAmount)'), 'incomingPaymentDate'));
        }
        
        $select->where(array('incomingPaymentType' => 'invoice'));
        if ($isPos) {
            $select->where->notEqualto('incomingPayment.pos', 0);
        } else {
            $select->where->notEqualto('incomingPayment.pos', 1);

        }
        $select->where->notEqualto('incomingPaymentStatus', 5);  // 5 mean deleted
        $select->where->between('incomingPaymentDate', $startDate, $endDate);
        $select->where(['incomingPayment.locationID' => $locationID]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();   
    }

    public function getPaymentsSummeryDataForSalesDashboard($fromDate, $toDate, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment')
                ->columns(array('*'))
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left')
                ->join('entity', 'entity.entityID = incomingPayment.entityID', array('deleted'), 'left')
        ->where->between('incomingPaymentDate', $fromDate, $toDate);
        $select->where(array('incomingPaymentType' => 'invoice'));
        $select->where->notEqualto('incomingPaymentStatus', 5);
        $select->where(['incomingPayment.locationID' => $locationID, 'deleted' => 0]);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getTotalLoyaltyByDateRangeAndLocationID($startDate, $endDate, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('incomingPaymentDate'))
               ->join('incomingPaymentMethodLoyaltyCard', 'incomingPayment.incomingPaymentID = incomingPaymentMethodLoyaltyCard.incomingPaymentID', array('totalCollectedPoints' => new Expression('SUM(collectedPoints)'),'totalRedeemedPoints' => new Expression('SUM(redeemedPoints)')));
        $select->where->notEqualto('incomingPaymentStatus', 5);  // 5 mean deleted
        $select->where->between('incomingPaymentDate', $startDate, $endDate);
        $select->where(['incomingPayment.locationID' => $locationID]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();   
    }

    public function getTotalRecivedChequesByDateRangeAndLocationID($startDate, $endDate, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('incomingPaymentDate'))
               ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('incomingPaymentMethodChequeId','incomingPaymentMethodAmount'));
        $select->where->notEqualto('incomingPaymentStatus', 5); // 5 mean deleted
        $select->where->notEqualto('incomingPaymentMethod.incomingPaymentMethodChequeId', 0);
        $select->where->between('incomingPaymentDate', $startDate, $endDate);
        $select->where(['incomingPayment.locationID' => $locationID]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;   
    }

    public function getAgedCustomerPayment($paymentDate = null, $chequeDate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('incomingPayment');
        $select->columns(array('incomingPaymentDate'))
               ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('incomingPaymentMethodChequeId','incomingPaymentMethodAmount'))
               ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('incomingPaymentMethodChequeNumber','postdatedChequeDate'));
        $select->where->notEqualto('incomingPaymentStatus', 5); // 5 mean deleted
        $select->where->notEqualto('incomingPaymentMethod.incomingPaymentMethodChequeId', 0);
        if (!is_null($chequeDate)) {
            $select->where->greaterThanOrEqualTo('incomingPaymentMethodCheque.postdatedChequeDate',$chequeDate);
        } else {
            $select->where->greaterThanOrEqualTo('incomingPayment.incomingPaymentDate',$paymentDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
}
