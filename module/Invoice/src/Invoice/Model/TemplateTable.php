<?php

/**
 * @author Damith <damith@thinkcube.com>
 * This file contains Template Table Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class TemplateTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function updateTemplate($data, $templateID)
    {
        try {
            $this->tableGateway->update($data, ['templateID' => $templateID]);
            return true;
        } catch (\Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    }

    public function getTemplateFooterDataByName($templateName)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->columns(array(
            "templateID" => "footer_id",
            "templateName" => "templateName",
            "footerEnabled" => "footerEnabled"
        ));

        $select->from(array("t" => "templates"));

        $select->join(array("f" => "footers"), "t.footer_id = f.id", array(
            "footer" => "content",
        ));

        $select->where("t.templateName = \"" . $templateName . "\"");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        $row = $rowset->current();
        if ($row) {
            return $row;
        }
        else {
            return NULL;
        }
    }

    public function getTemplateFooterDataByID($tmplateName, $footer_id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from("templates");
        $select->columns(array(
            "templateID" => "templateID",
            "templateName" => "templateName",
            "footer" => new \Zend\Db\Sql\Expression('(SELECT content FROM footers WHERE id=' . $footer_id . ')'),
            "footerEnabled" => "footerEnabled"
        ));
        $select->where('templateName ="' . $tmplateName . '"');

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        $row = $rowset->current();
        if ($row) {
            return $row;
        }
        else {
            return NULL;
        }
    }

}
