<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains returns product Table Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

class ReturnsProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();

        return $rowset;
    }

    public function saveReturnProducts(ReturnsProduct $returnProduct)
    {
        $data = array(
            'salesReturnID' => $returnProduct->salesReturnID,
            'deliveryNoteProductID' => $returnProduct->deliveryNoteProductID,
            'productID' => $returnProduct->productID,
            'salesReturnProductPrice' => $returnProduct->salesReturnProductPrice,
            'salesReturnProductDiscount' => $returnProduct->salesReturnProductDiscount,
            'salesReturnProductDiscountType' => $returnProduct->salesReturnProductDiscountType,
            'salesReturnProductQuantity' => $returnProduct->salesReturnProductQuantity,
            'salesReturnProductTotal' => $returnProduct->salesReturnProductTotal,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getReturnProductAndSubProductDataByDeliveryNoteProductID($deliveryNoteProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesReturnProduct")
                ->columns(array("*"))
                ->join('salesReturnSubProduct', 'salesReturnSubProduct.salesReturnProductID=salesReturnProduct.salesReturnProductID', array('salesReturnSubProductID', 'productBatchID', 'productSerialID', 'salesReturnSubProductQuantity'), 'left')
                ->join('salesReturn', 'salesReturnProduct.salesReturnID=salesReturn.salesReturnID', array('*'), 'left')
                ->join('entity', 'salesReturn.entityID=entity.entityID', array('*'), 'left')
                ->where(array('salesReturnProduct.deliveryNoteProductID' => $deliveryNoteProductID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;

//        $result = $this->tableGateway->select(array('deliveryNoteProductID' => $deliveryNoteProductID));
//        return $result;
    }

    public function getAllReturnProductDetailsByReturnID($retunrID, $directRetunFlag)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesReturnProduct');
        $select->join('product', 'salesReturnProduct.productID = product.productID', array('*',));
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        if ($directRetunFlag != "1") {
            $select->join('deliveryNoteProduct', 'salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID', array('*',), "left");
            $select->join('deliveryNote', 'deliveryNoteProduct.deliveryNoteID = deliveryNote.deliveryNoteID', array('*',), "left");
        }
        $select->where(array('productUomBase' => '1'));
        $select->where(array('salesReturnProduct.salesReturnID' => $retunrID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getSalesReturnProductsByDelvieryNoteID($deliveryID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesReturnProduct")
                ->columns(array("*"))
                ->join("deliveryNoteProduct", "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array("*"), "left")
                ->where(array("deliveryNoteProduct.deliveryNoteID" => $deliveryID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

}
