<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodBankTransfer related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class IncomingPaymentMethodBankTransferTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveBankTransferPayment(IncomingPaymentMethodBankTransfer $incomingPaymentMethodBankTransfer)
    {
        $cashData = array(
            'incomingPaymentMethodBankTransferBankId' => $incomingPaymentMethodBankTransfer->incomingPaymentMethodBankTransferBankId,
            'incomingPaymentMethodBankTransferAccountId' => $incomingPaymentMethodBankTransfer->incomingPaymentMethodBankTransferAccountId,
            'incomingPaymentMethodBankTransferCustomerBankName' => $incomingPaymentMethodBankTransfer->incomingPaymentMethodBankTransferCustomerBankName,
            'incomingPaymentMethodBankTransferCustomerAccountNumber' => $incomingPaymentMethodBankTransfer->incomingPaymentMethodBankTransferCustomerAccountNumber,
            'incomingPaymentMethodBankTransferRef' => $incomingPaymentMethodBankTransfer->incomingPaymentMethodBankTransferRef,
        );
        if ($this->tableGateway->insert($cashData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getBankTransferDataByBankTransferId($incomingPaymentMethodBankTransferId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodBankTransfer')
                ->columns(array('*'))
                ->where(array('incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId' => $incomingPaymentMethodBankTransferId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     * 
     * Get incoming bank transfers by account id.
     * @param string $accountId
     * @return mixed
     */
    public function getBankTransfersByAccountId($accountId,$recStatus = null,$startDate=null,$endDate=null){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodBankTransfer')
                ->columns(array('*'))
                ->join('incomingPaymentMethod','incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId = incomingPaymentMethod.incomingPaymentMethodBankTransferId',array('incomingPaymentMethodAmount','incomingPaymentId'))
                ->join('incomingPayment','incomingPaymentMethod.incomingPaymentId=incomingPayment.incomingPaymentID',array('incomingPaymentDate','incomingPaymentCode','entityID'))
                ->join('entity','incomingPayment.entityID = entity.entityID',array('deleted'))
                ->where->equalTo('incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferAccountId',$accountId)
                ->where->equalTo('entity.deleted', 0);
        if(!is_null($recStatus)){
                $select->where->equalTo('incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferReconciliationStatus',$recStatus);
        }
        if($startDate && !$endDate){
            $select->where->greaterThan('incomingPayment.incomingPaymentDate', $startDate);            
        }
        if($startDate && $endDate){
            $select->where->between('incomingPayment.incomingPaymentDate', $startDate, $endDate);            
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    
    public function getTotalIncomingBankTransfersByAccountId($accountId,$recStatus = null,$startDate=null,$endDate=null){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodBankTransfer')
                ->columns(array('*'))
                ->join('incomingPaymentMethod','incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId = incomingPaymentMethod.incomingPaymentMethodBankTransferId',array('incomingPaymentMethodAmount','incomingPaymentId','totalTransferInAmount' => new Expression('SUM(incomingPaymentMethodAmount)')))
                ->join('incomingPayment','incomingPaymentMethod.incomingPaymentId=incomingPayment.incomingPaymentID',array('incomingPaymentDate','incomingPaymentCode','entityID'))
                ->join('entity','incomingPayment.entityID = entity.entityID',array('deleted'))
                ->where->equalTo('incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferAccountId',$accountId)
                ->where->equalTo('entity.deleted', 0);
        // if(!is_null($recStatus)){
        //         $select->where->equalTo('incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferReconciliationStatus',$recStatus);
        // }
        if($startDate && !$endDate){
            $select->where->greaterThan('incomingPayment.incomingPaymentDate', $startDate);            
        }
        if($startDate && $endDate){
            $select->where->between('incomingPayment.incomingPaymentDate', $startDate, $endDate);            
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     * 
     * Get incoming bank transfer by bankTransfer id.
     * @param string $bankTransferId
     * @return mixed
     */
    public function getBankTransferByBankTransferId($bankTransferId){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('incomingPaymentMethodBankTransfer')
                ->columns(array('*'))
                ->join('incomingPaymentMethod','incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId = incomingPaymentMethod.incomingPaymentMethodBankTransferId',array('incomingPaymentMethodAmount','incomingPaymentId'))
                ->join('incomingPayment','incomingPaymentMethod.incomingPaymentId=incomingPayment.incomingPaymentID',array('incomingPaymentDate','entityID'))
                ->join('entity','incomingPayment.entityID = entity.entityID',array('deleted'))
                ->where->equalTo(array('incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId' => $bankTransferId))
                ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
    
    /**
     * @author Madawa Chandraratne <madawa@thinkcube.com>
     * 
     * Update bank transfer
     * @param array $data
     * @param string $bankTransferId
     * @return boolean
     */
    public function updateBankTransfer($data,$bankTransferId)
    {        
        if($bankTransferId){
            if($this->tableGateway->update($data, array('incomingPaymentMethodBankTransferId'=>$bankTransferId))){
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}
