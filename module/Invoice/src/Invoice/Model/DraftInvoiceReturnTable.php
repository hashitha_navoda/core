<?php

/**
 * @author ashan     <ashan@thinkcube.com>
 * This file contains creditc note Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Between;

class DraftInvoiceReturnTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('draftInvoiceReturns')
                    ->columns(array('*'))
                    ->join('customer', 'draftInvoiceReturns.customerID = customer.customerID', array('customerName', 'customerCode'), 'left')
                    ->join('currency', 'draftInvoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->order(array('draftInvoiceReturnID' => 'DESC'))
                    ->where(array('locationID' => $locationID));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('draftInvoiceReturns');
        $select->where(array('locationID' => $locationID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function checkInvoiceReturnByCode($creditNoteCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('draftInvoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "draftInvoiceReturns.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('draftInvoiceReturns.invoiceReturnCode' => $creditNoteCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function saveInvoiceReturn(DraftInvoiceReturn $invoiceReturn)
    {
        $data = array(
            'invoiceReturnCode' => $invoiceReturn->invoiceReturnCode,
            'customerID' => $invoiceReturn->customerID,
            'invoiceReturnDate' => $invoiceReturn->invoiceReturnDate,
            'invoiceReturnTotal' => $invoiceReturn->invoiceReturnTotal,
            'locationID' => $invoiceReturn->locationID,
            'invoiceID' => $invoiceReturn->invoiceID,
            'invoiceReturnComment' => $invoiceReturn->invoiceReturnComment,
            'statusID' => $invoiceReturn->statusID,
            'customCurrencyId' => $invoiceReturn->customCurrencyId,
            'invoiceReturnCustomCurrencyRate' => $invoiceReturn->invoiceReturnCustomCurrencyRate,
            'entityID' => $invoiceReturn->entityID,
            'invoiceReturnDiscountType' => $invoiceReturn->invoiceReturnDiscountType,
            'invoiceReturnTotalDiscount' => $invoiceReturn->invoiceReturnTotalDiscount,
            'promotionID' => $invoiceReturn->promotionID,
            'invoiceReturnPromotionDiscount' => $invoiceReturn->invoiceReturnPromotionDiscount,
            'invHashValue' => $invoiceReturn->invHashValue,
            'actInvData' => $invoiceReturn->actInvData,
            'isInvApproved' => $invoiceReturn->isInvApproved,
            'selectedWfID' => $invoiceReturn->selectedWfID,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    

    public function getInvoiceReturnByCustomerIDAndLocationID($customerID, $locationID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('draftInvoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "draftInvoiceReturns.entityID = e.entityID")
                    ->join('customer', "draftInvoiceReturns.customerID = customer.customerID")
                    ->join('currency', 'draftInvoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    // ->where(array('e.deleted' => '0'))
                    ->where(array('draftInvoiceReturns.customerID' => $customerID, 'draftInvoiceReturns.locationID' => $locationID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getInvoiceReturnByInvoiceReturnID($draftInvoiceReturnID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('draftInvoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "draftInvoiceReturns.entityID = e.entityID")
                    ->join('customer', "draftInvoiceReturns.customerID = customer.customerID", array('*'), 'left')
                    ->join('currency', 'draftInvoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    // ->where(array('e.deleted' => '0'))
                    ->where(array('draftInvoiceReturns.draftInvoiceReturnID' => $draftInvoiceReturnID,));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getOpenInvoiceReturnByInvoiceReturnID()
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('draftInvoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "draftInvoiceReturns.entityID = e.entityID")
                    ->join('customer', "draftInvoiceReturns.customerID = customer.customerID", array('*'), 'left')
                    ->join('currency', 'draftInvoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->where(array('e.deleted' => '0'))
                    ->where(array('draftInvoiceReturns.statusID' => 3));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getInvoiceReturnsByInvoiceID($invoiceID, $locationID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('draftInvoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "draftInvoiceReturns.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('draftInvoiceReturns.invoiceID' => $invoiceID,'draftInvoiceReturns.locationID' => $locationID));

            $select->where->notEqualTo('draftInvoiceReturns.statusID', 4);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    } 


    public function getInvoiceReturnByInvoiceIDForEdit($invoiceID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('draftInvoiceReturns')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "draftInvoiceReturns.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('draftInvoiceReturns.invoiceID' => $invoiceID));

            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getInvoiceReturnsByDate($fromdate, $todate, $customerID = NULL, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvoiceReturns")
                ->columns(array("*"))
                ->join("customer", "draftInvoiceReturns.customerID = customer.customerID", array("customerName", "customerShortName"), "left")
                ->join('currency', 'draftInvoiceReturns.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->order(array('invoiceReturnDate' => 'DESC'))
                ->where(array("draftInvoiceReturns.locationID" => $locationID));
        if ($customerID != NULL) {
            $select->where(array("draftInvoiceReturns.customerID" => $customerID));
        }
        
        $select->where->between('invoiceReturnDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function updateInvoiceReturnStatus($draftInvoiceReturnID, $status)
    {
        $invoiceReturnData = array(
            'statusID' => $status,
        );

        $res = $this->tableGateway->update($invoiceReturnData, array('draftInvoiceReturnID' => $draftInvoiceReturnID));
        return TRUE;
    }

    // /**
    //  * @author Sharmilan <sharmilan@thinkcube.com>
    //  * Search All Credit note for drop down
    //  * @param string $locationID
    //  * @param string $searchKey
    //  * @return Array limited to maximum 50 rows
    //  */
    public function searchAllInvoiceReturnForDropdown($locationID, $searchKey)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvoiceReturns")
                ->columns(array("draftInvoiceReturnID", "invoiceReturnCode"))
                ->order(array('invoiceReturnDate' => 'DESC'))
                ->where(array("draftInvoiceReturns.locationID" => $locationID));
        $select->where->like('invoiceReturnCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    

    public function getInvoiceReturnByIdForDeleteInvoiceReturn($id)
    {
        $rowset = $this->tableGateway->select(array('draftInvoiceReturnID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function updateInvoiceReturnStatusForCancel($draftInvoiceReturnID, $statusID)
    {
        $data = array(
            'statusID' => $statusID
        );
        return $this->tableGateway->update($data, array('draftInvoiceReturnID' => $draftInvoiceReturnID));
    }

    public function updateInvoiceReturn($data, $draftInvoiceReturnID)
    {
        $this->tableGateway->update($data, array('draftInvoiceReturnID' => $draftInvoiceReturnID));
        return TRUE;
    }

}
