<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Payment Methods IDs Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class PaymentMethodsNumbers
{

    public $paymentMethodsNumbersID;
    public $paymentID;
    public $paymentMethodID;
    public $paymentMethodReferenceNumber;
    public $paymentMethodBank;
    public $paymentMethodCardID;
    public $paymentMethodAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->paymentMethodsNumbersID = (!empty($data['paymentMethodsNumbersID'])) ? $data['paymentMethodsNumbersID'] : null;
        $this->paymentID = (!empty($data['paymentID'])) ? $data['paymentID'] : null;
        $this->paymentMethodID = (!empty($data['paymentMethodID'])) ? $data['paymentMethodID'] : null;
        $this->paymentMethodReferenceNumber = (!empty($data['paymentMethodReferenceNumber'])) ? $data['paymentMethodReferenceNumber'] : null;
        $this->paymentMethodBank = (!empty($data['paymentMethodBank'])) ? $data['paymentMethodBank'] : null;
        $this->paymentMethodCardID = (!empty($data['paymentMethodCardID'])) ? $data['paymentMethodCardID'] : null;
        $this->paymentMethodAmount = (!empty($data['paymentMethodAmount'])) ? $data['paymentMethodAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'paymentID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'paymentMethodID',
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'paymentMethodReferenceNumber',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'paymentMethodBank',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'paymentMethodCardID',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
