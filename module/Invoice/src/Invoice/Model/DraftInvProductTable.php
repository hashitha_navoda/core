<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\NotIn;

class DraftInvProductTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('invoice')
                    ->columns(array('*'))
                    ->order(array('issuedDate' => 'DESC'));
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Invoice());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveInvoiceProduct(DraftInvProduct $invocieProduct)
    {
        $data = array(
            'salesInvoiceID' => $invocieProduct->salesInvoiceID,
            'productID' => $invocieProduct->productID,
            'locationProductID' => $invocieProduct->locationProductID,
            'salesInvoiceProductDescription' => $invocieProduct->salesInvoiceProductDescription,
            'salesInvoiceProductPrice' => $invocieProduct->salesInvoiceProductPrice,
            'salesInvoiceProductQuantity' => $invocieProduct->salesInvoiceProductQuantity,
            'salesInvoiceProductTotal' => $invocieProduct->salesInvoiceProductTotal,
            'salesInvoiceProductDiscount' => $invocieProduct->salesInvoiceProductDiscount,
            'salesInvoiceProductDiscountType' => $invocieProduct->salesInvoiceProductDiscountType,
            'salesInvoiceProductTax' => $invocieProduct->salesInvoiceProductTax,
            'documentTypeID' => $invocieProduct->documentTypeID,
            'salesInvoiceProductDocumentID' => $invocieProduct->salesInvoiceProductDocumentID,
            'salesInvoiceProductSelectedUomId' => $invocieProduct->salesInvoiceProductSelectedUomId,
            'inclusiveTaxSalesInvoiceProductPrice' => $invocieProduct->inclusiveTaxSalesInvoiceProductPrice,
            'itemSalesPersonID' => $invocieProduct->itemSalesPersonID,
            'salesInvoiceProductMrpType' => $invocieProduct->salesInvoiceProductMrpType,
            'salesInvoiceProductMrpValue' => $invocieProduct->salesInvoiceProductMrpValue,
            'salesInvoiceProductMrpPercentage' => $invocieProduct->salesInvoiceProductMrpPercentage,
            'salesInvoiceProductMrpAmount' => $invocieProduct->salesInvoiceProductMrpAmount,
            'isFreeItem' => $invocieProduct->isFreeItem,
            'hasFreeItem' => $invocieProduct->hasFreeItem
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getProductsByInvoiceID($invoiceID, $flag = false)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('draftInvProduct')
                ->columns(array('*'))
                ->join('product', 'draftInvProduct.productID =  product.productID', array('*'), 'left')
//                ->join('locationProduct', 'locationProduct.productID =  product.productID', array('*'), 'left')
                ->join('draftInvProductTax', 'draftInvProductTax.salesInvoiceProductID =  draftInvProduct.draftInvProductID', array('taxTotal' => new Expression('SUM(draftInvProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay', 'productUomBase'))
                ->join('uom', 'draftInvProduct.salesInvoiceProductSelectedUomId = uom.uomID', array('uomAbbr', 'uomDecimalPlace'),'left')
                ->group(array('draftInvProduct.draftInvProductID'))
                ->where(array('salesInvoiceID' => $invoiceID));
        if($flag){
            $select->where(array('productUomBase' => '1'));

        }
                // ->where(array('productUomConversion' => '1'));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getInvoiceProductByInvoiceIDForCreditNote($InvocieID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvProduct")
                ->columns(array("*"))
                ->join("draftInvWf", "draftInvProduct.salesInvoiceID = draftInvWf.draftInvWfID", array("salesInvoiceSuspendedTax"), "left")
                ->join("product", "draftInvProduct.productID = product.productID", array("*"), "left")
                ->join("locationProduct", "draftInvProduct.locationProductID = locationProduct.locationProductID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("draftInvProductTax", "draftInvProduct.draftInvProductID= draftInvProductTax.salesInvoiceProductID", array("draftInvProductTaxID", "taxID", "salesInvoiceProductTaxPrecentage", "salesInvoiceProductTaxAmount"), "left")
                ->join("tax", "draftInvProductTax.taxID = tax.id", array("*"), "left")
                ->join("draftInvSubProduct", "draftInvProduct.draftInvProductID = draftInvSubProduct.salesInvocieProductID", array("draftInvSubProductID", "productBatchID", "productSerialID", "salesInvoiceSubProductQuantity","salesInvoiceSubProductWarranty","salesInvoiceSubProductWarrantyType"), "left")
                ->order(array('draftInvProduct.draftInvProductID','draftInvProduct.productID'))
                ->where(array('productUomBase' => '1'))
                ->where(array("draftInvProduct.salesInvoiceID" => $InvocieID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param int $productID
     * @param int $invoiceID
     */
    public function updateCopiedSalesInvoiceProducts($invoiceProductID)
    {
        $data = array(
            'copied' => 1
        );
        $this->tableGateway->update($data, array('salesInvoiceProductID' => $invoiceProductID));
    }


    /**
     * Get invoice serial products by invoice id
     * @param int $InvocieId
     * @return mixed
     */
    public function getInvoiceSerialProductByInvoiceId($InvocieId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvProduct")
                ->columns(array("*"))
                ->join("product", "draftInvProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->join("draftInvSubProduct", "draftInvProduct.draftInvProductID = draftInvSubProduct.salesInvocieProductID", array("draftInvSubProductID", "productBatchID", "productSerialID", "salesInvoiceSubProductQuantity", "salesInvoiceSubProductWarranty", "salesInvoiceSubProductWarrantyType"), "left")
                ->join("productSerial", "draftInvSubProduct.productSerialID = productSerial.productSerialID", array('productSerialCode', 'productSerialWarrantyPeriod','productSerialExpireDate'), "left")
                ->where(array('productUomBase' => '1'))
                ->where(array('serialProduct' => '1'))
                ->where(array("salesInvoiceID" => $InvocieId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get invoice batch products by invoice id
     * @param int $InvocieId
     * @return mixed
     */
    public function getInvoiceBatchProductByInvoiceId($InvocieId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvProduct")
                ->columns(array("*"))
                ->join("product", "draftInvProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("draftInvSubProduct", "draftInvProduct.draftInvProductID = draftInvSubProduct.salesInvocieProductID", array("draftInvSubProductID", "productBatchID", "productSerialID", "salesInvoiceSubProductQuantity"), "left")
                ->join("productBatch", "draftInvSubProduct.productBatchID = productBatch.productBatchID", array('productBatchCode', 'productBatchWarrantyPeriod','productBatchExpiryDate'), "left")
                ->where(array('productUomBase' => '1'))
                ->where(array('batchProduct' => '1'))
                ->where(array("salesInvoiceID" => $InvocieId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateSalesInvoicePoductStaus($data, $invoiceproductID)
    {
        try {
            $this->tableGateway->update($data, array('salesInvoiceProductID' => $invoiceproductID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    

    public function updateSalesInvoicePoductBySalesInvoiceID($data, $invoiceID)
    {
        try {
            $this->tableGateway->update($data, array('salesInvoiceID' => $invoiceID));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

}
