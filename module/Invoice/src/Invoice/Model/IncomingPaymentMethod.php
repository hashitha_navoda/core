<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethod
{

    public $incomingPaymentMethodId;
    public $incomingPaymentId;
    public $incomingPaymentMethodAmount;
    public $incomingPaymentMethodCashId;
    public $incomingPaymentMethodChequeId;
    public $incomingPaymentMethodCreditCardId;
    public $incomingPaymentMethodBankTransferId;
    public $incomingPaymentMethodLoyaltyCardId;
    public $incomingPaymentMethodGiftCardId;
    public $incomingPaymentMethodLCId;
    public $incomingPaymentMethodTTId;
    public $incomingPaymentMethodUniformVoucherId;
    public $entityId;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodId = (!empty($data['incomingPaymentMethodId'])) ? $data['incomingPaymentMethodId'] : null;
        $this->incomingPaymentId = (!empty($data['incomingPaymentId'])) ? $data['incomingPaymentId'] : null;
        $this->incomingPaymentMethodAmount = (!empty($data['incomingPaymentMethodAmount'])) ? $data['incomingPaymentMethodAmount'] : 0.00;
        $this->incomingPaymentMethodCashId = (!empty($data['incomingPaymentMethodCashId'])) ? $data['incomingPaymentMethodCashId'] : null;
        $this->incomingPaymentMethodChequeId = (!empty($data['incomingPaymentMethodChequeId'])) ? $data['incomingPaymentMethodChequeId'] : null;
        $this->incomingPaymentMethodCreditCardId = (!empty($data['incomingPaymentMethodCreditCardId'])) ? $data['incomingPaymentMethodCreditCardId'] : null;
        $this->incomingPaymentMethodBankTransferId = (!empty($data['incomingPaymentMethodBankTransferId'])) ? $data['incomingPaymentMethodBankTransferId'] : null;
        $this->incomingPaymentMethodLoyaltyCardId = (!empty($data['incomingPaymentMethodLoyaltyCardId'])) ? $data['incomingPaymentMethodLoyaltyCardId'] : null;
        $this->incomingPaymentMethodGiftCardId = (!empty($data['incomingPaymentMethodGiftCardId'])) ? $data['incomingPaymentMethodGiftCardId'] : null;
        $this->incomingPaymentMethodLCId = (!empty($data['incomingPaymentMethodLCId'])) ? $data['incomingPaymentMethodLCId'] : null;
        $this->incomingPaymentMethodTTId = (!empty($data['incomingPaymentMethodTTId'])) ? $data['incomingPaymentMethodTTId'] : null;
        $this->incomingPaymentMethodUniformVoucherId = (!empty($data['incomingPaymentMethodUniformVoucherId'])) ? $data['incomingPaymentMethodUniformVoucherId'] : null;
        $this->entityId = (!empty($data['entityId'])) ? $data['entityId'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
