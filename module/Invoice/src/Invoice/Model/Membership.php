<?php

namespace Invoice\Model;

use Zend\InputFilter\InputFilterInterface;

class Membership {

	public $membershipID;
	public $invoiceID;
	public $nextInvoiceDate;
	public $comment;
	
	protected $inputFilter;

	public function exchangeArray($data) {
		$this->membershipID = (!empty($data['membershipID'])) ? $data['membershipID'] : null;
		$this->invoiceID = (!empty($data['invoiceID'])) ? $data['invoiceID'] : null;
		$this->nextInvoiceDate = (!empty($data['nextInvoiceDate'])) ? $data['nextInvoiceDate'] : null;
		$this->comment = (!empty($data['comment'])) ? $data['comment'] : null;
	}

	public function setInputFilter(InputFilterInterface $inputFilter) {
		throw new \Exception("Not used");
	}

	public function getInputFilter() {

	}

	public function getArrayCopy() {
		return get_object_vars($this);
	}

}