<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class InvoiceProductTaxTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('invoice')
                    ->columns(array('*'))
                    ->order(array('issuedDate' => 'DESC'));
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Invoice());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getTaxByInvoiceProductID($invoiceProductID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();

        $select->from('salesInvoiceProductTax')
                ->columns(array('*'))
                ->join('tax', 'tax.id = salesInvoiceProductTax.taxID', array('*'), 'left')
                ->where(array('salesInvoiceProductTax.salesInvoiceProductID' => $invoiceProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getInvoiceTax($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProductTax")
                ->columns(array("*"))
                ->join("tax", "salesInvoiceProductTax.taxID = tax.id", array("*"), "left")
                ->join("salesInvoiceProduct", "salesInvoiceProductTax.salesInvoiceProductID = salesInvoiceProduct.salesInvoiceProductID", array("salesInvoiceID"), "left")
                ->where(array("salesInvoiceProduct.salesInvoiceID" => $invoiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function saveInvoiceProductTax(InvoiceProductTax $invoiceProductTax)
    {
        $data = array(
            'salesInvoiceProductTaxID' => $invoiceProductTax->salesInvoiceProductTaxID,
            'salesInvoiceProductID' => $invoiceProductTax->salesInvoiceProductID,
            'productID' => $invoiceProductTax->productID,
            'taxID' => $invoiceProductTax->taxID,
            'salesInvoiceProductTaxPrecentage' => $invoiceProductTax->salesInvoiceProductTaxPrecentage,
            'salesInvoiceProductTaxAmount' => ($invoiceProductTax->salesInvoiceProductTaxAmount) ? : 0,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getProductTax($invoiceID, $productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProductTax")
                ->columns(array("*"))
                ->join("salesInvoiceProduct", "salesInvoiceProductTax.salesInvoiceProductID = salesInvoiceProduct.salesInvoiceProductID", ["salesInvoiceID", "salesInvoiceProductQuantity"], "left")
                ->where(array("salesInvoiceProduct.salesInvoiceID" => $invoiceID))
                ->where(array("salesInvoiceProduct.productID" => $productID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function getProductTaxByInvoiceAndTaxID($invoiceID, $taxID = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoiceProductTax")
                ->columns(array('tax_total' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')))
                ->join("salesInvoiceProduct", "salesInvoiceProductTax.salesInvoiceProductID = salesInvoiceProduct.salesInvoiceProductID", ["salesInvoiceID", "salesInvoiceProductQuantity"], "left")
                ->where(array("salesInvoiceProduct.salesInvoiceID" => $invoiceID))
                ->where->in("salesInvoiceProductTax.taxID", $taxID);
        // $select->group(array('salesInvoiceProduct.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        return $results;
    }

}
