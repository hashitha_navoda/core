<?php

/**
 * @author Ashan     <ashan@thinkcube.com>
 * This file contains Credit Note Payment method numbers Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CreditNotePaymentMethodNumbers implements InputFilterAwareInterface
{

    public $creditNotePaymentMethodNumbersID;
    public $creditNotePaymentID;
    public $paymentMethodID;
    public $creditNotePaymentMethodReferenceNumber;
    public $creditNotePaymentMethodBank;
    public $creditNotePaymentMethodCardID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->creditNotePaymentMethodNumbersID = (!empty($data['creditNotePaymentMethodNumbersID'])) ? $data['creditNotePaymentMethodNumbersID'] : null;
        $this->creditNotePaymentID = (!empty($data['creditNotePaymentID'])) ? $data['creditNotePaymentID'] : null;
        $this->paymentMethodID = (!empty($data['paymentMethodID'])) ? $data['paymentMethodID'] : null;
        $this->creditNotePaymentMethodReferenceNumber = (!empty($data['creditNotePaymentMethodReferenceNumber'])) ? $data['creditNotePaymentMethodReferenceNumber'] : null;
        $this->creditNotePaymentMethodBank = (!empty($data['creditNotePaymentMethodBank'])) ? $data['creditNotePaymentMethodBank'] : null;
        $this->creditNotePaymentMethodCardID = (!empty($data['creditNotePaymentMethodCardID'])) ? $data['creditNotePaymentMethodCardID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
