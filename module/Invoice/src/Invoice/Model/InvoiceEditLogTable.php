<?php

/**
 * @author sandun<sandun@thinkcube.com>
 * This file contains InvoiceEditLog Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class InvoiceEditLogTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveInvoiceEditLog(InvoiceEditLog $invoiceEditLog)
    {
        $data = array(
            'userID' => $invoiceEditLog->userID,
            'salesInvoiceID' => $invoiceEditLog->salesInvoiceID,
            'dateAndTime' => $invoiceEditLog->dateAndTime
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

}
