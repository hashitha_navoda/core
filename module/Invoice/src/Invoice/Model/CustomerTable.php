<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains Customer related database operations
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class CustomerTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    /**
     *
     * @author Damith Thamara <damith@thinkcube.com>
     * @return \Zend\Paginator\Paginator
     * This functions return customer list
     */
    public function fetchAll($paginated = FALSE, $withoutDefaultCust = FALSE, $withDeactivated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        if($withDeactivated){
            if (!$withoutDefaultCust) {
                $select->from('customer')
                        ->columns(array('*'))
                        ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                        ->order('customer.customerName')
                        ->where(array('entity.deleted' => '0'));

            } else {
                $select->from('customer')
                        ->columns(array('*'))
                        ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                        ->order('customer.customerName')
                        ->where(array('entity.deleted' => '0'))
                ->where->notEqualTo('customer.customerID', '0');
            }
        }else{
             if (!$withoutDefaultCust) {
                $select->from('customer')
                        ->columns(array('*'))
                        ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                        ->order('customer.customerName')
                        ->where(array('entity.deleted' => '0'))
                        ->where(array('customer.customerStatus' => '1'));
            } else {
                $select->from('customer')
                        ->columns(array('*'))
                        ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                        ->order('customer.customerName')
                        ->where(array('entity.deleted' => '0'))
                        ->where(array('customer.customerStatus' => '1'))
                ->where->notEqualTo('customer.customerID', '0');
            }
        }
        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );

            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
//        $select->order('customer.customerName');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function fetchAllWithPrimaryContactDetails()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customerProfile', 'customer.customerID = customerProfile.customerID', array('*'), 'left')
                ->where(array('entity.deleted' => '0', 'customerProfile.isPrimary' => '1'));
        $select->order('customerName');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get Customer with full details
     * @return type
     */
    public function fetchAllWithFullDetails()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customerProfile', 'customer.customerID = customerProfile.customerID', array('*'), 'left')
                ->join('paymentTerm', 'customer.customerPaymentTerm = paymentTerm.paymentTermID', array('paymentTermName'), 'left')
                ->join('currency', 'customer.customerCurrency = currency.currencyID', array('currencyName'), 'left')
                ->where(array('entity.deleted' => '0', 'customerProfile.isPrimary' => '1'))
        ->where->notEqualTo('customer.customerID', '0');
        $select->order('customerName');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param type $customerName
     * @return type
     * This function gets cutomer by Name
     */
    public function getCustomerByName($customerName)
    {
        $rowset = $this->tableGateway->select(array('customerName' => $customerName));
        $row = $rowset->current();

        return $row;
    }

    public function getCustomersByCustomerEvent($customerEvent)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer');
        $select->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left');
        $select->join('customerProfile', 'customer.customerID = customerProfile.customerID', '*', 'left');
        $select->where(array('entity.deleted' => '0', 'customerProfile.isPrimary' => '1'));
        $select->where(array('customerEvent' => $customerEvent));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultList = [];
        foreach ($results as $result) {
            $resultList[] = $result;
        }
        return $resultList;
    }

//this function get customers details by customershort name
    public function getCustomerByShortname($customershortname)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'customerShortName' => $customershortname));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results->current();
        return $row;
    }

//this function get customers details by customer telephone number
    public function getCustomerByTelephoneNumber($customerTelephoneNumber)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'customerTelephoneNumber' => $customerTelephoneNumber));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results->current();
        return $row;
    }

//this function get customers details by customerCode
    public function getCustomerByCode($customerCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'customerCode' => $customerCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results->current();
        return $row;
    }

    public function checkCustomerTransaction($customerID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('customerID'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('creditNote', 'customer.customerID =  creditNote.customerID', array('creditNoteID'), 'left')
                ->join('deliveryNote', 'customer.customerID =  deliveryNote.customerID', array('deliveryNoteID'), 'left')
                ->join('incomingPayment', 'customer.customerID =  incomingPayment.customerID', array('incomingPaymentID'), 'left')
                ->join('quotation', 'customer.customerID =  quotation.customerID', array('quotationID'), 'left')
                ->join('salesInvoice', 'customer.customerID =  salesInvoice.customerID', array('salesInvoiceID'), 'left')
                ->join('salesOrders', 'customer.customerID =  salesOrders.customerID', array('soID'), 'left')
                ->join('inquiryLog', 'customer.customerID =  inquiryLog.customerID', array('inquiryLogID'), 'left')
                ->where(array('entity.deleted' => '0', 'customer.customerID' => $customerID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results;
        return $row;
    }

    public function getCustomerByID($customerID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customerLoyalty', new Expression('customer.customerID = customerLoyalty.customerID AND customerLoyalty.customerLoyaltyStatus = 1'), array('customerLoyaltyCode', 'customerLoyaltyStatus'), 'left')
                ->join('loyalty', 'customerLoyalty.loyaltyID = loyalty.loyaltyID', array('loyaltyID'), 'left')
                ->where(
                        array(
                            'entity.deleted' => '0',
                            'customer.customerID' => $customerID,
                        )
        );
        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();
        $rows = (object) $results->current();

        if (isset($rows->customerID)) {
            $row = $rows;
        } else {
            $row = NULL;
        }
        return $row;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $customerID
     * @return null
     */
    public function getCustomerAndPrimaryProfileByID($customerID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customerProfile', 'customer.customerID = customerProfile.customerID', '*', 'left')
                ->where(array('entity.deleted' => '0', 'customer.customerID' => $customerID, 'customerProfile.isPrimary' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $rows = (object) $results->current();
        if (isset($rows->customerID)) {
            $row = $rows;
        } else {
            $row = NULL;
        }
        return $row;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $customerSearchKey
     * @return type
     */
    public function searchCustomersForDropDown($customerSearchKey, $withDeactivated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(['customerName', 'customerCode', 'customerTitle', 'customerID',
                    'posi' => new Expression('LEAST('
                    . 'IF(POSITION(\'' . $customerSearchKey . '\' in customerName )>0,POSITION(\'' . $customerSearchKey . '\' in customerName), 9999),'
                    . 'IF(POSITION(\'' . $customerSearchKey . '\' in customerCode )>0,POSITION(\'' . $customerSearchKey . '\' in customerCode), 9999)) '),
                    'len' => new Expression('LEAST(CHAR_LENGTH(customerName), CHAR_LENGTH(customerCode)) '),
                    'occr' => new Expression('LEAST('
                        .'IF((CHAR_LENGTH(customerName) - CHAR_LENGTH(REPLACE(LOWER(customerName), "' . $customerSearchKey . '", ""))) >0, (CHAR_LENGTH(customerName) - CHAR_LENGTH(REPLACE(LOWER(customerName), "' . $customerSearchKey . '", ""))), 9999),'
                        .'IF((CHAR_LENGTH(customerCode) - CHAR_LENGTH(REPLACE(LOWER(customerCode), "' . $customerSearchKey . '", ""))) >0, (CHAR_LENGTH(customerCode) - CHAR_LENGTH(REPLACE(LOWER(customerCode), "' . $customerSearchKey . '", ""))), 9999)'
                        .')')
                    ])
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(new PredicateSet(array(new Operator('customerName', 'like', '%' . $customerSearchKey . '%'), new Operator('customerCode', 'like', '%' . $customerSearchKey . '%'),new Operator('customerTelephoneNumber', 'like', '%' . $customerSearchKey . '%')), PredicateSet::OP_OR))
        ->where->notEqualTo('customer.customerID', '0');
        if($withDeactivated){
            $select->where(array('entity.deleted' => 0));
        }else{
            $select->where(array('entity.deleted' => 0, 'customer.customerStatus' => '1'));
        }
        $select->order(new Expression('IF(posi > 0, posi, 9999) ASC, len ASC, occr DESC'));
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function getCustomerByCustomerNameforSearch($customerName, $withoutDefaultCust = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        if (!$withoutDefaultCust) {
            $select->from('customer')
                    ->columns(array('*'))
                    ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                    ->where(array('entity.deleted' => '0', 'customerName LIKE ?' => '%' . $customerName . '%'))
                    ->order(array(new Expression('POSITION(\'' . $customerName . '\' in customer.customerName)')));
        } else {
            $select->from('customer')
                    ->columns(array('*'))
                    ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                    ->where(array('entity.deleted' => '0', 'customerName LIKE ?' => '%' . $customerName . '%'))
                    ->order(array(new Expression('POSITION(\'' . $customerName . '\' in customer.customerName)')))
            ->where->notEqualTo('customer.customerID', '0');
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results;
        return $row;
    }

    public function getCustomerByCustomerCodeforSearch($customerCode, $withoutDefaultCust = false)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'customerCode LIKE ?' => '%' . $customerCode . '%'))
                ->order(array(new Expression('POSITION(\'' . $customerCode . '\' in customer.customerCode)')));
        if ($withoutDefaultCust) {
            $select->where->notEqualTo('customer.customerID', '0');
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results;
        return $row;
    }

    public function getCustomerByCustomerNameCodeOrTelephoneNumber($keyword)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customerProfile', 'customerProfile.customerID=customer.customerID', array('*'), 'left');
        $select->where(new PredicateSet(array(
            new Operator('customer.customerName', 'like', '%' . $keyword . '%'),
            new Operator('customer.customerTelephoneNumber', 'like', '%' . $keyword . '%'),
            new Operator('customer.customerCode', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLandTP2', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLandTP1', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileMobileTP2', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileMobileTP1', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLocationNo', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLocationRoadName1', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLocationRoadName2', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLocationRoadName3', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLocationSubTown', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLocationTown', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLocationPostalCode', 'like', '%' . $keyword . '%'),
            new Operator('customerProfile.customerProfileLocationCountry', 'like', '%' . $keyword . '%'),
                ), PredicateSet::OP_OR));
        $select->where(new PredicateSet(array(new Operator('entity.deleted', '=', 0))));
        $select->where(new PredicateSet(array(new Operator('customer.customerStatus', '=', 1))));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results;
        return $row;
    }

    public function getCustomerByCustomerShortNameforSearch($customerShortName)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'customerShortName LIKE ?' => '%' . $customerShortName . '%'))
                ->order(array(new Expression('POSITION(\'' . $customerShortName . '\' in customer.customerShortName)')));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results;
        return $row;
    }

    public function saveCustomer(Customer $Customer)
    {
        $data = array(
            'customerID' => $Customer->customerID,
            'customerCode' => $Customer->customerCode,
            'customerTitle' => $Customer->customerTitle,
            'customerName' => $Customer->customerName,
            'customerShortName' => $Customer->customerShortName,
            'customerTelephoneNumber' => $Customer->customerTelephoneNumber,
            'customerDateOfBirth' => $Customer->customerDateOfBirth,
            'customerIdentityNumber' => $Customer->customerIdentityNumber,
            'customerCategory' => $Customer->customerCategory,
            'customerPriceList' => $Customer->customerPriceList,
            'customerDesignation' => $Customer->customerDesignation,
            'customerDepartment' => $Customer->customerDepartment,
            'customerVatNumber' => $Customer->customerVatNumber,
            'customerSVatNumber' => $Customer->customerSVatNumber,
            'customerPaymentTerm' => $Customer->customerPaymentTerm,
            'customerCreditLimit' => $Customer->customerCreditLimit,
            'customerCurrentBalance' => $Customer->customerCurrentBalance,
            'customerCurrentCredit' => $Customer->customerCurrentCredit,
            'customerDiscount' => $Customer->customerDiscount,
            'customerOther' => $Customer->customerOther,
            'customerCurrency' => $Customer->customerCurrency,
            'customerEvent' => $Customer->customerEvent,
            'customerReceviableAccountID' => $Customer->customerReceviableAccountID,
            'customerSalesAccountID' => $Customer->customerSalesAccountID,
            'customerSalesDiscountAccountID' => $Customer->customerSalesDiscountAccountID,
            'customerAdvancePaymentAccountID' => $Customer->customerAdvancePaymentAccountID,
            'entityID' => $Customer->entityID,
            'customerGender' => $Customer->customerGender,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param \Invoice\Model\Customer $Customer
     * @return string
     * This function replaces customer If he exists in the Database else insert
     */
    public function importreplaceCustomer($data, $customerID)
    {
        try {
            $this->tableGateway->update($data, array('customerID' => $customerID));
            return true;
        } catch (\Exception $exc) {
            return false;
        }
    }

    /**
     *  @author Damith Thamara <damith@thinkcube.com>
     * @param type $dataarray
     * @return boolean
     * This funtion updates customers certain feilds from mass edit.
     */
    public function massupdateCustomer($dataarray)
    {
        $data = array(
            'customerTitle' => $dataarray['customerTitle'],
            'customerName' => $dataarray['customerName'],
            'customerCode' => $dataarray['customerCode'],
            'customerShortName' => $dataarray['customerShortName'],
            'customerCurrency' => $dataarray['customerCurrency'],
            'customerTelephoneNumber' => $dataarray['customerTelephoneNumber'],
            'customerVatNumber' => $dataarray['customerVatNumber'],
            'customerSVatNumber' => $dataarray['customerSVatNumber'],
            'customerPaymentTerm' => $dataarray['customerPaymentTerm'],
            'customerCategory' => $dataarray['customerCategory'],
            'customerDateOfBirth' => $dataarray['customerDateOfBirth'],
            'customerIdentityNumber' => $dataarray['customerIdentityNumber'],
            'customerCreditLimit' => $dataarray['customerCreditLimit'],
            'customerDiscount' => $dataarray['customerDiscount'],
        );
        try {
            $this->tableGateway->update($data, array('customerID' => $dataarray['customerID']));
            return TRUE;
        } catch (\Exception $ex) {
            return FALSE;
        }
    }

    public function updateCustomer(Customer $Customer)
    {

        $data = array(
            'customerID' => $Customer->customerID,
            'customerCode' => $Customer->customerCode,
            'customerTitle' => $Customer->customerTitle,
            'customerName' => $Customer->customerName,
            'customerShortName' => $Customer->customerShortName,
            'customerTelephoneNumber' => $Customer->customerTelephoneNumber,
            'customerDateOfBirth' => $Customer->customerDateOfBirth,
            'customerIdentityNumber' => $Customer->customerIdentityNumber,
            'customerCategory' => $Customer->customerCategory,
            'customerPriceList' => $Customer->customerPriceList,
            'customerDesignation' => $Customer->customerDesignation,
            'customerDepartment' => $Customer->customerDepartment,
            'customerVatNumber' => $Customer->customerVatNumber,
            'customerSVatNumber' => $Customer->customerSVatNumber,
            'customerPaymentTerm' => $Customer->customerPaymentTerm,
            'customerCreditLimit' => $Customer->customerCreditLimit,
            'customerDiscount' => $Customer->customerDiscount,
            'customerOther' => $Customer->customerOther,
            'customerEvent' => $Customer->customerEvent,
            'customerCurrency' => $Customer->customerCurrency,
            'customerReceviableAccountID' => $Customer->customerReceviableAccountID,
            'customerSalesAccountID' => $Customer->customerSalesAccountID,
            'customerSalesDiscountAccountID' => $Customer->customerSalesDiscountAccountID,
            'customerAdvancePaymentAccountID' => $Customer->customerAdvancePaymentAccountID,
            'customerGender' => $Customer->customerGender,
        );

        $result = $this->getCustomerByID($Customer->customerID);

        if ($result == NULL) {
            return FALSE;
        } else {
            $this->tableGateway->update($data, array('customerID' => $Customer->customerID));
            return TRUE;
        }
    }

    public function updateCustomerCredit(Customer $Customer)
    {
        $data = array(
            'customerCurrentCredit' => $Customer->customerCurrentCredit,
        );
        $this->tableGateway->update($data, array('customerID' => $Customer->customerID));
        return TRUE;
    }

    public function updateCustomerBalance(Customer $Customer)
    {
        try {
            $data = array(
                'customerCurrentBalance' => $Customer->customerCurrentBalance,
            );
            $this->tableGateway->update($data, array('customerID' => $Customer->customerID));
            return TRUE;
        } catch (\Exception $exc) {
            echo $exc;
            return false;
        }
    }

    public function updateCustomerByCustomerID($data, $customerID)
    {
        try {
            $this->tableGateway->update($data, array('customerID' => $customerID));
            return true;
        } catch (\Exception $exc) {
            echo $exc;
            return false;
        }
    }

    public function updateCustomerCurrentAndCreditBalance(Customer $Customer)
    {
        try {
            $data = array(
                'customerCurrentBalance' => $Customer->customerCurrentBalance,
                'customerCurrentCredit' => $Customer->customerCurrentCredit,
            );
            $this->tableGateway->update($data, array('customerID' => $Customer->customerID));
            return TRUE;
        } catch (\Exception $exc) {
            echo $exc;
        }exit;
    }

    public function deleteCustomer($customerID)
    {
        try {
            $this->tableGateway->delete(array('customerID' => $customerID));
            return TRUE;
        } catch (\Exception $exc) {
            return FALSE;
        }
    }

    public function getCustomerList()
    {

        $statement = $this->tableGateway->select(function ($select) {
            $select->columns(array('customerID', 'customerName'));
        });
        return $statement;
    }

    public function getCustomerNameList()
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from("customer");
//        $select->columns(array('customerID', 'cust_name' => new Expression("CONCAT_WS( ' - ', customerName, customerShortName )")));
        $select->columns(array('customerID', 'cust_name' => new Expression("CONCAT_WS( ' - ', customerName, customerCode )")));
        $select->join('entity', 'entity.entityID = customer.entityID', array('deleted'), 'left');
        $select->where(array('deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     *
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * This functions return customer list for customer balance view
     */
    public function getCustomers($order = array())
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('customer')
                ->columns(array('CusID' => new Expression('(customerID)'),
                    'CusName' => new Expression('customerName')));
        if (empty($order)) {
            $select->order(array('customerID'));
        } else {
            $select->order($order);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $cusData = array();

        foreach ($results as $val) {
            array_push($cusData, $val['CusID']);
        }

        return $cusData;
    }

    public function getCustomerDetailsByIds($cusIds)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->order('customerName ASC')
                ->where(array('entity.deleted' => '0'))
        ->where->in('customerID', $cusIds);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $cusIDs
     */
    public function getCustomerAndPrimaryProfileDetailsByIds($cusIDs, $isAllCustomers = false, $cusCategory = null)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customerProfile', 'customer.customerID = customerProfile.customerID', '*', 'left')
                ->order('customerName ASC')
                ->where(array('entity.deleted' => '0', 'customerProfile.isPrimary' => '1'));
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIDs);
        }
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

     public function getCustomerByCustomerIDs($cusIds, $isAllCustomers, $cusCategory = null)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('status', 'customer.CustomerStatus = status.statusID',array('statusName'),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0'));
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * get customer by customer ID for customer list view
     * @param type $customerID
     * @return type
     */
    public function getCustomerByCustomerIDForSearch($customerID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'customerID' => $customerID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results;
        return $row;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $customerEventID
     * @return type
     * This function get Customer By Event ID
     */
    public function getByCustomerEventID($customerEventID)
    {
        return $this->tableGateway->select(array('customerEvent' => $customerEventID));
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $customerTelephoneNumber
     * @return type
     * This function get Customer By Event ID
     */
    public function getCustomerByPrimaryTelephoneNumber($customerTelephoneNumber)
    {
        return $this->tableGateway->select(array('customerTelephoneNumber' => $customerTelephoneNumber));
    }

    public function getCustomerDetailsByPrimaryTelephoneNumber($customerTelephoneNumber)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'customerTelephoneNumber' => $customerTelephoneNumber));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $row = (object) $results;
        return $row;
    }

    public function updateCustomerByArray($data, $customerId)
    {
        return $this->tableGateway->update($data, array('customerID' => $customerId));
    }
    
    public function getChildCateogryIds($categoryIds)
    {
       $adapter = $this->tableGateway->getAdapter();
       $results = [];

       if ($categoryIds !="") {
       
            $parentIds = implode(',', $categoryIds);

            $query = <<<SQL
            SELECT GROUP_CONCAT(categoryID SEPARATOR ',') as categoryIds
            FROM category WHERE FIND_IN_SET(categoryID,(SELECT GROUP_CONCAT(lv SEPARATOR ',') FROM (SELECT @pv:=(SELECT GROUP_CONCAT(categoryID SEPARATOR ',') FROM category WHERE categoryParentID IN (@pv)) AS lv FROM category JOIN (SELECT @pv:={$parentIds})tmp WHERE categoryParentID IN (@pv)) a))
SQL;
       
           $statement = $adapter->query($query); 
           $results = $statement->execute()->current(); 
           
       } 
       
       return $results; 
    }

    //get customer wise sales item details
     public function getCustomerWiseSalesItemsDetails($cusIds, $isAllCustomers, $categoryIds, $isAllCategory, $formDate, $endDate, $isAllItem, $itemIDs, $cusCategory = null)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllCategory = filter_var($isAllCategory, FILTER_VALIDATE_BOOLEAN);
        $isAllItem = filter_var($isAllItem, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('customerName', 'customerCode', 'customerShortName', 'customerID'))
                ->join('salesInvoice', 'customer.customerID = salesInvoice.customerID', array('salesInvoiceID', 'salesInvoiceCode', 'salesInvoiceIssuedDate', 'entityID', 'statusID'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('productID', 'salesInvoiceProductQuantity', 'salesInvoiceProductTotal', 'salesInvoiceProductID'), 'left')
                ->join('product', 'salesInvoiceProduct.productID = product.productID', array('productCode', 'categoryID', 'productName'), 'left')
                ->join(['c1' => 'category'], 'product.categoryID = c1.categoryID', array('c1.catname' => 'categoryName'), 'left')
                ->join(['c2' => 'category'], 'c2.categoryID = c1.categoryParentID', array('c2.catname' => 'categoryName'), 'left')
                ->order('customerName ASC')
                ->where(array('entity.deleted' => '0'));

        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllCategory) {
            $select->where->in('c1.categoryID', $categoryIds);
        }

        if(!$isAllItem){
            $select->where->in('product.productID',$itemIDs);
        }

        if($formDate != null && $endDate != null ){
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $formDate, $endDate);
        }
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    //get customer wise sales item details
     public function getCustomerWiseSalesReport($cusIds, $isAllCustomers, $locations, $isAllLocation, $formDate, $endDate, $cusCategory = null)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('customerName', 'customerCode', 'customerShortName', 'customerID'))
                ->join('salesInvoice', 'customer.customerID = salesInvoice.customerID', array('salesInvoiceID', 'salesInvoiceCode', 'salesInvoiceIssuedDate', 'entityID', 'statusID', 'salesInvoiceComment','salesInvoiceWiseTotalDiscount','salesinvoiceTotalAmount','salesInvoiceTotalDiscountType'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'), 'left')
                ->join('product', 'salesInvoiceProduct.productID = product.productID', array('productCode', 'categoryID', 'productName'), 'left')
                ->order('customerName ASC')
                ->where(array('entity.deleted' => '0'));
        $select->where(array('product.productState' => '1'));
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('salesInvoice.locationID', $locations);
        }

        if($formDate != null && $endDate != null ){
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $formDate, $endDate);
        }
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getAllCustomerTransactionDetails($cusIds, $isAllCustomers, $locations, $isAllLocation, $formDate, $endDate, $cusCategory = null)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('CustomerID'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('quotation', 'customer.customerID = quotation.customerID', array('customerID'), 'left')
                ->join('deliveryNote', 'customer.customerID = deliveryNote.customerID', array('customerID'), 'left')
                ->join('salesOrders', 'customer.customerID = salesOrders.customerID', array('customerID'), 'left')
                ->join('salesInvoice', 'customer.customerID = salesInvoice.customerID', array('customerID'), 'left')
                ->join('incomingPayment', 'customer.customerID = incomingPayment.customerID', array('customerID'), 'left')
                ->join('salesReturn', 'customer.customerID = salesReturn.customerID', array('customerID'), 'left')
                // ->join('salesReturnProduct', 'salesReturnProduct.salesReturnID = salesReturn.salesReturnID', array('salesReturnID'),'left')
                ->join('creditNote', 'customer.customerID = creditNote.customerID', array('customerID'), 'left')
                // ->join('creditNotePayment', 'customer.customerID = creditNotePayment.customerID', array('customerID'), 'left')
                // ->join('creditNotePaymentDetails', 'creditNotePaymentDetails.creditNotePaymentID = creditNotePayment.creditNotePaymentID', array('creditNotePaymentID'), 'left')
                ->order('customer.customerID ASC')
                ->where(array('entity.deleted' => '0'));
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('salesInvoice.locationID', $locations);
        }

        if($formDate != null && $endDate != null ){
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $formDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

     public function updateCustomerAccountsByCustomerID($data, $customerId)
    {
        try {
            $this->tableGateway->update($data, array('customerID' => $customerId));
            return true;
        } catch (Exception $e) {
            return false;
        }
    }



    public function updateCustomerState($customerID, $status){
        $customerData = array(
            'customerStatus' => $status,
        );
        try {
            $this->tableGateway->update($customerData, array('customerId' => $customerID));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
        return true;
    }

    //get customer wise credite note data
    public function getCustomerWiseCreditNoteData($cusIds, $isAllCustomers, $locations, $isAllLocation, $formDate, $endDate, $cusCategory = null)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('customerName', 'customerCode', 'customerShortName', 'customerID'))
                ->join('creditNote', 'customer.customerID = creditNote.customerID', array('creditNoteID', 'creditNoteCode', 'creditNoteDate', 'entityID', 'statusID', 'creditNoteComment','creditNoteInvoiceDiscountAmount','creditNoteTotal','creditNoteInvoiceDiscountType'), 'left')
                ->join('entity', 'creditNote.entityID = entity.entityID', array('deleted'), 'left')
                ->join('creditNoteProduct', 'creditNote.creditNoteID = creditNoteProduct.creditNoteID', array('*'), 'left')
                ->join('product', 'creditNoteProduct.productID = product.productID', array('productCode', 'categoryID', 'productName'), 'left')
                ->order('customerName ASC')
                ->where(array('entity.deleted' => '0'));
        $select->where(array('product.productState' => '1'));
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($formDate != null && $endDate != null ){
            $select->where->between('creditNote.creditNoteDate', $formDate, $endDate);
        }
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

     /*
    * get the product data by gl account id
    */
    public function getCustomerByGlAccountID($glAccountID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->where(new PredicateSet(array(new Operator('customerReceviableAccountID', '=',$glAccountID), 
                    new Operator('customerSalesAccountID', '=',$glAccountID),
                    new Operator('customerSalesDiscountAccountID', '=',$glAccountID),
                    new Operator('customerAdvancePaymentAccountID', '=',$glAccountID)
                ), PredicateSet::OP_OR));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }


    public function getAllCustomerForPos()
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->join('customerLoyalty', new Expression('customer.customerID = customerLoyalty.customerID AND customerLoyalty.customerLoyaltyStatus = 1'), array('customerLoyaltyCode', 'customerLoyaltyStatus'), 'left')
                ->join('loyalty', 'customerLoyalty.loyaltyID = loyalty.loyaltyID', array('loyaltyID'), 'left')
                ->order('customer.customerName')
                ->where(array('entity.deleted' => '0'))
                ->where(array('customer.customerStatus' => '1'))
                ->where->notEqualTo('customer.customerID', '0');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;  
    }

    public function fetchAllCustomerDetails($customerID = null) {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer');
        $select->join('customerProfile', 'customer.customerID = customerProfile.customerID', '*', 'left');
        if (!is_null($customerID)) {
            $select->where(array('customerID' => $customerID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getCustomerDetails($data) {
        $filteringArray = [];
        if ($data['customerID']) {
            $filteringArray['customer.customerID'] = $data['customerID'];
        }
        if ($data['mobileNo']) {
            $filteringArray['customerTelephoneNumber'] = $data['mobileNo'];
        }
        if ($data['address']) {
            $filteringArray['customerAddress'] = $data['address'];
        }
        if ($data['emailAddress']) {
            $filteringArray['customerProfileEmail'] = $data['emailAddress'];
        }
        if (!$filteringArray) {
            return [];
        }
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer');
        $select->join('customerProfile', 'customer.customerID = customerProfile.customerID', '*', 'left');
        if (!is_null($data)) {
            $select->where($filteringArray);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }
     public function getCustomerWiseSalesForSalesDashboard($locations, $formDate, $endDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('customerName', 'customerCode', 'customerShortName', 'customerID'))
                ->join('salesInvoice', 'customer.customerID = salesInvoice.customerID', array('salesInvoiceID', 'salesInvoiceCode', 'salesInvoiceIssuedDate', 'entityID', 'statusID', 'salesInvoiceComment','salesInvoiceWiseTotalDiscount','salesinvoiceTotalAmount','salesInvoiceTotalDiscountType'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order('customerName ASC')
                ->where(array('entity.deleted' => '0'));
        $select->where->in('salesInvoice.locationID', $locations);

        if($formDate != null && $endDate != null ){
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $formDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getCustomerWiseCreditNoteDataForSalesDashboard($locations, $formDate, $endDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('customerName', 'customerCode', 'customerShortName', 'customerID'))
                ->join('creditNote', 'customer.customerID = creditNote.customerID', array('creditNoteID', 'creditNoteCode', 'creditNoteDate', 'entityID', 'statusID', 'creditNoteComment','creditNoteInvoiceDiscountAmount','creditNoteTotal','creditNoteInvoiceDiscountType'), 'left')
                ->join('entity', 'creditNote.entityID = entity.entityID', array('deleted'), 'left')
                ->order('customerName ASC')
                ->where(array('entity.deleted' => '0'));
        $select->where->in('creditNote.locationID', $locations);

        if($formDate != null && $endDate != null ){
            $select->where->between('creditNote.creditNoteDate', $formDate, $endDate);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getCustomerWiseSalesReturnDataForSalesDashboard($locations, $formDate, $endDate)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                ->columns(array('customerName', 'customerCode', 'customerShortName', 'customerID'))
                ->join('salesInvoice', 'customer.customerID = salesInvoice.customerID', array('salesInvoiceID', 'salesInvoiceCode', 'salesInvoiceIssuedDate', 'entityID', 'statusID', 'salesInvoiceComment','salesInvoiceWiseTotalDiscount','salesinvoiceTotalAmount','salesInvoiceTotalDiscountType'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order('customerName ASC')
                ->where(array('entity.deleted' => '0'));
        $select->where->in('salesInvoice.locationID', $locations);

        if($formDate != null && $endDate != null ){
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $formDate, $endDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;

    }
    public function getCustomerByCustomerCode($customerCode)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
        ->columns(array('*'))
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'customerCode' => $customerCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results->current();
    }

    /**
     *
     * Get Customers Count
     */
    public function getCustomerCountForDashboard($paginated = FALSE, $withoutDefaultCust = FALSE, $withDeactivated = FALSE)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customer')
                        ->columns(array('num' => new Expression('COUNT(*)')))
                        ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                        ->where(array('entity.deleted' => '0'))
                        ->where(array('customer.customerStatus' => '1'))
                        ->limit(1);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }
}
