<?php

/**
 * @author Prathap Weerasingeh <prathap@thinkcube.com>
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class SalesOrderProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveSoProductTax(SalesOrderProductTax $soProductTax)
    {
        $data = array(
            'soID' => $soProductTax->soID,
            'soProductID' => $soProductTax->soProductID,
            'soTaxID' => $soProductTax->soTaxID,
            'soTaxPrecentage' => $soProductTax->soTaxPrecentage,
            'soTaxAmount' => $soProductTax->soTaxAmount,
        );
        $this->tableGateway->insert($data);
    }

    public function getSalesOrderProductTax($salesOrderProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesOrderProductTax")
                ->columns(array("*"))
                ->join("tax", "salesOrderProductTax.soTaxID = tax.id", array("*"), "left")
                ->where(array("soProductID" => $salesOrderProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getSalesOrderTax($salesOrderID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesOrderProductTax")
                ->columns(array("*"))
                ->join("tax", "salesOrderProductTax.soTaxID = tax.id", array("*"), "left")
                ->where(array("soID" => $salesOrderID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function deleteSalesOrderProductTax($soProductID)
    {
        $rowset = $this->tableGateway->delete(array('soProductID' => $soProductID));
        return $rowset;
    }

    //get sales order product tax using sales order produt id and tax id
    public function getSalesOrderProductTaxBySalesOrderProductIDAndTaxID($soProductID, $taxID)
    {
        $rowset = $this->tableGateway->select(array('soProductID' => $soProductID, 'soTaxID' => $taxID));
        return $rowset->current();
    }

    public function updateSalesOrderProductTax($soProductTax, $soProductTaxID)
    {
        $data = array(
            'soTaxPrecentage' => $soProductTax->soTaxPrecentage,
            'soTaxAmount' => $soProductTax->soTaxAmount,
        );
        $this->tableGateway->update($data, array('soProductTaxID' => $soProductTaxID));
    }

}
