<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DraftInvProductTaxTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('invoice')
                    ->columns(array('*'))
                    ->order(array('issuedDate' => 'DESC'));
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Invoice());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getInvoiceTax($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvProductTax")
                ->columns(array("*"))
                ->join("tax", "draftInvProductTax.taxID = tax.id", array("*"), "left")
                ->join("draftInvProduct", "draftInvProductTax.salesInvoiceProductID = draftInvProduct.draftInvProductID", array("salesInvoiceID"), "left")
                ->where(array("draftInvProduct.salesInvoiceID" => $invoiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function saveInvoiceProductTax(DraftInvProductTax $invoiceProductTax)
    {
        $data = array(
            'draftInvProductTaxID' => $invoiceProductTax->draftInvProductTaxID,
            'salesInvoiceProductID' => $invoiceProductTax->salesInvoiceProductID,
            'productID' => $invoiceProductTax->productID,
            'taxID' => $invoiceProductTax->taxID,
            'salesInvoiceProductTaxPrecentage' => $invoiceProductTax->salesInvoiceProductTaxPrecentage,
            'salesInvoiceProductTaxAmount' => ($invoiceProductTax->salesInvoiceProductTaxAmount) ? : 0,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

}
