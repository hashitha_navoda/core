<?php

/**
 * @author Prathap Weerasinghe <prathap@thinkcube.com>
 * This is the QuotationProductTax model
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class QuotationProductTax implements InputFilterAwareInterface
{

    public $quotProductTaxID;
    public $quotID;
    public $quotProductID;
    public $quotTaxID;
    public $quotTaxPrecentage;
    public $quotTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->quotProductTaxID = (!empty($data['quotProductTaxID'])) ? $data['quotProductTaxID'] : null;
        $this->quotID = (!empty($data['quotID'])) ? $data['quotID'] : null;
        $this->quotProductID = (!empty($data['quotProductID'])) ? $data['quotProductID'] : null;
        $this->quotTaxID = (!empty($data['quotTaxID'])) ? $data['quotTaxID'] : null;
        $this->quotTaxPrecentage = (!empty($data['quotTaxPrecentage'])) ? $data['quotTaxPrecentage'] : null;
        $this->quotTaxAmount = (!empty($data['quotTaxAmount'])) ? $data['quotTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

