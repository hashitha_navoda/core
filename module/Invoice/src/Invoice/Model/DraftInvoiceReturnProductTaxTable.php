<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains credit note product tax Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DraftInvoiceReturnProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveInvoiceReturnProductTax(DraftInvoiceReturnProductTax $draftInvoiceReturnProductTax)
    {
        $data = array(
            'draftInvoiceReturnProductID' => $draftInvoiceReturnProductTax->draftInvoiceReturnProductID,
            'taxID' => $draftInvoiceReturnProductTax->taxID,
            'invoiceReturnProductTaxPrecentage' => $draftInvoiceReturnProductTax->invoiceReturnProductTaxPrecentage,
            'invoiceReturnProductTaxAmount' => $draftInvoiceReturnProductTax->invoiceReturnProductTaxAmount,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getInvoiceReturnProductTaxByInvoiceReturnProductID($draftInvoiceReturnProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvoiceReturnProductTax")
                ->columns(array("*"))
                ->join("tax", "draftInvoiceReturnProductTax.taxID = tax.id", array("*"), "left")
                ->where(array("draftInvoiceReturnProductID" => $draftInvoiceReturnProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

  

    public function getInvoiceReturnTax($draftInvoiceReturnID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("draftInvoiceReturnProductTax")
                ->columns(array("*"))
                ->join("tax", "draftInvoiceReturnProductTax.taxID = tax.id", array("*"), "left")
                ->join("draftInvoiceReturnProduct", "draftInvoiceReturnProductTax.draftInvoiceReturnProductID = draftInvoiceReturnProduct.draftInvoiceReturnProductID", array("draftInvoiceReturnProductID"), "left")
                ->where(array("draftInvoiceReturnProduct.draftInvoiceReturnID" => $draftInvoiceReturnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }


}
