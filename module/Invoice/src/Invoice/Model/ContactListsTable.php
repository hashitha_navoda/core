<?php

/**
 * @author Sahan Siriwardhan <sahan@thinkcube.com>
 * This file contains contact list related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class ContactListsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //get all contact list
    public function fetchAll($paginated = false)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contactLists')
                ->columns(array('*'))
                ->join('entity', 'contactLists.entityID= entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
                ->order('contactListsID DESC');

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        // var_dump($results);die();
        return (object) $results;
    }

    //save function of contact list
    public function save(ContactLists $contactLists)
    {
        // var_dump($contactLists);die();
        $promotionData = array(
            'name' => $contactLists->name,
            'code' => $contactLists->code,
            'noOfContacts' => $contactLists->noOfContacts,
            'entityID' => $contactLists->entityID,
        );

        $this->tableGateway->insert($promotionData);
        return $this->tableGateway->lastInsertValue;
    }

    public function update($contactListData, $contactListID)
    {
        $dataSet = array(
            'name' => $contactListData->name,
            'noOfContacts' => $contactListData->noOfContacts,
        );
        $this->tableGateway->update($dataSet, array('contactListsID' => $contactListID));
        return TRUE;
    }

    public function searchContactsListForDropDown($contactSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contactLists')
                ->columns(array('*'))
                ->join('entity', 'contactLists.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
                ->where(new PredicateSet(array(new Operator('contactLists.name', 'like', '%' . $contactSearchKey . '%'),new Operator('contactLists.code', 'like', '%' . $contactSearchKey . '%')), PredicateSet::OP_OR));
        $select->group("contactLists.contactListsID");
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function getRecordByListCode($listCode){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contactLists')
                ->columns(array('*'))
                ->join('entity', 'contactLists.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'code' => $listCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        if (!$results) {
            return NULL;
        }
        return $results->current();
    }

    public function getContactListByContactListIDForSearch($contactListId){
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contactLists')
                ->columns(array('*'))
                ->join('entity', 'contactLists.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => '0', 'contactListsID' => $contactListId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        
        if (!$results) {
            return NULL;
        }
        return (object) $results;
    }
}
