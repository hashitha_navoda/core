<?php

/**
 * @author Ashan     <ashan@thinkcube.com>
 * This file contains Credit Note product tax Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CreditNoteInvoiceReturn implements InputFilterAwareInterface
{

    public $creditNoteInvoiceReturnID;
    public $creditNoteID;
    public $invoiceReturnID;
    public $creditNoteProductTaxPercentage;
    public $creditNoteTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->creditNoteInvoiceReturnID = (!empty($data['creditNoteInvoiceReturnID'])) ? $data['creditNoteInvoiceReturnID'] : null;
        $this->creditNoteID = (!empty($data['creditNoteID'])) ? $data['creditNoteID'] : null;
        $this->invoiceReturnID = (!empty($data['invoiceReturnID'])) ? $data['invoiceReturnID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
