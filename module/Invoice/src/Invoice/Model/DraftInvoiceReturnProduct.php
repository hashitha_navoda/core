<?php

/**
 * @author Ashan     <ashan@thinkcube.com>
 * This file contains Credit Note product Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DraftInvoiceReturnProduct implements InputFilterAwareInterface
{

    public $draftInvoiceReturnProductID;
    public $draftInvoiceReturnID;
    public $invoiceProductID;
    public $productID;
    public $invoiceReturnProductPrice;
    public $invoiceReturnProductDiscount;
    public $invoiceReturnProductDiscountType;
    public $invoiceReturnProductQuantity;
    public $invoiceReturnProductTotal;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->draftInvoiceReturnProductID = (!empty($data['draftInvoiceReturnProductID'])) ? $data['draftInvoiceReturnProductID'] : null;
        $this->draftInvoiceReturnID = (!empty($data['draftInvoiceReturnID'])) ? $data['draftInvoiceReturnID'] : null;
        $this->invoiceProductID = (!empty($data['invoiceProductID'])) ? $data['invoiceProductID'] : 0;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->invoiceReturnProductPrice = (!empty($data['invoiceReturnProductPrice'])) ? $data['invoiceReturnProductPrice'] : null;
        $this->invoiceReturnProductDiscount = (!empty($data['invoiceReturnProductDiscount'])) ? $data['invoiceReturnProductDiscount'] : null;
        $this->invoiceReturnProductDiscountType = (!empty($data['invoiceReturnProductDiscountType'])) ? $data['invoiceReturnProductDiscountType'] : null;
        $this->invoiceReturnProductQuantity = (!empty($data['invoiceReturnProductQuantity'])) ? $data['invoiceReturnProductQuantity'] : null;
        $this->invoiceReturnProductTotal = (!empty($data['invoiceReturnProductTotal'])) ? $data['invoiceReturnProductTotal'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
