<?php

/**
 * @author Ashan <ashan@thinkcube.com>
 * This file contains return product tax model */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ReturnsProductTax implements InputFilterAwareInterface
{

    public $salesReturnProductTaxID;
    public $salesReturnProductID;
    public $taxID;
    public $salesReturnProductTaxPrecentage;
    public $salesReturnProductTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->salesReturnProductTaxID = (!empty($data['salesReturnProductTaxID'])) ? $data['salesReturnProductTaxID'] : null;
        $this->salesReturnProductID = (!empty($data['salesReturnProductID'])) ? $data['salesReturnProductID'] : null;
        $this->taxID = (!empty($data['taxID'])) ? $data['taxID'] : null;
        $this->salesReturnProductTaxPrecentage = (!empty($data['salesReturnProductTaxPrecentage'])) ? $data['salesReturnProductTaxPrecentage'] : 0.00;
        $this->salesReturnProductTaxAmount = (!empty($data['salesReturnProductTaxAmount'])) ? $data['salesReturnProductTaxAmount'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesReturnID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesReturnCode',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
