<?php

/**
 * @author Ashan <ashan@thinkcube.com>
 * This file contains return model */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Returns implements InputFilterAwareInterface
{

    public $salesReturnID;
    public $salesReturnCode;
    public $customerID;
    public $salesReturnDate;
    public $salesReturnTotal;
    public $locationID;
    public $salesReturnComment;
    public $statusID;
    public $customCurrencyId;
    public $salesReturnCustomCurrencyRate;
    public $entityID;
    public $directReturnFlag;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->salesReturnID = (!empty($data['salesReturnID'])) ? $data['salesReturnID'] : null;
        $this->salesReturnCode = (!empty($data['salesReturnCode'])) ? $data['salesReturnCode'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->salesReturnDate = (!empty($data['salesReturnDate'])) ? $data['salesReturnDate'] : null;
        $this->salesReturnTotal = (!empty($data['salesReturnTotal'])) ? $data['salesReturnTotal'] : 0.00;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->salesReturnComment = (!empty($data['salesReturnComment'])) ? $data['salesReturnComment'] : null;
        $this->statusID = (!empty($data['statusID'])) ? $data['statusID'] : null;
        $this->customCurrencyId = (!empty($data['customCurrencyId'])) ? $data['customCurrencyId'] : null;
        $this->salesReturnCustomCurrencyRate = (!empty($data['salesReturnCustomCurrencyRate'])) ? $data['salesReturnCustomCurrencyRate'] : 0;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->directReturnFlag = (!empty($data['directReturnFlag'])) ? $data['directReturnFlag'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesReturnID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));

            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesReturnCode',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
