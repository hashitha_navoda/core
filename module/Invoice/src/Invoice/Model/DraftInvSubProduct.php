<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DraftInvSubProduct implements InputFilterAwareInterface
{

    public $draftInvSubProductID;
    public $salesInvocieProductID;
    public $productBatchID;
    public $productSerialID;
    public $salesInvoiceSubProductQuantity;
    public $salesInvoiceSubProductWarranty;
    public $salesInvoiceSubProductWarrantyType;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->draftInvSubProductID = (!empty($data['draftInvSubProductID'])) ? $data['draftInvSubProductID'] : null;
        $this->salesInvocieProductID = (!empty($data['salesInvocieProductID'])) ? $data['salesInvocieProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->salesInvoiceSubProductQuantity = (!empty($data['salesInvoiceSubProductQuantity'])) ? $data['salesInvoiceSubProductQuantity'] : null;
        $this->salesInvoiceSubProductWarranty = (!empty($data['salesInvoiceSubProductWarranty'])) ? $data['salesInvoiceSubProductWarranty'] : null;
        $this->salesInvoiceSubProductWarrantyType = (!empty($data['salesInvoiceSubProductWarrantyType'])) ? $data['salesInvoiceSubProductWarrantyType'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
