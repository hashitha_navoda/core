<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateInterface;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Predicate;

class DraftInvWfTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID, $posflag)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('draftInvWf')
                    ->columns(array('*'))
                    ->join('status', 'draftInvWf.statusID = status.statusID', array('statusName'), 'left')
                    ->join('customer', 'draftInvWf.customerID = customer.customerID', array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->join('entity', 'draftInvWf.entityID = entity.entityID', array('deleted'), 'left')
                    ->join('currency', 'draftInvWf.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->order(array('draftInvWf.salesInvoiceIssuedDate DESC', 'draftInvWf.draftInvWfID DESC'))
                    ->where(array("draftInvWf.locationID" => $locationID, "entity.deleted" => 0))
            ->where->notEqualTo("draftInvWf.statusID", 10);
            if ($posflag == 0) {
                $select->where(array("pos" => 0));
            } else {
                $select->where(array("pos" => 1));
            }
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    

    public function getInvoiceByInvoiceID($id)
    {
        $id = (int) $id;
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('draftInvWf')
                ->columns(array('*'))
                ->join('entity', 'draftInvWf.entityID = entity.entityID', array('*'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('draftInvWfID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }


    public function getAllInvoiceDetailsByInvoiceID($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('draftInvWf')
                ->columns(array('*', 'soNum' => new Expression('draftInvWf.salesOrderID')));
        $select->join('location', 'draftInvWf.locationID = location.locationID', array('*'), 'left');
        $select->join('currency', 'draftInvWf.customCurrencyId = currency.currencyID', array('*'), 'left');
        $select->join('paymentTerm', 'draftInvWf.paymentTermID = paymentTerm.paymentTermID', array('*'), 'left');
        $select->join('deliveryNote', 'draftInvWf.deliveryNoteID = deliveryNote.deliveryNoteID', array('*'), 'left');
        $select->join('customer', 'draftInvWf.customerID = customer.customerID', array('*'), 'left');
        $select->join('customerProfile', 'draftInvWf.customerProfileID = customerProfile.customerProfileID', array('*'), 'left');
        $select->join('salesPerson', 'draftInvWf.salesPersonID = salesPerson.salesPersonID', array('*'), 'left');
        $select->join('entity', 'draftInvWf.entityID = entity.entityID', array('createdBy', 'createdTimeStamp'), 'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->where(array('draftInvWfID' => $id));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getInvoiceByID($id)
    {
        $rowset = $this->tableGateway->select(array('draftInvWfID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    //use in Inventory
    public function getInvoiceBySalesInvoiceCode($id)
    {
        $rowset = $this->tableGateway->select(array('salesInvoiceCode' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getInvoiceByQuotationID($id)
    {
        $rowset = $this->tableGateway->select(array('quotationID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function updateInvoice($data, $invoiceID)
    {
        $this->tableGateway->update($data, array('draftInvWfID' => $invoiceID));
        return TRUE;
    }

    public function updatePaymentOfInvoice(Invoice $invoice)
    {
        $data = array(
            'salesInvoicePayedAmount' => $invoice->salesInvoicePayedAmount,
            'statusID' => $invoice->statusID,
        );
        $this->tableGateway->update($data, array('salesInvoiceID' => $invoice->salesInvoiceID));
        return TRUE;
    }

//use in inventory
    public function saveInvoiceData(DraftInvWf $invoice)
    {
        $data = array(
            'draftInvWfID' => $invoice->salesInvoiceID,
            'salesInvoiceCode' => $invoice->salesInvoiceCode,
            'customerID' => $invoice->customerID,
            'locationID' => $invoice->locationID,
            'salesInvoiceIssuedDate' => $invoice->salesInvoiceIssuedDate,
            'salesInvoiceOverDueDate' => $invoice->salesInvoiceOverDueDate,
            'salesInvoiceTaxAmount' => $invoice->salesInvoiceTaxAmount,
            'salesInvoiceTotalDiscount' => $invoice->salesInvoiceTotalDiscount,
            'salesinvoiceTotalAmount' => $invoice->salesinvoiceTotalAmount,
            'templateInvoiceTotal' => $invoice->templateInvoiceTotal,
            'salesInvoicePayedAmount' => $invoice->salesInvoicePayedAmount,
            'salesInvoiceComment' => $invoice->salesInvoiceComment,
            'salesInvoiceDeliveryCharge' => $invoice->salesInvoiceDeliveryCharge,
            'paymentTermID' => $invoice->paymentTermID,
            'statusID' => $invoice->statusID,
            'salesInvoiceShowTax' => $invoice->salesInvoiceShowTax,
            'quotationID' => $invoice->quotationID,
            'deliveryNoteID' => $invoice->deliveryNoteID,
            'salesOrderID' => $invoice->salesOrderID,
            'activityID' => $invoice->activityID,
            'jobID' => $invoice->jobID,
            'projectID' => $invoice->projectID,
            'entityID' => $invoice->entityID,
            'pos' => $invoice->pos,
            'salesPersonID' => $invoice->salesPersonID,
            'salesInvoiceSuspendedTax' => $invoice->salesInvoiceSuspendedTax,
            'salesInvoiceDeliveryAddress' => $invoice->salesInvoiceDeliveryAddress,
            'promotionID' => $invoice->promotionID,
            'salesInvoicePromotionDiscount' => $invoice->salesInvoicePromotionDiscount,
            'salesInvoiceWiseTotalDiscount' => $invoice->salesInvoiceWiseTotalDiscount,
            'salesInvoiceTotalDiscountType' => $invoice->salesInvoiceTotalDiscountType,
            'salesInvoiceDiscountRate' => $invoice->salesInvoiceDiscountRate,
            'customCurrencyId' => $invoice->customCurrencyId,
            'salesInvoiceCustomCurrencyRate' => $invoice->salesInvoiceCustomCurrencyRate,
            'priceListId' => $invoice->priceListId,
            'salesInvoiceDeliveryChargeEnable' => $invoice->salesInvoiceDeliveryChargeEnable,
            'customerProfileID' => $invoice->customerProfileID,
            'salesInvoiceTemporaryCode' => $invoice->salesInvoiceTemporaryCode,
            'offlineSavedTime' => $invoice->offlineSavedTime,
            'offlineUser' => $invoice->offlineUser,
            'discountOnlyOnEligible' => $invoice->discountOnlyOnEligible,
            'customerCurrentOutstanding' => $invoice->customerCurrentOutstanding,
            'inclusiveTax' => $invoice->inclusiveTax,
            'salesInvoiceServiceChargeAmount' => $invoice->salesInvoiceServiceChargeAmount,
            'invHashValue' => $invoice->invHashValue,
            'actInvData' => $invoice->actInvData,
            'isInvApproved' => $invoice->isInvApproved,
            'selectedWfID' => $invoice->selectedWfID,
            'linkedCusOrderNum' => $invoice->linkedCusOrderNum
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Array of Date range
     * @param type $startTime
     * @param type $endTime
     * @return type
     */
    public function getDailySalesDataDays($startTime, $endTime)
    {

        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();
        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Array of Month range
     * @param type $first
     * @param type $last
     * @param type $step
     * @param type $format
     * @return type
     */
    public function dateRange($first, $last, $step = '+1 month', $format = 'd-m-Y')
    {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {

            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    
    /**
     * @author SANDUN <sandun@thinkcube.com>
     *
     *
     */
    public function getItemSoldDataByItemName($item_name)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('invoiceReceipt')
                ->columns(array('TotalQty' => new Expression('SUM(quantity)')))
                ->join('invoice', 'invoice.id = invoiceReceipt.invoiceID', array('Month' => new Expression('DATE_FORMAT(`issuedDate`, "%b")')), 'left')
                ->group(array('Month'))
                ->where(array('invoiceReceipt.productName' => $item_name));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }
        $itemCountByMonth = array(
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'May' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Aug' => 0,
            'Sep' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0,
        );

        foreach ($countArray as $element) {
            switch ($element['Month']) {
                case "Jan":
                    $itemCountByMonth['Jan'] = $element['TotalQty'];
                    break;
                case "Feb":
                    $itemCountByMonth['Feb'] = $element['TotalQty'];
                    break;
                case "Mar":
                    $itemCountByMonth['Mar'] = $element['TotalQty'];
                    break;
                case "Apr":
                    $itemCountByMonth['Apr'] = $element['TotalQty'];
                    break;
                case "May":
                    $itemCountByMonth['May'] = $element['TotalQty'];
                    break;
                case "Jun":
                    $itemCountByMonth['Jun'] = $element['TotalQty'];
                    break;
                case "Jul":
                    $itemCountByMonth['Jul'] = $element['TotalQty'];
                    break;
                case "Aug":
                    $itemCountByMonth['Aug'] = $element['TotalQty'];
                    break;
                case "Sep":
                    $itemCountByMonth['Sep'] = $element['TotalQty'];
                    break;
                case "Oct":
                    $itemCountByMonth['Oct'] = $element['TotalQty'];
                    break;
                case "Nov":
                    $itemCountByMonth['Nov'] = $element['TotalQty'];
                    break;
                case "Dec":
                    $itemCountByMonth['Dec'] = $element['TotalQty'];

                    break;
            }
        }
        return $itemCountByMonth;
    }

   
    public function fetchAllInvoice()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }



    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * update statusID
     */
    public function updateInvoiceStatusID($invoiceID, $statusID)
    {
        $data = array('statusID' => $statusID);
        $this->tableGateway->update($data, array('salesInvoiceID' => $invoiceID));
        return TRUE;
    }

    public function updateInvoiceState($id, $state)
    {
        $data = array(
            'statusID' => $state,
            );
        $this->tableGateway->update($data, array('salesInvoiceID' => $id));
        return TRUE;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Date range Array
     *
     */
    public function getDatesBetween2Dates($startTime, $endTime)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();
        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i *
                    $day)));
        }

        return $days;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Month range Array
     *
     */
    public function getMonthsBetweenTwoDates($fromDate, $toDate)
    {
        $time1 = strtotime($fromDate);
        $time2 = strtotime($toDate);
        $my = date('m', $time2);

        $months = array(date('F', $time1));

        while ($time1 < $time2
        ) {
            $time1 = strtotime(date('Y-m-d', $time1) . ' +1 month');
            if (date('m', $time1) != $my && ($time1 < $time2))
                $months[] = date('F', $time1);
        }

        $months[] = date('F', $time2);
        $months = array_unique($months);
        return $months;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Month range Array
     *
     */
    public function getMonthsYearBetweenTwoDates($fromDate, $toDate)
    {
        $time1 = strtotime($fromDate);
        $time2 = strtotime($toDate);

        $my = date('mY', $time2);

        $months = array(date('F Y', $time1));

        while ($time1 <= $time2
        ) {
            $time1 = strtotime(date('Y-m', $time1) . ' +1 month');
            if (date('mY', $time1) != $my && ($time1 <= $time2))
                $months[] = date('F Y', $time1);
        }

        $months[] = date('F Y', $time2);
        $months = array_unique($months);

        return $months;
    }

   

    public function updateSalesInvoiceStatus($salesInvoiceID, $closeStatusID)
    {
        $data = array(
            'statusID' => $closeStatusID
        );
        return $this->tableGateway->update($data, array('salesInvoiceID' => $salesInvoiceID));
    }

    
    public function isOfflineInvoiceExists($offlineId)
    {
        $rowset = $this->tableGateway->select(array('salesInvoiceTemporaryCode' => $offlineId));
        if ($rowset->count()) {
            return true;
        }
        return false;
    }

    

    public function updateInvoiceRemainingDiscountValue($invoiceID, $value)
    {
        $data = array('salesInvoiceRemainingDiscValue' => $value);
        $this->tableGateway->update($data, array('salesInvoiceID' => $invoiceID));
        return TRUE;
    }

    public function updatePromotionRemainingDiscountValue($invoiceID, $value)
    {
        $data = array('salesInvoiceRemainingPromotionDiscValue' => $value);
        $this->tableGateway->update($data, array('salesInvoiceID' => $invoiceID));
        return TRUE;
    }
}
