<?php

namespace Invoice\Model;

class DraftInvoice
{
    public $draftInvoiceID;
    public $customerID;
    public $invoiceValue;
    public $attachment;
    public $productID;

    public function exchangeArray($data)
    {
        $this->draftInvoiceID = (!empty($data['draftInvoiceID'])) ? $data['draftInvoiceID'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->invoiceValue = (!empty($data['invoiceValue'])) ? $data['invoiceValue'] : null;
        $this->attachment = (!empty($data['attachment'])) ? $data['attachment'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
