<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CustomerProfile
{

    public $customerProfileID;
    public $customerProfileName;
    public $customerID;
    public $customerProfileMobileTP1;
    public $customerProfileMobileTP2;
    public $customerProfileLandTP1;
    public $customerProfileLandTP2;
    public $customerProfileFaxNo;
    public $customerProfileEmail;
    public $customerProfileContactPersonName;
    public $customerProfileContactPersonNumber;
    public $customerProfileLocationNo;
    public $customerProfileLocationRoadName1;
    public $customerProfileLocationRoadName2;
    public $customerProfileLocationRoadName3;
    public $customerProfileLocationSubTown;
    public $customerProfileLocationTown;
    public $customerProfileLocationPostalCode;
    public $customerProfileLocationCountry;
    public $isPrimary;
    public $customerCurrency;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->customerProfileID = (!empty($data['customerProfileID'])) ? $data['customerProfileID'] : null;
        $this->customerProfileName = (!empty($data['customerProfileName'])) ? $data['customerProfileName'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->customerProfileMobileTP1 = (!empty($data['customerProfileMobileTP1'])) ? $data['customerProfileMobileTP1'] : '';
        $this->customerProfileMobileTP2 = (!empty($data['customerProfileMobileTP2'])) ? $data['customerProfileMobileTP2'] : '';
        $this->customerProfileLandTP1 = (!empty($data['customerProfileLandTP1'])) ? $data['customerProfileLandTP1'] : '';
        $this->customerProfileLandTP2 = (!empty($data['customerProfileLandTP2'])) ? $data['customerProfileLandTP2'] : '';
        $this->customerProfileFaxNo = (!empty($data['customerProfileFaxNo'])) ? $data['customerProfileFaxNo'] : '';
        $this->customerProfileEmail = (!empty($data['customerProfileEmail'])) ? $data['customerProfileEmail'] : '';
        $this->customerProfileContactPersonName = (!empty($data['customerProfileContactPersonName'])) ? $data['customerProfileContactPersonName'] : '';
        $this->customerProfileContactPersonNumber = (!empty($data['customerProfileContactPersonNumber'])) ? $data['customerProfileContactPersonNumber'] : '';
        $this->customerProfileLocationNo = (!empty($data['customerProfileLocationNo'])) ? $data['customerProfileLocationNo'] : '';
        $this->customerProfileLocationRoadName1 = (!empty($data['customerProfileLocationRoadName1'])) ? $data['customerProfileLocationRoadName1'] : '';
        $this->customerProfileLocationRoadName2 = (!empty($data['customerProfileLocationRoadName2'])) ? $data['customerProfileLocationRoadName2'] : '';
        $this->customerProfileLocationRoadName3 = (!empty($data['customerProfileLocationRoadName3'])) ? $data['customerProfileLocationRoadName3'] : '';
        $this->customerProfileLocationSubTown = (!empty($data['customerProfileLocationSubTown'])) ? $data['customerProfileLocationSubTown'] : '';
        $this->customerProfileLocationTown = (!empty($data['customerProfileLocationTown'])) ? $data['customerProfileLocationTown'] : '';
        $this->customerProfileLocationPostalCode = (!empty($data['customerProfileLocationPostalCode'])) ? $data['customerProfileLocationPostalCode'] : '';
        $this->customerProfileLocationCountry = (!empty($data['customerProfileLocationCountry'])) ? $data['customerProfileLocationCountry'] : '';
        $this->customerCurrency = (!empty($data['customerCurrency'])) ? $data['customerCurrency'] : '';
        $this->isPrimary = (!empty($data['isPrimary'])) ? $data['isPrimary'] : 0;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileName',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));

        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerID',
                    'required' => true,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileMobileTP1',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileMobileTP2',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileFaxNo',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileEmail',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileLocationNo',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileLocationRoadName1',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileLocationRoadName2',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileLocationRoadName3',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileLocationSubTown',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileLocationTown',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileLocationPostalCode',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $inputFilter->add($factory->createInput(array(
                    'name' => 'customerProfileLocationCountry',
                    'required' => false,
                    'filters' => array(
                        array('name' => 'StripTags'),
                        array('name' => 'StringTrim'),
                    ),
                    'validators' => array(
                        array(
                            'name' => 'StringLength',
                            'options' => array(
                                'encoding' => 'UTF-8',
                                'min' => 1,
                                'max' => 255,
                            ),
                        ),
                    ),
        )));
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
