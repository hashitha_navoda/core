<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class RatingTypes
{

    public $ratingTypesId;
    public $ratingTypesCode;
    public $ratingTypesName;
    public $ratingTypesDescription;
    public $ratingTypesMethod;
    public $ratingTypesMethodDefinition;
    public $ratingTypesState;
    public $ratingTypesSize;
    public $entityID;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->ratingTypesId = (isset($data['ratingTypesId'])) ? $data['ratingTypesId'] : null;
        $this->ratingTypesCode = (isset($data['ratingTypesCode'])) ? $data['ratingTypesCode'] : null;
        $this->ratingTypesName = (isset($data['ratingTypesName'])) ? $data['ratingTypesName'] : null;
        $this->ratingTypesDescription = (isset($data['ratingTypesDescription'])) ? $data['ratingTypesDescription'] : null;
        $this->ratingTypesMethod = (isset($data['ratingTypesMethod'])) ? $data['ratingTypesMethod'] : 1;
        $this->ratingTypesMethodDefinition = (isset($data['ratingTypesMethodDefinition'])) ? $data['ratingTypesMethodDefinition'] : null;
        $this->ratingTypesState = (isset($data['ratingTypesState'])) ? $data['ratingTypesState'] : 1;
        $this->ratingTypesSize = (isset($data['ratingTypesSize'])) ? $data['ratingTypesSize'] : 0;
        $this->entityID = (isset($data['entityID'])) ? $data['entityID'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

    }

}
