<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Sql\Predicate\PredicateInterface;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Predicate;

class InvoiceTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID, $posflag)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('salesInvoice')
                    ->columns(array('*'))
                    ->join('status', 'salesInvoice.statusID = status.statusID', array('statusName'), 'left')
                    ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                    ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->order(array('salesInvoice.salesInvoiceIssuedDate DESC', 'salesInvoice.salesInvoiceID DESC'))
                    ->where(array("salesInvoice.locationID" => $locationID))
            ->where->notEqualTo("salesInvoice.statusID", 10);
            if ($posflag == 0) {
                $select->where(array("pos" => 0));
            } else {
                $select->where(array("pos" => 1));
            }
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function fetchAllWithVehicleNo($paginated = FALSE, $locationID, $posflag)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('salesInvoice')
                    ->columns(array('*'))
                    ->join('status', 'salesInvoice.statusID = status.statusID', array('statusName'), 'left')
                    ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerCode', 'customerStatus'), 'left')
                    ->join('job', 'job.jobId =  salesInvoice.jobID', array("jobName"), "left")
                    ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID","jobVehicleKMs"), "left")
                    ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left")
                    ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                    ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->order(array('salesInvoice.salesInvoiceIssuedDate DESC', 'salesInvoice.salesInvoiceID DESC'))
                    ->where(array("salesInvoice.locationID" => $locationID))
            ->where->notEqualTo("salesInvoice.statusID", 10);
            $select->group(array('salesInvoice.salesInvoiceID'));
            if ($posflag == 0) {
                $select->where(array("pos" => 0));
            } else {
                $select->where(array("pos" => 1));
            }
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function getLocationRelatedInvoices($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join('entity', 'salesInvoice.entityID=entity.entityID', array('*'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('deleted' => '0', 'locationID' => $locationID, 'pos' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getLocationRelatedHalfInvoices($locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join('entity', 'salesInvoice.entityID=entity.entityID', array('*'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('deleted' => '0', 'locationID' => $locationID))
        ->where->notEqualTo('salesinvoiceTotalAmount', 'salesInvoicePayedAmount');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getRelatedInvoices($promotionID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join('entity', 'salesInvoice.entityID=entity.entityID', array('*'), 'left')
                ->where(array('deleted' => '0', 'promotionID' => $promotionID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        // if (!$results) {
        //     return null;
        // }
        return $results;
    }

    public function getInvoicesByDate($fromdate, $todate, $locationID, $customerID = NULL, $posFlag = 0, $profileName = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("customerProfile", "salesInvoice.customerProfileID = customerProfile.customerProfileID", array("customerProfileName"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('salesInvoice.locationID' => $locationID))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
        ->where->notEqualTo("salesInvoice.statusID", 10);
        if ($posFlag == 0) {
            $select->where(array('salesInvoice.pos' => 0));
        } else {
            $select->where(array('salesInvoice.pos' => 1));
        }
        if ($customerID) {
            $select->where(array('salesInvoice.customerID' => $customerID));
        }
        if ($profileName) {
            $select->where(array('customerProfile.customerProfileName' => $profileName));
        }
        $select->where->between('salesInvoiceIssuedDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoicesByDateWithVehicle($fromdate, $todate, $locationID, $customerID = NULL, $posFlag = 0, $profileName = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("customerProfile", "salesInvoice.customerProfileID = customerProfile.customerProfileID", array("customerProfileName"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('job', 'job.jobId =  salesInvoice.jobID', array("jobName"), "left")
                ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID","jobVehicleKMs"), "left")
                ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left")
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('salesInvoice.locationID' => $locationID))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
        ->where->notEqualTo("salesInvoice.statusID", 10);
        if ($posFlag == 0) {
            $select->where(array('salesInvoice.pos' => 0));
        } else {
            $select->where(array('salesInvoice.pos' => 1));
        }
        if ($customerID) {
            $select->where(array('salesInvoice.customerID' => $customerID));
        }
        if ($profileName) {
            $select->where(array('customerProfile.customerProfileName' => $profileName));
        }
        $select->where->between('salesInvoiceIssuedDate', $fromdate, $todate);
        $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoicesByInovoiceComment($searchString, $locationID, $posFlag = 0)
    {
        // var_dump($searchString).die();
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("customerProfile", "salesInvoice.customerProfileID = customerProfile.customerProfileID", array("customerProfileName"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('salesInvoice.locationID' => $locationID))
                ->where(new PredicateSet(array(new Operator('salesInvoice.salesInvoiceComment', 'like', '%' . $searchString . '%')), PredicateSet::OP_OR))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
        ->where->notEqualTo("salesInvoice.statusID", 10);
        if ($posFlag == 0) {
            $select->where(array('salesInvoice.pos' => 0));
        } else {
            $select->where(array('salesInvoice.pos' => 1));
        }
        // if ($customerID) {
        //     $select->where(array('salesInvoice.customerID' => $customerID));
        // }
        // if ($profileName) {
        //     $select->where(array('customerProfile.customerProfileName' => $profileName));
        // }
        // $select->where->between('salesInvoiceIssuedDate', $fromdate, $todate);
        $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoicesBySerialCode($searchString, $locationID, $posFlag = 0)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("customerProfile", "salesInvoice.customerProfileID = customerProfile.customerProfileID", array("customerProfileName"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('salesInvoiceProduct','salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID',array('*'),'left')
                ->join('product','salesInvoiceProduct.productID = product.productID',array('*'),'left')
                ->join('salesInvoiceSubProduct','salesInvoiceProduct.salesInvoiceProductID = salesInvoiceSubProduct.salesInvocieProductID',array('*'),'left')
                ->join('productSerial','salesInvoiceSubProduct.productSerialID = productSerial.productSerialID',array('*'),'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('salesInvoice.locationID' => $locationID))
                ->where(new PredicateSet(array(new Operator('productSerial.productSerialCode', 'like', '%' . $searchString . '%')), PredicateSet::OP_OR))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
        ->where->notEqualTo("salesInvoice.statusID", 10);
        if ($posFlag == 0) {
            $select->where(array('salesInvoice.pos' => 0));
        } else {
            $select->where(array('salesInvoice.pos' => 1));
        }
        $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoicesBySerialCodeWithVehicle($searchString, $locationID, $posFlag = 0)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("customerProfile", "salesInvoice.customerProfileID = customerProfile.customerProfileID", array("customerProfileName"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('job', 'job.jobId =  salesInvoice.jobID', array("jobName"), "left")
                ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID","jobVehicleKMs"), "left")
                ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left")
                ->join('salesInvoiceProduct','salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID',array('*'),'left')
                ->join('product','salesInvoiceProduct.productID = product.productID',array('*'),'left')
                ->join('salesInvoiceSubProduct','salesInvoiceProduct.salesInvoiceProductID = salesInvoiceSubProduct.salesInvocieProductID',array('*'),'left')
                ->join('productSerial','salesInvoiceSubProduct.productSerialID = productSerial.productSerialID',array('*'),'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('salesInvoice.locationID' => $locationID))
                ->where(new PredicateSet(array(new Operator('productSerial.productSerialCode', 'like', '%' . $searchString . '%')), PredicateSet::OP_OR))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
        ->where->notEqualTo("salesInvoice.statusID", 10);
        if ($posFlag == 0) {
            $select->where(array('salesInvoice.pos' => 0));
        } else {
            $select->where(array('salesInvoice.pos' => 1));
        }
        $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoicesByInovoiceCommentWithVehicle($searchString, $locationID, $posFlag = 0)
    {
        // var_dump($searchString).die();
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("customerProfile", "salesInvoice.customerProfileID = customerProfile.customerProfileID", array("customerProfileName"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('job', 'job.jobId =  salesInvoice.jobID', array("jobName"), "left")
                ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID","jobVehicleKMs"), "left")
                ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left")
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('salesInvoice.locationID' => $locationID))
                ->where(new PredicateSet(array(new Operator('salesInvoice.salesInvoiceComment', 'like', '%' . $searchString . '%')), PredicateSet::OP_OR))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
        ->where->notEqualTo("salesInvoice.statusID", 10);
        if ($posFlag == 0) {
            $select->where(array('salesInvoice.pos' => 0));
        } else {
            $select->where(array('salesInvoice.pos' => 1));
        }
        // if ($customerID) {
        //     $select->where(array('salesInvoice.customerID' => $customerID));
        // }
        // if ($profileName) {
        //     $select->where(array('customerProfile.customerProfileName' => $profileName));
        // }
        // $select->where->between('salesInvoiceIssuedDate', $fromdate, $todate);
        $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoiceByCustomerID($id, $locationID, $customCurrencyId = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('salesInvoice.customerID' => $id, 'salesInvoice.locationID' => $locationID))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
        ->where->notEqualTo("salesInvoice.statusID", 10);
        if ($customCurrencyId) {
            $select->where(['salesInvoice.customCurrencyId' => $customCurrencyId]);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }
    public function getInvoiceByCustomerIDWithVehicle($id, $locationID, $customCurrencyId = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('job', 'job.jobId =  salesInvoice.jobID', array("jobName"), "left")
                ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID","jobVehicleKMs"), "left")
                ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left")
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('salesInvoice.customerID' => $id, 'salesInvoice.locationID' => $locationID))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
        ->where->notEqualTo("salesInvoice.statusID", 10);
        if ($customCurrencyId) {
            $select->where(['salesInvoice.customCurrencyId' => $customCurrencyId]);
        }
        $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoiceByVehicleNumber($id, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('job', 'job.jobId =  salesInvoice.jobID', array("jobName"), "left")
                ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID","jobVehicleKMs"), "left")
                ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left")
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('serviceVehicle.serviceVehicleRegNo' => $id, 'salesInvoice.locationID' => $locationID))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
        ->where->notEqualTo("salesInvoice.statusID", 10);
        $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

     public function getInvoiceByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('*'))
                ->join('quotation', 'salesInvoice.quotationID = quotation.quotationID', array('quotationCode'), 'left')
                ->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesInvoiceProductDocumentID'), 'left')
                ->join('deliveryNote', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('deliveryNoteCode'), 'left')
                ->join('salesOrders', 'salesInvoice.salesOrderID = salesOrders.soID', array('soCode'), 'left')
                ->join('activity', 'salesInvoice.activityID = activity.activityId', array('activityCode'), 'left')
                ->join('job', 'salesInvoice.jobID = job.jobId', array('jobReferenceNumber'), 'left')
                ->join('project', 'salesInvoice.projectID = project.projectId', array('projectCode'), 'left')
                ->join('customer', 'customer.customerID = salesInvoice.customerID', array('*'),'left')
                ->join('status', 'salesInvoice.statusID = status.statusID',array('statusName'),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('entity.deleted' => '0'));
                $select->where->in('salesInvoice.statusID',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('salesInvoice.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCustomerCumulativeInvoiceBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('customerID','cumulative_total' => new \Zend\Db\Sql\Expression('SUM(salesinvoiceTotalAmount)')))
                ->join('customer', 'customer.customerID = salesInvoice.customerID', array(),'left')
                ->join('status', 'salesInvoice.statusID = status.statusID',array(),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array(), 'left')
                ->where(array('entity.deleted' => '0'));
                $select->where->in('salesInvoice.statusID',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('salesInvoice.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('salesInvoice.salesInvoiceIssuedDate', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $select->group(array('salesInvoice.customerID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    /**
     * Get invoices by profile name and location id
     * @param type $profileName
     * @param type $locationID
     * @return type
     */
    public function getInvoiceByCustomerProfileName($profileName, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("customerProfile", "salesInvoice.customerProfileID = customerProfile.customerProfileID", array("customerProfileName"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where->equalTo('customerProfile.customerProfileName', $profileName)
                ->where->equalTo('salesInvoice.locationID', $locationID)
                ->where->equalTo('entity.deleted', 0);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getInvoiceByInvoiceID($id)
    {
        $id = (int) $id;
        $select = new Select();
        $select->from('salesInvoice')
                ->columns(array('*'))
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('salesInvoiceID' => $id));
        $rowset = $this->tableGateway->selectWith($select);
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getInvoiceByInvoiceIDForJE($id)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('*'), 'left');
        $select->order(array('salesInvoiceIssuedDate' => 'DESC'));
        $select->where(array('salesInvoiceID' => $id));
        $select->where->notEqualTo('salesInvoice.statusID', 5);
        $select->where->notEqualTo('salesInvoice.statusID', 10);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getAllInvoiceDetailsByInvoiceID($id)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('*', 'soNum' => new Expression('salesInvoice.salesOrderID')));
        $select->join('location', 'salesInvoice.locationID = location.locationID', array('*'), 'left');
        $select->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('*'), 'left');
        $select->join('paymentTerm', 'salesInvoice.paymentTermID = paymentTerm.paymentTermID', array('*'), 'left');
        $select->join('deliveryNote', 'salesInvoice.deliveryNoteID = deliveryNote.deliveryNoteID', array('*'), 'left');
        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('*'), 'left');
        $select->join('customerProfile', 'salesInvoice.customerProfileID = customerProfile.customerProfileID', array('*'), 'left');
        $select->join('salesPerson', 'salesInvoice.salesPersonID = salesPerson.salesPersonID', array('*'), 'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('createdBy', 'createdTimeStamp'), 'left');
        $select->join('user', 'entity.createdBy = user.userID', array('userUsername'), 'left');
        $select->where(array('salesInvoiceID' => $id));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getInvoiceByID($id)
    {
        $rowset = $this->tableGateway->select(array('salesInvoiceID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    //use in Inventory
    public function getInvoiceBySalesInvoiceCode($id)
    {
        $rowset = $this->tableGateway->select(array('salesInvoiceCode' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function getInvoiceByQuotationID($id)
    {
        $rowset = $this->tableGateway->select(array('quotationID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function updateInvoice($data, $invoiceID)
    {
        $this->tableGateway->update($data, array('salesInvoiceID' => $invoiceID));
        return TRUE;
    }

    public function updatePaymentOfInvoice(Invoice $invoice)
    {
        $data = array(
            'salesInvoicePayedAmount' => $invoice->salesInvoicePayedAmount,
            'statusID' => $invoice->statusID,
        );
        $this->tableGateway->update($data, array('salesInvoiceID' => $invoice->salesInvoiceID));
        return TRUE;
    }

//use in inventory
    public function saveInvoiceData(Invoice $invoice)
    {
        $data = array(
            'salesInvoiceID' => $invoice->salesInvoiceID,
            'salesInvoiceCode' => $invoice->salesInvoiceCode,
            'customerID' => $invoice->customerID,
            'locationID' => $invoice->locationID,
            'salesInvoiceIssuedDate' => $invoice->salesInvoiceIssuedDate,
            'salesInvoiceOverDueDate' => $invoice->salesInvoiceOverDueDate,
            'salesInvoiceTaxAmount' => $invoice->salesInvoiceTaxAmount,
            'salesInvoiceTotalDiscount' => $invoice->salesInvoiceTotalDiscount,
            'salesinvoiceTotalAmount' => $invoice->salesinvoiceTotalAmount,
            'templateInvoiceTotal' => $invoice->templateInvoiceTotal,
            'salesInvoicePayedAmount' => $invoice->salesInvoicePayedAmount,
            'salesInvoiceComment' => $invoice->salesInvoiceComment,
            'salesInvoiceDeliveryCharge' => $invoice->salesInvoiceDeliveryCharge,
            'paymentTermID' => $invoice->paymentTermID,
            'statusID' => $invoice->statusID,
            'salesInvoiceShowTax' => $invoice->salesInvoiceShowTax,
            'quotationID' => $invoice->quotationID,
            'deliveryNoteID' => $invoice->deliveryNoteID,
            'salesOrderID' => $invoice->salesOrderID,
            'activityID' => $invoice->activityID,
            'jobID' => $invoice->jobID,
            'projectID' => $invoice->projectID,
            'entityID' => $invoice->entityID,
            'pos' => $invoice->pos,
            'salesPersonID' => $invoice->salesPersonID,
            'salesInvoiceSuspendedTax' => $invoice->salesInvoiceSuspendedTax,
            'salesInvoiceDeliveryAddress' => $invoice->salesInvoiceDeliveryAddress,
            'promotionID' => $invoice->promotionID,
            'salesInvoicePromotionDiscount' => $invoice->salesInvoicePromotionDiscount,
            'salesInvoiceWiseTotalDiscount' => $invoice->salesInvoiceWiseTotalDiscount,
            'salesInvoiceTotalDiscountType' => $invoice->salesInvoiceTotalDiscountType,
            'salesInvoiceDiscountRate' => $invoice->salesInvoiceDiscountRate,
            'customCurrencyId' => $invoice->customCurrencyId,
            'salesInvoiceCustomCurrencyRate' => $invoice->salesInvoiceCustomCurrencyRate,
            'priceListId' => $invoice->priceListId,
            'salesInvoiceDeliveryChargeEnable' => $invoice->salesInvoiceDeliveryChargeEnable,
            'customerProfileID' => $invoice->customerProfileID,
            'salesInvoiceTemporaryCode' => $invoice->salesInvoiceTemporaryCode,
            'offlineSavedTime' => $invoice->offlineSavedTime,
            'offlineUser' => $invoice->offlineUser,
            'discountOnlyOnEligible' => $invoice->discountOnlyOnEligible,
            'customerCurrentOutstanding' => $invoice->customerCurrentOutstanding,
            'inclusiveTax' => $invoice->inclusiveTax,
            'salesInvoiceServiceChargeAmount' => $invoice->salesInvoiceServiceChargeAmount,
            'linkedCusOrderNum' => $invoice->linkedCusOrderNum
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param type $InvoiceID
     * @return type
     */
    public function getInvoicesforSearch($InvoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array("salesInvoiceID" => $InvoiceID))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @param type $InvoiceID
     * @return type
     */
    public function getInvoicesforSearchWithVehicle($InvoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join('job', 'job.jobId =  salesInvoice.jobID', array("jobName"), "left")
                ->join('jobVehicle', 'job.jobId =  jobVehicle.jobId', array("vehicleID","jobVehicleKMs"), "left")
                ->join('serviceVehicle', 'jobVehicle.vehicleID =  serviceVehicle.serviceVehicleID', array('serviceVehicleID', 'vehicleType', 'vehicleModel', 'serviceVehicleRegNo', 'serviceVehicleEngineNo','vehicleSubModel'), "left")
                ->join("status", "salesInvoice.statusID = status.statusID", array("*"), "left")
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array("salesInvoiceID" => $InvoiceID))
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left');
        $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * this function returns revenue for current date month and year
     */
    public function getCurrentRevenueBy($type)
    {
        $whereClause;
        if ($type == 'year') {
            $whereClause = date('Y');
        } else if ($type == 'month') {
            $whereClause = date('Y-m');
        } else {
            $whereClause = date('Y-m-d');
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%m")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%m %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%d")'),
                    'Year' => new Expression('Year(salesInvoiceIssuedDate)'),
                    'TotalRevenue' => new Expression('SUM(salesinvoiceTotalAmount)')
                ))
        ->where->notEqualTo('salesInvoice.statusID', 5)
        ->where->notEqualTo('salesInvoice.statusID', 10)
        ->where->like('salesInvoiceIssuedDate', '%' . $whereClause . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function returns revenues for daily monthly and yearly
     */
    //TODO sort invoices by not delted or cancelled
    function getRevenueData($fromData, $toData, $format = NULL)
    {
        $groupArray = array();
        if ($format == 'year') {
            $groupArray = array('Year');
        } else if ($format == 'month') {
            $groupArray = array('Month', 'Year');
        } else {
            $groupArray = array('Date', 'Month', 'Year');
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%d")'),
                    'Year' => new Expression('Year(salesInvoiceIssuedDate)'),
                    'TotalRevenue' => new Expression('SUM(salesinvoiceTotalAmount)')
                ))
                ->join('entity', 'entity.entityID = salesInvoice.entityID', '*', 'left')
                ->group($groupArray)
                ->order(array('salesInvoice.salesInvoiceIssuedDate'))
                ->where(array('entity.deleted' => 0))
        ->where->notEqualTo('salesInvoice.statusID', 5)
        ->where->notEqualTo('salesInvoice.statusID', 10)
        ->where->between('salesInvoiceIssuedDate', $fromData, $toData);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * This function returns due invoices  for period
     */
    function getDueInvoicesData($fromData, $toData)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`salesInvoiceOverDueDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`salesInvoiceOverDueDate`, "%M %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`salesInvoiceOverDueDate`, "%d")'),
                    'Year' => new Expression('Year(salesInvoiceOverDueDate)'),
                    'TotalOverdue' => new Expression('SUM(salesinvoiceTotalAmount-salesInvoicePayedAmount)')
                ))
                ->group(array('Date', 'Month', 'Year'))
                ->order(array('salesInvoice.salesInvoiceOverDueDate'))
        ->where->notEqualTo('salesInvoice.statusID', 5)
        ->where->notEqualTo('salesInvoice.statusID', 10)
        ->where->between('salesInvoiceOverDueDate', $fromData, $toData);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = array();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param date $fromDate
     * @param date $toDate
     * @return array $resultArray
     */
    public function getMonthlySalesData($fromDate, $toDate, $locationID, $cusCatgory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M %Y")'),
            'Year' => new Expression('Year(salesInvoiceIssuedDate)'),
            'TotalAmount' => new Expression('SUM(salesinvoiceTotalAmount)')));
        $select->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left');
        $select->group(array('Month', 'Year'));
        $select->order(array('salesInvoiceIssuedDate'));
        $select->where(array('deleted' => 0));
        $select->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        if($cusCatgory != ""){
            $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCatgory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }

        return $result;
    }

    public function getMonthlyInvoiceData($fromDate, $toDate, $paymentTerm, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('InvoiceNumber' => 'salesInvoiceCode',
                    'Month' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M")'),
                    'Year' => new Expression('Year(salesInvoiceIssuedDate)'),
                    'IssuedDate' => 'salesInvoiceIssuedDate',
                    'DueDate' => 'salesInvoiceOverDueDate',
                    'TotalAmount' => 'salesInvoiceTotalAmount',
                    'status' => 'statusID',
                    'PayedAmount' => 'salesInvoicePayedAmount',
                    'invoiceID' => 'salesInvoiceID'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleteStatus' => new Expression('entity.deleted')), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('CusName' => new Expression('customer.customerName'), 'CusTitle' => new Expression('customer.customerTitle'), 'customerID'), 'left')
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
                    'TotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->group(array('salesInvoiceCode'))
                ->order(array('salesInvoiceCode'))
                ->where(array('entity.deleted' => '0'))
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        if (!empty($paymentTerm)) {
            $select->where->in('salesInvoice.paymentTermID', $paymentTerm);
        }
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            if ($val['status'] == 4 || $val['status'] == 3 || $val['status'] == 6) {
                $resultArray[] = $val;
            }
        }
        return $resultArray;
    }

    public function getAnnualInvoiceData($year, $paymentTerm, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('InvoiceNumber' => 'salesInvoiceCode',
                    'Month' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M")'),
                    'IssuedDate' => 'salesInvoiceIssuedDate',
                    'DueDate' => 'salesInvoiceOverDueDate',
                    'TotalAmount' => 'salesInvoiceTotalAmount',
                    'status' => 'statusID',
                    'PayedAmount' => 'salesInvoicePayedAmount',
                    'invoiceID' => 'salesInvoiceID'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleteStatus' => new Expression('entity.deleted')), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('CusName' => new Expression('customer.customerName'), 'CusTitle' => new Expression('customer.customerTitle'), 'customerID'), 'left')
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
                    'TotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->group(array('salesInvoiceCode'))
                ->order(array('salesInvoiceCode'))
                ->where(array('entity.deleted' => '0'))
        ->where->like('salesInvoice.salesInvoiceIssuedDate', '%' . $year . '%');
        if (!empty($paymentTerm)) {
            $select->where->in('salesInvoice.paymentTermID', $paymentTerm);
        }
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            if ($val['status'] == 4 || $val['status'] == 3 || $val['status'] == 6) {
                $countArray[] = $val;
            }
        }

        return $countArray;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param date $year
     * @return array $result
     */
    public function getAnnualSalesTaxData($year, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Year' => new Expression('Year(salesInvoice.salesInvoiceIssuedDate)')))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
                    'Q1Tax' => new Expression('SUM(case when month(salesInvoice.`salesInvoiceIssuedDate`)
        BETWEEN 1 AND 3 then salesInvoiceProductTax.`salesInvoiceProductTaxAmount` end)'),
                    'Q2Tax' => new Expression('SUM(case when month(salesInvoice.`salesInvoiceIssuedDate`)
        BETWEEN 4 AND 6 then salesInvoiceProductTax.`salesInvoiceProductTaxAmount` end)'),
                    'Q3Tax' => new Expression('SUM(case when month(salesInvoice.`salesInvoiceIssuedDate`)
        BETWEEN 7 AND 9 then salesInvoiceProductTax.`salesInvoiceProductTaxAmount` end)'),
                    'Q4Tax' => new Expression('SUM(case when month(salesInvoice.`salesInvoiceIssuedDate`)
        BETWEEN 10 AND 12 then salesInvoiceProductTax.`salesInvoiceProductTaxAmount` end)')), 'left')
                ->join('entity', 'entity.entityID = salesInvoice.entityID', array('deleted'), 'left')
                ->group(array('Year'))
                ->where(array('deleted' => 0))
        ->where->like('salesInvoice.salesInvoiceIssuedDate', '%' . $year . '%');
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        if($cusCategory !=""){
            $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    public function getAnnualCreditNoteProductData($year, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('Year' => new Expression('Year(creditNote.creditNoteDate)'),'creditNoteID',
                    'Q1CreditNote' => new Expression('SUM(case when month(creditNote.`creditNoteDate`)
        BETWEEN 1 AND 3 then (creditNote.`creditNoteTotal`) end)'),
                    'Q2CreditNote' => new Expression('SUM(case when month(creditNote.`creditNoteDate`)
        BETWEEN 4 AND 6 then (creditNote.`creditNoteTotal`) end)'),
                    'Q3CreditNote' => new Expression('SUM(case when month(creditNote.`creditNoteDate`)
        BETWEEN 7 AND 9 then (creditNote.`creditNoteTotal`) end)'),
                    'Q4CreditNote' => new Expression('SUM(case when month(creditNote.`creditNoteDate`)
        BETWEEN 10 AND 12 then (creditNote.`creditNoteTotal`) end)')))
                ->join('entity', 'entity.entityID = creditNote.entityID', array('deleted'), 'left')
                ->group(array('Year'))
                ->where(array('deleted' => 0))
        ->where->like('creditNote.creditNoteDate', '%' . $year . '%');
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    public function getAnnualCreditNoteProductTaxData($year, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('Year' => new Expression('Year(creditNote.creditNoteDate)'),'creditNoteID'))
                ->join('creditNoteProduct', 'creditNote.creditNoteID = creditNoteProduct.creditNoteID', array('creditNoteProductID'), 'left')
                ->join('creditNoteProductTax', 'creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID', array(
                    'Q1CreditNoteTax' => new Expression('SUM(case when month(creditNote.`creditNoteDate`)
        BETWEEN 1 AND 3 then creditNoteProductTax.`creditNoteTaxAmount` end)'),
                    'Q2CreditNoteTax' => new Expression('SUM(case when month(creditNote.`creditNoteDate`)
        BETWEEN 4 AND 6 then creditNoteProductTax.`creditNoteTaxAmount` end)'),
                    'Q3CreditNoteTax' => new Expression('SUM(case when month(creditNote.`creditNoteDate`)
        BETWEEN 7 AND 9 then creditNoteProductTax.`creditNoteTaxAmount` end)'),
                    'Q4CreditNoteTax' => new Expression('SUM(case when month(creditNote.`creditNoteDate`)
        BETWEEN 10 AND 12 then creditNoteProductTax.`creditNoteTaxAmount` end)')), 'left')
                ->join('entity', 'entity.entityID = creditNote.entityID', array('deleted'), 'left')
                ->group(array('Year'))
                ->where(array('deleted' => 0))
        ->where->like('creditNote.creditNoteDate', '%' . $year . '%');
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    public function getDailyInvoiceData($fromDate, $toDate, $paymentTerm, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('InvoiceNumber' => 'salesInvoiceCode',
                    'Day' => new Expression('salesInvoice.salesInvoiceIssuedDate'),
                    'IssuedDate' => 'salesInvoiceIssuedDate',
                    'DueDate' => 'salesInvoiceOverDueDate',
                    'TotalAmount' => 'salesInvoiceTotalAmount',
                    'status' => 'statusID',
                    'PayedAmount' => 'salesInvoicePayedAmount',
                    'invoiceID' => 'salesInvoiceID'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleteStatus' => new Expression('entity.deleted')), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('CusName' => new Expression('customer.customerName'), 'CusTitle' => new Expression('customer.customerTitle'), 'customerID'), 'left')
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
                    'TotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->group(array('salesInvoiceCode'))
                ->order(array('salesInvoiceCode'))
                ->where(array('entity.deleted' => '0'))
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        if (!empty($paymentTerm)) {
            $select->where->in('salesInvoice.paymentTermID', $paymentTerm);
        }
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            if ($val['status'] == 4 || $val['status'] == 3 || $val['status'] == 6) {
                $countArray[] = $val;
            }
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param date $fromDate
     * @param date $toDate
     * @return array $result
     */
    public function getMonthlySalesTaxData($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M %Y")'),
                    'Year' => new Expression('Year(salesInvoiceIssuedDate)')))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
                    'TotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left')
                ->group(array('Month', 'Year'))
                ->order(array('salesInvoice.salesInvoiceIssuedDate'))
                ->where(array('deleted' => 0))
        ->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }

        return $result;
    }

    public function getInvoiceSummeryWithOutTax($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('InvoiceNumber' => 'salesInvoiceCode',
                    'IssuedDate' => 'salesInvoiceIssuedDate',
                    'DueDate' => 'salesInvoiceOverDueDate',
                    'TotalAmount' => 'salesInvoiceTotalAmount',
                    'status' => 'statusID',
                    'PayedAmount' => 'salesInvoicePayedAmount'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleteStatus' => new Expression('entity.deleted')), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerTitle', 'customerID', 'customerShortName'), 'left')
                ->group(array('salesInvoice.salesInvoiceCode'))
                ->order(array('customer.customerName'))
                ->where(array('entity.deleted' => '0'))
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();
        foreach ($results as $val) {
            if ($val['status'] == 4 || $val['status'] == 3 || $val['status'] == 6) {
                $countArray[] = $val;
            }
        }
        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @return array $result
     */
    public function getMonthlySalesDataYears($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Year' => new Expression('Year(salesInvoiceIssuedDate)')))
                ->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left')
                ->group(array('Year'))
                ->order(array('salesInvoiceIssuedDate'))
                ->where(array('deleted' => 0))
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = array();
        foreach ($results as $val) {
            array_push($result, $val['Year']);
        }
        return $result;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getMonthlySalesDataYear($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('invoice')
                ->columns(array('Year' => new Expression('Year(issuedDate)')))
                ->group(array('Year'))
                ->order(array('issuedDate'))
        ->where->between('issuedDate', $fromDate, $toDate);


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getMonthlySalesDataGraph($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('invoice')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`issuedDate`, "%b")'),
                    'Year' => new Expression('Year(issuedDate)'),
                    'TotalTax' => new Expression('SUM(taxAmount)'),
                    'TotalAmount' => new Expression('SUM(TotalAmount)'),
                    'TotalPayed' => new Expression('SUM(payedAmount)')))
                ->group(array('Month', 'Year'))
                ->order(array('issuedDate'))
        ->where->between('issuedDate', $fromDate, $toDate);


        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * between two days sales function
     * @param date $fromDate
     * @param date $toDate
     * @return array $result
     */
    public function getDailySalesData($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategory = NULL, $withDefaultSalesPerson =false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('Day' => new Expression('salesInvoice.salesInvoiceIssuedDate'),
            'InvID' => new Expression('salesInvoice.salesInvoiceCode'),
            'InvNo' => new Expression('salesInvoice.salesInvoiceID'),
            'Amount' => new Expression('(salesInvoice.salesinvoiceTotalAmount)'),
            'salesPersonID', 'salesInvoiceID',
        ));
        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('CusName' => new Expression('customer.customerName'), 'CusTitle' => new Expression('customer.customerTitle')), 'left');
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left');
        $select->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
            'Tax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left');
        $select->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left');
        // $select->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left');
        // $select->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left');
        $select->group(array('salesInvoice.salesInvoiceID'));
        $select->where(array('deleted' => 0));
        $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        // if ($salesPersonIds != NULL) {
        //     if($withDefaultSalesPerson) {
        //             $select->where->AND->NEST
        //             ->in('invoiceSalesPersons.salesPersonID', $salesPersonIds)->orPredicate(
        //                 new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
        //             );
        //     } else {
        //         $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonIds);
        //     }
        // }
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        // $result = [];
        // foreach ($results as $val) {
        //     $result[] = $val;
        // }


        return $results;
    }

    public function getInvoiceRelatedTaxDetails($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategory = NULL, $withDefaultSalesPerson = false){
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('salesInvoiceID', 'salesInvoiceSuspendedTax'));
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left');
        $select->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array('*'), 'left');
        $select->join("tax", "salesInvoiceProductTax.taxID = tax.id", array("taxName"), "left");
        // $select->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left');
        // $select->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left');
        $select->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left');
        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerTitle'), 'left');
        $select->where(array('deleted' => 0));
        $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        // if ($salesPersonIds != NULL) {
        //     if($withDefaultSalesPerson) {
        //             $select->where->AND->NEST
        //             ->in('invoiceSalesPersons.salesPersonID', $salesPersonIds)->orPredicate(
        //                 new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
        //             );
        //     } else {
        //         $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonIds);
        //     }
        // }
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        if($cusCategory != ""){
            
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getDailySalesCreditNoteData($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategory = NULL, $withDefaultSalesPerson = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('Day' => new Expression('creditNote.creditNoteDate'),
            'creditNoteID','creditNoteCode','creditNoteProductTotal' => new Expression('SUM(creditNote.creditNoteTotal)')
        ));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->join("salesInvoice", "salesInvoice.salesInvoiceID = creditNote.invoiceID", array('salesPersonID', 'salesinvoiceID'), 'left');
        $select->join("customer", "creditNote.customerID = customer.customerID", array('CusTitle' => new Expression('customer.customerTitle'),'CusName' => new Expression('customer.customerName')), 'left');
        $select->group(array('creditNote.creditNoteID'));
        $select->where(array('deleted' => 0));
        $select->where->between('creditNote.creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }

        return $result;
    }


    public function getDailySalesCreditNoteTaxData($fromDate, $toDate, $salesPersonIds, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('creditNoteID'));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->join("salesInvoice", "salesInvoice.salesInvoiceID = creditNote.invoiceID", array('salesInvoiceSuspendedTax'), 'left');
        $select->join("creditNoteProduct", "creditNote.creditNoteID = creditNoteProduct.creditNoteID", array('creditNoteProductID'), 'left');
        $select->join("creditNoteProductTax", "creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID", array('*'), 'left');
        $select->where(array('entity.deleted' => 0));
        $select->where->between('creditNote.creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * invoice daily sales items
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @return array $resultArray
     */
    public function getInvoicedDailySalesItems($fromDate, $toDate, $locationID = NULL, $itemFlag = FALSE)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('productID',
            'salesInvoiceProductPrice',
            'salesInvoiceProductQuantity',
            'salesInvoiceProductDiscountType',
            'salesInvoiceProductDiscount',
            'salesInvoiceProductTotal'), 'left');
        $select->join('product', 'salesInvoiceProduct.productID = product.productID', array('productName' => new Expression('product.productName'), 'productCode' => new Expression('product.productCode')), 'left');
        $select->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left');
        $select->where([
            'salesInvoice.pos' => '0',
            'deleted' => 0]);
        $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        if ($locationID) {
            $select->where([
                'salesInvoice.locationID' => $locationID]
            );
        }
        if ($itemFlag) {
            $select->where->equalTo('productTypeID', 2);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = [];
        foreach ($results as $val) {
            $resultSet[] = $val;
        }
        return $resultSet;
    }

    /**
     * pos daily sales invoice items
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @return array $resultArray
     */
    public function getPosDailySalesItems($fromDate, $toDate, $location = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('*'))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('productID', 'salesInvoiceProductQuantity' => new Expression('COALESCE(SUM(salesInvoiceProductQuantity),0)'),
                    'salesInvoiceProductTotal' => new Expression('SUM(salesInvoiceProductTotal)')), 'left')
                ->join('product', 'salesInvoiceProduct.productID = product.productID', array(
                    'productCode',
                    'productName' => new Expression('product.productName')), 'left')
                ->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left')
                ->group(array('salesInvoiceProduct.productID'))
                ->where(array('salesInvoice.locationID' => $location,
                    'salesInvoice.pos' => '1',
                    'deleted' => 0))
        ->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultArray = [];
        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    /**
     * pos and Invoice daily sales invoice items
     * @author sharmilan <sharmilan@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @return array
     */
    public function getPosAndinvoiceDailySalesItems($fromDate, $toDate, $location = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('*'))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('productID', 'salesInvoiceProductQuantity' => new Expression('COALESCE(SUM(salesInvoiceProductQuantity),0)'),
                    'salesInvoiceProductTotal' => new Expression('SUM(salesInvoiceProductTotal)')), 'left')
                ->join('product', 'salesInvoiceProduct.productID = product.productID', array(
                    'productCode',
                    'productName' => new Expression('product.productName')), 'left')
                ->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left')
                ->group(array('salesInvoiceProduct.productID'))
                ->where(array(
                    'salesInvoice.locationID' => $location,
                    'deleted' => 0)
                )
        ->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @return $countArray
     */
    public function getDailySalesGraphData($fromDate, $toDate)
    {

        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array(
                    'Date' => new Expression('salesInvoiceIssuedDate'),
                    'TotalAmount' => new Expression('SUM(salesinvoiceTotalAmount)')))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
                    'TotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->group(array('salesInvoiceIssuedDate'))
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }
        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Array of Date range
     * @param type $startTime
     * @param type $endTime
     * @return type
     */
    public function getDailySalesDataDays($startTime, $endTime)
    {

        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();
        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        return $days;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Array of Month range
     * @param type $first
     * @param type $last
     * @param type $step
     * @param type $format
     * @return type
     */
    public function dateRange($first, $last, $step = '+1 month', $format = 'd-m-Y')
    {

        $dates = array();
        $current = strtotime($first);
        $last = strtotime($last);

        while ($current <= $last) {

            $dates[] = date($format, $current);
            $current = strtotime($step, $current);
        }

        return $dates;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $from_date
     * @param type $to_date
     * @return type
     */
    public function getDailySalesDataTotalTax($from_date, $to_date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Date' => new Expression('salesInvoice.salesInvoiceIssuedDate')))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array('TotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->group(array('salesInvoice.salesInvoiceIssuedDate'))
        ->where->between('salesInvoice.salesInvoiceIssuedDate', $from_date, $to_date);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param date $year
     * @return array $result
     */
    public function getAnnualSalesData($year, $locationID, $cusCategories = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Year' => new Expression('Year(salesInvoice.salesInvoiceIssuedDate)'),
                    'Q1Amount' => new Expression('SUM(case when month(salesInvoice.`salesInvoiceIssuedDate`)
        BETWEEN 1 AND 3 then salesInvoice.`salesinvoiceTotalAmount` end)'),
                    'Q2Amount' => new Expression('SUM(case when month(salesInvoice.`salesInvoiceIssuedDate`)
        BETWEEN 4 AND 6 then salesInvoice.`salesinvoiceTotalAmount` end)'),
                    'Q3Amount' => new Expression('SUM(case when month(salesInvoice.`salesInvoiceIssuedDate`)
        BETWEEN 7 AND 9 then salesInvoice.`salesinvoiceTotalAmount` end)'),
                    'Q4Amount' => new Expression('SUM(case when month(salesInvoice.`salesInvoiceIssuedDate`)
        BETWEEN 10 AND 12 then salesInvoice.`salesinvoiceTotalAmount` end)')))
                ->join('entity', 'entity.entityID = salesInvoice.entityID', array('deleted'), 'left')
                ->group(array('Year'))
                ->where(array('deleted' => 0))
        ->where->like('salesInvoice.salesInvoiceIssuedDate', '%' . $year . '%');
        if ($locationID != "") {
            $select->where->in("salesInvoice.locationID", $locationID);
        }
        if($cusCategories != ""){
            $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategories);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param date $year
     * @return array $result
     */
    public function getAnnualSalesGraphData($year)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('invoice')
                ->columns(array('Year' => new Expression('Year(invoice.issuedDate)'),
                    'Q1Amount' => new Expression('SUM(case when month(invoice.`issuedDate`)
        BETWEEN 1 AND 3 then invoice.`totalAmount` end)'),
                    'Q2Amount' => new Expression('SUM(case when month(invoice.`issuedDate`)
        BETWEEN 4 AND 6 then invoice.`totalAmount` end)'),
                    'Q3Amount' => new Expression('SUM(case when month(invoice.`issuedDate`)
        BETWEEN 7 AND 9 then invoice.`totalAmount` end)'),
                    'Q4Amount' => new Expression('SUM(case when month(invoice.`issuedDate`)
        BETWEEN 10 AND 12 then invoice.`totalAmount` end)'),
                    'Q1Tax' => new Expression('SUM(case when month(invoice.`issuedDate`)
        BETWEEN 1 AND 3 then taxAmount.`amount` end)'),
                    'Q2Tax' => new Expression('SUM(case when month(invoice.`issuedDate`)
        BETWEEN 4 AND 6 then taxAmount.`amount` end)'),
                    'Q3Tax' => new Expression('SUM(case when month(invoice.`issuedDate`)
        BETWEEN 7 AND 9 then taxAmount.`amount` end)'),
                    'Q4Tax' => new Expression('SUM(case when month(invoice.`issuedDate`)
        BETWEEN 10 AND 12 then taxAmount.`amount` end)')))
                ->join('taxAmount', 'invoice.id = taxAmount.refID', array('Tax' => new Expression('SUM(taxAmount.amount)')), 'left')
                ->group(array('Year'))
        ->where->like('invoice.issuedDate', '%' . $year . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }

        return $result;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * This function is given sales Summery data within two date
     * @param date $fromDate
     * @param date $toDate
     * @return array $result
     */
    public function getSalesSummeryWithOutTax($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID', 'salesInvoiceIssuedDate', 'TotalAmount' => new Expression('SUM(salesinvoiceTotalAmount)'), 'customerID', 'salesInvoiceSuspendedTax', 'salesInvoiceCode','salesInvoiceComment'))
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerTitle','customerTelephoneNumber', 'customerCode'), 'left')
                ->join('customerProfile', 'customer.customerID = customerProfile.customerID', array('customerProfileLocationNo', 'customerProfileLocationRoadName1','customerProfileLocationRoadName2','customerProfileLocationRoadName3', 'customerProfileLocationTown'), 'left')
                ->join('entity', 'entity.entityID = salesInvoice.entityID', array('deleted'), 'left')
                ->group(array('salesInvoice.salesInvoiceID'))
                ->order(array('customerID'))
                ->where(array('deleted' => 0))
                ->where(array('customerProfile.isPrimary' => 1))
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in("salesInvoice.locationID", $locationID);
        }
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    public function getSalesSummeryCreditNoteData($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('creditNoteID','creditNoteDate','customerID','creditNoteProductTotal' => new Expression('SUM(creditNote.creditNoteTotal)')));
        $select->join('customer', 'creditNote.customerID = customer.customerID', array('customerName', 'customerTitle'), 'left');
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->group(array('creditNoteID'));
        $select->order(array('customerID'));
        $select->where(array('deleted' => 0));
        $select->where->between('creditNote.creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }

        return $result;
    }

    public function getSalesSummeryCreditNoteTaxData($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('creditNoteID','creditNoteDate','customerID'));
        $select->join('customer', 'creditNote.customerID = customer.customerID', array('customerName', 'customerTitle'), 'left');
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->join("creditNoteProduct", "creditNote.creditNoteID = creditNoteProduct.creditNoteID", array('creditNoteProductID'), 'left');
        $select->join("creditNoteProductTax", "creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID", array(
            'creditNoteProductTax' => new Expression('SUM(creditNoteProductTax.creditNoteTaxAmount)')), 'left');
        $select->group(array('creditNoteID'));
        $select->order(array('customerID'));
        $select->where(array('deleted' => 0));
        $select->where->between('creditNote.creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }

        return $result;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * This file contains sales Summery data functions
     * @param date $fromDate
     * @param date $toDate
     * @param int $invoiceID
     * @return array $result
     */
    public function getSalesSummeryWithTax($fromDate, $toDate, $invoiceID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('customerID', 'salesInvoiceSuspendedTax'))
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerTitle'), 'left')
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
                    'TotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('entity', 'entity.entityID = salesInvoice.entityID', array('deleted'), 'left')
                ->group(array('salesInvoice.salesInvoiceID'))
                ->order(array('salesInvoiceIssuedDate'))
                ->where(array('deleted' => 0))
                ->where(array('salesInvoice.salesInvoiceID' => $invoiceID))
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param date $fromDate
     * @return array $result
     */
    public function getDailyInvoiceByDate($date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('invoice')
                ->order(array('issuedDate'))
                ->where(array('issuedDate' => $date));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }

        return $result;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param type $customer_name
     * @param type $fromDate
     * @return type
     */
    public function getDailyInvoiceByDateCusName($cusName, $date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('invoice')
                ->order(array('issuedDate'))
                ->where(array('customerName' => $cusName))
                ->where(array('issuedDate' => $date));

        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }
        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * This file contains CustomerTransaction Report related controller functions
     *
     */
    public function getInvoiceByCustomerName($customer_name, $fromDate = null, $toDate = null)
    {
        if (is_null($fromDate) AND is_null($toDate)) {
            $rowset = $this->tableGateway->select(array('customerName' => $name));
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } else {

            $sql = new Sql($this->tableGateway->getAdapter());

            $select = $sql->select();

            $select->from('invoice')
                    ->order(array('issuedDate'))
                    ->where(array('customerName' => $customer_name))
            ->where->between('issuedDate', $fromDate, $toDate);

            $statement = $sql->prepareStatementForSqlObject($select);
            $results = $statement->execute();

            foreach ($results as $val) {
                $countArray[] = $val;
            }

            return $countArray;
        }
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Get Customer Balance data functions
     *
     */
    public function getCustomerBalances($customerID, $from_date, $to_date)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('invoice')
                ->columns(array('customerName'))
                ->join('payement', 'invoice.customerID = payement.customerID', array('customerID'), 'left')
                ->join('creditNote', 'creditNote.customerID = payement.customerID', array('customerID'))
                ->where(array('invoice.customerID' => $customerID))
        ->where->between('payement.date', $from_date, $to_date);

        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Get Invoice Balance data function
     *
     */
    public function getInvoiceBalancesByName($customer_name, $fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('invoice')
                ->columns(array('totAmountInv' => new Expression('SUM(totalAmount)'),
                    'totSetledInv' => new Expression('SUM(payedAmount)')))
                ->order(array('issuedDate'))
                ->where(array('customerName' => $customer_name))
        ->where->between('issuedDate', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /////////////////////END///////////////////////
    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Get Invoice Balance data function
     * @param type $fromDate
     * @param type $toDate
     * @return type
     */
    public function getInvoiceBalances($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('invoice')
                ->columns(array('CusID' => new Expression('customerID'),
                    'CusName' => new Expression('customerName'),
                    'totAmountInv' => new Expression('SUM(totalAmount)'),
                    'totSetledInv' => new Expression('SUM(payedAmount)')))
                ->order(array('customerID'))
                ->group(array('customerID'))
                ->where('customerID IS NOT NULL')
        ->where->between('issuedDate', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * This file contains Invoice Bar Chart functions
     *
     */
    public function getAnnuvalInvoices($year)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('invoice')
                ->columns(array('totAmountInv' => new Expression('COUNT(id)'),
                    'Month' => new Expression('DATE_FORMAT(`issuedDate`, "%b")')))
                ->group(array('Month'))
                ->order(array('issuedDate'))
        ->where->like('issuedDate', '%' . $year . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }
        $invoiceCountByMonth = array(
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'May' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Aug' => 0,
            'Sep' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0,
        );

        foreach ($countArray as $element) {
            switch ($element['Month']) {
                case "Jan":
                    $invoiceCountByMonth['Jan'] = $element['totAmountInv'];
                    break;
                case "Feb":
                    $invoiceCountByMonth['Feb'] = $element['totAmountInv'];
                    break;
                case "Mar":
                    $invoiceCountByMonth['Mar'] = $element['totAmountInv'];
                    break;
                case "Apr":
                    $invoiceCountByMonth['Apr'] = $element['totAmountInv'];
                    break;
                case "May":
                    $invoiceCountByMonth['May'] = $element['totAmountInv'];
                    break;
                case "Jun":
                    $invoiceCountByMonth['Jun'] = $element['totAmountInv'];
                    break;
                case "Jul":
                    $invoiceCountByMonth['Jul'] = $element['totAmountInv'];
                    break;
                case "Aug":
                    $invoiceCountByMonth['Aug'] = $element['totAmountInv'];
                    break;
                case "Sep":
                    $invoiceCountByMonth['Sep'] = $element['totAmountInv'];
                    break;
                case "Oct":
                    $invoiceCountByMonth['Oct'] = $element['totAmountInv'];
                    break;
                case "Nov":
                    $invoiceCountByMonth['Nov'] = $element['totAmountInv'];
                    break;
                case "Dec":
                    $invoiceCountByMonth['Dec'] = $element['totAmountInv'];

                    break;
            }
        }
        return $invoiceCountByMonth;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     *
     *
     */
    public function getItemSoldDataByItemName($item_name)
    {
        $sql = new Sql($this->tableGateway->getAdapter());

        $select = $sql->select();

        $select->from('invoiceReceipt')
                ->columns(array('TotalQty' => new Expression('SUM(quantity)')))
                ->join('invoice', 'invoice.id = invoiceReceipt.invoiceID', array('Month' => new Expression('DATE_FORMAT(`issuedDate`, "%b")')), 'left')
                ->group(array('Month'))
                ->where(array('invoiceReceipt.productName' => $item_name));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }
        $itemCountByMonth = array(
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'May' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Aug' => 0,
            'Sep' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0,
        );

        foreach ($countArray as $element) {
            switch ($element['Month']) {
                case "Jan":
                    $itemCountByMonth['Jan'] = $element['TotalQty'];
                    break;
                case "Feb":
                    $itemCountByMonth['Feb'] = $element['TotalQty'];
                    break;
                case "Mar":
                    $itemCountByMonth['Mar'] = $element['TotalQty'];
                    break;
                case "Apr":
                    $itemCountByMonth['Apr'] = $element['TotalQty'];
                    break;
                case "May":
                    $itemCountByMonth['May'] = $element['TotalQty'];
                    break;
                case "Jun":
                    $itemCountByMonth['Jun'] = $element['TotalQty'];
                    break;
                case "Jul":
                    $itemCountByMonth['Jul'] = $element['TotalQty'];
                    break;
                case "Aug":
                    $itemCountByMonth['Aug'] = $element['TotalQty'];
                    break;
                case "Sep":
                    $itemCountByMonth['Sep'] = $element['TotalQty'];
                    break;
                case "Oct":
                    $itemCountByMonth['Oct'] = $element['TotalQty'];
                    break;
                case "Nov":
                    $itemCountByMonth['Nov'] = $element['TotalQty'];
                    break;
                case "Dec":
                    $itemCountByMonth['Dec'] = $element['TotalQty'];

                    break;
            }
        }
        return $itemCountByMonth;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param date $year
     * @return array $itemCountByMonth
     */
    public function getCashFlowDataYear($year)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('invoice')
                ->columns(array('TotalAmount' => new Expression('SUM(totalAmount)'),
                    'Month' => new Expression('DATE_FORMAT(`issuedDate`, "%b")')))
                ->group(array('Month'))
                ->order(array('issuedDate'))
        ->where->like('issuedDate', '%' . $year . '%');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }
        $itemCountByMonth = array(
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'May' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Aug' => 0,
            'Sep' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0,
        );

        foreach ($countArray as $element) {
            switch ($element['Month']) {
                case "Jan":
                    $itemCountByMonth['Jan'] = $element['TotalAmount'];
                    break;
                case "Feb":
                    $itemCountByMonth['Feb'] = $element['TotalAmount'];
                    break;
                case "Mar":
                    $itemCountByMonth['Mar'] = $element['TotalAmount'];
                    break;
                case "Apr":
                    $itemCountByMonth['Apr'] = $element['TotalAmount'];
                    break;
                case "May":
                    $itemCountByMonth['May'] = $element['TotalAmount'];
                    break;
                case "Jun":
                    $itemCountByMonth['Jun'] = $element['TotalAmount'];
                    break;
                case "Jul":
                    $itemCountByMonth['Jul'] = $element['TotalAmount'];
                    break;
                case "Aug":
                    $itemCountByMonth['Aug'] = $element['TotalAmount'];
                    break;
                case "Sep":
                    $itemCountByMonth['Sep'] = $element['TotalAmount'];
                    break;
                case "Oct":
                    $itemCountByMonth['Oct'] = $element['TotalAmount'];
                    break;
                case "Nov":
                    $itemCountByMonth['Nov'] = $element['TotalAmount'];
                    break;
                case "Dec":
                    $itemCountByMonth['Dec'] = $element['TotalAmount'];
                    break;
            }
        }
        return $itemCountByMonth;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * get Aged Customer data
     * @param Array $cusIds
     * @param Date $endDate
     * @param Boolean $groupBy
     * @return Array $agedAnalysisData
     */
    public function getAgedCusInvoiceData($cusIds, $endDate, $groupBy, $isAllCustomers = false, $cusCategory = false, $salesPerson = [], $sortBy, $withDefaultSalesPerson = false)
    {
        $agedAnalysisData = [];
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(['salesInvoiceIssuedDate','salesInvoiceCode','salesInvoiceID', 'customerID','salesinvoiceTotalAmount','statusID', 'entityID']);
        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerShortName', 'customerTitle', 'customerCode'), 'left');
        $select->join('invoiceEditLog', 'salesInvoice.salesInvoiceID = invoiceEditLog.salesInvoiceID', array('dateAndTime'), 'left');
        $select->join('invoiceEditLogDetails', 'invoiceEditLog.invoiceEditLogID = invoiceEditLogDetails.invoiceEditLogID', array('invoiceEditLogDetailsCategory','invoiceEditLogDetailsNewState'), 'left');

        if (!empty($salesPerson)) {
            $select->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left');
            $select->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left');
            // $select->join('salesPerson', 'salesInvoice.salesPersonID = salesPerson.salesPersonID', array('salesPersonSortName','salesPersonID'), 'left');
            if ($withDefaultSalesPerson) {
                $select->where->AND->NEST
                    ->in('invoiceSalesPersons.salesPersonID', $salesPerson)
                    ->orPredicate(new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID'));
            } else {
                $select->where->in('invoiceSalesPersons.salesPersonID', $salesPerson);
            }
        }
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted', 'deletedTimeStamp'), 'left');
        
        if ($sortBy == "invoiceNo") {
            $select->order(array('salesInvoice.salesInvoiceID DESC'));
        } else if ($sortBy == "invoiceDate") {
            $select->order(array('salesInvoice.salesInvoiceIssuedDate DESC'));
        } else if ($sortBy == "customerName") {
            $select->order(array('customer.customerName ASC'));
        } else {
            $select->order(array('salesInvoice.salesInvoiceID DESC'));
        }

	$select->group('salesInvoice.salesInvoiceID');
        if ($groupBy) {
            $select->group('customer.customerID');
        }
        $select->where->lessThanOrEqualTo('salesInvoiceIssuedDate', $endDate);
        if ($cusIds && !$isAllCustomers) {
            $select->where->in('salesInvoice.customerID', $cusIds);
        }
        $select->where->notEqualTo('salesInvoice.customerID', 0);

        // $select->where->notEqualTo('salesInvoice.statusID', 5);
        // $select->where->notEqualTo('salesInvoice.statusID', 10);
        $select->where->AND->NEST
            ->in('salesInvoice.statusID', [3,6,5])
            ->or->equalTo('salesInvoice.statusID', 4)
                ->equalTo('invoiceEditLogDetails.invoiceEditLogDetailsCategory', 'salesInvoiceStatusClosed')
                ->greaterThan('invoiceEditLog.dateAndTime', $endDate);

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $agedAnalysisData[] = $val;
        }

        return $agedAnalysisData;
    }

    public function getAgedCusInvoiceAdvanceData($cusIds, $endDate, $groupBy, $isAllCustomers = false, $cusCategory = false, $salesPerson = [], $sortBy, $withDefaultSalesPerson = false)
    {
        $agedAnalysisData = [];
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(['salesInvoiceIssuedDate','salesInvoiceCode', 'salesInvoiceID', 'customerID','salesinvoiceTotalAmount', 'statusID']);
        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerShortName', 'customerTitle', 'customerCode'), 'left');
        $select->join('invoiceEditLog', 'salesInvoice.salesInvoiceID = invoiceEditLog.salesInvoiceID', array('dateAndTime'), 'left');
        $select->join('invoiceEditLogDetails', 'invoiceEditLog.invoiceEditLogID = invoiceEditLogDetails.invoiceEditLogID', array('invoiceEditLogDetailsCategory','invoiceEditLogDetailsNewState'), 'left');

        if (!empty($salesPerson)) {
            // $select->join('salesPerson', 'salesInvoice.salesPersonID = salesPerson.salesPersonID', array('salesPersonSortName','salesPersonID'), 'left');
            $select->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left');
            $select->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left');
            if ($withDefaultSalesPerson) {
                $select->where->AND->NEST
                    ->in('invoiceSalesPersons.salesPersonID', $salesPerson)
                    ->orPredicate(new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID'));
            } else {
                $select->where->in('invoiceSalesPersons.salesPersonID', $salesPerson);
            }
        }
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted','deletedTimeStamp'), 'left');
        
        if ($sortBy == "invoiceNo") {
            $select->order(array('salesInvoice.salesInvoiceCode DESC'));
        } else if ($sortBy == "invoiceDate") {
            $select->order(array('salesInvoice.salesInvoiceIssuedDate DESC'));
        } else if ($sortBy == "customerName") {
            $select->order(array('customer.customerName ASC'));
        } else {
            $select->order(array('salesInvoice.salesInvoiceID DESC'));
        }

	$select->group('salesInvoice.salesInvoiceID');
        if ($groupBy) {
            $select->group('customer.customerID');
        }
        $select->where->lessThanOrEqualTo('salesInvoiceIssuedDate', $endDate);
        if ($cusIds && !$isAllCustomers) {
            $select->where->in('salesInvoice.customerID', $cusIds);
        }
        $select->where->notEqualTo('salesInvoice.customerID', 0);

        $select->where->AND->NEST
            ->in('salesInvoice.statusID', [3,6,5])
            ->or->equalTo('salesInvoice.statusID', 4)
                ->equalTo('invoiceEditLogDetails.invoiceEditLogDetailsCategory', 'salesInvoiceStatusClosed')
                ->greaterThan('invoiceEditLog.dateAndTime', $endDate);

        if ($cusCategory != "") {
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $agedAnalysisData[] = $val;
        }

        return $agedAnalysisData;
    }


    /**
     * @author SANDUN <sandun@thinkcube.com>
     * @param date $current
     * @param date $before
     * @return array $result
     */
    public function getTwoWeeksGraphData($current, $before)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($before);
        $endTime = strtotime($current);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();
        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i * $day)));
        }

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('invoice')
                ->columns(array(
                    $days[0] => new Expression("SUM(case when datediff(issuedDate, '" . $days[0] . "') = 0 then totalAmount end)"),
                    $days[1] => new Expression("SUM(case when datediff(issuedDate, '" . $days[1] . "') = 0 then totalAmount end)"),
                    $days[2] => new Expression("SUM(case when datediff(issuedDate, '" . $days[2] . "') = 0 then totalAmount end)"),
                    $days[3] => new Expression("SUM(case when datediff(issuedDate, '" . $days[3] . "') = 0 then totalAmount end)"),
                    $days[4] => new Expression("SUM(case when datediff(issuedDate, '" . $days[4] . "') = 0 then totalAmount end)"),
                    $days[5] => new Expression("SUM(case when datediff(issuedDate, '" . $days[5] . "') = 0 then totalAmount end)"),
                    $days[6] => new Expression("SUM(case when datediff(issuedDate, '" . $days[6] . "') = 0 then totalAmount end)"),
                    $days[7] => new Expression("SUM(case when datediff(issuedDate, '" . $days[7] . "') = 0 then totalAmount end)"),
                    $days[8] => new Expression("SUM(case when datediff(issuedDate, '" . $days[8] . "') = 0 then totalAmount end)"),
                    $days[9] => new Expression("SUM(case when datediff(issuedDate, '" . $days[9] . "') = 0 then totalAmount end)"),
                    $days[10] => new Expression("SUM(case when datediff(issuedDate, '" . $days[10] . "') = 0 then totalAmount end)"),
                    $days[11] => new Expression("SUM(case when datediff(issuedDate, '" . $days[11] . "') = 0 then totalAmount end)"),
                    $days[12] => new Expression("SUM(case when datediff(issuedDate, '" . $days[12] . "') = 0 then totalAmount end)"),
                    $days[13] => new Expression("SUM(case when datediff(issuedDate, '" . $days[13] . "') = 0 then totalAmount end)"),
                ))
        ->where->between('issuedDate', $before, $current);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }

        return $result;
    }

    public function fetchAllInvoice()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function getOverduenvoices($locationID)
    {
        $overdueStatusID = 6;
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("customerID", "salesInvoiceID", "salesInvoiceCode", "salesInvoiceIssuedDate", "salesInvoiceOverDueDate", "statusID", "locationID"));
        $select->from('salesInvoice');
        $select->join("customer", "customer.customerID = salesInvoice.customerID", array("customerName"), "left outer");
//        $select->join("logger", "logger.refID = invoice.id", array("refID", "userID"), "left outer");
//        $select->join("user", "logger.userID = user.userID", array("firstName", "email1"), "left outer");
        // TODO - check status ID
//        $select->where->lessThanOrEqualTo("salesInvoiceOverDueDate", new Expression('CURDATE()'))->AND->equalTo("statusID", "3")->AND->equalTo('locationID', $locationID);
        $select->where->equalTo("statusID", $overdueStatusID)->AND->equalTo('locationID', $locationID);
        $select->order("salesInvoiceOverDueDate ASC");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getMembershipInvoices($locationID, $today)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("membership");
        $select->columns(array("nextInvoiceDate", "membershipComment" => new Expression('membership.comment')));
        $select->join("salesInvoice", "membership.invoiceID = salesInvoice.salesInvoiceID", array("customerID", "salesInvoiceID", "salesInvoiceCode", "salesInvoiceIssuedDate", "salesInvoiceOverDueDate", "statusID", "locationID"));
        $select->join("customer", "customer.customerID = salesInvoice.customerID", array("customerName"), "left");
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('*'), 'left');
        $select->where(array('nextInvoiceDate' => $today, 'locationID' => $locationID));
        // $select->where(array('deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getMembershipByInvoiceId($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice");
        $select->columns(['*']);
        $select->join("membership", "membership.invoiceID = salesInvoice.salesInvoiceID", array("nextInvoiceDate", "membershipComment" => new Expression('membership.comment')));
        $select->where(array('salesInvoiceID' => $invoiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author sharmilan <sharmilan@thinkicube.com>
     * get overdue invoioce IDs
     * @param type $locationID
     * @param type $statusID
     * @return type
     */
    public function getOverdueInvoiceIDs($locationID)
    {
        $openStatusID = '3';
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("salesInvoiceID", "statusID", "locationID"));
        $select->from('salesInvoice');
        $select->where->lessThan("salesInvoiceOverDueDate", new Expression('CURDATE()'))->AND->equalTo("statusID", $openStatusID)->AND->equalTo('locationID', $locationID);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * @author Sahan Siriwardhan <sahan@thinkicube.com>
     * get overdue invoice 
     * @param type $statusID
     * @return type
     */
    public function getOverdueInvoiceData()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("customerID", "customerCode", "customerTitle", "customerName","customerTelephoneNumber"));
        $select->from('customer');
        $select->join("salesInvoice", "salesInvoice.customerID = customer.customerID", array(
            'invoiceCount' => new Expression('COUNT(salesInvoice.salesInvoiceID)'),
            'dueBalance' => new Expression('SUM(salesInvoice.salesinvoiceTotalAmount-salesInvoice.salesInvoicePayedAmount)')
        ), "left");
        $select->join("creditNote", "salesInvoice.salesInvoiceID = creditNote.invoiceID", array(
                    'creditNoteTotal' => new Expression('SUM(creditNote.creditNoteTotal)')), "left");
        $select->where->lessThanOrEqualTo("salesInvoice.salesInvoiceOverDueDate", new Expression('CURDATE()'))->AND->equalTo("salesInvoice.statusID", 3)->OR->equalTo("salesInvoice.statusID", 6);
        $select->group("customerID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

     /**
     * @author Sahan Siriwardhan <sahan@thinkicube.com>
     * get overdue invoice 
     * @param type $statusID
     * @return type
     */
    public function getOverdueInvoiceBetweenDate($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("customerID", "customerCode", "customerTitle", "customerName","customerTelephoneNumber"));
        $select->from('customer');
        $select->join("salesInvoice", "salesInvoice.customerID = customer.customerID", array(
            'invoiceCount' => new Expression('COUNT(salesInvoice.salesInvoiceID)'), 
            'dueBalance' => new Expression('SUM(salesInvoice.salesinvoiceTotalAmount)'),
            'invoicesDetails' => new Expression('GROUP_CONCAT(salesInvoice.salesInvoiceCode, " - " ,salesInvoice.salesInvoiceOverDueDate)')
        ), "left");
        $select->where->between('salesInvoice.salesInvoiceOverDueDate', $fromDate, $toDate);
        $select->where->in("salesInvoice.statusID", array(3, 6));
        $select->group("customerID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

     /**
     * @author Sahan Siriwardhan <sahan@thinkicube.com>
     * get invoice between date
     * @param type $statusID
     * @return type
     */
    public function getInvoiceBetweenDate($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->columns(array("customerID", "customerCode", "customerTitle", "customerName","customerTelephoneNumber"));
        $select->from('customer');
        $select->join("salesInvoice", "salesInvoice.customerID = customer.customerID", array(
            'invoiceIssuedDate'
             => new Expression('MAX(salesInvoice.salesInvoiceIssuedDate)'),
            'salesinvoiceTotalAmount','salesInvoiceCode'
        ), "left");
        $select->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('salesPersonID' => new Expression('MAX(invoiceSalesPersons.salesPersonID)')), 'left');
        $select->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left');
        $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        $select->where->in("salesInvoice.statusID", array(3, 4));
        $select->group(array('customerID', 'salesInvoiceID'));
        $select->order(array('salesInvoice.salesInvoiceIssuedDate' => 'DESC'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        // var_dump($results);
        return $results;
    }

    /**
     * @author sharmilan <sharmilan@thinkcube.com>
     * update statusID
     */
    public function updateInvoiceStatusID($invoiceID, $statusID)
    {
        $data = array('statusID' => $statusID);
        $this->tableGateway->update($data, array('salesInvoiceID' => $invoiceID));
        return TRUE;
    }

    public function updateInvoiceState($id, $state)
    {
        $data = array(
            'statusID' => $state,
            );
        $this->tableGateway->update($data, array('salesInvoiceID' => $id));
        return TRUE;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Date range Array
     *
     */
    public function getDatesBetween2Dates($startTime, $endTime)
    {
        $day = 86400;
        $format = 'Y-m-d';
        $startTime = strtotime($startTime);
        $endTime = strtotime($endTime);
        $numDays = round(($endTime - $startTime) / $day) + 1;
        $days = array();
        for ($i = 0; $i < $numDays; $i++) {
            $days[] = date($format, ($startTime + ($i *
                    $day)));
        }

        return $days;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Month range Array
     *
     */
    public function getMonthsBetweenTwoDates($fromDate, $toDate)
    {
        $time1 = strtotime($fromDate);
        $time2 = strtotime($toDate);
        $my = date('m', $time2);

        $months = array(date('F', $time1));

        while ($time1 < $time2
        ) {
            $time1 = strtotime(date('Y-m-d', $time1) . ' +1 month');
            if (date('m', $time1) != $my && ($time1 < $time2))
                $months[] = date('F', $time1);
        }

        $months[] = date('F', $time2);
        $months = array_unique($months);
        return $months;
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * return Month range Array
     *
     */
    public function getMonthsYearBetweenTwoDates($fromDate, $toDate)
    {
        $time1 = strtotime($fromDate);
        $time2 = strtotime($toDate);

        $my = date('mY', $time2);

        $months = array(date('F Y', $time1));

        while ($time1 <= $time2
        ) {
            $time1 = strtotime(date('Y-m', $time1) . ' +1 month');
            if (date('mY', $time1) != $my && ($time1 <= $time2))
                $months[] = date('F Y', $time1);
        }

        $months[] = date('F Y', $time2);
        $months = array_unique($months);

        return $months;
    }

    public function getInvoiceIds()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * @author SANDUN <sandun@thinkcube.com>
     * Get Sales Product data function
     * @param Date $fromDate
     * @param Date $toDate
     * @return $countArray
     */
    public function getAllProductSalesDetails($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();

        $select->columns(array('InvID' => new Expression('invoice.id'),
            'Day' => new Expression('invoice.issuedDate')));
        $select->from('invoice');
        $select->join("invoiceReceipt", "invoice.id = invoiceReceipt.invoiceID", array(
            'pID' => new Expression('invoiceReceipt.productID'),
            'tot' => new Expression('SUM(invoiceReceipt.total )'),
            'Qty' => new Expression('SUM(invoiceReceipt.quantity-invoiceReceipt.returnedQuantity)')), "left");
        $select->join("product", "invoiceReceipt.productID = product.id", array('pName' => new Expression('product.productName')), "left");
        $select->join("productCategories", "productCategories.productCategoriesID = product.category", array('pcID' => new Expression('productCategories.productCategoriesID'),
            'cName' => new Expression('productCategories.categoryName')), "left");
        $select->group("invoiceReceipt.productID");
        $select->where->between('invoice.issuedDate', $fromDate, $toDate);
        $select->where(array('productCategories.state' => 'Active'));
        $select->where('productCategoriesID IS NOT NULL');

        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    /**
     * @author Sandun  <sandun@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @param type $pID
     * @return type
     */
    public function getProductSalesDetailsByProID($fromDate, $toDate, $pID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();

        $select->columns(array('InvID' => new Expression('invoice.id'),
            'Day' => new Expression('invoice.issuedDate')));
        $select->from('invoice');
        $select->join("invoiceReceipt", "invoice.id = invoiceReceipt.invoiceID", array(
            'pID' => new Expression('invoiceReceipt.productID'),
            'tot' => new Expression('SUM(invoiceReceipt.total )'),
            'Qty' => new Expression('SUM(invoiceReceipt.quantity-invoiceReceipt.returnedQuantity)')), "left");
        $select->join("product", "invoiceReceipt.productID = product.id", array('pName' => new Expression('product.productName')), "left");
        $select->join("productCategories", "productCategories.productCategoriesID = product.category", array('pcID' => new Expression('productCategories.productCategoriesID'),
            'cName' => new Expression('productCategories.categoryName')), "left");
        $select->group("invoiceReceipt.productID");
        $select->where->between('invoice.issuedDate', $fromDate, $toDate);
        $select->where(array('productCategories.state' => 'Active'));
        $select->where(array('productCategories.productCategoriesID' => $pID));
        $select->where('productCategoriesID IS NOT NULL');

        $statement = $sql->prepareStatementForSqlObject($select);


        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    public function getProductSalesDetailsByInvoice($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();

        $select->columns(array('invID' => new Expression('invoice.id'),
            'day' => new Expression('invoice.issuedDate')));
        $select->from('invoice');
        $select->join("invoiceReceipt", "invoice.id = invoiceReceipt.invoiceID", array(
            'pID' => new Expression('invoiceReceipt.productID'),
            'tot' => new Expression('invoiceReceipt.total'),
            'qty' => new Expression('invoiceReceipt.quantity-invoiceReceipt.returnedQuantity')), "right");
        $select->join("product", "invoiceReceipt.productID = product.id", array(
            'pName' => new Expression('product.productName')), "left");
        $select->order("invoiceReceipt.invoiceID");
        $select->where->between('invoice.issuedDate', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        foreach ($results as $val) {
            $countArray[] = $val;
        }

        return $countArray;
    }

    public function getInvoiceProductByDelvieryID($deliveryID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("salesInvoiceProduct", "salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID", array("*"), "left")
                ->where(array("salesInvoice.deliveryNoteID" => $deliveryID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoiceProductBySalesOrderID($salesOrderID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("salesInvoiceProduct", "salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID", array("*"), "left")
                ->where(array("salesInvoice.salesOrderID" => $salesOrderID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getPosInvoiceData($fromDate = null, $toDate = null, $userIds = NULL, $locIds = NULL, $discount = FALSE)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice");
        $select->columns(array("*"));
        $select->join('location', 'location.locationID = salesInvoice.locationID', array('*'), 'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('*'), 'left');
        $select->join('user', 'user.userID = entity.createdBy', array('*'), 'left');
        $select->order(array('salesInvoice.salesInvoiceID' => 'DESC'));
        if ($fromDate != NULL && $toDate != NULL) {
            $select->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if ($userIds != NULL) {
            $select->where->in('entity.createdBy', $userIds);
        }
        if ($locIds != NULL) {
            $select->where->in('salesInvoice.locationID', $locIds);
        }
        if ($discount) {
            $select->where->greaterThan('salesInvoiceTotalDiscount', 0);
        }
        $select->where(array('deleted' => '0'));
        $select->where(array('salesInvoice.pos' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param date $date
     * @param array $locIds
     * @return array $results
     */
    public function getPosPaymentData($fromDate = null, $toDate = null, $locIds = NULL, $userIds = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('incomingInvoicePayment', 'incomingInvoicePayment.salesInvoiceID = salesInvoice.salesInvoiceID', array('*'), 'left');
        $select->join('incomingPaymentMethod', 'incomingInvoicePayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left');
        $select->join('incomingPayment', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentId', array('*'), 'left');
        $select->join('incomingPaymentMethodBankTransfer', 'incomingPaymentMethod.incomingPaymentMethodBankTransferId = incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId', array('*'), 'left');
        $select->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left');
        $select->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left');
        $select->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left');
        $select->join('incomingPaymentMethodGiftCard', 'incomingPaymentMethod.incomingPaymentMethodGiftCardId = incomingPaymentMethodGiftCard.incomingPaymentMethodGiftCardId', array('*'), 'left');
        $select->join('incomingPaymentMethodLoyaltyCard', 'incomingPaymentMethod.incomingPaymentMethodLoyaltyCardId = incomingPaymentMethodLoyaltyCard.incomingPaymentMethodLoyaltyCardId', array('*'), 'left');
        $select->join('cardType', 'cardType.cardTypeID = incomingPaymentMethodCreditCard.cardTypeID', array('cardTypeName'), 'left');
        $select->join('customer', 'customer.customerID = incomingPayment.customerID', array('customerName', 'customerTitle'), 'left');
        $select->join('entity', 'entity.entityID = incomingPayment.entityID', array('deleted','createdBy'), 'left');
        $select->join('user', 'entity.createdBy = user.userID', ['createdUser' => new Expression('user.userUsername')], 'left');
        $select->join('location', 'incomingPayment.locationID = location.locationID', array('locationName'), 'left');
        $select->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left');
        $select->order(array('salesInvoice.salesInvoiceID' => 'DESC'));
        $select->where(array('salesInvoice.pos' => 1));
        if ($fromDate != NULL && $toDate != null) {
            $select->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if ($locIds != NULL) {
            $select->where->in('salesInvoice.locationID', $locIds);
        }
        if ($userIds != NULL) {
            $select->where->in('entity.createdBy', $userIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return NULL;
        }
        return $results;
    }

    public function getSalesInvoiceIds($fromDate, $toDate, $cancelInvoice, $editedInvoice, $locIDs)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID'))
                ->order(array('salesInvoiceID' => 'DESC'));
        $select->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        $select->where->notEqualTo('statusID', $cancelInvoice);
        $select->where->notEqualTo('statusID', $editedInvoice);
        if (!empty($locIDs)) {
            $select->where->in('salesInvoice.locationID', $locIDs);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        } else {
            $invoiceIds = array();
            foreach ($results as $val) {
                array_push($invoiceIds, $val['salesInvoiceID']);
            }
            return $invoiceIds;
        }
    }

    /**
     * @author Sandun<sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @return $results
     */
    public function getSalesInvoiceDetails($fromDate, $toDate, $freeIssueFlag = FALSE, $locIDs, $categoryIds = null)
    {
        $cancelInvoiceStatus = 5;
        $invoiceReplaceStatus = 10;

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', ['*',
            'salesInvoiceProductQuantity' => new Expression("SUM(salesInvoiceProductQuantity)")], 'left');
        $select->join('deliveryNote', 'deliveryNote.deliveryNoteID = salesInvoice.deliveryNoteID', array(
            'deliveryNoteCode'
                ), 'left');
        $select->join('status', 'salesInvoice.statusID = status.statusID', array('statusName'), 'left');
        $select->join('product', 'product.productID = salesInvoiceProduct.productID', array(
            'pName' => new Expression('productName'),
            'pID' => new Expression('product.productID'),
            'pCD' => new Expression('productCode')
                ), 'left');
        $select->join('category', 'product.categoryID = category.categoryID', array(
                    'categoryName' => new Expression('categoryName')),'left');
        $select->join('location', 'location.locationID = salesInvoice.locationID', array(
            'locationName', 'locationCode',
            'locID' => new Expression('salesInvoice.locationID')
                ), 'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', ['*'], 'left');
        $select->join('user', 'entity.createdBy = user.userID', ['createdUser' => new Expression('user.userUsername')], 'left');
        $select->order(array('salesInvoiceIssuedDate' => 'DESC'));
        $select->group(['salesInvoice.salesInvoiceID', 'product.productID']);
        $select->where->notEqualTo('product.productID', 'NULL');
        $select->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        $select->where->notEqualTo('salesInvoice.statusID', $cancelInvoiceStatus);
        $select->where->notEqualTo('salesInvoice.statusID', $invoiceReplaceStatus);
        $select->where(array('product.productState' => 1));

        if ($freeIssueFlag) {
            $select->where(array('salesInvoiceProduct.salesInvoiceProductTotal' => null));
            // $select->where->equalTo('salesInvoiceProductDiscountType', 'precentage');
        }

        if (!empty($locIDs)) {
            $select->where->in('salesInvoice.locationID', $locIDs);
        }
        if (!is_null($categoryIds)) {
            $select->where->in('product.categoryID', $categoryIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function updateSalesInvoiceStatus($salesInvoiceID, $closeStatusID)
    {
        $data = array(
            'statusID' => $closeStatusID
        );
        return $this->tableGateway->update($data, array('salesInvoiceID' => $salesInvoiceID));
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @param type $invoiceSearchKey
     * @return type
     */
    public function searchInvoicesForDropDown($locationID, $invoiceSearchKey, $statusIDs = array(), $isFullPaid = false, $posflag = 0, $customerID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID', 'salesInvoiceCode', 'pos', 'salesInvoiceTemporaryCode'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('locationID' => $locationID));
        if ($posflag == 0) {
            $select->where(array('salesInvoice.pos' => 0));
        } else if ($posflag == 1) {
            $select->where(array('salesInvoice.pos' => 1));
        }
        if ($isFullPaid) {
            $select->where->notEqualTo('salesinvoiceTotalAmount', 'salesInvoicePayedAmount');
        }
        if ($statusIDs) {
            $select->where->in('salesInvoice.statusID', $statusIDs);
        }
        if($customerID != ''){
            $select->where->equalTo('salesInvoice.customerID', $customerID);   
        }

        $select->where->like('salesInvoiceCode', '%' . $invoiceSearchKey . '%');
        if ($posflag) {
            $select->where->or->like('salesInvoiceTemporaryCode', '%' . $invoiceSearchKey . '%');
        }
        $select->where->notEqualTo("salesInvoice.statusID", 10);
        $select->where->notEqualTo("salesInvoice.statusID", 4);
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    //get invoice details by activityID
    public function getInvoiceDetailsByActivityID($activityID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("salesInvoiceProduct", "salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID", array("*"), "left")
                ->where(array("salesInvoice.activityID" => $activityID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    //get invoice details by jobID
    public function getInvoiceDetailsByjobID($jobID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("salesInvoiceProduct", "salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID", array("*"), "left")
                ->where(array("salesInvoice.jobID" => $jobID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    //get invoice details by projectID
    public function getInvoiceDetailsByProjectID($projectID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("salesInvoiceProduct", "salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID", array("*"), "left")
                ->where(array("salesInvoice.projectID" => $projectID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param int $activityId
     * @param int $jobId
     * @param int $projectId
     * @return $results
     */
    public function getInvoiceByJobCardRelatedId($activityId = NULL, $jobId = NULL, $projectId = NULL, $typeId = NULL)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice");
        $select->columns(array('*', 'invValueTotal' => new Expression('salesinvoiceTotalAmount')));
        if ($typeId == 'activityNo') {
            $select->join("activity", "salesInvoice.activityID = activity.activityId", array("activityCode"));
            $select->where('salesInvoice.activityID IS NOT NULL');
        }
        if ($activityId != NULL) {
            $select->where->in("salesInvoice.activityId", $activityId);
        }
        if ($typeId == 'jobNo') {
            $select->join("job", "salesInvoice.jobID = job.jobId", array("jobReferenceNumber"));
            $select->where('salesInvoice.jobID IS NOT NULL');
        }
        if ($jobId != NULL) {
            $select->where->in("salesInvoice.jobId", $jobId);
        }
        if ($typeId == 'projectNo') {
            $select->join("project", "salesInvoice.projectID = project.projectCode", array("projectCode"));
            $select->where('salesInvoice.projectID IS NOT NULL');
        }
        if ($projectId != NULL) {
            $select->where->in("salesInvoice.projectId", $projectId);
        }
        $select->where->notEqualTo('statusID',10);
        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();
        if (!$results) {
            return NULL;
        }
        return $results;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * get costing details of jobcard related invoice
     * @param array $activityId
     * @param array $jobId
     * @param array $projectId
     * @param string $typeId
     * @return array $results
     */
    public function getCostingDataByJobCardRefId($activityId = NULL, $jobId = NULL, $projectId = NULL, $typeId = NULL)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice");
        $select->columns(array('salesInvoiceCode',
            'invValueTotal' => new Expression('salesinvoiceTotalAmount'),
            'salesInvoiceID'));
        if ($typeId == 'activityNo') {
            $select->join("itemOut", "salesInvoice.activityID = itemOut.itemOutJobCardReferenceID", array(
                "itemOutDocumentType", "itemOutJobCardReferenceID", "itemOutLocationProductID",
                "itemOutQty" => new Expression("SUM(itemOutQty)"), "itemOutPrice"));
            $select->join("activity", "salesInvoice.activityID = activity.activityId", array(
                "activityCode", "activityID", "activityName"));
            $select->where('salesInvoice.activityID IS NOT NULL');
            $select->order("salesInvoice.activityID");
        }
        if ($activityId != NULL) {
            $select->where->in("itemOut.itemOutJobCardReferenceID", $activityId);
        }
        if ($typeId == 'jobNo') {
            $select->join("itemOut", "salesInvoice.jobID = itemOut.itemOutJobCardReferenceID", array(
                "itemOutDocumentType", "itemOutJobCardReferenceID", "itemOutLocationProductID",
                "itemOutQty" => new Expression("SUM(itemOutQty)"), "itemOutPrice"));
            $select->join("job", "salesInvoice.jobID = job.jobId", array(
                "jobName", "jobReferenceNumber", "jobID"));
            $select->where('salesInvoice.jobID IS NOT NULL');
            $select->order("salesInvoice.jobID");
        }
        if ($jobId != NULL) {
            $select->where->in("itemOut.itemOutJobCardReferenceID", $jobId);
        }
        if ($typeId == 'projectNo') {
            $select->join("itemOut", "salesInvoice.projectID = itemOut.itemOutJobCardReferenceID", array(
                "itemOutDocumentType", "itemOutJobCardReferenceID", "itemOutLocationProductID",
                "itemOutQty" => new Expression("SUM(itemOutQty)"), "itemOutPrice"));
            $select->join("project", "salesInvoice.projectID = project.projectId", array(
                "projectName", "projectCode", "projectID"));
            $select->where('salesInvoice.projectID IS NOT NULL');
            $select->order("salesInvoice.projectID");
        }
        if ($projectId != NULL) {
            $select->where->in("itemOut.itemOutJobCardReferenceID", $projectId);
        }
        $select->join("itemIn", "itemIn.itemInID = itemOut.itemOutItemInID", array("itemInPrice"));
        $select->join("locationProduct", "locationProduct.locationProductID = itemOut.itemOutLocationProductID");
        $select->join("product", "locationProduct.productID = product.productID", array("productName", "productCode"), "left");
        $select->group(array("itemOutLocationProductID", "itemOutJobCardReferenceID"));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return NULL;
        }
        return $results;
    }

    public function getInvoiceByCode($invoiceCode)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("location", "location.locationID = salesInvoice.locationID", array("locationName", "locationCode"), "left")
                ->where(array("salesInvoice.salesInvoiceCode" => $invoiceCode));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoiceDataByInvoiceId($invoiceId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("location", "location.locationID = salesInvoice.locationID", array("locationName", "locationCode"), "left")
                ->where(array("salesInvoice.salesInvoiceId" => $invoiceId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * Given JobCard Related None Inventroy product details(Cost Type data)
     * @param array $activityId
     * @param array $jobId
     * @param array $projectId
     * @param string $jobCardType
     * @return $results
     */
    public function getInvoiceDetailsByInvoiceID($activityId = NULL, $jobId = NULL, $projectId = NULL, $jobCardType = NULL)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice");
        $select->columns(array("salesInvoiceID"));
        $select->join("salesInvoiceProduct", "salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID", array(
            'salesInvoiceProductPrice', 'salesInvoiceProductTotal', 'salesInvoiceProductID',
            "productID" => new Expression("salesInvoiceProduct.productID")), 'left');
        if ($jobCardType == 'projectNo') {
            $select->join("project", "project.projectId = salesInvoice.projectID", array(
                "referenceCode" => new Expression('projectCode'),
                "referenceName" => new Expression('projectName'),
                "projectID" => new Expression("project.projectId")));
            $select->join("job", "project.projectId = job.projectId", array("jobName"));
            $select->join("activity", "job.jobId = activity.jobId", array("activityCode"));
        }
        if ($jobCardType == "jobNo") {
            $select->join("job", "salesInvoice.jobID = job.jobId", array(
                "referenceCode" => new Expression('jobReferenceNumber'),
                "referenceName" => new Expression('jobName'),
                "jobID" => new Expression("job.jobId")
                    )
            );
            $select->join("activity", "job.jobId = activity.jobId", array("activityCode"));
        }
        if ($jobCardType == 'activityNo') {

            $select->join("activity", "salesInvoice.activityID = activity.activityId", array(
                "referenceCode" => new Expression('activityCode'),
                "referenceName" => new Expression('activityName'),
                "activityID" => new Expression("activity.activityId")));
        }
        $select->join("activityCostType", "activityCostType.activityId = activity.activityId", array(
            "estimateCost" => new Expression('activityCostTypeEstimatedCost'),
            "actualCost" => new Expression("activityCostTypeActualCost")));
        $select->join("costType", "costType.costTypeID = activityCostType.costTypeId", array("costTypeID"));
        $select->join("product", "salesInvoiceProduct.productID = product.productID", array("productTypeID", "productName", "productCode"));
        $select->join("locationProduct", "locationProduct.productID = product.productID", array("locationProductID"));
        $select->join(array("lP" => 'locationProduct'), "lP.locationID = salesInvoice.locationID", array("locationProductQuantity"));
        $select->where(array('product.productTypeID' => 2));
        if ($projectId != NULL) {
            $select->where->in("salesInvoice.projectID", $projectId);
        }
        if ($jobId != NULL) {
            $select->where->in("salesInvoice.jobID", $jobId);
        }
        if ($activityId != NULL) {
            $select->where->in("salesInvoice.activityID", $activityId);
        }
        $select->group("salesInvoiceProduct.salesInvoiceProductID");

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * Get open invoices by customer id
     * @param int $customerId
     * @param array $taxType
     * @return mixed
     */
    public function getOpenInvoicesByCustomerId($customerId, $taxType = array(), $locationIDs)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice");
        $select->columns(array("*"));
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left');
        $select->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName','customerCode', 'customerTelephoneNumber'), 'left');
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array(
            'salesInvoiceProductID'), 'left');
        $select->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
            'totalTax' => new Expression('(salesInvoiceProductTaxAmount)')), 'left');
        $select->where->in("salesInvoice.customerID", $customerId);
        $select->where->in("salesInvoice.statusID", array(3, 6));
        $select->where->equalTo('entity.deleted', 0);

        if (!empty($locationIDs)) {
            $select->where->in("salesInvoice.locationID", $locationIDs);
        }

        if (!empty($taxType)) {
            if ($taxType[0] == 1 && $taxType[1] != 2) {

                $select->where->equalTo('salesInvoiceSuspendedTax', 1);
            } else if ($taxType[0] == 2) {

                $select->where->equalTo('salesInvoiceSuspendedTax', 0);
            }
        }
        $select->group('salesInvoice.salesInvoiceID');
        $select->group('salesInvoice.customerID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get all invoices by locationIds
     * @param int $locationIds
     * @param string $searchKey
     * @param array $statusIds status id list
     * @return mixed
     */
    public function getAllSalesInvoicesByLocationIds($locationIds, $searchKey = null, $statusIds = array())
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
        ->where->in("salesInvoice.locationID", $locationIds)
        ->where->equalTo('entity.deleted', 0);
        if ($statusIds) {
            $select->where->in("salesInvoice.statusID", $statusIds);
        }
        if ($searchKey != NULL) {
            $select->where(new PredicateSet(array(new Operator('salesInvoice.salesInvoiceCode', 'like', '%' . $searchKey . '%'))));
            $select->limit(10);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getCancelledInvocieDetails($locations, $fromDate = FALSE, $toDate = false, $cancelledUsers = false, $customerIds = [])
    {
        $adaptor = $this->tableGateway->getAdapter();
        $sql = new Sql($adaptor);
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->join('customer', 'salesInvoice.customerID = customer.customerID', ['customerName', 'customerCode'], 'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', ['*'], 'left');
        $select->join('user', 'entity.createdBy = user.userID', ['createdUser' => new Expression('user.userUsername')], 'left');
        $select->join(['deleteUser' => 'user'], 'entity.deletedBy = deleteUser.userID', ['deletedUser' => new Expression('deleteUser.userUsername')], 'left');
        $select->where(['deleted' => '1']);
        $select->where->notEqualTo('salesInvoice.statusID', 10);
        $select->where->in('salesInvoice.locationID', $locations);
        if ($fromDate && $toDate) {
            $select->where->between('entity.deletedTimeStamp', $fromDate, $toDate);
        }
        if ($cancelledUsers) {
            $select->where->in('deleteUser.userID', $cancelledUsers);
        }
        if ($customerIds) {
            $select->where->in('salesInvoice.customerID', $customerIds);
        }
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    public function getInvoicedNonInventoryItems($locationID, $proList = false, $isAllProducts = false, $fromDate = false, $toDate = false)
    {
        $adaptor = $this->tableGateway->getAdapter();
        $sql = new Sql($adaptor);
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left');
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', ['*'], 'left');
        $select->join('product', 'salesInvoiceProduct.productID = product.productID', ['*'], 'left');
        $select->where(['product.productTypeID' => '2', 'locationID' => $locationID, 'entity.deleted' => '0']);
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if (!$isAllProducts && $proList) {
            $select->where->in('salesInvoiceProduct.productID', $proList);
        }

        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @param array $salesPersonIds
     */
    public function getInvoiceDataBySalesPersonIds($fromDate = NULL, $toDate = NULL, $salesPersonIds = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("salesPerson", "salesInvoice.salesPersonID = salesPerson.salesPersonID", array("*"), "left")
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate)
                ->where->notEqualTo('salesInvoice.statusID', 10)
                ->AND->NEST
                ->in('salesInvoice.salesPersonID', $salesPersonIds)->orPredicate(
                new \Zend\Db\Sql\Predicate\IsNull('salesInvoice.salesPersonID')
        );
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function checkInvoiceByID($invoiceID, $locationID)
    {


        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('salesInvoice')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "salesInvoice.entityID = e.entityID")
                    ->join('customer', "salesInvoice.customerID = customer.customerID", array("customerName", "customerCode"), 'left')
                    ->where(array('locationID' => $locationID))
                    ->where(array('e.deleted' => '0'))
                    ->where(array('salesInvoice.salesInvoiceID' => $invoiceID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();

            if (!$rowset) {
                return null;
            }


            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    /**
     * Get invoice metadata by invoice id
     * @param type $invoiceId
     * @return mixed
     */
    public function getInvoiceMetadataByInvoiceId($invoiceId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("customerName", "customerStatus"), "left")
                ->join("location", "salesInvoice.locationID = location.locationID", "locationName", "left")
                ->join("salesPerson", "salesInvoice.salesPersonID = salesPerson.salesPersonID", "*", "left");
        $select->where->equalTo("salesInvoice.salesInvoiceID", $invoiceId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if ($result->count() == 0) {
            return NULL;
        }
        return $result->current();
    }

    /**
     * get jobcard related active invoice list
     * @param type $fromDate
     * @param type $toDate
     * @return array $result
     */
    public function getJobCardInvoices($fromDate, $toDate, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', ['documentTypeID', 'salesInvoiceProductDocumentID','salesInvoiceProductID','salesInvoiceProductQuantity','salesInvoiceProductGrandTotal' => new Expression('SUM(salesInvoiceProductTotal)')], 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array('saleInvoiceTotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left')
                ->join("location", "salesInvoice.locationID = location.locationID", ["locationName", "locationCode"], "left")
                ->where(['entity.deleted' => 0])
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate)
        ->where->nest()->isNotNull('projectID')
        ->OR->isNotNull('jobID')
        ->OR->isNotNull('activityID');
        $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName','customerCode'), 'left');
        $select->group(array('salesInvoice.salesInvoiceID'));
        if($cusCategory != ""){
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if (!$result) {
            return null;
        }
        return $result;
    }

    public function getDeliveryNoteInvoices($fromDate, $toDate, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', ['documentTypeID', 'salesInvoiceProductDocumentID','salesInvoiceProductID','salesInvoiceProductQuantity','salesInvoiceProductTotal'], 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array('saleInvoiceTotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left')
                ->join("location", "salesInvoice.locationID = location.locationID", ["locationName", "locationCode"], "left")
                ->where(array('entity.deleted' => 0, 'documentTypeID'=> 4))
                ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        $select->group(array('salesInvoice.salesInvoiceID'));
        $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName','customerCode'), 'left');
        if($cusCategory != ""){
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if (!$result) {
            return null;
        }
        return $result;
    }

    /**
     * Get multiple location wise sales invoice
     * @param array $locationIds location id array
     * @param array $statusIds status id array
     * @param string $invoiceSearchKey search key of the invoice
     * @return mixed
     */
    public function searchLocationWiseInvoicesForDropDown($locationIds = array(), $statusIds = array(), $invoiceSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID', 'salesInvoiceCode'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
        ->where->in('locationID', $locationIds);
        if ($statusIds) {
            $select->where->in('salesInvoice.statusID', $statusIds);
        }
        $select->where->like('salesInvoiceCode', '%' . $invoiceSearchKey . '%');
        $select->where->notEqualTo("salesInvoice.statusID", 10);
        $select->limit(20);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get given invoices payment details by sales invoice date wise or
     * incoming payment date
     * @param array $invoiceIds
     * @param array $paymentIds
     * @param array $locationIds
     * @param string $fromDate
     * @param string $toDate
     * @param boolean $byPaymntDate
     * @return mixed
     */
    public function getInvoicePaymentDetails($invoiceIds = [], $paymentIds = [], $locationIds = [], $fromDate = null, $toDate = null, $customerIds = [], $accountId, $salesPerson = [], $withDefaultSalesPerson = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->join('incomingInvoicePayment', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID', array('*'), 'left')
                ->join('incomingPayment', 'incomingInvoicePayment.incomingPaymentID = incomingPayment.incomingPaymentID', array('*'), 'left')
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentID', array('incomingPaymentMethodId', 'incomingPaymentMethodAmount'), 'left')
                ->join('incomingPaymentMethodBankTransfer', 'incomingPaymentMethod.incomingPaymentMethodBankTransferId = incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId', array('*'), 'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left')
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                ->join('incomingPaymentMethodGiftCard', 'incomingPaymentMethod.incomingPaymentMethodGiftCardId = incomingPaymentMethodGiftCard.incomingPaymentMethodGiftCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLoyaltyCard', 'incomingPayment.incomingPaymentID = incomingPaymentMethodLoyaltyCard.incomingPaymentID', array('*'), 'left')
                ->join('incomingPaymentMethodLC', 'incomingPaymentMethod.incomingPaymentMethodLCId = incomingPaymentMethodLC.incomingPaymentMethodLCId', array('*'), 'left')
                ->join('incomingPaymentMethodTT', 'incomingPaymentMethod.incomingPaymentMethodTTId = incomingPaymentMethodTT.incomingPaymentMethodTTId', array('*'), 'left')
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerCode'), 'left')
                ->join(array('si' => 'entity'), "salesInvoice.entityID = si.entityID", ['*'], 'left')
                ->join(array('ipm' => 'entity'), "incomingPaymentMethod.entityID = ipm.entityID", ['*'], 'left')
                ->order(array('salesInvoice.salesInvoiceID ASC'));
        if ($invoiceIds) {
            $select->where->in('salesInvoice.salesInvoiceID', $invoiceIds);
        }
        if ($customerIds) {
            $select->where->in('salesInvoice.customerID', $customerIds);
        }
        if ($paymentIds) {
            $select->where->in('incomingPayment.incomingPaymentID', $paymentIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        if ($fromDate && $toDate) {
            $select->where->between('incomingPayment.incomingPaymentDate', $fromDate, $toDate);
        }
        $select->where->equalTo('si.deleted', 0)
        ->where->notEqualTo('incomingPayment.incomingPaymentStatus', 5);
        $select->where->AND->NEST
                    ->in('ipm.deleted', [0])->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('ipm.deleted')
                    );

        if (!is_null($accountId)) {
            $select->join('journalEntry','journalEntry.journalEntryDocumentID = incomingPayment.incomingPaymentID', array('journalEntryID'),'left');
            $select->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left');
            $select->where(array('journalEntry.documentTypeID' => 7,'journalEntryAccounts.financeAccountsID' => $accountId));
            $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        }

        if(!empty($salesPerson)){
            $select->join('salesPerson', 'salesInvoice.salesPersonID = salesPerson.salesPersonID', array('salesPersonSortName','salesPersonID'), 'left');
            if($withDefaultSalesPerson) {
                    $select->where->AND->NEST
                    ->in('salesInvoice.salesPersonID', $salesPerson)->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('salesInvoice.salesPersonID')
                    );
            } else {
                $select->where->in('salesInvoice.salesPersonID', $salesPerson);
            }

        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    /**
     * Get given invoices payment details by sales invoice date wise or
     * incoming payment date
     * @param array $invoiceIds
     * @param array $paymentIds
     * @param array $locationIds
     * @param string $fromDate
     * @param string $toDate
     * @param boolean $byPaymntDate
     * @return mixed
     */
    public function getInvoicePaymentDetailsForSalesPersonWise($invoiceIds = [], $paymentIds = [], $locationIds = [], $fromDate = null, $toDate = null, $customerIds = [], $accountId, $salesPerson = [], $withDefaultSalesPerson = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->join('incomingInvoicePayment', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID', array('*'), 'left')
                ->join('incomingPayment', 'incomingInvoicePayment.incomingPaymentID = incomingPayment.incomingPaymentID', array('*'), 'left')
                ->join('incomingPaymentMethod', 'incomingPayment.incomingPaymentID = incomingPaymentMethod.incomingPaymentID', array('incomingPaymentMethodId', 'incomingPaymentMethodAmount'), 'left')
                ->join('incomingPaymentMethodBankTransfer', 'incomingPaymentMethod.incomingPaymentMethodBankTransferId = incomingPaymentMethodBankTransfer.incomingPaymentMethodBankTransferId', array('*'), 'left')
                ->join('incomingPaymentMethodCash', 'incomingPaymentMethod.incomingPaymentMethodCashId = incomingPaymentMethodCash.incomingPaymentMethodCashId', array('*'), 'left')
                ->join('incomingPaymentMethodCheque', 'incomingPaymentMethod.incomingPaymentMethodChequeId = incomingPaymentMethodCheque.incomingPaymentMethodChequeId', array('*'), 'left')
                ->join('incomingPaymentMethodCreditCard', 'incomingPaymentMethod.incomingPaymentMethodCreditCardId = incomingPaymentMethodCreditCard.incomingPaymentMethodCreditCardId', array('*'), 'left')
                ->join('incomingPaymentMethodGiftCard', 'incomingPaymentMethod.incomingPaymentMethodGiftCardId = incomingPaymentMethodGiftCard.incomingPaymentMethodGiftCardId', array('*'), 'left')
                ->join('incomingPaymentMethodLoyaltyCard', 'incomingPayment.incomingPaymentID = incomingPaymentMethodLoyaltyCard.incomingPaymentID', array('*'), 'left')
                ->join('incomingPaymentMethodLC', 'incomingPaymentMethod.incomingPaymentMethodLCId = incomingPaymentMethodLC.incomingPaymentMethodLCId', array('*'), 'left')
                ->join('incomingPaymentMethodTT', 'incomingPaymentMethod.incomingPaymentMethodTTId = incomingPaymentMethodTT.incomingPaymentMethodTTId', array('*'), 'left')
                ->join('currency', 'incomingPayment.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerCode'), 'left')
                ->join(array('si' => 'entity'), "salesInvoice.entityID = si.entityID", ['*'], 'left')
                ->join(array('ipm' => 'entity'), "incomingPaymentMethod.entityID = ipm.entityID", ['*'], 'left')
                ->order(array('salesInvoice.salesInvoiceID ASC'));
        if ($invoiceIds) {
            $select->where->in('salesInvoice.salesInvoiceID', $invoiceIds);
        }
        if ($customerIds) {
            $select->where->in('salesInvoice.customerID', $customerIds);
        }
        if ($paymentIds) {
            $select->where->in('incomingPayment.incomingPaymentID', $paymentIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        if ($fromDate && $toDate) {
            $select->where->between('incomingPayment.incomingPaymentDate', $fromDate, $toDate);
        }
        $select->where->equalTo('si.deleted', 0)
        ->where->notEqualTo('incomingPayment.incomingPaymentStatus', 5);
        $select->where->AND->NEST
                    ->in('ipm.deleted', [0])->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('ipm.deleted')
                    );

        if (!is_null($accountId)) {
            $select->join('journalEntry','journalEntry.journalEntryDocumentID = incomingPayment.incomingPaymentID', array('journalEntryID'),'left');
            $select->join('journalEntryAccounts', 'journalEntry.journalEntryID = journalEntryAccounts.journalEntryID', array('*'), 'left');
            $select->where(array('journalEntry.documentTypeID' => 7,'journalEntryAccounts.financeAccountsID' => $accountId));
            $select->where->notEqualto('journalEntryAccounts.journalEntryAccountsDebitAmount','0');
        }

        if(!empty($salesPerson)){
            $select->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left');
            $select->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left');

            // $select->join('salesPerson', 'salesInvoice.salesPersonID = salesPerson.salesPersonID', array('salesPersonSortName','salesPersonID'), 'left');
            // if($withDefaultSalesPerson) {
            //         $select->where->AND->NEST
            //         ->in('salesInvoice.salesPersonID', $salesPerson)->orPredicate(
            //             new \Zend\Db\Sql\Predicate\IsNull('salesInvoice.salesPersonID')
            //         );
            // } else {
            //     $select->where->in('salesInvoice.salesPersonID', $salesPerson);
            // }

            if($withDefaultSalesPerson) {
                    $select->where->AND->NEST
                    ->in('invoiceSalesPersons.salesPersonID', $salesPerson)->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                    );
            } else {
                $select->where->in('invoiceSalesPersons.salesPersonID', $salesPerson);
            }

        }
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    public function isOfflineInvoiceExists($offlineId)
    {
        $rowset = $this->tableGateway->select(array('salesInvoiceTemporaryCode' => $offlineId));
        if ($rowset->count()) {
            return true;
        }
        return false;
    }

    /**
    * get existing loyalty point and earned loyality points that related to the given invoice ID
    * @param int $invoiceID
    * @return array
    */
    public function getLoyaltyDetailsByInvoiceID($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceCode'))
                ->join('incomingInvoicePayment', 'salesInvoice.salesInvoiceID = incomingInvoicePayment.salesInvoiceID')
                ->join('incomingPaymentMethodLoyaltyCard', 'incomingInvoicePayment.incomingPaymentID = incomingPaymentMethodLoyaltyCard.incomingPaymentID', array('collectedPoints'), 'left')
                ->join('customerLoyalty','incomingPaymentMethodLoyaltyCard.customerLoyaltyID = customerLoyalty.customerLoyaltyID',array('customerLoyaltyPoints'),'left')
               ->where(array('salesInvoice.salesInvoiceID'=>$invoiceID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        return $rowset;
    }

    /**
     * Get sales invoices by location id and date range
     * @param int $locationId
     * @param string $fromDate
     * @param string $toDate
     * @return mixed
     */
    public function getAllSalesInvoicesByLocationIdAndDateRange( $locationId, $fromDate = null, $toDate = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join('entity', 'salesInvoice.entityID=entity.entityID', array('*'), 'left')
                ->where(array('locationID' => $locationId));
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function getAllPOSInvoices()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $whrtype = new  \Zend\Db\Sql\Where();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID','salesInvoiceCode','pos','salesinvoiceTotalAmount'))
                ->join('status', 'salesInvoice.statusID = status.statusID', array('statusName'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array("salesInvoice.pos" => 1,"entity.deleted" => 0,"salesInvoice.statusID" => 4))
                ->order(array('salesInvoice.salesInvoiceID DESC'))
                ->where(array($whrtype->greaterThan('salesInvoice.salesinvoiceTotalAmount',0)));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function getInvoicedPOSDetails($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID','salesInvoiceCode','pos','salesInvoiceTotalDiscountType','salesInvoiceDiscountRate','salesInvoiceRemainingDiscValue','salesInvoiceRemainingPromotionDiscValue','salesinvoiceTotalAmount'))
                ->join('status', 'salesInvoice.statusID = status.statusID', array('statusName'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->join('salesInvoiceProduct','salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID',array('*'),'left')
                ->join('product','salesInvoiceProduct.productID = product.productID',array('*'),'left')
                ->join('salesInvoiceSubProduct','salesInvoiceProduct.salesInvoiceProductID = salesInvoiceSubProduct.salesInvocieProductID',array('*'),'left')
                ->join('productSerial','salesInvoiceSubProduct.productSerialID = productSerial.productSerialID',array('*'),'left')
                ->join('productBatch','salesInvoiceSubProduct.productBatchID = productBatch.productBatchID',array('*'),'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("salesInvoiceProductTax", "salesInvoiceProduct.salesInvoiceProductID= salesInvoiceProductTax.salesInvoiceProductID", array("salesInvoiceProductTaxID", "taxID", "salesInvoiceProductTaxPrecentage", "salesInvoiceProductTaxAmount"), "left")
                ->join("tax", "salesInvoiceProductTax.taxID = tax.id", array("*"), "left")
                ->where(array("salesInvoice.pos" => 1,"entity.deleted" => 0,"salesInvoice.salesInvoiceID" => $invoiceID, 'productUomDisplay' => '1'))
                ->where->notEqualTo("salesInvoice.statusID", 10);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    public function updateInvoiceRemainingDiscountValue($invoiceID, $value)
    {
        $data = array('salesInvoiceRemainingDiscValue' => $value);
        $this->tableGateway->update($data, array('salesInvoiceID' => $invoiceID));
        return TRUE;
    }

    public function updatePromotionRemainingDiscountValue($invoiceID, $value)
    {
        $data = array('salesInvoiceRemainingPromotionDiscValue' => $value);
        $this->tableGateway->update($data, array('salesInvoiceID' => $invoiceID));
        return TRUE;
    }

    public function getOpenPosInvoices($id, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice");
        $select->columns(array("*"));

        $prdct = new  \Zend\Db\Sql\Where();

        if(!$id){
            $select->where(array('pos' => 1, 'locationID' => $locationID, ));
            $select->where->in("statusID", array(3, 6));
        }else{
            $select->where(array(
                    'pos' => 1,
                    'locationID' => $locationID,
                    $prdct->greaterThan('salesInvoiceID', $id)
                )
            );
            $select->where->in("statusID", array(3, 6));
        }
        $select->order(array('salesInvoiceID' => 'DESC'));
        $select->where->notEqualTo("salesinvoiceTotalAmount",0);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getPosInvoiceSummeryDetails($fromDate = NULL, $toDate = NULL, $userIds = NULL, $locationIds = NULL, $statusIds = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('incomingInvoicePayment', 'incomingInvoicePayment.salesInvoiceID = salesInvoice.salesInvoiceID', array('*'), 'left');
        $select->join('incomingPayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('*'), 'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('*'), 'left');
        $select->join('user', 'user.userID = entity.createdBy', array('*'), 'left');
        $select->where(array('salesInvoice.pos' => '1'));
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if ($userIds) {
            $select->where->in('entity.createdBy', $userIds);
        }
        if ($invoiceStatusIds) {
            $select->where->in('salesInvoice.statusID', $invoiceStatusIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        if ($paymentStatus) {
            $select->where->in('incomingPayment.incomingPaymentStatus', $paymentStatus);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getPosPaymentSummeryDetails($fromDate = NULL, $toDate = NULL, $userIds = NULL, $locationIds = NULL, $invoiceStatusIds = NULL, $paymentStatus = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('incomingInvoicePayment', 'incomingInvoicePayment.salesInvoiceID = salesInvoice.salesInvoiceID', array('*'), 'left');
        $select->join('incomingPayment', 'incomingPayment.incomingPaymentID = incomingInvoicePayment.incomingPaymentID', array('*'), 'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('*'), 'left');
        $select->join('user', 'user.userID = entity.createdBy', array('*'), 'left');
        $select->where(array('salesInvoice.pos' => '1'));
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if ($userIds) {
            $select->where->in('entity.createdBy', $userIds);
        }
        if ($invoiceStatusIds) {
            $select->where->in('salesInvoice.statusID', $invoiceStatusIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        if ($paymentStatus) {
            $select->where->in('incomingPayment.incomingPaymentStatus', $paymentStatus);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;

    }

    public function getInvoiceStatusByInvoiceID($id)
    {
        $id = (int) $id;
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID','statusID'))
                ->where(array('salesInvoiceID' => $id));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getMonthlySalesCreditNoteData($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
            'Year' => new Expression('Year(creditNoteDate)'),
            'creditNoteProductTotal' => new Expression('SUM(creditNote.creditNoteTotal)')));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->group(array('Month', 'Year'));
        $select->order(array('creditNoteDate'));
        $select->where(array('deleted' => 0));
        $select->where->between('creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();


        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    /**
     * Get invoice data by sales person ids and customer ids
     * @param string $fromDate
     * @param string $toDate
     * @param array $salesPersonIds
     * @param array $customerIds
     * @return mixed
     */
    public function getInvoiceDataBySalesPersonIdsAndCustomerIds($fromDate = NULL, $toDate = NULL, $salesPersonIds = [], $customerIds = [], $withDefaultSalesPerson = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join("salesPerson", "salesInvoice.salesPersonID = salesPerson.salesPersonID", array("*"), "left")
                ->join("customer", "salesInvoice.customerID = customer.customerID", array("*"), "left")
                ->join("entity", "salesInvoice.entityID = entity.entityID", array("deleted"), "left")
                ->join("creditNote", "salesInvoice.salesInvoiceID = creditNote.invoiceID", array(
                    'creditNoteTotal' => new Expression('SUM(creditNote.creditNoteTotal)')), "left")
                ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate)
                ->where->equalTo('entity.deleted', 0)
                ->where->in('salesInvoice.customerID', $customerIds);
                if($withDefaultSalesPerson) {
                    $select->where->AND->NEST
                    ->in('salesInvoice.salesPersonID', $salesPersonIds)->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('salesInvoice.salesPersonID')
                    );
                } else {
                    $select->where->in('salesInvoice.salesPersonID', $salesPersonIds);
                }
                $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * Get invoice details by sales order id
     * @param string $salesOrderId
     * @return mixed
     */
    public function getInvoicesBySalesOrderId($salesOrderId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(['salesInvoiceID', 'salesInvoiceCode','customerID', 'salesinvoiceTotalAmount', 'salesInvoiceIssuedDate','entityID'])
                ->join("salesInvoiceProduct", "salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID", array("documentTypeID", "salesInvoiceProductDocumentID"), "left")
                ->join("deliveryNote", "salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID", array("salesOrderID"), "left")
                ->join("entity", "salesInvoice.entityID = entity.entityID", array("deleted"), "left")
                ->where->equalTo('entity.deleted', 0)
                ->where->nest
                ->where->equalTo('salesInvoice.salesOrderID', $salesOrderId)
                ->or
                ->where->equalTo('salesInvoiceProduct.documentTypeID', 4)
                ->where->equalTo('deliveryNote.salesOrderID', $salesOrderId);
                $select->group(array('salesInvoice.salesInvoiceID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * Get open invoices by customer ids and sales person ids
     * @param int $customerId
     * @param array $taxType
     * @param array $salesPersonIds
     * @param boolean $withDefaultSalesPerson
     * @return mixed
     */
    public function getOpenInvoicesByCustomerIdAndSalesPersonIds($customerId, $taxType = [], $salesPersonIds = [], $withDefaultSalesPerson = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice");
        $select->columns(array("*"));
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left');
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array(
            'salesInvoiceProductID'), 'left');
        $select->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
            'totalTax' => new Expression('(salesInvoiceProductTaxAmount)')), 'left');
        $select->where->equalTo("salesInvoice.customerID", $customerId);
        $select->where->in("salesInvoice.statusID", array(3, 6));
        $select->where->equalTo('entity.deleted', 0);
        if (!empty($taxType)) {
            if ($taxType[0] == 1 && $taxType[1] != 2) {

                $select->where->equalTo('salesInvoiceSuspendedTax', 1);
            } else if ($taxType[0] == 2) {

                $select->where->equalTo('salesInvoiceSuspendedTax', 0);
            }
        }
        if($withDefaultSalesPerson) {
            $select->where->AND->NEST
            ->in('salesInvoice.salesPersonID', $salesPersonIds)->orPredicate(
                new \Zend\Db\Sql\Predicate\IsNull('salesInvoice.salesPersonID')
            );
        } else {
            $select->where->in('salesInvoice.salesPersonID', $salesPersonIds);
        }
        $select->group('salesInvoice.salesInvoiceID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
    * get customerID for given InvoiceID
    **/
    public function getInvoicedCustomerIdByInvoiceID($invoiceID)
    {
        $invoiceID = (int) $invoiceID;
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID','customerID'))
                ->where(array('salesInvoiceID' => $invoiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * Get invoice product credit notes details by product id
     * @param string $productId
     * @param string $fromDate
     * @param string $toDate
     * @param array $locationIds
     * @return mixed
     */
    public function getInvoiceProductCreditNoteDetails($productId, $fromDate = null, $toDate = null, $locationIds = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
            ->join("creditNote", "salesInvoice.salesInvoiceID = creditNote.invoiceID", array('creditNoteID'), 'left')
            ->join("creditNoteProduct", "creditNote.creditNoteID = creditNoteProduct.creditNoteID", array(
                'creditNoteProductID',
                'creditNoteProductQty' => new Expression('SUM(creditNoteProduct.creditNoteProductQuantity)'),
                'creditNoteProductTotal' => new Expression('SUM(creditNoteProduct.creditNoteProductTotal)'), 'productID'), 'left')
            ->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left')
            ->where->equalTo('creditNoteProduct.productID', $productId)
            ->where->equalTo('entity.deleted', 0)
            ->where->notEqualto('creditNote.statusID', 5);
        if($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute()->current();
    }

    public function getCreditNoteCostByCreditNoteIDAndProductId($productId, $fromDate = null, $toDate = null, $locationIds = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
            ->join("creditNote", "salesInvoice.salesInvoiceID = creditNote.invoiceID", array('creditNoteID'), 'left')
            ->join("itemIn", "creditNote.creditNoteID = itemIn.itemInDocumentID", array('itemInDocumentType',
                'creditNoteCost' => new Expression('SUM(itemIn.itemInPrice)')), 'left')
            ->join('locationProduct','itemIn.itemInLocationProductID = locationProduct.locationProductID', array('productID'),'left')
            ->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left')
            ->where->equalTo('locationProduct.productID', $productId)
            ->where->equalTo('itemIn.itemInDocumentType', 'Credit Note')
            ->where->equalTo('entity.deleted', 0)
            ->where->notEqualto('creditNote.statusID', 5);
        if($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute()->current();
    }

    /**
     * Get invoices given data
     * @param string $fromDate
     * @param string $toDate
     * @param array $locationIds
     * @param array $invoiceIds
     * @return mixed
     */
    public function getInvoicesByDateRange($fromDate = null, $toDate = null, $locationIds = [], $invoiceIds = [], $customerIds = []) {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', [
                    'totalCreditNote' => new Expression('SUM(creditNoteTotal / creditNoteCustomCurrencyRate)')
                ], 'left')
                ->join('status', 'salesInvoice.statusID = status.statusID', ['statusName'], 'left')
                ->join('currency', 'salesInvoice.customCurrencyId = currency.currencyID', ['currencySymbol'], 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', ['customerName', 'customerCode'])
                ->join("entity", "entity.entityID = salesInvoice.entityID", ['deleted'], 'left')
                ->where->notEqualTo('salesInvoice.statusID', 10);
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if ($invoiceIds) {
            $select->where->in('salesInvoice.salesInvoiceID', $invoiceIds);
        }
        if ($customerIds) {
            $select->where->in('salesInvoice.customerID', $customerIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        $select->group(['salesInvoice.salesInvoiceID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * Get non-inventory item total by invoice id
     * @param String $invoiceId
     * @return int
     */
    public function getNonInventoryItemTotal($invoiceId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(['salesInvoiceID'])
                ->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', [
                    'productID','salesInvoiceProductID','nonInventoryTotal' => new Expression('SUM(salesInvoiceProductTotal)')
                    ], 'left')
                ->join("product", "salesInvoiceProduct.productID = product.productID", ['productTypeID'], 'left')
                ->where->equalTo('salesInvoice.salesInvoiceID', $invoiceId)
                ->where->equalTo('product.productTypeID', 2);
        $select->group(['salesInvoiceProduct.salesInvoiceProductID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute()->current();
        return $result['nonInventoryTotal'];
    }

    public function getNonAllInventoryItemTotalByInvoiceID($invoiceId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(['salesInvoiceID'])
                ->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', [
                    'salesInvoiceProductPrice','salesInvoiceProductQuantity','salesInvoiceProductDiscount','salesInvoiceProductDiscountType'
                    ], 'left')
                ->join("product", "salesInvoiceProduct.productID = product.productID", ['productTypeID'], 'left')
                ->where->equalTo('salesInvoice.salesInvoiceID', $invoiceId)
                ->where->equalTo('product.productTypeID', 2);
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function searchInvoicesForDropDownInInvoiceView($locationID, $invoiceSearchKey, $statusIDs = array(), $isFullPaid = false, $posflag = 0, $customerID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('salesInvoiceID', 'salesInvoiceCode', 'pos', 'salesInvoiceTemporaryCode'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('salesInvoiceIssuedDate' => 'DESC'))
                ->where(array('locationID' => $locationID));
        if ($posflag == 0) {
            $select->where(array('salesInvoice.pos' => 0));
        } else if ($posflag == 1) {
            $select->where(array('salesInvoice.pos' => 1));
        }
        if ($isFullPaid) {
            $select->where->notEqualTo('salesinvoiceTotalAmount', 'salesInvoicePayedAmount');
        }
        if ($statusIDs) {
            $select->where->in('salesInvoice.statusID', $statusIDs);
        }
        if($customerID != ''){
            $select->where->equalTo('salesInvoice.customerID', $customerID);   
        }

        $select->where->like('salesInvoiceCode', '%' . $invoiceSearchKey . '%');
        if ($posflag) {
            $select->where->or->like('salesInvoiceTemporaryCode', '%' . $invoiceSearchKey . '%');
        }
        $select->where->notEqualTo("salesInvoice.statusID", 10);
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getNonInventoryInvoices($fromDate, $toDate, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', ['documentTypeID', 'salesInvoiceProductDocumentID','salesInvoiceProductID','salesInvoiceProductQuantity','salesInvoiceProductGrandTotal' => new Expression('SUM(salesInvoiceProductTotal)')], 'left')
                ->join('locationProduct', 'locationProduct.locationProductID = salesInvoiceProduct.locationProductID', ['productID'], 'left')
                ->join('product', 'product.productID = locationProduct.productID', ['productTypeID'], 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array('saleInvoiceTotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left')
                ->join("location", "salesInvoice.locationID = location.locationID", ["locationName", "locationCode"], "left")
                ->where(array('entity.deleted' => 0, 'product.productTypeID'=> 2))
                ->where('salesInvoice.jobID IS NULL')
                ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        $select->group(array('salesInvoice.salesInvoiceID'));
        $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName','customerCode'), 'left');
        if($cusCategory != ""){
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if (!$result) {
            return null;
        }
        return $result;
    }

    // get delivery note related sales invoice by quotation id
    public function getDeliveryNoteRelatedSalesInvoiceDataByQuotationId($quotationID = null, $soID = null, $deliveryNoteID = null, $returnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
        $select->join('deliveryNote', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
        $select->join('salesOrders', 'deliveryNote.salesOrderID = salesOrders.soID', array('*'),'left');
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->where(array('salesOrders.quotationID' => $quotationID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($soID)) {
            $select->where(array('salesOrders.soID' => $soID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($deliveryNoteID)) {
            $select->where(array('deliveryNote.deliveryNoteID' => $deliveryNoteID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($returnID)) {
            $select->join('deliveryNoteProduct', "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array('*'), 'left');
            $select->join('salesReturnProduct', "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array('*'), 'left');
            $select->join('salesReturn', "salesReturn.salesReturnID = salesReturnProduct.salesReturnID", array('*'), 'left');
            $select->where(array('salesReturn.salesReturnID' => $returnID));
        }
        $select->group("salesInvoice.salesInvoiceID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get sales order related sales invoice by quotation id
    public function getSalesOrderRelatedSalesInvoiceDataByQuotationId($quotationID = null, $soID = null, $creditNoteID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->join('salesOrders', 'salesInvoice.salesOrderID = salesOrders.soID', array('*'),'left');
            $select->where(array('salesOrders.quotationID' => $quotationID));
        }
        if (!is_null($soID)) {
            $select->join('salesOrders', 'salesInvoice.salesOrderID = salesOrders.soID', array('*'),'left');
            $select->where(array('salesOrders.soID' => $soID));
        }
        if (!is_null($creditNoteID)) {
            $select->join('creditNote', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
            $select->where(array('creditNote.creditNoteID' => $creditNoteID));
        }
        $select->group("salesInvoice.salesInvoiceID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get quotation related sales invoice by quotation id
    public function getQuotationRelatedSalesInvoiceDataByQuotationId($quotationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesInvoice.quotationID' => $quotationID));
        $select->group("salesInvoice.salesInvoiceID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get invoice basic details with time stamp by invoice id 
    public function getInvoiceBasicDetailsByInvoiceId($salesInvoiceID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('*'));
        $select->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesInvoice.salesInvoiceID' => $salesInvoiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;

    }
    
    public function getCustomerCumulativeInvoiceBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('customerID','salesinvoiceTotalAmount','salesInvoiceID'))
                ->join('customer', 'customer.customerID = salesInvoice.customerID', array(),'left')
                ->join('status', 'salesInvoice.statusID = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'salesInvoice.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('salesInvoice.statusID',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('salesInvoice.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getInvoiceByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('*'))
                ->join('quotation', 'salesInvoice.quotationID = quotation.quotationID', array('quotationCode'), 'left')
                ->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesInvoiceProductDocumentID'), 'left')
                ->join('deliveryNote', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('deliveryNoteCode'), 'left')
                ->join('salesOrders', 'salesInvoice.salesOrderID = salesOrders.soID', array('soCode'), 'left')
                ->join('activity', 'salesInvoice.activityID = activity.activityId', array('activityCode'), 'left')
                ->join('job', 'salesInvoice.jobID = job.jobId', array('jobReferenceNumber'), 'left')
                ->join('project', 'salesInvoice.projectID = project.projectId', array('projectCode'), 'left')
                ->join('customer', 'customer.customerID = salesInvoice.customerID', array('*'),'left')
                ->join('status', 'salesInvoice.statusID = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'salesInvoice.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('salesInvoice.statusID',$status);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('salesInvoice.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }


    public function getInvoiceBalanceAmountAfterPayment($invoiceID, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('remainingInvoiceAmount' => new Expression('SUM(salesinvoiceTotalAmount-salesInvoicePayedAmount)')))
                ->join('customer', 'customer.customerID = salesInvoice.customerID', array(),'left')
                ->join('status', 'salesInvoice.statusID = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'salesInvoice.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0','salesInvoiceID' => $invoiceID));
                $select->where->in('salesInvoice.statusID',$status);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 0) {
            return NULL;
        }
        return $results->current();
    }

    public function getMonthlySalesTaxDetails($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M %Y")'),
                    'Year' => new Expression('Year(salesInvoiceIssuedDate)'), 'salesInvoiceSuspendedTax'))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
                    'salesInvoiceProductTaxAmount', 'taxID'), 'left')
                ->join('tax','salesInvoiceProductTax.taxID = tax.id',array('*'),'left')
                ->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left')
                ->order(array('salesInvoice.salesInvoiceIssuedDate'))
                ->where(array('deleted' => 0))
        ->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getMonthlySalesCreditNoteTaxData($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
            'Year' => new Expression('Year(creditNoteDate)'),
            ));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->join("salesInvoice", "salesInvoice.salesInvoiceID = creditNote.invoiceID", array('salesInvoiceSuspendedTax'), 'left');
        $select->join("creditNoteProduct", "creditNoteProduct.creditNoteID = creditNote.creditNoteID", array('creditNoteProductID'), 'left');
        $select->join("creditNoteProductTax", "creditNoteProductTax.creditNoteProductID = creditNoteProduct.creditNoteProductID", array('creditNoteTaxAmount'), 'left');
        $select->join("tax", "tax.id = creditNoteProductTax.taxID", array('*'), 'left');
        $select->order(array('creditNoteDate'));
        $select->where(array('deleted' => 0));
        $select->where->between('creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }


    public function getMonthlySalesDataBySalesPersonID($fromDate, $toDate, $locationID, $cusCatgory = NULL, $salesPersonIds = NULL, $withDefaultSalesPerson =false)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M %Y")'),
            'Year' => new Expression('Year(salesInvoiceIssuedDate)'),
            'TotalAmount' => new Expression('(salesinvoiceTotalAmount)'),'salesPersonID', 'salesInvoiceIssuedDate'));
        $select->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left');
        $select->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left');
        $select->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left');
        $select->order(array('salesInvoiceIssuedDate'));
        $select->where(array('deleted' => 0));
        $select->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        if($cusCatgory != ""){
            $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCatgory);
        }
        if ($salesPersonIds != NULL) {
            if($withDefaultSalesPerson) {
                    $select->where->AND->NEST
                    ->in('invoiceSalesPersons.salesPersonID', $salesPersonIds)->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                    );
            } else {
                $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonIds);
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getMonthlySalesTaxDetailsBySalesPersonID($fromDate, $toDate, $locationID, $cusCategory = NULL, $salesPersonIds = NULL, $withDefaultSalesPerson =false)
    {
        
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`salesInvoiceIssuedDate`, "%M %Y")'),
                    'Year' => new Expression('Year(salesInvoiceIssuedDate)'),'salesPersonID','salesInvoiceSuspendedTax', 'salesInvoiceID' ))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('salesInvoiceProductID'), 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array(
                    'salesInvoiceProductTaxAmount', 'taxID'), 'left')
                ->join('tax','salesInvoiceProductTax.taxID = tax.id',array('*'),'left')
                ->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left')
                ->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left')
                ->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left')
                ->order(array('salesInvoice.salesInvoiceIssuedDate'))
                ->where(array('deleted' => 0))
        ->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        if ($salesPersonIds != NULL) {
            if($withDefaultSalesPerson) {
                    $select->where->AND->NEST
                    ->in('invoiceSalesPersons.salesPersonID', $salesPersonIds)->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                    );
            } else {
                $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonIds);
            }
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    
    public function getMonthlySalesCreditNoteDataBySalesPersonID($fromDate, $toDate, $locationID, $cusCategory = NULL, $salesPersonIds = NULL, $withDefaultSalesPerson =false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
            'Year' => new Expression('Year(creditNoteDate)'),
            'creditNoteProductTotal' => new Expression('(creditNote.creditNoteTotal)')));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->join("salesInvoice", "salesInvoice.salesInvoiceID = creditNote.invoiceID", array('salesPersonID', 'salesInvoiceID'), 'left');
        $select->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left');
        $select->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left');
        $select->order(array('creditNoteDate'));
        $select->where(array('deleted' => 0));
        $select->where->between('creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        if ($salesPersonIds != NULL) {
            if($withDefaultSalesPerson) {
                    $select->where->AND->NEST
                    ->in('invoiceSalesPersons.salesPersonID', $salesPersonIds)->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                    );
            } else {
                $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonIds);
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getMonthlySalesCreditNoteTaxDataBySalesPersonID($fromDate, $toDate, $locationID, $cusCategory = NULL,$salesPersonIds = NULL, $withDefaultSalesPerson = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
            'Year' => new Expression('Year(creditNoteDate)'),
            ));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->join("creditNoteProduct", "creditNoteProduct.creditNoteID = creditNote.creditNoteID", array('creditNoteProductID'), 'left');
        $select->join("creditNoteProductTax", "creditNoteProductTax.creditNoteProductID = creditNoteProduct.creditNoteProductID", array('creditNoteTaxAmount'), 'left');
        $select->join("tax", "tax.id = creditNoteProductTax.taxID", array('*'), 'left');
        $select->join("salesInvoice", "salesInvoice.salesInvoiceID = creditNote.invoiceID", array('salesPersonID', 'salesInvoiceSuspendedTax', 'salesInvoiceID'), 'left');
        $select->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left');
        $select->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left');
        $select->order(array('creditNoteDate'));
        $select->where(array('deleted' => 0));
        $select->where->between('creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        if ($salesPersonIds != NULL) {
            if($withDefaultSalesPerson) {
                    $select->where->AND->NEST
                    ->in('invoiceSalesPersons.salesPersonID', $salesPersonIds)->orPredicate(
                        new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                    );
            } else {
                $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonIds);
            }
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }
    

    //get invoiced batch serial item data
    public function getBatchAndSerialProductsToInvoicedBatchSerialItemDetailsReport($InvoiceIds = null, $itemIds = null, $locationIds = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesInvoiceSubProduct')
                ->columns(array('*'))
                ->join('salesInvoiceProduct', 'salesInvoiceSubProduct.salesInvocieProductID = salesInvoiceProduct.salesInvoiceProductID', array('salesInvoiceID'))
                ->join('salesInvoice', 'salesInvoiceProduct.salesInvoiceID = salesInvoice.salesInvoiceID', array('salesInvoiceCode', 'salesInvoiceIssuedDate','statusID'))
                ->join('locationProduct', 'salesInvoiceProduct.locationProductID = locationProduct.locationProductID', array('locationID'))
                ->join('location', 'locationProduct.locationID = location.locationID', array('locationName'))
                ->join('product', 'locationProduct.productID = product.productID', array('productName'))
                ->join('productSerial', 'salesInvoiceSubProduct.productSerialID = productSerial.productSerialID', array('*'),'left')
                ->join('productBatch', 'salesInvoiceSubProduct.productBatchID = productBatch.productBatchID', array('*'),'left')
                ->join(['itemIn1' => 'itemIn'], 'productSerial.productSerialID = itemIn1.itemInSerialID', array('serialItemInDocumentID' => new Expression('itemIn1.itemInDocumentID'),'serialItemInDocumentType' => new Expression('itemIn1.itemInDocumentType')), 'left')
                ->join(['itemIn2' => 'itemIn'], 'productBatch.productBatchID = itemIn2.itemInBatchID', array('batchItemInDocumentID' => new Expression('itemIn2.itemInDocumentID'),'batchItemInDocumentType' => new Expression('itemIn2.itemInDocumentType')), 'left')
                ->join('deliveryNote', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('deliveryNoteCode'),'left')
        ->where->notEqualto('salesInvoice.statusID', 10);
        $select->group('salesInvoiceSubProduct.salesInvoiceSubProductID');
        if ($InvoiceIds) {
            $select->where->in('salesInvoice.salesInvoiceID', $InvoiceIds);
        } else {
            $select->where->in("salesInvoice.locationID", $locationIds);
        }
        if ($itemIds) {
            $select->where->in('product.productID', $itemIds);
        } else {
            $select->join('entity', 'product.entityID = entity.entityID', array('*'))
                ->where->equalTo('entity.deleted', 0);
            $select->where(new PredicateSet(array(new Operator('product.serialProduct','=',1), new Operator('product.batchProduct','=',1)), PredicateSet::OP_OR));
        }
        if ($locationIds) {
            $select->where->in('location.locationID', $locationIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    /*
    * This function is used to get invoice details by due date range
    * @param $startDate, $endDate, $locationIds, $custoemrIds
    */
    public function getInvoicesByDueDateRange($fromDate = null, $toDate = null, $locationIds = [], $customerIds = [], $statusIds) {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
                ->join('status', 'salesInvoice.statusID = status.statusID', ['statusName'], 'left')
                ->join('customer', 'salesInvoice.customerID = customer.customerID', ['*'])
                ->join("entity", "entity.entityID = salesInvoice.entityID", ['deleted'], 'left')
                ->join('location', 'salesInvoice.locationID = location.locationID', array('*'), 'left')
                ->where->notEqualTo('salesInvoice.statusID', 10);
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceOverDueDate', $fromDate, $toDate);
        }
        if ($customerIds) {
            $select->where->in('salesInvoice.customerID', $customerIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        if (!is_null($statusIds)) {
            $select->where->in('salesInvoice.statusID', $statusIds);
        }
        $select->group(['salesInvoice.salesInvoiceID']);
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
     * This function is used to get delivery note wise invoice credite details
     *
     */
    public function getInvoiceProductSalesReturnDetails($productId, $fromDate = null, $toDate = null, $invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('salesInvoice')
            ->join("salesInvoiceProduct", "salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID", array(
                'salesInvoiceProductID'), 'left')
            ->join(['locationProduct1' => 'locationProduct'],'salesInvoiceProduct.locationProductID = locationProduct1.locationProductID', array('productID'),'left')
            ->join('deliveryNoteProduct', 'deliveryNoteProduct.deliveryNoteID = salesInvoiceProduct.salesInvoiceProductDocumentID and locationProduct1.productID = deliveryNoteProduct.productID',array('deliveryNoteID'),'left')
            ->join('salesReturnProduct', 'deliveryNoteProduct.deliveryNoteProductID = salesReturnProduct.deliveryNoteProductID',array(
                'salesReturnProductID',
                'creditNoteProductQty' => new Expression('SUM(salesReturnProduct.salesReturnProductQuantity)'),
                'creditNoteProductTotal' => new Expression('SUM(salesReturnProduct.salesReturnProductTotal)'), 'productID'), 'left')
            ->join('salesReturn','salesReturn.salesReturnID = salesReturnProduct.salesReturnID',array('salesReturnID'),'left')
            ->join("itemIn", "salesReturn.salesReturnID = itemIn.itemInDocumentID", array('itemInDocumentType',
                'creditNoteCost' => new Expression('SUM(itemIn.itemInPrice)')), 'left')
            ->join(['locationProduct2' => 'locationProduct'],'itemIn.itemInLocationProductID = locationProduct2.locationProductID', array('productID'),'left')
            ->join("entity", "entity.entityID = salesInvoice.entityID", array('deleted'), 'left')
            ->where(array('salesInvoiceProduct.documentTypeID' => 4))
            ->where->equalTo('salesReturnProduct.productID', $productId)
            ->where->equalTo('deliveryNoteProduct.productID', $productId)
            ->where->equalTo('locationProduct1.productID', $productId)
            ->where->equalTo('locationProduct2.productID', $productId)
            ->where->equalTo('salesInvoice.salesInvoiceID', $invoiceID)
            ->where->equalTo('entity.deleted', 0);
        if($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute()->current();
    }

    public function getMonthlySalesDirectCreditNoteData($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
            'Year' => new Expression('Year(creditNoteDate)'),
            'directCreditNoteProductTotal' => new Expression('SUM(creditNote.creditNoteTotal)')));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->group(array('Month', 'Year'));
        $select->order(array('creditNoteDate'));
        $select->where(array('deleted' => 0, 'creditNoteDirectFlag' => 1));
        $select->where->between('creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();


        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    public function getMonthlySalesBuyBackCreditNoteData($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
            'Year' => new Expression('Year(creditNoteDate)'),
            'buybackCreditNoteProductTotal' => new Expression('SUM(creditNote.creditNoteTotal)')));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->group(array('Month', 'Year'));
        $select->order(array('creditNoteDate'));
        $select->where(array('deleted' => 0, 'creditNoteBuyBackFlag' => 1));
        $select->where->between('creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();


        $result = [];
        foreach ($results as $val) {
            $result[] = $val;
        }
        return $result;
    }

    public function getMonthlySalesDirectCreditNoteTaxData($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
            'Year' => new Expression('Year(creditNoteDate)'),
            ));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->join("creditNoteProduct", "creditNoteProduct.creditNoteID = creditNote.creditNoteID", array('creditNoteProductID'), 'left');
        $select->join("creditNoteProductTax", "creditNoteProductTax.creditNoteProductID = creditNoteProduct.creditNoteProductID", array('creditNoteTaxAmount'), 'left');
        $select->join("tax", "tax.id = creditNoteProductTax.taxID", array('*'), 'left');
        $select->order(array('creditNoteDate'));
        $select->where(array('deleted' => 0, 'creditNote.creditNoteDirectFlag' => 1));
        $select->where->between('creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getMonthlySalesBuyBackCreditNoteTaxData($fromDate, $toDate, $locationID, $cusCategory = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
            'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
            'Year' => new Expression('Year(creditNoteDate)'),
            ));
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->join("creditNoteProduct", "creditNoteProduct.creditNoteID = creditNote.creditNoteID", array('creditNoteProductID'), 'left');
        $select->join("creditNoteProductTax", "creditNoteProductTax.creditNoteProductID = creditNoteProduct.creditNoteProductID", array('creditNoteTaxAmount'), 'left');
        $select->join("tax", "tax.id = creditNoteProductTax.taxID", array('*'), 'left');
        $select->order(array('creditNoteDate'));
        $select->where(array('deleted' => 0, 'creditNote.creditNoteBuyBackFlag' => 1));
        $select->where->between('creditNoteDate', $fromDate, $toDate);
        if ($locationID != "") {
            $select->where->in('creditNote.locationID', $locationID);
        }
        if($cusCategory != ""){
            $select->join("customer", "creditNote.customerID = customer.customerID", array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }
   
    /**
    * this function use to get daily or monthly or anual sales
    * @param date $currentDate
    * @param date $startDate
    * @param string $type
    * return mix
    **/
    public function getInvoiceTotalByTimePerid($currentDate,$startDate = null, $type)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('TotalSales' => new Expression('SUM(salesinvoiceTotalAmount)')))
        ->where->notEqualTo("salesInvoice.statusID", 10);
        $select->where->notEqualTo("salesInvoice.statusID", 5);
        if ($type == 'daily') {
            $select->where->equalTo('salesInvoiceIssuedDate', $currentDate);
        } else {
            $select->where->between('salesInvoiceIssuedDate', $startDate, $currentDate);
            
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDailyInvoiceCount($currentDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('TotalInvoiceCount' => new Expression('COUNT(salesinvoiceID)')))
        ->where->notEqualTo("salesInvoice.statusID", 10);
        $select->where->notEqualTo("salesInvoice.statusID", 5);
        $select->where->equalTo('salesInvoiceIssuedDate', $currentDate);
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;   
    }

    public function getInvoiceTotalByDate($startDate,$endDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('dailyInvTotal' => new Expression('SUM(salesinvoiceTotalAmount)'),'salesInvoiceIssuedDate'))
        ->where->notEqualTo("salesInvoice.statusID", 10);
        $select->where->notEqualTo("salesInvoice.statusID", 5);
        $select->where->between('salesInvoiceIssuedDate', $startDate, $endDate);
        $select->group('salesInvoiceIssuedDate');
        
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;   
    }

    public function getTotalSalesByDateRangeAndLocationID($startDate,$endDate, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('invTotal' => new Expression('SUM(salesinvoiceTotalAmount)'),'invCount' => new Expression('count(salesinvoiceTotalAmount)'),'salesInvoiceIssuedDate'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where->notEqualTo("salesInvoice.statusID", 10);
        $select->where->notEqualTo("salesInvoice.statusID", 5);
        $select->where->between('salesInvoiceIssuedDate', $startDate, $endDate);
        $select->where(array('entity.deleted' => 0, 'salesInvoice.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results->current();   
    }

    public function getTotalSalesByDateRangeAndLocationIDAndStatus($startDate,$endDate, $locationID, $status)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('invAmount' => new Expression('salesinvoiceTotalAmount'),'invPayedAmountAmount' => new Expression('salesInvoicePayedAmount'),'salesInvoiceID','salesInvoiceIssuedDate'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left');
        $select->where->between('salesInvoiceIssuedDate', $startDate, $endDate);
        $select->where(array('entity.deleted' => 0, 'salesInvoice.locationID' => $locationID, 'salesInvoice.statusID' => $status));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;   
    }

    public function getTotalCustomerVisitByDateRangeAndLocationID($startDate,$endDate, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('invCount' => new Expression('count(salesinvoiceTotalAmount)'),'salesInvoiceIssuedDate','customerID'))
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerCode', 'customerStatus'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where->notEqualTo("salesInvoice.statusID", 10);
        $select->where->notEqualTo("salesInvoice.statusID", 5);
        $select->where->notEqualTo("salesInvoice.customerID", 0);
        $select->where->between('salesInvoiceIssuedDate', $startDate, $endDate);
        $select->where(array('entity.deleted' => 0, 'salesInvoice.locationID' => $locationID));
        $select->group(array('salesInvoice.customerID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;   
    }

    public function getCustomerVisitPromotionByDateRangeAndLocationID($startDate,$endDate, $locationID, $newCustomer = true, $promotionFlag = true)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('invCount' => new Expression('count(salesinvoiceTotalAmount)'),'promotionID','customerID'))
                ->join('customer', 'salesInvoice.customerID = customer.customerID', array('customerName', 'customerCode', 'customerStatus'), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('createdTimeStamp'), 'left');

        if ($promotionFlag) {
            $select->join('promotion', 'salesInvoice.promotionID = promotion.promotionID', array('promotionName'), 'left');
            $select->where('salesInvoice.promotionID IS NOT NULL');
            $select->where->notEqualTo("salesInvoice.promotionID", 0);
        }
        $select->where->notEqualTo("salesInvoice.statusID", 10);
        $select->where->notEqualTo("salesInvoice.statusID", 5);
        $select->where->notEqualTo("salesInvoice.customerID", 0);
        $select->where->between('salesInvoiceIssuedDate', $startDate, $endDate);

        if ($newCustomer) {
            $select->where->greaterThan('entity1.createdTimeStamp', $startDate);
        } else {
            $select->where->lessThan('entity1.createdTimeStamp',$startDate);
        }

        $select->where(array('entity.deleted' => 0, 'salesInvoice.locationID' => $locationID));
        if ($promotionFlag) {
            $select->group(array('salesInvoice.promotionID'));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;   
    } 

    public function getSalesByPromotionByDateRangeAndLocationID($startDate,$endDate, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('invTotal' => new Expression('SUM(salesinvoiceTotalAmount)'),'promotionID','customerID'))
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left');
        $select->join('promotion', 'salesInvoice.promotionID = promotion.promotionID', array('promotionName'), 'left');
        $select->where('salesInvoice.promotionID IS NOT NULL');
        $select->where->notEqualTo("salesInvoice.promotionID", 0);
        $select->where->notEqualTo("salesInvoice.statusID", 10);
        $select->where->notEqualTo("salesInvoice.statusID", 5);
        $select->where->notEqualTo("salesInvoice.customerID", 0);
        $select->where->between('salesInvoiceIssuedDate', $startDate, $endDate);
        $select->where(array('entity.deleted' => 0, 'salesInvoice.locationID' => $locationID));
        $select->group(array('salesInvoice.promotionID'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;   
    }

     /**
     * get jobcard related active invoice list
     * @param type $fromDate
     * @param type $toDate
     * @return array $result
     */
    public function getJobCardInvoicesForSalesDashboard($fromDate, $toDate, $cusCategory = NULL, $locationID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', ['documentTypeID', 'salesInvoiceProductDocumentID','salesInvoiceProductID','salesInvoiceProductQuantity','salesInvoiceProductGrandTotal' => new Expression('SUM(salesInvoiceProductTotal)')], 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array('saleInvoiceTotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left')
                // ->join("location", "salesInvoice.locationID = location.locationID", ["locationName", "locationCode"], "left")
                ->where(['entity.deleted' => 0])
        ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate)
        ->where->nest()->isNotNull('projectID')
        ->OR->isNotNull('jobID')
        ->OR->isNotNull('activityID');
        // $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName','customerCode'), 'left');
        $select->group(array('salesInvoice.salesInvoiceID'));

        if ($locationID != NULL) {
            $select->where(['salesInvoice.locationID' => $locationID]);
        }
        // if($cusCategory != ""){
        //     $select->where->in('customer.customerCategory', $cusCategory);
        // }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if (!$result) {
            return null;
        }
        return $result;
    }  

    public function getDeliveryNoteInvoicesForSalesDashboard($fromDate, $toDate, $cusCategory = NULL, $locationID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('salesInvoiceID', 'salesInvoiceCode', 'locationID', 'salesInvoiceIssuedDate', 'salesInvoiceTaxAmount'))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', ['documentTypeID', 'salesInvoiceProductDocumentID','salesInvoiceProductID','salesInvoiceProductQuantity','salesInvoiceProductTotal'], 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array('saleInvoiceTotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left')
                // ->join("location", "salesInvoice.locationID = location.locationID", ["locationName", "locationCode"], "left")
                ->where(array('entity.deleted' => 0, 'documentTypeID'=> 4))
                ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        $select->group(array('salesInvoice.salesInvoiceID'));
        if ($locationID != NULL) {
            $select->where(['salesInvoice.locationID' => $locationID]);
        }
        // $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName','customerCode'), 'left');
        // if($cusCategory != ""){
        //     $select->where->in('customer.customerCategory', $cusCategory);
        // }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if (!$result) {
            return null;
        }
        return $result;
    }  

    public function getNonInventoryInvoicesForSalesDashboard($fromDate, $toDate, $cusCategory = NULL, $locationID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array('salesInvoiceID', 'salesInvoiceCode', 'locationID', 'salesInvoiceIssuedDate', 'salesInvoiceTaxAmount'))
                ->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', ['documentTypeID', 'salesInvoiceProductDocumentID','salesInvoiceProductID','salesInvoiceProductQuantity','salesInvoiceProductGrandTotal' => new Expression('SUM(salesInvoiceProductTotal)')], 'left')
                ->join('locationProduct', 'locationProduct.locationProductID = salesInvoiceProduct.locationProductID', ['productID'], 'left')
                ->join('product', 'product.productID = locationProduct.productID', ['productTypeID'], 'left')
                ->join('salesInvoiceProductTax', 'salesInvoiceProduct.salesInvoiceProductID = salesInvoiceProductTax.salesInvoiceProductID', array('saleInvoiceTotalTax' => new Expression('SUM(salesInvoiceProductTax.salesInvoiceProductTaxAmount)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left')
                // ->join("location", "salesInvoice.locationID = location.locationID", ["locationName", "locationCode"], "left")
                ->where(array('entity.deleted' => 0, 'product.productTypeID'=> 2))
                ->where('salesInvoice.jobID IS NULL')
                ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate);
        $select->group(array('salesInvoice.salesInvoiceID'));
        if ($locationID != NULL) {
            $select->where(['salesInvoice.locationID' => $locationID]);
        }
        // $select->join("customer", "salesInvoice.customerID = customer.customerID", array('customerName','customerCode'), 'left');
        // if($cusCategory != ""){
        //     $select->where->in('customer.customerCategory', $cusCategory);
        // }
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        if (!$result) {
            return null;
        }
        return $result;
    }  

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param Date $fromDate
     * @param Date $toDate
     * @param array $salesPersonIds
     */
    public function getInvoiceDataBySalesPersonId($salesPersonId)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesInvoice")
                ->columns(array("*"));
        $select->where->equalTo('salesPersonID', $salesPersonId);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }  
}
