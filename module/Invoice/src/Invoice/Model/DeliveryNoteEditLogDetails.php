<?php
namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DeliveryNoteEditLogDetails implements InputFilterAwareInterface
{

    public $deliveryNoteEditLogDetailsID;
    public $deliveryNoteEditLogDetailsOldState;
    public $deliveryNoteEditLogDetailsNewState;
    public $deliveryNoteEditLogDetailsCategory;
    public $deliveryNoteEditLogID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->deliveryNoteEditLogDetailsID = (!empty($data['deliveryNoteEditLogDetailsID'])) ? $data['deliveryNoteEditLogDetailsID'] : null;
        $this->deliveryNoteEditLogDetailsOldState = (!empty($data['deliveryNoteEditLogDetailsOldState'])) ? $data['deliveryNoteEditLogDetailsOldState'] : null;
        $this->deliveryNoteEditLogDetailsNewState = (!empty($data['deliveryNoteEditLogDetailsNewState'])) ? $data['deliveryNoteEditLogDetailsNewState'] : null;
        $this->deliveryNoteEditLogDetailsCategory = (!empty($data['deliveryNoteEditLogDetailsCategory'])) ? $data['deliveryNoteEditLogDetailsCategory'] : null;
        $this->deliveryNoteEditLogID = (!empty($data['deliveryNoteEditLogID'])) ? $data['deliveryNoteEditLogID'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
