<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethodGiftCard
{

    public $incomingPaymentMethodGiftCardId;
    public $giftCardId;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodGiftCardId = (!empty($data['incomingPaymentMethodGiftCardId'])) ? $data['incomingPaymentMethodGiftCardId'] : null;
        $this->giftCardId = (!empty($data['giftCardId'])) ? $data['giftCardId'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
