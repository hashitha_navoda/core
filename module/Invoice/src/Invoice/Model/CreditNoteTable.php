<?php

/**
 * @author ashan     <ashan@thinkcube.com>
 * This file contains creditc note Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Between;

class CreditNoteTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE, $locationID)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('creditNote')
                    ->columns(array('*'))
                    ->join('customer', 'creditNote.customerID = customer.customerID', array('customerName', 'customerCode'), 'left')
                    ->join('currency', 'creditNote.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->order(array('creditNoteID' => 'DESC'))
                    ->where(array('locationID' => $locationID));
            $resultSetPrototype = new ResultSet();
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->where(array('locationID' => $locationID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function checkCreditNoteByCode($creditNoteCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('creditNote.creditNoteCode' => $creditNoteCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function saveCreditNote(CreditNote $creditNote)
    {
        $data = array(
            'creditNoteCode' => $creditNote->creditNoteCode,
            'customerID' => $creditNote->customerID,
            'creditNoteDate' => $creditNote->creditNoteDate,
            'creditNoteTotal' => $creditNote->creditNoteTotal,
            'locationID' => $creditNote->locationID,
            'paymentTermID' => $creditNote->paymentTermID,
            'invoiceID' => $creditNote->invoiceID,
            'creditNoteComment' => $creditNote->creditNoteComment,
            'statusID' => $creditNote->statusID,
            'creditNotePaymentEligible' => $creditNote->creditNotePaymentEligible,
            'creditNotePaymentAmount' => $creditNote->creditNotePaymentAmount,
            'customCurrencyId' => $creditNote->customCurrencyId,
            'creditNoteCustomCurrencyRate' => $creditNote->creditNoteCustomCurrencyRate,
            'entityID' => $creditNote->entityID,
            'creditNoteInvoiceDiscountType' => $creditNote->creditNoteInvoiceDiscountType,
            'creditNoteInvoiceDiscountAmount' => $creditNote->creditNoteInvoiceDiscountAmount,
            'creditNotePromotionDiscountType' => $creditNote->creditNotePromotionDiscountType,
            'creditNotePromotionDiscountAmount' => $creditNote->creditNotePromotionDiscountAmount,
            'creditNoteDirectFlag' => $creditNote->creditNoteDirectFlag,
            'creditNoteBuyBackFlag' => $creditNote->creditNoteBuyBackFlag,
            'pos' => $creditNote->pos,
            'creditDeviation' => $creditNote->creditDeviation,
            'outstandingDeviation' => $creditNote->outstandingDeviation,
            'ableToCancel' => $creditNote->ableToCancel,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getCreditNotesByInvoiceID($invoiceID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('creditNote.invoiceID' => $invoiceID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNotesByInvoiceIDForSPCalculation($invoiceID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('creditNote.invoiceID' => $invoiceID))
                    ->where->notEqualTo('creditNote.statusID',5)
                    ->where->notEqualTo('creditNote.statusID', 10);
            $query = $sql->prepareStatementForSqlObject($select);

            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNoteByCustomerIDAndLocationID($customerID, $locationID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->join('customer', "creditNote.customerID = customer.customerID")
                    ->join('currency', 'creditNote.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->where(array('e.deleted' => '0'))
                    ->where(array('creditNote.customerID' => $customerID, 'creditNote.locationID' => $locationID));
                    $select->where->notEqualTo('creditNote.statusID',5);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNoteByCustomerIDLocationIDAndCurrencyID($customerID, $locationID, $customCurrencyId = null)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->join('customer', "creditNote.customerID = customer.customerID")
                    ->join('currency', 'creditNote.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->where(array('e.deleted' => '0'))
                    ->where(array('creditNote.customerID' => $customerID, 'creditNote.locationID' => $locationID, 'creditNote.customCurrencyId' => $customCurrencyId));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNoteByCreditNoteID($creditNoteID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->join('customer', "creditNote.customerID = customer.customerID")
                    ->join('currency', 'creditNote.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->where(array('e.deleted' => '0'))
                    ->where(array('creditNote.creditNoteID' => $creditNoteID,));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNoteByCreditNoteIDForJE($creditNoteID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->join('customer', "creditNote.customerID = customer.customerID")
                    ->join('currency', 'creditNote.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                    ->join('user', 'e.createdBy = user.userID', array('userUsername'), 'left')
                    ->where(array('e.deleted' => '0'))
                    ->where(array('creditNote.creditNoteID' => $creditNoteID,));
            $select->where->notEqualTo('creditNote.statusID', 5);
            $select->where->notEqualTo('creditNote.statusID', 10);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCreditNotesByDate($fromdate, $todate, $customerID = NULL, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote")
                ->columns(array("*"))
                ->join("customer", "creditNote.customerID = customer.customerID", array("customerName", "customerShortName"), "left")
                ->join('currency', 'creditNote.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left')
                ->order(array('creditNoteDate' => 'DESC'))
                ->where(array("creditNote.locationID" => $locationID));
        if ($customerID != NULL) {
            $select->where(array("creditNote.customerID" => $customerID));
        }
        $select->where->between('creditNoteDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getActiveCreditNote($locationID,$pos = false, $cusIDs = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote")
                ->columns(array("*"))
                ->order(array('creditNoteDate' => 'DESC'))
                ->where(array("creditNote.locationID" => $locationID, 'statusID' => '3', 'creditNotePaymentEligible' => '1'));
        if($pos) {
            $select->where(array("creditNote.pos" => 1));
        }
        // for given customer and default customer credit notes
        if(count($cusIDs) > 0){
            $select->where->in('creditNote.customerID', $cusIDs);
        }
        //for default customer credit notes
        if(!count($cusIDs) > 0 && $pos){
            $select->where(array('creditNote.customerID' => 0));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

     public function getCreditNoteByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('salesInvoiceCode'),'left')
                ->join('customer', 'customer.customerID = creditNote.customerID', array('*'),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array('statusName'),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('creditNoteDate' => 'DESC'))
                ->where(array('entity.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('creditNoteDate', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    } 

    public function getDirectCreditNoteByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('salesInvoiceCode'),'left')
                ->join('customer', 'customer.customerID = creditNote.customerID', array('*'),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array('statusName'),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('creditNoteDate' => 'DESC'))
                ->where(array('entity.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.creditNoteDirectFlag', 0);
                $select->where->EqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('creditNoteDate', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getLinkedDirectCreditNoteByCustomerIDs($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('salesInvoiceCode'),'left')
                ->join('customer', 'customer.customerID = creditNote.customerID', array('*'),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array('statusName'),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array('deleted'), 'left')
                ->order(array('creditNoteDate' => 'DESC'))
                ->where(array('entity.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.creditNoteDirectFlag', 0);
                $select->where->notEqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('creditNoteDate', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCustomerCumulativeCreditNoteBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('customerID','cumulative_total' => new \Zend\Db\Sql\Expression('SUM(creditNoteTotal)')))
                ->join('customer', 'customer.customerID = creditNote.customerID', array(),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array(),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array(), 'left')
                ->where(array('entity.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('creditNote.creditNoteDate', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $select->group(array("creditNote.customerID"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCustomerDirectCumulativeCreditNoteBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('customerID','cumulative_total' => new \Zend\Db\Sql\Expression('SUM(creditNoteTotal)')))
                ->join('customer', 'customer.customerID = creditNote.customerID', array(),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array(),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array(), 'left')
                ->where(array('entity.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.creditNoteDirectFlag', 0);
                $select->where->EqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('creditNote.creditNoteDate', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $select->group(array("creditNote.customerID"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCustomerLinkedDirectCumulativeCreditNoteBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('customerID','cumulative_total' => new \Zend\Db\Sql\Expression('SUM(creditNoteTotal)')))
                ->join('customer', 'customer.customerID = creditNote.customerID', array(),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array(),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array(), 'left')
                ->where(array('entity.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.creditNoteDirectFlag', 0);
                $select->where->notEqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('creditNote.creditNoteDate', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $select->group(array("creditNote.customerID"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function updateCreditNoteSettleAmountAndStatusID(CreditNote $creditNote)
    {
        $creditNoteData = array(
            'creditNoteSettledAmount' => $creditNote->creditNoteSettledAmount,
            'statusID' => $creditNote->statusID,
        );

        $res = $this->tableGateway->update($creditNoteData, array('creditNoteID' => $creditNote->creditNoteID));
        return TRUE;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search Active Credit note for drop down
     * @param string $locationID
     * @param string $searchKey
     * @return Array limited to maximum 50 rows
     */
    public function searchActiveCreditNoteForDropdown($locationID, $searchKey)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote")
                ->columns(array("creditNoteID", "creditNoteCode"))
                ->order(array('creditNoteDate' => 'DESC'))
                ->where(array("creditNote.locationID" => $locationID, 'statusID' => '3', 'creditNotePaymentEligible' => '1'));
        $select->where->like('creditNoteCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Search All Credit note for drop down
     * @param string $locationID
     * @param string $searchKey
     * @return Array limited to maximum 50 rows
     */
    public function searchAllCreditNoteForDropdown($locationID, $searchKey)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote")
                ->columns(array("creditNoteID", "creditNoteCode"))
                ->order(array('creditNoteDate' => 'DESC'))
                ->where(array("creditNote.locationID" => $locationID));
        $select->where->like('creditNoteCode', '%' . $searchKey . '%');
        $select->limit(50);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCreditNoteByCode($creditNoteCode)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('creditNoteID'));
        $select->join('location', "location.locationID = creditNote.locationID", array('locationName', 'locationCode'), 'left');
        $select->where(array('creditNoteCode' => $creditNoteCode));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getCreditNoteById($creditNoteId)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('creditNoteID', 'creditNoteCode'));
        $select->join('location', "location.locationID = creditNote.locationID", array('locationName', 'locationCode'), 'left');
        $select->where(array('creditNoteID' => $creditNoteId));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param date $fromdate
     * @param date $todate
     * @param array $locationID
     * @return array $results
     */
    public function getCreditNoteDetails($fromdate = NULL, $todate = NULL, $locationID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote")
                ->columns(array("*"))
                ->join("salesInvoice", "creditNote.invoiceID = salesInvoice.salesInvoiceID", array(
                    "salesinvoiceTotalAmount",
                    "salesInvoiceCode"), "left")
                ->join("customer", "creditNote.customerID = customer.customerID", array(
                    "customerTitle",
                    "customerName", "customerCode"), "left")
                ->join("location", "location.locationID = creditNote.locationID", array("locationName", "locationCode"), "left")
                ->order(array('creditNoteID'))
        ->where->in("creditNote.locationID", $locationID);
        $select->where->between('creditNoteDate', $fromdate, $todate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCreditNoteTotalByInvoiceID($invoiceID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*', 'creditNoteTot' => new Expression('SUM(creditNoteTotal)')))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('creditNote.invoiceID' => $invoiceID));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }


    public function getCreditNoteTotalByInvoiceIDArray($invoiceID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*', 'creditNoteTot' => new Expression('SUM(creditNoteTotal)')))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->join('salesInvoice','creditNote.invoiceID = salesInvoice.salesInvoiceID', array('salesInvoiceCode', 'salesInvoiceID'), 'left')
                    ->where(array('e.deleted' => '0'));
            $select->where->in('creditNote.invoiceID',$invoiceID);
            $select->group('creditNote.invoiceID');
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    function getCreditData($fromData, $toData, $format = NULL)
    {
        $groupArray = array();
        if ($format == 'year') {
            $groupArray = array('Year');
        } else if ($format == 'month') {
            $groupArray = array('Month', 'Year');
        } else {
            $groupArray = array('Date', 'Month', 'Year');
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`creditNoteDate`, "%d")'),
                    'Year' => new Expression('Year(creditNoteDate)'),
                    'TotalCredit' => new Expression('SUM(creditNoteTotal)')
                ))
                ->join('entity', 'entity.entityID = creditNote.entityID', '*', 'left')
                ->group($groupArray)
                ->order(array('creditNote.creditNoteDate'))
        ->where->between('creditNoteDate', $fromData, $toData);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        foreach ($results as $val) {
            $resultArray[] = $val;
        }
        return $resultArray;
    }

    public function getCurrentCreditBy($type)
    {
        $whereClause = '';
        if ($type == 'year') {
            $whereClause = date('Y');
        } else if ($type == 'month') {
            $whereClause = date('Y-m');
        } else {
            $whereClause = date('Y-m-d');
        }
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('Month' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M")'),
                    'MonthWithYear' => new Expression('DATE_FORMAT(`creditNoteDate`, "%M %Y")'),
                    'Date' => new Expression('DATE_FORMAT(`creditNoteDate`, "%d")'),
                    'Year' => new Expression('Year(creditNoteDate)'),
                    'TotalCredit' => new Expression('SUM(creditNoteTotal)')
                ))
                ->order(array('creditNote.creditNoteDate'))
        ->where->like('creditNoteDate', '%' . $whereClause . '%');

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getCreditNoteSummeryDetails($fromDate = NULL, $toDate = NULL, $userIds = NULL, $locationIds = NULL, $invoiceStatusIds = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote")
                ->columns(array("*"))
                ->join("salesInvoice", "creditNote.invoiceID = salesInvoice.salesInvoiceID", array("*"), "left")
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('*'), 'left')
                ->join("location", "location.locationID = creditNote.locationID", array("locationName", "locationCode"), "left");
                $select->where(array('salesInvoice.pos' => '1'));
        if ($fromDate && $toDate) {
            $select->where->between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate);
        }
        if ($userIds) {
            $select->where->in('entity.createdBy', $userIds);
        }
        if ($invoiceStatusIds) {
            $select->where->in('salesInvoice.statusID', $invoiceStatusIds);
        }
        if ($locationIds) {
            $select->where->in('salesInvoice.locationID', $locationIds);
        }
        $select->where(array('entity.deleted' => '0'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }
    /**
    * get all credit note details that related to given date range and locationID
    *
    **/
    public function getCreditNoteDetailsByInvoiceDate($fromDate, $toDate, $locationID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('creditNoteID' => 'creditNoteID',
                    'TotalCreditAmount' => 'creditNoteTotal',
                    'invoiceID' => 'invoiceID'))
                ->join('salesInvoice', 'creditNote.invoiceID = salesInvoice.salesInvoiceID', array('salesInvoiceIssuedDate'), 'left')
                ->join('creditNoteProduct', 'creditNote.creditNoteID = creditNoteProduct.creditNoteID', array('creditNoteProductID'), 'left')
                ->join('creditNoteProductTax', 'creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID', array('creditNoteTaxAmount'), 'left')
                ->where->between('salesInvoiceIssuedDate', $fromDate, $toDate)
                ->where->notEqualTo('creditNote.statusID',5);

        if ($locationID != "") {
            $select->where->in('salesInvoice.locationID', $locationID);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    /**
     * Get total of the credit notes by invoice id
     * @param int $invoiceId
     * @return mixed
     */
    public function getInvoiceCreditNoteTotalByInvoiceId($invoiceId) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns([
                    'total' => new Expression('SUM(creditNoteTotal)')
                ])
                ->join('entity', "creditNote.entityID = entity.entityID", ['deleted'])
                ->where(['entity.deleted' => '0'])
                ->where(['creditNote.invoiceID' => $invoiceId]);
        $query = $sql->prepareStatementForSqlObject($select);
        return $query->execute()->current();
    }

     /**
     * Get total of the credit notes by invoice id
     * @param int $invoiceId
     * @return mixed
     */
    public function getInvoiceCreditNoteTotalByInvoiceIdForAge($invoiceId, $endDate) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns([
                    'total' => new Expression('SUM(creditNoteTotal)')
                ])
                ->join('entity', "creditNote.entityID = entity.entityID", ['deleted'])
                ->where(['entity.deleted' => '0'])
                ->where(['creditNote.invoiceID' => $invoiceId]);
        $select->where->notEqualTo('creditNote.statusID', 5);
        $select->where->lessThanOrEqualTo('creditNoteDate', $endDate);
        $query = $sql->prepareStatementForSqlObject($select);
        return $query->execute()->current();
    }

    /**
     * Get credit note details by sales person ids
     * @param string $fromDate
     * @param string $toDate
     * @param array $locationIds
     * @param array $salesPersonIds
     * @param boolean $withDefaultSalesPerson
     * @return mixed
     */
    public function getCreditNoteDetailsBySalesPersonIds($fromDate = null, $toDate = null, $locationIds = [], $salesPersonIds = [], $withDefaultSalesPerson = false, $cusCategory = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->join('salesInvoice', 'creditNote.invoiceID = salesInvoice.salesInvoiceID', ['locationID','salesPersonID','customerID','salesInvoiceCode','salesinvoiceTotalAmount', 'salesInvoiceID'], 'left')
                ->join('location', 'creditNote.locationID = location.locationID', ['locationName'], 'left')
                ->join('customer', 'creditNote.customerID = customer.customerID', ['customerName','customerCode'], 'left')
                ->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left')
                ->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left')
                ->join("entity", "creditNote.entityID = entity.entityID", array("deleted"), "left")
                ->where->equalTo('entity.deleted', 0);
        if ($fromDate && $toDate) {
            $select->where->between('creditNote.creditNoteDate', $fromDate, $toDate);
        }
        if ($locationIds) {
            $select->where->in('creditNote.locationID', $locationIds);
        }
        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        if($withDefaultSalesPerson) {
            $select->where->AND->NEST
                ->in('invoiceSalesPersons.salesPersonID', $salesPersonIds)->orPredicate(
                    new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
            );
        } else {
            $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }

    /**
    * get credit note tax values that given sales persons and product ids.
    * @param array $productId
    * @param array $salesPersonId
    * @param date $fromDate
    * @param date $toDate
    * return array
    **/
    public function getCreditNoteTaxesBySalesPersonIdsAndProIDs($productId , $salesPersonId , $fromDate = null, $toDate = null, $withDefaultSalesPerson = false)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('creditNoteID'))
                ->join('salesInvoice', 'creditNote.invoiceID = salesInvoice.salesInvoiceID',array('salesInvoiceID','salesInvoiceCode','salesInvoiceIssuedDate'), 'left')
                ->join("invoiceSalesPersons", "invoiceSalesPersons.invoiceID = salesInvoice.salesInvoiceID", array('*'), 'left')
                ->join("salesPerson", "salesPerson.salesPersonID = invoiceSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName'), 'left')
                ->join('creditNoteProduct', 'creditNote.creditNoteID = creditNoteProduct.creditNoteID', array('creditNoteProductID'), 'left')
                ->join('creditNoteProductTax', 'creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID',array('totalCreditNoteTax' => new Expression('SUM(creditNoteTaxAmount)')), 'left')
                ->join('entity', 'salesInvoice.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0 ))
                ->group('creditNote.invoiceID');
        
        $select->where->in('creditNoteProduct.productID', $productId);
        if($withDefaultSalesPerson) {
                $select->where->AND->NEST
                ->in('invoiceSalesPersons.salesPersonID', $salesPersonId)->orPredicate(
                    new \Zend\Db\Sql\Predicate\IsNull('invoiceSalesPersons.salesPersonID')
                );
        } else {
            $select->where->in('invoiceSalesPersons.salesPersonID', $salesPersonId);
        }
        
        if ($fromDate && $toDate) {
            $select->where(new Between('salesInvoice.salesInvoiceIssuedDate', $fromDate, $toDate));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getCreditNoteByIdForDeleteCreditNote($id)
    {
        $rowset = $this->tableGateway->select(array('creditNoteID' => $id));
        $row = $rowset->current();
        if (!$row) {
            return NULL;
        }
        return $row;
    }

    public function updateCreditNoteStatusForCancel($creditNoteID, $statusID)
    {
        $data = array(
            'statusID' => $statusID
        );
        return $this->tableGateway->update($data, array('creditNoteID' => $creditNoteID));
    }

    public function getCreditNoteTotalByInvoiceIDForOpenInvoice($invoiceID)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('creditNote')
                    ->columns(array('*', 'creditNoteTot' => new Expression('SUM(creditNoteTotal)')))
                    ->join(array('e' => 'entity'), "creditNote.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('creditNote.invoiceID' => $invoiceID))
                    ->where->notEqualTo('creditNote.statusID', 5);
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getPosAndcreditNoteDailyCreditNoteItems($fromDate, $toDate, $location = null)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('*'))
                ->join('creditNoteProduct', 'creditNote.creditNoteID = creditNoteProduct.creditNoteID', array('productID', 'creditNoteProductQuantity' => new Expression('COALESCE(SUM(creditNoteProductQuantity),0)'),
                    'creditNoteProductTotal' => new Expression('SUM(creditNoteProductTotal)')), 'left')
                ->join('product', 'creditNoteProduct.productID = product.productID', array(
                    'productCode',
                    'productName' => new Expression('product.productName')), 'left')
                ->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left')
                ->group(array('creditNoteProduct.productID'))
                ->where(array(
                    'creditNote.locationID' => $location,
                    'deleted' => 0)
                )
        ->where->between('creditNote.creditNoteDate', $fromDate, $toDate);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    public function getCreditNoteDailySalesNonInventoryItems($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('*'));
        $select->join('creditNoteProduct', 'creditNote.creditNoteID = creditNoteProduct.creditNoteID', array('productID',
            'creditNoteProductPrice',
            'creditNoteProductQuantity',
            'creditNoteProductDiscountType',
            'creditNoteProductDiscount',
            'creditNoteProductTotal'), 'left');
        $select->join('product', 'creditNoteProduct.productID = product.productID', array('productName' => new Expression('product.productName'), 'productCode' => new Expression('product.productCode')), 'left');
        $select->join("entity", "entity.entityID = creditNote.entityID", array('deleted'), 'left');
        $select->where([
            'creditNote.pos' => '0',
            'product.productTypeID' => 2,
            'deleted' => 0]);
        $select->where->between('creditNote.creditNoteDate', $fromDate, $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultSet = [];
        foreach ($results as $val) {
            $resultSet[] = $val;
        }
        return $resultSet;
    }

    public function getPosCreditNoteData($fromDate = null, $toDate = null, $userIds = NULL, $locIds = NULL, $discount = FALSE)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote");
        $select->columns(array("*"));
        $select->join('location', 'location.locationID = creditNote.locationID', array('*'), 'left');
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('sCode' => new Expression('salesInvoice.salesInvoiceCode')), 'left');
        $select->join('entity', 'creditNote.entityID = entity.entityID', array('*'), 'left');
        $select->join('user', 'user.userID = entity.createdBy', array('*'), 'left');
        $select->order(array('creditNote.creditNoteID' => 'DESC'));
        if ($fromDate != NULL && $toDate != NULL) {
            $select->where->between('creditNoteDate', $fromDate, $toDate);
        }
        if ($userIds != NULL) {
            $select->where->in('entity.createdBy', $userIds);
        }
        if ($locIds != NULL) {
            $select->where->in('creditNote.locationID', $locIds);
        }
        if ($discount) {
            $select->where->greaterThan('creditNoteInvoiceDiscountAmount', 0);
        }
        $select->where(array('deleted' => '0'));
        $select->where(array('creditNote.pos' => '1'));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getPosCreditNotePaymentData($fromDate = NULL, $toDate = NULL, $locIds = NULL, $userIds = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('*'));
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('sCode' => new Expression('salesInvoice.salesInvoiceCode')), 'left');
        $select->join('customer', 'customer.customerID = creditNote.customerID', array('customerName', 'customerTitle'), 'left');
        $select->join('entity', 'entity.entityID = creditNote.entityID', array('deleted','createdBy'), 'left');
        $select->join('user', 'entity.createdBy = user.userID', ['createdUser' => new Expression('user.userUsername')], 'left');
        $select->join('location', 'creditNote.locationID = location.locationID', array('locationName'), 'left');
        $select->join('currency', 'creditNote.customCurrencyId = currency.currencyID', array('currencySymbol'), 'left');
        $select->order(array('creditNote.creditNoteID' => 'DESC'));
        $select->where(array('creditNote.pos' => 1));
        if ($fromDate != NULL && $toDate != NULL) {
            $select->where->between('creditNoteDate', $fromDate, $toDate);
        }
        if ($locIds != NULL) {
            $select->where->in('creditNote.locationID', $locIds);
        }
        if ($userIds != NULL) {
            $select->where->in('entity.createdBy', $userIds);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return NULL;
        }
        return $results;
    }

    // get delivery note related credit note by quotation id
    public function getDeliveryNoteRelatedCreditNoteDataByQuotationId($quotationID = null, $soID = null, $deliveryNoteID = null, $invoiceID = null, $returnID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('*'));
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
        $select->join('salesInvoiceProduct', 'salesInvoice.salesInvoiceID = salesInvoiceProduct.salesInvoiceID', array('*'),'left');
        $select->join('deliveryNote', 'salesInvoiceProduct.salesInvoiceProductDocumentID = deliveryNote.deliveryNoteID', array('*'),'left');
        $select->join('salesOrders', 'deliveryNote.salesOrderID = salesOrders.soID', array('*'),'left');
        $select->join('entity', 'creditNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->where(array('salesOrders.quotationID' => $quotationID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($soID)) {
            $select->where(array('salesOrders.soID' => $soID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($deliveryNoteID)) {
            $select->where(array('deliveryNote.deliveryNoteID' => $deliveryNoteID, 'salesInvoiceProduct.documentTypeID' => 4));
        }
        if (!is_null($invoiceID)) {
            $select->where(array('salesInvoice.salesInvoiceID' => $invoiceID));   
        }
        if (!is_null($returnID)) {
            $select->join('deliveryNoteProduct', "deliveryNote.deliveryNoteID = deliveryNoteProduct.deliveryNoteID", array('*'), 'left');
            $select->join('salesReturnProduct', "salesReturnProduct.deliveryNoteProductID = deliveryNoteProduct.deliveryNoteProductID", array('*'), 'left');
            $select->join('salesReturn', "salesReturn.salesReturnID = salesReturnProduct.salesReturnID", array('*'), 'left');
            $select->where(array('salesReturn.salesReturnID' => $returnID));
        }
        $select->group("creditNote.creditNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get sales order related credit note by quotation id
    public function getSalesOrderRelatedCreditNoteDataByQuotationId($quotationID = null, $soID = null)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('*'));
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
        $select->join('salesOrders', 'salesInvoice.salesOrderID = salesOrders.soID', array('*'),'left');
        $select->join('entity', 'creditNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        if (!is_null($quotationID)) {
            $select->where(array('salesOrders.quotationID' => $quotationID));
        }
        if (!is_null($soID)) {
            $select->where(array('salesOrders.soID' => $soID));
        }
        $select->group("creditNote.creditNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get quotation related credit note by quotation id
    public function getQuotationRelatedCreditNoteDataByQuotationId($quotationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('*'));
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('*'), 'left');
        $select->join('entity', 'creditNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('salesInvoice.quotationID' => $quotationID));
        $select->group("creditNote.creditNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
       
        return $rowset;
    }

    // get credit note basic details with time stamp by credit note id 
    public function getCreditNoteBasicDetailsByCreditNoteID($creditNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote');
        $select->columns(array('*'));
        $select->join('entity', 'creditNote.entityID = entity.entityID', array('deleted','createdTimeStamp'));
        $select->where(array('creditNote.creditNoteID' => $creditNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        $row = $result->current();
        return $row;

    }
    
    public function getCustomerCumulativeCreditNoteBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('customerID','creditNoteTotal','creditNoteID','invoiceID'))
                ->join('customer', 'customer.customerID = creditNote.customerID', array(),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'creditNote.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCustomerCumulativeDirectCreditNoteBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('customerID','creditNoteTotal','creditNoteID','invoiceID'))
                ->join('customer', 'customer.customerID = creditNote.customerID', array(),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'creditNote.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.creditNoteDirectFlag', 0);
                $select->where->EqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCustomerCumulativeLinkedDirectCreditNoteBalanceForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('customerID','creditNoteTotal','creditNoteID','invoiceID'))
                ->join('customer', 'customer.customerID = creditNote.customerID', array(),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array(),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'creditNote.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.creditNoteDirectFlag', 0);
                $select->where->notEqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null){
            $select->where->lessThan('entity2.createdTimeStamp', $fromDate);
        } else {
            return null;
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }

        $resultSet = new ResultSet();

        $resultSet->initialize($results);

        return $resultSet->toArray();

    }

    public function getCreditNoteByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('*'))
                ->join('salesInvoice', 'salesInvoice.salesInvoiceID = creditNote.invoiceID', array('salesInvoiceCode'),'left')
                ->join('customer', 'customer.customerID = creditNote.customerID', array('*'),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'creditNote.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.creditNoteDirectFlag', 1);
                $select->where->notEqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDirectCreditNoteByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('*'))
                ->join('customer', 'customer.customerID = creditNote.customerID', array('*'),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'creditNote.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.creditNoteDirectFlag', 0);
                $select->where->EqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDirectCreditNoteLinkedWithINVByCustomerIDsForCustomerBalance($cusIds, $isAllCustomers, $locations, $isAllLocation, $fromDate, $endDate, $cusCategory = null, $status)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('*'))
                ->join('customer', 'customer.customerID = creditNote.customerID', array('*'),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array('statusName'),'left')
                ->join(['entity1' => 'entity'], 'customer.entityID = entity1.entityID', array('deleted'), 'left')
                ->join(['entity2' => 'entity'], 'creditNote.entityID = entity2.entityID', array('createdTimeStamp'), 'left')
                ->order(array('entity2.createdTimeStamp' => 'DESC'))
                ->where(array('entity1.deleted' => '0'));
                $select->where->in('creditNote.statusID',$status);
                $select->where->notEqualTo('creditNote.creditNoteDirectFlag', 0);
                $select->where->notEqualTo('creditNote.invoiceID', 0);
        if (!$isAllCustomers) {
            $select->where->in('customer.customerID', $cusIds);
        }

        if (!$isAllLocation) {
            $select->where->in('creditNote.locationID', $locations);
        }

        if($fromDate != null && $endDate != null ){
            $select->where->between('entity2.createdTimeStamp', $fromDate, $endDate);
        }

        if($cusCategory != ""){
            $select->join("customerCategory", "customer.customerCategory = customerCategory.customerCategoryID", array('customerCategoryName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoiceRelatedCreditNotesForInvoiceRemainingAmount($invoiceID, $status, $creditNoteID)
    {
        $isAllCustomers = filter_var($isAllCustomers, FILTER_VALIDATE_BOOLEAN);
        $isAllLocation = filter_var($isAllLocation, FILTER_VALIDATE_BOOLEAN);
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNote')
                ->columns(array('customerID','TotalCreditAmountAfterInvoice' => new \Zend\Db\Sql\Expression('SUM(creditNoteTotal)')))
                ->join('customer', 'customer.customerID = creditNote.customerID', array(),'left')
                ->join('status', 'creditNote.statusID = status.statusID',array(),'left')
                ->join('entity', 'customer.entityID = entity.entityID', array(), 'left')
                ->where(array('entity.deleted' => '0', 'creditNote.invoiceID' => $invoiceID));
                $select->where->in('creditNote.statusID',$status);
                $select->where->lessThan('creditNote.creditNoteID', $creditNoteID);
        $select->group(array("creditNote.invoiceID"));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        if ($results->count() == 0) {
            return NULL;
        }
        return $results->current();
    }

    /*
    get direct credit note
    */
    public function getDirectCreditNote($fromdate, $todate, $cusCategory = NULL, $locationID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote");
        $select->columns(array("*"));
        $select->join("creditNoteProduct", "creditNote.creditNoteID = creditNoteProduct.creditNoteID", array("productID", "creditNoteProductTotal", "creditNoteProductQuantity"), "left");
        $select->join('product', 'product.productID = creditNoteProduct.productID', ['productTypeID'], 'left');
        $select->join('creditNoteProductTax', 'creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID', array('creditNoteTotalTax' => new Expression('SUM(creditNoteProductTax.creditNoteTaxAmount)')), 'left');
        $select->join("customer", "creditNote.customerID = customer.customerID", array("customerName", "customerShortName"), "left");
        $select->join('entity', 'creditNote.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left');
        $select->join("location", "creditNote.locationID = location.locationID", ["locationName", "locationCode"], "left");
        $select->where(array('entity.deleted' => 0, 'creditNote.creditNoteDirectFlag'=> 1));
        $select->where->between('creditNoteDate', $fromdate, $todate);
        $select->group(array('creditNote.creditNoteID'));
        if ($cusCategory != NULL) {
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        if ($locationID != NULL) {
            $select->where->in('creditNote.locationID', $locationID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /*
    get direct credit note which copy from delivery notes
    */
    public function getCreditNoteFromDLN($fromdate, $todate, $cusCategory = NULL, $locationID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote");
        $select->columns(array("*"));
        $select->join("creditNoteProduct", "creditNote.creditNoteID = creditNoteProduct.creditNoteID", array("productID", "creditNoteProductTotal", "creditNoteProductQuantity"), "left");
        $select->join('product', 'product.productID = creditNoteProduct.productID', ['productTypeID'], 'left');
        $select->join('creditNoteProductTax', 'creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID', array('creditNoteTotalTax' => new Expression('SUM(creditNoteProductTax.creditNoteTaxAmount)')), 'left');
        $select->join("customer", "creditNote.customerID = customer.customerID", array("customerName", "customerShortName"), "left");
        $select->join('entity', 'creditNote.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left');
        $select->join("location", "creditNote.locationID = location.locationID", ["locationName", "locationCode"], "left");
        $select->join('salesInvoiceProduct', 'creditNote.invoiceID = salesInvoiceProduct.salesInvoiceID', array('documentTypeID', 'salesInvoiceProductDocumentID'), 'left');
        $select->where(array('entity.deleted' => 0, 'salesInvoiceProduct.documentTypeID'=> 4));
        $select->where->between('creditNote.creditNoteDate', $fromdate, $todate);
        $select->group(array('creditNote.creditNoteID'));
        if ($cusCategory != NULL) {
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        if ($locationID != NULL) {
            $select->where->in('creditNote.locationID', $locationID);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCreditNoteTotalByDateRangeAndLocationID($fromdate, $todate, $locationID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns([
                    'total' => new Expression('SUM(creditNoteTotal)')
                ])
                ->join('entity', "creditNote.entityID = entity.entityID", ['deleted'])
                ->where(['entity.deleted' => '0', 'creditNote.locationID' => $locationID]);
        $select->where->between('creditNote.creditNoteDate', $fromdate, $todate);
        $query = $sql->prepareStatementForSqlObject($select);
        return $query->execute()->current();
    }

    public function getCreditNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromdate, $todate, $locationID, $invoiceIDs) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('creditNote')
                ->columns([
                    'total' => new Expression('SUM(creditNoteTotal)')
                ])
                ->join('entity', "creditNote.entityID = entity.entityID", ['deleted'])
                ->where(['entity.deleted' => '0', 'creditNote.locationID' => $locationID]);
        $select->where->in('creditNote.invoiceID', $invoiceIDs);
        $select->where->between('creditNote.creditNoteDate', $fromdate, $todate);
        $query = $sql->prepareStatementForSqlObject($select);
        return $query->execute()->current();
    }

     /*
    get direct credit note
    */
    public function getDirectCreditNoteForSalesDashboard($fromdate, $todate, $cusCategory = NULL, $locationID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote");
        $select->columns(array("creditNoteID", "creditNoteCode", "creditNoteTotal", "locationID", "creditNoteDate"));
        $select->join("creditNoteProduct", "creditNote.creditNoteID = creditNoteProduct.creditNoteID", array("productID", "creditNoteProductTotal", "creditNoteProductQuantity"), "left");
        $select->join('product', 'product.productID = creditNoteProduct.productID', ['productTypeID'], 'left');
        $select->join('creditNoteProductTax', 'creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID', array('creditNoteTotalTax' => new Expression('SUM(creditNoteProductTax.creditNoteTaxAmount)')), 'left');
        // $select->join("customer", "creditNote.customerID = customer.customerID", array("customerName", "customerShortName"), "left");
        $select->join('entity', 'creditNote.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left');
        // $select->join("location", "creditNote.locationID = location.locationID", ["locationName", "locationCode"], "left");
        $select->where(array('entity.deleted' => 0, 'creditNote.creditNoteDirectFlag'=> 1));
        $select->where->between('creditNoteDate', $fromdate, $todate);
        $select->group(array('creditNote.creditNoteID'));
        // if ($cusCategory != NULL) {
        //     $select->where->in('customer.customerCategory', $cusCategory);
        // }
        if ($locationID != NULL) {
            $select->where(['creditNote.locationID' => $locationID]);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /*
    get direct credit note which copy from delivery notes
    */
    public function getCreditNoteFromDLNForSalesDashboard($fromdate, $todate, $cusCategory = NULL, $locationID = NULL)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNote");
        $select->columns(array("creditNoteID", "creditNoteCode", "creditNoteTotal", "locationID", "creditNoteDate"));
        $select->join("creditNoteProduct", "creditNote.creditNoteID = creditNoteProduct.creditNoteID", array("productID", "creditNoteProductTotal", "creditNoteProductQuantity"), "left");
        $select->join('product', 'product.productID = creditNoteProduct.productID', ['productTypeID'], 'left');
        $select->join('creditNoteProductTax', 'creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID', array('creditNoteTotalTax' => new Expression('SUM(creditNoteProductTax.creditNoteTaxAmount)')), 'left');
        // $select->join("customer", "creditNote.customerID = customer.customerID", array("customerName", "customerShortName"), "left");
        $select->join('entity', 'creditNote.entityID = entity.entityID', ['deleted', 'createdTimeStamp'], 'left');
        // $select->join("location", "creditNote.locationID = location.locationID", ["locationName", "locationCode"], "left");
        $select->join('salesInvoiceProduct', 'creditNote.invoiceID = salesInvoiceProduct.salesInvoiceID', array('documentTypeID', 'salesInvoiceProductDocumentID'), 'left');
        $select->where(array('entity.deleted' => 0, 'salesInvoiceProduct.documentTypeID'=> 4));
        $select->where->between('creditNote.creditNoteDate', $fromdate, $todate);
        $select->group(array('creditNote.creditNoteID'));
        // if ($cusCategory != NULL) {
        //     $select->where->in('customer.customerCategory', $cusCategory);
        // }
        if ($locationID != NULL) {
            $select->where(['creditNote.locationID' => $locationID]);
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }
}
