<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DeliveryNoteProduct
{

    public $deliveryNoteProductID;
    public $deliveryNoteID;
    public $productID;
    public $deliveryNoteProductPrice;
    public $deliveryNoteProductDiscount;
    public $deliveryNoteProductDiscountType;
    public $deliveryNoteProductTotal;
    public $deliveryNoteProductQuantity;
    public $copied;
    public $deliveryNoteProductCopiedQuantity;
    public $documentTypeID;
    public $deliveryNoteProductDocumentID;
    public $deliveryNoteProductDocumentTypeCopiedQty;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->deliveryNoteProductID = (!empty($data['deliveryNoteProductID'])) ? $data['deliveryNoteProductID'] : null;
        $this->deliveryNoteID = (!empty($data['deliveryNoteID'])) ? $data['deliveryNoteID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->deliveryNoteProductPrice = (!empty($data['deliveryNoteProductPrice'])) ? $data['deliveryNoteProductPrice'] : null;
        $this->deliveryNoteProductDiscount = (!empty($data['deliveryNoteProductDiscount'])) ? $data['deliveryNoteProductDiscount'] : null;
        $this->deliveryNoteProductDiscountType = (!empty($data['deliveryNoteProductDiscountType'])) ? $data['deliveryNoteProductDiscountType'] : null;
        $this->deliveryNoteProductTotal = (!empty($data['deliveryNoteProductTotal'])) ? $data['deliveryNoteProductTotal'] : null;
        $this->deliveryNoteProductQuantity = (!empty($data['deliveryNoteProductQuantity'])) ? $data['deliveryNoteProductQuantity'] : null;
        $this->copied = (!empty($data['copied'])) ? $data['copied'] : 0;
        $this->deliveryNoteProductCopiedQuantity = (!empty($data['deliveryNoteProductCopiedQuantity'])) ? $data['deliveryNoteProductCopiedQuantity'] : 0;
        $this->documentTypeID = (!empty($data['documentTypeID'])) ? $data['documentTypeID'] : 0;
        $this->deliveryNoteProductDocumentID = (!empty($data['deliveryNoteProductDocumentID'])) ? $data['deliveryNoteProductDocumentID'] : 0;
        $this->deliveryNoteProductDocumentTypeCopiedQty = (!empty($data['deliveryNoteProductDocumentTypeCopiedQty'])) ? $data['deliveryNoteProductDocumentTypeCopiedQty'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
