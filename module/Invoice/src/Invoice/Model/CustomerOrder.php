<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CustomerOrder
{

    public $customerOrderID;
    public $customerOrderCode;
    public $customerID;
    public $customerOrderDate;
    public $customerOrderAmount;
    public $customerOrderComment;
    public $customerOrderReference;
    public $customerOrderStatus;
    public $locationId;
    public $entityID;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->customerOrderID = (!empty($data['customerOrderID'])) ? $data['customerOrderID'] : null;
        $this->customerOrderCode = (!empty($data['customerOrderCode'])) ? $data['customerOrderCode'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->customerOrderDate = (!empty($data['customerOrderDate'])) ? $data['customerOrderDate'] : null;
        $this->customerOrderAmount = (!empty($data['customerOrderAmount'])) ? $data['customerOrderAmount'] : null;
        $this->customerOrderComment = (!empty($data['customerOrderComment'])) ? $data['customerOrderComment'] : null;
        $this->customerOrderReference = (!empty($data['customerOrderReference'])) ? $data['customerOrderReference'] : null;
        $this->customerOrderStatus = (!empty($data['customerOrderStatus'])) ? $data['customerOrderStatus'] : null;
        $this->locationId = (!empty($data['locationId'])) ? $data['locationId'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }


}
