<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains returns sub product Table Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;

class ReturnsSubProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();

        return $rowset;
    }

    public function saveReturnSubProduct(ReturnsSubProduct $returnSubProduct)
    {
        $data = array(
            'salesReturnProductID' => $returnSubProduct->salesReturnProductID,
            'productBatchID' => $returnSubProduct->productBatchID,
            'productSerialID' => $returnSubProduct->productSerialID,
            'salesReturnSubProductQuantity' => $returnSubProduct->salesReturnSubProductQuantity,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

}
