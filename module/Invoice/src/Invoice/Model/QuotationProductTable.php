<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class QuotationProductTable
{

    protected $tableGateWay;

    function __construct(TableGateway $tableGateWay)
    {
        $this->tableGateWay = $tableGateWay;
    }

    public function saveReceipt(QuotationProduct $qr)
    {

        $data = array(
            'locationProductID' => $qr->locationProductID,
            'quotationProductName' => $qr->productName,
            'quotationProductDescription' => $qr->quotationProductDescription,
            'quotationProductQuantity' => $qr->quantity,
            'quotationProductUnitPrice' => $qr->unitPrice,
            'uom' => $qr->uom,
            'quotationProductDiscount' => $qr->discount,
            'quotationProductDiscountType' => $qr->discountType,
            'quotationID' => $qr->quotationID,
            'quotationProductTotal' => $qr->total,
            'quotationProductTax' => $qr->tax,
        );


        if ($this->tableGateWay->insert($data)) {
            $result = $this->tableGateWay->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getProductByQuotationID($quotation)
    {
        $rowset = $this->tableGateWay->select(array('quotationID' => $quotation));
        return $rowset;
    }

    public function getProductsByQuotationCode($quotationCode)
    {
        $sql = new Sql($this->tableGateWay->getAdapter());
        $select = $sql->select();
        $select->from('quotationProduct');
        $select->join('locationProduct', 'quotationProduct.locationProductID = locationProduct.locationProductID', array('*'));
        $select->join('product', 'locationProduct.productID = product.productID', array('*'));
        $select->join('quotationProductTax', 'quotationProductTax.quotProductID =  quotationProduct.quotationProductID', array('taxTotal' => new Expression('SUM(quotationProductTax.quotTaxAmount)')));
        $select->group(array('quotationProduct.quotationProductID'));
        $select->where(array('quotationCode' => $quotationCode));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getProductsByQuotationID($quotationID)
    {
        $sql = new Sql($this->tableGateWay->getAdapter());
        $select = $sql->select();
        $select->from('quotationProduct');
        $select->join('locationProduct', 'quotationProduct.locationProductID = locationProduct.locationProductID', array('*'));
        $select->join('product', 'locationProduct.productID = product.productID', array('*'));
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('quotationProductTax', 'quotationProductTax.quotProductID =  quotationProduct.quotationProductID', array('taxTotal' => new Expression('SUM(quotationProductTax.quotTaxAmount)')), 'left');
        $select->group(array('quotationProduct.quotationProductID'));
        $select->where(array('productUomBase' => '1'));
        $select->where(array('quotationProduct.quotationID' => $quotationID));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getQuotationProductsByCode($quotationCode)
    {
        $sql = new Sql($this->tableGateWay->getAdapter());
        $select = $sql->select();
        $select->from("quotationProduct");
        $select->where(array('quotationCode' => $quotationCode));
        $rowset = $sql->prepareStatementForSqlObject($select);
        return $rowset->execute();
    }

    public function getQuotationProductsByID($quotationID)
    {

        $sql = new Sql($this->tableGateWay->getAdapter());
        $select = $sql->select();
        $select->from("quotationProduct");
        $select->join('locationProduct', 'locationProduct.locationProductID = quotationProduct.locationProductID', array('*'), 'left');
        $select->join('product', 'product.productID = locationProduct.productID', array('*'), 'left');
        $select->where(array('quotationProduct.quotationID' => $quotationID));
        $rowset = $sql->prepareStatementForSqlObject($select);
        return $rowset->execute();
    }

    public function getUncopiedQuotationProductsByID($quotationID)
    {

        $sql = new Sql($this->tableGateWay->getAdapter());
        $select = $sql->select();
        $select->from("quotationProduct");
        $select->join('locationProduct', 'locationProduct.locationProductID = quotationProduct.locationProductID', array('*'), 'left');
        $select->join('product', 'product.productID = locationProduct.productID', array('*'), 'left');
        $select->where(array('quotationProduct.quotationID' => $quotationID));
        $select->where(array('quotationProduct.quotationProductCopied' => '0'));
        $rowset = $sql->prepareStatementForSqlObject($select);
        return $rowset->execute();
    }

    public function getProductByQuotationIDAndProductId($quotationID, $productId)
    {
        $rowset = $this->tableGateWay->select(array('quotationID' => $quotationID, 'quotationProductID' => $productId));
        return $rowset->current();
    }

    public function editReceipt(QuotationProduct $qr)
    {
        $data = array(
            'quotationProductID' => $qr->locationProductID,
            'quotationProductName' => $qr->productName,
            'quotationProductQuantity' => $qr->quantity,
            'quotationProductUnitPrice' => $qr->unitPrice,
            'uom' => $qr->uom,
            'quotationProductDiscount' => $qr->discount,
            'quotationID' => $qr->quotID,
            'quotationProductTotal' => $qr->total,
            'quotationProductTax' => $qr->tax,
        );
        $this->tableGateWay->update($data, array('quotationProductID' => $qr->locationProductID, 'quotationID' => $qr->quotID));
    }

    public function deleteReceipt($quotationID, $productId)
    {
        $rowset = $this->tableGateWay->delete(array('quotationID' => $quotationID, 'quotationProductID' => $productId));
        return $rowset;
    }

    public function CheckQuotationProductByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateWay->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('quotationProduct')
                ->columns(array('*'))
                ->join('quotation', 'quotation.quotationID = quotationProduct.quotationID', array('entityID'), 'left')
                ->join('entity', 'entity.entityID = quotation.entityID', array('deleted'), 'left')
                ->where(array('locationProductID' => $locationProductID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $locationPID
     * @param type $quatationID
     * updating items which create Sales order using Quotation
     */
    public function updateCopiedQuotationProducts($locationPID, $quatationID, $data)
    {
        $this->tableGateWay->update($data, array('locationProductID' => $locationPID, 'quotationID' => $quatationID));
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $quotationID
     * @return arraySet
     * In this table,Forigen Key is 'quotationCode'
     * That is way I have used Quotation Code(It should be Quotation ID)
     */
    public function getUnCopiedQuotationProducts($quotationID)
    {
        $adapter = $this->tableGateWay->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('quotationProduct');
        $select->where(array('quotationID' => $quotationID));
        $select->where(array('quotationProductCopied' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $quatationID
     * updating items which related to Quotation ID
     */
    public function updateCopiedQuotationProductsByQuoID($quatationID)
    {
        $data = array(
            'quotationProductCopied' => 0
        );
        $this->tableGateWay->update($data, array('quotationID' => $quatationID));
    }

    /**
     * @author Ashan Madushka<ashan@thinkcube.com>
     * @param type $quatationID,$locationProdutId
     * get quotation product by quotationID and locationProductId
     */
    public function getQuotationProductByQuotationIdAndLocationProductId($quatationID, $locationProductID)
    {
        $sql = new Sql($this->tableGateWay->getAdapter());
        $select = $sql->select();
        $select->from("quotationProduct");
        $select->where(array('quotationProduct.quotationID' => $quatationID, 'quotationProduct.locationProductID' => $locationProductID));
        $rowset = $sql->prepareStatementForSqlObject($select);
        $row = $rowset->execute();
        return (object) $row->current();
    }

}

?>
