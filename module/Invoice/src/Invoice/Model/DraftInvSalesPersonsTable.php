<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains invoice table functions
 */

namespace Invoice\Model;


use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Between;

class DraftInvSalesPersonsTable
{

    protected $tableGateWay;

    function __construct(TableGateway $tableGateWay)
    {
        $this->tableGateWay = $tableGateWay;
    }

    public function saveSalesPerson($inv)
    {
        $data = array(
            'invoiceID' => $inv->invoiceID,
            'salesPersonID' => $inv->salesPersonID,
            'relatedSalesAmount' => $inv->relatedSalesAmount
        );
        $this->tableGateWay->insert($data);
        return TRUE;
    }

    public function getSalesPersonByInvoiceID($invoice)
    {
        $rowset = $this->tableGateWay->select(array('invoiceID' => $invoice));

        $ids = [];
        foreach ($rowset as $key => $value) {
           $ids[] = $value->salesPersonID;
        }

        return $ids;
    }

    public function getallsSalesPersonByInvoiceID($invoice)
    {
        try {
            $sql = new Sql($this->tableGateWay->getAdapter());
            $select = $sql->select();
            $select->from('draftInvSalesPersons')
                    ->columns(array('*'))
                    ->join("salesPerson", "salesPerson.salesPersonID = draftInvSalesPersons.salesPersonID", array('salesPersonSortName', 'salesPersonLastName', 'salesPersonAddress'), 'left')
                    ->where(array('draftInvSalesPersons.invoiceID' => $invoice));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            
            $ids = [];
            foreach ($rowset as $key => $value) {
               $ids[] = $value;
            }

            return $ids;
        } catch (\Exception $exc) {
            echo $exc;
        }


    }

    public function updateSPAmount($amount, $spID)
    {
        $data = array(
            'relatedSalesAmount' => $amount,
        );
        $this->tableGateWay->update($data, array('invoiceSalesPersonID' => $spID));
    }

    public function deleteSalesPersons($invoiceID)
    {
        $rowset = $this->tableGateWay->delete(array('invoiceID' => $invoiceID));
        return $rowset;
    }

}

?>
