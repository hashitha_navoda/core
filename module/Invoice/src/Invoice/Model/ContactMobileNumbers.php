<?php

namespace Invoice\Model;

/**
 * Class DispatchNote
 * @package Invoice\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class ContactMobileNumbers
{
    
    public $contactID;
    public $contactMobileNumberID;
    public $mobileNumber;
    public $mobileNumberType;

    public function exchangeArray($data)
    {
        $this->contactMobileNumberID      = (!empty($data['contactMobileNumberID'])) ? $data['contactMobileNumberID'] : null;
        $this->contactID      = (!empty($data['contactID'])) ? $data['contactID'] : null;
        $this->mobileNumber    = (!empty($data['mobileNumber'])) ? $data['mobileNumber'] : null;
        $this->mobileNumberType    = (!empty($data['mobileNumberType'])) ? $data['mobileNumberType'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
