<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethodCheque
{

    public $incomingPaymentMethodChequeId;
    public $incomingPaymentMethodChequeNumber;
    public $incomingPaymentMethodChequeBankName;    
        
    /**
     * @var int cheque reconciliation status.
     * 
     * 0 - not reconcile
     * 1 - reconciled
     */
    public $incomingPaymentMethodChequeReconciliationStatus;
    
    /**
     * @var int post-dated cheque status.
     * 
     * 0 - not post-dated cheque
     * 1 - post-dated cheque
     */
    public $postdatedChequeStatus;
    
    /**
     * @var Date post-dated cheque date.
     */
    public $postdatedChequeDate;
    
    /**
     * @var int bounced status
     * 
     * 0 - cheque not bounced
     * 1 - cheque bounced
     */
    public $incomingPaymentMethodChequeBouncedStatus;
    
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodChequeId = (!empty($data['incomingPaymentMethodChequeId'])) ? $data['incomingPaymentMethodChequeId'] : null;
        $this->incomingPaymentMethodChequeNumber = (!empty($data['incomingPaymentMethodChequeNumber'])) ? $data['incomingPaymentMethodChequeNumber'] : null;
        $this->incomingPaymentMethodChequeBankName = (!empty($data['incomingPaymentMethodChequeBankName'])) ? $data['incomingPaymentMethodChequeBankName'] : null;
        $this->incomingPaymentMethodChequeReconciliationStatus = (!empty($data['incomingPaymentMethodChequeReconciliationStatus'])) ? $data['incomingPaymentMethodChequeReconciliationStatus'] : null;
        $this->postdatedChequeStatus = (!empty($data['postdatedChequeStatus'])) ? $data['postdatedChequeStatus'] : 0;
        $this->postdatedChequeDate = (!empty($data['postdatedChequeDate'])) ? $data['postdatedChequeDate'] : null;
        $this->incomingPaymentMethodChequeBouncedStatus = (!empty($data['incomingPaymentMethodChequeBouncedStatus'])) ? $data['incomingPaymentMethodChequeBouncedStatus'] : 0;
        $this->incomingPaymentMethodChequeAccNo = (!empty($data['incomingPaymentMethodChequeAccNo'])) ? $data['incomingPaymentMethodChequeAccNo'] : 0;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
