<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Predicate\Between;
use Zend\Db\Sql\Predicate\NotIn;

class DraftInvFreeIssueItemsTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    

    public function saveInvoiceFreeProduct($data)
    {
        $dataSet = array(
            'draftInvFreeIssueItemsID' => $data->salesInvoiceFreeIssueItemsID,
            'invoiceID' => $data->invoiceID,
            'mainProductID' => $data->mainProductID,
            'mainSalesInvoiceProductID' => $data->mainSalesInvoiceProductID,
            'freeProductID' => $data->freeProductID,
            'freeQuantity' => $data->freeQuantity,
            'salesInvoiceFreeItemSelectedUom' => $data->salesInvoiceFreeItemSelectedUom,
            'locationID' => $data->locationID,
        );

        if ($this->tableGateway->insert($dataSet)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }


    public function getRelatedFreeProductsByInvoiceAndProductID($invoiceID, $productID)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('draftInvFreeIssueItems')
                ->columns(array('*'))
                ->where(array('invoiceID' => $invoiceID, 'mainProductID' => $productID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getRelatedFreeProductsByInvoiceAndFreeProductID($invoiceID, $productID)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('salesInvoiceFreeIssueItems')
                ->columns(array('*'))
                ->where(array('invoiceID' => $invoiceID, 'freeProductID' => $productID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    
}
