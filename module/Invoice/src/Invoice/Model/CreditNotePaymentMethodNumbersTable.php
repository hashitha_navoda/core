<?php

/**
 * @author ashan     <ashan@thinkcube.com>
 * This file contains credit note payment method numbers Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class CreditNotePaymentMethodNumbersTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCreditNotePaymentMethodsNumbers(CreditNotePaymentMethodNumbers $CreditNotePaymentMethodsNumbers)
    {
        $data = array(
            'creditNotePaymentID' => $CreditNotePaymentMethodsNumbers->creditNotePaymentID,
            'paymentMethodID' => $CreditNotePaymentMethodsNumbers->paymentMethodID,
            'creditNotePaymentMethodReferenceNumber' => $CreditNotePaymentMethodsNumbers->creditNotePaymentMethodReferenceNumber,
            'creditNotePaymentMethodBank' => $CreditNotePaymentMethodsNumbers->creditNotePaymentMethodBank,
            'creditNotePaymentMethodCardID' => $CreditNotePaymentMethodsNumbers->creditNotePaymentMethodCardID,
        );
        $this->tableGateway->insert($data);
        return true;
    }

}
