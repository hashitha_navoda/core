<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodTT related filters
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethodTT
{

    public $incomingPaymentMethodTTId;
    public $incomingPaymentMethodTTReference;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodTTId = (!empty($data['incomingPaymentMethodTTId'])) ? $data['incomingPaymentMethodTTId'] : null;
        $this->incomingPaymentMethodTTReference = (!empty($data['incomingPaymentMethodTTReference'])) ? $data['incomingPaymentMethodTTReference'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
