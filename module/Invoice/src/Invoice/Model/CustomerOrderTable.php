<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Rating Types related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;

class CustomerOrderTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //get all rating types
    public function fetchAll($locationId ,$paginated = false)
    {

        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerOrders')
                ->columns(array('*'))
                ->join('entity', 'customerOrders.entityID= entity.entityID', array('deleted'), 'left')
                ->join('customer', 'customerOrders.customerID = customer.customerID', ['customerName', 'customerCode'])
                ->where(array('entity.deleted' => 0))
                ->order('customerOrderID DESC');

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    public function getPaginatedCusOrderBydate($fromDate , $toDate, $paginated = false)
    {

        // var_dump($fromDate);
        // var_dump($toDate).die();
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerOrders')
                ->columns(array('*'))
                ->join('entity', 'customerOrders.entityID= entity.entityID', array('deleted'), 'left')
                ->join('customer', 'customerOrders.customerID = customer.customerID', ['customerName', 'customerCode'])
                ->where(array('entity.deleted' => 0))
                ->order('customerOrderID DESC');
        $select->where->between('customerOrderDate', $fromDate, $toDate);

        if ($paginated) {
            $paginatorAdapter = new DbSelect(
                    $select, $adapter
            );
            $paginator = new Paginator($paginatorAdapter);
            return (object) $paginator;
        }

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }

    //save function of rating types
    public function saveCustomerOrder($cusOrder)
    {
        $data = array(
            'customerOrderCode' => $cusOrder->customerOrderCode,
            'customerID' => $cusOrder->customerID,
            'customerOrderDate' => $cusOrder->customerOrderDate,
            'customerOrderAmount' => $cusOrder->customerOrderAmount,
            'customerOrderComment' => $cusOrder->customerOrderComment,
            'customerOrderReference' => $cusOrder->customerOrderReference,
            'customerOrderStatus' => $cusOrder->customerOrderStatus,
            'locationId' => $cusOrder->locationId,
            'entityID' => $cusOrder->entityID,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }
    

    public function searchCustomerOrderForDropDown($searchKey, $status = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerOrders')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "customerOrders.entityID = e.entityID")
                ->where(array('e.deleted' => '0'))
                ->order(array('customerOrderDate' => 'DESC'));
        if ($status) {
            $select->where(array('customerOrders.customerOrderStatus' => $status));
        }
        $select->where->like('customerOrderCode', '%' . $searchKey . '%');
        $select->limit(50);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function searchAllCustomerOrderByRefNoForDropdown($branch, $searchKey, $status = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerOrders')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "customerOrders.entityID = e.entityID")
                ->where(array('locationId' => $branch))
                ->where(array('e.deleted' => '0'))
                ->order(array('customerOrderDate' => 'DESC'));
        $select->where->like('customerOrderReference', '%' . $searchKey . '%');
        $select->group('customerOrderID');
        $select->limit(50);
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    } 

    public function updateCusOrderStatusByID($cusOdrID, $closeStatusID)
    {
        $data = array(
            'customerOrderStatus' => $closeStatusID
        );
        return $this->tableGateway->update($data, array('customerOrderID' => $cusOdrID));
    }

    public function retriveCustomerOrder($customerOrderID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerOrders')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "customerOrders.entityID = e.entityID")
                ->join('customer', 'customerOrders.customerID = customer.customerID', ['customerName', 'customerCode'])
                ->where(array('customerOrderID' => $customerOrderID))
                ->where(array('e.deleted' => '0'));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

     public function updateCustomerOrder($cusOrder)
    {
        $data = array(
            'customerID' => $cusOrder->customerID,
            'customerOrderDate' => $cusOrder->customerOrderDate,
            'customerOrderAmount' => $cusOrder->customerOrderAmount,
            'customerOrderComment' => $cusOrder->customerOrderComment,
            'customerOrderReference' => $cusOrder->customerOrderReference,
        );
        $this->tableGateway->update($data, array('customerOrderID' => $cusOrder->customerOrderID));
        return TRUE;
    }

    public function checkCustomerOrderByCode($customerOrderCode)
    {
        try {
            $sql = new Sql($this->tableGateway->getAdapter());
            $select = $sql->select();
            $select->from('customerOrders')
                    ->columns(array('*'))
                    ->join(array('e' => 'entity'), "customerOrders.entityID = e.entityID")
                    ->where(array('e.deleted' => '0'))
                    ->where(array('customerOrders.customerOrderCode' => $customerOrderCode));
            $query = $sql->prepareStatementForSqlObject($select);
            $rowset = $query->execute();
            if (!$rowset) {
                return null;
            }
            return $rowset;
        } catch (\Exception $exc) {
            echo $exc;
        }
    }

    public function getCustomerOrderforSearch($customerOrderID, $branch)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerOrders')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "customerOrders.entityID = e.entityID")
                ->join('customer', "customerOrders.customerID = customer.customerID", array('customerName', 'customerCode'), 'left')
                ->where(array('customerOrders.locationId' => $branch))
                ->where(array('customerOrderID' => $customerOrderID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }


    public function getCustomerOrderByCustomerforSearch($customerID, $branch)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('customerOrders')
                ->columns(array('*'))
                ->join(array('e' => 'entity'), "customerOrders.entityID = e.entityID")
                ->join('customer', "customerOrders.customerID = customer.customerID", array('customerName', 'customerCode'), 'left')
                ->where(array('customerOrders.locationId' => $branch))
                ->where(array('customerOrders.customerID' => $customerID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();
        if (!$rowset) {
            return null;
        }
        return $rowset;
    }


    public function searchContactsForDropDown($contactSearchKey)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('contacts')
                ->columns(array('*'))
                ->join('contactMobileNumbers', 'contacts.contactID = contactMobileNumbers.contactID', array('*'), 'left')
                ->join('entity', 'contacts.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0))
                ->where(new PredicateSet(array(new Operator('contacts.firstName', 'like', '%' . $contactSearchKey . '%'),new Operator('contacts.lastName', 'like', '%' . $contactSearchKey . '%'), new Operator('contactMobileNumbers.mobileNumber', 'like', '%' . $contactSearchKey . '%')), PredicateSet::OP_OR));
        $select->group("contacts.contactID");
        $select->limit(50);

        $statement = $sql->prepareStatementForSqlObject($select);
        return $statement->execute();
    }
}
