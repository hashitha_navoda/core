<?php

/**
 * @author Prathap Weerasinghe <prathap@thinkcube.com>
 * This is the QuotationProductTax model
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class SalesOrderProductTax implements InputFilterAwareInterface
{

    public $soProductTaxID;
    public $soID;
    public $soProductID;
    public $soTaxID;
    public $soTaxPrecentage;
    public $soTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->soProductTaxID = (!empty($data['soProductTaxID'])) ? $data['soProductTaxID'] : null;
        $this->soID = (!empty($data['soID'])) ? $data['soID'] : null;
        $this->soProductID = (!empty($data['soProductID'])) ? $data['soProductID'] : null;
        $this->soTaxID = (!empty($data['soTaxID'])) ? $data['soTaxID'] : null;
        $this->soTaxPrecentage = (!empty($data['soTaxPrecentage'])) ? $data['soTaxPrecentage'] : null;
        $this->soTaxAmount = (!empty($data['soTaxAmount'])) ? $data['soTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
