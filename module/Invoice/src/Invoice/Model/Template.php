<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains template Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Template
{

    public $templateID;
    public $templateName;
    public $footer_id;
    public $footerEnabled;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->templateID = (!empty($data['templateID'])) ? $data['templateID'] : null;
        $this->templateName = (!empty($data['templateName'])) ? $data['templateName'] : null;
        $this->footer_id = $data['footer_id'];
        $this->footerEnabled = $data['footerEnabled'];
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

