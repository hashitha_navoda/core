<?php

/**
 * @author Sahan Siriwardhan <sahan@thinkcube.com>
 * This file contains Rating Types related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Predicate\PredicateSet;
use Zend\Db\Sql\Predicate\Operator;
use Zend\Db\Sql\Predicate\Expression;

class ContactListContactsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //save function of rating types
    public function save($dataSet)
    {
        $data = array(
            'contactID' => $dataSet->contactID,
            'contactListID' => $dataSet->contactListID
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        }
        return FALSE;
        
    }

    public function getRelatedNumbersByContactListID($contactListID) {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();

        $select->from('contactListContacts')
                ->where(array('contactListID' => $contactListID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();

        if (!$rowset) {
            return NULL;
        }
        $resultSet = [];

        if (sizeof($rowset) > 0) {
            foreach ($rowset as $key => $value) {
                $resultSet[] = $value['contactID'];
            }
        }

        return $resultSet;
    }

    public function deleteContactListMobileNumbers($contactListID)
    {
        try {
            $result = $this->tableGateway->delete(array('contactListID' => $contactListID));
            return $result;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

}
