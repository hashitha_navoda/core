<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class DeliveryNoteProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveDeliveryNoteProduct(DeliveryNoteProduct $deliveryNoteProduct)
    {
        $data = array(
            'deliveryNoteID' => $deliveryNoteProduct->deliveryNoteID,
            'productID' => $deliveryNoteProduct->productID,
            'deliveryNoteProductPrice' => $deliveryNoteProduct->deliveryNoteProductPrice,
            'deliveryNoteProductDiscount' => $deliveryNoteProduct->deliveryNoteProductDiscount,
            'deliveryNoteProductDiscountType' => $deliveryNoteProduct->deliveryNoteProductDiscountType,
            'deliveryNoteProductTotal' => $deliveryNoteProduct->deliveryNoteProductTotal,
            'deliveryNoteProductQuantity' => $deliveryNoteProduct->deliveryNoteProductQuantity,
            'documentTypeID' => $deliveryNoteProduct->documentTypeID,
            'deliveryNoteProductDocumentID' => $deliveryNoteProduct->deliveryNoteProductDocumentID,
            'deliveryNoteProductDocumentTypeCopiedQty' => $deliveryNoteProduct->deliveryNoteProductDocumentTypeCopiedQty,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getDeliveryNoteProductByDeliveryNoteID($deliveryNoteID, $isNotCopied = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProduct")
                ->columns(array("*"))
                ->join("deliveryNote", "deliveryNoteProduct.deliveryNoteID = deliveryNote.deliveryNoteID", array("deliveryNoteLocationID"), "left")
                ->join("product", "deliveryNoteProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("deliveryNoteProductTax", "deliveryNoteProduct.deliveryNoteProductID = deliveryNoteProductTax.deliveryNoteProductID", array("deliveryNoteProductTaxID", "deliveryNoteTaxID", "deliveryNoteTaxPrecentage", "deliveryNoteTaxAmount"), "left")
                ->join("tax", "deliveryNoteProductTax.deliveryNoteTaxID = tax.id", array("*"), "left")
                ->join("deliveryNoteSubProducts", "deliveryNoteProduct.deliveryNoteProductID = deliveryNoteSubProducts.deliveryNoteProductID", array("deliveryNoteSubProductID", "productBatchID", "productSerialID", "deliveryProductSubQuantity", "deliveryNoteSubProductsCopiedQuantity", "deliveryNoteSubProductsWarranty","deliveryNoteSubProductsWarrantyType"), "left")
                ->join("productSerial", "deliveryNoteSubProducts.productSerialID = productSerial.productSerialID", array("productSerialCode", "productSerialSold", "productSerialReturned", "productSerialExpireDate", "productSerialWarrantyPeriod","productSerialWarrantyPeriodType", 'serialLocProID' => new Expression('productSerial.locationProductID')), "left")
                ->join("productBatch", "deliveryNoteSubProducts.productBatchID = productBatch.productBatchID", array("productBatchCode", "productBatchExpiryDate", "productBatchWarrantyPeriod", "productBatchManufactureDate","productBatchPrice", 'batchLocProID' => new Expression('productBatch.locationProductID')), "left")
                ->where(array('productUomBase' => '1'))
                ->where(array("deliveryNoteProduct.deliveryNoteID" => $deliveryNoteID));
        if ($isNotCopied) {
            $select->where(array("deliveryNoteProduct.copied" => '0'));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDeliveryNoteProductByDeliveryNoteIDs($deliveryNoteIDs, $isNotCopied = false)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProduct")
                ->columns(array("*"))
                ->join("deliveryNote", "deliveryNoteProduct.deliveryNoteID = deliveryNote.deliveryNoteID", array("deliveryNoteLocationID"), "left")
                ->join("product", "deliveryNoteProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr'))
                ->join("deliveryNoteProductTax", "deliveryNoteProduct.deliveryNoteProductID = deliveryNoteProductTax.deliveryNoteProductID", array("deliveryNoteProductTaxID", "deliveryNoteTaxID", "deliveryNoteTaxPrecentage", "deliveryNoteTaxAmount"), "left")
                ->join("tax", "deliveryNoteProductTax.deliveryNoteTaxID = tax.id", array("*"), "left")
                ->join("deliveryNoteSubProducts", "deliveryNoteProduct.deliveryNoteProductID = deliveryNoteSubProducts.deliveryNoteProductID", array("deliveryNoteSubProductID", "productBatchID", "productSerialID", "deliveryProductSubQuantity", "deliveryNoteSubProductsCopiedQuantity", "deliveryNoteSubProductsWarranty","deliveryNoteSubProductsWarrantyType"), "left")
                ->join("productSerial", "deliveryNoteSubProducts.productSerialID = productSerial.productSerialID", array("productSerialCode", "productSerialSold", "productSerialReturned", "productSerialExpireDate", "productSerialWarrantyPeriod", "productSerialWarrantyPeriodType"), "left")
                ->join("productBatch", "deliveryNoteSubProducts.productBatchID = productBatch.productBatchID", array("productBatchCode", "productBatchExpiryDate", "productBatchWarrantyPeriod", "productBatchPrice"), "left")
                ->where(array('productUomBase' => '1'));
        if(is_array($deliveryNoteIDs)){
            $select->where->in('deliveryNoteProduct.deliveryNoteID', $deliveryNoteIDs);
        }else{
            $select->where(array('deliveryNoteProduct.deliveryNoteID' => $deliveryNoteIDs));
        }
        if ($isNotCopied) {
            $select->where(array("deliveryNoteProduct.copied" => '0'));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getDeliveryNoteProductByID($deliveryNoteID)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('deliveryNoteProduct')
                ->columns(array('*'))
                ->join('product', 'deliveryNoteProduct.productID =  product.productID', array('*'), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('deliveryNoteProductTax', 'deliveryNoteProductTax.deliveryNoteProductID =  deliveryNoteProduct.deliveryNoteProductID', array('taxTotal' => new Expression('SUM(deliveryNoteProductTax.deliveryNoteTaxAmount)')), 'left')
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->where(array('productUomBase' => '1'))
                ->group(array('deliveryNoteProduct.deliveryNoteProductID'))
                ->where(array('deliveryNoteID' => $deliveryNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        return $results;
    }

    /**
     * @author Yashora  <yashora@thinkcube.com>
     * return database table data to get total item quantities for invoiced delivery notes
     * @param date $fromDate
     * @param date $toDate
     * @return array sql query data
     */
    public function getInvoicedDeliveryNoteItemsData($fromDate, $toDate)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = $sql->select();
        $select->from('deliveryNoteProduct')
                ->columns(array('itemCode' => new Expression('product.productCode'),
                    'itemName' => new Expression('product.productName'),
                    'quantity' => new Expression('SUM(deliveryNoteProduct.deliveryNoteProductCopiedQuantity)')
                ))
                ->join('product', 'product.productId = deliveryNoteProduct.productId')
                ->join('deliveryNote', 'deliveryNote.deliveryNoteId = deliveryNoteProduct.deliveryNoteId')
                ->group('product.productId')
                ->order('product.productId')
                ->where->notEqualTo('deliveryNoteProduct.deliveryNoteProductCopiedQuantity',0)
        ->where->between('deliveryNote.deliveryNoteDeliveryDate', $fromDate, $toDate);
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus',10);

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $resultArray = [];
        foreach ($results as $val) {
            $resultArray[] = $val;
        }

        return $resultArray;
    }

    /**
     * @author Peushani Jayasekara <peushani@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @return null|array
     *
     */
    public function getDailyDeliveryItemByDate($fromDate, $toDate, $cusCategory = NULL)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProduct")
                ->columns(array('deliveryNoteID', 'productID', 'deliveryProductQuantity' => new Expression('SUM(deliveryNoteProductQuantity)')))
                ->join("deliveryNote", "deliveryNoteProduct.deliveryNoteID = deliveryNote.deliveryNoteID", array('deliveryNoteDeliveryDate'))
                ->join("product", "deliveryNoteProduct.productID = product.productID", array('productCode', 'productName'))
                ->group(array('deliveryNoteProduct.productID'))
                ->order(array('deliveryNoteProduct.productID'))
        ->where->between('deliveryNoteDeliveryDate', $fromDate, $toDate);
        if($cusCategory != ''){
            $select->join('customer', 'deliveryNote.customerID = customer.customerID', array('customerName'), 'left');
            $select->where->in('customer.customerCategory', $cusCategory);
        }
        $select->where->notEqualTo('deliveryNote.deliveryNoteStatus', 10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        } else {
            $countArray = array();
            foreach ($results as $val) {
                array_push($countArray, $val);
            }
            return $countArray;
        }
    }

    /**
     * @author Peushani Jayasekara <peushani@thinkcube.com>
     * @param type $fromDate
     * @param type $toDate
     * @return null|array
     *
     */
    public function getNotInvoiceDeliveryItemByDate($fromDate, $toDate)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProduct")
                ->columns(array('deliveryNoteID', 'productID', 'deliveryProductQuantity' => new Expression('SUM(deliveryNoteProductQuantity-deliveryNoteProductCopiedQuantity)')))
                ->join("deliveryNote", "deliveryNoteProduct.deliveryNoteID = deliveryNote.deliveryNoteID", array('deliveryNoteDeliveryDate'),'left')
                ->join("product", "deliveryNoteProduct.productID = product.productID", array('productCode', 'productName'))
                ->group(array('deliveryNoteProduct.productID'))
                ->order(array('deliveryNoteProduct.productID'))
                ->where->notEqualTo('deliveryNote.deliveryNoteStatus', 10)
                ->where->notEqualTo('deliveryNote.deliveryNoteStatus', 4)
        ->where->between('deliveryNoteDeliveryDate', $fromDate, $toDate);
        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();
        if (!$results) {
            return null;
        } else {
            $countArray = array();
            foreach ($results as $val) {
                array_push($countArray, $val);
            }

            return $countArray;
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $productID
     * @param type $deliveryNoteID
     * update items which create Sales Invoice using DeliveryNote
     */
    public function updateCopiedDeliveryNoteProducts($productID, $deliveryNoteID, $deliveryNoteProData)
    {
        $this->tableGateway->update($deliveryNoteProData, array('productID' => $productID, 'deliveryNoteID' => $deliveryNoteID));
    }


    public function updateCopiedDeliveryAsEmptyNoteProductsByDlnID($deliveryNoteID, $deliveryNoteProData)
    {
        $this->tableGateway->update($deliveryNoteProData, array('deliveryNoteID' => $deliveryNoteID));
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $deliveryNoteID
     * @return arraySet
     */
    public function getUnCopiedDeliveryNoteProducts($deliveryNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNoteProduct');
        $select->where(array('deliveryNoteID' => $deliveryNoteID));
        $select->where(array('copied' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    //get delivery note products by deliverynoteid and ProdutID
    public function getProductByDeliveryNoteIDAndProductId($deliveryNoteID, $productID)
    {
        $rowset = $this->tableGateway->select(array('deliveryNoteID' => $deliveryNoteID, 'productID' => $productID));
        return $rowset->current();
    }
    
    /**
     * Get delivery note serial products by delivery note id
     * @param int $deliveryNoteId
     * @return mixed
     */
    public function getDeliveryNoteSerialProductByDeliveryNoteId($deliveryNoteId) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProduct")
                ->columns(array("*"))
                ->join("product", "deliveryNoteProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->join("deliveryNoteSubProducts", "deliveryNoteProduct.deliveryNoteProductID = deliveryNoteSubProducts.deliveryNoteProductID", array("deliveryNoteSubProductID", "productBatchID", "productSerialID", "deliveryProductSubQuantity"), "left")
                ->join("productSerial", "deliveryNoteSubProducts.productSerialID = productSerial.productSerialID", array('productSerialCode', 'productSerialWarrantyPeriod','productSerialWarrantyPeriodType'), "left")
                ->where(array('productUomBase' => '1'))
                ->where(array('serialProduct' => '1'))
                ->where(array("deliveryNoteID" => $deliveryNoteId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }    

    public function getProductsForDeliveryNoteEditByDeliveryNoteID($deliveryNoteID,$locationID)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('deliveryNoteProduct')
                ->columns(array('*'))
                ->join('product', 'deliveryNoteProduct.productID =  product.productID', array('*'), 'left')
                ->join('locationProduct', 'locationProduct.productID =  product.productID', array('*'), 'left')
                ->join('deliveryNoteProductTax', 'deliveryNoteProductTax.deliveryNoteProductID =  deliveryNoteProduct.deliveryNoteProductID', array('taxTotal' => new Expression('SUM(deliveryNoteProductTax.deliveryNoteTaxAmount)')), 'left')
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomDisplay', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->group(array('deliveryNoteProduct.deliveryNoteProductID'))
                ->where(array('productUomBase' => '1'))
                ->where(array('locationProduct.locationID' => $locationID))
                ->where(array('deliveryNoteID' => $deliveryNoteID));

        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    /**
     * Get delivery note batch products by delivery note id
     * @param int $deliveryNoteId
     * @return mixed
     */
    public function getDeliveryNoteBatchProductByDeliveryNoteId($deliveryNoteId) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProduct")
                ->columns(array("*"))
                ->join("product", "deliveryNoteProduct.productID = product.productID", array("*"), "left")
                ->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'))
                ->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'))
                ->join("deliveryNoteSubProducts", "deliveryNoteProduct.deliveryNoteProductID = deliveryNoteSubProducts.deliveryNoteProductID", array("deliveryNoteSubProductID", "productBatchID", "productSerialID", "deliveryProductSubQuantity", "deliveryNoteSubProductsCopiedQuantity"), "left")
                ->join("productBatch", "deliveryNoteSubProducts.productBatchID = productBatch.productBatchID", array('productBatchCode', 'productBatchWarrantyPeriod'), "left")
                ->where(array('productUomBase' => '1'))
                ->where(array('batchProduct' => '1'))
                ->where(array("deliveryNoteID" => $deliveryNoteId));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function getProductsDetailsByDeliveryNoteID($deliveryNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->where(array(
            'itemOut.itemOutDocumentID' => $deliveryNoteID,
            'itemOut.itemOutDocumentType' => 'Delivery Note',
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result;
    }

    public function getProductsDetailsByDeliveryNoteIDLocationProductIDAndProductSerialID($deliveryNoteID, $locationProductID, $serialID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->where(array(
            'itemOut.itemOutDocumentID' => $deliveryNoteID,
            'itemOut.itemOutDocumentType' => 'Delivery Note',
            'itemOut.itemOutLocationProductID' => $locationProductID,
            'itemOut.itemOutSerialID' => $serialID,
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result->current();
    }

    public function getProductsDetailsByDeliveryNoteIDLocationProductIDAndProductBatchID($deliveryNoteID, $locationProductID, $batchID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemOut');
        $select->where(array(
            'itemOut.itemOutDocumentID' => $deliveryNoteID,
            'itemOut.itemOutDocumentType' => 'Delivery Note',
            'itemOut.itemOutLocationProductID' => $locationProductID,
            'itemOut.itemOutBatchID' => $batchID,
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result->current();
    }

    public function getCopiedDeliveryNoteItemsBySoId($soId)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNoteProduct')
                ->columns(['*'])
                ->join('deliveryNote', 'deliveryNoteProduct.deliveryNoteID = deliveryNote.deliveryNoteID', [
                    'deliveryNoteCode',
                    'deliveryNoteDeliveryDate',
                    'copiedQty' => new Expression('SUM(deliveryNoteProductDocumentTypeCopiedQty)')
                    ], 'left');
        $select->where->equalTo('deliveryNoteProductDocumentID', $soId)
               ->where->equalTo('documentTypeID', 3);
        $select->group("deliveryNoteProduct.deliveryNoteID");
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();
        return $result;
    }

    public function getDeliveryNoteDetailsByDeliveryNoteID($deliveryNoteID, $batchID = null, $serialID = null)
    {
        $adapater = $this->tableGateway->getAdapter();
        $sql = new Sql($adapater);
        $select = $sql->select();
        $select->from('deliveryNoteProduct')
                ->columns(array('*'))
                ->join("deliveryNote", "deliveryNoteProduct.deliveryNoteID = deliveryNote.deliveryNoteID", array('deliveryNoteStatus'), "left")
                ->join("deliveryNoteSubProducts", "deliveryNoteProduct.deliveryNoteProductID = deliveryNoteSubProducts.deliveryNoteProductID", array("deliveryNoteSubProductID", "productBatchID", "productSerialID",'deliveryNoteSubProductsCopiedQuantity','deliveryProductSubQuantity'), "left")
                ->where(array('deliveryNoteProduct.deliveryNoteID' => $deliveryNoteID));

        if (!is_null($batchID)) {
            $select->where(array('deliveryNoteSubProducts.productBatchID' => $batchID));
        }

        if (!is_null($serialID)) {
            $select->where(array('deliveryNoteSubProducts.productSerialID' => $serialID));
        }

        $statement = $sql->prepareStatementForSqlObject($select);

        $results = $statement->execute();

        return $results;

    }

    public function getDeliveryNoteProductQuantityByDeliveryNoteProductID($deliveryNoteProductID) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProduct")
                ->columns(array("deliveryNoteProductQuantity",'deliveryNoteProductCopiedQuantity'))
                ->where(array("deliveryNoteProductID" => $deliveryNoteProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }    

    public function updateDeliveryNoteProductReturnQuantity($deliveryNoteProductID, $deliveryNoteProductQuantityUpdateData)
    {
        $this->tableGateway->update($deliveryNoteProductQuantityUpdateData, array('deliveryNoteProductID' => $deliveryNoteProductID));
    
    }

    public function getCopiedDeliveryNoteProducts($deliveryNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('deliveryNoteProduct');
        $select->where(array('deliveryNoteID' => $deliveryNoteID));
        $select->where(array('copied' => 1));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getDeliveryNoteIDByDeliveryNoteProductID($deliveryNoteProductID) 
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("deliveryNoteProduct")
                ->columns(array("deliveryNoteProductQuantity",'deliveryNoteProductCopiedQuantity'))
                ->join("deliveryNote", "deliveryNoteProduct.deliveryNoteID = deliveryNote.deliveryNoteID", array('deliveryNoteID','deliveryNoteLocationID'), "left")
                ->where(array("deliveryNoteProductID" => $deliveryNoteProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    public function updateDeliveryNoteProduct($deliveryNoteProductID, $data)
    {
    
        try {
            $this->tableGateway->update($data, array('deliveryNoteProductID' => $deliveryNoteProductID));
            return true;
        } catch (Exception $e) {
            error_log($e->getMessage());
            return false;
        }
    
    }
    
}
