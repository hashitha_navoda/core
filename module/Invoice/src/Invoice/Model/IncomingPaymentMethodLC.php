<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodLC related filters
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethodLC
{

    public $incomingPaymentMethodLCId;
    public $incomingPaymentMethodLCReference;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodLCId = (!empty($data['incomingPaymentMethodLCId'])) ? $data['incomingPaymentMethodLCId'] : null;
        $this->incomingPaymentMethodLCReference = (!empty($data['incomingPaymentMethodLCReference'])) ? $data['incomingPaymentMethodLCReference'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
