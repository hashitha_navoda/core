<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains creditc note product Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class CreditNoteProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getCreditNoteProductsByInvoiceID($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProduct")
                ->columns(array("*"))
                ->join("salesInvoiceProduct", "creditNoteProduct.invoiceProductID= salesInvoiceProduct.salesInvoiceProductID", array("*"), "left")
                ->join('product', 'creditNoteProduct.productID =  product.productID', array('*'), 'left')
                ->join('locationProduct', 'locationProduct.locationProductID =  salesInvoiceProduct.locationProductID', array('*'), 'left')
                ->where(array("salesInvoiceProduct.salesInvoiceID" => $invoiceID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCreditNoteProductAndSubProductDataByInvoiceProductID($invoiceProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProduct")
                ->columns(array("*"))
                ->join('creditNoteSubProduct', 'creditNoteSubProduct.creditNoteProductID=creditNoteProduct.creditNoteProductID', array('creditNoteSubProductID', 'productBatchID', 'productSerialID', 'creditNoteSubProductQuantity'), 'left')
                ->join('creditNote', 'creditNoteProduct.creditNoteID=creditNote.creditNoteID', array('*'), 'left')
                ->join('entity', 'creditNote.entityID=entity.entityID', array('*'), 'left')
                ->where(array('creditNoteProduct.invoiceProductID' => $invoiceProductID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function saveCreditNoteProducts(CreditNoteProduct $creditNoteProduct)
    {
        $data = array(
            'creditNoteID' => $creditNoteProduct->creditNoteID,
            'invoiceProductID' => $creditNoteProduct->invoiceProductID,
            'productID' => $creditNoteProduct->productID,
            'creditNoteProductPrice' => $creditNoteProduct->creditNoteProductPrice,
            'creditNoteProductDiscount' => $creditNoteProduct->creditNoteProductDiscount,
            'creditNoteProductDiscountType' => $creditNoteProduct->creditNoteProductDiscountType,
            'creditNoteProductQuantity' => $creditNoteProduct->creditNoteProductQuantity,
            'creditNoteProductTotal' => $creditNoteProduct->creditNoteProductTotal,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllCreditNoteProductDetailsByCreditNoteID($creditNoteID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNoteProduct');
        $select->join(
                'product', 'creditNoteProduct.productID = product.productID', array(
            '*',
                )
        );
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->where(array('productUomBase' => '1'));
        $select->where(array('creditNoteProduct.creditNoteID' => $creditNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function getCreditNoteProductDetailsByInvoiceProductID($invoiceProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProduct")
                ->columns(array("*"))
                ->join('creditNote', 'creditNoteProduct.creditNoteID = creditNote.creditNoteID', array('*'), 'left')
                ->where(array('creditNoteProduct.invoiceProductID' => $invoiceProductID))
                ->where->in('creditNote.statusID',[3,4]);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCreditNoteProductDetailsByInvoiceIDAndProductID($invoiceID, $productID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProduct")
                ->columns(array("*"))
                ->join("salesInvoiceProduct", "creditNoteProduct.invoiceProductID= salesInvoiceProduct.salesInvoiceProductID", array("*"), "left")
                ->where(array(
                    'salesInvoiceProduct.salesInvoiceID' => $invoiceID,
                    'salesInvoiceProduct.productID' => $productID
                        )
        );
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param int $creditNote
     * @return $result
     */
    public function getProductsDetailsByInvoiceID($creditNote)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('itemIn');
        $select->where(array(
            'itemIn.itemInDocumentID' => $creditNote,
            'itemIn.itemInDocumentType' => 'Credit Note',
        ));
        $statement = $sql->prepareStatementForSqlObject($select);
        $result = $statement->execute();

        if (!$result) {
            return NULL;
        }
        return $result;
    }

    public function getCreditNoteProductByInvoiceProductID($invoiceProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProduct")
                ->columns(array("*"))
                ->join('creditNote', 'creditNoteProduct.creditNoteID = creditNote.creditNoteID', array('*'), 'left')
                ->join('entity', 'creditNote.entityID = entity.entityID', array('*'), 'left')
                ->where(array('creditNoteProduct.invoiceProductID' => $invoiceProductID, 'entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getAllCreditNoteProductsForReport()
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProduct")
                ->columns(array("*"))
                ->join('creditNote', 'creditNoteProduct.creditNoteID = creditNote.creditNoteID', array('*'), 'left')
                ->join('entity', 'creditNote.entityID = entity.entityID', array('*'), 'left')
                ->where(array('entity.deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    /**
     * get all credit note product details related to the given sales invoice product ids
     * @param array $invoiceProductID
     * @return $result
     */

    public function getCreditNoteProductByInvoiceProductIDs($invoiceProductID = [])
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProduct")
                ->columns(array("*"))
                ->join('creditNote', 'creditNoteProduct.creditNoteID = creditNote.creditNoteID', array('*'), 'left')
                ->join('entity', 'creditNote.entityID = entity.entityID', array('deleted'), 'left')
                ->where(array('entity.deleted' => 0));
        $select->where->in('creditNoteProduct.invoiceProductID', $invoiceProductID);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getCreditNoteDetailsByCreditNoteID($creditNoteID , $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNoteProduct')
               ->columns(array('*','creditNoteProID' => new Expression('creditNoteProduct.creditNoteProductID')))
               ->join('creditNote', 'creditNote.creditNoteID = creditNoteProduct.creditNoteID', array('locationID','invoiceID'),'left')
               ->join('locationProduct', 'locationProduct.productID = creditNoteProduct.productID', array('locationProductID'),'left')
               ->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductID = creditNoteProduct.invoiceProductID', array('documentTypeID','salesInvoiceProductDocumentID'),'left')
               ->join('creditNoteSubProduct', 'creditNoteSubProduct.creditNoteProductID = creditNoteProduct.creditNoteProductID', array('*'),'left');
        $select->where(array('creditNoteProduct.creditNoteID' => $creditNoteID));
        $select->where(array('locationProduct.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function checkAnyCrediteNotesUsedThisProductAfterThisCreditNoteID($creditNoteID , $serialID, $batchID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('creditNoteProduct')
               ->columns(array('creditNoteID'))
               ->join('creditNote', 'creditNote.creditNoteID = creditNoteProduct.creditNoteID', array('invoiceID'),'left')
               ->join('creditNoteSubProduct', 'creditNoteSubProduct.creditNoteProductID = creditNoteProduct.creditNoteProductID', array('*'),'left');
        $select->where->notEqualTo('creditNote.statusID',5);
        $select->where->greaterThan('creditNoteProduct.creditNoteID', $creditNoteID);
        if (!is_null($serialID)) {
            $select->where(array('creditNoteSubProduct.productSerialID' => $serialID));
        }
        if (!is_null($batchID)) {
            $select->where(array('creditNoteSubProduct.productBatchID' => $batchID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
