<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains credit note payment details Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CreditNotePaymentDetails implements InputFilterAwareInterface
{

    public $creditNotePaymentDetailsID;
    public $paymentMethodID;
    public $creditNotePaymentID;
    public $creditNoteID;
    public $creditNotePaymentDetailsAmount;
    public $creditNotePaymentDetailsCurrencyGainOrLoss;
    protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->creditNotePaymentDetailsID = (!empty($data['creditNotePaymentDetailsID'])) ? $data['creditNotePaymentDetailsID'] : null;
        $this->paymentMethodID = (!empty($data['paymentMethodID'])) ? $data['paymentMethodID'] : null;
        $this->creditNotePaymentID = (!empty($data['creditNotePaymentID'])) ? $data['creditNotePaymentID'] : null;
        $this->creditNoteID = (!empty($data['creditNoteID'])) ? $data['creditNoteID'] : null;
        $this->creditNotePaymentDetailsAmount = (!empty($data['creditNotePaymentDetailsAmount'])) ? $data['creditNotePaymentDetailsAmount'] : 0.00;
        $this->creditNotePaymentDetailsCurrencyGainOrLoss = (!empty($data['creditNotePaymentDetailsCurrencyGainOrLoss'])) ? $data['creditNotePaymentDetailsCurrencyGainOrLoss'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

// Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
