<?php

/**
 * @author Ashan     <ashan@thinkcube.com>
 * This file contains Credit Note product tax Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DraftInvoiceReturnProductTax implements InputFilterAwareInterface
{

    public $draftInvoiceReturnProductTaxID;
    public $draftInvoiceReturnProductID;
    public $taxID;
    public $invoiceReturnProductTaxPrecentage;
    public $invoiceReturnProductTaxAmount;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->draftInvoiceReturnProductTaxID = (!empty($data['draftInvoiceReturnProductTaxID'])) ? $data['draftInvoiceReturnProductTaxID'] : null;
        $this->draftInvoiceReturnProductID = (!empty($data['draftInvoiceReturnProductID'])) ? $data['draftInvoiceReturnProductID'] : null;
        $this->taxID = (!empty($data['taxID'])) ? $data['taxID'] : null;
        $this->invoiceReturnProductTaxPrecentage = (!empty($data['invoiceReturnProductTaxPrecentage'])) ? $data['invoiceReturnProductTaxPrecentage'] : null;
        $this->invoiceReturnProductTaxAmount = (!empty($data['invoiceReturnProductTaxAmount'])) ? $data['invoiceReturnProductTaxAmount'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
