<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains return product tax Table Functions
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;

class ReturnsProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();

        return $rowset;
    }

    public function saveReturnsProductTax(ReturnsProductTax $returnProductTax)
    {
        $data = array(
            'salesReturnProductID' => $returnProductTax->salesReturnProductID,
            'taxID' => $returnProductTax->taxID,
            'salesReturnProductTaxPrecentage' => $returnProductTax->salesReturnProductTaxPrecentage,
            'salesReturnProductTaxAmount' => $returnProductTax->salesReturnProductTaxAmount,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getReturnProductTaxByReturnProductID($salesReturProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("salesReturnProductTax")
                ->columns(array("*"))
                ->join("tax", "salesReturnProductTax.taxID = tax.id", array("*"), "left")
                ->where(array("salesReturnProductID" => $salesReturProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

}
