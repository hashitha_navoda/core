<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DeliveryNote
{

    public $deliveryNoteID;
    public $deliveryNoteCode;
    public $customerID;
    public $deliveryNoteLocationID;
    public $deliveryNoteDeliveryDate;
    public $deliveryNoteCharge;
    public $deliveryNotePriceTotal;
    public $deliveryNoteAddress;
    public $deliveryNoteComment;
    public $salesOrderID;
    public $entityID;
    public $salesPersonID;
    public $customCurrencyId;
    public $deliveryNoteCustomCurrencyRate;
    public $priceListId;
    public $status;
    public $customerProfileID;
    public $jobId;
    protected $inputFilter;

    public function exchangeArray($data)
    {

        $this->deliveryNoteID = (!empty($data['deliveryNoteID'])) ? $data['deliveryNoteID'] : null;
        $this->deliveryNoteCode = (!empty($data['deliveryNoteCode'])) ? $data['deliveryNoteCode'] : null;
        $this->customerID = (!empty($data['customerID'])) ? $data['customerID'] : null;
        $this->deliveryNoteLocationID = (!empty($data['deliveryNoteLocationID'])) ? $data['deliveryNoteLocationID'] : null;
        $this->deliveryNoteDeliveryDate = (!empty($data['deliveryNoteDeliveryDate'])) ? $data['deliveryNoteDeliveryDate'] : null;
        $this->deliveryNoteCharge = (!empty($data['deliveryNoteCharge'])) ? $data['deliveryNoteCharge'] : null;
        $this->deliveryNotePriceTotal = (!empty($data['deliveryNotePriceTotal'])) ? $data['deliveryNotePriceTotal'] : null;
        $this->deliveryNoteAddress = (!empty($data['deliveryNoteAddress'])) ? $data['deliveryNoteAddress'] : null;
        $this->deliveryNoteComment = (!empty($data['deliveryNoteComment'])) ? $data['deliveryNoteComment'] : null;
        $this->salesOrderID = (!empty($data['salesOrderID'])) ? $data['salesOrderID'] : null;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->salesPersonID = (!empty($data['salesPersonID'])) ? $data['salesPersonID'] : null;
        $this->customCurrencyId = (!empty($data['customCurrencyId'])) ? $data['customCurrencyId'] : null;
        $this->deliveryNoteCustomCurrencyRate = (!empty($data['deliveryNoteCustomCurrencyRate'])) ? $data['deliveryNoteCustomCurrencyRate'] : 0;
        $this->priceListId = (!empty($data['priceListId'])) ? $data['priceListId'] : 0;
        $this->status = (!empty($data['status'])) ? $data['status'] : 0;
        $this->customerProfileID = (!empty($data['customerProfileID'])) ? $data['customerProfileID'] : null;
        $this->jobId = (!empty($data['jobId'])) ? (int)$data['jobId'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        return $this->inputFilter;
    }

    // Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
