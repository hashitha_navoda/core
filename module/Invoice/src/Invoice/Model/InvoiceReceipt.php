<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contaions invoice reciept model
 */

namespace Invoice\Model;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilterInterface;

class InvoiceReceipt
{

    public $id;
    public $productID;
    public $productName;
    public $quantity;
    public $returnedQuantity;
    public $unitPrice;
    public $uom;
    public $discount;
    public $invoiceID;
    public $total;
    public $tax;
    private $inputFilter;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->invoiceID = (!empty($data['invoiceID'])) ? $data['invoiceID'] : null;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->productName = (!empty($data['productName'])) ? $data['productName'] : null;
        $this->unitPrice = (!empty($data['unitPrice'])) ? $data['unitPrice'] : null;
        $this->uom = (!empty($data['uom'])) ? $data['uom'] : null;
        $this->quantity = (!empty($data['quantity'])) ? $data['quantity'] : null;
        $this->returnedQuantity = (!empty($data['returnedQuantity'])) ? $data['returnedQuantity'] : 0;
        $this->discount = (!empty($data['discount'])) ? $data['discount'] : null;
        $this->total = (!empty($data['total'])) ? $data['total'] : null;
        $this->tax = (!empty($data['tax'])) ? $data['tax'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new inputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'item_code',
                        'required' => true,
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'item',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 50,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'unit_price',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'discount',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'quantity',
                        'required' => true,
            )));

            $this->inputFilter = $inputFilter;
        }
        return $this->inputFilter;
    }

}

?>