<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains credit note product tax Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class CreditNoteProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveCreditNoteProductTax(CreditNoteProductTax $creditNoteProductTax)
    {
        $data = array(
            'creditNoteProductID' => $creditNoteProductTax->creditNoteProductID,
            'taxID' => $creditNoteProductTax->taxID,
            'creditNoteProductTaxPercentage' => $creditNoteProductTax->creditNoteProductTaxPercentage,
            'creditNoteTaxAmount' => $creditNoteProductTax->creditNoteTaxAmount,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getCreditNoteProductTaxByCreditNoteProductID($creditNoteProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProductTax")
                ->columns(array("*"))
                ->join("tax", "creditNoteProductTax.taxID = tax.id", array("*"), "left")
                ->where(array("creditNoteProductID" => $creditNoteProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

    public function getCreditNoteProductTax($creditNoteID = NULL, $locationProductID = NULL, $locationID = NULL)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProductTax")
                ->columns(array("*"))
                ->join("creditNoteProduct", "creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID", array("*"))
                ->join("product", "product.productID = creditNoteProduct.productID", array("*"))
                ->join("locationProduct", "locationProduct.productID = creditNoteProduct.productID", array("*"))
                ->where(array(
                    'locationProduct.locationProductID' => $locationProductID,
                    'locationProduct.locationID' => $locationID,
                    'creditNoteProduct.creditNoteID' => $creditNoteID
                ))
                ->group('taxID');
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        $data = [];
        foreach ($results as $result) {
            $data[] = $result;
        }
        return $data;
    }

    /**
    * this function use to calculate all tax values to the given credit note ID
    * @param string $creditNoteID
    * return array
    **/

    public function getTotalCreditNoteProductTax($creditNoteID)
    {

        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProductTax")
                ->columns(array('totalTax' => new Expression('SUM(creditNoteTaxAmount)')))
                ->join("creditNoteProduct", "creditNoteProduct.creditNoteProductID = creditNoteProductTax.creditNoteProductID", array("*"))
                ->where(array('creditNoteProduct.creditNoteID' => $creditNoteID));
                
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();

        return $results;
    }

    public function getCreditTax($creditNoteID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("creditNoteProductTax")
                ->columns(array("*"))
                ->join("tax", "creditNoteProductTax.taxID = tax.id", array("*"), "left")
                ->join("creditNoteProduct", "creditNoteProductTax.creditNoteProductID = creditNoteProduct.creditNoteProductID", array("creditNoteProductID"), "left")
                ->where(array("creditNoteProduct.creditNoteID" => $creditNoteID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }


}
