<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DraftInvSubProductTable
{

    protected $tableGateway;
    public $adapter;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll($paginated = FALSE)
    {
        if ($paginated) {
            $select = new Select();
            $select->from('invoice')
                    ->columns(array('*'))
                    ->order(array('issuedDate' => 'DESC'));
            $resultSetPrototype = new ResultSet();
            $resultSetPrototype->setArrayObjectPrototype(new Invoice());
            $paginatorAdapter = new DbSelect(
                    $select, $this->tableGateway->getAdapter(), $resultSetPrototype
            );
            $paginator = new Paginator($paginatorAdapter);
            return $paginator;
        }

        $resultSet = $this->tableGateway->select();
        return $resultSet;
    }

    public function saveInvoiceSubProduct(DraftInvSubProduct $invoiceSubProduct)
    {  
        
        $data = array(
            'salesInvocieProductID' => $invoiceSubProduct->salesInvocieProductID,
            'productBatchID' => $invoiceSubProduct->productBatchID,
            'productSerialID' => $invoiceSubProduct->productSerialID,
            'salesInvoiceSubProductQuantity' => $invoiceSubProduct->salesInvoiceSubProductQuantity,
            'salesInvoiceSubProductWarranty' => $invoiceSubProduct->salesInvoiceSubProductWarranty,
            'salesInvoiceSubProductWarrantyType' => $invoiceSubProduct->salesInvoiceSubProductWarrantyType,
        );
        
        if (!$this->tableGateway->insert($data)) {
            return FALSE;
        }

        $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        return $result;
    }

}
