<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodTT related filters
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class IncomingPaymentMethodUniformVoucher
{

    public $incomingPaymentMethodUniformVoucherID;
    public $voucherNumber;
    public $voucherAccountID;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->incomingPaymentMethodUniformVoucherID = (!empty($data['incomingPaymentMethodUniformVoucherID'])) ? $data['incomingPaymentMethodUniformVoucherID'] : null;
        $this->voucherNumber = (!empty($data['voucherNumber'])) ? $data['voucherNumber'] : null;
        $this->voucherAccountID = (!empty($data['voucherAccountID'])) ? $data['voucherAccountID'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();
        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
