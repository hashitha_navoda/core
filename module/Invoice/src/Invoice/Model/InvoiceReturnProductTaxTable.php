<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains credit note product tax Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class InvoiceReturnProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveInvoiceReturnProductTax(InvoiceReturnProductTax $invoiceReturnProductTax)
    {
        $data = array(
            'invoiceReturnProductID' => $invoiceReturnProductTax->invoiceReturnProductID,
            'taxID' => $invoiceReturnProductTax->taxID,
            'invoiceReturnProductTaxPrecentage' => $invoiceReturnProductTax->invoiceReturnProductTaxPrecentage,
            'invoiceReturnProductTaxAmount' => $invoiceReturnProductTax->invoiceReturnProductTaxAmount,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getInvoiceReturnProductTaxByInvoiceReturnProductID($invoiceReturnProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("invoiceReturnProductTax")
                ->columns(array("*"))
                ->join("tax", "invoiceReturnProductTax.taxID = tax.id", array("*"), "left")
                ->where(array("invoiceReturnProductID" => $invoiceReturnProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

  

    public function getInvoiceReturnTax($invoiceReturnID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("invoiceReturnProductTax")
                ->columns(array("*"))
                ->join("tax", "invoiceReturnProductTax.taxID = tax.id", array("*"), "left")
                ->join("invoiceReturnProduct", "invoiceReturnProductTax.invoiceReturnProductID = invoiceReturnProduct.invoiceReturnProductID", array("invoiceReturnProductID"), "left")
                ->where(array("invoiceReturnProduct.invoiceReturnID" => $invoiceReturnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }


}
