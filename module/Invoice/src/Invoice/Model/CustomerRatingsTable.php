<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains Customer Ratings related database operations
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class CustomerRatingsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    //save customer rating types Data
    public function saveCustomerRatingTypes(CustomerRatings $customerRatings)
    {
        $data = array(
            'customerId' => $customerRatings->customerId,
            'customerProfileId' => $customerRatings->customerProfileId,
            'ratingTypesId' => $customerRatings->ratingTypesId,
            'customerRatingsRatingValue' => $customerRatings->customerRatingsRatingValue,
        );

        $value = $this->tableGateway->insert($data);
        return $value;
    }

    //get customer rating types by customer id
    public function getCustomerRatingsByCustomerId($customerID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('customerRatings')
                ->columns(array('*'))
                ->join('ratingTypes', 'customerRatings.ratingTypesId= ratingTypes.ratingTypesId', array('*'), 'left')
                ->where(array('customerRatings.customerId' => $customerID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return $results;
    }

    //update customer ratings by id
    public function updateCustomerRatingsTypeById($data, $id)
    {
        try {
            $this->tableGateway->update($data, array('customerRatingsId' => $id));
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
        }
    }

    //delete customer raings by customer rating id
    public function deleteCustomerRatings($customerRatingsId)
    {
        try {
            $result = $this->tableGateway->delete(array('customerRatingsId' => $customerRatingsId));
            return $result;
        } catch (Exception $exc) {
            echo $exc->getTraceAsString();
            return false;
        }
    }

}
