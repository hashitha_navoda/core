<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class DeliveryNoteSubProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function fetchAll()
    {
        $rowset = $this->tableGateway->select();
        return $rowset;
    }

    public function saveDeliveryNoteSubProduct(DeliveryNoteSubProduct $deliveryNoteSubProduct)
    {
        $data = array(
            'deliveryNoteProductID' => $deliveryNoteSubProduct->deliveryNoteProductID,
            'productBatchID' => $deliveryNoteSubProduct->productBatchID,
            'productSerialID' => $deliveryNoteSubProduct->productSerialID,
            'deliveryProductSubQuantity' => $deliveryNoteSubProduct->deliveryProductSubQuantity,
            'deliveryNoteSubProductsWarranty' => $deliveryNoteSubProduct->deliveryNoteSubProductsWarranty,
            'deliveryNoteSubProductsWarrantyType' => $deliveryNoteSubProduct->deliveryNoteSubProductsWarrantyType,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    //get  productserial by deliverynoteProductid and ProdutSerialID
    public function getProductSerialByDeliveryNoteProductIDAndProductSerialID($deliveryNoteProductID, $serialID)
    {
        $rowset = $this->tableGateway->select(array('deliveryNoteProductID' => $deliveryNoteProductID, 'productSerialID' => $serialID));
        return $rowset->current();
    }

    //update delivery Note sub product data
    public function updateDeliveryNoteSubProductData($data, $deliveryNoteSubProductID)
    {
        $state = $this->tableGateway->update($data, array('deliveryNoteSubProductID' => $deliveryNoteSubProductID));
        return $state;
    }

    //get  productBatch by deliverynoteProductid and ProdutBatchID
    public function getProductBatchByDeliveryNoteProductIDAndProductBatchID($deliveryNoteProductID, $batchID)
    {
        $rowset = $this->tableGateway->select(array('deliveryNoteProductID' => $deliveryNoteProductID, 'productBatchID' => $batchID));
        return $rowset->current();
    }

}
