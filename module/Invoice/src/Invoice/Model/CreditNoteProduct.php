<?php

/**
 * @author Ashan     <ashan@thinkcube.com>
 * This file contains Credit Note product Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CreditNoteProduct implements InputFilterAwareInterface
{

    public $creditNoteProductID;
    public $creditNoteID;
    public $invoiceProductID;
    public $productID;
    public $creditNoteProductPrice;
    public $creditNoteProductDiscount;
    public $creditNoteProductDiscountType;
    public $creditNoteProductQuantity;
    public $creditNoteProductTotal;
    public $rackID;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->creditNoteProductID = (!empty($data['creditNoteProductID'])) ? $data['creditNoteProductID'] : null;
        $this->creditNoteID = (!empty($data['creditNoteID'])) ? $data['creditNoteID'] : null;
        $this->invoiceProductID = (!empty($data['invoiceProductID'])) ? $data['invoiceProductID'] : 0;
        $this->productID = (!empty($data['productID'])) ? $data['productID'] : null;
        $this->creditNoteProductPrice = (!empty($data['creditNoteProductPrice'])) ? $data['creditNoteProductPrice'] : null;
        $this->creditNoteProductDiscount = (!empty($data['creditNoteProductDiscount'])) ? $data['creditNoteProductDiscount'] : null;
        $this->creditNoteProductDiscountType = (!empty($data['creditNoteProductDiscountType'])) ? $data['creditNoteProductDiscountType'] : null;
        $this->creditNoteProductQuantity = (!empty($data['creditNoteProductQuantity'])) ? $data['creditNoteProductQuantity'] : null;
        $this->creditNoteProductTotal = (!empty($data['creditNoteProductTotal'])) ? $data['creditNoteProductTotal'] : 0;
        $this->rackID = (!empty($data['rackID'])) ? $data['rackID'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
