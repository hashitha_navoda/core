<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains creditc note sub product Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class InvoiceReturnSubProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveInvoiceReturnSubProduct(InvoiceReturnSubProduct $invoiceReturnSubProduct)
    {
        $data = array(
            'invoiceReturnProductID' => $invoiceReturnSubProduct->invoiceReturnProductID,
            'productBatchID' => $invoiceReturnSubProduct->productBatchID,
            'productSerialID' => $invoiceReturnSubProduct->productSerialID,
            'invoiceReturnSubProductQuantity' => $invoiceReturnSubProduct->invoiceReturnSubProductQuantity,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

}
