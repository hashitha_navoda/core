<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\Db\Sql\Select;
// use Invoice\Model\InvoiceProductDeliveryNoteProduct;


class InvoiceProductDeliveryNoteProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function save(InvoiceProductDeliveryNoteProduct $invoiceProductDeliveryNoteProduct)
    {
        $data = array(
            'jobID' => $invoiceProductDeliveryNoteProduct->jobID,
            'jobCostID' => $invoiceProductDeliveryNoteProduct->jobCostID,
            'deliveryNoteProductID' => $invoiceProductDeliveryNoteProduct->deliveryNoteProductID,
            'invoiceProductID' => $invoiceProductDeliveryNoteProduct->invoiceProductID,
            'jobProductCostQty' => $invoiceProductDeliveryNoteProduct->jobProductCostQty,
            
        );
        try {
        	$this->tableGateway->insert($data);
        	return $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
        } catch (Exception $e) {
        	error_log($e->getMessage());
        	return false;	
        }
        
    }

    public function getJobMappingByInvoiceProductIdAndJobId($invoiceProductID, $jobID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("invoiceProductDeliveryNoteProduct")
                ->columns(array("*"))
                ->where(array('invoiceProductID' => $invoiceProductID, 'jobID' => $jobID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function update($data, $invoiceProductDeliveryNoteProductID)
    {
        $this->tableGateway->update($data, array('invoiceProductDeliveryNoteProductID' => $invoiceProductDeliveryNoteProductID));
        return TRUE;
    }
}