<?php

/**
 * @author Prathap Weerasingeh <prathap@thinkcube.com>
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;

class QuotationProductTaxTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveQuotProductTax(QuotationProductTax $quotProductTax)
    {
        $data = array(
            'quotationID' => $quotProductTax->quotID,
            'quotProductID' => $quotProductTax->quotProductID,
            'quotTaxID' => $quotProductTax->quotTaxID,
            'quotTaxPrecentage' => $quotProductTax->quotTaxPrecentage,
            'quotTaxAmount' => $quotProductTax->quotTaxAmount,
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getQuotationProductTax($quotProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("quotationProductTax")
                ->columns(array("*"))
                ->join("tax", "quotationProductTax.quotTaxID = tax.id", array("*"), "left")
                ->where(array("quotProductID" => $quotProductID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getQuotationTax($quotID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("quotationProductTax")
                ->columns(array("*"))
                ->join("tax", "quotationProductTax.quotTaxID = tax.id", array("*"), "left")
                ->where(array("quotationID" => $quotID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultsArray = [];
        foreach ($results as $result) {
            $resultsArray[] = $result;
        }
        return $resultsArray;
    }

}
