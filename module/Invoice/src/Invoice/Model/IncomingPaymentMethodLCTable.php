<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodLC related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Paginator\Adapter\DbSelect;

class IncomingPaymentMethodLCTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveLCPaymentMethod(IncomingPaymentMethodLC $incomingPaymentMethodLC)
    {
        $lCData = array(
            'incomingPaymentMethodLCReference' => $incomingPaymentMethodLC->incomingPaymentMethodLCReference,
        );
        if ($this->tableGateway->insert($lCData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function updateLCPaymentMethod($data, $lcID)
    {
        if ($lcID) {
            if ($this->tableGateway->update($data, array('incomingPaymentMethodLCId' => $lcID))) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            return FALSE;
        }
    }

}
