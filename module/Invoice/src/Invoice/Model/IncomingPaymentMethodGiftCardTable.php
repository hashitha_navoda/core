<?php

/**
 * @author Ashan Madushka <ashan@thinkcube.com>
 * This file contains incomingPaymentMethodGiftCard related database operations
 */

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;

class IncomingPaymentMethodGiftCardTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveGiftCardPayment(IncomingPaymentMethodGiftCard $incomingPaymentMethodGiftCard)
    {
        $giftCardData = array(
            'giftCardId' => $incomingPaymentMethodGiftCard->giftCardId,
        );
        if ($this->tableGateway->insert($giftCardData)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

}
