<?php

/**
 * @author ashan <ashan@thinkcube.com>
 * This file contains creditc note product Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class InvoiceReturnProductTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function getInvoiceReturnProductsByInvoiceID($invoiceID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("invoiceReturnProduct")
                ->columns(array("*"))
                ->join("invoiceReturns", "invoiceReturnProduct.invoiceReturnID= invoiceReturns.invoiceReturnID", array("*"), "left")
                ->join("salesInvoiceProduct", "invoiceReturnProduct.invoiceProductID= salesInvoiceProduct.salesInvoiceProductID", array("*"), "left")
                ->join('product', 'invoiceReturnProduct.productID =  product.productID', array('*'), 'left')
                ->join('locationProduct', 'locationProduct.locationProductID =  salesInvoiceProduct.locationProductID', array('*'), 'left')
                ->where(array("salesInvoiceProduct.salesInvoiceID" => $invoiceID));
        $select->where->notEqualTo('invoiceReturns.statusID',4);
        $select->where->notEqualTo('invoiceReturns.statusID',5);
        $select->where->notEqualTo('invoiceReturns.statusID',10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function getInvoiceReturnProductAndSubProductDataByInvoiceProductID($invoiceProductID)
    {
        $sql = new Sql($this->tableGateway->getAdapter());
        $select = new Select();
        $select->from("invoiceReturnProduct")
                ->columns(array("*"))
                ->join('invoiceReturnSubProduct', 'invoiceReturnSubProduct.invoiceReturnProductID=invoiceReturnProduct.invoiceReturnProductID', array('invoiceReturnSubProductID', 'productBatchID', 'productSerialID', 'invoiceReturnSubProductQuantity'), 'left')
                ->join('invoiceReturns', 'invoiceReturnProduct.invoiceReturnID=invoiceReturns.invoiceReturnID', array('*'), 'left')
                ->join('entity', 'invoiceReturns.entityID=entity.entityID', array('*'), 'left')
                ->where(array('invoiceReturnProduct.invoiceProductID' => $invoiceProductID, 'entity.deleted' => 0));
        $select->where->notEqualTo('invoiceReturns.statusID',4);
        $select->where->notEqualTo('invoiceReturns.statusID',5);
        $select->where->notEqualTo('invoiceReturns.statusID',10);
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function saveInvoiceReturnProducts(InvoiceReturnProduct $invoiceReturnProduct)
    {
        $data = array(
            'invoiceReturnID' => $invoiceReturnProduct->invoiceReturnID,
            'invoiceProductID' => $invoiceReturnProduct->invoiceProductID,
            'productID' => $invoiceReturnProduct->productID,
            'invoiceReturnProductPrice' => $invoiceReturnProduct->invoiceReturnProductPrice,
            'invoiceReturnProductDiscount' => $invoiceReturnProduct->invoiceReturnProductDiscount,
            'invoiceReturnProductDiscountType' => $invoiceReturnProduct->invoiceReturnProductDiscountType,
            'invoiceReturnProductQuantity' => $invoiceReturnProduct->invoiceReturnProductQuantity,
            'invoiceReturnProductTotal' => $invoiceReturnProduct->invoiceReturnProductTotal,
        );
        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getAllInvoiceReturnProductDetailsByInvoiceReturnID($invoiceReturnID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('invoiceReturnProduct');
        $select->join(
                'product', 'invoiceReturnProduct.productID = product.productID', array(
            '*',
                )
        );
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->where(array('productUomBase' => '1'));
        $select->where(array('invoiceReturnProduct.invoiceReturnID' => $invoiceReturnID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

   
    public function getInvoiceReturnDetailsByInvoiceReturnID($creditNoteID , $locationID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('invoiceReturnProduct')
               ->columns(array('*','invoiceReturnProID' => new Expression('invoiceReturnProduct.invoiceReturnProductID')))
               ->join('invoiceReturns', 'invoiceReturns.invoiceReturnID = invoiceReturnProduct.invoiceReturnID', array('locationID','invoiceID'),'left')
               ->join('locationProduct', 'locationProduct.productID = invoiceReturnProduct.productID', array('locationProductID'),'left')
               ->join('salesInvoiceProduct', 'salesInvoiceProduct.salesInvoiceProductID = invoiceReturnProduct.invoiceProductID', array('documentTypeID','salesInvoiceProductDocumentID'),'left')
               ->join('invoiceReturnSubProduct', 'invoiceReturnSubProduct.invoiceReturnProductID = invoiceReturnProduct.invoiceReturnProductID', array('*'),'left');
        $select->where(array('invoiceReturnProduct.invoiceReturnID' => $creditNoteID));
        $select->where(array('locationProduct.locationID' => $locationID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

    public function checkAnyInvoiceReturnUsedThisProductAfterThisInvoiceReturnID($invoiceReturnID , $serialID, $batchID)
    {
        $adapter = $this->tableGateway->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('invoiceReturnProduct')
               ->columns(array('invoiceReturnID'))
               ->join('invoiceReturns', 'invoiceReturns.invoiceReturnID = invoiceReturnProduct.invoiceReturnID', array('invoiceID'),'left')
               ->join('invoiceReturnSubProduct', 'invoiceReturnSubProduct.invoiceReturnProductID = invoiceReturnProduct.invoiceReturnProductID', array('*'),'left');
        $select->where->notEqualTo('invoiceReturns.statusID',5);
        $select->where->greaterThan('invoiceReturnProduct.invoiceReturnID', $invoiceReturnID);
        if (!is_null($serialID)) {
            $select->where(array('invoiceReturnSubProduct.productSerialID' => $serialID));
        }
        if (!is_null($batchID)) {
            $select->where(array('invoiceReturnSubProduct.productBatchID' => $batchID));
        }
        $statement = $sql->prepareStatementForSqlObject($select);
        $rowset = $statement->execute();
        if (!$rowset) {
            return NULL;
        }
        return $rowset;
    }

}
