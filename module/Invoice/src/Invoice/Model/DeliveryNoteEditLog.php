<?php
namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class DeliveryNoteEditLog implements InputFilterAwareInterface
{

    public $deliveryNoteEditLogID;
    public $userID;
    public $deliveryNoteID;
    public $dateAndTime;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->deliveryNoteEditLogID = (!empty($data['deliveryNoteEditLogID'])) ? $data['deliveryNoteEditLogID'] : null;
        $this->userID = (!empty($data['userID'])) ? $data['userID'] : null;
        $this->deliveryNoteID = (!empty($data['deliveryNoteID'])) ? $data['deliveryNoteID'] : null;
        $this->dateAndTime = (!empty($data['dateAndTime'])) ? $data['dateAndTime'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
