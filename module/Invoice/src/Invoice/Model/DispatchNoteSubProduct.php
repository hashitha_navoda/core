<?php

namespace Invoice\Model;

/**
 * Class DispatchNoteSubProduct
 * @package Invoice\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class DispatchNoteSubProduct
{
    /**
     * @var int Auto incremented dispatch note sub product Id. (Primary key)
     */
    public $dispatchNoteSubProductId;
    
    /**
     * @var int dispatch note product id
     */
    public $dispatchNoteProductId;
    
    /**
     * @var int product batch id 
     */
    public $productBatchId;
    
    /**
     * @var int product serial id 
     */
    public $productSerialId;
    
    /**
     * @var decimal dispatch note product quantity
     */
    public $dispatchNoteSubProductQuantity;
    
    /**
     * @var int product warranty 
     */
    public $dispatchNoteSubProductWarranty;    

    public function exchangeArray($data)
    {
        $this->dispatchNoteSubProductId = (!empty($data['dispatchNoteSubProductId'])) ? $data['dispatchNoteSubProductId'] : null;
        $this->dispatchNoteProductId    = (!empty($data['dispatchNoteProductId'])) ? $data['dispatchNoteProductId'] : null;
        $this->productBatchId           = (!empty($data['productBatchId'])) ? $data['productBatchId'] : null;
        $this->productSerialId          = (!empty($data['productSerialId'])) ? $data['productSerialId'] : null;
        $this->dispatchNoteSubProductQuantity = (!empty($data['dispatchNoteSubProductQuantity'])) ? $data['dispatchNoteSubProductQuantity'] : null;
        $this->dispatchNoteSubProductWarranty = (!empty($data['dispatchNoteSubProductWarranty'])) ? $data['dispatchNoteSubProductWarranty'] : null;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }
}
