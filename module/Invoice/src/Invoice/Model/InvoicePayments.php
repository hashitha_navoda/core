<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Invoice Payments Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoicePayments implements InputFilterAwareInterface
{

    public $incomingInvoicePaymentID;
    public $incomingPaymentID;
    public $salesInvoiceID;
    public $incomingInvoicePaymentAmount;
    public $incomingInvoicePaymentCurrencyGainOrLoss;
    public $incomeID;
    protected $inputFilter;                       // <-- Add this variable

    public function exchangeArray($data)
    {
        $this->incomingInvoicePaymentID = (!empty($data['incomingInvoicePaymentID'])) ? $data['incomingInvoicePaymentID'] : null;
        $this->incomingPaymentID = (!empty($data['incomingPaymentID'])) ? $data['incomingPaymentID'] : null;
        $this->salesInvoiceID = (!empty($data['salesInvoiceID'])) ? $data['salesInvoiceID'] : null;
        $this->incomeID = (!empty($data['incomeID'])) ? $data['incomeID'] : null;
        $this->incomingInvoicePaymentAmount = (!empty($data['incomingInvoicePaymentAmount'])) ? $data['incomingInvoicePaymentAmount'] : null;
        $this->incomingInvoicePaymentCurrencyGainOrLoss = (!empty($data['incomingInvoicePaymentCurrencyGainOrLoss'])) ? $data['incomingInvoicePaymentCurrencyGainOrLoss'] : 0.00;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {

        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'incomingPaymentID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'salesInvoiceID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $inputFilter->add($factory->createInput(array(
                        'name' => 'incomingInvoicePaymentAmount',
                        'required' => true,
                        'filters' => array(
                            array('name' => 'Int'),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

// Add the following method:
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
