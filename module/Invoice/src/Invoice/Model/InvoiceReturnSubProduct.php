<?php

/**
 * @author Ashan <ashan@thinkcube.com>
 * This file contains Credit Note sub product Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoiceReturnSubProduct implements InputFilterAwareInterface
{

    public $invoiceReturnSubProductID;
    public $invoiceReturnProductID;
    public $productBatchID;
    public $productSerialID;
    public $invoiceReturnSubProductQuantity;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->invoiceReturnSubProductID = (!empty($data['invoiceReturnSubProductID'])) ? $data['invoiceReturnSubProductID'] : null;
        $this->invoiceReturnProductID = (!empty($data['invoiceReturnProductID'])) ? $data['invoiceReturnProductID'] : null;
        $this->productBatchID = (!empty($data['productBatchID'])) ? $data['productBatchID'] : null;
        $this->productSerialID = (!empty($data['productSerialID'])) ? $data['productSerialID'] : null;
        $this->invoiceReturnSubProductQuantity = (!empty($data['invoiceReturnSubProductQuantity'])) ? $data['invoiceReturnSubProductQuantity'] : null;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
