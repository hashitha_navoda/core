<?php

namespace Invoice\Model;

/**
 * Class DispatchNote
 * @package Invoice\Model
 * @author Madawa Chandraratne <madawa@thinkcube.com>
 */
class ContactListContacts
{
    
    public $contactListContactsID;
    public $contactID;
    public $contactListID;
    public $noOfContacts;

    public function exchangeArray($data)
    {
        $this->contactListContactsID      = (!empty($data['contactListContactsID'])) ? $data['contactListContactsID'] : null;
        $this->contactID    = (!empty($data['contactID'])) ? $data['contactID'] : 0;
        $this->contactListID    = (!empty($data['contactListID'])) ? $data['contactListID'] : 0;
    }
    
    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
