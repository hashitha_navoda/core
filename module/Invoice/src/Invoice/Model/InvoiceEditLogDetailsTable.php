<?php

/**
 * @author sandun <sandun@thinkcube.com>
 * This file contains InvoiceEditLogDetails Table Functions
 */

namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class InvoiceEditLogDetailsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveInvoiceEditLogDetails(InvoiceEditLogDetials $invoiceEditLogDetails)
    {
        $data = array(
            'invoiceEditLogDetailsOldState' => $invoiceEditLogDetails->invoiceEditLogDetailsOldState,
            'invoiceEditLogDetailsNewState' => $invoiceEditLogDetails->invoiceEditLogDetailsNewState,
            'invoiceEditLogDetailsCategory' => $invoiceEditLogDetails->invoiceEditLogDetailsCategory,
            'invoiceEditLogID' => $invoiceEditLogDetails->invoiceEditLogID
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getEditedInvocieDetails($fromDate = NULL, $toDate = NULL, $users = [], $customerIds = [])
    {
        $adaptor = $this->tableGateway->getAdapter();
        $sql = new Sql($adaptor);
        $select = $sql->select();
        $select->from('invoiceEditLog');
        $select->columns(array("*"));
        $select->join('invoiceEditLogDetails', 'invoiceEditLog.invoiceEditLogID = invoiceEditLogDetails.invoiceEditLogID', ['*'], 'left');
        $select->join('user', 'invoiceEditLog.userID = user.userID', ['createdUser' => new Expression('user.userUsername')], 'left');
        $select->join('salesInvoice', 'salesInvoice.salesInvoiceID = invoiceEditLog.salesInvoiceID', ['*'], 'left');
        $select->join('customer', 'customer.customerID = salesInvoice.customerID', ['customerName', 'customerCode'], 'left');
//        $select->where->notEqualTo('salesInvoice.statusID', 8);
        if ($fromDate && $toDate) {
            $select->where->between('invoiceEditLog.dateAndTime', $fromDate, $toDate);
        }
        if ($users) {
            $select->where->in('invoiceEditLog.userID', $users);
        }
        if ($customerIds) {
            $select->where->in('salesInvoice.customerID', $customerIds);
        }
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

}
