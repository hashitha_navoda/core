<?php

/**
 * @author sandun<sandun@thinkcube.com>
 * This file contains invoiceEditLog Model
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class InvoiceEditLog implements InputFilterAwareInterface
{

    public $invoiceEditLogID;
    public $userID;
    public $salesInvoiceID;
    public $dateAndTime;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->invoiceEditLogID = (!empty($data['invoiceEditLogID'])) ? $data['invoiceEditLogID'] : null;
        $this->userID = (!empty($data['userID'])) ? $data['userID'] : null;
        $this->salesInvoiceID = (!empty($data['salesInvoiceID'])) ? $data['salesInvoiceID'] : null;
        $this->dateAndTime = (!empty($data['dateAndTime'])) ? $data['dateAndTime'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();

            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
