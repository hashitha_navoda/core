<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class ServiceChargeType
{

    public $serviceChargeTypeID;
    public $serviceChargeTypeName;
    public $serviceChargeTypeCode;
    public $serviceChargeTypeGLAccountID;
    public $locationID;
    
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->serviceChargeTypeID = (!empty($data['serviceChargeTypeID'])) ? $data['serviceChargeTypeID'] : null;
        $this->serviceChargeTypeName = (!empty($data['serviceChargeTypeName'])) ? $data['serviceChargeTypeName'] : null;
        $this->serviceChargeTypeCode = (!empty($data['serviceChargeTypeCode'])) ? $data['serviceChargeTypeCode'] : null;
        $this->serviceChargeTypeGLAccountID = (!empty($data['serviceChargeTypeGLAccountID'])) ? $data['serviceChargeTypeGLAccountID'] : null;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
