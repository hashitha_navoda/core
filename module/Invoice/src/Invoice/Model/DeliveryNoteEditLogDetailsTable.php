<?php
namespace Invoice\Model;

use Zend\Db\ResultSet\ResultSet;
use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Paginator;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;
use Zend\Db\Adapter\Adapter;

class DeliveryNoteEditLogDetailsTable
{

    protected $tableGateway;

    public function __construct(TableGateway $tableGateway)
    {
        $this->tableGateway = $tableGateway;
    }

    public function saveDeliveryNoteEditLogDetails(DeliveryNoteEditLogDetails $deliveryNoteEditLogDetails)
    {
        $data = array(
            'deliveryNoteEditLogDetailsOldState' => $deliveryNoteEditLogDetails->deliveryNoteEditLogDetailsOldState,
            'deliveryNoteEditLogDetailsNewState' => $deliveryNoteEditLogDetails->deliveryNoteEditLogDetailsNewState,
            'deliveryNoteEditLogDetailsCategory' => $deliveryNoteEditLogDetails->deliveryNoteEditLogDetailsCategory,
            'deliveryNoteEditLogID' => $deliveryNoteEditLogDetails->deliveryNoteEditLogID
        );

        if ($this->tableGateway->insert($data)) {
            $result = $this->tableGateway->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getEditedDeliveryNoteDetails($fromDate = NULL, $toDate = NULL, $users = [], $customerIds = [])
    {
        $adaptor = $this->tableGateway->getAdapter();
        $sql = new Sql($adaptor);
        $select = $sql->select();
        $select->from('deliveryNoteEditLog');
        $select->columns(array("*"));
        $select->join('deliveryNoteEditLogDetails', 'deliveryNoteEditLog.deliveryNoteEditLogID = deliveryNoteEditLogDetails.deliveryNoteEditLogID', ['*'], 'left');
        $select->join('user', 'deliveryNoteEditLog.userID = user.userID', ['createdUser' => new Expression('user.userUsername')], 'left');
        $select->join('deliveryNote', 'deliveryNote.deliveryNoteID = deliveryNoteEditLog.deliveryNoteID', ['*'], 'left');
         $select->join('customer', 'customer.customerID = deliveryNote.customerID', ['customerName'], 'left');
//        $select->where->notEqualTo('salesInvoice.statusID', 8);
        if ($fromDate && $toDate) {
            $select->where->between('deliveryNoteEditLog.dateAndTime', $fromDate, $toDate);
        }
        if ($users) {
            $select->where->in('deliveryNoteEditLog.userID', $users);
        }
        if ($customerIds) {
            $select->where->in('deliveryNote.customerID', $customerIds);
        }
        $statment = $sql->prepareStatementForSqlObject($select);
        return $statment->execute();
    }

}
