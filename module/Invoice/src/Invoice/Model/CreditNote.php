<?php

/**
 * @author Ashan     <ashan@thinkcube.com>
 * This file contains Credit Note Model Functions
 */

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CreditNote implements InputFilterAwareInterface
{

    public $creditNoteID;
    public $creditNoteCode;
    public $creditNoteTotal;
    public $locationID;
    public $paymentTermID;
    public $invoiceID;
    public $creditNoteComment;
    public $creditNoteDate;
    public $customerID;
    public $statusID;
    public $creditNotePaymentEligible;
    public $creditNotePaymentAmount;
    public $creditNoteSettledAmount;
    public $customCurrencyId;
    public $creditNoteCustomCurrencyRate;
    public $creditNoteInvoiceDiscountType;
    public $creditNoteInvoiceDiscountAmount;
    public $creditNotePromotionDiscountType;
    public $creditNotePromotionDiscountAmount;
    public $creditNoteDirectFlag;
    public $creditNoteBuyBackFlag;
    public $entityID;
    public $pos;
    public $creditDeviation;
    public $outstandingDeviation;
    public $ableToCancel;
    protected $inputFilter;

    public function exchangeArray($data)
    {
        $this->creditNoteID = (!empty($data['creditNoteID'])) ? $data['creditNoteID'] : null;
        $this->creditNoteCode = (!empty($data['creditNoteCode'])) ? $data['creditNoteCode'] : null;
        $this->creditNoteTotal = (!empty($data['creditNoteTotal'])) ? $data['creditNoteTotal'] : 0.00;
        $this->locationID = (!empty($data['locationID'])) ? $data['locationID'] : null;
        $this->paymentTermID = (!empty($data['paymentTermID'])) ? $data['paymentTermID'] : 0;
        $this->invoiceID = (!empty($data['invoiceID'])) ? $data['invoiceID'] : 0;
        $this->creditNoteComment = (!empty($data['creditNoteComment'])) ? $data['creditNoteComment'] : null;
        $this->creditNoteDate = (!empty($data['creditNoteDate'])) ? $data['creditNoteDate'] : null;
        $this->customerID = $data['customerID'];
        $this->statusID = (!empty($data['statusID'])) ? $data['statusID'] : null;
        $this->creditNotePaymentEligible = (!empty($data['creditNotePaymentEligible'])) ? $data['creditNotePaymentEligible'] : 0;
        $this->creditNotePaymentAmount = (!empty($data['creditNotePaymentAmount'])) ? $data['creditNotePaymentAmount'] : 0.00;
        $this->creditNoteSettledAmount = (!empty($data['creditNoteSettledAmount'])) ? $data['creditNoteSettledAmount'] : 0.00;
        $this->customCurrencyId = (!empty($data['customCurrencyId'])) ? $data['customCurrencyId'] : 0.00;
        $this->creditNoteCustomCurrencyRate = (!empty($data['creditNoteCustomCurrencyRate'])) ? $data['creditNoteCustomCurrencyRate'] : 0.00;
        $this->entityID = (!empty($data['entityID'])) ? $data['entityID'] : null;
        $this->creditNoteInvoiceDiscountType = (!empty($data['creditNoteInvoiceDiscountType'])) ? $data['creditNoteInvoiceDiscountType'] : null;
        $this->creditNoteInvoiceDiscountAmount = (!empty($data['creditNoteInvoiceDiscountAmount'])) ? $data['creditNoteInvoiceDiscountAmount'] : null;
        $this->creditNotePromotionDiscountType = (!empty($data['creditNotePromotionDiscountType'])) ? $data['creditNotePromotionDiscountType'] : null;
        $this->creditNotePromotionDiscountAmount = (!empty($data['creditNotePromotionDiscountAmount'])) ? $data['creditNotePromotionDiscountAmount'] : null;
        $this->creditNoteDirectFlag = (!empty($data['creditNoteDirectFlag'])) ? $data['creditNoteDirectFlag'] : 0;
        $this->creditNoteBuyBackFlag = (!empty($data['creditNoteBuyBackFlag'])) ? $data['creditNoteBuyBackFlag'] : 0;
        $this->pos = (!empty($data['pos'])) ? $data['pos'] : 0;
        $this->creditDeviation = (!empty($data['creditDeviation'])) ? $data['creditDeviation'] : 0.00;
        $this->outstandingDeviation = (!empty($data['outstandingDeviation'])) ? $data['outstandingDeviation'] : 0.00;
        $this->ableToCancel = (!empty($data['ableToCancel'])) ? $data['ableToCancel'] : 0;
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception(
        "Not used");
    }

    public function getInputFilter()
    {
        if (!$this->inputFilter) {
            $inputFilter = new InputFilter();
            $factory = new InputFactory();

            $inputFilter->add($factory->createInput(array(
                        'name' => 'creditNoteID',
                        'required' => false,
                        'filters' => array(
                            array('name' => 'StripTags'),
                            array('name' => 'StringTrim'),
                        ),
                        'validators' => array(
                            array(
                                'name' => 'StringLength',
                                'options' => array(
                                    'encoding' => 'UTF-8',
                                    'min' => 1,
                                    'max' => 100,
                                ),
                            ),
                        ),
            )));
            $this->inputFilter = $inputFilter;
        }

        return $this->inputFilter;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}
