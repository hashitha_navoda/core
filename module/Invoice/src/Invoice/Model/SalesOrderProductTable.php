<?php

namespace Invoice\Model;

use Zend\Db\TableGateway\TableGateway;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Expression;

class SalesOrderProductTable
{

    protected $tableGateWay;

    function __construct(TableGateway $tableGateWay)
    {
        $this->tableGateWay = $tableGateWay;
    }

    public function saveReceipt(SalesOrderProduct $sor)
    {
        $data = array(
            'locationProductID' => $sor->locationProductID,
            'productName' => $sor->productName,
            'soProductDescription' => $sor->soProductDescription,
            'quantity' => $sor->quantity,
            'unitPrice' => $sor->unitPrice,
            'uom' => $sor->uom,
            'discount' => $sor->discount,
            'discountType' => $sor->discountType,
//            'soCode' => $sor->soCode,
            'soID' => $sor->soID,
            'total' => $sor->total,
            'tax' => $sor->tax
        );
        if ($this->tableGateWay->insert($data)) {
            $result = $this->tableGateWay->adapter->getDriver()->getLastGeneratedValue();
            return $result;
        } else {
            return FALSE;
        }
    }

    public function getProductBySalesOrderID($salesOrderID)
    {
        $sql = new Sql($this->tableGateWay->getAdapter());
        $select = $sql->select();
        $select->from('salesOrdersProduct');
        $select->join('locationProduct', 'salesOrdersProduct.locationProductID = locationProduct.locationProductID', array('*'));
        $select->join('product', 'locationProduct.productID = product.productID', array('*'));
        $select->where(array('soID' => $salesOrderID));
        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getProductWithUomBySalesOrderID($salesOrderID)
    {
        $sql = new Sql($this->tableGateWay->getAdapter());
        $select = $sql->select();
        $select->from('salesOrdersProduct');
        $select->join('locationProduct', 'salesOrdersProduct.locationProductID = locationProduct.locationProductID', array('*'));
        $select->join('product', 'locationProduct.productID = product.productID', array('*'));
        $select->join('productUom', 'product.productID = productUom.productID', array('uomID', 'productUomConversion', 'productUomBase'));
        $select->join('uom', 'productUom.uomID = uom.uomID', array('uomAbbr', 'uomDecimalPlace'));
        $select->join('salesOrderProductTax', 'salesOrderProductTax.soProductID =  salesOrdersProduct.soProductID', array('taxTotal' => new Expression('SUM(salesOrderProductTax.soTaxAmount)')), 'left');
        $select->group(array('salesOrdersProduct.soProductID'));
        $select->where(array('productUomBase' => '1'));
        $select->where(array('salesOrdersProduct.soID' => $salesOrderID));

        $query = $sql->prepareStatementForSqlObject($select);
        $rowset = $query->execute();

        if (!$rowset) {
            return null;
        }
        return $rowset;
    }

    public function getproductByID($salesOrder, $productID)
    {
        $rowset = $this->tableGateWay->select(array('salesOrderID' => $salesOrder, 'productID' => $productID));
        return $rowset->current();
    }

    public function updateReceiptQuantity($updateQuantity, $salesOrder, $productID)
    {
        $data = array(
            'invoicedQuantity' => $updateQuantity
        );
        $this->tableGateWay->update($data, array('salesOrderID' => $salesOrder, 'productID' => $productID));
    }

    public function editReceipt(SalesOrderProduct $sor, $soProductID)
    {
        $data = array(
            'soProductDescription' => $sor->soProductDescription,
            'quantity' => $sor->quantity,
            'unitPrice' => $sor->unitPrice,
            'uom' => $sor->uom,
            'discount' => $sor->discount,
            'discountType' => $sor->discountType,
            'total' => $sor->total,
            'tax' => $sor->tax
        );
        $this->tableGateWay->update($data, array('soProductID' => $soProductID));
    }

    public function deleteReceipt($salesorderID)
    {
        $rowset = $this->tableGateWay->delete(array('salesOrderID' => $salesorderID));
    }

    public function getProductBySalesOrderIDAndProductId($salesorderID, $productId)
    {
        $rowset = $this->tableGateWay->select(array('salesOrderID' => $salesorderID, 'productID' => $productId));
        return $rowset->current();
    }

    public function getProductBySalesOrderCodeAndLocationProductId($salesorderCode, $locationProductID)
    {
        $rowset = $this->tableGateWay->select(array('soCode' => $salesorderCode, 'locationProductID' => $locationProductID));
        return $rowset->current();
    }

    public function getProductBySalesOrderIDAndLocationProductId($salesorderID, $locationProductID, $unitPrice)
    {
        $rowset = $this->tableGateWay->select(array('soID' => $salesorderID, 'locationProductID' => $locationProductID, 'unitPrice' => $unitPrice));
        return $rowset->current();
    }

    public function deleteSalesOrderProduct($soProductID)
    {
        $rowset = $this->tableGateWay->delete(array('soProductID' => $soProductID));
        return $rowset;
    }

    public function getSalesOrderProductBysalesOrderID($salesOrderID)
    {
        $sql = new Sql($this->tableGateWay->getAdapter());
        $select = new Select();
        $select->from("salesOrdersProduct")
                ->columns(array("*"))
                ->join("locationProduct", "salesOrdersProduct.locationProductID= locationProduct.locationProductID", array("*"), "left")
                ->join("product", "locationProduct.productID = product.productID", array("*"), "left")
                ->join("salesOrderProductTax", "salesOrdersProduct.soProductID = salesOrderProductTax.soProductID", array("*"), "left")
                ->join("tax", "salesOrderProductTax.soTaxID = tax.id", array("*"), "left")
                ->where(array("salesOrdersProduct.soID" => $salesOrderID));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        if (!$results) {
            return null;
        }
        return $results;
    }

    public function CheckSalesOrderProductByLocationProductID($locationProductID)
    {
        $adapter = $this->tableGateWay->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrdersProduct')
                ->columns(array('*'))
                ->join('salesOrders', 'salesOrders.soID = salesOrdersProduct.soID', array('entityID'), 'left')
                ->join('entity', 'entity.entityID = salesOrders.entityID', array('deleted'), 'left')
                ->where(array('locationProductID' => $locationProductID, 'deleted' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results->current();
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $locationPID
     * @param type $salesOrderID
     * update items which create Sales Invoice using Sales Order
     */
    public function updateCopiedSalesOrderProducts($locationPID, $salesOrderID, $data)
    {

        $this->tableGateWay->update($data, array('locationProductID' => $locationPID, 'soID' => $salesOrderID));
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $salesOrderID
     * @return arraySet
     */
    public function getUnCopiedSalesOrderProducts($salesOrderID)
    {
        $adapter = $this->tableGateWay->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrdersProduct');
        $select->where(array('soID' => $salesOrderID));
        $select->where(array('copied' => 0));
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        $resultArray = [];

        foreach ($results as $result) {
            $resultArray[] = $result;
        }
        return $resultArray;
    }

    public function getSalesOrderProductQtyByProductID($fromDate, $toDate, $productID = null, $locationID = null)
    {
        $adapter = $this->tableGateWay->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrdersProduct')
                ->columns(array('totalQty' => new Expression('SUM(salesOrdersProduct.quantity)'),
                        'totalSold' => new Expression('SUM(salesOrdersProduct.salesOrdersProductCopiedQuantity)')))
                ->join('locationProduct', 'salesOrdersProduct.locationProductID = locationProduct.locationProductID'
                        , array('locationProductID'), 'left')
                ->join('product', 'locationProduct.productID = product.productID', array('productID'), 'left');
        $select->join('entity', 'product.entityID = entity.entityID', array('deleted'));
        $select->join('salesOrders','salesOrdersProduct.soID = salesOrders.soID',array('branch'));
        $select->group(array('product.productID'));
        $select->where(array('salesOrdersProduct.copied' => 0, 'entity.deleted' => 0));
        $select->where->between('salesOrders.expireDate', $fromDate, $toDate);
        $select->where('product.productState','=',1);
        if($productID){
            $select->where(array('product.productID' => $productID));
        }
        if($locationID){
            $select->where->in('salesOrders.branch', $locationID);
        }
                
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return (object) $results;
    }
    public function getSumOfSalesOrderProductBySalesOrderId($salesOrderID)
    {
        $adapter = $this->tableGateWay->getAdapter();
        $sql = new Sql($adapter);
        $select = $sql->select();
        $select->from('salesOrdersProduct')
                ->columns(array('totalValue' => new Expression('SUM(salesOrdersProduct.total)')))
                ->where(array('salesOrdersProduct.soID' => $salesOrderID));
            
        $statement = $sql->prepareStatementForSqlObject($select);
        $results = $statement->execute();
        return  $results;   
    }

}

?>
