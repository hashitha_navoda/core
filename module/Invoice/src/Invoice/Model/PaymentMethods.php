<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file contains Product Model Functions
 */

namespace Invoice\Model;

class PaymentMethods
{

    public $id;
    public $method;

    public function exchangeArray($data)
    {
        $this->id = (!empty($data['id'])) ? $data['id'] : null;
        $this->method = (!empty($data['method'])) ? $data['method'] : null;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

}

