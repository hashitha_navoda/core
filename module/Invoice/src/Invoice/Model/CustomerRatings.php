<?php

namespace Invoice\Model;

use Zend\InputFilter\Factory as InputFactory;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class CustomerRatings
{

    public $customerRatingsId;
    public $customerId;
    public $customerProfileId;
    public $ratingTypesId;
    public $customerRatingsRatingValue;
    protected $inputFiler;

    public function exchangeArray($data)
    {
        $this->customerRatingsId = (!empty($data['customerRatingsId'])) ? $data['customerRatingsId'] : null;
        $this->customerId = (!empty($data['customerId'])) ? $data['customerId'] : 0;
        $this->customerProfileId = (!empty($data['customerProfileId'])) ? $data['customerProfileId'] : 0;
        $this->ratingTypesId = (!empty($data['ratingTypesId'])) ? $data['ratingTypesId'] : 0;
        $this->customerRatingsRatingValue = (!empty($data['customerRatingsRatingValue'])) ? $data['customerRatingsRatingValue'] : 0;
    }

    public function getArrayCopy()
    {
        return get_object_vars($this);
    }

    public function setInputFilter(InputFilterInterface $inputFilter)
    {
        throw new \Exception("Not used");
    }

    public function getInputFilter()
    {
        $inputFilter = new InputFilter();
        $factory = new InputFactory();

        $this->inputFilter = $inputFilter;

        return $this->inputFilter;
    }

}
