<?php

namespace Invoice\Service;

use Core\Service\BaseService;
use Invoice\Model\Customer;
use Invoice\Model\CustomerRatings;
use Invoice\Form\CustomerProfileForm;
use Invoice\Model\CustomerProfile;
use Settings\Model\CustomerLoyalty;

class CustomerService extends BaseService 
{

    public function getActiveCustomerEvents()
    {   
       	$customerEventList = [];
        $customerEventList[''] = '';
        foreach ($this->getModel('CustomerEventTable')->getActiveCutomerEvents() as $customerEvent) {
            $customerEventList[$customerEvent['customerEventID']] = $customerEvent['customerEventName'];
        }

        return $this->returnSuccess(['customerEventList' => $customerEventList]);
    }


    public function getDetailsForCustomerCreation($locationID)
    {
    	$config = $this->getServiceLocator()->get('config');
        $customerTitleArray = $config['personTitle'];
        $countriesArray = $config['all_countries'];

        $customerTitle = [];
        foreach ($customerTitleArray as $key => $value) {
        	if (!empty($key)) {
        		$customerTitle[$key]['customerTitle'] = $value;
        		$customerTitle[$key]['customerTitleValue'] = $key;
        	}
        }

        $countries = [];
        foreach ($countriesArray as $key => $value) {
        	if ($value != false) {
        		$countries[$key]['countryName'] = $value;
        		$countries[$key]['countryValue'] = $key;
        	}
        }

        // Get currencys list
        $allCurrency = $this->getModel('CurrencyTable')->fetchAll();
        $currencyList = array();
        foreach ($allCurrency as $row) {
            $currencyList[$row->currencyID]['currencyName'] = $row->currencyName;
            $currencyList[$row->currencyID]['currencyID'] = $row->currencyID;
        }

        //Get customer category list
        $allCustomerCategory = $this->getModel('CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategoryList = array();
        foreach ($allCustomerCategory as $key => $row) {
            $customerCategoryList[$key]['value'] = $row;
            $customerCategoryList[$key]['key'] = $key;
        }


        // Get payment terms list
        $paymentTerms = $this->getModel('PaymentTermTable')->fetchAll();
        $terms = array();
        foreach ($paymentTerms as $row) {
            $id = (int) $row->paymentTermID;
            $terms[$id]['paymentTermName'] = $row->paymentTermName;
            $terms[$id]['paymentTermID'] = $id;
        }

        // Get Active Customer Evnt for customer creation form
        $customerEvents = $this->getModel('CustomerEventTable')->getActiveCutomerEvents();
        $customerEvenList = [];
        foreach ($customerEvents as $cEvent) {
            $customerEvenList[$cEvent['customerEventID']]['customerEventName'] = $cEvent['customerEventName'];
            $customerEvenList[$cEvent['customerEventID']]['customerEventID'] = $cEvent['customerEventID'];
        }

        // Get Loyalty Card list
        $loyalty = $this->getModel("LoyaltyTable")->getLoyaltyList();
        $loyaltyCardList = [];
        while ($l = $loyalty->current()) {
            $loyaltyCardList[$l->id] = $l;
        }


        $allAccountsDetails = $this->getModel('FinanceAccountsTable')->getAllActiveAccounts();
        $acconts = [];
        foreach ($allAccountsDetails as $acc) {
            $accounts[$acc['financeAccountsID']]['accontName'] = $acc['financeAccountsCode']."_".$acc['financeAccountsName'];
            $accounts[$acc['financeAccountsID']]['accontID'] = $acc['financeAccountsID'];
        }

        $referenceService = $this->getService('ReferenceService');
		$reference = $referenceService->getReferenceNumber('20', $locationID);
		$locationReferenceID = $reference['data']['locationReferenceID'];
		$customerCode = $referenceService->getReferencePrefixNumber($locationReferenceID);


		$glAccountExist = $this->getModel('GlAccountSetupTable')->fetchAll();
        $glAccountSetupData = $glAccountExist->current();

        if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
            $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
        }

        if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
            $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
        }

        if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
            $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
        }

        if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
            $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
        }


        $customerData = $this->getModel('CustomerTable')->getAllCustomerForPos();
        $customerLoyalty = $this->getModel('CustomerLoyaltyTable');
        $customerDataArray = [];
        foreach ($customerData as $value) {
            $customerDataArray[$value['customerID']] = $value;  
            if (!is_null($value['customerLoyaltyCode'])) {
                $loyalty_card = $customerLoyalty->getLoyaltyCardByCardNo($value['customerLoyaltyCode']);
            } else if (!is_null($value['customerTelephoneNumber'])) {
                $loyalty_card = $customerLoyalty->getLoyaltyCardByPhoneNo($value['customerTelephoneNumber']);
            }

            if ($loyalty_card) {
                $loyaltyData = array(
                    'lid' => $loyalty_card['loyaltyID'],
                    'lcode' => $loyalty_card['customerLoyaltyCode'],
                    'lname' => $loyalty_card['loyaltyName'],
                    'lactive_min' => $loyalty_card['loyaltyActiveMinVal'],
                    'lactive_max' => $loyalty_card['loyaltyActiveMaxVal'],
                    'ladd' => $loyalty_card['loyaltyEarningPerPoint'],
                    'lredeem' => $loyalty_card['loyaltyRedeemPerPoint'],
                    'cust_id' => $loyalty_card['customerID'],
                    'cust_lylt_id' => $loyalty_card['customerLoyaltyID'],
                    'cust_name' => $loyalty_card['customerTitle'] . ' ' . ucfirst($loyalty_card['customerName']),
                    'cust_lpoints' => $loyalty_card['customerLoyaltyPoints']
                );

                $customerDataArray[$value['customerID']]['loyaltyData'] = $loyaltyData;
            }
        }

        $data = [
        	'customerTitle' => $customerTitle,
        	'countries' => $countries,
        	'currencyList' => $currencyList,
        	'customerCategoryList' => $customerCategoryList,
        	'terms' => $terms,
        	'customerEvenList' => $customerEvenList,
        	'loyaltyCardList' => $loyaltyCardList,
        	'accounts' => $accounts,
        	'customerCode' => $customerCode,
        	'customerReceviableAccountID' => $customerReceviableAccountID,
        	'customerSalesAccountID'=> $customerSalesAccountID,
        	'customerSalesDiscountAccountID' => $customerSalesDiscountAccountID,
        	'customerAdvancePaymentAccountID' => $customerAdvancePaymentAccountID,
        	'allCustomers' => $customerDataArray
        ];

        return $this->returnSuccess($data);
    }


    public function createCustomer($data)
    {
        $post = $data;
        $customerProfileData = $post['customerProfile'];
        $customerLoyaltyNo = $post['customerLoyaltyNo'];
        $customerLoyaltyCode = $post['customerLoyaltyCode'];
        $customerRatingTypes = $post['customerRatingTypes'];

        unset($post['customerProfile']);
        $customerPrimaryProfileID = $post['customerPrimaryProfileID'];
        unset($post['customerPrimaryProfileID']);

        // let user to save customer without telephone number
        if($data["customerTelephoneNumber"]){
            // check telephone number already exists
            $searchcustByTp = $this->getModel('CustomerTable')->getCustomerDetailsByPrimaryTelephoneNumber($data["customerTelephoneNumber"]);
            if (count($searchcustByTp) > 0) {
            	return $this->returnError('ERR_CUSTAPI_PRI_TP_EXISTS');
            }
        }

        //create customer exchange array
        $customer = new Customer();
        $customer->exchangeArray($post);
        $searchcust = $this->getModel('CustomerTable')->getCustomerByCode($post["customerCode"]);
        //check whether customer Code name alredy exist
        if (isset($searchcust->customerID)) {
        	return $this->returnError('ERR_CUSTAPI_CUST_EXIST');
        }
        //create entity id for customer
        $entityService = $this->getService('EntityService');
        $entityID = $entityService->createEntity($userId)['data']['entityID'];
        $customer->entityID = $entityID;

        // access customer model and call to saveCustomer function to add detailss
        $res = $this->getModel('CustomerTable')->saveCustomer($customer);
        if ($res == FALSE) {
            //if error occurred then transaction shoul rollbcak
            return $this->returnError('ERR_CUSTAPI_CREATE_CUST');
        } 

        $locationID = 1;
        $referenceService = $this->getService('ReferenceService');
		$reference = $referenceService->getReferenceNumber('20', $locationID);
		$locationReferenceID = $reference['data']['locationReferenceID'];
        //save customer rating types
        if (is_array($customerRatingTypes)) {

            foreach ($customerRatingTypes as $crtKey => $crtVal) {
                $cusRatingData = array(
                    'customerId' => $res,
                    'customerProfileId' => null,
                    'ratingTypesId' => $crtKey,
                    'customerRatingsRatingValue' => $crtVal['ratingTypesRatingValue']
                );

                $customerRatings = new CustomerRatings;
                $customerRatings->exchangeArray($cusRatingData);
                $result = $this->getModel('CustomerRatingsTable')->saveCustomerRatingTypes($customerRatings);
            }
        }


        foreach ($customerProfileData as $key => $singleCustomerProfileData) {
            $customerProfileForm = new CustomerProfileForm();
            $customerProfile = new CustomerProfile();
            $customerProfileForm->setInputFilter($customerProfile->getInputFilter());
            $singleCustomerProfileData['customerID'] = $res;
            $customerProfileForm->setData($singleCustomerProfileData);
            if ($customerPrimaryProfileID === $key) {
                $singleCustomerProfileData['isPrimary'] = TRUE;
            } else {
                $singleCustomerProfileData['isPrimary'] = FALSE;
            }
            if ($customerProfileForm->isValid()) {
                $customerProfile->exchangeArray($singleCustomerProfileData);
                $insertedProfileID = $this->getModel('CustomerProfileTable')->saveCustomerProfile($customerProfile);
            }

            if ($insertedProfileID == FALSE) {
                //if error occurred then transaction shoul rollbcak
                return $this->returnError('ERR_CUSTAPI_CREATE_CUST_PROFILE');
            }

            // Create Loyalty card for the customer
            if ($customerLoyaltyNo) {

                $cusLoyaltyData = array(
                    "loyaltyID" => $customerLoyaltyNo,
                    "customerID" => $res,
                    "customerLoyaltyCode" => $customerLoyaltyCode,
                    "customerLoyaltyPoints" => 0,
                    "customerLoyaltyCreateDate" => $this->getGMTDateTime(),
                    "customerLoyaltyStatus" => 1
                );

                $cust_loyalty = new CustomerLoyalty();
                $cust_loyalty->exchangeArray($cusLoyaltyData);
                $this->getModel('CustomerLoyaltyTable')->save($cust_loyalty);
            }

            //save customer profile rating types

            if (is_array($singleCustomerProfileData['customerProfileRatingTypes'])) {

                foreach ($singleCustomerProfileData['customerProfileRatingTypes'] as $crtKey => $crtVal) {
                    $cusProSingleRatingData = array(
                        'customerId' => $res,
                        'customerProfileId' => $insertedProfileID,
                        'ratingTypesId' => $crtKey,
                        'customerRatingsRatingValue' => $crtVal['ratingTypesRatingValue']
                    );

                    $customerRatings = new CustomerRatings;
                    $customerRatings->exchangeArray($cusProSingleRatingData);
                    $result = $this->getModel('CustomerRatingsTable')->saveCustomerRatingTypes($customerRatings);
                }
            }
        }

        if ($post['customerCode'] == $referenceService->getReferencePrefixNumber($locationReferenceID)) {
            $referenceService->updateReferenceNumber($locationReferenceID);
        }

        return $this->returnSuccess(['customerID' => $res],'SUCC_CUSTAPI_ADD_CUST');
    }

    public function getAllCustomersDetails($data) {
        $customerID =($data['customerID'] == "") ? null : $data['customerID'];
        $customers = $this->getModel('CustomerTable')->fetchAllCustomerDetails($customerID);
        $customerDetailsArr = [];
        $mobileNumbers = [];
        $emails = [];
        $userNames = [];
        foreach ($customers as $customer) {
            if($customer['customerID'] != '0'){
                array_push($userNames,['key'=> $customer['customerID'],'value'=> $customer['customerName']."-".$customer['customerCode']]);
                array_push($mobileNumbers,['key'=> $customer['customerID'],'value'=> $customer['customerTelephoneNumber']]);
                if ($customer['customerProfileEmail']) {
                    array_push($emails,['key'=> $customer['customerID'],'value'=> $customer['customerProfileEmail']]);
                }
            }   
        }
        $customerDetailsArr['mobileNumbersArr'] = $mobileNumbers;
        $customerDetailsArr['emailsArr'] = $emails;
        $customerDetailsArr['userNameArr'] = $userNames;
        return $customerDetailsArr;
    }

    public function getCustomerDetails($data) {
        $customerID =($data['customerID'] == "") ? null : $data['customerID'];
        $customers = $this->getModel('CustomerTable')->getCustomerDetails($data);
        $customerDetailsArr = [];
        foreach ($customers as $customer) {
            $customerDetailsArr['customerID'] = $customer['customerID'];
            $customerDetailsArr['customerName'] = $customer['customerName'];
            $customerDetailsArr['customerCode'] = $customer['customerCode'];
            $customerDetailsArr['customerAddress'] = $customer['customerProfileLocationNo'];
            $customerDetailsArr['customerEmail'] = $customer['customerProfileEmail'];
            $customerDetailsArr['customerTelephoneNumber'] = $customer['customerTelephoneNumber'];
        }
        return $customerDetailsArr;
    }
}

   

