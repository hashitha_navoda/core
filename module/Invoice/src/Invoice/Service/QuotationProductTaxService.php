<?php

namespace Invoice\Service;

use Core\Service\BaseService;
use Invoice\Model\QuotationProductTax;

class QuotationProductTaxService extends BaseService
{

    public function saveQuotationProductTaxes($quoProductTax, $quotationID, $quoProductID, $customCurrencyRate)
    {
    	 foreach ($quoProductTax as $txID => $txd) {
            $taxData = array(
                'quotID' => $quotationID,
               	'quotProductID' => $quoProductID,
                'quotTaxID' => $txID,
                'quotTaxPrecentage' => $txd["tP"],
                'quotTaxAmount' => $txd["tA"] * $customCurrencyRate
            );
            $quotProTax = new QuotationProductTax();
            $quotProTax->exchangeArray($taxData);
            $res = $this->getModel('QuotationProductTaxTable')->saveQuotProductTax($quotProTax);
            //check whether quotationProductsTax data save or not
            if(!$res){
                return $this->returnError('ERR_QUOTA_PRODUCT_TAX_SAVE');
            }
        }
        return $this->returnSuccess();
    }

}






