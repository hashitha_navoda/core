<?php

namespace Invoice\Service;

use Core\Service\BaseService;
use Invoice\Model\QuotationProduct;

class QuotationProductService extends BaseService 
{

    public function saveQuotationProduct($data)
    {
        $quotationProduct = new QuotationProduct();
        $quotationProduct->exchangeArray($data);
        $quotationProductId = $this->getModel('QuotationProductTable')->saveReceipt($quotationProduct);
        
        if (!$quotationProductId) {
            return $this->returnError('ERR_QUOTA_PRODUCT_SAVE');
        }
        
        return $this->returnSuccess(['quotationProductID' => $quotationProductId]);
    }

}






