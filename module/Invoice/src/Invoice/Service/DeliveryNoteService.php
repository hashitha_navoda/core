<?php

namespace Invoice\Service;

use Core\Service\BaseService;
use Core\Contracts\SearchableFromCode;
use Invoice\Model\DeliveryNote;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Support\Collection;

class DeliveryNoteService extends BaseService implements SearchableFromCode
{

    public function codeExists($code, $locationId)
    {
        //get quotation by quotation code and location ID
        $res = $this->getModel('DeliveryNoteTable')
            ->checkDeliveryNoteByCode($code)
            ->current();

        return $this->returnSuccess(['exists' => (bool) $res]);
    }
}
