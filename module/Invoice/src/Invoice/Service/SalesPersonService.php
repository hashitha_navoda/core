<?php

namespace Invoice\Service;

use Core\Service\BaseService;

class SalesPersonService extends BaseService 
{

    public function getSalesPersons()
    {   
    	$salesPersons = [];
        foreach ($this->getModel('SalesPersonTable')->fetchAll() as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        return $this->returnSuccess(['salesPersons' => $salesPersons]);
    }

}
