<?php

namespace Invoice\Service;

use Core\Contracts\SearchableFromCode;
use Invoice\Model\ServiceChargeType;
use Core\Service\BaseService;
use Illuminate\Support\Collection;
use Illuminate\Validation\Factory as Validator;


class ServiceChargeTypeService extends BaseService
{
	
	public function saveServiceChargeType($data, $locationID)
	{

		// first need to check cost type code exist or not
		$codeData = $this->getModel('ServiceChargeTypeTable')->getServiceChargeTypeDetailsByCode($data['srcCode'])->current();
        if ($codeData != null) {
        	return [
        		'status' => false,
        		'msg' => 'ERR_SRC_TYPE_CODE_USE'
        	];
        }
        
        // save Cost Type
        $srcTypeArray = [
        	'serviceChargeTypeName' => $data['srcName'],
        	'serviceChargeTypeCode' => $data['srcCode'], 
            'serviceChargeTypeGLAccountID' => $data['serviceChargeTypeAccountID'],
            'locationID' => $locationID 
        ];
        $srcType = new ServiceChargeType();
        $srcType->exchangeArray($srcTypeArray);

        $saveSrcType = $this->getModel('ServiceChargeTypeTable')->save($srcType);
        if ($saveSrcType == null) {
        	return [
        		'status' => false,
        		'msg' => 'ERR_SRC_TYPE_SAVE'
        	];	
        }

        return [
    		'status' => true,
    		'msg' => 'SUCC_SAVE_SRC_TYPE'
    	];
	}

    public function deleteServiceChargeType($data)
    {

        $relatedInvoice = $this->getModel('InvoiceServiceChargeTable')->getInvoiceChargesByServiceChargeTypeID($data['srcTypeID']);
        if (sizeof($relatedInvoice) > 0) {
            return [
                'status' => false,
                'msg' => 'ERR_HAS_LINKED_INV'
            ];  
        } 

        $updatedData = [
            'serviceChargeTypeIsDelete' => 1
        ];
        $deleteCostType = $this->getModel('ServiceChargeTypeTable')->update($updatedData, $data['srcTypeID']);
        if (!$deleteCostType) {
            return [
                'status' => false,
                'msg' => 'ERR_SRC_TYPE_DELETE'
            ];   
        }
        return [
            'status' => true,
            'msg' => 'SUCC_SRC_TYPE_DELETE'
        ];
    }

    public function searchServiceChargeType($data)
    {
        $paginated = false;
        if (!empty($data['searchKey'])) {
            $srcTypeList = $this->getModel('ServiceChargeTypeTable')->ServiceChargeTypeSearchByKey($data['searchKey']);
            $status = true;
        } else {
            $status = true;
            // $serviceChargeT = $this->getServiceLocator()->get("ServiceChargeController");
            // $srcTypeList =  $serviceChargeT->getPaginatedCostTypes(6, true);
            $srcTypeList = $this->getPaginatedSRCTypes(6, true);
            $paginated = true;
        }
        return [
            'status' => $status,
            'srcTypeList' => $srcTypeList,
            'paginated' => $paginated
        ];
        
    }

    public function getPaginatedSRCTypes($perPage = 6, $fromSearch = false)
    {
        $this->paginator = $this->getModel('ServiceChargeTypeTable')->fetchAll(true);
        if($fromSearch) {
            $this->paginator->setCurrentPageNumber(1);
        } else {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        }
        $this->paginator->setItemCountPerPage($perPage);
        
        return $this->paginator;
    }

    public function searchServiceChargesForDropdown($data)
    {

        $searchKey = $data['searchKey'];
       
        $serviceCharges = $this->getModel('ServiceChargeTypeTable')->ServiceChargeTypeSearchByKey($searchKey);
        $serviceChargesList = array();
        foreach ($serviceCharges as $src) {
            $temp['value'] = $src['serviceChargeTypeID'];
            $temp['text'] = $src['serviceChargeTypeName'] . '-' . $src['serviceChargeTypeCode'];
            $serviceChargesList[] = $temp;
        }

        return $serviceChargesList;
    }

}