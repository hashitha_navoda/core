<?php

namespace Invoice\Service;

use Core\Service\BaseService;
use Core\Contracts\SearchableFromCode;
use Invoice\Model\Quotation;
use Invoice\Model\CustomerOrder;
use Invoice\Model\QuotationProduct;
use Illuminate\Validation\Factory as Validator;
use Illuminate\Support\Collection;

class CustomerOrderService extends BaseService implements SearchableFromCode
{

    /* @example
     * {
     *      quotationCustomerName: 'John Doe',
     *      customerID: 1,
     *      quotationPayementTerm: 1,
     *      quotationIssuedDate: '2016.01.27',
     *      quotationExpireDate: '2016.01.27',
     *      quotationCode: 'QUT_001',
     *      quotationSalesPersonID: 1,
     *      quotationComment: 'Sample comment',
     *      quotationTaxAmount: 1000,
     *      quotationTotalAmount: 1000,
     *      quotationTotalDiscount: 100,
     *      products: [
     *          {
     *              quotationProductCode: SGR0001,
     *              quotationProductName: Sugar,
     *              quotationProductQuantity: 1000.000,
     *              quotationProductDiscount: 20,
     *              quotationProductDiscountType: vl, # vl | pr
     *              quotationProductUnitPrice: 200,
     *              uom: 2,
     *              quotationProductTotal: 180000,
     *              productType: 1,
     *              taxDetails: [
     *                  tTA : 100,
     *                  tL  : [
     *                      [
     *                          tN : 'VAT',
     *                          tP : 10,
     *                          tA : 100,
     *                          susTax : 0 | 1
     *                      ]
     *                  ]
     *              ]
     *           }
     *      ],
     *      productTaxes: [[1, 2], [1, 2]],
     *      quotationShowTax: 0,
     *      jobRefType: 1 | 2 | 3,
     *      jobRefTypeID: 1,
     *      priceListId: 2,
     *      customerProfileID: 3,
     *      customCurrencyId: 4,
     *      companyCurrencySymbol: 'Rs',
     *      customCurrencyRate: 100.00,
     *      userID: 1,
     *      locationID: 1
     * }
     */
    public function createCustomerOrder($data)
    {

        $referenceService = $this->getService('ReferenceService');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $result = $this->getReferenceNoForLocation('47', $locationID);
        $locationReferenceID = $result['locRefID'];

        $customerOrderCode = $data['cus_order_no'];
        //check credit note numer already exist in credit note table if exist give next number for credit note code
        while ($customerOrderCode) {
            if ($this->getModel('CustomerOrderTable')->checkCustomerOrderByCode($customerOrderCode)->current()) {
                if ($locationReferenceID) {
                    $newCustomerOrderCode = $this->getReferenceNumber($locationReferenceID);
                    if ($newCustomerOrderCode == $customerOrderCode) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $customerOrderCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $customerOrderCode = $newCustomerOrderCode;
                    }
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CREDIT_CODE_EXIST');
                    return $this->JSONRespond();
                }
            } else {
                break;
            }
        }

        $customerOrderDate = $this->convertDateToStandardFormat($data['cus_order_date']);
        $customerOrderData = array(
            'customerID' => $data['cust_id'],
            'customerOrderCode' => $customerOrderCode,
            'customerOrderDate' => $customerOrderDate,
            'customerOrderAmount' => (!empty($data['cus_order_amount'])) ? $data['cus_order_amount'] : null,
            'customerOrderComment' => (!empty($data['cmnt'])) ? $data['cmnt'] : null,
            'customerOrderReference' => (!empty($data['reference'])) ? $data['reference'] : null,
            'customerOrderStatus' => 3,
            'locationId' => $this->user_session->userActiveLocation["locationID"],
        );

        $cusOrderData = new CustomerOrder();
        $cusOrderData->exchangeArray($customerOrderData);



        $entityService = $this->getService('EntityService');     
        $entityID = $entityService->createEntity($this->user_session->userID)['data']['entityID'];

        $cusOrderData->entityID = $entityID;


        $res = $this->getModel('CustomerOrderTable')->saveCustomerOrder($cusOrderData);
        
        if (!$res) {
            return $this->returnError('ERR_CUS_ORDER_CREATE');
        }

        $updateRef = $referenceService->updateReferenceNumber($locationReferenceID);
        if ($updateRef['status'] == false) {
            return $this->returnError('ERR_WHEN_UPDATE_CUS_ORDER_CODE');
        }

        return $this->returnSuccess(['customerOrderCode' => $customerOrderCode, 'customerOrderID' => $res], 'SUCC_CUS_ORDER_CREATE');
    }

    public function updateCustomerOrder($data)
    {

        $referenceService = $this->getService('ReferenceService');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $result = $this->getReferenceNoForLocation('47', $locationID);
        $locationReferenceID = $result['locRefID'];

        $customerOrderCode = $data['cus_order_no'];
    
        $customerOrderDate = $this->convertDateToStandardFormat($data['cus_order_date']);
        $customerOrderData = array(
            'customerID' => $data['cust_id'],
            'customerOrderID' => $data['cust_order_id'],
            'customerOrderCode' => $customerOrderCode,
            'customerOrderDate' => $customerOrderDate,
            'customerOrderAmount' => (!empty($data['cus_order_amount'])) ? $data['cus_order_amount'] : null,
            'customerOrderComment' => (!empty($data['cmnt'])) ? $data['cmnt'] : null,
            'customerOrderReference' => (!empty($data['reference'])) ? $data['reference'] : null,
            'customerOrderStatus' => 3,
            'locationId' => $this->user_session->userActiveLocation["locationID"],
        );

        $cusOrderData = new CustomerOrder();
        $cusOrderData->exchangeArray($customerOrderData);

        $res = $this->getModel('CustomerOrderTable')->updateCustomerOrder($cusOrderData);
        
        if (!$res) {
            return $this->returnError('ERR_CUS_ORDER_UPDATE');
        }

        return $this->returnSuccess(['customerOrderCode' => $customerOrderCode, 'customerOrderID' => $res], 'SUCC_CUS_ORDER_UPDATE');
    }

    public function getCustomerOrdersForDropdown($searchKey, $status = false)
    {
        $quotaions = $this->getModel('CustomerOrderTable')->searchCustomerOrderForDropDown($searchKey, $status);
        foreach ($quotaions as $quotaion) {
            $temp['value'] = $quotaion['customerOrderID'];
            $temp['text'] = $quotaion['customerOrderCode'];
            $quotaionList[] = $temp;
        }
        return ($quotaionList) ? $quotaionList : array();
    }

    public function searchAllCustomerOrderByRefNoForDropdown($locationID, $searchKey, $status = false)
    {
        $quotaions = $this->getModel('CustomerOrderTable')->searchAllCustomerOrderByRefNoForDropdown($locationID, $searchKey, $status);
        foreach ($quotaions as $quotaion) {
            $temp['value'] = $quotaion['customerOrderID'];
            $temp['text'] = $quotaion['customerOrderCode'].' - '.$quotaion['customerOrderReference'];
            $quotaionList[] = $temp;
        }
        return ($quotaionList) ? $quotaionList : array();
    }

    public function retriveCustomerOrder($customerOrderID)
    {
        $cusOrder = $this->getModel('CustomerOrderTable')->retriveCustomerOrder($customerOrderID)->current();



        $uploadedAttachments = [];
        $attachmentData = $this->getModel('DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($customerOrderID, 46);
        $companyName = $this->getSubdomain();
        $path = '/userfiles/' . md5($companyName) . "/attachments";

        foreach ($attachmentData as $value) {
            $temp = [];
            $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
            $temp['documentRealName'] = $value['documentRealName'];
            $temp['docLink'] = $path . "/" .$value['documentName'];
            $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
        }

        if (!$cusOrder) {
            return $this->returnError('ERR_WHEN_RETRIVE_CUS_ORDER');
        }

        $cusOrder['uploadedAttachments'] = $uploadedAttachments;

        return $this->returnSuccess($cusOrder, 'SUCC_CUS_ORDER_RETRIVE');
    }



    public function codeExists($code, $locationId)
    {   
        //get quotation by quotation code and location ID
        $res = $this->getModel('QuotationTable')
            ->checkQuotationByCode($code, $locationId)
            ->current();

        return $this->returnSuccess(['exists' => (bool) $res]);
    }

    public function saveQuotation($data)
    {
        $quotation = new quotation();
        $quotation->exchangeArray($data);

        $res = $this->getModel('QuotationTable')->saveQuotation($quotation);
        
        if (!$res) {
            return $this->returnError('ERR_QUOTA_CREATE');
        }
        
        return $this->returnSuccess(['quotationID' => $res]);
    }

    public function saveQuotationProductTaxes($data)
    {
        $quotationProduct = new QuotationProduct();
        $quotationProduct->exchangeArray($data);
        $quotationProductId = $this->getModel('QuotationProductTable')->saveReceipt($quotationProduct);
        
        if (!$quotationProductId) {
            return $this->returnError('ERR_QUOTA_CREATE');
        }
        
        return $this->returnSuccess(['quotationProductID' => $quotationProductId]);
    }

}






