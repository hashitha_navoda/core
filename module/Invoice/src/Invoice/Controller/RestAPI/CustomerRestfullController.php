<?php

namespace Invoice\Controller\RestAPI;

use Core\Controller\RestAPI\CoreRestfullController;

/**
 *
 * CustomerRestfullController
 *
 *
 * Restfull controller for products
 */
class CustomerRestfullController extends CoreRestfullController
{
    private $service;

    function __construct()
    {
    }

    public function getList()
    {
        $request = $this->getRequest();
        $locationID = $this->getRequest()->getHeaders()->get("Locationid")->getFieldValue();
        $this->service = $this->getService('CustomerService');

        $customerCreateDetails = $this->service->getDetailsForCustomerCreation($locationID);

        if ($customerCreateDetails['status']) {
            return $this->returnJsonSuccess($customerCreateDetails['data'], $customerCreateDetails['msg']);
        }

        return $this->returnJsonError($customerCreateDetails['msg']);
    }

    public function get($id)
    {
        return new JsonModel(['status' => true, 'data' => null, 'msg' => 'successfuly done']);
    }

    public function create($data)
    {
        $this->service = $this->getService('CustomerService');

        //customer add transaction begin
        $this->beginTransaction();
        $customerCreateResult = $this->service->createCustomer($data);

        if ($customerCreateResult['status']) {
            $this->commit();
            return $this->returnJsonSuccess($customerCreateResult['data'], $this->getMessage($customerCreateResult['msg']));
        }

        $this->rollback();
        return $this->returnJsonError($this->getMessage($customerCreateResult['msg']));
    }

    public function update($id, $data)
    {
        # code...
    }

    public function delete($id)
    {
        # code...
    }

}
