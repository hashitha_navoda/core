<?php

/**
 * @author Damith Thamara <damith@thinkcube.com>
 * This file contains CustomerAPI related controller functions
 */

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Form\AddCustomerForm;
use Invoice\Model\Customer;
use Zend\View\Model\JsonModel;
use Zend\Session\Container;
use Invoice\Form\CustomerCategoryForm;
use Invoice\Form\CustomerProfileForm;
use Invoice\Model\CustomerCategory;
use Invoice\Model\CustomerProfile;
use Settings\Model\CustomerLoyalty;
use Invoice\Model\CustomerRatings;
use Inventory\Model\Product;
use Inventory\Form\ProductForm;
use Inventory\Model\ProductUom;
use Inventory\Model\ProductHandeling;
use Inventory\Model\LocationProduct;
use Invoice\Model\Contacts;
use Invoice\Model\ContactMobileNumbers;
use Invoice\Model\ContactEmails;

class CustomerAPIController extends CoreController
{

    protected $userID;
    protected $username;
    protected $user_session;
    public $data;
    public $status;
    public $msg;
    public $custad;

    public function __construct()
    {
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
    }

    //get customer details by customer ID
    public function getcustomerByIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $re = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($request->getPost('customerID'));
            $customerProfileData = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerID($request->getPost('customerID'));
            $selectedCustomerProfileData = array();
            foreach ($customerProfileData as $data) {
                $selectedCustomerProfileData[$data['customerProfileID']] = $data;
            }

            if ($re) {
                $this->data = array('customerData' => $re, 'customerProfiles' => $selectedCustomerProfileData);
                $this->status = true;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUSTAPI_INVALID_CUSTID');
            }
            return $this->JSONRespond();
        }
    }

    //add customer to database
    public function addAction()
    {
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        foreach ($paymentTerms as $row) {
            $id = (int) $row->paymentTermID;
            $terms[$id] = $row->paymentTermName;
        }
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $dd = reset($post['customerProfile']);
            $customerProfileData = $post['customerProfile'];
            $cusEmail = (!empty($dd['customerProfileEmail'])) ? $dd['customerProfileEmail'] : null;
            $customerLoyaltyNo = $post['customerLoyaltyNo'];
            $customerLoyaltyCode = $post['customerLoyaltyCode'];
            $customerRatingTypes = $post['customerRatingTypes'];

            unset($post['customerProfile']);
            $customerPrimaryProfileID = $post['customerPrimaryProfileID'];
            unset($post['customerPrimaryProfileID']);

            // let user to save customer without telephone number
            if($request->getPost("customerTelephoneNumber")){
                // check telephone number already exists
                $searchcustByTp = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerDetailsByPrimaryTelephoneNumber($request->getPost("customerTelephoneNumber"));
                if (count($searchcustByTp) > 0) {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTAPI_PRI_TP_EXISTS');
                    return $this->JSONRespond();
                }
            }

            //customer add transaction begin
            $this->beginTransaction();

            //create customer exchange array
            $customer = new Customer();
            $customer->exchangeArray($request->getPost());
            $searchcust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCode($request->getPost("customerCode"));
            //check whether customer Code name alredy exist
            if (!isset($searchcust->customerID)) {
                //create entity id for customer
                $entityID = $this->createEntity();
                $customer->entityID = $entityID;
                //access customer model and call to saveCustomer function to add detailss
                $res = $this->CommonTable('Invoice\Model\CustomerTable')->saveCustomer($customer);
                if ($res == FALSE) {
                    //if error occurred then transaction shoul rollbcak
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CREATE_CUST');
                } else {
                    $locationID = $this->user_session->userActiveLocation['locationID'];
                    $refData = $this->getReferenceNoForLocation(20, $locationID);
                    $rid = $refData["refNo"];
                    $locationReferenceID = $refData["locRefID"];

                    //save customer rating types

                    if (is_array($customerRatingTypes)) {

                        foreach ($customerRatingTypes as $crtKey => $crtVal) {
                            $data = array(
                                'customerId' => $res,
                                'customerProfileId' => null,
                                'ratingTypesId' => $crtKey,
                                'customerRatingsRatingValue' => $crtVal['ratingTypesRatingValue']
                            );

                            $customerRatings = new CustomerRatings;
                            $customerRatings->exchangeArray($data);
                            $result = $this->CommonTable('Invoice\Model\CustomerRatingsTable')->saveCustomerRatingTypes($customerRatings);
                        }
                    }


                    foreach ($customerProfileData as $key => $singleCustomerProfileData) {
                        $customerProfileForm = new CustomerProfileForm();
                        $customerProfile = new CustomerProfile();
                        $customerProfileForm->setInputFilter($customerProfile->getInputFilter());
                        $singleCustomerProfileData['customerID'] = $res;
                        $customerProfileForm->setData($singleCustomerProfileData);
                        if ($customerPrimaryProfileID === $key) {
                            $singleCustomerProfileData['isPrimary'] = TRUE;
                        } else {
                            $singleCustomerProfileData['isPrimary'] = FALSE;
                        }
                        if ($customerProfileForm->isValid()) {
                            $customerProfile->exchangeArray($singleCustomerProfileData);
                            $insertedProfileID = $this->CommonTable('Invoice\Model\CustomerProfileTable')->saveCustomerProfile($customerProfile);
                        }

                        if ($insertedProfileID == FALSE) {
                            //if error occurred then transaction shoul rollbcak
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_CUSTAPI_CREATE_CUST_PROFILE');
                            return $this->JSONRespond();
                        }

                        // Create Loyalty card for the customer
                        if ($customerLoyaltyNo) {

                            $data = array(
                                "loyaltyID" => $customerLoyaltyNo,
                                "customerID" => $res,
                                "customerLoyaltyCode" => $customerLoyaltyCode,
                                "customerLoyaltyPoints" => 0,
                                "customerLoyaltyCreateDate" => $this->getGMTDateTime(),
                                "customerLoyaltyStatus" => 1
                            );

                            $cust_loyalty = new CustomerLoyalty();
                            $cust_loyalty->exchangeArray($data);
                            $this->CommonTable('CustomerLoyaltyTable')->save($cust_loyalty);
                        }

                        //save customer profile rating types

                        if (is_array($singleCustomerProfileData['customerProfileRatingTypes'])) {

                            foreach ($singleCustomerProfileData['customerProfileRatingTypes'] as $crtKey => $crtVal) {
                                $data = array(
                                    'customerId' => $res,
                                    'customerProfileId' => $insertedProfileID,
                                    'ratingTypesId' => $crtKey,
                                    'customerRatingsRatingValue' => $crtVal['ratingTypesRatingValue']
                                );

                                $customerRatings = new CustomerRatings;
                                $customerRatings->exchangeArray($data);
                                $result = $this->CommonTable('Invoice\Model\CustomerRatingsTable')->saveCustomerRatingTypes($customerRatings);
                            }
                        }
                    }

                    //add curomer to contacts

                    $contactData = [
                        'firstName' => $post['customerName'],
                        'isConvertToCustomer' => 1,
                        'title' => $post['customerTitle'],
                        'relatedCusID' => $res
                    ];


                    $mobileNumberDetails = [
                        [
                            'numberTypeId' => 1,
                            'number' => $post['customerTelephoneNumber']
                        ]

                    ];


                    $emailDetails = [];
                    if (!empty($cusEmail)) {
                        $emailDetails = [
                            [
                                'emailTypeId' => 1,
                                'address' => $cusEmail
                            ]

                        ];

                    }

                    $contact = new Contacts();
                    $contact->exchangeArray($contactData);
                    $contact->entityID = $this->createEntity();

                    $contactId = $this->CommonTable('Invoice\Model\ContactsTable')->saveContact($contact);

                    if (!$contactId) {
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }


                    if (sizeof($mobileNumberDetails) > 0) {
                        foreach ($mobileNumberDetails as $key1 => $value1) {
                            $temp = [
                                'mobileNumber' => $value1['number'],
                                "contactID" => $contactId,
                                "mobileNumberType" => $value1['numberTypeId']
                            ];

                            $contactNumber = new ContactMobileNumbers();
                            $contactNumber->exchangeArray($temp);

                            $contactNumID = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->saveContactMobileNumber($contactNumber);


                            if (!$contactNumID) {
                                $this->rollback();
                                $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                                $this->status  = false; 
                                return $this->JSONRespond();
                            }
                        }
                    }


                    if (sizeof($emailDetails) > 0) {
                        foreach ($emailDetails as $key2 => $value2) {
                            $temp = [
                                'emailAddress' => $value2['address'],
                                "contactID" => $contactId,
                                "emailType" => $value2['emailTypeId']
                            ];

                            $contactEmail = new ContactEmails();
                            $contactEmail->exchangeArray($temp);

                            $contactEmailID = $this->CommonTable('Invoice\Model\ContactEmailsTable')->saveContactEmail($contactEmail);

                            if (!$contactEmailID) {
                                $this->rollback();
                                $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                                $this->status  = false; 
                                return $this->JSONRespond();
                            }
                        }
                    }


                    //when call this transaction happen real
                    $this->commit();
                    $this->setLogMessage('Customer '.$post['customerCode']." is created");
                    if ($post['customerCode'] == $this->getReferenceNumber($locationReferenceID)) {
                        $this->updateReferenceNumber($locationReferenceID);
                    }
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_CUSTAPI_ADD_CUST');
//                    $this->flashMessenger()->addMessage($this->getMessage('SUCC_CUSTAPI_ADD_CUST'));

                    $this->data = ['customerID' => $res, 'newCustomerCode' => $this->getReferenceNumber($locationReferenceID)];
                }
            } else {
                //if error occurred then transaction shoul rollbcak
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_EXIST');
            }
            return $this->JSONRespond();
        }
    }

    //get customer details by customer short name
    public function getcustomerAction()
    {
        $getcusrequest = $this->getRequest();
        $customer = NULL;

        //check whether request is post request
        if ($getcusrequest->isPost()) {
            /*
             * @auther Sandun Dissanayake
             * following function has changed as getCustomerByID(customerID)
             * because of added boostrap select
             */
            $checkcustomer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($getcusrequest->getPost('customerID'));
            //check wheather customerID isset
            if (isset($checkcustomer->customerID)) {
                $currencyID = $checkcustomer->customerCurrency;
                $customer[] = $checkcustomer;
                $customer[0]->customerCurrencyID = $currencyID;
                //get customer currency name
                $currency = $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customer[0]->customerCurrency);
                //if customer currency isset then add it to customer array otherwise add null
                $customer[0]->customerCurrency = (isset($currency->currencyName)) ? $currency->currencyName : '';
                $this->data = $customer;
                $this->status = true;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUSTAPI_INVALID_CUST');
            }
            return $this->JSONRespond();
        }
    }

    //update customer
    public function updatecustomerAction()
    {
        //get payment terms list
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();

        //pust payment terms into array
        foreach ($paymentTerms as $row) {
            $id = (int) $row->paymentTermID;
            $terms[$id] = $row->paymentTermName;
        }

        $request = $this->getRequest();
        //check whether request is post
        if ($request->isPost()) {
//          check telephone number already exists
            $searchcustByTp = $this->CommonTable('Invoice\Model\CustomerTable')
                    ->getCustomerDetailsByPrimaryTelephoneNumber($request->getPost("customerTelephoneNumber"))
                    ->current();
            if ($searchcustByTp['customerID'] && ($searchcustByTp['customerID'] != $request->getPost('customerID'))) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUSTAPI_PRI_TP_EXISTS');
                return $this->JSONRespond();
            }
            $customerRatingTypes = $request->getPost('customerRatingTypes');
            //customer update transaction begin
            $this->beginTransaction();

            //add customer data to exchange array
            $customer = new Customer();
            $customer->exchangeArray($request->getPost());
            $customerID = $request->getPost('customerID');
            //get customer entityID
            $customerResult = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
            if (isset($customerResult->customerID)) {
//                check already customer code used to other customer
                if ($customerResult->customerCode !== $request->getPost('customerCode')) {
                    $customerCodeResult = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCode($request->getPost('customerCode'));
                    if (isset($customerCodeResult->customerCode)) {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CODE_EXIST');
                        return $this->JSONRespond();
                    }
                }
                $entityID = (isset($customerResult->entityID)) ? $customerResult->entityID : '';

                //if entity id isset then update this entity id
                if ($entityID) {
                    $this->updateEntity($entityID);
                }

                $deleteData = $this->CommonTable('Invoice\Model\CustomerRatingsTable')->getCustomerRatingsByCustomerId($customerID);
                foreach ($deleteData as $val) {
                    $val = (object) $val;
                    $result = $this->CommonTable('Invoice\Model\CustomerRatingsTable')->deleteCustomerRatings($val->customerRatingsId);
                    if (!$result) {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_OCC_UPDATE_CUST_RATINGS');
                        return $this->JSONRespond();
                    }
                }

//save customer rating types
                if (count($customerRatingTypes) > 0) {
                    foreach ($customerRatingTypes as $crtKey => $crtVal) {
                        $data = array(
                            'customerRatingsId' => $result->customerRatingsId,
                            'customerId' => $customerID,
                            'customerProfileId' => null,
                            'ratingTypesId' => $crtKey,
                            'customerRatingsRatingValue' => $crtVal['ratingTypesRatingValue']
                        );
                        $customerRatings = new CustomerRatings;
                        $customerRatings->exchangeArray($data);
                        $result = $this->CommonTable('Invoice\Model\CustomerRatingsTable')->saveCustomerRatingTypes($customerRatings);
                    }
                }

                //update the customer data
                $res = $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomer($customer);
                $cust_id = $customer->customerID;
                $loyalty_id = $request->getPost('customerLoyaltyNo');
                $loyalty_code = $request->getPost('customerLoyaltyCode');
                $date = $this->getGMTDateTime();
                $this->CommonTable('CustomerLoyaltyTable')->updateLoyaltyCard($cust_id, $loyalty_id, $loyalty_code, $date);

                if ($res == FALSE) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_UPDATE');
                } else {
                    $this->_updateCustomerProfile($request->getPost('customerProfile'), $customerID, $request->getPost('customerPrimaryProfileID'));
                    $changeArray = $this->getCustomerChangeArray($customerResult, $request->getPost());
                    //set log details
                    $previousData = json_encode($changeArray['previousData']);
                    $newData = json_encode($changeArray['newData']);

                    //update contact
                    $res = $this->CommonTable('Invoice\Model\ContactsTable')->getCotactDetailsCustomerId($customerID)->current();

                    if ($res) {
                        $contactData = [
                            'firstName' => $customer->customerName,
                            'isConvertToCustomer' => 1,
                            'title' => $customer->customerTitle,
                            'designation' => $res['designation'],
                        ];

                        $update = $this->CommonTable('Invoice\Model\ContactsTable')->updateContact($contactData, $res['contactID']);

                        if (!$update) {
                            $this->rollback();
                            $this->msg  = $this->getMessage('ERR_CONTACT_UPDATE'); 
                            $this->status  = false; 
                            return $this->JSONRespond();
                        }
                        $deleteMobNumbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->deleteContactMobileNumbers($res['contactID']);
                        $mobileNumberDetails = [
                            [
                                'numberTypeId' => 1,
                                'number' => $customer->customerTelephoneNumber
                            ]

                        ];

                        foreach ($mobileNumberDetails as $key1 => $value1) {
                            $temp = [
                                'mobileNumber' => $value1['number'],
                                "contactID" => $res['contactID'],
                                "mobileNumberType" => $value1['numberTypeId']
                            ];

                            $contactNumber = new ContactMobileNumbers();
                            $contactNumber->exchangeArray($temp);

                            $contactNumID = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->saveContactMobileNumber($contactNumber);


                            if (!$contactNumID) {
                                $this->rollback();
                                $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                                $this->status  = false; 
                                return $this->JSONRespond();
                            }
                        }


                        // $emailDetails = [];
                        // if (!empty($cusEmail)) {
                        //     $emailDetails = [
                        //         [
                        //             'emailTypeId' => 1,
                        //             'address' => $cusEmail
                        //         ]

                        //     ];

                        // }
                    }
                    $this->commit();
                    $this->setLogDetailsArray($previousData,$newData);
                    $this->setLogMessage("Customer ".$changeArray['previousData']['Customer Code']." is updated.");
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_CUSTAPI_CUST_UPDATE');
                    $this->data = TRUE;
                }
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_SNAME_EXIST');
            }
        } else {
            $this->rollback();
            $this->status = true;
            $this->msg = $this->getMessage('ERR_CUSTAPI_WRONG_CUSID');
            $this->data = TRUE;
        }
        return $this->JSONRespond();
    }

    public function getCustomerChangeArray($oldData, $newwData)
    {
        $previousData = [];
        $previousData['Customer Name'] = $oldData->customerName;
        $previousData['Customer Code'] = $oldData->customerCode;
        $previousData['Customer Short Name'] = $oldData->customerShortName;
        $previousData['Customer Identity Number'] = $oldData->customerIdentityNumber;
        $previousData['Customer Telephone Number'] = $oldData->customerTelephoneNumber;
        $previousData['Customer Vat Number'] = $oldData->customerVatNumber;
        $previousData['Customer SVat Number'] = $oldData->customerSVatNumber;
        $previousData['Customer Date Of Birth'] = $oldData->customerDateOfBirth;
        $previousData['Customer Designation'] = $oldData->customerDesignation;
        $previousData['Customer Department'] = $oldData->customerDepartment;
        $previousData['Customer Credit Limit'] = $oldData->customerCreditLimit;
        $previousData['Customer Discount'] = $oldData->customerDiscount;

        $newData = [];
        $newData['Customer Name'] = $newwData->customerName;
        $newData['Customer Code'] = $newwData->customerCode;
        $newData['Customer Short Name'] = $newwData->customerShortName;
        $newData['Customer Identity Number'] = $newwData->customerIdentityNumber;
        $newData['Customer Telephone Number'] = $newwData->customerTelephoneNumber;
        $newData['Customer Vat Number'] = $newwData->customerVatNumber;
        $newData['Customer SVat Number'] = $newwData->customerSVatNumber;
        $newData['Customer Date Of Birth'] = $newwData->customerDateOfBirth;
        $newData['Customer Designation'] = $newwData->customerDesignation;
        $newData['Customer Department'] = $newwData->customerDepartment;
        $newData['Customer Credit Limit'] = $newwData->customerCreditLimit;
        $newData['Customer Discount'] = $newwData->customerDiscount;

        return array('previousData' => $previousData, 'newData' => $newData);
    }

    private function _updateCustomerProfile($newCustomerProfiles, $customerID, $primaryProfileID)
    {
        $primaryProfileUpdatedFlag = FALSE;
        $existingProfiles = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerID($customerID);
//        if already exists customer Profile will update or delete from database
        foreach ($existingProfiles as $existingProfile) {
            if (array_key_exists($existingProfile['customerProfileID'], $newCustomerProfiles)) {
                $newCustomerProfiles[$existingProfile['customerProfileID']]['customerProfileID'] = $existingProfile['customerProfileID'];
                $newCustomerProfiles[$existingProfile['customerProfileID']]['customerID'] = $customerID;

                $customerProfile = new CustomerProfile();
                $customerProfileForm = new CustomerProfileForm();
                $customerProfileForm->setInputFilter($customerProfile->getInputFilter());
                $customerProfileForm->setData($newCustomerProfiles[$existingProfile['customerProfileID']]);

//              Checking is this primary profile for this customer
                if ($newCustomerProfiles[$existingProfile['customerProfileID']]['customerProfileID'] === $primaryProfileID) {
                    $newCustomerProfiles[$existingProfile['customerProfileID']]['isPrimary'] = TRUE;
                } else {
                    $newCustomerProfiles[$existingProfile['customerProfileID']]['isPrimary'] = FALSE;
                }

                if ($customerProfileForm->isValid()) {
                    $customerProfile->exchangeArray($newCustomerProfiles[$existingProfile['customerProfileID']]);
                    $this->CommonTable('Invoice\Model\CustomerProfileTable')->updateCustomerProfile($customerProfile);

                    //save customer profile rating types
                    if (count($newCustomerProfiles[$existingProfile['customerProfileID']]['customerProfileRatingTypes']) > 0) {
                        foreach ($newCustomerProfiles[$existingProfile['customerProfileID']]['customerProfileRatingTypes'] as $crtKey => $crtVal) {
                            $data = array(
                                'customerId' => $customerID,
                                'customerProfileId' => $existingProfile['customerProfileID'],
                                'ratingTypesId' => $crtKey,
                                'customerRatingsRatingValue' => $crtVal['ratingTypesRatingValue']
                            );
                            $customerRatings = new CustomerRatings;
                            $customerRatings->exchangeArray($data);
                            $result = $this->CommonTable('Invoice\Model\CustomerRatingsTable')->saveCustomerRatingTypes($customerRatings);
                        }
                    }
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_PROFILE_UPDATE');
                }
            } else {
                foreach ($newCustomerProfiles as $key => $value) {
                    $deletedID = explode('_', $key);
                    if ($deletedID[0] == "delete") {
                        $this->CommonTable('Invoice\Model\CustomerProfileTable')->deleteCustomerProfile($deletedID[1]);
                    }
                }
            }
        }

        foreach ($newCustomerProfiles as $key => $value) {
            $deletedKey = explode("_", $key);
            $deletedKeyFlag = false;
            if ($deletedKey[0] == "delete") {
                $deletedKeyFlag = true;
            }
            if ($deletedKeyFlag) {
                unset($newCustomerProfiles[$key]);    
                unset($newCustomerProfiles[$deletedKey[1]]);    
            }
        }

        //get primary profiles
        $primaryProfiles =  $this->CommonTable('Invoice\Model\CustomerProfileTable')->getDefaultProfileByCustomerID($customerID);
        foreach ($primaryProfiles as $key => $value) {
            $this->CommonTable('Invoice\Model\CustomerProfileTable')->updatePrimaryStateByCustomerProfileID($value['customerProfileID'], 0);
        }

//        check if there new profile will be save to database
        foreach ($newCustomerProfiles as $key => $newCustomerProfile) {
            if ((strpos($key, 'unsavedProfile') !== false)) {
                $customerProfile = new CustomerProfile();
                $customerProfileForm = new CustomerProfileForm();
                $newCustomerProfile['customerID'] = $customerID;
                $customerProfileForm->setInputFilter($customerProfile->getInputFilter());
                $customerProfileForm->setData($newCustomerProfile);

                // Checking is this primary profile for this customer
                if ($key === $primaryProfileID) {
                    $newCustomerProfile['isPrimary'] = TRUE;
                    $primaryProfileUpdatedFlag = TRUE;
                } else {
                    $newCustomerProfile['isPrimary'] = FALSE;
                }

                if ($customerProfileForm->isValid()) {
                    $customerProfile->exchangeArray($newCustomerProfile);
                    $this->CommonTable('Invoice\Model\CustomerProfileTable')->saveCustomerProfile($customerProfile);
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_PROFILE_UPDATE');
                }
            }
        }

        if (!$primaryProfileUpdatedFlag) {
            $this->CommonTable('Invoice\Model\CustomerProfileTable')->updatePrimaryStateByCustomerProfileID($primaryProfileID, true);
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * customer list without default customer
     * @return type
     */
    public function searchCustomersForDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $state = false;
            $customerSearchKey = $searchrequest->getPost('searchKey');
            $addFlag = $searchrequest->getPost('addFlag');
            if($addFlag == 'withDeactivatedCustomers'){
                $state = true;
            }
            $customerList = array();
            if($addFlag == 'Invoice'){

                $customers = $this->CommonTable('Invoice\Model\CustomerProfileTable')->searchCustomersForDropDown($customerSearchKey);
                $tempCustArr = array();

                foreach ($customers as $customer) {
                    $tempCustArr[$customer['customerID']]['customerName'] = $customer['customerName'];
                    $tempCustArr[$customer['customerID']]['customerCode'] = $customer['customerCode'];
                    if($customer['customerProfileName'] != "Default"){
                        $tempCustArr[$customer['customerID']]['customerProfiles'][] = $customer['customerProfileName'];
                    }
                }

                foreach ($tempCustArr as $key => $value) {
                    $temp['value'] = $key;
                    if(isset($value['customerProfiles'])){
                        $temp['text'] = $value['customerName'] . '-' . $value['customerCode'].' ( '.implode(",", $value['customerProfiles']).' ) ';
                    }else{
                        $temp['text'] = $value['customerName'] . '-' . $value['customerCode'];
                    }
                    $customerList[] = $temp;
                }

                $this->data = array('list' => $customerList);

            } elseif ($addFlag == 'Job') {
                $customers = $this->CommonTable('Invoice\Model\CustomerTable')->searchCustomersForDropDown($customerSearchKey,$state);

                foreach ($customers as $customer) {
                    $temp['value'] = $customer['customerID'];
                    $temp['text'] = (!empty($customer['customerTitle'])) ? $customer['customerTitle'].'. '.$customer['customerName'] . '-' . $customer['customerCode'] : $customer['customerName'] . '-' . $customer['customerCode'];
                    $customerList[] = $temp;
                }
                $this->data = array('list' => $customerList);
            } else{

                $customers = $this->CommonTable('Invoice\Model\CustomerTable')->searchCustomersForDropDown($customerSearchKey,$state);

                foreach ($customers as $customer) {
                    $temp['value'] = $customer['customerID'];
                    $temp['text'] = $customer['customerName'] . '-' . $customer['customerCode'];
                    $customerList[] = $temp;
                }

                $this->data = array('list' => $customerList);
            }

            return $this->JSONRespond();
        }
    }

    public function searchCustomerProfilesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $customerSearchKey = $searchrequest->getPost('searchKey');
            $customerProfiles = $this->CommonTable('Invoice\Model\CustomerProfileTable')->searchCustomerProfileForDropDown($customerSearchKey);

            $customerList = array();
            foreach ($customerProfiles as $profile) {
                $temp['value'] = $profile['customerProfileName'];
                $temp['text'] = $profile['customerProfileName'];
                $customerList[] = $temp;
            }

            $this->data = array('list' => $customerList);
            return $this->JSONRespond();
        }
    }

//get customer list by customer name
    public function getCustomerFromSearchByCustomerNameAction()
    {
        $searchrequest = $this->getRequest();
//check request is post
        if ($searchrequest->isPost()) {
            $customerName = $searchrequest->getPost('customerSearchKey');
            $customers = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCustomerNameforSearch($customerName, TRUE);
//call _loadCustomerListFromSearchData for load customer data for view
            return $this->_loadCustomerListFromSearchData($customers);
        }
    }

//get customer list by customer Code
    public function getCustomerFromSearchByCustomerCodeAction()
    {
        $searchrequest = $this->getRequest();
//check request is post
        if ($searchrequest->isPost()) {
            $customerCode = $searchrequest->getPost('customerSearchKey');
            $customers = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCustomerCodeforSearch($customerCode, TRUE);
//call _loadCustomerListFromSearchData for load customer data for view
            return $this->_loadCustomerListFromSearchData($customers);
        }
    }

//get customer list by customer short name
    public function getCustomerFromSearchByCustomerShortNameAction()
    {
        $searchrequest = $this->getRequest();
//check request is post
        if ($searchrequest->isPost()) {
            $customerShortName = $searchrequest->getPost('customerName');
            $customers = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCustomerShortNameforSearch($customerShortName);
//call _loadCustomerListFromSearchData for load customer data for view
            return $this->_loadCustomerListFromSearchData($customers);
        }
    }

    /**
     * get Customer from search customer list
     * @return type
     */
    public function getCustomerFromSearchByCustomerIDAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $customerID = $searchrequest->getPost('customerID');
            $isSale = ($searchrequest->getPost('isSale')) ? $searchrequest->getPost('isSale') : false;
            $customers = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCustomerIDForSearch($customerID);
            return $this->_loadCustomerListFromSearchData($customers, $isSale);
        }
    }

    private function _loadCustomerListFromSearchData($customers = array(), $isSale = false)
    {
//set customer-list phtml as customer list view template
        if ($isSale) {
            $customerListView = new ViewModel([
                'customers' => $customers,
                'isSale' => $isSale,
                'editAction' => 'sales-customer-edit',
                'indexAction' => 'sales-customer-index',
                    ]
            );
        } else {
            $customerListView = new ViewModel(
                    array('customers' => $customers, 'isSale' => $isSale));
        }
        $customerListView->setTerminal(true);
        $customerListView->setTemplate('invoice/customer-api/customer-list');

        $this->status = true;
        $this->html = $customerListView;
        return $this->JSONRespondHtml();
    }

//get customer delete confirm box
    public function getCusdeleteConfirmBoxAction()
    {
        $viewmodel = new ViewModel();
        $viewmodel->setTerminal(true);
        return $viewmodel;
    }

//delete customer by customerID
    public function deleteCustomerAction()
    {
        $request = $this->getRequest();
//check whether request is post
        if ($request->isPost()) {

//customer update transaction begin
            $this->beginTransaction();

//check request customer id is null
            if (!is_null($request->getPost('customerID'))) {
                $chkcust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($request->getPost('customerID'));
                //check whether request delete customer in database or not
                if ($chkcust != NULL) {
                    //check whether customer have transaction
                    $checkCustomer = $this->CommonTable('Invoice\Model\CustomerTable')->checkCustomerTransaction($request->getPost('customerID'));
                    $custTran = ($checkCustomer) ? (object) $checkCustomer->current() : '';
                    //if customer does not have transaction then delete customer from database
                    if ($custTran == '' || !$custTran->creditNoteID && !$custTran->deliveryNoteID && !$custTran->incomingPaymentID && !$custTran->quotationID && !$custTran->salesInvoiceID && !$custTran->soID && !$custTran->inquiryLogID) {
                        $entityID = $chkcust->entityID;
                        $res = $this->updateDeleteInfoEntity($entityID);
                        if ($res == false) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CANNOT_DELETE');
                            $this->data = FALSE;
                        } else {

                            //update contact
                            $updateContact = $this->CommonTable('Invoice\Model\ContactsTable')->updateLinkedCusData($request->getPost('customerID'));
                            $this->commit();
                            $this->setLogMessage("Customer ".$chkcust->customerCode." is deleted");
                            $this->status = true;
                            $this->msg = $this->getMessage('SUCC_CUSTAPI_CUST_DELETE');
                            $this->data = TRUE;
                        }
                        //if customer have any transaction the given message for user
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CANNOT_DELETE');
                        $this->data = FALSE;
                    }
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTAPI_INVALID_CUST');
                    $this->data = FALSE;
                }
            } else {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CANNOT_DELETE');
                $this->data = FALSE;
            }
        }
        return $this->JSONRespond();
    }

//get customer import confirm view
    public function getImportConfirmAction()
    {
        $viewmodel = new ViewModel();
        $viewmodel->setTerminal(true);
        return $viewmodel;
    }

//get customer edit details by customer id
    public function getCustomerEditBoxAction()
    {
        $req = $this->getRequest();
//check whether request is post
        if ($req->isPost()) {
            $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($req->getPost('customerID'));
            $currencyID = (isset($customerData->customerCurrency)) ? $customerData->customerCurrency : '';
//check whether this customer is in database
            if (isset($customerData->customerID)) {
                $customer[] = $customerData;
                $customer[0]->customerCurrencyID = $currencyID;
                //get customer currency name
                $re2 = $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customer[0]->customerCurrency);
                $customer[0]->customerCurrency = $re2->currencyName;
                $this->data = $customer;
                $this->status = true;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUSTAPI_INVALID_CUST');
            }
        }
        return $this->JSONRespond();
    }

//customer import action
    public function importcustomerAction()
    {
        $request = $this->getRequest();
//check whether request is post
        $writtenrowcount = 0;
        $replacerowcount = 0;
        if ($request->isPost()) {
            $subCategory = $request->getPost('subCategory');
            foreach ($subCategory as $key => $value) {
                $id = explode('-', $value)[0];
                if ($id == 'new_category') {
                    $category = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->getByCustomerCategoryByCusCategoryName($key);
                    //check new category name exist
                    if ($category['customerCategoryID']) {//if exist assign existing category id
                        $subCategory[$key] = $category['customerCategoryID'];
                    } else {// if not exist craete category
                        $categoryData = [
                            'customerCategoryName' => $key
                        ];
                        $newCategory = new CustomerCategory();
                        $newCategory->exchangeArray($categoryData);
                        $categoryId = $this->CommonTable('Invoice/Model/CustomerCategoryTable')->saveCustomerCategory($newCategory);
                        if (!$categoryId) {
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_SAVE');
                            return $this->JSONRespond();
                        }
                        $subCategory[$key] = $categoryId;
                    }
                }
            }

//customer import transaction begin
            $this->beginTransaction();

            $delim = $request->getPost('delim');
//open csv file
            $filecontent = fopen('/tmp/ezBiz.customerImportData.csv', "r");

            if ($request->getPost('header') == TRUE) {
                $linecontent = fgetcsv($filecontent, 1000, $delim);
            }
            $colarray = $request->getPost('col');

            $columncount = $request->getPost('columncount');
            
            //check this account use accounting..
            $companyDetails = $this->getCompanyDetails();
            $companyUseAccountingFlag = false;
            $glAcconts = [];
            if($companyDetails[0]->companyUseAccounting == 1){
                $companyUseAccountingFlag = true;
                //get customer related default accounts
                $accDetails = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
                $glAcconts = iterator_to_array($accDetails)[0];
            }
                  
            //validate customer CSV
            $cusValidate = $this->validateCustomerCsv($delim, $linecontent, $colarray);
            if (!$cusValidate['status']) {
                $this->rollback();
                $this->status = false;
                $this->msg = $cusValidate['msg'];
                return $this->JSONRespond();
            }

            //customer loop
            while ($linecontent = fgetcsv($filecontent, 1000, $delim)) {
                $importarray = array();
                $columns = count($linecontent);
                //get ecah customer details for array
                for ($content = 0; $content < $columns; $content++) {
                    if ($colarray[$content] == 'Customer Title') {
                        $importarray['customerTitle'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Customer Name') {
                        $importarray['customerName'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Customer Code') {
                        $importarray['customerCode'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Customer Short Name') {
                        $importarray['customerShortName'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Currency') {
                        $importarray['customerCurrency'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Telephone') {
                        $importarray['customerTelephoneNumber'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Email') {
                        $importarray['customerProfileEmail'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Address1') {
                        $importarray['customerProfileLocationNo'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Address2') {
                        $importarray['customerProfileLocationRoadName1'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Address3') {
                        $importarray['customerProfileLocationRoadName2'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Address4') {
                        $importarray['customerProfileLocationRoadName3'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Address5') {
                        $importarray['customerProfileLocationSubTown'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Address6') {
                        $importarray['customerProfileLocationTown'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Vat Number') {
                        $importarray['customerVatNumber'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Outstanding Balance') {
                        $importarray['customerCurrentBalance'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Credit Balance') {
                        $importarray['customerCurrentCredit'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Credit Limit') {
                        $importarray['customerCreditLimit'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Discount') {
                        $importarray['customerDiscount'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Other') {
                        $importarray['customerOther'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Customer Title') {
                        $importarray['customerTitle'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Additional Telephone1') {
                        $importarray['customerProfileMobileTP1'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Additional Telephone2') {
                        $importarray['customerProfileLandTP1'] = $linecontent[$content];
                    } elseif ($colarray[$content] == 'Customer Category') {
                        $importarray['customerCategory'] = $subCategory[$linecontent[$content]];
                    }
                }

                if (!$importarray['customerCode']) {
                    //get customer reference number.
                    $refData = $this->getReferenceNoForLocation(20, $locationID);
                    $rid = $refData["refNo"];
                    $locationReferenceID = $refData["locRefID"];
                    $customerCode = $this->getReferenceNumber($locationReferenceID);
                    
                    //set customer reference number into the data set.
                    $importarray['customerCode'] = $customerCode;
                }

                //set customer default gLAccounts
                if($companyUseAccountingFlag){
                    $importarray['customerReceviableAccountID'] = $glAcconts->glAccountSetupSalesAndCustomerReceivableAccountID;
                    $importarray['customerSalesAccountID'] = $glAcconts->glAccountSetupSalesAndCustomerSalesAccountID;
                    $importarray['customerSalesDiscountAccountID'] = $glAcconts->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                    $importarray['customerAdvancePaymentAccountID'] = $glAcconts->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                }

                //set customer exchange array
                $importcustomer = new Customer();
                $importcustomer->exchangeArray($importarray);

                $currencyData = $this->CommonTable('Core\Model\CurrencyTable')->getCurrencyByName($importcustomer->customerCurrency);
                if ($currencyData) {
                    $importcustomer->customerCurrency = $currencyData->currencyID;
                } else {
                    $currencyData = $this->CommonTable('Settings\Model\DisplaySetupTable')->getSystemCurrency()->current();
                    $importcustomer->customerCurrency = $currencyData['currencyID'];
                }

                if ($request->getPost('method') == 'discard') {
                    //check customer is alredy in database
                    $searchcust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByTelephoneNumber($importcustomer->customerTelephoneNumber);

                    //if customer not in database save those customers
                    if (!isset($searchcust->customerID)) {
                        //check customer is alredy in database
                        $searchcustByCode = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCode($importcustomer->customerCode);
                        if (!isset($searchcustByCode->customerID)) {
                            //create entity id for customer
                            $entityID = $this->createEntity();
                            $importcustomer->entityID = $entityID;
                            //call customer save function in customerTable
                            $importres = $this->CommonTable('Invoice\Model\CustomerTable')->saveCustomer($importcustomer);
                            if ($importres == TRUE) {
                                $importarray['customerID'] = $importres;
                                $importarray['customerProfileName'] = 'Default';
                                $importarray['isPrimary'] = '1';
                                $importCusProfile = new CustomerProfile();
                                $importCusProfile->exchangeArray($importarray);
                                //call customer profile Update function in customerProfileTable
                                $cusAditionalData = $this->CommonTable('Invoice\Model\CustomerProfileTable')->saveCustomerProfile($importCusProfile);
                                //update customer reference
                                $this->updateReferenceNumber($locationReferenceID);
                                $writtenrowcount++;
                                if (floatval($importcustomer->customerCurrentBalance) > 0) {
                                    $invResult = $this->storeInvoiceForCustomerOpeningBalance($importres, $importcustomer, $cusAditionalData);
                                    if (!$invResult['status']) {
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $invResult['msg'];
                                        return $this->JSONRespond();
                                    }
                                }
                                if (floatval($importcustomer->customerCurrentCredit) > 0) {
                                    $advResult = $this->storeAdvancedPaymentForCustomerOpeningCreditBalance($importres, $importcustomer);
                                    if (!$advResult['status']) {
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $advResult['msg'];
                                        return $this->JSONRespond();
                                    }
                                }
                            } else {
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_SAVE');
                                return $this->JSONRespond();
                            }
                        }
                    }
                } else if ($request->getPost('method') == 'replace') {
                    //check customer is alredy in database
                    $searchcust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByTelephoneNumber($importcustomer->customerTelephoneNumber);
                    //if customer is in database update those customers
                    if (isset($searchcust->customerID)) {
                        //set customer entity id for new update
                        $importcustomer->customerID = $searchcust->customerID;
                        $importcustomer->entityID = $searchcust->entityID;
                        $importcustomer->customerCode = $searchcust->customerCode;

                        if (floatval($searchcust->customerCurrentBalance) != 0 && floatval($searchcust->customerCurrentCredit) != 0) {
                            $cusData = array(
                                'customerName' => $importcustomer->customerName,
                                'customerTitle' => $importcustomer->customerTitle,
                                'customerShortName' => $importcustomer->customerShortName,
                                'customerCurrency' => $importcustomer->customerCurrency,
                                'customerCategory' => $importcustomer->customerCategory,
                                'customerTelephoneNumber' => $importcustomer->customerTelephoneNumber,
                                'customerVatNumber' => $importcustomer->customerVatNumber,
                                'customerPaymentTerm' => $importcustomer->customerPaymentTerm,
                                'customerCreditLimit' => $importcustomer->customerCreditLimit,
                                'customerDiscount' => $importcustomer->customerDiscount,
                                'customerOther' => $importcustomer->customerOther,
                            );
                        } else if (floatval($searchcust->customerCurrentBalance) != 0 && floatval($searchcust->customerCurrentCredit) == 0) {
                            $cusData = array(
                                'customerName' => $importcustomer->customerName,
                                'customerTitle' => $importcustomer->customerTitle,
                                'customerShortName' => $importcustomer->customerShortName,
                                'customerCurrency' => $importcustomer->customerCurrency,
                                'customerCategory' => $importcustomer->customerCategory,
                                'customerTelephoneNumber' => $importcustomer->customerTelephoneNumber,
                                'customerVatNumber' => $importcustomer->customerVatNumber,
                                'customerPaymentTerm' => $importcustomer->customerPaymentTerm,
                                'customerCurrentCredit' => $importcustomer->customerCurrentCredit,
                                'customerCreditLimit' => $importcustomer->customerCreditLimit,
                                'customerDiscount' => $importcustomer->customerDiscount,
                                'customerOther' => $importcustomer->customerOther,
                            );
                        } else if (floatval($searchcust->customerCurrentBalance) == 0 && floatval($searchcust->customerCurrentCredit) != 0) {
                            $cusData = array(
                                'customerName' => $importcustomer->customerName,
                                'customerTitle' => $importcustomer->customerTitle,
                                'customerShortName' => $importcustomer->customerShortName,
                                'customerCurrency' => $importcustomer->customerCurrency,
                                'customerCategory' => $importcustomer->customerCategory,
                                'customerTelephoneNumber' => $importcustomer->customerTelephoneNumber,
                                'customerVatNumber' => $importcustomer->customerVatNumber,
                                'customerPaymentTerm' => $importcustomer->customerPaymentTerm,
                                'customerCurrentBalance' => $importcustomer->customerCurrentBalance,
                                'customerCreditLimit' => $importcustomer->customerCreditLimit,
                                'customerDiscount' => $importcustomer->customerDiscount,
                                'customerOther' => $importcustomer->customerOther,
                            );
                        } else {
                            $cusData = array(
                                'customerName' => $importcustomer->customerName,
                                'customerTitle' => $importcustomer->customerTitle,
                                'customerShortName' => $importcustomer->customerShortName,
                                'customerCurrency' => $importcustomer->customerCurrency,
                                'customerCategory' => $importcustomer->customerCategory,
                                'customerTelephoneNumber' => $importcustomer->customerTelephoneNumber,
                                'customerVatNumber' => $importcustomer->customerVatNumber,
                                'customerPaymentTerm' => $importcustomer->customerPaymentTerm,
                                'customerCreditLimit' => $importcustomer->customerCreditLimit,
                                'customerDiscount' => $importcustomer->customerDiscount,
                                'customerOther' => $importcustomer->customerOther,
                            );
                        }

                        //call customer update function in customerTable
                        $importres = $this->CommonTable('Invoice\Model\CustomerTable')->importreplaceCustomer($cusData,$importcustomer->customerID);
                        if ($importres == TRUE) {
                            $importarray['customerID'] = $searchcust->customerID;
                            $importarray['customerProfileName'] = 'Default';
                            $importarray['isPrimary'] = '1';
                            $importCusProfile = new CustomerProfile();
                            $importCusProfile->exchangeArray($importarray);
                            //call customer profile update function in customerProfileTable
                            $cusUpdateAditionalData = $this->CommonTable('Invoice\Model\CustomerProfileTable')->updateCustomerProfileByCustomerID($importCusProfile);
                            $replacerowcount++;
                            if ((floatval($searchcust->customerCurrentBalance) != floatval($importcustomer->customerCurrentBalance)) && $importcustomer->customerCurrentBalance != null) {
                                $locationID = $this->user_session->userActiveLocation['locationID'];
                                $invDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByCustomerID($searchcust->customerID,$locationID);

                                if (count($invDetails) == 0) {
                                    $invResult = $this->storeInvoiceForCustomerOpeningBalance($searchcust->customerID, $importcustomer, $cusUpdateAditionalData);
                                    if (!$invResult['status']) {
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $invResult['msg'];
                                        return $this->JSONRespond();
                                    }
                                } else if (count($invDetails) == 1) {
                                    foreach ($invDetails as $key => $value) {
                                        if ($value['salesInvoiceComment'] === "Created By Customer Opening Balance" && floatval($value['salesInvoicePayedAmount']) == 0 && $value['statusID'] == "3") {
                                            $invoiceData = [
                                                'salesinvoiceTotalAmount' => $importcustomer->customerCurrentBalance,
                                                'customerCurrentOutstanding' => $importcustomer->customerCurrentBalance
                                            ];
                                            $invoiceUpdateStatus = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoice($invoiceData, $value['salesInvoiceID']);

                                            $invProductData = array(
                                                'salesInvoiceProductPrice' => $importcustomer->customerCurrentBalance,
                                                'salesInvoiceProductTotal' => $importcustomer->customerCurrentBalance
                                            );
                                            
                                            //update sale invoice product table
                                            $updateNormalItem = $this->CommonTable('Invoice\Model\InvoiceProductTable')->updateSalesInvoicePoductBySalesInvoiceID($invProductData, $value['salesInvoiceID']);
                                        } else {
                                            $this->rollback();
                                            $this->status = false;
                                            $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_REPLACE_INV_CLOSED');
                                            return $this->JSONRespond();
                                        }
                                    }
                                } else if (count($invDetails) > 1) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_REPLACE_MORE_INV');
                                    return $this->JSONRespond();
                                }                               
                            }
                            if ((floatval($searchcust->customerCurrentCredit) != floatval($importcustomer->customerCurrentCredit)) && $importcustomer->customerCurrentCredit != NULL) {
                                $locationID = $this->user_session->userActiveLocation['locationID'];
                                $advPaymentDetails = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByCustomerID($searchcust->customerID,$locationID, null);

                                if (count($advPaymentDetails) == 0) {
                                    $advResult = $this->storeAdvancedPaymentForCustomerOpeningCreditBalance($searchcust->customerID, $importcustomer);
                                    if (!$advResult['status']) {
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $advResult['msg'];
                                        return $this->JSONRespond();
                                    }
                                } else if (count($advPaymentDetails) == 1) {
                                    foreach ($advPaymentDetails as $key => $value) {
                                        if ($value['incomingPaymentMemo'] === "Created By Customer Opening Credit Balance" && $value['incomingPaymentStatus'] == "4") {
                                            $advData = [
                                                'incomingPaymentAmount' => $importcustomer->customerCurrentCredit,
                                                'incomingPaymentPaidAmount' => $importcustomer->customerCurrentCredit
                                            ];
                                            $paymentUpdateStatus = $this->CommonTable('Invoice\Model\PaymentsTable')->update($advData, $value['incomingPaymentID']);

                                            $payMethodData = array(
                                                'incomingPaymentMethodAmount' => $importcustomer->customerCurrentCredit
                                            );
                                            
                                            //update sale invoice product table
                                            $updateNormalItem = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->updateIncomingPaymentAmountByPaymentID($payMethodData, $value['incomingPaymentID']);
                                        } else {
                                            $this->rollback();
                                            $this->status = false;
                                            $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_REPLACE_ADVPAY_EDITED');
                                            return $this->JSONRespond();
                                        }
                                    }
                                } else if (count($advPaymentDetails) > 1) {
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_REPLACE_MORE_ADVPAY');
                                    return $this->JSONRespond();
                                }                               
                            }
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_REPLACE');
                            return $this->JSONRespond();
                        }
                        //if customer not in database save those customers
                    } else {
                        $searchcustByCode = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCode($importcustomer->customerCode);
                        if (!isset($searchcustByCode->customerID)) {
                            //create entity id for customer
                            $entityID = $this->createEntity();
                            $importcustomer->entityID = $entityID;
                            //call customer save function in customerTable
                            $importres = $this->CommonTable('Invoice\Model\CustomerTable')->saveCustomer($importcustomer);
                            if ($importres == TRUE) {
                                $importarray['customerID'] = $importres;
                                $importarray['customerProfileName'] = 'Default';
                                $importarray['isPrimary'] = '1';
                                $importCusProfile = new CustomerProfile();
                                $importCusProfile->exchangeArray($importarray);
                                //call customer profile in customerProfileTable
                                $cusAditionalData = $this->CommonTable('Invoice\Model\CustomerProfileTable')->saveCustomerProfile($importCusProfile);
                                //update customer reference
                                $this->updateReferenceNumber($locationReferenceID);
                                $writtenrowcount++;
                                if (floatval($importcustomer->customerCurrentBalance) > 0) {
                                    $invResult = $this->storeInvoiceForCustomerOpeningBalance($importres, $importcustomer, $cusAditionalData);
                                    if (!$invResult['status']) {
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $invResult['msg'];
                                        return $this->JSONRespond();
                                    }
                                }
                                if (floatval($importcustomer->customerCurrentCredit) > 0) {
                                    $advResult = $this->storeAdvancedPaymentForCustomerOpeningCreditBalance($importres, $importcustomer);
                                    if (!$advResult['status']) {
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $advResult['msg'];
                                        return $this->JSONRespond();
                                    }
                                }
                            } else {
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_SAVE');
                                return $this->JSONRespond();
                            }
                        }

                    }
                }
            }
            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_SAVE', array($writtenrowcount, $replacerowcount));
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CUSTAPI_REQ_NOTPOST');
        }
        return $this->JSONRespond();
    }

    /*
    * This fuction is used validate customer import csv
    * @param $delimeter, $fileContent
    * @return @resultset
    */
    public function validateCustomerCsv($delim, $linecontent, $colarray)
    {
        $finalArray = array();
        $fContent = fopen('/tmp/ezBiz.customerImportData.csv', "r");
        while ($linecontent = fgetcsv($fContent, 1000, $delim)) {
            $importarray = array();
            $columns = count($linecontent);
            //get ecah customer details for array
            for ($content = 0; $content < $columns; $content++) {
                if ($colarray[$content] == 'Customer Title') {
                    $importarray['customerTitle'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Customer Name') {
                    $importarray['customerName'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Customer Code') {
                    $importarray['customerCode'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Customer Short Name') {
                    $importarray['customerShortName'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Currency') {
                    $importarray['customerCurrency'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Telephone') {
                    $importarray['customerTelephoneNumber'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Email') {
                    $importarray['customerProfileEmail'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Address1') {
                    $importarray['customerProfileLocationNo'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Address2') {
                    $importarray['customerProfileLocationRoadName1'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Address3') {
                    $importarray['customerProfileLocationRoadName2'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Address4') {
                    $importarray['customerProfileLocationRoadName3'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Address5') {
                    $importarray['customerProfileLocationSubTown'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Address6') {
                    $importarray['customerProfileLocationTown'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Vat Number') {
                    $importarray['customerVatNumber'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Outstanding Balance') {
                    $importarray['customerCurrentBalance'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Credit Balance') {
                    $importarray['customerCurrentCredit'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Credit Limit') {
                    $importarray['customerCreditLimit'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Discount') {
                    $importarray['customerDiscount'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Other') {
                    $importarray['customerOther'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Customer Title') {
                    $importarray['customerTitle'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Additional Telephone1') {
                    $importarray['customerProfileMobileTP1'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Additional Telephone2') {
                    $importarray['customerProfileLandTP1'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Customer Category') {
                    $importarray['customerCategory'] = $subCategory[$linecontent[$content]];
                }
            }
            $finalArray[] = $importarray;
        }

        $customerCodeArray = Array();
        $telephoneNumberArray = Array();
        foreach ($finalArray as $key => $value) {
            if ($value['customerTelephoneNumber'] == "") {
                return ['status' => false, 'msg' => $this->getMessage('ERR_CUST_TELE_EMPTY_IN_CSV')];
            }
            array_push($telephoneNumberArray, $value['customerTelephoneNumber']);
            array_push($customerCodeArray, $value['customerCode']);
        }

        if (count($telephoneNumberArray) != count(array_unique($telephoneNumberArray))) {
            return ['status' => false, 'msg' => $this->getMessage('ERR_CUST_TELE_DUPLICATE_IN_CSV')];
        }

        if (count($customerCodeArray) != count(array_unique($customerCodeArray))) {
            return ['status' => false, 'msg' => $this->getMessage('ERR_CUST_CODE_DUPLICATE_IN_CSV')];
        }

        return ['status' => true];

    }


    /*
    * This function is used to create invoice for customer opening balance
    * @param customerID, customer details
    * @return array
    */
    public function storeInvoiceForCustomerOpeningBalance($customerID, $customerDetails, $cusProfileID)
    {
        $nonInventoryProductDetails = $this->CommonTable("Inventory\Model\ProductTable")->getActiveNonInventoryItems()->current();
        if (!$nonInventoryProductDetails) {
            $res = $this->saveNonInventoryProduct();
            if ($res['status']) {
                $nonInventoryProductDetails = $this->CommonTable("Inventory\Model\ProductTable")->getActiveNonInventoryItems()->current();
            } else {
                return ['status' => false, 'msg'=> $res['msg']];
            }
        }
        
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $result = $this->getReferenceNoForLocation('3', $locationID);
        $stlocationReferenceID = $result['locRefID'];
        $invoiceCode = $this->getReferenceNumber($stlocationReferenceID);

        if ($invoiceCode == "") {
            return ['status' => false, 'msg'=> $this->getMessage('ERR_DELINOTECON_CHANGE_REF_MAX_REACH')];
        }

        $locationProductID = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProduct($nonInventoryProductDetails['productID'], $locationID);
        $invProducts = [];
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productID'] = $nonInventoryProductDetails['productID'];
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['documentTypeID'] = "";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['documentID'] = "";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['locationID'] = $locationID;
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['locationProductID'] = $locationProductID->locationProductID;
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productIncID'] = $nonInventoryProductDetails['productID'].'_1';
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productCode'] = $nonInventoryProductDetails['productCode'];
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productName'] = $nonInventoryProductDetails['productName'];
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productPrice'] = $customerDetails->customerCurrentBalance;
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productDiscount'] = "";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productDiscountType'] = "";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productTotal'] = $customerDetails->customerCurrentBalance;
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productType'] = "2";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['productDescription'] = "";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['stockUpdate'] = "true";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['giftCard'] = "0";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['availableQuantity']['qty'] = "0";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['deliverQuantity']['qty'] = "1";
        $invProducts[$nonInventoryProductDetails['productID'].'_1']['selectedUomId'] = "1";
        $invoiceDetails = [
            'invoiceCode' => $invoiceCode,
            'locationOutID' => $locationID,
            'products' => $invProducts,
            'invoiceDate' =>  $this->convertDateToStandardFormat(date("Y-m-d")),
            'dueDate' => $this->convertDateToStandardFormat(date("Y-m-d")),
            'customer' => $customerID,
            'customerProfID' => $cusProfileID,
            'customerName' => $customerDetails->customerName.'-'.$customerDetails->customerCode,
            'deliveryCharge' => "",
            'invoiceTotalPrice' => $customerDetails->customerCurrentBalance,
            'invoiceComment' => "Created By Customer Opening Balance",
            'paymentTermID' => "1",
            'productState' => "1",
            'categoryID' => "1",
            'invoiceDiscountType' => "presentage",
            'invoiceDiscountRate' => "",
            'totalTaxValue' => "0",
            'susTaxAmount' => "0",
            'showTax' => "1",
            'salesOrderID' => "",
            'quotationID' => "",
            'activityID' => "",
            'jobID' => "",
            'projectID' => "",
            'salesPersonID' => "",
            'suspendedTax' => "0",
            'deliveryAddress' => "",
            'promotionID' => "",
            'promotionDiscountValue' => "0",
            'invoiceWisePromotionDiscountType' => "",
            'customCurrencyId' => "8",
            'customCurrencyRate' => "0",
            'priceListId' => "",
            'salesInvoiceDeliveryChargeEnable' => "0",
            'ignoreBudgetLimit' => true,
        ];

        $invoice = $this->getServiceLocator()->get("InvoiceController");
        $invSaveState = $invoice->storeInvoice($invoiceDetails, $customerID, false, false);
        if($invSaveState['status']){
            return ['status' => true, 'msg'=> $invSaveState['msg']];
        } else {
            return ['status' => false, 'msg'=> $invSaveState['msg']];
        }
    }

    /*
    * This function is used to create non- inventory item for customer opening balance
    */
    public function saveNonInventoryProduct()
    {
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->user_session->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
            }
        }

        $proHandling = array('productHandelingManufactureProduct');
        $proUOM = [];
        $proUOM[1]['uomConversion'] = "1";
        $proUOM[1]['baseUom'] = "true";
        $proUOM[1]['displayUom'] = "true";

        $refData = $this->getReferenceNoForLocation(33, 1);
        $rid = $refData["refNo"];

        $post = [
            'productCode' => $rid,
            'productBarcode' => "",
            'productName' => "Non Inventory - Customer opening balance",
            'productTypeID' => "2",
            'handeling' => $proHandling,
            'productDescription' => "",
            'productDiscountPercentage' => "",
            'productDiscountValue' => "",
            'purchaseDiscountEligible' => false,
            'productPurchaseDiscountPercentage' => "",
            'productPurchaseDiscountValue' => "",
            'productImageURL' => "0",
            'productState' => "1",
            'categoryID' => "1",
            'productGlobalProduct' => true,
            'batchProduct' => false,
            'serialProduct' => false,
            'hsCode' => "",
            'customDutyValue' => "",
            'customDutyPercentage' => "",
            'productDefaultOpeningQuantity' => "",
            'productDefaultSellingPrice' => "",
            'productDefaultMinimumInventoryLevel' => "",
            'productDefaultReorderLevel' => "",
            'productTaxEligible' => false,
            'discountEligible' => false,
            'productDefaultPurchasePrice' => "",
            'rackID' => "",
            'flag' => "product",
            'itemDetail' => "",
            'uomDisplay' => "1",
            'displayUoms' => "1",
            'uom' => $proUOM,
            'productSalesAccountID' => $productSalesAccountID,
            'productInventoryAccountID' => $productInventoryAccountID,
            'productCOGSAccountID' => $productCOGSAccountID,
            'productAdjusmentAccountID' => $productAdjusmentAccountID,
        ];

        $product = $this->getServiceLocator()->get("ProductAPIController");
        $productSaveState = $product->storeProduct($post, $openingBalanceFlag = true);
        if($productSaveState['status']){
            return ['status' => true, 'data' => $productSaveState['data']['productID'], 'msg'=> $productSaveState['msg']];
        } else {
            return ['status' => false, 'data' => $productSaveState['data']['productID'], 'msg'=> $productSaveState['msg']];
        }
    }

//customer massupdate
    public function massupdateAction()
    {
        $massupdaterequest = $this->getRequest();
//check request is post
        if ($massupdaterequest->isPost()) {

//customer mass update transaction begin
            $this->beginTransaction();

//get post data to array
            $tdarray = $massupdaterequest->getPost('cupd');
            $cAddressArray = $massupdaterequest->getPost('custAddress');
            foreach ($tdarray as $key => $massdata) {
//                foreach ($massdata as $key2 => $uarray) {

                $tempCurr = $massdata['customerCurrency'];
                $tempay = $massdata['customerPaymentTerm'];
                //get customer currency id
                $retCurr = $this->CommonTable('Core\Model\CurrencyTable')->getCurrencyByName($tempCurr);
                //get customer payment term
                $retpay = $this->CommonTable('Core\Model\PaymentTermTable')->getPaymentTermByName($tempay);
                $massdata['customerCurrency'] = isset($retCurr->currencyID) ? $retCurr->currencyID : NULL;
                $massdata['customerPaymentTerm'] = isset($retpay->paymentTermID) ? $retpay->paymentTermID : NULL;

//update customer details
                $ret = $this->CommonTable('Invoice\Model\CustomerTable')->massupdateCustomer($massdata);
                $retProfile = $this->CommonTable('Invoice\Model\CustomerProfileTable')->updateCustomerPrimaryEmail($massdata['customerEmail'], $massdata['customerID']);

                if ($ret == FALSE || $retProfile == FALSE) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_MASS_SAVE');
                    return $this->JSONRespond();
                }
//                }
            }
//            update customer Primary Profile details
            foreach ($cAddressArray as $customerID => $cProfile) {

                $retProfile = $this->CommonTable('Invoice\Model\CustomerProfileTable')->updateCustomerPrimaryAddress($cProfile, $customerID);
                if ($retProfile == FALSE) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_MASS_SAVE');
                    return $this->JSONRespond();
                }
            }
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_CUSTAPI_CUST_MASS_UPDATE');
            $this->commit();
        }
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * Add new Customer Category
     * @return Array new Customer Category List
     */
    public function addNewCustomerCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $customerCategory = new CustomerCategory();

            $customerCategoryForm = new CustomerCategoryForm();
            $customerCategoryForm->setInputFilter($customerCategory->getInputFilter());
            $customerCategoryForm->setData($request->getPost());
            if ($customerCategoryForm->isValid()) {
                $customerCategory->exchangeArray($customerCategoryForm->getData());
//              check same name already exists
                $isExists = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->getByCustomerCategoryName($customerCategory->customerCategoryName);
                if ($isExists) {
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CATEG_EXIST');
                    $this->status = FALSE;
                    return $this->JSONRespond();
                }
            }
            $insertedID = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->saveCustomerCategory($customerCategory);
            if (!$insertedID) {
                $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CATEG_INSERT');
                $this->status = FALSE;
            } else {
                $this->data = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->getByCustomerCategoryID($insertedID);
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_CUSTAPI_CUST_CATEG_INSERT');
                return $this->JSONRespond();
            }
        }
        return $this->JSONRespondHtml();
    }

    /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     * Get Customer Related profiles
     * @return Customer profile list
     */
    public function getCustomerProfilesByCustomerIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerID = $request->getPost('customerID');
            $result = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerID($customerID);
            $customerProfiles = array();
            $customerDetails = array();
            $customerProfileData = array();
            foreach ($result as $row) {
                $row = (object) $row;
                $customerProfiles[$row->customerProfileID] = $row->customerProfileName;
                $customerDetails[$row->customerID] = $row;
                $customerProfileData[$row->customerProfileID]['profName'] = $row->customerProfileName;
                $customerProfileData[$row->customerProfileID]['isPrimary'] = $row->isPrimary;

            }
            $this->data = array('customerProfiles' => $customerProfiles, 'customerDetails' => $customerDetails, 'customerProfileData' => $customerProfileData);
            $this->status = TRUE;
            return $this->JSONRespond();
        }
    }

    /**
     * Get profile data that related to given CustomerProfileID
     * @return Profile Details related to given profileID
     */
    public function getCustomerProfileDataByCustomerProfileIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerProfileID = $request->getPost('customerProfileID');
            $result = $this->CommonTable('Invoice/Model/CustomerProfileTable')->getProfileByCustomerProfileID($customerProfileID)->current();
            if ($result['customerProfileMobileTP1'] != null) {
                $result['firstPhoneNumber'] = $result['customerProfileMobileTP1'];
            } else {
                $result['firstPhoneNumber'] = $result['customerTelephoneNumber'];
            }
            $value = 0;
            if ($result['customerProfileLocationRoadName1']) {
                $addressArray[$value] = $result['customerProfileLocationRoadName1'];
                $value++;
            } if ($result['customerProfileLocationRoadName2']) {
                $addressArray[$value] = $result['customerProfileLocationRoadName2'];
                $value++;
            } if ($result['customerProfileLocationRoadName3']) {
                $addressArray[$value] = $result['customerProfileLocationRoadName3'];
                $value++;
            }if ($result['customerProfileLocationSubTown']) {
                $addressArray[$value] = $result['customerProfileLocationSubTown'];
                $value++;
            }if ($result['customerProfileLocationTown']) {
                $addressArray[$value] = $result['customerProfileLocationTown'];
                $value++;
            }if ($result['customerProfileLocationCountry']) {
                $addressArray[$value] = $result['customerProfileLocationCountry'];
                $value++;
            }if ($result['customerProfileLocationPostalCode']) {
                $addressArray[$value] = ' Postal Code - '.$result['customerProfileLocationPostalCode'];
                $value++;
            }



            if ($addressArray) {
                $addressLine = implode(',', $addressArray);
                $result['addressLine'] = $addressLine;
            }
        }
        if ($result) {
            $this->data = $result;
            $this->status = true;
        } else {
            $this->status = false;
        }
        return $this->JSONRespond();
    }

    public function getCustomerDetailsByCustomerNameCodeOrTelephoneAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $keyword = $request->getPost('searchKey');
            $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCustomerNameCodeOrTelephoneNumber($keyword);
            $customerData = array();
            foreach ($customerDetails as $cusDta) {
                $phoneNumber = array($cusDta['customerProfileMobileTP1'], $cusDta['customerProfileMobileTP2'], $cusDta['customerProfileLandTP1'], $cusDta['customerProfileLandTP2']);
                if (in_array($keyword, $phoneNumber)) {
                    $telephneNumber = $keyword;
                } else {
                    $telephneNumber = $cusDta['customerTelephoneNumber'];
                }
                $temp['value'] = $cusDta['customerID'];
                $address = implode(',', array_filter([
                    $cusDta['customerProfileLocationNo'],
                    $cusDta['customerProfileLocationRoadName1'],
                    $cusDta['customerProfileLocationRoadName2'],
                    $cusDta['customerProfileLocationRoadName3'],
                    $cusDta['customerProfileLocationSubTown'],
                    $cusDta['customerProfileLocationTown'],
                    $cusDta['customerProfileLocationCountry'],
                    $cusDta['customerProfileLocationPostalCode'],
                ]));

                $temp['text'] = $cusDta['customerCode'] . ' - ' . $cusDta['customerName'] . ' - ' . $telephneNumber . ' - ' . $address;
                $customerData[] = $temp;
            }
            $this->data = array('list' => $customerData,);
            return $this->JSONRespond();
        }
    }

    public function getNextCustomerCodeAction(){
        //       get customer code
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(20, $locationID);
        $this->status = true;
        $this->data = $refData;
        return $this->JSONRespond();
    }

    public function changeStatusIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerID = $request->getPost('customerID');
            $status = $request->getPost('status');
            $customerData = $this->CommonTable('Invoice/Model/CustomerTable')->getCustomerByID($customerID); 
            $updateState = $this->CommonTable('Invoice/Model/CustomerTable')->updateCustomerState($customerID, $status);
            if ($updateState) {
                if ($status == 2) {
                    $this->setLogMessage('Customer '.$customerData->customerCode.' is inactivated');
                } else {
                    $this->setLogMessage('Customer '.$customerData->customerCode.' is activated');
                }
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_CUST_STATUS_UPDATE');
                return $this->JSONRespond();
            }
        }
    }

    public function storeAdvancedPaymentForCustomerOpeningCreditBalance($customerID, $customerDetails)
    {

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $result = $this->getReferenceNoForLocation('5', $locationID);
        $stlocationReferenceID = $result['locRefID'];
        $advPayCode = $this->getReferenceNumber($stlocationReferenceID);

        $cashPaymentMethodID = "";
        $paymentMethodsGlAccounts = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentMethodsGlAccounts as $key => $value) {
            if ($value['paymentMethodName'] == "Cash") {
                $cashPaymentMethodID = $value['paymentMethodSalesFinanceAccountID'];
            }
        }

        if ($cashPaymentMethodID == "") {
            return ['msg' => false, 'msg' => "Gl Account is not set for the payment method cash", 'data' => null];
        }
        $paymentMethods = [];
        $paymentMethods[0]['methodID'] = "1";
        $paymentMethods[0]['paidAmount'] = $customerDetails->customerCurrentCredit;
        $paymentMethods[0]['checkNumber'] = "";
        $paymentMethods[0]['bank'] = "";
        $paymentMethods[0]['reciptnumber'] = "";
        $paymentMethods[0]['cardnumber'] = "";
        $paymentMethods[0]['bankID'] = "";
        $paymentMethods[0]['accountID'] = "";
        $paymentMethods[0]['customerBank'] = "";
        $paymentMethods[0]['customerAccountNumber'] = "";
        $paymentMethods[0]['giftCardID'] = "";
        $paymentMethods[0]['postdatedStatus'] = "0";
        $paymentMethods[0]['postdatedDate'] = "";
        $paymentMethods[0]['lcPaymentReference'] = "";
        $paymentMethods[0]['ttPaymentReference'] = "";
        $paymentMethods[0]['cashAccountID'] = $cashPaymentMethodID;
        $paymentMethods[0]['chequeAccountID']['qty'] = "";
        $paymentMethods[0]['creditAccountID']['qty'] = "";
        $paymentMethods[0]['bankTransferAccountID'] = "";
        $paymentMethods[0]['lcAccountID'] = "";
        $paymentMethods[0]['ttAccountID'] = "";
        $advDetails = [
            'paymentID' => $advPayCode,
            'customerName' => $customerDetails->customerName.'-'.$customerDetails->customerCode,
            'date' => $this->convertDateToStandardFormat(date("Y-m-d")),
            'amount' => $customerDetails->customerCurrentCredit ,
            'memo' => "Created By Customer Opening Credit Balance",
            'locationID' => $locationID,
            'paymentType' => "advance",
            'customerID' => $customerID,
            'paymentMethods' => $paymentMethods,
            'totalPaidAmount' => $customerDetails->customerCurrentCredit,
            'customCurrencyId' => "8",
            'customCurrencyRate' => "0",
            'cardType' => ""
        ];

        $advancePayment = $this->getServiceLocator()->get("CustomerPaymentsAPIController");
        $advSaveState = $advancePayment->makeAdvancePayment($advDetails ,true);
        if($advSaveState['state']){
            return ['status' => true, 'msg'=> $advSaveState['msg']];
        } else {
            return ['status' => false, 'msg'=> $advSaveState['msg']];
        }
    }

    public function getAllCustomersDetailsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_REQUEST');
            return $this->JSONRespond();
        }

        $respond = $this->getService('CustomerService')->getAllCustomersDetails($request->getPost());
        if ($respond) {
            $this->data = $respond;
            $this->status = true;
        }
        return $this->JSONRespond();
    }

    public function getCustomerDetailsAction($data)
    {
        $request = $this->getRequest();
        $respond = $this->getService('CustomerService')->getCustomerDetails($request->getPost());
        $this->data = $respond;
        return $this->JSONRespond();
    }

}

/////////////////// END OF CUSTOMER API CONTROLLER \\\\\\\\\\\\\\\\\\\\
