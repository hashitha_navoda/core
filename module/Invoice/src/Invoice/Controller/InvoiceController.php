<?php

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Invoice\Form\AddCustomerForm;
use Invoice\Form\InvoiceForm;
use Inventory\Form\ProductForm;
use Invoice\Form\RecurrentInvoiceForm;
use Invoice\Model\Reference;
use Inventory\Form\ProductHandelingForm;
use Inventory\Form\CategoryForm;
use Core\Controller\Interfaces\DocumentInterface;
use Zend\I18n\Translator\Translator;
use Settings\Model\Template;

class InvoiceController extends CoreController implements DocumentInterface
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_upper_menu';
    protected $downMenus = '';
    protected $userID;
    protected $cdnUrl;
    protected $user_session;
    private $_invoiceViewData;
    private $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->username = $this->user_session->username;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID("Inventory\Controller\Product", "index", $this->user_session->userID);

        $isCanCreateItem = $isEnable;

        $this->getSideAndUpperMenus('Invoices', 'Create Invoice', 'SALES');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $universalDateRestrictionOverride = $this->user_session->UniversalDateRestrictionOverride;
        $refData = $this->getReferenceNoForLocation(3, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];
        $strefData = $this->getReferenceNoForLocation(25, $locationID);
        $strid = $strefData["refNo"];

        $tempInvoieID = $this->params()->fromRoute('param1');
    
        // TODO - currency details move to session
        $currencyResultSet = $this->CommonTable('Core\Model\CurrencyTable')->fetchAll();
        $currencies = array();
        $sup = array();
        $tax = array();
        $ctax = array();
        $productType = array();
        $location = array();
        $currencies['0'] = '';
        foreach ($currencyResultSet as $row) {
            $currencies[$row->currencyID] = $row->currencyName;
        }
        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

        //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        foreach ($paymentTermsResultSet as $row) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }
        $today = date('Y-m-d');
        $promotions = $this->CommonTable('Settings\Model\PromotionTable')->getToDayPromotions($today, $locationID);
        $promotionData = array();
        foreach ($promotions as $pro) {
            $pro = (object) $pro;
            $promotionData[$pro->promotionID] = $pro;
        }

        $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAll();
        foreach ($supplier as $r) {
            $sup[$r->supplierID] = $r->supplierName;
        }

        $tax_resource = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
        foreach ($tax_resource as $t) {
            $tax[$t->id] = $t->taxName;
            if ($t->taxType == 'c') {
                $ctax[$t->id] = $t->taxName;
            }
        }

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $locations = $this->CommonTable('Core\Model\LocationTable')->fetchAll();
        foreach ($locations as $r) {
            $r = (object) $r;
            $location[$r->locationID] = $r->locationName;
        }

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();
        //get price list
        $priceList = $this->getActivePriceList();

        // Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        // get person Title (Mr, Miss ..) from personTitle.config file
        $config = $this->getServiceLocator()->get('config');
        $customerTitle = $config['personTitle'];
        $countries = ['' => ''] + $config['all_countries'];

//        get customer Code
        $cusRefData = $this->getReferenceNoForLocation(20, $locationID);
        $cusRid = $refData["refNo"];
        $locationReferenceID = $cusRefData["locRefID"];
        $customerCode = $this->getReferenceNumber($locationReferenceID);

        $userdateFormat = $this->getUserDateFormat();
        $currencyValue = $this->getDefaultCurrency();
        $customerForm = new AddCustomerForm(NULL, $paymentTerms, $currencies, $customerTitle, $currencyValue,$customerCategories, $userdateFormat, null,[], $customerPriceLists);
        $customerForm->get('customerFormsubmit')->setValue(_("Add New Customer"));
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' => $this->useAccounting
        ));
        $customerAddView->setTemplate('invoice/customer/customer-add-modal');

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');


        $serviceChargeView = new ViewModel();
        $serviceChargeView->setTemplate('invoice/invoice/add-service-charge-modal');

        $viewMrpValluesView = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $viewMrpValluesView->setTemplate('invoice/invoice/view-mrp-values-modal');


        $freeIssueItemModal = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $freeIssueItemModal->setTemplate('invoice/invoice/free-issue-item-modal');

        $financeAccountsArray = [];
        $financeAccounts = $this->CommonTable('Accounting\Model\FinanceAccountsTable')->fetchAll();
        foreach ($financeAccounts as $key => $value) {
            $financeAccountsArray[$value['financeAccountsID']] = $value;
        }

        $productForm = new ProductForm(array(
            'supplier' => $sup,
            'tax' => $tax,
            'productType' => $productType,
            'location' => $location,
        ));

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $productSalesAccountName = $financeAccountsArray[$productSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productSalesAccountID]['financeAccountsName'];
                $productForm->get('productSalesAccountID')->setAttribute('data-id',$productSalesAccountID);
                $productForm->get('productSalesAccountID')->setAttribute('data-value',$productSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $productInventoryAccountName = $financeAccountsArray[$productInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productInventoryAccountID]['financeAccountsName'];
                $productForm->get('productInventoryAccountID')->setAttribute('data-id',$productInventoryAccountID);
                $productForm->get('productInventoryAccountID')->setAttribute('data-value',$productInventoryAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $productCOGSAccountName = $financeAccountsArray[$productCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productCOGSAccountID]['financeAccountsName'];
                $productForm->get('productCOGSAccountID')->setAttribute('data-id',$productCOGSAccountID);
                $productForm->get('productCOGSAccountID')->setAttribute('data-value',$productCOGSAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $productAdjusmentAccountName = $financeAccountsArray[$productAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productAdjusmentAccountID]['financeAccountsName'];
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-id',$productAdjusmentAccountID);
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-value',$productAdjusmentAccountName);
            }
        }

        $productForm->get('submit')->setValue('Save');
        $productForm->get('productState')->setValue('Active');
        $productForm->setAttribute('id', 'create-product-form');

        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $defaultCategoryID = array_search('General', $allCategoryList);
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        $defaultUomID = $this->CommonTable('Settings\Model\UomTable')->getUomByName('Unit')->uomID;
        $categoryForm = new CategoryForm($allCategoryList);
        $categoryForm->get('categoryParentID')->setAttribute('data-id', $defaultCategoryID);
        $productHandelingForm = new ProductHandelingForm();
        $productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", true);
        $createProductView = new ViewModel(array(
            'productForm' => $productForm,
            'productHandelingForm' => $productHandelingForm,
            'categoryForm' => $categoryForm,
            'uomList' => $uoms,
            'tax' => $tax,
            'ctax' => $ctax,
            'location' => $location,
            'defaultUom' => $defaultUomID,
            'useAccounting' => $this->useAccounting
        ));
        $createProductView->setTemplate('invoice/invoice-api/create-product');
        $userID = $this->user_session->userID;
        $currentSalesPersonID = $this->CommonTable('User/Model/SalesPersonTable')->getSalesPersonIDByActiveUserId($userID);

        $attrDetails = [];
        $itemAttrDataSet = $this->CommonTable('/settings/Model/itemAttributeValueTable')->selectAll();
        foreach ($itemAttrDataSet as $key => $value) {
            $attrDetails[$value['itemAttributeID']] = array(
                'itemAttributeID' => $value['itemAttributeID'],
                'description' => $value['itemAttributeCode']." - ".$value['itemAttributeName'],
                );
        }
        $displaySetupDetails = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();
        $invoiceForm = new InvoiceForm(
                array(
            'paymentTerms' => $paymentTerms,
            'salesPersons' => $salesPersons,
            'promotions' => $promotionData,
            'customCurrency' => $currency,
            'priceList' => $priceList
                )
        );

        $displaySetup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll()->current();

        $invoiceForm->get('customCurrencyId')->setAttribute('data-baseid', $displaySetup->currencyID);
        $invoiceForm->get('discount')->setAttribute('id', 'deliveryNoteDiscount');
        $invoiceForm->get('salesPersonID')->setValue($currentSalesPersonID['salesPersonID']);
        $invoiceForm->get('customCurrencyId')->setValue($displaySetup->currencyID);

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/modalAddCustomer.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/invoice.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/calculations.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/preview-document.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/product.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/category.js');

        $checkIsSalesInvoiceApprovalActive = $this->CommonTable('Settings/Model/ApprovalDocumentDetailsTable')->checkDocIsActiveForApprovalWF(1);
        $approvalWorkflows = [];

        if ($checkIsSalesInvoiceApprovalActive['isActive']) {
            $getApprovalWorkflows = $this->CommonTable('Settings/Model/ApprovalWorkflowsTable')->getApprovalWorkflowsByDocID(1);

            foreach ($getApprovalWorkflows as $key => $value) {
                $approvalWorkflows[] = $value;
            }

        }

        $invoiceAddView = new ViewModel(
                array(
                    'invoiceForm' => $invoiceForm,
                    'locationID' => $locationID,
                    'locationName' => $locationName,
                    'locationCode' => $locationCode,
                    'referenceNumber' => $rid,
                    'suspendedTaxReferenceNumber' => $strid,
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,
                    'userdateFormat' => $userdateFormat,
                    'useAccounting' => $this->useAccounting,
                    'packageID' => $this->packageID,
                    'epttInvoice' => (is_null($tempInvoieID)) ? 0 : $tempInvoieID,
                    'attrDetails' => $attrDetails,
                    'salesPersons' => $salesPersons,
                    'isCanCreateItem' => $isCanCreateItem,
                    'isEnableForProductWiseSalesPerson' => $this->user_session->productWiseSalesPersonEnable,
                    'isUseMrpValues' => $displaySetupDetails['useMrpSettings'],
                    'isActiveApproval' => $checkIsSalesInvoiceApprovalActive['isActive'],
                    'approvalWorkflows' => $approvalWorkflows
                )
        );

        $creditLimitValidationView = new ViewModel();
        $creditLimitValidationView->setTemplate('invoice/invoice/confirm-credit-limit-exceed-box');

        // TODO - fix customer add
        $invoiceAddView->addChild($createProductView, 'createProductView');
        $invoiceAddView->addChild($customerAddView, 'customerAddView');
        $invoiceAddView->addChild($dimensionAddView, 'dimensionAddView');
        $invoiceAddView->addChild($serviceChargeView, 'serviceChargeView');
        $invoiceAddView->addChild($viewMrpValluesView, 'viewMrpValluesView');
        $invoiceAddView->addChild($freeIssueItemModal, 'freeIssueItemModal');
        $invoiceAddView->addChild($creditLimitValidationView, 'creditLimitValidationView');

        $barCodeAddModal = new ViewModel();
        $barCodeAddModal->setTemplate('/inventory/product/add-bar-codes-modal');
        $invoiceAddView->addChild($barCodeAddModal, 'barCodeAddModal');
        
        $invoiceProductsView = new ViewModel();
        $invoiceProductsView->setTemplate('invoice/invoice/invoice-note-add-products');
        $invoiceAddView->addChild($invoiceProductsView, 'invoiceProducts');
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Invocie Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Invoice Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $invoiceAddView->addChild($refNotSet, 'refNotSet');
        }

        $nextInvoiceDateView = new ViewModel(array(
            'userdateFormat' => $userdateFormat
        ));
        $nextInvoiceDateView->setTemplate('invoice/invoice/add-membership-modal');
        $invoiceAddView->addChild($nextInvoiceDateView, 'nextInvoiceDateView');

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories', 'uomByID', 'taxes'
        ]);
        $this->setLogMessage("Invoice Create Page Accessed");
        return $invoiceAddView;
    }

    public function viewAction()
    {
        $this->setLogMessage("Invoice View List accessed");
        $this->getSideAndUpperMenus('Invoices', 'View Invoices', 'SALES');
        $posflag = 0;
        $this->getPaginatedInvoices(15, $posflag, $considerJob = true);
        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        $dateFormat = $this->getUserDateFormat();
        $showVehicleNum = false;
        if($displaySettings->jobModuleEnabled == 1 &&  $displaySettings->jobBusinessType == 2) {
            $showVehicleNum = true;
        }

        $invoiceView = new ViewModel(array(
            'invoices' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat,
            'controller' => $this,
            'showVehicleNum' => $showVehicleNum,
            'companyCurrencySymbol' => $this->companyCurrencySymbol
                )
        );
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('invoice/invoice/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/invoice/document-view');
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $invoiceView->addChild($attachmentsView, 'attachmentsView');
        $invoiceView->addChild($docHistoryView, 'docHistoryView');
        $invoiceView->addChild($documentView, 'documentView');

        return $invoiceView;
    }

    public function viewPosListAction()
    {
        $this->setLogMessage("Invoice View List accessed");
        $this->getSideAndUpperMenus('Invoices', 'View POS Invoices', 'SALES');
        $posflag = 1;
        $this->getPaginatedInvoices(15, $posflag);
        $dateFormat = $this->getUserDateFormat();

        $invoiceView = new ViewModel(array(
            'invoices' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat,
            'companyCurrencySymbol' => $this->companyCurrencySymbol
                )
        );

        return $invoiceView;
    }

    public function documentAction()
    {
        $invoiceID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Sales Invoice';


        // if 'pos' was passed as 2nd parameter, template ID is the next parameter
        if ($templateID == 'pos') {
            $documentType = 'POS Printout';
            $templateID = $this->params()->fromRoute('param3');
        }
        else if ($templateID == 'pos-inv-only') {
            $documentType = 'POS (invoice only)';
            $templateID = $this->params()->fromRoute('param3');
        }
        else if ($templateID == 'pos-pay-only') {
            $documentType = 'POS (pay only)';
            $templateID = $this->params()->fromRoute('param3');
        }
        else if ($invoiceID == 'pos') {
            $documentType = 'POS Printout';
            $invoiceID = $this->params()->fromRoute('param2');
            $templateID = $this->params()->fromRoute('param3');
        }


        echo $this->generateDocumentWithPaymentDetails($invoiceID, $documentType, $templateID, true);
        exit;
    }

    public function _getDocumentPaymentTable($paymentIDs, $documentSize = 'A4') {
        $data_table_vars = array();
        $currencySymbol = null;
        $total = 0.0;
        $discountTot = 0.0;
        $i = 0;
        if(count($paymentIDs) > 0 ){
            foreach ($paymentIDs as $pi){
                $data_table_vars[] = $this->_getDataForPaymentView($pi);
                // $leftToPay = $data_table_vars[$i]['leftToPay'];
                $total += $data_table_vars[$i]['totalPaymentAmount'];
                // $discountTot += $data_table_vars[$i]['cr_discount'];
                $i++;
            }
        }

        $view = new ViewModel(
            array(
                'paymentDetails' => $data_table_vars,
                'settledTotal' => $total,
                'discountTotal' => $discountTot,
                'leftToPay' => $leftToPay
            ));
        $view->setTemplate('/invoice/invoice/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    public function _getDataForPaymentView($paymentID) {
        $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsByPaymentID($paymentID)->current();
        $paymentMethods = $this->CommonTable('Invoice\Model\IncomingPaymentMethodTable')->getPaymentMethodAllDetailsByPaymentId($paymentID);


        $currencySymbol = $this->companyCurrencySymbol;
        if ($paymentData['customCurrencyId'] != '') {
            $currencySymbol = $paymentData['currencySymbol'];
        }

        $paymentMethodData = [];
        foreach ($paymentMethods as $key2 => $value2) {
            $value = (object) $value2;
            $method = 0;
            $methodID = 0;
            $increment++;
            $name = '';
            if ($value->incomingPaymentMethodCashId != null) {
                $method = 1;
                $methodID = $value->incomingPaymentMethodCashId;
                $name = 'Cash';
                $temp = [
                    'paymentMethod' => $name,
                    'paymentAmount' => $currencySymbol.' '.number_format(($value->incomingPaymentMethodAmount), 2),
                ];

            } else if ($value->incomingPaymentMethodChequeId != null) {
                $method = 2;
                $methodID = $value->incomingPaymentMethodChequeId;
                $name = 'Cheque';
                $temp = [
                    'paymentMethod' => $name,
                    'paymentAmount' => $currencySymbol.' '.number_format(($value->incomingPaymentMethodAmount), 2), 
                    'bank' => (!empty($value->incomingPaymentMethodChequeBankName)) ? $value->incomingPaymentMethodChequeBankName : '-',
                    'chequeNumber' => (!empty($value->incomingPaymentMethodChequeNumber)) ? $value->incomingPaymentMethodChequeNumber : '-',
                ];

            } else if ($value->incomingPaymentMethodCreditCardId != null) {
                $method = 3;
                $methodID = $value->incomingPaymentMethodCreditCardId;
                $name = 'Credit Card';
                $temp = [
                    'paymentMethod' => $name,
                    'paymentAmount' => $currencySymbol.' '.number_format(($value->incomingPaymentMethodAmount), 2), 
                ];
            } else if ($value->incomingPaymentMethodLoyaltyCardId != null) {
                $method = 4;
                $methodID = $value->incomingPaymentMethodLoyaltyCardId;
                $name = 'Loyalty Card';
                $temp = [
                    'paymentMethod' => $name,
                    'paymentAmount' => $currencySymbol.' '.number_format(($value->incomingPaymentMethodAmount), 2), 
                ];
            } else if ($value->incomingPaymentMethodBankTransferId != null) {
                $method = 5;
                $methodID = $value->incomingPaymentMethodBankTransferId;
                $name = 'Bank Transfer';
                $temp = [
                    'paymentMethod' => $name,
                    'paymentAmount' => $currencySymbol.' '.number_format(($value->incomingPaymentMethodAmount), 2), 
                ];
            } else if ($value->incomingPaymentMethodGiftCardId != null) {
                $method = 6;
                $methodID = $value->incomingPaymentMethodGiftCardId;
                $name = 'Gift Card';
                $temp = [
                    'paymentMethod' => $name,
                    'paymentAmount' => $currencySymbol.' '.number_format(($value->incomingPaymentMethodAmount), 2), 
                ];
            } else if ($value->incomingPaymentMethodLCId != null) {
                $method = 7;
                $methodID = $value->incomingPaymentMethodLCId;
                $name = 'LC';
                $temp = [
                    'paymentMethod' => $name,
                    'paymentAmount' => $currencySymbol.' '.number_format(($value->incomingPaymentMethodAmount), 2), 
                ];
            } else if ($value->incomingPaymentMethodTTId != null) {
                $method = 8;
                $methodID = $value->incomingPaymentMethodTTId;
                $name = 'TT';
                $temp = [
                    'paymentMethod' => $name,
                    'paymentAmount' => $currencySymbol.' '.number_format(($value->incomingPaymentMethodAmount), 2), 
                ];
            }

            $paymentMethodData[] = $temp;
        }

        if (!empty($paymentData['incomingPaymentCreditAmount'])) {
            $temp = [
                'paymentMethod' => 'Credit',
                'paymentAmount' => $currencySymbol.' '.number_format(($paymentData['incomingPaymentCreditAmount']), 2), 
            ];
            $paymentMethodData[] = $temp;
        }



        $banks = $this->CommonTable('Expenses\Model\BankTable')->fetchAll(false, array('bank.bankName' => 'ASC'));
        foreach ($banks as $t) {
            $t = (object) $t;
            $bank[$t->bankId] = $t->bankName;
        }

        $gCard = $this->CommonTable('Inventory\Model\GiftCardTable')->getAllOpenGiftCards();
        foreach ($gCard as $g) {
            $g = (object) $g;
            $giftCards[$g->giftCardId] = $g->giftCardCode;
        }

        $currencyRate = 1;
        if ($paymentData['incomingPaymentCustomCurrencyRate'] > 0) {
            $currencyRate = $paymentData['incomingPaymentCustomCurrencyRate'];
        }
       
        $paymentFinalData = array(
            'paymentMethods' => $paymentMethodData,
            'paymentCode' => $paymentData['incomingPaymentCode'],
            'paymentDate' => $paymentData['incomingPaymentDate'],
            'totalPaymentAmount' => number_format($paymentData['incomingPaymentAmount'], 2),
            'paymentId' => $paymentID,
            'discount' => $currencySymbol.' '.number_format($paymentData['incomingPaymentDiscount'], 2)
        );

        return $paymentFinalData;

    }


    public function documentPdfAction()
    {
        $invoiceID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        $documentType = 'Sales Invoice';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);

        $pathToDocument = '/invoice/invoice/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($invoiceID);

        if ($documentData['inclusiveTax'] == "1" && $documentData['pos'] == "0") {
            $documentData['salesinvoiceTotalAmount'] = $documentData['templateInvoiceTotal'];
        } else {
            $documentData['salesinvoiceTotalAmount'] = $documentData['salesinvoiceTotalAmount'];
        }

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($invoiceID, $documentType, $documentData, $templateID);

        return;
    }

    public function getDataForDocument($invoiceID)
    {
        if (!empty($this->_invoiceViewData)) {
            return $this->_invoiceViewData;
        }
        $itemAttrDetails = $this->CommonTable('Settings\Model\ItemAttributeTable')->fetchAll();
        $itemAttrDetailArray = [];
        foreach ($itemAttrDetails as $value) {
            $itemAttrDetailArray[$value['itemAttributeID']] = $value['itemAttributeName'];
        }

        $data = $this->getDataForDocumentView();

        $invoiceDetails = (array) $this->CommonTable('Invoice\Model\InvoiceTable')->getAllInvoiceDetailsByInvoiceID($invoiceID)->current();

        if (!empty($invoiceDetails['soNum'])) {
            $getSoData = (array) $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($invoiceDetails['soNum']);
            $invoiceDetails['soNum'] = $getSoData['soCode'];
        } else {
            $invoiceDetails['soNum'] = "-";  
        }


        if ($invoiceDetails['salesInvoiceServiceChargeAmount'] > 0) {
            $relatedServiceCharges = $this->CommonTable('Invoice\Model\InvoiceServiceChargeTable')->getInvoiceChargesByInvoiceID($invoiceID);
            $serviceChargeString = "";
            if (sizeof($relatedServiceCharges) > 0) {
                foreach ($relatedServiceCharges as $key8 => $value8) {
                    $tempString = $value8['serviceChargeTypeName'].'-'.$value8['serviceChargeTypeCode']. ' : '.number_format($value8['serviceChargeAmount'],2);
                    $serviceChargeString .= "<small> - " . $tempString . "</small><br>";
                }
            }

        }

        $invoiceDetails['serviceChargeDetails'] = $serviceChargeString;
        $invoiceDetails['salesInvoiceIssuedDate'] = $this->convertDateToUserFormat($invoiceDetails['salesInvoiceIssuedDate']);
        $invoiceDetails['salesInvoiceOverDueDate'] = $this->convertDateToUserFormat($invoiceDetails['salesInvoiceOverDueDate']);
        $invoiceDetails['createdTimeStamp'] = $this->getUserDateTime($invoiceDetails['createdTimeStamp']);
        $customCurrencyRate = ($invoiceDetails['salesInvoiceCustomCurrencyRate'] != 0) ? $invoiceDetails['salesInvoiceCustomCurrencyRate'] : 1;
        $invoiceDetails['customerProfile'] = $invoiceDetails['customerProfileName'];
        $invoiceDetails['location_name'] = $invoiceDetails['locationName'];

        $invoiceSPDetails = $this->CommonTable('Invoice\Model\InvoiceSalesPersonsTable')->getallsSalesPersonByInvoiceID($invoiceID);
        $tempSPString = "";
        if (sizeof($invoiceSPDetails) > 0) {
            foreach ($invoiceSPDetails as $key => $value) {

                if ($key == (sizeof($invoiceSPDetails) -1)) {
                    $tempSPString .= $value['salesPersonSortName'];
                } else {
                    $tempSPString .= $value['salesPersonSortName'].',';
                }
            }
        }
               
        $invoiceDetails['salesPersonSortName'] = $tempSPString;

        if (sizeof($invoiceSPDetails) == 1) {
            $invoiceDetails['salesPersonAddress'] = (!empty($invoiceSPDetails[0]['salesPersonAddress']))  ? $invoiceSPDetails[0]['salesPersonAddress'] : "";
        } else {
            $invoiceDetails['salesPersonAddress'] = "";
        }

        // replace customer Address and Email from primary profile of current customer
        $addressArray = array(
            trim($invoiceDetails['customerProfileLocationNo'], ','),
            trim($invoiceDetails['customerProfileLocationRoadName1'], ','),
            trim($invoiceDetails['customerProfileLocationRoadName2'], ','),
            trim($invoiceDetails['customerProfileLocationRoadName3'], ','),
            trim($invoiceDetails['customerProfileLocationSubTown'], ','),
            trim($invoiceDetails['customerProfileLocationTown'], ','),
            trim($invoiceDetails['customerProfileLocationPostalCode'], ','),
            trim($invoiceDetails['customerProfileLocationCountry'], ','),
        );

        if (!empty($invoiceDetails['jobID'])) {
            $jobDetails = (array) $this->CommonTable('Jobs\Model\JobTable')->getServiceJobById($invoiceDetails['jobID']);
            $invoiceDetails['jobCode'] = $jobDetails['jobReferenceNumber'];
            $invoiceDetails['vehicleRegNo'] = $jobDetails['serviceVehicleRegNo'];
            $invoiceDetails['vehicleType'] = $jobDetails['vehicleTypeName'];
            $invoiceDetails['mileage'] = $jobDetails['jobVehicleKMs'];
            $invoiceDetails['jobInTime'] = $this->getUserDateTime($jobDetails['createdTimeStamp']);
            $invoiceDetails['jobOutTime'] = $this->getUserDateTime($jobDetails['completed_at']);
        }

        $address = implode(', ', array_filter($addressArray));
        $invoiceDetails['customerAddress'] = ($address != '') ? $address . '.' : $address;
        $invoiceDetails['customerEmail'] = $invoiceDetails['customerProfileEmail'];
        $invoiceDetails['customerGender'] = (!empty($invoiceDetails['customerGender'])) ? ucfirst($invoiceDetails['customerGender']): '-';
        $invoiceDetails['customerDOB'] = $invoiceDetails['customerDateOfBirth'];
        $invoiceDetails['customerAge'] = "-";

        if (!empty($invoiceDetails['customerDateOfBirth'])) {
            $birthday_timestamp = strtotime($invoiceDetails['customerDateOfBirth']);  
            $age = date('md', $birthday_timestamp) > date('md') ? date('Y') - date('Y', $birthday_timestamp) - 1 : date('Y') - date('Y', $birthday_timestamp);
            $invoiceDetails['customerAge'] = $age;
        }

        $invoiceDetails['customerCreditLimit'] = $data['currencySymbol'] . ' ' . number_format($invoiceDetails['customerCreditLimit'], 2);
        $invoiceDetails['customerCurrentBalance'] = $data['currencySymbol'] . ' ' . number_format($invoiceDetails['customerCurrentBalance'], 2);
        $invoiceDetails['customerCurrentCredit'] = $data['currencySymbol'] . ' ' . number_format($invoiceDetails['customerCurrentCredit'], 2);

        //use to divide date and time from timeStamp
        $data['current_time'] = date("h:i:sA",strtotime($invoiceDetails['createdTimeStamp']));
        $data = array_merge($data, $invoiceDetails);
        $data['currencyRate'] = $customCurrencyRate;
        $data['currencySymbol'] = ($data['currencySymbol'] != '') ? $data['currencySymbol'] : $data['companyCurrencySymbol'];

        //payment details
        $paymentDetails = (object) $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getPaymentsDetailsByInvoiceID($invoiceID)->current();

        // Invoice product details
        $forValidateInv = true;
        $products = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getProductsByInvoiceID($invoiceID, $forValidateInv);
        $forValidateInv = false;

        //get all tax details
        $taxDetails = $this->CommonTable('Settings\Model\TaxCompoundTable')->fetchAll();
        $compoundTaxes = [];

        foreach ($taxDetails as $key => $taxValue) {
            $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;

        }


        $sub_total = 0;
        $productsTax = $this->CommonTable('Invoice\Model\InvoiceProductTaxTable')->getInvoiceTax($invoiceID);
        $txTypes = array();
        $totalTax = 0;
        $calculatableTaxesTotal = 0;
        $suspendedTaxesTotal = 0;
        $normalTaxTotal = 0;
        $compoundTaxTotal = 0;
        $calculatableTaxes = array();
        $suspendableTaxes = array();
        $normalTaxesSet = array();
        $compoundTaxesSet = array();
        foreach ($productsTax as $productTax) {

            $thisTax = $productTax['salesInvoiceProductTaxAmount'] / $customCurrencyRate;
            if(array_key_exists($productTax['taxID'], $compoundTaxes)){
                    $compoundTaxTotal += floatval($thisTax);
                    $compoundTaxesSet[] = $productTax['taxName'];
            } else {
                    $normalTaxTotal += floatval($thisTax);
                    $normalTaxesSet[] = $productTax['taxName'];
            }


            $txTypes[$productTax['taxName']] = (!isset($txTypes[$productTax['taxName']])) ? $thisTax : $txTypes[$productTax['taxName']] + $thisTax;
            $calculatableTaxesTotal += ($productTax['taxSuspendable'] != 1) ? $thisTax : 0;
            $suspendedTaxesTotal += ($productTax['taxSuspendable'] == 1) ? $thisTax : 0;
            $totalTax += $thisTax;

            if ($productTax['taxSuspendable'] == 1) {
                if ($invoiceDetails['salesInvoiceSuspendedTax'] == 1) {
                    $suspendableTaxes[] = $productTax['taxName'];                        // }
                }else{
                    $calculatableTaxes[] = $productTax['taxName'];
                }
            } else {
                $calculatableTaxes[] = $productTax['taxName'];
            }
        }

        $data['calculatableTaxesTotal'] = $calculatableTaxesTotal;
        $data['suspendedTaxesTotal'] = $suspendedTaxesTotal;
        $data['normalTaxTotal'] = number_format($normalTaxTotal, 2);
        $data['normalTaxesSet'] = (!empty($normalTaxesSet)) ? implode(', ', array_unique($normalTaxesSet)) : '';
        $data['compoundTaxesSet'] = (!empty($compoundTaxesSet)) ? implode(', ', array_unique($compoundTaxesSet)) : '';
        $data['compoundTaxTotal'] = number_format($compoundTaxTotal, 2);
        $data['calculatableTaxes'] = (!empty($calculatableTaxes)) ? implode(', ', array_unique($calculatableTaxes)) : '';
        $data['suspendableTaxes'] = (!empty($suspendableTaxes)) ? implode(', ', array_unique($suspendableTaxes)) : '';

        $invoiceDetails = (object) $invoiceDetails;
        $totalDiscount = 0;

        // get template specific data for the document
        $templateDefaultOptions = array('product_table' => $this->getServiceLocator()->get('config')['template-data-table']['invoice']);

        // get template options from database and override the default options
        $templateOptions = array_replace_recursive($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);

        $defaultFooterRows = [];
        foreach ($templateDefaultOptions['product_table']['rows'] as $row) {
            $defaultFooterRows[$row['name']] = $row;
        }

        $templateFooterRows = [];
        foreach ((array) $this->templateDetails['templateOptions']['product_table']['rows'] as $row) {
            $templateFooterRows[$row['name']] = $row;
        }

        $templateOptions['product_table']['rows'] = $templateFooterRows + $defaultFooterRows;

        $data['templateOptions'] = $templateOptions;
        $data['templateOptions'] = $templateOptions;

        // reorder columns
        $productsTableColumns = array_filter($templateOptions['product_table']['columns'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableColumns, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        // reorder rows
        $productsTableRows = array_filter($templateOptions['product_table']['rows'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableRows, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        $templateOptions['product_table']['columns'] = $productsTableColumns;
        $templateOptions['product_table']['rows'] = $productsTableRows;

        $allowedColumnsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['columns']);
        $allowedRowsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['rows']);

        // to prevent loading categories from the database in the following foreach loop
        $allCategories = array();
        if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
            $allCategories = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        }
        
        $allDln = array();
        if ($templateOptions['categorize_products_by_dln'] && $templateOptions['categorize_products_by_dln'] != "false") {
            $allDln = $this->CommonTable('Invoice/Model/DeliveryNoteTable')->getDeliveryNotes();
        }

        // Get Invoice serial product
        $invoiceSerialProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceSerialProductByInvoiceId($invoiceID);
        $invoiceBatchProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceBatchProductByInvoiceId($invoiceID);

        $invoiceSubProducts = [];
        foreach ($invoiceBatchProducts as $subProduct) {
            $invProdId = $subProduct['salesInvoiceProductID'];
            $prodBatchId = $subProduct['productBatchID'];
            $prodBatchCode = $subProduct['productBatchCode'];
            $productBatchExpiryDate = $subProduct['productBatchExpiryDate'];
            $batchData = [
                'code' => $prodBatchCode,
                'expiryDate' => $productBatchExpiryDate,
                'qty' => $subProduct['salesInvoiceSubProductQuantity'],
                'type' => 'batch'
            ];

            // For batch serial items
            // Eg: if 5 serial items from 2 batches were invoiced,
            // check if batch data is already in array and increment it's quantity by 1
            if ($subProduct['serialProduct'] == 1 && isset($invoiceSubProducts[$invProdId][$prodBatchId])) {
                $batchData['qty'] = $invoiceSubProducts[$invProdId][$prodBatchId]['qty'] + $batchData['qty'];
            }

            $invoiceSubProducts[$invProdId][$prodBatchId] = $batchData;
        }

        foreach ($invoiceSerialProducts as $subProduct) {
            $invProdId = $subProduct['salesInvoiceProductID'];
            $prodSerialId = $subProduct['productSerialID'];
            $prodSerialCode = $subProduct['productSerialCode'];
            $prodBatchId = $subProduct['productBatchID'];
            $productSerialExpireDate = $subProduct['productSerialExpireDate'];

            $warranty = $this->calculateWarrantyPeriod($subProduct['salesInvoiceSubProductWarranty'],$subProduct['salesInvoiceSubProductWarrantyType']);     
            $serialData = [
                'code' => $prodSerialCode,
                'expireDate' => $productSerialExpireDate,
                'warranty' => $warranty,
                'type' => 'serial'
            ];

            if ($prodBatchId) {
                $invoiceSubProducts[$invProdId][$prodBatchId]['type'] = 'batch_serial';
                $invoiceSubProducts[$invProdId][$prodBatchId]['serial'][$prodSerialId] = $serialData;
            } else {
                $invoiceSubProducts[$invProdId][$prodSerialId] = $serialData;
            }
        }

// //      Get Invoice serial product
//         $invoiceSubProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceSerialProductByInvoiceId($invoiceID);
//         foreach ($invoiceSubProducts as $subProduct) {
//             $subProductList[$subProduct['salesInvoiceProductID']][$subProduct['salesInvoiceSubProductID']] = 'Serial NO: ' . $subProduct['productSerialCode'] . ', Warranty: ' . $this->calculateWarrantyPeriod($subProduct['productSerialWarrantyPeriod']);
//         }

        //get item Details that related to the given item wise promotion.
        if ($data['promotionID']) {
            $promotionItemDetails = $this->CommonTable('Settings\Model\PromotionTable')->getItemWisePromotionDetails($data['promotionID']);
            $itemWisePromoDetails = [];
        }

       $subTotalWithoutDiscount = 0;

        foreach ($products as $productIndex => $product) {
            $product = (object) $product;

            $item_salesPerson = "";
            if (!empty($product->itemSalesPersonID)) {
                $salesPerson = $this->CommonTable('User\Model\SalesPersonTable')->getSalesPersonByID($product->itemSalesPersonID)->current();
                $salesPersonDetail = $salesPerson['salesPersonSortName'].' '.$salesPerson['salesPersonLastName'];
                // $item_salesPerson = "<br><small>(" . implode("<br>", $salesPersonDetail) . ")</small>";
            }


            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID, $product->salesInvoiceProductSelectedUomId);
            $qty = ($product->salesInvoiceProductQuantity == 0) ? 1 : $product->salesInvoiceProductQuantity; // as a precaution
            $item_tot_tax = ($product->taxTotal > 0) ? $this->getProductUnitPriceViaDisplayUom((($product->taxTotal / $customCurrencyRate) / $product->salesInvoiceProductQuantity), $productUom) : 0.00;
            $item_tax_string = ($item_tot_tax > 0) ? "Tax: " . $data['currencySymbol'] . number_format($item_tot_tax, 2) : "";
            if ($product->salesInvoiceProductDiscountType == 'value') {
                $item_dic_string = ($product->salesInvoiceProductDiscount > 0) ? "Disc: " . $this->getProductUnitPriceViaDisplayUom(($product->salesInvoiceProductDiscount / $customCurrencyRate), $productUom) . " " . $data['currencySymbol'] : "";
                $product->totalDiscount = $qty * ($product->salesInvoiceProductDiscount / $customCurrencyRate);
            } else {
                $item_dic_string = ($product->salesInvoiceProductDiscount > 0) ? "Disc: " . $product->salesInvoiceProductDiscount . "%" : "";
                $product->totalDiscount = (($product->salesInvoiceProductPrice / $customCurrencyRate) * $qty) / 100 * $product->salesInvoiceProductDiscount;
            }

            // check if any subproducts (serial, batch) are available for this product
            $invoiceSubProductsDisplay = [];
            if (isset($invoiceSubProducts[$product->salesInvoiceProductID])) {

                foreach ($invoiceSubProducts[$product->salesInvoiceProductID] as $subProduct) {
                    switch ($subProduct['type']) {
                        case 'batch':
                        // case 'batch_serial':
                            $qty = $this->getProductQuantityViaDisplayUom($subProduct['qty'], $productUom);
                            $displayStrs = [];
                            $displayStrs[] = 'Batch: ' . $subProduct['code'];
                            $displayStrs[] = 'Quantity: ' . $qty['quantity'] . ' ' . $qty['uomAbbr'];
                            $displayStrs[] = ($subProduct['expiryDate']) ? 'Expire Date: ' . $subProduct['expiryDate'] : null;

                            $invoiceSubProductsDisplay[] = implode(', ', $displayStrs);
                            break;
                        case 'serial':
                            $displayStrs = [];
                            $displayStrs[] = 'Serial No: ' . $subProduct['code'];
                            $displayStrs[] = ($subProduct['warranty']) ? 'Warranty: ' . $subProduct['warranty'] : null;
                            $displayStrs[] = ($subProduct['expireDate']) ? 'Expire Date: ' . $subProduct['expireDate'] : null;

                            $invoiceSubProductsDisplay[] = implode(', ', array_filter($displayStrs));
                            break;
                        case 'batch_serial':
                            $qty = $this->getProductQuantityViaDisplayUom($subProduct['qty'], $productUom);
                            $displayStrs = [];
                            $displayStrs[] = 'Batch: ' . $subProduct['code'];
                            $displayStrs[] = 'Quantity: ' . $qty['quantity'] . ' ' . $qty['uomAbbr'];

                            $invoiceSubProductsDisplay[] = implode(', ', $displayStrs);

                            foreach ($subProduct['serial'] as $serialSubProduct) {
                                $displayStrs = [];
                                $displayStrs[] = '- Serial No: ' . $serialSubProduct['code'];
                                $displayStrs[] = ($serialSubProduct['warranty']) ? 'Warranty: ' . $serialSubProduct['warranty'] : null;

                                $invoiceSubProductsDisplay[] = implode(', ', array_filter($displayStrs));
                            }
                            break;
                    }
                }
            }
        // foreach ($invoiceSubProducts['batch'] as $invProdId => $subProducts) {
        //     foreach ($subProducts as $subProduct) {
                // $displayStrs = [];
                // $displayStrs[] = 'Batch: ' . $subProduct['code'];
                // $displayStrs[] = 'Quantity: ' . $subProduct['qty'];

                // $invoiceSubProductsDisplay[$invProdId][] = implode(', ', $displayStrs);
        //     }
        // }

        // echo '<pre>';
        // // print_r($invoiceSubProducts['batch']);
        // print_r($invoiceSubProducts);
        // exit;


            $item_sub_pro_line = "";
            if (!empty($invoiceSubProductsDisplay)) {
                $item_sub_pro_line = "<br><small>(" . implode("<br>", $invoiceSubProductsDisplay) . ")</small>";
            }

            //add sub product serial and warranty details to the product array.
            $product->subProductserialAndWarranty = $item_sub_pro_line;

            $totalDiscount += floatval($product->totalDiscount);
            $sub_line = "";

            $productDescLine = "";

            //Product description line
            if ($product->salesInvoiceProductDescription != '' && $product->salesInvoiceProductDescription != NULL) {
                $productDescLine = "<br /><small class=\"description\"><span>(</span>" . $product->salesInvoiceProductDescription . "<span>)</span></small>";
                $productDiscription = $product->salesInvoiceProductDescription;

            }
            $item_salesPerson = (!empty($salesPersonDetail)) ? "<br/><small><span>(</span>" . $salesPersonDetail . "<span>)</span></small>" : "";

            if ($invoiceDetails->salesInvoiceShowTax == "1") {    // this must be rebuld by adding a column to the database.
                $sub_line_vals = array_filter(array($item_tax_string, $item_dic_string));
                if (!empty($sub_line_vals)) {
                    $sub_line = "<br /><small class=\"taxes\">(" . implode(",&nbsp;", $sub_line_vals) . ")</small>";
                }
                $product->unitPrice = ($product->salesInvoiceProductPrice / $customCurrencyRate);

            } else {
                $product->unitPrice = (floatval($product->salesInvoiceProductTotal / $customCurrencyRate) + floatval($product->totalDiscount)) / floatval($qty);
                $sub_line = ($product->salesInvoiceProductDiscount > 0) ? "<br><small>(" . $item_dic_string . ")</small>" : "";
            }
            // get template options from database and override the default options
            $templateDefaultOptions = array(
                'categorize_products' => false
            );


            //use to set discount and unite price when product tax exists.
            $totalTaxValuePerItem = 0;

            if ($product->productTaxEligible == '1') {
                if ($product->salesInvoiceProductDiscountType == 'precentage' && $invoiceDetails->salesInvoiceShowTax != "1") {
                    $totalTaxValuePerItem = ((floatval($product->taxTotal / $customCurrencyRate) * 100) / (100 - floatval($product->salesInvoiceProductDiscount))) / floatval($product->salesInvoiceProductQuantity);
                    $product->unitPrice = floatval($totalTaxValuePerItem) + floatval($product->salesInvoiceProductPrice / $customCurrencyRate);
                    $product->totalDiscount = ($product->unitPrice - (floatval($product->salesInvoiceProductTotal / $customCurrencyRate) / floatval($product->salesInvoiceProductQuantity))) * floatval($product->salesInvoiceProductQuantity);
                }
            }

            $unitPrice = $this->getProductUnitPriceViaDisplayUom(($product->unitPrice), $productUom);
            $mrpPrice = (!empty($product->salesInvoiceProductMrpType)) ? $this->getProductUnitPriceViaDisplayUom(($product->salesInvoiceProductMrpAmount), $productUom) : '0.00';
            $thisqty = $this->getProductQuantityViaDisplayUom($product->salesInvoiceProductQuantity, $productUom);
            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

            $lineItemDiscount = ($product->salesInvoiceProductDiscount > 0) ? (($product->salesInvoiceProductDiscountType == 'value') ? number_format($product->totalDiscount, 2) : $product->salesInvoiceProductDiscount . '%') : '-';
            $itemTotalWithoutDiscount = (($product->salesInvoiceProductPrice * $product->salesInvoiceProductQuantity) + $product->taxTotal) / $customCurrencyRate;

            $data['applied_pos_invoice_discount'] = ($data['pos'] && $data['salesInvoiceTotalDiscountType'] == 'presentage' && $data['salesInvoiceDiscountRate'] > 0) ? true : false;

            if($data['applied_pos_invoice_discount']){
                if(!$data['discountOnlyOnEligible'] || ($data['discountOnlyOnEligible'] && $product->productDiscountEligible)){
                    $product->productCode = "*" . $product->productCode;
                }
            }
            //calculate tax value per item
            $itemUnitTax = floatval($product->taxTotal / $customCurrencyRate)/ floatval($product->salesInvoiceProductQuantity);

            //calculate discount value per item
            $itemUnitDisc = floatval($product->totalDiscount / $customCurrencyRate)/ floatval($product->salesInvoiceProductQuantity);

            //calculate unit tax for real item price
            $realUnitTax = $this->calculateProductUnitTax($product->salesInvoiceProductPrice, $product->productID);
            $attribute_01 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeOneID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_02 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeTwoID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_03 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeThreeID,$product->productID)->current()['itemAttributeValueDescription'];

            $product_record = array(
                'product_index' => $productIndex + 1,
                'product_code' => $product->productCode,
                'product_name' => $product->productName . $productDescLine . $sub_line . $item_sub_pro_line . $item_salesPerson,
                'product_description' => $productDiscription,
                'product_code_and_name' => $product->productCode . ' - ' . $product->productName . $productDescLine . $sub_line . $item_sub_pro_line,
                'quantity' => $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'],
                'price' => number_format($unitPrice, 2),
                'mrp-price' => number_format($mrpPrice, 2),
                'price_w_tax' => number_format((floatval($product->salesInvoiceProductPrice) + floatval($realUnitTax)),2 ),
                'discount' => number_format($product->totalDiscount, 2),
                'discount_per_or_val' => $lineItemDiscount,
                'total' => number_format(($product->salesInvoiceProductTotal / $customCurrencyRate), 2),
                'total_wo_tx' => number_format(($product->salesInvoiceProductTotal - $product->taxTotal) / $customCurrencyRate, 2),
                'total_wo_discounts' => number_format($itemTotalWithoutDiscount, 2),
                'attribute_01' => (!is_null($attribute_01)) ? $attribute_01 : '-',
                'attribute_02' => (!is_null($attribute_02)) ? $attribute_02 : '-',
                'attribute_03' => (!is_null($attribute_03)) ? $attribute_03 : '-',
            );

            $subTotalWithoutDiscount = $subTotalWithoutDiscount + floatval($itemTotalWithoutDiscount);

            $templateOptions = array_merge($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);
            // filter only the columns that need to be displayed
            $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

            // nest products under category if they should be categorized
            if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
                $categoryId = $product->categoryID;
                $records[$categoryId]['products'][] = $product_record;

                // add category name to the list
                if (!isset($records[$categoryId]['category'])) {
                    $records[$categoryId]['category'] = (isset($allCategories[$categoryId])) ? $allCategories[$categoryId] : ''; // change this to category name
                }
            } else if ($templateOptions['categorize_products_by_dln'] && $templateOptions['categorize_products_by_dln'] != "false") {
                if ($product->documentTypeID == '4' && $product->salesInvoiceProductDocumentID != null) {
                    $records[$product->salesInvoiceProductDocumentID]['products'][] = $product_record;

                    // add category name to the list
                    if (!isset($records[$product->salesInvoiceProductDocumentID]['category'])) {
                        $records[$product->salesInvoiceProductDocumentID]['category'] = (isset($allDln[$product->salesInvoiceProductDocumentID])) ? $allDln[$product->salesInvoiceProductDocumentID] : '';
                    }
                } else {
                    $records[0]['products'][] = $product_record;

                    // add category name to the list
                    if (!isset($records[0]['category'])) {
                        $records[0]['category'] = 'Addional Items';
                    }
                }
            } else {
                $records[0]['products'][] = $product_record;
            }

            if ($invoiceDetails->inclusiveTax == "1") {
                $subAmount = number_format(($product->salesInvoiceProductTotal / $customCurrencyRate), 2);
                $subAmount = str_replace( ',', '', $subAmount );
                $sub_total+= (float)$subAmount;
            } else {
                $sub_total+= ($product->salesInvoiceProductTotal / $customCurrencyRate);
            }

            $product->unitPrice = $unitPrice;
            $product->itemQuntity = $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'];
            $data['productsList'][] = (array) $product;
            if($data['promotionID'] && count($promotionItemDetails)>0 && in_array($product->productID,$promotionItemDetails))
            {
                $itemWisePromoDetails[] = array(
                    'item' => $product->productName.' '.$product->productCode,
                    'itemDiscount' => $product->totalDiscount,
                    );
            }
        }

        $data['itemWiseProDetails'] = $itemWisePromoDetails;

        $tx_line = "";
        foreach ($txTypes as $tx => $vl) {
            $tx_line .= ($tx_line) ? ",&nbsp;" . $tx . ":&nbsp;" . $data['currencySymbol'] . number_format($vl, 2) :
                    $tx . ":&nbsp;" . $data['currencySymbol'] . number_format($vl, 2);
        }

        $tx_line = trim($tx_line, "&nbsp;");

        $headers = array(
            'product_index' => array('label' => _(""), 'align' => 'left'),
            'product_code' => array('label' => _("Item Code"), 'align' => 'left'),
            'product_name' => array('label' => _("Item Name"), 'align' => 'left'),
            'product_code_and_name' => array('label' => _("Item Code & Name"), 'align' => 'left'),
            'product_description' => array('label' => _("Product Description"), 'align' => 'left'),
            'attribute_01' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeOneID]), 'align' => 'left'),
            'attribute_02' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeTwoID]), 'align' => 'left'),
            'attribute_03' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeThreeID]), 'align' => 'left'),
            'quantity' => array('label' => _("Quantity"), 'align' => 'left'),
            'price' => array('label' => _("Unit Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'price_w_tax' => array('label' => _("Unit Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'mrp-price' => array('label' => _("MRP</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'discount' => array('label' => _("Total Discount (-)"), 'align' => 'right'),
            'discount_per_or_val' => array('label' => _("Discount (-)"), 'align' => 'right'),
            'total' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_tx' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_discounts' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
        );


        $footer_rows = array(
            'sub_total' => true,
            'discount' => true,
            'vat' => true,
            'total' => true
        );

        // filter only the headers that need to be displayed
        $headers = array_splice(array_replace(array_flip($allowedColumnsList), $headers), 0, count($allowedColumnsList));

        // filter only the footer rows that need to be displayed
        $footer_rows = array_splice(array_replace(array_flip($allowedRowsList), $footer_rows), 0, count($allowedRowsList));

        // set column width
        if (isset($templateOptions['product_table']['columns'])) {
            foreach ($templateOptions['product_table']['columns'] as $column) {
                $columnName = $column['name'];
                $columnWidth = $column['width'];

                // if column is in allowed list
                if (isset($headers[$columnName])) {
                    $headers[$columnName]['width'] = $columnWidth;
                }
            }
        }

        $product_record = count($product_record) ? $product_record : [];
        $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

        // set variables
        $data['table'] = array(
            'hide_table_header' => ($templateOptions['product_table']['hide_table_header'] === true || $templateOptions['product_table']['hide_table_header'] == 'true'),
            'hide_table_borders' => ($templateOptions['product_table']['hide_table_borders'] === true || $templateOptions['product_table']['hide_table_borders'] == 'true'),
            'headers' => $headers,
            'footers' => $footer_rows,
            'records' => $records,
        );
        $itemWiseDiscount = $totalDiscount;
        $totalDiscount += ($invoiceDetails->salesInvoiceTotalDiscount / $customCurrencyRate);
        
        if ($invoiceDetails->inclusiveTax == "1" && $invoiceDetails->templateInvoiceTotal != null) {
            $templateFinalTotal = $invoiceDetails->templateInvoiceTotal / $customCurrencyRate;
        } else {
            $templateFinalTotal = $invoiceDetails->salesinvoiceTotalAmount / $customCurrencyRate;
        }

        $data['salesInvoiceDeliveryCharge'] = $invoiceDetails->salesInvoiceDeliveryCharge / $customCurrencyRate;
        $data['serviceChargeDetails'] = $invoiceDetails->serviceChargeDetails;
        $data['delivery_charge'] = $invoiceDetails->salesInvoiceDeliveryCharge / $customCurrencyRate;
        $data['comment'] = $invoiceDetails->salesInvoiceComment;
        $data['salesInvoiceComment'] = $invoiceDetails->salesInvoiceComment;
        $data['sub_total'] = $sub_total;
        $data['sub_total_wo_tax'] = $sub_total - $totalTax;
        $data['sub_total_wo_discounts'] = $subTotalWithoutDiscount;
        $data['total'] = $templateFinalTotal;
        $data['totalTax'] = number_format($totalTax, 2);
        $data['tax_record'] = "<strong>" . _("Tax:") . "</strong>&nbsp;";
        $data['tax_record_line'] = $tx_line;
        $data['discountStatement'] = _("Total discount received:");
        $data['discount'] = number_format($totalDiscount, 2); // total discount received
        $data['discountWithPromoDisc'] = $totalDiscount + $data['salesInvoicePromotionDiscount'];
        $data['salesInvoiceRealPayedAmount'] = number_format($invoiceDetails->salesInvoicePayedAmount / $customCurrencyRate, 2);
        $data['salesInvoiceRemainPayAmount'] = number_format((floatval($invoiceDetails->salesinvoiceTotalAmount) - floatval($invoiceDetails->salesInvoicePayedAmount))/$customCurrencyRate, 2);
        $data['CustomerCurrentBalanceBeforeInvoice'] = (is_null($invoiceDetails->customerCurrentOutstanding)) ? '-' : 'Previous Outstanding: '.$data['currencySymbol'] .number_format($invoiceDetails->customerCurrentOutstanding / $customCurrencyRate, 2);
        $outstandingAfterInvoice = ($invoiceDetails->customerCurrentOutstanding / $customCurrencyRate) + ($invoiceDetails->salesinvoiceTotalAmount / $customCurrencyRate);
        $data['CustomerCurrentBalanceAfterInvoice'] = 'Outstanding After Invoice: '.$data['currencySymbol'] .number_format($outstandingAfterInvoice, 2);
        if ($data['customerShortName'] == 'DefaultCustomer') {
            $data['customerName'] = '';
        } else {
            $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];
        }
        $data['incomingPaymentPaidAmount'] = number_format(($paymentDetails->incomingPaymentPaidAmount / $customCurrencyRate), 2);
        $data['incomingPaymentBalance'] = number_format($paymentDetails->incomingPaymentBalance / $customCurrencyRate, 2);
        $data['salesInvoiceTotalDiscount'] = number_format($invoiceDetails->salesInvoiceTotalDiscount / $customCurrencyRate, 2);
        $data['salesinvoiceTotalAmountWithCurr'] = $data['currencySymbol'] . number_format($templateFinalTotal, 2);
        //add loyalty total points and earnedLoyalty points in to the data array.
        $loyaltyData=$this->CommonTable('Invoice\Model\InvoiceTable')->getLoyaltyDetailsByInvoiceID($invoiceID)->current();

        if ($loyaltyData['customerLoyaltyPoints'] != null)
        {
            $data['totalLoyaltyPoints'] = number_format($loyaltyData['customerLoyaltyPoints'],2);
            $data['earnedLoyaltyPoints'] = number_format($loyaltyData['collectedPoints'],2);
        }

        if ($invoiceID == 'template') {
            $data['template'] = true;
            $data['customerName'] = '[customerName]';
            $data['salesInvoiceCode'] = '[salesInvoiceCode]';
            $data['salesInvoiceIssuedDate'] = '[salesInvoiceIssuedDate]';
            $data['current_time'] = '[current_time]';
            $data['sub_total'] = '{{sub_total | currency : "' . $data['currencySymbol'] . '"}}';
            $data['totalTax'] = '{{totalTax | currency : "' . $data['currencySymbol'] . '" }}';
            $data['discount'] = '{{totalDiscount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['salesInvoiceTotalDiscount'] = '{{invoiceTotalDiscount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['incomingPaymentPaidAmount'] = '{{paidAmount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['incomingPaymentBalance'] = '{{paymentBalance | currency : "' . $data['currencySymbol'] . '" }}';
            $data['incomingPaymentDiscount'] = '{{paymentDiscount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['salesinvoiceTotalAmountWithCurr'] = $data['currencySymbol'] . '{{totalAmount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['totalLoyaltyPoints'] = '[totalLoyaltyPoints]';
            $data['earnedLoyaltyPoints'] = '[earnedLoyaltyPoints]';
            $data['salesInvoiceComment'] = '[comment]';
        }
        $tempTotalValue = 0;
        //use to create total discount and itemwise discount
        $item_count = sizeof($data['productsList']);
        for ($i = 0; $i < $item_count; $i++) {
            $tempTotalValue+=$data['productsList'][$i]['totalDiscount'];
        }
        $suspendedTaxAmount = 0.00;
        if ($data['salesInvoiceSuspendedTax'] == 1) {
            $suspendedTaxAmount = $suspendedTaxesTotal;
        }

        $sub_tot = $data['sub_total'];
        $massDiscount = 0.00;
        if ($data['salesInvoiceSuspendedTax'] == 1) {
            $massDiscount = floatval($sub_tot) - floatval($suspendedTaxesTotal) - floatval($data['total']);
        } else {
            $massDiscount = floatval($sub_tot) - floatval($data['total']);
        }
        $mass_disc_without_product_dis = $massDiscount;
        $mass_disc = $tempTotalValue + $mass_disc_without_product_dis;
        if ($data['salesInvoiceDeliveryCharge']) {
            $mass_disc = $mass_disc + $data['salesInvoiceDeliveryCharge'];
            $mass_disc_without_product_dis = $mass_disc_without_product_dis + $data['salesInvoiceDeliveryCharge'];
        }
        $data['mass_disc_without_product_dis'] = abs($mass_disc_without_product_dis);
        $data['mass_discount'] = $mass_disc;
        if ($invoiceDetails->salesInvoiceTotalDiscountType != NULL) {
            $data['mass_discount'] = $itemWiseDiscount + ($invoiceDetails->salesInvoiceWiseTotalDiscount / $customCurrencyRate);
            $data['mass_disc_without_product_dis'] = ($invoiceDetails->salesInvoiceWiseTotalDiscount / $customCurrencyRate);
            if ($invoiceDetails->salesInvoicePromotionDiscount) {
                $data['mass_discount'] = $itemWiseDiscount + (($invoiceDetails->salesInvoiceWiseTotalDiscount + $invoiceDetails->salesInvoicePromotionDiscount) / $customCurrencyRate);
            }
        }

        // get promotion details
        if ($data['promotionID']) {
            $promotion = $this->CommonTable('Settings\Model\PromotionTable')->getPromotion($data['promotionID']);
            $data['promotion'] = (isset($promotion['promotionName'])) ? $promotion['promotionName'] : null;
        }
        return $this->_invoiceViewData = $data;
    }

    public function getDocumentDataTable($invoiceID, $documentSize = 'A4')
    {
        $data_table_vars = $this->getDataForDocument($invoiceID);

        if ($data_table_vars['inclusiveTax'] == "1" && $data_table_vars['pos'] == "0") {
            $data_table_vars['salesinvoiceTotalAmount'] = $data_table_vars['templateInvoiceTotal'];
        } else {
            $data_table_vars['salesinvoiceTotalAmount'] = $data_table_vars['salesinvoiceTotalAmount'];
        }
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/invoice/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

// Displays the overlay which loads an iframe inside (calls documentAction)
    public function invoiceViewAction()
    {
        return $this->documentPreviewAction();
    }

    public function documentPreviewAction()
    {

        $invoiceID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($invoiceID);

        if (!$data['customerName']) {
            $customerSalutaion = "";
            $customerNameString = "";
        } else {
            $customerSalutaion = "Dear " . $data['customerName'] . ",";
            $customerNameString = "<strong>Customer Name:</strong>" . $data['customerName'] . " <br />";
        }
        if ($data['inclusiveTax'] == "1" && $data['pos'] == "0") {
            $data['salesinvoiceTotalAmount'] = number_format(($data['templateInvoiceTotal'] / $data['currencyRate']), 2);
        } else {
            $data['salesinvoiceTotalAmount'] = number_format(($data['salesinvoiceTotalAmount'] / $data['currencyRate']), 2);
        }
        $path = ($data['pos']) ? '/invoice/document/pos/' : '/invoice/document/'; //.$invoiceID;
        $translator = new Translator();
        $createNew = $translator->translate('New Invoice');
        $createPath = (!$data['pos']) ? "/invoice/create" : null;
        $data["email"] = ($data['pos']) ? null : array(
            "to" => $data['customerEmail'],
            "subject" => "Invoice from " . $data['companyName'],
            "body" => <<<EMAILBODY

{$customerSalutaion} <br /><br />

Thank you for your business. <br /><br />

An Invoice has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

{$customerNameString}
<strong>Invoice No:</strong> {$data['salesInvoiceCode']} <br />
<strong>Invoice Amount:</strong> {$data['currencySymbol']}{$data['salesinvoiceTotalAmount']} <br />
<strong>Payment due date:</strong> {$data['salesInvoiceOverDueDate']} <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        if(!$data['pos'] && $this->hasSmsData("After Invoice") && $data['customerTelephoneNumber'] != ""){
            $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("After Invoice");
            $message = $smsTypeDetails->smsTypeMessage .' ';

            $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);

            if($smsTypeDetails->isIncludeInvoiceDetails){
                while ($row = $smsIncludedDetailsByType->current()) {
                    if((bool) $row['isSelected']){
                        switch ($row['smsIncludedName']) {
                            case "Invoice Date":
                                $message .=$row['smsIncludedName'] .' : '.$data['salesInvoiceIssuedDate'].' ,';
                                break;
                            case "Invoice Number":
                                $message .=$row['smsIncludedName'] .' : '.$data['salesInvoiceCode'].' ,';
                                break;
                            case "Invoice Amount":
                                $message .=$row['smsIncludedName'] .' : '.$data['currencySymbol'].$data['salesinvoiceTotalAmount'].' ,';
                                break;
                            case "Due Date":
                                $message .=$row['smsIncludedName'] .' : '.$data['salesInvoiceOverDueDate'].' ,';
                                break;
                            case "Sales Person":
                                $message .=$row['smsIncludedName'] .' : '.$data['salesPersonSortName'].' ,';
                                break;
                            default:
                                break;
                        }
                    }
                }
            }
            
            $data["sms"] = array(
                "customerTelephoneNumber" => $data["customerTelephoneNumber"],
                "message" => $message
            );
        }else{
            $data["sms"] = null;
        }

        $journalEntryValues = [];
        if($this->useAccounting == 1){
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('1',$invoiceID);
            $journalEntryID = $journalEntryData['journalEntryID'];
            $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryValues = $jeResult['JEDATA'];
        }

        $JEntry = new ViewModel(
            array(
             'journalEntry' => $journalEntryValues,
             'closeHidden' => true
             )
            );
        $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $documentType = ($data['pos']) ? 'POS printout' : 'Sales Invoice';
        $data['documentType'] = $documentType;

        $invoiceView = $this->getCommonPreview($data, $path, $createNew, $documentType, $invoiceID, $createPath);
        $invoiceView->addChild($JEntry, 'JEntry');
        $invView = new ViewModel(array('invoiceID' => $invoiceID, 'pos' => $data['pos'], 'glAccountFlag' => $this->useAccounting));
        $invView->setTemplate('invoice/invoice/invoice-preview');
        $invoiceView->addChild($invView, 'additionalButton');
        $invoiceView->setTerminal(TRUE);
        
        return $invoiceView;
    }

    public function hasSmsData($smsType){
         $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

        if ($displaySettings->smsServiceStatus) {
                $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName($smsType);
                
                if($smsTypeDetails->isEnable && (int)$smsTypeDetails->smsSendType == 2){
                    return true;
                   
                }
        }
        return false;
    }

    public function recurentInvoiceAction()
    {
        $this->setLogMessage("New Recurrent Invoice add form accessed");
        $this->getSideAndUpperMenus('Invoices', 'Recurrent Invoice');
        $recurrentInvoiceID = $this->params()->fromRoute('param1');
        $invoice_form = new RecurrentInvoiceForm(array(
            'name' => 'invoice',
            'id' => 'invoice_no'));
        $invoice = new ViewModel(array(
            'invoiceForm' => $invoice_form,
            'invoiceID' => $recurrentInvoiceID,
            'invoice_list' => $this->getInvoiceIdsJson(),
        ));
        return $invoice;
    }

    public function recurentInvoiceViewAction()
    {
        $this->setLogMessage("View Recurrent Invoice page accessed");
        $this->getSideAndUpperMenus('Invoices', 'View Recurrent Invoice');

        $this->getPaginatedRecurrentInvoices();
        $invoice_form = new RecurrentInvoiceForm(array(
            'name' => 'invoice',
            'id' => 'invoice_no'));
        $recurrentinvoiceView = new ViewModel(array(
            'invoiceForm' => $invoice_form,
            'rec_inv_list' => $this->getRecurrentIDsJson("Invoice"),
            'cust_list' => $this->getCustomerNameJson(),
        ));
        $recurrentinvoicelist = new ViewModel(array(
            'recurrentinvoices' => $this->paginator,
                )
        );
        $delete_recurrent_invoice = new viewModel(array(
        ));
        $delete_recurrent_invoice->setTemplate('/invoice/invoice/confirm-delete-recurrent-invoice');
        $recurrentinvoiceView->addChild($delete_recurrent_invoice, 'delete_inv');

        $recurrentinvoicelist->setTemplate('invoice/invoice-api/recurrent_invoice-list');
        $recurrentinvoiceView->addChild($recurrentinvoicelist, 'recurrent_inv_list');
        return $recurrentinvoiceView;
    }

    private function getPaginatedInvoices($count = 15, $posflag = 0, $considerJob = false )
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

        // if($displaySettings->jobModuleEnabled == 1 &&  $displaySettings->jobBusinessType == 2 && $considerJob) {
            // $this->paginator = $this->CommonTable('Invoice\Model\InvoiceTable')->fetchAllWithVehicleNo(true, $locationID, $posflag);
        // } else {
            $this->paginator = $this->CommonTable('Invoice\Model\InvoiceTable')->fetchAll(true, $locationID, $posflag);
        // }

        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($count);
    }

    private function getPaginatedDraftInvoices($count = 15, $posflag = 0, $considerJob = false )
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

        // if($displaySettings->jobModuleEnabled == 1 &&  $displaySettings->jobBusinessType == 2 && $considerJob) {
        //     $this->paginator = $this->CommonTable('Invoice\Model\InvWfTable')->fetchAllWithVehicleNo(true, $locationID, $posflag);
        // } else {
        // }
        $this->paginator = $this->CommonTable('Invoice\Model\DraftInvWfTable')->fetchAll(true, $locationID, $posflag);

        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($count);
    }

//get product list and make two json arrays for typerheads.
    public function getProductJson()
    {
        $pr_boject = $this->CommonTable('Invoice\Model\ProductTable')->getProductNames();
        while ($row = $pr_boject->current()) {
            $pro_ids[] = $row['id'];
            $pro_name[] = $row['productName'];
            $pro_details[$row['id']] = $row;
            $pro_name_map[$row['productName']] = $row['id'];
        }
        return array(
            "pro_ids" => json_encode($pro_ids),
            "pro_name" => json_encode($pro_name),
            "pro_details" => json_encode($pro_details),
            "pro_name_map" => json_encode($pro_name_map)
        );
    }

    public function editAction()
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID("Inventory\Controller\Product", "index", $this->user_session->userID);

        $isCanCreateItem = $isEnable;
        $this->getSideAndUpperMenus('Invoices', 'View Invoices', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $invoiceID = $this->params()->fromRoute('param1');
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];

        $tax = array();
        $sup = array();
        $ctax = array();
        $productType = array();

        //get creditNotes related to current invoice ID
        $getCreditNote = $this->CommonTable("Invoice\Model\CreditNoteTable")->getCreditNotesByInvoiceID($invoiceID);
        //get dispatch notes related to current invoice
        $getDispatchNote = $this->CommonTable("Invoice\Model\DispatchNoteTable")->getDispatchNoteByInvoiceId($invoiceID);
        //get invoice produts copied from delivery Note by invoiceID
        $getInvoiceProduct = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getDeliveryNoteCopiedProductsByInvoiceID($invoiceID);
        //get invoice returns relate to this inoice
        $getInvoiceReturn = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->getInvoiceReturnByInvoiceIDForEdit($invoiceID);

        $deliveryNoteIDs = [];
        foreach ($getInvoiceProduct as $key => $value) {
            $deliveryNoteIDs[] = $value['salesInvoiceProductDocumentID'];
        }

        if ($getCreditNote->current() != NULL) {
            $this->flashMessenger()->addMessage(['status' => FALSE, 'msg' => 'This invoice has a credit note.So you can not edit this invoice..!']);

            return $this->redirect()->toRoute(null, array(
                        'module' => 'invoice',
                        'controller' => 'invoice',
                        'action' => 'view')
            );
        } else if ($getDispatchNote != NULL) {
            $this->flashMessenger()->addMessage(['status' => FALSE, 'msg' => 'This invoice has a dispatch note.So you can not edit this invoice..!']);

            return $this->redirect()->toRoute(null, array(
                        'module' => 'invoice',
                        'controller' => 'invoice',
                        'action' => 'view')
            );
        } 

        if (sizeof($getInvoiceReturn) > 0) {
            $this->flashMessenger()->addMessage(['status' => FALSE, 'msg' => 'This invoice has a related invoice returns.So you can not edit this invoice..!']);

            return $this->redirect()->toRoute(null, array(
                        'module' => 'invoice',
                        'controller' => 'invoice',
                        'action' => 'view')
            );
        }
        $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }

        // Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll(false,true);
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        $today = date('Y-m-d');
        $promotions = $this->CommonTable('Settings\Model\PromotionTable')->getToDayPromotions($today, $locationID);
        $promotionData = array();
        foreach ($promotions as $pro) {
            $pro = (object) $pro;
            $promotionData[$pro->promotionID] = $pro;
        }

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();

        //get priceLists
        $priceList = $this->getActivePriceList();

        $invoiceForm = new InvoiceForm(array(
            'name' => 'invoice',
            'id' => 'invoiceNo',
            'paymentTerms' => $terms,
            'salesPersons' => $salesPersons,
            'promotions' => $promotionData,
            'customCurrency' => $currency,
            'priceList'=>$priceList,

        ));


        $freeIssueItemModal = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $freeIssueItemModal->setTemplate('invoice/invoice/free-issue-item-modal');

        $serviceChargeView = new ViewModel();
        $serviceChargeView->setTemplate('invoice/invoice/add-service-charge-modal');

        $displaySetup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll()->current();

        $invoiceForm->get('customCurrencyId')->setAttribute('data-baseid', $displaySetup->currencyID);

        $invoiceForm->get('invoiceNo')->setValue($invoiceData->salesInvoiceCode);

        $userdateFormat = $this->getUserDateFormat();
        $attrDetails = [];
        $itemAttrDataSet = $this->CommonTable('/settings/Model/itemAttributeValueTable')->selectAll();
        foreach ($itemAttrDataSet as $key => $value) {
            $attrDetails[$value['itemAttributeID']] = array(
                'itemAttributeID' => $value['itemAttributeID'],
                'description' => $value['itemAttributeCode']." - ".$value['itemAttributeName'],
                );
        }
        $displaySetupDetails = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();
        $invoice = new ViewModel(array(
            'locationID' => $locationID,
            'locationName' => $locationName,
            'locationCode' => $locationCode,
            'invoiceForm' => $invoiceForm,
            'cur' => $this->companyCurrencySymbol,
            'editInvoiceID' => $invoiceID,
            'currentLocationID' => $locationID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'cusID' => $invoiceData->customerID,
            'activeCusList' => $this->getActiveCustomerList(),
            'userDateFormat' => $userdateFormat,
            'useAccounting' => $this->useAccounting,
            'isCanCreateItem' => $isCanCreateItem,
            'multipleDeleNotes' => (count(array_unique($deliveryNoteIDs)) > 1) ? true : false,
            'attrDetails' => $attrDetails,
            'salesPersons' => $salesPersons,
            'isEnableForProductWiseSalesPerson' => $this->user_session->productWiseSalesPersonEnable,
            'isUseMrpValues' => $displaySetupDetails['useMrpSettings']
        ));


        $viewMrpValluesView = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $viewMrpValluesView->setTemplate('invoice/invoice/view-mrp-values-modal');

        $invoiceForm->get('discount')->setAttribute('id', 'deliveryNoteDiscount');
        $customerForm = new AddCustomerForm('add_new_customer', $terms);
        $customerForm->setAttribute('id', 'add_customer_form');
        $customerForm->setAttribute('name', 'add_customer_form');
        $customerForm->get('customerDiscount')->setAttribute("id", "discnt");
        $addCustomer = new ViewModel(array(
            'form' => $customerForm,
            'action' => 'add',
        ));
        $addCustomer->setTemplate('/invoice/customer-api/get-customer-edit-box');
        $invoice->addChild($addCustomer, 'cust_formv');

        $invoiceProductsView = new ViewModel();
        $invoiceProductsView->setTemplate('invoice/invoice/invoice-note-add-products');
        $invoice->addChild($invoiceProductsView, 'invoiceProducts');

        $barCodeAddModal = new ViewModel();
        $barCodeAddModal->setTemplate('/inventory/product/add-bar-codes-modal');
        $invoice->addChild($barCodeAddModal, 'barCodeAddModal');

        $creditLimitValidationView = new ViewModel();
        $creditLimitValidationView->setTemplate('invoice/invoice/confirm-credit-limit-exceed-box');
        $invoice->addChild($creditLimitValidationView, 'creditLimitValidationView');

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAll();
        foreach ($supplier as $r) {
            $sup[$r->supplierID] = $r->supplierName;
        }

        $tax_resource = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
        foreach ($tax_resource as $t) {
            $tax[$t->id] = $t->taxName;
            if ($t->taxType == 'c') {
                $ctax[$t->id] = $t->taxName;
            }
        }

        $productForm = new ProductForm(array(
            'supplier' => $sup,
            'tax' => $tax,
            'productType' => $productType,
            'location' => $location,
        ));

        $financeAccountsArray = $this->getFinanceAccounts();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $productSalesAccountName = $financeAccountsArray[$productSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productSalesAccountID]['financeAccountsName'];
                $productForm->get('productSalesAccountID')->setAttribute('data-id',$productSalesAccountID);
                $productForm->get('productSalesAccountID')->setAttribute('data-value',$productSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $productInventoryAccountName = $financeAccountsArray[$productInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productInventoryAccountID]['financeAccountsName'];
                $productForm->get('productInventoryAccountID')->setAttribute('data-id',$productInventoryAccountID);
                $productForm->get('productInventoryAccountID')->setAttribute('data-value',$productInventoryAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $productCOGSAccountName = $financeAccountsArray[$productCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productCOGSAccountID]['financeAccountsName'];
                $productForm->get('productCOGSAccountID')->setAttribute('data-id',$productCOGSAccountID);
                $productForm->get('productCOGSAccountID')->setAttribute('data-value',$productCOGSAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $productAdjusmentAccountName = $financeAccountsArray[$productAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productAdjusmentAccountID]['financeAccountsName'];
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-id',$productAdjusmentAccountID);
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-value',$productAdjusmentAccountName);
            }
        }

        $productForm->get('submit')->setValue('Save');
        $productForm->get('productState')->setValue('Active');
        $productForm->setAttribute('id', 'create-product-form');

        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $defaultCategoryID = array_search('General', $allCategoryList);
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        $defaultUomID = $this->CommonTable('Settings\Model\UomTable')->getUomByName('Unit')->uomID;
        $categoryForm = new CategoryForm($allCategoryList);
        $categoryForm->get('categoryParentID')->setAttribute('data-id', $defaultCategoryID);
        $productHandelingForm = new ProductHandelingForm();
        $productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", true);
        $createProductView = new ViewModel(array(
            'productForm' => $productForm,
            'productHandelingForm' => $productHandelingForm,
            'categoryForm' => $categoryForm,
            'uomList' => $uoms,
            'tax' => $tax,
            'ctax' => $ctax,
            'location' => $location,
            'defaultUom' => $defaultUomID,
            'useAccounting' => $this->useAccounting
        ));
        $createProductView->setTemplate('invoice/invoice-api/create-product');


        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories', 'uomByID', 'taxes'
        ]);

        $this->getViewHelper('HeadScript')->prependFile('/js/product.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/category.js');

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";
        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');

        $invoice->addChild($dimensionAddView, 'dimensionAddView');
        
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $invoice->addChild($attachmentsView, 'attachmentsView');
        $invoice->addChild($serviceChargeView, 'serviceChargeView');
        $invoice->addChild($viewMrpValluesView, 'viewMrpValluesView');
        $invoice->addChild($freeIssueItemModal, 'freeIssueItemModal');

        $nextInvoiceDateView = new ViewModel(array(
            'userdateFormat' => $userdateFormat
        ));
        $nextInvoiceDateView->setTemplate('invoice/invoice/add-membership-modal');
        $invoice->addChild($nextInvoiceDateView, 'nextInvoiceDateView');

        $invoice->addChild($createProductView, 'createProductView');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories', 'uomByID', 'taxes'
        ]);

        return $invoice;
    }

    public function getCustomerNameJson()
    {
        $cust_obj = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerNameList();
        while ($row1 = $cust_obj->current()) {
            $cust_list[] = $row1["cust_name"];
        }
        return json_encode($cust_list);
    }

    //get sales Orders ids in json format for typer head.
    public function getSalesOrdersIdsJson()
    {
        $so_id_obj = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderIds();
        while ($row = $so_id_obj->current()) {
            $sales_orders_list[] = $row['id'];
        }
        return json_encode($sales_orders_list);
    }

    //get all quotation id list for typerhead
    public function getQuotationIdsJson()
    {
        $qot_obj = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotationIdList();
        while ($row = $qot_obj->current()) {
            $quot_list[] = $row['id'];
        }
        return json_encode($quot_list);
    }

    public function getInvoiceIdsJson()
    {
        $ids_obj = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceIds();
        while ($row = $ids_obj->current()) {
            $ids[] = $row['salesInvoiceID'];
        }
        return json_encode($ids);
    }


    public function getDataForDraftDocument($invoiceID)
    {
        if (!empty($this->_invoiceViewData)) {
            return $this->_invoiceViewData;
        }
        $itemAttrDetails = $this->CommonTable('Settings\Model\ItemAttributeTable')->fetchAll();
        $itemAttrDetailArray = [];
        foreach ($itemAttrDetails as $value) {
            $itemAttrDetailArray[$value['itemAttributeID']] = $value['itemAttributeName'];
        }

        $data = $this->getDataForDocumentView();

        $invoiceDetails = (array) $this->CommonTable('Invoice\Model\DraftInvWfTable')->getAllInvoiceDetailsByInvoiceID($invoiceID)->current();

        if (!empty($invoiceDetails['soNum'])) {
            $getSoData = (array) $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($invoiceDetails['soNum']);
            $invoiceDetails['soNum'] = $getSoData['soCode'];
        } else {
            $invoiceDetails['soNum'] = "-";  
        }


        if ($invoiceDetails['salesInvoiceServiceChargeAmount'] > 0) {
            $relatedServiceCharges = $this->CommonTable('Invoice\Model\DraftInvServiceChargeTable')->getInvoiceChargesByInvoiceID($invoiceID);
            $serviceChargeString = "";
            if (sizeof($relatedServiceCharges) > 0) {
                foreach ($relatedServiceCharges as $key8 => $value8) {
                    $tempString = $value8['serviceChargeTypeName'].'-'.$value8['serviceChargeTypeCode']. ' : '.number_format($value8['serviceChargeAmount'],2);
                    $serviceChargeString .= "<small> - " . $tempString . "</small><br>";
                }
            }

        }

        $invoiceDetails['serviceChargeDetails'] = $serviceChargeString;
        $invoiceDetails['salesInvoiceIssuedDate'] = $this->convertDateToUserFormat($invoiceDetails['salesInvoiceIssuedDate']);
        $invoiceDetails['salesInvoiceOverDueDate'] = $this->convertDateToUserFormat($invoiceDetails['salesInvoiceOverDueDate']);
        $invoiceDetails['createdTimeStamp'] = $this->getUserDateTime($invoiceDetails['createdTimeStamp']);
        $customCurrencyRate = ($invoiceDetails['salesInvoiceCustomCurrencyRate'] != 0) ? $invoiceDetails['salesInvoiceCustomCurrencyRate'] : 1;
        $invoiceDetails['customerProfile'] = $invoiceDetails['customerProfileName'];

        $invoiceSPDetails = $this->CommonTable('Invoice\Model\DraftInvSalesPersonsTable')->getallsSalesPersonByInvoiceID($invoiceID);
        $tempSPString = "";
        if (sizeof($invoiceSPDetails) > 0) {
            foreach ($invoiceSPDetails as $key => $value) {

                if ($key == (sizeof($invoiceSPDetails) -1)) {
                    $tempSPString .= $value['salesPersonSortName'];
                } else {
                    $tempSPString .= $value['salesPersonSortName'].',';
                }
            }
        }
               
        $invoiceDetails['salesPersonSortName'] = $tempSPString;

        if (sizeof($invoiceSPDetails) == 1) {
            $invoiceDetails['salesPersonAddress'] = (!empty($invoiceSPDetails[0]['salesPersonAddress']))  ? $invoiceSPDetails[0]['salesPersonAddress'] : "";
        } else {
            $invoiceDetails['salesPersonAddress'] = "";
        }

        // replace customer Address and Email from primary profile of current customer
        $addressArray = array(
            trim($invoiceDetails['customerProfileLocationNo'], ','),
            trim($invoiceDetails['customerProfileLocationRoadName1'], ','),
            trim($invoiceDetails['customerProfileLocationRoadName2'], ','),
            trim($invoiceDetails['customerProfileLocationRoadName3'], ','),
            trim($invoiceDetails['customerProfileLocationSubTown'], ','),
            trim($invoiceDetails['customerProfileLocationTown'], ','),
            trim($invoiceDetails['customerProfileLocationPostalCode'], ','),
            trim($invoiceDetails['customerProfileLocationCountry'], ','),
        );

        if (!empty($invoiceDetails['jobID'])) {
            $jobDetails = (array) $this->CommonTable('Jobs\Model\JobTable')->getServiceJobById($invoiceDetails['jobID']);
            $invoiceDetails['jobCode'] = $jobDetails['jobReferenceNumber'];
            $invoiceDetails['vehicleRegNo'] = $jobDetails['serviceVehicleRegNo'];
            $invoiceDetails['vehicleType'] = $jobDetails['vehicleTypeName'];
            $invoiceDetails['mileage'] = $jobDetails['jobVehicleKMs'];
            $invoiceDetails['jobInTime'] = $this->getUserDateTime($jobDetails['createdTimeStamp']);
            $invoiceDetails['jobOutTime'] = $this->getUserDateTime($jobDetails['completed_at']);
        }

        $address = implode(', ', array_filter($addressArray));
        $invoiceDetails['customerAddress'] = ($address != '') ? $address . '.' : $address;
        $invoiceDetails['customerEmail'] = $invoiceDetails['customerProfileEmail'];
        $invoiceDetails['customerGender'] = (!empty($invoiceDetails['customerGender'])) ? ucfirst($invoiceDetails['customerGender']): '-';
        $invoiceDetails['customerDOB'] = $invoiceDetails['customerDateOfBirth'];
        $invoiceDetails['customerAge'] = "-";

        if (!empty($invoiceDetails['customerDateOfBirth'])) {
            $birthday_timestamp = strtotime($invoiceDetails['customerDateOfBirth']);  
            $age = date('md', $birthday_timestamp) > date('md') ? date('Y') - date('Y', $birthday_timestamp) - 1 : date('Y') - date('Y', $birthday_timestamp);
            $invoiceDetails['customerAge'] = $age;
        }

        $invoiceDetails['customerCreditLimit'] = $data['currencySymbol'] . ' ' . number_format($invoiceDetails['customerCreditLimit'], 2);
        $invoiceDetails['customerCurrentBalance'] = $data['currencySymbol'] . ' ' . number_format($invoiceDetails['customerCurrentBalance'], 2);
        $invoiceDetails['customerCurrentCredit'] = $data['currencySymbol'] . ' ' . number_format($invoiceDetails['customerCurrentCredit'], 2);

        //use to divide date and time from timeStamp
        $data['current_time'] = date("h:i:sA",strtotime($invoiceDetails['createdTimeStamp']));
        $data = array_merge($data, $invoiceDetails);
        $data['currencyRate'] = $customCurrencyRate;
        $data['currencySymbol'] = ($data['currencySymbol'] != '') ? $data['currencySymbol'] : $data['companyCurrencySymbol'];

        //payment details
        $paymentDetails = (object) $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getPaymentsDetailsByInvoiceID($invoiceID)->current();

        // Invoice product details
        $forValidateInv = true;
        $products = $this->CommonTable('Invoice\Model\DraftInvProductTable')->getProductsByInvoiceID($invoiceID, $forValidateInv);
        $forValidateInv = false;

        //get all tax details
        $taxDetails = $this->CommonTable('Settings\Model\TaxCompoundTable')->fetchAll();
        $compoundTaxes = [];

        foreach ($taxDetails as $key => $taxValue) {
            $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;

        }


        $sub_total = 0;
        $productsTax = $this->CommonTable('Invoice\Model\DraftInvProductTaxTable')->getInvoiceTax($invoiceID);
        $txTypes = array();
        $totalTax = 0;
        $calculatableTaxesTotal = 0;
        $suspendedTaxesTotal = 0;
        $normalTaxTotal = 0;
        $compoundTaxTotal = 0;
        $calculatableTaxes = array();
        $suspendableTaxes = array();
        $normalTaxesSet = array();
        $compoundTaxesSet = array();
        foreach ($productsTax as $productTax) {

            $thisTax = $productTax['salesInvoiceProductTaxAmount'] / $customCurrencyRate;
            if(array_key_exists($productTax['taxID'], $compoundTaxes)){
                    $compoundTaxTotal += floatval($thisTax);
                    $compoundTaxesSet[] = $productTax['taxName'];
            } else {
                    $normalTaxTotal += floatval($thisTax);
                    $normalTaxesSet[] = $productTax['taxName'];
            }


            $txTypes[$productTax['taxName']] = (!isset($txTypes[$productTax['taxName']])) ? $thisTax : $txTypes[$productTax['taxName']] + $thisTax;
            $calculatableTaxesTotal += ($productTax['taxSuspendable'] != 1) ? $thisTax : 0;
            $suspendedTaxesTotal += ($productTax['taxSuspendable'] == 1) ? $thisTax : 0;
            $totalTax += $thisTax;

            if ($productTax['taxSuspendable'] == 1) {
                if ($invoiceDetails['salesInvoiceSuspendedTax'] == 1) {
                    $suspendableTaxes[] = $productTax['taxName'];                        // }
                }else{
                    $calculatableTaxes[] = $productTax['taxName'];
                }
            } else {
                $calculatableTaxes[] = $productTax['taxName'];
            }
        }

        $data['calculatableTaxesTotal'] = $calculatableTaxesTotal;
        $data['suspendedTaxesTotal'] = $suspendedTaxesTotal;
        $data['normalTaxTotal'] = number_format($normalTaxTotal, 2);
        $data['normalTaxesSet'] = (!empty($normalTaxesSet)) ? implode(', ', array_unique($normalTaxesSet)) : '';
        $data['compoundTaxesSet'] = (!empty($compoundTaxesSet)) ? implode(', ', array_unique($compoundTaxesSet)) : '';
        $data['compoundTaxTotal'] = number_format($compoundTaxTotal, 2);
        $data['calculatableTaxes'] = (!empty($calculatableTaxes)) ? implode(', ', array_unique($calculatableTaxes)) : '';
        $data['suspendableTaxes'] = (!empty($suspendableTaxes)) ? implode(', ', array_unique($suspendableTaxes)) : '';

        $invoiceDetails = (object) $invoiceDetails;
        $totalDiscount = 0;

        // get template specific data for the document
        $templateDefaultOptions = array('product_table' => $this->getServiceLocator()->get('config')['template-data-table']['invoice']);

        // get template options from database and override the default options
        $templateOptions = array_replace_recursive($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);

        $defaultFooterRows = [];
        foreach ($templateDefaultOptions['product_table']['rows'] as $row) {
            $defaultFooterRows[$row['name']] = $row;
        }

        $templateFooterRows = [];
        foreach ((array) $this->templateDetails['templateOptions']['product_table']['rows'] as $row) {
            $templateFooterRows[$row['name']] = $row;
        }

        $templateOptions['product_table']['rows'] = $templateFooterRows + $defaultFooterRows;

        $data['templateOptions'] = $templateOptions;
        $data['templateOptions'] = $templateOptions;

        // reorder columns
        $productsTableColumns = array_filter($templateOptions['product_table']['columns'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableColumns, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        // reorder rows
        $productsTableRows = array_filter($templateOptions['product_table']['rows'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableRows, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        $templateOptions['product_table']['columns'] = $productsTableColumns;
        $templateOptions['product_table']['rows'] = $productsTableRows;

        $allowedColumnsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['columns']);
        $allowedRowsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['rows']);

        // to prevent loading categories from the database in the following foreach loop
        $allCategories = array();
        if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
            $allCategories = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        }
        
        $allDln = array();
        if ($templateOptions['categorize_products_by_dln'] && $templateOptions['categorize_products_by_dln'] != "false") {
            $allDln = $this->CommonTable('Invoice/Model/DeliveryNoteTable')->getDeliveryNotes();
        }

        // Get Invoice serial product
        $invoiceSerialProducts = $this->CommonTable('Invoice\Model\DraftInvProductTable')->getInvoiceSerialProductByInvoiceId($invoiceID);
        $invoiceBatchProducts = $this->CommonTable('Invoice\Model\DraftInvProductTable')->getInvoiceBatchProductByInvoiceId($invoiceID);

        $invoiceSubProducts = [];
        foreach ($invoiceBatchProducts as $subProduct) {
            $invProdId = $subProduct['salesInvoiceProductID'];
            $prodBatchId = $subProduct['productBatchID'];
            $prodBatchCode = $subProduct['productBatchCode'];
            $productBatchExpiryDate = $subProduct['productBatchExpiryDate'];
            $batchData = [
                'code' => $prodBatchCode,
                'expiryDate' => $productBatchExpiryDate,
                'qty' => $subProduct['salesInvoiceSubProductQuantity'],
                'type' => 'batch'
            ];

            // For batch serial items
            // Eg: if 5 serial items from 2 batches were invoiced,
            // check if batch data is already in array and increment it's quantity by 1
            if ($subProduct['serialProduct'] == 1 && isset($invoiceSubProducts[$invProdId][$prodBatchId])) {
                $batchData['qty'] = $invoiceSubProducts[$invProdId][$prodBatchId]['qty'] + $batchData['qty'];
            }

            $invoiceSubProducts[$invProdId][$prodBatchId] = $batchData;
        }

        foreach ($invoiceSerialProducts as $subProduct) {
            $invProdId = $subProduct['draftInvProductID'];
            $prodSerialId = $subProduct['productSerialID'];
            $prodSerialCode = $subProduct['productSerialCode'];
            $prodBatchId = $subProduct['productBatchID'];
            $productSerialExpireDate = $subProduct['productSerialExpireDate'];

            $warranty = $this->calculateWarrantyPeriod($subProduct['salesInvoiceSubProductWarranty'],$subProduct['salesInvoiceSubProductWarrantyType']);     
            $serialData = [
                'code' => $prodSerialCode,
                'expireDate' => $productSerialExpireDate,
                'warranty' => $warranty,
                'type' => 'serial'
            ];

            if ($prodBatchId) {
                $invoiceSubProducts[$invProdId][$prodBatchId]['type'] = 'batch_serial';
                $invoiceSubProducts[$invProdId][$prodBatchId]['serial'][$prodSerialId] = $serialData;
            } else {
                $invoiceSubProducts[$invProdId][$prodSerialId] = $serialData;
            }
        }

        //get item Details that related to the given item wise promotion.
        if ($data['promotionID']) {
            $promotionItemDetails = $this->CommonTable('Settings\Model\PromotionTable')->getItemWisePromotionDetails($data['promotionID']);
            $itemWisePromoDetails = [];
        }

       $subTotalWithoutDiscount = 0;

        foreach ($products as $productIndex => $product) {
            $product = (object) $product;

            $item_salesPerson = "";
            if (!empty($product->itemSalesPersonID)) {
                $salesPerson = $this->CommonTable('User\Model\SalesPersonTable')->getSalesPersonByID($product->itemSalesPersonID)->current();
                $salesPersonDetail = $salesPerson['salesPersonSortName'].' '.$salesPerson['salesPersonLastName'];
                // $item_salesPerson = "<br><small>(" . implode("<br>", $salesPersonDetail) . ")</small>";
            }


            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID, $product->salesInvoiceProductSelectedUomId);
            $qty = ($product->salesInvoiceProductQuantity == 0) ? 1 : $product->salesInvoiceProductQuantity; // as a precaution
            $item_tot_tax = ($product->taxTotal > 0) ? $this->getProductUnitPriceViaDisplayUom((($product->taxTotal / $customCurrencyRate) / $product->salesInvoiceProductQuantity), $productUom) : 0.00;
            $item_tax_string = ($item_tot_tax > 0) ? "Tax: " . $data['currencySymbol'] . number_format($item_tot_tax, 2) : "";
            if ($product->salesInvoiceProductDiscountType == 'value') {
                $item_dic_string = ($product->salesInvoiceProductDiscount > 0) ? "Disc: " . $this->getProductUnitPriceViaDisplayUom(($product->salesInvoiceProductDiscount / $customCurrencyRate), $productUom) . " " . $data['currencySymbol'] : "";
                $product->totalDiscount = $qty * ($product->salesInvoiceProductDiscount / $customCurrencyRate);
            } else {
                $item_dic_string = ($product->salesInvoiceProductDiscount > 0) ? "Disc: " . $product->salesInvoiceProductDiscount . "%" : "";
                $product->totalDiscount = (($product->salesInvoiceProductPrice / $customCurrencyRate) * $qty) / 100 * $product->salesInvoiceProductDiscount;
            }

            // check if any subproducts (serial, batch) are available for this product
            $invoiceSubProductsDisplay = [];

            if (isset($invoiceSubProducts[$product->draftInvProductID])) {

                foreach ($invoiceSubProducts[$product->draftInvProductID] as $subProduct) {
                    switch ($subProduct['type']) {
                        case 'batch':
                        // case 'batch_serial':
                            $qty = $this->getProductQuantityViaDisplayUom($subProduct['qty'], $productUom);
                            $displayStrs = [];
                            $displayStrs[] = 'Batch: ' . $subProduct['code'];
                            $displayStrs[] = 'Quantity: ' . $qty['quantity'] . ' ' . $qty['uomAbbr'];
                            $displayStrs[] = ($subProduct['expiryDate']) ? 'Expire Date: ' . $subProduct['expiryDate'] : null;

                            $invoiceSubProductsDisplay[] = implode(', ', $displayStrs);
                            break;
                        case 'serial':
                            $displayStrs = [];
                            $displayStrs[] = 'Serial No: ' . $subProduct['code'];
                            $displayStrs[] = ($subProduct['warranty']) ? 'Warranty: ' . $subProduct['warranty'] : null;
                            $displayStrs[] = ($subProduct['expireDate']) ? 'Expire Date: ' . $subProduct['expireDate'] : null;

                            $invoiceSubProductsDisplay[] = implode(', ', array_filter($displayStrs));
                            break;
                        case 'batch_serial':
                            $qty = $this->getProductQuantityViaDisplayUom($subProduct['qty'], $productUom);
                            $displayStrs = [];
                            $displayStrs[] = 'Batch: ' . $subProduct['code'];
                            $displayStrs[] = 'Quantity: ' . $qty['quantity'] . ' ' . $qty['uomAbbr'];

                            $invoiceSubProductsDisplay[] = implode(', ', $displayStrs);

                            foreach ($subProduct['serial'] as $serialSubProduct) {
                                $displayStrs = [];
                                $displayStrs[] = '- Serial No: ' . $serialSubProduct['code'];
                                $displayStrs[] = ($serialSubProduct['warranty']) ? 'Warranty: ' . $serialSubProduct['warranty'] : null;

                                $invoiceSubProductsDisplay[] = implode(', ', array_filter($displayStrs));
                            }
                            break;
                    }
                }
            }
        

            $item_sub_pro_line = "";
            if (!empty($invoiceSubProductsDisplay)) {
                $item_sub_pro_line = "<br><small>(" . implode("<br>", $invoiceSubProductsDisplay) . ")</small>";
            }

            //add sub product serial and warranty details to the product array.
            $product->subProductserialAndWarranty = $item_sub_pro_line;

            $totalDiscount += floatval($product->totalDiscount);
            $sub_line = "";

            $productDescLine = "";

            //Product description line
            if ($product->salesInvoiceProductDescription != '' && $product->salesInvoiceProductDescription != NULL) {
                $productDescLine = "<br /><small class=\"description\"><span>(</span>" . $product->salesInvoiceProductDescription . "<span>)</span></small>";
                $productDiscription = $product->salesInvoiceProductDescription;

            }
            $item_salesPerson = (!empty($salesPersonDetail)) ? "<br/><small><span>(</span>" . $salesPersonDetail . "<span>)</span></small>" : "";

            if ($invoiceDetails->salesInvoiceShowTax == "1") {    // this must be rebuld by adding a column to the database.
                $sub_line_vals = array_filter(array($item_tax_string, $item_dic_string));
                if (!empty($sub_line_vals)) {
                    $sub_line = "<br /><small class=\"taxes\">(" . implode(",&nbsp;", $sub_line_vals) . ")</small>";
                }
                $product->unitPrice = ($product->salesInvoiceProductPrice / $customCurrencyRate);

            } else {
                $product->unitPrice = (floatval($product->salesInvoiceProductTotal / $customCurrencyRate) + floatval($product->totalDiscount)) / floatval($qty);
                $sub_line = ($product->salesInvoiceProductDiscount > 0) ? "<br><small>(" . $item_dic_string . ")</small>" : "";
            }
            // get template options from database and override the default options
            $templateDefaultOptions = array(
                'categorize_products' => false
            );


            //use to set discount and unite price when product tax exists.
            $totalTaxValuePerItem = 0;

            if ($product->productTaxEligible == '1') {
                if ($product->salesInvoiceProductDiscountType == 'precentage' && $invoiceDetails->salesInvoiceShowTax != "1") {
                    $totalTaxValuePerItem = ((floatval($product->taxTotal / $customCurrencyRate) * 100) / (100 - floatval($product->salesInvoiceProductDiscount))) / floatval($product->salesInvoiceProductQuantity);
                    $product->unitPrice = floatval($totalTaxValuePerItem) + floatval($product->salesInvoiceProductPrice / $customCurrencyRate);
                    $product->totalDiscount = ($product->unitPrice - (floatval($product->salesInvoiceProductTotal / $customCurrencyRate) / floatval($product->salesInvoiceProductQuantity))) * floatval($product->salesInvoiceProductQuantity);
                }
            }

            $unitPrice = $this->getProductUnitPriceViaDisplayUom(($product->unitPrice), $productUom);
            $mrpPrice = (!empty($product->salesInvoiceProductMrpType)) ? $this->getProductUnitPriceViaDisplayUom(($product->salesInvoiceProductMrpAmount), $productUom) : '0.00';
            $thisqty = $this->getProductQuantityViaDisplayUom($product->salesInvoiceProductQuantity, $productUom);
            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

            $lineItemDiscount = ($product->salesInvoiceProductDiscount > 0) ? (($product->salesInvoiceProductDiscountType == 'value') ? number_format($product->totalDiscount, 2) : $product->salesInvoiceProductDiscount . '%') : '-';
            $itemTotalWithoutDiscount = (($product->salesInvoiceProductPrice * $product->salesInvoiceProductQuantity) + $product->taxTotal) / $customCurrencyRate;

            $data['applied_pos_invoice_discount'] = ($data['pos'] && $data['salesInvoiceTotalDiscountType'] == 'presentage' && $data['salesInvoiceDiscountRate'] > 0) ? true : false;

            if($data['applied_pos_invoice_discount']){
                if(!$data['discountOnlyOnEligible'] || ($data['discountOnlyOnEligible'] && $product->productDiscountEligible)){
                    $product->productCode = "*" . $product->productCode;
                }
            }
            //calculate tax value per item
            $itemUnitTax = floatval($product->taxTotal / $customCurrencyRate)/ floatval($product->salesInvoiceProductQuantity);

            //calculate discount value per item
            $itemUnitDisc = floatval($product->totalDiscount / $customCurrencyRate)/ floatval($product->salesInvoiceProductQuantity);

            //calculate unit tax for real item price
            $realUnitTax = $this->calculateProductUnitTax($product->salesInvoiceProductPrice, $product->productID);
            $attribute_01 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeOneID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_02 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeTwoID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_03 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeThreeID,$product->productID)->current()['itemAttributeValueDescription'];

            $product_record = array(
                'product_index' => $productIndex + 1,
                'product_code' => $product->productCode,
                'product_name' => $product->productName . $productDescLine . $sub_line . $item_sub_pro_line . $item_salesPerson,
                'product_description' => $productDiscription,
                'product_code_and_name' => $product->productCode . ' - ' . $product->productName . $productDescLine . $sub_line . $item_sub_pro_line,
                'quantity' => $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'],
                'price' => number_format($unitPrice, 2),
                'mrp-price' => number_format($mrpPrice, 2),
                'price_w_tax' => number_format((floatval($product->salesInvoiceProductPrice) + floatval($realUnitTax)),2 ),
                'discount' => number_format($product->totalDiscount, 2),
                'discount_per_or_val' => $lineItemDiscount,
                'total' => number_format(($product->salesInvoiceProductTotal / $customCurrencyRate), 2),
                'total_wo_tx' => number_format(($product->salesInvoiceProductTotal - $product->taxTotal) / $customCurrencyRate, 2),
                'total_wo_discounts' => number_format($itemTotalWithoutDiscount, 2),
                'attribute_01' => (!is_null($attribute_01)) ? $attribute_01 : '-',
                'attribute_02' => (!is_null($attribute_02)) ? $attribute_02 : '-',
                'attribute_03' => (!is_null($attribute_03)) ? $attribute_03 : '-',
            );

            $subTotalWithoutDiscount = $subTotalWithoutDiscount + floatval($itemTotalWithoutDiscount);

            $templateOptions = array_merge($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);
            // filter only the columns that need to be displayed
            $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

            // nest products under category if they should be categorized
            if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
                $categoryId = $product->categoryID;
                $records[$categoryId]['products'][] = $product_record;

                // add category name to the list
                if (!isset($records[$categoryId]['category'])) {
                    $records[$categoryId]['category'] = (isset($allCategories[$categoryId])) ? $allCategories[$categoryId] : ''; // change this to category name
                }
            } else if ($templateOptions['categorize_products_by_dln'] && $templateOptions['categorize_products_by_dln'] != "false") {
                if ($product->documentTypeID == '4' && $product->salesInvoiceProductDocumentID != null) {
                    $records[$product->salesInvoiceProductDocumentID]['products'][] = $product_record;

                    // add category name to the list
                    if (!isset($records[$product->salesInvoiceProductDocumentID]['category'])) {
                        $records[$product->salesInvoiceProductDocumentID]['category'] = (isset($allDln[$product->salesInvoiceProductDocumentID])) ? $allDln[$product->salesInvoiceProductDocumentID] : '';
                    }
                } else {
                    $records[0]['products'][] = $product_record;

                    // add category name to the list
                    if (!isset($records[0]['category'])) {
                        $records[0]['category'] = 'Addional Items';
                    }
                }
            } else {
                $records[0]['products'][] = $product_record;
            }

            if ($invoiceDetails->inclusiveTax == "1") {
                $subAmount = number_format(($product->salesInvoiceProductTotal / $customCurrencyRate), 2);
                $subAmount = str_replace( ',', '', $subAmount );
                $sub_total+= (float)$subAmount;
            } else {
                $sub_total+= ($product->salesInvoiceProductTotal / $customCurrencyRate);
            }

            $product->unitPrice = $unitPrice;
            $product->itemQuntity = $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'];
            $data['productsList'][] = (array) $product;
            if($data['promotionID'] && count($promotionItemDetails)>0 && in_array($product->productID,$promotionItemDetails))
            {
                $itemWisePromoDetails[] = array(
                    'item' => $product->productName.' '.$product->productCode,
                    'itemDiscount' => $product->totalDiscount,
                    );
            }
        }

        $data['itemWiseProDetails'] = $itemWisePromoDetails;

        $tx_line = "";
        foreach ($txTypes as $tx => $vl) {
            $tx_line .= ($tx_line) ? ",&nbsp;" . $tx . ":&nbsp;" . $data['currencySymbol'] . number_format($vl, 2) :
                    $tx . ":&nbsp;" . $data['currencySymbol'] . number_format($vl, 2);
        }

        $tx_line = trim($tx_line, "&nbsp;");

        $headers = array(
            'product_index' => array('label' => _(""), 'align' => 'left'),
            'product_code' => array('label' => _("Item Code"), 'align' => 'left'),
            'product_name' => array('label' => _("Item Name"), 'align' => 'left'),
            'product_code_and_name' => array('label' => _("Item Code & Name"), 'align' => 'left'),
            'product_description' => array('label' => _("Product Description"), 'align' => 'left'),
            'attribute_01' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeOneID]), 'align' => 'left'),
            'attribute_02' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeTwoID]), 'align' => 'left'),
            'attribute_03' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeThreeID]), 'align' => 'left'),
            'quantity' => array('label' => _("Quantity"), 'align' => 'left'),
            'price' => array('label' => _("Unit Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'price_w_tax' => array('label' => _("Unit Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'mrp-price' => array('label' => _("MRP</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'discount' => array('label' => _("Total Discount (-)"), 'align' => 'right'),
            'discount_per_or_val' => array('label' => _("Discount (-)"), 'align' => 'right'),
            'total' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_tx' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_discounts' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
        );


        $footer_rows = array(
            'sub_total' => true,
            'discount' => true,
            'vat' => true,
            'total' => true
        );

        // filter only the headers that need to be displayed
        $headers = array_splice(array_replace(array_flip($allowedColumnsList), $headers), 0, count($allowedColumnsList));

        // filter only the footer rows that need to be displayed
        $footer_rows = array_splice(array_replace(array_flip($allowedRowsList), $footer_rows), 0, count($allowedRowsList));

        // set column width
        if (isset($templateOptions['product_table']['columns'])) {
            foreach ($templateOptions['product_table']['columns'] as $column) {
                $columnName = $column['name'];
                $columnWidth = $column['width'];

                // if column is in allowed list
                if (isset($headers[$columnName])) {
                    $headers[$columnName]['width'] = $columnWidth;
                }
            }
        }

        $product_record = count($product_record) ? $product_record : [];
        $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

        // set variables
        $data['table'] = array(
            'hide_table_header' => ($templateOptions['product_table']['hide_table_header'] === true || $templateOptions['product_table']['hide_table_header'] == 'true'),
            'hide_table_borders' => ($templateOptions['product_table']['hide_table_borders'] === true || $templateOptions['product_table']['hide_table_borders'] == 'true'),
            'headers' => $headers,
            'footers' => $footer_rows,
            'records' => $records,
        );
        $itemWiseDiscount = $totalDiscount;
        $totalDiscount += ($invoiceDetails->salesInvoiceTotalDiscount / $customCurrencyRate);
        
        if ($invoiceDetails->inclusiveTax == "1" && $invoiceDetails->templateInvoiceTotal != null) {
            $templateFinalTotal = $invoiceDetails->templateInvoiceTotal / $customCurrencyRate;
        } else {
            $templateFinalTotal = $invoiceDetails->salesinvoiceTotalAmount / $customCurrencyRate;
        }

        $data['salesInvoiceDeliveryCharge'] = $invoiceDetails->salesInvoiceDeliveryCharge / $customCurrencyRate;
        $data['serviceChargeDetails'] = $invoiceDetails->serviceChargeDetails;
        $data['delivery_charge'] = $invoiceDetails->salesInvoiceDeliveryCharge / $customCurrencyRate;
        $data['comment'] = $invoiceDetails->salesInvoiceComment;
        $data['salesInvoiceComment'] = $invoiceDetails->salesInvoiceComment;
        $data['sub_total'] = $sub_total;
        $data['sub_total_wo_tax'] = $sub_total - $totalTax;
        $data['sub_total_wo_discounts'] = $subTotalWithoutDiscount;
        $data['total'] = $templateFinalTotal;
        $data['totalTax'] = number_format($totalTax, 2);
        $data['tax_record'] = "<strong>" . _("Tax:") . "</strong>&nbsp;";
        $data['tax_record_line'] = $tx_line;
        $data['discountStatement'] = _("Total discount received:");
        $data['discount'] = number_format($totalDiscount, 2); // total discount received
        $data['discountWithPromoDisc'] = $totalDiscount + $data['salesInvoicePromotionDiscount'];
        $data['salesInvoiceRealPayedAmount'] = number_format($invoiceDetails->salesInvoicePayedAmount / $customCurrencyRate, 2);
        $data['salesInvoiceRemainPayAmount'] = number_format((floatval($invoiceDetails->salesinvoiceTotalAmount) - floatval($invoiceDetails->salesInvoicePayedAmount))/$customCurrencyRate, 2);
        $data['CustomerCurrentBalanceBeforeInvoice'] = (is_null($invoiceDetails->customerCurrentOutstanding)) ? '-' : 'Previous Outstanding: '.$data['currencySymbol'] .number_format($invoiceDetails->customerCurrentOutstanding / $customCurrencyRate, 2);
        $outstandingAfterInvoice = ($invoiceDetails->customerCurrentOutstanding / $customCurrencyRate) + ($invoiceDetails->salesinvoiceTotalAmount / $customCurrencyRate);
        $data['CustomerCurrentBalanceAfterInvoice'] = 'Outstanding After Invoice: '.$data['currencySymbol'] .number_format($outstandingAfterInvoice, 2);
        if ($data['customerShortName'] == 'DefaultCustomer') {
            $data['customerName'] = '';
        } else {
            $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];
        }
        $data['incomingPaymentPaidAmount'] = number_format(($paymentDetails->incomingPaymentPaidAmount / $customCurrencyRate), 2);
        $data['incomingPaymentBalance'] = number_format($paymentDetails->incomingPaymentBalance / $customCurrencyRate, 2);
        $data['salesInvoiceTotalDiscount'] = number_format($invoiceDetails->salesInvoiceTotalDiscount / $customCurrencyRate, 2);
        $data['salesinvoiceTotalAmountWithCurr'] = $data['currencySymbol'] . number_format($templateFinalTotal, 2);
        //add loyalty total points and earnedLoyalty points in to the data array.
        $loyaltyData=$this->CommonTable('Invoice\Model\InvoiceTable')->getLoyaltyDetailsByInvoiceID($invoiceID)->current();

        if ($loyaltyData['customerLoyaltyPoints'] != null)
        {
            $data['totalLoyaltyPoints'] = number_format($loyaltyData['customerLoyaltyPoints'],2);
            $data['earnedLoyaltyPoints'] = number_format($loyaltyData['collectedPoints'],2);
        }

        if ($invoiceID == 'template') {
            $data['template'] = true;
            $data['customerName'] = '[customerName]';
            $data['salesInvoiceCode'] = '[salesInvoiceCode]';
            $data['salesInvoiceIssuedDate'] = '[salesInvoiceIssuedDate]';
            $data['current_time'] = '[current_time]';
            $data['sub_total'] = '{{sub_total | currency : "' . $data['currencySymbol'] . '"}}';
            $data['totalTax'] = '{{totalTax | currency : "' . $data['currencySymbol'] . '" }}';
            $data['discount'] = '{{totalDiscount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['salesInvoiceTotalDiscount'] = '{{invoiceTotalDiscount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['incomingPaymentPaidAmount'] = '{{paidAmount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['incomingPaymentBalance'] = '{{paymentBalance | currency : "' . $data['currencySymbol'] . '" }}';
            $data['incomingPaymentDiscount'] = '{{paymentDiscount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['salesinvoiceTotalAmountWithCurr'] = $data['currencySymbol'] . '{{totalAmount | currency : "' . $data['currencySymbol'] . '" }}';
            $data['totalLoyaltyPoints'] = '[totalLoyaltyPoints]';
            $data['earnedLoyaltyPoints'] = '[earnedLoyaltyPoints]';
            $data['salesInvoiceComment'] = '[comment]';
        }
        $tempTotalValue = 0;
        //use to create total discount and itemwise discount
        $item_count = sizeof($data['productsList']);
        for ($i = 0; $i < $item_count; $i++) {
            $tempTotalValue+=$data['productsList'][$i]['totalDiscount'];
        }
        $suspendedTaxAmount = 0.00;
        if ($data['salesInvoiceSuspendedTax'] == 1) {
            $suspendedTaxAmount = $suspendedTaxesTotal;
        }

        $sub_tot = $data['sub_total'];
        $massDiscount = 0.00;
        if ($data['salesInvoiceSuspendedTax'] == 1) {
            $massDiscount = floatval($sub_tot) - floatval($suspendedTaxesTotal) - floatval($data['total']);
        } else {
            $massDiscount = floatval($sub_tot) - floatval($data['total']);
        }
        $mass_disc_without_product_dis = $massDiscount;
        $mass_disc = $tempTotalValue + $mass_disc_without_product_dis;
        if ($data['salesInvoiceDeliveryCharge']) {
            $mass_disc = $mass_disc + $data['salesInvoiceDeliveryCharge'];
            $mass_disc_without_product_dis = $mass_disc_without_product_dis + $data['salesInvoiceDeliveryCharge'];
        }
        $data['mass_disc_without_product_dis'] = abs($mass_disc_without_product_dis);
        $data['mass_discount'] = $mass_disc;
        if ($invoiceDetails->salesInvoiceTotalDiscountType != NULL) {
            $data['mass_discount'] = $itemWiseDiscount + ($invoiceDetails->salesInvoiceWiseTotalDiscount / $customCurrencyRate);
            $data['mass_disc_without_product_dis'] = ($invoiceDetails->salesInvoiceWiseTotalDiscount / $customCurrencyRate);
            if ($invoiceDetails->salesInvoicePromotionDiscount) {
                $data['mass_discount'] = $itemWiseDiscount + (($invoiceDetails->salesInvoiceWiseTotalDiscount + $invoiceDetails->salesInvoicePromotionDiscount) / $customCurrencyRate);
            }
        }

        // get promotion details
        if ($data['promotionID']) {
            $promotion = $this->CommonTable('Settings\Model\PromotionTable')->getPromotion($data['promotionID']);
            $data['promotion'] = (isset($promotion['promotionName'])) ? $promotion['promotionName'] : null;
        }
        return $this->_invoiceViewData = $data;
    }

    public function draftEditAction()
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID("Inventory\Controller\Product", "index", $this->user_session->userID);

        $isCanCreateItem = $isEnable;
        $this->getSideAndUpperMenus('Invoices', 'View Draft Invoices', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $invoiceID = $this->params()->fromRoute('param1');
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];

        $tax = array();
        $sup = array();
        $ctax = array();
        $productType = array();

        $getInvoiceProduct = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getDeliveryNoteCopiedProductsByInvoiceID($invoiceID);

        $deliveryNoteIDs = [];
        foreach ($getInvoiceProduct as $key => $value) {
            $deliveryNoteIDs[] = $value['salesInvoiceProductDocumentID'];
        }


        $invoiceData = $this->CommonTable('Invoice\Model\DraftInvWfTable')->getInvoiceByID($invoiceID);
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }

        // Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll(false,true);
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        $today = date('Y-m-d');
        $promotions = $this->CommonTable('Settings\Model\PromotionTable')->getToDayPromotions($today, $locationID);
        $promotionData = array();
        foreach ($promotions as $pro) {
            $pro = (object) $pro;
            $promotionData[$pro->promotionID] = $pro;
        }

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();

        //get priceLists
        $priceList = $this->getActivePriceList();

        $invoiceForm = new InvoiceForm(array(
            'name' => 'invoice',
            'id' => 'invoiceNo',
            'paymentTerms' => $terms,
            'salesPersons' => $salesPersons,
            'promotions' => $promotionData,
            'customCurrency' => $currency,
            'priceList'=>$priceList,

        ));


        $freeIssueItemModal = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $freeIssueItemModal->setTemplate('invoice/invoice/free-issue-item-modal');

        $serviceChargeView = new ViewModel();
        $serviceChargeView->setTemplate('invoice/invoice/add-service-charge-modal');

        $displaySetup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll()->current();

        $invoiceForm->get('customCurrencyId')->setAttribute('data-baseid', $displaySetup->currencyID);

        $invoiceForm->get('invoiceNo')->setValue($invoiceData->salesInvoiceCode);

        $userdateFormat = $this->getUserDateFormat();
        $attrDetails = [];
        $itemAttrDataSet = $this->CommonTable('/settings/Model/itemAttributeValueTable')->selectAll();
        foreach ($itemAttrDataSet as $key => $value) {
            $attrDetails[$value['itemAttributeID']] = array(
                'itemAttributeID' => $value['itemAttributeID'],
                'description' => $value['itemAttributeCode']." - ".$value['itemAttributeName'],
                );
        }
        $displaySetupDetails = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();

        $checkIsSalesInvoiceApprovalActive = $this->CommonTable('Settings/Model/ApprovalDocumentDetailsTable')->checkDocIsActiveForApprovalWF(1);
        $approvalWorkflows = [];

        if ($checkIsSalesInvoiceApprovalActive['isActive']) {
            $getApprovalWorkflows = $this->CommonTable('Settings/Model/ApprovalWorkflowsTable')->getApprovalWorkflowsByDocID(1);

            foreach ($getApprovalWorkflows as $key => $value) {
                $approvalWorkflows[] = $value;
            }

        }


        $invoice = new ViewModel(array(
            'locationID' => $locationID,
            'locationName' => $locationName,
            'locationCode' => $locationCode,
            'invoiceForm' => $invoiceForm,
            'cur' => $this->companyCurrencySymbol,
            'editInvoiceID' => $invoiceID,
            'currentLocationID' => $locationID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'cusID' => $invoiceData->customerID,
            'activeCusList' => $this->getActiveCustomerList(),
            'userDateFormat' => $userdateFormat,
            'useAccounting' => $this->useAccounting,
            'isCanCreateItem' => $isCanCreateItem,
            'multipleDeleNotes' => (count(array_unique($deliveryNoteIDs)) > 1) ? true : false,
            'attrDetails' => $attrDetails,
            'salesPersons' => $salesPersons,
            'isEnableForProductWiseSalesPerson' => $this->user_session->productWiseSalesPersonEnable,
            'isUseMrpValues' => $displaySetupDetails['useMrpSettings'],
            'isActiveApproval' => $checkIsSalesInvoiceApprovalActive['isActive'],
            'approvalWorkflows' => $approvalWorkflows,
            'isView' => false
        ));


        $viewMrpValluesView = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $viewMrpValluesView->setTemplate('invoice/invoice/view-mrp-values-modal');

        $invoiceForm->get('discount')->setAttribute('id', 'deliveryNoteDiscount');
        $customerForm = new AddCustomerForm('add_new_customer', $terms);
        $customerForm->setAttribute('id', 'add_customer_form');
        $customerForm->setAttribute('name', 'add_customer_form');
        $customerForm->get('customerDiscount')->setAttribute("id", "discnt");
        $addCustomer = new ViewModel(array(
            'form' => $customerForm,
            'action' => 'add',
        ));
        $addCustomer->setTemplate('/invoice/customer-api/get-customer-edit-box');
        $invoice->addChild($addCustomer, 'cust_formv');

        $invoiceProductsView = new ViewModel();
        $invoiceProductsView->setTemplate('invoice/invoice/invoice-note-add-products');
        $invoice->addChild($invoiceProductsView, 'invoiceProducts');

        $barCodeAddModal = new ViewModel();
        $barCodeAddModal->setTemplate('/inventory/product/add-bar-codes-modal');
        $invoice->addChild($barCodeAddModal, 'barCodeAddModal');

        $creditLimitValidationView = new ViewModel();
        $creditLimitValidationView->setTemplate('invoice/invoice/confirm-credit-limit-exceed-box');
        $invoice->addChild($creditLimitValidationView, 'creditLimitValidationView');

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAll();
        foreach ($supplier as $r) {
            $sup[$r->supplierID] = $r->supplierName;
        }

        $tax_resource = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
        foreach ($tax_resource as $t) {
            $tax[$t->id] = $t->taxName;
            if ($t->taxType == 'c') {
                $ctax[$t->id] = $t->taxName;
            }
        }

        $productForm = new ProductForm(array(
            'supplier' => $sup,
            'tax' => $tax,
            'productType' => $productType,
            'location' => $location,
        ));

        $financeAccountsArray = $this->getFinanceAccounts();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $productSalesAccountName = $financeAccountsArray[$productSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productSalesAccountID]['financeAccountsName'];
                $productForm->get('productSalesAccountID')->setAttribute('data-id',$productSalesAccountID);
                $productForm->get('productSalesAccountID')->setAttribute('data-value',$productSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $productInventoryAccountName = $financeAccountsArray[$productInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productInventoryAccountID]['financeAccountsName'];
                $productForm->get('productInventoryAccountID')->setAttribute('data-id',$productInventoryAccountID);
                $productForm->get('productInventoryAccountID')->setAttribute('data-value',$productInventoryAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $productCOGSAccountName = $financeAccountsArray[$productCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productCOGSAccountID]['financeAccountsName'];
                $productForm->get('productCOGSAccountID')->setAttribute('data-id',$productCOGSAccountID);
                $productForm->get('productCOGSAccountID')->setAttribute('data-value',$productCOGSAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $productAdjusmentAccountName = $financeAccountsArray[$productAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productAdjusmentAccountID]['financeAccountsName'];
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-id',$productAdjusmentAccountID);
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-value',$productAdjusmentAccountName);
            }
        }

        $productForm->get('submit')->setValue('Save');
        $productForm->get('productState')->setValue('Active');
        $productForm->setAttribute('id', 'create-product-form');

        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $defaultCategoryID = array_search('General', $allCategoryList);
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        $defaultUomID = $this->CommonTable('Settings\Model\UomTable')->getUomByName('Unit')->uomID;
        $categoryForm = new CategoryForm($allCategoryList);
        $categoryForm->get('categoryParentID')->setAttribute('data-id', $defaultCategoryID);
        $productHandelingForm = new ProductHandelingForm();
        $productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", true);
        $createProductView = new ViewModel(array(
            'productForm' => $productForm,
            'productHandelingForm' => $productHandelingForm,
            'categoryForm' => $categoryForm,
            'uomList' => $uoms,
            'tax' => $tax,
            'ctax' => $ctax,
            'location' => $location,
            'defaultUom' => $defaultUomID,
            'useAccounting' => $this->useAccounting
        ));
        $createProductView->setTemplate('invoice/invoice-api/create-product');


        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories', 'uomByID', 'taxes'
        ]);

        $this->getViewHelper('HeadScript')->prependFile('/js/product.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/category.js');

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";
        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');

        $invoice->addChild($dimensionAddView, 'dimensionAddView');
        
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $invoice->addChild($attachmentsView, 'attachmentsView');
        $invoice->addChild($serviceChargeView, 'serviceChargeView');
        $invoice->addChild($viewMrpValluesView, 'viewMrpValluesView');
        $invoice->addChild($freeIssueItemModal, 'freeIssueItemModal');

        $nextInvoiceDateView = new ViewModel(array(
            'userdateFormat' => $userdateFormat
        ));
        $nextInvoiceDateView->setTemplate('invoice/invoice/add-membership-modal');
        $invoice->addChild($nextInvoiceDateView, 'nextInvoiceDateView');

        $invoice->addChild($createProductView, 'createProductView');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories', 'uomByID', 'taxes'
        ]);

        return $invoice;
    }


    public function viewDraftInvoiceAction()
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID("Inventory\Controller\Product", "index", $this->user_session->userID);

        $isCanCreateItem = $isEnable;
        $this->getSideAndUpperMenus('Invoices', 'View Draft Invoices', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $invoiceID = $this->params()->fromRoute('param1');
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];

        $tax = array();
        $sup = array();
        $ctax = array();
        $productType = array();

        //get creditNotes related to current invoice ID
        $getCreditNote = $this->CommonTable("Invoice\Model\CreditNoteTable")->getCreditNotesByInvoiceID($invoiceID);
        //get dispatch notes related to current invoice
        $getDispatchNote = $this->CommonTable("Invoice\Model\DispatchNoteTable")->getDispatchNoteByInvoiceId($invoiceID);
        //get invoice produts copied from delivery Note by invoiceID
        $getInvoiceProduct = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getDeliveryNoteCopiedProductsByInvoiceID($invoiceID);

        $deliveryNoteIDs = [];
        foreach ($getInvoiceProduct as $key => $value) {
            $deliveryNoteIDs[] = $value['salesInvoiceProductDocumentID'];
        }

        if ($getCreditNote->current() != NULL) {
            $this->flashMessenger()->addMessage(['status' => FALSE, 'msg' => 'This invoice has a credit note.So you can not edit this invoice..!']);

            return $this->redirect()->toRoute(null, array(
                        'module' => 'invoice',
                        'controller' => 'invoice',
                        'action' => 'view')
            );
        } else if ($getDispatchNote != NULL) {
            $this->flashMessenger()->addMessage(['status' => FALSE, 'msg' => 'This invoice has a dispatch note.So you can not edit this invoice..!']);

            return $this->redirect()->toRoute(null, array(
                        'module' => 'invoice',
                        'controller' => 'invoice',
                        'action' => 'view')
            );
        }

        $invoiceData = $this->CommonTable('Invoice\Model\DraftInvWfTable')->getInvoiceByID($invoiceID);
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }

        // Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll(false,true);
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        $today = date('Y-m-d');
        $promotions = $this->CommonTable('Settings\Model\PromotionTable')->getToDayPromotions($today, $locationID);
        $promotionData = array();
        foreach ($promotions as $pro) {
            $pro = (object) $pro;
            $promotionData[$pro->promotionID] = $pro;
        }

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();

        //get priceLists
        $priceList = $this->getActivePriceList();

        $invoiceForm = new InvoiceForm(array(
            'name' => 'invoice',
            'id' => 'invoiceNo',
            'paymentTerms' => $terms,
            'salesPersons' => $salesPersons,
            'promotions' => $promotionData,
            'customCurrency' => $currency,
            'priceList'=>$priceList,

        ));


        $freeIssueItemModal = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $freeIssueItemModal->setTemplate('invoice/invoice/free-issue-item-modal');

        $serviceChargeView = new ViewModel();
        $serviceChargeView->setTemplate('invoice/invoice/add-service-charge-modal');

        $displaySetup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll()->current();

        $invoiceForm->get('customCurrencyId')->setAttribute('data-baseid', $displaySetup->currencyID);

        $invoiceForm->get('invoiceNo')->setValue($invoiceData->salesInvoiceCode);

        $userdateFormat = $this->getUserDateFormat();
        $attrDetails = [];
        $itemAttrDataSet = $this->CommonTable('/settings/Model/itemAttributeValueTable')->selectAll();
        foreach ($itemAttrDataSet as $key => $value) {
            $attrDetails[$value['itemAttributeID']] = array(
                'itemAttributeID' => $value['itemAttributeID'],
                'description' => $value['itemAttributeCode']." - ".$value['itemAttributeName'],
                );
        }
        $displaySetupDetails = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();

        $checkIsSalesInvoiceApprovalActive = $this->CommonTable('Settings/Model/ApprovalDocumentDetailsTable')->checkDocIsActiveForApprovalWF(1);
        $approvalWorkflows = [];

        if ($checkIsSalesInvoiceApprovalActive['isActive']) {
            $getApprovalWorkflows = $this->CommonTable('Settings/Model/ApprovalWorkflowsTable')->getApprovalWorkflowsByDocID(1);

            foreach ($getApprovalWorkflows as $key => $value) {
                $approvalWorkflows[] = $value;
            }

        }


        $invoice = new ViewModel(array(
            'locationID' => $locationID,
            'locationName' => $locationName,
            'locationCode' => $locationCode,
            'invoiceForm' => $invoiceForm,
            'cur' => $this->companyCurrencySymbol,
            'editInvoiceID' => $invoiceID,
            'currentLocationID' => $locationID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'cusID' => $invoiceData->customerID,
            'activeCusList' => $this->getActiveCustomerList(),
            'userDateFormat' => $userdateFormat,
            'useAccounting' => $this->useAccounting,
            'isCanCreateItem' => $isCanCreateItem,
            'multipleDeleNotes' => (count(array_unique($deliveryNoteIDs)) > 1) ? true : false,
            'attrDetails' => $attrDetails,
            'salesPersons' => $salesPersons,
            'isEnableForProductWiseSalesPerson' => $this->user_session->productWiseSalesPersonEnable,
            'isUseMrpValues' => $displaySetupDetails['useMrpSettings'],
            'isActiveApproval' => $checkIsSalesInvoiceApprovalActive['isActive'],
            'approvalWorkflows' => $approvalWorkflows,
            'isView' => true
        ));


        $viewMrpValluesView = new ViewModel(array(
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
        ));
        $viewMrpValluesView->setTemplate('invoice/invoice/view-mrp-values-modal');

        $invoiceForm->get('discount')->setAttribute('id', 'deliveryNoteDiscount');
        $customerForm = new AddCustomerForm('add_new_customer', $terms);
        $customerForm->setAttribute('id', 'add_customer_form');
        $customerForm->setAttribute('name', 'add_customer_form');
        $customerForm->get('customerDiscount')->setAttribute("id", "discnt");
        $addCustomer = new ViewModel(array(
            'form' => $customerForm,
            'action' => 'add',
        ));
        $addCustomer->setTemplate('/invoice/customer-api/get-customer-edit-box');
        $invoice->addChild($addCustomer, 'cust_formv');

        $invoiceProductsView = new ViewModel();
        $invoiceProductsView->setTemplate('invoice/invoice/invoice-note-add-products');
        $invoice->addChild($invoiceProductsView, 'invoiceProducts');

        $barCodeAddModal = new ViewModel();
        $barCodeAddModal->setTemplate('/inventory/product/add-bar-codes-modal');
        $invoice->addChild($barCodeAddModal, 'barCodeAddModal');

        $creditLimitValidationView = new ViewModel();
        $creditLimitValidationView->setTemplate('invoice/invoice/confirm-credit-limit-exceed-box');
        $invoice->addChild($creditLimitValidationView, 'creditLimitValidationView');

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $supplier = $this->CommonTable('Inventory\Model\SupplierTable')->fetchAll();
        foreach ($supplier as $r) {
            $sup[$r->supplierID] = $r->supplierName;
        }

        $tax_resource = $this->CommonTable('Settings\Model\TaxTable')->fetchAll();
        foreach ($tax_resource as $t) {
            $tax[$t->id] = $t->taxName;
            if ($t->taxType == 'c') {
                $ctax[$t->id] = $t->taxName;
            }
        }

        $productForm = new ProductForm(array(
            'supplier' => $sup,
            'tax' => $tax,
            'productType' => $productType,
            'location' => $location,
        ));

        $financeAccountsArray = $this->getFinanceAccounts();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
                $productSalesAccountName = $financeAccountsArray[$productSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productSalesAccountID]['financeAccountsName'];
                $productForm->get('productSalesAccountID')->setAttribute('data-id',$productSalesAccountID);
                $productForm->get('productSalesAccountID')->setAttribute('data-value',$productSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
                $productInventoryAccountName = $financeAccountsArray[$productInventoryAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productInventoryAccountID]['financeAccountsName'];
                $productForm->get('productInventoryAccountID')->setAttribute('data-id',$productInventoryAccountID);
                $productForm->get('productInventoryAccountID')->setAttribute('data-value',$productInventoryAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
                $productCOGSAccountName = $financeAccountsArray[$productCOGSAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productCOGSAccountID]['financeAccountsName'];
                $productForm->get('productCOGSAccountID')->setAttribute('data-id',$productCOGSAccountID);
                $productForm->get('productCOGSAccountID')->setAttribute('data-value',$productCOGSAccountName);
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
                $productAdjusmentAccountName = $financeAccountsArray[$productAdjusmentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$productAdjusmentAccountID]['financeAccountsName'];
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-id',$productAdjusmentAccountID);
                $productForm->get('productAdjusmentAccountID')->setAttribute('data-value',$productAdjusmentAccountName);
            }
        }

        $productForm->get('submit')->setValue('Save');
        $productForm->get('productState')->setValue('Active');
        $productForm->setAttribute('id', 'create-product-form');

        $allCategoryList = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        $defaultCategoryID = array_search('General', $allCategoryList);
        $uoms = $this->CommonTable('Settings\Model\UomTable')->activeFetchAll();
        $defaultUomID = $this->CommonTable('Settings\Model\UomTable')->getUomByName('Unit')->uomID;
        $categoryForm = new CategoryForm($allCategoryList);
        $categoryForm->get('categoryParentID')->setAttribute('data-id', $defaultCategoryID);
        $productHandelingForm = new ProductHandelingForm();
        $productHandelingForm->get('productHandelingManufactureProduct')->setAttribute("checked", true);
        $createProductView = new ViewModel(array(
            'productForm' => $productForm,
            'productHandelingForm' => $productHandelingForm,
            'categoryForm' => $categoryForm,
            'uomList' => $uoms,
            'tax' => $tax,
            'ctax' => $ctax,
            'location' => $location,
            'defaultUom' => $defaultUomID,
            'useAccounting' => $this->useAccounting
        ));
        $createProductView->setTemplate('invoice/invoice-api/create-product');


        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories', 'uomByID', 'taxes'
        ]);

        $this->getViewHelper('HeadScript')->prependFile('/js/product.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/inventory/category.js');

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";
        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');

        $invoice->addChild($dimensionAddView, 'dimensionAddView');
        
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $invoice->addChild($attachmentsView, 'attachmentsView');
        $invoice->addChild($serviceChargeView, 'serviceChargeView');
        $invoice->addChild($viewMrpValluesView, 'viewMrpValluesView');
        $invoice->addChild($freeIssueItemModal, 'freeIssueItemModal');

        $nextInvoiceDateView = new ViewModel(array(
            'userdateFormat' => $userdateFormat
        ));
        $nextInvoiceDateView->setTemplate('invoice/invoice/add-membership-modal');
        $invoice->addChild($nextInvoiceDateView, 'nextInvoiceDateView');

        $invoice->addChild($createProductView, 'createProductView');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'categories', 'uomByID', 'taxes'
        ]);

        return $invoice;
    }

        /**
     * @author Damith Thamara <damith@thinkcube.com>
     * document action should be called inside the view
     * @return viewModel Grn View ViewModel
     */
    public function draftPreviewAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        if ($paramIn != NULL) {
            $data = $this->getDataForDraftDocument($paramIn);
            $data['invCode'] = $data['gCd'];
            $data['total'] = number_format($data['gT'], 2);
            $path = "/invoice/draftDocument/"; //.$paramIn;
            $translator = new Translator();
            $createNew = $translator->translate('New Invoice');
//            $createNew = "New GRN";
            $createPath = "/invoice/create";
            $gT = number_format($data['gT'], 2);

            $documentType = 'Sales Invoice';
            $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $paramIn, $createPath);
            return $preview;
        }
    }

    /**
     * this functoion will echo the print view of a grn (ID should be passed as a param1)
     * @return viewModel Grn View ViewModel
     */
    public function draftDocumentAction()
    {
        $paramIn = $this->params()->fromRoute('param1');
        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        $documentType = 'Sales Invoice';
        $invID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        // If template ID is not passed, get default template
        if (!$templateID) {
            $defaultTpl = $tpl->getDefaultTemplate($documentType);
            $templateID = $defaultTpl['templateID'];
        }

        // if saved document file is available, render it
        $filename = $this->getDocumentFileName('Draft Sales Invoice', $invID, $templateID);
//        if ($document = $tpl->getDocumentFile($filename, $documentType)) {
//            echo $document;
//            exit;
//        }
        // Get template details
        $templateDetails = $tpl->getTemplateByID($templateID);

        $data = $this->getDataForDraftDocument($invID);
        // Get data table (eg: products list, total, etc.)
        $dataTableView = new ViewModel(array());
        $dataTableView->setTerminal(TRUE);
        switch (strtolower($templateDetails['documentSizeName'])) {
            case 'a4 (portrait)':
            case 'a4 (landscape)':
            case 'a5 (portrait)':
            case 'a5 (landscape)':
                $data['data_table'] = $this->getDocumentDataTable($grnID, strtolower(trim(preg_replace('/\(.*\)/i', '', $templateDetails['documentSizeName']))));
                break;
        }
        echo $tpl->renderTemplateByID($templateID, $data, $filename);
        exit;
    }


    public function draftViewAction()
    {
        $this->setLogMessage("Invoice View List accessed");
        $this->getSideAndUpperMenus('Invoices', 'View Draft Invoices', 'SALES');
        $posflag = 0;
        $this->getPaginatedDraftInvoices(15, $posflag, $considerJob = true);
        $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        $dateFormat = $this->getUserDateFormat();
        $showVehicleNum = false;
        if($displaySettings->jobModuleEnabled == 1 &&  $displaySettings->jobBusinessType == 2) {
            $showVehicleNum = true;
        }

        $invoiceView = new ViewModel(array(
            'invoices' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat,
            'showVehicleNum' => $showVehicleNum,
            'companyCurrencySymbol' => $this->companyCurrencySymbol
                )
        );
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('invoice/invoice/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/invoice/document-view');
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $invoiceView->addChild($attachmentsView, 'attachmentsView');
        $invoiceView->addChild($docHistoryView, 'docHistoryView');
        $invoiceView->addChild($documentView, 'documentView');

        return $invoiceView;
    }


     public function getVehicleRegNumber($jobID)
    {
        try {
            $getVehicleByJobId = $this->CommonTable('Jobs\Model\JobVehicleTable')->getServiceVehicleByJobID($jobID)->current();

            if ($getVehicleByJobId) {
                return $getVehicleByJobId['serviceVehicleRegNo'];
            } else {
                return '-';
            }
        } catch (Exception $e) {
            return '-';
        }
    }
}

/////////////////// END OF IMVOICE CONTROLLER \\\\\\\\\\\\\\\\\\\\
