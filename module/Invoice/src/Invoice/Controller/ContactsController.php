<?php

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Invoice\Form\DispatchNoteForm;
use Zend\I18n\Translator\Translator;
use Core\Controller\Interfaces\DocumentInterface;
use Invoice\Form\DataimportForm;

class ContactsController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'contacts_upper_menu';
    protected $downMenus = '';
    protected $userID;
    protected $cdnUrl;
    protected $user_session;    
    private $dispatchNoteViewData;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID       = $this->user_session->userID;
        $this->username     = $this->user_session->username;
        $this->company      = $this->user_session->companyDetails;
        $this->cdnUrl       = $this->user_session->cdnUrl;
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Contacts', 'Contact Master', 'CRM');


        $this->getContactPaginatedList(10);
        $mobNumbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->fetchAll();

        $contactNumebrs = [];
        foreach ($mobNumbers as $key => $value) {
            $contactNumebrs[$value['contactID']][$value['mobileNumberType']] = $value['mobileNumber'];
        }


        $emails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->fetchAll();

        $contactEmails = [];
        foreach ($emails as $key2 => $value2) {
            $contactEmails[$value2['contactID']][$value2['emailType']] = $value2['emailAddress'];
        }

        $view = new ViewModel(array(
            'contactList' => $this->paginator,
            'contactNumebrs' => $contactNumebrs,
            'contactEmails' => $contactEmails ,
            'isPaginated' => true
        ));
        
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/contacts.js');
        
        return $view;
    }

    private function getContactPaginatedList($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Invoice\Model\ContactsTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
    }

    public function contactListAction()
    {
        $this->setLogMessage("Contact list view accessed");
        $this->getSideAndUpperMenus('Contacts', 'Contact List', 'CRM');


        $this->getContactListPaginatedList(10);

        $view = new ViewModel(array(
            'contactList' => $this->paginator,
            'isPaginated' => true
        ));
        
        $form = new DataimportForm();
        $form->get('submit')->setValue(_('Next'));

        $contactListView = new ViewModel();
        $contactListView->setTemplate('invoice/contacts/contact-list-modal');

        $contactCreateView = new ViewModel();
        $contactCreateView->setTemplate('invoice/contacts/contact-create-modal');

        $contactCsvImportView = new ViewModel(array(
            'form' => $form
        ));
        $contactCsvImportView->setTemplate('invoice/contacts/contact-csv-import');

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/contactsList.js');

        $view->addChild($contactListView, 'contactListView');
        $view->addChild($contactCreateView, 'contactCreateView');
        $view->addChild($contactCsvImportView, 'contactCsvImportView');

        return $view;
    }

    private function getContactListPaginatedList($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Invoice\Model\ContactListsTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
    }

    public function importAction()
    {
        $this->setLogMessage("Contact master import view accessed");
        $this->getSideAndUpperMenus('Contacts', 'Contact Master', 'CRM');

        $form = new DataimportForm();
        $form->get('submit')->setValue(_('Next'));

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/contacts.js');

        $view = new ViewModel(array(
            'form' => $form
        ));

        $request = $this->getRequest();

        if ($request->isPost()) {

            if ($_FILES["fileupload"]["tmp_name"] == null) {
                $view = new ViewModel(array('form' => $form, 'fileerror' => 'nofile'));
                $view->setTemplate("invoice/contacts/contact-csv-import");
                return $view;
            } else if (substr($_FILES["fileupload"]["name"], -3) != 'csv') {
                $view = new ViewModel(array('form' => $form, 'fileerror' => 'utferror'));
                $view->setTemplate("invoice/contacts/contact-csv-import");
                return $view;
            }
            if ($request->getPost('delimiter') == 0) {
                $delim = ',';
            } elseif ($request->getPost('delimiter') == 1) {
                $delim = ';';
            } elseif ($request->getPost('delimiter') == 2) {
                $delim = '.';
            } else {
                $delim = ',';
            }

            if ($request->getPost('header') == 1) {
                $headerchecked = TRUE;
            } else {
                $headerchecked = FALSE;
            }
            $content = fopen($_FILES["fileupload"]["tmp_name"], "r");
            $encodingtype = $request->getPost('characterencoding');
            $fileenc = file($_FILES["fileupload"]["tmp_name"]);
            $string = $fileenc[0];
            if ($encodingtype == 0) {
                $enres = mb_check_encoding($string, 'UTF-8');
                if ($enres == FALSE) {
                    $view = new ViewModel(array('form' => $form, 'fileerror' => 'utferror'));
                    $view->setTemplate("invoice/contacts/contact-csv-import");
                    return $view;
                }
            } elseif ($encodingtype == 1) {
                $enres = mb_check_encoding($string, 'ISO-8859-1');
                if ($enres == FALSE) {
                    $view = new ViewModel(array('form' => $form, 'fileerror' => 'isoerror'));
                    $view->setTemplate("invoice/contacts/contact-csv-import");
                    return $view;
                }
            }
            move_uploaded_file($_FILES["fileupload"]["tmp_name"], '/tmp/ezBiz.contactImportData.csv');
            chmod('/tmp/ezBiz.contactImportData.csv', 0777);

            $columncount = 0;

            if ($headerchecked == TRUE) {
                $headerlist = fgetcsv($content, 1000, $delim);
                $firstdataline = fgetcsv($content, 1000, $delim);
                for ($headers = 0; $headers < sizeof($headerlist); $headers++) {
                    $da[$headers]['header'] = $headerlist[$headers];
                    $da[$headers]['row'] = $firstdataline[$headers];
                }
                $columncount = sizeof($headerlist);
            } elseif ($headerchecked == FALSE) {
                $firstdataline = fgetcsv($content, 1000, $delim);
                for ($columns = 0; $columns < sizeof($firstdataline); $columns++) {
                    $da[$columns]['header'] = "Column" . ($columns + 1);
                    $da[$columns]['row'] = $firstdataline[$columns];
                }
                $columncount = sizeof($firstdataline);
            }
            $fileData = $this->getFileData($request->getPost()->toArray(),$request->getFiles()->toArray());
            $view = new ViewModel(array(
                'data' => $da,
                'form' => $form,
                'header' => TRUE,
                'delim' => $delim,
                'columns' => $columncount,
                'fData' => $fileData['data']
            ));
            $this->getViewHelper('HeadScript')->prependFile('/js/invoice/contacts.js');
            $view->setTemplate("invoice/contacts/contact-import-mapping");
            return $view;
        }

        $view->setTemplate('invoice/contacts/contact-csv-import');
        return $view;
    }

    private function getFileData($data, $file)
    {
        $fileName = '/tmp/ezBiz.contactImportData.csv';
        move_uploaded_file( $file['fileupload']["tmp_name"], $fileName);
        chmod( $fileName, 0777);

        $fileenc = file($fileName);
        $encodingStatus = mb_check_encoding($fileenc[0], ($data['characterencoding'] == 0) ? 'UTF-8' : 'ISO-8859-1');

        if(!$encodingStatus){
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_ENCODING'),
                'data' => []
            ];
        }

        if (($handle = fopen($fileName, "r")) == FALSE) {
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_READ'),
                'data' => []
            ];
        }

        $dataArr = [];
        while (($fileData = fgetcsv($handle, 1000, $data['delimiter'])) !== FALSE) {
            $rowElementStatusArr = [];
            $rowElementStatusArr = array_map(function($element){
                $element = trim($element);
                return (empty($element)) ? false : true;
            }, $fileData);
            if(in_array(true, $rowElementStatusArr)){//for ignore empty lines
                $dataArr[] = $fileData;
            }
        }
        fclose($handle);
        return [
            'status' => true,
            'msg' => $this->getMessage('SUCC_UPLOADED_FILE'),
            'data' => $dataArr
        ];
    }

}