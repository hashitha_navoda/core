<?php

/**
 * Description of CustomerCategory
 *
 * @author Sharmilan <sharmilan@thinkcube.com>
 */

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Form\CustomerCategoryForm;
use Invoice\Model\CustomerCategory;

class CustomerCategoryController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'customer_upper_menu';
    protected $paginator;
    protected $userID;
    protected $username;
    protected $user_session;
    protected $sidemenu;
    protected $uppermenu;

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Customers', 'Customer Category', 'CRM');

        $this->getPaginatedCustomerCategory();
        $customerCategoryForm = new CustomerCategoryForm();

        $indexView = new ViewModel(array(
            'form' => $customerCategoryForm,
            'categoryList' => $this->paginator,
            'isPaginated' => true,
        ));

        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');
        return $indexView;
    }

    protected function getPaginatedCustomerCategory($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
    }

    public function saveCustomerCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $customerCategory = new CustomerCategory();

            $customerCategoryForm = new CustomerCategoryForm();
            $customerCategoryForm->setInputFilter($customerCategory->getInputFilter());
            $customerCategoryForm->setData($request->getPost());
            if ($customerCategoryForm->isValid()) {
                $customerCategory->exchangeArray($customerCategoryForm->getData());
//              check same name already exists
                $existingCategory = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->getByCustomerCategoryName($customerCategory->customerCategoryName);
                if (isset($existingCategory[0]) && $existingCategory[0]['customerCategoryID'] !== $customerCategory->customerCategoryID) {
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CATEG_EXIST');
                    $this->status = FALSE;
                    return $this->JSONRespond();
                }

                $insertedID = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->saveCustomerCategory($customerCategory);
                if (!$insertedID) {
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CATEG_INSERT');
                    $this->status = FALSE;
                } else {
                    $this->getPaginatedCustomerCategory();
                    $categoryListView = new ViewModel(array(
                        'categoryList' => $this->paginator,
                        'isPaginated' => true,
                    ));
                    $categoryListView->setTerminal(true);
                    $categoryListView->setTemplate('invoice/customer-category/customer-category-list');
                    $this->msg = $this->getMessage('SUCC_CUSTAPI_CUST_CATEG_INSERT');
                    $this->status = TRUE;
                    $this->html = $categoryListView;
                }
            }
            return $this->JSONRespondHtml();
        }
    }

    public function getCustomerCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerCategory = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->getByCustomerCategoryID($request->getPost('customerCategoryID'));
            foreach ($customerCategory as $value) {
                $value = (object) $value;
            }
            $this->status = true;
            $this->data = $value;
            return $this->JSONRespond();
        }
    }

    public function updateCustomerCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {

            $customerCategory = new CustomerCategory();
            $customerCategoryForm = new CustomerCategoryForm();
            $customerCategoryForm->setInputFilter($customerCategory->getInputFilter());
            $customerCategoryForm->setData($request->getPost());

            if ($customerCategoryForm->isValid()) {
                $customerCategory->exchangeArray($customerCategoryForm->getData());
//                check alredy exists with same name
                $existingCategory = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->getByCustomerCategoryName($customerCategory->customerCategoryName);
                if (isset($existingCategory[0]) && $existingCategory[0]['customerCategoryID'] !== $customerCategory->customerCategoryID) {
                    $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CATEG_EXIST');
                    $this->status = FALSE;
                    return $this->JSONRespond();
                }

                $this->CommonTable('Invoice\Model\CustomerCategoryTable')->updateCustomerCategory($customerCategory);

                $this->getPaginatedCustomerCategory();
                $categoryListView = new ViewModel(array(
                    'categoryList' => $this->paginator,
                    'isPaginated' => true,
                ));
                $categoryListView->setTerminal(true);
                $categoryListView->setTemplate('invoice/customer-category/customer-category-list');
                $this->msg = $this->getMessage('SUCC_CUSTAPI_CUST_CATEG_UPDATE');
                $this->status = TRUE;
                $this->html = $categoryListView;
            }
            return $this->JSONRespondHtml();
        }
    }

    public function searchCustomerCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($request->getPost('searchKey') === '') {
                $this->getPaginatedCustomerCategory();
                $categoryListView = new ViewModel(array(
                    'categoryList' => $this->paginator,
                    'isPaginated' => TRUE,
                ));
            } else {
                $resultCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->searchByCustomerCategoryName($request->getPost('searchKey'));
                $categoryListView = new ViewModel(array(
                    'categoryList' => $resultCategories,
                    'isPaginated' => FALSE,
                ));
            }
            $categoryListView->setTerminal(true);
            $categoryListView->setTemplate('invoice/customer-category/customer-category-list');
            $this->status = TRUE;
            $this->html = $categoryListView;
        }
        return $this->JSONRespondHtml();
    }

    public function deleteCustomerCategoryAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
//            Check is alredy used anywhere
            $isUsed = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->getCustomerByCategoryID($request->getPost('customerCategoryID'));
            if ($isUsed['customerID']) {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_CATEG_CANNOT_DELETE');
                return $this->JSONRespond();
            }
            $deleteID = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->deleteCustomerCategory($request->getPost('customerCategoryID'));
            if ($deleteID) {
                $this->getPaginatedCustomerCategory();
                $categoryListView = new ViewModel(array(
                    'categoryList' => $this->paginator,
                    'isPaginated' => TRUE,
                ));
                $categoryListView->setTerminal(true);
                $categoryListView->setTemplate('invoice/customer-category/customer-category-list');
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_CUSTAPI_CUST_CATEG_DELETE');
                $this->html = $categoryListView;
            }
        }
        return $this->JSONRespondHtml();
    }

}
