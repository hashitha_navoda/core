<?php

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Inventory\Model\ProductBatch;
use Inventory\Model\ProductSerial;
use Invoice\Model\DeliveryNote;
use Invoice\Model\DeliveryNoteProduct;
use Invoice\Model\DeliveryNoteSubProduct;
use Invoice\Model\DeliveryNoteProductTax;
use Inventory\Model\ItemOut;
use Inventory\Model\ItemIn;
use Zend\Session\Container;
use Zend\View\Model\JsonModel;
use Invoice\Model\DeliveryNoteEditLog;
use Invoice\Model\DeliveryNoteEditLogDetails;

class DeliveryNoteAPIController extends CoreController
{
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function saveDeliverDetailsAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DELINOTEAPI_PRODUCT_SAVE');
            return $this->JSONRespond();
        }

        $customerID = $request->getPost('customer');
        $customerName = $request->getPost('customerName');

        $customCurrencyId = $request->getPost('customCurrencyId');
        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "") ? $customCurrencyData->currencyRate : 1;
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $ccrate = $request->getPost['customCurrencyRate'];
        //if custom currency rate alredy set add that rate to sales Order
        if ($request->getPost('salesOrderID') && !($ccrate == 0 || $ccrate == 1)) {
            $customCurrencyRate = $ccrate;
        }

        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        if ($cust->customerName . '-' . $cust->customerCode != $customerName) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DELINOTEAPI_VALID_CUST');
            return $this->JSONRespond();
        }

        $products = $request->getPost('products');
        $deliverCode = $request->getPost('deliverCode');
        $locationOut = $request->getPost('locationOutID');
        $date = $this->convertDateToStandardFormat($request->getPost('date'));
        $deliveryCharge = $request->getPost('deliveryCharge') * $customCurrencyRate;
        $totalPrice = trim($request->getPost('deliveryTotalPrice')) * $customCurrencyRate;
        $deliveryAddress = $request->getPost('deliveryAddress');
        $comment = $request->getPost('deliveryComment');
        $salesOrderID = $request->getPost('salesOrderID');
        $salesPersonID = $request->getPost('salesPersonID');
        $priceListId = $request->getPost('priceListId');
        $customerProfID = $request->getPost('customer_prof_ID');
        $dimensionData = $request->getPost('dimensionData');
        $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');

        if (!empty($salesOrderID)) {
            $fromSalesOrder = TRUE;
        } else {
            $fromSalesOrder = FALSE;
        }

        $itemAttrDetailsSet = [];
        foreach ($products as $key => $p) {
            $productID = $p['productID'];
            $itemAttributeMap = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->getDetailsByProductID($productID);
            foreach ($itemAttributeMap as $key => $value) {
                if($value['itemAttributeID'] == 1){
                    $itemAttrDetailsSet[$productID][] = $value['itemAttributeValueID'];
                }
            }
        }

        //check the same credit period is exist for all items
        if(count($itemAttrDetailsSet) > 1){
            foreach ($itemAttrDetailsSet as $ckey => $cvalue) {
                if(isset($oldVal)){
                    $newVal = $cvalue;
                    $oldVal = array_intersect($oldVal, $newVal);
                    if(!(count($oldVal) > 0)){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DELINOTEAPI_ITEMATTRIBUTE_CREDIT_PERIOD');
                        return $this->JSONRespond();
                        break;
                    }
                }else{
                    $oldVal = $cvalue;
                }
            }
        }

        $this->beginTransaction();

        $result = $this->getReferenceNoForLocation('6', $locationOut);
        $locationReferenceID = $result['locRefID'];

        // check if a Delivery from the same Delivery Code exists if exist add next number to delivery note code
        while ($deliverCode) {
            if (!$this->CommonTable('Invoice\Model\DeliveryNoteTable')->checkDeliveryNoteByCode($deliverCode)->current()) {
                break;
            }

            if ($locationReferenceID) {
                $newDeliverCode = $this->getReferenceNumber($locationReferenceID);
                if ($newDeliverCode == $deliverCode) {
                    $this->updateReferenceNumber($locationReferenceID);
                    $deliverCode = $this->getReferenceNumber($locationReferenceID);
                } else {
                    $deliverCode = $newDeliverCode;
                }
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DELINOTEAPI_DELICODE_EXIST');
                return $this->JSONRespond();
            }
        }

        if ($locationReferenceID) {
            $this->updateReferenceNumber($locationReferenceID);
        }

        $deliveryData = array(
            'deliveryNoteCode' => $deliverCode,
            'customerID' => $customerID,
            'deliveryNoteLocationID' => $locationOut,
            'deliveryNoteDeliveryDate' => $date,
            'deliveryNoteCharge' => $deliveryCharge,
            'deliveryNotePriceTotal' => $totalPrice,
            'deliveryNoteAddress' => $deliveryAddress,
            'deliveryNoteComment' => $comment,
            'salesOrderID' => $salesOrderID,
            'salesPersonID' => $salesPersonID,
            'customCurrencyId' => $customCurrencyId,
            'deliveryNoteCustomCurrencyRate' => $customCurrencyRate,
            'priceListId' => $priceListId,
            'customerProfileID' => $customerProfID,
        );
        
        $res = $this->saveDeliverDetails($deliveryData, $products, $request->getPost('subProducts'), false, $dimensionData, $ignoreBudgetLimit);

        if ($res['status']) {
            $this->commit();
            $this->flashMessenger()->addMessage($res['msg']);
        } else {
            $this->rollback();
        }

        $this->status = $res['status'];
        $this->msg = $res['msg'];
        $this->data = $res['data'] ? $res['data'] : [];

        return $this->JSONRespond();
    }


    public function saveDeliverDetails($deliveryData, $products, $subProducts, $jobId = false, $dimensionData, $ignoreBudgetLimit = true)
    {

        $locationOut = $deliveryData['deliveryNoteLocationID'];
        $deliverCode = $deliveryData['deliveryNoteCode'];
        $customCurrencyRate = $deliveryData['deliveryNoteCustomCurrencyRate'];

        $deliveryData['entityID'] = $this->createEntity();
        $deliveryData['jobId'] = $jobId;

        $deliveryNote = new DeliveryNote;
        $deliveryNote->exchangeArray($deliveryData);

        $deliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->saveDeliveryNote($deliveryNote);

        $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
        $displayData = $result->current();
        $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
        $averageCostingFlag = $displayData['averageCostingFlag'];

        $accountProduct = array();

        foreach ($products as $key => $p) {

            $batchProducts = $subProducts[$p['productID']];
            
            $productID = $p['productID'];
            $deliveryQty = $p['deliverQuantity']['qty'];
            $productType = $p['productType'];
            $locationProductOUT = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationOut);
            $averageCostingPrice = $locationProductOUT->locationProductAverageCostingPrice;
            $locationProductID = $locationProductOUT->locationProductID;

            $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

            if($this->useAccounting == 1){
                if(empty($pData['productInventoryAccountID'])){
                    $respond['status'] = false;
                    $respond['msg'] = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                    return $respond;
                }
                 //check whether Product Cost of Goods Sold Gl account id set or not
                if(empty($pData['productCOGSAccountID'])){
                    $respond['status'] = false;
                    $respond['msg'] = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName']));
                    return $respond;
                }
            }

            if ($productType != 2) {
                if ($locationProductOUT->locationProductQuantity < $deliveryQty) {
                    $respond['status'] = false;
                    $respond['msg'] = $this->getMessage('ERR_DELINOTEAPI_PRODUCT_UPDATE', [$pData['productCode']]);
                    return $respond;
                }
                $locationProductOUTData = array(
                    'locationProductQuantity' => $locationProductOUT->locationProductQuantity - $deliveryQty,
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductOUTData, $productID, $locationOut);
            }

            //calculate discount value
            $discountValue = 0;
            if ($p['productDiscountType'] == "precentage") {
                $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $customCurrencyRate;
            } else if ($p['productDiscountType'] == "value") {
                $discountValue = $p['productDiscount'] * $customCurrencyRate;
            }

            if ($p['productDiscountType'] == 'value') {
                $p['productDiscount'] = $p['productDiscount'] * $customCurrencyRate;
            }
            $p['copied'] = filter_var($p['copied'], FILTER_VALIDATE_BOOLEAN);
            $documentTypeID = 0;
            $deliveryNoteProductDocumentID = 0;
            $deliveryNoteProductDocumentCopiedQty = 0;
            if($p['copied'] && $fromSalesOrder == true){
                $documentTypeID = 3;
                $deliveryNoteProductDocumentID = $salesOrderID;
                $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderIDAndLocationProductId($salesOrderID, $locationProductID);
                $availableQty = $salesOrderProducts->quantity - $salesOrderProducts->salesOrdersProductCopiedQuantity;
                if($availableQty > $deliveryQty){
                    $deliveryNoteProductDocumentCopiedQty = $deliveryQty;
                }else{
                    $deliveryNoteProductDocumentCopiedQty = $availableQty;
                }
            }

            $deliveryNoteProductData = array(
                'deliveryNoteID' => $deliveryNoteID,
                'productID' => $productID,
                'deliveryNoteProductPrice' => $p['productPrice'] * $customCurrencyRate,
                'deliveryNoteProductDiscount' => $p['productDiscount'],
                'deliveryNoteProductDiscountType' => $p['productDiscountType'],
                'deliveryNoteProductTotal' => $p['productTotal'] * $customCurrencyRate,
                'deliveryNoteProductQuantity' => $deliveryQty,
                'documentTypeID' => $documentTypeID,
                'deliveryNoteProductDocumentID' => $deliveryNoteProductDocumentID,
                'deliveryNoteProductDocumentTypeCopiedQty' => $deliveryNoteProductDocumentCopiedQty
            );

            $deliveryNoteProduct = new DeliveryNoteProduct;
            $deliveryNoteProduct->exchangeArray($deliveryNoteProductData);
            $deliveryNoteProductID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->saveDeliveryNoteProduct($deliveryNoteProduct);
            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationOut);
            $locationPID = $locationProduct->locationProductID;
            if ($p['pTax']) {
                foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                    $deliveryNotePTaxesData = array(
                        'deliveryNoteProductID' => $deliveryNoteProductID,
                        'productID' => $productID,
                        'deliveryNoteTaxID' => $taxKey,
                        'deliveryNoteTaxPrecentage' => $productTax['tP'],
                        'deliveryNoteTaxAmount' => $productTax['tA'] * $customCurrencyRate
                    );
                    $deliveryNotePTaxM = new DeliveryNoteProductTax();
                    $deliveryNotePTaxM->exchangeArray($deliveryNotePTaxesData);
                    $this->CommonTable('Invoice\Model\DeliveryNoteProductTaxTable')->saveDeliveryNoteProductTax($deliveryNotePTaxM);
                }
            }

            //check and insert items to itemout table
            if ($productType != 2) {
                if (!count($batchProducts) > 0) {
                    $sellingQty = $deliveryQty;
                    while ($sellingQty != 0) {
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($locationProductOUT->locationProductID);
                        if (!$itemInDetails) {
                            // break;
                            $respond['status'] = false;
                            $respond['msg'] = $this->getMessage('ERR_NO_ANY_REMAIN_QTY', array($pData['productCode'].' - '.$pData['productName']));
                            return $respond;
                        }
                        if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                            $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Delivery Note',
                                'itemOutDocumentID' => $deliveryNoteID,
                                'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $sellingQty,
                                'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                            if($this->useAccounting == 1){
                                if($averageCostingFlag == 1){
                                //set gl accounts for the journal entry
                                    $productTotal = $sellingQty * $averageCostingPrice;
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }else{
                                    $productTotal = $sellingQty * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }
                            }
                            $sellingQty = 0;
                            break;
                        } else {
                            $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Delivery Note',
                                'itemOutDocumentID' => $deliveryNoteID,
                                'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                            if($this->useAccounting == 1){
                                if($averageCostingFlag == 1){
                                //set gl accounts for the journal entry
                                    $productTotal = $itemInDetails['itemInRemainingQty'] * $averageCostingPrice;
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }else{
                                    $productTotal = $itemInDetails['itemInRemainingQty'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }
                            }
                            $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                        }
                    }
                }
            }
                    //update SalesOrder Products when it coming from SalesOrder
            if ($p['copied'] && $fromSalesOrder == true) {
                $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderIDAndLocationProductId($salesOrderID, $locationPID);
                if (isset($salesOrderProducts)) {
                    $newCopiedQuantity = $salesOrderProducts->salesOrdersProductCopiedQuantity + (float) $deliveryQty;
                    $copiedflag = 0;
                    if ($newCopiedQuantity >= $salesOrderProducts->quantity) {
                        $newCopiedQuantity = $salesOrderProducts->quantity;
                        $copiedflag = 1;
                    }
                    $soProData = array(
                        'copied' => $copiedflag,
                        'salesOrdersProductCopiedQuantity' => $newCopiedQuantity,
                    );
                    $this->CommonTable('Invoice\Model\SalesOrderProductTable')->updateCopiedSalesOrderProducts($locationPID, $salesOrderID, $soProData);
                }
            }
            if (count($batchProducts) > 0) {
                        
                foreach ($batchProducts as $batchKey => $batchValue) {
                    if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                        $this->saveDeliverySubProductData($batchValue, $deliveryNoteProductID);
                        $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                        $productBatchQuentity = $batchProduct->productBatchQuantity - $batchValue['qtyByBase'];
                        if ($batchProduct->productBatchQuantity < $batchValue['qtyByBase']) {
                            $respond['status'] = false;
                            $respond['msg'] = $this->getMessage('ERR_DELINOTEAPI_PRODUCT_UPDATE', [$pData['productCode']]);
                            return $respond;
                        }
                        if ($batchValue['serialID']) {
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProductOUT->locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Delivery Note',
                                'itemOutDocumentID' => $deliveryNoteID,
                                'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                'itemOutBatchID' => $batchValue['batchID'],
                                'itemOutSerialID' => $batchValue['serialID'],
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $batchValue['qtyByBase'],
                                'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                            if($this->useAccounting == 1){
                                if($averageCostingFlag == 1){
                                //set gl accounts for the journal entry
                                    $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }else{
                                    $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }
                            }

                        } else {
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProductOUT->locationProductID, $batchValue['batchID']);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Delivery Note',
                                'itemOutDocumentID' => $deliveryNoteID,
                                'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                'itemOutBatchID' => $batchValue['batchID'],
                                'itemOutSerialID' => NULL,
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => $batchValue['qtyByBase'],
                                'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                            );
                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                            if($this->useAccounting == 1){
                                if($averageCostingFlag == 1){
                                //set gl accounts for the journal entry
                                    $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }else{
                                    $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }
                            }
                        }

                        $productBatchQty = array(
                            'productBatchQuantity' => $productBatchQuentity,
                        );
                        $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                        if ($batchValue['serialID']) {
                            $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                            if ($checkSerial->locationProductID != $locationProductOUT->locationProductID || $checkSerial->productSerialSold == 1) {
                                $respond['status'] = false;
                                $respond['msg'] = $this->getMessage('ERR_DELINOTEAPI_PRODUCT_UPDATE', [$pData['productCode']]);
                                return $respond;
                            }
                            $serialProductData = array(
                                'productSerialSold' => '1',
                            );
                            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                        }
                    } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                        $this->saveDeliverySubProductData($batchValue, $deliveryNoteProductID);
                        $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                        if ($checkSerial->locationProductID != $locationProductOUT->locationProductID || $checkSerial->productSerialSold == 1) {
                            $respond['status'] = false;
                            $respond['msg'] = $this->getMessage('ERR_DELINOTEAPI_PRODUCT_UPDATE', [$pData['productCode']]);
                            return $respond;
                        }

                        ///insert to the item out table
                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProductOUT->locationProductID, $batchValue['serialID']);
                        $itemOutM = new ItemOut();
                        $itemOutData = array(
                            'itemOutDocumentType' => 'Delivery Note',
                            'itemOutDocumentID' => $deliveryNoteID,
                            'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                            'itemOutBatchID' => NULL,
                            'itemOutSerialID' => $batchValue['serialID'],
                            'itemOutItemInID' => $itemInDetails['itemInID'],
                            'itemOutQty' => 1,
                            'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                            'itemOutDiscount' => $discountValue,
                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                            'itemOutDateAndTime' => $this->getGMTDateTime()
                        );

                        $itemOutM->exchangeArray($itemOutData);
                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);

                        if($this->useAccounting == 1){
                            if($averageCostingFlag == 1){
                                //set gl accounts for the journal entry
                                $productTotal = 1 * $averageCostingPrice;
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }else{
                                $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                    $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                    $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                }

                                if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                    $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                    $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                }
                            }
                        }

                        $serialProductData = array(
                            'productSerialSold' => '1',
                        );
                        $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                    }
                }
            } else {
                   // $this->saveTransferProductData(array('batchID' => 0, 'serialID' => 0, 'qtyByBase' => $p['transferQuantity']['qtyByBase']), $trasferID, $productID);
            }
        }
        
        //update SalesOrder status when all products are used to Delivery Note
        if ($fromSalesOrder == TRUE) {
            $this->_checkAndUpdateSalesOrderStatus($salesOrderID);
        }

        // call product updated event
        $productIDs = array_map(function($element) {
            return $element['productID'];
        }, $products);

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationOut]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        if ($this->useAccounting == 1) {
            //create data array for the journal entry save.
            $i=0;
            $journalEntryAccounts = array();

            foreach ($accountProduct as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Delivery Note '.$deliverCode.'.';
                $i++;
            }

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $deliveryData['deliveryNoteDeliveryDate'],
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Delivery Note '.$deliverCode.'.',
                'documentTypeID' => 4,
                'journalEntryDocumentID' => $deliveryNoteID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

            $resultData = $this->saveJournalEntry($journalEntryData);
            if($resultData['status']){
                if (!empty($dimensionData)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$deliverCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                    if(!$saveRes['status']){
                        return array(
                            'status'=> false,
                            'msg'=>$saveRes['msg'],
                            'data'=>$saveRes['data'],
                            );
                    }   
                }

                $respond['status'] = true;
                $respond['data'] = array('deliveryNoteCode' => $deliverCode, 'deliveryNoteID' => $deliveryNoteID);
                $respond['msg'] = $this->getMessage('SUCC_DELINOTEAPI_PRODUCT_ADD', array($deliverCode));
                $this->setLogMessage($customCurrencySymbol . $p['productTotal'] . ' Delivery Product details were successfully saved - ' . $deliverCode);
            }else{
                $respond['status'] = false;
                $respond['msg'] = $resultData['msg'];
                $respond['data'] = $resultData['data'];
            }

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($deliveryNoteID,4, 3);
            if(!$jEDocStatusUpdate['status']){
                return array(
                    'status'=> false,
                    'msg'=>$jEDocStatusUpdate['msg'],
                    );
            }
        }else{
            $respond['status'] = true;
            $respond['data'] = array('deliveryNoteCode' => $deliverCode, 'deliveryNoteID' => $deliveryNoteID);
            $respond['msg'] = $this->getMessage('SUCC_DELINOTEAPI_PRODUCT_ADD', array($deliverCode));

            $this->setLogMessage($customCurrencySymbol . $p['productTotal'] . ' Delivery Product details were successfully saved - ' . $deliverCode);
        }
        return $respond;
    }

    function saveDeliverySubProductData($batchValue, $deliveryNoteProductID)
    {
        
        $data = array(
            'deliveryNoteProductID' => $deliveryNoteProductID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'deliveryProductSubQuantity' => $batchValue['qtyByBase'],
            'deliveryNoteSubProductsWarranty' => $batchValue['warranty'],
            'deliveryNoteSubProductsWarrantyType' => $batchValue['warrantyType'],
        );
        $deliveryNoteSubProduct = new DeliveryNoteSubProduct;
        $deliveryNoteSubProduct->exchangeArray($data);

        $deliveryNoteSubProductID = $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->saveDeliveryNoteSubProduct($deliveryNoteSubProduct);
        return $deliveryNoteSubProductID;
    }

    public function getDeliveryNoteNoForLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $refNumber = NULL;
            $locationRefID = NULL;
            $result = $this->getReferenceNoForLocation('6', $locationID);
            $locationRefID = $result['locRefID'];
            $refNumber = $result['refNo'];
            if ($refNumber) {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $refNumber,
                    'locRefID' => $locationRefID,
                );
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_NOREFNUM');
                $this->data = FALSE;
                return $this->JSONRespond();
            }
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * updates SalesOrder status when all products are used to Delivery Note
     * @param Int $salesOrderID
     */
    private function _checkAndUpdateSalesOrderStatus($salesOrderID)
    {
        $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getUnCopiedSalesOrderProducts($salesOrderID);
        if (empty($salesOrderProducts)) {
            $statusID = $this->getStatusID('closed');
        }else{
            $statusID = $this->getStatusID('open');
        }
        $this->CommonTable('Invoice\Model\SalesOrderTable')->updateSalesOrderStatus($salesOrderID, $statusID);
    }

    public function getDeliveryNoteFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($searchrequest->isPost()) {
            $deliveryNoteID = $searchrequest->getPost('deliveryNoteID');
            $deliveryNotes = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteforSearch($deliveryNoteID);
            while ($t = $deliveryNotes->current()) {
                $deliveryNote[$t['deliveryNoteID']] = (object) $t;
            }
            if ($deliveryNotes->count() == 0) {
                return new JsonModel(array(FALSE));
            } else {
                $DeliveryNote = new ViewModel(
                        array('deliveryNote' => $deliveryNote,
                    'statuses' => $this->getStatusesList(),
                    'userdateFormat' => $dateFormat,
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,));
                $DeliveryNote->setTerminal(TRUE);
                $DeliveryNote->setTemplate('invoice/delivery-note/deliveryNoteList');
                return $DeliveryNote;
            }
        }
    }

    public function getDeliveryNoteByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($invrequest->isPost()) {
            $fromdate = $this->convertDateToStandardFormat($invrequest->getPost('fromdate'));
            $todate = $this->convertDateToStandardFormat($invrequest->getPost('todate'));
            $customerID = $invrequest->getPost('customerID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $filteredDeliveryNote = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteByDate($fromdate, $todate, $customerID, $locationID);

            while ($t = $filteredDeliveryNote->current()) {
                $deliveryNotes[$t['deliveryNoteID']] = (object) $t;
            }
            $DateFilteredDeliveryNote = new ViewModel(
                    array('deliveryNote' => $deliveryNotes,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,));
            $DateFilteredDeliveryNote->setTerminal(TRUE);
            $DateFilteredDeliveryNote->setTemplate('invoice/delivery-note/deliveryNoteList');
            return $DateFilteredDeliveryNote;
        }
    }

    public function retriveCustomerDeliveryNoteAction()
    {
        $er = array();
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $cust_id = $request->getPost('customerID');
            $deliveryNotes = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteByCustomerID($cust_id, $locationID);

            while ($t = $deliveryNotes->current()) {
                $deliveryNote[$t['deliveryNoteID']] = (object) $t;
            }
            $customerDeliveryNote = new ViewModel(
                    array('deliveryNote' => $deliveryNote,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,));
            $customerDeliveryNote->setTerminal(TRUE);
            $customerDeliveryNote->setTemplate('invoice/delivery-note/deliveryNoteList');
            return $customerDeliveryNote;
        }
    }

    //send delivery note email
    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Delivery Note';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_DELINOTEAPI_EMAIL_SENT');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_DELINOTEAPI_EMAIL_SENT');
        return $this->JSONRespond();
    }

    //get delivery note details for sales return
    public function getDeliveryNoteDetailsAction()
    {  
        $request = $this->getRequest();
        if ($request->isPost()) {
            $deliveryNoteID = $request->getPost('deliveryNoteID');
            $deliveryProductsArray = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductByDeliveryNoteID($deliveryNoteID, true);

            $dparray;
            $dparrayQty;
            $iparray;
            $iparrayQty;
            $realproduct;
            $checkProduct = 0;
            $productIdForTax = null;
            foreach ($deliveryProductsArray as $dp) {

                $dp = (object) $dp;
                $dparray[$dp->productID] = $dp->productID;
                $dparrayQty[$dp->productID] = $dp->deliveryNoteProductQuantity;
            }
            $data = $this->CommonTable('SalesReturnsProductTable')->getSalesReturnProductsByDelvieryNoteID($deliveryNoteID);

            foreach ($data as $ip) {
                $ip = (object) $ip;
                $iparray[$ip->productID] = $ip->productID;
                $iparrayQty[$ip->productID]+=$ip->salesReturnProductQuantity;
            }

            if (isset($dparrayQty)) {
                foreach ($dparrayQty as $key => $Pdata) {
                    if ($iparrayQty[$key] != $Pdata) {
                        $checkProduct = 1;
                        $realproduct[$key] = $key;
                    }
                }
            }

            if ($checkProduct) {

                $deliveryNote = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteByID($deliveryNoteID);
                $deliveryNote->deliveryNoteDeliveryDate = $this->convertDateToUserFormat($deliveryNote->deliveryNoteDeliveryDate);

                $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductByDeliveryNoteID($deliveryNoteID, true);
                $tax = array(
                    'deliveryNoteTaxID' => '',
                    'deliveryNoteTaxName' => '',
                    'deliveryNoteTaxPrecentage' => '',
                    'deliveryNoteTaxAmount' => '',
                );
                $subProduct = array(
                    'deliveryNoteSubProductID' => '',
                    'productBatchID' => '',
                    'productSerialID' => '',
                    'deliveryProductSubQuantity' => '',
                );

                $Products = array(
                    'deliveryNoteProductID' => '',
                    'deliveryNoteID' => '',
                    'productID' => '',
                    'deliveryNoteProductPrice' => '',
                    'deliveryNoteProductDiscount' => '',
                    'deliveryNoteProductDiscountType' => '',
                    'deliveryNoteProductQuantity' => '',
                    'productCode' => '',
                    'productName' => '',
                    'tax' => '',
                    'subProduct' => '',
                );

                while ($tt = $deliveryNoteProducts->current()) {
                    $t = (object) $tt;
                    if ($realproduct[$t->productID] == $t->productID) {
                        $returnProductData = $this->CommonTable('SalesReturnsProductTable')->getReturnProductAndSubProductDataByDeliveryNoteProductID($t->deliveryNoteProductID);
                        $salesReturnProductQty = 0;
                        $returnSubProductSerial = array();
                        $returnSubProductBatch = array();
                        $salesReturnProductIDArray = array();
                        foreach ($returnProductData as $prValue) {
                            $prValue = (object) $prValue;
                            if ($prValue->salesReturnProductID) {
//use this if statement for ignore repeating salesReturnProducts
                                if (!$salesReturnProductIDArray[$prValue->salesReturnProductID] == 1) {
                                    $salesReturnProductQty+=$prValue->salesReturnProductQuantity;
                                    $salesReturnProductIDArray[$prValue->salesReturnProductID] = 1;
                                }
                                if ($prValue->productSerialID) {
                                    $returnSubProductSerial[$prValue->productSerialID] += $prValue->salesReturnSubProductQuantity;
                                } else if ($prValue->productBatchID) {
                                    $returnSubProductBatch[$prValue->productBatchID] += $prValue->salesReturnSubProductQuantity;
                                }
                            }
                        }

                        $deliveryProduct[$t->productID] = (object) $Products;
                        $deliveryProduct[$t->productID]->deliveryNoteProductID = $t->deliveryNoteProductID;
                        $deliveryProduct[$t->productID]->deliveryNoteID = $t->deliveryNoteID;
                        $deliveryProduct[$t->productID]->productID = $t->productID;
                        $deliveryProduct[$t->productID]->deliveryNoteProductPrice = $t->deliveryNoteProductPrice;
                        $deliveryProduct[$t->productID]->deliveryNoteProductDiscount = $t->deliveryNoteProductDiscount;
                        $deliveryProduct[$t->productID]->deliveryNoteProductDiscountType = $t->deliveryNoteProductDiscountType;
                        $deliveryProduct[$t->productID]->deliveryNoteProductQuantity = $t->deliveryNoteProductQuantity -  $t->deliveryNoteProductCopiedQuantity;
                        $deliveryProduct[$t->productID]->productCode = $t->productCode;
                        $deliveryProduct[$t->productID]->productName = $t->productName;
                        $deliveryProduct[$t->productID]->productType = $t->productTypeID;

                        if ($t->deliveryNoteSubProductID != '') {
                            $returnQuantity = 0;
                            if ($returnSubProductSerial[$t->productSerialID]) {
                                $returnQuantity = $returnSubProductSerial[$t->productSerialID];
                            } else if ($returnSubProductBatch[$t->productBatchID]) {
                                $returnQuantity = $returnSubProductBatch[$t->productBatchID];
                            }
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID] = (object) $subProduct;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->deliveryNoteSubProductID = $t->deliveryNoteSubProductID;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->productBatchID = $t->productBatchID;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->productBatchCode = $t->productBatchCode;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->productSerialID = $t->productSerialID;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->productSerialCode = $t->productSerialCode;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->deliveryProductSubQuantity = $t->deliveryProductSubQuantity - $returnQuantity - $t->deliveryNoteSubProductsCopiedQuantity;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->serialLocProID = $t->serialLocProID;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->batchLocProID = $t->batchLocProID;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->batchExpireDate = $t->productBatchExpiryDate;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->batchWarranty = $t->productBatchWarrantyPeriod;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->serialWarranty = $t->productSerialWarrantyPeriod;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->serialWarrantyType = $t->productSerialWarrantyPeriodType;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->serialExpireDate = $t->productSerialExpireDate;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->batchManuFtDate = $t->productBatchManufactureDate;
                            $deliverySubProduct[$t->productID][$t->deliveryNoteSubProductID]->batchPrice = $t->productBatchPrice;
                        }
                        if ($deliverySubProduct) {
                            $deliveryProduct[$t->productID]->subProduct = $deliverySubProduct[$t->productID];
                        }

                        if ($t->deliveryNoteTaxID) {
                            //reset $taxes array for new deleivery note product ids
                            if ($productIdForTax != $t->productID) {
                                $taxes = [];
                            }
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID] = (object) $tax;
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxID = $t->deliveryNoteTaxID;
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxName = $t->taxName;
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxPrecentage = $t->deliveryNoteTaxPrecentage;
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxAmount = $t->deliveryNoteTaxAmount;
                            $deliveryProduct[$t->productID]->tax = $taxes[$t->deliveryNoteSubProductID];
                        }
                        $productIdForTax = $t->productID;
                    }
                    $locationID = $this->user_session->userActiveLocation['locationID'];
                    $locationProducts[$t->productID] = $this->getLocationProductDetailsForReturn($locationID, $t->productID);
                }
                $customerID = ($deliveryNote->customerID) ? $deliveryNote->customerID : 0;
                $customer = $this->getCustomerDetailsByCustomerID($customerID);

                if($customer->customerStatus == 2){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_RETURNAPI_INACTIVE_CUSTOMER');
                    return $this->JSONRespond();
                }


                $locationPro = Array();
                $inactiveItems = Array();
                foreach ($deliveryProduct as $value) {
                    $locationPro[$value->productID] = $this->getLocationProductDetails($locationID, $value->productID);
                    if ($locationPro[$value->productID] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($value->productID);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }
                }

                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);
                    $errorMsg = $this->getMessage('ERR_DLN_ITM_INACTIVE_COPY', [$errorItems]);
                }

                $this->status = true;
                $this->data = array(
                    'status' => true,
                    'deliveryNote' => $deliveryNote,
                    'deliveryNoteProduct' => $deliveryProduct,
                    'customer' => $customer,
                    'errorMsg' => $errorMsg,
                    'inactiveItemFlag' => $inactiveItemFlag,
                    'locationProducts' => $locationProducts
                );
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_RETURNAPI_USED_DELINOTE');
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchDeliveryNoteForDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $deliveryNoteSearchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->getActiveAllLocationsIds();
            $deliveryNotes = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->searchDeliveryNotesForDropDown($locationID, $deliveryNoteSearchKey);

            $deliveryNoteList = array();
            foreach ($deliveryNotes as $deliveryNote) {
                $temp['value'] = $deliveryNote['deliveryNoteID'];
                $temp['text'] = $deliveryNote['deliveryNoteCode'];
                $deliveryNoteList[] = $temp;
            }

            $this->setLogMessage("Retrive delivery note list for dropdown.");
            $this->data = array('list' => $deliveryNoteList);
            return $this->JSONRespond();
        }
    }

    /**
     * get active state Delivery notes for dropown
     * @author sharmilan <sharmilan@thincune.com>
     * @return type
     */
    public function searchActiveDeliveryNoteForDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $deliveryNoteSearchKey = $searchrequest->getPost('searchKey');
            $customerID = $_GET['customerID'];
            $currencyID = $_GET['customCurrencyId'];
            $deliveryNotes = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->searchDeliveryNotesForDropDown('', $deliveryNoteSearchKey, ['3','15'], $customerID, $currencyID);

            $deliveryNoteList = array();
            foreach ($deliveryNotes as $deliveryNote) {
                $temp['value'] = $deliveryNote['deliveryNoteID'];
                $temp['text'] = $deliveryNote['deliveryNoteCode'];
                $deliveryNoteList[] = $temp;
            }

            $this->data = array('list' => $deliveryNoteList);
            return $this->JSONRespond();
        }
    }

    /**
     * get  Delivery notes for document dropown
     * @author Ashan Madushka <ashan@thinkcube.com>
     * @return type
     */
    public function searchDeliveryNoteForDocumentDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $deliveryNoteSearchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost("locationID");
            $deliveryNotes = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->searchDeliveryNotesForDropDown($locationID, $deliveryNoteSearchKey, false);

            $deliveryNoteList = array();
            foreach ($deliveryNotes as $deliveryNote) {
                $temp['value'] = $deliveryNote['deliveryNoteID'];
                $temp['text'] = $deliveryNote['deliveryNoteCode'];
                $deliveryNoteList[] = $temp;
            }

            $this->data = array('list' => $deliveryNoteList);
            return $this->JSONRespond();
        }
    }

    //get delivery note edit details for edit page
    public function getDeliveryNoteEditDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $deliveryNoteID = $request->getPost('deliveryNoteID');
            $dparray;
            $dparrayQty;
            $iparray;
            $iparrayQty;
            $checkProduct = 0;
            $dproductType;
            $locationID = $this->user_session->userActiveLocation['locationID'];


            $deliveryNote = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteByID($deliveryNoteID);
            $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductByDeliveryNoteID($deliveryNoteID, false);
            $tax = array(
                'deliveryNoteTaxID' => '',
                'deliveryNoteTaxName' => '',
                'deliveryNoteTaxPrecentage' => '',
                'deliveryNoteTaxAmount' => '',
                );
            $subProduct = array(
                'deliveryNoteSubProductID' => '',
                'productBatchID' => '',
                'productSerialID' => '',
                'deliveryNoteSubProductQuantity' => '',
                'deliveryNoteSubProductsWarranty'=>'',
                );

            $Products = array(
                'deliveryNoteProductID' => '',
                'deliveryNoteID' => '',
                'productID' => '',
                'deliveryNoteProductPrice' => '',
                'deliveryNoteProductDiscount' => '',
                'deliveryNoteProductDiscountType' => '',
                'deliveryNoteProductQuantity' => '',
                'productCode' => '',
                'productName' => '',
                'productType' => '',
                'tax' => '',
                'subProduct' => '',
                'documentTypeID' => '',
                'deliveryNoteProductDocumentID' => '',
                );

            $incID = 1;
            $incFlag = FALSE;
            foreach ($deliveryNoteProducts as $key => $tt) {
                $t = (object) $tt;
                $deliveryNoteProduct[$t->productID . '_' . $incID] = (object) $Products;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductID = $t->deliveryNoteProductID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteID = $t->deliveryNoteID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productID = $t->productID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductPrice = $t->deliveryNoteProductPrice;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductDiscount = $t->deliveryNoteProductDiscount;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductDiscountType = $t->deliveryNoteProductDiscountType;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductQuantity = $t->deliveryNoteProductQuantity;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productCode = $t->productCode;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productName = $t->productName;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productType = $t->productTypeID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->productDescription = $t->productDescription;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->documentTypeID = $t->documentTypeID;
                $deliveryNoteProduct[$t->productID . '_' . $incID]->deliveryNoteProductDocumentID = $t->deliveryNoteProductDocumentID;

                $subIncFlag = $incID - 1;
                if ($t->deliveryNoteSubProductID != '') {

                    $subIncFlag = $incID - 1;
                    if (!empty($deliveryNoteProduct[$t->productID . '_' . $subIncFlag]->subProduct) && $deliveryNoteProduct[$t->productID . '_' . $subIncFlag]->deliveryNoteProductID == $t->deliveryNoteProductID) {
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID] = (object) $subProduct;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->deliveryNoteSubProductID = $t->deliveryNoteSubProductID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->productBatchID = $t->productBatchID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->productSerialID = $t->productSerialID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->deliveryNoteSubProductQuantity = $t->deliveryProductSubQuantity;
                        $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag][$t->deliveryNoteSubProductID]->deliveryNoteSubProductsWarranty = $t->deliveryNoteSubProductsWarranty;
                        $deliveryNoteProduct[$t->productID . '_' . $subIncFlag]->subProduct = $deliveryNoteSubProduct[$t->productID . '_' . $subIncFlag];
                        unset($deliveryNoteProduct[$t->productID . '_' . $incID]);
                        --$incID;
                        $incFlag = TRUE;
                    } else {
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID] = (object) $subProduct;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->deliveryNoteSubProductID = $t->deliveryNoteSubProductID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->productBatchID = $t->productBatchID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->productSerialID = $t->productSerialID;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->deliveryNoteSubProductQuantity = $t->deliveryProductSubQuantity;
                        $deliveryNoteSubProduct[$t->productID . '_' . $incID][$t->deliveryNoteSubProductID]->deliveryNoteSubProductsWarranty = $t->deliveryNoteSubProductsWarranty;
                        $deliveryNoteProduct[$t->productID . '_' . $incID]->subProduct = $deliveryNoteSubProduct[$t->productID . '_' . $incID];
                    }
                }

                if ($t->deliveryNoteTaxID) {
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID] = (object) $tax;
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxID = $t->deliveryNoteTaxID;
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxName = $t->taxName;
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxPrecentage = $t->deliveryNoteTaxPrecentage;
                    $taxes[$t->deliveryNoteProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxAmount = $t->deliveryNoteTaxAmount;

                    $subTaxIncFlag = $incID - 1;

                    if (!empty($deliveryNoteProduct[$t->productID . '_' . $subTaxIncFlag]->tax) && $deliveryNoteProduct[$t->productID . '_' . $subTaxIncFlag]->deliveryNoteProductID == $t->deliveryNoteProductID) {
                        $deliveryNoteProduct[$t->productID . '_' . $subTaxIncFlag]->tax = $taxes[$t->deliveryNoteProductID];
                        unset($deliveryNoteProduct[$t->productID . '_' . $incID]);
                                //If there has sub products so dont need to reduce $incID
                                //because if it has a sub product then it already has reduce 1 that previous condition
                        if (!$incFlag) {
                            --$incID;
                        }
                    } else {

                        $deliveryNoteProduct[$t->productID . '_' . $incID]->tax = $taxes[$t->deliveryNoteProductID];
                    }
                }
                $locationID = $this->user_session->userActiveLocation['locationID'];

                $products[$t->productID] = $this->getLocationProductDetailsForReturn($locationID, $t->productID);
                $incID++;
            }

            $customerID = ($deliveryNote->customerID) ? $deliveryNote->customerID : 0;
            $customer = $this->getCustomerDetailsByCustomerID($customerID);
            $currencySymbol = ($deliveryNote->customCurrencyId) ? $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($deliveryNote->customCurrencyId)->currencySymbol : $this->companyCurrencySymbol;

            $locationProducts = Array();
            $inactiveItems = Array();
            foreach ($deliveryNoteProduct as $deleProducts) {
                $locationProducts[$deleProducts->productID] = $this->getLocationProductDetails($locationID, $deleProducts->productID);
                if ($locationProducts[$deleProducts->productID] == null) {
                    $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($deleProducts->productID);
                    $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                }
            }

            $inactiveItemFlag = false;
            $errorMsg = null;
            if (!empty($inactiveItems)) {
                $inactiveItemFlag = true;
                $errorItems = implode(",",$inactiveItems);
                $errorMsg = $this->getMessage('ERR_DLN_ITM_INACTIVE', [$errorItems]);
            }

            $dimensionData = [];
            $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(4,$deliveryNoteID);
            foreach ($jEDimensionData as $value) {
                if (!is_null($value['journalEntryID'])) {
                    $temp = [];
                    if($value['dimensionType'] == "job" || $value['dimensionType'] == "project") {
                        if ($value['dimensionType'] == "job") {
                            $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($value['dimensionValueID']);
                            $valueTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                            $dimensionTxt = "Job";
                        } else if ($value['dimensionType'] == "project") {
                            $proData = $this->CommonTable('Jobs\Model\ProjectTable')->getProjectByProjectId($value['dimensionValueID']);
                            $valueTxt = $proData->projectCode.'-'.$proData->projectName;
                            $dimensionTxt = "Project";
                        }

                        $temp['dimensionTypeId'] = $value['dimensionType'];
                        $temp['dimensionTypeTxt'] = $dimensionTxt;
                        $temp['dimensionValueId'] = $value['dimensionValueID'];
                        $temp['dimensionValueTxt'] = $valueTxt;
                    } else {
                        $temp['dimensionTypeId'] = $value['dimensionType'];
                        $temp['dimensionTypeTxt'] = $value['dimensionName'];
                        $temp['dimensionValueId'] = $value['dimensionValueID'];
                        $temp['dimensionValueTxt'] = $value['dimensionValue'];
                    }
                    $dimensionData[$deliveryNote->deliveryNoteCode][$value['dimensionType']] = $temp;
                }
            }

            $uploadedAttachments = [];
            $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($deliveryNoteID, 4);
            $companyName = $this->getSubdomain();
            $path = '/userfiles/' . md5($companyName) . "/attachments";

            foreach ($attachmentData as $value) {
                $temp = [];
                $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
                $temp['documentRealName'] = $value['documentRealName'];
                $temp['docLink'] = $path . "/" .$value['documentName'];
                $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
            }

            $this->status = true;
            $this->data = array(
                'status' => true,
                'deliveryNote' => $deliveryNote,
                'deliveryNoteProduct' => $deliveryNoteProduct,
                'products' => $products,
                'customer' => $customer,
                'currencySymbol' => $currencySymbol,
                'errorMsg' => $errorMsg,
                'inactiveItemFlag' => $inactiveItemFlag,
                'uploadedAttachments' => $uploadedAttachments,
                'dimensionData' => $dimensionData
                );

            return $this->JSONRespond();
        }
    }

    public function updateDeliveryNoteDetailsAction(){
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = 'Request is not a post resquest.!';
            return $this->JSONRespond();
        }

        $post = $request->getPost();
        //check this creditNoteId is active or not for update
        $deliveryNoteStatus = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteStatusByDeliveryNoteID($post['deliveryNoteID'])->current();
        //user can update, 'open status' delivery notes only.(open status id = 3)
        if($deliveryNoteStatus['deliveryNoteStatus'] != '3' && $deliveryNoteStatus['deliveryNoteStatus'] != '15'){
            $this->status = false;
            $this->data = 'UPDATE_FAIL';
            $this->msg = $this->getMessage('ERR_CREDITNOTE_UPDATE');
            $this->flashMessenger()->addMessage($this->msg);
            return $this->JSONRespond();
        }

        // check whether the delivery note used to create invoice or sales return
        $salesReturn = $this->CommonTable("SalesReturnsTable")->getSalesReturnByDeliveryNoteID($post['deliveryNoteID']);
        $invoiceDeliveryNote = $this->CommonTable("Invoice\Model\DeliveryNoteTable")->getInvoicesRelatedToDeliveryNoteByDeliveryNoteID($post['deliveryNoteID']);

        if ($salesReturn->current() != NULL) {
            $this->status = false;
            $this->data = 'UPDATE_FAIL';
            $this->msg = 'This Delivery Note has a return.So you can not edit this Delivery Note..!';
            return $this->JSONRespond();
        } else if($invoiceDeliveryNote->current() != NULL){
            $this->status = false;
            $this->data = 'UPDATE_FAIL';
            $this->msg = 'This Delivery Note used to create invoice.So you can not edit this Delivery Note..!';
            return $this->JSONRespond();
        }

        //currency rate check
        $customCurrencyId = $post['customCurrencyId'];
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "") ? $customCurrencyData->currencyRate : 1;

        //typical data set
        $deliveryNoteID = $post['deliveryNoteID'];
        $deliveryNoteCode = $post['deliverCode'];
        $locationID = $post['locationOutID'];
        $deliveryNoteProducts = $post['products'];
        $deliveryNoteSubProducts = $post['subProducts'];
        $deliveryNoteDate = $this->convertDateToStandardFormat($post['date']);
        $customerID = $post['customer'];
        $salesOrderID = ($post['salesOrderID'])? $post['salesOrderID']: null;
        $customerProfileID = $post['customer_prof_ID'];
        $deliveryCharge = $post['deliveryCharge'];
        $deliveryTotalPrice = $post['deliveryTotalPrice'];
        $deliveryAddress = $post['deliveryAddress'];
        $deliveryComment = $post['deliveryComment'];
        $salesPersonID = (!empty($post['salesPersonID'])) ? $post['salesPersonID'] : null;
        $customCurrencyRate = $post['customCurrencyRate'];
        $priceListId = (!empty($post['priceListId'])) ? $post['priceListId'] : null;
        $previousProducts = $post['insertedProducts'];
        $previousSubProducts = $post['insertedSubProducts'];
        $previousDeliveryNoteTotal = $post['previousDeliveryNoteTotal'];
        $previousDeliveryNoteTopVals = $post['insertedDeliveryNoteTopVals'];
        $ignoreBudgetLimit = $post['ignoreBudgetLimit'];

        $deliveryNoteResult = $this->CommonTable("Invoice\Model\DeliveryNoteTable")->getDeliveryNoteDetailsByID($deliveryNoteID)->current();

        if (!empty($salesPersonID) && $deliveryNoteResult['salesPersonID'] != $salesPersonID) {
            $salesPersonData = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByID($salesPersonID)->current();
            if ($salesPersonData['salesPersonStatus'] != 1) {
                $this->status = false;
                $this->msg =  $this->getMessage('ERR_SELECTED_SALES_PERSON');
                return $this->JSONRespond();   
            }

        }
        
        $entityID = $deliveryNoteResult['entityID'];
        if($deliveryNoteResult['deliveryNoteStatus'] == 4){
            $this->status = false;
            $this->msg =  $this->getMessage('ERR_DELINOTEAPI_STATUS_CLOSED',array($deliveryNoteCode));
            return $this->JSONRespond();
        }

        $deliveryNoteData = array(
            'deliveryNoteCode' => $deliveryNoteCode,
            'customerID' => $customerID,
            'deliveryNoteLocationID' => $locationID,
            'deliveryNoteDeliveryDate' => $deliveryNoteDate,
            'deliveryNoteCharge' => (float) $deliveryCharge * $customCurrencyRate,
            'deliveryNotePriceTotal' => (float) $deliveryTotalPrice * $customCurrencyRate,
            'deliveryNoteAddress' => $deliveryAddress,
            'deliveryNoteComment' => $deliveryComment,
            'salesOrderID' => $salesOrderID,
            'salesPersonID' => $salesPersonID,
            'customCurrencyId' => $customCurrencyId,
            'deliveryNoteCustomCurrencyRate' => $customCurrencyRate,
            'priceListId' => $priceListId,
            'customerProfileID' => $customerProfileID,
            'deliveryNoteStatus' => 3,
            'entityID' => $entityID
        );

        $itemAttrDetailsSet = [];
        foreach ($deliveryNoteProducts as $key => $value) {
            $productID = $value['productID'];
            $itemAttributeMap = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->getDetailsByProductID($productID);
            foreach ($itemAttributeMap as $nkey => $nvalue) {
                if($nvalue['itemAttributeID'] == 1){
                    $itemAttrDetailsSet[$productID][] = $nvalue['itemAttributeValueID'];
                }
            }

            unset($value['stockUpdate']);
            unset($value['selected_serials']);
            $products[$key] = $value;
        }
        //check the same credit period is exist for all items
        if(count($itemAttrDetailsSet) > 1){
            foreach ($itemAttrDetailsSet as $ckey => $cvalue) {
                if(isset($oldVal)){
                    $newVal = $cvalue;
                    $oldVal = array_intersect($oldVal, $newVal);
                    if(!(count($oldVal) > 0)){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DELINOTEAPI_ITEMATTRIBUTE_CREDIT_PERIOD');
                        return $this->JSONRespond();
                        break;
                    }
                }else{
                    $oldVal = $cvalue;
                }
            }
        }

        //will mark as true if products has been changed
        $itemListChanged = FALSE;

        //then compare products list with old product list
        if (!empty($products) && !empty($previousProducts)) {
            if (count($products) == count($previousProducts)) {
                foreach ($previousProducts as $iID => $i) {
                    if (array_key_exists($iID, $products)) {
                        foreach ($products as $pID => $p) {
                            $productID = $p['productID'];
                            if ($iID == $pID) {
                                $diffProduct = array_diff_assoc($products[$pID], $previousProducts[$iID]);

                                //check the quantity change
                                if (floatval($p['deliverQuantity']['qty']) != floatval($i['deliverQuantity']['qty'])) {
                                    $itemListChanged = TRUE;
                                    break;
                                }
                                //if there have any different with products
                                if (!empty($diffProduct)) {
                                    $itemListChanged = TRUE;
                                    break;
                                }

                                //compaire subproducts list with current and already inserted
                                if (!empty($deliveryNoteSubProducts) && !empty($previousSubProducts)) {
                                    if (count($deliveryNoteSubProducts) == count($previousSubProducts)) {
                                        foreach ($previousSubProducts as $iSID => $iS) {
                                            foreach ($deliveryNoteSubProducts as $sID => $s) {
                                                if (array_key_exists($iSID, $deliveryNoteSubProducts)) {
                                                    foreach ($previousSubProducts[$iSID] as $iKey => $iv) {
                                                        foreach ($deliveryNoteSubProducts[$iSID] as $sKey => $sv) {
                                                            if ($iKey == $sKey) {
                                                                $diffSubProduct = array_diff_assoc($iv, $sv);
                                                                //if there is any different
                                                                if (!empty($diffSubProduct)) {
                                                                    $itemListChanged = TRUE;
                                                                    break;
                                                                }
                                                            }
                                                        }
                                                    }
                                                } else {
                                                    $itemListChanged = TRUE;
                                                }
                                            }
                                        }
                                    } else {
                                        $itemListChanged = TRUE;
                                    }
                                }
                            }
                        }
                    } else {
                        $itemListChanged = TRUE;
                    }
                }
            } else {
                $itemListChanged = TRUE;
            }
        }

        $this->beginTransaction();
        $getDeliveryNoteChanges = $this->_getDeliveryNoteChanges($products, $previousProducts, $deliveryNoteSubProducts, $previousSubProducts, $deliveryNoteData, $previousDeliveryNoteTopVals);
        //if deliveryNote have changes
        if (!empty($getDeliveryNoteChanges)) {
            //save data into deliveryNoteEditLog table
            $deliveryNoteEditLogData = [
                'userID' => $this->userID,
                'deliveryNoteID' => $deliveryNoteID,
                'dateAndTime' => $this->getGMTDateTime()
            ];
            $deliveryNoteEditLogModel = new DeliveryNoteEditLog();
            $deliveryNoteEditLogModel->exchangeArray($deliveryNoteEditLogData);
            $deliveryNoteEditLogLastInsertID = $this->CommonTable("Invoice\Model\DeliveryNoteEditLogTable")->saveDeliveryNoteEditLog($deliveryNoteEditLogModel);
            //save deliveryNote title changes
            if ($getDeliveryNoteChanges['titleChanges']) {
                foreach ($getDeliveryNoteChanges['titleChanges'] as $key => $value) {
                    $from = ($value['from'] == NULL) ? '-' : $value['from'];
                    $to = ($value['to'] == NULL) ? '-' : $value['to'];

                    if ($key == "salesPerson") {
                        $getFromSalesPerosn = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByID($from);
                        $fromSalesPerson = $getFromSalesPerosn->current()['salesPersonSortName'];
                        $from = ($fromSalesPerson != NULL) ? $fromSalesPerson : '-';

                        $getToSalesPerosn = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByID($to);
                        $toSalesPerson = $getToSalesPerosn->current()['salesPersonSortName'];
                        $to = ($toSalesPerson != NULL) ? $toSalesPerson : '-';
                    }

                    $newState = $key . " Changes from " . $from . " to " . $to;
                    $newState = $newState = $newItemsData = [
                        'deliveryNoteEditLogDetailsOldState' => NULL,
                        'deliveryNoteEditLogDetailsNewState' => $newState,
                        'deliveryNoteEditLogDetailsCategory' => $key,
                        'deliveryNoteEditLogID' => $deliveryNoteEditLogLastInsertID
                    ];
                    $deliveryNoteEditLogDetailsModel = new DeliveryNoteEditLogDetails();
                    $deliveryNoteEditLogDetailsModel->exchangeArray($newItemsData);
                    $this->CommonTable("Invoice\Model\DeliveryNoteEditLogDetailsTable")->saveDeliveryNoteEditLogDetails($deliveryNoteEditLogDetailsModel);
                }
            }

            //save the deliveryNote edit details
            foreach ($getDeliveryNoteChanges as $productIncID => $data) {
                foreach ($data as $key => $value) {
                    $productID = explode("_", $productIncID)[0];
                    $getProduct = $this->CommonTable("Inventory\Model\ProductTable")->getProduct($productID);
                    $productName = $getProduct->productName;
                    $productCode = $getProduct->productCode;

                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productID);
                    $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUom);
                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                    $uomAbbr = $thisqty['uomAbbr'];

                    if ($key == 'newItems') {
                        $newState = "Added Item Called " . $value['productName'] . "-" . $value['productCode'] . " and quantity " . number_format($value['qty'], 2);
                        $newItemsData = [
                            'deliveryNoteEditLogDetailsOldState' => NULL,
                            'deliveryNoteEditLogDetailsNewState' => $newState,
                            'deliveryNoteEditLogDetailsCategory' => $key,
                            'deliveryNoteEditLogID' => $deliveryNoteEditLogLastInsertID
                        ];

                        $deliveryNoteEditLogDetailsModel = new DeliveryNoteEditLogDetails();
                        $deliveryNoteEditLogDetailsModel->exchangeArray($newItemsData);
                        $this->CommonTable("Invoice\Model\DeliveryNoteEditLogDetailsTable")->saveDeliveryNoteEditLogDetails($deliveryNoteEditLogDetailsModel);
                    }

                    if ($key == "removeItems") {
                        $newState = "Removed Item Called " . $value['productName'] . "-" . $value['productCode'] . " and quantity " . number_format($value['qty'], 2);
                        $newItemsData = [
                            'deliveryNoteEditLogDetailsOldState' => NULL,
                            'deliveryNoteEditLogDetailsNewState' => $newState,
                            'deliveryNoteEditLogDetailsCategory' => $key,
                            'deliveryNoteEditLogID' => $deliveryNoteEditLogLastInsertID
                        ];

                        $deliveryNoteEditLogDetailsModel = new DeliveryNoteEditLogDetails();
                        $deliveryNoteEditLogDetailsModel->exchangeArray($newItemsData);
                        $this->CommonTable("Invoice\Model\DeliveryNoteEditLogDetailsTable")->saveDeliveryNoteEditLogDetails($deliveryNoteEditLogDetailsModel);
                    }

                    if ($key == "itemChanges") {
                        foreach ($value as $k => $data) {
                            $from = ( $data['from']['change'] == NULL) ? '-' : $data['from']['change'];
                            $to = ($data['to']['change'] == NULL) ? '-' : $data['to']['change'];
                            $newState = $data['productName'] . "-" . $data['productCode'] . ' ' . $data['from']['source'] . " changed from " . $from . " to " . $to;

                            $newItemsData = [
                                'deliveryNoteEditLogDetailsOldState' => NULL,
                                'deliveryNoteEditLogDetailsNewState' => $newState,
                                'deliveryNoteEditLogDetailsCategory' => $key,
                                'deliveryNoteEditLogID' => $deliveryNoteEditLogLastInsertID
                            ];

                            $deliveryNoteEditLogDetailsModel = new DeliveryNoteEditLogDetails();
                            $deliveryNoteEditLogDetailsModel->exchangeArray($newItemsData);
                            $this->CommonTable("Invoice\Model\DeliveryNoteEditLogDetailsTable")->saveDeliveryNoteEditLogDetails($deliveryNoteEditLogDetailsModel);
                        }
                    }

                    if ($key == "removedTax") {
                        if (!empty($value)) {
                            foreach ($value as $k => $v) {
                                $newState = $productName . " - " . $productCode . " tax removed - tax name - " . $v['tN'] . " tax percentage - " . $v['tP'] . "% tax Amount - " . $v['tA'];
                                $newItemsData = [
                                    'deliveryNoteEditLogDetailsOldState' => NULL,
                                    'deliveryNoteEditLogDetailsNewState' => $newState,
                                    'deliveryNoteEditLogDetailsCategory' => $key,
                                    'deliveryNoteEditLogID' => $deliveryNoteEditLogLastInsertID
                                ];
                                $deliveryNoteEditLogDetailsModel = new DeliveryNoteEditLogDetails();
                                $deliveryNoteEditLogDetailsModel->exchangeArray($newItemsData);
                                $this->CommonTable("Invoice\Model\DeliveryNoteEditLogDetailsTable")->saveDeliveryNoteEditLogDetails($deliveryNoteEditLogDetailsModel);
                            }
                        }
                    }

                    if ($key == "addedTax") {
                        if (!empty($value)) {
                            foreach ($value as $k => $v) {
                                $newState = $productName . " - " . $productCode . " added tax - tax name - " . $v['tN'] . " tax percentage - " . $v['tP'] . "% tax Amount - " . $v['tA'];
                                $newItemsData = [
                                    'deliveryNoteEditLogDetailsOldState' => NULL,
                                    'deliveryNoteEditLogDetailsNewState' => $newState,
                                    'deliveryNoteEditLogDetailsCategory' => $key,
                                    'deliveryNoteEditLogID' => $deliveryNoteEditLogLastInsertID
                                ];
                                $deliveryNoteEditLogDetailsModel = new DeliveryNoteEditLogDetails();
                                $deliveryNoteEditLogDetailsModel->exchangeArray($newItemsData);
                                $this->CommonTable("Invoice\Model\DeliveryNoteEditLogDetailsTable")->saveDeliveryNoteEditLogDetails($deliveryNoteEditLogDetailsModel);
                            }
                        }
                    }

                    if ($key == "subProducts") {
                        foreach ($value as $s => $subData) {
                            if ($s == 'added') {
                                foreach ($subData as $v) {
                                    $qty = 0;
                                    $serialCode = NULL;
                                    $batchCode = NULL;
                                    if ($v['serialID'] != NULL && $v['batchID'] == NULL) {
                                        $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($v['serialID']);
                                        $serialCode = $getBatchSerialData->productSerialCode;
                                        $qty = 1;
                                    } else if ($v['serialID'] == NULL && $v['batchID'] != NULL) {
                                        $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($v['batchID']);
                                        $batchCode = $getBatchSerialData->productBatchCode;
                                        $qty = $v['qtyByBase'];
                                    } else if ($v['serialID'] != NULL && $v['batchID'] != NULL) {
                                        $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getBatchSerialProduct($v['serialID'], $v['batchID']);
                                        $batchCode = $getBatchSerialData->productBatchCode;
                                        $serialCode = $getBatchSerialData->productSerialCode;
                                        $qty = 1;
                                    }
                                    $serialCode = ($serialCode == NULL) ? '-' : $serialCode;
                                    $batchCode = ($batchCode == NULL) ? '-' : $batchCode;
                                    $newState = "Added new subproduct - Serial Code - " . $serialCode . " Batch Code - " . $batchCode . " Quantity - " . $qty . $uomAbbr;
                                    $newItemsData = [
                                        'deliveryNoteEditLogDetailsOldState' => NULL,
                                        'deliveryNoteEditLogDetailsNewState' => $newState,
                                        'deliveryNoteEditLogDetailsCategory' => $key,
                                        'deliveryNoteEditLogID' => $deliveryNoteEditLogLastInsertID
                                    ];
                                    $deliveryNoteEditLogDetailsModel = new DeliveryNoteEditLogDetails();
                                    $deliveryNoteEditLogDetailsModel->exchangeArray($newItemsData);
                                    $this->CommonTable("Invoice\Model\DeliveryNoteEditLogDetailsTable")->saveDeliveryNoteEditLogDetails($deliveryNoteEditLogDetailsModel);
                                }
                            }
                            if ($s == 'removed') {
                                foreach ($subData as $v) {
                                    $qty = 0;
                                    $serialCode = NULL;
                                    $batchCode = NULL;

                                    if (!isset($v['serialID'])) {
                                        $v['serialID'] = "";
                                    }
                                    if (!isset($v['batchID'])) {
                                        $v['batchID'] = "";
                                    }

                                    if (isset($v['serialID']) && $v['serialID'] != "" && ($v['batchID'] == "" | $v['batchID'] == NULL)) {
                                        $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($v['serialID']);
                                        $serialCode = $getBatchSerialData->productSerialCode;
                                        $qty = 1;
                                    } else if (isset($v['batchID']) && $v['batchID'] != "" && ($v['serialID'] == "" | $v['serialID'] == NULL)) {
                                        $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($v['batchID']);
                                        $batchCode = $getBatchSerialData->productBatchCode;
                                        $qty = $v['qtyByBase'];
                                    } else if (isset($v['serialID']) && isset($v['batchID']) && $v['serialID'] != "" && $v['batchID'] != "") {
                                        $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getBatchSerialProduct($v['serialID'], $v['batchID']);
                                        $serialCode = $getBatchSerialData->productSerialCode;
                                        $batchID = $getBatchSerialData->productBatchID;
                                        $getBatchData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchID);
                                        $batchCode = $getBatchData->productBatchCode;
                                        $qty = 1;
                                    }
                                    $serialCode = ($serialCode == NULL) ? '-' : $serialCode;
                                    $batchCode = ($batchCode == NULL) ? '-' : $batchCode;

                                    $newState = "Removed new subproduct - Serial Code - " . $serialCode . " Batch Code - " . $batchCode . " Quantity - " . $qty . $uomAbbr;
                                    $newItemsData = [
                                        'deliveryNoteEditLogDetailsOldState' => NULL,
                                        'deliveryNoteEditLogDetailsNewState' => $newState,
                                        'deliveryNoteEditLogDetailsCategory' => $key,
                                        'deliveryNoteEditLogID' => $deliveryNoteEditLogLastInsertID
                                    ];
                                    $deliveryNoteEditLogDetailsModel = new DeliveryNoteEditLogDetails();
                                    $deliveryNoteEditLogDetailsModel->exchangeArray($newItemsData);
                                    $this->CommonTable("Invoice\Model\DeliveryNoteEditLogDetailsTable")->saveDeliveryNoteEditLogDetails($deliveryNoteEditLogDetailsModel);
                                }
                            }
                        }
                    }
                }
            }
        }

        //if edit delivery note has item changes
        if ($itemListChanged) {
        //updates entity table

            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            if($FIFOCostingFlag == 0 && $averageCostingFlag == 0){
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE');
                return $this->JSONRespond();
            }

            $entityData = array(
                'deleted' => 1,
                'deletedBy' => $this->user_session->userID,
                'deletedTimeStamp' => $this->getUserDateTime()
            );
            $this->CommonTable("Core\Model\EntityTable")->updateEntity($entityData, $entityID);

            //update deliveryNote table status as replace(10)
            $this->CommonTable("Invoice\Model\DeliveryNoteTable")->updateDeliveryNoteStatus($deliveryNoteID, 10);

            //get products details by related delivery Note ID
            $getDeliveryNoteProductData = $this->CommonTable("Invoice\Model\DeliveryNoteProductTable")->getProductsForDeliveryNoteEditByDeliveryNoteID($deliveryNoteID, $locationID);
            $deliveryNoteProductData = [];

            $accountProduct = array();
                ////
            foreach ($getDeliveryNoteProductData as $key => $g) {
                $locationProductData = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProduct($g['productID'], $locationID);
                $deliveryNoteProductQuantity = 0;
                if (is_array($deliveryNoteProductData['products'][$g['productID']])) {
                    $deliveryNoteProductQuantity = $g['deliveryNoteProductQuantity'] + $deliveryNoteProductData['products'][$g['productID']]['deliveryNoteQuantity'];
                } else {
                    $deliveryNoteProductQuantity = $g['deliveryNoteProductQuantity'];
                }

                $deliveryNoteProductData['products'][$g['productID']] = array(
                    'productID' => $g['productID'],
                    'deliveryNoteID' => $g['deliveryNoteID'],
                    'deliveryNoteQuantity' => $deliveryNoteProductQuantity,
                    'locationProductID' => $locationProductData->locationProductID,
                    'documentTypeID' => $g['documentTypeID'],
                    'deliveryNoteProductDocumentID' => $g['deliveryNoteProductDocumentID']
                );
                if ($g['productTypeID'] === '1') {
                    $inventoryProductIDs[] = $g['productID'];
                }

                $getDeliveryNoteSubProductData = $this->CommonTable("Invoice\Model\DeliveryNoteProductTable")->getProductsDetailsByDeliveryNoteID($deliveryNoteID);
                //all subproducts maps to every product--check???
                $deliveryNoteSubProducts = [];
                foreach ($getDeliveryNoteSubProductData as $key => $s) {
                    if ($s['itemOutBatchID'] != NULL || $s['itemOutSerialID'] != NULL) {
                        $getLocationProduct = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProductByLocationProductID($s['itemOutLocationProductID']);
                        $deliveryNoteSubProducts[$getLocationProduct->productID][] = array(
                            'batchID' => $s['itemOutBatchID'],
                            'serialID' => $s['itemOutSerialID'],
                            'qtyByBase' => $s['itemOutQty'],
                            'productPrice' => $s['itemOutPrice'],
                            'locationProductID' => $s['itemOutLocationProductID'],
                        );
                    }
                }
                $deliveryNoteProductData['subProducts'] = $deliveryNoteSubProducts;
            }

            //there have products changes so we need to updates products
            foreach ($deliveryNoteProductData['products'] as $key => $p) {
                $batchProducts = isset($deliveryNoteProductData['subProducts'][$p['productID']]) ? $deliveryNoteProductData['subProducts'][$p['productID']] : [];
                $loreturnQty = $returnQty = $p['deliveryNoteQuantity'];
                $locationProductID = $p['locationProductID'];

                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($p['productID']);

                if($this->useAccounting == 1){
                    if(empty($pData['productInventoryAccountID'])){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                        return $this->JSONRespond();
                    }
                         //check whether Product Cost of Goods Sold Gl account id set or not
                    if(empty($pData['productCOGSAccountID'])){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName']));
                        return $this->JSONRespond();
                    }
                }

                //update SalesOrder Products when it coming from SalesOrder
                if ($p['documentTypeID'] == 3) {
                    $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderIDAndLocationProductId($salesOrderID, $locationProductID);
                    if (isset($salesOrderProducts)) {
                        $newCopiedQuantity = $salesOrderProducts->salesOrdersProductCopiedQuantity - (float) $loreturnQty;
                        $copiedflag = 0;
                        if ($newCopiedQuantity >= $salesOrderProducts->quantity) {
                            $newCopiedQuantity = $salesOrderProducts->quantity;
                            $copiedflag = 1;
                        }
                        $soProData = array(
                            'copied' => $copiedflag,
                            'salesOrdersProductCopiedQuantity' => $newCopiedQuantity,
                            );
                        $this->CommonTable('Invoice\Model\SalesOrderProductTable')->updateCopiedSalesOrderProducts($locationProductID, $salesOrderID, $soProData);
                    }
                }

                //update location product qty by delivery Note qty
                $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);

                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

                $locationProductData = array(
                    'locationProductQuantity' => $locationProduct->locationProductQuantity + $loreturnQty,
                );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateQuantityByID($locationProductData, $locationProductID);

                //if item is non batch or non serial
                if (!count($batchProducts) > 0) {
                    $itemReturnQty = $returnQty;
                    //add item in details for non serial and batch products
                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($locationProduct->locationProductID, 'Delivery Note', $deliveryNoteID);

                    foreach (array_reverse($itemOutDetails) as $itemOutData) {
                        if ($itemReturnQty != 0) {
                            if ($itemOutData->itemOutQty > $itemOutData->itemOutReturnQty) {
                                $itemInPreDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetails($itemOutData->itemOutItemInID);
                                $taxValue = $this->getTaxValue($itemInPreDetails['itemInDocumentID'], $itemInPreDetails['itemInDocumentType'], $itemInPreDetails['itemInLocationProductID']);
                                $newItemInPrice = ($itemInPreDetails['itemInPrice'] + $taxValue) - $itemInPreDetails['itemInDiscount'];
                                $leftQty = floatval($itemOutData->itemOutQty) - floatval($itemOutData->itemOutReturnQty);
                                $itemInInsertQty = 0;
                                $itemOutUpdateReturnQty = 0;
                                if ($leftQty >= $itemReturnQty) {
                                    $itemInInsertQty = $itemReturnQty;
                                    $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) + floatval($itemReturnQty);
                                    $itemReturnQty = 0;
                                } else {
                                    $itemInInsertQty = $leftQty;
                                    $itemOutUpdateReturnQty = $itemOutData->itemOutQty;
                                    $itemReturnQty = floatval($itemReturnQty) - floatval($leftQty);
                                }

                                $unitPrice = $itemInPreDetails['itemInPrice'];
                                $unitDiscount = $itemInPreDetails['itemInDiscount'];
                                if($averageCostingFlag == 1){
                                    $unitPrice = $itemOutData->itemOutAverageCostingPrice;
                                    $unitDiscount = 0;
                                }

                                //hit data to the item in table with left qty
                                $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInPreDetails['itemInIndexID']);
                                $itemInData = array(
                                    'itemInIndex' => $newItemInIndex,
                                    'itemInDocumentType' => 'Delivery Note Edit',
                                    'itemInDocumentID' => $deliveryNoteID,
                                    'itemInLocationProductID' => $locationProduct->locationProductID,
                                    'itemInBatchID' => NULL,
                                    'itemInSerialID' => NULL,
                                    'itemInQty' => $itemInInsertQty,
                                    'itemInPrice' => $unitPrice,
                                    'itemInDiscount' => $unitDiscount,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                //update item out return qty
                                $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);

                                if($this->useAccounting == 1){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = $itemInInsertQty * $itemOutData->itemOutAverageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $itemInInsertQty * ($itemInPreDetails['itemInPrice'] - $itemInPreDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }
                            }
                        } else {
                            break;
                        }
                    }
                }

                //if items are either batch or serial products
                if (count($batchProducts) > 0) {
                    foreach ($batchProducts as $batchKey => $batchValue) {
                        $locationProductID = $batchValue['locationProductID'];
                        if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                            $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                            $productBatchQuentity = $batchProduct->productBatchQuantity + $batchValue['qtyByBase'];
                            $productBatchQty = array(
                                'productBatchQuantity' => $productBatchQuentity,
                            );
                            $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                            if ($batchValue['serialID']) {
                                //Add details to item in table for batch and serial products
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchSerialProductDetails($locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'], $batchValue['batchID'], 'Delivery Note', $deliveryNoteID);

                                $unitPrice = $itemInDetails['itemInPrice'];
                                $unitDiscount = $itemInDetails['itemInDiscount'];
                                if($averageCostingFlag == 1){
                                    $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                    $unitDiscount = 0;
                                }

                                $taxValue = $this->getTaxValue($itemInDetails['itemInDocumentID'], $itemInDetails['itemInDocumentType'], $itemInDetails['itemInLocationProductID']);
                                $newItemInPrice = ($itemInDetails['itemInPrice'] + $taxValue) - $itemInDetails['itemInDiscount'];
                                $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                $itemInData = array(
                                    'itemInIndex' => $newItemInIndex,
                                    'itemInDocumentType' => 'Delivery Note Edit',
                                    'itemInDocumentID' => $deliveryNoteID,
                                    'itemInLocationProductID' => $locationProductID,
                                    'itemInBatchID' => $batchValue['batchID'],
                                    'itemInSerialID' => $batchValue['serialID'],
                                    'itemInQty' => 1,
                                    'itemInPrice' => $unitPrice,
                                    'itemInDiscount' => $unitDiscount,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                //update itemOut return qty
                                $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, '1');

                                if($this->useAccounting == 1){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = 1 * $itemOutDetails->itemOutAverageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }

                            } else {
                                //Add details to item in table for batch products
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchProductDetails($locationProductID, $batchValue['batchID']);
                                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetails($batchValue['batchID'], 'Delivery Note', $deliveryNoteID);

                                $unitPrice = $itemInDetails['itemInPrice'];
                                $unitDiscount = $itemInDetails['itemInDiscount'];
                                if($averageCostingFlag == 1){
                                    $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                    $unitDiscount = 0;
                                }

                                $taxValue = $this->getTaxValue($itemInDetails['itemInDocumentID'], $itemInDetails['itemInDocumentType'], $itemInDetails['itemInLocationProductID']);
                                $newItemInPrice = ($itemInDetails['itemInPrice'] + $taxValue) - $itemInDetails['itemInDiscount'];
                                $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                $itemInData = array(
                                    'itemInIndex' => $newItemInIndex,
                                    'itemInDocumentType' => 'Delivery Note Edit',
                                    'itemInDocumentID' => $deliveryNoteID,
                                    'itemInLocationProductID' => $locationProductID,
                                    'itemInBatchID' => $batchValue['batchID'],
                                    'itemInSerialID' => NULL,
                                    'itemInQty' => $batchValue['qtyByBase'],
                                    'itemInPrice' => $unitPrice,
                                    'itemInDiscount' => $unitDiscount,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);
                                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                //update itemout return qty
                                $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, $batchValue['qtyByBase']);

                                if($this->useAccounting == 1){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = $batchValue['qtyByBase']* $itemOutDetails->itemOutAverageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }
                            }
                        } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                            //Add details to item in table for serial products
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getSerialProductDetails($locationProductID, $batchValue['serialID']);
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Delivery Note', $deliveryNoteID);

                            $unitPrice = $itemInDetails['itemInPrice'];
                            $unitDiscount = $itemInDetails['itemInDiscount'];
                            if($averageCostingFlag == 1){
                                $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                $unitDiscount = 0;
                            }

                            $taxValue = $this->getTaxValue($itemInDetails['itemInDocumentID'], $itemInDetails['itemInDocumentType'], $itemInDetails['itemInLocationProductID']);
                            $newItemInPrice = ($itemInDetails['itemInPrice'] + $taxValue) - $itemInDetails['itemInDiscount'];
                            $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                            $itemInData = array(
                                'itemInIndex' => $newItemInIndex,
                                'itemInDocumentType' => 'Delivery Note Edit',
                                'itemInDocumentID' => $deliveryNoteID,
                                'itemInLocationProductID' => $locationProductID,
                                'itemInBatchID' => NULL,
                                'itemInSerialID' => $batchValue['serialID'],
                                'itemInQty' => 1,
                                'itemInPrice' => $unitPrice,
                                'itemInDiscount' => $unitDiscount,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            //update itemOut return qty
                            $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, '1');
                            if($this->useAccounting == 1){
                                if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                    $productTotal = 1* $itemOutDetails->itemOutAverageCostingPrice;
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }else{
                                    $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }
                            }
                        }
                        if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                            $serialProductData = array(
                                'productSerialSold' => '0',
                            );
                            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                        }
                    }
                }

                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductID);
                $locationPrTotalQty = 0;
                $locationPrTotalPrice = 0;
                $newItemInIds = array();
                if(count($itemInData) > 0){
                    foreach ($itemInData as $key => $value) {
                        if($value->itemInID > $locationProductLastItemInID){
                            $newItemInIds[] = $value->itemInID;
                            $remainQty = $value->itemInQty - $value->itemInSoldQty;
                            if($remainQty > 0){
                                $itemInPrice = $remainQty*$value->itemInPrice;
                                $itemInDiscount = $remainQty*$value->itemInDiscount;
                                $locationPrTotalQty += $remainQty;
                                $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                            }
                            $locationProductLastItemInID = $value->itemInID;
                        }
                    }
                }
                $locationPrTotalQty += $locationProductQuantity;
                $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                if($locationPrTotalQty > 0){
                    $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                    $lpdata = array(
                        'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                        'locationProductLastItemInID' => $locationProductLastItemInID,
                        );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationProductID);

                    foreach ($newItemInIds as $inID) {
                        $intemInData = array(
                            'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                            );
                        $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                    }
                }
            }

            //update SalesOrder status when all products are used to Delivery Note
            if ($salesOrderID) {
                $this->_checkAndUpdateSalesOrderStatus($salesOrderID);
            }

            if ($this->useAccounting == 1) {
                //create data array for the journal entry save.
                $i=0;
                $journalEntryAccounts = array();

                foreach ($accountProduct as $key => $value) {
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Delivery Note Edit '.$deliveryNoteCode;
                    $i++;
                }

                //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);


                $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();

                $checkFiscalPeriod = false;
                if(count($fiscalPeriod) > 0){
                    foreach ($fiscalPeriod as $key => $value) {
                        if($value['fiscalPeriodStatusID'] == 14){
                            if(($value['fiscalPeriodStartDate'] <= $deliveryNoteDate && $deliveryNoteDate <= $value['fiscalPeriodEndDate'])){
                                $checkFiscalPeriod = true;
                            }
                        }
                    }
                }
                if (!$checkFiscalPeriod) {
                    $this->status = false;
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_DATE_IS_NOTIN_FISCALPERIOD_RANGE');
                    return $this->JSONRespond();
                }

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $deliveryNoteDate,
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when Edit Delivery Note Edit.',
                    'documentTypeID' => 4,
                    'journalEntryDocumentID' => $deliveryNoteID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                );

                $resultData = $this->saveJournalEntry($journalEntryData);
                if(!$resultData['status']){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $resultData['msg'];
                    $this->data = $resultData['data'];
                    return $this->JSONRespond();
                }

                $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($deliveryNoteID,4, 10);
                if(!$jEDocStatusUpdate['status']){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $jEDocStatusUpdate['msg'];
                    return $this->JSONRespond();
                }

                foreach ($post['dimensionData'] as $value) {
                    if (!empty($value)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($value, $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $saveRes['msg'];
                            $this->data = $saveRes['data'];
                            return $this->JSONRespond();
                        }   
                    }
                }
            }

            $saveDeliveryNote = $this->storeDeliveryNote($post);
            if ($saveDeliveryNote['status']) {
                $row = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteDetailsByID($post['deliveryNoteID'])->current();
                $changeArray = $this->getDeliveryNoteChageDetails($row, $post);

                //set log details
                $previousData = json_encode($changeArray['previousData']);
                $newData = json_encode($changeArray['newData']);

                $this->setLogDetailsArray($previousData,$newData);
                $this->setLogMessage("Delivery Note ".$changeArray['previousData']['Delivery Note Code']." is updated.");

                $this->commit();
                $this->status = true;
                $this->msg = 'Delivery Note has successfully updated.!';
                $this->data =$saveDeliveryNote['data'];
                $this->flashMessenger()->addMessage($this->msg);
                return $this->JSONRespond();
            } else {
                $this->rollback();
                $this->status = false;
                $this->data = $saveDeliveryNote['data'];
                $this->msg = $saveDeliveryNote['msg'];

                return $this->JSONRespond();
            }
        } else {
            //there has no products changes
            //so normaly update only InvoiceTable
            if ($deliveryCharge != '') {
                $realItemTotalPrice = $deliveryTotalPrice - $deliveryCharge;
            } else {
                $realItemTotalPrice = $deliveryTotalPrice;
            }
            $row = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteDetailsByID($post['deliveryNoteID'])->current();
            $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNote($deliveryNoteData, $deliveryNoteID);

            $changeArray = $this->getDeliveryNoteChageDetails($row, $post);

            //set log details
            $previousData = json_encode($changeArray['previousData']);
            $newData = json_encode($changeArray['newData']);



            if($this->useAccounting){

                /**
                * get journal entry details by given id and document id = 10
                * because grn document id is 10
                */

                $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();

                $checkFiscalPeriod = false;
                if(count($fiscalPeriod) > 0){
                    foreach ($fiscalPeriod as $key => $value) {
                        if($value['fiscalPeriodStatusID'] == 14){
                            if(($value['fiscalPeriodStartDate'] <= $deliveryNoteDate && $deliveryNoteDate <= $value['fiscalPeriodEndDate'])){
                                $checkFiscalPeriod = true;
                            }
                        }
                    }
                }
                if (!$checkFiscalPeriod) {
                    $this->status = false;
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_DATE_IS_NOTIN_FISCALPERIOD_RANGE');
                    return $this->JSONRespond();
                }

                $jeData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('4', $deliveryNoteID);

                $jeNewData['journalEntryDate'] = $deliveryNoteDate;


                $jeUpdated = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntry($jeNewData, $jeData['journalEntryID']);
            }

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("Delivery Note ".$changeArray['previousData']['Delivery Note Code']." is updated.");

            $this->commit();
            $this->status = true;
            $this->data = array('deliveryNoteID' => $deliveryNoteID);
            $this->msg = 'Delivery Note has successfully Updated.!';

            return $this->JSONRespond();
        }
    }

    public function getDeliveryNoteChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Delivery Note Code'] = $row['deliveryNoteCode'];
        $previousData['Customer'] = $row['customerCode'] . '-' . $row['customerName'];
        $previousData['Delivery Note Delivery Date'] = $this->convertDateToUserFormat($row['deliveryNoteDeliveryDate']);
        $previousData['Delivery Note Delivery Charge'] = $row['deliveryNoteCharge'];
        $previousData['Delivery Note Total'] = number_format($row['deliveryNotePriceTotal'],2);
        $previousData['Delivery Note Address'] = $row['deliveryNoteAddress'];
        $previousData['Delivery Note Comment'] = $row['deliveryNoteComment'];

        $newData = [];
        $newData['Delivery Note Code'] = $data['deliverCode'];
        $newData['Customer'] = $row['customerCode'] . '-' . $row['customerName'];
        $newData['Delivery Note Delivery Date'] = $this->convertDateToUserFormat($data['date']);
        $newData['Delivery Note Delivery Charge'] = $data['deliveryCharge'];
        $newData['Delivery Note Total'] = number_format($data['deliveryTotalPrice'],2);
        $newData['Delivery Note Address'] = $data['deliveryAddress'];
        $newData['Delivery Note Comment'] = $data['deliveryComment'];

        return array('previousData' => $previousData, 'newData' => $newData);
    }

    private function _getDeliveryNoteChanges($products = [], $previousProducts = [], $deliveryNoteSubProducts = [], $previousSubProducts = [], $deliveryNoteData, $previousDeliveryNoteTopVals)
    {
        $deliveryNoteChangedList = [];
        if (!empty($products) && !empty($previousProducts)) {

            //if there have new items
            if (array_diff_key($products, $previousProducts)) {
                $newItems = array_diff_key($products, $previousProducts);
                foreach ($newItems as $npID => $value) {
                    $deliveryNoteChangedList[$npID]['newItems']['productID'] = $value['productID'];
                    $deliveryNoteChangedList[$npID]['newItems']['productName'] = $value['productName'];
                    $deliveryNoteChangedList[$npID]['newItems']['productCode'] = $value['productCode'];
                    $deliveryNoteChangedList[$npID]['newItems']['productPrice'] = $value['productPrice'];
                    $deliveryNoteChangedList[$npID]['newItems']['qty'] = $value['deliverQuantity']['qty'];
                }
            }

            foreach ($previousProducts as $iID => $i) {
                 // $proid = explode('_', $iID)[0];
                foreach ($products as $pID => $p) {

                    if (!array_key_exists($iID, $products)) {
                        $deliveryNoteChangedList[$iID]['removeItems']['productID'] = $i['productID'];
                        $deliveryNoteChangedList[$iID]['removeItems']['productName'] = $i['productName'];
                        $deliveryNoteChangedList[$iID]['removeItems']['productCode'] = $i['productCode'];
                        $deliveryNoteChangedList[$iID]['removeItems']['productPrice'] = $i['productPrice'];
                        $deliveryNoteChangedList[$iID]['removeItems']['qty'] = $i['deliverQuantity']['qty'];
                    }

                    if (array_key_exists($iID, $products)) {
                        //if tax is removed from invoice edit
                        //then current array no any element called pTax
                        if ($pID == $iID) {
                            if (!isset($products[$pID]['pTax']) && isset($previousProducts[$iID]['pTax'])) {
                                $deliveryNoteChangedList[$iID]['removedTax'] = $previousProducts[$iID]['pTax']['tL'];
                            } if (isset($products[$pID]['pTax']) && isset($previousProducts[$iID]['pTax'])) {
                                $products[$pID]['pTax'] = (empty($products[$pID]['pTax'])) ? [] : $products[$pID]['pTax'];
                                $previousProducts[$iID]['pTax'] = (empty($previousProducts[$iID]['pTax'])) ? [] : $previousProducts[$iID]['pTax'];
                                $diffProductTax = array_diff_assoc($products[$pID]['pTax'], $previousProducts[$iID]['pTax']);
                                if (!empty($diffProductTax)) {
                                    foreach ($previousProducts[$iID]['pTax']['tL'] as $itKey => $value) {
                                        foreach ($products[$pID]['pTax']['tL'] as $ptKey => $value) {

                                            if (array_key_exists($itKey, $products[$pID]['pTax']['tL'])) {
                                                continue;
                                            } else {
                                                $deliveryNoteChangedList[$iID]['removedTax'][$itKey] = $previousProducts[$iID]['pTax']['tL'][$itKey];
                                            }
                                        }
                                    }
                                }
                            }

                            //products changes get this methode without product qty
                            if ($diffProductInsideChanges = array_diff_assoc($products[$pID], $previousProducts[$iID])) {
                                unset($diffProductInsideChanges['productTotal']);
                                unset($diffProductInsideChanges['pTax']);
                                unset($diffProductInsideChanges['productID']);

                                if (!empty($diffProductInsideChanges)) {
                                    foreach ($diffProductInsideChanges as $key => $value) {
                                        $deliveryNoteChangedList[$iID]['itemChanges'][$key] = [
                                            'productID' => $previousProducts[$iID]['productID'],
                                            'productName' => $previousProducts[$iID]['productName'],
                                            'productCode' => $previousProducts[$iID]['productCode'],
                                            'from' => [
                                                'source' => $key,
                                                'change' => $previousProducts[$iID][$key]],
                                            'to' => [
                                                'source' => $key,
                                                'change' => $products[$pID][$key]
                                            ]
                                        ];
                                    }
                                }
                            }
                            //get qty changes
                            //this methode is using because of qty is inside the array(and also array_diff_assoc method)
                            if ($diffProductQtyInsideChanges = array_diff_assoc($products[$pID]['deliverQuantity'], $previousProducts[$iID]['deliverQuantity'])) {
                                unset($diffProductInsideChanges['productTotal']);
                                foreach ($diffProductQtyInsideChanges as $key => $value) {

                                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($i['productID']);
                                    $thisqty = $this->getProductQuantityViaDisplayUom($p['deliverQuantity']['qty'], $productUom);
                                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                                    $deliveryNoteChangedList[$iID]['itemChanges'][$key] = [
                                        'productID' => $previousProducts[$iID]['productID'],
                                        'productName' => $previousProducts[$iID]['productName'],
                                        'productCode' => $previousProducts[$iID]['productCode'], 'from' => [
                                            'source' => $key,
                                            'change' => number_format($previousProducts[$iID]['deliverQuantity']['qty'], 2) . ' ' . $thisqty['uomAbbr']
                                        ], 'to' => [
                                            'source' => $key,
                                            'change' => number_format($products[$pID]['deliverQuantity']['qty'], 2) . ' ' . $thisqty['uomAbbr']
                                        ]
                                    ];
                                }
                            }

                            $productsTax = isset($products[$pID]['pTax']['tL']) ? $products[$pID]['pTax']['tL'] : [];
                            $insertedProductsTax = isset($previousProducts[$iID]['pTax']['tL']) ? $previousProducts[$iID]['pTax']['tL'] : [];
                            if ($diffProductTaxInsideChanges = array_diff_assoc($productsTax, $insertedProductsTax)) {
                                unset($diffProductInsideChanges['productTotal']);
                                foreach ($diffProductTaxInsideChanges as $key => $value) {
                                    $deliveryNoteChangedList[$iID]['addedTax'][$key] = $value;
                                }
                            }
                        }
                    }
                }
            }

            //check invoice title data has changed

            $deliveryNoteTitleChanges = array_diff_assoc($deliveryNoteData, $previousDeliveryNoteTopVals);
            if (!empty($previousDeliveryNoteTopVals)) {
                foreach ($deliveryNoteTitleChanges as $key => $value) {
                    $deliveryNoteChangedList['titleChanges'][$key]['to'] = $deliveryNoteData[$key];
                    $deliveryNoteChangedList['titleChanges'][$key]['from'] = $previousDeliveryNoteTopVals[$key];
                }
            }
            //compaire subproducts list with current and already inserted
            if (!empty($deliveryNoteSubProducts)) {
                foreach ($deliveryNoteSubProducts as $pID => $data) {
                    foreach ($data as $key => $value) {
                        unset($deliveryNoteSubProducts[$pID][$key]['warranty']);
                        unset($deliveryNoteSubProducts[$pID][$key]['incrementID']);
                    }
                }
            }

            if (!empty($previousSubProducts)) {
                foreach ($previousSubProducts as $pID => $data) {
                    foreach ($data as $key => $value) {
                        unset($previousSubProducts[$pID][$key]['warranty']);
                        unset($previousSubProducts[$pID][$key]['incrementID']);
                    }
                }
            }

            $sortSubProducts = empty($deliveryNoteSubProducts) ? $deliveryNoteSubProducts : $this->_sortSubProducts($deliveryNoteSubProducts);
            $sortInsertedSubProducts = empty($previousSubProducts) ? $previousSubProducts : $this->_sortSubProducts($previousSubProducts);

            if (!empty($sortSubProducts) && !empty($sortInsertedSubProducts)) {
                foreach ($sortInsertedSubProducts as $iProId => $iData) {
                    foreach ($sortSubProducts as $sProId => $sData) {

                        if (array_key_exists($iProId, $sortSubProducts)) {
                            foreach ($sortInsertedSubProducts[$iProId] as $iKey => $iSubData) {
                                foreach ($sortSubProducts[$iProId] as $sKey => $sSubData) {

                                    if (!array_key_exists($iKey, $sortSubProducts[$iProId])) {
                                        $deliveryNoteChangedList[$iProId]['subProducts']['removed'][$iKey] = $iSubData;
                                    }

                                    if (!array_key_exists($sKey, $sortInsertedSubProducts[$iProId])) {
                                        $deliveryNoteChangedList[$iProId]['subProducts']['added'][$sKey] = $sSubData;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return $deliveryNoteChangedList;
        }
    }

    private function _sortSubProducts($subProducts)
    {
        $sortSubProducts = [];
        foreach ($subProducts as $pID => $data) {
            foreach ($data as $key => $value) {
                $batchID = isset($value['batchID']) ? $value['batchID'] : '';
                $serialID = isset($value['serialID']) ? $value['serialID'] : '';
                $sortSubProducts[$pID][$serialID . $batchID] = [
                    'batchID' => $batchID,
                    'serialID' => $serialID,
                    'qtyByBase' => $value['qtyByBase']
                ];
            }
        }

        return $sortSubProducts;
    }

    public function storeDeliveryNote($post)
    {
        $deliveryNoteID = $post['deliveryNoteID'];
        $customerID = $post['customer'];
        $customerName = $post['customerName'];
        $customCurrencyId = $post['customCurrencyId'];
        $ignoreBudgetLimit = $post['ignoreBudgetLimit'];

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "") ? $customCurrencyData->currencyRate : 1;
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $ccrate = $post['customCurrencyRate'];
        //if custom currency rate alredy set add that rate to sales Order
        if ($salesOrderID && !($ccrate == 0 || $ccrate == 1)) {
            $customCurrencyRate = $ccrate;
        }
        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        if ($cust->customerName . '-' . $cust->customerCode == $customerName) {
            $products =  $post['products'];
            $deliverCode = $post['deliverCode'];
            $locationOut = $post['locationOutID'];
            $date = $this->convertDateToStandardFormat($post['date']);
            $deliveryCharge = $post['deliveryCharge'] * $customCurrencyRate;
            $totalPrice = trim($post['deliveryTotalPrice']) * $customCurrencyRate;
            $deliveryAddress = $post['deliveryAddress'];
            $comment = $post['deliveryComment'];
            $salesOrderID = $post['salesOrderID'];
            $salesPersonID = $post['salesPersonID'];
            $priceListId = $post['priceListId'];
            $customerProfID = $post['customer_prof_ID'];
            if (!empty($salesOrderID)) {
                $fromSalesOrder = TRUE;
            } else {
                $fromSalesOrder = FALSE;
            }

            $entityID = $this->createEntity();
            $deliveryData = array(
                'deliveryNoteCode' => $deliverCode,
                'customerID' => $customerID,
                'deliveryNoteLocationID' => $locationOut,
                'deliveryNoteDeliveryDate' => $date,
                'deliveryNoteCharge' => $deliveryCharge,
                'deliveryNotePriceTotal' => $totalPrice,
                'deliveryNoteAddress' => $deliveryAddress,
                'deliveryNoteComment' => $comment,
                'salesOrderID' => $salesOrderID,
                'entityID' => $entityID,
                'salesPersonID' => $salesPersonID,
                'customCurrencyId' => $customCurrencyId,
                'deliveryNoteCustomCurrencyRate' => $customCurrencyRate,
                'priceListId' => $priceListId,
                'customerProfileID' => $customerProfID,
                );

            $deliveryNote = new DeliveryNote;
            $deliveryNote->exchangeArray($deliveryData);

            $deliveryNoteID = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->saveDeliveryNote($deliveryNote);

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }

            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            $accountProduct = array();

            foreach ($products as $key => $p) {
                $batchProducts = $post['subProducts'][$key];
                $productID = $p['productID'];
                $deliveryQty = $p['deliverQuantity']['qty'];
                $productType = $p['productType'];
                $locationProductOUT = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationOut);
                $averageCostingPrice = $locationProductOUT->locationProductAverageCostingPrice;
                $locationProductID = $locationProductOUT->locationProductID;

                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

                if($this->useAccounting == 1){
                    if($pData['productInventoryAccountID'] == ''){
                        return array(
                            'status'=> false,
                            'msg'=> $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                        );
                    }
                         //check whether Product Cost of Goods Sold Gl account id set or not
                    if($pData['productCOGSAccountID'] == ''){
                        return array(
                            'status'=> false,
                            'msg'=> $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName'])),
                        );
                    }
                }

                if ($productType != 2) {
                    if ($locationProductOUT->locationProductQuantity < $deliveryQty) {
                        return array(
                            'status'=> false,
                            'msg'=> $this->getMessage('ERR_DELINOTEAPI_PRODUCT_UPDATE'),
                        );
                    }
                    $locationProductOUTData = array(
                        'locationProductQuantity' => $locationProductOUT->locationProductQuantity - $deliveryQty,
                        );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductOUTData, $productID, $locationOut);
                }

                    //calculate discount value
                $discountValue = 0;
                if ($p['productDiscountType'] == "precentage") {
                    $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $customCurrencyRate;
                } else if ($p['productDiscountType'] == "value") {
                    $discountValue = $p['productDiscount'] * $customCurrencyRate;
                }

                if ($p['productDiscountType'] == 'value') {
                    $p['productDiscount'] = $p['productDiscount'] * $customCurrencyRate;
                }

                $deliveryNoteProductDocumentCopiedQty = 0;
                if($p['documentTypeID'] == 3){
                    $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderIDAndLocationProductId($p['documentID'], $locationProductID);
                    $availableQty = $salesOrderProducts->quantity - $salesOrderProducts->salesOrdersProductCopiedQuantity;
                    if($availableQty > $deliveryQty){
                        $deliveryNoteProductDocumentCopiedQty = $deliveryQty;
                    }else{
                        $deliveryNoteProductDocumentCopiedQty = $availableQty;
                    }
                }

                $deliveryNoteProductData = array(
                    'deliveryNoteID' => $deliveryNoteID,
                    'productID' => $productID,
                    'deliveryNoteProductPrice' => $p['productPrice'] * $customCurrencyRate,
                    'deliveryNoteProductDiscount' => $p['productDiscount'],
                    'deliveryNoteProductDiscountType' => $p['productDiscountType'],
                    'deliveryNoteProductTotal' => $p['productTotal'] * $customCurrencyRate,
                    'deliveryNoteProductQuantity' => $deliveryQty,
                    'documentTypeID' => $p['documentTypeID'],
                    'deliveryNoteProductDocumentID' => $p['documentID'],
                    'deliveryNoteProductDocumentTypeCopiedQty' => $deliveryNoteProductDocumentCopiedQty
                );

                $deliveryNoteProduct = new DeliveryNoteProduct;
                $deliveryNoteProduct->exchangeArray($deliveryNoteProductData);
                $deliveryNoteProductID = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->saveDeliveryNoteProduct($deliveryNoteProduct);
                $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationOut);
                $locationPID = $locationProduct->locationProductID;
                if ($p['pTax']) {
                    foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                        $deliveryNotePTaxesData = array(
                            'deliveryNoteProductID' => $deliveryNoteProductID,
                            'productID' => $productID,
                            'deliveryNoteTaxID' => $taxKey,
                            'deliveryNoteTaxPrecentage' => $productTax['tP'],
                            'deliveryNoteTaxAmount' => $productTax['tA'] * $customCurrencyRate
                            );
                        $deliveryNotePTaxM = new DeliveryNoteProductTax();
                        $deliveryNotePTaxM->exchangeArray($deliveryNotePTaxesData);
                        $this->CommonTable('Invoice\Model\DeliveryNoteProductTaxTable')->saveDeliveryNoteProductTax($deliveryNotePTaxM);
                    }
                }
                    //check and insert items to itemout table
                if ($productType != 2) {
                    if (!count($batchProducts) > 0) {
                        $sellingQty = $deliveryQty;
                        while ($sellingQty != 0) {
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($locationProductOUT->locationProductID);
                            if (!$itemInDetails) {
                                $respond['status'] = false;
                                $respond['msg'] = $this->getMessage('ERR_NO_ANY_REMAIN_QTY', array($pData['productCode'].' - '.$pData['productName']));
                                return $respond;
                                // break;
                            }
                            if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Delivery Note',
                                    'itemOutDocumentID' => $deliveryNoteID,
                                    'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $sellingQty,
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                if($this->useAccounting == 1){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = $sellingQty * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $sellingQty * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }
                                $sellingQty = 0;
                                break;
                            } else {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Delivery Note',
                                    'itemOutDocumentID' => $deliveryNoteID,
                                    'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                 if($this->useAccounting == 1){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal =  $itemInDetails['itemInRemainingQty'] * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $itemInDetails['itemInRemainingQty'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }

                                $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                            }
                        }
                    }
                }
                    //update SalesOrder Products when it coming from SalesOrder
                if ($p['documentTypeID'] == 3 && $fromSalesOrder == TRUE) {
                    $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderIDAndLocationProductId($salesOrderID, $locationPID);
                    if (isset($salesOrderProducts)) {
                        $newCopiedQuantity = $salesOrderProducts->salesOrdersProductCopiedQuantity + (float) $deliveryQty;
                        $copiedflag = 0;
                        if ($newCopiedQuantity >= $salesOrderProducts->quantity) {
                            $newCopiedQuantity = $salesOrderProducts->quantity;
                            $copiedflag = 1;
                        }
                        $soProData = array(
                            'copied' => $copiedflag,
                            'salesOrdersProductCopiedQuantity' => $newCopiedQuantity,
                            );
                        $this->CommonTable('Invoice\Model\SalesOrderProductTable')->updateCopiedSalesOrderProducts($locationPID, $salesOrderID, $soProData);
                    }
                }
                if (count($batchProducts) > 0) {
                    foreach ($batchProducts as $batchKey => $batchValue) {
                        if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                            $this->saveDeliverySubProductData($batchValue, $deliveryNoteProductID);
                            $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                            $productBatchQuentity = $batchProduct->productBatchQuantity - $batchValue['qtyByBase'];
                            if ($batchProduct->productBatchQuantity < $batchValue['qtyByBase']) {
                                return array(
                                    'status'=> false,
                                    'msg'=>$this->getMessage('ERR_DELINOTEAPI_PRODUCT_UPDATE'),
                                );
                            }
                            if ($batchValue['serialID']) {
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProductOUT->locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Delivery Note',
                                    'itemOutDocumentID' => $deliveryNoteID,
                                    'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                    'itemOutBatchID' => $batchValue['batchID'],
                                    'itemOutSerialID' => $batchValue['serialID'],
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $batchValue['qtyByBase'],
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                if($this->useAccounting == 1){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }

                            } else {
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProductOUT->locationProductID, $batchValue['batchID']);
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Delivery Note',
                                    'itemOutDocumentID' => $deliveryNoteID,
                                    'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                    'itemOutBatchID' => $batchValue['batchID'],
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $batchValue['qtyByBase'],
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                if($this->useAccounting == 1){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }
                            }

                            $productBatchQty = array(
                                'productBatchQuantity' => $productBatchQuentity,
                                );
                            $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                            if ($batchValue['serialID']) {
                                $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                                if ($checkSerial->locationProductID != $locationProductOUT->locationProductID || $checkSerial->productSerialSold == 1) {
                                    return array(
                                        'status'=> false,
                                        'msg'=>$this->getMessage('ERR_DELINOTEAPI_PRODUCT_UPDATE'),
                                    );
                                }
                                $serialProductData = array(
                                    'productSerialSold' => '1',
                                    );
                                $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                            }
                        } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                            $this->saveDeliverySubProductData($batchValue, $deliveryNoteProductID);
                            $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                            if ($checkSerial->locationProductID != $locationProductOUT->locationProductID || $checkSerial->productSerialSold == 1) {
                                return array(
                                    'status'=> false,
                                    'msg'=>$this->getMessage('ERR_DELINOTEAPI_PRODUCT_UPDATE'),
                                );
                            }

                                ///insert to the item out table
                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProductOUT->locationProductID, $batchValue['serialID']);
                            $itemOutM = new ItemOut();
                            $itemOutData = array(
                                'itemOutDocumentType' => 'Delivery Note',
                                'itemOutDocumentID' => $deliveryNoteID,
                                'itemOutLocationProductID' => $locationProductOUT->locationProductID,
                                'itemOutBatchID' => NULL,
                                'itemOutSerialID' => $batchValue['serialID'],
                                'itemOutItemInID' => $itemInDetails['itemInID'],
                                'itemOutQty' => 1,
                                'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                'itemOutDiscount' => $discountValue,
                                'itemOutAverageCostingPrice' => $averageCostingPrice,
                                'itemOutDateAndTime' => $this->getGMTDateTime()
                                );

                            $itemOutM->exchangeArray($itemOutData);
                            $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                            $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);

                            if($this->useAccounting == 1){
                                if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                    $productTotal = 1 * $averageCostingPrice;
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }else{
                                    $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                    if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                    }

                                    if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                    }else{
                                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                    }
                                }
                            }

                            $serialProductData = array(
                                'productSerialSold' => '1',
                                );
                            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                        }
                    }
                } else {
                    //     $this->saveTransferProductData(array('batchID' => 0, 'serialID' => 0, 'qtyByBase' => $p['transferQuantity']['qtyByBase']), $trasferID, $productID);
                }
            }
                //update SalesOrder status when all products are used to Delivery Note
            if ($fromSalesOrder == TRUE) {
                $this->_checkAndUpdateSalesOrderStatus($salesOrderID);
            }

                // call product updated event
            $productIDs = array_map(function($element) {
                return $element['productID'];
            }, $products);

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationOut]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            if ($this->useAccounting == 1) {
                    //create data array for the journal entry save.
                $i=0;
                $journalEntryAccounts = array();

                foreach ($accountProduct as $key => $value) {
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Delivery Note '.$deliverCode.'.';
                    $i++;
                }

                    //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $date,
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create Delivery Note '.$deliverCode.'.',
                    'documentTypeID' => 4,
                    'journalEntryDocumentID' => $deliveryNoteID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                );

                $resultData = $this->saveJournalEntry($journalEntryData);
                if(!$resultData['status']){
                    return array(
                        'status'=> false,
                        'msg'=>$resultData['msg'],
                        'data'=>$resultData['data'],
                    );
                }

                $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($deliveryNoteID,4, 3);
                if(!$jEDocStatusUpdate['status']){
                    return array(
                        'status'=> false,
                        'msg'=>$jEDocStatusUpdate['msg'],
                        );
                }

                foreach ($post['dimensionData'] as $value) {
                    if (!empty($value)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($value, $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $saveRes['msg'];
                            $this->data = $saveRes['data'];
                            return $this->JSONRespond();
                        }   
                    }
                }
            }
            return array(
                'status'=> true,
                'data'=>(object) array('deliveryNoteCode' => $deliverCode, 'deliveryNoteID' => $deliveryNoteID),
                'msg'=>$this->getMessage('SUCC_DELINOTEAPI_PRODUCT_ADD', array($deliverCode)),
            );
        } else {
             return array(
                'status'=> false,
                'msg'=>$this->getMessage('ERR_DELINOTEAPI_VALID_CUST'),
            );
        }
    }

    public function getAllRelatedDocumentDetailsByDeliveryNoteIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $deliveryNoteID = $request->getPost('deliveryNoteID');
            $deliveryNoteData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteDetailsByDeliveryNoteId($deliveryNoteID);
            $salesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getDlnRelatedSalesOrderDataBydlnId($deliveryNoteID);
            $quotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getDlnRelatedQuotationDataBydlnId($deliveryNoteID);
            $salesReturnData = $this->CommonTable('SalesReturnsTable')->getDeliveryNoteRelatedReturnsDataByQuotationId(null, null, $deliveryNoteID);
            $dlnRelatedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getDeliveryNoteRelatedSalesInvoiceDataByQuotationId(null, null, $deliveryNoteID);
            $dlnRelatedPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getDeliveryNoteRelatedInvoicePaymentsDataByQuotationId(null, null, $deliveryNoteID);
            $dlnRelatedCreditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDeliveryNoteRelatedCreditNoteDataByQuotationId(null, null, $deliveryNoteID);
            $dlnRelatedCreditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getDeliveryNoteRelatedCreditNotePaymentDataByQuotationId(null, null, $deliveryNoteID);


            $dataExistsFlag = false;
            if ($deliveryNoteData) {
                $deliveryNoteData = array(
                    'type' => 'DeliveryNote',
                    'documentID' => $deliveryNoteData['deliveryNoteID'],
                    'code' => $deliveryNoteData['deliveryNoteCode'],
                    'amount' => number_format($deliveryNoteData['deliveryNotePriceTotal'], 2),
                    'issuedDate' => $deliveryNoteData['deliveryNoteDeliveryDate'],
                    'created' => $deliveryNoteData['createdTimeStamp'],
                );
                $deliveryNoteDetails[] = $deliveryNoteData;
                $dataExistsFlag = true;
            }

            if (isset($salesOrderData)) {
                foreach ($salesOrderData as $soDta) {
                    $soData = array(
                        'type' => 'SalesOrder',
                        'documentID' => $soDta['soID'],
                        'code' => $soDta['soCode'],
                        'amount' => number_format($soDta['totalAmount'], 2),
                        'issuedDate' => $soDta['issuedDate'],
                        'created' => $soDta['createdTimeStamp'],
                    );
                    $deliveryNoteDetails[] = $soData;
                    if (isset($soDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($quotaionData)) {
                foreach ($quotaionData as $qtnDta) {
                    $qtnDeta = array(
                        'type' => 'Quotation',
                        'documentID' => $qtnDta['quotationID'],
                        'code' => $qtnDta['quotationCode'],
                        'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                        'issuedDate' => $qtnDta['quotationIssuedDate'],
                        'created' => $qtnDta['createdTimeStamp'],
                    );
                    $deliveryNoteDetails[] = $qtnDeta;
                    if (isset($qtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }



            if (isset($salesReturnData)) {
                foreach ($salesReturnData as $rtnDta) {
                    $srData = array(
                        'type' => 'SalesReturn',
                        'documentID' => $rtnDta['salesReturnID'],
                        'code' => $rtnDta['salesReturnCode'],
                        'amount' => number_format($rtnDta['salesReturnTotal'], 2),
                        'issuedDate' => $rtnDta['salesReturnDate'],
                        'created' => $rtnDta['createdTimeStamp'],
                    );
                    $deliveryNoteDetails[] = $srData;
                    if (isset($rtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedInvoiceData)) {
                foreach ($dlnRelatedInvoiceData as $invDta) {
                    $invData = array(
                        'type' => 'SalesInvoice',
                        'documentID' => $invDta['salesInvoiceID'],
                        'code' => $invDta['salesInvoiceCode'],
                        'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                        'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                        'created' => $invDta['createdTimeStamp'],
                    );
                    $deliveryNoteDetails[] = $invData;
                    if (isset($invDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedPaymentData)) {
                foreach ($dlnRelatedPaymentData as $payDta) {
                    $paymentData = array(
                        'type' => 'CustomerPayment',
                        'documentID' => $payDta['incomingPaymentID'],
                        'code' => $payDta['incomingPaymentCode'],
                        'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                        'issuedDate' => $payDta['incomingPaymentDate'],
                        'created' => $payDta['createdTimeStamp'],
                    );
                    $deliveryNoteDetails[] = $paymentData;
                    if (isset($payDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedCreditNoteData)) {
                foreach ($dlnRelatedCreditNoteData as $cNDta) {
                    $creditNoteData = array(
                        'type' => 'CreditNote',
                        'documentID' => $cNDta['creditNoteID'],
                        'code' => $cNDta['creditNoteCode'],
                        'amount' => number_format($cNDta['creditNoteTotal'], 2),
                        'issuedDate' => $cNDta['creditNoteDate'],
                        'created' => $cNDta['createdTimeStamp'],
                    );
                    $deliveryNoteDetails[] = $creditNoteData;
                    if (isset($cNDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedCreditNotePaymentData)) {
                foreach ($dlnRelatedCreditNotePaymentData as $cNPDta) {
                    $creditNotePaymentData = array(
                        'type' => 'CreditNotePayment',
                        'documentID' => $cNPDta['creditNotePaymentID'],
                        'code' => $cNPDta['creditNotePaymentCode'],
                        'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                        'issuedDate' => $cNPDta['creditNotePaymentDate'],
                        'created' => $cNPDta['createdTimeStamp'],
                    );
                    $deliveryNoteDetails[] = $creditNotePaymentData;
                    if (isset($cNPDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($deliveryNoteDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $deliveryNoteDetails);

            $documentDetails = array(
                'deliveryNoteDetails' => $deliveryNoteDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }


    public function getJobRelatedDeliveryNotesAction() 
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = 'Request is not a post resquest.!';
            return $this->JSONRespond();
        }        

        $deliveryNoteDetails = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteDetailsByJobId($request->getPost('jobID'));

        $deliveryNoteIds = [];
        $hasProducts = false;
        foreach ($deliveryNoteDetails as $value) {
            $hasProducts = true;
            $deliveryNoteIds[] = $value['deliveryNoteID'];
        }

        if ($hasProducts) {
            $this->status = true;
        } else {
            $this->status = false;
        }

        $this->data = array('deliveryNoteIds' => $deliveryNoteIds);

        return $this->JSONRespond();
    }


    public function getDlnBySerialCodefilterAction() {
        $invrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($invrequest->isPost()) {
            $searchString = $invrequest->getPost('searchString');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $filteredDeliveryNote = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDlnBySerialCode($searchString, $locationID);

            while ($t = $filteredDeliveryNote->current()) {
                $deliveryNotes[$t['deliveryNoteID']] = (object) $t;
            }

            $DateFilteredDeliveryNote = new ViewModel(
                    array('deliveryNote' => $deliveryNotes,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,));
            $DateFilteredDeliveryNote->setTerminal(TRUE);
            $DateFilteredDeliveryNote->setTemplate('invoice/delivery-note/deliveryNoteList');
            return $DateFilteredDeliveryNote;
        }

    }
}

?>
