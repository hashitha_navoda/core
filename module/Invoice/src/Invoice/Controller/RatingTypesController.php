<?php

/**
 * Description of Rating Types
 *
 * @author Ashan Madushka <ashan@thinkcube.com>
 */

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Form\RatingTypesForm;

class RatingTypesController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'customer_upper_menu';
    protected $sidemenu;
    protected $uppermenu;

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Customers', 'Rating Types', 'CRM');

        $this->getPaginatedRatingTypes(6);
        $form = new RatingTypesForm();

        $view = new ViewModel(array(
            'form' => $form,
            'ratingTypesList' => $this->paginator,
            'isPaginated' => true
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/ratingTypes.js');

        return $view;
    }

    private function getPaginatedRatingTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Invoice\Model\RatingTypesTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
    }

}
