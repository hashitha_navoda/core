<?php

/**
 * @author ashan madushka <ashan@thinkcube.com>
 * This file contains Return Process related controller functions
 */

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Invoice\Form\ReturnForm;
use Core\Controller\Interfaces\DocumentInterface;
use Zend\I18n\Translator\Translator;

class ReturnController extends CoreController implements DocumentInterface
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'sales_returns_upper_menu';
    protected $userID;
    protected $user_session;
    protected $company;
    protected $cdnUrl;
    protected $useAccounting;
    private $_returnViewData;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        $this->jobModuleEnabled = $this->user_session->jobModuleEnabled;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Returns', 'Create Return', 'SALES');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(7, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();

        $returnForm = new ReturnForm(
                array(
            'customCurrency' => $currency,
                )
        );

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/return.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/calculations.js');

        $returnForm->get('customer')->setAttribute('disabled', true);

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');

        $todayDateTime = $this->getUserDateTime();
        $todayDate = date('Y-m-d', strtotime($todayDateTime));
        $dateFormat = $this->getUserDateFormat();
        $returnAddView = new ViewModel(
                array(
            'returnForm' => $returnForm,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'referenceNumber' => $rid,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'jobModuleEnabled' => $this->jobModuleEnabled,
            'todayDate' => $todayDate,
            'dateFormat' => $dateFormat
                )
        );
        $returnsProductsView = new ViewModel();
        $returnsProductsView->setTemplate('invoice/return/return-add-products');
        $returnAddView->addChild($returnsProductsView, 'returnAddProducts');
        $returnsubProductsView = new ViewModel();
        $returnsubProductsView->setTemplate('invoice/return/return-add-sub-products');
        $returnAddView->addChild($returnsubProductsView, 'returnAddSubProducts');
        $returnAddView->addChild($dimensionAddView, 'dimensionAddView');
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Sales Return Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Sales Return Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $returnAddView->addChild($refNotSet, 'refNotSet');
        }
        return $returnAddView;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Returns', 'View Returns', 'SALES');
        $this->getPaginatedReturns();

        $dateFormat = $this->getUserDateFormat();
        $invoiceView = new ViewModel(array(
            'return' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'dateFormat' => $dateFormat,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
                )
        );
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('invoice/return/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/return/document-view');
        $invoiceView->addChild($docHistoryView, 'docHistoryView');
        $invoiceView->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $invoiceView->addChild($attachmentsView, 'attachmentsView');

        return $invoiceView;
    }

    public function viewReturnReceiptAction()
    {
        return $this->documentPreviewAction();
    }

    public function documentPreviewAction()
    {
        $returnID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($returnID);
        $data['salesReturnID'] = $data['salesReturnID'];
        $data['total'] = number_format($data['total'], 2);
        $path = "/return/document/"; //.$returnID;
        $translator = new Translator();
        $createNew = $translator->translate("New Return");
        $createPath = "/return/create";

        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Sales Return from " . $data['companyName'],
            "body" => <<<EMAILBODY

Dear {$data['customerName']}, <br /><br />

Thank you for your inquiry. <br /><br />

A Sales Return has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>SR No:</strong> {$data['salesReturnCode']} <br />
<strong>SR Amount:</strong> {$data['currencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $journalEntryValues = [];
        if($this->useAccounting == 1){
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('5',$returnID);
            $journalEntryID = $journalEntryData['journalEntryID'];
            $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryValues = $jeResult['JEDATA']; 
        }

        $JEntry = new ViewModel(
            array(
             'journalEntry' => $journalEntryValues,
             'closeHidden' => true 
             )
            );
        $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $documentType = 'Sales Returns';
        $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $returnID, $createPath);
        $preview->addChild($JEntry, 'JEntry');
        
        $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
        $additionalButton->setTemplate('additionalButton');
        $preview->addChild($additionalButton, 'additionalButton');
        return  $preview;
    }

    public function getDataForDocument($returnID)
    {

        if (!empty($this->_returnViewData)) {
            return $this->_returnViewData;
        }

        $data = $this->getDataForDocumentView();

        $returnDetails = $this->CommonTable('SalesReturnsTable')->getReturnDetailsByReturnID($returnID)->current();
        $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($returnDetails['customerID']);

        $customCurrencyId = $returnDetails['customCurrencyId'];
        $returnDetails['createdTimeStamp'] = $this->getUserDateTime($returnDetails['createdTimeStamp']);

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "" || $customCurrencyId != 0) ? $returnDetails['salesReturnCustomCurrencyRate'] : 1;
        $customCurrencySymbol = ($customCurrencyId != "" || $customCurrencyId != 0) ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;
        $data['currencySymbol'] = $customCurrencySymbol;

        $returnDetails['salesReturnDate'] = $this->convertDateToUserFormat($returnDetails['salesReturnDate']);
        $data = array_merge($data, $returnDetails, (array) $customerDetails);

        // to be used by below functions - convert to object
        $returnDetails = (object) $returnDetails;

        $returnProductDetails = $this->CommonTable('SalesReturnsProductTable')->getAllReturnProductDetailsByReturnID($returnID, $returnDetails->directReturnFlag);

        $txTypes = array();
        $sub_total = 0;
        foreach ($returnProductDetails as $product) {

            $product = (object) $product;

            $total_itm_tx = 0;
            $productsTax = $this->CommonTable('SalesReturnsProductTaxTable')->getReturnProductTaxByReturnProductID($product->salesReturnProductID);
            foreach ($productsTax as $productTax) {
                if (!isset($txTypes[$productTax['taxName']])) {
                    $txTypes[$productTax['taxName']] = $productTax['salesReturnProductTaxAmount'] / $customCurrencyRate;
                } else {
                    $txTypes[$productTax['taxName']] += $productTax['salesReturnProductTaxAmount'] / $customCurrencyRate;
                }
                $total_itm_tx += $productTax['salesReturnProductTaxAmount'] / $customCurrencyRate;
            }

            $itemDiscount = ($product->salesReturnProductDiscountType == 'precentage') ? ($product->salesReturnProductPrice * $product->salesReturnProductDiscount / 100) / $customCurrencyRate : $product->salesReturnProductDiscount / $customCurrencyRate;
            $item_tax_string = ($total_itm_tx > 0) ? "Tax " . number_format($total_itm_tx, 2) : "";
            $item_disc_string = "";

            if ($itemDiscount > 0) {
                if ($product->salesReturnProductDiscountType == 'precentage') {
                    $item_disc_string = "Disc: " . number_format($product->salesReturnProductDiscount, 2) . "%";
                } else {
                    $item_disc_string = "Disc: " . $data['currencySymbol'] . number_format($itemDiscount, 2) . "";
                }
            }
            $sub_line = "";
            if ($total_itm_tx > 0) {
                if ($item_tax_string != "" && $item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                } else if ($item_tax_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ")</small>";
                } else if ($item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                }
                $unit = $product->salesReturnProductPrice / $customCurrencyRate;
            } else {
                $unit = $product->salesReturnProductPrice / $customCurrencyRate + ($total_itm_tx / $product->salesReturnProductQuantity);
                $sub_line = ($product->salesReturnProductDiscount > 0) ? "<br><small>(" . $item_disc_string . ")</small>" : "";
            }
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID);

            $unitPrice = $this->getProductUnitPriceViaDisplayUom($product->salesReturnProductPrice, $productUom) / $customCurrencyRate;
            $productQtyDetails = $this->getProductQuantityViaDisplayUom($product->salesReturnProductQuantity, $productUom);
            $productQtyDetails['quantity'] = ($productQtyDetails['quantity'] == 0) ? '-' : $productQtyDetails['quantity'];

            $records[] = array(
                $product->productCode,
                $product->productName . $sub_line,
                $productQtyDetails['quantity'] . ' ' . $productQtyDetails['uomAbbr'],
                number_format($unitPrice, 2),
                number_format($product->salesReturnProductTotal / $customCurrencyRate, 2),
            );
            $sub_total+= $product->salesReturnProductTotal / $customCurrencyRate;
        }

        foreach ($txTypes as $t_name => $t_val) {
            $tx_line.=($tx_line == null) ? $t_name . ":&nbsp; " . $data['currencySymbol'] . " " . number_format($t_val, 2) :
                    ",&nbsp;" . $t_name . ":&nbsp; " . $data['currencySymbol'] . " " . number_format($t_val, 2);
        }
        $data['deliveryNoteCode'] = $product->deliveryNoteCode;
        $data['type'] = _("Sales Order");
        $data['cust_name'] = '';
        $data['state'] = $returnDetails->statusID;
        $data['doc_data'] = array(
            _("Date") => $returnDetails->salesReturnDate,
            _("Valid till") => '',
            _("Payment terms") => '',
            _("Delivery Note No.") => '',
            _("Sales Return No.") => $returnDetails->salesReturnCode,
        );
        $data['cust_data'] = array(
            _("address") => '',
        );
        $data['table'] = array(
            'col_size' => array(10, 90, 290, 50, 100, 100),
            'col_allign' => array("left", "left", "left", "right", "right"),
            'headers' => array(
                _("Item code") => 2,
                _("Item Name") => 1,
                _("return qty") => 1,
                _("price</br>" . "(" . $data['currencySymbol'] . ")") => 1,
                _("total</br>" . "(" . $data['currencySymbol'] . ")") => 1
            ),
            'records' => $records,
        );
        $data['comment'] = $returnDetails->salesReturnComment;
        $data['sub_total'] = $sub_total;
        $data['total'] = $returnDetails->salesReturnTotal / $customCurrencyRate;
        $data['discount'] = '';
        $data['discountStatement'] = '';
        $data['show_tax'] = true;
        $data['tax_record'] = $tx_line;
        $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];


        return $this->_returnViewData = $data;
    }

    public function documentAction()
    {
        $documentType = 'Sales Returns';
        $salesReturnID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        echo $this->generateDocument($salesReturnID, $documentType, $templateID);
        exit;
    }

    public function getDocumentDataTable($salesReturnID, $documentSize = 'A4')
    {

        $data_table_vars = $this->getDataForDocument($salesReturnID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/return/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    private function getPaginatedReturns()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('SalesReturnsTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function documentPdfAction()
    {
        $quotationID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Sales Returns';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/invoice/return/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($quotationID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($quotationID, $documentType, $documentData, $templateID);

        return;
    }

}

/////////////////// END OF RETURN CONTROLLER \\\\\\\\\\\\\\\\\\\\
