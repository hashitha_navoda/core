<?php

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Core\Controller\Interfaces\DocumentInterface;
use Core\Service\ReferenceService;
use Invoice\Form\AddCustomerForm;
use Inventory\Form\ProductForm;
use Inventory\Form\CategoryForm;
use Inventory\Form\ProductHandelingForm;
use Invoice\Form\QuotationForm;
use Invoice\Form\CustomerOrderForm;
use Invoice\Model\Reference;
use Zend\I18n\Translator\Translator;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;

class CustomerOrderController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'customer_order_upper_menu';
    private $_quotationViewData;
    private $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID("Inventory\Controller\Product", "index", $this->user_session->userID);

        $isCanCreateItem = $isEnable;
        $this->getSideAndUpperMenus('Customer Orders', 'Create Customer Order', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];

        //get reference number for quotation from reference service
        $referenceService = $this->getService('ReferenceService');
        // document type 1 = quotation
        $reference = $referenceService->getReferenceNumber(47, $locationID);
        $refId = $reference['data']['referenceNo'];
        $locRefId = $reference['data']['locationReferenceID'];

        $paymentTermsResultSet = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $paymentTerms = array();
        foreach ($paymentTermsResultSet as $row) {
            $paymentTerms[$row->paymentTermID] = $row->paymentTermName;
        }

        $currency = $this->getCurrencyListWithRates();
        $priceList = $this->getActivePriceList();

        // Get sales persons list
        $salesPersonService = $this->getService('SalesPersonService');
        $salesPersons = $salesPersonService->getSalesPersons()['data']['salesPersons'];

        //get job card model
        $config = $this->getService('config');
        $jobCardModel = $config['jobCardModel'];

        $currencyValue = $this->getDefaultCurrency();
        $quot_form = CustomerOrderForm::getCustomerOrderForm(
                                        $paymentTerms,
                                        $salesPersons, 
                                        $jobCardModel, 
                                        $currency, 
                                        $priceList,
                                        $refId,
                                        $currencyValue
                                    );
      
        $this->getService('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);

        //taking the user selected date format from the database
        $userdateFormat = $this->getUserDateFormat(); 

         // Get sales persons list
        $currencyService = $this->getService('CurrencyService');
        $currencies = $currencyService->getAllCurrency()['data']['currencies'];

        // get person Title (Mr, Miss ..) from personTitle.config file
        $customerTitle = $config['personTitle'];

        //get countries
        $countries = ['' => ''] + $config['all_countries'];

        //get customer Code
        $custReference = $referenceService->getReferenceNumber(20, $locationID);
        $customerCode = $custReference['data']['referenceNo'];

        //get Active Customer Evnt for customer creation form
        $customerService = $this->getService('CustomerService');
        $customerEventList = $customerService->getActiveCustomerEvents()['data']['customerEventList'];
      

        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

         //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $customerForm = new AddCustomerForm(NULL, $paymentTerms, $currencies, $customerTitle, $currencyValue,$customerCategories, NULL, NULL, $customerEventList, $customerPriceLists);
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);
        $customerForm->get('customerFormsubmit')->setValue(_("Add New Customer"));
        
        $customerOrder = new ViewModel(array(
            'quot_form' => $quot_form,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'userdateformat' => $userdateFormat,
            'isCanCreateItem' => $isCanCreateItem,
            'locationID' => $locationID
        ));

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' => $this->useAccounting,
        ));

        $customerAddView->setTemplate('invoice/customer/customer-add-modal');

        $customerOrder->addChild($customerAddView, 'customerAddView');
        
        //if reference id is not set, then load a modal with message to add reference to that document.
        if ($refId == null) {

            $title = 'Customer Order Reference Number not set';
            $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            
            if ($locRefId != null) {
                $title = 'Customer Order Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference',
            ));
            
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $customerOrder->addChild($refNotSet, 'refNotSet');
        }
        
        return $customerOrder;
    }

    public function listAction()
    {

        $this->getSideAndUpperMenus('Customer Orders', 'View Customer Orders', 'SALES');
        $this->getPaginatedCustomerOrders();
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();

        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }

        $quot_form = new QuotationForm(array(
            'name' => 'quatation',
            'id' => 'qot_no',
            'terms' => $terms
        ));


        $location = $this->user_session->userActiveLocation["locationID"];
        $sales_order = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll($location, false);
        $salesorder = array();
        foreach ($sales_order as $sa) {
            if ($sa['quotationID'] != '') {
                $salesorder[$sa['quotationID']] = $sa['quotationID'];
            }
        }
        // get quotationIDs of copyied product
        $copiedQuotations = $this->CommonTable('Invoice\Model\QuotationTable')->getCopiedQuotationProductDetails($location);
        foreach ($copiedQuotations as $copiedQuotation) {
            if ($copiedQuotation['quotationProductCopied'] == '1') {
                $copiedQuoationList[$copiedQuotation['quotationID']] = $copiedQuotation['quotationID'];
            }
        }

        $userdateFormat = $this->getUserDateFormat();
        $quotation = new ViewModel(array(
            'quot_form' => $quot_form,
            'customerOrderList' => $this->paginator,
            'salesOrder' => $salesorder,
            'copiedQuotations' => $copiedQuoationList,
            'statuses' => $this->getStatusesList(),
            'cur' => $this->companyCurrencySymbol,
            'userdateFormat' => $userdateFormat,
            'locationID' => $location,
            'isPaginated' => true
        ));

        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('invoice/quotation/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/quotation/document-view');
        $quotation->addChild($docHistoryView, 'docHistoryView');
        $quotation->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $quotation->addChild($attachmentsView, 'attachmentsView');

        return $quotation;
    }


    public function EditAction()
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID("Inventory\Controller\Product", "index", $this->user_session->userID);

        $isCanCreateItem = $isEnable;
        $this->getSideAndUpperMenus('Customer Orders', 'View Customer Orders', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $customerOrderID = $this->params()->fromRoute('param1');

        //get reference number for quotation from reference service
        $referenceService = $this->getService('ReferenceService');
        // document type 1 = quotation
        $reference = $referenceService->getReferenceNumber(47, $locationID);
        $refId = $reference['data']['referenceNo'];
        $locRefId = $reference['data']['locationReferenceID'];


        $currency = $this->getCurrencyListWithRates();
        $priceList = $this->getActivePriceList();

        // Get sales persons list
        $salesPersonService = $this->getService('SalesPersonService');
        $salesPersons = $salesPersonService->getSalesPersons()['data']['salesPersons'];

        //get job card model
        $config = $this->getService('config');
        $jobCardModel = $config['jobCardModel'];

        $currencyValue = $this->getDefaultCurrency();
        $quot_form = CustomerOrderForm::getCustomerOrderForm(
                                        $paymentTerms,
                                        $salesPersons, 
                                        $jobCardModel, 
                                        $currency, 
                                        $priceList,
                                        $refId,
                                        $currencyValue
                                    );
      
        $this->getService('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);

        //taking the user selected date format from the database
        $userdateFormat = $this->getUserDateFormat(); 

         // Get sales persons list
        $currencyService = $this->getService('CurrencyService');
        $currencies = $currencyService->getAllCurrency()['data']['currencies'];

        // get person Title (Mr, Miss ..) from personTitle.config file
        $customerTitle = $config['personTitle'];

        //get countries
        $countries = ['' => ''] + $config['all_countries'];

        //get customer Code
        $custReference = $referenceService->getReferenceNumber(20, $locationID);
        $customerCode = $custReference['data']['referenceNo'];

        //get Active Customer Evnt for customer creation form
        $customerService = $this->getService('CustomerService');
        $customerEventList = $customerService->getActiveCustomerEvents()['data']['customerEventList'];
      

        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

         //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $customerForm = new AddCustomerForm(NULL, $paymentTerms, $currencies, $customerTitle, $currencyValue,$customerCategories, NULL, NULL, $customerEventList, $customerPriceLists);
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);
        
        $customerOrder = new ViewModel(array(
            'quot_form' => $quot_form,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'userdateformat' => $userdateFormat,
            'isCanCreateItem' => $isCanCreateItem,
            'locationID' => $locationID,
            'edit_ID' => $customerOrderID,
            'is_View' => false
        ));

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' => $this->useAccounting,
        ));

        $customerAddView->setTemplate('invoice/customer/customer-add-modal');

        $customerOrder->addChild($customerAddView, 'customerAddView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $customerOrder->addChild($attachmentsView, 'attachmentsView');
        
        //if reference id is not set, then load a modal with message to add reference to that document.
        if ($refId == null) {

            $title = 'Customer Order Reference Number not set';
            $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            
            if ($locRefId != null) {
                $title = 'Customer Order Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference',
            ));
            
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $customerOrder->addChild($refNotSet, 'refNotSet');
        }
        
        return $customerOrder;
    }

    public function ViewAction()
    {
        $isEnable = $this->CommonTable('User\Model\RoleFeatureTable')->getFeatureEnablityByUserID("Inventory\Controller\Product", "index", $this->user_session->userID);

        $isCanCreateItem = $isEnable;
        $this->getSideAndUpperMenus('Customer Orders', 'View Customer Orders', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $customerOrderID = $this->params()->fromRoute('param1');

        //get reference number for quotation from reference service
        $referenceService = $this->getService('ReferenceService');
        // document type 1 = quotation
        $reference = $referenceService->getReferenceNumber(47, $locationID);
        $refId = $reference['data']['referenceNo'];
        $locRefId = $reference['data']['locationReferenceID'];


        $currency = $this->getCurrencyListWithRates();
        $priceList = $this->getActivePriceList();

        // Get sales persons list
        $salesPersonService = $this->getService('SalesPersonService');
        $salesPersons = $salesPersonService->getSalesPersons()['data']['salesPersons'];

        //get job card model
        $config = $this->getService('config');
        $jobCardModel = $config['jobCardModel'];

        $currencyValue = $this->getDefaultCurrency();
        $quot_form = CustomerOrderForm::getCustomerOrderForm(
                                        $paymentTerms,
                                        $salesPersons, 
                                        $jobCardModel, 
                                        $currency, 
                                        $priceList,
                                        $refId,
                                        $currencyValue
                                    );
      
        $this->getService('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);

        //taking the user selected date format from the database
        $userdateFormat = $this->getUserDateFormat(); 

         // Get sales persons list
        $currencyService = $this->getService('CurrencyService');
        $currencies = $currencyService->getAllCurrency()['data']['currencies'];

        // get person Title (Mr, Miss ..) from personTitle.config file
        $customerTitle = $config['personTitle'];

        //get countries
        $countries = ['' => ''] + $config['all_countries'];

        //get customer Code
        $custReference = $referenceService->getReferenceNumber(20, $locationID);
        $customerCode = $custReference['data']['referenceNo'];

        //get Active Customer Evnt for customer creation form
        $customerService = $this->getService('CustomerService');
        $customerEventList = $customerService->getActiveCustomerEvents()['data']['customerEventList'];
      

        $customerCategories[''] = '';
        $existingCustomerCategories = $this->CommonTable('Invoice\Model\CustomerCategoryTable')->fetchAllForDorpDown();
        $customerCategories = $customerCategories + $existingCustomerCategories;

         //price list
        $customerPriceLists[''] = '';
        $existingCustomerPriceLists = $this->CommonTable('Inventory\Model\PriceListTable')->fetchAllForDorpDown();
        $customerPriceLists = $customerPriceLists + $existingCustomerPriceLists;

        $customerForm = new AddCustomerForm(NULL, $paymentTerms, $currencies, $customerTitle, $currencyValue,$customerCategories, NULL, NULL, $customerEventList, $customerPriceLists);
        $customerForm->get('customerCode')->setAttribute('value', $customerCode);
        
        $customerOrder = new ViewModel(array(
            'quot_form' => $quot_form,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'userdateformat' => $userdateFormat,
            'isCanCreateItem' => $isCanCreateItem,
            'locationID' => $locationID,
            'edit_ID' => $customerOrderID,
            'is_View' => true
        ));

        $proType = $this->CommonTable('Core\Model\ProductTypeTable')->fetchAll();
        foreach ($proType as $pType) {
            $productType[$pType->productTypeID] = $pType->productTypeName;
        }

        $financeAccountsArray = $this->getFinanceAccounts();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        if ($this->useAccounting == 1) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID != ''){
                $customerReceviableAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerReceivableAccountID;
                $customerReceviableAccountName = $financeAccountsArray[$customerReceviableAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerReceviableAccountID]['financeAccountsName'];
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-id',$customerReceviableAccountID);
                $customerForm->get('customerReceviableAccountID')->setAttribute('data-value',$customerReceviableAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID != ''){
                $customerSalesAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesAccountID;
                $customerSalesAccountName = $financeAccountsArray[$customerSalesAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesAccountID')->setAttribute('data-id',$customerSalesAccountID);
                $customerForm->get('customerSalesAccountID')->setAttribute('data-value',$customerSalesAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID != ''){
                $customerSalesDiscountAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerSalesDiscountAccountID;
                $customerSalesDiscountAccountName = $financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerSalesDiscountAccountID]['financeAccountsName'];
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-id',$customerSalesDiscountAccountID);
                $customerForm->get('customerSalesDiscountAccountID')->setAttribute('data-value',$customerSalesDiscountAccountName);
            }

            if($glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID != ''){
                $customerAdvancePaymentAccountID = $glAccountSetupData->glAccountSetupSalesAndCustomerAdvancePaymentAccountID;
                $customerAdvancePaymentAccountName = $financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsCode']."_".$financeAccountsArray[$customerAdvancePaymentAccountID]['financeAccountsName'];
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-id',$customerAdvancePaymentAccountID);
                $customerForm->get('customerAdvancePaymentAccountID')->setAttribute('data-value',$customerAdvancePaymentAccountName);
            }
        }

        $customerAddView = new ViewModel(array(
            'customerForm' => $customerForm,
            'countries' => $countries,
            'useAccounting' => $this->useAccounting,
        ));

        $customerAddView->setTemplate('invoice/customer/customer-add-modal');
        $customerOrder->setTemplate('invoice/customer-order/edit');

        $customerOrder->addChild($customerAddView, 'customerAddView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $customerOrder->addChild($attachmentsView, 'attachmentsView');
        
        //if reference id is not set, then load a modal with message to add reference to that document.
        if ($refId == null) {

            $title = 'Customer Order Reference Number not set';
            $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            
            if ($locRefId != null) {
                $title = 'Customer Order Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference',
            ));
            
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $customerOrder->addChild($refNotSet, 'refNotSet');
        }
        
        return $customerOrder;
    }

    
    private function getPaginatedCustomerOrders()
    {
        $location = $this->user_session->userActiveLocation["locationID"];
        $this->paginator = $this->CommonTable('Invoice\Model\CustomerOrderTable')->fetchAll($location, $paginated = true);
        $this->paginator->setCurrentPageNumber($this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

}

/////////////////// END OF IMVOICE CONTROLLER \\\\\\\\\\\\\\\\\\\\
