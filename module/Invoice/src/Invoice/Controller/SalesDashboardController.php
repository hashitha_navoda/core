<?php

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;


class SalesDashboardController extends CoreController
{

	protected $sideMenus = 'invoice_side_menu';
    protected $downMenus = 'invoice_upper_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('Sales Dashboard', null, 'SALES');
        
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/sales-dashboard.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/sales-dashboard.js');
    }
}
