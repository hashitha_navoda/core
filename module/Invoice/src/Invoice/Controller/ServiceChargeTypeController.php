<?php

namespace Invoice\Controller;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;

class ServiceChargeTypeController extends CoreController
{

    protected $sideMenus = 'company_side_menu';
    protected $upperMenus = 'settings_products_upper_menu';

    public function indexAction()
    {

        $this->getSideAndUpperMenus('Product Setup', 'Service Charge Type');
        $this->useAccounting = $this->user_session->useAccounting;
        
        $srcTypeList = $this->getPaginatedSRCTypes();
        $view = new ViewModel(array(
            'srcTypeList' => $srcTypeList,
            'paginated' => true,
            
        ));

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/service-charge-type.js');
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/inventory/inventory.css');

        return $view;
    }

    public function getPaginatedSRCTypes($perPage = 6, $fromSearch = false)
    {
        $this->paginator = $this->CommonTable('Invoice\Model\ServiceChargeTypeTable')->fetchAll(true);
        if($fromSearch) {
            $this->paginator->setCurrentPageNumber(1);
        } else {
            $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        }
        $this->paginator->setItemCountPerPage($perPage);
        
        return $this->paginator;
    }

}
