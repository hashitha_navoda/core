<?php

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;


class CrmDashboardController extends CoreController
{

	protected $sideMenus = 'crm_side_menu';
    protected $downMenus = 'invoice_upper_menu';

    public function indexAction()
    {
        $this->getSideAndUpperMenus('CRM Dashboard', null, 'CRM');
        
        $this->getViewHelper('HeadLink')->prependStylesheet('/css/crm-dashboard.css');
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/crm-dashboard.js');
    }
}
