<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file credit note payments related controller functions
 */

namespace Invoice\Controller;

use Invoice\Form\CustomerPaymentsForm;
use Invoice\Form\AddCustomerForm;
use Invoice\Form\AdvancePaymentsForm;
use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Model\Reference;
use Zend\Session\Container;


/**
 * This is the credit note Payments controller class
 */
class CreditNotePaymentsController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'credit_note_payments_upper_menu';
    protected $user_session;
    private $_creditNotePaymentViewData;
    private $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Credit Note Payments', 'Create Payments', 'SALES');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(17, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $creditNoteID = $this->params()->fromRoute('param1');

        $creditNote = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNoteID);

        $paymentterm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        while ($t = $paymentterm->current()) {
            $payterm[$t->paymentTermID] = $t->paymentTermName;
        }

        $paymentmethod = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentmethod as $key => $t) {
            if(!($t['paymentMethodID'] == 4 || $t['paymentMethodID'] == 6) && $t['paymentMethodInSales'] == 1){
                $paymethod[$t['paymentMethodID']] = $t['paymentMethodName'];
            }
        }

        $customer = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        while ($t = $customer->current()) {
            $t = (object) $t;
            $cust[$t->customerID] = $t->customerName;
        }
        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();


        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');

        /** create a viewModel with data
         */
        $index = new ViewModel(array(
            'creditNoteID' => $creditNoteID,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'referenceNumber' => $rid,
            'paymentTerm' => $payterm,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'customCurrency' => $currency,
        ));
        $index->addChild($dimensionAddView, 'dimensionAddView');
        $check = 0;
        if ($rid == '' || $rid == NULL) {
            $check = 1;
            if ($lrefID == null) {
                $title = 'Credit Note Payment Reference Number not set';
                $msg = $this->getMessage('ERR_CNPAYMENT_ADD_REF');
            } else {
                $title = 'Credit Note Payment Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_CNPAYMENT_CHANGE_REF');
            }
        }
        if ($check == 1) {
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $index->addChild($refNotSet, 'refNotSet');
        }
        $this->setLogMessage('Credit Note Payment view succsessfully accessed');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['location']);
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/creditNotePayment.js');
        return $index;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Credit Note Payments', 'View Payments', 'SALES');
        $this->getPaginatedCreditNotePayments();
        $payments = new ViewModel(array(
            'creditNotePayments' => $this->paginator,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'statuses' => $this->getStatusesList()
        ));
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('invoice/credit-note/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/credit-note/document-view');
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $payments->addChild($docHistoryView, 'docHistoryView');
        $payments->addChild($attachmentsView, 'attachmentsView');
        $payments->addChild($documentView, 'documentView');

        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/viewCreditNotePayments.js');
        return $payments;
    }

    public function getPaginatedCreditNotePayments()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function viewCreditNotePaymentReceiptAction()
    {
        return $this->documentPreviewAction();
    }

    public function documentPreviewAction()
    {
        $creditNotePaymentID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($creditNotePaymentID);
        $data['creditNotePaymentID'] = $data['creditNotePaymentID'];
        $data['total'] = number_format($data['total'], 2);
        $path = "/credit-note-payments/document/";
        $createNew = "New Credit Note Payment";
        $createPath = "/credit-note-payments/create";

        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Credit Note Payment from " . $data['companyName'],
            "body" => <<<EMAILBODY

Dear {$data['customerName']}, <br /><br />

Thank you for your inquiry. <br /><br />

A Credit Note Payment has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>CN Number:</strong> {$data['creditNotePaymentCode']} <br />
<strong>CN Amount:</strong> {$data['currencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $journalEntryValues = [];
        if($this->useAccounting == 1){
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('17',$creditNotePaymentID);
            $journalEntryID = $journalEntryData['journalEntryID'];
            $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryValues = $jeResult['JEDATA'];
        }

        $JEntry = new ViewModel(
            array(
               'journalEntry' => $journalEntryValues,
               'closeHidden' => true
               )
            );
        $JEntry->setTemplate('accounting/journal-entries/view-modal');
        $documentType = 'Credit Note Payment';
        $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $creditNotePaymentID, $createPath);
        $preview->addChild($JEntry, 'JEntry');

        $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
        $additionalButton->setTemplate('additionalButton');
        $preview->addChild($additionalButton, 'additionalButton');

        return $preview;
    }

    public function documentPdfAction()
    {
        $creditNotePaymentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Credit Note Payment';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/invoice/credit-note-payments/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($creditNotePaymentID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($creditNotePaymentID, $documentType, $documentData, $templateID);

        return;
    }

    public function getDataForDocument($creditNotePaymentID)
    {

        if (!empty($this->_creditNotePaymentViewData)) {
            return $this->_creditNotePaymentViewData;
        }

        $data = $this->getDataForDocumentView();

        $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentID($creditNotePaymentID, true)->current();
        $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($creditNotePaymentData['customerID']);

        $customCurrencyId = $creditNotePaymentData['customCurrencyId'];
        $creditNotePaymentData['createdTimeStamp'] =  $this->getUserDateTime($creditNotePaymentData['createdTimeStamp']);

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId == "" || $customCurrencyId == 0) ? 1 : $creditNotePaymentData['creditNotePaymentCustomCurrencyRate'];
        $customCurrencySymbol = ($customCurrencyId == "" || $customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->currencySymbol;
        $creditNotePaymentData['customCurrencySymbol'] = $customCurrencySymbol;

        $data = array_merge($data, $creditNotePaymentData, (array) $customerDetails);
        $data['currencySymbol'] = $customCurrencySymbol;
        // to be used by below functions - convert to object
        $creditNotePaymentData = (object) $creditNotePaymentData;

        $creditNotePaymentDetailsData = $this->CommonTable('Invoice\Model\CreditNotePaymentDetailsTable')->getCreditNotePaymentDetailsByCreditNotePaymentID($creditNotePaymentID);

        $paymentmethod = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
        foreach ($paymentmethod as $t) {
            $paymethod[$t['paymentMethodID']] = $t['paymentMethodName'];
        }

        $paymentterm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        foreach ($paymentterm as $t) {
            $payterm[$t->paymentTermID] = $t->paymentTermName;
        }


        $sub_total = 0;
        foreach ($creditNotePaymentDetailsData as $val) {

            $val = (object) $val;
            $creditNote = (object) $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($val->creditNoteID)->current();
            $cCrate = ($creditNote->creditNoteCustomCurrencyRate != 0) ? $creditNote->creditNoteCustomCurrencyRate : 1;
            $records[] = array(
                $creditNote->creditNoteCode,
                number_format(($creditNote->creditNotePaymentAmount / $cCrate), 2),
                number_format(($creditNote->creditNoteSettledAmount / $cCrate), 2),
                $paymethod[$val->paymentMethodID],
                number_format(($creditNote->creditNotePaymentAmount - $creditNote->creditNoteSettledAmount) / $cCrate, 2),
                number_format(($val->creditNotePaymentDetailsAmount / $cCrate), 2),
            );
            $sub_total+= ($val->creditNotePaymentDetailsAmount / $cCrate);
        }

        $data['type'] = _("Credit Note Payment");
        $data['creditNotePaymentTerm'] = $payterm[$creditNotePaymentData->paymentTermID];
        $data['state'] = $creditNotePaymentData->statusID;
        $data['doc_data'] = array(
            _("Date") => $creditNotePaymentData->creditNotePaymentDate,
            _("Payment terms") => $payterm[$creditNotePaymentData->paymentTermID],
            _("Credit Note Payment No") => $creditNotePaymentData->creditNotePaymentCode,
        );
        $data['cust_data'] = array(
            _("address") => '',
        );
        $data['table'] = array(
            'col_size' => array(10, 90, 290, 50, 100, 100),
            'col_allign' => array("left", "left", "left", "right", "right"),
            'headers' => array(
                _("Code") => 2,
                _("Amount </br>(" . $data['currencySymbol']) . ")" => 1,
                _("Total Settled Amount </br>(" . $data['currencySymbol']) . ")" => 1,
                _("Payment Method") => 1,
                _("Left To Pay </br>(" . $data['currencySymbol']) . ")" => 1,
                _("Payed amount </br>(" . $data['currencySymbol']) . ")" => 1
            ),
            'records' => $records,
        );
        $data['comment'] = $creditNotePaymentData->creditNotePaymentMemo;
        $data['sub_total'] = $sub_total;
        $data['total'] = ($creditNotePaymentData->creditNotePaymentAmount - $creditNotePaymentData->creditNotePaymentDiscount) / $customCurrencyRate;
        $data['discount'] = $creditNotePaymentData->creditNotePaymentDiscount / $customCurrencyRate;
        $data['discountStatement'] = '';
        $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];

        return $this->_creditNotePaymentViewData = $data;
    }

    public function documentAction()
    {
        $documentType = 'Credit Note Payment';
        $creditNotePaymentID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        echo $this->generateDocument($creditNotePaymentID, $documentType, $templateID);
        exit;
    }

    public function getDocumentDataTable($creditNotePaymentID, $documentSize = 'A4')
    {

        $data_table_vars = $this->getDataForDocument($creditNotePaymentID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/credit-note-payments/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

}
