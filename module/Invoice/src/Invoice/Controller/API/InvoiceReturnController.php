<?php

/**
 * @authora Ashan Madushka  <ashan@thinkcube.com>
 * This file contains Credit Note API related controller functions
 */

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Invoice\Model\CreditNote;
use Invoice\Model\Customer;
use Invoice\Model\CreditNoteProduct;
use Invoice\Model\CreditNoteProductTax;
use Invoice\Model\CreditNoteSubProduct;
use Invoice\Model\InvoiceReturn;
use Invoice\Model\InvoiceReturnProduct;
use Invoice\Model\InvoiceReturnSubProduct;
use Invoice\Model\InvoiceReturnProductTax;
use Invoice\Model\DraftInvoiceReturn;
use Invoice\Model\DraftInvoiceReturnProduct;
use Invoice\Model\DraftInvoiceReturnSubProduct;
use Invoice\Model\DraftInvoiceReturnProductTax;
use Zend\View\Model\JsonModel;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemOut;
use Inventory\Model\ProductBatch;
use Inventory\Model\ProductSerial;

class InvoiceReturnController extends CoreController
{

    protected $userID;
    protected $user_session;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function saveInvoiceReturnDetailsAction()
    {
       
        $request = $this->getRequest();
        if ($request->isPost()) {
            $creditNoteDirectType = $request->getPost('creditNoteDirectType');
            $creditNoteBuyBackType = $request->getPost('creditNoteBuyBackType');



            $customerID = $request->getPost('customerID');
            $customerName = $request->getPost('customerName');
            $customCurrencyId = $request->getPost('customCurrencyId');
            $dimensionData = $request->getPost('dimensionData');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $invoiceReturnCustomCurrencyRate = (float) $request->getPost('invoiceReturnCustomCurrencyRate');
            $inactiveFlag = false;
            $inactiveFlag =  filter_var($request->getPost('invFlag'), FILTER_VALIDATE_BOOLEAN);
            $invCloseFlag = filter_var($request->getPost('invCloseFlag'), FILTER_VALIDATE_BOOLEAN);
            $dupString = "DUP";
            $dupStringPreFix = false;
            $dupStringPostFix = true;
            $cdNotDirFlag = false;
            $cdNotBuyBackFlag = false;
            $isActiveWF = (int)filter_var($request->getPost('isActiveWF'), FILTER_VALIDATE_BOOLEAN);
            $wfID = $request->getPost('wfID');

            if ($isActiveWF == 1) {

                if (!empty($wfID)) {
                    $limitData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->getLimitsByWFId($wfID);
                    $limitID = null;


                    foreach ($limitData as $key => $value) {
                        if (floatval($request->getPost('invoiceReturnTotalPrice')) <= floatval($value['approvalWorkflowLimitMax']) && floatval($request->getPost('invoiceReturnTotalPrice')) >= floatval($value['approvalWorkflowLimitMin'])) {
                            $limitID = $value['approvalWorkflowLimitsID'];
                            break;
                        }
                    }

                    if ($limitID != null) {
                        $limitApproversData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->getLimitApproversByWFLimitId($limitID);
                        $approvers = [];

                        foreach ($limitApproversData as $key => $value) {
                            $approvers[] = $value['workflowLimitApprover'];
                        }

                        $customerID = ($request->getPost('customer')) ? $request->getPost('customer') : 0;
                        $param = $request->getPost();
                        $this->beginTransaction();

                        $saveInvRtnDraft = $this->saveInvRtnDraft($request->getPost());

                        if (!$saveInvRtnDraft['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $saveInvRtnDraft['msg'];
                            return $this->JSONRespond();
                        } else {
                            $this->commit();
                            $this->status = true;
                            $this->data = array('invoiceReturnID' => $saveInvRtnDraft['data']['invoiceReturnID'], 'approvers' => $approvers, 'hashValue' => $saveInvRtnDraft['data']['hashValue'], 'invoiceReturnCode' => $saveInvRtnDraft['data']['invoiceReturnCode'], 'isDraft' => true);
                            $this->msg = $this->getMessage('SUCC_INV_RTN_WF');
                            return $this->JSONRespond();
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('SELECTED_APPROVAL_WF_RANGES_MISMATCH_INV_RTN');
                        return $this->JSONRespond();
                    }
                }

            }

                        
            //get customCurrency data by customer CurrencyId
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

            $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
            
            if(!is_null($cust)){
                $products = $request->getPost('products');
                $invoiceID = $request->getPost('invoiceID');
                $locationID = $request->getPost('locationID');
                if(!$invoiceID && $creditNoteDirectType != 'true'){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE');
                    return $this->JSONRespond();
                }
             
                if ($invoiceID != "") {
                    $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                    if ($invoiceDetails['statusID'] == '10') {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE_EDITED');
                        return $this->JSONRespond();
                    }

                if ($invoiceDetails['statusID'] == '5') {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE_CANCELED');
                        return $this->JSONRespond();
                    }
                }
                    
                if($creditNoteDirectType == 'true'){
                    $cdNotDirFlag = true;
                }
                if($creditNoteDirectType == 'true' && $invoiceID == ''){
                    $invoiceID = 0;
                    
                } else if($creditNoteDirectType == 'true' && $invoiceID != ''){
                    //validate customerID and invoiceID
                    $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                    if($invoiceDetails['customerID'] != $customerID){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DIR_CUSTOMER_NOT_MATCH');
                        return $this->JSONRespond();
                    } else if($invoiceDetails['locationID'] != $locationID){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DIR_LOC_NOT_MATCH');
                        return $this->JSONRespond();
                    }
                    
                }
                $totalPrice = (float) trim($request->getPost('invoiceReturnTotalPrice')) * $invoiceReturnCustomCurrencyRate;


                
                if($creditNoteDirectType == 'true' && $invoiceID != ''){
                    $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                    $invoiceTotalAmount = $invoiceDetails['salesinvoiceTotalAmount'];

                    $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceID($invoiceID)->current();

                    $creditNoteAmount = 0;
                    if (!is_null($creditNoteTotalAmountForTheInvoice['creditNoteTot'])) {
                        $creditNoteAmount = $creditNoteTotalAmountForTheInvoice['creditNoteTot'];
                    }

                    $restAmountOfInvoice = $invoiceTotalAmount - $creditNoteAmount;

                    if ($totalPrice > $restAmountOfInvoice) {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DIR_TOTAL_AMOUNT');
                        return $this->JSONRespond();
                    }

                }
                
                $subProducts = $request->getPost('subProducts');
                $invoiceReturnCode = $request->getPost('invoiceReturnCode');
                $date = $this->convertDateToStandardFormat($request->getPost('date'));
                $comment = $request->getPost('creditNoteComment');
                $paymentTermID = $request->getPost('paymentTermID');
                $invoiceReturnDiscountType = $request->getPost('invoiceReturnDiscountType');
                $invoiceReturnTotalDiscount = $request->getPost('invoiceReturnTotalDiscount');
                $promotionID = $request->getPost('promotionID');
                $invoiceReturnPromotionDiscount = $request->getPost('invoiceReturnPromotionDiscount');
                $creditNotePaymentEligible = 0;
                $creditBalance = 0;
                $oustandingBalance = 0;
                $this->beginTransaction();

                $result = $this->getReferenceNoForLocation('49', $locationID);
                $locationReferenceID = $result['locRefID'];
                //check credit note numer already exist in credit note table if exist give next number for credit note code
                while ($invoiceReturnCode) {
                    if ($this->CommonTable('Invoice\Model\InvoiceReturnTable')->checkInvoiceReturnByCode($invoiceReturnCode)->current()) {
                        if ($locationReferenceID) {
                            $newInvoiceReturnCode = $this->getReferenceNumber($locationReferenceID);
                            if ($newInvoiceReturnCode == $invoiceReturnCode) {
                                $this->updateReferenceNumber($locationReferenceID);
                                $invoiceReturnCode = $this->getReferenceNumber($locationReferenceID);
                            } else {
                                $invoiceReturnCode = $newInvoiceReturnCode;
                            }
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_CREDIT_CODE_EXIST');
                            return $this->JSONRespond();
                        }
                    } else {
                        break;
                    }
                }


                $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
                $displayData = $result->current();
                $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
                $averageCostingFlag = $displayData['averageCostingFlag'];

                if(empty($FIFOCostingFlag) && empty($averageCostingFlag)){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE');
                    return $this->JSONRespond();
                }

                $accountProduct = array();
                $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
                $glAccountsData = $glAccountExist->current();
                $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

                $custSalesAccountIsset = true;
                if($this->useAccounting == 1){
                    if(empty($customerData->customerSalesAccountID)){
                        $custSalesAccountIsset = false;
                    }else if(empty($customerData->customerReceviableAccountID)){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode));
                        return $this->JSONRespond();
                    }
                }


                $creditDeviation = 0;
                $outstandingDeviation = 0;
                //no need to process invoice details for direct credit notes
                if($creditNoteDirectType == 'true'){
                    if ($invoiceID == 0) {
                        $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                        $customerCurrentBalance = $cust->customerCurrentBalance;
                    } else {
                        if($cust->customerCurrentBalance == 0){
                            $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                            $customerCurrentBalance = $cust->customerCurrentBalance;
                            $creditDeviation = $totalPrice;
                        } else if($cust->customerCurrentBalance - $totalPrice < 0){
                            $customerCurrentBalance = 0;
                            $customerCreditBalance = $cust->customerCurrentCredit + ($totalPrice - $cust->customerCurrentBalance);
                            $creditDeviation = $totalPrice - $cust->customerCurrentBalance;
                            $outstandingDeviation = -($cust->customerCurrentBalance);
                        } else {
                            $customerCurrentBalance = $cust->customerCurrentBalance - $totalPrice;
                            $customerCreditBalance = $cust->customerCurrentCredit;
                            $outstandingDeviation = -$totalPrice;
                        }
                    }                        

                    $custData = array(
                        'customerID' => $customerID,
                        'customerCurrentCredit' => $customerCreditBalance,
                        'customerCurrentBalance' => $customerCurrentBalance,
                    );
                } else {
                    $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                    $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceID($invoiceID);
                    $pastCreditNoteTotal = 0;
                    foreach ($creditNotes as $values) {
                        $value = (object) $values;
                        $pastCreditNoteTotal+=$value->creditNoteTotal * $invoiceReturnCustomCurrencyRate;
                    }
                    $invoicePayedAmount = $invoice->salesInvoicePayedAmount * $invoiceReturnCustomCurrencyRate;
                    $invoiceTotalAmount = $invoice->salesinvoiceTotalAmount * $invoiceReturnCustomCurrencyRate;
                    $ammountDifferent = $invoiceTotalAmount - $invoicePayedAmount - $pastCreditNoteTotal;

                    if ($ammountDifferent < $totalPrice) {
                        $creditNotePaymentEligible = 1;
                        if ($ammountDifferent < 0) {
                            $creditBalance = $totalPrice;
                            $creditDeviation = $totalPrice;
                            $outstandingDeviation = 0;
                        } else {
                            $creditBalance = $totalPrice - $ammountDifferent;
                            $oustandingBalance = $ammountDifferent;
                            $creditDeviation = $totalPrice - $ammountDifferent;
                            $outstandingDeviation = -$ammountDifferent;
                        }
                        // $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceStatusID($invoiceID, 4);
                    } else {
                        $oustandingBalance = $totalPrice;
                        $creditDeviation = 0;
                        $outstandingDeviation = $totalPrice;
                    }
                    $invoiceProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getUnCopiedSalesOrderProducts($invoiceID);
                    if(empty($invoiceProducts)){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_INVOICE_ALL_PRO_CREDITED');
                        return $this->JSONRespond();
                    }

                    $customerCreditBalance = $cust->customerCurrentCredit + $creditBalance;
                    $customerCurrentBalance = $cust->customerCurrentBalance - $oustandingBalance;
                    $custData = array(
                        'customerID' => $customerID,
                        'customerCurrentCredit' => $customerCreditBalance,
                        'customerCurrentBalance' => $customerCurrentBalance,
                    );
                }
               
                $preCreditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceIDForSPCalculation($invoiceID);
                // $customer = new Customer;
                // $customer->exchangeArray($custData);
                // $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCurrentAndCreditBalance($customer);

                $entityID = $this->createEntity();
                $invoiceReturnData = array(
                    'invoiceReturnCode' => $invoiceReturnCode,
                    'customerID' => $customerID,
                    'locationID' => $locationID,
                    'paymentTermID' => $paymentTermID,
                    'invoiceID' => $invoiceID,
                    'invoiceReturnDate' => $date,
                    'invoiceReturnTotal' => $totalPrice,
                    'invoiceReturnComment' => $comment,
                    'statusID' => 3,
                    'customCurrencyId' => $customCurrencyId,
                    'invoiceReturnCustomCurrencyRate' => $invoiceReturnCustomCurrencyRate,
                    'invoiceReturnDiscountType' => $invoiceReturnDiscountType,
                    'invoiceReturnTotalDiscount' => $invoiceReturnTotalDiscount,
                    'promotionID' => $promotionID,
                    'invoiceReturnPromotionDiscount' => $invoiceReturnPromotionDiscount,
                    'entityID' => $entityID,
                    'isApproved' => true
                );


                $invoiceReturn = new InvoiceReturn;
                $invoiceReturn->exchangeArray($invoiceReturnData);
                $invoiceReturnID = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->saveInvoiceReturn($invoiceReturn);
                // var_dump($invoiceReturnID).die();

                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }
                //check and update the invoice remainin discount amount
                $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
            
                $totalCRAmounts = 0;
                $totalProductAmounts = 0;
                $totalTaxAmount = 0;
                foreach ($products as $keyt => $pt) {
                    if(isset($pt['pTax']['tTA'])){
                        $ptwTax = ($pt['productTotal'] - $pt['pTax']['tTA']); 
                        $totalTaxAmount += $pt['pTax']['tTA']; 
                    }else{
                        $ptwTax = $pt['productTotal'];
                    }
                    $totalProductAmounts += $ptwTax;
                }


                foreach ($products as $key => $p) {
                    $productDup = false;
                    $invoiceProductID = $p['invoiceproductID'];
                    
                    if($creditNoteDirectType == 'true'){
                        $invoiceProductID = 0;
                    }
                    $batchProducts = $subProducts[$invoiceProductID];
                    $productID = $p['productID'];
                    $oldProID = $p['productID'];
                    $documentTypeID = $p['documentTypeID'];
                    $documentID = $p['documentID'];
                    $loCreditNoteQty = $creditNoteQty = $p['invoiceReturnQuantity']['qty'];
                    $productType = $p['productType'];
                    if ($productType == 2) {
                        $loCreditNoteQty = 0;
                    }
                    
                    // check this is inactive or not
                    if($inactiveFlag){
                        // find correct product id for inactive items
                        $proCode = explode($dupString, $p['productCode']);
                        $realProCode = '';
                        
                        if($dupStringPreFix){
                            $realProCode = $proCode[1]; 
                        } else if($dupStringPostFix){
                            $realProCode = $proCode[0];
                        }
                        if($realProCode != $p['productCode']){
                            //get product details by real product code
                            $productDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductCode($realProCode);
                            $productID = $productDetails->productID;
                            $productDup = true;
                        }
                    }

                    $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationID);
                    $locationProductID = $locationProduct->locationProductID;
                    //get location Product Data for calculate avarage costing
                    $locationProductAverageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
                    $locationProductQuantity = $locationProduct->locationProductQuantity;
                    $locationProductLastItemInID = $locationProduct->locationProductLastItemInID;

                    if (!isset($locationProductID)) {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CREDIT_PROD_NOT_IN_LOCATION', array($p['productCode']));
                        return $this->JSONRespond();
                    }

                    $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

                    
                    if ($p['productDiscountType'] == 'value') {
                        $p['productDiscount'] = $p['productDiscount'] * $invoiceReturnCustomCurrencyRate;
                    }

                    $invoiceReturnProductData = array(
                        'invoiceReturnID' => $invoiceReturnID,
                        'invoiceProductID' => $invoiceProductID,
                        'productID' => $productID,
                        'invoiceReturnProductPrice' => $p['productPrice'] * $invoiceReturnCustomCurrencyRate,
                        'invoiceReturnProductDiscount' => $p['productDiscount'],
                        'invoiceReturnProductDiscountType' => $p['productDiscountType'],
                        'invoiceReturnProductTotal' => $p['productTotal'] * $invoiceReturnCustomCurrencyRate,
                        'invoiceReturnProductQuantity' => $creditNoteQty
                    );


                    //calculate discount value
                    $discountValue = 0;
                    if ($p['productDiscountType'] == "precentage") {
                        $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $invoiceReturnCustomCurrencyRate;
                    } else if ($p['productDiscountType'] == "value") {
                        $discountValue = $p['productDiscount'] * $invoiceReturnCustomCurrencyRate;
                    }

                    $invoiceReturnProduct = new InvoiceReturnProduct;
                    $invoiceReturnProduct->exchangeArray($invoiceReturnProductData);
                    $invoiceReturnProductID = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->saveInvoiceReturnProducts($invoiceReturnProduct);
                    if ($p['pTax']) {
                        foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                            $creditNoteProductTaxData = array(
                                'invoiceReturnProductID' => $invoiceReturnProductID,
                                'taxID' => $taxKey,
                                'invoiceReturnProductTaxPrecentage' => $productTax['tP'],
                                'invoiceReturnProductTaxAmount' => (float) $productTax['tA'] * $invoiceReturnCustomCurrencyRate
                            );
                            $creditNoteProductTax = new InvoiceReturnProductTax();
                            $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);

                            $this->CommonTable('Invoice\Model\InvoiceReturnProductTaxTable')->saveInvoiceReturnProductTax($creditNoteProductTax);
                        }
                    }
                    

                    if (count($batchProducts) > 0) {
                        foreach ($batchProducts as $batchKey => $batchValue) {
                            $newBatchValue = [];
                            $result = $this->saveInvoiceReturnSubProductData($batchValue, $invoiceReturnProductID);
                        }
                    } 
                }
                

                $this->commit();
                $this->status = true;
                $this->data = array('invoiceReturnID' => $invoiceReturnID);
                $this->msg = $this->getMessage('SUCC_CREDIT_PRODUCTDETAIL_ADD', array($invoiceReturnCode));
                // $this->setLogMessage($customCurrencySymbol . $p['productTotal'] . ' amount credit Note Product details were successfully added - ' . $invoiceReturnCode);
                $this->flashMessenger()->addMessage($this->msg);
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CREDIT_VALID_CUST');
                return $this->JSONRespond();
            }
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CREDIT_PRODUCT_SAVE');
            return $this->JSONRespond();
        }
    }


    public function sendApproverEmailAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invoiceReturnID = $request->getPost('invoiceReturnID');
            $invoiceReturnCode = $request->getPost('invoiceReturnCode');
            $approvers = $request->getPost('approvers');
            $token = $request->getPost('token');

            if ($invoiceReturnID && $invoiceReturnCode && $approvers && $token) {
                $this->sendDraftInvoiceReturnApproversEmail($invoiceReturnID, $invoiceReturnCode, $approvers, $token);
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_INV_SEND_FOR_WF');
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INV_SEND_FOR_WF');
                return $this->JSONRespond();
            }
        }
    }

    private function sendDraftInvoiceReturnApproversEmail($invoiceReturnID, $invoiceReturnCode, $approvers, $token)
    {   
        foreach ($approvers as $approver) {

            $approverData =  $this->CommonTable('Settings\Model\ApproverTable')->getApproverByID($approver)->current();
            $approverID = $approverData['approverID'];


            $approveUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/invoice-return-api/approve/?invoiceReturnID=' . $invoiceReturnID . '&action=approve&token=' . $token .'&approverID='.$approverID;
            $rejectUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/invoice-return-api/approve/?invoiceReturnID=' . $invoiceReturnID . '&action=reject&token=' . $token .'&approverID='.$approverID;;
            $approveGrnEmailView = new ViewModel(array(
                'name' => $approverData['firstName'] . ' ' . $approverData['lastName'],
                'invoiceReturnCode' => $invoiceReturnCode,
                'approveLink' => $approveUrl,
                'rejectLink' => $rejectUrl,
            ));
            $approveGrnEmailView->setTemplate('/core/email/inv-rtn-approve');
            $renderedEmailHtml = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($approveGrnEmailView);
            $documentType = 'Sales Invoice Return';
            $this->sendDocumentEmail($approverData['email'], "Request for Approve - Sales Invoice Return - $invoiceReturnCode", $renderedEmailHtml, $documentType, $invoiceReturnID, null, false, true);
        }
    }

    public function saveInvRtnDraft($data) {
        $creditNoteDirectType = $data['creditNoteDirectType'];
        $creditNoteBuyBackType = $data['creditNoteBuyBackType'];


        $result = $this->getReferenceNoForLocation('50', $locationOut);
        $locationReferenceID = $result['locRefID'];

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(50, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $invoiceReturnCode = $rid;

        $customerID = $data['customerID'];
        $customerName = $data['customerName'];
        $customCurrencyId = $data['customCurrencyId'];
        $dimensionData = $data['dimensionData'];
        $ignoreBudgetLimit = $data['ignoreBudgetLimit'];
        $wfID = $data['wfID'];
        $invoiceReturnCustomCurrencyRate = (float) $data['invoiceReturnCustomCurrencyRate'];
        $inactiveFlag = false;
        $inactiveFlag =  filter_var($data['invFlag'], FILTER_VALIDATE_BOOLEAN);
        $invCloseFlag = filter_var($data['invCloseFlag'], FILTER_VALIDATE_BOOLEAN);
        $dupString = "DUP";
        $dupStringPreFix = false;
        $dupStringPostFix = true;
        $cdNotDirFlag = false;
        $cdNotBuyBackFlag = false;
        

         
        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        
        if(!is_null($cust)){
            $products = $data['products'];
            $invoiceID = $data['invoiceID'];
            $locationID = $data['locationID'];
            if(!$invoiceID && $creditNoteDirectType != 'true'){
                
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CREDIT_NOTE_INVOICE'),
                );

            }
         
            if ($invoiceID != "") {
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                if ($invoiceDetails['statusID'] == '10') {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE_EDITED');
                    return $this->JSONRespond();
                }

            if ($invoiceDetails['statusID'] == '5') {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE_CANCELED');
                    return $this->JSONRespond();
                }
            }
                
            if($creditNoteDirectType == 'true'){
                $cdNotDirFlag = true;
            }
            if($creditNoteDirectType == 'true' && $invoiceID == ''){
                $invoiceID = 0;
                
            } else if($creditNoteDirectType == 'true' && $invoiceID != ''){
                //validate customerID and invoiceID
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                if($invoiceDetails['customerID'] != $customerID){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_DIR_CUSTOMER_NOT_MATCH');
                    return $this->JSONRespond();
                } else if($invoiceDetails['locationID'] != $locationID){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_DIR_LOC_NOT_MATCH');
                    return $this->JSONRespond();
                }
                
            }
            $totalPrice = (float) trim($data['invoiceReturnTotalPrice']) * $invoiceReturnCustomCurrencyRate;


            
            if($creditNoteDirectType == 'true' && $invoiceID != ''){
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                $invoiceTotalAmount = $invoiceDetails['salesinvoiceTotalAmount'];

                $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceID($invoiceID)->current();

                $creditNoteAmount = 0;
                if (!is_null($creditNoteTotalAmountForTheInvoice['creditNoteTot'])) {
                    $creditNoteAmount = $creditNoteTotalAmountForTheInvoice['creditNoteTot'];
                }

                $restAmountOfInvoice = $invoiceTotalAmount - $creditNoteAmount;

                if ($totalPrice > $restAmountOfInvoice) {
                    
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_DIR_TOTAL_AMOUNT'),
                    );
                }

            }
            
            $subProducts = $data['subProducts'];
            // $invoiceReturnCode = $data['invoiceReturnCode'];
            $date = $this->convertDateToStandardFormat($data['date']);
            $comment = $data['creditNoteComment'];
            $paymentTermID = $data['paymentTermID'];
            $invoiceReturnDiscountType = $data['invoiceReturnDiscountType'];
            $invoiceReturnTotalDiscount = $data['invoiceReturnTotalDiscount'];
            $promotionID = $data['promotionID'];
            $invoiceReturnPromotionDiscount = $data['invoiceReturnPromotionDiscount'];
            $creditNotePaymentEligible = 0;
            $creditBalance = 0;
            $oustandingBalance = 0;
            // $this->beginTransaction();

            // $result = $this->getReferenceNoForLocation('50', $locationID);
            // $locationReferenceID = $result['locRefID'];
            //check credit note numer already exist in credit note table if exist give next number for credit note code
            while ($invoiceReturnCode) {
                if ($this->CommonTable('Invoice\Model\InvoiceReturnTable')->checkInvoiceReturnByCode($invoiceReturnCode)->current()) {
                    if ($locationReferenceID) {
                        $newInvoiceReturnCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newInvoiceReturnCode == $invoiceReturnCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $invoiceReturnCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $invoiceReturnCode = $newInvoiceReturnCode;
                        }
                    } else {
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_CREDIT_CODE_EXIST'),
                        );

                    }
                } else {
                    break;
                }
            }


            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            if(empty($FIFOCostingFlag) && empty($averageCostingFlag)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE'),
                );
            }

            $accountProduct = array();
            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountsData = $glAccountExist->current();
            $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

            $custSalesAccountIsset = true;
            if($this->useAccounting == 1){
                if(empty($customerData->customerSalesAccountID)){
                    $custSalesAccountIsset = false;
                }else if(empty($customerData->customerReceviableAccountID)){
                  
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                    );

                }
            }


            $creditDeviation = 0;
            $outstandingDeviation = 0;
            //no need to process invoice details for direct credit notes
            if($creditNoteDirectType == 'true'){
                if ($invoiceID == 0) {
                    $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                    $customerCurrentBalance = $cust->customerCurrentBalance;
                } else {
                    if($cust->customerCurrentBalance == 0){
                        $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                        $customerCurrentBalance = $cust->customerCurrentBalance;
                        $creditDeviation = $totalPrice;
                    } else if($cust->customerCurrentBalance - $totalPrice < 0){
                        $customerCurrentBalance = 0;
                        $customerCreditBalance = $cust->customerCurrentCredit + ($totalPrice - $cust->customerCurrentBalance);
                        $creditDeviation = $totalPrice - $cust->customerCurrentBalance;
                        $outstandingDeviation = -($cust->customerCurrentBalance);
                    } else {
                        $customerCurrentBalance = $cust->customerCurrentBalance - $totalPrice;
                        $customerCreditBalance = $cust->customerCurrentCredit;
                        $outstandingDeviation = -$totalPrice;
                    }
                }                        

                $custData = array(
                    'customerID' => $customerID,
                    'customerCurrentCredit' => $customerCreditBalance,
                    'customerCurrentBalance' => $customerCurrentBalance,
                );
            } else {
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceID($invoiceID);
                $pastCreditNoteTotal = 0;
                foreach ($creditNotes as $values) {
                    $value = (object) $values;
                    $pastCreditNoteTotal+=$value->creditNoteTotal * $invoiceReturnCustomCurrencyRate;
                }
                $invoicePayedAmount = $invoice->salesInvoicePayedAmount * $invoiceReturnCustomCurrencyRate;
                $invoiceTotalAmount = $invoice->salesinvoiceTotalAmount * $invoiceReturnCustomCurrencyRate;
                $ammountDifferent = $invoiceTotalAmount - $invoicePayedAmount - $pastCreditNoteTotal;

                if ($ammountDifferent < $totalPrice) {
                    $creditNotePaymentEligible = 1;
                    if ($ammountDifferent < 0) {
                        $creditBalance = $totalPrice;
                        $creditDeviation = $totalPrice;
                        $outstandingDeviation = 0;
                    } else {
                        $creditBalance = $totalPrice - $ammountDifferent;
                        $oustandingBalance = $ammountDifferent;
                        $creditDeviation = $totalPrice - $ammountDifferent;
                        $outstandingDeviation = -$ammountDifferent;
                    }
                    // $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceStatusID($invoiceID, 4);
                } else {
                    $oustandingBalance = $totalPrice;
                    $creditDeviation = 0;
                    $outstandingDeviation = $totalPrice;
                }
                $invoiceProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getUnCopiedSalesOrderProducts($invoiceID);
                if(empty($invoiceProducts)){
                    
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_INVOICE_ALL_PRO_CREDITED'),
                    );
                }

                $customerCreditBalance = $cust->customerCurrentCredit + $creditBalance;
                $customerCurrentBalance = $cust->customerCurrentBalance - $oustandingBalance;
                $custData = array(
                    'customerID' => $customerID,
                    'customerCurrentCredit' => $customerCreditBalance,
                    'customerCurrentBalance' => $customerCurrentBalance,
                );
            }
           
            $preCreditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceIDForSPCalculation($invoiceID);
            // $customer = new Customer;
            // $customer->exchangeArray($custData);
            // $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCurrentAndCreditBalance($customer);
            $actInvData = json_encode($data);
            $hashValue = md5($invoiceReturnCode.'-'.$actInvData);

            $entityID = $this->createEntity();
            $invoiceReturnData = array(
                'invoiceReturnCode' => $invoiceReturnCode,
                'customerID' => $customerID,
                'locationID' => $locationID,
                'paymentTermID' => $paymentTermID,
                'invoiceID' => $invoiceID,
                'invoiceReturnDate' => $date,
                'invoiceReturnTotal' => $totalPrice,
                'invoiceReturnComment' => $comment,
                'statusID' => 3,
                'customCurrencyId' => $customCurrencyId,
                'invoiceReturnCustomCurrencyRate' => $invoiceReturnCustomCurrencyRate,
                'invoiceReturnDiscountType' => $invoiceReturnDiscountType,
                'invoiceReturnTotalDiscount' => $invoiceReturnTotalDiscount,
                'promotionID' => $promotionID,
                'invoiceReturnPromotionDiscount' => $invoiceReturnPromotionDiscount,
                'entityID' => $entityID,
                'invHashValue' => $hashValue,
                'actInvData' => $actInvData,
                'isInvApproved' => 0,
                'selectedWfID' => $wfID,
            );



            $invoiceReturn = new DraftInvoiceReturn;
            $invoiceReturn->exchangeArray($invoiceReturnData);
            $invoiceReturnID = $this->CommonTable('Invoice\Model\DraftInvoiceReturnTable')->saveInvoiceReturn($invoiceReturn);

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }
            //check and update the invoice remainin discount amount
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
        
            $totalCRAmounts = 0;
            $totalProductAmounts = 0;
            $totalTaxAmount = 0;
            foreach ($products as $keyt => $pt) {
                if(isset($pt['pTax']['tTA'])){
                    $ptwTax = ($pt['productTotal'] - $pt['pTax']['tTA']); 
                    $totalTaxAmount += $pt['pTax']['tTA']; 
                }else{
                    $ptwTax = $pt['productTotal'];
                }
                $totalProductAmounts += $ptwTax;
            }


            foreach ($products as $key => $p) {
                $productDup = false;
                $invoiceProductID = $p['invoiceproductID'];
                
                if($creditNoteDirectType == 'true'){
                    $invoiceProductID = 0;
                }
                $batchProducts = $subProducts[$invoiceProductID];
                $productID = $p['productID'];
                $oldProID = $p['productID'];
                $documentTypeID = $p['documentTypeID'];
                $documentID = $p['documentID'];
                $loCreditNoteQty = $creditNoteQty = $p['invoiceReturnQuantity']['qty'];
                $productType = $p['productType'];
                if ($productType == 2) {
                    $loCreditNoteQty = 0;
                }
                
                // check this is inactive or not
                if($inactiveFlag){
                    // find correct product id for inactive items
                    $proCode = explode($dupString, $p['productCode']);
                    $realProCode = '';
                    
                    if($dupStringPreFix){
                        $realProCode = $proCode[1]; 
                    } else if($dupStringPostFix){
                        $realProCode = $proCode[0];
                    }
                    if($realProCode != $p['productCode']){
                        //get product details by real product code
                        $productDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductCode($realProCode);
                        $productID = $productDetails->productID;
                        $productDup = true;
                    }
                }

                $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationID);
                $locationProductID = $locationProduct->locationProductID;
                //get location Product Data for calculate avarage costing
                $locationProductAverageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProduct->locationProductQuantity;
                $locationProductLastItemInID = $locationProduct->locationProductLastItemInID;

                if (!isset($locationProductID)) {

                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_CREDIT_PROD_NOT_IN_LOCATION', array($p['productCode'])),
                    );
                }

                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

                
                if ($p['productDiscountType'] == 'value') {
                    $p['productDiscount'] = $p['productDiscount'] * $invoiceReturnCustomCurrencyRate;
                }

                $invoiceReturnProductData = array(
                    'draftInvoiceReturnID' => $invoiceReturnID,
                    'invoiceProductID' => $invoiceProductID,
                    'productID' => $productID,
                    'invoiceReturnProductPrice' => $p['productPrice'] * $invoiceReturnCustomCurrencyRate,
                    'invoiceReturnProductDiscount' => $p['productDiscount'],
                    'invoiceReturnProductDiscountType' => $p['productDiscountType'],
                    'invoiceReturnProductTotal' => $p['productTotal'] * $invoiceReturnCustomCurrencyRate,
                    'invoiceReturnProductQuantity' => $creditNoteQty
                );


                //calculate discount value
                $discountValue = 0;
                if ($p['productDiscountType'] == "precentage") {
                    $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $invoiceReturnCustomCurrencyRate;
                } else if ($p['productDiscountType'] == "value") {
                    $discountValue = $p['productDiscount'] * $invoiceReturnCustomCurrencyRate;
                }

                $invoiceReturnProduct = new DraftInvoiceReturnProduct;
                $invoiceReturnProduct->exchangeArray($invoiceReturnProductData);
                $invoiceReturnProductID = $this->CommonTable('Invoice\Model\DraftInvoiceReturnProductTable')->saveInvoiceReturnProducts($invoiceReturnProduct);
                if ($p['pTax']) {
                    foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                        $creditNoteProductTaxData = array(
                            'draftInvoiceReturnProductID' => $invoiceReturnProductID,
                            'taxID' => $taxKey,
                            'invoiceReturnProductTaxPrecentage' => $productTax['tP'],
                            'invoiceReturnProductTaxAmount' => (float) $productTax['tA'] * $invoiceReturnCustomCurrencyRate
                        );
                        $creditNoteProductTax = new DraftInvoiceReturnProductTax();
                        $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);

                        $this->CommonTable('Invoice\Model\DraftInvoiceReturnProductTaxTable')->saveInvoiceReturnProductTax($creditNoteProductTax);
                    }
                }
                

                if (count($batchProducts) > 0) {
                    foreach ($batchProducts as $batchKey => $batchValue) {
                        $newBatchValue = [];
                        $result = $this->saveDraftInvoiceReturnSubProductData($batchValue, $invoiceReturnProductID);
                    }
                } 
            }
            

            return array(
                'status' => true,
                'msg' => $this->getMessage('SUCC_CREDIT_PRODUCTDETAIL_ADD', array($invoiceReturnCode)),
                'data' => array('invoiceReturnID' => $invoiceReturnID, 'invoiceReturnCode' => $invoiceReturnCode, 'hashValue' => $hashValue)
            );
        } else {
           
            return array(
                'status' => false,
                'msg' => $this->getMessage('ERR_CREDIT_VALID_CUST'),
            );
        }
    }


    public function approveAction()
    {
        $action = $_GET['action'];
        $invoiceReturnID = $_GET['invoiceReturnID'];
        $token = $_GET['token'];
        $approverID = $_GET['approverID'];
        $invRtnDetails = $this->CommonTable("Invoice\Model\DraftInvoiceReturnTable")->getInvoiceReturnByInvoiceReturnID($invoiceReturnID)->current();
        $invRtnDetails = (array) $invRtnDetails;

        $invoiceReturnCode = $invRtnDetails['invoiceReturnCode'];

        if ($invRtnDetails['deleted'] == 1) {
            echo "This link and this link related draft invoice $invoiceReturnCode is no more valid...!";
            exit();
        }


        $user = $invRtnDetails['createdBy'];
        $this->beginTransaction();
        if ($invRtnDetails['invHashValue'] == $token) {
            if ($action == 'approve') {
                if ($invRtnDetails['isInvApproved'] == 0 && $invRtnDetails['statusID'] == 13) {
                    $this->commit();
                    echo "Invoice Return $invoiceReturnCode is already rejected, you cannot approve any more..!";
                    exit();
                }

                if ($invRtnDetails['isInvApproved'] == 0) {

                    $data = json_decode($invRtnDetails['actInvData']);
                    $dataSet = (array) $data;

                    
                    $dataSet['products'] = (array) $dataSet['products'];

                    // $locationID = $this->user_session->userActiveLocation['locationID'];
                    $customerID = (!empty($dataSet['customerID'])) ? $dataSet['customerID'] : NULL;

                    $this->beginTransaction();
                    //default customer ID 0 is sent to the database
                    $refData = $this->getReferenceNoForLocation(49, $dataSet['locationID']);
                    $rid = $refData["refNo"];

                    $dataSet['invoiceReturnCode'] = $rid;


                    $res = $this->converDraftToInvoiceReturn($dataSet, 0, false, false, true, $user);
                    if ($res['status']) {
                       
                        //update draft inv status
                        $dataSet = [
                            'isInvApproved' => 1,
                            'approvedBy' => $approverID,
                            'approvedAt' => date('Y-m-d h:i:s'),
                            'statusID' => 16
                        ];
                        $this->CommonTable('Invoice\Model\DraftInvoiceReturnTable')->updateInvoiceReturn($dataSet, $invoiceReturnID);

                        //update act inv workflow state
                        $dataParam = [
                            'isApproved' => 1
                        ];
                        $this->CommonTable('Invoice\Model\InvoiceReturnTable')->updateInvoiceReturn($dataParam, $res['data']['invoiceReturnID']);

                        $this->commit();
                        echo "Invoice Return Approved..!";
                        exit();
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $res['msg'];

                        echo "Error occured while convert draft invoice return to original invoice the error is ". $res['msg'];
                        exit();
                        
                    }
                    
                } else {
                    $this->commit();
                    echo "Invoice Return $invoiceReturnCode is already approved..!";
                    exit();
                }
            } else {
                if ($invRtnDetails['isInvApproved'] == 1) {
                    $this->commit();
                    echo "Invoice Return $invoiceReturnCode is already approved, you cannot reject any more..!";
                    exit();
                }

                if ($invRtnDetails['isInvApproved'] == 0 && $invRtnDetails['status'] == 13) {
                    $this->commit();
                    echo "Invoice Return $invoiceReturnCode is already rejected..!";
                    exit();
                } else {
                    $draftData = [
                        'isInvApproved' => 0,
                        'statusID' => 13,
                        'rejectedBy' => $approverID,
                        'rejectedAt' => date('Y-m-d h:i:s')
                    ];

                    $draftRes = $this->CommonTable('Invoice\Model\DraftInvoiceReturnTable')->updateInvoiceReturn($draftData, $invoiceReturnID);

                    if (!$draftRes){
                        $this->rollback();
                        echo $this->getMessage('ERR_INV_RTN_DRAFT_TO_INV');
                        exit();
                    } else{
                        $this->commit();
                        echo "Sales Invoice return $invoiceReturnCode rejected..!";
                        exit();
                    }
                }
            }
        } else {
            $this->rollback();
            echo 'Token Mismatch';
            exit();
        }
    }


    public function converDraftToInvoiceReturn($data) {
        $creditNoteDirectType = false;
        $creditNoteBuyBackType = false;


        $customerID = $data['customerID'];
        $customerName = $data['customerName'];
        $customCurrencyId = $data['customCurrencyId'];
        $dimensionData = (!empty($data['dimensionData'])) ? $data['dimensionData'] : [];
        $ignoreBudgetLimit = $data['ignoreBudgetLimit'];
        $invoiceReturnCustomCurrencyRate = (float) $data['invoiceReturnCustomCurrencyRate'];
        $inactiveFlag = false;
        $inactiveFlag =  filter_var($data['invFlag'], FILTER_VALIDATE_BOOLEAN);
        $invCloseFlag = filter_var($data['invCloseFlag'], FILTER_VALIDATE_BOOLEAN);
        $dupString = "DUP";
        $dupStringPreFix = false;
        $dupStringPostFix = true;
        $cdNotDirFlag = false;
        $cdNotBuyBackFlag = false;
        $isActiveWF = (int)filter_var($data['isActiveWF'], FILTER_VALIDATE_BOOLEAN);
        $wfID = $data['wfID'];

                  
        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        
        if(!is_null($cust)){
            $products = $data['products'];
            $invoiceID = $data['invoiceID'];
            $locationID = $data['locationID'];
            if(!$invoiceID && $creditNoteDirectType != 'true'){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CREDIT_NOTE_INVOICE'),
                );
            }
         
            if ($invoiceID != "") {
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                if ($invoiceDetails['statusID'] == '10') {
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_CREDIT_NOTE_INVOICE_EDITED'),
                    );
                }

            if ($invoiceDetails['statusID'] == '5') {
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_CREDIT_NOTE_INVOICE_CANCELED'),
                    );
                }
            }
                
            if($creditNoteDirectType == 'true'){
                $cdNotDirFlag = true;
            }
            if($creditNoteDirectType == 'true' && $invoiceID == ''){
                $invoiceID = 0;
                
            } else if($creditNoteDirectType == 'true' && $invoiceID != ''){
                //validate customerID and invoiceID
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                if($invoiceDetails['customerID'] != $customerID){
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_DIR_CUSTOMER_NOT_MATCH'),
                    );
                } else if($invoiceDetails['locationID'] != $locationID){
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_DIR_LOC_NOT_MATCH'),
                    );
                }
                
            }
            $totalPrice = (float) trim($data['invoiceReturnTotalPrice']) * $invoiceReturnCustomCurrencyRate;


            
            if($creditNoteDirectType == 'true' && $invoiceID != ''){
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                $invoiceTotalAmount = $invoiceDetails['salesinvoiceTotalAmount'];

                $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceID($invoiceID)->current();

                $creditNoteAmount = 0;
                if (!is_null($creditNoteTotalAmountForTheInvoice['creditNoteTot'])) {
                    $creditNoteAmount = $creditNoteTotalAmountForTheInvoice['creditNoteTot'];
                }

                $restAmountOfInvoice = $invoiceTotalAmount - $creditNoteAmount;

                if ($totalPrice > $restAmountOfInvoice) {
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_DIR_TOTAL_AMOUNT'),
                    );
                }

            }

            $arr = [];
            foreach ($data['subProducts'] as $key => $value) {

                $arr[$key][] = $value;
            }
            
            $subProducts = $arr;
            $invoiceReturnCode = $data['invoiceReturnCode'];
            $date = $this->convertDateToStandardFormat($data['date']);
            $comment = $data['creditNoteComment'];
            $paymentTermID = $data['paymentTermID'];
            $invoiceReturnDiscountType = $data['invoiceReturnDiscountType'];
            $invoiceReturnTotalDiscount = $data['invoiceReturnTotalDiscount'];
            $promotionID = $data['promotionID'];
            $invoiceReturnPromotionDiscount = $data['invoiceReturnPromotionDiscount'];
            $creditNotePaymentEligible = 0;
            $creditBalance = 0;
            $oustandingBalance = 0;
            $this->beginTransaction();

            $result = $this->getReferenceNoForLocation('49', $locationID);
            $locationReferenceID = $result['locRefID'];
            //check credit note numer already exist in credit note table if exist give next number for credit note code
            while ($invoiceReturnCode) {
                if ($this->CommonTable('Invoice\Model\InvoiceReturnTable')->checkInvoiceReturnByCode($invoiceReturnCode)->current()) {
                    if ($locationReferenceID) {
                        $newInvoiceReturnCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newInvoiceReturnCode == $invoiceReturnCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $invoiceReturnCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $invoiceReturnCode = $newInvoiceReturnCode;
                        }
                    } else {
                        
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_CREDIT_CODE_EXIST'),
                        );
                    }
                } else {
                    break;
                }
            }


            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            if(empty($FIFOCostingFlag) && empty($averageCostingFlag)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE'),
                );
            }

            $accountProduct = array();
            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountsData = $glAccountExist->current();
            $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

            $custSalesAccountIsset = true;
            if($this->useAccounting == 1){
                if(empty($customerData->customerSalesAccountID)){
                    $custSalesAccountIsset = false;
                }else if(empty($customerData->customerReceviableAccountID)){
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                    );
                }
            }


            $creditDeviation = 0;
            $outstandingDeviation = 0;
            //no need to process invoice details for direct credit notes
            if($creditNoteDirectType == 'true'){
                if ($invoiceID == 0) {
                    $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                    $customerCurrentBalance = $cust->customerCurrentBalance;
                } else {
                    if($cust->customerCurrentBalance == 0){
                        $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                        $customerCurrentBalance = $cust->customerCurrentBalance;
                        $creditDeviation = $totalPrice;
                    } else if($cust->customerCurrentBalance - $totalPrice < 0){
                        $customerCurrentBalance = 0;
                        $customerCreditBalance = $cust->customerCurrentCredit + ($totalPrice - $cust->customerCurrentBalance);
                        $creditDeviation = $totalPrice - $cust->customerCurrentBalance;
                        $outstandingDeviation = -($cust->customerCurrentBalance);
                    } else {
                        $customerCurrentBalance = $cust->customerCurrentBalance - $totalPrice;
                        $customerCreditBalance = $cust->customerCurrentCredit;
                        $outstandingDeviation = -$totalPrice;
                    }
                }                        

                $custData = array(
                    'customerID' => $customerID,
                    'customerCurrentCredit' => $customerCreditBalance,
                    'customerCurrentBalance' => $customerCurrentBalance,
                );
            } else {
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceID($invoiceID);
                $pastCreditNoteTotal = 0;
                foreach ($creditNotes as $values) {
                    $value = (object) $values;
                    $pastCreditNoteTotal+=$value->creditNoteTotal * $invoiceReturnCustomCurrencyRate;
                }
                $invoicePayedAmount = $invoice->salesInvoicePayedAmount * $invoiceReturnCustomCurrencyRate;
                $invoiceTotalAmount = $invoice->salesinvoiceTotalAmount * $invoiceReturnCustomCurrencyRate;
                $ammountDifferent = $invoiceTotalAmount - $invoicePayedAmount - $pastCreditNoteTotal;

                if ($ammountDifferent < $totalPrice) {
                    $creditNotePaymentEligible = 1;
                    if ($ammountDifferent < 0) {
                        $creditBalance = $totalPrice;
                        $creditDeviation = $totalPrice;
                        $outstandingDeviation = 0;
                    } else {
                        $creditBalance = $totalPrice - $ammountDifferent;
                        $oustandingBalance = $ammountDifferent;
                        $creditDeviation = $totalPrice - $ammountDifferent;
                        $outstandingDeviation = -$ammountDifferent;
                    }
                    // $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceStatusID($invoiceID, 4);
                } else {
                    $oustandingBalance = $totalPrice;
                    $creditDeviation = 0;
                    $outstandingDeviation = $totalPrice;
                }
                $invoiceProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getUnCopiedSalesOrderProducts($invoiceID);
                if(empty($invoiceProducts)){
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_INVOICE_ALL_PRO_CREDITED'),
                    );
                }

                $customerCreditBalance = $cust->customerCurrentCredit + $creditBalance;
                $customerCurrentBalance = $cust->customerCurrentBalance - $oustandingBalance;
                $custData = array(
                    'customerID' => $customerID,
                    'customerCurrentCredit' => $customerCreditBalance,
                    'customerCurrentBalance' => $customerCurrentBalance,
                );
            }
           
            $preCreditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceIDForSPCalculation($invoiceID);
            // $customer = new Customer;
            // $customer->exchangeArray($custData);
            // $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCurrentAndCreditBalance($customer);

            $entityID = $this->createEntity();
            $invoiceReturnData = array(
                'invoiceReturnCode' => $invoiceReturnCode,
                'customerID' => $customerID,
                'locationID' => $locationID,
                'paymentTermID' => $paymentTermID,
                'invoiceID' => $invoiceID,
                'invoiceReturnDate' => $date,
                'invoiceReturnTotal' => $totalPrice,
                'invoiceReturnComment' => $comment,
                'statusID' => 3,
                'customCurrencyId' => $customCurrencyId,
                'invoiceReturnCustomCurrencyRate' => $invoiceReturnCustomCurrencyRate,
                'invoiceReturnDiscountType' => $invoiceReturnDiscountType,
                'invoiceReturnTotalDiscount' => $invoiceReturnTotalDiscount,
                'promotionID' => $promotionID,
                'invoiceReturnPromotionDiscount' => $invoiceReturnPromotionDiscount,
                'entityID' => $entityID,
                'isApproved' => true
            );


            $invoiceReturn = new InvoiceReturn;
            $invoiceReturn->exchangeArray($invoiceReturnData);
            $invoiceReturnID = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->saveInvoiceReturn($invoiceReturn);

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }
            //check and update the invoice remainin discount amount
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
        
            $totalCRAmounts = 0;
            $totalProductAmounts = 0;
            $totalTaxAmount = 0;
            foreach ($products as $keyt => $pt) {
                $pt = (array) $pt;
                if(isset($pt['pTax']['tTA'])){
                    $ptwTax = ($pt['productTotal'] - $pt['pTax']['tTA']); 
                    $totalTaxAmount += $pt['pTax']['tTA']; 
                }else{
                    $ptwTax = $pt['productTotal'];
                }
                $totalProductAmounts += $ptwTax;
            }


            foreach ($products as $key => $p) {
                $productDup = false;
                $p = (array) $p;
                $invoiceProductID = $p['invoiceproductID'];
                
                if($creditNoteDirectType == 'true'){
                    $invoiceProductID = 0;
                }
                $p['invoiceReturnQuantity'] = (array) $p['invoiceReturnQuantity'];
                $batchProducts = $subProducts[$invoiceProductID][0];
                $productID = $p['productID'];
                $oldProID = $p['productID'];
                $documentTypeID = $p['documentTypeID'];
                $documentID = $p['documentID'];
                $loCreditNoteQty = $creditNoteQty = $p['invoiceReturnQuantity']['qty'];
                $productType = $p['productType'];
                if ($productType == 2) {
                    $loCreditNoteQty = 0;
                }
                
                // check this is inactive or not
                if($inactiveFlag){
                    // find correct product id for inactive items
                    $proCode = explode($dupString, $p['productCode']);
                    $realProCode = '';
                    
                    if($dupStringPreFix){
                        $realProCode = $proCode[1]; 
                    } else if($dupStringPostFix){
                        $realProCode = $proCode[0];
                    }
                    if($realProCode != $p['productCode']){
                        //get product details by real product code
                        $productDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductCode($realProCode);
                        $productID = $productDetails->productID;
                        $productDup = true;
                    }
                }

                $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationID);
                $locationProductID = $locationProduct->locationProductID;
                //get location Product Data for calculate avarage costing
                $locationProductAverageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProduct->locationProductQuantity;
                $locationProductLastItemInID = $locationProduct->locationProductLastItemInID;

                if (!isset($locationProductID)) {
                    return array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_CREDIT_PROD_NOT_IN_LOCATION', array($p['productCode'])),
                    );
                }

                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

                
                if ($p['productDiscountType'] == 'value') {
                    $p['productDiscount'] = $p['productDiscount'] * $invoiceReturnCustomCurrencyRate;
                }

                $invoiceReturnProductData = array(
                    'invoiceReturnID' => $invoiceReturnID,
                    'invoiceProductID' => $invoiceProductID,
                    'productID' => $productID,
                    'invoiceReturnProductPrice' => $p['productPrice'] * $invoiceReturnCustomCurrencyRate,
                    'invoiceReturnProductDiscount' => $p['productDiscount'],
                    'invoiceReturnProductDiscountType' => $p['productDiscountType'],
                    'invoiceReturnProductTotal' => $p['productTotal'] * $invoiceReturnCustomCurrencyRate,
                    'invoiceReturnProductQuantity' => $creditNoteQty
                );


                //calculate discount value
                $discountValue = 0;
                if ($p['productDiscountType'] == "precentage") {
                    $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $invoiceReturnCustomCurrencyRate;
                } else if ($p['productDiscountType'] == "value") {
                    $discountValue = $p['productDiscount'] * $invoiceReturnCustomCurrencyRate;
                }

                $invoiceReturnProduct = new InvoiceReturnProduct;
                $invoiceReturnProduct->exchangeArray($invoiceReturnProductData);
                $invoiceReturnProductID = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->saveInvoiceReturnProducts($invoiceReturnProduct);
                if ($p['pTax']) {
                    $p['pTax'] = (array)$p['pTax'];
                    foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                        $productTax = (array) $productTax; 
                        $creditNoteProductTaxData = array(
                            'invoiceReturnProductID' => $invoiceReturnProductID,
                            'taxID' => $taxKey,
                            'invoiceReturnProductTaxPrecentage' => $productTax['tP'],
                            'invoiceReturnProductTaxAmount' => (float) $productTax['tA'] * $invoiceReturnCustomCurrencyRate
                        );
                        $creditNoteProductTax = new InvoiceReturnProductTax();
                        $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);

                        $this->CommonTable('Invoice\Model\InvoiceReturnProductTaxTable')->saveInvoiceReturnProductTax($creditNoteProductTax);
                    }
                }

                if (count($batchProducts) > 0) {
                    foreach ($batchProducts as $batchKey => $batchValue) {
                        $batchValue = (array) $batchValue;                        
                        $newBatchValue = [];
                        $result = $this->saveInvoiceReturnSubProductData($batchValue, $invoiceReturnProductID);
                    }
                } 
            }
            
            return array(
                'status' => true,
                'data' => array('invoiceReturnID' => $invoiceReturnID),
                'msg' => $this->getMessage('SUCC_CREDIT_PRODUCTDETAIL_ADD', array($invoiceReturnCode)),
            );

        } else {
            
            return array(
                'status' => false,
                'msg' => $this->getMessage('ERR_CREDIT_PROD_NOT_IN_LOCATION', array($p['productCode'])),
            );
        }
    }



    // private function _checkAndUpdateDeliveryNoteStatus($deliveryNoteID)
    // {
    //     $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getUnCopiedDeliveryNoteProducts($deliveryNoteID);
    //     if (!empty($deliveryNoteProducts)) {
    //         $statusID = 15;
    //         $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNoteStatus(
    //                 $deliveryNoteID, $statusID);
    //     }
    // }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * If all products of sales invoice has used for credit note
     * Then updates sales invoice status
     * @param int $invoiceID
     */
    private function _checkAndUpdateSalesInvoiceStatus($invoiceID)
    {
        $invoiceProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getUnCopiedSalesOrderProducts($invoiceID);
        if (empty($invoiceProducts)) {
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Invoice\Model\InvoiceTable')->updateSalesInvoiceStatus($invoiceID, $closeStatusID);
        }
    }

    function saveInvoiceReturnSubProductData($batchValue, $invoiceReturnProductID)
    {
        $data = array(
            'invoiceReturnProductID' => $invoiceReturnProductID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'invoiceReturnSubProductQuantity' => $batchValue['qtyByBase'],
        );
        $invoiceReturnSubProduct = new InvoiceReturnSubProduct;
        $invoiceReturnSubProduct->exchangeArray($data);
        $invoiceReturnSubProductID = $this->CommonTable('Invoice\Model\InvoiceReturnSubProductTable')->saveInvoiceReturnSubProduct($invoiceReturnSubProduct);
        return $invoiceReturnSubProductID;
    }

    function saveDraftInvoiceReturnSubProductData($batchValue, $invoiceReturnProductID)
    {
        $data = array(
            'draftInvoiceReturnProductID' => $invoiceReturnProductID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'invoiceReturnSubProductQuantity' => $batchValue['qtyByBase'],
        );
        $invoiceReturnSubProduct = new DraftInvoiceReturnSubProduct;
        $invoiceReturnSubProduct->exchangeArray($data);
        $invoiceReturnSubProductID = $this->CommonTable('Invoice\Model\DraftInvoiceReturnSubProductTable')->saveInvoiceReturnSubProduct($invoiceReturnSubProduct);
        return $invoiceReturnSubProductID;
    }

    public function retriveCustomerInvoiceReturnAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $cust_id = $request->getPost('customerID');
            $invoiceReturns = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->getInvoiceReturnByCustomerIDAndLocationID($cust_id, $locationID);
            foreach ($invoiceReturns as $t) {
                $invoiceReturnArray[$t['invoiceReturnID']] = (object) $t;
            }


            $customerCreditNote = new ViewModel(array(
                'invoiceReturn' => $invoiceReturnArray,
                'statuses' => $this->getStatusesList(),
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            ));
            $customerCreditNote->setTerminal(TRUE);
            $customerCreditNote->setTemplate('invoice/invoice-return/invoice-return-list');
            return $customerCreditNote;
        }
    }

    public function retriveInvoiceReturnFromInvoiceReturnIDAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $invoiceReturnID = $searchrequest->getPost('invoiceReturnID');
            $invoiceReturn = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->getInvoiceReturnByInvoiceReturnID($invoiceReturnID);
            // var_dump($invoiceReturn).die();

            foreach ($invoiceReturn as $t) {
                $invoiceReturnArray[$t['invoiceReturnID']] = (object) $t;
            }
               
            if ($invoiceReturn->count() == 0) {
                return new JsonModel(array(FALSE));
            } else {
                $return = new ViewModel(array(
                    'invoiceReturn' => $invoiceReturnArray,
                    'statuses' => $this->getStatusesList(),
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,
                ));
                $return->setTerminal(TRUE);
                $return->setTemplate('invoice/invoice-return/invoice-return-list');
                return $return;
            }
        }
    }

    public function retriveInvoiceReturnByDatefilterAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromdate = $request->getPost('fromdate');
            $todate = $request->getPost('todate');
            $customerID = $request->getPost('customerID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $filteredInvoiceReturns = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->getInvoiceReturnsByDate($fromdate, $todate, $customerID, $locationID);

//            if ($filteredCreditNotes->count() == 0) {
//                return new JsonModel(array('msg' => $this->getMessage('SUCC_RETURNAPI_NORETURN')));
//            } else {
            foreach ($filteredInvoiceReturns as $t) {
                $invoiceReturnsArray[$t['invoiceReturnID']] = (object) $t;
            }
            $DateFilteredCreditNote = new ViewModel(array(
                'invoiceReturn' => $invoiceReturnsArray,
                'statuses' => $this->getStatusesList(),
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            ));

            $DateFilteredCreditNote->setTerminal(TRUE);
            $DateFilteredCreditNote->setTemplate('invoice/invoice-return/invoice-return-list');
            return $DateFilteredCreditNote;
//            }
        }
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Sales Invoice Return';
        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_INVOICE_RETURN_SENT_EMAIL'); //TODO : create messages according to module
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_INVOICE_RETURN_SENT_EMAIL');
        return $this->JSONRespond();
    }

    // /**
    //  * @author Sharmilan <sharmilan@thinkcube.com>
    //  * @return type
    //  */
    // public function searchActiveCreditNoteForDropdownAction()
    // {

    //     $searchrequest = $this->getRequest();
    //     if ($searchrequest->isPost()) {
    //         $creditNoteSearchKey = $searchrequest->getPost('searchKey');
    //         $locationID = $searchrequest->getPost('locationID');

    //         $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->searchActiveCreditNoteForDropdown($locationID, $creditNoteSearchKey);
    //         $creditNoteList = array();
    //         foreach ($creditNotes as $creditNote) {
    //             $temp['value'] = $creditNote['creditNoteID'];
    //             $temp['text'] = $creditNote['creditNoteCode'];
    //             $creditNoteList[] = $temp;
    //         }

    //         $this->data = array('list' => $creditNoteList);
    //         return $this->JSONRespond();
    //     }
    // } 


    public function getInvoiceRelatedInvoiceRetunrsAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $invoiceID = $searchrequest->getPost('invoiceID');
            $locationID = $this->user_session->userActiveLocation['locationID'];


            $invoiceReturns = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->getInvoiceReturnsByInvoiceID($invoiceID,$locationID);
            $invoiceReturnList = array();
            foreach ($invoiceReturns as $invoiceReturn) {
                $temp['value'] = $invoiceReturn['invoiceReturnID'];
                $temp['text'] = $invoiceReturn['invoiceReturnCode'];
                $invoiceReturnList[] = $temp;
            }

            $this->status = true;
            $this->data = array('list' => $invoiceReturnList);
            return $this->JSONRespond();
        }
    }

    /**
     * Search All Credit note for drop down
     * @return type
     */
    public function searchAllInvoiceReturnForDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $invoiceReturnSearchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');

            $creditNotes = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->searchAllInvoiceReturnForDropdown($locationID, $invoiceReturnSearchKey);
            $creditNoteList = array();
            foreach ($creditNotes as $creditNote) {
                $temp['value'] = $creditNote['invoiceReturnID'];
                $temp['text'] = $creditNote['invoiceReturnCode'];
                $creditNoteList[] = $temp;
            }

            $this->data = array('list' => $creditNoteList);
            return $this->JSONRespond();
        }
    }
     
    
    public function deleteInvoiceReturnAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $invoiceReturnID = $request->getPost('invoiceReturnID');
        $creditNoteTypeFlag = $request->getPost('creditNoteTypeFlag');
        $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            
        $getInvoiceReturnData = $this->CommonTable("Invoice\Model\InvoiceReturnTable")->getInvoiceReturnByIdForDeleteInvoiceReturn($invoiceReturnID);


        // to avoid cancel already canceled creditNote
        if ($getInvoiceReturnData->statusID == 5) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CANCEL_CANCELED_CREDIT_NOTE');
            return $this->JSONRespond();
        }

        //this condition use to avoid replaced creditNote delete problem by two tabs
        if($getInvoiceReturnData->statusID == 10){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DELETE_EDITED_CREDIT_NOTE');
            return $this->JSONRespond();
        }

        $invoiceID = $getInvoiceReturnData->invoiceID;
        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($getCreditNoteData->customerID);
        $creditNoteTotal = $getInvoiceReturnData->invoiceReturnTotal;
        
        
        $invoiceReturnDataSet = $this->checkAndGetInvoiceReturnDataSetByInvoiceReturnID($invoiceReturnID,$invoiceID,$getInvoiceReturnData->locationID);
        $result = $this->checkInvoiceReturnForCancel($invoiceReturnID, $invoiceID, $invoiceReturnDataSet);
        
        if (!$result['status']) {
            $this->rollback();
            $this->status =  false;
            $this->msg =  $this->getMessage('ERR_CANCEL_CREDIT_NOTE_PRODUCT');
            return $this->JSONRespond();
        }

        $statusID = 5;
        $updateInvoiceReturnStatus = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->updateInvoiceReturnStatusForCancel($invoiceReturnID, $statusID);

        if(!$updateInvoiceReturnStatus){
            $this->rollback();
            $this->status =  false;
            $this->msg =  $this->getMessage('ERR_UPDATE_INVOICE_RETURN_STATE');
            return $this->JSONRespond();
        }

       
        //update entityTable as deleted.
        $updateEntity = $this->updateDeleteInfoEntity($getInvoiceReturnData->entityID);
        $this->commit();
        $this->setLogMessage('Invoice Return '.$getInvoiceReturnData->invoiceReturnCode.' is deleted.');
        $this->status =  true;
        $this->msg =  $this->getMessage('SUCC_INVOCIE_RETURN_CANCELED');
        return $this->JSONRespond();
            
            
    }

    public function deleteDraftInvoiceReturnAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $draftInvoiceReturnID = $request->getPost('draftInvoiceReturnID'); 

            $oldDraftInvRtn = (array) $this->CommonTable('Invoice\Model\DraftInvoiceReturnTable')->getInvoiceReturnByInvoiceReturnID($draftInvoiceReturnID)->current();
            $dInvoiceRtnCode = $oldDraftInvRtn['invoiceReturnCode'];

            $oldDraftInvRtn = (array) $oldDraftInvRtn;

            $statusID = 5;
            $updateInvoiceReturnStatus = $this->CommonTable('Invoice\Model\DraftInvoiceReturnTable')->updateInvoiceReturnStatusForCancel($draftInvoiceReturnID, $statusID);

           
            $this->commit();
            $this->setLogMessage('Draft Invoice '.$dInvoiceRtnCode.' is deleted.');
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_DRAFT_INVOICE_RETURN_CANCEL');
            
            return $this->JSONRespond();
        }
            
            
    }

    private function checkAndGetInvoiceReturnDataSetByInvoiceReturnID($invoiceReturnID , $invoiceID, $locationID)
    {
        $creditNoteProductDetails = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->getInvoiceReturnDetailsByInvoiceReturnID($invoiceReturnID, $locationID);
        
        $invoiceReturnBatch = [];
        $invoiceReturnSerial = [];
        $invoiceReturnNormalItem = [];
        $piBatchDetails = [];
        $piSerialDetails = [];
        $piNormalItems = [];

        $creditNoteProducts = [];
        foreach ($creditNoteProductDetails as $value) {
            if($value['productSerialID'] != ''){
                $invoiceReturnSerial[$value['productSerialID']] = $value;
            } else if($value['productBatchID'] != '') {
                $invoiceReturnBatch[$value['productBatchID']] = $value;
            } else {
                $invoiceReturnNormalItem[$value['invoiceReturnProID']] = $value;
            }
        }

        $returnDataSet = array(
            'invoiceReturnBatch' => $invoiceReturnBatch,
            'invoiceReturnSerial' => $invoiceReturnSerial,
            'invoiceReturnNormalItem' => $invoiceReturnNormalItem,
            );

        return $returnDataSet;
    }

    
    public function checkInvoiceReturnForCancel($invoiceReturnID, $invoiceID, $invoiceReturnDataSet)
    {
        foreach ($invoiceReturnDataSet['invoiceReturnSerial'] as $value) {
            $resCreditNote = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->checkAnyInvoiceReturnUsedThisProductAfterThisInvoiceReturnID($invoiceReturnID, $value['productSerialID'],null)->current();

            if ($resCreditNote) {
                $respondData = array(
                    'status' => false,
                );
                return $respondData;
            }

            $resInvoice = $this->CommonTable('Invoice\Model\InvoiceProductTable')->checkAnyInvoicesUsedThisProductAfterThisInvoiceID($invoiceID, $value['productSerialID'],null)->current();

            if ($resInvoice) {
                $respondData = array(
                    'status' => false,
                );
                return $respondData;
            }
        }

        foreach ($invoiceReturnDataSet['invoiceReturnBatch'] as $value) {
            $resCreditNote = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->checkAnyInvoiceReturnUsedThisProductAfterThisInvoiceReturnID($invoiceReturnID, null, $value['productBatchID'])->current();

            if ($resCreditNote) {
                $respondData = array(
                    'status' => false,
                );
                return $respondData;
            }

            $resInvoice = $this->CommonTable('Invoice\Model\InvoiceProductTable')->checkAnyInvoicesUsedThisProductAfterThisInvoiceID($invoiceID, null, $value['productBatchID'])->current();

            if ($resInvoice) {
                $respondData = array(
                    'status' => false,
                );
                return $respondData;
            }
        }
        foreach ($invoiceReturnDataSet['invoiceReturnNormalItem'] as $value) {
            $res = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($value['productID'], $value['locationID']);
            if ($res->productTypeID != "2") {
                if ( $value['documentTypeID'] != "4") {
                    if ($res->locationProductQuantity < $value['invoiceReturnProductQuantity']) {
                        $respondData = array(
                            'status' => false,
                        );
                        return $respondData;
                    }
                } else {
                    $data = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($value['salesInvoiceProductDocumentID'] ,$value['productID']);
                    $availableDeliveryNoteProductQuantity = floatval($data->deliveryNoteProductQuantity) - floatval($data->deliveryNoteProductCopiedQuantity);
                    if ($availableDeliveryNoteProductQuantity < $value['creditNoteProductQuantity']) {
                        $respondData = array(
                            'status' => false,
                        );
                        return $respondData;
                    }
                }
            }
        }

        $respondData = array(
            'status' => true,
        );
        return $respondData;       
    }
    
}

/////////////////// END OF CUSTOMER API CONTROLLER \\\\\\\\\\\\\\\\\\\\