<?php

/**
 * @authora Ashan Madushka  <ashan@thinkcube.com>
 * This file contains Credit Note API related controller functions
 */

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Invoice\Model\CreditNote;
use Invoice\Model\Customer;
use Invoice\Model\CreditNoteProduct;
use Invoice\Model\CreditNoteProductTax;
use Invoice\Model\CreditNoteSubProduct;
use Invoice\Model\CreditNoteInvoiceReturn;
use Invoice\Model\InvoiceEditLog;
use Invoice\Model\InvoiceEditLogDetials;
use Zend\View\Model\JsonModel;
use Inventory\Model\ItemIn;
use Inventory\Model\ItemOut;
use Inventory\Model\ProductBatch;
use Inventory\Model\ProductSerial;

class CreditNoteAPIController extends CoreController
{

    protected $userID;
    protected $user_session;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function saveCreditNoteDetailsAction()
    {
       
        $request = $this->getRequest();
        if ($request->isPost()) {
            $creditNoteDirectType = $request->getPost('creditNoteDirectType');
            $creditNoteBuyBackType = $request->getPost('creditNoteBuyBackType');

            if ($creditNoteDirectType == "true") {
                $this->beginTransaction();
                $directCreditSaveResult = $this->saveDirectCreditNoteDetails($request->getPost(), $creditNoteDirectType);
                if (!$directCreditSaveResult['status']) {
                    $this->rollback();
                    $this->msg = $directCreditSaveResult['msg'];
                    $this->data = $directCreditSaveResult['data'];
                    return $this->JSONRespond();
                } else {
                    $this->commit();
                    $this->status = true;
                    $this->data = $directCreditSaveResult['data'];
                    $this->msg = $directCreditSaveResult['msg'];
                    return $this->JSONRespond();
                }
            }

            if ($creditNoteBuyBackType == "true") {
                $this->beginTransaction();
                $buyBackCreditSaveResult = $this->saveBuyBackCreditNoteDetails($request->getPost(), $creditNoteBuyBackType);
                if (!$buyBackCreditSaveResult['status']) {
                    $this->rollback();
                    $this->msg = $buyBackCreditSaveResult['msg'];
                    $this->data = $buyBackCreditSaveResult['data'];
                    return $this->JSONRespond();
                } else {
                    $this->commit();
                    $this->status = true;
                    $this->data = $buyBackCreditSaveResult['data'];
                    $this->msg = $buyBackCreditSaveResult['msg'];
                    return $this->JSONRespond();
                }
            }

            $customerID = $request->getPost('customerID');
            $relatedInvoiceReturns = $request->getPost('relatedInvoiceReturns');
            $useInvoiceReturn = $request->getPost('useInvoiceReturn');
            $customerName = $request->getPost('customerName');
            $customCurrencyId = $request->getPost('customCurrencyId');
            $dimensionData = $request->getPost('dimensionData');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $creditNoteCustomCurrencyRate = (float) $request->getPost('creditNoteCustomCurrencyRate');
            $inactiveFlag = false;
            $inactiveFlag =  filter_var($request->getPost('invFlag'), FILTER_VALIDATE_BOOLEAN);
            $invCloseFlag = filter_var($request->getPost('invCloseFlag'), FILTER_VALIDATE_BOOLEAN);
            $dupString = "DUP";
            $dupStringPreFix = false;
            $dupStringPostFix = true;
            $cdNotDirFlag = false;
            $cdNotBuyBackFlag = false;
                        
            //get customCurrency data by customer CurrencyId
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

            $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
            
            if(!is_null($cust)){
                $products = $request->getPost('products');
                $invoiceID = $request->getPost('invoiceID');
                $locationID = $request->getPost('locationID');
                if(!$invoiceID && $creditNoteDirectType != 'true'){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE');
                    return $this->JSONRespond();
                }
             
                if ($invoiceID != "") {
                    $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                    if ($invoiceDetails['statusID'] == '10') {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE_EDITED');
                        return $this->JSONRespond();
                    }

                if ($invoiceDetails['statusID'] == '5') {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE_CANCELED');
                        return $this->JSONRespond();
                    }
                }
                    
                if($creditNoteDirectType == 'true'){
                    $cdNotDirFlag = true;
                }
                if($creditNoteDirectType == 'true' && $invoiceID == ''){
                    $invoiceID = 0;
                    
                } else if($creditNoteDirectType == 'true' && $invoiceID != ''){
                    //validate customerID and invoiceID
                    $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                    if($invoiceDetails['customerID'] != $customerID){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DIR_CUSTOMER_NOT_MATCH');
                        return $this->JSONRespond();
                    } else if($invoiceDetails['locationID'] != $locationID){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DIR_LOC_NOT_MATCH');
                        return $this->JSONRespond();
                    }
                    
                }
                $totalPrice = (float) trim($request->getPost('creditNoteTotalPrice')) * $creditNoteCustomCurrencyRate;
                
                if($creditNoteDirectType == 'true' && $invoiceID != ''){
                    $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                    $invoiceTotalAmount = $invoiceDetails['salesinvoiceTotalAmount'];

                    $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceID($invoiceID)->current();

                    $creditNoteAmount = 0;
                    if (!is_null($creditNoteTotalAmountForTheInvoice['creditNoteTot'])) {
                        $creditNoteAmount = $creditNoteTotalAmountForTheInvoice['creditNoteTot'];
                    }

                    $restAmountOfInvoice = $invoiceTotalAmount - $creditNoteAmount;

                    if ($totalPrice > $restAmountOfInvoice) {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_DIR_TOTAL_AMOUNT');
                        return $this->JSONRespond();
                    }

                }
                
                $subProducts = $request->getPost('subProducts');
                $creditNoteCode = $request->getPost('creditNoteCode');
                $date = $this->convertDateToStandardFormat($request->getPost('date'));
                $comment = $request->getPost('creditNoteComment');
                $paymentTermID = $request->getPost('paymentTermID');
                $creditNoteInvoiceDiscountType = $request->getPost('creditNoteInvoiceDiscountType');
                $creditNoteInvoiceDiscountAmount = $request->getPost('creditNoteInvoiceDiscountAmount');
                $creditNotePromotionDiscountType = $request->getPost('creditNotePromotionDiscountType');
                $creditNotePromotionDiscountAmount = $request->getPost('creditNotePromotionDiscountAmount');
                $creditNotePaymentEligible = 0;
                $creditBalance = 0;
                $oustandingBalance = 0;
                $this->beginTransaction();

                $result = $this->getReferenceNoForLocation('8', $locationID);
                $locationReferenceID = $result['locRefID'];
                //check credit note numer already exist in credit note table if exist give next number for credit note code
                while ($creditNoteCode) {
                    if ($this->CommonTable('Invoice\Model\CreditNoteTable')->checkCreditNoteByCode($creditNoteCode)->current()) {
                        if ($locationReferenceID) {
                            $newCreditNoteCode = $this->getReferenceNumber($locationReferenceID);
                            if ($newCreditNoteCode == $creditNoteCode) {
                                $this->updateReferenceNumber($locationReferenceID);
                                $creditNoteCode = $this->getReferenceNumber($locationReferenceID);
                            } else {
                                $creditNoteCode = $newCreditNoteCode;
                            }
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_CREDIT_CODE_EXIST');
                            return $this->JSONRespond();
                        }
                    } else {
                        break;
                    }
                }

                $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
                $displayData = $result->current();
                $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
                $averageCostingFlag = $displayData['averageCostingFlag'];

                if(empty($FIFOCostingFlag) && empty($averageCostingFlag)){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE');
                    return $this->JSONRespond();
                }

                $accountProduct = array();
                $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
                $glAccountsData = $glAccountExist->current();
                $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

                $custSalesAccountIsset = true;
                if($this->useAccounting == 1){
                    if(empty($customerData->customerSalesAccountID)){
                        $custSalesAccountIsset = false;
                        // $this->rollback();
                        // $this->status = false;
                        // $this->msg = $this->getMessage('ERR_CUSTOMER_SALES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode));
                        // return $this->JSONRespond();
                    }else if(empty($customerData->customerReceviableAccountID)){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode));
                        return $this->JSONRespond();
                    }
                }


                $creditDeviation = 0;
                $outstandingDeviation = 0;
                //no need to process invoice details for direct credit notes
                if($creditNoteDirectType == 'true'){
                    if ($invoiceID == 0) {
                        $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                        $customerCurrentBalance = $cust->customerCurrentBalance;
                    } else {
                        if($cust->customerCurrentBalance == 0){
                            $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                            $customerCurrentBalance = $cust->customerCurrentBalance;
                            $creditDeviation = $totalPrice;
                        } else if($cust->customerCurrentBalance - $totalPrice < 0){
                            $customerCurrentBalance = 0;
                            $customerCreditBalance = $cust->customerCurrentCredit + ($totalPrice - $cust->customerCurrentBalance);
                            $creditDeviation = $totalPrice - $cust->customerCurrentBalance;
                            $outstandingDeviation = -($cust->customerCurrentBalance);
                        } else {
                            $customerCurrentBalance = $cust->customerCurrentBalance - $totalPrice;
                            $customerCreditBalance = $cust->customerCurrentCredit;
                            $outstandingDeviation = -$totalPrice;
                        }
                    }                        

                    $custData = array(
                        'customerID' => $customerID,
                        'customerCurrentCredit' => $customerCreditBalance,
                        'customerCurrentBalance' => $customerCurrentBalance,
                    );
                } else {
                    $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                    $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceID($invoiceID);
                    $pastCreditNoteTotal = 0;
                    foreach ($creditNotes as $values) {
                        $value = (object) $values;
                        $pastCreditNoteTotal+=$value->creditNoteTotal * $creditNoteCustomCurrencyRate;
                    }
                    $invoicePayedAmount = $invoice->salesInvoicePayedAmount * $creditNoteCustomCurrencyRate;
                    $invoiceTotalAmount = $invoice->salesinvoiceTotalAmount * $creditNoteCustomCurrencyRate;
                    $ammountDifferent = $invoiceTotalAmount - $invoicePayedAmount - $pastCreditNoteTotal;

                    if ($ammountDifferent < $totalPrice) {
                        $creditNotePaymentEligible = 1;
                        if ($ammountDifferent < 0) {
                            $creditBalance = $totalPrice;
                            $creditDeviation = $totalPrice;
                            $outstandingDeviation = 0;
                        } else {
                            $creditBalance = $totalPrice - $ammountDifferent;
                            $oustandingBalance = $ammountDifferent;
                            $creditDeviation = $totalPrice - $ammountDifferent;
                            $outstandingDeviation = -$ammountDifferent;
                        }
                        $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceStatusID($invoiceID, 4);

                        $invoiceEditLogModel = new InvoiceEditLog();
                        $invoiceEditLogModel->exchangeArray([
                            'userID' => $this->userID,
                            'salesInvoiceID' => $invoiceID,
                            'dateAndTime' => $this->getGMTDateTime()
                        ]);
                        $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);

                        $newItemsData = [
                            'invoiceEditLogDetailsOldState' => $invoice->statusID,
                            'invoiceEditLogDetailsNewState' => 'Invoice closed with adding the credit note '.$creditNoteCode,
                            'invoiceEditLogDetailsCategory' => 'salesInvoiceStatusClosed',
                            'invoiceEditLogID' => $invoiceEditLastInsertID
                        ];
                        $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                        $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                        $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                    } else {
                        $oustandingBalance = $totalPrice;
                        $creditDeviation = 0;
                        $outstandingDeviation = $totalPrice;
                    }
                    $invoiceProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getUnCopiedSalesOrderProducts($invoiceID);
                    if(empty($invoiceProducts)){
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_INVOICE_ALL_PRO_CREDITED');
                        return $this->JSONRespond();
                    }

                    $customerCreditBalance = $cust->customerCurrentCredit + $creditBalance;
                    $customerCurrentBalance = $cust->customerCurrentBalance - $oustandingBalance;
                    $custData = array(
                        'customerID' => $customerID,
                        'customerCurrentCredit' => $customerCreditBalance,
                        'customerCurrentBalance' => $customerCurrentBalance,
                    );
                }
               
                $preCreditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceIDForSPCalculation($invoiceID);
                $customer = new Customer;
                $customer->exchangeArray($custData);
                $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCurrentAndCreditBalance($customer);

                $entityID = $this->createEntity();
                $creditNoteData = array(
                    'creditNoteCode' => $creditNoteCode,
                    'customerID' => $customerID,
                    'locationID' => $locationID,
                    'paymentTermID' => $paymentTermID,
                    'invoiceID' => $invoiceID,
                    'creditNoteDate' => $date,
                    'creditNoteTotal' => $totalPrice,
                    'creditNoteComment' => $comment,
                    'statusID' => 3,
                    'creditNotePaymentEligible' => $creditNotePaymentEligible,
                    'creditNotePaymentAmount' => $creditBalance,
                    'customCurrencyId' => $customCurrencyId,
                    'creditNoteCustomCurrencyRate' => $creditNoteCustomCurrencyRate,
                    'creditNoteInvoiceDiscountType' => $creditNoteInvoiceDiscountType,
                    'creditNoteInvoiceDiscountAmount' => $creditNoteInvoiceDiscountAmount,
                    'creditNotePromotionDiscountType' => $creditNotePromotionDiscountType,
                    'creditNotePromotionDiscountAmount' => $creditNotePromotionDiscountAmount,
                    'creditNoteDirectFlag' => $cdNotDirFlag,
                    'creditNoteBuyBackFlag' => $cdNotBuyBackFlag,
                    'entityID' => $entityID,
                    'creditDeviation' => $creditDeviation,
                    'outstandingDeviation' => $outstandingDeviation,
                    'ableToCancel' => true
                );

                $creditNote = new CreditNote;
                $creditNote->exchangeArray($creditNoteData);
                $creditNoteID = $this->CommonTable('Invoice\Model\CreditNoteTable')->saveCreditNote($creditNote);

                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }
                //check and update the invoice remainin discount amount
                $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                if ($creditNoteInvoiceDiscountType == 'value'){
                    if ($creditNoteInvoiceDiscountAmount > 0) {
                        if ($invoiceData->salesInvoiceRemainingDiscValue != null) {
                            $invoiceDiscountRemaingValue = $invoiceData->salesInvoiceRemainingDiscValue;
                        } else {
                            $invoiceDiscountRemaingValue = $invoiceData->salesInvoiceDiscountRate;
                        }
                        if($invoiceDiscountRemaingValue >= $creditNoteInvoiceDiscountAmount){
                            $remainingDiscount = $invoiceDiscountRemaingValue - $creditNoteInvoiceDiscountAmount;
                            $updateInvoiceDiscountValue = $this->CommonTable('Invoice\Model\InvoiceTable')
                            ->updateInvoiceRemainingDiscountValue($invoiceID,$remainingDiscount);
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_DISCT_CANT_BE_MORE_THAN_INVOICE_DISCOUNT');
                            return $this->JSONRespond();
                        }

                    }
                }

                //update salesPerson amount
                // $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getallsSalesPersonByInvoiceID($invoiceID);

                // $calculatePreviousCRAmount = 0;

                // foreach ($preCreditNotes as $keycrn => $crn) {
                //     if ($crn['$creditNoteID'] != $creditNoteID) {
                //         $calculatePreviousCRAmount += $crn['creditNoteTotal'];
                //     }
                // }

                // if (sizeof($relatedSalesPersons) > 0) {
                //     $updatedSpAmount = round(($invoiceData->salesinvoiceTotalAmount - ($calculatePreviousCRAmount + $totalPrice))/ sizeof($relatedSalesPersons),2);

                //     foreach ($relatedSalesPersons as $keysp => $sp) {
                //         $update = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->updateSPAmount($updatedSpAmount, $sp->invoiceSalesPersonID);
                //     }
                // }

                if ($creditNotePromotionDiscountType == 'value') {
                    if ($creditNotePromotionDiscountAmount > 0) {
                        if ($invoiceData->salesInvoiceRemainingPromotionDiscValue != null) {
                            $promotionDiscountRemaingValue = $invoiceData->salesInvoiceRemainingPromotionDiscValue;
                        } else {
                            $promotionDiscountRemaingValue = $invoiceData->salesInvoicePromotionDiscount;
                        }
                        if($promotionDiscountRemaingValue >= $creditNotePromotionDiscountAmount){
                            $remainingDiscount = $promotionDiscountRemaingValue - $creditNotePromotionDiscountAmount;
                            $updatePromotionDiscountValue = $this->CommonTable('Invoice\Model\InvoiceTable')
                            ->updatePromotionRemainingDiscountValue($invoiceID,$remainingDiscount);
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_DISCT_CANT_BE_MORE_THAN_PROMOTION_DISCOUNT');
                            return $this->JSONRespond();
                        }
                    }

                }

                $totalCRAmounts = 0;
                $totalProductAmounts = 0;
                $totalTaxAmount = 0;
                foreach ($products as $keyt => $pt) {
                    if(isset($pt['pTax']['tTA'])){
                        $ptwTax = ($pt['productTotal'] - $pt['pTax']['tTA']); 
                        $totalTaxAmount += $pt['pTax']['tTA']; 
                    }else{
                        $ptwTax = $pt['productTotal'];
                    }
                    $totalProductAmounts += $ptwTax;
                }

                foreach ($products as $key => $p) {
                    $productDup = false;
                    $invoiceProductID = $p['invoiceproductID'];
                    
                    if($creditNoteDirectType == 'true'){
                        $invoiceProductID = 0;
                    }
                    $batchProducts = $subProducts[$invoiceProductID];
                    $productID = $p['productID'];
                    $oldProID = $p['productID'];
                    $documentTypeID = $p['documentTypeID'];
                    $documentID = $p['documentID'];
                    $loCreditNoteQty = $creditNoteQty = $p['creditNoteQuantity']['qty'];
                    $productType = $p['productType'];
                    if ($productType == 2) {
                        $loCreditNoteQty = 0;
                    }
                    
                    // check this is inactive or not
                    if($inactiveFlag){
                        // find correct product id for inactive items
                        $proCode = explode($dupString, $p['productCode']);
                        $realProCode = '';
                        
                        if($dupStringPreFix){
                            $realProCode = $proCode[1]; 
                        } else if($dupStringPostFix){
                            $realProCode = $proCode[0];
                        }
                        if($realProCode != $p['productCode']){
                            //get product details by real product code
                            $productDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductCode($realProCode);
                            $productID = $productDetails->productID;
                            $productDup = true;
                        }
                    }

                    $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $locationID);
                    $locationProductID = $locationProduct->locationProductID;
                    //get location Product Data for calculate avarage costing
                    $locationProductAverageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
                    $locationProductQuantity = $locationProduct->locationProductQuantity;
                    $locationProductLastItemInID = $locationProduct->locationProductLastItemInID;

                    if (!isset($locationProductID)) {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CREDIT_PROD_NOT_IN_LOCATION', array($p['productCode']));
                        return $this->JSONRespond();
                    }

                    $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);


                    if($this->useAccounting == 1){
                        if($p['giftCard'] != 1 && $p['productType'] != 2){
                            if(empty($pData['productInventoryAccountID'])){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                                return $this->JSONRespond();
                            }
                         //check whether Product Cost of Goods Sold Gl account id set or not
                            if(empty($pData['productCOGSAccountID'])){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName']));
                                return $this->JSONRespond();
                            }
                        }



                        if(!$custSalesAccountIsset){
                            if(empty($pData['productSalesAccountID'])){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_PRPODUCT_SALES_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                                return $this->JSONRespond();
                        
                            }
                        }

                    //set gl accounts for the journal entry
                        $pTotal = $p['productTotal'] - $p['pTax']['tTA'];
                        if($creditNoteData['creditNoteInvoiceDiscountType'] == "value"){
                            $productTotal = $pTotal - $creditNoteData['creditNoteInvoiceDiscountAmount']*($pTotal/$totalProductAmounts);
                        }else {
                            $productTotal = $pTotal;
                        }
                        if($creditNotePromotionDiscountType == 'value'){
                            $productTotal = $productTotal - $creditNotePromotionDiscountAmount*($productTotal/$totalProductAmounts);
                        }

                        if($p['giftCard'] != 1){
                            $totalCRAmounts+=$productTotal;
                            // if($custSalesAccountIsset){
                            //     if(isset($accountProduct[$customerData->customerSalesAccountID]['debitTotal'])){
                            //         $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] += $productTotal;
                            //     }else{
                            //         $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] = $productTotal;
                            //         $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] = 0.00;
                            //         $accountProduct[$customerData->customerSalesAccountID]['accountID'] = $customerData->customerSalesAccountID;
                            //     }
                            // }else{
                            if(isset($accountProduct[$pData['productSalesAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productSalesAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productSalesAccountID']]['debitTotal'] = $productTotal;
                                $accountProduct[$pData['productSalesAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productSalesAccountID']]['accountID'] = $pData['productSalesAccountID'];
                            }
                            // }
                        }
                    }

                    if ($p['productType'] != 2) {

                        $checkProductCopiedFromDeliveryNote = $this->CommonTable('Invoice\Model\InvoiceProductTable')->checkProductCopiedFromDeliveryNote($invoiceProductID)->current();

                        if (!$checkProductCopiedFromDeliveryNote) {
                            $locationProductData = array(
                                'locationProductQuantity' => $locationProduct->locationProductQuantity + $loCreditNoteQty,
                                );

                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductData, $productID, $locationID);
                        } else if ($checkProductCopiedFromDeliveryNote && $inactiveFlag) {
                            $locationProductData = array(
                                'locationProductQuantity' => $locationProduct->locationProductQuantity + $loCreditNoteQty,
                                );

                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductData, $productID, $locationID);
                        } else {
                            $deliveryNoteID = $checkProductCopiedFromDeliveryNote['salesInvoiceProductDocumentID'];

                            $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($deliveryNoteID, $productID);

                            if (isset($deliveryNoteProducts)) {
                                $newCopiedQuantity = $deliveryNoteProducts->deliveryNoteProductCopiedQuantity - (float) $loCreditNoteQty;
                                $copiedflag = 1;
                                if ($newCopiedQuantity < $deliveryNoteProducts->deliveryNoteProductQuantity) {
                                    $copiedflag = 0;
                                }
                                $deliveryNoteProData = array(
                                    'copied' => $copiedflag,
                                    'deliveryNoteProductCopiedQuantity' => $newCopiedQuantity,
                                    );

                                $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryNoteProducts($productID, $deliveryNoteID, $deliveryNoteProData);
                                $this->_checkAndUpdateDeliveryNoteStatus($deliveryNoteID);

                            }
                        }

                    }
                    
                    if ($p['productDiscountType'] == 'value') {
                        $p['productDiscount'] = $p['productDiscount'] * $creditNoteCustomCurrencyRate;
                    }

                    $creditNoteProductData = array(
                        'creditNoteID' => $creditNoteID,
                        'invoiceProductID' => $invoiceProductID,
                        'productID' => $productID,
                        'creditNoteProductPrice' => $p['productPrice'] * $creditNoteCustomCurrencyRate,
                        'creditNoteProductDiscount' => $p['productDiscount'],
                        'creditNoteProductDiscountType' => $p['productDiscountType'],
                        'creditNoteProductTotal' => $p['productTotal'] * $creditNoteCustomCurrencyRate,
                        'creditNoteProductQuantity' => $creditNoteQty
                    );

                    //calculate discount value
                    $discountValue = 0;
                    if ($p['productDiscountType'] == "precentage") {
                        $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $creditNoteCustomCurrencyRate;
                    } else if ($p['productDiscountType'] == "value") {
                        $discountValue = $p['productDiscount'] * $creditNoteCustomCurrencyRate;
                    }

                    $creditNoteProduct = new CreditNoteProduct;
                    $creditNoteProduct->exchangeArray($creditNoteProductData);
                    $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->saveCreditNoteProducts($creditNoteProduct);
                    if ($p['pTax']) {
                        foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                            $creditNoteProductTaxData = array(
                                'creditNoteProductID' => $creditNoteProductID,
                                'taxID' => $taxKey,
                                'creditNoteProductTaxPercentage' => $productTax['tP'],
                                'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
                            );
                            $creditNoteProductTax = new CreditNoteProductTax();
                            $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
                            $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->saveCreditNoteProductTax($creditNoteProductTax);
                        
                            $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxKey);
                            if($this->useAccounting == 1 && empty($taxData['taxSalesAccountID'])){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_TAX_SALES_ACCOUNT', array($taxData['taxName']));
                                return $this->JSONRespond();
                            }

                            $totalCRAmounts += $productTax['tA'] * $creditNoteCustomCurrencyRate;
                                //set tax gl accounts for the journal Entry
                            if(isset($accountProduct[$taxData['taxSalesAccountID']]['debitTotal'])){
                                $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] += $productTax['tA'] * $creditNoteCustomCurrencyRate;
                            }else{
                                $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = $productTax['tA'] * $creditNoteCustomCurrencyRate;
                                $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                            }
                        }
                    }
                    if ((!count($batchProducts) > 0) && ($p['productType'] != 2)) {
                        $itemReturnQty = $creditNoteQty;
                        //add item in details for non serial and batch products
                        if($p['documentTypeID'] == 4){
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($p['locationProductID'], 'Delivery Note', $p['documentID']);
                        }else{
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($p['locationProductID'], 'Sales Invoice', $invoiceID);
                        }
                        foreach (array_reverse($itemOutDetails) as $itemOutData) {
                            if ($itemReturnQty != 0) {
                                if ($itemOutData->itemOutQty > $itemOutData->itemOutReturnQty) {
                                    $itemInPreDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetails($itemOutData->itemOutItemInID);
                                    
                                    $unitPrice = $itemInPreDetails['itemInPrice'];
                                    $unitDiscount = $itemInPreDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutData->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }
                                    $leftQty = floatval($itemOutData->itemOutQty) - floatval($itemOutData->itemOutReturnQty);

                                    $itemInInsertQty = 0;
                                    $itemOutUpdateReturnQty = 0;
                                    if ($leftQty >= $itemReturnQty) {
                                        $itemInInsertQty = $itemReturnQty;
                                        $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) + floatval($itemReturnQty);
                                        $itemReturnQty = 0;
                                    } else {
                                        $itemInInsertQty = $leftQty;
                                        $itemOutUpdateReturnQty = $itemOutData->itemOutQty;
                                        $itemReturnQty = round(floatval($itemReturnQty) - floatval($leftQty), 2);
                                    }
                                    $newAvgCst = 0;
                                    if ($averageCostingFlag == 1) {

                                        $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')
                                                ->getLocationProduct($productID, $locationID);
                                        $locationProductAverageCostingPriceForUpdate = $locationProduct->locationProductAverageCostingPrice;
                                        $locationProductQuantityForUpdate = $locationProduct->locationProductQuantity;
                                        
                                        // calcutate current avg costing
                                        $newAvgCst = ((floatval($unitPrice) * floatval($itemInInsertQty)) + (floatval($locationProductAverageCostingPriceForUpdate) * (floatval($locationProductQuantityForUpdate)- floatval($itemInInsertQty))))/(floatval($locationProductQuantityForUpdate));
                                        
                                        // update location product avg costing
                                        $locationProductAvgData = array(
                                            'locationProductAverageCostingPrice' => $newAvgCst,
                                        );

                                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductAvgData, $productID, $locationID);

                                    }
                                    //hit data to the item in table with left qty
                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInPreDetails['itemInIndexID']);
                                    if($inactiveFlag && $productDup){
                                        $newItemInIndex = null;
                                    }
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Credit Note',
                                        'itemInDocumentID' => $creditNoteID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => NULL,
                                        'itemInSerialID' => NULL,
                                        'itemInQty' => $itemInInsertQty,
                                        'itemInPrice' => $unitPrice,
                                        'itemInAverageCostingPrice' => $newAvgCst,
                                        'itemInDiscount' => $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                        //update item out return qty
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                                    }
                                    if($this->useAccounting == 1 && $p['documentTypeID'] != 4){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $itemInInsertQty * $itemOutData->itemOutAverageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = $itemInInsertQty * ($itemInPreDetails['itemInPrice'] - $itemInPreDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                }
                            } else {
                                break;
                            }
                        }

                        if ($itemReturnQty != 0) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_INV_NOT_ENOUGH_QTY',array($pData['productCode'].' - '.$pData['productName']));
                            return $this->JSONRespond();
                        }

                    }

                    if (count($batchProducts) > 0) {
                        foreach ($batchProducts as $batchKey => $batchValue) {
                            $newBatchValue = [];
                            //if this is come from inactive item then need to find correct batch and serial ids
                            if($inactiveFlag && $productDup){
                                if($batchValue['batchID'] != ''){
                                    //get correct batch ID
                                    $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProductFromLocationProductIdAndProductBatchCode($locationProductID, $batchValue['bathchCode']);
                                    
                                    if(count($batchDetails) == 0){
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $this->getMessage('ERR_BATCH_CODE_CANNOT_FIND', [$batchValue['bathchCode']]);
                                        return $this->JSONRespond();
                                    }
                                    $newBatchValue = array(
                                        'batchID' => $batchDetails->productBatchID,
                                        'serialID' => $batchValue['serialID'],
                                        'bathchCode' =>  $batchValue['bathchCode'],
                                        'serialCode' => $batchValue['serialCode'],
                                        'qtyByBase' => $batchValue['qtyByBase']
                                    );
                                                              
                                } else if($batchValue['serialID'] != ''){
                                    // need to implement 
                                }
                                $result = $this->saveCreditNoteSubProductData($newBatchValue, $creditNoteProductID);
                            } else {
                                $result = $this->saveCreditNoteSubProductData($batchValue, $creditNoteProductID);
                            }
                            if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                                $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProductsByLocIdCode($batchValue['batchID'], $p['locationProductID']);
                                if ($p['locationProductID'] != $locationProductID) {
                                    $bpdata = array(
                                        'locationProductID' => $locationProductID,
                                        'productBatchCode' => $batchProduct->productBatchCode,
                                        'productBatchExpiryDate' => $batchProduct->productBatchExpiryDate,
                                        'productBatchWarrantyPeriod' => $batchProduct->productBatchWarrantyPeriod,
                                        'productBatchManufactureDate' => $batchProduct->productBatchManufactureDate,
                                        'productBatchQuantity' => $batchValue['qtyByBase'],
                                    );
                                    $batchProduct = new ProductBatch();
                                    $batchProduct->exchangeArray($bpdata);
                                    if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                        $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                                    }
                                    if ($batchValue['serialID']) {
                                        $sresult = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductsBySerialCodeAndLocationProdutId($batchValue['serialCode'], $p['locationProductID']);
                                        $spdata = array(
                                            'locationProductID' => $locationProductID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialCode' => $batchValue['serialCode'],
                                            'productSerialExpireDate' => $sresult->productSerialExpireDate,
                                            'productSerialWarrantyPeriod' => $sresult->productSerialWarrantyPeriod,
                                            'productSerialWarrantyPeriodType' => (int)$sresult->productSerialWarrantyPeriodType,
                                            'productSerialSold' => '0',
                                            'productSerialReturned' => '0',
                                        );
                                        $serialPr = new ProductSerial();
                                        $serialPr->exchangeArray($spdata);

                                        if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                            $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                        }
                                    } else {
                                        $insertedSID = NULL;
                                    }
                                } else {
                                    $insertedBID = $batchValue['batchID'];
                                    $productBatchQuentity = $batchProduct->productBatchQuantity + $batchValue['qtyByBase'];
                                    $productBatchQty = array(
                                        'productBatchQuantity' => $productBatchQuentity,
                                    );
                                    if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                        $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($insertedBID, $productBatchQty);
                                    }

                                    if ($batchValue['serialID']) {
                                        $insertedSID = $batchValue['serialID'];
                                        $serialProductData = array(
                                            'productSerialSold' => '0',
                                        );

                                        if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                            $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $insertedSID);
                                        }
                                    } else {
                                        $insertedSID = NULL;
                                    }
                                }
                                if ($batchValue['serialID']) {
                                    //Add details to item in table for batch and serial products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchSerialProductDetails($p['locationProductID'], $batchValue['batchID'], $batchValue['serialID']);

                                    if ($p['documentTypeID'] == 4) {
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'],$batchValue['batchID'], 'Delivery Note', $p['documentID']);
                                    } else {
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'],$batchValue['batchID'], 'Sales Invoice', $invoiceID);
                                    }   
                                    
                                    $unitPrice = $itemInDetails['itemInPrice'];
                                    $unitDiscount = $itemInDetails['itemInDiscount'];
                                    $newAvgCst = 0;
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;

                                        $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')
                                                ->getLocationProduct($productID, $locationID);
                                        $locationProductAverageCostingPriceForUpdate = $locationProduct->locationProductAverageCostingPrice;
                                        $locationProductQuantityForUpdate = $locationProduct->locationProductQuantity;
                                        
                                        // calcutate current avg costing
                                        $newAvgCst = (floatval($unitPrice) + (floatval($locationProductAverageCostingPriceForUpdate) * (floatval($locationProductQuantityForUpdate)-1)))/(floatval($locationProductQuantityForUpdate));
                                        
                                        // update location product avg costing
                                        $locationProductAvgData = array(
                                            'locationProductAverageCostingPrice' => $newAvgCst,
                                        );

                                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductAvgData, $productID, $locationID);
                                    }

                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Credit Note',
                                        'itemInDocumentID' => $creditNoteID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => $insertedBID,
                                        'itemInSerialID' => $insertedSID,
                                        'itemInQty' => 1,
                                        'itemInPrice' => $unitPrice,
                                        'itemInAverageCostingPrice' => $newAvgCst,
                                        'itemInDiscount' => $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                    );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    
                                    if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID,'1');
                                    } else {
                                        $deliveryNoteDetails = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteDetailsByDeliveryNoteID($p['documentID'],null,$itemOutDetails->itemOutSerialID)->current();
                                            
                                            $deliveryNoteSubProductID = $deliveryNoteDetails['deliveryNoteSubProductID'];
                                            $deliveryNoteCopiedQuantity = $deliveryNoteDetails['deliveryNoteSubProductsCopiedQuantity'];

                                            $newCopiedQuantity = $deliveryNoteCopiedQuantity - $batchValue['qtyByBase'];
                                            $data = array(
                                                    'deliveryNoteSubProductsCopiedQuantity' => $newCopiedQuantity
                                                    ); 
                                            $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->updateDeliveryNoteSubProductData($data,$deliveryNoteSubProductID);
                                    }

                                    if($this->useAccounting == 1 && $p['documentTypeID'] != 4){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = 1 * $itemOutDetails->itemOutAverageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }

                                    ///////////////////
                                } else {
                                    //Add details to item in table for batch products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchProductDetails($p['locationProductID'], $batchValue['batchID']);

                                    if ($p['documentTypeID'] == 4) {
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetailsForCreditNote($batchValue['batchID'], 'Delivery Note', $p['documentID']);
                                    } else {
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetailsForCreditNote($batchValue['batchID'], 'Sales Invoice', $invoiceID);
                                    }
                                    if ($p['documentTypeID'] == 4 && !$inactiveFlag) {
                                        foreach ($itemOutDetails as $key => $value) {
                                            $deliveryNoteDetails = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteDetailsByDeliveryNoteID($p['documentID'],$value['itemOutBatchID'],null)->current();
                                            
                                            $deliveryNoteSubProductID = $deliveryNoteDetails['deliveryNoteSubProductID'];
                                            $deliveryNoteCopiedQuantity = $deliveryNoteDetails['deliveryNoteSubProductsCopiedQuantity'];

                                            $newCopiedQuantity = $deliveryNoteCopiedQuantity - $batchValue['qtyByBase'];
                                            $data = array(
                                                    'deliveryNoteSubProductsCopiedQuantity' => $newCopiedQuantity
                                                    ); 
                                            $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->updateDeliveryNoteSubProductData($data,$deliveryNoteSubProductID);
                                        }

                                    } else {
                                        $unitPrice = $itemInDetails['itemInPrice'];
                                        $unitDiscount = $itemInDetails['itemInDiscount'];
                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                        if($inactiveFlag && $productDup){
                                            $newItemInIndex = null;
                                        }
                                        
                                        $batchItemQty = $batchValue['qtyByBase'];
                                        foreach ($itemOutDetails as $key => $singleBatchValue) {
                                            $remQty = $singleBatchValue['itemOutQty'] - $singleBatchValue['itemOutReturnQty'];
                                            if ($remQty > $batchItemQty) {
                                                $usedQty = $batchItemQty;
                                                $batchItemQty = 0;                                             
                                            } else {
                                                $usedQty = $remQty;
                                                $batchItemQty =  $batchItemQty - $remQty;
                                            }
                                            if($averageCostingFlag == 1){
                                                $unitPrice = $singleBatchValue['itemOutAverageCostingPrice'];
                                                $unitDiscount = 0;

                                                $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')
                                                ->getLocationProduct($productID, $locationID);
                                                $locationProductAverageCostingPriceForUpdate = $locationProduct->locationProductAverageCostingPrice;
                                                $locationProductQuantityForUpdate = $locationProduct->locationProductQuantity;
                                                // calcutate current avg costing
                                                $newAvgCst = ((floatval($unitPrice) * floatval($usedQty)) + (floatval($locationProductAverageCostingPriceForUpdate) * (floatval($locationProductQuantityForUpdate)- floatval($usedQty))))/(floatval($locationProductQuantityForUpdate));
                                        
                                                // update location product avg costing
                                                $locationProductAvgData = array(
                                                    'locationProductAverageCostingPrice' => $newAvgCst,
                                                );

                                                $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductAvgData, $productID, $locationID);
                                            }
                                            
                                            $itemInData = array(
                                                'itemInIndex' => $newItemInIndex,
                                                'itemInDocumentType' => 'Credit Note',
                                                'itemInDocumentID' => $creditNoteID,
                                                'itemInLocationProductID' => $locationProductID,
                                                'itemInBatchID' => $insertedBID,
                                                'itemInSerialID' => NULL,
                                                'itemInQty' => $usedQty,
                                                'itemInPrice' =>  $unitPrice,
                                                'itemInAverageCostingPrice' => $newAvgCst,
                                                'itemInDiscount' => $unitDiscount,
                                                'itemInDateAndTime' => $this->getGMTDateTime(),
                                            );
                                            $itemInModel = new ItemIn();
                                            $itemInModel->exchangeArray($itemInData);

                                            if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                                $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                                $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($singleBatchValue['itemOutID'],$usedQty + $singleBatchValue['itemOutReturnQty']);
                                            }    
                                        }
                                        if ($batchItemQty != 0) {
                                            $this->rollback();
                                            $this->status = false;
                                            $this->msg = $this->getMessage('ERR_INV_NOT_ENOUGH_QTY',array($pData['productCode'].' - '.$pData['productName']));
                                            return $this->JSONRespond();
                                        }
                                    }
                                    if($this->useAccounting == 1 && $p['documentTypeID'] != 4){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $batchValue['qtyByBase'] * $itemOutDetails->itemOutAverageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        } else {
                                            $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                }
                            } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                //Add details to item in table for serial products
                                if ($p['locationProductID'] != $locationProductID) {
                                    $sresult = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProductsBySerialCodeAndLocationProdutId($batchValue['serialCode'], $p['locationProductID']);
                                    $spdata = array(
                                        'locationProductID' => $locationProductID,
                                        'productBatchID' => NULL,
                                        'productSerialCode' => $batchValue['serialCode'],
                                        'productSerialExpireDate' => $sresult->productSerialExpireDate,
                                        'productSerialWarrantyPeriod' => $sresult->productSerialWarrantyPeriod,
                                        'productSerialWarrantyPeriodType' =>(int)$sresult->productSerialWarrantyPeriodType,
                                        'productSerialSold' => '0',
                                        'productSerialReturned' => '0',
                                        );
                                    $serialPr = new ProductSerial();
                                    $serialPr->exchangeArray($spdata);
                                    if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                    }
                                } else {
                                    $insertedSID = $batchValue['serialID'];
                                    $serialProductData = array(
                                        'productSerialSold' => '0',
                                    );
                                    
                                    if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                        $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $insertedSID);
                                    }
                                }

                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getSerialProductDetails($p['locationProductID'], $batchValue['serialID']);

                                if ($p['documentTypeID'] == 4) {
                                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Delivery Note', $p['documentID']);
                                } else {
                                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Sales Invoice', $invoiceID);
                                } 
                                
                                $unitPrice = $itemInDetails['itemInPrice'];
                                $unitDiscount = $itemInDetails['itemInDiscount'];
                                if($averageCostingFlag == 1){
                                    $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                    $unitDiscount = 0;

                                    $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')
                                                ->getLocationProduct($productID, $locationID);
                                    $locationProductAverageCostingPriceForUpdate = $locationProduct->locationProductAverageCostingPrice;
                                    $locationProductQuantityForUpdate = $locationProduct->locationProductQuantity;
                                        
                                    // calcutate current avg costing
                                    $newAvgCst = (floatval($unitPrice)) + (floatval($locationProductAverageCostingPriceForUpdate) * floatval($locationProductQuantityForUpdate))/(floatval($locationProductQuantityForUpdate) + 1);
                                        
                                    // update location product avg costing
                                    $locationProductAvgData = array(
                                        'locationProductAverageCostingPrice' => $newAvgCst,
                                    );

                                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductAvgData, $productID, $locationID);
                                }
                                if ($p['documentTypeID'] == 4 && !$inactiveFlag) {
                                    $deliveryNoteDetails = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteDetailsByDeliveryNoteID($p['documentID'],null,$itemOutDetails->itemOutSerialID)->current();
                                    
                                    $deliveryNoteSubProductID = $deliveryNoteDetails['deliveryNoteSubProductID'];
                                    
                                    $data = array(
                                            'deliveryNoteSubProductsCopiedQuantity' => 0
                                            ); 
                                    $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->updateDeliveryNoteSubProductData($data,$deliveryNoteSubProductID);

                                }

                                $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                $itemInData = array(
                                    'itemInIndex' => $newItemInIndex,
                                    'itemInDocumentType' => 'Credit Note',
                                    'itemInDocumentID' => $creditNoteID,
                                    'itemInLocationProductID' => $locationProductID,
                                    'itemInBatchID' => NULL,
                                    'itemInSerialID' => $insertedSID,
                                    'itemInQty' => 1,
                                    'itemInPrice' => $unitPrice,
                                    'itemInAverageCostingPrice' => $newAvgCst,
                                    'itemInDiscount' => $unitDiscount,
                                    'itemInDateAndTime' => $this->getGMTDateTime(),
                                );
                                $itemInModel = new ItemIn();
                                $itemInModel->exchangeArray($itemInData);

                                if(!($p['documentTypeID'] == 4 && !$inactiveFlag)) {
                                    if ((float)$itemOutDetails->itemOutReturnQty == 0) {
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID,'1');
                                        $returnItemCount = $returnItemCount - 1;

                                    } else {
                                        $this->rollback();
                                        $this->status = false;
                                        $this->msg = $this->getMessage('ERR_INV_SERIAL_ITEM_RETURN',array($pData['productCode'].' - '.$pData['productName'].'-serial code-'.$batchValue['serialCode']));
                                        return $this->JSONRespond();
                                    }
                                }
                            
                                if($this->useAccounting == 1 && $p['giftCard'] != 1 && $p['documentTypeID'] != 4){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = 1 * $itemOutDetails->itemOutAverageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = 1* ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }
                                
                                if ($p['giftCard'] == 1) {
                                    $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                                    $psCode = $checkSerial->productSerialCode;
                                    $giftCardDetails = $this->CommonTable('Inventory\Model\GiftCardTable')->getGiftCardDetailsByGiftCardCode($psCode);
                                    $giftData = array(
                                        'giftCardIssueDate' => '',
                                        'giftCardExpireDate' => '',
                                        'giftCardStatus' => 2,
                                    );

                                    if($p['documentTypeID'] != 4) {
                                        $this->CommonTable('Inventory\Model\GiftCardTable')->updateGiftCardDetailsByGiftCardCode($giftData, $psCode);
                                    }
                                }
                            }
                        }

                        
                        if($p['giftCard'] == 1){
                            if($this->useAccounting == 1 && empty($giftCardDetails->giftCardGlAccountID)){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $this->getMessage('ERR_GIFTCARD_GL_ACCOUNT_NOT_SET', [$psCode]);
                                return $this->JSONRespond();
                            }else{
                                        //set gl accounts for the journal entry
                                $pTotal = $p['productTotal'] - $p['pTax']['tTA'];
                                if($creditNoteData['creditNoteInvoiceDiscountType'] == "value"){
                                    $productTotal = $pTotal - $creditNoteData['creditNoteInvoiceDiscountAmount']*($pTotal/$totalProductAmounts);
                                }else {
                                    $productTotal = $pTotal;
                                }
                                if($creditNotePromotionDiscountType == 'value'){
                                    $productTotal = $productTotal - $creditNotePromotionDiscountAmount*($productTotal/$totalProductAmounts);
                                }

                                $totalCRAmounts+=$productTotal;
                                if(isset($accountProduct[$giftCardDetails->giftCardGlAccountID]['debitTotal'])){
                                    $accountProduct[$giftCardDetails->giftCardGlAccountID]['debitTotal'] += $productTotal;
                                }else{
                                    $accountProduct[$giftCardDetails->giftCardGlAccountID]['debitTotal'] = $productTotal;
                                    $accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'] = 0.00;
                                    $accountProduct[$giftCardDetails->giftCardGlAccountID]['accountID'] = $giftCardDetails->giftCardGlAccountID;
                                }
                            }
                        }
                    } else {
//                    $this->saveTransferProductData(array('batchID' => 0, 'serialID' => 0, 'qtyByBase' => $p['transferQuantity']['qtyByBase']), $trasferID, $productID);
                    }
                    //update Invoice products status

                    /////////////////////////////////////////////
                    $creditnoteProcuctDetails = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductDetailsByInvoiceProductID($invoiceProductID);
                    if (count($creditnoteProcuctDetails)) {
                        $productQty=0;
                        foreach ($creditnoteProcuctDetails as $creditnoteProduct) {
                            $productQty += $creditnoteProduct['creditNoteProductQuantity'];
                        }

                        $invoiceProductDetails = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getProductDetailsByInvoiceProductID($invoiceProductID)->current();

                        if ($productQty == $invoiceProductDetails['salesInvoiceProductQuantity']) {
                            $this->CommonTable('Invoice\Model\InvoiceProductTable')->updateCopiedSalesInvoiceProducts($invoiceProductID);
                        }
                    }

                    $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductID);
                    $locationPrTotalQty = 0;
                    $locationPrTotalPrice = 0;
                    $newItemInIds = array();
                    if(count($itemInData) > 0){
                        foreach ($itemInData as $key => $value) {
                            if($value->itemInID > $locationProductLastItemInID){
                                $newItemInIds[] = $value->itemInID; 
                                $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                if($remainQty > 0){
                                    $itemInPrice = $remainQty*$value->itemInPrice;
                                    $itemInDiscount = $remainQty*$value->itemInDiscount;
                                    $locationPrTotalQty += $remainQty;
                                    $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                                }
                                $locationProductLastItemInID = $value->itemInID; 
                            }
                        }
                    }
                    $locationPrTotalQty += $locationProductQuantity;
                    $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;
                    if($p['documentTypeID'] != 4) {
                        if($locationPrTotalQty > 0){
                            $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                            $lpdata = array(
                                'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                                'locationProductLastItemInID' => $locationProductLastItemInID,
                                );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationProductID);
                        
                            foreach ($newItemInIds as $inID) {
                                $intemInData = array(
                                    'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                    );
                                $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                            }
                        }
                    }
                }
                if(isset($accountProduct[$customerData->customerReceviableAccountID]['creditTotal'])){
                    $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] += $totalCRAmounts;
                }else{
                    $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] = $totalCRAmounts;
                    $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] = 0.00;
                    $accountProduct[$customerData->customerReceviableAccountID]['accountID'] = $customerData->customerReceviableAccountID;
                }
                
                //If all products of Invoice has used,Then updates Invoice status
                if (isset($invoiceID)) {
                    $this->_checkAndUpdateSalesInvoiceStatus($invoiceID);
                }
                // call product updated event
                $productIDs = array_map(function($element) {
                    return $element['productID'];
                }, $products);

                $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
                $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

                if ($useInvoiceReturn) {
                    foreach ($relatedInvoiceReturns as $key => $value) {


                        $crInvRtn = new CreditNoteInvoiceReturn();

                        $crInvRtnData = [
                            'creditNoteID' => $creditNoteID,
                            'invoiceReturnID' => $value
                        ];

                        $crInvRtn->exchangeArray($crInvRtnData);

                        $saveCrInvRtn = $this->CommonTable('Invoice\Model\CreditNoteInvoiceReturnTable')->saveCreditNoteInvoiceReturn($crInvRtn);

                        if ($saveCrInvRtn) {
                            $this->CommonTable('Invoice\Model\InvoiceReturnTable')->updateInvoiceReturnStatus($value, 4);
                        }
                        
                    }
                }

                if ($this->useAccounting == 1) {
                    //create data array for the journal entry save.
                    $i=0;
                    $journalEntryAccounts = array();

                    foreach ($accountProduct as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = '';
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Credit Note '.$creditNoteCode.'.';
                        $i++;
                    }
                    //get journal entry reference number.
                    $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                    $jelocationReferenceID = $jeresult['locRefID'];
                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                    $journalEntryData = array(
                        'journalEntryAccounts' => $journalEntryAccounts,
                        'journalEntryDate' => $date,
                        'journalEntryCode' => $JournalEntryCode,
                        'journalEntryTypeID' => '',
                        'journalEntryIsReverse' => 0,
                        'journalEntryComment' => 'Journal Entry is posted when create Credit Note '.$creditNoteCode.'.',
                        'documentTypeID' => 6,
                        'journalEntryDocumentID' => $creditNoteID,
                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                        );

                    $resultData = $this->saveJournalEntry($journalEntryData);
                    if(!$resultData['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg =$resultData['msg'];
                        $this->data =$resultData['data'];
                        return $this->JSONRespond();
                    }

                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($creditNoteID,6, 3);
                    if(!$jEDocStatusUpdate['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg =$jEDocStatusUpdate['msg'];
                        return $this->JSONRespond();
                    }

                    if ($invoiceID != '' && $invoiceID != 0 && $invoiceID != null) {
                        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(1,$invoiceID);
                        $dimensionData = [];
                        foreach ($jEDimensionData as $value) {
                            if (!is_null($value['journalEntryID'])) {
                                $temp = [];
                                $temp['dimensionTypeId'] = $value['dimensionType'];
                                $temp['dimensionValueId'] = $value['dimensionValueID'];
                                $dimensionData[$creditNoteCode][] = $temp;
                            }
                        }
                        if (!empty($dimensionData)) {
                            $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$creditNoteCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                            if(!$saveRes['status']){
                                $this->rollback();
                                $this->status = false;
                                $this->msg =$saveRes['msg'];
                                $this->data =$saveRes['data'];
                                return $this->JSONRespond();
                            }   
                        }
                    }
                }

                if ($creditNoteDirectType == 'true' && $invoiceID != '') {
                    $invoiceEditLogModel = new InvoiceEditLog();
                    $invoiceEditLogModel->exchangeArray([
                        'userID' => $this->userID,
                        'salesInvoiceID' => $invoiceID,
                        'dateAndTime' => $this->getGMTDateTime()
                    ]);
                    $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                    $newItemsData = [
                        'invoiceEditLogDetailsOldState' => $invoice->statusID,
                        'invoiceEditLogDetailsNewState' => 'Invoice closed with adding a direct credit note '.$creditNoteCode,
                        'invoiceEditLogDetailsCategory' => 'salesInvoiceStatusClosed'
                    ];

                    //close invoice state
                    if ($invCloseFlag) {
                        $state = '4'; //close state
                        $update = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceState($invoiceID, $state);

                        $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);
                        $newItemsData['invoiceEditLogID'] = $invoiceEditLastInsertID;
                        $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                        $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                    } else {
                        $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                        $invoiceTotalAmount = $invoiceDetails['salesinvoiceTotalAmount'];
                        $invoicePayedAmount = $invoiceDetails['salesInvoicePayedAmount'];

                        $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceID($invoiceID)->current();

                        $creditNoteAmount = 0;
                        if (!is_null($creditNoteTotalAmountForTheInvoice['creditNoteTot'])) {
                            $creditNoteAmount = $creditNoteTotalAmountForTheInvoice['creditNoteTot'];
                        }

                        $totalSettledAmountForInvoice = $creditNoteAmount + $invoicePayedAmount;
                        if ($totalSettledAmountForInvoice >= $invoiceTotalAmount) {
                            $state = '4'; //close state
                            $update = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceState($invoiceID, $state);

                            $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);
                            $newItemsData['invoiceEditLogID'] = $invoiceEditLastInsertID;
                            $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                            $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                        }

                    }
                }

                $this->commit();
                $this->status = true;
                $this->data = array('creditNoteID' => $creditNoteID);
                $this->msg = $this->getMessage('SUCC_CREDIT_PRODUCTDETAIL_ADD', array($creditNoteCode));
                $this->setLogMessage($customCurrencySymbol . $p['productTotal'] . ' amount credit Note Product details were successfully added - ' . $creditNoteCode);
                $this->flashMessenger()->addMessage($this->msg);
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CREDIT_VALID_CUST');
                return $this->JSONRespond();
            }
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CREDIT_PRODUCT_SAVE');
            return $this->JSONRespond();
        }
    }


    private function _checkAndUpdateDeliveryNoteStatus($deliveryNoteID)
    {
        $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getUnCopiedDeliveryNoteProducts($deliveryNoteID);
        if (!empty($deliveryNoteProducts)) {
            $statusID = 15;
            $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNoteStatus(
                    $deliveryNoteID, $statusID);
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * If all products of sales invoice has used for credit note
     * Then updates sales invoice status
     * @param int $invoiceID
     */
    private function _checkAndUpdateSalesInvoiceStatus($invoiceID)
    {
        $invoiceProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getUnCopiedSalesOrderProducts($invoiceID);
        if (empty($invoiceProducts)) {
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Invoice\Model\InvoiceTable')->updateSalesInvoiceStatus($invoiceID, $closeStatusID);
        }
    }

    function saveCreditNoteSubProductData($batchValue, $creditNoteProductID)
    {
        $data = array(
            'creditNoteProductID' => $creditNoteProductID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'creditNoteSubProductQuantity' => $batchValue['qtyByBase'],
        );
        $creditNoteSubProduct = new CreditNoteSubProduct;
        $creditNoteSubProduct->exchangeArray($data);
        $creditNoteSubProductID = $this->CommonTable('Invoice\Model\CreditNoteSubProductTable')->saveCreditNoteSubProduct($creditNoteSubProduct);
        return $creditNoteSubProductID;
    }

    public function retriveCustomerCreditNoteAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $cust_id = $request->getPost('customerID');
            $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCustomerIDAndLocationID($cust_id, $locationID);
            foreach ($creditNotes as $t) {
                $creditNoteArray[$t['creditNoteID']] = (object) $t;
            }
            $customerCreditNote = new ViewModel(array(
                'creditNote' => $creditNoteArray,
                'statuses' => $this->getStatusesList(),
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            ));
            $customerCreditNote->setTerminal(TRUE);
            $customerCreditNote->setTemplate('invoice/credit-note/credit-note-list');
            return $customerCreditNote;
        }
    }

    public function retriveCreditNoteFromCreditNoteIDAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $creditNotID = $searchrequest->getPost('CreditNoteID');
            $creditNote = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNotID);

            foreach ($creditNote as $t) {
                $creditNoteArray[$t['creditNoteID']] = (object) $t;
            }
            if ($creditNote->count() == 0) {
                return new JsonModel(array(FALSE));
            } else {
                $return = new ViewModel(array(
                    'creditNote' => $creditNoteArray,
                    'statuses' => $this->getStatusesList(),
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,
                ));
                $return->setTerminal(TRUE);
                $return->setTemplate('invoice/credit-note/credit-note-list');
                return $return;
            }
        }
    }

    public function retriveCreditNoteByDatefilterAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $fromdate = $request->getPost('fromdate');
            $todate = $request->getPost('todate');
            $customerID = $request->getPost('customerID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $filteredCreditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByDate($fromdate, $todate, $customerID, $locationID);
//            if ($filteredCreditNotes->count() == 0) {
//                return new JsonModel(array('msg' => $this->getMessage('SUCC_RETURNAPI_NORETURN')));
//            } else {
            foreach ($filteredCreditNotes as $t) {
                $creditNotesArray[$t['creditNoteID']] = (object) $t;
            }
            $DateFilteredCreditNote = new ViewModel(array(
                'creditNote' => $creditNotesArray,
                'statuses' => $this->getStatusesList(),
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            ));

            $DateFilteredCreditNote->setTerminal(TRUE);
            $DateFilteredCreditNote->setTemplate('invoice/credit-note/credit-note-list');
            return $DateFilteredCreditNote;
//            }
        }
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Credit Note';
        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_CRENOTAPI_SENT_EMAIL'); //TODO : create messages according to module
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_CRENOTAPI_SENT_EMAIL');
        return $this->JSONRespond();
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchActiveCreditNoteForDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $creditNoteSearchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');

            $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->searchActiveCreditNoteForDropdown($locationID, $creditNoteSearchKey);
            $creditNoteList = array();
            foreach ($creditNotes as $creditNote) {
                $temp['value'] = $creditNote['creditNoteID'];
                $temp['text'] = $creditNote['creditNoteCode'];
                $creditNoteList[] = $temp;
            }

            $this->data = array('list' => $creditNoteList);
            return $this->JSONRespond();
        }
    }

    /**
     * Search All Credit note for drop down
     * @return type
     */
    public function searchAllCreditNoteForDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $creditNoteSearchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');

            $creditNotes = $this->CommonTable('Invoice\Model\CreditNoteTable')->searchAllCreditNoteForDropdown($locationID, $creditNoteSearchKey);
            $creditNoteList = array();
            foreach ($creditNotes as $creditNote) {
                $temp['value'] = $creditNote['creditNoteID'];
                $temp['text'] = $creditNote['creditNoteCode'];
                $creditNoteList[] = $temp;
            }

            $this->data = array('list' => $creditNoteList);
            return $this->JSONRespond();
        }
    }
     /**
     * Use to save credit note by POS 
     * @param array $creditNoteDataSet
     * @param array $products
     * @param array $subProducts
     * @param string $locationID
     * @return array
     */

    public function saveCreditNote($creditNoteDataSet,$products,$subProducts,$locationID, $ignoreBudgetLimit = true)
    {

        //genarate credit note reference number for POS credit notes.
        $refData = $this->getReferenceNoForLocation(8, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];
        $creditNoteDataSet['creditNoteCode'] = $rid;
        $invoiceID = $creditNoteDataSet['invoiceID'];
        $date = $creditNoteDataSet['creditNoteDate'];
        $customerID = (!empty($creditNoteDataSet['customerID'])) ? $creditNoteDataSet['customerID'] : 0;

        $creditNote = new CreditNote;
        $creditNote->exchangeArray($creditNoteDataSet);
        $creditNoteID = $this->CommonTable('Invoice\Model\CreditNoteTable')
            ->saveCreditNote($creditNote);
        if(!$creditNoteID){
            return $data = array(
                'status' => false,
                'msg' => $this->getMessage('ERR_CREDIT_NT_SAVE'),
            );
        }
           
        //update credit Note reference Number in reference table.            
        if ($lrefID) {
            $this->updateReferenceNumber($lrefID);
        }

        $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
        $displayData = $result->current();
        $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
        $averageCostingFlag = $displayData['averageCostingFlag'];

        if($FIFOCostingFlag == 0 && $averageCostingFlag == 0){
            return array(
                'status' => false,
                'msg' => $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE'),
            );
        }

        $accountProduct = array();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountsData = $glAccountExist->current();
        $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        
        if($this->useAccounting == 1){
            if(empty($customerData->customerSalesAccountID)){
                return array(
                    "status" => false,
                    "msg" => $this->getMessage('ERR_CUSTOMER_SALES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                );
            }else if(empty($customerData->customerReceviableAccountID)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                );
            }
        }

        $creditNotePromotionDiscountType = $creditNoteDataSet['creditNotePromotionDiscountType'];
        $creditNotePromotionDiscountAmount = $creditNoteDataSet['creditNotePromotionDiscountAmount'];

        $totalCRAmounts = 0;
        $totalProductAmounts = 0;
        $totalTaxAmount = 0;
        foreach ($products as $keyt => $pt) {
            if(isset($pt['pTax']['tTA'])){
                $totalTaxAmount += $pt['pTax']['tTA']; 
            }
            $totalProductAmounts += $pt['productTotal'];
        }

        //update item by item details  in to the location product table, item in table and out table    
        foreach ($products as $key => $product) {
                   
            $batchProducts = $subProducts[$product['productID']];
            $productID = $product['productID'];
            $invoiceProductID = $product['invoiceproductID'];
            $loCreditNoteQty = $creditNoteQty = $product['creditNoteQuantity']['qty'];
            $productType = $product['productType'];
            //POS credit note use system currency
            $creditNoteCustomCurrencyRate = 1;

            //for non inventory items, because quantity isn't maintained
            if ($productType == 2) {
                $loCreditNoteQty = 0;
            }
               
            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')
                ->getLocationProduct($productID, $locationID);

            $locationProductID = $locationProduct->locationProductID;
           //get location Product Data for calculate avarage costing
            $locationProductAverageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
            $locationProductQuantity = $locationProduct->locationProductQuantity;
            $locationProductLastItemInID = $locationProduct->locationProductLastItemInID;

            if (!isset($locationProductID)) {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CREDIT_PROD_NOT_IN_LOCATION', array($product['productCode'])),
                );
            }

            if(is_null($locationProduct)) {
                return $data = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_NO_LOC_PRODUCT'),
                );
            }
            
            $locationProductData = array(
                'locationProductQuantity' => $locationProduct->locationProductQuantity + $loCreditNoteQty,
            );

            if ($product['productType'] != 2) {
            //update location product table    
                $locUpdate = $this->CommonTable('Inventory\Model\LocationProductTable')
                ->updateQty($locationProductData, $productID, $locationID);
                if(!$locUpdate){
                    return $data = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_LOCA_PRODUCT_SAVE'),
                        );
                }
            }

            if ($product['productDiscountType'] == 'value') {
                $product['productDiscount'] = $product['productDiscount'] * $creditNoteCustomCurrencyRate;
            }

            $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

            if($this->useAccounting == 1){
                if($product['giftCard'] != 1 && $product['productType'] != 2){
                    if(empty($pData['productInventoryAccountID'])){
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                        );
                    }
                         //check whether Product Cost of Goods Sold Gl account id set or not
                    if(empty($pData['productCOGSAccountID'])){
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName'])),
                        );
                    }
                }

                    //set gl accounts for the journal entry
                $pTotal = $product['productTotal'] ;
                if($creditNoteDataSet['creditNoteInvoiceDiscountType'] == "value"){
                    $productTotal = $pTotal - $creditNoteDataSet['creditNoteInvoiceDiscountAmount']*($pTotal/$totalProductAmounts);
                }else {
                    $productTotal = $pTotal;
                }
                if($creditNotePromotionDiscountType == 'value'){
                    $productTotal = $productTotal - $creditNotePromotionDiscountAmount*($productTotal/$totalProductAmounts);
                }

                if($product['giftCard'] != 1){
                    $totalCRAmounts+=$productTotal;

                    if(isset($accountProduct[$pData['productSalesAccountID']]['debitTotal'])){
                        $accountProduct[$pData['productSalesAccountID']]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productSalesAccountID']]['debitTotal'] = $productTotal;
                        $accountProduct[$pData['productSalesAccountID']]['creditTotal'] = 0.00;
                        $accountProduct[$pData['productSalesAccountID']]['accountID'] = $pData['productSalesAccountID'];
                    }



                    // if(isset($accountProduct[$customerData->customerSalesAccountID]['debitTotal'])){
                    //     $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] += $productTotal;
                    // }else{
                    //     $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] = $productTotal;
                    //     $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] = 0.00;
                    //     $accountProduct[$customerData->customerSalesAccountID]['accountID'] = $customerData->customerSalesAccountID;
                    // }
                }
            }

            $creditNoteProductData = array(
                'creditNoteID' => $creditNoteID,
                'invoiceProductID' => $invoiceProductID,
                'productID' => $productID,
                'creditNoteProductPrice' => $product['productPrice'] * $creditNoteCustomCurrencyRate,
                'creditNoteProductDiscount' => $product['productDiscount'],
                'creditNoteProductDiscountType' => $product['productDiscountType'],
                'creditNoteProductTotal' => $product['productTotal'] * $creditNoteCustomCurrencyRate,
                'creditNoteProductQuantity' => $creditNoteQty
            );

            //calculate discount value
            $discountValue = 0;
            if ($product['productDiscountType'] == "precentage") {
                $discountValue = ($product['productPrice'] / 100 * $product['productDiscount']) * $creditNoteCustomCurrencyRate;
            } else if ($product['productDiscountType'] == "value") {
                $discountValue = $product['productDiscount'] * $creditNoteCustomCurrencyRate;
            }

            $creditNoteProduct = new CreditNoteProduct;
            $creditNoteProduct->exchangeArray($creditNoteProductData);
            //update credit note product table
            $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                ->saveCreditNoteProducts($creditNoteProduct);
                    
            if(!$creditNoteProductID) {
                return $data = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CREDIT_NTE_PRODUCT_SAVE'),
                    );
            }
            // update credit note tax details   
            if ($product['pTax'] && count($product['pTax']) > 1) {
                $cNTax = $this->updateCreditNoteTax(
                    $product['pTax']['tL'],
                    $creditNoteProductID,
                    $creditNoteCustomCurrencyRate,
                    $accountProduct,
                    $totalCRAmounts
                    );
                if(!$cNTax['status']){
                    return $data = array(
                        'status' => $cNTax['status'],
                        'msg' => $cNTax['msg'],
                    );
                }else{
                    $totalCRAmounts = $cNTax['data']['totalCRAmounts'];
                    $accountProduct = $cNTax['data']['accountProduct'];
                }
            }
            // update non batch-serial items into the itemIn ,itemOut table              
            if (!count($batchProducts) > 0 && ($product['productType'] != 2)) {
                $nrmItemUpdate = $this->updateItemInOutTableForNormalItemByCreditNote(
                    $creditNoteQty,
                    $locationProduct,
                    $invoiceID,
                    $creditNoteID,
                    $averageCostingFlag,
                    $accountProduct,
                    $pData
                );

                if(!$nrmItemUpdate['status']){
                     return $data = array(
                        'satus' => $nrmItemUpdate['status'],
                        'msg' => $this->getMessage('ERR_CRDNT_NRML_ITEM_SAVE'),
                    );
                }else{
                    $accountProduct = $nrmItemUpdate['data']['accountProduct'];
                }
            }

            // update batch-serial item details into the itemIn and itemOut tables.
            if (count($batchProducts) > 0) {
                $updateItemTables = $this->updateItemInOutTableByCreditNote(
                    $batchProducts,
                    $creditNoteProductID, 
                    $locationProduct,
                    $product,
                    $creditNoteCustomCurrencyRate,
                    $creditNoteID,
                    $invoiceID,
                    $averageCostingFlag,
                    $accountProduct,
                    $pData,
                    $product['giftCard']
                );
                if(!$updateItemTables['status']){
                     return $data = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_CRDNT_B_S_ITEM_SAVE'),
                    );
                }else{
                    $giftCardDetails = $updateItemTables['data']['giftCardDetails'];
                    $giftCardCode = $updateItemTables['data']['psCode'];
                    $accountProduct = $updateItemTables['data']['accountProduct'];

                    if($product['giftCard'] == 1){
                        if($this->useAccounting == 1 && empty($giftCardDetails->giftCardGlAccountID)){
                            return array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_GIFTCARD_GL_ACCOUNT_NOT_SET', [$giftCardCode]),
                            );
                        }else{
                            //set gl accounts for the journal entry
                            $pTotal = $product['productTotal'];
                            if($creditNoteDataSet['creditNoteInvoiceDiscountType'] == "value"){
                                $productTotal = $pTotal - $creditNoteDataSet['creditNoteInvoiceDiscountAmount']*($pTotal/$totalProductAmounts);
                            }else {
                                $productTotal = $pTotal;
                            }
                            if($creditNotePromotionDiscountType == 'value'){
                                $productTotal = $productTotal - $creditNotePromotionDiscountAmount*($productTotal/$totalProductAmounts);
                            }

                            $totalCRAmounts+=$productTotal;
                            if(isset($accountProduct[$giftCardDetails->giftCardGlAccountID]['debitTotal'])){
                                $accountProduct[$giftCardDetails->giftCardGlAccountID]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$giftCardDetails->giftCardGlAccountID]['debitTotal'] = $productTotal;
                                $accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'] = 0.00;
                                $accountProduct[$giftCardDetails->giftCardGlAccountID]['accountID'] = $giftCardDetails->giftCardGlAccountID;
                            }
                        }
                    }
                }
            }
            
            //update Invoice products status
            $creditnoteProcuctDetails = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                ->getCreditNoteProductDetailsByInvoiceIDAndProductID($invoiceID, $productID);
        
            if (count($creditnoteProcuctDetails)) {
                $productQty=0;
            
                foreach ($creditnoteProcuctDetails as $creditnoteProduct) {
                    $productQty += $creditnoteProduct['creditNoteProductQuantity'];
                }

                $invoiceProductDetails = $this->CommonTable('Invoice\Model\InvoiceProductTable')
                    ->getProductDetailsByProductIDAndInvoiceID($invoiceID, $productID)->current();

                if ($productQty == $invoiceProductDetails['salesInvoiceProductQuantity']) {
                    $this->CommonTable('Invoice\Model\InvoiceProductTable')
                        ->updateCopiedSalesInvoiceProducts($productID, $invoiceID);
                }
            }

            $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductID);
            $locationPrTotalQty = 0;
            $locationPrTotalPrice = 0;
            $newItemInIds = array();
            if(count($itemInData) > 0){
                foreach ($itemInData as $key => $value) {
                    if($value->itemInID > $locationProductLastItemInID){
                        $newItemInIds[] = $value->itemInID; 
                        $remainQty = $value->itemInQty - $value->itemInSoldQty;
                        if($remainQty > 0){
                            $itemInPrice = $remainQty*$value->itemInPrice;
                            $itemInDiscount = $remainQty*$value->itemInDiscount;
                            $locationPrTotalQty += $remainQty;
                            $locationPrTotalPrice += $itemInPrice - $itemInDiscount; 
                        }
                        $locationProductLastItemInID = $value->itemInID; 
                    }
                }
            }
            $locationPrTotalQty += $locationProductQuantity;
            $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

            if($locationPrTotalQty > 0){
                $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                $lpdata = array(
                    'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                    'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationProductID);

                foreach ($newItemInIds as $inID) {
                    $intemInData = array(
                        'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                        );
                    $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                }
            }
        }

        if(isset($accountProduct[$customerData->customerReceviableAccountID]['creditTotal'])){
            $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] += $totalCRAmounts;
        }else{
            $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] = $totalCRAmounts;
            $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] = 0.00;
            $accountProduct[$customerData->customerReceviableAccountID]['accountID'] = $customerData->customerReceviableAccountID;
        }
             
        //If all products of Invoice has used,Then updates Invoice status
        if (isset($invoiceID)) {
            $this->_checkAndUpdateSalesInvoiceStatus($invoiceID);
        }

        // call product updated event
        $productIDs = array_map(function($element) {
            return $element['productID'];
                }, $products);

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        if ($this->useAccounting == 1) {
                    //create data array for the journal entry save.
            $i=0;
            $journalEntryAccounts = array();

            foreach ($accountProduct as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Credit Note '.$rid.'.';
                $i++;
            }
                    //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $date,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Credit Note '.$rid.'.',
                'documentTypeID' => 6,
                'journalEntryDocumentID' => $creditNoteID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
                );

            $resultData = $this->saveJournalEntry($journalEntryData);
            if(!$resultData['status']){
                return array(
                    'status' => false,
                    'msg' => $resultData['msg'],
                    'data' => $resultData['data'],
                );
            }

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($creditNoteID,6, 3);
            if(!$jEDocStatusUpdate['status']){
                return array(
                    'status'=> false,
                    'msg'=>$jEDocStatusUpdate['msg'],
                    );
            }
        }
        
        return $data = array(
            'creditNoteID' => $creditNoteID,
            'creditNoteCode' => $rid,
            'msg' => $this->getMessage('SUCC_CREDIT_PRODUCTDETAIL_ADD'), 
            'status' => true,
            );
                
    }
    /**
    * update credit Note tax 
    * @param array $taxData
    * @param int $creditNoteProductID
    * @param float $creditNoteCustomCurrencyRate
    * return booean
    */

    function updateCreditNoteTax($taxData, $creditNoteProductID, $creditNoteCustomCurrencyRate, $accountProduct, $totalCRAmounts)
    {
        foreach ($taxData as $taxKey => $productTax) {
            $creditNoteProductTaxData = array(
                'creditNoteProductID' => $creditNoteProductID,
                'taxID' => $taxKey,
                'creditNoteProductTaxPercentage' => $productTax['tP'],
                'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
            );
                
            $creditNoteProductTax = new CreditNoteProductTax();
            $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
        
            //save product tax into the creditNoteProductTax table
            $respond = $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')
                ->saveCreditNoteProductTax($creditNoteProductTax);

            if(!$respond){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CRDNT_TAX_SAVE'),
                );
            }

            $tData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxKey);
            if($this->useAccounting == 1 && empty($tData['taxSalesAccountID'])){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_TAX_SALES_ACCOUNT', array($tData['taxName'])),
                );
            }

            $totalCRAmounts += $productTax['tA'] * $creditNoteCustomCurrencyRate;
            //set tax gl accounts for the journal Entry
            if(isset($accountProduct[$tData['taxSalesAccountID']]['debitTotal'])){
                $accountProduct[$tData['taxSalesAccountID']]['debitTotal'] += $productTax['tA'] * $creditNoteCustomCurrencyRate;
            }else{
                $accountProduct[$tData['taxSalesAccountID']]['debitTotal'] = $productTax['tA'] * $creditNoteCustomCurrencyRate;
                $accountProduct[$tData['taxSalesAccountID']]['creditTotal'] = 0.00;
                $accountProduct[$tData['taxSalesAccountID']]['accountID'] = $tData['taxSalesAccountID'];
            }
            
        }
        
        return array(
            'status' => true,
            'data' => array(
                'accountProduct' => $accountProduct,
                'totalCRAmounts' => $totalCRAmounts,
            ),
        );       
    }
    /**
    * update itmIn ItemOut and Credit Note sub products tables for batch serial items.
    * @param array $batchProducts
    * @param int $creditNoteProductID
    * @param array $locationProduct
    * @param array $product
    * @param float $creditNoteCustomCurrencyRate
    * @param int $creditNoteID
    * @param int $invoiceID
    * return booean
    */


    function updateItemInOutTableByCreditNote(array $batchProducts, $creditNoteProductID, $locationProduct, array $product, $creditNoteCustomCurrencyRate, $creditNoteID, $invoiceID, $averageCostingFlag, $accountProduct, $pData, $giftCard)
    {
        foreach ($batchProducts as $batchKey => $batchValue) {
           
            //save creditNote sub products 
            if($batchValue['qtyByBase'] != 0){
                $result = $this->saveCreditNoteSubProductData($batchValue, $creditNoteProductID);
            }
        
            if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')
                    ->getBatchProducts($batchValue['batchID']);
                $productBatchQuentity = $batchProduct->productBatchQuantity + $batchValue['qtyByBase'];
                    
                $productBatchQty = array(
                    'productBatchQuantity' => $productBatchQuentity,
                );
                //update product batch quantity
                $this->CommonTable('Inventory\Model\ProductBatchTable')
                    ->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                            
                if ($batchValue['serialID']) {

                   $insertedSID = $batchValue['serialID'];
                   $serialProductData = array(
                    'productSerialSold' => '0',
                    );
                   $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $insertedSID);
                        
                    //Add details to itemIn table for batch and serial products
                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')
                        ->getBatchSerialProductDetails(
                            $locationProduct->locationProductID, 
                            $batchValue['batchID'], 
                            $batchValue['serialID']
                        );

                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'],$batchValue['batchID'], 'Sales Invoice', $invoiceID);

                    $unitPrice = $itemInDetails['itemInPrice'];
                    $unitDiscount = $itemInDetails['itemInDiscount'];
                    if($averageCostingFlag == 1){
                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                        $unitDiscount = 0;
                    }

                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')
                        ->getNewItemInIndex($itemInDetails['itemInID']);
                        
                    $itemInData = array(
                        'itemInIndex' => $newItemInIndex,
                        'itemInDocumentType' => 'Credit Note',
                        'itemInDocumentID' => $creditNoteID,
                        'itemInLocationProductID' => $locationProduct->locationProductID,
                        'itemInBatchID' => $batchValue['batchID'],
                        'itemInSerialID' => $batchValue['serialID'],
                        'itemInQty' => 1,
                        'itemInPrice' => $unitPrice,
                        'itemInDiscount' => $unitDiscount,
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                    );
                        
                    $itemInModel = new ItemIn();
                    $itemInModel->exchangeArray($itemInData);
                    //add batch-serial item to itemIn table. 
                    $res = $this->CommonTable('Inventory\Model\ItemInTable')
                        ->saveItemIn($itemInModel);
                    if(!$res){
                        return array('status' => false);
                    }    
                    //update itemOut table for batch-serial item                     
                    $outRes = $this->CommonTable('Inventory\Model\ItemOutTable')
                        ->updateItemOutReturnQty($itemOutDetails->itemOutID,'1');
                    if(!$outRes){
                        return array('status' => false);
                    }

                    if($this->useAccounting == 1){
                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                            $productTotal = 1 * $itemOutDetails->itemOutAverageCostingPrice;
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                            }

                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                            }
                        }else{
                            $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                            }

                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                            }
                        }
                    }
                } else {
                                   
                    //Add details to itemIn table for batch products
                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')
                        ->getBatchProductDetails(
                            $locationProduct->locationProductID, 
                            $batchValue['batchID']
                        );

                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetails($batchValue['batchID'], 'Sales Invoice', $invoiceID);

                    $unitPrice = $itemInDetails['itemInPrice'];
                    $unitDiscount = $itemInDetails['itemInDiscount'];
                    if($averageCostingFlag == 1){
                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                        $unitDiscount = 0;
                    }

                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')
                        ->getNewItemInIndex($itemInDetails['itemInID']);
                        
                    $itemInData = array(
                        'itemInIndex' => $newItemInIndex,
                        'itemInDocumentType' => 'Credit Note',
                        'itemInDocumentID' => $creditNoteID,
                        'itemInLocationProductID' => $locationProduct->locationProductID,
                        'itemInBatchID' => $batchValue['batchID'],
                        'itemInSerialID' => NULL,
                        'itemInQty' => $batchValue['qtyByBase'],
                        'itemInPrice' => $unitPrice,
                        'itemInDiscount' => $unitDiscount,
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                    );
                                                  
                    $itemInModel = new ItemIn();
                    $itemInModel->exchangeArray($itemInData);
                    //update itemIn table for batch item
                    $res = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                    if(!$res){
                        return array('status' => false);
                    }

                    //update itemOut table for batch item    
                    $res = $this->CommonTable('Inventory\Model\ItemOutTable')
                        ->updateItemOutReturnQty(
                            $itemOutDetails->itemOutID, 
                            $batchValue['qtyByBase'] + $itemOutDetails->itemOutReturnQty
                        );
                    if(!$res){
                        return array('status' => false);
                    }

                    if($this->useAccounting == 1){
                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                            $productTotal = $batchValue['qtyByBase'] * $itemOutDetails->itemOutAverageCostingPrice;
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                            }

                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                            }
                        }else{
                            $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                            }

                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                            }
                        }
                    }
                }
                
            } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {

                $insertedSID = $batchValue['serialID'];
                $serialProductData = array(
                    'productSerialSold' => '0',
                );
                $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $insertedSID);

                //Add details to itemIn table for serial products
                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')
                    ->getSerialProductDetails(
                        $locationProduct->locationProductID, 
                        $batchValue['serialID']
                    );

                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Sales Invoice', $invoiceID);

                $unitPrice = $itemInDetails['itemInPrice'];
                $unitDiscount = $itemInDetails['itemInDiscount'];
                if($averageCostingFlag == 1){
                    $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                    $unitDiscount = 0;
                }

                $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')
                    ->getNewItemInIndex($itemInDetails['itemInID']);
                    
                $itemInData = array(
                    'itemInIndex' => $newItemInIndex,
                    'itemInDocumentType' => 'Credit Note',
                    'itemInDocumentID' => $creditNoteID,
                    'itemInLocationProductID' => $locationProduct->locationProductID,
                    'itemInBatchID' => NULL,
                    'itemInSerialID' => $batchValue['serialID'],
                    'itemInQty' => 1,
                    'itemInPrice' => $unitPrice,
                    'itemInDiscount' => $unitDiscount,
                    'itemInDateAndTime' => $this->getGMTDateTime(),
                );
                    
                $itemInModel = new ItemIn();
                $itemInModel->exchangeArray($itemInData);
                //add serial items to the ItemIn table
                $res = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                if(!$res){
                    return array('status' => false);
                }
               
                //update itemOut table for serial item.    
                $res = $this->CommonTable('Inventory\Model\ItemOutTable')
                    ->updateItemOutReturnQty($itemOutDetails->itemOutID,'1');
                if(!$res){
                    return array('status' => false);
                }

                if($this->useAccounting == 1 && $giftCard != 1){
                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                        $productTotal = 1 * $itemOutDetails->itemOutAverageCostingPrice;
                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                        }else{
                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                        }

                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                        }else{
                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                        }
                    }else{
                        $productTotal = 1* ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                        if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                        }else{
                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                        }

                        if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                        }else{
                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                        }
                    }
                }

                if ($giftCard == 1) {
                    $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                    $psCode = $checkSerial->productSerialCode;
                    $giftCardDetails = $this->CommonTable('Inventory\Model\GiftCardTable')->getGiftCardDetailsByGiftCardCode($psCode);
                    $giftData = array(
                        'giftCardIssueDate' => '',
                        'giftCardExpireDate' => '',
                        'giftCardStatus' => 2,
                        );

                    $this->CommonTable('Inventory\Model\GiftCardTable')->updateGiftCardDetailsByGiftCardCode($giftData, $psCode);

                }
            }
        }

        if($giftCard != 1){
            $giftCardDetails = '';
            $psCode = '';
        }

        return array(
            'status' => true,
            'data' => array('giftCardDetails' => $giftCardDetails, 'psCode' => $psCode, 'accountProduct' => $accountProduct),
            );
    }
    /**
    * update itemIn and ItemOut tables for non batch serial items
    * @param int $creditNoteQty
    * @param int $locationProduct
    * @param int $invoiceID
    * @param int $creditNoteID
    * return boolean
    */
    function updateItemInOutTableForNormalItemByCreditNote($creditNoteQty, $locationProduct, $invoiceID, $creditNoteID, $averageCostingFlag, $accountProduct,$pData)
    {
        $itemReturnQty = $creditNoteQty;
    
        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')
            ->getItemOutDetailsForProductByDocument(
                $locationProduct->locationProductID, 
                'Sales Invoice', 
                $invoiceID
            );
            
        foreach (array_reverse($itemOutDetails) as $itemOutData) {
            if ($itemReturnQty != 0) {
                if ($itemOutData->itemOutQty > $itemOutData->itemOutReturnQty) {
                    $itemInPreDetails = $this->CommonTable('Inventory\Model\ItemInTable')
                        ->getItemInDetails(
                            $itemOutData->itemOutItemInID
                        );
                    $leftQty = floatval($itemOutData->itemOutQty) - floatval($itemOutData->itemOutReturnQty);
                    $itemInInsertQty = 0;
                    $itemOutUpdateReturnQty = 0;
                        
                    if ($leftQty >= $itemReturnQty) {
                        $itemInInsertQty = $itemReturnQty;
                        $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) + floatval($itemReturnQty);
                        $itemReturnQty = 0;
                    } else {
                        $itemInInsertQty = $leftQty;
                        $itemOutUpdateReturnQty = $itemOutData->itemOutQty;
                        $itemReturnQty = floatval($itemReturnQty) - floatval($leftQty);
                    }
                        
                    //hit data to the item in table with left qty
                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')
                        ->getNewItemInIndex(
                            $itemInPreDetails['itemInIndexID']);
                    $itemInData = array(
                        'itemInIndex' => $newItemInIndex,
                        'itemInDocumentType' => 'Credit Note',
                        'itemInDocumentID' => $creditNoteID,
                        'itemInLocationProductID' => $locationProduct->locationProductID,
                        'itemInBatchID' => NULL,
                        'itemInSerialID' => NULL,
                        'itemInQty' => $itemInInsertQty,
                        'itemInPrice' => $itemInPreDetails['itemInPrice'],
                        'itemInDiscount' => $discountValue,
                        'itemInDateAndTime' => $this->getGMTDateTime(),
                    );
                        
                    $itemInModel = new ItemIn();
                    $itemInModel->exchangeArray($itemInData);
                    $respond = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                    if(!$respond){
                        return array('status' => false);
                    }
                        
                    //update item out return qty
                    $itOutResp = $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                    
                    if(!$itOutResp){
                        return array('status' => false);
                    }

                    if($this->useAccounting == 1){
                        if($averageCostingFlag == 1){
                            //set gl accounts for the journal entry
                            $productTotal = $itemInInsertQty * $itemOutData->itemOutAverageCostingPrice;
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                            }

                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                            }
                        }else{
                            $productTotal = $itemInInsertQty * ($itemInPreDetails['itemInPrice'] - $itemInPreDetails['itemInDiscount']);
                            if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                            }

                            if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                            }else{
                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                            }
                        }
                    }
                }
            
            } else {
                break;
            }
        }
        return array(
            'status' => true,
            'data' => array('accountProduct' => $accountProduct)
        );
    }

    public function deleteCreditNoteAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            return $this->JSONRespond();
        }

        $creditNoteID = $request->getPost('creditNoteID');
        $creditNoteTypeFlag = $request->getPost('creditNoteTypeFlag');
        $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            
        $getCreditNoteData = $this->CommonTable("Invoice\Model\CreditNoteTable")->getCreditNoteByIdForDeleteCreditNote($creditNoteID);

        //chek cancel falg for cancel
        if (!$getCreditNoteData->ableToCancel) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CANNOT_CANCEL_CREDIT_NOTE');
            return $this->JSONRespond();
        }

        // to avoid cancel already canceled creditNote
        if ($getCreditNoteData->statusID == 5) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CANCEL_CANCELED_CREDIT_NOTE');
            return $this->JSONRespond();
        }

        //this condition use to avoid replaced creditNote delete problem by two tabs
        if($getCreditNoteData->statusID == 10){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_DELETE_EDITED_CREDIT_NOTE');
            return $this->JSONRespond();
        }

        //check credit notes payment that related given creditnote id. if its true then cannot delete.
        $creditNotePaymentDetails = $this->CommonTable('Invoice\Model\CreditNotePaymentDetailsTable')->getCreditNotePaymentsByCreditNoteID($creditNoteID)->current();
            
        if(!empty($creditNotePaymentDetails)){

            $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentsByCreditNotePaymentID($creditNotePaymentDetails['creditNotePaymentID'])->current();
            if ($creditNotePaymentData) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DELETE_CREDIT_NOTED_HAS_PAYMENT');
                return $this->JSONRespond();
            }
        }

        $directCreditNoteFlag = false;
        $buyBackCreditNoteFlag = false;
        $directCreditNoteLinkWithInvoiceFlag = false; 
        
        $this->beginTransaction();

        if ($getCreditNoteData->creditNoteDirectFlag == 1 && $getCreditNoteData->invoiceID == 0) {
            $directCreditNoteFlag = true;
        } else if ($getCreditNoteData->creditNoteDirectFlag == 1 && $getCreditNoteData->invoiceID != 0) {
            $directCreditNoteLinkWithInvoiceFlag = true;
        } 

        if ($getCreditNoteData->creditNoteBuyBackFlag == 1 && $getCreditNoteData->invoiceID == 0) {

            $buyBackCreditNoteFlag = true;
        }

        $invoiceID = $getCreditNoteData->invoiceID;
        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($getCreditNoteData->customerID);
        $creditNoteTotal = $getCreditNoteData->creditNoteTotal;
        
        if ($directCreditNoteFlag) {
            $customerCreditBalance = $cust->customerCurrentCredit - $creditNoteTotal;
            $customerCurrentBalance = $cust->customerCurrentBalance;

            $creditNoteDataSet = $this->checkAndGetDirectCreditNoteDataSetByCreditNoteID($creditNoteID,$invoiceID,$getCreditNoteData->locationID);

            $reverseItems = $this->reverseDirectCreditNoteProducts($creditNoteID, $creditNoteDataSet);
            if(!$reverseItems['status']){
                $this->rollback();
                $this->status = false;
                $this->msg = $reverseItems['msg'];
                return $this->JSONRespond();
            }
        } else if ($directCreditNoteLinkWithInvoiceFlag) {

            if($getCreditNoteData->outstandingDeviation == 0 && $getCreditNoteData->creditDeviation > 0){
                $customerCreditBalance = $cust->customerCurrentCredit - $getCreditNoteData->creditDeviation;
                $customerCurrentBalance = $cust->customerCurrentBalance;
            } else if($getCreditNoteData->outstandingDeviation < 0 && $getCreditNoteData->creditDeviation > 0){
                $customerCurrentBalance = abs($getCreditNoteData->outstandingDeviation);
                $customerCreditBalance = $cust->customerCurrentCredit - $getCreditNoteData->creditDeviation;
            } else if($getCreditNoteData->outstandingDeviation < 0 && $getCreditNoteData->creditDeviation == 0) {
                $customerCurrentBalance = $cust->customerCurrentBalance + abs($getCreditNoteData->outstandingDeviation);
                $customerCreditBalance = $cust->customerCurrentCredit;
            }

        } elseif ($buyBackCreditNoteFlag) {
            $customerCreditBalance = $cust->customerCurrentCredit - $creditNoteTotal;
            $customerCurrentBalance = $cust->customerCurrentBalance;

            $creditNoteDataSet = $this->checkAndGetDirectCreditNoteDataSetByCreditNoteID($creditNoteID,$invoiceID,$getCreditNoteData->locationID);

            $reverseItems = $this->reverseBuyBackCreditNoteProducts($creditNoteID, $creditNoteDataSet);
            if(!$reverseItems['status']){
                $this->rollback();
                $this->status = false;
                $this->msg = $reverseItems['msg'];
                return $this->JSONRespond();
            }
        } else {
            $creditNoteDataSet = $this->checkAndGetDirectCreditNoteDataSetByCreditNoteID($creditNoteID,$invoiceID,$getCreditNoteData->locationID);
            
            $result = $this->checkCreditNoteForCancel($creditNoteID, $invoiceID, $creditNoteDataSet);
            
            if (!$result['status']) {
                $this->rollback();
                $this->status =  false;
                $this->msg =  $this->getMessage('ERR_CANCEL_CREDIT_NOTE_PRODUCT');
                return $this->JSONRespond();
            }
    
            $updateCreditNote = $this->updateCreditNoteRelatedDetails($invoiceID, $creditNoteDataSet);
            
            if(!$updateCreditNote['status']){
                $this->rollback();
                $this->status = false;
                $this->msg = $updateCreditNote['msg'];
                return $this->JSONRespond();
            }

            //reverse existing items
            $reverseExistsItems = $this->reverseCreditNoteProducts($creditNoteID, $creditNoteDataSet ,$invoiceID);
            if(!$reverseExistsItems['status']){
                $this->rollback();
                $this->status = false;
                $this->msg = $reverseExistsItems['msg'];
                return $this->JSONRespond();
            }
            if($getCreditNoteData->outstandingDeviation == 0 && $getCreditNoteData->creditDeviation > 0){
                $customerCreditBalance = $cust->customerCurrentCredit - $getCreditNoteData->creditDeviation;
                $customerCurrentBalance = $cust->customerCurrentBalance;
            } else if($getCreditNoteData->outstandingDeviation < 0 && $getCreditNoteData->creditDeviation > 0){
                $customerCurrentBalance = $cust->customerCurrentBalance + abs($getCreditNoteData->outstandingDeviation);
                $customerCreditBalance = $cust->customerCurrentCredit - $getCreditNoteData->creditDeviation;
            } else if($getCreditNoteData->outstandingDeviation > 0 && $getCreditNoteData->creditDeviation == 0) {
                $customerCurrentBalance = $cust->customerCurrentBalance + $getCreditNoteData->outstandingDeviation;
                $customerCreditBalance = $cust->customerCurrentCredit;
            }

        }

        // for update juornal entry records first we need to check that use accounting
        if($this->useAccounting){
            $updateJournalAccount = $this->journalEntryreverseByCreditNote(
                    $getCreditNoteData->creditNoteID,
                    $getCreditNoteData->creditNoteCode,
                    $getCreditNoteData->locationID,
                    $ignoreBudgetLimit
                );

            if(!$updateJournalAccount['status']){
                $this->rollback();
                $this->status =  false;
                $this->msg =  $updateJournalAccount['msg'];
                $this->data =  $updateJournalAccount['data'];
                return $this->JSONRespond();
            }
        }

        //updating customer credit and outstanding balance
        $custData = array(
            'customerID' => $getCreditNoteData->customerID,
            'customerCurrentCredit' => $customerCreditBalance,
            'customerCurrentBalance' => $customerCurrentBalance,
        );

        $customer = new Customer;
        $customer->exchangeArray($custData);
        $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCurrentAndCreditBalance($customer);

        $statusID = 5;
        $updateCreditNoteStatus = $this->CommonTable('Invoice\Model\CreditNoteTable')->updateCreditNoteStatusForCancel($creditNoteID, $statusID);

        if(!$updateCreditNoteStatus){
            $this->rollback();
            $this->status =  false;
            $this->msg =  $this->getMessage('ERR_UPDATE_CREDIT_NOTE_STATE');
            return $this->JSONRespond();
        }

        if ($directCreditNoteLinkWithInvoiceFlag) {
            $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceIDForOpenInvoice($invoiceID)->current();

            $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
            
            $invoiceTotalAmount = $invoiceDetails['salesinvoiceTotalAmount'];
            $invoicePayedAmount = $invoiceDetails['salesinvoicePayedAmount'];

            $resToPay = $invoiceTotalAmount - $invoicePayedAmount - $creditNoteTotalAmountForTheInvoice['creditNoteTot'];

            if ($resToPay > 0) {
                $state = '3'; //open state
                $update = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceState($invoiceID, $state);
            }

        }
        //update entityTable as deleted.
        $updateEntity = $this->updateDeleteInfoEntity($getCreditNoteData->entityID);
        $this->commit();
        $this->setLogMessage('Credit Note '.$getCreditNoteData->creditNoteCode.' is deleted.');
        $this->status =  true;
        $this->msg =  $this->getMessage('SUCC_CREDIT_NOTE_CANCELED');
        return $this->JSONRespond();
            
            
    }

    private function checkAndGetDirectCreditNoteDataSetByCreditNoteID($creditNoteID , $invoiceID, $locationID)
    {
        $creditNoteProductDetails = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteDetailsByCreditNoteID($creditNoteID, $locationID);
        
        $creditNoteBatch = [];
        $creditNoteSerial = [];
        $creditNoteNormalItem = [];
        $piBatchDetails = [];
        $piSerialDetails = [];
        $piNormalItems = [];

        $creditNoteProducts = [];
        foreach ($creditNoteProductDetails as $value) {
            if($value['productSerialID'] != ''){
                $creditNoteSerial[$value['productSerialID']] = $value;
            } else if($value['productBatchID'] != '') {
                $creditNoteBatch[$value['productBatchID']] = $value;
            } else {
                $creditNoteNormalItem[$value['creditNoteProID']] = $value;
            }
        }

        $returnDataSet = array(
            'creditNoteBatch' => $creditNoteBatch,
            'creditNoteSerial' => $creditNoteSerial,
            'creditNoteNormalItem' => $creditNoteNormalItem,
            );

        return $returnDataSet;
    }

    private function updateCreditNoteRelatedDetails($invoiceID, $creditNoteDataSet)
    {
        //update normal item data set
        if(!empty($creditNoteDataSet['creditNoteNormalItem'])){
            foreach ($creditNoteDataSet['creditNoteNormalItem'] as $key => $normalValue) {
                $data = array(
                    'copied' => 0
                    );
                //update sale invoice product table
                $updateNormalItem = $this->CommonTable('Invoice\Model\InvoiceProductTable')->updateSalesInvoicePoductStaus($data, $normalValue['invoiceProductID']);
                if(!$updateNormalItem) {
                    $respondSet = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_SALES_INVOICE_PRODUCT_UPDATE'),
                        );
                    return $respondSet;
                }
                $salesInvoiceOpenFlag = true;
            }

        }
        //update batch details
        if(!empty($creditNoteDataSet['creditNoteBatch'])){
            foreach ($creditNoteDataSet['creditNoteBatch'] as $key => $batchUpdateValue) {
                $data = array(
                    'copied' => 0
                    );
                //update sale invoice product table
                $updateNormalItem = $this->CommonTable('Invoice\Model\InvoiceProductTable')->updateSalesInvoicePoductStaus($data, $batchUpdateValue['invoiceProductID']);
                if(!$updateNormalItem) {
                    $respondSet = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_SALES_INVOICE_PRODUCT_UPDATE'),
                        );
                    return $respondSet;
                }
                $salesInvoiceOpenFlag = true;
            }

        }

        //update serial grn data set
        if(!empty($creditNoteDataSet['creditNoteSerial'])){
            foreach ($creditNoteDataSet['creditNoteSerial'] as $key => $serialUpdateValue) {
                $data = array(
                    'copied' => 0
                    );
                //update sale invoice product table
                $updateNormalItem = $this->CommonTable('Invoice\Model\InvoiceProductTable')->updateSalesInvoicePoductStaus($data, $serialUpdateValue['invoiceProductID']);
                if(!$updateNormalItem) {
                    $respondSet = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_SALES_INVOICE_PRODUCT_UPDATE'),
                        );
                    return $respondSet;
                }
                $salesInvoiceOpenFlag = true;
            }

        }

        if($salesInvoiceOpenFlag){
            $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
            
            $invoiceTotalAmount = $invoiceDetails['salesinvoiceTotalAmount'];
            $invoicePayedAmount = $invoiceDetails['salesInvoicePayedAmount'];

            if ($invoicePayedAmount < $invoiceTotalAmount) {
                //update sales invoice status as open
                $openState = 3;
                $updateInvoiceState = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceState($invoiceID, $openState);

                if(!$updateInvoiceState){
                    $respondSet = array(
                        'status' => false,
                        'msg' => $this->getMessage('ERR_SALES_INVOICE_STATUS_UPDATE'),
                    );
                    return $respondSet;
                }
            }
         }


        $respondSet = array(
            'status' => true,
        );

        return $respondSet;
    }

    private function journalEntryreverseByCreditNote($creditNoteID, $creditNoteCode, $locationID, $ignoreBudgetLimit = true)
    {
        /**
        * get journal entry details by given id and document id =6
        * because creditNote document id is 6
        */

        $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('6', $creditNoteID);
        $journalEntryID = $journalEntryData['journalEntryID'];
       
        $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);


        $i=0;
        $journalEntryAccounts = array();
        foreach ($jEAccounts as $key => $value) {
            $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
            $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
            $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
            $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
            $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Credit Note Delete '.$creditNoteCode.'.';
            $i++;
        }

        //get journal entry reference number.
        $jeresult = $this->getReferenceNoForLocation('30', $locationID);
        $jelocationReferenceID = $jeresult['locRefID'];
        $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

        $journalEntryData = array(
            'journalEntryAccounts' => $journalEntryAccounts,
            'journalEntryDate' => $this->convertDateToStandardFormat($journalEntryData['journalEntryDate']),
            'journalEntryCode' => $JournalEntryCode,
            'journalEntryTypeID' => '',
            'journalEntryIsReverse' => 0,
            'journalEntryComment' => 'Journal Entry is posted when cancel Credit Note '.$creditNoteCode.'.',
            'documentTypeID' => 6,
            'journalEntryDocumentID' => $creditNoteID,
            'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );

        $resultData = $this->saveJournalEntry($journalEntryData);

        if(!$resultData['status']){
           return ['status' => false, 'msg' => $resultData['msg'], 'data' => $resultData['data']];
        }

        $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($creditNoteID,6, 5);
        if(!$jEDocStatusUpdate['status']){
            return array(
                'status'=> false,
                'msg'=>$jEDocStatusUpdate['msg'],
                );
        } 

        $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(6,$creditNoteID);
        $dimensionData = [];
        foreach ($jEDimensionData as $value) {
            if (!is_null($value['journalEntryID'])) {
                $temp = [];
                $temp['dimensionTypeId'] = $value['dimensionType'];
                $temp['dimensionValueId'] = $value['dimensionValueID'];
                $dimensionData[$creditNoteCode][] = $temp;
            }
        }
        if (!empty($dimensionData)) {
            $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$creditNoteCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
            if(!$saveRes['status']){
                return ['status' => false, 'msg' => $saveRes['msg'], 'data' => $saveRes['data']];
            }   
        }

        return ['status' => true];
    }


    private function reverseCreditNoteProducts($creditNoteID, $dataSet, $invoiceID)
    {
        $docType = 'Credit Note';

        // reverse serial products
        if(sizeof($dataSet['creditNoteSerial']) > 0){
            $batchSerialQty = [];
            foreach ($dataSet['creditNoteSerial'] as $key => $serialDataSet) {
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID(NULL, $serialDataSet['productSerialID']);
                $serialSoldFlag = false;
                $this->productIds[] = $serialDataSet['productID'];

                if ($serialDataSet['documentTypeID'] == 4) {
                    $deliveryNoteDetails = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteDetailsByDeliveryNoteID($serialDataSet['salesInvoiceProductDocumentID'],null,$serialDataSet['productSerialID'])->current();

                        $productQtyDiff = $deliveryNoteDetails['deliveryProductSubQuantity'] - $deliveryNoteDetails['deliveryNoteSubProductsCopiedQuantity'];
                        
                        if ($productQtyDiff != 0) {
                            $deliveryNoteSubProductID = $deliveryNoteDetails['deliveryNoteSubProductID'];
                            $deliveryNoteCopiedQuantity = $deliveryNoteDetails['deliveryNoteSubProductsCopiedQuantity'];
                            $newCopiedQuantity = $deliveryNoteCopiedQuantity + $serialDataSet['creditNoteSubProductQuantity'];
                            
                            $data = array(
                                    'deliveryNoteSubProductsCopiedQuantity' => $newCopiedQuantity
                                    ); 
                            $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->updateDeliveryNoteSubProductData($data,$deliveryNoteSubProductID);

                            $deliveryNoteID = $deliveryNoteDetails['deliveryNoteID'];

                            $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($deliveryNoteID, $serialDataSet['productID']);

                            if (isset($deliveryNoteProducts)) {
                                $newCopiedQuantity = $deliveryNoteProducts->deliveryNoteProductCopiedQuantity + $serialDataSet['creditNoteSubProductQuantity'];
                                $copiedflag = 0;
                                if ($newCopiedQuantity == $deliveryNoteProducts->deliveryNoteProductQuantity) {
                                    $copiedflag = 1;
                                }
                                $deliveryNoteProData = array(
                                    'copied' => $copiedflag,
                                    'deliveryNoteProductCopiedQuantity' => $newCopiedQuantity,
                                    );

                                $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryNoteProducts($serialDataSet['productID'], $deliveryNoteID, $deliveryNoteProData);
                                $this->_checkAndUpdateDeliveryNoteClosedStatus($deliveryNoteID);

                            }
                        }                
                } else {
                    foreach ($itemInData as $key => $serialDta) {
                        if($serialDta['itemInSoldQty'] == 0){
                            if($serialDta['itemInBatchID'] != ''){
                                $batchSerialQty[$serialDta['itemInBatchID']] ++;
                            }
                            $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($serialDataSet['productID'], $serialDataSet['locationID'])->current();
                            $returnQty = '1';
                            $serialSoldFlag = true;
                            //update locationproduct ItemIn and ItemOut
                            $locProStatue = $this->updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $serialDta, $returnQty,'CreditNote cancel');
                            if($locProStatue['status'] != 'true'){
                                $errorData = array(
                                    'msg' => $locProStatue['msg'],
                                    'status' => false,
                                );
                                return $errorData;
                            }

                            //update serial table
                            $data = array(
                                'productSerialSold' => '1',
                                );

                            $updateSerialTable = $this->CommonTable('Inventory\Model\ProductSerialTable')
                                    ->updateProductSerialData($data, $serialDta['itemInSerialID']);


                        }
                    }
                    if(!$serialSoldFlag){
                        $errorData = array(
                            'msg' => $this->getMessage('ERR_SERIAL_PRODUCT_IN_USE_CN'),
                            'status' => false,
                            );
                        return $errorData;
                    }
                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocumentAndSerialID($serialDataSet['locationProductID'], 'Sales Invoice', $invoiceID, $serialDataSet['productSerialID']);

                    $creditNoteProductQuantity = $serialDataSet['creditNoteProductQuantity'];
                    foreach ($itemOutDetails as $itemOutData) {
                        $itemOutUpdateReturnQty = 0;
                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                    }
                }

            }
        }
        
        //reverse batch products
        if(sizeof($dataSet['creditNoteBatch']) > 0) {

            foreach ($dataSet['creditNoteBatch'] as $key => $batchValue) {
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID($batchValue['productBatchID'],null);
                
                $totalUpdatedQty = 0;

                $this->productIds[] = $batchValue['productID'];

                if ($batchValue['documentTypeID'] == 4) {
                    $deliveryNoteDetails = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteDetailsByDeliveryNoteID($batchValue['salesInvoiceProductDocumentID'],$batchValue['productBatchID'],null)->current();

                    $productQtyDiff = $deliveryNoteDetails['deliveryNoteSubProductsCopiedQuantity'] + $batchValue['creditNoteSubProductQuantity'];
                    
                    if ($productQtyDiff <= $deliveryNoteDetails['deliveryProductSubQuantity']) {
                        $deliveryNoteSubProductID = $deliveryNoteDetails['deliveryNoteSubProductID'];
                        $deliveryNoteCopiedQuantity = $deliveryNoteDetails['deliveryNoteSubProductsCopiedQuantity'];
                        $newCopiedQuantity = $deliveryNoteCopiedQuantity + $batchValue['creditNoteSubProductQuantity'];
                        
                        $data = array(
                                'deliveryNoteSubProductsCopiedQuantity' => $newCopiedQuantity
                                ); 
                        $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->updateDeliveryNoteSubProductData($data,$deliveryNoteSubProductID);

                        $deliveryNoteID = $deliveryNoteDetails['deliveryNoteID'];

                        $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($deliveryNoteID, $batchValue['productID']);

                        if (isset($deliveryNoteProducts)) {
                            $newCopiedQuantity = $deliveryNoteProducts->deliveryNoteProductCopiedQuantity + $batchValue['creditNoteSubProductQuantity'];
                            $copiedflag = 0;
                            if ($newCopiedQuantity == $deliveryNoteProducts->deliveryNoteProductQuantity) {
                                $copiedflag = 1;
                            }
                            $deliveryNoteProData = array(
                                'copied' => $copiedflag,
                                'deliveryNoteProductCopiedQuantity' => $newCopiedQuantity,
                                );

                            $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryNoteProducts($batchValue['productID'], $deliveryNoteID, $deliveryNoteProData);
                            $this->_checkAndUpdateDeliveryNoteClosedStatus($deliveryNoteID);

                        }
                    }                
                } else {
                    $remQty = $batchValue['creditNoteSubProductQuantity'];
                    foreach ($itemInData as $key => $itemInBatchValue) {
                        //get remaining qty in the current item in record
                        $currentBatchRemQty = $itemInBatchValue['itemInQty'] - $itemInBatchValue['itemInSoldQty'];
                        if ($currentBatchRemQty > 0  && $remQty != 0 ) {

                            if ($remQty > $currentBatchRemQty) {
                                $returnQty = floatval($currentBatchRemQty);
                                $remQty = $remQty - $currentBatchRemQty;
                                
                            } else {
                                $returnQty = $remQty;
                                $remQty = 0; 
                            }
                            $totalUpdatedQty += $returnQty;

                            $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($batchValue['productID'], $batchValue['locationID'])->current();

                            //update locationproduct and ItemIn 
                            $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $itemInBatchValue, $returnQty,'CreditNote cancel');
                            if($locUpdateStatus['status'] != 'true'){
                                $errorData = array(
                                    'msg' => $locUpdateStatus['msg'],
                                    'status' => false,
                                );
                                return $errorData;
                            }

                            
                            $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($itemInBatchValue['itemInBatchID']);
                            $newBatchQty = floatval($batchDetails->productBatchQuantity) - floatval($returnQty);

                            //update batch table
                            if($batchValue['itemInSerialID'] == ''){
                                $data = array(
                                    'productBatchQuantity' => $newBatchQty,
                                );
                                $updateBatchTable = $this->CommonTable('Inventory\Model\ProductBatchTable')
                                           ->updateProductBatchQuantity($itemInBatchValue['itemInBatchID'], $data);
                            }
                        }
                    }

                    if(floatval($totalUpdatedQty) != floatval($batchValue['creditNoteProductQuantity'])){
                        $errorData = array(
                            'msg' => $this->getMessage('ERR_BATCH_PRODUCT_IN_USE_CN'),
                            'status' => false,
                            );
                        return $errorData;

                    }

                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocumentAndBatchID($batchValue['locationProductID'], 'Sales Invoice', $invoiceID, $batchValue['productBatchID']);
                    $creditNoteSubProductQuantity = $batchValue['creditNoteSubProductQuantity'];

                    foreach ($itemOutDetails as $itemOutData) {
                        if ($creditNoteSubProductQuantity != 0) {
                            $itemOutUpdateReturnQty = 0;
                            if (floatval($itemOutData->itemOutReturnQty) >= floatval($creditNoteSubProductQuantity)) {
                                $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) - floatval($creditNoteSubProductQuantity);
                                $creditNoteSubProductQuantity = 0;
                            } else {
                                $itemOutUpdateReturnQty = 0;
                                $creditNoteSubProductQuantity = floatval($creditNoteSubProductQuantity) - floatval($itemOutData->itemOutReturnQty);
                            }
                            $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                        }
                    }
                }
            }

        }
        //reverse normal products
        if(sizeof($dataSet['creditNoteNormalItem']) > 0){
            $exisistCreditNoteDetails = [];
            foreach ($dataSet['creditNoteNormalItem'] as $key => $normalDataSet) {
                if(empty($exisistCreditNoteDetails[$normalDataSet['locationProductID']])){
                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getProductDetailsBylocationProductIDDocTypeAndDocumentID($normalDataSet['locationProductID'], $docType, $normalDataSet['creditNoteID']);

                    $this->productIds[] = $normalDataSet['productID'];

                    if ($normalDataSet['documentTypeID'] == 4) {
                        $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getProductDetailsByInvoiceProductID($normalDataSet['invoiceProductID'])->current();

                        $deliveryNoteID = $invoiceDetails['salesInvoiceProductDocumentID'];

                        $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($deliveryNoteID, $normalDataSet['productID']);

                        if (isset($deliveryNoteProducts)) {
                            $newCopiedQuantity = $deliveryNoteProducts->deliveryNoteProductCopiedQuantity + $normalDataSet['creditNoteProductQuantity'];
                            $copiedflag = 0;
                            if ($newCopiedQuantity == $deliveryNoteProducts->deliveryNoteProductQuantity) {
                                $copiedflag = 1;
                            }
                            $deliveryNoteProData = array(
                                'copied' => $copiedflag,
                                'deliveryNoteProductCopiedQuantity' => $newCopiedQuantity,
                                );

                            $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryNoteProducts($normalDataSet['productID'], $deliveryNoteID, $deliveryNoteProData);
                            $this->_checkAndUpdateDeliveryNoteClosedStatus($deliveryNoteID);

                        }
                    } else {
                        foreach ($itemInDetails as $key => $itemInSingleValue) {
                            if($itemInSingleValue['itemInSoldQty'] == 0){
                                $returnQty = floatval($itemInSingleValue['itemInQty']);
                                //update locationproduct ItemIn and ItemOut
                                $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($normalDataSet['productID'], $normalDataSet['locationID'])->current();

                                $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $itemInSingleValue, $returnQty, 'CreditNote cancel');
                                if($locUpdateStatus['status'] != 'true'){
                                    $errorData = array(
                                        'msg' => $locUpdateStatus['msg'],
                                        'status' => false,
                                    );
                                    return $errorData;
                                }
                            } else {
                               $errorData = array(
                                    'msg' => $this->getMessage('ERR_NORMAL_PRODUCT_IN_USE_CN'),
                                    'status' => false,
                                );
                                return $errorData;
                            }
                        }
                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocumentAndItemOutPrice($normalDataSet['locationProductID'], 'Sales Invoice', $invoiceID, $normalDataSet['creditNoteProductPrice']);

                        $creditNoteProductQuantity = $normalDataSet['creditNoteProductQuantity'];
                        foreach ($itemOutDetails as $itemOutData) {
                            if ($creditNoteProductQuantity != 0) {
                                $itemOutUpdateReturnQty = 0;
                                if (floatval($itemOutData->itemOutReturnQty) >= floatval($creditNoteProductQuantity)) {
                                    $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) - floatval($creditNoteProductQuantity);
                                    $creditNoteProductQuantity = 0;
                                } else {
                                    $itemOutUpdateReturnQty = 0;
                                    $creditNoteProductQuantity = floatval($creditNoteProductQuantity) - floatval($itemOutData->itemOutReturnQty);
                                }
                                $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                            }
                        }
                    }
                    // validate location product update or not
                    $exisistCreditNoteDetails[$normalDataSet['locationProductID']] = $normalDataSet;
                }
            }
        }

        $errorData = array(
            'status' => true,
        );
        return $errorData;
    }

    private function updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $productDataSet, $returnQty, $document)
    {
        if($returnQty != 0){

            $outData = array(
                'itemOutDocumentType' => $document,
                'itemOutDocumentID' => $creditNoteID,
                'itemOutLocationProductID' => $locationProductDetails['locationProductID'],
                'itemOutBatchID' => $productDataSet['itemInBatchID'],
                'itemOutSerialID' => $productDataSet['itemInSerialID'],
                'itemOutItemInID' => $productDataSet['itemInID'],
                'itemOutQty' => $returnQty,
                'itemOutPrice' => $productDataSet['itemInPrice'],
                'itemOutAverageCostingPrice' => $locationProductDetails['locationProductAverageCostingPrice'],
                'itemOutDiscount' => $productDataSet['itemInDiscount'],
                'itemOutDateAndTime' => $this->getGMTDateTime(),
                'itemOutDeletedFlag' => 0,
            );
            //add record to the item out
            $itemOutM = new ItemOut();
            $itemOutM->exchangeArray($outData);
            $itemOutState = $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
            if($itemOutState == ''){
                $updateLocRespondData = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_ADD_ITEM_OUT'),
                    );
                return $updateLocRespondData;
            }

            //update itemIn table as all items sold.
            $updatedItemInDetails = array(
                'itemInSoldQty' => $returnQty + $productDataSet['itemInSoldQty'],
            );

            $itemInUpdateStatue = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($updatedItemInDetails, $productDataSet['itemInID']);

            if(!$itemInUpdateStatue){
                $updateLocRespondData = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_UPDATE_ITEM_IN'),
                    );
                return $updateLocRespondData;
            }

            $locationProductQuantity = floatval($locationProductDetails['locationProductQuantity']) - floatval($returnQty);
            //set data to update locationProduct
            $locationProductUpdatedData = array(
                'locationProductQuantity' => $locationProductQuantity
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $productDataSet['itemInLocationProductID']);

            if(!$updateLocPro){
                $updateResData = array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_UPDATE_ITEM_IN'),
                    );
                return $updateResData;
            }

             // calculate new avarageCosting
            $updateAverageCosting = $this->calculateAvgCostAndOtherDetails($locationProductDetails, $productDataSet);
            if(!$updateAverageCosting['status']){
                $this->rollback();
                $returnBatchSerialState = array(
                    'msg' => $this->getMessage('ERR_UPDATE_AVERAGE_COSTING'),
                    'status' => false, 
                );
                return $returnBatchSerialState;
               
            }
        }

        $updateLocRespondData = array(
            'status' => true,
        );
        return $updateLocRespondData;

    }


    private function _checkAndUpdateDeliveryNoteClosedStatus($deliveryNoteID)
    {
        $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getCopiedDeliveryNoteProducts($deliveryNoteID);
        if (!empty($deliveryNoteProducts)) {
            $statusID = $this->getStatusID('closed');
            $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNoteStatus(
                    $deliveryNoteID, $statusID);
        }

    }

    public function checkCreditNoteForCancel($creditNoteID, $invoiceID, $creditNoteDataSet)
    {
        foreach ($creditNoteDataSet['creditNoteSerial'] as $value) {
            $resCreditNote = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->checkAnyCrediteNotesUsedThisProductAfterThisCreditNoteID($creditNoteID, $value['productSerialID'],null)->current();

            if ($resCreditNote) {
                $respondData = array(
                    'status' => false,
                );
                return $respondData;
            }

            $resInvoice = $this->CommonTable('Invoice\Model\InvoiceProductTable')->checkAnyInvoicesUsedThisProductAfterThisInvoiceID($invoiceID, $value['productSerialID'],null)->current();

            if ($resInvoice) {
                $respondData = array(
                    'status' => false,
                );
                return $respondData;
            }
        }

        foreach ($creditNoteDataSet['creditNoteBatch'] as $value) {
            $resCreditNote = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->checkAnyCrediteNotesUsedThisProductAfterThisCreditNoteID($creditNoteID, null, $value['productBatchID'])->current();

            if ($resCreditNote) {
                $respondData = array(
                    'status' => false,
                );
                return $respondData;
            }

            $resInvoice = $this->CommonTable('Invoice\Model\InvoiceProductTable')->checkAnyInvoicesUsedThisProductAfterThisInvoiceID($invoiceID, null, $value['productBatchID'])->current();

            if ($resInvoice) {
                $respondData = array(
                    'status' => false,
                );
                return $respondData;
            }
        }
        foreach ($creditNoteDataSet['creditNoteNormalItem'] as $value) {
            $res = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($value['productID'], $value['locationID']);
            if ($res->productTypeID != "2") {
                if ( $value['documentTypeID'] != "4") {
                    if ($res->locationProductQuantity < $value['creditNoteProductQuantity']) {
                        $respondData = array(
                            'status' => false,
                        );
                        return $respondData;
                    }
                } else {
                    $data = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($value['salesInvoiceProductDocumentID'] ,$value['productID']);
                    $availableDeliveryNoteProductQuantity = floatval($data->deliveryNoteProductQuantity) - floatval($data->deliveryNoteProductCopiedQuantity);
                    if ($availableDeliveryNoteProductQuantity < $value['creditNoteProductQuantity']) {
                        $respondData = array(
                            'status' => false,
                        );
                        return $respondData;
                    }
                }
            }
        }

        $respondData = array(
            'status' => true,
        );
        return $respondData;       
    }

    private function calculateAvgCostAndOtherDetails($locationProductDetails, $productDataSet = null)
    {
        //get all iteminDetails
        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductIDInDecendingOrder($productDataSet['itemInLocationProductID'],$productDataSet['itemInID']);
        $initialState = 0;
        $locationProductQuantity = floatval($locationProductDetails['locationProductQuantity']);

        foreach ($itemInDetails as $key => $itemInSingleValue) {
            if ($initialState == 0) {
                //get sum of qty from item out for calculate costing
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
            } else {
                $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();

                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();

            }
            $initialState++;

            $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQty['totalQty'])) - floatval($itemInSingleValue['itemInQty']);

        }
        $nextInRecordFromCancelCN = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();

        $outDetailsQtyForCN = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($productDataSet['itemInLocationProductID'], $productDataSet['itemInDateAndTime'], $nextInRecordFromCancelCN['itemInDateAndTime'])->current();  


        $locationProductQuantity = (floatval($locationProductQuantity) + floatval($outDetailsQtyForCN['totalQty'])) - floatval($productDataSet['itemInQty']);

        $previousItemInFromCancelCN = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryPreviousRecordByIDAndLocationProductID($productDataSet['itemInLocationProductID'],$productDataSet['itemInID'])->current();

        $outDetailsQtyForPreviousDoc = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($previousItemInFromCancelCN['itemInLocationProductID'], $previousItemInFromCancelCN['itemInDateAndTime'], $productDataSet['itemInDateAndTime'])->current();

        $locationProductQuantity = floatval($locationProductQuantity) + floatval($outDetailsQtyForPreviousDoc['totalQty']);
        //get all iteminDetails after cancel
        $itemInDetailsAfterCancel = $this->CommonTable('Inventory\Model\ItemInTable')->getAllNextRecordByInIDAndLocationProductID($previousItemInFromCancelCN['itemInLocationProductID'],$previousItemInFromCancelCN['itemInID']);

        $avarageCosting = floatval($previousItemInFromCancelCN['itemInAverageCostingPrice']);
        $initialState = 0;
        foreach ($itemInDetailsAfterCancel as $key => $itemInSingleValue) {
            //get very Next record in itemIn Table
            $nextInRecord = $this->CommonTable('Inventory\Model\ItemInTable')->getVeryNextRecordByIDAndLocationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInID'])->current();
            
            if($nextInRecord){
                $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDStartDateAndEndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'], $nextInRecord['itemInDateAndTime'])->current();
                
            } else {
                if (sizeof($itemInDetails) != 0) {
                    $outDetailsQty = $this->CommonTable('Inventory\Model\ItemOutTable')->getOutDataByLocationProductIDAndDate($itemInSingleValue['itemInLocationProductID'], $itemInSingleValue['itemInDateAndTime'])->current();
                }
            }
            
            if ($initialState == 0) {
                $newAvgCosting = $avarageCosting;
            } else {
                $newAvgCosting = (($locationProductQuantity * $avarageCosting) + (floatval($itemInSingleValue['itemInQty']) * floatval($itemInSingleValue['itemInPrice'])))/(floatval($itemInSingleValue['itemInQty']) + $locationProductQuantity);
                $avarageCosting = $newAvgCosting;
                $locationProductQuantity = $locationProductQuantity + floatval($itemInSingleValue['itemInQty']);
            }
            
            //set data to update itemIn Table
            $itemInUpdatedDataSet = array(
                'itemInAverageCostingPrice' => $newAvgCosting,
            );

            //update item in table
            $itemInUpdateFlag = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($itemInUpdatedDataSet, $itemInSingleValue['itemInID']);
            if(!$itemInUpdateFlag){
                $returnCostingDetails = array(
                    'status' => false,
                    );
                return $returnCostingDetails;
            }
            
            $locationProductQuantity = $locationProductQuantity - floatval($outDetailsQty['totalQty']);

            //set data to update locationProduct
            $locationProductUpdatedData = array(
                'locationProductAverageCostingPrice' => $newAvgCosting
            );

            //update locationProduct table
            $updateLocPro = $this->CommonTable('Inventory\Model\LocationProductTable')->updateAvgCostingByLocationProductID($locationProductUpdatedData, $itemInSingleValue['itemInLocationProductID']);


            if($nextInRecord){
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime'],$nextInRecord['itemInDateAndTime']);
        
            } else {
                //get all item out records by locationProductID and given date range
                $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsByGivenDateRangeAndLOcationProductID($itemInSingleValue['itemInLocationProductID'],$itemInSingleValue['itemInDateAndTime']);

            }

           
            foreach ($itemOutDetails as $key => $itemOutSingleValue) {
                //set Data to update itemOut table
                $updateItemOutCValue = array(
                    'itemOutAverageCostingPrice' => $newAvgCosting,
                );

                //update costing value in itemOutRecords
                $updateItemOut = $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutTable($itemOutSingleValue['itemOutID'],$updateItemOutCValue);

                $itemOutIds = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDataByItemOutID($itemOutSingleValue['itemOutID'])->current();

                $journalEntryData = array(
                    'journalEntryAccountsDebitAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    'journalEntryAccountsCreditAmount' => $newAvgCosting * $itemOutIds['itemOutQty'],
                    );

                $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->updateJournalEntryByDocumentId($journalEntryData, $itemOutIds['journalEntryID']);
                
                if(!$updateItemOut){
                    
                    $returnCalculateData = array(
                        'status' => false,
                    );
                
                    return $returnCalculateData;
                }
            }
            $initialState++;
        }

        $returnCostingDetails = array(
            'status' => true,
            );
        return $returnCostingDetails;
    }

    public function getAllRelatedDocumentDetailsByCreditNoteIDAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $creditNoteID = $request->getPost('creditNoteID');
            $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteBasicDetailsByCreditNoteID($creditNoteID);
            $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getQuotationRelatedCreditNotePaymentDataByQuotationId(null, $creditNoteID);
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesOrderRelatedSalesInvoiceDataByQuotationId(null, null ,$creditNoteID);
            $deliveryNoteData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getinvoiceRelatedDeliveryNoteDataByInvoiceId(null, $creditNoteID);
            $salesReturnData = $this->CommonTable('SalesReturnsTable')->getSalesReturnByDeliveryNoteID(null,$creditNoteID);
            $dlnRelatedSalesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getDlnRelatedSalesOrderDataBycreditNoteId($creditNoteID);
            $soRelatedQuotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getSalesOrderRelatedQuotationDataByCreditNoteId($creditNoteID);
            $invRelatedSalesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getDlnRelatedSalesOrderDataBydlnId(null, null, null, $creditNoteID);
            $invRelatedQuotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getSalesOrderRelatedQuotationDataBySOId(null, null, null, $creditNoteID);
            $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getSalesOrderRelatedInvoicePaymentsDataByQuotationId(null, null, $creditNoteID);
            

            $dataExistsFlag = false;
            if ($creditNoteData) {
                $creditNoteData = array(
                    'type' => 'CreditNote',
                    'documentID' => $creditNoteData['creditNoteID'],
                    'code' => $creditNoteData['creditNoteCode'],
                    'amount' => number_format($creditNoteData['creditNoteTotal'], 2),
                    'issuedDate' => $creditNoteData['creditNoteDate'],
                    'created' => $creditNoteData['createdTimeStamp'],
                );
                $creditNoteDetails[] = $creditNoteData;
                $dataExistsFlag = true;
            }

            if (isset($creditNotePaymentData)) {
                foreach ($creditNotePaymentData as $cNPDta) {
                    $creditNotePaymentData = array(
                        'type' => 'CreditNotePayment',
                        'documentID' => $cNPDta['creditNotePaymentID'],
                        'code' => $cNPDta['creditNotePaymentCode'],
                        'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                        'issuedDate' => $cNPDta['creditNotePaymentDate'],
                        'created' => $cNPDta['createdTimeStamp'],
                    );
                    $creditNoteDetails[] = $creditNotePaymentData;
                    if (isset($cNPDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($invoiceData)) {
                foreach ($invoiceData as $invDta) {
                    $invDeta = array(
                        'type' => 'SalesInvoice',
                        'documentID' => $invDta['salesInvoiceID'],
                        'code' => $invDta['salesInvoiceCode'],
                        'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                        'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                        'created' => $invDta['createdTimeStamp'],
                    );
                    $creditNoteDetails[] = $invDeta;
                    if (isset($invDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            if (isset($salesReturnData)) {
                foreach ($salesReturnData as $srDta) {
                    $returnData = array(
                        'type' => 'SalesReturn',
                        'documentID' => $srDta['salesReturnID'],
                        'code' => $srDta['salesReturnCode'],
                        'amount' => number_format($srDta['salesReturnTotal'], 2),
                        'issuedDate' => $srDta['salesReturnDate'],
                        'created' => $srDta['createdTimeStamp'],
                    );
                    $creditNoteDetails[] = $returnData;
                    if (isset($srDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($deliveryNoteData)) {
                foreach ($deliveryNoteData as $dlnDta) {
                    $dlnData = array(
                        'type' => 'DeliveryNote',
                        'documentID' => $dlnDta['salesInvoiceProductDocumentID'],
                        'code' => $dlnDta['deliveryNoteCode'],
                        'amount' => number_format($dlnDta['deliveryNotePriceTotal'], 2),
                        'issuedDate' => $dlnDta['deliveryNoteDeliveryDate'],
                        'created' => $dlnDta['createdTimeStamp'],
                    );
                    $creditNoteDetails[] = $dlnData;
                    if (isset($dlnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedSalesOrderData)) {
                foreach ($dlnRelatedSalesOrderData as $soDta) {
                    $soData = array(
                        'type' => 'SalesOrder',
                        'documentID' => $soDta['soID'],
                        'code' => $soDta['soCode'],
                        'amount' => number_format($soDta['totalAmount'], 2),
                        'issuedDate' => $soDta['issuedDate'],
                        'created' => $soDta['createdTimeStamp'],
                    );
                    $creditNoteDetails[] = $soData;
                    if (isset($soDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($invRelatedSalesOrderData)) {
                foreach ($invRelatedSalesOrderData as $soDta) {
                    $soData = array(
                        'type' => 'SalesOrder',
                        'documentID' => $soDta['soID'],
                        'code' => $soDta['soCode'],
                        'amount' => number_format($soDta['totalAmount'], 2),
                        'issuedDate' => $soDta['issuedDate'],
                        'created' => $soDta['createdTimeStamp'],
                    );
                    $creditNoteDetails[] = $soData;
                    if (isset($soDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($soRelatedQuotaionData)) {
                foreach ($soRelatedQuotaionData as $qtnDta) {
                    $qtnDeta = array(
                        'type' => 'Quotation',
                        'documentID' => $qtnDta['quotationID'],
                        'code' => $qtnDta['quotationCode'],
                        'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                        'issuedDate' => $qtnDta['quotationIssuedDate'],
                        'created' => $qtnDta['createdTimeStamp'],
                    );
                    $creditNoteDetails[] = $qtnDeta;
                    if (isset($qtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($invRelatedQuotaionData)) {
                foreach ($invRelatedQuotaionData as $qtnDta) {
                    $qtnDeta = array(
                        'type' => 'Quotation',
                        'documentID' => $qtnDta['quotationID'],
                        'code' => $qtnDta['quotationCode'],
                        'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                        'issuedDate' => $qtnDta['quotationIssuedDate'],
                        'created' => $qtnDta['createdTimeStamp'],
                    );
                    $creditNoteDetails[] = $qtnDeta;
                    if (isset($qtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($paymentData)) {
                foreach ($paymentData as $payDta) {
                    $paymentData = array(
                        'type' => 'CustomerPayment',
                        'documentID' => $payDta['incomingPaymentID'],
                        'code' => $payDta['incomingPaymentCode'],
                        'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                        'issuedDate' => $payDta['incomingPaymentDate'],
                        'created' => $payDta['createdTimeStamp'],
                    );
                    $creditNoteDetails[] = $paymentData;
                    if (isset($payDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            $sortData = Array();
            foreach ($creditNoteDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $creditNoteDetails);

            $documentDetails = array(
                'creditNoteDetails' => $creditNoteDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

    public function saveDirectCreditNoteDetails($directSaveData, $creditNoteDirectType) 
    {
        $customerID = $directSaveData['customerID'];
        $customerName = $directSaveData['customerName'];
        $customCurrencyId = $directSaveData['customCurrencyId'];
        $dimensionData = $directSaveData['dimensionData'];
        $ignoreBudgetLimit = $directSaveData['ignoreBudgetLimit'];
        $creditNoteCustomCurrencyRate = (float) $directSaveData['creditNoteCustomCurrencyRate'];
        $inactiveFlag = false;
        $inactiveFlag =  filter_var($directSaveData['invFlag'], FILTER_VALIDATE_BOOLEAN);
        $invCloseFlag = filter_var($directSaveData['invCloseFlag'], FILTER_VALIDATE_BOOLEAN);
        $dupString = "DUP";
        $dupStringPreFix = false;
        $dupStringPostFix = true;
        $cdNotDirFlag = false;
        $cdNotBuyBackFlag = false;
                    
        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        
        if(!is_null($cust)){
            $products = $directSaveData['products'];
            $invoiceID = $directSaveData['invoiceID'];
            $locationID = $directSaveData['locationID'];

            if ($invoiceID != "") {
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                if ($invoiceDetails['statusID'] == '10') {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_CREDIT_NOTE_INVOICE_EDITED')];
                }
            }
                
            if($creditNoteDirectType == 'true'){
                $cdNotDirFlag = true;
            }

            if($creditNoteDirectType == 'true' && $invoiceID == ''){
                $invoiceID = 0;
            } else if($creditNoteDirectType == 'true' && $invoiceID != ''){
                //validate customerID and invoiceID
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                if($invoiceDetails['customerID'] != $customerID){
                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CUSTOMER_NOT_MATCH')];
                } else if($invoiceDetails['locationID'] != $locationID){
                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_LOC_NOT_MATCH')];
                }
                
            }
            $totalPrice = (float) trim($directSaveData['creditNoteTotalPrice']) * $creditNoteCustomCurrencyRate;
            
            if($creditNoteDirectType == 'true' && $invoiceID != ''){
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                $invoiceTotalAmount = $invoiceDetails['salesinvoiceTotalAmount'];

                $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceID($invoiceID)->current();

                $creditNoteAmount = 0;
                if (!is_null($creditNoteTotalAmountForTheInvoice['creditNoteTot'])) {
                    $creditNoteAmount = $creditNoteTotalAmountForTheInvoice['creditNoteTot'];
                }

                $restAmountOfInvoice = $invoiceTotalAmount - $creditNoteAmount;

                if ($totalPrice > $restAmountOfInvoice) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_TOTAL_AMOUNT')];
                }

            }
            
            $subProducts = $directSaveData['subProducts'];
            $creditNoteCode = $directSaveData['creditNoteCode'];
            $date = $this->convertDateToStandardFormat($directSaveData['date']);
            $comment = $directSaveData['creditNoteComment'];
            $paymentTermID = $directSaveData['paymentTermID'];
            $creditNoteInvoiceDiscountType = $directSaveData['creditNoteInvoiceDiscountType'];
            $creditNoteInvoiceDiscountAmount = $directSaveData['creditNoteInvoiceDiscountAmount'];
            $creditNotePromotionDiscountType = $directSaveData['creditNotePromotionDiscountType'];
            $creditNotePromotionDiscountAmount = $directSaveData['creditNotePromotionDiscountAmount'];
            $creditNotePaymentEligible = 0;
            $creditBalance = 0;
            $oustandingBalance = 0;
            

            $result = $this->getReferenceNoForLocation('8', $locationID);
            $locationReferenceID = $result['locRefID'];
            //check credit note numer already exist in credit note table if exist give next number for credit note code
            while ($creditNoteCode) {
                if ($this->CommonTable('Invoice\Model\CreditNoteTable')->checkCreditNoteByCode($creditNoteCode)->current()) {
                    if ($locationReferenceID) {
                        $newCreditNoteCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newCreditNoteCode == $creditNoteCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $creditNoteCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $creditNoteCode = $newCreditNoteCode;
                        }
                    } else {
                        return ['status' => false, 'msg' => $this->getMessage('ERR_CREDIT_CODE_EXIST')];
                    }
                } else {
                    break;
                }
            }

            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            if(empty($FIFOCostingFlag) && empty($averageCostingFlag)){
                return ['status' => false, 'msg' => $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE')];
            }

            $accountProduct = array();
            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountsData = $glAccountExist->current();
            $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

            $custSalesAccountIsset = true;
            if($this->useAccounting == 1){
                if(empty($customerData->customerSalesAccountID)){
                    $custSalesAccountIsset = false;
                }else if(empty($customerData->customerReceviableAccountID)){
                    return ['status' => false, 'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode))];
                }
            }


            $creditDeviation = 0;
            $outstandingDeviation = 0;
            //no need to process invoice details for direct credit notes
            if ($invoiceID == 0) {
                $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                $customerCurrentBalance = $cust->customerCurrentBalance;
            } else {
                if($cust->customerCurrentBalance == 0){
                    $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                    $customerCurrentBalance = $cust->customerCurrentBalance;
                    $creditDeviation = $totalPrice;
                } else if($cust->customerCurrentBalance - $totalPrice < 0){
                    $customerCurrentBalance = 0;
                    $customerCreditBalance = $cust->customerCurrentCredit + ($totalPrice - $cust->customerCurrentBalance);
                    $creditDeviation = $totalPrice - $cust->customerCurrentBalance;
                    $outstandingDeviation = -($cust->customerCurrentBalance);
                } else {
                    $customerCurrentBalance = $cust->customerCurrentBalance - $totalPrice;
                    $customerCreditBalance = $cust->customerCurrentCredit;
                    $outstandingDeviation = -$totalPrice;
                }
            }                        

            $custData = array(
                'customerID' => $customerID,
                'customerCurrentCredit' => $customerCreditBalance,
                'customerCurrentBalance' => $customerCurrentBalance,
            );
           
            $customer = new Customer;
            $customer->exchangeArray($custData);
            $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCurrentAndCreditBalance($customer);

            $entityID = $this->createEntity();
            $creditNoteData = array(
                'creditNoteCode' => $creditNoteCode,
                'customerID' => $customerID,
                'locationID' => $locationID,
                'paymentTermID' => $paymentTermID,
                'invoiceID' => $invoiceID,
                'creditNoteDate' => $date,
                'creditNoteTotal' => $totalPrice,
                'creditNoteComment' => $comment,
                'statusID' => 3,
                'creditNotePaymentEligible' => $creditNotePaymentEligible,
                'creditNotePaymentAmount' => $creditBalance,
                'customCurrencyId' => $customCurrencyId,
                'creditNoteCustomCurrencyRate' => $creditNoteCustomCurrencyRate,
                'creditNoteInvoiceDiscountType' => $creditNoteInvoiceDiscountType,
                'creditNoteInvoiceDiscountAmount' => $creditNoteInvoiceDiscountAmount,
                'creditNotePromotionDiscountType' => $creditNotePromotionDiscountType,
                'creditNotePromotionDiscountAmount' => $creditNotePromotionDiscountAmount,
                'creditNoteDirectFlag' => $cdNotDirFlag,
                'creditNoteBuyBackFlag' => $cdNotBuyBackFlag,
                'entityID' => $entityID,
                'creditDeviation' => $creditDeviation,
                'outstandingDeviation' => $outstandingDeviation,
                'ableToCancel' => true
            );

            $creditNote = new CreditNote;
            $creditNote->exchangeArray($creditNoteData);
            $creditNoteID = $this->CommonTable('Invoice\Model\CreditNoteTable')->saveCreditNote($creditNote);

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }

            //check and update the invoice remainin discount amount
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
            if ($creditNoteInvoiceDiscountType == 'value'){
                if ($creditNoteInvoiceDiscountAmount > 0) {
                    if ($invoiceData->salesInvoiceRemainingDiscValue != null) {
                        $invoiceDiscountRemaingValue = $invoiceData->salesInvoiceRemainingDiscValue;
                    } else {
                        $invoiceDiscountRemaingValue = $invoiceData->salesInvoiceDiscountRate;
                    }
                    if($invoiceDiscountRemaingValue >= $creditNoteInvoiceDiscountAmount){
                        $remainingDiscount = $invoiceDiscountRemaingValue - $creditNoteInvoiceDiscountAmount;
                        $updateInvoiceDiscountValue = $this->CommonTable('Invoice\Model\InvoiceTable')
                        ->updateInvoiceRemainingDiscountValue($invoiceID,$remainingDiscount);
                    } else {
                        return ['status' => false, 'msg' => $this->getMessage('ERR_DISCT_CANT_BE_MORE_THAN_INVOICE_DISCOUNT')];
                    }

                }
            }

            if ($creditNotePromotionDiscountType == 'value') {
                if ($creditNotePromotionDiscountAmount > 0) {
                    if ($invoiceData->salesInvoiceRemainingPromotionDiscValue != null) {
                        $promotionDiscountRemaingValue = $invoiceData->salesInvoiceRemainingPromotionDiscValue;
                    } else {
                        $promotionDiscountRemaingValue = $invoiceData->salesInvoicePromotionDiscount;
                    }
                    if($promotionDiscountRemaingValue >= $creditNotePromotionDiscountAmount){
                        $remainingDiscount = $promotionDiscountRemaingValue - $creditNotePromotionDiscountAmount;
                        $updatePromotionDiscountValue = $this->CommonTable('Invoice\Model\InvoiceTable')
                        ->updatePromotionRemainingDiscountValue($invoiceID,$remainingDiscount);
                    } else {
                        return ['status' => false, 'msg' => $this->getMessage('ERR_DISCT_CANT_BE_MORE_THAN_PROMOTION_DISCOUNT')];
                    }
                }

            }

            $allProductTotal = 0;
            foreach ($products as $product) {
                $locationPID = $product['locationPID'];
                //get location product data for update average costing.
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;
             
                $productBaseQty = $product['pQuantity'];
                $productType = $product['productType'];
                $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);
                $discountValue = 0;
                if ($product['discountType'] == "precentage") {
                    $discountValue = ($product['pUnitPrice']  * $product['pDiscount']/ 100);
                } else if ($product['discountType'] == "value") {
                    $discountValue = floatval($product['pDiscount']);
                }
                $productTotal = $product['pUnitPrice']*$product['pQuantity'] - $discountValue;
                $allProductTotal+=$productTotal;

                //If product is a batch product
                if (array_key_exists('bProducts', $product)) {
                    $batchProductCount = 0;
                    $batchSerialProductCount = 0;
                    foreach ($product['bProducts'] as $bProduct) {

                        $batchProductCount++;
                        $bPData = array(
                            'locationProductID' => $locationPID,
                            'productBatchCode' => $bProduct['bCode'],
                            'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                            'productBatchWarrantyPeriod' => $bProduct['warnty'],
                            'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                            'productBatchQuantity' => $bProduct['bQty'] * $productConversionRate
                        );

                        $batchProduct = new ProductBatch();
                        $batchProduct->exchangeArray($bPData);
                        $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                        if(!$insertedBID){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }
                        //if product is a batch and serial product
                        if (array_key_exists('sProducts', $product)) {
                            foreach ($product['sProducts'] as $sProduct) {
                                $batchSerialProductCount++;
                                if (isset($sProduct['sBCode'])) {

                                    if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                        $sPData = array(
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialCode' => $sProduct['sCode'],
                                            'productSerialExpireDate' => $this->convertDateToStandardFormat($sProduct['sEdate']),
                                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                            'productSerialWarrantyPeriodType' => $sProduct['sWarrantyType'],
                                        );
                                        $serialPr = new ProductSerial();
                                        $serialPr->exchangeArray($sPData);
                                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                        if(!$insertedSID){
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                        }
                                        
                                        if ($batchSerialProductCount == 1) {
                                            $creditNoteProductData = array(
                                                'creditNoteID' => $creditNoteID,
                                                'invoiceProductID' => 0,
                                                'productID' => $product['pID'],
                                                'creditNoteProductPrice' => $product['pUnitPrice'] * $creditNoteCustomCurrencyRate,
                                                'creditNoteProductDiscount' => $product['pDiscount'],
                                                'creditNoteProductDiscountType' => $product['discountType'],
                                                'creditNoteProductTotal' => $product['pTotal'] * $creditNoteCustomCurrencyRate,
                                                'creditNoteProductQuantity' => $product['pQuantity']
                                            );
                                            $creditNoteProduct = new CreditNoteProduct;
                                            $creditNoteProduct->exchangeArray($creditNoteProductData);
                                            //update credit note product table
                                            $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                                                ->saveCreditNoteProducts($creditNoteProduct);
                                            if(!$creditNoteProductID){
                                                return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                            }
                                        }
                                        //save item in details
                                        $itemInData = array(
                                            'itemInDocumentType' => 'Direct Credit Note',
                                            'itemInDocumentID' => $creditNoteID,
                                            'itemInLocationProductID' => $locationPID,
                                            'itemInBatchID' => $insertedBID,
                                            'itemInSerialID' => $insertedSID,
                                            'itemInQty' => 1,
                                            'itemInPrice' => $product['pUnitPrice'],
                                            'itemInDiscount' => $discountValue,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                        if(!$itemInID){
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                        }

                                        $data = array(
                                            'creditNoteProductID' => $creditNoteProductID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialID' => $insertedSID,
                                            'creditNoteSubProductQuantity' => 1,
                                        );
                                        $creditNoteSubProduct = new CreditNoteSubProduct;
                                        $creditNoteSubProduct->exchangeArray($data);
                                        $creditNoteSubProductID = $this->CommonTable('Invoice\Model\CreditNoteSubProductTable')->saveCreditNoteSubProduct($creditNoteSubProduct);

                                        if (array_key_exists('pTax', $product)) {
                                            if (array_key_exists('tL', $product['pTax'])) {
                                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                    $creditNoteProductTaxData = array(
                                                        'creditNoteProductID' => $creditNoteProductID,
                                                        'taxID' => $taxKey,
                                                        'creditNoteProductTaxPercentage' => $productTax['tP'],
                                                        'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
                                                    );
                                                    $creditNoteProductTax = new CreditNoteProductTax();
                                                    $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
                                                    $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->saveCreditNoteProductTax($creditNoteProductTax);
                                                }
                                            }
                                        }
                                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                        $newPQtyData = array(
                                            'locationProductQuantity' => $newPQty
                                        );
                                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                        if(!$status){
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($batchProductCount == 1) {
                                $creditNoteProductData = array(
                                    'creditNoteID' => $creditNoteID,
                                    'invoiceProductID' => 0,
                                    'productID' => $product['pID'],
                                    'creditNoteProductPrice' => $product['pUnitPrice'] * $creditNoteCustomCurrencyRate,
                                    'creditNoteProductDiscount' => $product['pDiscount'],
                                    'creditNoteProductDiscountType' => $product['discountType'],
                                    'creditNoteProductTotal' => $product['pTotal'] * $creditNoteCustomCurrencyRate,
                                    'creditNoteProductQuantity' => $product['pQuantity']
                                );
                                $creditNoteProduct = new CreditNoteProduct;
                                $creditNoteProduct->exchangeArray($creditNoteProductData);
                                //update credit note product table
                                $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                                                ->saveCreditNoteProducts($creditNoteProduct);
                                if(!$creditNoteProductID){
                                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                }
                            }
                            //save item in details
                            $itemInData = array(
                                'itemInDocumentType' => 'Direct Credit Note',
                                'itemInDocumentID' => $creditNoteID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => $insertedBID,
                                'itemInSerialID' => NULL,
                                'itemInQty' => $bProduct['bQty'] * $productConversionRate,
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if(!$itemInID){
                                return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                            }

                            $data = array(
                                'creditNoteProductID' => $creditNoteProductID,
                                'productBatchID' => $insertedBID,
                                'productSerialID' => NULL,
                                'creditNoteSubProductQuantity' => $bProduct['bQty'],
                            );
                            $creditNoteSubProduct = new CreditNoteSubProduct;
                            $creditNoteSubProduct->exchangeArray($data);
                            $creditNoteSubProductID = $this->CommonTable('Invoice\Model\CreditNoteSubProductTable')->saveCreditNoteSubProduct($creditNoteSubProduct);
                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $creditNoteProductTaxData = array(
                                            'creditNoteProductID' => $creditNoteProductID,
                                            'taxID' => $taxKey,
                                            'creditNoteProductTaxPercentage' => $productTax['tP'],
                                            'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
                                        );
                                        $creditNoteProductTax = new CreditNoteProductTax();
                                        $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
                                        $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->saveCreditNoteProductTax($creditNoteProductTax);
                                    }
                                }
                            }
                            if ($batchProductCount == 1) {
                                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                                $newPQtyData = array(
                                    'locationProductQuantity' => $newPQty
                                );
                                $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                if(!$status){
                                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                }
                            }
                        }
                    }
                } elseif (array_key_exists('sProducts', $product)) {
                    $serialProductCount = 0;
                    //If the product is a serial product
                    foreach ($product['sProducts'] as $sProduct) {
                        $serialProductCount++;
                        $sPData = array('locationProductID' => $locationPID,
                            'productSerialCode' => $sProduct['sCode'],
                            'productSerialExpireDate' => $sProduct['sEdate'],
                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                            'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                        );
                        $serialPr = new ProductSerial();
                        $serialPr->exchangeArray($sPData);
                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                        if(!$insertedSID){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }
                        if ($serialProductCount == 1) {
                            $creditNoteProductData = array(
                                'creditNoteID' => $creditNoteID,
                                'invoiceProductID' => 0,
                                'productID' => $product['pID'],
                                'creditNoteProductPrice' => $product['pUnitPrice'] * $creditNoteCustomCurrencyRate,
                                'creditNoteProductDiscount' => $product['pDiscount'],
                                'creditNoteProductDiscountType' => $product['discountType'],
                                'creditNoteProductTotal' => $product['pTotal'] * $creditNoteCustomCurrencyRate,
                                'creditNoteProductQuantity' => $product['pQuantity']
                            );
                            $creditNoteProduct = new CreditNoteProduct;
                            $creditNoteProduct->exchangeArray($creditNoteProductData);
                            //update credit note product table
                            $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                                            ->saveCreditNoteProducts($creditNoteProduct);
                            if(!$creditNoteProductID){
                                return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                            }
                        }
                        //save item in details
                        $itemInData = array(
                            'itemInDocumentType' => 'Direct Credit Note',
                            'itemInDocumentID' => $creditNoteID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => $insertedSID,
                            'itemInQty' => 1,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }

                        $data = array(
                            'creditNoteProductID' => $creditNoteProductID,
                            'productBatchID' => NULL,
                            'productSerialID' => $insertedSID,
                            'creditNoteSubProductQuantity' => 1,
                        );
                        $creditNoteSubProduct = new CreditNoteSubProduct;
                        $creditNoteSubProduct->exchangeArray($data);
                        $creditNoteSubProductID = $this->CommonTable('Invoice\Model\CreditNoteSubProductTable')->saveCreditNoteSubProduct($creditNoteSubProduct);
                        if ($serialProductCount == 1) {
                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $creditNoteProductTaxData = array(
                                            'creditNoteProductID' => $creditNoteProductID,
                                            'taxID' => $taxKey,
                                            'creditNoteProductTaxPercentage' => $productTax['tP'],
                                            'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
                                        );
                                        $creditNoteProductTax = new CreditNoteProductTax();
                                        $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
                                        $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->saveCreditNoteProductTax($creditNoteProductTax);
                                    }
                                }
                            }
                        }
                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                        );
                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        if(!$status){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }
                    }
                } else {
                    // if the product is non serial and non batch
                    $creditNoteProductData = array(
                        'creditNoteID' => $creditNoteID,
                        'invoiceProductID' => 0,
                        'productID' => $product['pID'],
                        'creditNoteProductPrice' => $product['pUnitPrice'] * $creditNoteCustomCurrencyRate,
                        'creditNoteProductDiscount' => $product['pDiscount'],
                        'creditNoteProductDiscountType' => $product['discountType'],
                        'creditNoteProductTotal' => $product['pTotal'] * $creditNoteCustomCurrencyRate,
                        'creditNoteProductQuantity' => $product['pQuantity']
                    );
                    $creditNoteProduct = new CreditNoteProduct;
                    $creditNoteProduct->exchangeArray($creditNoteProductData);
                    //update credit note product table
                    $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                                    ->saveCreditNoteProducts($creditNoteProduct);
                    if(!$creditNoteProductID){
                        return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                    }

                    if ($productType != 2) {
                        $itemInData = array(
                            'itemInDocumentType' => 'Direct Credit Note',
                            'itemInDocumentID' => $creditNoteID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => NULL,
                            'itemInQty' => $productBaseQty,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }
                    }
                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $creditNoteProductTaxData = array(
                                    'creditNoteProductID' => $creditNoteProductID,
                                    'taxID' => $taxKey,
                                    'creditNoteProductTaxPercentage' => $productTax['tP'],
                                    'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
                                );
                                $creditNoteProductTax = new CreditNoteProductTax();
                                $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
                                $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->saveCreditNoteProductTax($creditNoteProductTax);
                            }
                        }
                    }
                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    if ($productType == 2) {
                        $productBaseQty = 0;
                    }

                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                    if(!$status){
                        return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                    }
                }
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
                $locationPrTotalQty = 0;
                $locationPrTotalPrice = 0;
                $newItemInIds = array();
                if(count($itemInData) > 0){
                    foreach ($itemInData as $key => $value) {
                        if($value->itemInID > $locationProductLastItemInID){
                            $newItemInIds[] = $value->itemInID;
                            $remainQty = $value->itemInQty - $value->itemInSoldQty;
                            if($remainQty > 0){
                                $itemInPrice = $remainQty*$value->itemInPrice;
                                $itemInDiscount = $remainQty*$value->itemInDiscount;
                                $locationPrTotalQty += $remainQty;
                                $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                            }
                            $locationProductLastItemInID = $value->itemInID;
                        }
                    }
                }
                $locationPrTotalQty += $locationProductQuantity;
                $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                if($locationPrTotalQty > 0){
                    $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                    $lpdata = array(
                        'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                        'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                    foreach ($newItemInIds as $inID) {
                        $intemInData = array(
                            'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                            );
                        $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                    }
                }
            }
            //call product updated event
            $productIDs = array_map(function($element) {
                return $element['pID'];
            }, $products);

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            if($creditNoteDirectType == 'true' && $invoiceID != ''){
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                $invoiceEditLogModel = new InvoiceEditLog();
                $invoiceEditLogModel->exchangeArray([
                    'userID' => $this->userID,
                    'salesInvoiceID' => $invoiceID,
                    'dateAndTime' => $this->getGMTDateTime()
                ]);
                $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                $newItemsData = [
                    'invoiceEditLogDetailsOldState' => $invoice->statusID,
                    'invoiceEditLogDetailsNewState' => 'Invoice closed with adding a direct credit note '.$creditNoteCode,
                    'invoiceEditLogDetailsCategory' => 'salesInvoiceStatusClosed'
                ];

                //close invoice state
                if ($invCloseFlag) {
                    $state = '4'; //close state
                    $update = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceState($invoiceID, $state);

                    $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);
                    $newItemsData['invoiceEditLogID'] = $invoiceEditLastInsertID;
                    $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                    $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                } else {
                    $invoiceTotalAmount = $invoice->salesinvoiceTotalAmount;
                    $invoicePayedAmount = $invoice->salesInvoicePayedAmount;

                    $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceID($invoiceID)->current();

                    $creditNoteAmount = 0;
                    if (!is_null($creditNoteTotalAmountForTheInvoice['creditNoteTot'])) {
                        $creditNoteAmount = $creditNoteTotalAmountForTheInvoice['creditNoteTot'];
                    }

                    $totalSettledAmountForInvoice = $creditNoteAmount + $invoicePayedAmount;
                    if ($totalSettledAmountForInvoice >= $invoiceTotalAmount) {
                        $state = '4'; //close state
                        $update = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceState($invoiceID, $state);

                        $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);
                        $newItemsData['invoiceEditLogID'] = $invoiceEditLastInsertID;
                        $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                        $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                    }

                }
            }

            $jeResult = $this->saveJournalEntryDataForDirectCredit($creditNoteID, $creditNoteCode, $customerID, $products, $allProductTotal, $date, $averageCostingFlag, $creditNoteCustomCurrencyRate, $dimensionData, $ignoreBudgetLimit);

            if($jeResult['status']){

                $this->setLogMessage($customCurrencySymbol . ' amount credit note Product details were successfully added - ' . $creditNoteCode);
                $this->flashMessenger()->addMessage($jeResult['msg']);
            }
         
            return ['status' => $jeResult['status'], 'msg' => $jeResult['msg'], 'data' => $jeResult['data']];
        } else {
            return ['status' => false, 'msg' => $this->getMessage('ERR_CREDIT_VALID_CUST')];
        }
    }

    /**
    * This fuction is used to save journal entry for direct credit
    **/
    public function saveJournalEntryDataForDirectCredit($creditNoteID, $creditNoteCode, $customerID, $productsData,$allProductTotal, $date, $averageCostingFlag, $creditNoteCustomCurrencyRate, $dimensionData, $ignoreBudgetLimit)
    {
        $accountProduct = array();
        $totalCRAmounts = 0;

        $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

        $custSalesAccountIsset = true;
        if($this->useAccounting == 1){
            if(empty($customerData->customerSalesAccountID)){
                $custSalesAccountIsset = false;
            }else if(empty($customerData->customerReceviableAccountID)){
                return ['status' => false, 'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode))];
            }
        }
        if ($this->useAccounting == 1) {
            foreach ($productsData as $key => $product) {
                $locationPID = $product['locationPID'];
                $productID = $product['pID'];
                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);
                if(empty($pData['productInventoryAccountID'])){
                    return array('status' => false, 'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])));
                }
                 //check whether Product Cost of Goods Sold Gl account id set or not
                if(empty($pData['productCOGSAccountID'])){
                    return array('status' => false, 'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName'])));
                }

                if(!$custSalesAccountIsset){
                    if(empty($pData['productSalesAccountID'])){
                        return array('status' => false, 'msg' => $this->getMessage('ERR_PRPODUCT_SALES_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])));
                    }
                }

                //calculate discount value
                $discountValue = 0;
                if ($product['discountType'] == "precentage") {
                    $discountValue = ($product['pUnitPrice']  * $product['pDiscount']/ 100);
                } else if ($product['discountType'] == "value") {
                    $discountValue = floatval($product['pDiscount']);
                }

                $productTotal = $product['pQuantity'] * ($product['pUnitPrice'] - $discountValue);
                $totalCRAmounts+=$productTotal;
                if($custSalesAccountIsset){
                    if(isset($accountProduct[$customerData->customerSalesAccountID]['debitTotal'])){
                        $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] = $productTotal;
                        $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] = 0.00;
                        $accountProduct[$customerData->customerSalesAccountID]['accountID'] = $customerData->customerSalesAccountID;
                    }
                }else{
                    if(isset($accountProduct[$pData['productSalesAccountID']]['debitTotal'])){
                        $accountProduct[$pData['productSalesAccountID']]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productSalesAccountID']]['debitTotal'] = $productTotal;
                        $accountProduct[$pData['productSalesAccountID']]['creditTotal'] = 0.00;
                        $accountProduct[$pData['productSalesAccountID']]['accountID'] = $pData['productSalesAccountID'];
                    }
                }


                if($averageCostingFlag == 1){
                    $locationProDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);

                    //set gl accounts for the journal entry
                    $productTotal = $product['pQuantity'] * $locationProDetails->locationProductAverageCostingPrice;
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                    }

                    if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                    }
                }else{
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                    }

                    if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                        $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                    }
                }

                if ($product['pTax']) {
                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                        $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxKey);
                        if($this->useAccounting == 1 && empty($taxData['taxSalesAccountID'])){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_TAX_SALES_ACCOUNT', array($taxData['taxName']))];
                        }

                        $totalCRAmounts += $productTax['tA'] * $creditNoteCustomCurrencyRate;
                            //set tax gl accounts for the journal Entry
                        if(isset($accountProduct[$taxData['taxSalesAccountID']]['debitTotal'])){
                            $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] += $productTax['tA'] * $creditNoteCustomCurrencyRate;
                        }else{
                            $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = $productTax['tA'] * $creditNoteCustomCurrencyRate;
                            $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = 0.00;
                            $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                        }
                    }
                }

            }

            if(isset($accountProduct[$customerData->customerReceviableAccountID]['creditTotal'])){
                $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] += $totalCRAmounts;
            }else{
                $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] = $totalCRAmounts;
                $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] = 0.00;
                $accountProduct[$customerData->customerReceviableAccountID]['accountID'] = $customerData->customerReceviableAccountID;
            }


            $i=0;
            $journalEntryAccounts = array();

            foreach ($accountProduct as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Direct Credit Note '.$creditNoteCode.'.';
                $i++;
            }

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $date,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Direct Credit Note '.$creditNoteCode.'.',
                'documentTypeID' => 6,
                'journalEntryDocumentID' => $creditNoteID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );
            $resultData = $this->saveJournalEntry($journalEntryData);

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($creditNoteID,6, 3);
            if(!$jEDocStatusUpdate['status']){
                return array(
                    'status'=> false,
                    'msg'=>$jEDocStatusUpdate['msg'],
                    );
            }

            if($resultData['status']){
                if (!empty($dimensionData)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$creditNoteCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                    if(!$saveRes['status']){
                        return ['status' => false,
                                'msg' => $saveRes['msg'],
                                'data' => $saveRes['data']
                            ];
                    }   
                }
                $status = true;
                $data = array('creditNoteID' => $creditNoteID);
                $msg = $this->getMessage('SUCC_RETURNAPI_PRODUCTDETAIL_ADD', array($creditNoteCode));
            }else{
                $status = false;
                $msg = $resultData['msg'];
                $data = $resultData['data'];
            }
        } else {
            $status = true;
            $data = array('creditNoteID' => $creditNoteID);
            $msg = $this->getMessage('SUCC_RETURNAPI_PRODUCTDETAIL_ADD', array($creditNoteCode));
        }
        return array('status' => $status, 'msg' => $msg, 'data' => $data);
    }


    public function saveJournalEntryDataForBuyBackCredit($creditNoteID, $creditNoteCode, $customerID, $productsData,$allProductTotal, $date, $averageCostingFlag, $creditNoteCustomCurrencyRate, $dimensionData, $ignoreBudgetLimit)
    {
        $accountProduct = array();
        $totalCRAmounts = 0;

        $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

        $custSalesAccountIsset = true;
        if($this->useAccounting == 1){
            if(empty($customerData->customerSalesAccountID)){
                $custSalesAccountIsset = false;
            }else if(empty($customerData->customerReceviableAccountID)){
                return ['status' => false, 'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode))];
            }
        }
        if ($this->useAccounting == 1) {
            foreach ($productsData as $key => $product) {
                $locationPID = $product['locationPID'];
                $productID = $product['pID'];
                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);
                if(empty($pData['productInventoryAccountID'])){
                    return array('status' => false, 'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])));
                }
                //  //check whether Product Cost of Goods Sold Gl account id set or not
                // if(empty($pData['productCOGSAccountID'])){
                //     return array('status' => false, 'msg' => $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName'])));
                // }

                // if(!$custSalesAccountIsset){
                //     if(empty($pData['productSalesAccountID'])){
                //         return array('status' => false, 'msg' => $this->getMessage('ERR_PRPODUCT_SALES_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])));
                //     }
                // }

                //calculate discount value
                $discountValue = 0;
                if ($product['discountType'] == "precentage") {
                    $discountValue = ($product['pUnitPrice']  * $product['pDiscount']/ 100);
                } else if ($product['discountType'] == "value") {
                    $discountValue = floatval($product['pDiscount']);
                }

                $productTotal = $product['pQuantity'] * ($product['pUnitPrice'] - $discountValue);
                $totalCRAmounts+=$productTotal;
                // if($custSalesAccountIsset){
                //     if(isset($accountProduct[$customerData->customerSalesAccountID]['debitTotal'])){
                //         $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] += $productTotal;
                //     }else{
                //         $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] = $productTotal;
                //         $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] = 0.00;
                //         $accountProduct[$customerData->customerSalesAccountID]['accountID'] = $customerData->customerSalesAccountID;
                //     }
                // }else{
                //     if(isset($accountProduct[$pData['productSalesAccountID']]['debitTotal'])){
                //         $accountProduct[$pData['productSalesAccountID']]['debitTotal'] += $productTotal;
                //     }else{
                //         $accountProduct[$pData['productSalesAccountID']]['debitTotal'] = $productTotal;
                //         $accountProduct[$pData['productSalesAccountID']]['creditTotal'] = 0.00;
                //         $accountProduct[$pData['productSalesAccountID']]['accountID'] = $pData['productSalesAccountID'];
                //     }
                // }


                if($averageCostingFlag == 1){
                    $locationProDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);

                    //set gl accounts for the journal entry
                    $productTotal = $product['pQuantity'] * $locationProDetails->locationProductAverageCostingPrice;
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                    }

                    // if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                    //     $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                    // }else{
                    //     $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                    //     $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                    //     $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                    // }
                }else{
                    if(isset($accountProduct[$pData['productInventoryAccountID']]['debitTotal'])){
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = $productTotal;
                        $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = 0.00;
                        $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                    }

                    // if(isset($accountProduct[$pData['productCOGSAccountID']]['creditTotal'])){
                    //     $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] += $productTotal;
                    // }else{
                    //     $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] = 0.00;
                    //     $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] =  $productTotal;
                    //     $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                    // }
                }

                if ($product['pTax']) {
                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                        $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxKey);
                        if($this->useAccounting == 1 && empty($taxData['taxSalesAccountID'])){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_TAX_SALES_ACCOUNT', array($taxData['taxName']))];
                        }

                        $totalCRAmounts += $productTax['tA'] * $creditNoteCustomCurrencyRate;
                            //set tax gl accounts for the journal Entry
                        if(isset($accountProduct[$taxData['taxSalesAccountID']]['debitTotal'])){
                            $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] += $productTax['tA'] * $creditNoteCustomCurrencyRate;
                        }else{
                            $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = $productTax['tA'] * $creditNoteCustomCurrencyRate;
                            $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = 0.00;
                            $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                        }
                    }
                }

            }

            if(isset($accountProduct[$customerData->customerReceviableAccountID]['creditTotal'])){
                $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] += $totalCRAmounts;
            }else{
                $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] = $totalCRAmounts;
                $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] = 0.00;
                $accountProduct[$customerData->customerReceviableAccountID]['accountID'] = $customerData->customerReceviableAccountID;
            }


            $i=0;
            $journalEntryAccounts = array();

            foreach ($accountProduct as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Buy Back Credit Note '.$creditNoteCode.'.';
                $i++;
            }

            //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);


            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $date,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Buy Back Credit Note '.$creditNoteCode.'.',
                'documentTypeID' => 6,
                'journalEntryDocumentID' => $creditNoteID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
            );
            $resultData = $this->saveJournalEntry($journalEntryData);

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($creditNoteID,6, 3);
            if(!$jEDocStatusUpdate['status']){
                return array(
                    'status'=> false,
                    'msg'=>$jEDocStatusUpdate['msg'],
                    );
            }

            if($resultData['status']){
                if (!empty($dimensionData)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$creditNoteCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                    if(!$saveRes['status']){
                        return ['status' => false,
                                'msg' => $saveRes['msg'],
                                'data' => $saveRes['data']
                            ];
                    }   
                }
                $status = true;
                $data = array('creditNoteID' => $creditNoteID);
                $msg = $this->getMessage('SUCC_RETURNAPI_PRODUCTDETAIL_ADD', array($creditNoteCode));
            }else{
                $status = false;
                $msg = $resultData['msg'];
                $data = $resultData['data'];
            }
        } else {
            $status = true;
            $data = array('creditNoteID' => $creditNoteID);
            $msg = $this->getMessage('SUCC_RETURNAPI_PRODUCTDETAIL_ADD', array($creditNoteCode));
        }
        return array('status' => $status, 'msg' => $msg, 'data' => $data);
    }

    public function reverseDirectCreditNoteProducts($creditNoteID, $dataSet)
    {
        $docType = 'Direct Credit Note';
        // reverse serial products
        if(sizeof($dataSet['creditNoteSerial']) > 0){
            $batchSerialQty = [];
            foreach ($dataSet['creditNoteSerial'] as $key => $serialDataSet) {
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID(NULL, $serialDataSet['productSerialID']);
                $serialSoldFlag = false;

                $this->productIds[] = $serialDataSet['productID'];

                foreach ($itemInData as $key => $serialDta) {
                    if($serialDta['itemInSoldQty'] == 0){
                        if($serialDta['itemInBatchID'] != ''){
                            $batchSerialQty[$serialDta['itemInBatchID']] ++;
                        }
                        $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($serialDataSet['productID'], $serialDataSet['locationID'])->current();
                        $returnQty = '1';
                        $serialSoldFlag = true;
                        //update locationproduct ItemIn and ItemOut
                        $locProStatue = $this->updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $serialDta, $returnQty,'Direct Credit note cancel');
                        if($locProStatue['status'] != 'true'){
                            $errorData = array(
                                'msg' => $locProStatue['msg'],
                                'status' => false,
                            );
                            return $errorData;
                        }

                        //update serial table
                        $data = array(
                            'productSerialReturned' => '1',
                            );

                        $updateSerialTable = $this->CommonTable('Inventory\Model\ProductSerialTable')
                                ->updateProductSerialData($data, $serialDta['itemInSerialID']);


                    }
                }
                if(!$serialSoldFlag){
                    $errorData = array(
                        'msg' => $this->getMessage('ERR_SERIAL_PRODUCT_IN_USE'),
                        'status' => false,
                        );
                    return $errorData;
                }

            }
            //update batch-serilaItem batch table
            if(!empty($batchSerialQty)){
                foreach ($batchSerialQty as $key => $value) {
                    $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($key);
                    $newBatchQty = floatval($batchDetails->productBatchQuantity) - floatval($value);

                    //update batchTable
                    $data = array(
                        'productBatchQuantity' => $newBatchQty,
                        );
                    $update = $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($key, $data);
                    if(!$update){
                        $errorData = array(
                            'msg' => $this->getMessage('ERR_SERIAL_BATCH_PRODUCT_UPDATE'),
                            'status' => false,
                        );
                        return $errorData;
                    }
                }
            }

        }

        //reverse batch products
        if(sizeof($dataSet['creditNoteBatch']) > 0) {

            foreach ($dataSet['creditNoteBatch'] as $key => $batchValue) {
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID($batchValue['productBatchID'],null);

                $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($batchValue['productID'], $batchValue['locationID'])->current();
                $totalUpdatedQty = 0;

                $this->productIds[] = $batchValue['productID'];

                foreach ($itemInData as $key => $itemInBatchValue) {

                    $returnQty = floatval($itemInBatchValue['itemInQty']) - floatval($itemInBatchValue['itemInSoldQty']);
                    $totalUpdatedQty += $returnQty;
                    //update locationproduct ItemIn and ItemOut
                    $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $itemInBatchValue, $returnQty, 'Direct Credit note cancel');
                    if($locUpdateStatus['status'] != 'true'){
                        $errorData = array(
                            'msg' => $locUpdateStatus['msg'],
                            'status' => false,
                        );
                        return $errorData;
                    }

                    //update batch table
                    if($batchValue['itemInSerialID'] == ''){
                        $data = array(
                            'productBatchQuantity' => '0',
                        );
                        $updateBatchTable = $this->CommonTable('Inventory\Model\ProductBatchTable')
                                   ->updateProductBatchQuantity($itemInBatchValue['itemInBatchID'], $data);
                    }

                }
                if(floatval($totalUpdatedQty) != floatval($batchValue['creditNoteProductQuantity'])){
                    $errorData = array(
                        'msg' => $this->getMessage('ERR_BATCH_PRODUCT_IN_USE'),
                        'status' => false,
                        );
                    return $errorData;

                }

            }

        }

        //reverse normal products
        if(sizeof($dataSet['creditNoteNormalItem']) > 0){

            $exisistdCredDetails = [];
            $itemInIds =[];
            foreach ($dataSet['creditNoteNormalItem'] as $key => $normalDataSet) {
                if(empty($exisistdCredDetails[$normalDataSet['creditNoteProID']])){
                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getProductDetailsBylocationProductIDDocTypeAndDocumentID($normalDataSet['locationProductID'], $docType, $normalDataSet['creditNoteID']);

                    $this->productIds[] = $normalDataSet['productID'];

                    foreach ($itemInDetails as $key => $itemInSingleValue) {
                        if(empty($itemInIds[$itemInSingleValue['itemInID']])) {
                            if($itemInSingleValue['itemInSoldQty'] == 0){
                                $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($normalDataSet['productID'], $normalDataSet['locationID'])->current();

                                $returnQty = floatval($itemInSingleValue['itemInQty']);
                                //update locationproduct ItemIn and ItemOut
                                $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $itemInSingleValue, $returnQty, 'Direct Credit note cancel');
                                if($locUpdateStatus['status'] != 'true'){
                                    $errorData = array(
                                        'msg' => $locUpdateStatus['msg'],
                                        'status' => false,
                                    );
                                    return $errorData;
                                }
                            } else {
                               $errorData = array(
                                    'msg' => $this->getMessage('ERR_NORMAL_PRODUCT_IN_USE'),
                                    'status' => false,
                                );
                                return $errorData;
                            }
                        }
                        $itemInIds[$itemInSingleValue['itemInID']] = $itemInSingleValue;
                    }
                    // validate location product update or not
                    $exisistdCredDetails[$normalDataSet['creditNoteProID']] = $normalDataSet;

                }
            }
        }

        $errorData = array(
            'status' => true,
        );
        return $errorData;
    }

    public function reverseBuyBackCreditNoteProducts($creditNoteID, $dataSet)
    {
        $docType = 'Buy Back Credit Note';
        // reverse serial products
        if(sizeof($dataSet['creditNoteSerial']) > 0){
            $batchSerialQty = [];
            foreach ($dataSet['creditNoteSerial'] as $key => $serialDataSet) {
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID(NULL, $serialDataSet['productSerialID']);
                $serialSoldFlag = false;

                $this->productIds[] = $serialDataSet['productID'];

                foreach ($itemInData as $key => $serialDta) {
                    if($serialDta['itemInSoldQty'] == 0){
                        if($serialDta['itemInBatchID'] != ''){
                            $batchSerialQty[$serialDta['itemInBatchID']] ++;
                        }
                        $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($serialDataSet['productID'], $serialDataSet['locationID'])->current();
                        $returnQty = '1';
                        $serialSoldFlag = true;
                        //update locationproduct ItemIn and ItemOut
                        $locProStatue = $this->updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $serialDta, $returnQty,'Direct Credit note cancel');
                        if($locProStatue['status'] != 'true'){
                            $errorData = array(
                                'msg' => $locProStatue['msg'],
                                'status' => false,
                            );
                            return $errorData;
                        }

                        //update serial table
                        $data = array(
                            'productSerialReturned' => '1',
                            );

                        $updateSerialTable = $this->CommonTable('Inventory\Model\ProductSerialTable')
                                ->updateProductSerialData($data, $serialDta['itemInSerialID']);


                    }
                }
                if(!$serialSoldFlag){
                    $errorData = array(
                        'msg' => $this->getMessage('ERR_SERIAL_PRODUCT_IN_USE'),
                        'status' => false,
                        );
                    return $errorData;
                }

            }
            //update batch-serilaItem batch table
            if(!empty($batchSerialQty)){
                foreach ($batchSerialQty as $key => $value) {
                    $batchDetails = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($key);
                    $newBatchQty = floatval($batchDetails->productBatchQuantity) - floatval($value);

                    //update batchTable
                    $data = array(
                        'productBatchQuantity' => $newBatchQty,
                        );
                    $update = $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($key, $data);
                    if(!$update){
                        $errorData = array(
                            'msg' => $this->getMessage('ERR_SERIAL_BATCH_PRODUCT_UPDATE'),
                            'status' => false,
                        );
                        return $errorData;
                    }
                }
            }

        }

        //reverse batch products
        if(sizeof($dataSet['creditNoteBatch']) > 0) {

            foreach ($dataSet['creditNoteBatch'] as $key => $batchValue) {
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetailsByBatchIDOrSerialID($batchValue['productBatchID'],null);

                $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($batchValue['productID'], $batchValue['locationID'])->current();
                $totalUpdatedQty = 0;

                $this->productIds[] = $batchValue['productID'];

                foreach ($itemInData as $key => $itemInBatchValue) {

                    $returnQty = floatval($itemInBatchValue['itemInQty']) - floatval($itemInBatchValue['itemInSoldQty']);
                    $totalUpdatedQty += $returnQty;
                    //update locationproduct ItemIn and ItemOut
                    $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $itemInBatchValue, $returnQty, 'Buy Back Credit note cancel');
                    if($locUpdateStatus['status'] != 'true'){
                        $errorData = array(
                            'msg' => $locUpdateStatus['msg'],
                            'status' => false,
                        );
                        return $errorData;
                    }

                    //update batch table
                    if($batchValue['itemInSerialID'] == ''){
                        $data = array(
                            'productBatchQuantity' => '0',
                        );
                        $updateBatchTable = $this->CommonTable('Inventory\Model\ProductBatchTable')
                                   ->updateProductBatchQuantity($itemInBatchValue['itemInBatchID'], $data);
                    }

                }
                if(floatval($totalUpdatedQty) != floatval($batchValue['creditNoteProductQuantity'])){
                    $errorData = array(
                        'msg' => $this->getMessage('ERR_BATCH_PRODUCT_IN_USE'),
                        'status' => false,
                        );
                    return $errorData;

                }

            }

        }

        //reverse normal products
        if(sizeof($dataSet['creditNoteNormalItem']) > 0){

            $exisistdCredDetails = [];
            $itemInIds =[];
            foreach ($dataSet['creditNoteNormalItem'] as $key => $normalDataSet) {
                if(empty($exisistdCredDetails[$normalDataSet['creditNoteProID']])){
                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getProductDetailsBylocationProductIDDocTypeAndDocumentID($normalDataSet['locationProductID'], $docType, $normalDataSet['creditNoteID']);

                    $this->productIds[] = $normalDataSet['productID'];

                    foreach ($itemInDetails as $key => $itemInSingleValue) {
                        if(empty($itemInIds[$itemInSingleValue['itemInID']])) {
                            if($itemInSingleValue['itemInSoldQty'] == 0){
                                $locationProductDetails = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductDetailsWithProductDetailsByLocIDAndProductID($normalDataSet['productID'], $normalDataSet['locationID'])->current();

                                $returnQty = floatval($itemInSingleValue['itemInQty']);
                                //update locationproduct ItemIn and ItemOut
                                $locUpdateStatus = $this->updateLocationProductAndItemInAndItemOut($creditNoteID, $locationProductDetails, $itemInSingleValue, $returnQty, 'Buy Back Credit note cancel');
                                if($locUpdateStatus['status'] != 'true'){
                                    $errorData = array(
                                        'msg' => $locUpdateStatus['msg'],
                                        'status' => false,
                                    );
                                    return $errorData;
                                }
                            } else {
                               $errorData = array(
                                    'msg' => $this->getMessage('ERR_NORMAL_PRODUCT_IN_USE'),
                                    'status' => false,
                                );
                                return $errorData;
                            }
                        }
                        $itemInIds[$itemInSingleValue['itemInID']] = $itemInSingleValue;
                    }
                    // validate location product update or not
                    $exisistdCredDetails[$normalDataSet['creditNoteProID']] = $normalDataSet;

                }
            }
        }

        $errorData = array(
            'status' => true,
        );
        return $errorData;
    }

    public function saveBuyBackCreditNoteDetails($directSaveData, $buyBackNoteDirectType) 
    {

        $customerID = $directSaveData['customerID'];
        $customerName = $directSaveData['customerName'];
        $customCurrencyId = $directSaveData['customCurrencyId'];
        $dimensionData = $directSaveData['dimensionData'];
        $ignoreBudgetLimit = $directSaveData['ignoreBudgetLimit'];
        $creditNoteCustomCurrencyRate = (float) $directSaveData['creditNoteCustomCurrencyRate'];
        $inactiveFlag = false;
        $inactiveFlag =  filter_var($directSaveData['invFlag'], FILTER_VALIDATE_BOOLEAN);
        $invCloseFlag = filter_var($directSaveData['invCloseFlag'], FILTER_VALIDATE_BOOLEAN);
        $dupString = "DUP";
        $dupStringPreFix = false;
        $dupStringPostFix = true;
        $cdNotDirFlag = false;
        $cdNotBuyBackFlag = false;
                    
        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        
        if(!is_null($cust)){
            $products = $directSaveData['products'];
            $invoiceID = $directSaveData['invoiceID'];
            $locationID = $directSaveData['locationID'];

            if ($invoiceID != "") {
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                if ($invoiceDetails['statusID'] == '10') {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_CREDIT_NOTE_INVOICE_EDITED')];
                }
            }
                
            if($buyBackNoteDirectType == 'true'){
                $cdNotBuyBackFlag = true;
            }

            if($buyBackNoteDirectType == 'true' && $invoiceID == ''){
                $invoiceID = 0;
            } else if($buyBackNoteDirectType == 'true' && $invoiceID != ''){
                //validate customerID and invoiceID
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                if($invoiceDetails['customerID'] != $customerID){
                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CUSTOMER_NOT_MATCH')];
                } else if($invoiceDetails['locationID'] != $locationID){
                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_LOC_NOT_MATCH')];
                }
                
            }
            $totalPrice = (float) trim($directSaveData['creditNoteTotalPrice']) * $creditNoteCustomCurrencyRate;
            
            if($buyBackNoteDirectType == 'true' && $invoiceID != ''){
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceDataByInvoiceId($invoiceID)->current();
                $invoiceTotalAmount = $invoiceDetails['salesinvoiceTotalAmount'];

                $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceID($invoiceID)->current();

                $creditNoteAmount = 0;
                if (!is_null($creditNoteTotalAmountForTheInvoice['creditNoteTot'])) {
                    $creditNoteAmount = $creditNoteTotalAmountForTheInvoice['creditNoteTot'];
                }

                $restAmountOfInvoice = $invoiceTotalAmount - $creditNoteAmount;

                if ($totalPrice > $restAmountOfInvoice) {
                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_TOTAL_AMOUNT')];
                }

            }
            
            $subProducts = $directSaveData['subProducts'];
            $creditNoteCode = $directSaveData['creditNoteCode'];
            $date = $this->convertDateToStandardFormat($directSaveData['date']);
            $comment = $directSaveData['creditNoteComment'];
            $paymentTermID = $directSaveData['paymentTermID'];
            $creditNoteInvoiceDiscountType = $directSaveData['creditNoteInvoiceDiscountType'];
            $creditNoteInvoiceDiscountAmount = $directSaveData['creditNoteInvoiceDiscountAmount'];
            $creditNotePromotionDiscountType = $directSaveData['creditNotePromotionDiscountType'];
            $creditNotePromotionDiscountAmount = $directSaveData['creditNotePromotionDiscountAmount'];
            $creditNotePaymentEligible = 0;
            $creditBalance = 0;
            $oustandingBalance = 0;
            

            $result = $this->getReferenceNoForLocation('8', $locationID);
            $locationReferenceID = $result['locRefID'];
            //check credit note numer already exist in credit note table if exist give next number for credit note code
            while ($creditNoteCode) {
                if ($this->CommonTable('Invoice\Model\CreditNoteTable')->checkCreditNoteByCode($creditNoteCode)->current()) {
                    if ($locationReferenceID) {
                        $newCreditNoteCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newCreditNoteCode == $creditNoteCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $creditNoteCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $creditNoteCode = $newCreditNoteCode;
                        }
                    } else {
                        return ['status' => false, 'msg' => $this->getMessage('ERR_CREDIT_CODE_EXIST')];
                    }
                } else {
                    break;
                }
            }

            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            if(empty($FIFOCostingFlag) && empty($averageCostingFlag)){
                return ['status' => false, 'msg' => $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE')];
            }

            $accountProduct = array();
            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountsData = $glAccountExist->current();
            $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

            $custSalesAccountIsset = true;
            if($this->useAccounting == 1){
                if(empty($customerData->customerSalesAccountID)){
                    $custSalesAccountIsset = false;
                }else if(empty($customerData->customerReceviableAccountID)){
                    return ['status' => false, 'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode))];
                }
            }


            $creditDeviation = 0;
            $outstandingDeviation = 0;
            //no need to process invoice details for direct credit notes
            if ($invoiceID == 0) {
                $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                $customerCurrentBalance = $cust->customerCurrentBalance;
            } else {
                if($cust->customerCurrentBalance == 0){
                    $customerCreditBalance = $cust->customerCurrentCredit + $totalPrice;
                    $customerCurrentBalance = $cust->customerCurrentBalance;
                    $creditDeviation = $totalPrice;
                } else if($cust->customerCurrentBalance - $totalPrice < 0){
                    $customerCurrentBalance = 0;
                    $customerCreditBalance = $cust->customerCurrentCredit + ($totalPrice - $cust->customerCurrentBalance);
                    $creditDeviation = $totalPrice - $cust->customerCurrentBalance;
                    $outstandingDeviation = -($cust->customerCurrentBalance);
                } else {
                    $customerCurrentBalance = $cust->customerCurrentBalance - $totalPrice;
                    $customerCreditBalance = $cust->customerCurrentCredit;
                    $outstandingDeviation = -$totalPrice;
                }
            }                        

            $custData = array(
                'customerID' => $customerID,
                'customerCurrentCredit' => $customerCreditBalance,
                'customerCurrentBalance' => $customerCurrentBalance,
            );
           
            $customer = new Customer;
            $customer->exchangeArray($custData);
            $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCurrentAndCreditBalance($customer);

            $entityID = $this->createEntity();
            $creditNoteData = array(
                'creditNoteCode' => $creditNoteCode,
                'customerID' => $customerID,
                'locationID' => $locationID,
                'paymentTermID' => $paymentTermID,
                'invoiceID' => $invoiceID,
                'creditNoteDate' => $date,
                'creditNoteTotal' => $totalPrice,
                'creditNoteComment' => $comment,
                'statusID' => 3,
                'creditNotePaymentEligible' => $creditNotePaymentEligible,
                'creditNotePaymentAmount' => $creditBalance,
                'customCurrencyId' => $customCurrencyId,
                'creditNoteCustomCurrencyRate' => $creditNoteCustomCurrencyRate,
                'creditNoteInvoiceDiscountType' => $creditNoteInvoiceDiscountType,
                'creditNoteInvoiceDiscountAmount' => $creditNoteInvoiceDiscountAmount,
                'creditNotePromotionDiscountType' => $creditNotePromotionDiscountType,
                'creditNotePromotionDiscountAmount' => $creditNotePromotionDiscountAmount,
                'creditNoteDirectFlag' => $cdNotDirFlag,
                'creditNoteBuyBackFlag' => $cdNotBuyBackFlag,
                'entityID' => $entityID,
                'creditDeviation' => $creditDeviation,
                'outstandingDeviation' => $outstandingDeviation,
                'ableToCancel' => true
            );

            $creditNote = new CreditNote;
            $creditNote->exchangeArray($creditNoteData);
            $creditNoteID = $this->CommonTable('Invoice\Model\CreditNoteTable')->saveCreditNote($creditNote);

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }

            //check and update the invoice remainin discount amount
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
            if ($creditNoteInvoiceDiscountType == 'value'){
                if ($creditNoteInvoiceDiscountAmount > 0) {
                    if ($invoiceData->salesInvoiceRemainingDiscValue != null) {
                        $invoiceDiscountRemaingValue = $invoiceData->salesInvoiceRemainingDiscValue;
                    } else {
                        $invoiceDiscountRemaingValue = $invoiceData->salesInvoiceDiscountRate;
                    }
                    if($invoiceDiscountRemaingValue >= $creditNoteInvoiceDiscountAmount){
                        $remainingDiscount = $invoiceDiscountRemaingValue - $creditNoteInvoiceDiscountAmount;
                        $updateInvoiceDiscountValue = $this->CommonTable('Invoice\Model\InvoiceTable')
                        ->updateInvoiceRemainingDiscountValue($invoiceID,$remainingDiscount);
                    } else {
                        return ['status' => false, 'msg' => $this->getMessage('ERR_DISCT_CANT_BE_MORE_THAN_INVOICE_DISCOUNT')];
                    }

                }
            }

            if ($creditNotePromotionDiscountType == 'value') {
                if ($creditNotePromotionDiscountAmount > 0) {
                    if ($invoiceData->salesInvoiceRemainingPromotionDiscValue != null) {
                        $promotionDiscountRemaingValue = $invoiceData->salesInvoiceRemainingPromotionDiscValue;
                    } else {
                        $promotionDiscountRemaingValue = $invoiceData->salesInvoicePromotionDiscount;
                    }
                    if($promotionDiscountRemaingValue >= $creditNotePromotionDiscountAmount){
                        $remainingDiscount = $promotionDiscountRemaingValue - $creditNotePromotionDiscountAmount;
                        $updatePromotionDiscountValue = $this->CommonTable('Invoice\Model\InvoiceTable')
                        ->updatePromotionRemainingDiscountValue($invoiceID,$remainingDiscount);
                    } else {
                        return ['status' => false, 'msg' => $this->getMessage('ERR_DISCT_CANT_BE_MORE_THAN_PROMOTION_DISCOUNT')];
                    }
                }

            }

            $allProductTotal = 0;
            foreach ($products as $product) {
                $locationPID = $product['locationPID'];
                //get location product data for update average costing.
                $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                $locationProductQuantity = $locationProductData->locationProductQuantity;
                $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;
             
                $productBaseQty = $product['pQuantity'];
                $productType = $product['productType'];
                $productConversionRate = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomConversionValue($product['pID'], $product['pUom']);
                $discountValue = 0;
                if ($product['discountType'] == "precentage") {
                    $discountValue = ($product['pUnitPrice']  * $product['pDiscount']/ 100);
                } else if ($product['discountType'] == "value") {
                    $discountValue = floatval($product['pDiscount']);
                }
                $productTotal = $product['pUnitPrice']*$product['pQuantity'] - $discountValue;
                $allProductTotal+=$productTotal;

                //If product is a batch product
                if (array_key_exists('bProducts', $product)) {
                    $batchProductCount = 0;
                    $batchSerialProductCount = 0;
                    foreach ($product['bProducts'] as $bProduct) {

                        $batchProductCount++;
                        $bPData = array(
                            'locationProductID' => $locationPID,
                            'productBatchCode' => $bProduct['bCode'],
                            'productBatchExpiryDate' => $this->convertDateToStandardFormat($bProduct['eDate']),
                            'productBatchWarrantyPeriod' => $bProduct['warnty'],
                            'productBatchManufactureDate' => $this->convertDateToStandardFormat($bProduct['mDate']),
                            'productBatchQuantity' => $bProduct['bQty']
                        );

                        $batchProduct = new ProductBatch();
                        $batchProduct->exchangeArray($bPData);
                        $insertedBID = $this->CommonTable('Inventory\Model\ProductBatchTable')->saveBatchProduct($batchProduct);
                        if(!$insertedBID){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }
                        //if product is a batch and serial product
                        if (array_key_exists('sProducts', $product)) {
                            foreach ($product['sProducts'] as $sProduct) {
                                $batchSerialProductCount++;
                                if (isset($sProduct['sBCode'])) {

                                    if ($sProduct['sBCode'] == $bProduct['bCode']) {
                                        $sPData = array(
                                            'locationProductID' => $locationPID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialCode' => $sProduct['sCode'],
                                            'productSerialExpireDate' => $this->convertDateToStandardFormat($sProduct['sEdate']),
                                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                                            'productSerialWarrantyPeriodType' => $sProduct['sWarrantyType'],
                                        );
                                        $serialPr = new ProductSerial();
                                        $serialPr->exchangeArray($sPData);
                                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                                        if(!$insertedSID){
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                        }
                                        
                                        if ($batchSerialProductCount == 1) {
                                            $creditNoteProductData = array(
                                                'creditNoteID' => $creditNoteID,
                                                'invoiceProductID' => 0,
                                                'productID' => $product['pID'],
                                                'creditNoteProductPrice' => $product['pUnitPrice'] * $creditNoteCustomCurrencyRate,
                                                'creditNoteProductDiscount' => $product['pDiscount'],
                                                'creditNoteProductDiscountType' => $product['discountType'],
                                                'creditNoteProductTotal' => $product['pTotal'] * $creditNoteCustomCurrencyRate,
                                                'creditNoteProductQuantity' => $product['pQuantity']
                                            );
                                            $creditNoteProduct = new CreditNoteProduct;
                                            $creditNoteProduct->exchangeArray($creditNoteProductData);
                                            //update credit note product table
                                            $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                                                ->saveCreditNoteProducts($creditNoteProduct);
                                            if(!$creditNoteProductID){
                                                return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                            }
                                        }
                                        //save item in details
                                        $itemInData = array(
                                            'itemInDocumentType' => 'Buy Back Credit Note',
                                            'itemInDocumentID' => $creditNoteID,
                                            'itemInLocationProductID' => $locationPID,
                                            'itemInBatchID' => $insertedBID,
                                            'itemInSerialID' => $insertedSID,
                                            'itemInQty' => 1,
                                            'itemInPrice' => $product['pUnitPrice'],
                                            'itemInDiscount' => $discountValue,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                        if(!$itemInID){
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                        }

                                        $data = array(
                                            'creditNoteProductID' => $creditNoteProductID,
                                            'productBatchID' => $insertedBID,
                                            'productSerialID' => $insertedSID,
                                            'creditNoteSubProductQuantity' => 1,
                                        );
                                        $creditNoteSubProduct = new CreditNoteSubProduct;
                                        $creditNoteSubProduct->exchangeArray($data);
                                        $creditNoteSubProductID = $this->CommonTable('Invoice\Model\CreditNoteSubProductTable')->saveCreditNoteSubProduct($creditNoteSubProduct);

                                        if (array_key_exists('pTax', $product)) {
                                            if (array_key_exists('tL', $product['pTax'])) {
                                                foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                                    $creditNoteProductTaxData = array(
                                                        'creditNoteProductID' => $creditNoteProductID,
                                                        'taxID' => $taxKey,
                                                        'creditNoteProductTaxPercentage' => $productTax['tP'],
                                                        'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
                                                    );
                                                    $creditNoteProductTax = new CreditNoteProductTax();
                                                    $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
                                                    $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->saveCreditNoteProductTax($creditNoteProductTax);
                                                }
                                            }
                                        }
                                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                                        $newPQtyData = array(
                                            'locationProductQuantity' => $newPQty
                                        );
                                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                        if(!$status){
                                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                        }
                                    }
                                }
                            }
                        } else {
                            if ($batchProductCount == 1) {
                                $creditNoteProductData = array(
                                    'creditNoteID' => $creditNoteID,
                                    'invoiceProductID' => 0,
                                    'productID' => $product['pID'],
                                    'creditNoteProductPrice' => $product['pUnitPrice'] * $creditNoteCustomCurrencyRate,
                                    'creditNoteProductDiscount' => $product['pDiscount'],
                                    'creditNoteProductDiscountType' => $product['discountType'],
                                    'creditNoteProductTotal' => $product['pTotal'] * $creditNoteCustomCurrencyRate,
                                    'creditNoteProductQuantity' => $product['pQuantity']
                                );
                                $creditNoteProduct = new CreditNoteProduct;
                                $creditNoteProduct->exchangeArray($creditNoteProductData);
                                //update credit note product table
                                $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                                                ->saveCreditNoteProducts($creditNoteProduct);
                                if(!$creditNoteProductID){
                                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                }
                            }
                            //save item in details
                            $itemInData = array(
                                'itemInDocumentType' => 'Buy Back Credit Note',
                                'itemInDocumentID' => $creditNoteID,
                                'itemInLocationProductID' => $locationPID,
                                'itemInBatchID' => $insertedBID,
                                'itemInSerialID' => NULL,
                                'itemInQty' => $bProduct['bQty'],
                                'itemInPrice' => $product['pUnitPrice'],
                                'itemInDiscount' => $discountValue,
                                'itemInDateAndTime' => $this->getGMTDateTime(),
                            );
                            $itemInModel = new ItemIn();
                            $itemInModel->exchangeArray($itemInData);
                            $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                            if(!$itemInID){
                                return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                            }

                            $data = array(
                                'creditNoteProductID' => $creditNoteProductID,
                                'productBatchID' => $insertedBID,
                                'productSerialID' => NULL,
                                'creditNoteSubProductQuantity' => $bProduct['bQty'],
                            );
                            $creditNoteSubProduct = new CreditNoteSubProduct;
                            $creditNoteSubProduct->exchangeArray($data);
                            $creditNoteSubProductID = $this->CommonTable('Invoice\Model\CreditNoteSubProductTable')->saveCreditNoteSubProduct($creditNoteSubProduct);
                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $creditNoteProductTaxData = array(
                                            'creditNoteProductID' => $creditNoteProductID,
                                            'taxID' => $taxKey,
                                            'creditNoteProductTaxPercentage' => $productTax['tP'],
                                            'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
                                        );
                                        $creditNoteProductTax = new CreditNoteProductTax();
                                        $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
                                        $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->saveCreditNoteProductTax($creditNoteProductTax);
                                    }
                                }
                            }
                            if ($batchProductCount == 1) {
                                $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                                $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                                $newPQtyData = array(
                                    'locationProductQuantity' => $newPQty
                                );
                                $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                                if(!$status){
                                    return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                                }
                            }
                        }
                    }
                } elseif (array_key_exists('sProducts', $product)) {
                    $serialProductCount = 0;
                    //If the product is a serial product
                    foreach ($product['sProducts'] as $sProduct) {
                        $serialProductCount++;
                        $sPData = array('locationProductID' => $locationPID,
                            'productSerialCode' => $sProduct['sCode'],
                            'productSerialExpireDate' => $sProduct['sEdate'],
                            'productSerialWarrantyPeriod' => $sProduct['sWarranty'],
                            'productSerialWarrantyPeriodType' => (int)$sProduct['sWarrantyType'],
                        );
                        $serialPr = new ProductSerial();
                        $serialPr->exchangeArray($sPData);
                        $insertedSID = $this->CommonTable('Inventory\Model\ProductSerialTable')->saveSerialProduct($serialPr);
                        if(!$insertedSID){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }
                        if ($serialProductCount == 1) {
                            $creditNoteProductData = array(
                                'creditNoteID' => $creditNoteID,
                                'invoiceProductID' => 0,
                                'productID' => $product['pID'],
                                'creditNoteProductPrice' => $product['pUnitPrice'] * $creditNoteCustomCurrencyRate,
                                'creditNoteProductDiscount' => $product['pDiscount'],
                                'creditNoteProductDiscountType' => $product['discountType'],
                                'creditNoteProductTotal' => $product['pTotal'] * $creditNoteCustomCurrencyRate,
                                'creditNoteProductQuantity' => $product['pQuantity']
                            );
                            $creditNoteProduct = new CreditNoteProduct;
                            $creditNoteProduct->exchangeArray($creditNoteProductData);
                            //update credit note product table
                            $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                                            ->saveCreditNoteProducts($creditNoteProduct);
                            if(!$creditNoteProductID){
                                return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                            }
                        }
                        //save item in details
                        $itemInData = array(
                            'itemInDocumentType' => 'Buy Back Credit Note',
                            'itemInDocumentID' => $creditNoteID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => $insertedSID,
                            'itemInQty' => 1,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }

                        $data = array(
                            'creditNoteProductID' => $creditNoteProductID,
                            'productBatchID' => NULL,
                            'productSerialID' => $insertedSID,
                            'creditNoteSubProductQuantity' => 1,
                        );
                        $creditNoteSubProduct = new CreditNoteSubProduct;
                        $creditNoteSubProduct->exchangeArray($data);
                        $creditNoteSubProductID = $this->CommonTable('Invoice\Model\CreditNoteSubProductTable')->saveCreditNoteSubProduct($creditNoteSubProduct);
                        if ($serialProductCount == 1) {
                            if (array_key_exists('pTax', $product)) {
                                if (array_key_exists('tL', $product['pTax'])) {
                                    foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                        $creditNoteProductTaxData = array(
                                            'creditNoteProductID' => $creditNoteProductID,
                                            'taxID' => $taxKey,
                                            'creditNoteProductTaxPercentage' => $productTax['tP'],
                                            'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
                                        );
                                        $creditNoteProductTax = new CreditNoteProductTax();
                                        $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
                                        $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->saveCreditNoteProductTax($creditNoteProductTax);
                                    }
                                }
                            }
                        }
                        $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                        $newPQty = floatval($currentPQty->locationProductQuantity) + floatval(1);
                        $newPQtyData = array(
                            'locationProductQuantity' => $newPQty
                        );
                        $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                        if(!$status){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }
                    }
                } else {
                    // if the product is non serial and non batch
                    $creditNoteProductData = array(
                        'creditNoteID' => $creditNoteID,
                        'invoiceProductID' => 0,
                        'productID' => $product['pID'],
                        'creditNoteProductPrice' => $product['pUnitPrice'] * $creditNoteCustomCurrencyRate,
                        'creditNoteProductDiscount' => $product['pDiscount'],
                        'creditNoteProductDiscountType' => $product['discountType'],
                        'creditNoteProductTotal' => $product['pTotal'] * $creditNoteCustomCurrencyRate,
                        'creditNoteProductQuantity' => $product['pQuantity']
                    );
                    $creditNoteProduct = new CreditNoteProduct;
                    $creditNoteProduct->exchangeArray($creditNoteProductData);
                    //update credit note product table
                    $creditNoteProductID = $this->CommonTable('Invoice\Model\CreditNoteProductTable')
                                    ->saveCreditNoteProducts($creditNoteProduct);
                    if(!$creditNoteProductID){
                        return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                    }

                    if ($productType != 2) {
                        $itemInData = array(
                            'itemInDocumentType' => 'Buy Back Credit Note',
                            'itemInDocumentID' => $creditNoteID,
                            'itemInLocationProductID' => $locationPID,
                            'itemInBatchID' => NULL,
                            'itemInSerialID' => NULL,
                            'itemInQty' => $productBaseQty,
                            'itemInPrice' => $product['pUnitPrice'],
                            'itemInDiscount' => $discountValue,
                            'itemInDateAndTime' => $this->getGMTDateTime(),
                        );
                        $itemInModel = new ItemIn();
                        $itemInModel->exchangeArray($itemInData);
                        $itemInID = $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                        if(!$itemInID){
                            return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                        }
                    }
                    if (array_key_exists('pTax', $product)) {
                        if (array_key_exists('tL', $product['pTax'])) {
                            foreach ($product['pTax']['tL'] as $taxKey => $productTax) {
                                $creditNoteProductTaxData = array(
                                    'creditNoteProductID' => $creditNoteProductID,
                                    'taxID' => $taxKey,
                                    'creditNoteProductTaxPercentage' => $productTax['tP'],
                                    'creditNoteTaxAmount' => (float) $productTax['tA'] * $creditNoteCustomCurrencyRate
                                );
                                $creditNoteProductTax = new CreditNoteProductTax();
                                $creditNoteProductTax->exchangeArray($creditNoteProductTaxData);
                                $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->saveCreditNoteProductTax($creditNoteProductTax);
                            }
                        }
                    }
                    $currentPQty = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationPID);
                    if ($productType == 2) {
                        $productBaseQty = 0;
                    }

                    $newPQty = floatval($currentPQty->locationProductQuantity) + floatval($productBaseQty);
                    $newPQtyData = array(
                        'locationProductQuantity' => $newPQty
                    );
                    $status = $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductQuantity($newPQtyData, $locationPID);
                    if(!$status){
                        return ['status' => false, 'msg' => $this->getMessage('ERR_DIR_CREDIT_CREATE')];
                    }
                }
                $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationPID);
                $locationPrTotalQty = 0;
                $locationPrTotalPrice = 0;
                $newItemInIds = array();
                if(count($itemInData) > 0){
                    foreach ($itemInData as $key => $value) {
                        if($value->itemInID > $locationProductLastItemInID){
                            $newItemInIds[] = $value->itemInID;
                            $remainQty = $value->itemInQty - $value->itemInSoldQty;
                            if($remainQty > 0){
                                $itemInPrice = $remainQty*$value->itemInPrice;
                                $itemInDiscount = $remainQty*$value->itemInDiscount;
                                $locationPrTotalQty += $remainQty;
                                $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                            }
                            $locationProductLastItemInID = $value->itemInID;
                        }
                    }
                }
                $locationPrTotalQty += $locationProductQuantity;
                $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                if($locationPrTotalQty > 0){
                    $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                    $lpdata = array(
                        'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                        'locationProductLastItemInID' => $locationProductLastItemInID,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationPID);

                    foreach ($newItemInIds as $inID) {
                        $intemInData = array(
                            'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                            );
                        $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                    }
                }
            }
            //call product updated event
            $productIDs = array_map(function($element) {
                return $element['pID'];
            }, $products);

            $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationID]];
            $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            if($buyBackNoteDirectType == 'true' && $invoiceID != ''){
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                $invoiceEditLogModel = new InvoiceEditLog();
                $invoiceEditLogModel->exchangeArray([
                    'userID' => $this->userID,
                    'salesInvoiceID' => $invoiceID,
                    'dateAndTime' => $this->getGMTDateTime()
                ]);
                $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                $newItemsData = [
                    'invoiceEditLogDetailsOldState' => $invoice->statusID,
                    'invoiceEditLogDetailsNewState' => 'Invoice closed with adding a direct credit note '.$creditNoteCode,
                    'invoiceEditLogDetailsCategory' => 'salesInvoiceStatusClosed'
                ];

                //close invoice state
                if ($invCloseFlag) {
                    $state = '4'; //close state
                    $update = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceState($invoiceID, $state);

                    $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);
                    $newItemsData['invoiceEditLogID'] = $invoiceEditLastInsertID;
                    $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                    $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                } else {
                    $invoiceTotalAmount = $invoice->salesinvoiceTotalAmount;
                    $invoicePayedAmount = $invoice->salesInvoicePayedAmount;

                    $creditNoteTotalAmountForTheInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByInvoiceID($invoiceID)->current();

                    $creditNoteAmount = 0;
                    if (!is_null($creditNoteTotalAmountForTheInvoice['creditNoteTot'])) {
                        $creditNoteAmount = $creditNoteTotalAmountForTheInvoice['creditNoteTot'];
                    }

                    $totalSettledAmountForInvoice = $creditNoteAmount + $invoicePayedAmount;
                    if ($totalSettledAmountForInvoice >= $invoiceTotalAmount) {
                        $state = '4'; //close state
                        $update = $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoiceState($invoiceID, $state);

                        $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);
                        $newItemsData['invoiceEditLogID'] = $invoiceEditLastInsertID;
                        $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                        $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                    }

                }
            }

            $jeResult = $this->saveJournalEntryDataForBuyBackCredit($creditNoteID, $creditNoteCode, $customerID, $products, $allProductTotal, $date, $averageCostingFlag, $creditNoteCustomCurrencyRate, $dimensionData, $ignoreBudgetLimit);

            if($jeResult['status']){

                $this->setLogMessage($customCurrencySymbol . ' amount credit note Product details were successfully added - ' . $creditNoteCode);
                $this->flashMessenger()->addMessage($jeResult['msg']);
            }
         
            return ['status' => $jeResult['status'], 'msg' => $jeResult['msg'], 'data' => $jeResult['data']];
        } else {
            return ['status' => false, 'msg' => $this->getMessage('ERR_CREDIT_VALID_CUST')];
        }
    }
}

/////////////////// END OF CUSTOMER API CONTROLLER \\\\\\\\\\\\\\\\\\\\