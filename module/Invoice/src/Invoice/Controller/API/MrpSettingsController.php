<?php
namespace Invoice\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeValue;
use Settings\Form\ItemAttributeForm;
use Zend\View\Model\JsonModel;

class MrpSettingsController extends CoreController
{

    public function changeManageMrpSettingStatusAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }

        $state = $req->getPost('state');
        $status = 0;
        if ($state == 'true') {
            $status = 1;
        }
        $displaySetup = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();

        $displayData = [
            'useMrpSettings' => $status,
        ];
        $saveBusinessType = $this->CommonTable('DisplaySetupTable')->updateDisplaySetupDetails($displayData, 1);

        $this->status = true;
        $this->msg = $this->getMessage('SUCCESS_MRP_STATE');
        return $this->JSONRespond();
        
    }

    public function saveMrpSettingsForItemsAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }

        $data = $req->getPost();

        $selectedItems = $data['selectedProducts'];
        $locations = $data['locations'];
        $mrpType = $data['mrpType'];
        $mrpPerValue = $data['mrpPercentageVal'];
        $mrpValue = $data['mrpValue'];
        $bulk = ($data['isBulkItemWise'] == 'true') ? true : false;
        $single = ($data['isSingleItemWise'] == 'true') ? true : false;


        $this->beginTransaction();
        if ($bulk) {
            foreach ($selectedItems as $key => $value) {
                $datset = [
                    'mrpType' => $mrpType,
                    'mrpPercentageValue' => $mrpPerValue,
                    'mrpValue' => NULL
                ];
                $updateMrp = $this->CommonTable('LocationProductTable')->updateLocationProductMrpDetails($datset, $locations, $value);

                if ($updateMrp == NULL) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('SUCC_MRP_SETTING');
                    return $this->JSONRespond();
                }
            }
        } elseif ($single) {
            $mrpPer = ($mrpType == 1) ? $mrpPerValue : null;
            $mrpVal = ($mrpType == 2) ? $mrpValue : null;

            $datset = [
                'mrpType' => $mrpType,
                'mrpPercentageValue' => $mrpPer,
                'mrpValue' => $mrpVal
            ];


            $updateMrp = $this->CommonTable('LocationProductTable')->updateLocationProductMrpDetails($datset, $locations, $selectedItems);

            if ($updateMrp == NULL) {
                $this->rollback();
                $this->status = false;
                $this->msg = $this->getMessage('ERR_MRP_SETTING');
                return $this->JSONRespond();
            } else {
                $this->commit();
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_MRP_SETTING');
                return $this->JSONRespond();
            }   
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_MRP_SETTING');
        return $this->JSONRespond();
    }

    public function getCategoryWiseItemMrpDetailsAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $catID = $req->getPost('categoryID');

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $items = $this->CommonTable('Inventory\Model\ProductTable')->getProductListByCategoryIdAndLocationID($catID, $locationID);

        if (!$items) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR');
            return $this->JSONRespond();
        }

        $view = new ViewModel(array(
            'itemList' => $items,
            'paginated' => false
        ));
        
        $view->setTerminal(true);
        $this->status = true;
        $view->setTemplate('invoice/mrp-settings/mrp-item-list');
        $this->html = $view;
        return $this->JSONRespondHtml();
        
    }

    public function searchItemMrpDetailsByKeywordAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $products = $this->CommonTable('Inventory\Model\ProductTable')->getProductsforSearch($req->getPost('searchKey'), $locationID);

        if (!$products) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR');
            return $this->JSONRespond();
        }

        $view = new ViewModel(array(
            'itemList' => $products,
            'paginated' => false
        ));
        
        $view->setTerminal(true);
        $this->status = true;
        $view->setTemplate('invoice/mrp-settings/mrp-item-list');
        $this->html = $view;
        return $this->JSONRespondHtml();   
    }

    
}
