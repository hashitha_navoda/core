<?php

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Model\DispatchNote;
use Invoice\Model\DispatchNoteProduct;
use Invoice\Model\DispatchNoteSubProduct;

class DispatchNoteController extends CoreController
{
    public function createAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;

        $request = $this->getRequest();
        if (!$request->isPost())
        {
            return $this->JSONRespond();
        }

        $postData = $request->getPost()->toArray();

        if (!$postData['invoiceId'])
        {
            $this->msg = $this->getMessage('ERR_DISNOTE_INVOICE_NOT_EXIST');
            return $this->JSONRespond();
        }

        $isDispatchCodeExist = $this->CommonTable('Invoice\Model\DispatchNoteTable')->getDispatchNoteByDispatchNoteCode($postData['dispatchNoteCode']);
        if ($isDispatchCodeExist)
        {
            $this->msg = $this->getMessage('REQ_DISNOTE_REFRESH');
            return $this->JSONRespond();
        }

        try {
            $this->beginTransaction();
            $dispatchNote = new DispatchNote();
            $dispatchNote->exchangeArray($postData);
            $dispatchNote->entityId = $this->createEntity();
            //save dispatch note
            $dispatchNoteId = $this->CommonTable('Invoice\Model\DispatchNoteTable')->saveDispatchNote($dispatchNote);

            foreach ($postData['products'] as $invoiceProductId => $product) {
                $invoiceProduct = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceProductId($invoiceProductId);
                
                if(!($invoiceProduct['salesInvoiceProductQuantity'] >= $product['dispatchQuantity'])){
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_DISNOTE_PRODUCT_QTY');
                    return $this->JSONRespond();
                }

                if($product['dispatchQuantity'] == 0){
                    $this->rollback();
                    $this->msg = $this->getMessage('ERR_DISNOTE_PRODUCT_QTY_ZERO');
                    return $this->JSONRespond();
                }

                $dispatchNoteProduct = new DispatchNoteProduct();
                $dispatchNoteProduct->dispatchNoteId = $dispatchNoteId;
                $dispatchNoteProduct->salesInvoiceProductId = $invoiceProductId;
                $dispatchNoteProduct->dispatchNoteProductQuantity = $product['dispatchQuantity'];
                //for save invoice product table
                $dispatchNoteProductId = $this->CommonTable('Invoice\Model\DispatchNoteProductTable')->saveDispatchNoteProduct($dispatchNoteProduct);
                $subProducts = empty($postData['subProducts']) ? array() : $postData['subProducts'];
                //check sub product exist
                if(array_key_exists( $invoiceProductId, $subProducts)){
                    foreach ($subProducts[$invoiceProductId] as $subProduct){
                        $dispatchNoteSubProduct = new DispatchNoteSubProduct();
                        $dispatchNoteSubProduct->dispatchNoteProductId = $dispatchNoteProductId;
                        $dispatchNoteSubProduct->productSerialId = empty($subProduct['serialId']) ? null : $subProduct['serialId'];
                        $dispatchNoteSubProduct->productBatchId = empty($subProduct['batchId']) ? null : $subProduct['batchId'];
                        $dispatchNoteSubProduct->dispatchNoteSubProductQuantity = $subProduct['quantity'];

                        $this->CommonTable('Invoice\Model\DispatchNoteSubProductTable')->saveDispatchNoteSubProduct($dispatchNoteSubProduct);
                    }
                }
            }

            $locationId = $this->user_session->userActiveLocation['locationID'];
            $refData = $this->getReferenceNoForLocation(29, $locationId);
            $locationReferenceId = $refData["locRefID"];
            $this->updateReferenceNumber($locationReferenceId);
            $this->commit();

            $this->status = true;
            $this->msg    = $this->getMessage('SUCC_DISNOTE_SAVE');
            $this->data   = $dispatchNoteId;
            $this->JSONRespond();

        } catch (\Exception $ex) {
            $this->rollback();
            $this->msg    = $this->getMessage('ERR_DISNOTE_SAVE');
        }

        return $this->JSONRespond();
    }

    public function getInvoiceDetailsForDispatchNoteAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;

        $request = $this->getRequest();
        if ($request->isPost()){
            $invoiceId = $request->getPost('invoiceId');

            if($invoiceId){
                //get invoice metadata
                $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceMetadataByInvoiceId($invoiceId);

                if($invoiceDetails['customerStatus'] == 2){
                    $this->status = false;
                    $this->msg    = $this->getMessage('ERR_DISNOTE_CUSTOMER_INACTIVE');
                    return $this->JSONRespond();
                }
                $productArr = [];
                //get invoice producs
                $products = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceIdForDispatchNote($invoiceId);
                foreach ($products as $product){
                    //get product dispatched quentity
                    $dispatchedQty = $this->CommonTable('Invoice\Model\DispatchNoteTable')->getDispatchedQuantityOfInvoiceProduct( $invoiceId, $product['salesInvoiceProductID']);
                    $product = (array) $product;
                    $invoicedQty = $product['salesInvoiceProductQuantity'];
                    $remainingQty = $invoicedQty - $dispatchedQty;
                    $product['salesInvoiceProductQuantity'] = number_format( $remainingQty, $product['uomDecimalPlace'],'.','');
                    $productArr[] = $product;
                }

                $subProductsArr = [];
                //for serial product
                $subProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceSerialProductByInvoiceId($invoiceId);
                foreach ($subProducts as $product){
                    $isDispached = $this->CommonTable('Invoice\Model\DispatchNoteSubProductTable')->isSubProductDispatched( $product['salesInvoiceProductID'], $product['productSerialID'], 'serial');
                    if(!$isDispached){
                        $subProductsArr['serial'][$product['salesInvoiceProductID']][] = $product;
                    }
                }

                //for batch product
                $batchProducts = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceBatchProductByInvoiceId($invoiceId);
                foreach ($batchProducts as $product){
                    $dispatchedQty = $this->CommonTable('Invoice\Model\DispatchNoteSubProductTable')->getDispatchedQuantityOfSubProduct( $product['salesInvoiceProductID']);
                    $product = (array) $product;
                    $invoicedQty = $product['salesInvoiceSubProductQuantity'];
                    $remainingQty = $invoicedQty - $dispatchedQty;
                    $product['salesInvoiceSubProductQuantity'] = number_format( $remainingQty, $product['uomDecimalPlace'],'.','');
                    $subProductsArr['batch'][$product['salesInvoiceProductID']][] = $product;
                }

                $this->status = true;
                $this->msg    = $this->getMessage('SUCC_INV_PRODUCT_RECIEVED');
                $this->data   = array(
                    'productDetails' => $productArr,
                    'invoiceDetails' => $invoiceDetails,
                    'subProducts' => $subProductsArr
                );
            }
        }

        return $this->JSONRespond();
    }

    public function searchDispatchCodeForDropdownAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $key = $request->getPost('searchKey');
            $dispatchNotes = $this->CommonTable('Invoice\Model\DispatchNoteTable')->searchDispatchNoteCodeForDropDown($key);

            $noteList = array();
            foreach ($dispatchNotes as $dispatchNote) {
                $temp['value'] = $dispatchNote['dispatchNoteId'];
                $temp['text']  = $dispatchNote['dispatchNoteCode'];
                $noteList[] = $temp;
            }

            $this->data = array('list' => $noteList);
            return $this->JSONRespond();
        }
    }

    public function searchAction()
    {
        $page = $this->params('param1', 1);

        $request = $this->getRequest();
        if($request->isPost()){
            $postData = $request->getPost()->toArray();
            $paginated = false;

            if($postData['key'] == 'dispatch-note-code'){
                $dispatchNotes = $this->CommonTable('Invoice\Model\DispatchNoteTable')->searchDispatchNote( $postData['dispatchNoteId'], null, $postData['fromDate'], $postData['toDate']);
            } else if($postData['key'] == 'customer-name') {
                $dispatchNotes = $this->CommonTable('Invoice\Model\DispatchNoteTable')->searchDispatchNote( null, $postData['customerId'], $postData['fromDate'], $postData['toDate']);
            } else {
                $dispatchNotes = $this->CommonTable('Invoice\Model\DispatchNoteTable')->getDispatchNotes( true, array('dispatchNote.dispatchNoteCode DESC'));
                $dispatchNotes->setCurrentPageNumber($page);
                $dispatchNotes->setItemCountPerPage(10);
                $paginated = true;
            }

            $view = new ViewModel(array( 'dispatchNotes'=>$dispatchNotes, 'paginated'=>$paginated));
            $view->setTerminal(true);
            $view->setTemplate('invoice/dispatch-note/dispatch-note-list');
            return $view;
        }
    }

    public function deleteAction()
    {
        $this->status = false;
        $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
        $this->data   = null;

        $request = $this->getRequest();
        if ($request->isPost()) {
            $dispatchNoteId = $request->getPost('dispatchNoteId');

            $dispatchNote = $this->CommonTable('Invoice\Model\DispatchNoteTable')->getDispatchNoteByDispatchNoteId($dispatchNoteId);
            if($dispatchNote->count() > 0){
                $this->updateDeleteInfoEntity($dispatchNote->current()['entityId']);
                $this->status = true;
                $this->msg    = $this->getMessage('SUCC_DISNOTE_DELETE');
            } else {
                $this->msg = $this->getMessage('ERR_DISNOTE_NOT_EXIST');
            }
        }
        return $this->JSONRespond();
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Dispatch Note';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_DISNOTE_EMAIL_SENT');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_DISNOTE_EMAIL_SENT');
        return $this->JSONRespond();
    }

}