<?php

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Invoice\Model\Quotation;
use Invoice\Model\TaxAmount;
use Invoice\Model\QuotationProductTax;
use Invoice\Model\QuotationProduct;
use Invoice\Form\QuotationForm;
use Invoice\Form\AddCustomerForm;
use Invoice\Model\Reference;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class CustomerOrderController extends CoreController
{
    /**
     * Save quotation details to database and return saved quotation
     *
     * @return json
     */
    public function createCustomerOrderAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            return $this->returnJsonError('ERR_QUOTA_CREATE');
        }

        $postData = $request->getPost();

        $this->beginTransaction();

        $res = $this->getService('CustomerOrderService')
                ->createCustomerOrder($postData);

        if (!$res['status']) {
            $this->rollback();
            return $this->returnJsonError($res['msg']);
        } else {
            $this->setLogMessage('Customer order created successfully - ' . $res['data']['customerOrderCode']);
            $this->commit();

            $referenceService = $this->getService('ReferenceService');
            // document type 1 = quotation
            $reference = $referenceService->getReferenceNumber(47, $this->user_session->userActiveLocation["locationID"]);
            $refId = $reference['data']['referenceNo'];
            $locRefId = $reference['data']['locationReferenceID'];


            return $this->returnJsonSuccess($res['data']['customerOrderID'], [$res['msg'], array($refId)]);
        }

    }

    
    public function updateCustomerOrderAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            return $this->returnJsonError('ERR_QUOTA_CREATE');
        }

        $postData = $request->getPost();

        $this->beginTransaction();

        $res = $this->getService('CustomerOrderService')
                ->updateCustomerOrder($postData);

        if (!$res['status']) {
            $this->rollback();
            return $this->returnJsonError($res['msg']);
        } else {
            $this->setLogMessage('Customer order updated successfully - ' . $res['data']['customerOrderCode']);
            $this->commit();

            
            return $this->returnJsonSuccess($res['data']['customerOrderID'], $res['msg']);
        }

    }

    public function deleteCustomerOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerOrderID = $request->getPost('id');
            // $salesOrderQID = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByQuotationID($quotationID);

            //check customerOrder link with any Sales Order
            $refCustomerOrderData = $this->CommonTable('Core\Model\DocumentReferenceTable')->getDocumentReferenceBySourceDocIDAndRefDocDetails(3,$customerOrderID, 46);

            if (sizeof($refCustomerOrderData) > 0) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUS_ODR_CANNOT_DELETE');
            } else {
                $this->beginTransaction();
                $qData = $this->CommonTable('Invoice\Model\CustomerOrderTable')->retriveCustomerOrder($customerOrderID)->current();
                $deleted = $this->updateDeleteInfoEntity($qData['entityID']);
                if ($deleted == 1) {
                    //update quotationStatus
                    $closeStatusID = $this->getStatusID('cancelled');
                    $this->CommonTable('Invoice\Model\CustomerOrderTable')->updateCusOrderStatusByID($customerOrderID, $closeStatusID);
                    $this->commit();
                    $this->setLogMessage('Customer Order '.$qData['customerOrderCode'].' is deleted');
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_CUS_ODR_DELETE');
                    $this->flashMessenger()->addMessage($this->msg);
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUS_ODR_DELETE');
                }
            }
        }
        return $this->JSONRespond();
    }

    public function getCustomerOrderByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        if ($invrequest->isPost()) {
            $fromdate = $this->convertDateToStandardFormat($invrequest->getPost('fromdate')); //converting date to database format y-m-d
            $todate = $this->convertDateToStandardFormat($invrequest->getPost('todate')); //converting date to database format y-m-d

            $this->getPaginatedCusOrderBydate($fromdate, $todate);

            $dateFormat = $this->getUserDateFormat(); //getting user selected date format to paas it to the view
            
            $isPaginated = true;
            $view = new ViewModel(array(
                'customerOrderList' => $this->paginator,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat,
                'isPaginated'=> $isPaginated
            ));

            

            $view->setTerminal(true);
            $view->setTemplate('invoice/customer-order/customer-order-edit-list');

            $this->status = true;
            $this->html = $view;
            $this->msg = $msg;
            return $this->JSONRespondHtml();
        }
    }

    private function getPaginatedCusOrderBydate($fromdate, $todate)
    {
        $location = $this->user_session->userActiveLocation["locationID"];
        $this->paginator = $this->CommonTable('Invoice\Model\CustomerOrderTable')->getPaginatedCusOrderBydate($fromdate, $todate, true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function getAllRelatedDocumentDetailsByCustomerOrderIDAction()
    {
        $request = $this->getRequest();
        $customerOrderID = $request->getPost('customerOrderID');

        $refCustomerOrderData = $this->CommonTable('Core\Model\DocumentReferenceTable')->getDocumentReferenceBySourceDocIDAndRefDocDetails(3,$customerOrderID, 46);

        $locationID = $this->user_session->userActiveLocation['locationID'];

            $dataSet = [];
        if (sizeof($refCustomerOrderData) > 0) {
            foreach ($refCustomerOrderData as $key => $value) {
                $salesOrderID = $value;
                $salesOrderDetails = [];

                $salesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderDetailsBySalesOrderId($salesOrderID);
                $quotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getSalesOrderRelatedQuotationDataBySOId($salesOrderID);
                $deliveryNoteData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getSalesOrderRelatedDeliveryNoteDataByQuotationId(null, $salesOrderID);
                $salesReturnData = $this->CommonTable('SalesReturnsTable')->getDeliveryNoteRelatedReturnsDataByQuotationId(null, $salesOrderID);
                $dlnRelatedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getDeliveryNoteRelatedSalesInvoiceDataByQuotationId(null, $salesOrderID);
                $soRelatedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesOrderRelatedSalesInvoiceDataByQuotationId(null, $salesOrderID);
                $dlnRelatedPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getDeliveryNoteRelatedInvoicePaymentsDataByQuotationId(null, $salesOrderID);
                $soRelatedPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getSalesOrderRelatedInvoicePaymentsDataByQuotationId(null, $salesOrderID);
                $dlnRelatedCreditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDeliveryNoteRelatedCreditNoteDataByQuotationId(null, $salesOrderID);
                $soRelatedCreditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getSalesOrderRelatedCreditNoteDataByQuotationId(null, $salesOrderID);
                $dlnRelatedCreditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getDeliveryNoteRelatedCreditNotePaymentDataByQuotationId(null, $salesOrderID);
                $soRelatedCreditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getSalesOrderRelatedCreditNotePaymentDataByQuotationId(null, $salesOrderID);
                

                $dataExistsFlag = false;
                if ($salesOrderData) {
                    $salesOrderData = array(
                        'type' => 'SalesOrder',
                        'documentID' => $salesOrderData['soID'],
                        'code' => $salesOrderData['soCode'],
                        'amount' => number_format($salesOrderData['totalAmount'], 2),
                        'issuedDate' => $salesOrderData['issuedDate'],
                        'created' => $salesOrderData['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $salesOrderData;
                    $dataExistsFlag = true;
                }

                if (isset($quotaionData)) {
                    foreach ($quotaionData as $qtnDta) {
                        $qtnDeta = array(
                            'type' => 'Quotation',
                            'documentID' => $qtnDta['quotationID'],
                            'code' => $qtnDta['quotationCode'],
                            'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                            'issuedDate' => $qtnDta['quotationIssuedDate'],
                            'created' => $qtnDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $qtnDeta;
                        if (isset($qtnDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($deliveryNoteData)) {
                    foreach ($deliveryNoteData as $dlnDta) {
                        $dlnData = array(
                            'type' => 'DeliveryNote',
                            'documentID' => $dlnDta['deliveryNoteID'],
                            'code' => $dlnDta['deliveryNoteCode'],
                            'amount' => number_format($dlnDta['deliveryNotePriceTotal'], 2),
                            'issuedDate' => $dlnDta['deliveryNoteDeliveryDate'],
                            'created' => $dlnDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $dlnData;
                        if (isset($dlnDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                
                if (isset($salesReturnData)) {
                    foreach ($salesReturnData as $rtnDta) {
                        $srData = array(
                            'type' => 'SalesReturn',
                            'documentID' => $rtnDta['salesReturnID'],
                            'code' => $rtnDta['salesReturnCode'],
                            'amount' => number_format($rtnDta['salesReturnTotal'], 2),
                            'issuedDate' => $rtnDta['salesReturnDate'],
                            'created' => $rtnDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $srData;
                        if (isset($rtnDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                
                if (isset($dlnRelatedInvoiceData)) {
                    foreach ($dlnRelatedInvoiceData as $invDta) {
                        $invData = array(
                            'type' => 'SalesInvoice',
                            'documentID' => $invDta['salesInvoiceID'],
                            'code' => $invDta['salesInvoiceCode'],
                            'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                            'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                            'created' => $invDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $invData;
                        if (isset($invDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($soRelatedInvoiceData)) {
                    foreach ($soRelatedInvoiceData as $invDta) {
                        $invData = array(
                            'type' => 'SalesInvoice',
                            'documentID' => $invDta['salesInvoiceID'],
                            'code' => $invDta['salesInvoiceCode'],
                            'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                            'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                            'created' => $invDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $invData;
                        if (isset($invDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                
                if (isset($dlnRelatedPaymentData)) {
                    foreach ($dlnRelatedPaymentData as $payDta) {
                        $paymentData = array(
                            'type' => 'CustomerPayment',
                            'documentID' => $payDta['incomingPaymentID'],
                            'code' => $payDta['incomingPaymentCode'],
                            'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                            'issuedDate' => $payDta['incomingPaymentDate'],
                            'created' => $payDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $paymentData;
                        if (isset($payDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                
                if (isset($soRelatedPaymentData)) {
                    foreach ($soRelatedPaymentData as $payDta) {
                        $paymentData = array(
                            'type' => 'CustomerPayment',
                            'documentID' => $payDta['incomingPaymentID'],
                            'code' => $payDta['incomingPaymentCode'],
                            'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                            'issuedDate' => $payDta['incomingPaymentDate'],
                            'created' => $payDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $paymentData;
                        if (isset($payDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($dlnRelatedCreditNoteData)) {
                    foreach ($dlnRelatedCreditNoteData as $cNDta) {
                        $creditNoteData = array(
                            'type' => 'CreditNote',
                            'documentID' => $cNDta['creditNoteID'],
                            'code' => $cNDta['creditNoteCode'],
                            'amount' => number_format($cNDta['creditNoteTotal'], 2),
                            'issuedDate' => $cNDta['creditNoteDate'],
                            'created' => $cNDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $creditNoteData;
                        if (isset($cNDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                
                if (isset($soRelatedCreditNoteData)) {
                    foreach ($soRelatedCreditNoteData as $cNDta) {
                        $creditNoteData = array(
                            'type' => 'CreditNote',
                            'documentID' => $cNDta['creditNoteID'],
                            'code' => $cNDta['creditNoteCode'],
                            'amount' => number_format($cNDta['creditNoteTotal'], 2),
                            'issuedDate' => $cNDta['creditNoteDate'],
                            'created' => $cNDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $creditNoteData;
                        if (isset($cNDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($dlnRelatedCreditNotePaymentData)) {
                    foreach ($dlnRelatedCreditNotePaymentData as $cNPDta) {
                        $creditNotePaymentData = array(
                            'type' => 'CreditNotePayment',
                            'documentID' => $cNPDta['creditNotePaymentID'],
                            'code' => $cNPDta['creditNotePaymentCode'],
                            'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                            'issuedDate' => $cNPDta['creditNotePaymentDate'],
                            'created' => $cNPDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $creditNotePaymentData;
                        if (isset($cNPDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                
                if (isset($soRelatedCreditNotePaymentData)) {
                    foreach ($soRelatedCreditNotePaymentData as $cNPDta) {
                        $creditNotePaymentData = array(
                            'type' => 'CreditNotePayment',
                            'documentID' => $cNPDta['creditNotePaymentID'],
                            'code' => $cNPDta['creditNotePaymentCode'],
                            'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                            'issuedDate' => $cNPDta['creditNotePaymentDate'],
                            'created' => $cNPDta['createdTimeStamp'],
                        );
                        $salesOrderDetails[] = $creditNotePaymentData;
                        if (isset($cNPDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                $sortData = Array();
                foreach ($salesOrderDetails as $key => $r) {
                    $sortData[$key] = strtotime($r['created']);
                }
                array_multisort($sortData, SORT_ASC, $salesOrderDetails);

                $dataSet = array_merge($dataSet,$salesOrderDetails);
                
            }
        }
        
        $documentDetails = array(
            'customerOrderDetails' => $dataSet
        );


        if (sizeof($dataSet) > 0) {
            $this->data = $documentDetails;
            $this->status = true;
        } else {
            $this->status = false;
        }

        return $this->JSONRespond();

    }

    
    public function searchAllCustomerOrderForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            $cusOrderList = $this->getService('CustomerOrderService')->getCustomerOrdersForDropdown($searchKey);
            $this->data = array('list' => $cusOrderList);

            $this->setLogMessage("Retrive customer order list for dropdown.");
            return $this->JSONRespond();
        }
    }

    public function searchAllCustomerOrderByRefNoForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            $cusOrderList = $this->getService('CustomerOrderService')->searchAllCustomerOrderByRefNoForDropdown($locationID, $searchKey);
            $this->data = array('list' => $cusOrderList);

            $this->setLogMessage("Retrive customer order list for dropdown.");
            return $this->JSONRespond();
        }
    }

    public function getCustomerOrderListFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $dateFormat = $this->getUserDateFormat();
            $customerOrderID = $searchrequest->getPost('customerOrderID');
            $location = $this->user_session->userActiveLocation["locationID"];
            $globaldata = $this->getServiceLocator()->get('config');
            $statuses = $globaldata['statuses'];

            if  (!empty($customerOrderID))  {

                $custmerOrders = $this->CommonTable('CustomerOrderTable')->getCustomerOrderforSearch($customerOrderID, $location);
                $isPaginated = false;
                $view = new ViewModel(array(
                    'customerOrderList' => $custmerOrders,
                    'statuses' => $this->getStatusesList(),
                    'userdateFormat' => $dateFormat,
                    'isPaginated'=> $isPaginated
                ));

            } else {
                $custmerOrders = $this->getPaginatedCustomerOrders();
                $isPaginated = true;
                $view = new ViewModel(array(
                    'customerOrderList' => $this->paginator,
                    'statuses' => $this->getStatusesList(),
                    'userdateFormat' => $dateFormat,
                    'isPaginated'=> $isPaginated
                ));

            }

            $view->setTerminal(true);
            $view->setTemplate('invoice/customer-order/customer-order-edit-list');

            $this->status = true;
            $this->html = $view;
            $this->msg = $msg;
            return $this->JSONRespondHtml();
        }
    }

    public function getCustomerWiseCustomerOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cust_id = trim($request->getPost('customerID'));
            $location = $this->user_session->userActiveLocation["locationID"];


            $globaldata = $this->getServiceLocator()->get('config');
            $statuses = $globaldata['statuses'];

            if  (!empty($cust_id))  {

                $custmerOrders = $this->CommonTable('CustomerOrderTable')->getCustomerOrderByCustomerforSearch($cust_id, $location);
                $isPaginated = false;
                $view = new ViewModel(array(
                    'customerOrderList' => $custmerOrders,
                    'statuses' => $this->getStatusesList(),
                    'userdateFormat' => $dateFormat,
                    'isPaginated'=> $isPaginated
                ));

            } 

            $view->setTerminal(true);
            $view->setTemplate('invoice/customer-order/customer-order-edit-list');

            $this->status = true;
            $this->html = $view;
            $this->msg = $msg;
            return $this->JSONRespondHtml();
            
        }
        return false;
    }


    private function getPaginatedCustomerOrders()
    {
        $location = $this->user_session->userActiveLocation["locationID"];
        $this->paginator = $this->CommonTable('Invoice\Model\CustomerOrderTable')->fetchAll($location, $paginated = true);
        $this->paginator->setCurrentPageNumber($this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(8);
    }

    public function retriveCustomerOrderAction()
    {
        $request = $this->getRequest();
        $cusOrder = [];
        if ($request->isPost()) {
            $in = array();
            $customerOrderID = $request->getPost('customerOrderID');

            $cusOrder = $this->getService('CustomerOrderService')->retriveCustomerOrder($customerOrderID);
        }

        if (!$cusOrder['status']) {
            $this->status = $cusOrder['status'];
            $this->msg = $cusOrder['msg'];
        } else {
            $this->data = $cusOrder['data'];
            $this->status = $cusOrder['status'];
            $this->setLogMessage("Retrive customer order successfully.");
        }

        return $this->JSONRespond();
    }
}

/////////////////// END OF IMVOICE CONTROLLER \\\\\\\\\\\\\\\\\\\\
