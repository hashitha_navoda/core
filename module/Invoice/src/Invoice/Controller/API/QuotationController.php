<?php

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Invoice\Model\Quotation;
use Invoice\Model\TaxAmount;
use Invoice\Model\QuotationProductTax;
use Invoice\Model\QuotationProduct;
use Invoice\Form\QuotationForm;
use Invoice\Form\AddCustomerForm;
use Invoice\Model\Reference;
use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class QuotationController extends CoreController
{
    /**
     * Save quotation details to database and return saved quotation
     *
     * @return json
     */
    public function createQuotationAction()
    {
        $request = $this->getRequest();

        if (!$request->isPost()) {
            return $this->returnJsonError('ERR_QUOTA_CREATE');
        }

        $postData = $request->getPost();

        $quotationCode = $postData['qot_no'];
        $quotationCodeChangeInEdit = false;
        if (!is_null($postData['editedQuotationID'])) {
            if ($postData['newQuotationNumber'] == "false") {
                $updateQuotationStaus = $this->CommonTable('Invoice\Model\QuotationTable')->updateQuotationStatusByID($postData['editedQuotationID'],10);

                $oldQuotationDetails = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByID($postData['editedQuotationID']);
                $quotationCode = $oldQuotationDetails->quotationCode;
                $quotationCodeChangeInEdit = true;
            }
        }

        // Prepare POST data to be passed to the service
        $quotationData = array(
            'quotationCustomerName' => $postData['cust_name'],
            'customerID' => $postData['cust_id'],
            'quotationPayementTerm' => $postData['payment_term'],
            'quotationIssuedDate' => $postData['issue_date'],
            'quotationExpireDate' => $postData['expire_date'],
            'quotationCode' => $quotationCode,
            'quotationSalesPersonID' => $postData['salesPersonID'],
            'quotationComment' => $postData['cmnt'],
            'quotationAdditionalDetail1' => $postData['additionalDetail1'],
            'quotationAdditionalDetail2' => $postData['additionalDetail2'],
            'quotationTaxAmount' => $postData['taxAmount'],
            'quotationTotalAmount' => $postData['total_amount'],
            'quotationTotalDiscount' => $postData['total_discount'],
            'products' =>  [],
            'productTaxes' => [],
            'quotationShowTax' => $postData['show_tax'],
            'jobRefType' => $postData['jobRefType'],
            'quotationTotalDiscountType' => $postData['quotationTotalDiscountType'],
            'quotationDiscountRate' => $postData['quotationDiscountRate'],
            'jobRefTypeID' => $postData['jobRefTypeID'],
            'priceListId' => $postData['priceListId'],
            'customerProfileID' => $postData['cust_pro_id'],
            'customCurrencyId' => $postData['customCurrencyId'],
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'customCurrencyRate' => $postData['customCurrencyRate'],
            'userID' => $this->userID,
            'quotationCodeChangeInEdit' => $quotationCodeChangeInEdit,
            'locationID' => $this->user_session->userActiveLocation["locationID"],
        );

        //set productTaxes
        foreach (json_decode($postData['itemTaxes']) as $key => $value) {
            $quotationData['productTaxes'][$key] = (array) json_decode($value);
        }

        foreach ($postData['items'] as $value) {
            $quotationData['products'][] = [
                'quotationProductCode' => $value['item_code'],
                'quotationProductName' => $value['item'],
                'quotationProductDescription' => $value['item_discription'],
                'quotationProductQuantity' => $value['quantity'],
                'quotationProductDiscount' => $value['discount'],
                'quotationProductDiscountType' => $value['discountType'],
                'quotationProductUnitPrice' => $value['unit_price'],
                'uom' => $value['uom'],
                'quotationProductTotal' => $value['total_cost'],
                'productType' => $value['productType'],
                'taxes' => $value['taxes'],
                'taxDetails' => $value['taxDetails'],
            ];
        }

        $this->beginTransaction();

        $res = $this->getService('QuotationService')
                ->createQuotation($quotationData);

        if (!$res['status']) {
            $this->rollback();
            return $this->returnJsonError($res['msg']);
        } else {
            $this->setLogMessage($customCurrencySymbol . $postData['total_amount'] . ' amount quotation created successfully - ' . $quotationCode);
            $this->commit();
            return $this->returnJsonSuccess($res['data']['quotationID'], [$res['msg'], array($res['data']['quotationCode'])]);
        }

    }

    public function getQuotationAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\QuotationTable')->fetchAll();
        while ($row = $re->current()) {
            $a[] = $row;
        }
        return new JsonModel($a);
    }

    public function getQuotationidfromInvoiceSalesOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $quotation = $request->getPost('quotationID');
            $sales_order = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByQuotationID($quotation);
            $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByQuotationID($quotation);
            if ($sales_order != null) {
                return new JsonModel(array('flag' => 1));
            } elseif ($invoice != null) {
                return new JsonModel(array('flag' => 1));
            } else {
                return new JsonModel(array('flag' => 0));
            }
        }
    }

    public function getQuotationByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        if ($invrequest->isPost()) {
            $fromdate = $this->convertDateToStandardFormat($invrequest->getPost('fromdate')); //converting date to database format y-m-d
            $todate = $this->convertDateToStandardFormat($invrequest->getPost('todate')); //converting date to database format y-m-d

            $this->getPaginatedQuotationBydate($fromdate, $todate);
            $sales_order = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll();
            while ($sa = $sales_order->current()) {
                if ($sa->quotationCode != '') {
                    $salesorder[$sa->quotationCode] = $sa->quotationCode;
                }
            }
            $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->fetchAll();
            while ($sa = $invoice->current()) {
                if ($sa->quotationCode != '') {
                    $inv[$sa->quotationCode] = $sa->quotationCode;
                }
            }

            $dateFormat = $this->getUserDateFormat(); //getting user selected date format to paas it to the view
            $paymenteview = new ViewModel(array(
                'quotation' => $this->paginator,
                'salesOrder' => $salesorder,
                'invoice' => $inv,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat
                    )
            );


            $paymenteview->setTerminal(true);
            $paymenteview->setTemplate('invoice/quotation-api/quotation-edit-list');
            return $paymenteview;
        }
    }

    private function getPaginatedQuotationBydate($fromdate, $todate)
    {
        $location = $this->user_session->userActiveLocation["locationID"];
        $this->paginator = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotationsByDate($fromdate, $todate, $location, false);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function getCustomerAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        while ($row = $re->current()) {
            $a[] = $row;
        }
        return new JsonModel($a);
    }

    public function retriveQuotationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $in = array();
            $quotation = $request->getPost('quataionID');
            $forInvoice = ($request->getPost('forInvoice') != '') ? TRUE : FALSE;
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $quotationCheck = $this->CommonTable('Invoice\Model\QuotationTable')->checkQuotationByID($quotation, $locationID)->current();
            $value = (object) $quotationCheck;
            if (!$forInvoice) {
                $pvalue = $this->CommonTable('Invoice\Model\QuotationProductTable')->getQuotationProductsByID($value->quotationID);
            } else {
                $pvalue = $this->CommonTable('Invoice\Model\QuotationProductTable')->getUncopiedQuotationProductsByID($value->quotationID);
            }
            $cusProfDetails = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerProfileID($value->customerProfileID)->current();

            if($cusProfDetails['customerStatus'] == 2){
                $in[0] = 'inactiveCustomer';
                return new JsonModel($in);
            }
            if ($quotationCheck != NULL) {

                $ccbalance = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($value->customerID);
                $curBal = 0;
                if ($ccbalance != NULL) {
                    $curBal = $ccbalance->customerCurrentBalance;
                    $curCre = $ccbalance->customerCurrentCredit;
                } else {
                    $curBal = 0;
                    $curCre = 0;
                }
                //get customCurrency data by customer CurrencyId
                $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($value->customCurrencyId);
                $customCurrencySymbol = ($value->customCurrencyId == "" || $value->customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->currencySymbol;
                $data = array();
                $pdata = array();
                $data['customCurrencyId'] = $value->customCurrencyId;
                $data['customCurrencySymbol'] = $customCurrencySymbol;
                $data['quotationCustomCurrencyRate'] = $value->quotationCustomCurrencyRate;
                $data['quotation_no'] = $value->quotationCode;
                $data['customer_name'] = $value->quotationCustomerName;
                $data['customer_id'] = $value->customerID;
                $data['customer_prof_id'] = $value->customerProfileID;
                $data['customer_prof_name'] = $cusProfDetails['customerProfileName'];
                $data['salesPersonID'] = $value->salesPersonID;
                $data['issue_date'] = $value->quotationIssuedDate;
                $data['expire_date'] = $value->quotationExpireDate;
                $data['payment_term'] = $value->quotationPayementTerm;
                $data['tax'] = $value->quotationTaxAmount;
                $data['total_discount'] = $value->quotationTotalDiscount;
                $data['total'] = $value->quotationTotalAmount;
                $data['comment'] = $value->quotationComment;
                $data['quotationAdditionalDetail1'] = $value->quotationAdditionalDetail1;
                $data['quotationAdditionalDetail2'] = $value->quotationAdditionalDetail2;
                $data['quotationTotalDiscountType'] = $value->quotationTotalDiscountType;
                $data['quotationWiseTotalDiscount'] = $value->quotationWiseTotalDiscount;
                $data['quotationDiscountRate'] = $value->quotationDiscountRate;
                $data['branch'] = $value->quotationBranch;
                $data['showTax'] = $value->quotationShowTax;
                $data['salesPersonID'] = $value->salesPersonID;
                $data['priceListId'] = $value->priceListId;
                $data['current_balance'] = $curBal;
                $data['current_credit'] = $curCre;
                while ($t = $pvalue->current()) {
                    $pdata[] = $t;
                    $ptax = $this->CommonTable('Invoice\Model\QuotationProductTaxTable')->getQuotationProductTax($t['quotationProductID']);
                    $Taxes = '';
                    while ($taxs = $ptax->current()) {
                        $tax = (object) $taxs;
                        $Taxes[$tax->quotTaxID] = $tax;
                    }
                    if ($Taxes) {
                        $productTax[$t['quotationProductID']] = $Taxes;
                    }
                    $locationProducts[$t['productID']] = $this->getLocationProductDetails($locationID, $t['productID']);
                }
                $customer = $this->getCustomerDetailsByCustomerID($value->customerID);
                $data['product'] = $pdata;

                $locProducts = Array();
                $inactiveItems = Array();
                foreach ($pdata as $pro) {
                    $locProducts[$pro['productID']] = $this->getLocationProductDetails($pro['locationID'], $pro['productID']);
                    if ($locProducts[$pro['productID']] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($pro['productID']);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }
                }
                
                $uploadedAttachments = [];
                $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($quotation, 2);
                $companyName = $this->getSubdomain();
                $path = '/userfiles/' . md5($companyName) . "/attachments";

                foreach ($attachmentData as $value) {
                    $temp = [];
                    $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
                    $temp['documentRealName'] = $value['documentRealName'];
                    $temp['docLink'] = $path . "/" .$value['documentName'];
                    $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
                }

                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_QTN_ITM_INACTIVE', [$errorItems]);
                }
                $data['errorMsg'] = $errorMsg;
                $data['uploadedAttachments'] = $uploadedAttachments;
                $data['inactiveItemFlag'] = $inactiveItemFlag;
                $data['productTaxes'] = $productTax;
                $data['locationProducts'] = $locationProducts;
                $data['customer'] = $customer;
                return new JsonModel($data);
            } else {
                $in[0] = 'false';
                return new JsonModel($in);
            }
        }
        $in[0] = 'wrongquono';
        return new JsonModel($in);
    }

    public function retriveQuotationForViewAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $quotation = $request->getPost('quono');
            $value = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByCode($quotation);
            if ($value != NULL) {
                $in = array();
                $pvalue = $this->CommonTable('Invoice\Model\QuotationProductTable')->getProductByQuotationID($quotation);
                $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($value->customerID);
                $curBal = 0;
                if ($cust != NULL) {
                    $curBal = $cust->currentBalance;
                    $cusAdd = $cust->address;
                    $custp = $cust->telephoneNumber;
                    $cusmail = $cust->email;
                } else {
                    $curBal = 0;
                    $cusAdd = NULL;
                    $custp = NULL;
                    $cusmail = NULL;
                }
                $companyres = $this->CommonTable('Invoice\Model\CompanyTable')->fetchAll();
                $com = NULL;
                if ($companyres != NULL) {
                    $com = $companyres->current();
                }
                $payterm = $this->CommonTable('Core\Model\PaymentTermTable')->getPaymentterm($value->payementTerm);
                if ($payterm != NULL) {
                    $paytermname = $payterm->term;
                } else {
                    $paytermname = 'Not specified';
                }
                $data = array();
                $pdata = array();
                $data['customerName'] = $value->quotationCustomerName;
                $data['customerID'] = $value->customerID;
                $data['customerAddress'] = ($cusAdd != NULL) ? $cusAdd : NULL;
                $data['customertelephone'] = ($custp != NULL) ? $custp : NULL;
                $data['customerEmail'] = ($cusmail != NULL) ? $cusmail : NULL;
                $data['companyName'] = ($com != NULL) ? $com->companyName : NULL;
                $data['companyAddress'] = ($com != NULL) ? $com->companyAddress : NULL;
                $data['companyTelephone'] = ($com != NULL) ? $com->telephoneNumber : NULL;
                $data['companyEmail'] = ($com != NULL) ? $com->email : NULL;
                $data['companyReg'] = ($com != NULL) ? $com->trNumber : NULL;
                $data['issuedDate'] = $value->quotationIssuedDate;
                $data['expireDate'] = $value->quotationExpire_date;
                $data['deliveryCharge'] = $value->deliveryCharge;
                $data['deliveryAddress'] = $value->deliveryAddress;
                $data['payementTerm'] = $paytermname;
                $data['taxAmount'] = $value->quotationTaxAmount;
                $data['totalDiscount'] = $value->quotationTotalDiscount;
                $data['total'] = $value->quotationTotalAmount;
                $data['comment'] = $value->quotationComment;
                $data['quotationAdditionalDetail1'] = $value->quotationAdditionalDetail1;
                $data['quotationAdditionalDetail2'] = $value->quotationAdditionalDetail2;
                $data['quotationTotalDiscountType'] = $value->quotationTotalDiscountType;
                $data['quotationWiseTotalDiscount'] = $value->quotationWiseTotalDiscount;
                $data['quotationDiscountRate'] = $value->quotationDiscountRate;
                $data['quotationID'] = $value->quotation_no;
                $data['currentBalance'] = $curBal;
                $data['cdnUrl'] = $this->cdnUrl;
                while ($t = $pvalue->current()) {
                    $pdata[] = $t;
                }
                $data['product'] = $pdata;
                return new JsonModel($data);
            }
            $in[0] = 'wrongquono';
            return new JsonModel($in);
        }
    }

    public function retriveCustomerQuotationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $cust_id = trim($request->getPost('customerID'));
            $location = $this->user_session->userActiveLocation["locationID"];
            $quotationsBycustomer = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByCustomerID($cust_id, $location);
            $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
            $terms = array();
            while ($row = $paymentTerms->current()) {
                $id = $row->paymentTermID;
                if ($id == NULL) {
                    $id = 0;
                }
                $terms[$id] = $row->paymentTermName;
            }
            $quot_form = new QuotationForm(array(
                'name' => 'quatation',
                'id' => 'qot_no',
                'terms' => $terms));
            $sales_order = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll($location);
            while ($sa = $sales_order->current()) {
                if ($sa->soCode != '') {
                    $salesorder[$sa->soCode] = $sa->soCode;
                }
            }
            $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->fetchAll(false, '', '');
            while ($sa = $invoice->current()) {
                if ($sa->salesInvoiceCode != '') {
                    $inv[$sa->salesInvoiceCode] = $sa->salesInvoiceCode;
                }
            }

            // get quotationIDs of copyied product
            $copiedQuotations = $this->CommonTable('Invoice\Model\QuotationTable')->getCopiedQuotationProductDetails($location);
            foreach ($copiedQuotations as $copiedQuotation) {
                if ($copiedQuotation['quotationProductCopied'] == '1') {
                    $copiedQuoationList[$copiedQuotation['quotationID']] = $copiedQuotation['quotationID'];
                }
            }

            $dateFormat = $this->getUserDateFormat();
            $quotation = new ViewModel(array(
                'quot_form' => $quot_form,
                'quotation' => $quotationsBycustomer,
                'salesOrder' => $salesorder,
                'invoice' => $inv,
                'statuses' => $this->getStatusesList(),
                'cur' => $this->companyCurrencySymbol,
                'userdateFormat' => $dateFormat,
                'copiedQuotations' => $copiedQuoationList,
            ));
            $cust_form = new AddCustomerForm();
            $cust_form->setAttribute('id', 'add_customer_form');
            $cust_form->setAttribute('name', 'add_customer_form');
            $add_cust = new ViewModel(array(
                'form' => $cust_form,
                'action' => 'add',
            ));
            $add_cust->setTemplate('/invoice/customer-api/get-customer-edit-box');
            $quotation->addChild($add_cust, 'cust_formv');
            $delet_quo = new viewModel(array(
            ));
            $delet_quo->setTemplate('/invoice/quotation-api/confirm-delete-quotation');
            $quotation->addChild($delet_quo, 'delete_quot');
            $quotation->setTerminal(true);
            $quotation->setTemplate('invoice/quotation-api/quotation-edit-list');
            return $quotation;
        }
        return false;
    }

    public function deleteQuotationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $quotationID = $request->getPost('quotationID');
            $salesOrderQID = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByQuotationID($quotationID);
            if ($salesOrderQID) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_QUOTA_CANNOT_DELETE');
            } else {
                $entitID = $this->CommonTable('Invoice\Model\QuotationTable')->getEntityID($quotationID);
                $qData = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByID($quotationID);
                $deleted = $this->updateDeleteInfoEntity($entitID);
                if ($deleted == 1) {
                    //update quotationStatus
                    $closeStatusID = $this->getStatusID('cancelled');
                    $this->CommonTable('Invoice\Model\QuotationTable')->updateQuotationStatusByID($quotationID, $closeStatusID);

                    $this->setLogMessage('Quotation '.$qData->quotationCode.' is deleted');
                    $this->status = true;
                    $this->msg = $this->getMessage('SUCC_QUOTA_DELETE');
                    $this->flashMessenger()->addMessage($this->msg);
                } else {
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_QUOTA_DELETE');
                }
            }
        }
        return $this->JSONRespond();
    }

    public function editQuotationAction()
    {
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->id;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->term;
        }
        $quot_form = new InvoiceForm(array(
            'name' => 'quatation',
            'id' => 'qot_no',
            'terms' => $terms
        ));
        $quot_receipt_form = new InvoiceForm(array(
            'name' => 'quatation',
            'id' => 'qot_no',
            'terms' => $terms
        ));
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $quotation = new Quotation();
            $quot_form->setInputFilter($quotation->getInputFilter());
            $quot_form->setData($post);
            $valid_items = TRUE;
            $q_receipt = new QuotationProduct();
            $quot_receipt_form->setInputFilter($q_receipt->getInputFilter());
            foreach ($post['items'] as $item) {
                $quot_receipt_form->setData($item);
                if (!$quot_form->isValid()) {
                    $valid_items = FALSE;
                }
            }
            $quotationExchange = array(
                'id' => $request->getPost('qot_no'),
                'customerName' => $request->getPost('cust_name'),
                'customerID' => $request->getPost('cust_id'),
                'currentBalance' => $request->getPost('currentBalance'),
                'issuedDate' => $request->getPost('issue_date'),
                'expireDate' => $request->getPost('expire_date'),
                'payementTerm' => $request->getPost('payment_term'),
                'taxAmount' => '',
                'totalDiscount' => $request->getPost('total_discount'),
                'totalAmount' => $request->getPost('total_amount'),
                'comment' => $request->getPost('cmnt'),
                'branch' => ''
            );
            $quotation->exchangeArray($quotationExchange);
            $retQ = $this->CommonTable('Invoice\Model\QuotationTable')->updateQuotation($quotation);
            if ($post['deleteItems'] != null) {
                foreach ($post['deleteItems'] as $row) {
                    $sendrow = array(
                        'productID' => $row['item_code'],
                        'productName' => $row['item'],
                        'quantity' => $row['quantity'],
                        'unitPrice' => $row['unit_price'],
                        'uom' => $row['uom'],
                        'discount' => $row['discount'],
                        'quotationID' => $request->getPost('qot_no'),
                        'total' => $row['total_cost']
                    );
                    $value = $this->CommonTable('Invoice\Model\QuotationProductTable')->deleteReceipt($request->getPost('qot_no'), $row['item_code']);
                }
            }
            if ($post['items'] != null) {
                foreach ($post['items'] as $row) {
                    $sendrow = array(
                        'productID' => $row['item_code'],
                        'productName' => $row['item'],
                        'quantity' => $row['quantity'],
                        'unitPrice' => $row['unit_price'],
                        'uom' => $row['uom'],
                        'discount' => $row['discount'],
                        'quotationID' => $request->getPost('qot_no'),
                        'total' => $row['total_cost']
                    );
                    $checkcproduct = $this->CommonTable('Invoice\Model\QuotationProductTable')->getProductByQuotationIDAndProductId($request->getPost('qot_no'), $row['item_code']);
                    $q_receipt->exchangeArray($sendrow);
                    if ($checkcproduct->locationProductID == $row['item_code']) {
                        $this->CommonTable('Invoice\Model\QuotationProductTable')->editReceipt($q_receipt);
                    } else {
                        $this->CommonTable('Invoice\Model\QuotationProductTable')->saveReceipt($q_receipt);
                    }
                }
            }
            $this->setLogMessage("Updated");
            return new JsonModel(array("result" => true));
        }
        return new JsonModel(array("result" => false));
    }

    public function sendQuotationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($post["cust_id"]);
            if (!empty($cust->email) || TRUE) {
                $to = $post["to_email"];
                $from = "ezbiztc@gmail.com";
                $fromName = "ezBiz";
                $subject = $post["subject"];
                $file = null;
                $Bcc = null;
                $Cc = null;
                $textContent = null;
                $htmlMarkup = $post["body"];

                $msg = $this->sendEmail($to, $subject, $htmlMarkup, $file, $fromName);
                $error = FALSE;
            } else {
                $error = true;
                $msg = $this->getMessage('ERR_INVOICE_NO_EMAIL');
            }
        } else {
            $error = "true";
            $msg = $this->getMessage('ERR_INVOICE_UNKNOWN');
        }
        return new JsonModel(array(
            "error" => $error,
            "msg" => $msg
        ));
    }

    public function getQuotationsByDatefilterAction()
    {
        $quorequest = $this->getRequest();
        if ($quorequest->isPost()) {
            $fromdate = $this->convertDateToStandardFormat($quorequest->getPost('fromdate'));
            $todate = $this->convertDateToStandardFormat($quorequest->getPost('todate'));
            $customerID = trim($quorequest->getPost('customerID'));
            $location = $this->user_session->userActiveLocation["locationID"];
            $filteredQuotations = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotationsByDate($fromdate, $todate, $location, $customerID);

            $sales_order = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll($location, false);
            $salesorder = array();
            foreach ($sales_order as $sa) {
                if ($sa['quotationID'] != '') {
                    $salesorder[$sa['quotationID']] = $sa['quotationID'];
                }
            }
            // get quotationIDs of copyied product
            $copiedQuotations = $this->CommonTable('Invoice\Model\QuotationTable')->getCopiedQuotationProductDetails($location);
            foreach ($copiedQuotations as $copiedQuotation) {
                if ($copiedQuotation['quotationProductCopied'] == '1') {
                    $copiedQuoationList[$copiedQuotation['quotationID']] = $copiedQuotation['quotationID'];
                }
            }
            $dateFormat = $this->getUserDateFormat();
            $quotation = new ViewModel(array(
                'quotation' => $filteredQuotations,
                'salesOrder' => $salesorder,
                'copiedQuotations' => $copiedQuoationList,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat,
                'cur' => $this->companyCurrencySymbol,
                    )
            );
            $quotation->setTerminal(TRUE);
            $quotation->setTemplate('invoice/quotation-api/quotation-edit-list');
            return $quotation;
        }
    }

    public function getQuotationsFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $dateFormat = $this->getUserDateFormat();
            $QID = $searchrequest->getPost('quotationID');
            $location = $this->user_session->userActiveLocation["locationID"];
            $quotations = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotationforSearch($QID, $location);

            if ($quotations->count() == 0) {
                return new JsonModel(array(FALSE));
            } else {
                $sales_order = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll($location, false);
                $salesorder = array();
                foreach ($sales_order as $sa) {
                    if ($sa['quotationID'] != '') {
                        $salesorder[$sa['quotationID']] = $sa['quotationID'];
                    }
                }
                // get quotationIDs of copyied product
                $copiedQuotations = $this->CommonTable('Invoice\Model\QuotationTable')->getCopiedQuotationProductDetails($location);
                foreach ($copiedQuotations as $copiedQuotation) {
                    if ($copiedQuotation['quotationProductCopied'] == '1') {
                        $copiedQuoationList[$copiedQuotation['quotationID']] = $copiedQuotation['quotationID'];
                    }
                }

                $globaldata = $this->getServiceLocator()->get('config');
                $statuses = $globaldata['statuses'];

                $quotation = new ViewModel(array(
                    'quotation' => $quotations,
                    'salesOrder' => $salesorder,
                    'copiedQuotations' => $copiedQuoationList,
                    'statuses' => $this->getStatusesList(),
                    'userdateFormat' => $dateFormat,
                    'cur' => $this->companyCurrencySymbol,
                        )
                );
                $quotation->setTerminal(TRUE);
                $quotation->setTemplate('invoice/quotation-api/quotation-edit-list');
                return $quotation;
            }
        }
    }

    public function getUomDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $uomAbb = $request->getPost('uom');
            $uomData = $this->CommonTable('Invoice\Model\UnitMeasureTable')->getUnitMeasureByAbbrevation($uomAbb);
            if ($uomAbb) {
                return new JsonModel(array('decimalPlaces' => $uomData->decimalPlace));
            } else {
                return new JsonModel(NULL);
            }
        }
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Sales Quotation';
        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_QUOTA_EMAIL_SENT');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_QUOTA_EMAIL_SENT');
        return $this->JSONRespond();
    }

    /**
     * Search open quotations for dropdown
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchOpenQuotationForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            $openQuotationList = $this->_getQuotationForDropdown($locationID, $searchKey, 3);
            $this->data = array('list' => $openQuotationList);
            return $this->JSONRespond();
        }
    }

    /**
     * Search All quotations for dropdown
     * @return type
     */
    public function searchAllQuotationForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $searchrequest->getPost('locationID');
            $openQuotationList = $this->_getQuotationForDropdown($locationID, $searchKey);
            $this->data = array('list' => $openQuotationList);

            $this->setLogMessage("Retrive quotation list for dropdown.");
            return $this->JSONRespond();
        }
    }

    /**
     * Get Quotation For Dropdown
     * @param type $locationID
     * @param type $searchKey
     * @param type $status
     * @return type
     */
    private function _getQuotationForDropdown($locationID, $searchKey, $status = false)
    {
        $quotaions = $this->CommonTable('Invoice\Model\QuotationTable')->searchQuotationForDropDown($locationID, $searchKey, $status);
        foreach ($quotaions as $quotaion) {
            $temp['value'] = $quotaion['quotationID'];
            $temp['text'] = $quotaion['quotationCode'];
            $quotaionList[] = $temp;
        }
        return ($quotaionList) ? $quotaionList : array();
    }

    public function getAllRelatedDocumentDetailsByQuotationIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $quotationID = $request->getPost('quotationID');
            $quotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotationDetailsByQuotationId($quotationID);
            $salesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getQuotationRelatedSalesOrderDataByQuotationId($quotationID);
            $deliveryNoteData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getSalesOrderRelatedDeliveryNoteDataByQuotationId($quotationID);
            $salesReturnData = $this->CommonTable('SalesReturnsTable')->getDeliveryNoteRelatedReturnsDataByQuotationId($quotationID);
            $dlnRelatedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getDeliveryNoteRelatedSalesInvoiceDataByQuotationId($quotationID);
            $soRelatedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesOrderRelatedSalesInvoiceDataByQuotationId($quotationID);
            $qtnRelatedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getQuotationRelatedSalesInvoiceDataByQuotationId($quotationID);
            $dlnRelatedPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getDeliveryNoteRelatedInvoicePaymentsDataByQuotationId($quotationID);
            $soRelatedPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getSalesOrderRelatedInvoicePaymentsDataByQuotationId($quotationID);
            $qtnRelatedPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getQuotationRelatedInvoicePaymentsDataByQuotationId($quotationID);
            $dlnRelatedCreditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDeliveryNoteRelatedCreditNoteDataByQuotationId($quotationID);
            $soRelatedCreditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getSalesOrderRelatedCreditNoteDataByQuotationId($quotationID);
            $qtnRelatedCreditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getQuotationRelatedCreditNoteDataByQuotationId($quotationID);
            $dlnRelatedCreditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getDeliveryNoteRelatedCreditNotePaymentDataByQuotationId($quotationID);
            $soRelatedCreditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getSalesOrderRelatedCreditNotePaymentDataByQuotationId($quotationID);
            $qtnRelatedCreditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getQuotationRelatedCreditNotePaymentDataByQuotationId($quotationID);
            

            $dataExistsFlag = false;
            if ($quotaionData) {
                $quotaionData = array(
                    'type' => 'Quotation',
                    'documentID' => $quotaionData['quotationID'],
                    'code' => $quotaionData['quotationCode'],
                    'amount' => number_format($quotaionData['quotationTotalAmount'], 2),
                    'issuedDate' => $quotaionData['quotationIssuedDate'],
                    'created' => $quotaionData['createdTimeStamp'],
                );
                $quotationDetails[] = $quotaionData;
                $dataExistsFlag = true;
            }
            if (isset($salesOrderData)) {
                foreach ($salesOrderData as $soDta) {
                    $soDeta = array(
                        'type' => 'SalesOrder',
                        'documentID' => $soDta['soID'],
                        'code' => $soDta['soCode'],
                        'amount' => number_format($soDta['totalAmount'], 2),
                        'issuedDate' => $soDta['issuedDate'],
                        'created' => $soDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $soDeta;
                    if (isset($soDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($deliveryNoteData)) {
                foreach ($deliveryNoteData as $dlnDta) {
                    $dlnData = array(
                        'type' => 'DeliveryNote',
                        'documentID' => $dlnDta['deliveryNoteID'],
                        'code' => $dlnDta['deliveryNoteCode'],
                        'amount' => number_format($dlnDta['deliveryNotePriceTotal'], 2),
                        'issuedDate' => $dlnDta['deliveryNoteDeliveryDate'],
                        'created' => $dlnDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $dlnData;
                    if (isset($dlnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($salesReturnData)) {
                foreach ($salesReturnData as $rtnDta) {
                    $srData = array(
                        'type' => 'SalesReturn',
                        'documentID' => $rtnDta['salesReturnID'],
                        'code' => $rtnDta['salesReturnCode'],
                        'amount' => number_format($rtnDta['salesReturnTotal'], 2),
                        'issuedDate' => $rtnDta['salesReturnDate'],
                        'created' => $rtnDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $srData;
                    if (isset($rtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($dlnRelatedInvoiceData)) {
                foreach ($dlnRelatedInvoiceData as $invDta) {
                    $invData = array(
                        'type' => 'SalesInvoice',
                        'documentID' => $invDta['salesInvoiceID'],
                        'code' => $invDta['salesInvoiceCode'],
                        'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                        'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                        'created' => $invDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $invData;
                    if (isset($invDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($soRelatedInvoiceData)) {
                foreach ($soRelatedInvoiceData as $invDta) {
                    $invData = array(
                        'type' => 'SalesInvoice',
                        'documentID' => $invDta['salesInvoiceID'],
                        'code' => $invDta['salesInvoiceCode'],
                        'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                        'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                        'created' => $invDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $invData;
                    if (isset($invDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($qtnRelatedInvoiceData)) {
                foreach ($qtnRelatedInvoiceData as $invDta) {
                    $invData = array(
                        'type' => 'SalesInvoice',
                        'documentID' => $invDta['salesInvoiceID'],
                        'code' => $invDta['salesInvoiceCode'],
                        'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                        'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                        'created' => $invDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $invData;
                    if (isset($invDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($dlnRelatedPaymentData)) {
                foreach ($dlnRelatedPaymentData as $payDta) {
                    $paymentData = array(
                        'type' => 'CustomerPayment',
                        'documentID' => $payDta['incomingPaymentID'],
                        'code' => $payDta['incomingPaymentCode'],
                        'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                        'issuedDate' => $payDta['incomingPaymentDate'],
                        'created' => $payDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $paymentData;
                    if (isset($payDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($soRelatedPaymentData)) {
                foreach ($soRelatedPaymentData as $payDta) {
                    $paymentData = array(
                        'type' => 'CustomerPayment',
                        'documentID' => $payDta['incomingPaymentID'],
                        'code' => $payDta['incomingPaymentCode'],
                        'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                        'issuedDate' => $payDta['incomingPaymentDate'],
                        'created' => $payDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $paymentData;
                    if (isset($payDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($qtnRelatedPaymentData)) {
                foreach ($qtnRelatedPaymentData as $payDta) {
                    $paymentData = array(
                        'type' => 'CustomerPayment',
                        'documentID' => $payDta['incomingPaymentID'],
                        'code' => $payDta['incomingPaymentCode'],
                        'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                        'issuedDate' => $payDta['incomingPaymentDate'],
                        'created' => $payDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $paymentData;
                    if (isset($payDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedCreditNoteData)) {
                foreach ($dlnRelatedCreditNoteData as $cNDta) {
                    $creditNoteData = array(
                        'type' => 'CreditNote',
                        'documentID' => $cNDta['creditNoteID'],
                        'code' => $cNDta['creditNoteCode'],
                        'amount' => number_format($cNDta['creditNoteTotal'], 2),
                        'issuedDate' => $cNDta['creditNoteDate'],
                        'created' => $cNDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $creditNoteData;
                    if (isset($cNDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($soRelatedCreditNoteData)) {
                foreach ($soRelatedCreditNoteData as $cNDta) {
                    $creditNoteData = array(
                        'type' => 'CreditNote',
                        'documentID' => $cNDta['creditNoteID'],
                        'code' => $cNDta['creditNoteCode'],
                        'amount' => number_format($cNDta['creditNoteTotal'], 2),
                        'issuedDate' => $cNDta['creditNoteDate'],
                        'created' => $cNDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $creditNoteData;
                    if (isset($cNDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($qtnRelatedCreditNoteData)) {
                foreach ($qtnRelatedCreditNoteData as $cNDta) {
                    $creditNoteData = array(
                        'type' => 'CreditNote',
                        'documentID' => $cNDta['creditNoteID'],
                        'code' => $cNDta['creditNoteCode'],
                        'amount' => number_format($cNDta['creditNoteTotal'], 2),
                        'issuedDate' => $cNDta['creditNoteDate'],
                        'created' => $cNDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $creditNoteData;
                    if (isset($cNDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($dlnRelatedCreditNotePaymentData)) {
                foreach ($dlnRelatedCreditNotePaymentData as $cNPDta) {
                    $creditNotePaymentData = array(
                        'type' => 'CreditNotePayment',
                        'documentID' => $cNPDta['creditNotePaymentID'],
                        'code' => $cNPDta['creditNotePaymentCode'],
                        'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                        'issuedDate' => $cNPDta['creditNotePaymentDate'],
                        'created' => $cNPDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $creditNotePaymentData;
                    if (isset($cNPDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($soRelatedCreditNotePaymentData)) {
                foreach ($soRelatedCreditNotePaymentData as $cNPDta) {
                    $creditNotePaymentData = array(
                        'type' => 'CreditNotePayment',
                        'documentID' => $cNPDta['creditNotePaymentID'],
                        'code' => $cNPDta['creditNotePaymentCode'],
                        'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                        'issuedDate' => $cNPDta['creditNotePaymentDate'],
                        'created' => $cNPDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $creditNotePaymentData;
                    if (isset($cNPDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($qtnRelatedCreditNotePaymentData)) {
                foreach ($qtnRelatedCreditNotePaymentData as $cNPDta) {
                    $creditNotePaymentData = array(
                        'type' => 'CreditNotePayment',
                        'documentID' => $cNPDta['creditNotePaymentID'],
                        'code' => $cNPDta['creditNotePaymentCode'],
                        'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                        'issuedDate' => $cNPDta['creditNotePaymentDate'],
                        'created' => $cNPDta['createdTimeStamp'],
                    );
                    $quotationDetails[] = $creditNotePaymentData;
                    if (isset($cNPDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($quotationDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $quotationDetails);

            $documentDetails = array(
                'quotationDetails' => $quotationDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

}

/////////////////// END OF IMVOICE CONTROLLER \\\\\\\\\\\\\\\\\\\\
