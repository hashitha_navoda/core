<?php

/**
 * Description of Rating Types
 *
 * @author Ashan Madushka <ashan@thinkcube.com>
 */

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Form\RatingTypesForm;
use Invoice\Model\RatingTypes;

class RatingTypesController extends CoreController
{

    protected $sideMenus = 'crm_side_menu';
    protected $upperMenus = 'customer_upper_menu';
    protected $sidemenu;
    protected $uppermenu;

    //save rating types data
    public function saveRatingTypesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $ratingTypesCode = $post['ratingTypesCode'];
            $ratingTypesName = $post['ratingTypesName'];
            $ratingTypesDescription = $post['ratingTypesDescription'];
            $ratingTypesMethod = $post['ratingTypesMethod'];
            $ratingTypesMethodSize = $post['ratingTypesMethodSize'];
            $ratingTypesMethodDefinition = $post['ratingTypesMethodDefinition'];
            $ratingTypesState = $post['ratingTypesState'];

//check whether the rating types code alredy exist. if exist return error message otherwise save the rating types.
            $ratingTypesExist = (object) $this->CommonTable('Invoice\Model\RatingTypesTable')->getRatingTypesByRatingTypesCode($ratingTypesCode);
            if (!isset($ratingTypesExist->ratingTypesCode)) {

                $this->beginTransaction();
                $data = array(
                    'ratingTypesCode' => $ratingTypesCode,
                    'ratingTypesName' => $ratingTypesName,
                    'ratingTypesDescription' => $ratingTypesDescription,
                    'ratingTypesMethod' => $ratingTypesMethod,
                    'ratingTypesSize' => $ratingTypesMethodSize,
                    'ratingTypesMethodDefinition' => $ratingTypesMethodDefinition,
                    'ratingTypesState' => $ratingTypesState,
                    'entityID' => $this->createEntity(),
                );

                $ratingTypes = new RatingTypes();
                $ratingTypes->exchangeArray($data);
                $ratingTypesId = $this->CommonTable('Invoice\Model\RatingTypesTable')->saveRatingTypes($ratingTypes);
                $this->commit();
                $this->status = TRUE;
                $this->msg = $this->getMessage("SUCC_SAVED_RATING_TYPES_");
                $this->getPaginatedRatingTypes(6);
                $ratingTypeListView = new ViewModel(array(
                    'ratingTypesList' => $this->paginator,
                    'isPaginated' => true,
                ));
                $ratingTypeListView->setTerminal(true);
                $ratingTypeListView->setTemplate('invoice/rating-types/rating-types-list');
                $this->html = $ratingTypeListView;
                return $this->JSONRespondHtml();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage("ERR_RATING_TYPES_CODE_ALREDY_EXIST");
                return $this->JSONRespond();
            }
        }
    }

    private function getPaginatedRatingTypes($perPage = 6)
    {
        $this->paginator = $this->CommonTable('Invoice\Model\RatingTypesTable')->fetchAll(true);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage($perPage);
    }

    //update Rating Types data
    public function updateRatingTypesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $ratingTypesId = $post['ratingTypesId'];
            $ratingTypesCode = $post['ratingTypesCode'];
            $ratingTypesName = $post['ratingTypesName'];
            $ratingTypesDescription = $post['ratingTypesDescription'];
            $ratingTypesMethod = $post['ratingTypesMethod'];
            $ratingTypesMethodSize = $post['ratingTypesMethodSize'];
            $ratingTypesMethodDefinition = $post['ratingTypesMethodDefinition'];
            $ratingTypesState = $post['ratingTypesState'];

            $this->beginTransaction();
            $data = array(
                'ratingTypesCode' => $ratingTypesCode,
                'ratingTypesName' => $ratingTypesName,
                'ratingTypesDescription' => $ratingTypesDescription,
                'ratingTypesMethod' => $ratingTypesMethod,
                'ratingTypesSize' => $ratingTypesMethodSize,
                'ratingTypesMethodDefinition' => $ratingTypesMethodDefinition,
                'ratingTypesState' => $ratingTypesState,
            );

            $status = $this->CommonTable('Invoice\Model\RatingTypesTable')->updateRatingTypes($data, $ratingTypesId);
            if ($status) {
                $this->commit();
                $this->status = TRUE;
                $this->msg = $this->getMessage("SUCC_UPDATE_RATING_TYPES_");
                $this->getPaginatedRatingTypes(6);
                $ratingTypeListView = new ViewModel(array(
                    'ratingTypesList' => $this->paginator,
                    'isPaginated' => true,
                ));
                $ratingTypeListView->setTerminal(true);
                $ratingTypeListView->setTemplate('invoice/rating-types/rating-types-list');
                $this->html = $ratingTypeListView;
                return $this->JSONRespondHtml();
            } else {
                $this->rollback();
                $this->status = FALSE;
                $this->msg = $this->getMessage("ERR_UPDATE_RATING_TYPES_");
            }
        }
    }

    //update state of ratingTypes
    public function updateStateOfRatingTypesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $ratingTypesId = $post['ratingTypesId'];
            $ratingTypesState = $post['ratingTypesState'];
            $this->beginTransaction();
            $data = array(
                'ratingTypesState' => $ratingTypesState,
            );
            $status = $this->CommonTable('Invoice\Model\RatingTypesTable')->updateRatingTypes($data, $ratingTypesId);
            if ($status) {
                $this->commit();
                $this->status = TRUE;
                $this->msg = $this->getMessage("SUCC_CHANGE_RATING_TYPES_STATE");
            } else {
                $this->rollback();
                $this->status = FALSE;
                $this->msg = $this->getMessage("ERR_CHANGE_RATING_TYPES_STATE");
            }
            return $this->JSONRespond();
        }
    }

    //delete ratingTypes by id
    public function deleteRatingTypesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $ratingTypesId = $post['ratingTypesId'];

            $ratingTypes = (object) $this->CommonTable('Invoice\Model\RatingTypesTable')->getRatingTypesByRatingTypesId($ratingTypesId);
            $entityID = $ratingTypes->entityID;
            $this->updateDeleteInfoEntity($entityID);

            $this->status = TRUE;
            $this->msg = $this->getMessage("SUCC_DELETED_RATING_TYPES");
            $this->getPaginatedRatingTypes(6);
            $ratingTypeListView = new ViewModel(array(
                'ratingTypesList' => $this->paginator,
                'isPaginated' => true,
            ));
            $ratingTypeListView->setTerminal(true);
            $ratingTypeListView->setTemplate('invoice/rating-types/rating-types-list');
            $this->html = $ratingTypeListView;
            return $this->JSONRespondHtml();
        }
    }

    public function searchRatingTypesBySearchKeyAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            if ($request->getPost('searchKey') === '') {
                $this->getPaginatedRatingTypes();
                $ratingTypeListView = new ViewModel(array(
                    'ratingTypesList' => $this->paginator,
                    'isPaginated' => TRUE,
                ));
            } else {
                $ratingTypes = $this->CommonTable('Invoice\Model\RatingTypesTable')->searchRatingTypesBySearchKey($request->getPost('searchKey'));
                $ratingTypeListView = new ViewModel(array(
                    'ratingTypesList' => $ratingTypes,
                    'isPaginated' => FALSE,
                ));
            }
            $ratingTypeListView->setTerminal(true);
            $ratingTypeListView->setTemplate('invoice/rating-types/rating-types-list');
            $this->status = TRUE;
            $this->html = $ratingTypeListView;
        }
        return $this->JSONRespondHtml();
    }

}
