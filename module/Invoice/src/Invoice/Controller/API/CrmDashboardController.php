<?php

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;

class CrmDashboardController extends CoreController
{
	protected $userID;
    protected $cdnUrl;
    protected $user_session;
    protected $paginator;
    protected $company;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function updateFooterChartDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $oldCustomerAquisitionData = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerVisitPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID, false, false)->current();
            $newCustomerAquisitionData = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerVisitPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID, true, false)->current();

            $salsByPromotion = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesByPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID);
            $salesPromoData = [];
            $salesPromoTotal = 0;
            foreach ($salsByPromotion as $value) {
                $salesPromoData[] = $value;
                $salesPromoTotal += floatval($value['invTotal']);
            }

            if (!empty($salesPromoData)) {
                $invTotal = array_column($salesPromoData, 'invTotal');
                array_multisort($invTotal, SORT_DESC, $salesPromoData);
            }

            header('Content-Type: application/json');
            $widgetData = array(
                'salesPromoData' => $salesPromoData,
                'salesPromoTotal' => $salesPromoTotal,
                'oldCustomerAquisitionData' => $oldCustomerAquisitionData,
                'newCustomerAquisitionData' => $newCustomerAquisitionData,
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function updateChartDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $promotionWiseVisitDataOfOldCustomer = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerVisitPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID, false);


            $oldCustomerWisePromoVisit = [];
            foreach ($promotionWiseVisitDataOfOldCustomer as $value) {
                $oldCustomerWisePromoVisit[$value['promotionID']]['promotionName'] = $value['promotionName'];
                $oldCustomerWisePromoVisit[$value['promotionID']]['oldCustomerVisits'] = $value['invCount'];
            }

            $promotionWiseVisitDataOfNewCustomer = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerVisitPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID, true);


            $newCustomerWisePromoVisit = [];
            foreach ($promotionWiseVisitDataOfNewCustomer as $value) {
                $newCustomerWisePromoVisit[$value['promotionID']]['promotionName'] = $value['promotionName'];
                $newCustomerWisePromoVisit[$value['promotionID']]['newCustomerVisits'] = $value['invCount'];
            }

            $customerVisitByPromotionData = [];
            if (!empty($oldCustomerWisePromoVisit)) {
                foreach ($oldCustomerWisePromoVisit as $key => $value) {
                    $customerVisitByPromotionData[$key] = $value;
                    $customerVisitByPromotionData[$key]['newCustomerVisits'] = $newCustomerWisePromoVisit[$key]['newCustomerVisits'];
                }
            } else if (!empty($newCustomerWisePromoVisit)) {
                foreach ($newCustomerWisePromoVisit as $key => $value) {
                    $customerVisitByPromotionData[$key] = $value;
                    $customerVisitByPromotionData[$key]['oldCustomerVisits'] = $oldCustomerWisePromoVisit[$key]['oldCustomerVisits'];
                }
            }


            header('Content-Type: application/json');
            $widgetData = array(
                'customerVisitByPromotionData' => $customerVisitByPromotionData,
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function getCustomerVisitDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerCountForDashboard()->current();

            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalCustomerVisitByDateRangeAndLocationID($fromDate,$toDate,$locationID);
            $totalCustomerVisits = 0;
            $mostCustomerVisits = [];
            foreach ($invoiceData as $value) {
                $totalCustomerVisits += $value['invCount'];         
                $mostCustomerVisits[] = $value;
            }

            if (!empty($mostCustomerVisits)) {
                $invCount = array_column($mostCustomerVisits, 'invCount');
                array_multisort($invCount, SORT_DESC, $mostCustomerVisits);
            }

            $lastPeriodInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalCustomerVisitByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);
            $LastPeriodTotalCustomerVisits = 0;
            foreach ($lastPeriodInvoiceData as $value) {
                $LastPeriodTotalCustomerVisits += $value['invCount'];         
            }

            $customerVisitProfitLoss = (($totalCustomerVisits - $LastPeriodTotalCustomerVisits) / $totalCustomerVisits) * 100;

            header('Content-Type: application/json');
            $widgetData = array(
                'totalCustomer' => $customerData['num'],
                'totalCustomerVisits' => $totalCustomerVisits,
                'mostCustomerVisits' => $mostCustomerVisits,
                'lastPeriodTotalCustomerVisits' => $LastPeriodTotalCustomerVisits,
                'customerVisitProfitLoss' => round($customerVisitProfitLoss,2),
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }

    public function getLoyalityPointsDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }
            
            $loyaltyData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalLoyaltyByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $lastPeriodLoyaltyData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalLoyaltyByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $loyaltyEarnedProfitLoss = (($loyaltyData['totalCollectedPoints'] - $lastPeriodLoyaltyData['totalCollectedPoints']) / $loyaltyData['totalCollectedPoints']) * 100;
            $loyaltyRedeemProfitLoss = (($loyaltyData['totalRedeemedPoints'] - $lastPeriodLoyaltyData['totalRedeemedPoints']) / $loyaltyData['totalRedeemedPoints']) * 100;

           

            header('Content-Type: application/json');
            $widgetData = array(
                'loyaltyEarnedProfitLoss' => round($loyaltyEarnedProfitLoss,2),
                'loyaltyRedeemProfitLoss' => round($loyaltyRedeemProfitLoss,2),
                'totalCollectedPoints' => round($loyaltyData['totalCollectedPoints'],2),
                'lastPeriodtotalCollectedPoints' => round($lastPeriodLoyaltyData['totalCollectedPoints'],2),
                'totalRedeemedPoints' => round($loyaltyData['totalRedeemedPoints'],2),
                'lastPeriodtotalRedeemedPoints' => round($lastPeriodLoyaltyData['totalRedeemedPoints'],2),
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }




	public function getWidgetDataAction()
	{
		$request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                	$lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                 	$lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                 	$lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();

            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalCustomerVisitByDateRangeAndLocationID($fromDate,$toDate,$locationID);
            $totalCustomerVisits = 0;
            $mostCustomerVisits = [];
            foreach ($invoiceData as $value) {
                $totalCustomerVisits += $value['invCount'];         
                $mostCustomerVisits[] = $value;
            }

            if (!empty($mostCustomerVisits)) {
                $invCount = array_column($mostCustomerVisits, 'invCount');
                array_multisort($invCount, SORT_DESC, $mostCustomerVisits);
            }

            $lastPeriodInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalCustomerVisitByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);
            $LastPeriodTotalCustomerVisits = 0;
            foreach ($lastPeriodInvoiceData as $value) {
                $LastPeriodTotalCustomerVisits += $value['invCount'];         
            }

            $customerVisitProfitLoss = (($totalCustomerVisits - $LastPeriodTotalCustomerVisits) / $totalCustomerVisits) * 100;
            
            $loyaltyData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalLoyaltyByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $lastPeriodLoyaltyData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalLoyaltyByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $loyaltyEarnedProfitLoss = (($loyaltyData['totalCollectedPoints'] - $lastPeriodLoyaltyData['totalCollectedPoints']) / $loyaltyData['totalCollectedPoints']) * 100;
            $loyaltyRedeemProfitLoss = (($loyaltyData['totalRedeemedPoints'] - $lastPeriodLoyaltyData['totalRedeemedPoints']) / $loyaltyData['totalRedeemedPoints']) * 100;

            $customerSales = $this->getCustomerWiseSalesReportData([$locationID],$fromDate,$toDate);


            $promotionWiseVisitDataOfOldCustomer = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerVisitPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID, false);


            $oldCustomerWisePromoVisit = [];
            foreach ($promotionWiseVisitDataOfOldCustomer as $value) {
                $oldCustomerWisePromoVisit[$value['promotionID']]['promotionName'] = $value['promotionName'];
                $oldCustomerWisePromoVisit[$value['promotionID']]['oldCustomerVisits'] = $value['invCount'];
            }

            $promotionWiseVisitDataOfNewCustomer = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerVisitPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID, true);


            $newCustomerWisePromoVisit = [];
            foreach ($promotionWiseVisitDataOfNewCustomer as $value) {
                $newCustomerWisePromoVisit[$value['promotionID']]['promotionName'] = $value['promotionName'];
                $newCustomerWisePromoVisit[$value['promotionID']]['newCustomerVisits'] = $value['invCount'];
            }

            $customerVisitByPromotionData = [];
            if (!empty($oldCustomerWisePromoVisit)) {
                foreach ($oldCustomerWisePromoVisit as $key => $value) {
                    $customerVisitByPromotionData[$key] = $value;
                    $customerVisitByPromotionData[$key]['newCustomerVisits'] = $newCustomerWisePromoVisit[$key]['newCustomerVisits'];
                }
            } else if (!empty($newCustomerWisePromoVisit)) {
                foreach ($newCustomerWisePromoVisit as $key => $value) {
                    $customerVisitByPromotionData[$key] = $value;
                    $customerVisitByPromotionData[$key]['oldCustomerVisits'] = $oldCustomerWisePromoVisit[$key]['oldCustomerVisits'];
                }
            }

            $oldCustomerAquisitionData = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerVisitPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID, false, false)->current();
            $newCustomerAquisitionData = $this->CommonTable('Invoice\Model\InvoiceTable')->getCustomerVisitPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID, true, false)->current();

            $salsByPromotion = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesByPromotionByDateRangeAndLocationID($fromDate,$toDate,$locationID);
            $salesPromoData = [];
            $salesPromoTotal = 0;
            foreach ($salsByPromotion as $value) {
                $salesPromoData[] = $value;
                $salesPromoTotal += floatval($value['invTotal']);
            }

            if (!empty($salesPromoData)) {
                $invTotal = array_column($salesPromoData, 'invTotal');
                array_multisort($invTotal, SORT_DESC, $salesPromoData);
            }

            header('Content-Type: application/json');
            $widgetData = array(
                'totalCustomer' => count($customerData),
                'totalCustomerVisits' => $totalCustomerVisits,
                'mostCustomerVisits' => $mostCustomerVisits,
                'customerSales' => $customerSales,
                'salesPromoData' => $salesPromoData,
                'salesPromoTotal' => $salesPromoTotal,
                'oldCustomerAquisitionData' => $oldCustomerAquisitionData,
                'newCustomerAquisitionData' => $newCustomerAquisitionData,
                'lastPeriodTotalCustomerVisits' => $LastPeriodTotalCustomerVisits,
                'customerVisitByPromotionData' => $customerVisitByPromotionData,
                'customerVisitProfitLoss' => round($customerVisitProfitLoss,2),
                'loyaltyEarnedProfitLoss' => round($loyaltyEarnedProfitLoss,2),
                'loyaltyRedeemProfitLoss' => round($loyaltyRedeemProfitLoss,2),
                'totalCollectedPoints' => round($loyaltyData['totalCollectedPoints'],2),
                'lastPeriodtotalCollectedPoints' => round($lastPeriodLoyaltyData['totalCollectedPoints'],2),
                'totalRedeemedPoints' => round($loyaltyData['totalRedeemedPoints'],2),
                'lastPeriodtotalRedeemedPoints' => round($lastPeriodLoyaltyData['totalRedeemedPoints'],2),
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
	}

    public function getCustomerWiseSalesReportData($locations = null, $formDate = null, $endDate = null)
    {

        ini_set('memory_limit','-1');
        ini_set('max_execution_time','0');
        $cusData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerWiseSalesForSalesDashboard($locations, $formDate, $endDate);
        $cusCreditNoteData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerWiseCreditNoteDataForSalesDashboard($locations, $formDate, $endDate);
        $cusSalesReturnData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerWiseSalesReturnDataForSalesDashboard($locations, $formDate, $endDate);
        $cusDetails = array();
        $invoice = array();
        $product = array();
        $tmpCusData = [];
        $tmpInvProIds = [];

        foreach ($cusData as $c) {
            $tmpCusData[] = $c;
        }

        foreach ($cusCreditNoteData as $c) {
            $tmpCusData[] = $c;
        }

        foreach ($cusSalesReturnData as $c) {
            $tmpCusData[] = $c;
        }

        $creditNoteTotal = [];
        $creditNoteTotalDisc = [];
        $creditNoteValidation =[];
        $invoiceValidation = [];
        $invoiceTotalDisc = [];
        foreach ($tmpCusData as $c) {
            if ($c['salesInvoiceID']) {
                $cusDetails[$c['customerID']]['totalInvAmount'] +=  floatval($c['salesinvoiceTotalAmount']);
                $cusDetails[$c['customerID']]['customerName'] = $c['customerName'];
                $cusDetails[$c['customerID']]['customerCode'] = $c['customerCode'];
            }
            
            if ($c['creditNoteID']) {
                $cusDetails[$c['customerID']]['totalInvAmount'] -=  floatval($c['creditNoteTotal']);
                $cusDetails[$c['customerID']]['customerName'] = $c['customerName'];
                $cusDetails[$c['customerID']]['customerCode'] = $c['customerCode'];                
            }

            if ($c['salesReturnID']) {
                $cusDetails[$c['customerID']]['totalReturnAmount'] +=  floatval($c['salesReturnTotal']);
                $cusDetails[$c['customerID']]['customerName'] = $c['customerName'];
                $cusDetails[$c['customerID']]['customerCode'] = $c['customerCode'];                
            }
        }

        $totalSales = array_column($cusDetails, 'totalInvAmount');

        array_multisort($totalSales, SORT_DESC, $cusDetails);

        return $cusDetails;
    }
}