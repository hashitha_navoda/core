<?php

/**
 * @author ASHAN <ashan@thinkcube.com>
 * This file credit note payments API related controller functions
 */

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Invoice\Model\CreditNotePayment;
use Invoice\Model\CreditNotePaymentDetails;
use Invoice\Model\Customer;
use Invoice\Model\CreditNote;
use Invoice\Model\CreditNotePaymentMethodNumbers;
use Zend\View\Model\JsonModel;

/**
 * This is the credit note Payments Api controller class
 */
class CreditNotePaymentsAPIController extends CoreController
{

    protected $user_session;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
    }

    function getCustomerRelatedCreditNotesAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $flag = 0;
            $customerID = $request->getPost('customerID');
            $customCurrencyId = $request->getPost('customCurrencyId');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCustomerIDLocationIDAndCurrencyID($customerID, $locationID, $customCurrencyId);
            $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
            $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
            foreach ($paymentMethos as $t) {
                if(!($t['paymentMethodID'] == 4 || $t['paymentMethodID'] == 6) && $t['paymentMethodInSales'] == 1 ){
                    $payMethod[$t['paymentMethodID']] = $t['paymentMethodName'];
                }
            }

            foreach ($creditNoteData as $cn) {
                $cNote = (object) $cn;
                if ($cNote->statusID == 3 && $cNote->creditNotePaymentEligible == 1) {
                    $flag = 1;
                }
                $creditNotes[] = $cNote;
            }
            $creditNotePaymenteView = new ViewModel(array(
                'creditNotes' => $creditNotes,
                'paymentMethod' => $payMethod,
                    )
            );
            $creditNotePaymenteView->setTerminal(true);
            $creditNotePaymenteView->setTemplate('invoice/credit-note-payments-api/addCreditNotePaymentList');
            if ($flag == 1) {
                $this->status = true;
                $this->data = array('customer' => $customer);
                $this->html = $creditNotePaymenteView;
                return $this->JSONRespondHtml();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CNPAY_NO_CN_FOUND');
                return $this->JSONRespond();
            }
        }
    }

    function getCreditNoteDetailsBycreditNoteIDAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customCurrencyId = 0;
            $creditNoteID = $request->getPost('creditNoteID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNoteID);
            foreach ($creditNoteData as $cn) {
                $cNote = (object) $cn;
                $customerID = $cNote->customerID;
                $creditNotes[] = $cNote;
                $customCurrencyId = $cNote->customCurrencyId;
            }

            $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
            if($customer->customerStatus == 2){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CNPAY_CUSTOMER_INACTIVE');
                return $this->JSONRespond();
            }
            $paymentMethos = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
            foreach ($paymentMethos as $t) {
                if(!($t['paymentMethodID'] == 4 || $t['paymentMethodID'] == 6) && $t['paymentMethodInSales'] == 1 ){
                    $payMethod[$t['paymentMethodID']] = $t['paymentMethodName'];
                }
            }

            $creditNotePaymenteView = new ViewModel(array(
                'creditNotes' => $creditNotes,
                'paymentMethod' => $payMethod,
                    )
            );
            $creditNotePaymenteView->setTerminal(true);
            $creditNotePaymenteView->setTemplate('invoice/credit-note-payments-api/addCreditNotePaymentList');
            $this->status = true;
            $this->data = array('customer' => $customer, 'customCurrencyId' => $customCurrencyId);
            $this->html = $creditNotePaymenteView;
            return $this->JSONRespondHtml();
        }
    }

    function addCreditNotePaymentAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerID = $request->getPost('customerID');
            $customerCredit = $request->getPost('customerCredit');
            $cnPaymentData = $request->getPost('creditNotePaymentsData');
            $creditNotepaymentCode = $request->getPost('creditNotepaymentCode');
            $creditNotePaymentDate = $this->convertDateToStandardFormat($request->getPost('date'));
            $paymentTerm = $request->getPost('paymentTerm');
            $creditNotePaymentAmount = $request->getPost('amount');
            $creditNotePaymentDiscount = $request->getPost('discount');
            $creditNotePaymentMemo = $request->getPost('memo');
            $locationID = $request->getPost('locationID');
            $PaymentMethodDetails = json_decode($request->getPost('PaymentMethodDetails'));
            $customCurrencyId = $request->getPost('customCurrencyId');
            $dimensionData = $request->getPost('dimensionData');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');

            $accountPayments = array();

            $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
            $glAccountData = $glAccountExist->current();

            //get customCurrency data by customer CurrencyId
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            $customCurrencyRate = ($customCurrencyId == "" || $customCurrencyId == 0) ? 1 : $customCurrencyData->currencyRate;
            $customCurrencySymbol = ($customCurrencyId == "" || $customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->currencySymbol;

            //calculate loyalty points
            $loyaltyCard = $this->CommonTable('CustomerLoyaltyTable')->getLoyaltyCardByCustomerID($customerID);
            $loyaltyIsSet = false;
            if ($loyaltyCard) {
                $loyaltyIsSet = true;
                $loyltyData = array(
                    'cust_loyal_id' => $loyaltyCard['customerLoyaltyID'],
                    'code' => $loyaltyCard['customerLoyaltyCode'],
                    'available_points' => $loyaltyCard['customerLoyaltyPoints'],
                    'card_name' => $loyaltyCard['loyaltyName'],
                    'min' => $loyaltyCard['loyaltyActiveMinVal'],
                    'max' => $loyaltyCard['loyaltyActiveMaxVal'],
                    'earning_ratio' => $loyaltyCard['loyaltyEarningPerPoint'],
                    'redeem_ratio' => $loyaltyCard['loyaltyRedeemPerPoint'],
                    'loyaltyGlAccountID' => $loyaltyCard['loyaltyGlAccountID']
                );

                $totalBill = ($creditNotePaymentAmount - $creditNotePaymentDiscount)*$customCurrencyRate;
                $earned_loyalty_points = 0;
                if ($loyltyData['min'] <= $totalBill && $loyltyData['max'] >= $totalBill) {
                    $earned_loyalty_points = $totalBill / $loyltyData['earning_ratio'];
                }else if ($loyltyData['max'] <= $totalBill) {
                    $earned_loyalty_points = $loyltyData['max'] / $loyltyData['earning_ratio'];
                }

                if($this->useAccounting == 1){
                    if($earned_loyalty_points > 0){
                        if(empty($loyltyData['loyaltyGlAccountID'])){
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PAY_LOYALTY_CARD_GL_ACCOUNT_NOT_SET',array($loyltyData['code']));
                            return $this->JSONRespond();
                        }else if(empty($glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID)){
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_PAY_LOYALTY_EXPENSE_GL_ACCOUNT_NOT_SET');
                            return $this->JSONRespond();
                        }

                        if(isset($accountPayments[$loyltyData['loyaltyGlAccountID']]['debitTotal'])){
                            $accountPayments[$loyltyData['loyaltyGlAccountID']]['debitTotal'] += $earned_loyalty_points;
                        }else{
                            $accountPayments[$loyltyData['loyaltyGlAccountID']]['debitTotal'] = $earned_loyalty_points;
                            $accountPayments[$loyltyData['loyaltyGlAccountID']]['creditTotal'] = 0.00;
                            $accountPayments[$loyltyData['loyaltyGlAccountID']]['accountID'] = $loyltyData['loyaltyGlAccountID'];
                        }

                        if(isset($accountPayments[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['creditTotal'])){
                            $accountPayments[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['creditTotal'] += $earned_loyalty_points;
                        }else{
                            $accountPayments[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['creditTotal'] = $earned_loyalty_points;
                            $accountPayments[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['debitTotal'] = 0.00;
                            $accountPayments[$glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID]['accountID'] = $glAccountData->glAccountSetupGeneralLoyaltyExpenseAccountID;
                        }
                    }
                }

            }

            $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);


            if($this->useAccounting == 1 && $customerID != ''){
                if(empty($customer->customerReceviableAccountID)){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customer->customerName."-".$customer->customerCode));
                    return $this->JSONRespond();
                } else if($creditNotePaymentDiscount * $customCurrencyRate > 0 && empty($customer->customerSalesDiscountAccountID)){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CUSTOMER_SALES_DISCOUNT_ACCOUNT_NOT_SET',array($customer->customerName."-".$customer->customerCode));
                    return $this->JSONRespond();
                }
            }

            $this->beginTransaction();

            $result = $this->getReferenceNoForLocation('17', $locationID);
            $locationReferenceID = $result['locRefID'];
// check if a Credit note payment from the same credit note payment code exists if exist add next number to credit note payment code
            while ($creditNotepaymentCode) {
                if ($this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentCode($creditNotepaymentCode)) {
                    if ($locationReferenceID) {
                        $this->updateReferenceNumber($locationReferenceID);
                        $creditNotepaymentCode = $this->getReferenceNumber($locationReferenceID);
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CNPAY_CODE_EXISTS');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }

            $entityID = $this->createEntity();

            $loyaltyPoints = 0;
            if($loyaltyIsSet){
                $loyaltyPoints = $earned_loyalty_points;
            }

            $creditNotePaymentData = array(
                'creditNotePaymentCode' => $creditNotepaymentCode,
                'creditNotePaymentDate' => $creditNotePaymentDate,
                'paymentTermID' => $paymentTerm,
                'creditNotePaymentAmount' => $creditNotePaymentAmount * $customCurrencyRate,
                'customerID' => $customerID,
                'creditNotePaymentDiscount' => $creditNotePaymentDiscount * $customCurrencyRate,
                'creditNotePaymentMemo' => $creditNotePaymentMemo,
                'locationID' => $locationID,
                'statusID' => 4,
                'customCurrencyId' => $customCurrencyId,
                'creditNotePaymentCustomCurrencyRate' => $customCurrencyRate,
                'creditNotePaymentLoyaltyAmount' => $loyaltyPoints,
                'entityID' => $entityID,
            );

            if($loyaltyIsSet){
                if (floatval($loyltyData['available_points']) < $loyaltyPoints) {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_LOYALTY_REDEEM_EXCEED');
                    return $this->JSONRespond();
                }
                // Update total loyalty points of the customer
                $Lpoints = (float) $loyltyData['available_points'] - $loyaltyPoints;
                $this->CommonTable('CustomerLoyaltyTable')->updateLoyaltyPoints($loyltyData['cust_loyal_id'], $Lpoints);
            }

            $creditNotePayment = new CreditNotePayment;
            $creditNotePayment->exchangeArray($creditNotePaymentData);
            $creditNotePaymentID = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->saveCreditNotePayment($creditNotePayment);

            $customerData = array(
                'customerCurrentCredit' => $customerCredit,
                'customerID' => $customerID,
            );
            $customerDetails = new Customer;
            $customerDetails->exchangeArray($customerData);
            $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCredit($customerDetails);

            $paymentMethodAmounts = array();
            foreach ($cnPaymentData as $key => $val) {
                $value = (object) $val;
                $creditNoteID = $value->creditNoteID;
                $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNoteID)->current();
                if ($creditNoteData['creditNotePaymentAmount'] > $creditNoteData['creditNoteSettledAmount']) {
                    $creditNotePaymentDetailsData = array(
                        'creditNoteID' => $creditNoteID,
                        'creditNotePaymentID' => $creditNotePaymentID,
                        'creditNotePaymentDetailsAmount' => $value->settledAmount * $value->cCurrencyRate,
                        'paymentMethodID' => $value->paymentMethod,
                        'creditNotePaymentDetailsCurrencyGainOrLoss' => $value->settledAmount * $customCurrencyRate - $value->settledAmount * $value->cCurrencyRate
                        );
                    $creditNotePaymentDetails = new CreditNotePaymentDetails;
                    $creditNotePaymentDetails->exchangeArray($creditNotePaymentDetailsData);
                    $this->CommonTable('Invoice\Model\CreditNotePaymentDetailsTable')->saveCreditNotePaymentDetails($creditNotePaymentDetails);

                    $newCreditNoteSettleAmount = $creditNoteData['creditNoteSettledAmount'] + $value->settledAmount * $value->cCurrencyRate;
                    if ($newCreditNoteSettleAmount == $creditNoteData['creditNotePaymentAmount']) {
                        $statusID = 4;
                    } else {
                        $statusID = 3;
                    }

                    if(isset($paymentMethodAmounts[$value->paymentMethod])){
                        $paymentMethodAmounts[$value->paymentMethod] += $value->settledAmount * $value->cCurrencyRate;
                    }else{
                        $paymentMethodAmounts[$value->paymentMethod] = $value->settledAmount * $value->cCurrencyRate;
                    }

                    $creditNoteUpdateData = array(
                        'creditNoteSettledAmount' => $newCreditNoteSettleAmount,
                        'statusID' => $statusID,
                        'creditNoteID' => $value->creditNoteID,
                        );

                    $creditNoteUpdate = new CreditNote;
                    $creditNoteUpdate->exchangeArray($creditNoteUpdateData);
                    $this->CommonTable('Invoice\Model\CreditNoteTable')->updateCreditNoteSettleAmountAndStatusID($creditNoteUpdate);

                    $this->updateReferenceNumber($locationReferenceID);

                    if ($value->paymentMethod == '1') {
//              update the cash in hand to current location
                        $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($locationID)->current();
                        $newlocationCashInHand = $currentLocationDetails['locationCashInHand'] - ($value->settledAmount * $customCurrencyRate);
                        $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($locationID, $newlocationCashInHand);
                    }
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_ALL_AMPOUNT_HAS_PAID',array($creditNoteData['creditNoteCode']));
                    return $this->JSONRespond();
                }
            }

            if($this->useAccounting == 1){
                $paymethodACC = array();
                $paymentmethod = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
                foreach ($paymentmethod as $key => $t) {
                    if(!($t['paymentMethodID'] == 4 || $t['paymentMethodID'] == 6) && $t['paymentMethodInSales'] == 1){
                        $paymethodACC[$t['paymentMethodID']] = $t['paymentMethodSalesFinanceAccountID'];
                    }
                }
                $totalCRAmounts = 0;
                foreach ($PaymentMethodDetails as $pmkey => $pmvalue) {
                    $financeAccountID = $paymethodACC[$pmvalue->paymentMethodID];
                    if(!empty($financeAccountID)){
                        $totalCRAmounts+=$paymentMethodAmounts[$pmvalue->paymentMethodID];
                        if(isset($accountPayments[$financeAccountID]['creditTotal'])){
                            $accountPayments[$financeAccountID]['creditTotal'] += $paymentMethodAmounts[$pmvalue->paymentMethodID];
                        }else{
                            $accountPayments[$financeAccountID]['creditTotal'] = $paymentMethodAmounts[$pmvalue->paymentMethodID];
                            $accountPayments[$financeAccountID]['debitTotal'] = 0.00;
                            $accountPayments[$financeAccountID]['accountID'] = $financeAccountID;
                        }
                    }else{
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PLEASE_SET_SALES_ACCOUNT_FOR_PAY_METHODS');
                        return $this->JSONRespond();
                    }
                }

                if($creditNotePaymentDiscount * $customCurrencyRate > 0){
                    if(isset($accountPayments[$customer->customerSalesDiscountAccountID]['creditTotal'])){
                        $accountPayments[$customer->customerSalesDiscountAccountID]['creditTotal'] += $creditNotePaymentDiscount * $customCurrencyRate;
                    }else{
                        $accountPayments[$customer->customerSalesDiscountAccountID]['creditTotal'] = $creditNotePaymentDiscount * $customCurrencyRate;
                        $accountPayments[$customer->customerSalesDiscountAccountID]['debitTotal'] = 0.00;
                        $accountPayments[$customer->customerSalesDiscountAccountID]['accountID'] = $customer->customerSalesDiscountAccountID;
                    }
                    $totalCRAmounts += $creditNotePaymentDiscount * $customCurrencyRate;
                }

                if(isset($accountPayments[$customer->customerReceviableAccountID]['debitTotal'])){
                    $accountPayments[$customer->customerReceviableAccountID]['debitTotal'] += $totalCRAmounts;
                }else{
                    $accountPayments[$customer->customerReceviableAccountID]['debitTotal'] = $totalCRAmounts;
                    $accountPayments[$customer->customerReceviableAccountID]['creditTotal'] = 0.00;
                    $accountPayments[$customer->customerReceviableAccountID]['accountID'] = $customer->customerReceviableAccountID;
                }

                $i=0;
                $journalEntryAccounts = array();

                foreach ($accountPayments as $key => $value) {
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Credit Note Payment '.$creditNotepaymentCode.'.';
                    $i++;
                }

                //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $creditNotePaymentDate,
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create Credit Note Payment '.$creditNotepaymentCode.'.',
                    'documentTypeID' => 17,
                    'journalEntryDocumentID' => $creditNotePaymentID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                    );

                $resultData = $this->saveJournalEntry($journalEntryData);
                if(!$resultData['status']){
                    $this->rollback();
                    $this->status = false;
                    $this->msg =$resultData['msg'];
                    $this->data =$resultData['data'];
                    return $this->JSONRespond();
                }

                $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($creditNotePaymentID,17, 4);
                if(!$jEDocStatusUpdate['status']){
                    $this->rollback();
                    $this->status = false;
                    $this->msg =$jEDocStatusUpdate['msg'];
                    return $this->JSONRespond();
                } 

                if (!empty($dimensionData)) {
                    $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$creditNotepaymentCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                    if(!$saveRes['status']){
                        $this->rollback();
                        $this->status = false;
                        $this->msg =$saveRes['msg'];
                        $this->data =$saveRes['data'];
                        return $this->JSONRespond();
                    }                       
                } else {
                    if (sizeof($cnPaymentData) == 1) {
                        foreach ($cnPaymentData as $value) {
                            $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(6,$value['creditNoteID']);
                            $dimensionData = [];
                            foreach ($jEDimensionData as $value) {
                                if (!is_null($value['journalEntryID'])) {
                                    $temp = [];
                                    $temp['dimensionTypeId'] = $value['dimensionType'];
                                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                                    $dimensionData[$creditNotepaymentCode][] = $temp;
                                }
                            }
                            if (!empty($dimensionData)) {
                                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$creditNotepaymentCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                if(!$saveRes['status']){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->msg =$saveRes['msg'];
                                    $this->data =$saveRes['data'];
                                    return $this->JSONRespond();
                                }   
                            }
                        }
                    }
                }
            }

            $this->commit();
            $this->status = true;
            $this->msg = $this->getMessage('SUC_CNPAY_DETAILS_SUC_ADDED');
            $this->data = array('creditNotePaymetID' => $creditNotePaymentID, 'creditNotePaymentCode' => $creditNotepaymentCode);
            return $this->JSONRespond();
        }
    }

    public function addCreditNotePaymentMethodNumbersAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $creditNotePaymentID = $request->getPost('creditNotePaymentID');
            $creditNotePaymentMethodNumbersDetails = json_decode($request->getPost('creditNotePaymentMethodDetails'));

            foreach ($creditNotePaymentMethodNumbersDetails as $key => $value) {
                # code...
                if($value->paymentMethodID != ''){
                    $creditNotePaymentMethodReferenceNumber = (isset($value->creditNotePaymentMethodReferenceNumber))?$value->creditNotePaymentMethodReferenceNumber : '';
                    $creditNotePaymentMethodBank = (isset($value->creditNotePaymentMethodBank))?$value->creditNotePaymentMethodBank : '';
                    $creditNotePaymentMethodCardID = (isset($value->creditNotePaymentMethodCardID))?$value->creditNotePaymentMethodCardID : '';
                    $data = array(
                        'creditNotePaymentID' => $creditNotePaymentID,
                        'paymentMethodID' => $value->paymentMethodID,
                        'creditNotePaymentMethodReferenceNumber' => $creditNotePaymentMethodReferenceNumber,
                        'creditNotePaymentMethodBank' => $creditNotePaymentMethodBank,
                        'creditNotePaymentMethodCardID' => $creditNotePaymentMethodCardID,
                    );
                    $CreditNotePaymentMethodsNumbers = new CreditNotePaymentMethodNumbers();
                    $CreditNotePaymentMethodsNumbers->exchangeArray($data);
                    $this->CommonTable('Invoice\Model\CreditNotePaymentMethodNumbers')->saveCreditNotePaymentMethodsNumbers($CreditNotePaymentMethodsNumbers);
                }
            }

            $this->status = true;
            $this->msg = $this->getMessage('SUC_CNPAY_METHOD_NUMBERS_SUC_ADDED');
            $this->data = $creditNotePaymentID;
            $this->setLogMessage('Credit Note Payment method numbers detais sucssesfully saved ');
        } else {
            $this->setLogMessage("Credit Note Add payment method numbers request is not a post request");
            $this->status = false;
        }
        return $this->JSONRespond();
    }

    public function retriveCustomerCreditNotePaymentsAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $cust_id = $request->getPost('customerID');
            $creditNotePayments = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentsByCustomerIDAndLocationID($cust_id, $locationID);
//            if ($creditNotePayments->count() == 0) {
//                $er = 'noCreditNotePayments';
//                return new JsonModel(array($er));
//            } else {
            foreach ($creditNotePayments as $t) {
                $creditNotePaymentsArray[$t['creditNotePaymentID']] = (object) $t;
            }
            $customerCreditNotePayments = new ViewModel(
                    array(
                'creditNotePayments' => $creditNotePaymentsArray,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'statuses' => $this->getStatusesList()
            ));
            $customerCreditNotePayments->setTerminal(TRUE);
            $customerCreditNotePayments->setTemplate('invoice/credit-note-payments/credit-note-payment-list');
            return $customerCreditNotePayments;
//            }
        }
    }

    public function retriveCreditNotePaymentByCreditNotePaymentIDAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $creditNotePaymentID = $request->getPost('creditNotePaymentID');
            $creditNotePayments = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentID($creditNotePaymentID, true);
            if ($creditNotePayments->count() == 0) {
                $er = 'notFindCreditNotePayment';
                return new JsonModel(array($er));
            } else {
                foreach ($creditNotePayments as $t) {
                    $creditNotePaymentsArray[$t['creditNotePaymentID']] = (object) $t;
                }
                $customerCreditNotePayments = new ViewModel(
                        array(
                    'creditNotePayments' => $creditNotePaymentsArray,
                    'companyCurrencySymbol' => $this->companyCurrencySymbol,
                    'statuses' => $this->getStatusesList()
                ));
                $customerCreditNotePayments->setTerminal(TRUE);
                $customerCreditNotePayments->setTemplate('invoice/credit-note-payments/credit-note-payment-list');
                return $customerCreditNotePayments;
            }
        }
    }

    public function retriveCreditNotePaymentsByDatefilterAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $fromdate = $request->getPost('fromdate');
            $todate = $request->getPost('todate');
            $customerID = $request->getPost('customerID');
            $getpayment = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentsByDate($fromdate, $todate, $customerID, $locationID);
            foreach ($getpayment as $t) {
                $creditNotePaymentsArray[$t['creditNotePaymentID']] = (object) $t;
            }
            $customerCreditNotePayments = new ViewModel(array(
                'creditNotePayments' => $creditNotePaymentsArray,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'statuses' => $this->getStatusesList()
                    )
            );
            $customerCreditNotePayments->setTerminal(TRUE);
            $customerCreditNotePayments->setTemplate('invoice/credit-note-payments/credit-note-payment-list');
            return $customerCreditNotePayments;
        }
    }

    public function deleteCreditNotePaymentAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $creditNotePaymentID = $request->getPost('creditNotePaymentID');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $username = trim($request->getPost('username'));
            $password = trim($request->getPost('password'));
            $creditNotePaymentCancelMessage = $request->getPost('creditNotePaymentCancelMessage');
            $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
            $locID = $this->user_session->userActiveLocation['locationID'];
            if ($user) {
                if ($user->roleID == '1') {
                    $passwordData = explode(':', $user->userPassword);
                    $storedPassword = $passwordData[0];
                    $checkPassword = md5($password . $passwordData[1]);

                    if ($storedPassword == $checkPassword) {
                        $this->beginTransaction();

                        $customerAddCreditBalance = 0.00;
                        $creditNotePaymentdata = (object) $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentID($creditNotePaymentID)->current();
                        $creditNotePaymentDetailsdata = $this->CommonTable('Invoice\Model\CreditNotePaymentDetailsTable')->getCreditNotePaymentDetailsByCreditNotePaymentID($creditNotePaymentID);
                        foreach ($creditNotePaymentDetailsdata as $data) {
                            $creditNoteNewSettledAmount = 0.00;
                            $data = (object) $data;
                            $customerAddCreditBalance += $data->creditNotePaymentDetailsAmount;
                            $creditNoteID = $data->creditNoteID;
                            $creditNoteDetails = (object) $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNoteID)->current();
                            $creditNoteNewSettledAmount = $creditNoteDetails->creditNoteSettledAmount - $data->creditNotePaymentDetailsAmount;
                            $creditNoteData = array(
                                'creditNoteID' => $creditNoteID,
                                'statusID' => 3,
                                'creditNoteSettledAmount' => $creditNoteNewSettledAmount,
                            );
                            $creditNote = new CreditNote;
                            $creditNote->exchangeArray($creditNoteData);
                            $this->CommonTable('Invoice\Model\CreditNoteTable')->updateCreditNoteSettleAmountAndStatusID($creditNote);
                            if ($data->paymentMethodID == 1) {
//                                update the cash in hand to current locations
                                $locationID = $this->CommonTable('Invoice\ModelCreditNoteTable')->getCreditNoteByCreditNoteID($data->creditNoteID)->current()['locationID'];
                                $currentLocationDetails = $this->CommonTable('Settings\ModelLocationTable')->getLocationCashInHandAmount($locationID)->current();
                                $newlocationCashInHand = $currentLocationDetails['locationCashInHand'] + $data->creditNotePaymentDetailsAmount;
                                $this->CommonTable('Settings\Model\LocationTable')->updateLocationAmount($locationID, $newlocationCashInHand);
                            }
                        }
                        $customerID = $creditNotePaymentdata->customerID;

                        $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                        $customerNewCurrentCredit = $customerDetails->customerCurrentCredit + $customerAddCreditBalance;

                        $customerData = array(
                            'customerCurrentCredit' => $customerNewCurrentCredit,
                            'customerID' => $customerID,
                        );
                        $customerUpdateData = new Customer;
                        $customerUpdateData->exchangeArray($customerData);
                        $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerCredit($customerUpdateData);

                        $loyaltyCard = $this->CommonTable('CustomerLoyaltyTable')->getLoyaltyCardByCustomerID($customerID);
                        if($loyaltyCard){
                            $loyaltyPoints =(float) $creditNotePaymentdata->creditNotePaymentLoyaltyAmount;
                            $Lpoints = (float) $loyaltyCard['customerLoyaltyPoints'] + $loyaltyPoints;
                            $this->CommonTable('CustomerLoyaltyTable')->updateLoyaltyPoints($loyaltyCard['customerLoyaltyID'], $Lpoints);
                        }

                        $accountProduct = array();
                        if ($this->useAccounting == 1) {
                            $memo =  'Created By Credit Note Payment Delete '.$creditNotePaymentdata->creditNotePaymentCode.'.';
                            $comment =  'Journal Entry is posted when delete Credit Note Payment '.$creditNotePaymentdata->creditNotePaymentCode.'.';

                            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('17', $creditNotePaymentID);
                            $journalEntryID = $journalEntryData['journalEntryID'];
                            $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);

                            $i=0;
                            $journalEntryAccounts = array();
                            foreach ($jEAccounts as $key => $value) {
                                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                                $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
                                $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
                                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
                                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
                                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = $memo;
                                $i++;
                            }

                    //get journal entry reference number.
                            $jeresult = $this->getReferenceNoForLocation('30', $locID);
                            $jelocationReferenceID = $jeresult['locRefID'];
                            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                            $journalEntryData = array(
                                'journalEntryAccounts' => $journalEntryAccounts,
                                'journalEntryDate' => $this->convertDateToStandardFormat($this->getUserDateTime()),
                                'journalEntryCode' => $JournalEntryCode,
                                'journalEntryTypeID' => '',
                                'journalEntryIsReverse' => 0,
                                'journalEntryComment' => $comment,
                                'documentTypeID' => 17,
                                'journalEntryDocumentID' => $creditNotePaymentID,
                                'ignoreBudgetLimit' => $ignoreBudgetLimit,
                                );

                            $resultData = $this->saveJournalEntry($journalEntryData);

                            if(!$resultData['status']){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $resultData['msg'];
                                $this->data = $resultData['data'];
                                return $this->JSONRespond();
                            }

                            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($creditNotePaymentID,17, 5);
                            if(!$jEDocStatusUpdate['status']){
                                $this->rollback();
                                $this->status = false;
                                $this->msg =$jEDocStatusUpdate['msg'];
                                return $this->JSONRespond();
                            } 

                            $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(17,$creditNotePaymentID);
                            $dimensionData = [];
                            foreach ($jEDimensionData as $value) {
                                if (!is_null($value['journalEntryID'])) {
                                    $temp = [];
                                    $temp['dimensionTypeId'] = $value['dimensionType'];
                                    $temp['dimensionValueId'] = $value['dimensionValueID'];
                                    $dimensionData[$creditNotePaymentdata->creditNotePaymentCode][] = $temp;
                                }
                            }
                            if (!empty($dimensionData)) {
                                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$creditNotePaymentdata->creditNotePaymentCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                                if(!$saveRes['status']){
                                    $this->rollback();
                                    $this->status = false;
                                    $this->data = $saveRes['data'];
                                    $this->msg = $saveRes['msg'];
                                    return $this->JSONRespond();
                                }   
                            }
                        }

                        $entityID = $creditNotePaymentdata->entityID;
                        $updateResult = $this->updateDeleteInfoEntity($entityID);
                        $this->_updatePaymentStatus($creditNotePaymentID, $creditNotePaymentCancelMessage);
                        if ($updateResult) {
                            $this->commit();
                            $this->status = true;
                            $this->data = true;
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->data = false;
                        }
                    } else {
                        $this->status = false;
                        $this->data = 'pass';
                    }
                } else {
                    $this->status = false;
                    $this->data = 'admin';
                }
            } else {
                $this->status = false;
                $this->data = 'user';
            }
        } else {
            $this->setLogMessage("Credit Note Payment delete request is not a post request");
            $this->status = false;
            $this->data = false;
        }
        return $this->JSONRespond();
    }

    private function _updatePaymentStatus($paymentID, $creditNotePaymentCancelMessage)
    {
        $cancelStatusID = $this->getStatusID('cancelled');
        $data = array(
            'statusID' => $cancelStatusID,
            'creditNotePaymentCancelMessage' => $creditNotePaymentCancelMessage
        );
        $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->update($data, $paymentID);
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Credit Note Payment';
        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_CNPAYAPI_SENT_EMAIL');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_CNPAYAPI_SENT_EMAIL');
        return $this->JSONRespond();
    }

    /**
     * Search All Credit note payments for drop down
     * @return type
     */
    public function searchAllCreditNotePaymentsForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $allCrediNotePayments = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->searchAllCreditNotepaymentsForDropdown($locationID, $searchKey);

            $allCrediNotePaymentList = array();
            foreach ($allCrediNotePayments as $allCrediNotePayment) {
                $temp['value'] = $allCrediNotePayment['creditNotePaymentID'];
                $temp['text'] = $allCrediNotePayment['creditNotePaymentCode'];
                $allCrediNotePaymentList[] = $temp;
            }

            $this->data = array('list' => $allCrediNotePaymentList);
            return $this->JSONRespond();
        }
    }
    /**
    * use to update credit note payment by pos
    *  @param int $creditNoteAmount
    *  @param int $creditNoteID
    *  @param int $locationID
    *   return array
    */
    public function saveCreditNotePaymentByPOS($creditNoteAmount,$creditNoteID,$locationID ,$customerID, $ignoreBudgetLimit = true)
    {

        $paymentDate = $this->convertDateToStandardFormat($this->getGMTDateTime());
        $refData = $this->getReferenceNoForLocation(17, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];
        $entityID = $this->createEntity();

        $accountPayments = array();

        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountData = $glAccountExist->current();

        $creditNotePaymentDiscount = isset($creditNotePaymentDiscount)?$creditNotePaymentDiscount :0;
        $customer = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

        if($this->useAccounting == 1 && $customerID != ''){
            if(empty($customer->customerReceviableAccountID)){
                return array(
                    'status' =>false,
                    'msg' =>$this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customer->customerName."-".$customer->customerCode)),
                );
            } else if($creditNotePaymentDiscount > 0 && empty($customer->customerSalesDiscountAccountID)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CUSTOMER_SALES_DISCOUNT_ACCOUNT_NOT_SET',array($customer->customerName."-".$customer->customerCode)),
                );
            }
        }

        $this->beginTransaction();

        $creditNotePaymentData = array(
            'creditNotePaymentCode' => $rid,
            'creditNotePaymentDate' => $paymentDate,
            'paymentTermID' => 1, //currenty no other options.
            'creditNotePaymentAmount' => $creditNoteAmount,
            'customerID' => $customerID,
            'creditNotePaymentDiscount' => $creditNotePaymentDiscount,
            'locationID' => $locationID,
            'statusID' => 4,
            'creditNotePaymentCustomCurrencyRate' => 1, // this is default currency
            'entityID' => $entityID,
        );

        $creditNotePayment = new CreditNotePayment;
        $creditNotePayment->exchangeArray($creditNotePaymentData);
        $creditNotePaymentID = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->saveCreditNotePayment($creditNotePayment);
        if(!$creditNotePaymentID){
            $this->rollback();
            return $data = array(
                'status' => false,
                'msg'=> 'ERR_CREDIT_NOTE_PAYMENT_SAVE',
                );
        }

        //update credit note payment Details table. for this case payment method ID always equal to 1.
        $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNoteID)->current();
        if ($creditNoteData['creditNotePaymentAmount'] > $creditNoteData['creditNoteSettledAmount']) {

            $creditNotePaymentDetailsData = array(
                'creditNoteID' => $creditNoteID,
                'creditNotePaymentID' => $creditNotePaymentID,
                'creditNotePaymentDetailsAmount' => $creditNoteAmount,
                'paymentMethodID' => 1,
                'creditNotePaymentDetailsCurrencyGainOrLoss' => 0,
                );

            $creditNotePaymentDetails = new CreditNotePaymentDetails;
            $creditNotePaymentDetails->exchangeArray($creditNotePaymentDetailsData);
            $res =  $this->CommonTable('Invoice\Model\CreditNotePaymentDetailsTable')->saveCreditNotePaymentDetails($creditNotePaymentDetails);

            if(!$res){
                $this->rollback();
                return $data = array(
                    'status' => false,
                    'msg'=> 'ERR_CREDIT_NOTE_PAYMENT_DETAILS_SAVE',
                    );
            }

        }else{
           $this->rollback();
           return array(
                'status' => false,
                'msg' => $this->getMessage('ERR_CREDIT_NOTE_ALL_AMPOUNT_HAS_PAID',array($creditNoteData['creditNoteCode'])),
            );
        }

        $this->updateReferenceNumber($lrefID);
        if($this->useAccounting == 1){
            $totalCRAmounts = 0;
            $paymethodACC = array();
            $paymentmethod = $this->CommonTable('Core\Model\PaymentMethodTable')->fetchAll();
            foreach ($paymentmethod as $key => $t) {
                $paymethodACC[$t['paymentMethodID']] = $t['paymentMethodSalesFinanceAccountID'];
            }

            $financeAccountID = $paymethodACC[1];
            if(!empty($financeAccountID)){
                if(isset($accountPayments[$financeAccountID]['creditTotal'])){
                    $accountPayments[$financeAccountID]['creditTotal'] += $creditNoteAmount;
                }else{
                    $accountPayments[$financeAccountID]['creditTotal'] = $creditNoteAmount;
                    $accountPayments[$financeAccountID]['debitTotal'] = 0.00;
                    $accountPayments[$financeAccountID]['accountID'] = $financeAccountID;
                }
                $totalCRAmounts+= $creditNoteAmount;
            }else{
                $this->rollback();
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_PLEASE_SET_SALES_ACCOUNT_FOR_PAY_METHODS'),
                );
            }

            if($creditNotePaymentDiscount > 0){
                if(isset($accountPayments[$customer->customerSalesDiscountAccountID]['creditTotal'])){
                    $accountPayments[$customer->customerSalesDiscountAccountID]['creditTotal'] += $creditNotePaymentDiscount ;
                }else{
                    $accountPayments[$customer->customerSalesDiscountAccountID]['creditTotal'] = $creditNotePaymentDiscount ;
                    $accountPayments[$customer->customerSalesDiscountAccountID]['debitTotal'] = 0.00;
                    $accountPayments[$customer->customerSalesDiscountAccountID]['accountID'] = $customer->customerSalesDiscountAccountID;
                }
                $totalCRAmounts += $creditNotePaymentDiscount;
            }

            if(isset($accountPayments[$customer->customerReceviableAccountID]['debitTotal'])){
                $accountPayments[$customer->customerReceviableAccountID]['debitTotal'] += $totalCRAmounts;
            }else{
                $accountPayments[$customer->customerReceviableAccountID]['debitTotal'] = $totalCRAmounts;
                $accountPayments[$customer->customerReceviableAccountID]['creditTotal'] = 0.00;
                $accountPayments[$customer->customerReceviableAccountID]['accountID'] = $customer->customerReceviableAccountID;
            }

            $i=0;
            $journalEntryAccounts = array();

            foreach ($accountPayments as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Credit Note Payment '.$rid.'.';
                $i++;
            }

                //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationID);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $paymentDate,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Credit Note Payment '.$rid.'.',
                'documentTypeID' => 17,
                'journalEntryDocumentID' => $creditNotePaymentID,
                'ignoreBudgetLimit' => $ignoreBudgetLimit,
                );

            $resultData = $this->saveJournalEntry($journalEntryData);
            if(!$resultData['status']){
                $this->rollback();
                return array(
                    'status' =>false,
                    'msg' =>$resultData['msg'],
                    'data' =>$resultData['data'],
                );
            }
        }

        $this->commit();
        return $data =array(
            'status' => true,
            'paymentCode' => $rid,
            );
   }

   public function getAllRelatedDocumentDetailsByCreditNotePaymentIDAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $creditNotePaymentID = $request->getPost('creditNotePaymentID');
            $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getCreditNotePaymentByCreditNotePaymentID($creditNotePaymentID)->current();


            if (isset($creditNotePaymentData)) {
                $tempCreditNotePaymentData = array(
                    'type' => 'CreditNotePayment',
                    'documentID' => $creditNotePaymentData['creditNotePaymentID'],
                    'code' => $creditNotePaymentData['creditNotePaymentCode'],
                    'amount' => number_format($creditNotePaymentData['creditNotePaymentAmount'], 2),
                    'issuedDate' => $creditNotePaymentData['creditNotePaymentDate'],
                    'created' => $creditNotePaymentData['createdTimeStamp'],
                );
                $historyData[] = $tempCreditNotePaymentData;
                if (isset($creditNotePaymentData)) {
                    $dataExistsFlag = true;
                }
            }

            $getCreditNotePaymentsRelatedCreditNotes = $this->CommonTable('Invoice\Model\CreditNotePaymentDetailsTable')->getCreditNotePaymentDetailsByCreditNotePaymentID($creditNotePaymentID);

            foreach ($getCreditNotePaymentsRelatedCreditNotes as $key => $cr) {
                $creditNoteID = $cr['creditNoteID'];
                $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteBasicDetailsByCreditNoteID($creditNoteID);
                // $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getQuotationRelatedCreditNotePaymentDataByQuotationId(null, $creditNoteID);
                $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesOrderRelatedSalesInvoiceDataByQuotationId(null, null ,$creditNoteID);
                $deliveryNoteData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getinvoiceRelatedDeliveryNoteDataByInvoiceId(null, $creditNoteID);
                $salesReturnData = $this->CommonTable('SalesReturnsTable')->getSalesReturnByDeliveryNoteID(null,$creditNoteID);
                $dlnRelatedSalesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getDlnRelatedSalesOrderDataBycreditNoteId($creditNoteID);
                $soRelatedQuotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getSalesOrderRelatedQuotationDataByCreditNoteId($creditNoteID);
                $invRelatedSalesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getDlnRelatedSalesOrderDataBydlnId(null, null, null, $creditNoteID);
                $invRelatedQuotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getSalesOrderRelatedQuotationDataBySOId(null, null, null, $creditNoteID);
                $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getSalesOrderRelatedInvoicePaymentsDataByQuotationId(null, null, $creditNoteID);
                

                $dataExistsFlag = false;
                if ($creditNoteData) {
                    $creditNoteData = array(
                        'type' => 'CreditNote',
                        'documentID' => $creditNoteData['creditNoteID'],
                        'code' => $creditNoteData['creditNoteCode'],
                        'amount' => number_format($creditNoteData['creditNoteTotal'], 2),
                        'issuedDate' => $creditNoteData['creditNoteDate'],
                        'created' => $creditNoteData['createdTimeStamp'],
                    );
                    $historyData[] = $creditNoteData;
                    $dataExistsFlag = true;
                }

            
                if (isset($invoiceData)) {
                    foreach ($invoiceData as $invDta) {
                        $invDeta = array(
                            'type' => 'SalesInvoice',
                            'documentID' => $invDta['salesInvoiceID'],
                            'code' => $invDta['salesInvoiceCode'],
                            'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                            'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                            'created' => $invDta['createdTimeStamp'],
                        );
                        $historyData[] = $invDeta;
                        if (isset($invDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                if (isset($salesReturnData)) {
                    foreach ($salesReturnData as $srDta) {
                        $returnData = array(
                            'type' => 'SalesReturn',
                            'documentID' => $srDta['salesReturnID'],
                            'code' => $srDta['salesReturnCode'],
                            'amount' => number_format($srDta['salesReturnTotal'], 2),
                            'issuedDate' => $srDta['salesReturnDate'],
                            'created' => $srDta['createdTimeStamp'],
                        );
                        $historyData[] = $returnData;
                        if (isset($srDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($deliveryNoteData)) {
                    foreach ($deliveryNoteData as $dlnDta) {
                        $dlnData = array(
                            'type' => 'DeliveryNote',
                            'documentID' => $dlnDta['salesInvoiceProductDocumentID'],
                            'code' => $dlnDta['deliveryNoteCode'],
                            'amount' => number_format($dlnDta['deliveryNotePriceTotal'], 2),
                            'issuedDate' => $dlnDta['deliveryNoteDeliveryDate'],
                            'created' => $dlnDta['createdTimeStamp'],
                        );
                        $historyData[] = $dlnData;
                        if (isset($dlnDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($dlnRelatedSalesOrderData)) {
                    foreach ($dlnRelatedSalesOrderData as $soDta) {
                        $soData = array(
                            'type' => 'SalesOrder',
                            'documentID' => $soDta['soID'],
                            'code' => $soDta['soCode'],
                            'amount' => number_format($soDta['totalAmount'], 2),
                            'issuedDate' => $soDta['issuedDate'],
                            'created' => $soDta['createdTimeStamp'],
                        );
                        $historyData[] = $soData;
                        if (isset($soDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
                
                if (isset($invRelatedSalesOrderData)) {
                    foreach ($invRelatedSalesOrderData as $soDta) {
                        $soData = array(
                            'type' => 'SalesOrder',
                            'documentID' => $soDta['soID'],
                            'code' => $soDta['soCode'],
                            'amount' => number_format($soDta['totalAmount'], 2),
                            'issuedDate' => $soDta['issuedDate'],
                            'created' => $soDta['createdTimeStamp'],
                        );
                        $historyData[] = $soData;
                        if (isset($soDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($soRelatedQuotaionData)) {
                    foreach ($soRelatedQuotaionData as $qtnDta) {
                        $qtnDeta = array(
                            'type' => 'Quotation',
                            'documentID' => $qtnDta['quotationID'],
                            'code' => $qtnDta['quotationCode'],
                            'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                            'issuedDate' => $qtnDta['quotationIssuedDate'],
                            'created' => $qtnDta['createdTimeStamp'],
                        );
                        $historyData[] = $qtnDeta;
                        if (isset($qtnDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($invRelatedQuotaionData)) {
                    foreach ($invRelatedQuotaionData as $qtnDta) {
                        $qtnDeta = array(
                            'type' => 'Quotation',
                            'documentID' => $qtnDta['quotationID'],
                            'code' => $qtnDta['quotationCode'],
                            'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                            'issuedDate' => $qtnDta['quotationIssuedDate'],
                            'created' => $qtnDta['createdTimeStamp'],
                        );
                        $historyData[] = $qtnDeta;
                        if (isset($qtnDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }

                if (isset($paymentData)) {
                    foreach ($paymentData as $payDta) {
                        $paymentData = array(
                            'type' => 'CustomerPayment',
                            'documentID' => $payDta['incomingPaymentID'],
                            'code' => $payDta['incomingPaymentCode'],
                            'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                            'issuedDate' => $payDta['incomingPaymentDate'],
                            'created' => $payDta['createdTimeStamp'],
                        );
                        $historyData[] = $paymentData;
                        if (isset($payDta)) {
                            $dataExistsFlag = true;
                        }
                    }
                }
            }
 
            $sortData = Array();
            foreach ($historyData as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $historyData);

            $documentDetails = array(
                'historyData' => $historyData
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }

}

/////////////////// END OF Product CONTROLLER \\\\\\\\\\\\\\\\\\\\



