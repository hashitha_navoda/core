<?php
namespace Invoice\Controller\API;

use Zend\View\Model\ViewModel;
use Core\Controller\CoreController;
use Zend\Session\Container;
use Settings\Model\ItemAttribute;
use Settings\Model\ItemAttributeValue;
use Settings\Form\ItemAttributeForm;
use Zend\View\Model\JsonModel;

class ServiceChargeTypeController extends CoreController
{

    public function saveAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $respond = $this->getService('ServiceChargeTypeService')->saveServiceChargeType($req->getPost(), $locationID);
        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
        
    }

    public function deleteServiceChargeTypeAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('ServiceChargeTypeService')->deleteServiceChargeType($req->getPost());
        if (!$respond['status']) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }
        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage($respond['msg']);
        return $this->JSONRespond();
    }

    public function searchAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $respond = $this->getService('ServiceChargeTypeService')->searchServiceChargeType($req->getPost());

        if (!$respond['status']) {
            $this->status = false;
            $this->msg = $this->getMessage($respond['msg']);
            return $this->JSONRespond();
        }

        $view = new ViewModel(array(
            'srcTypeList' => $respond['srcTypeList'],
            'paginated' => $respond['paginated']
        ));
        
        $view->setTerminal(true);
        $this->status = true;
        $view->setTemplate('invoice/service-charge-type/list');
        $this->html = $view;
        return $this->JSONRespondHtml();
        
    }

    public function searchServiceChargesForDropdownAction()
    {
        $req = $this->getRequest();
        if(!$req->isPost()){
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONNECTION');
            return $this->JSONRespond();
        }
        $this->beginTransaction();
        $respond = $this->getService('ServiceChargeTypeService')->searchServiceChargesForDropdown($req->getPost());
        
        $this->commit();
        $this->status = true;
        $this->data = array('list' => $respond);
        return $this->JSONRespond();   
    }

    
}
