<?php

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Zend\Session\Container;

class SalesDashboardController extends CoreController
{
	protected $userID;
    protected $cdnUrl;
    protected $user_session;
    protected $paginator;
    protected $company;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

	public function getWidgetDataAction()
	{
		$request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                	$lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                 	$lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                 	$lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $lastYearInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationID($fromDate,$toDate,$locationID);


            $lastYearCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $creditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $totalSales = floatval($invoiceData['invTotal']) - floatval($creditnoteData['total']);
            $totalSalesLastYear = floatval($lastYearInvoiceData['invTotal']) - floatval($lastYearCreditnoteData['total']);

            $totalSalesProfitLoss = (($totalSales - $totalSalesLastYear) / $totalSales) * 100;

            $invoiceCountProfitLoss = (($invoiceData['invCount'] - $lastYearInvoiceData['invCount']) / $invoiceData['invCount']) * 100;

            $grossProfitData = $this->getService('SalesReportService')->_getGrossProfitData([$locationID], $fromDate, $toDate);
            
            $lastYearGrossProfitData = $this->getService('SalesReportService')->_getGrossProfitData([$locationID], $lastPeriodFromDate, $lastPeriodToDate);

            $totalGrossProfit = 0;
            $totalGValue = 0;
            $totalCost = 0;
            foreach ($grossProfitData as $key => $d) {
                foreach ($d['locGPD'] as $v) {
                    $nonInventoryItemTotal = ($v['nonInventoryItemTotal']) ? $v['nonInventoryItemTotal'] : 0;
                    $value = $v['documentValue'] ? $v['documentValue'] : 0;
                    $cost = $v['documentCost'] ? $v['documentCost'] : 0;
                    $totalTax = $v['totalTax'] ? $v['totalTax'] : 0;
                    if ($v['documentType'] == 'Sales Invoice') {
                        $value = $value + $nonInventoryItemTotal;
                        $grossProfit = $value - ($cost);
                        $totalGValue+=$value;
                        $totalCost+=($cost);
                    } else if ($v['documentType'] == 'Credit Note') {
                        $value = $value - $totalTax;
                        $grossProfit = ($cost) - $value;
                        $totalGValue-=$value;
                        $totalCost-=($cost);
                    }                                    
                    $totalGrossProfit+=$grossProfit;      
            	}
            }

            $lastYearTotalGrossProfit = 0;
            foreach ($lastYearGrossProfitData as $key => $d) {
                foreach ($d['locGPD'] as $v) {
                    $nonInventoryItemTotal = ($v['nonInventoryItemTotal']) ? $v['nonInventoryItemTotal'] : 0;
                    $value = $v['documentValue'] ? $v['documentValue'] : 0;
                    $cost = $v['documentCost'] ? $v['documentCost'] : 0;
                    $totalTax = $v['totalTax'] ? $v['totalTax'] : 0;
                    if ($v['documentType'] == 'Sales Invoice') {
                        $value = $value + $nonInventoryItemTotal;
                        $grossProfit = $value - ($cost);
                        // $totalGValue+=$value;
                    } else if ($v['documentType'] == 'Credit Note') {
                        $value = $value - $totalTax;
                        $grossProfit = ($cost) - $value;
                        // $totalGValue-=$value;
                    }                                    
                    $lastYearTotalGrossProfit+=$grossProfit;      
                }
            }

            $gpPercentage = ($totalGrossProfit / $totalGValue) * 100;

            $totalGrossProfitProfitLoss = (($totalGrossProfit - $lastYearTotalGrossProfit) / $totalGrossProfit) * 100;

            $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalPaymentsByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $lastYearPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalPaymentsByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $totalPaymentProfitLoss = (($paymentData['totalPayment'] - $lastYearPaymentData['totalPayment']) / $paymentData['totalPayment']) * 100;

            //get open invoices
            $openInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationIDAndStatus($fromDate,$toDate,$locationID, 3);

            $openInvoiceIDs = [];
            $openInvoiceTotal = 0;
            foreach ($openInvoiceData as $value) {
            	$openInvoiceIDs[] = $value['salesInvoiceID'];
            	$openInvoiceTotal += (floatval($value['invAmount']) - floatval($value['invPayedAmountAmount']));
            }

            if (!empty($openInvoiceIDs)) {
            	$openInvoiceCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromDate,$toDate,$locationID, $openInvoiceIDs);
            }

            $openInvoiceTotal = $openInvoiceTotal - floatval($openInvoiceCreditnoteData['total']);

			//overdueInvoice
			//get open invoices
            $overdueInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationIDAndStatus($fromDate,$toDate,$locationID, 6);

            $overdueInvoiceIDs = [];
            $overdueInvoiceTotal = 0;
            foreach ($overdueInvoiceData as $value) {
            	$overdueInvoiceIDs[] = $value['salesInvoiceID'];
            	$overdueInvoiceTotal += (floatval($value['invAmount']) - floatval($value['invPayedAmountAmount']));
            }

            if (!empty($overdueInvoiceIDs)) {
            	$overdueInvoiceCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromDate,$toDate,$locationID, $overdueInvoiceIDs);
            }

            $overdueInvoiceTotal = $overdueInvoiceTotal - floatval($overdueInvoiceCreditnoteData['total']);                    

            // closed invoice total
            $closedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationIDAndStatus($fromDate,$toDate,$locationID, 4);

            $closedInvoiceIDs = [];
            $closedInvoiceTotal = 0;
            foreach ($closedInvoiceData as $value) {
            	$closedInvoiceIDs[] = $value['salesInvoiceID'];
            	$closedInvoiceTotal += floatval($value['invAmount']);
            }
            if (!empty($closedInvoiceIDs)) {
            	$closedInvoiceCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromDate,$toDate,$locationID, $closedInvoiceIDs);
            }

            $closedInvoiceTotal = $closedInvoiceTotal - floatval($closedInvoiceCreditnoteData['total']); 

            //payment data with payment method
            $paymentDataWithMethods = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsSummeryDataForSalesDashboard($fromDate,$toDate,$locationID);
            $cashPaymentAmount = 0;
            $chequePaymentAmount = 0;
            $cardPaymentAmount = 0;
            $loyaltyPaymentAmount = 0;
            $bankTransferPaymentAmount = 0;
            $giftCardPaymentAmount = 0;
            $lcPaymentAmount = 0;
            $ttPaymentAmount = 0;
            $creditPaymentAmount = 0;
            $creditArray = [];
            foreach ($paymentDataWithMethods as $res) {
                if ($res['incomingPaymentMethodCashId'] != 0) {
                    $cashPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if ($res['incomingPaymentMethodChequeId'] != 0) {
                    $chequePaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if ($res['incomingPaymentMethodCreditCardId'] != 0) {
                    $cardPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if (isset($res['incomingPaymentMethodLoyaltyCardId']) && is_null($res['incomingPaymentMethodCashId']) && is_null($res['incomingPaymentMethodChequeId']) && is_null($res['incomingPaymentMethodCreditCardId']) && is_null($res['incomingPaymentMethodBankTransferId']) && is_null($res['incomingPaymentMethodGiftCardId'])) {
                    $loyaltyPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if ($res['incomingPaymentMethodBankTransferId'] != 0) {
                    $bankTransferPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if ($res['incomingPaymentMethodGiftCardId'] != 0) {
                    $giftCardPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if (isset($res['incomingPaymentMethodLCId']) && $res['incomingPaymentMethodLCId'] != "0") {
                    $lcPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if (isset($res['incomingPaymentMethodTTId']) && $res['incomingPaymentMethodTTId'] != "0") {
                    $ttPaymentAmount += $res['incomingPaymentMethodAmount'];
                }

                if (!is_null($res['incomingPaymentCreditAmount'])) {
            		if (!array_key_exists($res['incomingPaymentID'],$creditArray)) {
	                	$creditPaymentAmount += $res['incomingPaymentCreditAmount'];
	            		$creditArray[$res['incomingPaymentID']] = $res;
            		}
                }
            }

            //high sale items
            $heigstSalesItemData = $this->_getDailySalesItemsDetails($fromDate,$toDate,$locationID);
            $customerSales = $this->getCustomerWiseSalesReportData([$locationID],$fromDate,$toDate);
            
            header('Content-Type: application/json');
            $widgetData = array(
                'totalSales' => round($totalSales,2),
                'totalGrossValue' => round($totalGValue,2),
                'totalCost' => round($totalCost,2),
                'totalSalesLastYear' => round($totalSalesLastYear,2),
                'totalSalesProfitLoss' => round($totalSalesProfitLoss,2),
                'totalGrossProfitProfitLoss' => round($totalGrossProfitProfitLoss,2),
                'totalPaymentProfitLoss' => round($totalPaymentProfitLoss,2),
                'invoiceCountProfitLoss' => round($invoiceCountProfitLoss,2),
                'gpPercentage' => round($gpPercentage),
                'grossProfit' => round($totalGrossProfit,2),
                'openInvoiceTotal' => round($openInvoiceTotal,2),
                'overdueInvoiceTotal' => round($overdueInvoiceTotal,2),
                'closedInvoiceTotal' => round($closedInvoiceTotal,2),
                'lastYearGrossProfit' => round($lastYearTotalGrossProfit,2),
                'totalPayment' => $paymentData['totalPayment'],
                'lastPeriodTotalPayment' => $lastYearPaymentData['totalPayment'],
                'totalInvoiceCount' => $invoiceData['invCount'],
                'totalInvoiceCountLastPeriod' => $lastYearInvoiceData['invCount'],
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'cashPaymentAmount' => round($cashPaymentAmount,2),
                'chequePaymentAmount' => round($chequePaymentAmount,2),
                'cardPaymentAmount' => round($cardPaymentAmount,2),
                'loyaltyPaymentAmount' => round($loyaltyPaymentAmount,2),
                'bankTransferPaymentAmount' => round($bankTransferPaymentAmount,2),
                'giftCardPaymentAmount' => round($giftCardPaymentAmount,2),
                'lcPaymentAmount' => round($lcPaymentAmount,2),
                'ttPaymentAmount' => round($ttPaymentAmount,2),
                'creditPaymentAmount' => round($creditPaymentAmount,2),
                'heigstSalesItemData' => $heigstSalesItemData,
                'customerSales' => $customerSales,
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
	}


	private function _getDailySalesItemsDetails($fromDate, $toDate, $locationID)
    {
        if (isset($fromDate) && isset($toDate)) {
            $dailySalesItemData = array();
            //because of mysql BETWEEN function
//            $toDate = date('Y-m-d H:i:s', strtotime($toDate . ' + 1 day'));

            $getInvoiceItemDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSalesInvoiceDetails($fromDate, $toDate,null,  $locationID);
            $getDlnWiseInvoiceItemDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getDlnWiseSalesInvoiceDetails($fromDate, $toDate, null, $locationID);

            $getItemDetails = array_merge($getInvoiceItemDetails, $getDlnWiseInvoiceItemDetails);
            if (!empty($getItemDetails)) {
                foreach ($getItemDetails as $row) {
                    $getLocationProduct = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProducts($row['itemOutLocationProductID'], $row['locationID']);

                    $productID = $getLocationProduct->productID;
                    $productName = $getLocationProduct->productName;
                    $productCode = $getLocationProduct->productCode;

                    if ($productID) {
                        //set quantity and uomAbbr according to display UOM
                        $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productID);
                        $thisqty = $this->getProductQuantityViaDisplayUom($row['itemOutQty'], $productUom);
                        $quantity = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                        $itemInUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemInPrice'], $productUom);
                        $itemOutUnitPrice = $this->getProductUnitPriceViaDisplayUom($row['itemOutPrice'], $productUom);

                        $itemOutAvergeCosting = $row['itemOutAverageCostingPrice']; 
                        $itemInAverageCosting = $row['itemInAverageCostingPrice'];

                        if ($row['itemOutDocumentType'] == "Sales Invoice") {
                            //get product credit note details
                            $creditNoteProductDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceProductCreditNoteDetails($productID, $fromDate, $toDate);
                            $creditNoteCostDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getCreditNoteCostByCreditNoteIDAndProductId($productID, $fromDate, $toDate);
                            $creditNoteProductQty = $creditNoteProductDetails['creditNoteProductQty'];
                            $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal'];
                            $creditNoteCost = $creditNoteCostDetails['creditNoteCost'];
                        } else {
                            //if any return made to dln in this date range
                            $creditNoteProductDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceProductSalesReturnDetails($productID, $fromDate, $toDate, $row['salesInvoiceID']);

                            if (!is_null($row['itemOutSerialID'])) {
                                $creditNoteProductQty = sqrt($creditNoteProductDetails['creditNoteProductQty']);
                                $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal']/$creditNoteProductQty;
                                $creditNoteCost = ($creditNoteProductDetails['creditNoteCost']/$creditNoteProductQty) * $creditNoteProductQty;
                            } else {
                                $creditNoteProductQty = $creditNoteProductDetails['creditNoteProductQty'];
                                $creditNoteProductTotal = $creditNoteProductDetails['creditNoteProductTotal'];
                                $creditNoteCost = $creditNoteProductDetails['creditNoteCost'] * $creditNoteProductQty;
                            }
                        }
                           
                        if ($row['itemOutDocumentType'] == "Delivery Note") {
                            $deliveryNoteID = $row['itemOutDocumentID'];
                            $getDeliveryNoteProductTax = $this->CommonTable("Invoice\Model\DeliveryNoteProductTaxTable")->getProductTax($deliveryNoteID, $productID);
                            $itemOutTaxAmount = 0;
                            if (!empty($getDeliveryNoteProductTax)) {
                                foreach ($getDeliveryNoteProductTax as $t) {
                                    $qty = ($t['deliveryNoteProductQuantity'] == NULL) ? 1 : $t['deliveryNoteProductQuantity'];
                                    $tax = $t['deliveryNoteTaxAmount'] / $qty;
                                    $itemOutTaxAmount += $tax;
                                }
                            }
                        } else {
                            $invoiceID = $row['itemOutDocumentID'];
                            $getInvoiceProductTax = $this->CommonTable("Invoice\Model\InvoiceProductTaxTable")->getProductTax($invoiceID, $productID);
                            $itemOutTaxAmount = 0;
                            if (!empty($getInvoiceProductTax)) {
                                foreach ($getInvoiceProductTax as $t) {
                                    $qty = ($t['salesInvoiceProductQuantity'] == NULL) ? 1 : $t['salesInvoiceProductQuantity'];
                                    $tax = $t['salesInvoiceProductTaxAmount'] / $qty;
                                    $itemOutTaxAmount += $tax;
                                }
                            }
                        }

                        $itemInTaxAmount = 0;
                        if ($row['itemInDocumentType'] == "Goods Received Note") {
                            $grnID = $row['itemInDocumentID'];
                            $getGrnTax = $this->CommonTable("Inventory\Model\GrnProductTaxTable")->getGrnProductTax($grnID, $row['itemInLocationProductID']);
                            foreach ($getGrnTax as $g) {
                                $grnQty = ($g['grnProductTotalQty'] == NULL) ? 1 : $g['grnProductTotalQty'];
                                $itemInTaxAmount += $g['grnTaxAmount'] / $grnQty;
                            }
                        } else if ($row['itemInDocumentType'] == "Payment Voucher") {
                        	$purchaseInvoiceID = $row['itemInDocumentID'];
                            $purchaseInvoiceTax = $this->CommonTable("Inventory\Model\PurchaseInvoiceProductTaxTable")->getPurchaseInvoiceProductTax($purchaseInvoiceID, $row['itemInLocationProductID']);
                            foreach ($purchaseInvoiceTax as $p) {
                                $purchaseInvoiceQty = ($p['purchaseInvoiceProductTotalQty'] == NULL) ? 1 : $p['purchaseInvoiceProductTotalQty'];
                                $itemInTaxAmount += $p['purchaseInvoiceTaxAmount'] / $purchaseInvoiceQty;
                            }
                        }

                        // check costing method
        				$setup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

        
        				if (!$setup['averageCostingFlag']) {
                        	$dailySalesItemData[$productID]['costingMethod'] = 'FIFO';
        				} else {
                        	$dailySalesItemData[$productID]['costingMethod'] = "Average";
        				}

                        $dailySalesItemData[$productID]['productName'] = $productName;
                        $dailySalesItemData[$productID]['productCode'] = $productCode;
                        $dailySalesItemData[$productID]['creditNoteProductTotal'] = $creditNoteProductTotal;
                        $dailySalesItemData[$productID]['creditNoteProductQty'] = $creditNoteProductQty;
                        $dailySalesItemData[$productID]['creditNoteCost'] = $creditNoteCost;
                        $dailySalesItemData[$productID]['data'][$row['itemOutID']] = array(
                            'itemOutQty' => $quantity,
                            'itemOutPrice' => $itemOutUnitPrice,
                            'itemOutAvergeCosting' => $itemOutAvergeCosting,
                            'itemOutDiscount' => $row['itemOutDiscount'],
                            'itemInPrice' => $itemInUnitPrice,
                            'itemInAverageCosting' => $itemInAverageCosting,
                            'itemInDiscount' => $row['itemInDiscount'],
                            'uomAbbr' => $thisqty['uomAbbr'],
                            'itemOutTaxAmount' => $itemOutTaxAmount,
                            'itemInTaxAmount' => $itemInTaxAmount
                        );

                    }
                }
            }

            $finalData = [];
            $netTotalCost = 0;
            $netTotalSales = 0;
            $isDataExist = false;
            foreach ($dailySalesItemData as $data) {
                $totalCost = 0;
                $totalSales = 0;
                $totalQty = 0;
                $uomAbbr = '';
                $itemCreditNoteQty = ($data['creditNoteProductQty']) ? $data['creditNoteProductQty'] : 0;
                $itemCreditNoteTotal = ($data['creditNoteProductTotal']) ? $data['creditNoteProductTotal'] : 0;
                $itemCreditNoteCost = ($data['creditNoteCost']) ? $data['creditNoteCost'] : 0;
                foreach ($data['data'] as $value) {
                    $qty = ($value['itemOutQty'] != NULL) ? $value['itemOutQty'] : 0;
                    $totalQty+= $qty;
                    //item in calculations
                    $itemInPrice = ($value['itemInPrice'] != NULL) ? $value['itemInPrice'] : 0;
                    $itemInDiscount = ($value['itemInDiscount'] != NULL) ? $value['itemInDiscount'] : 0;
                    $itemInTaxAmount = $value['itemInTaxAmount'];
                    $itemInActualPrice = ($itemInPrice + $itemInTaxAmount) - $itemInDiscount;
                    $totalCost+= $itemInActualPrice * $qty;
                    $netTotalCost += $itemInActualPrice * $qty;
                    //item out calculations
                    $itemOutPrice = ($value['itemOutPrice'] != NULL) ? $value['itemOutPrice'] : 0;
                    $itemOutDiscount = ($value['itemOutDiscount'] != NULL) ? $value['itemOutDiscount'] : 0;
                    $itemOutTaxAmount = $value['itemOutTaxAmount'];
                    $itemOutActualPrice = ($itemOutPrice + $itemOutTaxAmount) - $itemOutDiscount;
                    $totalSales+= $itemOutActualPrice * $qty;
                    $netTotalSales += $itemOutActualPrice * $qty;
                    $uomAbbr = $value['uomAbbr'];
                }
                //item credit note calculations
                $totalQty = $totalQty - $itemCreditNoteQty;
                $totalSales = $totalSales - $itemCreditNoteTotal;
                $netTotalSales = $netTotalSales - $itemCreditNoteTotal;
                $totalCost = $totalCost - $itemCreditNoteCost;
                $netTotalCost = $netTotalCost - $itemCreditNoteCost;
                $profit = $totalSales - $totalCost;

                $temp['productName'] = $data['productName'];
                $temp['productCode'] = $data['productCode'];
                $temp['totalQty'] = round($totalQty);
                $temp['totalSales'] = $totalSales;
                $temp['totalCost'] = $totalCost;
                $temp['profit'] = $profit;

                $finalData[] = $temp;
            }

            $totalSales = array_column($finalData, 'totalSales');

			array_multisort($totalSales, SORT_DESC, $finalData);

            return $finalData;
        }
    }



    public function getCustomerWiseSalesReportData($locations = null, $formDate = null, $endDate = null)
    {

        ini_set('memory_limit','-1');
        ini_set('max_execution_time','0');
        $cusData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerWiseSalesForSalesDashboard($locations, $formDate, $endDate);
        $cusCreditNoteData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerWiseCreditNoteDataForSalesDashboard($locations, $formDate, $endDate);
        $cusSalesReturnData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerWiseSalesReturnDataForSalesDashboard($locations, $formDate, $endDate);
        $cusDetails = array();
        $invoice = array();
        $product = array();
        $tmpCusData = [];
        $tmpInvProIds = [];

        foreach ($cusData as $c) {
            $tmpCusData[] = $c;
        }

        foreach ($cusCreditNoteData as $c) {
            $tmpCusData[] = $c;
        }

        foreach ($cusSalesReturnData as $c) {
            $tmpCusData[] = $c;
        }

        $creditNoteTotal = [];
        $creditNoteTotalDisc = [];
        $creditNoteValidation =[];
        $invoiceValidation = [];
        $invoiceTotalDisc = [];
        foreach ($tmpCusData as $c) {
            if ($c['salesInvoiceID']) {
                $cusDetails[$c['customerID']]['totalInvAmount'] +=  floatval($c['salesinvoiceTotalAmount']);
                $cusDetails[$c['customerID']]['customerName'] = $c['customerName'];
                $cusDetails[$c['customerID']]['customerCode'] = $c['customerCode'];
            }
            
            if ($c['creditNoteID']) {
                $cusDetails[$c['customerID']]['totalInvAmount'] -=  floatval($c['creditNoteTotal']);
                $cusDetails[$c['customerID']]['customerName'] = $c['customerName'];
                $cusDetails[$c['customerID']]['customerCode'] = $c['customerCode'];                
            }

            if ($c['salesReturnID']) {
                $cusDetails[$c['customerID']]['totalReturnAmount'] +=  floatval($c['salesReturnTotal']);
                $cusDetails[$c['customerID']]['customerName'] = $c['customerName'];
                $cusDetails[$c['customerID']]['customerCode'] = $c['customerCode'];                
            }
        }

        $totalSales = array_column($cusDetails, 'totalInvAmount');

		array_multisort($totalSales, SORT_DESC, $cusDetails);

        return $cusDetails;
    }

    public function getHeaderWidgetDataAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            $lastYearInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationID($fromDate,$toDate,$locationID);


            $lastYearCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);

            $creditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationID($fromDate,$toDate,$locationID);

            $totalSales = floatval($invoiceData['invTotal']) - floatval($creditnoteData['total']);
            $totalSalesLastYear = floatval($lastYearInvoiceData['invTotal']) - floatval($lastYearCreditnoteData['total']);

            $totalSalesProfitLoss = (($totalSales - $totalSalesLastYear) / $totalSales) * 100;

            $invoiceCountProfitLoss = (($invoiceData['invCount'] - $lastYearInvoiceData['invCount']) / $invoiceData['invCount']) * 100;

            $grossProfitData = $this->getService('SalesReportService')->_getGrossProfitDataForSalesDashboard([$locationID], $fromDate, $toDate);
            
            $lastYearGrossProfitData = $this->getService('SalesReportService')->_getGrossProfitDataForSalesDashboard([$locationID], $lastPeriodFromDate, $lastPeriodToDate);

            $totalGrossProfit = 0;
            $totalGValue = 0;
            $totalCost = 0;
            // foreach ($grossProfitData as $key => $d) {
            foreach ($grossProfitData as $v) {
                $nonInventoryItemTotal = ($v['nonInventoryItemTotal']) ? $v['nonInventoryItemTotal'] : 0;
                $value = $v['documentValue'] ? $v['documentValue'] : 0;
                $cost = $v['documentCost'] ? $v['documentCost'] : 0;
                $totalTax = $v['totalTax'] ? $v['totalTax'] : 0;
                if ($v['documentType'] == 'Sales Invoice') {
                    $value = $value + $nonInventoryItemTotal;
                    $grossProfit = $value - ($cost);
                    $totalGValue+=$value;
                    $totalCost+=($cost);
                } else if ($v['documentType'] == 'Credit Note') {
                    $value = $value - $totalTax;
                    $grossProfit = ($cost) - $value;
                    $totalGValue-=$value;
                    $totalCost-=($cost);
                }                                    
                $totalGrossProfit+=$grossProfit;      
            }
            // }


            $lastYearTotalGrossProfit = 0;
            // foreach ($lastYearGrossProfitData as $key => $d) {
            foreach ($lastYearGrossProfitData as $v) {
                $nonInventoryItemTotal = ($v['nonInventoryItemTotal']) ? $v['nonInventoryItemTotal'] : 0;
                $value = $v['documentValue'] ? $v['documentValue'] : 0;
                $cost = $v['documentCost'] ? $v['documentCost'] : 0;
                $totalTax = $v['totalTax'] ? $v['totalTax'] : 0;
                if ($v['documentType'] == 'Sales Invoice') {
                    $value = $value + $nonInventoryItemTotal;
                    $grossProfit = $value - ($cost);
                    // $totalGValue+=$value;
                } else if ($v['documentType'] == 'Credit Note') {
                    $value = $value - $totalTax;
                    $grossProfit = ($cost) - $value;
                    // $totalGValue-=$value;
                }                                    
                $lastYearTotalGrossProfit+=$grossProfit;      
            }
            // }

            $gpPercentage = ($totalGrossProfit / $totalGValue) * 100;

            $totalGrossProfitProfitLoss = (($totalGrossProfit - $lastYearTotalGrossProfit) / $totalGrossProfit) * 100;

            $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalPaymentsByDateRangeAndLocationID($fromDate,$toDate,$locationID);
            $paymentPosData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalPaymentsByDateRangeAndLocationID($fromDate,$toDate,$locationID, true);

            $lastYearPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalPaymentsByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID);
            $lastYearPaymentPosData = $this->CommonTable('Invoice\Model\PaymentsTable')->getTotalPaymentsByDateRangeAndLocationID($lastPeriodFromDate,$lastPeriodToDate,$locationID, true);

            $totalPaymentProfitLoss = ((($paymentData['totalPayment'] + $paymentPosData['totalPayment']) - ($lastYearPaymentData['totalPayment'] + $lastYearPaymentPosData['totalPayment']) ) / $paymentData['totalPayment']) * 100;
            
            header('Content-Type: application/json');
            $widgetData = array(
                'totalSales' => round($totalSales,2),
                'totalGrossValue' => round($totalGValue,2),
                'totalCost' => round($totalCost,2),
                'totalSalesLastYear' => round($totalSalesLastYear,2),
                'totalSalesProfitLoss' => round($totalSalesProfitLoss,2),
                'totalGrossProfitProfitLoss' => round($totalGrossProfitProfitLoss,2),
                'totalPaymentProfitLoss' => round($totalPaymentProfitLoss,2),
                'invoiceCountProfitLoss' => round($invoiceCountProfitLoss,2),
                'gpPercentage' => round($gpPercentage),
                'grossProfit' => round($totalGrossProfit,2),
                'lastYearGrossProfit' => round($lastYearTotalGrossProfit,2),
                'totalPayment' => $paymentData['totalPayment'] + $paymentPosData['totalPayment'],
                'lastPeriodTotalPayment' => $lastYearPaymentData['totalPayment']+ $lastYearPaymentPosData['totalPayment'],
                'totalInvoiceCount' => $invoiceData['invCount'],
                'totalInvoiceCountLastPeriod' => $lastYearInvoiceData['invCount'],
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }
    public function updateFooterChartDataAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            //get open invoices
            $openInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationIDAndStatus($fromDate,$toDate,$locationID, 3);

            $openInvoiceIDs = [];
            $openInvoiceTotal = 0;
            foreach ($openInvoiceData as $value) {
                $openInvoiceIDs[] = $value['salesInvoiceID'];
                $openInvoiceTotal += (floatval($value['invAmount']) - floatval($value['invPayedAmountAmount']));
            }

            if (!empty($openInvoiceIDs)) {
                $openInvoiceCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromDate,$toDate,$locationID, $openInvoiceIDs);
            }

            $openInvoiceTotal = $openInvoiceTotal - floatval($openInvoiceCreditnoteData['total']);

            //overdueInvoice
            //get open invoices
            $overdueInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationIDAndStatus($fromDate,$toDate,$locationID, 6);

            $overdueInvoiceIDs = [];
            $overdueInvoiceTotal = 0;
            foreach ($overdueInvoiceData as $value) {
                $overdueInvoiceIDs[] = $value['salesInvoiceID'];
                $overdueInvoiceTotal += (floatval($value['invAmount']) - floatval($value['invPayedAmountAmount']));
            }

            if (!empty($overdueInvoiceIDs)) {
                $overdueInvoiceCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromDate,$toDate,$locationID, $overdueInvoiceIDs);
            }

            $overdueInvoiceTotal = $overdueInvoiceTotal - floatval($overdueInvoiceCreditnoteData['total']);                    

            // closed invoice total
            $closedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getTotalSalesByDateRangeAndLocationIDAndStatus($fromDate,$toDate,$locationID, 4);

            $closedInvoiceIDs = [];
            $closedInvoiceTotal = 0;
            foreach ($closedInvoiceData as $value) {
                $closedInvoiceIDs[] = $value['salesInvoiceID'];
                $closedInvoiceTotal += floatval($value['invAmount']);
            }
            if (!empty($closedInvoiceIDs)) {
                $closedInvoiceCreditnoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteTotalByDateRangeAndLocationIDAndInvoiceIDs($fromDate,$toDate,$locationID, $closedInvoiceIDs);
            }

            $closedInvoiceTotal = $closedInvoiceTotal - floatval($closedInvoiceCreditnoteData['total']); 

            //payment data with payment method
            $paymentDataWithMethods = $this->CommonTable('Invoice\Model\PaymentsTable')->getPaymentsSummeryDataForSalesDashboard($fromDate,$toDate,$locationID);
            $cashPaymentAmount = 0;
            $chequePaymentAmount = 0;
            $cardPaymentAmount = 0;
            $loyaltyPaymentAmount = 0;
            $bankTransferPaymentAmount = 0;
            $giftCardPaymentAmount = 0;
            $lcPaymentAmount = 0;
            $ttPaymentAmount = 0;
            $creditPaymentAmount = 0;
            $creditArray = [];
            foreach ($paymentDataWithMethods as $res) {
                if ($res['incomingPaymentMethodCashId'] != 0) {
                    $cashPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if ($res['incomingPaymentMethodChequeId'] != 0) {
                    $chequePaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if ($res['incomingPaymentMethodCreditCardId'] != 0) {
                    $cardPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if (isset($res['incomingPaymentMethodLoyaltyCardId']) && is_null($res['incomingPaymentMethodCashId']) && is_null($res['incomingPaymentMethodChequeId']) && is_null($res['incomingPaymentMethodCreditCardId']) && is_null($res['incomingPaymentMethodBankTransferId']) && is_null($res['incomingPaymentMethodGiftCardId'])) {
                    $loyaltyPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if ($res['incomingPaymentMethodBankTransferId'] != 0) {
                    $bankTransferPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if ($res['incomingPaymentMethodGiftCardId'] != 0) {
                    $giftCardPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if (isset($res['incomingPaymentMethodLCId']) && $res['incomingPaymentMethodLCId'] != "0") {
                    $lcPaymentAmount += $res['incomingPaymentMethodAmount'];
                } else if (isset($res['incomingPaymentMethodTTId']) && $res['incomingPaymentMethodTTId'] != "0") {
                    $ttPaymentAmount += $res['incomingPaymentMethodAmount'];
                }

                if (!is_null($res['incomingPaymentCreditAmount'])) {
                    if (!array_key_exists($res['incomingPaymentID'],$creditArray)) {
                        $creditPaymentAmount += $res['incomingPaymentCreditAmount'];
                        $creditArray[$res['incomingPaymentID']] = $res;
                    }
                }
            }
 
            header('Content-Type: application/json');
            $widgetData = array(
                'openInvoiceTotal' => round($openInvoiceTotal,2),
                'overdueInvoiceTotal' => round($overdueInvoiceTotal,2),
                'closedInvoiceTotal' => round($closedInvoiceTotal,2),
                'cashPaymentAmount' => round($cashPaymentAmount,2),
                'chequePaymentAmount' => round($chequePaymentAmount,2),
                'cardPaymentAmount' => round($cardPaymentAmount,2),
                'loyaltyPaymentAmount' => round($loyaltyPaymentAmount,2),
                'bankTransferPaymentAmount' => round($bankTransferPaymentAmount,2),
                'giftCardPaymentAmount' => round($giftCardPaymentAmount,2),
                'lcPaymentAmount' => round($lcPaymentAmount,2),
                'ttPaymentAmount' => round($ttPaymentAmount,2),
                'creditPaymentAmount' => round($creditPaymentAmount,2),
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
        
    }

    public function getBaseChartDataAction() {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $currentDate = date('Y-m-d');
            $period = $request->getPost('period');
            $monthFirstDate = date('Y-m-01');
            $yearFirstDate = date('Y-01-01');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            switch ($period) {
                case 'thisYear':
                    $lastPeriodFromDate = strtotime("-1 year", strtotime($yearFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 year", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $yearFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisMonth':
                    $lastPeriodFromDate = strtotime("-1 months", strtotime($monthFirstDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 months", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $monthFirstDate;
                    $toDate = $currentDate;
                    break;
                case 'thisWeek':
                    $lastPeriodFromDate = strtotime("-1 week", strtotime(date("Y-m-d", strtotime('monday this week'))));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 week", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = date("Y-m-d", strtotime('monday this week'));
                    $toDate = $currentDate;
                    break;
                case 'thisDay':
                    $lastPeriodFromDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodFromDate = date("Y-m-d", $lastPeriodFromDate);

                    $lastPeriodToDate = strtotime("-1 day", strtotime($currentDate));
                    $lastPeriodToDate = date("Y-m-d", $lastPeriodToDate);

                    $fromDate = $currentDate;
                    $toDate = $currentDate;
                    break;
                default:
                    break;
            }

            //high sale items
            $heigstSalesItemData = $this->_getDailySalesItemsDetails($fromDate,$toDate,$locationID);
            $customerSales = $this->getCustomerWiseSalesReportData([$locationID],$fromDate,$toDate);
            
            header('Content-Type: application/json');
            $widgetData = array(
                'heigstSalesItemData' => $heigstSalesItemData,
                'customerSales' => $customerSales,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            );
            echo json_encode($widgetData);
            exit();
        } else {
            exit();
        }
    }
}