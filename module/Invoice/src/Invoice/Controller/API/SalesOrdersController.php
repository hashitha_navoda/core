<?php

/**
 * @author Prathap <prathap@thinkcube.com>
 * This file contains SalesOrdersAPI related controller functions
 */

namespace Invoice\Controller\API;

use Zend\Mvc\Controller\AbstractRestfulController;
use Zend\View\Model\JsonModel;
use Invoice\Model\SalesOrder;
use Invoice\Model\SalesOrderProduct;
use Invoice\Form\RecurrentSalesOrderForm;
use Invoice\Model\RecurrentInvoice;
use Invoice\Form\SalesOrdersForm;
use Invoice\Model\Reference;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Core\Controller\CoreController;
use Invoice\Model\SalesOrderProductTax;
use Core\Model\DocumentReference;

class SalesOrdersController extends CoreController
{

    protected $userID;
    protected $cdnUrl;
    protected $user_session;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->cdnUrl = $this->user_session->cdnUrl;
    }

    public function createAction()
    {

        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->paymentTermID;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->paymentTermName;
        }


        $so_form = new SalesOrdersForm(array(
            'name' => 'sales_orders_form',
            'id' => 'so_no',
            'terms' => $terms
        ));
        $so_receipt_form = new SalesOrdersForm(array(
            'name' => 'sales_orders_form',
            'id' => 'so_no',
            'terms' => $terms
        ));
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $documentReference = $post['documentReference'];
            if (isset($post['documentReference'])) {
                unset($post['documentReference']);
            }
            $sales_order = new SalesOrder();
            $so_form->setInputFilter($sales_order->getInputFilter());
            $so_form->setData($post);
            $valid_items = TRUE;
            $locationID = $this->user_session->userActiveLocation["locationID"];
            $result = $this->getReferenceNoForLocation('2', $locationID);
            $locationReferenceID = $result['locRefID'];
            // check if a salesOrder from the same salesOrder Code exists if exist add next number to sales order code
            $salesOrderCode = $request->getPost('so_no');

            while ($salesOrderCode) {
                if ($this->CommonTable('Invoice\Model\SalesOrderTable')->checkSalesOrderByCode($salesOrderCode)->current()) {
                    if ($locationReferenceID) {
                        $newSalesOrderCode = $this->getReferenceNumber($locationReferenceID);
                        if ($newSalesOrderCode == $salesOrderCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $salesOrderCode = $this->getReferenceNumber($locationReferenceID);
                        } else {
                            $salesOrderCode = $newSalesOrderCode;
                        }
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_SALESORDER_EXIST');
                        return $this->JSONRespond();
                    }
                } else {
                    break;
                }
            }

            $customCurrencyId = $post['customCurrencyId'];

            //get customCurrency data by customer CurrencyId
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            $customCurrencyRate = ($customCurrencyId == "" || $customCurrencyId == 0) ? 1 : $customCurrencyData->currencyRate;
            $customCurrencySymbol = ($customCurrencyId == "" || $customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->currencySymbol;

            //if custom currency rate alredy set add that rate to sales Order
            if ($request->getPost('quotationID') && !($post['customCurrencyRate'] == 0 || $post['customCurrencyRate'] == 1)) {
                $customCurrencyRate = $post['customCurrencyRate'];
            }

            //converting date to the standard date format y-m-d
            $issDate = $this->convertDateToStandardFormat($request->getPost('issue_date'));
            $completionDate = $this->convertDateToStandardFormat($request->getPost('completion_date'));

            $entityID = $this->createEntity();
            $salesOrderExchange = array(
                'soCode' => $salesOrderCode,
                'customerName' => $request->getPost('cust_name'),
                'customerID' => $request->getPost('cust_id'),
                'currentBalance' => $request->getPost('currentBalance'),
                'issuedDate' => $issDate,
                'expireDate' => $completionDate,
                'payementTerm' => $request->getPost('payment_term'),
                'taxAmount' => '',
                'totalDiscount' => $request->getPost('total_discount') * $customCurrencyRate,
                'totalAmount' => $request->getPost('total_amount') * $customCurrencyRate,
                'comment' => $request->getPost('cmnt'),
                'state' => $request->getPost('state'),
                'quotationID' => $request->getPost('quotationID'),
                'showTax' => $request->getPost('show_tax'),
                'footer_id' => $footer["templateID"],
                'branch' => $locationID,
                'entityID' => $entityID,
                'salesPersonID' => $request->getPost('salesPersonID'),
                'customCurrencyId' => $customCurrencyId,
                'salesOrdersCustomCurrencyRate' => $customCurrencyRate,
                'priceListId' => $request->getPost('priceListId'),
                'customerProfileID' => $request->getPost('cust_prof_id'),
            );

            //check whether SalesOrder is created via a Quatation
            if ($post['quotationID'] != '') {

                $quatationData = $this->CommonTable('Invoice\Model\QuotationTable')->getQuotaionByID($post['quotationID']);
                $quatationID = $quatationData->quotationID;
                $fromQuatation = TRUE;
            } else {
                $fromQuatation = FALSE;
            }

            $sales_order->exchangeArray($salesOrderExchange);
            $retQ = $this->CommonTable('Invoice\Model\SalesOrderTable')->saveSalesOrders($sales_order);


            if (count($documentReference) > 0) {
                foreach ($documentReference as $key => $val) {
                    foreach ($val as $key1 => $value) {
                        $data = array(
                            'sourceDocumentTypeId' => 3,
                            'sourceDocumentId' => $retQ,
                            'referenceDocumentTypeId' => $key,
                            'referenceDocumentId' => $value,
                        );
                        $documentReference = new DocumentReference();
                        $documentReference->exchangeArray($data);
                        $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                    }
                }
            }

            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }

            $itemTaxes = json_decode($post['itemTaxes']);
            $saveProductQuantity = array();
            foreach ($post['items'] as $ke => $row) {

                $s_receipt = new SalesOrderProduct();
                $so_receipt_form->setInputFilter($s_receipt->getInputFilter());

                $product = $this->CommonTable('Inventory/Model/ProductTable')->getProductByCode($row['item_code']);
                $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($product['productID'], $this->user_session->userActiveLocation["locationID"]);
                $locationPID = $locationProduct->locationProductID;

                if ($row['discountType'] == 'vl') {
                    $row['discount'] = $row['discount'] * $customCurrencyRate;
                }
                $saveProductQuantity[$locationPID] = $row['quantity'];
                $sendrow = array(
                    'locationProductID' => $locationPID,
                    'productName' => $row['item'],
                    'soProductDescription' => $row['item_discription'],
                    'quantity' => $row['quantity'],
                    'unitPrice' => $row['unit_price'] * $customCurrencyRate,
                    'uom' => $row['uom'],
                    'discount' => $row['discount'],
                    'discountType' => $row['discountType'],
//                    'soCode' => $salesOrderExchange['soCode'],
                    'soID' => $retQ,
                    'total' => $row['total_cost'] * $customCurrencyRate,
                    'tax' => $itemTaxes[$ke]
                );

                $s_receipt->exchangeArray($sendrow);
                $soProductID = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->saveReceipt($s_receipt);
                //if copied from Quotation,update those products as copied.
                if ($fromQuatation == TRUE) {
                    $quotationProducts = $this->CommonTable('Invoice\Model\QuotationProductTable')->getQuotationProductByQuotationIdAndLocationProductId($quatationID, $locationPID);
                    if (isset($quotationProducts)) {
                        $newCopiedQuantity = $quotationProducts->quotationProductCopiedQuantity + (float) $row['quantity'];
                        $copiedflag = 0;
                        if ($newCopiedQuantity >= $quotationProducts->quotationProductQuantity) {
                            $newCopiedQuantity = $quotationProducts->quotationProductQuantity;
                            $copiedflag = 1;
                        }
                        $quoProData = array(
                            'quotationProductCopied' => $copiedflag,
                            'quotationProductCopiedQuantity' => $newCopiedQuantity,
                        );
                        $this->CommonTable('Invoice\Model\QuotationProductTable')->updateCopiedQuotationProducts($locationPID, $post['quotationID'], $quoProData);
                    }
                }
                $item = $row;
                if (isset($item["taxDetails"]["tL"])) {
                    foreach ($item["taxDetails"]["tL"] as $txID => $txd) {
                        $taxData = array(
                            'soID' => $retQ,
                            'soProductID' => $soProductID,
                            'soTaxID' => $txID,
                            'soTaxPrecentage' => $txd["tP"],
                            'soTaxAmount' => $txd["tA"] * $customCurrencyRate
                        );

                        $soProTax = new SalesOrderProductTax();
                        $soProTax->exchangeArray($taxData);
                        $this->CommonTable('Invoice\Model\SalesOrderProductTaxTable')->saveSoProductTax($soProTax);
                    }
                }
            }

            if ($request->getPost('recsoID') != '') {
                $sinv = $request->getPost('recsoID');
                $ReInvoice = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByID($sinv);
                $next_date = date('Y-m-d', strtotime($ReInvoice->nextDate . ' + ' . $ReInvoice->timeValue . ' ' . $ReInvoice->timeFormat));
                if ($next_date < $ReInvoice->endDate) {
                    $RecurrentInvoice = new RecurrentInvoice();
                    $data = array(
                        'id' => $sinv,
                        'nextDate' => $next_date,
                        'state' => 'Active',
                    );
                    $RecurrentInvoice->exchangeArray($data);
                    $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->updateRecurrentInvoiceNextdate($RecurrentInvoice);
                } else {
                    $RecurrentInvoice = new RecurrentInvoice();
                    $data = array(
                        'id' => $sinv,
                        'nextDate' => $ReInvoice->nextDate,
                        'state' => 'Close',
                    );
                    $RecurrentInvoice->exchangeArray($data);
                    $ReInvoice = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->updateRecurrentInvoiceNextdate($RecurrentInvoice);
                }
            }

            //update Quotation status if copied from it
            if ($fromQuatation == TRUE) {
                $this->_checkAndUpdateQuotationStatus($post['quotationID']);
            }
            $this->status = true;
//            $this->data = $salesOrderExchange['soCode'];
            $this->data = array('soID' => $retQ, 'soCode' => $salesOrderExchange['soCode']);
            $this->msg = $this->getMessage('SUCC_SALESORDER_CREATE', array($salesOrderCode));
            $this->setLogMessage($customCurrencySymbol . $row['total_cost'] . ' amount sales order created successfully - ' . $salesOrderCode);
            return $this->JSONRespond();
        }
        $this->status = false;
        $this->msg = $this->getMessage('ERR_SALESORDER_CREATE');
        return $this->JSONRespond();
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $quotationID
     * If all quotation products are used to create Sales Order,after that changed Status of Quotation
     */
    private function _checkAndUpdateQuotationStatus($quotationID)
    {
        $quotationProducts = $this->CommonTable('Invoice\Model\QuotationProductTable')->getUnCopiedQuotationProducts($quotationID);
        if (empty($quotationProducts)) {
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Invoice\Model\QuotationTable')->updateQuotationStatus($quotationID, $closeStatusID);
        }
    }

    public function getSalesOrderFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $salesOrderID = $searchrequest->getPost('salesOrderID');
            $salesOrder = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderforSearch($salesOrderID);
            $dateFormat = $this->getUserDateFormat(); //getting user selected date format

            if ($salesOrder->count() == 0) {
                return new JsonModel(array(FALSE));
            } else {
                while ($t = $salesOrder->current()) {
                    $data[$t['soID']] = (object) $t;
                }

                $SalesOrders = new ViewModel(
                        array('salesOrders' => $data,
                    'statuses' => $this->getStatusesList(),
                    'userdateFormat' => $dateFormat,
                    'companyCurrencySymbol' => $this->companyCurrencySymbol));
                $SalesOrders->setTerminal(TRUE);
                $SalesOrders->setTemplate('invoice/sales-orders-api/salesorder-list');
                return $SalesOrders;
            }
        }
    }

    public function retriveCustomerSalesOrdersAction()
    {
        $er = array();
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($request->isPost()) {
            $cust_id = $request->getpost('customerID');
            $salesorders = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByCustomerID($cust_id);

            while ($t = $salesorders->current()) {
                $data[$t['soID']] = (object) $t;
            }
            $CustomerSalesOrders = new ViewModel(
                    array('salesOrders' => $data,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol));
            $CustomerSalesOrders->setTerminal(TRUE);
            $CustomerSalesOrders->setTemplate('invoice/sales-orders-api/salesorder-list');
            return $CustomerSalesOrders;
        }
    }

    public function getSalesOrdersByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        if ($invrequest->isPost()) {
            $location = $this->user_session->userActiveLocation["locationID"];
            $fromdate = $this->convertDateToStandardFormat($invrequest->getPost('fromdate'));
            $todate = $this->convertDateToStandardFormat($invrequest->getPost('todate'));
            $customerName = $invrequest->getPost('customerID');
            $filteredSalesOrders = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByDate($fromdate, $todate, $location, $customerName);
            $dateFormat = $this->getUserDateFormat();

            while ($t = $filteredSalesOrders->current()) {
                $data[$t['soID']] = (object) $t;
            }
            $DateFilteredSalesOrders = new ViewModel(
                    array('salesOrders' => $data,
                'statuses' => $this->getStatusesList(),
                'userdateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $DateFilteredSalesOrders->setTerminal(TRUE);
            $DateFilteredSalesOrders->setTemplate('invoice/sales-orders-api/salesorder-list');
            return $DateFilteredSalesOrders;
        }
    }

    public function getRecurrentSalesOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $ID = $request->getpost('id');
            $ReInvoice = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByID($ID);
            $salesOrder = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($ID);
            if ($ReInvoice != NULL) {
                return new JsonModel(array("result" => $ReInvoice));
            } else {
                return new JsonModel(array(
                    "result" => "null",
                    "salesOrder" => $salesOrder
                ));
            }
        }
    }

    public function saveRecurrentSalesOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form = new RecurrentSalesOrderForm();
            $RecurrentInvoice = new RecurrentInvoice();
            $form->setInputFilter($RecurrentInvoice->getInputFilter());
            $post = $request->getPost();
            $next_date = date('Y-m-d', strtotime($post["startDate"] . ' + ' . $post['timeValue'] . ' ' . $post['timeFormat']));
            if ($post["endDate"] != null) {
                if ($next_date <= $post["endDate"]) {
                    $post['nextDate'] = $next_date;
                    $form->setData($post);
                    if ($form->isValid()) {
                        $RecurrentInvoice->exchangeArray($post);
                        $res = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->saveRecurrentInvoice($RecurrentInvoice);
                        if ($res) {
                            $this->setLogMessage("Issued recurrent Sales order.");
                            return new JsonModel(array("result" => true));
                        }
                    } else {
                        return new JsonModel($form->getMessages());
                    }
                } else {
                    $post['nextDate'] = NULL;
                    return new JsonModel(array("result" => false));
                }
            } else {
                $post['nextDate'] = $next_date;
                $form->setData($post);
                if ($form->isValid()) {
                    $RecurrentInvoice->exchangeArray($post);
                    $res = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->saveRecurrentInvoice($RecurrentInvoice);
                    if ($res) {
                        $this->setLogMessage("Issued recurrent Sales Order.");
                    }
                    return new JsonModel(array("result" => true));
                } else {
                    return new JsonModel(array("result" => "invalid", "msg" => $form->getMessages()));
                }
            }
        }
    }

    public function recurrentSalesOrderUpdateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByID($request->getPost('id'));
            if ($data) {
                $form = new RecurrentSalesOrderForm();
                $RecurrentInvoice = new RecurrentInvoice();
                $form->setInputFilter($RecurrentInvoice->getInputFilter());
                $post = $request->getPost();
                $next_date = date('Y-m-d', strtotime($post["startDate"] . ' + ' . $post['timeValue'] . ' ' . $post['timeFormat']));
                if ($post["endDate"] != null) {
                    if ($next_date < $post["endDate"])
                        $post['nextDate'] = $next_date;
                    else
                        $post['nextDate'] = NULL;
                    $form->setData($post);
                    if ($form->isValid()) {
                        $RecurrentInvoice->exchangeArray($post);
                        $res = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->updateRecurrentInvoice($RecurrentInvoice);
                        if ($res) {
                            $this->setLogMessage("Update recurrent Sales Order.");
                        }
                    } else {
                        return new JsonModel(array("result" => false));
                    }
                    return new JsonModel(array("result" => true));
                } else {
                    $post['nextDate'] = $next_date;
                    $form->setData($post);
                    if ($form->isValid()) {
                        $RecurrentInvoice->exchangeArray($post);
                        $res = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->updateRecurrentInvoice($RecurrentInvoice);
                        if ($res) {
                            $this->setLogMessage("Update recurrent Sales Order.");
                        }
                        return new JsonModel(array("result" => true));
                    } else {
                        return new JsonModel(array("result" => false));
                    }
                }
            } else {
                return new JsonModel(array("result" => false));
            }
        }
        return new JsonModel(array("result" => false));
    }

    public function getSalesOrderAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll();
        while ($row = $re->current()) {
            $a[] = $row;
        }
        return new JsonModel($a);
    }

    public function getCustomerAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        while ($row = $re->current()) {
            $a[] = $row;
        }
        return new JsonModel($a);
    }

    public function retriveCustomerSalesOrderAction()
    {
        $er = array();
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
            $custa = trim($request->getPost('customerShortName'));

            $value = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByShortname($custa);


            if (isset($value->customerID)) {
                $cust_id = $value->customerID;
            } else {
                return new JsonModel(array(false));
            }
            $salesorder = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByCustomerID($cust_id);

            if ($salesorder->count() == 0) {
                $er[0] = 'nosalesorders';
                return new JsonModel($er);
            } else {
                $salesorderview = new ViewModel(array(
                    'salesOrders' => $salesorder,
                    'userdateFormat' => $dateFormat
                ));
                $salesorderview->setTemplate('invoice/sales-orders-api/salesorder-list');
                $salesorderview->setTerminal(TRUE);
                return $salesorderview;
            }
        }
    }

//   In SalesOrderProduct soCode removed and added soID
//    In salesorders quotCode changed to quotationID
    public function getSalesOrderWithItemsAction()
    {
        $getinvrequest = $this->getRequest();
        $in = array();

        if ($getinvrequest->isPost()) {
            $soID = $getinvrequest->getpost('sono');
            $ReSalesOrder = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($soID);
            if ($ReSalesOrder != NULL) {
                $productsBySo = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderID($soID);
                $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($ReSalesOrder->customerID);
                $curBal = 0;
                if ($cust != NULL) {
                    $curBal = $cust->currentBalance;
                    $cusAdd = $cust->address;
                    $custp = $cust->telephoneNumber;
                    $cusmail = $cust->email;
                } else {
                    $curBal = 0;
                    $cusAdd = NULL;
                    $custp = NULL;
                    $cusmail = NULL;
                }

                $companyres = $this->CommonTable('CompanyTable')->fetchAll();
                $com = NULL;
                if ($companyres != NULL) {
                    $com = $companyres->current();
                }

                $payterm = $this->CommonTable('Core\Model\PaymentTermTable')->getPaymentterm($ReSalesOrder->payementTerm);
                if ($payterm != NULL) {
                    $paytermname = $payterm->term;
                } else {
                    $paytermname = 'Not specified';
                }
                $data = array();
                $pdata = array();
                $data['SalesOrderNo'] = $ReSalesOrder->sales_order_no;
                $data['customerName'] = $ReSalesOrder->customerName;
                $data['customerID'] = $ReSalesOrder->customerID;
                $data['customerAddress'] = ($cusAdd != NULL) ? $cusAdd : NULL;
                $data['customertelephone'] = ($custp != NULL) ? $custp : NULL;
                $data['customerEmail'] = ($cusmail != NULL) ? $cusmail : NULL;
                $data['companyName'] = ($com != NULL) ? $com->companyName : NULL;
                $data['companyAddress'] = ($com != NULL) ? $com->companyAddress : NULL;
                $data['companyTelephone'] = ($com != NULL) ? $com->telephoneNumber : NULL;
                $data['companyEmail'] = ($com != NULL) ? $com->email : NULL;
                $data['companyReg'] = ($com != NULL) ? $com->trNumber : NULL;
                $data['issuedDate'] = $ReSalesOrder->issuedDate;
                $data['overDueDate'] = $ReSalesOrder->expireDate;
                $data['quotationId'] = $ReSalesOrder->quotationID;
                $data['payementTerm'] = $paytermname;
                $data['taxAmount'] = $ReSalesOrder->taxAmount;
                $data['totalDiscount'] = $ReSalesOrder->totalDiscount;
                $data['totalAmount'] = $ReSalesOrder->totalAmount;
                $data['comment'] = $ReSalesOrder->comment;
                $data['quotationID'] = $ReSalesOrder->quotationID;
                $data['currentBalance'] = $curBal;

                while ($p = $productsBySo->current()) {
                    $pdata[] = $p;
                }

                $data['product'] = $pdata;
                return new JsonModel($data);
            } else {
                $in[0] = 'wronginvno';
                return new JsonModel($in);
            }
        }
    }

    public function retriveSalesOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $in = array();
            $salesOrderID = $request->getPost('salesOrderID');
            $value = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($salesOrderID);
            $cusProfDetails = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerProfileID($value->customerProfileID)->current();
            $pvalue = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderID($value->soID);
            $locationID = $this->user_session->userActiveLocation["locationID"];
            if ($value != NULL) {
                $ccbalance = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($value->customerID);
                $curBal = 0;
                if ($ccbalance != NULL) {
                    $curBal = $ccbalance->customerCurrentBalance;
                    $curCre = $ccbalance->customerCurrentCredit;
                } else {
                    $curBal = 0;
                    $curCre = 0;
                }

                $docrefData = $this->getDocumentReferenceBySourceData(3, $salesOrderID);
                $data = array();
                $pdata = array();
                $csymbol = $this->companyCurrencySymbol;
                if(isset($value->customCurrencyId) && $value->customCurrencyId!=0 ){
                    $customCurrency = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($value->customCurrencyId);
                    $csymbol=$customCurrency->currencySymbol;
                }
                $data['customCurrencyId'] = $value->customCurrencyId;
                $data['companyCurrencySymbol'] =$csymbol;
                $data['salesOrdersCustomCurrencyRate'] = $value->salesOrdersCustomCurrencyRate;
                $data['salesOrder_no'] = $value->soCode;
                $data['customer_name'] = $value->customerName;
                $data['customer_prof_name'] = $cusProfDetails['customerProfileName'];
                $data['customer_id'] = $value->customerID;
                $data['issue_date'] = $value->issuedDate;
                $data['expire_date'] = $value->expireDate;
                $data['salesPersonID'] = $value->salesPersonID;
                $data['payment_term'] = $value->payementTerm;
                $data['tax'] = $value->taxAmount;
                $data['total_discount'] = $value->totalDiscount;
                $data['total'] = $value->totalAmount;
                $data['comment'] = $value->comment;
                $data['branch'] = $value->branch;
                $data['showTax'] = $value->showTax;
                $data['priceListId'] = $value->priceListId;
                $data['current_balance'] = $curBal;
                $data['current_credit'] = $curCre;
                $data['docRefData'] = $docrefData;

                foreach ($pvalue as $t) {
                    $pdata[] = $t;
                    $ptax = $this->CommonTable('Invoice\Model\SalesOrderProductTaxTable')->getSalesOrderProductTax($t['soProductID']);

                    $Taxes = '';
                    while ($taxs = $ptax->current()) {

                        $tax = (object) $taxs;
                        $Taxes[$tax->soTaxID] = $tax;
                    }
                    if ($Taxes) {
                        $productTax[$t['soProductID']] = $Taxes;
                    }
                    $locationProducts[$t['productID']] = $this->getLocationProductDetails($locationID, $t['productID']);
                }

                $locProducts = Array();
                $inactiveItems = Array();
                foreach ($pdata as $pro) {
                    $locProducts[$pro['productID']] = $this->getLocationProductDetails($pro['locationID'], $pro['productID']);
                    if ($locProducts[$pro['productID']] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($pro['productID']);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }
                }
                
                $uploadedAttachments = [];
                $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($salesOrderID, 3);
                $companyName = $this->getSubdomain();
                $path = '/userfiles/' . md5($companyName) . "/attachments";

                foreach ($attachmentData as $value) {
                    $temp = [];
                    $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
                    $temp['documentRealName'] = $value['documentRealName'];
                    $temp['docLink'] = $path . "/" .$value['documentName'];
                    $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
                }

                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_SO_ITM_INACTIVE', [$errorItems]);
                }
                
                $data['errorMsg'] = $errorMsg;
                $data['inactiveItemFlag'] = $inactiveItemFlag;
                $data['product'] = $pdata;
                $data['productTaxes'] = $productTax;
                $data['locationProducts'] = $locationProducts;
                $data['uploadedAttachments'] = $uploadedAttachments;
                return new JsonModel($data);
            }
        }
        $in[0] = 'wrongsono';
        return new JsonModel($in);
    }

    public function editSalesOrderAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();
            $documentReference = $post['documentReference'];
            if (isset($post['documentReference'])) {
                unset($post['documentReference']);
            }
            $customCurrencyId = $post['customCurrencyId'];

            //get customCurrency data by customer CurrencyId
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            $customCurrencyRate = ($customCurrencyId == "" || $customCurrencyId == 0) ? 1 : $customCurrencyData->currencyRate;
//            $customCurrencySymbol = ($customCurrencyId == "" || $customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->customCurrencySymbol;
//if custom currency rate alredy set add that rate to quotation
            if (isset($post['customCurrencyRate']) && !($customCurrencyId == "" || $customCurrencyId == 0)) {
                $customCurrencyRate = (float) $post['customCurrencyRate'];
            }
            $salesorderOldData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($request->getPost('so_id'));
            $salesOrder = new SalesOrder();
            $so_product = new SalesOrderProduct();
            $salesOrderExchange = array(
                'soID' => $request->getPost('so_id'),
                'expireDate' => $this->convertDateToStandardFormat($request->getPost('expire_date')),
                'payementTerm' => $request->getPost('payment_term'),
                'totalDiscount' => (float) $request->getPost('total_discount') * $customCurrencyRate,
                'totalAmount' => (float) $request->getPost('total_amount') * $customCurrencyRate,
                'comment' => $request->getPost('cmnt'),
                'showTax' => $request->getPost('show_tax'),
                'salesPersonID' => $request->getPost('salesPersonID'),
                'priceListId' => $request->getPost('priceListId'),
            );
            $salesOrder->exchangeArray($salesOrderExchange);
            $this->CommonTable('Invoice\Model\SalesOrderTable')->updateSalesOrder($salesOrder);

            $this->CommonTable('Core\model\DocumentReferenceTable')->deleteDocumentReference(3, $request->getPost('so_id'));
            if (isset($documentReference)) {
                foreach ($documentReference as $key => $val) {
                    foreach ($val as $key1 => $value) {
                        $data = array(
                            'sourceDocumentTypeId' => 3,
                            'sourceDocumentId' => $request->getPost('so_id'),
                            'referenceDocumentTypeId' => $key,
                            'referenceDocumentId' => $value,
                        );
                        $documentReference = new DocumentReference();
                        $documentReference->exchangeArray($data);
                        $this->CommonTable('Core\model\DocumentReferenceTable')->saveDocumentReference($documentReference);
                    }
                }
            }


            if ($post['deleteItems'] != null) {
                foreach ($post['deleteItems'] as $row) {
                    $product = $this->CommonTable('Inventory/Model/ProductTable')->getProductByCode($row['item_code']);
                    $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($product['productID'], $this->user_session->userActiveLocation["locationID"]);
//                    $checkProduct = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderCodeAndLocationProductId($request->getPost('so_no'), $locationProduct->locationProductID);
                    $checkProduct = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderIDAndLocationProductId($request->getPost('so_id'), $locationProduct->locationProductID, $row['unit_price']);
                    if ($checkProduct) {
                        $this->CommonTable('Invoice\Model\SalesOrderProductTaxTable')->deleteSalesOrderProductTax($checkProduct->soProductID);
                        $this->CommonTable('Invoice\Model\SalesOrderProductTable')->deleteSalesOrderProduct($checkProduct->soProductID);
                    }
                }
            }
            if ($post['items'] != null) {
                $itemTaxes = json_decode($post['itemTaxes']);
                foreach ($post['items'] as $ke => $row) {
                    $product = $this->CommonTable('Inventory/Model/ProductTable')->getProductByCode($row['item_code']);
                    $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($product['productID'], $this->user_session->userActiveLocation["locationID"]);

                    if ($row['discountType'] == 'vl') {
                        $row['discount'] = $row['discount'] * $customCurrencyRate;
                    }
                    $sendrow = array(
                        'soID' => $request->getPost('so_id'),
                        'locationProductID' => $locationProduct->locationProductID,
                        'productName' => $row['item'],
                        'soProductDescription' => $row['item_discription'],
                        'quantity' => $row['quantity'],
                        'unitPrice' => $row['unit_price'] * $customCurrencyRate,
                        'uom' => $row['uom'],
                        'discountType' => $row['discountType'],
                        'discount' => is_nan($row['discount'])? null : $row['discount'],
                        'total' => $row['total_cost'] * $customCurrencyRate,
                        'tax' => $itemTaxes[$ke]
                    );
                    $so_product->exchangeArray($sendrow);
                    $checkProduct = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderIDAndLocationProductId($request->getPost('so_id'), $locationProduct->locationProductID, $row['unit_price'] * $customCurrencyRate);
                    $soProductID = '';
                    if ($checkProduct->locationProductID == $locationProduct->locationProductID) {
                        $this->CommonTable('Invoice\Model\SalesOrderProductTable')->editReceipt($so_product, $checkProduct->soProductID);
                        $soProductID = $checkProduct->soProductID;
                    } else {
                        $soProductID = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->saveReceipt($so_product);
                    }
                    if (isset($row["taxDetails"]["tL"])) {
                        foreach ($row["taxDetails"]["tL"] as $txID => $txd) {
                            $taxData = array(
                                'soID' => $request->getPost('so_id'),
                                'soProductID' => $soProductID,
                                'soTaxID' => $txID,
                                'soTaxPrecentage' => $txd["tP"],
                                'soTaxAmount' => $txd["tA"] * $customCurrencyRate
                            );
                            $soProTax = new SalesOrderProductTax();
                            $soProTax->exchangeArray($taxData);
                            $checkProduct = $this->CommonTable('Invoice\Model\SalesOrderProductTaxTable')->getSalesOrderProductTaxBySalesOrderProductIDAndTaxID($soProductID, $txID);
                            if ($checkProduct->soTaxID == $txID) {
                                $this->CommonTable('Invoice\Model\SalesOrderProductTaxTable')->updateSalesOrderProductTax($soProTax, $checkProduct->soProductTaxID);
                            } else {
                                $this->CommonTable('Invoice\Model\SalesOrderProductTaxTable')->saveSoProductTax($soProTax);
                            }
                        }
                    }
                }
            }
            // this below part use to avoid two tabs edit issue.....
            $salesOrderDetails = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getSumOfSalesOrderProductBySalesOrderId($request->getPost('so_id'));
            $valueArray = iterator_to_array($salesOrderDetails);
            $updateDataSet = array(
                'totalAmount' => $valueArray[0]['totalValue'],
                'soID' => $request->getPost('so_id'),
                );
            $updateSalesOrderTotal = $this->CommonTable('Invoice\Model\SalesOrderTable')->updateSalesOrderTotalBysalesOrderID($updateDataSet);


            $changeArray = $this->getSalesOrderChangeArray($salesorderOldData, $post);
            //set log details
            $previousData = json_encode($changeArray['previousData']);
            $newData = json_encode($changeArray['newData']);

            $this->setLogDetailsArray($previousData,$newData);
            $this->setLogMessage("Sales Order ".$changeArray['previousData']['Sales Order Code']." is updated.");
            $this->status = true;
            $this->data = $request->getPost('so_id');
            $this->msg = $this->getMessage('SUCC_SALESORDER_UPDATE', array($request->getPost('so_no')));
            return $this->JSONRespond();
        }
        $this->status = false;
        $this->data = $request->getPost('so_id');
        $this->msg = $this->getMessage('ERR_SALESORDER_UPDATE', array($request->getPost('so_no')));
        return $this->JSONRespond();
    }

    public function getSalesOrderChangeArray($oldData, $newwData)
    {
        $previousData = [];
        $previousData['Sales Order Code'] = $oldData->soCode;
        $previousData['Customer Name'] = $oldData->customerName;
        $previousData['Sales Order issue date'] = $oldData->issuedDate;
        $previousData['Sales Order expire date'] = $oldData->expireDate;
        $previousData['Sales Order Total'] = number_format($oldData->totalAmount,2);
        $previousData['Sales Order Comment'] = $oldData->comment;

        $newData = [];
        $newData['Sales Order Code'] = $newwData->so_no;
        $newData['Customer Name'] = $oldData->customerName;
        $newData['Sales Order issue date'] = $this->convertDateToStandardFormat($newwData->issue_date);
        $newData['Sales Order expire date'] = $this->convertDateToStandardFormat($newwData->expire_date);
        $newData['Sales Order Total'] = number_format($newwData->total_amount,2);
        $newData['Sales Order Comment'] = $newwData->cmnt;

        return array('previousData' => $previousData, 'newData' => $newData);
    }

    public function cancelSalesOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $salesorder = new SalesOrder();
            $data = array(
                'id' => $request->getPost('salesOrderID'),
                'comment' => $request->getPost('comment'),
                'state' => 'Cancelled'
            );
            $salesorder->exchangeArray($data);
            $salesOrderQID = $this->CommonTable('Invoice\Model\SalesOrderTable')->updateSalesOrderStateAndComment($salesorder);
            return new JsonModel(array("result" => $salesOrderQID));
        }
    }

    public function updateSalesOrderAction()
    {
        $paymentTerms = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->id;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->term;
        }

        $so_form = new InvoiceForm(array(
            'name' => 'salesorder',
            'id' => 'so_no',
            'terms' => $terms
        ));
        $so_receipt_form = new InvoiceForm(array(
            'name' => 'salesorder',
            'id' => 'so_no',
            'terms' => $terms
        ));
        $request = $this->getRequest();

        if ($request->isPost()) {
            $post = $request->getPost();
            $salesorder = new SalesOrder();
            $so_form->setInputFilter($salesorder->getInputFilter());
            $so_form->setData($post);

            $valid_items = TRUE;
            $so_receipt = new SalesOrderReceipt();
            $so_receipt_form->setInputFilter($so_receipt->getInputFilter());
            foreach ($post['items'] as $item) {
                $so_receipt_form->setData($item);
                if (!$so_form->isValid()) {
                    $valid_items = FALSE;
                }
            }

            $salesOrderExchange = array(
                'id' => $request->getPost('so_no'),
                'customerName' => $request->getPost('cust_name'),
                'customerID' => $request->getPost('cust_id'),
                'currentBalance' => $request->getPost('currentBalance'),
                'issuedDate' => $request->getPost('issue_date'),
                'completionDate' => $request->getPost('completion_date'),
                'payementTerm' => $request->getPost('payment_term'),
                'taxAmount' => '',
                'totalDiscount' => $request->getPost('total_discount'),
                'totalAmount' => $request->getPost('total_amount'),
                'comment' => $request->getPost('cmnt'),
                'branch' => $locationID = $this->user_session->userActiveLocation["locationID"]
            );
            $salesorder->exchangeArray($salesOrderExchange);
            $retQ = $this->CommonTable('Invoice\Model\SalesOrderTable')->updateSalesOrder($salesorder);
            $this->CommonTable('Invoice\Model\SalesOrderProductTable')->deleteReceipt($request->getPost('so_no'));
            foreach ($post['items'] as $row) {
                $sendrow = array(
                    'productID' => $row['item_code'],
                    'productName' => $row['item'],
                    'quantity' => $row['quantity'],
                    'unitPrice' => $row['unit_price'],
                    'uom' => $row['uom'],
                    'discount' => $row['discount'],
                    'salesOrderID' => $request->getPost('so_no'),
                    'total' => $row['total_cost']
                );
                $so_receipt->exchangeArray($sendrow);
                $this->CommonTable('Invoice\Model\SalesOrderProductTable')->saveReceipt($so_receipt);
            }
        }

        return new JsonModel(array("result" => true));
    }

//   In SalesOrderProduct soCode removed and added soID
    public function updateSalesOrderReceiptAction()
    {
        $updaterequest = $this->getRequest();
        if ($updaterequest->isPost()) {
            $itemarray = $updaterequest->getPost('items');
            $so_no = $updaterequest->getPost('so_no');
            foreach ($itemarray as $key) {
                $row = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getproductByID($so_no, $key['item_code']);
                $newquantity = ($row->invoicedQuantity + $key['quantity']);
                $updaterow = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->updateReceiptQuantity($newquantity, $so_no, $key['item_code']);
            }
            $this->CommonTable('Invoice\Model\SalesOrderTable')->updateSalesOrderStatus('Inprogress', $so_no);
            $pvalue = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderID($so_no);
            $chkso = TRUE;
            while ($t = $pvalue->current()) {
                if ($t->invoicedQuantity < $t->quantity) {
                    $chkso = FALSE;
                }
            }
            if ($chkso != FALSE) {
                $this->CommonTable('Invoice\Model\SalesOrderTable')->updateSalesOrderStatus('Completed', $so_no);
            }
            return TRUE;
        }
    }

    public function cancelRecurrentSalesOrderAction()
    {

        $request = $this->getRequest();
        if ($request->isPost()) {
            $id = $request->getPost('id');
            $username = trim($request->getPost('username'));
            $password = trim($request->getPost('password'));
            $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
            if ($user) {
                if ($user->roleID == '1') {
                    if ($user->password == md5($username . $password . $username)) {
                        $recurrentInvoice = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByID($id);
                        if ($recurrentInvoice->statusID != 'Close') {
                            $data = array(
                                'state' => 'Close',
                                'id' => $id
                            );
                            $RecurrentInvoice = new RecurrentInvoice();
                            $RecurrentInvoice->exchangeArray($data);
                            $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->cancelRecurrentInvoice($RecurrentInvoice);
                            return new JsonModel(array(true));
                        } else {
                            return new JsonModel(array('state'));
                        }
                    } else {
                        return new JsonModel(array('pass'));
                    }
                } else {
                    return new JsonModel(array('admin'));
                }
            } else {
                return new JsonModel(array('user'));
            }
        }
    }

    public function getAllRecurrentSalesOrderAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->fetchAll();
        while ($row = $re->current()) {
            if ($row->Type == 'SalesOrder') {
                $a[] = $row;
            }
        }
        return new JsonModel($a);
    }

    public function getCustomerRecurrentSalesOrderAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $custa = trim($request->getPost('customerShortName'));
            $value = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByShortname($custa);
            if (isset($value->customerID)) {
                $cust_id = $value->customerID;
                $data = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentSalesOrderByCustomerID($cust_id);
                $recurrentSalesOrderview = new ViewModel(array(
                    'recurrentSalesOrder' => $data
                        )
                );
                $delete_recurrent_sales_order = new viewModel(array(
                ));
                $delete_recurrent_sales_order->setTemplate('/invoice/sales-orders-api/confirm-delete-recurrent-sales-order');
                $recurrentSalesOrderview->addChild($delete_recurrent_sales_order, 'delete_so');

                $recurrentSalesOrderview->setTerminal(true);
                $recurrentSalesOrderview->setTemplate('invoice/sales-orders-api/recurrent-sales-order-custom-list');
                return $recurrentSalesOrderview;
            } else {
                $er[0] = 'wrongcust';
                return new JsonModel($er);
            }
        }
    }

    public function getRecurrentSalesOrderByIDAction()
    {

        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $salesOrder = trim($request->getPost('salesOrder'));
            $value = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByID($salesOrder);
            if (isset($value->id)) {
                $data = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentSalesOrder($salesOrder);
                $recurrentSalesOrderview = new ViewModel(array(
                    'recurrentSalesOrder' => $data
                        )
                );
                $delete_recurrent_sales_order = new viewModel(array(
                ));
                $delete_recurrent_sales_order->setTemplate('/invoice/sales-orders-api/confirm-delete-recurrent-sales-order');
                $recurrentSalesOrderview->addChild($delete_recurrent_sales_order, 'delete_so');

                $recurrentSalesOrderview->setTerminal(true);
                $recurrentSalesOrderview->setTemplate('invoice/sales-orders-api/recurrent-sales-order-custom-list');
                return $recurrentSalesOrderview;
            } else {
                $er[0] = 'wrongsalesOrder';
                return new JsonModel($er);
            }
        }
    }

    public function getRecurrentSalesOrdersByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        if ($invrequest->isPost()) {
            $fromdate = $invrequest->getPost('fromdate');
            $todate = $invrequest->getPost('todate');
            $data = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentSalesOrderByDate($fromdate, $todate);
            $recurrentSalesOrderview = new ViewModel(array(
                'recurrentSalesOrder' => $data
                    )
            );
            $delete_recurrent_sales_order = new viewModel(array(
            ));
            $delete_recurrent_sales_order->setTemplate('/invoice/sales-orders-api/confirm-delete-recurrent-sales-order');
            $recurrentSalesOrderview->addChild($delete_recurrent_sales_order, 'delete_so');

            $recurrentSalesOrderview->setTerminal(true);
            $recurrentSalesOrderview->setTemplate('invoice/sales-orders-api/recurrent-sales-order-custom-list');
            return $recurrentSalesOrderview;
        }
    }

    public function getSalesOrderByDatefilterAction()
    {
        $sorequest = $this->getRequest();
        if ($sorequest->isPost()) {
            $fromdate = $sorequest->getPost('fromdate');
            $todate = $sorequest->getPost('todate');
            $customerName = $sorequest->getPost('customerName');
            $filteredSalesOrders = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByDate($fromdate, $todate, $customerName);
            if ($filteredSalesOrders->count() == 0) {
                return new JsonModel(array('msg' => $this->getMessage('ERR_SALESORDER_NO_SORDER')));
            } else {

                $salesOrder = new ViewModel(array(
                    'salesOrders' => $filteredSalesOrders,
                        )
                );
                $salesOrder->setTerminal(TRUE);
                $salesOrder->setTemplate('invoice/sales-orders-api/salesorder-list');
                return $salesOrder;
            }
        }
    }

    public function sendSalesOrderAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($post["cust_id"]);
            if (!empty($cust->email) || TRUE) {
                $to = $post["to_email"];
                $subject = $post["subject"];
                $file = null;
                $Bcc = null;
                $Cc = null;
                $textContent = null;
                $htmlMarkup = $post["body"];
                $msg = $this->sendEmail($to, $subject, $htmlMarkup, $file);
                $error = FALSE;
            } else {
                $error = true;
                $msg = $this->getMessage('ERR_INVOICE_NO_EMAIL');
            }
        } else {
            $error = "true";
            $msg = $this->getMessage('ERR_INVOICE_UNKNOWN');
        }
        return new JsonModel(array(
            "error" => $error,
            "msg" => $msg
        ));
    }

    /**
     * @author  Damith Thamara <damith@thinkcube.com>
     * @return \Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getSalesOrdersFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $SalesOrderID = $searchrequest->getPost('salesOrderID');
            $salesOrders = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersforSearch($SalesOrderID);
            if ($salesOrders->count() == 0) {
                return new JsonModel(array(FALSE));
            } else {
                $salesOrder = new ViewModel(array(
                    'salesOrders' => $salesOrders,
                        )
                );
                $salesOrder->setTerminal(TRUE);
                $salesOrder->setTemplate('invoice/sales-orders-api/salesorder-list');
                return $salesOrder;
            }
        }
    }

    //get sales order related data for delivery note copy to option
    public function getSalesOrderDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $salesOrderID = $request->getPost('salesOrderID');
            $salOrProduct = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderProductBysalesOrderID($salesOrderID);
            foreach ($salOrProduct as $soProducts) {
                $productTypeID = $soProducts['productTypeID'];
                $soProducts = (object) $soProducts;
                $restQuantity = $soProducts->quantity - $soProducts->salesOrdersProductCopiedQuantity;
                if ($restQuantity > 0) {
                    $soProductArray[$soProducts->productID."_".floatval($soProducts->unitPrice)] = $soProducts->productID;
                }
            }
            $locationProducts;
            if (count($soProductArray) < 1 && $productTypeID == 1) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_SALESORDER_ALREADY_USED');
            } else {
                $salesOrder = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderforSearch($salesOrderID)->current();
                $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getSalesOrderProductBysalesOrderID($salesOrderID);
                $tax = array(
                    'salesOrderTaxID' => '',
                    'salesOrderTaxName' => '',
                    'salesOrderTaxPrecentage' => '',
                    'salesOrderTaxAmount' => '',
                );
                $Products = array(
                    'soProductID' => '',
                    'soID' => '',
                    'productID' => '',
                    'unitPrice' => '',
                    'discount' => '',
                    'DiscountType' => '',
                    'quantity' => '',
                    'productCode' => '',
                    'productName' => '',
                    'productType' => '',
                    'tax' => '',
                );
                while ($tt = $salesOrderProducts->current()) {
                    $t = (object) $tt;
                    $locationID = $this->user_session->userActiveLocation['locationID'];
                    $locationProducts[$t->productID] = $this->getLocationProductDetails($locationID, $t->productID);
                    if ($IssueProductArray[$t->productID."_".floatval($t->unitPrice)] != $t->productID) {
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)] = (object) $Products;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->soProductID = $t->soProductID;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->soID = $t->soID;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->productID = $t->productID;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->unitPrice = $t->unitPrice;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->discount = $t->discount;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->productType = $t->productTypeID;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->locationID = $t->locationID;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->locationProductID = $t->locationProductID;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->mrpType = $locationProducts[$t->productID]['mrpType'];
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->mrpValue = $locationProducts[$t->productID]['mrpValue'];
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->mrpPercentageValue = $locationProducts[$t->productID]['mrpPercentageValue'];
                        if ($t->discountType == 'vl') {
                            $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->DiscountType = 'value';
                        } else {
                            $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->DiscountType = 'precentage';
                        }
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->quantity = $t->quantity - $t->salesOrdersProductCopiedQuantity;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->productCode = $t->productCode;
                        $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->productName = $t->productName;
                        if ($t->soTaxID) {
                            $taxes[$t->productID."_".floatval($t->unitPrice)][$t->soTaxID] = (object) $tax;
                            $taxes[$t->productID."_".floatval($t->unitPrice)][$t->soTaxID]->salesOrderTaxID = $t->soTaxID;
                            $taxes[$t->productID."_".floatval($t->unitPrice)][$t->soTaxID]->salesOrderTaxName = $t->taxName;
                            $taxes[$t->productID."_".floatval($t->unitPrice)][$t->soTaxID]->salesOrderTaxPrecentage = $t->soTaxPrecentage;
                            $taxes[$t->productID."_".floatval($t->unitPrice)][$t->soTaxID]->salesOrderTaxAmount = $t->soTaxAmount;
                            $salesOrderProduct[$t->productID."_".floatval($t->unitPrice)]->tax = $taxes[$t->productID."_".floatval($t->unitPrice)];
                        }
                    }
                }
                $customerID = ($salesOrder['customerID']) ? $salesOrder['customerID'] : 0;
                $customerProfileID = ($salesOrder['customerProfileID']) ? $salesOrder['customerProfileID'] : null;
                $customer = $this->getCustomerDetailsByCustomerID($customerID);
                $cusProfDetails = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerProfileID($customerProfileID)->current();
                if($cusProfDetails['customerStatus'] == 2){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_SALESORDER_CUSTOMER_INACTIVE');
                    return $this->JSONRespond();
                }

                $locationPro = Array();
                $inactiveItems = Array();
                foreach ($salesOrderProduct as $value) {
                    $locationPro[$value->productID] = $this->getLocationProductDetails($locationID, $value->productID);
                    if ($locationPro[$value->productID] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($value->productID);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }
                }

                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_SO_ITM_INACTIVE_COPY', [$errorItems]);
                }


                $refCustomerOrderData = $this->CommonTable('Core\Model\DocumentReferenceTable')->getDocumentReferenceBySourceDocIDAndRefTypeDetails(3,46, $salesOrderID)->current();

                $customerOrderData = $this->CommonTable('Invoice\Model\CustomerOrderTable')->retriveCustomerOrder($refCustomerOrderData['referenceDocumentId'])->current();

                $relatedCusOrder = null;

                if (!empty($customerOrderData)) {

                    $relatedCusOrder = (!empty($customerOrderData['customerOrderReference'])) ? $customerOrderData['customerOrderCode'].' / '.$customerOrderData['customerOrderReference'] : $customerOrderData['customerOrderCode'];
                }

                $this->status = true;
                $this->data = array(
                    'salesOrder' => $salesOrder,
                    'relatedCusOrder' => $relatedCusOrder,
                    'salesOrderProduct' => $salesOrderProduct,
                    'customer' => $customer,
                    'customerProfileID' => $customerProfileID,
                    'locationProducts' => $locationProducts,
                    'errorMsg' => $errorMsg,
                    'inactiveItemFlag' => $inactiveItemFlag,
                    'customerProfileName' => $cusProfDetails['customerProfileName'],
                );
            }
            return $this->JSONRespond();
        }
    }

    private function getPaginatedRecurrentSalesOrder($fromdate, $todate)
    {
        $this->paginator = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentSalesOrderByDate($fromdate, $todate);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    private function getCustomerPaginatedRecurrentSalesOrder($cust_id)
    {
        $this->paginator = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentSalesOrderByCustomerID($cust_id);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function sendEmailAction()
    {
        $request = $this->getRequest();
        $documentType = 'Sales Order';

        if ($request->isPost()) {
            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_SALESORDER_EMAIL_SENT');
            return $this->JSONRespond();
        }
        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_SALESORDER_EMAIL_SENT');
        return $this->JSONRespond();
    }

    public function searchSalesOrdersForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $salesOrdersSearchKey = $searchrequest->getPost('searchKey');
            $documentType = $searchrequest->getPost('documentType');
            $locationID = $searchrequest->getPost('locationID');
            if ($locationID == '' || $locationID == null) {
                $locationID = $this->user_session->userActiveLocation["locationID"];
            }
            $salesOrders = $this->CommonTable('Invoice\Model\SalesOrderTable')->searchSalesOrdersForDropDown($locationID, $salesOrdersSearchKey);

            $salesOrdersList = array();
            foreach ($salesOrders as $salesOrder) {
                $temp['value'] = $salesOrder['soID'];
                $temp['text'] = $salesOrder['soCode'];
                if ($documentType == 'invoice' || $documentType == 'deliveryNote') {
                    if ($salesOrder['statusID'] == 3) {
                        $salesOrdersList[] = $temp;
                    }
                } else {
                    $salesOrdersList[] = $temp;
                }
            }

            $this->setLogMessage("Retrive sales order list for dropdown.");
            $this->data = array('list' => $salesOrdersList);
            return $this->JSONRespond();
        }
    }

    public function getAllRelatedDocumentDetailsBySalesOrderIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $salesOrderID = $request->getPost('salesOrderID');
            $salesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderDetailsBySalesOrderId($salesOrderID);
            $quotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getSalesOrderRelatedQuotationDataBySOId($salesOrderID);
            $deliveryNoteData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getSalesOrderRelatedDeliveryNoteDataByQuotationId(null, $salesOrderID);
            $salesReturnData = $this->CommonTable('SalesReturnsTable')->getDeliveryNoteRelatedReturnsDataByQuotationId(null, $salesOrderID);
            $dlnRelatedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getDeliveryNoteRelatedSalesInvoiceDataByQuotationId(null, $salesOrderID);
            $soRelatedInvoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getSalesOrderRelatedSalesInvoiceDataByQuotationId(null, $salesOrderID);
            $dlnRelatedPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getDeliveryNoteRelatedInvoicePaymentsDataByQuotationId(null, $salesOrderID);
            $soRelatedPaymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getSalesOrderRelatedInvoicePaymentsDataByQuotationId(null, $salesOrderID);
            $dlnRelatedCreditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDeliveryNoteRelatedCreditNoteDataByQuotationId(null, $salesOrderID);
            $soRelatedCreditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getSalesOrderRelatedCreditNoteDataByQuotationId(null, $salesOrderID);
            $dlnRelatedCreditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getDeliveryNoteRelatedCreditNotePaymentDataByQuotationId(null, $salesOrderID);
            $soRelatedCreditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getSalesOrderRelatedCreditNotePaymentDataByQuotationId(null, $salesOrderID);
            

            $dataExistsFlag = false;
            if ($salesOrderData) {
                $salesOrderData = array(
                    'type' => 'SalesOrder',
                    'documentID' => $salesOrderData['soID'],
                    'code' => $salesOrderData['soCode'],
                    'amount' => number_format($salesOrderData['totalAmount'], 2),
                    'issuedDate' => $salesOrderData['issuedDate'],
                    'created' => $salesOrderData['createdTimeStamp'],
                );
                $salesOrderDetails[] = $salesOrderData;
                $dataExistsFlag = true;
            }

            if (isset($quotaionData)) {
                foreach ($quotaionData as $qtnDta) {
                    $qtnDeta = array(
                        'type' => 'Quotation',
                        'documentID' => $qtnDta['quotationID'],
                        'code' => $qtnDta['quotationCode'],
                        'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                        'issuedDate' => $qtnDta['quotationIssuedDate'],
                        'created' => $qtnDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $qtnDeta;
                    if (isset($qtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($deliveryNoteData)) {
                foreach ($deliveryNoteData as $dlnDta) {
                    $dlnData = array(
                        'type' => 'DeliveryNote',
                        'documentID' => $dlnDta['deliveryNoteID'],
                        'code' => $dlnDta['deliveryNoteCode'],
                        'amount' => number_format($dlnDta['deliveryNotePriceTotal'], 2),
                        'issuedDate' => $dlnDta['deliveryNoteDeliveryDate'],
                        'created' => $dlnDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $dlnData;
                    if (isset($dlnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($salesReturnData)) {
                foreach ($salesReturnData as $rtnDta) {
                    $srData = array(
                        'type' => 'SalesReturn',
                        'documentID' => $rtnDta['salesReturnID'],
                        'code' => $rtnDta['salesReturnCode'],
                        'amount' => number_format($rtnDta['salesReturnTotal'], 2),
                        'issuedDate' => $rtnDta['salesReturnDate'],
                        'created' => $rtnDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $srData;
                    if (isset($rtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($dlnRelatedInvoiceData)) {
                foreach ($dlnRelatedInvoiceData as $invDta) {
                    $invData = array(
                        'type' => 'SalesInvoice',
                        'documentID' => $invDta['salesInvoiceID'],
                        'code' => $invDta['salesInvoiceCode'],
                        'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                        'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                        'created' => $invDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $invData;
                    if (isset($invDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($soRelatedInvoiceData)) {
                foreach ($soRelatedInvoiceData as $invDta) {
                    $invData = array(
                        'type' => 'SalesInvoice',
                        'documentID' => $invDta['salesInvoiceID'],
                        'code' => $invDta['salesInvoiceCode'],
                        'amount' => number_format($invDta['salesinvoiceTotalAmount'], 2),
                        'issuedDate' => $invDta['salesInvoiceIssuedDate'],
                        'created' => $invDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $invData;
                    if (isset($invDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($dlnRelatedPaymentData)) {
                foreach ($dlnRelatedPaymentData as $payDta) {
                    $paymentData = array(
                        'type' => 'CustomerPayment',
                        'documentID' => $payDta['incomingPaymentID'],
                        'code' => $payDta['incomingPaymentCode'],
                        'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                        'issuedDate' => $payDta['incomingPaymentDate'],
                        'created' => $payDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $paymentData;
                    if (isset($payDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($soRelatedPaymentData)) {
                foreach ($soRelatedPaymentData as $payDta) {
                    $paymentData = array(
                        'type' => 'CustomerPayment',
                        'documentID' => $payDta['incomingPaymentID'],
                        'code' => $payDta['incomingPaymentCode'],
                        'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                        'issuedDate' => $payDta['incomingPaymentDate'],
                        'created' => $payDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $paymentData;
                    if (isset($payDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedCreditNoteData)) {
                foreach ($dlnRelatedCreditNoteData as $cNDta) {
                    $creditNoteData = array(
                        'type' => 'CreditNote',
                        'documentID' => $cNDta['creditNoteID'],
                        'code' => $cNDta['creditNoteCode'],
                        'amount' => number_format($cNDta['creditNoteTotal'], 2),
                        'issuedDate' => $cNDta['creditNoteDate'],
                        'created' => $cNDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $creditNoteData;
                    if (isset($cNDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($soRelatedCreditNoteData)) {
                foreach ($soRelatedCreditNoteData as $cNDta) {
                    $creditNoteData = array(
                        'type' => 'CreditNote',
                        'documentID' => $cNDta['creditNoteID'],
                        'code' => $cNDta['creditNoteCode'],
                        'amount' => number_format($cNDta['creditNoteTotal'], 2),
                        'issuedDate' => $cNDta['creditNoteDate'],
                        'created' => $cNDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $creditNoteData;
                    if (isset($cNDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedCreditNotePaymentData)) {
                foreach ($dlnRelatedCreditNotePaymentData as $cNPDta) {
                    $creditNotePaymentData = array(
                        'type' => 'CreditNotePayment',
                        'documentID' => $cNPDta['creditNotePaymentID'],
                        'code' => $cNPDta['creditNotePaymentCode'],
                        'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                        'issuedDate' => $cNPDta['creditNotePaymentDate'],
                        'created' => $cNPDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $creditNotePaymentData;
                    if (isset($cNPDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($soRelatedCreditNotePaymentData)) {
                foreach ($soRelatedCreditNotePaymentData as $cNPDta) {
                    $creditNotePaymentData = array(
                        'type' => 'CreditNotePayment',
                        'documentID' => $cNPDta['creditNotePaymentID'],
                        'code' => $cNPDta['creditNotePaymentCode'],
                        'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                        'issuedDate' => $cNPDta['creditNotePaymentDate'],
                        'created' => $cNPDta['createdTimeStamp'],
                    );
                    $salesOrderDetails[] = $creditNotePaymentData;
                    if (isset($cNPDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            $sortData = Array();
            foreach ($salesOrderDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $salesOrderDetails);

            $documentDetails = array(
                'salesOrderDetails' => $salesOrderDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }
}

/////////////////// END OF IMVOICE CONTROLLER \\\\\\\\\\\\\\\\\\\\
