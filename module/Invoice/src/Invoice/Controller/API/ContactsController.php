<?php

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Invoice\Model\DispatchNote;
use Invoice\Model\Contacts;
use Invoice\Model\ContactMobileNumbers;
use Invoice\Model\ContactEmails;
use Invoice\Model\DispatchNoteProduct;
use Invoice\Model\DispatchNoteSubProduct;
use Invoice\Model\ContactLists;
use Invoice\Model\ContactListContacts;

class ContactsController extends CoreController
{
    public function createAction()
    {

        $request = $this->getRequest();
        if (!$request->isPost())
        {
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }

        $postData = $request->getPost()->toArray();


        $mobileNumberDetails = (!empty($postData['telNumbers'])) ? $postData['telNumbers'] : [];
        $emailDetails = (!empty($postData['emails'])) ? $postData['emails'] : [];

        //check mobile numbers ar unique

        $existNumArr = [];
        foreach ($mobileNumberDetails as $key => $value) {
            $isMobileNumberExist = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRecrdByMobileNumber($value['number']);

            if ($isMobileNumberExist) {
                $existNumArr[] = $value['number'];
            }

        }

        if (sizeof($existNumArr) > 0) {
            $mobstring = implode(', ', $existNumArr);
            $this->msg = $this->getMessage('ERR_MOBILE_NUM_EXIST',array($mobstring));
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $contact = new Contacts();
        $contact->exchangeArray($postData);
        $contact->entityID = $this->createEntity();

        $contactId = $this->CommonTable('Invoice\Model\ContactsTable')->saveContact($contact);

        if (!$contactId) {
            $this->rollback();
            $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
            $this->status  = false; 
            return $this->JSONRespond();
        }



        if (sizeof($mobileNumberDetails) > 0) {
            foreach ($mobileNumberDetails as $key1 => $value1) {
                $temp = [
                    'mobileNumber' => $value1['number'],
                    "contactID" => $contactId,
                    "mobileNumberType" => $value1['numberTypeId']
                ];

                $contactNumber = new ContactMobileNumbers();
                $contactNumber->exchangeArray($temp);

                $contactNumID = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->saveContactMobileNumber($contactNumber);


                if (!$contactNumID) {
                    $this->rollback();
                    $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                    $this->status  = false; 
                    return $this->JSONRespond();
                }
            }
        }


        if (sizeof($emailDetails) > 0) {
            foreach ($emailDetails as $key2 => $value2) {
                $temp = [
                    'emailAddress' => $value2['address'],
                    "contactID" => $contactId,
                    "emailType" => $value2['emailTypeId']
                ];

                $contactEmail = new ContactEmails();
                $contactEmail->exchangeArray($temp);

                $contactEmailID = $this->CommonTable('Invoice\Model\ContactEmailsTable')->saveContactEmail($contactEmail);

                if (!$contactEmailID) {
                    $this->rollback();
                    $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                    $this->status  = false; 
                    return $this->JSONRespond();
                }

            }
        }

        $this->commit();

        $contacts = $this->CommonTable('Invoice\Model\ContactsTable')->fetchAll(true);
        $paginated = true;
        $msg  = $this->getMessage('SUCC_CONTACT_SAVE');
        return $this->_loadContactListFromSearchData($contacts, $paginated, $msg);
    }

    public function updateAction()
    {

        $request = $this->getRequest();
        if (!$request->isPost())
        {
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }

        $postData = $request->getPost()->toArray();
        $contactID = $postData['contactID'];


        $mobileNumberDetails = (!empty($postData['telNumbers'])) ? $postData['telNumbers'] : [];
        $emailDetails = (!empty($postData['emails'])) ? $postData['emails'] : [];

        //check mobile numbers ar unique

        $existNumArr = [];
        foreach ($mobileNumberDetails as $key => $value) {
            $isMobileNumberExist = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRecrdByMobileNumberByContactID($value['number']);

            if ($isMobileNumberExist) {
                $existNumArr[] = $value['number'];
            }

        }

        if (sizeof($existNumArr) > 0) {
            $mobstring = implode(', ', $existNumArr);
            $this->msg = $this->getMessage('ERR_MOBILE_NUM_EXIST',array($mobstring));
            return $this->JSONRespond();
        }

        $this->beginTransaction();

        $data = array(
            'firstName' => $postData['firstName'],
            'lastName' => $postData['lastName'],
            'title' => $postData['title'],
            'designation' => $postData['designation'],
        );

        $updateContact = $this->CommonTable('Invoice\Model\ContactsTable')->updateContact($data, $contactID);

        if (!$updateContact) {
            $this->rollback();
            $this->msg  = $this->getMessage('ERR_CONTACT_UPDATE'); 
            $this->status  = false; 
            return $this->JSONRespond();
        }

        $deleteMobNumbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->deleteContactMobileNumbers($contactID);

        if (sizeof($mobileNumberDetails) > 0) {
            foreach ($mobileNumberDetails as $key1 => $value1) {
                $temp = [
                    'mobileNumber' => $value1['number'],
                    "contactID" => $contactID,
                    "mobileNumberType" => $value1['numberTypeId']
                ];

                $contactNumber = new ContactMobileNumbers();
                $contactNumber->exchangeArray($temp);

                $contactNumID = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->saveContactMobileNumber($contactNumber);


                if (!$contactNumID) {
                    $this->rollback();
                    $this->msg  = $this->getMessage('ERR_CONTACT_UPDATE'); 
                    $this->status  = false; 
                    return $this->JSONRespond();
                }
            }
        }

        $deleteEmails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->deleteContactEmails($contactID);

        if (sizeof($emailDetails) > 0) {
            foreach ($emailDetails as $key2 => $value2) {
                $temp = [
                    'emailAddress' => $value2['address'],
                    "contactID" => $contactID,
                    "emailType" => $value2['emailTypeId']
                ];

                $contactEmail = new ContactEmails();
                $contactEmail->exchangeArray($temp);

                $contactEmailID = $this->CommonTable('Invoice\Model\ContactEmailsTable')->saveContactEmail($contactEmail);

                if (!$contactEmailID) {
                    $this->rollback();
                    $this->msg  = $this->getMessage('ERR_CONTACT_UPDATE'); 
                    $this->status  = false; 
                    return $this->JSONRespond();
                }
            }
        }

        $this->commit();

        $contacts = $this->CommonTable('Invoice\Model\ContactsTable')->fetchAll(true);
        $paginated = true;
        $msg  = $this->getMessage('SUCC_CONTACT_UPDATE');
        return $this->_loadContactListFromSearchData($contacts, $paginated, $msg);
    }

    public function deleteContactByIdAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost())
        {
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }


        $this->beginTransaction();
        $postData = $request->getPost()->toArray();
        $contactID = $postData['contactID'];

        $contact = $this->CommonTable('Invoice\Model\ContactsTable')->getContactsByContactIDForSearch($contactID)->current();

        if ($contact['isConvertToCustomer'] == "1") {
            $this->rollback();
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_CONTACT_DELTE');
            $this->data   = null;
            return $this->JSONRespond();
        }


        $res = $this->updateDeleteInfoEntity($contact['entityID']);
        if ($res == false) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONTACT_DELETE');
            $this->data = FALSE;
            return $this->JSONRespond();
        } else {
            $deleteMobNumbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->deleteContactMobileNumbers($contactID);
            
            $deleteEmails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->deleteContactEmails($contactID);
            
            $this->commit();
            $this->setLogMessage("Contact is deleted");
            $this->status = true;
            $this->msg = $this->getMessage('SUCC_CONTACT_DELETE');
            $this->data = TRUE;
            return $this->JSONRespond();
        }

    }

    public function getContactFromSearchByContactIDAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {

            if (!empty($searchrequest->getPost('contactID'))) { 
                $contactID = $searchrequest->getPost('contactID');
                // $isSale = ($searchrequest->getPost('isSale')) ? $searchrequest->getPost('isSale') : false;
                $contacts = $this->CommonTable('Invoice\Model\ContactsTable')->getContactsByContactIDForSearch($contactID);
                $paginated = false;
            } else {

                $contacts = $this->CommonTable('Invoice\Model\ContactsTable')->fetchAll(true);
                $paginated = true;
            }


            return $this->_loadContactListFromSearchData($contacts, $paginated);
        }
    }

    public function getCotactDetailsByIdAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {

            if (!empty($searchrequest->getPost('contactID'))) { 
                $contactID = $searchrequest->getPost('contactID');


                // $isSale = ($searchrequest->getPost('isSale')) ? $searchrequest->getPost('isSale') : false;
                $contacts = $this->CommonTable('Invoice\Model\ContactsTable')->getContactsByContactIDForSearch($contactID)->current();

                $emails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->getRelatedEmailByContactID($contactID);

                $numbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRelatedNumbersByContactID($contactID);


                $contacts['emails'] = $emails;
                $contacts['numbers'] = $numbers;

                $this->data = $contacts;
                $this->status = true;

            } 
            return $this->JSONRespond();

        }
    }

    private function _loadContactListFromSearchData($contacts = array(), $paginated = false, $msg = null)
    {
        
        $mobNumbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->fetchAll();

        $contactNumebrs = [];
        foreach ($mobNumbers as $key => $value) {
            $contactNumebrs[$value['contactID']][$value['mobileNumberType']] = $value['mobileNumber'];
        }


        $emails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->fetchAll();

        $contactEmails = [];
        foreach ($emails as $key2 => $value2) {
            $contactEmails[$value2['contactID']][$value2['emailType']] = $value2['emailAddress'];
        }

        $view = new ViewModel(array(
            'contactList' => $contacts,
            'contactNumebrs' => $contactNumebrs,
            'contactEmails' => $contactEmails ,
            'isPaginated' => $paginated
        ));

        $view->setTerminal(true);
        $view->setTemplate('invoice/contacts/master-contact-list');

        $this->status = true;
        $this->html = $view;
        $this->msg = $msg;
        return $this->JSONRespondHtml();
    }

    public function searchContactsForDropdownAction()
    {

        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $state = false;
            $contactSearchKey = $searchrequest->getPost('searchKey');
            $addFlag = $searchrequest->getPost('addFlag');
            if($addFlag == 'withDeactivatedCustomers'){
                $state = true;
            }
            $customerList = array();


            $contacts = $this->CommonTable('Invoice\Model\ContactsTable')->searchContactsForDropDown($contactSearchKey);

            foreach ($contacts as $contact) {
                $firstName = $contact['firstName'];
                $lastName = (!empty($contact['lastName'])) ? $contact['lastName'] : '';
                $fullName = $firstName.' '.$lastName;
                $temp['value'] = $contact['contactID'];
                $temp['text'] = $fullName;
                $customerList[] = $temp;
            }

            $this->data = array('list' => $customerList);

            return $this->JSONRespond();
        }
    }  

    public function getContactsForDirectPromotionsAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $state = false;
            $notifyType = $searchrequest->getPost('notifyType');

            $contacts = $this->CommonTable('Invoice\Model\ContactsTable')->fetchAll(false);

            if ($notifyType == "Email") {
                $contactList = [];
                foreach ($contacts as $key => $contact) {
                    $emails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->getRelatedEmailByContactID($contact['contactID']);

                    if (sizeof($emails) > 0) {
                        if (array_key_exists(1, $emails)) {
                            $relateEmail = $emails[1];
                        } elseif (array_key_exists(2, $emails)) {
                            $relateEmail = $emails[2];
                        } elseif (array_key_exists(3, $emails)) {
                            $relateEmail = $emails[3];
                        }
                        $contact['email'] = $relateEmail;

                        $contactList[$contact['contactID']] = $contact;

                    }
                }
            } elseif ($notifyType == "Sms") {
                $contactList = [];
                foreach ($contacts as $key => $contact) {
                    $numbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRelatedNumbersByContactID($contact['contactID']);

                    if (sizeof($numbers) > 0) {
                        if (array_key_exists(1, $numbers)) {
                            $relateTel = $numbers[1];
                        } elseif (array_key_exists(2, $numbers)) {
                            $relateTel = $numbers[2];
                        } elseif (array_key_exists(3, $numbers)) {
                            $relateTel = $numbers[3];
                        }
                        $contact['tel'] = $relateTel;

                        $contactList[$contact['contactID']] = $contact;

                    }
                }
            }

            $this->data = $contactList;

            return $this->JSONRespond();
        }
    }  

    public function getContactJsonFromSearchByContactIDAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            
            $contactList = array();
            $contactID = $searchrequest->getPost('contactID');

            $contacts = $this->CommonTable('Invoice\Model\ContactsTable')->getContactById($contactID);

            foreach ($contacts as $contact) {
                $firstName = $contact['firstName'];
                $lastName = (!empty($contact['lastName'])) ? $contact['lastName'] : '';
                $fullName = $firstName.' '.$lastName;
                $temp['name'] = $fullName;
                $temp['contactID'] = $contact['contactID'];
                $temp['designation'] = $contact['designation'];
                $temp['mobileNumber'] = "";
                $temp['emailAddress'] = "";
                $numbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRelatedNumbersByContactID($contact['contactID']);

                if (sizeof($numbers) > 0) {
                    if (array_key_exists(1, $numbers)) {
                        $relateTel = $numbers[1];
                    } elseif (array_key_exists(2, $numbers)) {
                        $relateTel = $numbers[2];
                    } elseif (array_key_exists(3, $numbers)) {
                        $relateTel = $numbers[3];
                    }
                    $temp['mobileNumber'] = $relateTel;
                }
                $emails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->getRelatedEmailByContactID($contact['contactID']);

                if (sizeof($emails) > 0) {
                    if (array_key_exists(1, $emails)) {
                        $relateEmail = $emails[1];
                    } elseif (array_key_exists(2, $emails)) {
                        $relateEmail = $emails[2];
                    } elseif (array_key_exists(3, $emails)) {
                        $relateEmail = $emails[3];
                    }

                    $temp['emailAddress'] = $relateEmail;
                }
                
                
                $contactList[] = $temp;
            }

            $this->data = $contactList;
            $this->status = true;
        }
        return $this->JSONRespond();
    } 

    /**
     * Create Contact list
     * @return type
     */
    public function createContactListAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost())
        {
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }
        $post = $request->getPost()->toArray();

        $this->beginTransaction();
        $dataInserted = FALSE;
        $entityID = $this->createEntity();

        $mobileNumberId = (!empty($post['telNumbers'])) ? $post['telNumbers'] : [];

        $promotionData = array(
            'name' => $post['listName'],
            'code' => $post['listCode'],
            'noOfContacts' => count($post['telNumbers']),
            'entityID' => $entityID,
        );

        $isListCodeExist = $this->CommonTable('Invoice\Model\ContactListsTable')->getRecordByListCode($post['listCode']);

        if ($isListCodeExist) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_LIST_CODE_EXIST',array($post['listCode']));
            return $this->JSONRespond();
        }

        $contactLists = new ContactLists();
        $contactLists->exchangeArray($promotionData);
        $contactListsID = $this->CommonTable('Invoice\Model\ContactListsTable')->save($contactLists);

        if($contactListsID == null){
            $this->rollback();
            $this->msg = $this->getMessage('ERR_CONTACT_LIST_SAVE');
            $this->status = false;
            return $this->JSONRespond();
        }

        if (sizeof($mobileNumberId) > 0) {
            foreach ($mobileNumberId as $key1 => $value) {
                $temp = [
                    'contactListID' => $contactListsID,
                    "contactID" => $value,
                ];

                $contactNumber = new ContactListContacts();
                $contactNumber->exchangeArray($temp);

                $contactNumID = $this->CommonTable('Invoice\Model\ContactListContactsTable')->save($contactNumber);

                if (!$contactNumID) {
                    $this->rollback();
                    $this->msg  = $this->getMessage('ERR_CONTACT_LIST_CONTACT_SAVE'); 
                    $this->status  = false; 
                    return $this->JSONRespond();
                }
                $dataInserted = true;
            }
        }

        $contactList = $this->CommonTable('Invoice\Model\ContactListsTable')->fetchAll(true);
        $paginated = true;

        $this->commit();
        $msg = $this->getMessage('SUCC_CONTACT_LIST_SAVE');
        return $this->_loadCustomerListFromSearchData($contactList, $paginated, $msg);
    }

    /**
     * Search Contact list for dropdown
     * @return type array
     */
    public function searchContactsListForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $state = false;
            $contactListSearchKey = $searchrequest->getPost('searchKey');
            $customerList = array();

            $contactLists = $this->CommonTable('Invoice\Model\ContactListsTable')->searchContactsListForDropDown($contactListSearchKey);

            foreach ($contactLists as $contactList) {
                $name = $contactList['name'];
                $code = $contactList['code'];
                $fullString = $name.' - '.$code;
                $temp['value'] = $contactList['contactListsID'];
                $temp['text'] = $fullString;
                $customerList[] = $temp;
            }

            $this->data = array('list' => $customerList);

            return $this->JSONRespond();
        }
    } 

    /**
     * get Contact list from search ContactList list
     * @return type
     */
    public function getCustomerListFromSearchByContactListIDAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            if (!empty($searchrequest->getPost('contactListID'))) { 
                $contactListID = $searchrequest->getPost('contactListID');
                $contactList = $this->CommonTable('Invoice\Model\ContactListsTable')->getContactListByContactListIDForSearch($contactListID);
                $paginated = false;
            }else{
                $contactList = $this->CommonTable('Invoice\Model\ContactListsTable')->fetchAll(true);
                $paginated = true;
            }
            return $this->_loadCustomerListFromSearchData($contactList, $paginated);
        }
    }

    private function _loadCustomerListFromSearchData($contactList = array(), $paginated = false, $msg = null)
    {
        $view = new ViewModel(array(
            'contactList' => $contactList,
            'isPaginated' => $paginated
        ));

        $view->setTerminal(true);
        $view->setTemplate('invoice/contacts/contact-list-table');

        $this->status = true;
        $this->html = $view;
        $this->msg = $msg;
        return $this->JSONRespondHtml();
    }

    /**
     * Delete Contact list by ContactListId
     * @return type JSONRespond
     */
    public function deleteContactListByIdAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost())
        {
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }

        $this->beginTransaction();
        $postData = $request->getPost()->toArray();
        $contactListID = $postData['contactListID'];

        $contactList = $this->CommonTable('Invoice\Model\ContactListsTable')->getContactListByContactListIDForSearch($contactListID)->current();

        $res = $this->updateDeleteInfoEntity($contactList['entityID']);
        if ($res == false) {
            $this->rollback();
            $this->status = false;
            $this->msg = $this->getMessage('ERR_CONTACT_LIST_DELETE');
            $this->data = FALSE;
            return $this->JSONRespond();
        } 

        $deleteMobNumbers = $this->CommonTable('Invoice\Model\ContactListContactsTable')->deleteContactListMobileNumbers($contactListID);
        
        $this->commit();
        $this->setLogMessage("Contact list is deleted");
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_CONTACT_LIST_DELETE');
        $this->data = TRUE;
        return $this->JSONRespond();
    }

    /**
     * get Contact list details from ContactListId
     * @return type array contactList
     */
    public function getContactListDetailsByIdAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {

            if (!empty($searchrequest->getPost('contactListID'))) { 
                $contactListID = $searchrequest->getPost('contactListID');

                $contacts = $this->CommonTable('Invoice\Model\ContactListsTable')->getContactListByContactListIDForSearch($contactListID)->current();
                $contactIDs = $this->CommonTable('Invoice\Model\ContactListContactsTable')->getRelatedNumbersByContactListID($contactListID);

                $contacts['numbers'] = $contactIDs;
                $contactList = array();
                if($searchrequest->getPost('isRequestContact')){
                    foreach ($contactIDs as $contactID) {
                        $contactDetails = $this->CommonTable('Invoice\Model\ContactsTable')->getContactById($contactID)->current();
                    
                        $firstName = $contactDetails['firstName'];
                        $lastName = (!empty($contactDetails['lastName'])) ? $contactDetails['lastName'] : '';
                        $fullName = $firstName.' '.$lastName;
                        $temp['name'] = $fullName;
                        $temp['contactID'] = $contactDetails['contactID'];
                        $temp['designation'] = $contactDetails['designation'];
                        $temp['mobileNumber'] = "";
                        $temp['emailAddress'] = "";
                        $numbers = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRelatedNumbersByContactID($contactDetails['contactID']);

                        if (sizeof($numbers) > 0) {
                            if (array_key_exists(1, $numbers)) {
                                $relateTel = $numbers[1];
                            } elseif (array_key_exists(2, $numbers)) {
                                $relateTel = $numbers[2];
                            } elseif (array_key_exists(3, $numbers)) {
                                $relateTel = $numbers[3];
                            }
                            $temp['mobileNumber'] = $relateTel;
                        }
                        $emails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->getRelatedEmailByContactID($contactDetails['contactID']);

                        if (sizeof($emails) > 0) {
                            if (array_key_exists(1, $emails)) {
                                $relateEmail = $emails[1];
                            } elseif (array_key_exists(2, $emails)) {
                                $relateEmail = $emails[2];
                            } elseif (array_key_exists(3, $emails)) {
                                $relateEmail = $emails[3];
                            }

                            $temp['emailAddress'] = $relateEmail;
                        }
                        
                        
                        $contactList[] = $temp;
                    }
                    $contacts['contactList'] = $contactList;

                }
                

                $this->data = $contacts;
                $this->status = true;

            } 
            return $this->JSONRespond();

        }
    }

    /**
     * update Contact list from ContactListId
     * @return type JSONRespondHtml
     */
    public function updateContactListAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost())
        {
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }
        $post = $request->getPost()->toArray();

        $this->beginTransaction();
        $dataInserted = FALSE;

        $mobileNumberId = (!empty($post['telNumbers'])) ? $post['telNumbers'] : [];
        $contactListID = $post['contactListID'];
        $contactListsData = array(
            'name' => $post['listName'],
            'noOfContacts' => count($post['telNumbers']),
        );

        $contactLists = new ContactLists();
        $contactLists->exchangeArray($contactListsData);
        $result = $this->CommonTable('Invoice\Model\ContactListsTable')->update($contactLists,$contactListID);

        if(!$result){
            $this->rollback();
            $this->msg = $this->getMessage('ERR_CONTACT_LIST_UPDATE');
            $this->status = false;
            return $this->JSONRespond();
        }

        $deleteMobNumbers = $this->CommonTable('Invoice\Model\ContactListContactsTable')->deleteContactListMobileNumbers($contactListID);

        if (sizeof($mobileNumberId) > 0) {
            foreach ($mobileNumberId as $key1 => $value) {
                $temp = [
                    'contactListID' => $contactListID,
                    "contactID" => $value,
                ];

                $contactNumber = new ContactListContacts();
                $contactNumber->exchangeArray($temp);

                $contactNumID = $this->CommonTable('Invoice\Model\ContactListContactsTable')->save($contactNumber);

                if (!$contactNumID) {
                    $this->rollback();
                    $this->msg  = $this->getMessage('ERR_CONTACT_LIST_CONTACT_UPDATE'); 
                    $this->status  = false; 
                    return $this->JSONRespond();
                }
                $dataInserted = true;
            }
        }

        $contactList = $this->CommonTable('Invoice\Model\ContactListsTable')->fetchAll(true);
        $paginated = true;

        $this->commit();
        $msg = $this->getMessage('SUCC_CONTACT_LIST_UPDATE');
        return $this->_loadCustomerListFromSearchData($contactList, $paginated, $msg);
    }

    /**
     * Import contact for show mapping 
     * @return type JSONRespondHtml
     */
    public function importContactAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost())
        {
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }

        if ($_FILES["fileupload"]["tmp_name"] == null) {
            return array('form' => $form, 'fileerror' => 'nofile');
        } else if (substr($_FILES["fileupload"]["name"], -3) != 'csv') {
            return array('form' => $form, 'fileerror' => 'utferror');
        }
        if ($request->getPost('delimiter') == 0) {
            $delim = ',';
        } elseif ($request->getPost('delimiter') == 1) {
            $delim = ';';
        } elseif ($request->getPost('delimiter') == 2) {
            $delim = '.';
        } else {
            $delim = ',';
        }

        if ($request->getPost('header') == 1) {
            $headerchecked = TRUE;
        } else {
            $headerchecked = FALSE;
        }
        $content = fopen($_FILES["fileupload"]["tmp_name"], "r");
        $encodingtype = $request->getPost('characterencoding');
        $fileenc = file($_FILES["fileupload"]["tmp_name"]);
        $string = $fileenc[0];
        if ($encodingtype == 0) {
            $enres = mb_check_encoding($string, 'UTF-8');
            if ($enres == FALSE) {
                return array('form' => $form, 'fileerror' => 'utferror');
            }
        } elseif ($encodingtype == 1) {
            $enres = mb_check_encoding($string, 'ISO-8859-1');
            if ($enres == FALSE) {
                return array('form' => $form, 'fileerror' => 'isoerror');
            }
        }
        move_uploaded_file($_FILES["fileupload"]["tmp_name"], '/tmp/ezBiz.contactImportData.csv');
        chmod('/tmp/ezBiz.contactImportData.csv', 0777);

        $columncount = 0;

        if ($headerchecked == TRUE) {
            $headerlist = fgetcsv($content, 1000, $delim);
            $firstdataline = fgetcsv($content, 1000, $delim);
            for ($headers = 0; $headers < sizeof($headerlist); $headers++) {
                $da[$headers]['header'] = $headerlist[$headers];
                $da[$headers]['row'] = $firstdataline[$headers];
            }
            $columncount = sizeof($headerlist);
        } elseif ($headerchecked == FALSE) {
            $firstdataline = fgetcsv($content, 1000, $delim);
            for ($columns = 0; $columns < sizeof($firstdataline); $columns++) {
                $da[$columns]['header'] = "Column" . ($columns + 1);
                $da[$columns]['row'] = $firstdataline[$columns];
            }
            $columncount = sizeof($firstdataline);
        }

        $fileData = $this->getFileData($request->getPost()->toArray(),$request->getFiles()->toArray());
        $view = new ViewModel(array(
            'data' => $da,
            // 'csvData' => $csvData,
            'form' => $form,
            'header' => TRUE,
            'delim' => $delim,
            'columns' => $columncount,
            'fData' => $fileData['data']
        ));
        $view->setTemplate("invoice/contacts/contact-import-mapping");

        $this->status = true;
        $this->html = $view;
        $this->msg = $msg;
        return $this->JSONRespondHtml();
    }

    private function getFileData($data, $file)
    {
        $fileName = '/tmp/ezBiz.contactImportData.csv';
        move_uploaded_file( $file['fileupload']["tmp_name"], $fileName);
        chmod( $fileName, 0777);

        $fileenc = file($fileName);
        $encodingStatus = mb_check_encoding($fileenc[0], ($data['characterencoding'] == 0) ? 'UTF-8' : 'ISO-8859-1');

        if(!$encodingStatus){
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_ENCODING'),
                'data' => []
            ];
        }

        if (($handle = fopen($fileName, "r")) == FALSE) {
            return [
                'status' => false,
                'msg' => $this->getMessage('ERR_UPLOADED_FILE_READ'),
                'data' => []
            ];
        }

        $dataArr = [];
        while (($fileData = fgetcsv($handle, 1000, $data['delimiter'])) !== FALSE) {
            $rowElementStatusArr = [];
            $rowElementStatusArr = array_map(function($element){
                $element = trim($element);
                return (empty($element)) ? false : true;
            }, $fileData);
            if(in_array(true, $rowElementStatusArr)){//for ignore empty lines
                $dataArr[] = $fileData;
            }
        }
        fclose($handle);
        return [
            'status' => true,
            'msg' => $this->getMessage('SUCC_UPLOADED_FILE'),
            'data' => $dataArr
        ];
    }

    /**
     * Import contact for create contact
     * @return type JSONRespondHtml
     */
    public function processContactForViewAction()
    {
        $request = $this->getRequest();
        
        $writtenrowcount = 0;
        $replacerowcount = 0;

        //check whether request is post
        if(!$request->isPost()){
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }

        //contact import transaction begin
        $this->beginTransaction();

        $delim = $request->getPost('delim');
        $colarray = $request->getPost('col');
        $columncount = $request->getPost('columncount');

        //open csv file
        $filecontent = fopen('/tmp/ezBiz.contactImportData.csv', "r");

        if ($request->getPost('header') == TRUE) {
            $linecontent = fgetcsv($filecontent, 1000, $delim);
        }

        //contact loop
        $row =0;
        $importarray = array();
        $existNumArr = array();

        while ($linecontent = fgetcsv($filecontent, 1000, $delim)) {
            
            $columns = count($linecontent);
            //get each customer details for array
            for ($content = 0; $content < $columns; $content++) {
                if ($colarray[$content] == 'Contact Title') {
                    $importarray[$row]['Contact Title'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Contact Name') {
                    $importarray[$row]['Contact Name'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Telephone') {
                    $importarray[$row]['Telephone'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Email') {
                    $importarray[$row]['Email'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Designation') {
                    $importarray[$row]['Designation'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Other') {
                    $importarray[$row]['Other'] = $linecontent[$content];
                }
                
            }
            $isMobileNumberExist = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRecrdByMobileNumber($importarray[$row]['Telephone']);

            if ($isMobileNumberExist) {
                $existNumArr[] = $importarray[$row]['Telephone'];
            }
            // var_dump($isMobileNumberExist);
            $row++;
            $this->commit();
        }

        $contactCountDetails = array(
            'noOfContacts' => $row,
            'noOfExistContacts' => count($existNumArr),
            'noOfNewContacts' => $row - count($existNumArr)
        );
        // var_dump($existNumArr);die();
        $view = new ViewModel(array(
            'data' => $da,
            'importArray' => $importarray,
            'form' => $form,
            'header' => TRUE,
            'delim' => $delim,
            'columnsCount' => $columncount,
            'colarray' => $colarray,
            'fData' => $fileData['data'],
            'contactCountDetails' => $contactCountDetails
        ));
        $view->setTemplate("invoice/contacts/contact-import-list-view");

        $this->status = true;
        $this->html = $view;
        $this->msg = $msg;
        return $this->JSONRespondHtml();
    }

    /**
     * Save import contact and create contact list
     * @return type JSONRespondHtml
     */
    public function saveImportContactAction()
    {
        $request = $this->getRequest();
        //check whether request is post
        $writtenrowcount = 0;
        $replacerowcount = 0;

        $insertedContact = [
            'existing' => array(),
            'inserted' => array()
        ];
        
        if(!$request->isPost()){
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }

        //customer import transaction begin
        $this->beginTransaction();

        $delim = $request->getPost('delim');
        $colarray = $request->getPost('col');
        $columncount = $request->getPost('columncount');

        //open csv file
        $filecontent = fopen('/tmp/ezBiz.contactImportData.csv', "r");

        if ($request->getPost('header') == TRUE) {
            $linecontent = fgetcsv($filecontent, 1000, $delim);
        }

        //contact loop
        $row =0;
        $existNumArr = array();

        while ($linecontent = fgetcsv($filecontent, 1000, $delim)) {
            $importarray = array();
            $columns = count($linecontent);

            //get each customer details for array
            for ($content = 0; $content < $columns; $content++) {
                if ($colarray[$content] == 'Contact Title') {
                    $importarray['title'] = $linecontent[$content];
                    if(!preg_match("/^(?:[A-Z]+\. ?)+$/i", $linecontent[$content])) {
                        $importarray['title'] = $linecontent[$content].".";
                    }
                } elseif ($colarray[$content] == 'Contact Name') {
                    $importarray['firstName'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Telephone') {
                    $importarray['mobileNumber'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Email') {
                    $importarray['emailAddress'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Designation') {
                    $importarray['designation'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Other') {
                    $importarray['Other'] = $linecontent[$content];
                }
            }
            $isMobileNumberExist = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRecrdByMobileNumber($importarray['mobileNumber']);

            if ($isMobileNumberExist) {
                // var_dump($isMobileNumberExist);
                $existNumArr[] = $importarray['mobileNumber'];
                $insertedContact['existing'][] = $isMobileNumberExist['contactID'];
            }else {

                if(empty($importarray['firstName'])){
                    $this->rollback();
                    $this->msg  = $this->getMessage('ERR_CONTACT_SAVE_CSV'); 
                    $this->status  = false; 
                    return $this->JSONRespond();
                }

                if(empty($importarray['mobileNumber'])){
                    $this->rollback();
                    $this->msg  = $this->getMessage('ERR_CONTACT_SAVE_CSV'); 
                    $this->status  = false; 
                    return $this->JSONRespond();
                }

                $contact = new Contacts();
                $contact->exchangeArray($importarray);
                $contact->entityID = $this->createEntity();

                $contactId = $this->CommonTable('Invoice\Model\ContactsTable')->saveContact($contact);

                if (!$contactId) {
                    $this->rollback();
                    $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                    $this->status  = false; 
                    return $this->JSONRespond();
                }


                if (!empty($importarray['mobileNumber'])) {
                    $temp = [
                        'mobileNumber' => $importarray['mobileNumber'],
                        "contactID" => $contactId,
                        "mobileNumberType" => 1
                    ];

                    $contactNumber = new ContactMobileNumbers();
                    $contactNumber->exchangeArray($temp);

                    $contactNumID = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->saveContactMobileNumber($contactNumber);

                    if (!$contactNumID) {
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }
                }


                if (!empty($importarray['emailAddress'])) {
                    $temp = [
                        'emailAddress' => $importarray['emailAddress'],
                        "contactID" => $contactId,
                        "emailType" => 1
                    ];

                    $contactEmail = new ContactEmails();
                    $contactEmail->exchangeArray($temp);

                    $contactEmailID = $this->CommonTable('Invoice\Model\ContactEmailsTable')->saveContactEmail($contactEmail);

                    if (!$contactEmailID) {
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }
                }
                $row++;
                $insertedContact['inserted'][] = $contactId;
            }    
        }

        if($request->getPost('listCode') && $request->getPost('listName')){
            
            $this->beginTransaction();
            $dataInserted = FALSE;
            $entityID = $this->createEntity();

            $post = $request->getPost()->toArray();

            $mobileNumberId = array_merge($insertedContact['existing'],$insertedContact['inserted']);

            $promotionData = array(
                'name' => $post['listName'],
                'code' => $post['listCode'],
                'noOfContacts' => count($mobileNumberId),
                'entityID' => $entityID,
            );

            $isListCodeExist = $this->CommonTable('Invoice\Model\ContactListsTable')->getRecordByListCode($post['listCode']);

            if ($isListCodeExist) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_LIST_CODE_EXIST',array($post['listCode']));
                return $this->JSONRespond();
            }

            $contactLists = new ContactLists();
            $contactLists->exchangeArray($promotionData);
            $contactListsID = $this->CommonTable('Invoice\Model\ContactListsTable')->save($contactLists);

            if($contactListsID == null){
                $this->rollback();
                $this->msg = $this->getMessage('ERR_CONTACT_LIST_SAVE');
                $this->status = false;
                return $this->JSONRespond();
            }

            if (sizeof($mobileNumberId) > 0) {
                foreach ($mobileNumberId as $key1 => $value) {
                    $temp = [
                        'contactListID' => $contactListsID,
                        "contactID" => $value,
                    ];

                    $contactNumber = new ContactListContacts();
                    $contactNumber->exchangeArray($temp);

                    $contactNumID = $this->CommonTable('Invoice\Model\ContactListContactsTable')->save($contactNumber);

                    if (!$contactNumID) {
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_LIST_CONTACT_SAVE'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }
                    $dataInserted = true;
                }
            }

            $contactList = $this->CommonTable('Invoice\Model\ContactListsTable')->fetchAll(true);
            $paginated = true;

            $this->commit();
            $msg = $this->getMessage('SUCC_CONTACT_LIST_SAVE');
            return $this->_loadCustomerListFromSearchData($contactList, $paginated, $msg);
        
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage('SUCC_CONTACT_SAVE');
        $this->data = $insertedContact;
        return $this->JSONRespond();
    }

    /**
     * Return import confirm modal
     * @return type ViewModel
     */
    public function getImportConfirmAction()
    {
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    /**
     * Save import contact
     * @return type JSONRespondHtml
     */
    public function importContactSaveAction()
    {
        $request = $this->getRequest();
        //check whether request is post
        $writtenrowcount = 0;
        $replacerowcount = 0;

        
        if(!$request->isPost()){
            $this->status = false;
            $this->msg    = $this->getMessage('ERR_INVALID_REQUEST');
            $this->data   = null;
            return $this->JSONRespond();
        }

        //customer import transaction begin
        $this->beginTransaction();

        $delim = $request->getPost('delim');
        $colarray = $request->getPost('col');
        $columncount = $request->getPost('columncount');

        //open csv file
        $filecontent = fopen('/tmp/ezBiz.contactImportData.csv', "r");

        if ($request->getPost('header') == TRUE) {
            $linecontent = fgetcsv($filecontent, 1000, $delim);
        }

        //contact loop
        $existNumArr = array();

        while ($linecontent = fgetcsv($filecontent, 1000, $delim)) {
            $importarray = array();
            $columns = count($linecontent);

            //get each customer details for array
            for ($content = 0; $content < $columns; $content++) {
                if ($colarray[$content] == 'Contact Title') {
                    $importarray['title'] = $linecontent[$content];
                    if(!preg_match("/^(?:[A-Z]+\. ?)+$/i", $linecontent[$content])) {
                        $importarray['title'] = $linecontent[$content].".";
                    } 
                } elseif ($colarray[$content] == 'Contact Name') {
                    $importarray['firstName'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Telephone') {
                    $importarray['mobileNumber'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Email') {
                    $importarray['emailAddress'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Designation') {
                    $importarray['designation'] = $linecontent[$content];
                } elseif ($colarray[$content] == 'Other') {
                    $importarray['Other'] = $linecontent[$content];
                }
            }
            //check contact is already in database
            $isMobileNumberExist = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->getRecrdByMobileNumber($importarray['mobileNumber']);

            if ($request->getPost('method') == 'discard') {

                //if contact not in database save those contacts
                if (!isset($isMobileNumberExist['contactID'])) {

                    if(empty($importarray['firstName'])){
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE_CSV'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }

                    if(empty($importarray['mobileNumber'])){
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE_CSV'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }

                    $contact = new Contacts();
                    $contact->exchangeArray($importarray);
                    $contact->entityID = $this->createEntity();
    


                    $contactId = $this->CommonTable('Invoice\Model\ContactsTable')->saveContact($contact);
    
                    if (!$contactId) {
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }
    
    
                    if (!empty($importarray['mobileNumber'])) {
                        $temp = [
                            'mobileNumber' => $importarray['mobileNumber'],
                            "contactID" => $contactId,
                            "mobileNumberType" => 1
                        ];
    
                        $contactNumber = new ContactMobileNumbers();
                        $contactNumber->exchangeArray($temp);
    
                        $contactNumID = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->saveContactMobileNumber($contactNumber);
    
                        if (!$contactNumID) {
                            $this->rollback();
                            $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                            $this->status  = false; 
                            return $this->JSONRespond();
                        }
                    }
    
    
                    if (!empty($importarray['emailAddress'])) {
                        $temp = [
                            'emailAddress' => $importarray['emailAddress'],
                            "contactID" => $contactId,
                            "emailType" => 1
                        ];
    
                        $contactEmail = new ContactEmails();
                        $contactEmail->exchangeArray($temp);
    
                        $contactEmailID = $this->CommonTable('Invoice\Model\ContactEmailsTable')->saveContactEmail($contactEmail);
    
                        if (!$contactEmailID) {
                            $this->rollback();
                            $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                            $this->status  = false; 
                            return $this->JSONRespond();
                        }
                    }
                    $writtenrowcount++;
                }
            } else if ($request->getPost('method') == 'replace') {
                //if contact is in database update those contacts
                if (isset($isMobileNumberExist['contactID'])) {
                    $postData = $isMobileNumberExist;
                    $contactID = $postData['contactID'];
                

                    $this->beginTransaction();

                    if(empty($importarray['firstName'])){
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE_CSV'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }

                    if(empty($importarray['mobileNumber'])){
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE_CSV'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }

                    $data = array(
                        'firstName' => $importarray['firstName'],
                        'title' => $importarray['title'],
                        'designation' => $importarray['designation'],
                    );

                    $updateContact = $this->CommonTable('Invoice\Model\ContactsTable')->updateContact($data, $contactID);
                    
                    if (!$updateContact) {
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_UPDATE'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }

                    $deleteEmails = $this->CommonTable('Invoice\Model\ContactEmailsTable')->deleteContactEmails($contactID);

                    if (!empty($importarray['emailAddress'])) {
                        $temp = [
                            'emailAddress' => $importarray['emailAddress'],
                            "contactID" => $contactID,
                            "emailType" => 1
                        ];

                        $contactEmail = new ContactEmails();
                        $contactEmail->exchangeArray($temp);
    
                        $contactEmailID = $this->CommonTable('Invoice\Model\ContactEmailsTable')->saveContactEmail($contactEmail);
                        
                        if (!$contactEmailID) {
                            $this->rollback();
                            $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                            $this->status  = false; 
                            return $this->JSONRespond();
                        }
                    }
                    $replacerowcount++;

                }else{

                    if(empty($importarray['firstName'])){
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE_CSV'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }

                    if(empty($importarray['mobileNumber'])){
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE_CSV'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }

                    // Insert new contact 
                    $contact = new Contacts();
                    $contact->exchangeArray($importarray);
                    $contact->entityID = $this->createEntity();
    
                    $contactId = $this->CommonTable('Invoice\Model\ContactsTable')->saveContact($contact);
    
                    if (!$contactId) {
                        $this->rollback();
                        $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                        $this->status  = false; 
                        return $this->JSONRespond();
                    }
    
    
                    if (!empty($importarray['mobileNumber'])) {
                        $temp = [
                            'mobileNumber' => $importarray['mobileNumber'],
                            "contactID" => $contactId,
                            "mobileNumberType" => 1
                        ];
    
                        $contactNumber = new ContactMobileNumbers();
                        $contactNumber->exchangeArray($temp);
    
                        $contactNumID = $this->CommonTable('Invoice\Model\ContactMobileNumbersTable')->saveContactMobileNumber($contactNumber);
    
                        if (!$contactNumID) {
                            $this->rollback();
                            $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                            $this->status  = false; 
                            return $this->JSONRespond();
                        }
                    }
    
    
                    if (!empty($importarray['emailAddress'])) {
                        $temp = [
                            'emailAddress' => $importarray['emailAddress'],
                            "contactID" => $contactId,
                            "emailType" => 1
                        ];
    
                        $contactEmail = new ContactEmails();
                        $contactEmail->exchangeArray($temp);
    
                        $contactEmailID = $this->CommonTable('Invoice\Model\ContactEmailsTable')->saveContactEmail($contactEmail);
    
                        if (!$contactEmailID) {
                            $this->rollback();
                            $this->msg  = $this->getMessage('ERR_CONTACT_SAVE'); 
                            $this->status  = false; 
                            return $this->JSONRespond();
                        }
                    }
                    $writtenrowcount++;
                }
            }
                
        }

        $this->commit();
        $this->status = true;
        $this->msg = $this->getMessage('ERR_CUSTAPI_CUST_SAVE', array($writtenrowcount, $replacerowcount));
        $this->data = $true;
        return $this->JSONRespond();
    }

    /**
     * Function for get contact list
     * @return type JSONRespond
     */
    public function getContactListAction(){
        $contactLists = $this->CommonTable('Invoice\Model\ContactListsTable')->fetchAll(false);

        $contactListArray = array();

        foreach($contactLists as $contactList){
            $contactListArray[$contactList['contactListsID']] = array(
                "contactListsID" => $contactList['contactListsID'],
                "contactListsName" => $contactList['name'],
                "contactListsCode" => $contactList['code'],
                "contactListsNoOfContacts" => $contactList['noOfContacts'],
            );
        }

        $this->status = true;
        $this->data = $contactListArray;
        return $this->JSONRespond();
    }

}