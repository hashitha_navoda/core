<?php

namespace Invoice\Controller\API;

use Core\Controller\CoreController;
use Zend\View\Model\JsonModel;
use Invoice\Model\Invoice;
use Invoice\Model\Membership;
use Invoice\Model\Customer;
use Invoice\Model\InvoiceProduct;
use Invoice\Model\InvoiceSubProduct;
use Invoice\Model\InvoiceProductTax;
use Invoice\Form\RecurrentInvoiceForm;
use Invoice\Model\RecurrentInvoice;
use Invoice\Model\InvoiceReceipt;
use Invoice\Model\TaxAmount;
use Invoice\Model\Reference;
use Inventory\Model\ItemOut;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use JobCard\Model\Activity;
use Inventory\Model\ItemIn;
use Invoice\Model\InvoiceEditLog;
use Invoice\Model\DraftInvoice;
use Invoice\Model\InvoiceEditLogDetials;
use Invoice\Model\InvoicedBomSubItems;
use Invoice\Model\InvoiceSalesPersons;
use Invoice\Model\InvoiceServiceCharge;
use Invoice\Model\InvoiceFreeProduct;
use Invoice\Model\DraftInvWf;
use Invoice\Model\DraftInvSalesPersons;
use Invoice\Model\DraftInvServiceCharge;
use Invoice\Model\DraftInvProduct;
use Invoice\Model\DraftInvFreeIssueItems;
use Invoice\Model\DraftInvProductTax;
use Invoice\Model\DraftInvSubProduct;
use Core\Model\DocumentAttachementMapping;



class InvoiceController extends CoreController
{

    protected $userID;
    protected $cdnUrl;
    protected $user_session;
    protected $paginator;
    protected $company;
    protected $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
    }

    public function getInvoiceNoForLocationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $request->getPost('locationID');
            $refNumber = NULL;
            $locationRefID = NULL;
            $result = $this->getReferenceNoForLocation('3', $locationID);
            $locationRefID = $result['locRefID'];
            $refNumber = $result['refNo'];
            if ($refNumber) {
                $this->status = TRUE;
                $this->data = array(
                    'refNo' => $refNumber,
                    'locRefID' => $locationRefID,
                );
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_NOREFNUM');
                $this->data = FALSE;
                return $this->JSONRespond();
            }
        }
    }

    public function getCustomerDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $customerID = $request->getPost('customerID');
            $customer = $this->getCustomerDetailsByCustomerID($customerID);

            if ($customer) {

                $this->status = TRUE;
                $this->data = $customer;
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_CUSID_INCRT');
            }
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan Madushka <ashan@thinkcube.com>
     * @return type
     */
    public function searchSalesInvoicesForDocumentDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $invoiceSearchKey = $searchrequest->getPost('searchKey');
            $posflag = $searchrequest->getPost('addFlag');
            $locationID = $searchrequest->getPost('locationID');
            $customerID = $searchrequest->getPost('documentType');
            
            if($customerID != null){
                $posflag = false;
            }

            $this->data = array(
                'list' => $this->_searchSalesInvoiceForDropdown($invoiceSearchKey, array(3, 4, 6), '', $posflag, $locationID, $customerID),
            );

            $this->setLogMessage("Retrive sales invoice list for dropDown");
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sharmilan <sharmilan@thinkcube.com>
     * @return type
     */
    public function searchSalesInvoicesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $invoiceSearchKey = $searchrequest->getPost('searchKey');
            $posflag = $searchrequest->getPost('addFlag');
            $viewFlag = $searchrequest->getPost('locationID');
            if($viewFlag){
                $status = array(3, 4, 6, 5);
            } else {
                $status = array(3,4, 6);
            }

            $this->data = array(
                'list' => $this->_searchSalesInvoiceForDropdown($invoiceSearchKey, $status, '', $posflag),
            );
            return $this->JSONRespond();
        }
    }

    public function searchOpenSalesInvoicesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $invoiceSearchKey = $searchrequest->getPost('searchKey');
            $posflag = $searchrequest->getPost('addFlag');

            $this->data = array(
                'list' => $this->_searchSalesInvoiceForDropdown($invoiceSearchKey, array(3), '', $posflag)
            );
            return $this->JSONRespond();
        }
    }

    public function searchOpenSalesInvoiceForPaymentsDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $invoiceSearchKey = $searchrequest->getPost('searchKey');
            $posflag = $searchrequest->getPost('addFlag');

            $this->data = array(
                'list' => $this->_searchSalesInvoiceForDropdown($invoiceSearchKey, array(3, 6), true, $posflag)
            );
            return $this->JSONRespond();
        }
    }

    public function searchLocationWiseOpenSalesInvoicesForDocumentDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $invoiceSearchKey = $searchrequest->getPost('searchKey');
            $locationIds = empty($searchrequest->getPost('locationID')) ? [] : $searchrequest->getPost('locationID');

            $invoiceList = array();
            if (!empty($locationIds)) {
                $invoices = $this->CommonTable('Invoice\Model\InvoiceTable')->searchLocationWiseInvoicesForDropDown($locationIds, array(), $invoiceSearchKey);
                foreach ($invoices as $invoice) {
                    $temp['value'] = $invoice['salesInvoiceID'];
                    $temp['text'] = $invoice['salesInvoiceCode'];
                    $invoiceList[] = $temp;
                }
            }
            $this->data = ['list' => $invoiceList];

            return $this->JSONRespond();
        }
    }

    private function _searchSalesInvoiceForDropdown($invoiceSearchKey, $statusIDs = array(), $isFullPaid = false, $posflag, $locationID = NULL, $customerID = null)
    {
        if ($locationID == NULL) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
        }
        $invoices = $this->CommonTable('Invoice\Model\InvoiceTable')->searchInvoicesForDropDownInInvoiceView($locationID, $invoiceSearchKey, $statusIDs, $isFullPaid, $posflag,$customerID);

        $invoiceList = array();
        foreach ($invoices as $invoice) {
            $temp['value'] = $invoice['salesInvoiceID'];
            if ($posflag) {
                $temp['text'] = $invoice['salesInvoiceTemporaryCode'] ? $invoice['salesInvoiceCode'] .
                        " (" . $invoice['salesInvoiceTemporaryCode'] . ")" : $invoice['salesInvoiceCode'];
            } else {
                $temp['text'] = $invoice['salesInvoiceCode'];
            }
            $invoiceList[] = $temp;
        }
        return $invoiceList;
    }

//get delivery note details for invoice copy to option
    public function getDeliveryNoteDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $deliveryNoteIDs = $request->getPost('deliveryNoteIDs');
            $forReturn = $request->getPost('forReturn');

            if(!is_array($deliveryNoteIDs)){
                $deliveryNoteArray = array($deliveryNoteIDs);
            } else {
                $deliveryNoteArray = $deliveryNoteIDs;
            }

            $deliveryNotesData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNotesByIDs($deliveryNoteArray);

            $checkDeliveryNoteStatus = false;
            $checkDeliveryNoteCreatedByJob = false;
            foreach ($deliveryNotesData as $value) {
                if($value['deliveryNoteStatus'] != 3 && $value['deliveryNoteStatus'] != 15){
                    $checkDeliveryNoteStatus = true;
                }

                if (!is_null($value['jobId'])) {
                    $checkDeliveryNoteCreatedByJob = true;                  
                }
            }

            if($checkDeliveryNoteStatus){
                $this->status = false;
                $this->data = array('refresh'=>true);
                $this->msg = $this->getMessage('ERR_INVOICE_EDITED_DELINOTE');
                if(is_array($deliveryNoteIDs)){
                    $this->msg = $this->getMessage('ERR_INVOICE_EDITED_DELINOTES');
                }
                $this->flashMessenger()->addMessage(['status'=>false,'msg'=>$this->msg]);
                return $this->JSONRespond();
            }

            if($checkDeliveryNoteCreatedByJob == true && $forReturn == "false"){
                $this->status = false;
                $this->data = array('refresh'=>true);
                $this->msg = $this->getMessage('ERR_INVOICE_DLN_BY_JOB');
                if(is_array($deliveryNoteIDs)){
                    $this->msg = $this->getMessage('ERR_INVOICE_DELINOTE_BY_JOB');
                }
                $this->flashMessenger()->addMessage(['status'=>false,'msg'=>$this->msg]);
                return $this->JSONRespond();
            }

            $currencyID = $request->getPost('customCurrencyID');
            $deliveryProductsArray = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductByDeliveryNoteIDs($deliveryNoteIDs);

            $dparray;

            foreach ($deliveryProductsArray as $dp) {
                $dp = (object) $dp;
                if ($dp->deliveryNoteProductQuantity > $dp->deliveryNoteProductCopiedQuantity) {
                    $dparray[$dp->deliveryNoteID + "_" + $dp->productID] = $dp->productID;
                }
            }

            $locationProducts;

            if (count($dparray) > 0) {

                $customCurrency = $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($currencyID);
                $customCurrencySymbol = (isset($customCurrency->currencySymbol)) ? $customCurrency->currencySymbol : $this->companyCurrencySymbol;
                $customCurrencyRate = (isset($customCurrency->currencyRate)) ? $customCurrency->currencyRate : 1;

                $documentTypeID = $this->CommonTable('Settings\Model\DocumentTypeTable')->getDocumentTypeByName('Delivery Note')->documentTypeID;

                $deliveryNote = array();
                if(is_array($deliveryNoteIDs)){
                    $deliveryNoteArray = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNotesByIDs($deliveryNoteIDs);
                    foreach ($deliveryNoteArray  as $key => $value) {
                        $customerID = $value['customerID'];
                        $customerProfileID[] = $value['customerProfileID'];
                        $salesPersonID[] = $value['salesPersonID'];
                        $deliveryNoteComment.= $value['deliveryNoteComment'];
                        $customCurrencyId= $value['customCurrencyId'];
                        $priceListId[] = $value['priceListId'];
                        $currencyRate[] = $value['deliveryNoteCustomCurrencyRate'];
                        $deliveryNoteCharge += (float) $value['deliveryNoteCharge'];
                    }
                    $deliveryNote = (object) array(
                        'customerID' => $customerID,
                        'customerProfileID' => (count(array_unique($customerProfileID))==1)? $customerProfileID[0] : null,
                        'salesPersonID' => $salesPersonID,
                        'deliveryNoteComment' => $deliveryNoteComment,
                        'customCurrencyId' => $customCurrencyId,
                        'deliveryNoteCustomCurrencyRate' => (count(array_unique($currencyRate))==1)? $currencyRate[0] : $customCurrencyRate,
                        'priceListId' => (count(array_unique($priceListId))==1)? $priceListId[0] : null,
                        'deliveryNoteCharge' => $deliveryNoteCharge,
                    );
                }else{
                    $deliveryNote = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteByID($deliveryNoteIDs);
                }

                $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductByDeliveryNoteIDs($deliveryNoteIDs);
                $tax = array(
                    'deliveryNoteTaxID' => '',
                    'deliveryNoteTaxName' => '',
                    'deliveryNoteTaxPrecentage' => '',
                    'deliveryNoteTaxAmount' => '',
                );
                $subProduct = array(
                    'deliveryNoteSubProductID' => '',
                    'productBatchID' => '',
                    'productBatchCode' => '',
                    'productSerialID' => '',
                    'productSerialCode' => '',
                    'deliveryProductSubQuantity' => '',
                );

                $Products = array(
                    'deliveryNoteProductID' => '',
                    'deliveryNoteID' => '',
                    'productID' => '',
                    'deliveryNoteProductPrice' => '',
                    'deliveryNoteProductDiscount' => '',
                    'deliveryNoteProductDiscountType' => '',
                    'deliveryNoteProductQuantity' => '',
                    'productCode' => '',
                    'productName' => '',
                    'tax' => '',
                    'subProduct' => '',
                    'locationID' => '',
                    'locationProductID' => '',
                );

                $productIdForTax = null;

                while ($tt = $deliveryNoteProducts->current()) {
                    $t = (object) $tt;

                        $returnProductData = $this->CommonTable('SalesReturnsProductTable')->getReturnProductAndSubProductDataByDeliveryNoteProductID($t->deliveryNoteProductID);
                        $salesReturnProductQty = 0;
                        $returnSubProductSerial = array();
                        $returnSubProductBatch = array();
                        $salesReturnProductIDArray = array();
                        foreach ($returnProductData as $prValue) {
                            $prValue = (object) $prValue;
                            if ($prValue->salesReturnProductID) {
//use this if statement for ignore repeating salesReturnProducts
                                if (!$salesReturnProductIDArray[$prValue->salesReturnProductID] == 1) {
                                    $salesReturnProductQty+=$prValue->salesReturnProductQuantity;
                                    $salesReturnProductIDArray[$prValue->salesReturnProductID] = 1;
                                }
                                if ($prValue->productSerialID) {
                                    $returnSubProductSerial[$prValue->productSerialID] += $prValue->salesReturnSubProductQuantity;
                                } else if ($prValue->productBatchID) {
                                    $returnSubProductBatch[$prValue->productBatchID] += $prValue->salesReturnSubProductQuantity;
                                }
                            }
                        }

                        $locationProduct = $this->getLocationProductDetailsForReturn($t->deliveryNoteLocationID, $t->productID);

                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID] = (object) $Products;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->deliveryNoteProductID = $t->deliveryNoteProductID;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->deliveryNoteID = $t->deliveryNoteID;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->documentTypeID = $documentTypeID;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->productID = $t->productID;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->deliveryNoteProductPrice = $t->deliveryNoteProductPrice;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->deliveryNoteProductDiscount = $t->deliveryNoteProductDiscount;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->deliveryNoteProductDiscountType = $t->deliveryNoteProductDiscountType;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->deliveryNoteProductQuantity = $t->deliveryNoteProductQuantity - $t->deliveryNoteProductCopiedQuantity;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->productCode = $t->productCode;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->productName = $t->productName;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->productType = $t->productTypeID;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->locationID = $t->deliveryNoteLocationID;
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->locationProductID = $locationProduct['lPID'];
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->mrpType = $locationProduct['mrpType'];
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->mrpValue = $locationProduct['mrpValue'];
                        $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->mrpPercentageValue = $locationProduct['mrpPercentageValue'];

                        if ($t->deliveryNoteSubProductID != '') {
                            $returnQuantity = 0;
                            if ($returnSubProductSerial[$t->productSerialID]) {
                                $returnQuantity = $returnSubProductSerial[$t->productSerialID];
                            } else if ($returnSubProductBatch[$t->productBatchID]) {
                                $returnQuantity = $returnSubProductBatch[$t->productBatchID];
                            }
                            if ($t->deliveryProductSubQuantity - $returnQuantity - $t->deliveryNoteSubProductsCopiedQuantity > 0) {
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID] = (object) $subProduct;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->deliveryNoteSubProductID = $t->deliveryNoteSubProductID;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->productBatchID = $t->productBatchID;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->productBatchCode = $t->productBatchCode;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->productSerialID = $t->productSerialID;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->productSerialCode = $t->productSerialCode;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->deliveryProductSubQuantity = $t->deliveryProductSubQuantity - $returnQuantity - $t->deliveryNoteSubProductsCopiedQuantity;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->deliverySubProdWrnty = $t->deliveryNoteSubProductsWarranty;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->deliverySubProdWrntyType = $t->deliveryNoteSubProductsWarrantyType;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->deliverySubProdBatchPrice = $t->productBatchPrice;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->productBatchExpDate = $t->productBatchExpiryDate;
                                $deliverySubProduct[$t->deliveryNoteID."_".$t->productID][$t->deliveryNoteSubProductID]->productSerialExpDate = $t->productSerialExpireDate;

                            }
                        }
                        if ($deliverySubProduct) {
                            $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->subProduct = $deliverySubProduct[$t->deliveryNoteID."_".$t->productID];
                        }

                        if ($t->deliveryNoteTaxID) {
                            //reset $taxes array for new deleivery note product ids
                            if ($productIdForTax != $t->productID) {
                                $taxes = [];
                            }
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID] = (object) $tax;
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxID = $t->deliveryNoteTaxID;
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxName = $t->taxName;
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxPrecentage = $t->deliveryNoteTaxPrecentage;
                            $taxes[$t->deliveryNoteSubProductID][$t->deliveryNoteTaxID]->deliveryNoteTaxAmount = $t->deliveryNoteTaxAmount;
                            $deliveryProduct[$t->deliveryNoteID."_".$t->productID]->tax = $taxes[$t->deliveryNoteSubProductID];
                        }
                        $productIdForTax = $t->productID;

                        $locationID = $this->user_session->userActiveLocation['locationID'];
                        
                        if ($forReturn == "false") {
                            $locationProducts[$t->productID] = $this->getLocationProductDetails($locationID, $t->productID);
                        } else {
                            $locationProducts[$t->productID] = $this->getLocationProductDetailsForReturn($locationID, $t->productID);
                        }
                }

                $customerID = ($deliveryNote->customerID) ? $deliveryNote->customerID : 0;
                $customer = $this->getCustomerDetailsByCustomerID($customerID);
                $cusProfDetails = $this->CommonTable('Invoice\Model\CustomerProfileTable')->getProfileByCustomerProfileID($deliveryNote->customerProfileID)->current();
                if($customer->customerStatus == 2){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_INVOICE_CUSTOMER_INACTIVE');
                    $this->data = array('refresh' => false);
                    return $this->JSONRespond();
                }

                $locationPro = Array();
                $inactiveItems = Array();
                foreach ($deliveryProduct as $value) {
                    $locationPro[$value->productID] = $this->getLocationProductDetails($locationID, $value->productID);
                    if ($locationPro[$value->productID] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($value->productID);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }
                }

                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_DLN_ITM_INACTIVE_COPY', [$errorItems]);
                }


                $dimensionData = [];
                if (!is_array($request->getPost('deliveryNoteIDs'))) {
                    $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(4,$request->getPost('deliveryNoteIDs'));
                    
                    $dlnSingleData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteByID($request->getPost('deliveryNoteIDs'));

                    foreach ($jEDimensionData as $value) {
                        if (!is_null($value['journalEntryID'])) {
                            $temp = [];
                            if($value['dimensionType'] == "job" || $value['dimensionType'] == "project") {
                                if ($value['dimensionType'] == "job") {
                                    $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($value['dimensionValueID']);
                                    $valueTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                                    $dimensionTxt = "Job";
                                } else if ($value['dimensionType'] == "project") {
                                    $proData = $this->CommonTable('Jobs\Model\ProjectTable')->getProjectByProjectId($value['dimensionValueID']);
                                    $valueTxt = $proData->projectCode.'-'.$proData->projectName;
                                    $dimensionTxt = "Project";
                                }

                                $temp['dimensionTypeId'] = $value['dimensionType'];
                                $temp['dimensionTypeTxt'] = $dimensionTxt;
                                $temp['dimensionValueId'] = $value['dimensionValueID'];
                                $temp['dimensionValueTxt'] = $valueTxt;
                            } else {
                                $temp['dimensionTypeId'] = $value['dimensionType'];
                                $temp['dimensionTypeTxt'] = $value['dimensionName'];
                                $temp['dimensionValueId'] = $value['dimensionValueID'];
                                $temp['dimensionValueTxt'] = $value['dimensionValue'];
                            }
                            $dimensionData[$dlnSingleData->deliveryNoteCode][$value['dimensionType']] = $temp;
                        }
                    }
                }

                $this->status = true;
                $this->data = array(
                    'status' => true,
                    'deliveryNote' => $deliveryNote,
                    'deliveryNoteProduct' => $deliveryProduct,
                    'customer' => $customer,
                    'locationProducts' => $locationProducts,
                    'customCurrencySymbol' => $customCurrencySymbol,
                    'errorMsg' => $errorMsg,
                    'inactiveItemFlag' => $inactiveItemFlag,
                    'dimensionData' => $dimensionData,
                    'customerProfName' => $cusProfDetails['customerProfileName'],
                );
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_USED_DELINOTE');
            }
            return $this->JSONRespond();
        }
    }

    /**
     * This function is used by both Invoice and POS controllers *to save
     * invoices and apdate inventory (serial and batch items are handled).
     * @param array $param
     * @param string $customerID
     * @param bool $pos
     * @return array
     * @author User <prathap@thinkcube.com>
     */
    public function storeInvoice($param, $customerID = 0, $pos = false, $edit = false, $restFlag = false, $posUserID)
    {      
        // please notify if we do a change within this function we need do the same change to converDraftToInvoice named function too

        $customCurrencyId = $param['customCurrencyId'];

//get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "") ? $customCurrencyData->currencyRate : 1;
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $ccrate = $param['customCurrencyRate'];
//if custom currency rate alredy set add that rate to sales Order
        if (!($ccrate == 0 || $ccrate == 1)) {
            $customCurrencyRate = $ccrate;
        }

        if ($restFlag == true && $customerID == NULL) {
            $customerID = 0;
        }

        // if invoice crated with POS , sms send feature will be disabled
        $posModeFlag = true;
        if($pos)
        {
            $posModeFlag = false;
        }

        $products = $param['products'];
        $invoiceCode = $param['invoiceCode'];
        $locationOut = $param['locationOutID'];
        $invoiceDate = $this->convertDateToStandardFormat($param['invoiceDate']);
        $dueDate = $this->convertDateToStandardFormat($param['dueDate']);
        $deliveryCharge = $param['deliveryCharge'] * $customCurrencyRate;
        $totalPrice = trim($param['invoiceTotalPrice']) * $customCurrencyRate;
        $finalTotalWithInclusiveTax = trim($param['finalTotalWithInclusiveTax']) * $customCurrencyRate;
        $comment = $param['invoiceComment'];
        $paymentTermID = $param['paymentTermID'];
        $showTax = $param['showTax'];
        $deliveryNoteID = $param['deliveryNoteID'];
        $salesOrderID = $param['salesOrderID'];
        $quotationID = $param['quotationID'];
        $activityID = $param['activityID'];
        $jobID = $param['jobID'];
        $projectID = $param['projectID'];
        $salesPersonIDArr = $param['salesPersonID'];
        $invoiceTotalDiscount = $param['invoiceTotalDiscount'] * $customCurrencyRate;
        $salesInvoicePayedAmount = $param['salesInvoicePayedAmount'] * $customCurrencyRate;
        $suspendedTax = $param['suspendedTax'];
        $deliveryAddress = $param['deliveryAddress'];
        $promotionID = $param['promotionID'];
        $promotionDiscountValue = $param['promotionDiscountValue'];
        $totalInvoiceDiscountValue = $param['invoiceDiscountRate'];
        $invoiceDiscoutType = $param['invoiceDiscountType'];
        $totalTax = $param['totalTaxValue'] * $customCurrencyRate;
        $invoiceWisePromotionDiscountType = $param['invoiceWisePromotionDiscountType'];
        $salesInvoiceDeliveryChargeEnable = $param['salesInvoiceDeliveryChargeEnable'];
        $priceListId = $param['priceListId'];
        $customerProfID = $param['customerProfID'];
        $invTempCode = $param['invTempCode'];
        $journalEntryData = $param['journalEntryData'];
        $inclusiveTax = (int)filter_var($param['inclusiveTax'], FILTER_VALIDATE_BOOLEAN);
        $dimensionData = $param['dimensionData'];
        $ignoreBudgetLimit = $param['ignoreBudgetLimit'];
        $nextInvoiceDate = ($param['nextInvoiceDate'] == "") ? null : $param['nextInvoiceDate'];
        $nextInvoiceDateComment = $param['nextInvoiceDateComment'];
        $isSetJournalEntry = false;
        $serviceChargeAmount = $param['serviceChargeAmount'];
        $addedServiceChargeArray = $param['addedServiceChargeArray'];
        $addedFreeIssueItems = (!empty($param['addedFreeIssueItems'])) ? $param['addedFreeIssueItems'] : [];
        $linkedCusOrder = (!empty($param['linkedCusOrder'])) ? $param['linkedCusOrder'] : null;

        if (!$pos) {
            $showTax = 1;
        }

        if(count($journalEntryData) > 0){
            $isSetJournalEntry = true;
        }

        if (!is_null($deliveryNoteID)) {
            $deliveryNoteStatus = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteStatusByDeliveryNoteID($deliveryNoteID)->current();
            if ($deliveryNoteStatus['deliveryNoteStatus'] == "10") {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_DLN'),
                );
            }

            if ($deliveryNoteStatus['deliveryNoteStatus'] == "4") {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_CLOSE_DLN'),
                );
            }
        }

        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        $customerCurrentOutstanding = $cust->customerCurrentBalance;

        $promotionData = $this->getPromotionDetails($promotionID);

        $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
        $displayData = $result->current();
        $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
        $averageCostingFlag = $displayData['averageCostingFlag'];

        $accountProduct = array();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountsData = $glAccountExist->current();
        $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

       
        $custSalesAccountIsset = true;
        if ($restFlag) {
            $this->useAccounting = 1;
        }
        if($this->useAccounting == 1 && !$isSetJournalEntry){
            if(empty($customerData->customerSalesAccountID)){
                $custSalesAccountIsset = false;
                // return array(
                //     'status' => false,
                //     'msg' => $this->getMessage('ERR_CUSTOMER_SALES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                // );
            }
            if(empty($customerData->customerReceviableAccountID)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                );
            }
            if($deliveryCharge > 0 && empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_GL_DELIVERY_CHARGES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                );
            }
        }

        if(!$customerProfID){
            $customerDefaultProfile = $this->CommonTable('Invoice\Model\CustomerProfileTable')
                            ->getDefaultProfileByCustomerID($customerID);
            foreach ($customerDefaultProfile as $key => $value) {
                $customerProfID = $value['customerProfileID'];
            }
        }

        if(!$pos){
            $itemAttrDetailsSet = [];
            foreach ($products as $key => $p) {
                $productID = $p['productID'];
                $itemAttributeMap = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->getDetailsByProductID($productID);
                foreach ($itemAttributeMap as $key => $value) {
                    if($value['itemAttributeID'] == 1){
                        $itemAttrDetailsSet[$productID][] = $value['itemAttributeValueID'];
                    }
                }
            }
                //check the same credit period is exist for all items
            if(count($itemAttrDetailsSet) > 1){
                foreach ($itemAttrDetailsSet as $ckey => $cvalue) {
                    if(isset($oldVal)){
                        $newVal = $cvalue;
                        $oldVal = array_intersect($oldVal, $newVal);
                        if(!(count($oldVal) > 0)){
                            return array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_INVOICE_API_ITEMATTRIBUTE_CREDIT_PERIOD')
                            );
                            break;
                        }
                    }else{
                        $oldVal = $cvalue;
                    }
                }
            }
        }

        //check whether SalesInvoice is created via a SalesOrder, DeliveryNote or activity
        if (!empty($salesOrderID) || !empty($deliveryNoteID) || !empty($activityID) || !empty($jobID) || !empty($projectID) || !empty($quotationID)) {
            $fromSalesOrderOrDeliveryNote = TRUE;
        } else {
            $fromSalesOrderOrDeliveryNote = FALSE;
        }

        $result = $this->getReferenceNoForLocation('3', $locationOut);
        $locationReferenceID = $result['locRefID'];

        $stresult = $this->getReferenceNoForLocation('25', $locationOut);
        $stlocationReferenceID = $stresult['locRefID'];

        // check if a invoice from the same invoice Code exists if exist add next number to invoice code
        //if this is coming from invoice edit then it does not go to inside while
        if (!$edit) {
            while ($invoiceCode) {
                if ($this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBySalesInvoiceCode($invoiceCode)) {
                    if ($locationReferenceID && $stlocationReferenceID) {
                        $newInvoiceCode = $this->getReferenceNumber($locationReferenceID);
                        $stnewInvoiceCode = $this->getReferenceNumber($stlocationReferenceID);
                        if ($newInvoiceCode == $invoiceCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $invoiceCode = $this->getReferenceNumber($locationReferenceID);
                        } else if ($stnewInvoiceCode == $invoiceCode) {
                            $this->updateReferenceNumber($stlocationReferenceID);
                            $stnewInvoiceCode = $this->getReferenceNumber($stlocationReferenceID);
                        } else {
                            if ($suspendedTax) {
                                $invoiceCode = $stnewInvoiceCode;
                            } else {
                                $invoiceCode = $newInvoiceCode;
                            }
                        }
                    } else {
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_INVOICE_CODE_EXISTS')
                        );
                    }
                } else {
                    break;
                }
            }
        }

        if ($deliveryCharge != '') {
            $realItemTotalPrice = $totalPrice - $deliveryCharge;
        } else {
            $realItemTotalPrice = $totalPrice;
        }


        if ($serviceChargeAmount != 0 || $serviceChargeAmount != '') {
            $realItemTotalPrice = $realItemTotalPrice - $serviceChargeAmount;
        }


        if ($suspendedTax) {
            $realItemTotalPrice = $realItemTotalPrice + $totalTax;
        }
        if ($invoiceDiscoutType == 'presentage') {
            $totalInvoiceDiscount = ($totalInvoiceDiscountValue / (100 - $totalInvoiceDiscountValue)) * ($realItemTotalPrice - $totalTax);
        } else {
            $totalInvoiceDiscount = $totalInvoiceDiscountValue * $customCurrencyRate;
        }

        if ($pos) {
            $totalInvoiceDiscount = $param['invoiceTotalDiscount'] * $customCurrencyRate;
            $totalPromotionDiscountValue = $promotionDiscountValue * $customCurrencyRate;
        }

        if ($invoiceWisePromotionDiscountType == '2') {
            $realItemTotalPrice = ($realItemTotalPrice - ($totalTax));
            $totalPromotionDiscountValue = (($realItemTotalPrice * 100) / (100 - $promotionDiscountValue)) - $realItemTotalPrice;
        } else if ($invoiceWisePromotionDiscountType == '1') {
            $totalPromotionDiscountValue = $promotionDiscountValue * $customCurrencyRate;
        }

        if ($restFlag) {
            $entityID = $this->createEntityForRestApi($posUserID);
        } else {
            $entityID = $this->createEntity();
        }

        $statusName = 'Open';
        $statusID = $this->getStatusIDByStatusName($statusName);
        $invoiceData = array(
            'salesInvoiceCode' => $invoiceCode,
            'customerID' => $customerID,
            'locationID' => $locationOut,
            'salesInvoiceIssuedDate' => $invoiceDate,
            'salesInvoiceOverDueDate' => $dueDate,
            'salesinvoiceTotalAmount' => $totalPrice,
            'salesInvoiceComment' => $comment,
            'salesInvoiceDeliveryCharge' => $deliveryCharge,
            'paymentTermID' => $paymentTermID,
            'quotationID' => $quotationID,
            //'deliveryNoteID' => $deliveryNoteID,
            'salesOrderID' => $salesOrderID,
            'activityID' => $activityID,
            'jobID' => $jobID,
            'projectID' => $projectID,
            'salesInvoiceShowTax' => $showTax,
            'statusID' => $statusID,
            'entityID' => $entityID,
            'pos' => $pos,
            'salesPersonID' => null,
            'salesInvoicePayedAmount' => $salesInvoicePayedAmount,
            'salesInvoiceTotalDiscount' => $invoiceTotalDiscount,
            'salesInvoiceSuspendedTax' => $suspendedTax,
            'salesInvoiceDeliveryAddress' => $deliveryAddress,
            'promotionID' => $promotionID,
            'salesInvoicePromotionDiscount' => $totalPromotionDiscountValue,
            'salesInvoiceWiseTotalDiscount' => $totalInvoiceDiscount,
            'salesInvoiceTotalDiscountType' => $invoiceDiscoutType,
            'salesInvoiceDiscountRate' => $totalInvoiceDiscountValue,
            'customCurrencyId' => $customCurrencyId,
            'salesInvoiceCustomCurrencyRate' => $customCurrencyRate,
            'priceListId' => $priceListId,
            'salesInvoiceDeliveryChargeEnable' => $salesInvoiceDeliveryChargeEnable,
            'customerProfileID' => $customerProfID,
            'salesInvoiceTemporaryCode' => $invTempCode,
            'offlineSavedTime' => $param['offlineSavedTime'],
            'offlineUser' => $param['offlineUser'],
            'discountOnlyOnEligible' => $param['discountOnlyOnEligible'],
            'customerCurrentOutstanding' => $customerCurrentOutstanding,
            'inclusiveTax' => $inclusiveTax,
            'templateInvoiceTotal' => $finalTotalWithInclusiveTax,
            'salesInvoiceServiceChargeAmount' => $serviceChargeAmount,
            'linkedCusOrderNum'=> $linkedCusOrder
        );

        $invoiceExData = new Invoice;
        $invoiceExData->exchangeArray($invoiceData);
        $salesInvoiceID = $this->CommonTable('Invoice\Model\InvoiceTable')->saveInvoiceData($invoiceExData);
        //if tax suspended is true update tsinvoice reference number otherwise update invoice reference number
        if (!$edit) {
            if ($suspendedTax) {
                if ($stlocationReferenceID) {
                    $this->updateReferenceNumber($stlocationReferenceID);
                }
            } else {
                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }
            }
        }

        if (!is_null($nextInvoiceDate)) {
            $membershipData = [
                'invoiceID' => $salesInvoiceID,
                'nextInvoiceDate' => $this->convertDateToStandardFormat($nextInvoiceDate),
                'comment' => $nextInvoiceDateComment
            ];

            $memData = new Membership;
            $memData->exchangeArray($membershipData);
            $memID = $this->CommonTable('Invoice\Model\MembershipTable')->saveMembership($memData);

            if (!$memID) {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_MEMBERSHIP')
                );
            }
        }


        if (sizeof($addedServiceChargeArray) > 0) {
            foreach ($addedServiceChargeArray as $key1 => $value1) {
                $serviceChargeTypeData = $this->CommonTable('Invoice\Model\ServiceChargeTypeTable')->getServiceChargeTypeDetailsById($value1['serviceChargeTypeId'])->current();
                $serviceChargeData = [
                    'invoiceID' => $salesInvoiceID,
                    'serviceChargeTypeID' => $serviceChargeTypeData['serviceChargeTypeID'],
                    'serviceChargeAmount' => $value1['serviceChargeValueTxt'],
                ];

                $scData = new InvoiceServiceCharge;
                $scData->exchangeArray($serviceChargeData);

                $memID = $this->CommonTable('Invoice\Model\InvoiceServiceChargeTable')->save($scData);
            }
        }


        $totalCRAmounts = 0;
        unset($products['subProduct']);
        $totalProductAmounts = 0;
        $totalTaxAmount = 0;
        foreach ($products as $keyt => $pt) {
            if(isset($pt['pTax']['tTA'])){
                if($pos){
                    $ptwTax = ($pt['productTotal']);
                }else{
                    $ptwTax = ($pt['productTotal'] - $pt['pTax']['tTA']);
                }
                $totalTaxAmount += $pt['pTax']['tTA'];
            }else{
                $ptwTax = $pt['productTotal'];
            }
            $totalProductAmounts += $ptwTax;
        }

        if (!empty($salesPersonIDArr) && sizeof($salesPersonIDArr) > 0) {

            $valueForOneSalesPerson = $totalPrice / sizeof($salesPersonIDArr);

            foreach ($salesPersonIDArr as $key5 => $value5) {
                
                $salesPersonData = [
                    "invoiceID" => $salesInvoiceID,
                    "salesPersonID" => $value5,
                    "relatedSalesAmount" => $valueForOneSalesPerson
                ];


                $spData = new InvoiceSalesPersons;
                $spData->exchangeArray($salesPersonData);

                $spID = $this->CommonTable('Invoice\Model\InvoiceSalesPersonsTable')->saveSalesPerson($spData);
            }
        } 
        foreach ($products as $key => $p) {
            $batchProducts = [];
            if ($pos == false) {
                if (isset($p['selected_serials']) && isset($param['subProducts'][$key])) {
                    foreach ($p['selected_serials'] as $in => $srl) {
                        $a = array_search($srl, array_column($param['subProducts'][$key], 'serialID'));
                        $batchProducts[] = $param['subProducts'][$key][$a];
                    }
                } else {
                    $batchProducts = $param['subProducts'][$key];
                }
            } else {
                $batchProducts = $param['subProducts'][$p['productID']];
            }

            $displaySetupDetails = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();

            $hasFreeItem = false;

            if ($p['isFreeItem'] != 'true') {
                if (!empty($addedFreeIssueItems[$key])) {
                    $hasFreeItem = true;
                }
            }


            $productID = $p['productID'];
            $deliveryQty = $p['deliverQuantity']['qty'];
            $invoiceProductData = array(
                'salesInvoiceID' => $salesInvoiceID,
                'productID' => $productID,
                'locationProductID' => $p['locationProductID'],
                'salesInvoiceProductDescription' => $p['productDescription'],
                'salesInvoiceProductPrice' => $p['productPrice'] * $customCurrencyRate,
                'salesInvoiceProductDiscount' => ($p['productDiscountType'] == 'precentage') ? $p['productDiscount'] : $p['productDiscount'] * $customCurrencyRate,
                'salesInvoiceProductDiscountType' => $p['productDiscountType'],
                'salesInvoiceProductTotal' => $p['productTotal'] * $customCurrencyRate,
                'salesInvoiceProductQuantity' => $deliveryQty,
                'itemSalesPersonID' => (!empty($p['itemSalesPerson']) && $p['itemSalesPerson'] != 0 && $p['itemSalesPerson'] != null) ? $p['itemSalesPerson'] : null,
                'documentTypeID' => $p['documentTypeID'],
                'salesInvoiceProductDocumentID' => $p['documentID'],
                'salesInvoiceProductSelectedUomId' => $p['selectedUomId'],
                'inclusiveTaxSalesInvoiceProductPrice' => $p['inclusiveTaxProductPrice'] * $customCurrencyRate,
                'salesInvoiceProductMrpType' =>  ($displaySetupDetails['useMrpSettings'] == 1) ? $p['mrpType'] : null, 
                'salesInvoiceProductMrpValue' => ($displaySetupDetails['useMrpSettings'] == 1) ? $p['mrpValue'] : null,
                'salesInvoiceProductMrpPercentage' => ($displaySetupDetails['useMrpSettings'] == 1) ? $p['mrpPercentageValue'] : null,
                'salesInvoiceProductMrpAmount' => ($displaySetupDetails['useMrpSettings'] == 1) ? $p['actmrpValue']: null,
                'isFreeItem' => ($p['isFreeItem'] == 'true') ? true: false,
                'hasFreeItem' => $hasFreeItem
            );
            //calculate discount value
            $discountValue = 0;
            if ($p['productDiscountType'] == "precentage") {
                $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $customCurrencyRate;
            } else if ($p['productDiscountType'] == "value") {
                $discountValue = $p['productDiscount'] * $customCurrencyRate;
            }

            $invoiceProduct = new InvoiceProduct;
            $invoiceProduct->exchangeArray($invoiceProductData);
            $invoiceProductID = $this->CommonTable('Invoice\Model\InvoiceProductTable')->saveInvoiceProduct($invoiceProduct);

            if ($hasFreeItem) {
                $freeItems = $addedFreeIssueItems[$key];

                foreach ($freeItems as $key5 => $value5) {
                    if (!empty($value5['incVal'])) {
                        $k = $value5['productID'].'_'.$value5['incVal'];
                        $relatedItem = $products[$k];

                        $freeItem = [
                            'invoiceID' => $salesInvoiceID,
                            'mainProductID' => $productID,
                            'mainSalesInvoiceProductID' => $invoiceProductID,
                            'freeProductID' => $value5['productID'] ,
                            'freeQuantity' => $relatedItem['deliverQuantity']['qty'],
                            'salesInvoiceFreeItemSelectedUom' => $relatedItem['selectedUomId'],
                            'locationID' => $relatedItem['locationID'],

                        ];
                    } else {
                        $freeItem = [
                            'invoiceID' => $salesInvoiceID,
                            'mainProductID' => $productID,
                            'mainSalesInvoiceProductID' => $invoiceProductID,
                            'freeProductID' => $value5['freeProductID'] ,
                            'freeQuantity' => $value5['freeQuantity'],
                            'salesInvoiceFreeItemSelectedUom' => $value5['salesInvoiceFreeItemSelectedUom'],
                            'locationID' => $value5['locationID'],
                        ];
                    }

                    $freeItemProduct = new InvoiceFreeProduct;
                    $freeItemProduct->exchangeArray($freeItem);

                    $freeItemProductID = $this->CommonTable('Invoice\Model\InvoiceFreeProductTable')->saveInvoiceFreeProduct($freeItemProduct);
                }
            }

            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $p['locationID']);
            $locationPID = $locationProduct->locationProductID;
            $averageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
            if ($p['productType'] != 2 && $p['stockUpdate'] == 'true' && $locationProduct->locationProductQuantity < $deliveryQty && $p['documentTypeID'] != 19) {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                );
            }
            $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

            if($this->useAccounting == 1 && !$isSetJournalEntry){
                if($p['giftCard'] != 1 && $p['productType'] != 2){
                    if($p['stockUpdate'] == 'true' && empty($pData['productInventoryAccountID'])){
                        return array(
                            'status'=> false,
                            'msg'=> $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                            );
                    }
                         //check whether Product Cost of Goods Sold Gl account id set or not
                    if($p['stockUpdate'] == 'true' && empty($pData['productCOGSAccountID'])){
                        return array(
                            'status'=> false,
                            'msg'=> $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName'])),
                            );
                    }
                }

                if(!$custSalesAccountIsset){
                    if(empty($pData['productSalesAccountID'])){
                        return array(
                            'status'=> false,
                            'msg'=> $this->getMessage('ERR_PRPODUCT_SALES_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                            );
                    }
                }

                //set gl accounts for the journal entry
                //TODO after set the productTotal with tax value in pos should have to remove belowe condition the correct $pTotal is $p['productTotal'] - $p['pTax']['tTA'];
                if($pos){
                    $pTotal = $p['productTotal'];
                }else{
                    $pTotal = $p['productTotal'] - $p['pTax']['tTA'];
                }
                if($invoiceData['salesInvoiceTotalDiscountType'] == "Value"){
                    $productTotal = $pTotal - $invoiceData['salesInvoiceDiscountRate']*($pTotal/$totalProductAmounts);
                }else {
                    $productTotal = $pTotal;
                }

                if($promotionID != '' && $promotionData[$promotionID]['promoType'] == 2 && $promotionData[$promotionID]['discountType'] == 1){
                    $productTotal = $productTotal - $invoiceData['salesInvoicePromotionDiscount']*($productTotal/$totalProductAmounts);
                }

                if($p['giftCard'] !=1){
                    $totalCRAmounts+=$productTotal;
                    // if($custSalesAccountIsset){
                        // if(isset($accountProduct[$customerData->customerSalesAccountID]['creditTotal'])){
                        //     $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] += $productTotal;
                        // }else{
                        //     $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] = $productTotal;
                        //     $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] = 0.00;
                        //     $accountProduct[$customerData->customerSalesAccountID]['accountID'] = $customerData->customerSalesAccountID;
                        // }
                    // }else{
                    if(isset($accountProduct[$pData['productSalesAccountID']]['creditTotal'])){
                        $accountProduct[$pData['productSalesAccountID']]['creditTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productSalesAccountID']]['creditTotal'] = $productTotal;
                        $accountProduct[$pData['productSalesAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$pData['productSalesAccountID']]['accountID'] = $pData['productSalesAccountID'];
                    }
                    // }
                }
            }
            if ($p['stockUpdate'] == 'true' && $p['productType'] != 2) {
                if ($p['documentTypeID'] != 19) {
                    $locationProductData = array(
                        'locationProductQuantity' => $locationProduct->locationProductQuantity - $deliveryQty,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductData, $productID, $p['locationID']);
                }
            }
            if ($p['pTax'] && $p['pTax']['tL']) {
                foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                    $invoicePTaxesData = array(
                        'salesInvoiceProductID' => $invoiceProductID,
                        'productID' => $productID,
                        'taxID' => $taxKey,
                        'salesInvoiceProductTaxPrecentage' => $productTax['tP'],
                        'salesInvoiceProductTaxAmount' => $productTax['tA'] * $customCurrencyRate
                    );
                    $invoicePTaxM = new InvoiceProductTax();
                    $invoicePTaxM->exchangeArray($invoicePTaxesData);
                    $this->CommonTable('Invoice\Model\InvoiceProductTaxTable')->saveInvoiceProductTax($invoicePTaxM);

                    $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxKey);
                    if($this->useAccounting == 1 && empty($taxData['taxSalesAccountID']) && !$isSetJournalEntry){
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_TAX_SALES_ACCOUNT', array($taxData['taxName']))
                            );
                    }

                    $totalCRAmounts += $productTax['tA'] * $customCurrencyRate;
                    //set tax gl accounts for the journal Entry
                    if ($pos) {
                        if(isset($accountProduct[$taxData['taxSalesAccountID']]['creditTotal'])){
                            $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] += $productTax['tA'] * $customCurrencyRate;
                        }else{
                            $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = $productTax['tA'] * $customCurrencyRate;
                            $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = 0.00;
                            $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                        }
                    } else {
                        if (($productTax['susTax'] == '0' && $param['suspendedTax'] == '1') || ($productTax['susTax'] == '0' && $param['suspendedTax'] == '0') || ($productTax['susTax'] == '1' && $param['suspendedTax'] == '0')) {
                            if(isset($accountProduct[$taxData['taxSalesAccountID']]['creditTotal'])){
                                $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] += $productTax['tA'] * $customCurrencyRate;
                            }else{
                                $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = $productTax['tA'] * $customCurrencyRate;
                                $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                            }
                        }
                    }
                }
            }
            //if copied from SalesOrder or DeliveryNote or quotation,update those products as copied.
            if ($fromSalesOrderOrDeliveryNote == TRUE) {
                if ($p['stockUpdate'] == 'true' && !empty($salesOrderID)) {
                    $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderIDAndLocationProductId($salesOrderID, $locationPID, $p['productPrice']);
                    if ($salesOrderProducts) {
                        $newCopiedQuantity = $salesOrderProducts->salesOrdersProductCopiedQuantity + (float) $deliveryQty;
                        $copiedflag = 0;
                        if ($newCopiedQuantity >= $salesOrderProducts->quantity) {
                            $newCopiedQuantity = $salesOrderProducts->quantity;
                            $copiedflag = 1;
                        }
                        $soProData = array(
                            'copied' => $copiedflag,
                            'salesOrdersProductCopiedQuantity' => $newCopiedQuantity,
                        );
                        $this->CommonTable('Invoice\Model\SalesOrderProductTable')->updateCopiedSalesOrderProducts($locationPID, $salesOrderID, $soProData);
                    }
                } else if (($p['documentTypeID'] == 4 && !empty($p['documentID'])) && $p['stockUpdate'] == 'false') {
                    $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($p['documentID'], $productID);
                    if (isset($deliveryNoteProducts)) {
                        $newCopiedQuantity = $deliveryNoteProducts->deliveryNoteProductCopiedQuantity + (float) $deliveryQty;
                        $copiedflag = 0;
                        if ($newCopiedQuantity == $deliveryNoteProducts->deliveryNoteProductQuantity) {
                            $newCopiedQuantity = $deliveryNoteProducts->deliveryNoteProductQuantity;
                            $copiedflag = 1;
                        } else if ($newCopiedQuantity > $deliveryNoteProducts->deliveryNoteProductQuantity) {
                            return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_DELI_PRODUCT_QUAN_CHECK')
                            );
                        }
                        $deliveryNoteProData = array(
                            'copied' => $copiedflag,
                            'deliveryNoteProductCopiedQuantity' => $newCopiedQuantity,
                        );
                        $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryNoteProducts($productID, $p['documentID'], $deliveryNoteProData);
                        $this->_checkAndUpdateDeliveryNoteStatus($p['documentID']);
                    }
                    if (count($batchProducts) > 0) {
                        foreach ($batchProducts as $batchValue) {
                            if ($batchValue['serialID'] != '') {
                                $deliveryNoteSubProducts = $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->getProductSerialByDeliveryNoteProductIDAndProductSerialID($deliveryNoteProducts->deliveryNoteProductID, $batchValue['serialID']);
                                $deliveryNoteProData = array(
                                    'deliveryNoteSubProductsCopiedQuantity' => 1,
                                );
                            } else {
                                $deliveryNoteSubProducts = $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->getProductBatchByDeliveryNoteProductIDAndProductBatchID($deliveryNoteProducts->deliveryNoteProductID, $batchValue['batchID']);
                                $newCopiedQuantity = $deliveryNoteSubProducts->deliveryNoteSubProductsCopiedQuantity + (float) $batchValue['qtyByBase'];

                                $deliveryNoteProData = array(
                                    'deliveryNoteSubProductsCopiedQuantity' => $newCopiedQuantity,
                                );
                            }
                            $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->updateDeliveryNoteSubProductData($deliveryNoteProData, $deliveryNoteSubProducts->deliveryNoteSubProductID);
                        }
                    }
                } else if (!empty($quotationID)) {
                    $quotationProducts = $this->CommonTable('Invoice\Model\QuotationProductTable')->getQuotationProductByQuotationIdAndLocationProductId($quotationID, $locationPID);
                    if (isset($quotationProducts)) {
                        $newCopiedQuantity = $quotationProducts->quotationProductCopiedQuantity + (float) $deliveryQty;
                        $copiedflag = 0;
                        if ($newCopiedQuantity >= $quotationProducts->quotationProductQuantity) {
                            $newCopiedQuantity = $quotationProducts->quotationProductQuantity;
                            $copiedflag = 1;
                        }
                        $quoProData = array(
                            'quotationProductCopied' => $copiedflag,
                            'quotationProductCopiedQuantity' => $newCopiedQuantity,
                        );
                        $this->CommonTable('Invoice\Model\QuotationProductTable')->updateCopiedQuotationProducts($locationPID, $quotationID, $quoProData);
                    }
                } else if ($p['documentTypeID'] == 19) {
                    $resJob = $this->CommonTable('Invoice\Model\InvoiceProductDeliveryNoteProductTable')->getJobMappingByInvoiceProductIdAndJobId($p['invoiceProductID'], $p['documentID']);
                    foreach ($resJob as $jPro) {
                        $jobDelivData = [
                            'invoiceProductID' => $invoiceProductID
                        ];
                        $res = $this->CommonTable('Invoice\Model\InvoiceProductDeliveryNoteProductTable')->update($jobDelivData, $jPro['invoiceProductDeliveryNoteProductID']);
                    }
                }
            }
            if (!empty($p['isBom'] && $p['isBom'] == "true" && $p['productType'] == 2 )) {
                // if ($p['isBomJeSubItem']) {

                    //get related bom Item
                    $res = $this->CommonTable('Inventory\Model\BomTable')->getBomDataByProductID($productID)->current();

                    // if ($res['isJEPostSubItemWise']) {
                        $subItemData = $this->CommonTable('Inventory\Model\BomSubItemTable')->getBomSubDataByBomID($res['bomID']);
                        //check all sub items are non inventory or not
                        $nonInventoryCount = 0;
                        foreach ($subItemData as $key => $value) {

                            $invoicedBomSubProductData = array(
                                'salesInvoiceID' => $salesInvoiceID,
                                'salesInvoiceProductID' => $invoiceProductID,
                                'bomID' => $value['bomID'],
                                'subProductID' => $value['productID'],
                                'subProductBomPrice' => $value['bomSubItemUnitPrice'] * $customCurrencyRate,
                                'subProductBomQty' => $value['bomSubItemQuantity'] * $value['productUomConversion'],
                                'subProductTotalQty' => $value['bomSubItemQuantity'] * $value['productUomConversion'] * ($deliveryQty),
                                'subProductTotal' => $value['bomSubItemUnitPrice'] * $customCurrencyRate * ($deliveryQty),
                            );

                            $invoicedBomSubItems = new InvoicedBomSubItems;
                            $invoicedBomSubItems->exchangeArray($invoicedBomSubProductData);
                            $invoicedBomSubProductID = $this->CommonTable('Invoice\Model\InvoicedBomSubItemsTable')->saveInvoicedBomSubProduct($invoicedBomSubItems);

                            if ($value['productTypeID'] == 2){
                                $nonInventoryCount ++;
                            }
                        }
                        if (sizeof($subItemData) != $nonInventoryCount) {
                            //Todo assemble the with inventory items and handle je
                        }
                    // }
                // }
            } 

            if ($p['productType'] != 2 && $p['documentTypeID'] != 19) {
                if ($p['stockUpdate'] == 'true') {

                    if (!count($batchProducts) > 0) {
                        $sellingQty = $deliveryQty;
                        while ($sellingQty != 0) {

                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($locationProduct->locationProductID);
                            if (!$itemInDetails) {
                                break;
                            }
                            if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Sales Invoice',
                                    'itemOutDocumentID' => $salesInvoiceID,
                                    'itemOutLocationProductID' => $locationProduct->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $sellingQty,
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                if($this->useAccounting == 1 && !$isSetJournalEntry){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = $sellingQty * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $sellingQty * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);

                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }

                                $sellingQty = 0;
                                break;
                            } else {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Sales Invoice',
                                    'itemOutDocumentID' => $salesInvoiceID,
                                    'itemOutLocationProductID' => $locationProduct->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                if($this->useAccounting == 1 && !$isSetJournalEntry){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal =  $itemInDetails['itemInRemainingQty'] * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $itemInDetails['itemInRemainingQty'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }

                                $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                            }
                        }
                    }
                }
            }
            if (count($batchProducts) > 0) {
                foreach ($batchProducts as $batchKey => $batchValue) {
                    if(!empty($batchValue)){
                        $this->saveInvoiceSubProductData($batchValue, $invoiceProductID);
                    //insert data to the item out table
                        if ($p['stockUpdate'] == 'true') {
                            if ($batchValue['batchID']) {
                                if ($batchValue['serialID']) {
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProduct->locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                    $itemOutM = new ItemOut();
                                    $itemOutData = array(
                                        'itemOutDocumentType' => 'Sales Invoice',
                                        'itemOutDocumentID' => $salesInvoiceID,
                                        'itemOutLocationProductID' => $locationProduct->locationProductID,
                                        'itemOutBatchID' => $batchValue['batchID'],
                                        'itemOutSerialID' => $batchValue['serialID'],
                                        'itemOutItemInID' => $itemInDetails['itemInID'],
                                        'itemOutQty' => $batchValue['qtyByBase'],
                                        'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                        'itemOutDiscount' => $discountValue,
                                        'itemOutAverageCostingPrice' => $averageCostingPrice,
                                        'itemOutDateAndTime' => $this->getGMTDateTime()
                                        );
                                    $itemOutM->exchangeArray($itemOutData);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                    if($this->useAccounting == 1 && !$isSetJournalEntry){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                } else {
                                    $remainBatchQty = $batchValue['qtyByBase'];

                                    //check current batch qty zero or not

                                    while($remainBatchQty != 0) {

                                        //get first avilable batch record in itemin table
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProduct->locationProductID, $batchValue['batchID']);

                                        $itemOutM = new ItemOut();
                                        $itemOutData = array(
                                            'itemOutDocumentType' => 'Sales Invoice',
                                            'itemOutDocumentID' => $salesInvoiceID,
                                            'itemOutLocationProductID' => $locationProduct->locationProductID,
                                            'itemOutBatchID' => $batchValue['batchID'],
                                            'itemOutSerialID' => NULL,
                                            'itemOutItemInID' => $itemInDetails['itemInID'],
                                            'itemOutQty' => $batchValue['qtyByBase'],
                                            'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                            'itemOutDiscount' => $discountValue,
                                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                                            'itemOutDateAndTime' => $this->getGMTDateTime()
                                            );
                                        //check existing record has enough qty
                                        if ($itemInDetails['itemInRemainingQty'] > $remainBatchQty) {
                                            $itemOutData['itemOutQty'] = $remainBatchQty;
                                            $updatedBatchQty = $remainBatchQty;    
                                            $remainBatchQty = 0;
                                        } else {

                                            if (!$itemInDetails['itemInRemainingQty']) {
                                                return array(
                                                    'status' => false,
                                                    'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                                                );
                                            }

                                            $itemOutData['itemOutQty'] = $itemInDetails['itemInRemainingQty'];
                                            $remainBatchQty = $remainBatchQty - $itemInDetails['itemInRemainingQty'];
                                            $updatedBatchQty = $itemInDetails['itemInRemainingQty'];
                                        }
                                        
                                        $itemOutM->exchangeArray($itemOutData);
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                        $updateQty = $itemInDetails['itemInSoldQty'] + $updatedBatchQty;
                                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                          
                                    }

                                    if($this->useAccounting == 1 && !$isSetJournalEntry){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                }
                            } else if ($batchValue['serialID']) {
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProduct->locationProductID, $batchValue['serialID']);
                                if (is_null($itemInDetails)) {
                                    return array(
                                        'status' => false,
                                        'msg' => $this->getMessage('ERR_INVOICE_SERIAL_ID', [$p['productCode']])
                                        );
                                }
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Sales Invoice',
                                    'itemOutDocumentID' => $salesInvoiceID,
                                    'itemOutLocationProductID' => $locationProduct->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => $batchValue['serialID'],
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => 1,
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );

                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);

                                if($this->useAccounting == 1 && $p['giftCard'] != 1 && !$isSetJournalEntry){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = 1 * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }
                            }
                        }
                    //////////Item out insert data END /////////
                        if ($p['stockUpdate'] == 'true' && $p['productType'] != 2) {
                            if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                                $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                                if ($batchProduct->productBatchQuantity < $batchValue['qtyByBase']) {
                                    return array(
                                        'status' => false,
                                        'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                                        );
                                }
                                $productBatchQuentity = $batchProduct->productBatchQuantity - $batchValue['qtyByBase'];
                                $productBatchQty = array(
                                    'productBatchQuantity' => $productBatchQuentity,
                                    );
                                $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                                if ($batchValue['serialID']) {
                                    $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                                    if ($checkSerial->locationProductID != $locationProduct->locationProductID || $checkSerial->productSerialSold == 1) {

                                        return array(
                                            'status' => false,
                                            'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                                            );
                                    }
                                    $serialProductData = array(
                                        'productSerialSold' => '1',
                                        );
                                    $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                                }
                            } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {

                                $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                                if ($checkSerial->locationProductID != $locationProduct->locationProductID || $checkSerial->productSerialSold == 1) {
                                    return array(
                                        'status' => false,
                                        'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                                        );
                                }
                                $serialProductData = array(
                                    'productSerialSold' => '1',
                                    );
                                $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                                if ($p['giftCard'] == 1) {
                                    $psCode = $checkSerial->productSerialCode;
                                    $giftCardDetails = $this->CommonTable('Inventory\Model\GiftCardTable')->getGiftCardDetailsByGiftCardCode($psCode);
                                    $duration = $giftCardDetails->giftCardDuration;
                                    $issueDate = $invoiceDate;
                                    $expireDate = ($duration != 0) ? $this->convertDateToStandardFormat(date('Y-m-d', strtotime($issueDate . ' + ' . $duration . 'days'))) : NULL;
                                    $giftData = array(
                                        'giftCardIssueDate' => $issueDate,
                                        'giftCardExpireDate' => $expireDate,
                                        'giftCardStatus' => 3,
                                        );

                                    $this->CommonTable('Inventory\Model\GiftCardTable')->updateGiftCardDetailsByGiftCardCode($giftData, $psCode);


                                }
                            }
                        }
                    }
                }

                if($this->useAccounting == 1 && !$isSetJournalEntry){
                    if($p['giftCard'] == 1 && empty($giftCardDetails->giftCardGlAccountID)){
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_GIFTCARD_GL_ACCOUNT_NOT_SET', [$psCode])
                            );
                    }else if($p['giftCard'] == 1){
                                        //set gl accounts for the journal entry
                    //TODO after set the productTotal with tax value in pos should have to remove belowe condition the correct $pTotal is $p['productTotal'] - $p['pTax']['tTA'];
                        if($pos){
                            $pTotal = $p['productTotal'];
                        }else{
                            $pTotal = $p['productTotal'] - $p['pTax']['tTA'];
                        }
                        if($invoiceData['salesInvoiceTotalDiscountType'] == "Value"){
                            $productTotal = $pTotal - $invoiceData['salesInvoiceDiscountRate']*($pTotal/$totalProductAmounts);
                        }else {
                            $productTotal = $pTotal;
                        }
                        if($promotionID != '' && $promotionData[$promotionID]['promoType'] == 2 && $promotionData[$promotionID]['discountType'] == 1){
                            $productTotal = $productTotal - $invoiceData['salesInvoicePromotionDiscount']*($productTotal/$totalProductAmounts);
                        }

                        $totalCRAmounts+=$productTotal;
                        if(isset($accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'])){
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'] += $productTotal;
                        }else{
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'] = $productTotal;
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['debitTotal'] = 0.00;
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['accountID'] = $giftCardDetails->giftCardGlAccountID;
                        }
                    }
                }
            }

            //update unitprice on itemOut Table
            $itemOutDocumentType = NULL;
            $referenceID = NULL;
            if ($projectID != NULL || $jobID != NULL || $activityID != NULL) {

                $getActivity = array();
                if ($projectID) {

                    $referenceID = $projectID;
                    $itemOutDocumentType = 'Project';
                    $getActivity = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByProjectID($projectID);
                    $unitPrice = $p['productPrice'] * $customCurrencyRate;
                    foreach ($getActivity as $value) {
                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateJobCardUnitPrice($referenceID, $unitPrice, $itemOutDocumentType, $value['activityId']);
                    }
                } else if ($jobID) {

                    $referenceID = $jobID;
                    $itemOutDocumentType = 'Job';
                    $getActivity = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID);
                    $unitPrice = $p['productPrice'] * $customCurrencyRate;
                    foreach ($getActivity as $value) {
                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateJobCardUnitPrice($referenceID, $unitPrice, $itemOutDocumentType, $value['activityId']);
                    }
                } else if ($activityID) {

                    $referenceID = $activityID;
                    $itemOutDocumentType = 'Activity';
                    $getActivity = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByActivityID($activityID);
                    $unitPrice = $p['productPrice'] * $customCurrencyRate;
                    foreach ($getActivity as $value) {
                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateJobCardUnitPrice($referenceID, $unitPrice, $itemOutDocumentType, $value['activityId']);
                    }
                }
            }

            //update item out table unit price if it is delivery note
            //and if delivery note unit price is change when saving with changing unit price on invoice by delivery note
            if ($p['documentTypeID'] == 4 && $p['documentID'] != NULL) {
                $locationProductData = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProduct($productID, $p['locationID']);
                $price = $p['productPrice'] * $customCurrencyRate;
                $locationProductID = $locationProductData->locationProductID;

                $data = array(
                    'itemOutPrice' => $price,
                );
                $this->CommonTable("Inventory\Model\ItemOutTable")->updateDeliveryNoteUnitPrice($data, $locationProductID, $p['documentID']);
            }
        }
         //set gl accounts for the journal entry
        $deliveryTotal = $deliveryCharge;
        if($this->useAccounting == 1 && !$isSetJournalEntry){
            $totalCRAmounts += $deliveryCharge;
            if($deliveryTotal > 0){
                if(isset($accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'])){
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] += $deliveryTotal;
                }else{
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] = $deliveryTotal;
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['debitTotal'] = 0.00;
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['accountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                }
            }

            if(sizeof($addedServiceChargeArray) > 0 && $serviceChargeAmount > 0){
                foreach ($addedServiceChargeArray as $key => $value) {
                    $totalCRAmounts += $value['serviceChargeValueTxt'];
                    $serviceChargeTypeData =  $this->CommonTable("Invoice\Model\ServiceChargeTypeTable")->getServiceChargeTypeDetailsById($value['serviceChargeTypeId'])->current();

                    if(isset($accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['creditTotal'])){
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['creditTotal'] += $value['serviceChargeValueTxt'];
                    }else{
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['creditTotal'] = $value['serviceChargeValueTxt'];
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['accountID'] = $serviceChargeTypeData['serviceChargeTypeGlAccountID'];
                    }
                }
            }


            if ($param['suspendedTax'] == '1' && $pos == false) {
                $totalCRAmounts -= floatval($param['susTaxAmount']);
            }

            if(isset($accountProduct[$customerData->customerReceviableAccountID]['debitTotal'])){
                $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] += $totalCRAmounts;
            }else{
                $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] = $totalCRAmounts;
                $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] = 0.00;
                $accountProduct[$customerData->customerReceviableAccountID]['accountID'] = $customerData->customerReceviableAccountID;
            }
        }

        //update Sales Order status or Delivery Note status if copied from it
        if ($fromSalesOrderOrDeliveryNote == TRUE) {
            if (!empty($salesOrderID)) {
                $this->_checkAndUpdateSalesOrderStatus($salesOrderID);
            } else if (!empty($deliveryNoteID)) {
                //$this->_checkAndUpdateDeliveryNoteStatus($deliveryNoteID);
            } else if (!empty($activityID)) {
                $this->updateActivityStatus($activityID, $products);
            } else if (!empty($jobID)) {
                // $this->updateJobStatus($jobID, $products);
            } else if (!empty($projectID)) {
                $this->updateProjectStatus($projectID, $products);
            } else if (!empty($quotationID)) {
                $quotationProducts = $this->CommonTable('Invoice\Model\QuotationProductTable')->getUnCopiedQuotationProducts($quotationID);
                if (empty($quotationProducts)) {
                    $closeStatusID = $this->getStatusID('closed');
                    $this->CommonTable('Invoice\Model\QuotationTable')->updateQuotationStatus($quotationID, $closeStatusID);
                }
            }
        }
        $this->setLogMessage('total amount ' . $customCurrencySymbol . $totalPrice . ' of invoice successfully saved - ' . $invoiceCode);

        //call product updated event
        $productIDs = array_map(function($element) {
            return $element['productID'];
        }, $products);

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationOut]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

        if ($this->useAccounting == 1 ) {
                    //create data array for the journal entry save.
            if(!$isSetJournalEntry){

                $i=0;
                $journalEntryAccounts = array();

                foreach ($accountProduct as $key => $value) {
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Invoice '.$invoiceCode.'.';
                    $i++;
                }
                    //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $invoiceDate,
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create Invoice '.$invoiceCode.'.',
                    'documentTypeID' => 1,
                    'journalEntryDocumentID' => $salesInvoiceID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                    );

            }else{

                $journalEntryAccounts = array();

                foreach ($journalEntryData['journalEntryAccounts'] as $key => $value) {
                    if(empty($value['financeAccountsID'])){
                        return array(
                            'status'=> false,
                            'msg'=>$this->getMessage("ERR_PLEASE_SET_GL_ACCOUNT_IN_JOURNAL_ENTRY"),
                        );
                    }

                    $journalEntryAccounts[$key]['fAccountsIncID'] = $key;
                    $journalEntryAccounts[$key]['financeAccountsID'] = $value['financeAccountsID'];
                    $journalEntryAccounts[$key]['financeGroupsID'] = '';
                    $journalEntryAccounts[$key]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsDebitAmount'];
                    $journalEntryAccounts[$key]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsCreditAmount'];
                    $journalEntryAccounts[$key]['journalEntryAccountsMemo'] = $value['journalEntryAccountsMemo'];
                }
                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $journalEntryData['journalEntryDate'],
                    'journalEntryCode' => $journalEntryData['journalEntryCode'],
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => $journalEntryData['journalEntryComment'],
                    'documentTypeID' => 1,
                    'journalEntryDocumentID' => $salesInvoiceID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                    );
            }


            $resultData = $this->saveJournalEntry($journalEntryData, null, null, $restFlag, $locationOut);

            if(!$resultData['status']){
                return array(
                    'status'=> false,
                    'data' => $resultData['data'],
                    'msg'=>$resultData['msg'],
                    );
            }

            if (!empty($dimensionData)) {
                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$invoiceCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                if(!$saveRes['status']){
                    return array(
                        'status'=> false,
                        'msg'=>$saveRes['msg'],
                        'data'=>$saveRes['data'],
                        );
                }   
            }

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($salesInvoiceID,1, 3);
            if(!$jEDocStatusUpdate['status']){
                return array(
                    'status'=> false,
                    'msg'=>$jEDocStatusUpdate['msg'],
                    );
            }            
        }

        if(!$edit && $customerID != 0){
            if($posModeFlag){
                $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

                if ($displaySettings->smsServiceStatus) {
                    
                    if ($cust->customerTelephoneNumber != "") {
                        $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("After Invoice");
                        
                        if($smsTypeDetails->isEnable && (int)$smsTypeDetails->smsSendType == 1){
                            
                            $message = $smsTypeDetails->smsTypeMessage ."\n";

                            $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);
                            $salesPerson = '';
                            if( sizeof($salesPersonIDArr) > 0){
                                foreach ($salesPersonIDArr as $key => $value) {
                                    $salesPersonData =  $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByID($value)->current();
                                    if(sizeof($salesPersonIDArr) == 1){
                                        $salesPerson = $salesPersonData["salesPersonSortName"];
                                    }else{
                                        $salesPerson .= $salesPersonData["salesPersonSortName"] .', ';
                                    }
                                }
                            }
                            
                            if($smsTypeDetails->isIncludeInvoiceDetails){
                                while ($row = $smsIncludedDetailsByType->current()) {
                                    if((bool) $row['isSelected']){
                                        switch ($row['smsIncludedName']) {
                                            case "Invoice Date":
                                                $message .=$row['smsIncludedName'] .' : '.$invoiceDate."\n";
                                                break;
                                            case "Invoice Number":
                                                $message .=$row['smsIncludedName'] .' : '.$invoiceCode."\n";
                                                break;
                                            case "Invoice Amount":
                                                $message .=$row['smsIncludedName'] .' : '.number_format($totalPrice , 2)."\n";
                                                break;
                                            case "Due Date":
                                                $message .=$row['smsIncludedName'] .' : '.$dueDate."\n";
                                                break;
                                            case "Sales Person":
                                                if(!$pos){
                                                    if( sizeof($salesPersonIDArr) > 0){
                                                        $message .=$row['smsIncludedName'] .' : '.$salesPerson."\n";
                                                    }
                                                }
                                                else
                                                {
                                                    $message .=$row['smsIncludedName'] .' : '.$this->user_session->username."\n";
                                                }
                                                break;
                                            default:
                                                break;
                                        }
                                    }
                                }
                            }
                            $sendRes = $this->sendSms($cust->customerTelephoneNumber, $message);
                        }
                    }
                }
            }
        }
        return array(
            'status' => true,
            'data' => array('salesInvoiceID' => $salesInvoiceID, 'salesInvoiceCode' => $invoiceCode),
            'msg' => $this->getMessage('SUCC_INVOICE_ADD_INVO_PRODETAILS', array($invoiceCode)),
            'totalPrice' => $totalPrice
        );
    }

    public function saveInvoiceDetailsAction()
    {      
        $request = $this->getRequest();
        if ($request->isPost()) {//  $message = "Dear ".$customer->customerName.", Thank you for your payment of ".$customCurrencyData->currencySymbol." ".number_format($amount,2)." for the invoice(s) ".$invoiceCodesForPayment.". Remaining outstanding balance: ".$customCurrencyData->currencySymbol." ".number_format($newCurrentBalance,2);

            $post = $request->getPost();
            // $isGoThroughTheWf = false;
            if ($post['isActiveWF'] == 1) {

                if (!empty($post['wfID'])) {
                    $limitData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->getLimitsByWFId($post['wfID']);
                    $limitID = null;


                    foreach ($limitData as $key => $value) {
                        if (floatval($post['invoiceTotalPrice']) <= floatval($value['approvalWorkflowLimitMax']) && floatval($post['invoiceTotalPrice']) >= floatval($value['approvalWorkflowLimitMin'])) {
                            $limitID = $value['approvalWorkflowLimitsID'];
                            break;
                        }
                    }

                    if ($limitID != null) {
                        $limitApproversData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->getLimitApproversByWFLimitId($limitID);
                        $approvers = [];

                        foreach ($limitApproversData as $key => $value) {
                            $approvers[] = $value['workflowLimitApprover'];
                        }

                        $customerID = ($request->getPost('customer')) ? $request->getPost('customer') : 0;
                        $param = $request->getPost();
                        $this->beginTransaction();

                        $saveInvDraft = $this->saveInvDraft($post, $customerID, false, false);

                        if (!$saveInvDraft['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $saveInvDraft['msg'];
                            return $this->JSONRespond();
                        } else {
                            $this->commit();
                            $this->status = true;
                            $this->data = array('invID' => $saveInvDraft['data']['invID'], 'approvers' => $approvers, 'hashValue' => $saveInvDraft['data']['hashValue'], 'invCode' => $saveInvDraft['data']['invCode'], 'isDraft' => true);
                            $this->msg = $this->getMessage('SUCC_INV_WF');
                            return $this->JSONRespond();
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('SELECTED_APPROVAL_WF_RANGES_MISMATCH_INV');
                        return $this->JSONRespond();
                    }
                }

            } else {
                $locationID = $this->user_session->userActiveLocation['locationID'];
                $customerID = ($request->getPost('customer')) ? $request->getPost('customer') : NULL;
                $param = $request->getPost();

                if (!$customerID) {
                    $this->beginTransaction();
                    //default customer ID 0 is sent to the database
                    $res = $this->storeInvoice($param, 0, false, false);
                    if ($res['status']) {
                        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID(0);
                        $custNewCurrentBalance = $cust->customerCurrentBalance + $res['totalPrice'];
                        $custData = array(
                            'customerCurrentBalance' => $custNewCurrentBalance,
                            'customerID' => '0'
                        );
                        $customerAllData = new Customer;
                        $customerAllData->exchangeArray($custData);

                        $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);

                        $this->commit();
                        $this->status = true;
                        $this->msg = $res['msg'];
                        $this->data = $res['data'];

                        return $this->JSONRespond();
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $res['msg'];
                        $this->data = $res['data'];

                        return $this->JSONRespond();
                    }
                } else {
                    $customerName = $request->getPost('customerName');
                    $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                    if ($cust->customerName . '-' . $cust->customerCode == $customerName) {

                        $this->beginTransaction();

                        $res = $this->storeInvoice($param, $customerID, false, false);
                        if ($res['status']) {

                            $custNewCurrentBalance = $cust->customerCurrentBalance + $res['totalPrice'];
                            $custData = array(
                                'customerCurrentBalance' => $custNewCurrentBalance,
                                'customerID' => $customerID
                            );
                            $customerAllData = new Customer;
                            $customerAllData->exchangeArray($custData);

                            $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);

                            $this->commit();
                            $this->status = true;
                            $this->msg = $res['msg'];
                            $this->data = $res['data'];

                            return $this->JSONRespond();
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $res['msg'];
                            $this->data = $res['data'];

                            return $this->JSONRespond();
                        }
                    } else {
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_INVOICE_INVALID_CUST');

                        return $this->JSONRespond();
                    }
                }
            }   
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVOICE_PRODUCT_SAVE');

            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $
     * If all SalesOrder products are used to create Sales Invoice,after that changed Status of SalesOrder
     */
    private function _checkAndUpdateSalesOrderStatus($salesOrderID)
    {
        $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getUnCopiedSalesOrderProducts($salesOrderID);
        if (empty($salesOrderProducts)) {
            $closeStatusID = $this->getStatusID('closed');
            $this->CommonTable('Invoice\Model\SalesOrderTable')->updateSalesOrderStatus($salesOrderID, $closeStatusID);
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * @param type $
     * If all DeliveryNote products are used to create Sales Invoice,after that changed Status of DeliveryNote
     */
    private function _checkAndUpdateDeliveryNoteStatus($deliveryNoteID)
    {
        $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getUnCopiedDeliveryNoteProducts($deliveryNoteID);
        if (empty($deliveryNoteProducts)) {
            $statusID = $this->getStatusID('closed');
        } else {
            $deliveryNoteStatus = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteStatusByDeliveryNoteID($deliveryNoteID)->current();
            if ($deliveryNoteStatus['deliveryNoteStatus'] != '15') {
                $statusID = $this->getStatusID('open');
            }
        }
            if ($statusID) {
                $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNoteStatus($deliveryNoteID, $statusID);
            }
    }

    public function saveInvoiceSubProductData($batchValue, $invoiceProductID)
    {
        
        if($batchValue['warranty'] == '' && $batchValue['serialID'] != null){      
            $serialDetails = $this->CommonTable('Inventory\Model\ProductSerialTable')
               ->getSerialProducts($batchValue['serialID']);
            $batchValue['warranty'] = $serialDetails->productSerialWarrantyPeriod;
        }
        $data = array(
            'salesInvocieProductID' => $invoiceProductID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'salesInvoiceSubProductQuantity' => $batchValue['qtyByBase'],
            'salesInvoiceSubProductWarranty' => $batchValue['warranty'],
            'salesInvoiceSubProductWarrantyType' => (int)$batchValue['warrantyType'],
        );

        $invoiceSubProduct = new InvoiceSubProduct;
        $invoiceSubProduct->exchangeArray($data);
       
        $invoiceSubProductID = $this->CommonTable('Invoice\Model\InvoiceSubProductTable')->saveInvoiceSubProduct($invoiceSubProduct);

        return $invoiceSubProductID;
    }

    public function getQuotationAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\QuotationTable')->fetchAll();
        while ($row = $re->current()) {
            $a[] = $row;
        }

        return new JsonModel($a);
    }

    public function getInvoiceAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\InvoiceTable')->fetchAll();
        while ($row = $re->current()) {
            $a[] = $row;
        }

        return new JsonModel($a);
    }

    public function getAllRecurrentInvoiceAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->fetchAll();
        while ($row = $re->current()) {
            if ($row->Type == 'Invoice') {
                $a[] = $row;
            }
        }

        return new JsonModel($a);
    }

    public function getSalesOrderAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\SalesOrderTable')->fetchAll();
        while ($row = $re->current()) {
            $a[] = $row;
        }

        return new JsonModel($a);
    }

    public function getInvoicesByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($invrequest->isPost()) {
            $fromdate = $this->convertDateToStandardFormat($invrequest->getPost('fromdate'));
            $todate = $this->convertDateToStandardFormat($invrequest->getPost('todate'));
            $customerName = $invrequest->getPost('customerID');
            $profileName = $invrequest->getPost('profileName');
            $posFlag = $invrequest->getPost('posFlag');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
            $showVehicleNum = false;
            if($displaySettings->jobModuleEnabled == 1 &&  $displaySettings->jobBusinessType == 2) {
                $showVehicleNum = true;
                $filteredInvoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicesByDateWithVehicle($fromdate, $todate, $locationID, $customerName, $posFlag, $profileName);
            } else {
                $filteredInvoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicesByDate($fromdate, $todate, $locationID, $customerName, $posFlag, $profileName);
            }

            while ($t = $filteredInvoices->current()) {
                $data[$t['salesInvoiceID']] = (object) $t;
            }
            $DateFilteredInvoices = new ViewModel(
                    array('invoices' => $data,
                'statuses' => $this->getStatusesList(),
                'showVehicleNum' => $showVehicleNum,
                'dateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $DateFilteredInvoices->setTerminal(TRUE);
            $DateFilteredInvoices->setTemplate('invoice/invoice-api/invoice-list');

            return $DateFilteredInvoices;
        }
    }

    public function getInvoicesByInvoiceCommentfilterAction()
    {
        $invrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($invrequest->isPost()) {
            $searchString = $invrequest->getPost('searchString');
            // $todate = $this->convertDateToStandardFormat($invrequest->getPost('todate'));
            // $customerName = $invrequest->getPost('customerID');
            // $profileName = $invrequest->getPost('profileName');
            // $posFlag = $invrequest->getPost('posFlag');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
            $showVehicleNum = false;
            if($displaySettings->jobModuleEnabled == 1 &&  $displaySettings->jobBusinessType == 2) {
                $showVehicleNum = true;
                $filteredInvoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicesByInovoiceCommentWithVehicle($searchString,$locationID);
            } else {
                $filteredInvoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicesByInovoiceComment($searchString,$locationID);
            }

            while ($t = $filteredInvoices->current()) {
                $data[$t['salesInvoiceID']] = (object) $t;
            }
            $DateFilteredInvoices = new ViewModel(
                    array('invoices' => $data,
                'statuses' => $this->getStatusesList(),
                'showVehicleNum' => $showVehicleNum,
                'dateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $DateFilteredInvoices->setTerminal(TRUE);
            $DateFilteredInvoices->setTemplate('invoice/invoice-api/invoice-list');

            return $DateFilteredInvoices;
        }
    }

    public function getInvoicesBySerialCodefilterAction()
    {
        $invrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();
        if ($invrequest->isPost()) {
            $searchString = $invrequest->getPost('searchString');
            $posflag = $invrequest->getPost('posFlag');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
            $showVehicleNum = false;
            if($displaySettings->jobModuleEnabled == 1 &&  $displaySettings->jobBusinessType == 2) {
                $showVehicleNum = true;
                $filteredInvoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicesBySerialCodeWithVehicle($searchString,$locationID, $posflag);
            } else {
                $filteredInvoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicesBySerialCode($searchString,$locationID,$posflag);
            }

            while ($t = $filteredInvoices->current()) {
                $data[$t['salesInvoiceID']] = (object) $t;
            }
            $DateFilteredInvoices = new ViewModel(
                    array('invoices' => $data,
                'statuses' => $this->getStatusesList(),
                'showVehicleNum' => $showVehicleNum,
                'dateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
            ));
            $DateFilteredInvoices->setTerminal(TRUE);
            $DateFilteredInvoices->setTemplate('invoice/invoice-api/invoice-list');

            return $DateFilteredInvoices;
        }
    }

    public function getRecurrentInvoicesByDatefilterAction()
    {
        $invrequest = $this->getRequest();
        if ($invrequest->isPost()) {
            $fromdate = $invrequest->getPost('fromdate');
            $todate = $invrequest->getPost('todate');
            $data = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoicesByDate($fromdate, $todate);
            $recurrentInvoiceview = new ViewModel(array(
                'recurrentinvoices'
                => $data
                    )
            );

            $delete_recurrent_invoice = new viewModel(array());
            $delete_recurrent_invoice->setTemplate('/invoice/invoice/confirm-delete-recurrent-invoice');
            $recurrentInvoiceview->addChild($delete_recurrent_invoice, 'delete_inv');

            $recurrentInvoiceview->setTerminal(true);
            $recurrentInvoiceview->setTemplate('invoice/invoice-api/recurrent_custom_invoice-list');

            return $recurrentInvoiceview;
        }
    }

    public function cancelRecurrentInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $id = $request->getPost('id');
            $username = trim($request->getPost('username'));
            $password = trim($request->getPost('password'));
            $user = $this->CommonTable('User\Model\UserTable')->getUserByUsername($username);
            if ($user) {
                if ($user->roleID == '1') {
                    if ($user->password == md5($username . $password . $username)) {
                        $recurrentInvoice = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByID($id);
                        if ($recurrentInvoice->state != 'Close') {
                            $data = array(
                                'state' => 'Close',
                                'id' => $id
                            );
                            $RecurrentInvoice = new RecurrentInvoice();
                            $RecurrentInvoice->exchangeArray($data);
                            $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->cancelRecurrentInvoice($RecurrentInvoice);

                            return new JsonModel(array(true));
                        } else {
                            return new JsonModel(array('state'));
                        }
                    } else {
                        return new JsonModel(array('pass'));
                    }
                } else {
                    return new JsonModel(array('admin'));
                }
            } else {
                return new JsonModel(array(
                    'user'));
            }
        }
    }

    public function getInvoiceWithItemsAction()
    {
        $getinvrequest = $this->getRequest();
        $in = array();

        if ($getinvrequest->isPost()) {
            $invID = $getinvrequest->getpost('invno');
            $ReInvoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invID);
            if ($ReInvoice != NULL) {
                $productsByInv = $this->CommonTable('Invoice\Model\InvoiceReceiptTable')->getProductsByInvoiceID($invID);
                $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($ReInvoice->customerID);
                $curBal = 0;
                if ($cust != NULL) {
                    $curBal = $cust->currentBalance;
                    $cusAdd = $cust->address;
                    $custp = $cust->telephoneNumber;
                    $cusmail = $cust->email;
                } else {
                    $curBal = 0;
                    $cusAdd = NULL;
                    $custp = NULL;
                    $cusmail = NULL;
                }

                $companyres = $this->CommonTable('Invoice\Model\CompanyTable')->fetchAll();
                $com = NULL;
                if ($companyres != NULL) {
                    $com = $companyres->current();
                }

                $payterm = $this->CommonTable('Invoice\Model\PaymenttermsTable')->getPaymentterm($ReInvoice->payementTerm);
                if ($payterm != NULL) {
                    $paytermname = $payterm->term;
                } else {
                    $paytermname = 'Not specified';
                }

                $taxAmountData = $this->CommonTable('Invoice\Model\TaxAmountTable')->getTaxAmountData($ReInvoice->id);
                $sendtaxdata = array();
                if ($taxAmountData != NULL) {
                    while ($f = $taxAmountData->current()) {
                        $sendtaxdata[] = $f;
                    }
                } else {
                    $sendtaxdata = 0;
                }


                $data = array();
                $pdata = array();
                $data['invoiceNo'] = $ReInvoice->id;
                $data['customerName'] = $ReInvoice->
                        customerName;
                $data['customerID'] = $ReInvoice->customerID;
                $data['customerAddress'] = ($cusAdd != NULL) ? $cusAdd : NULL;
                $data['customertelephone'] = ($custp != NULL) ? $custp : NULL;
                $data['customerEmail'] = ($cusmail != NULL) ? $cusmail : NULL;
                $data['companyName'] = ($com != NULL) ? $com->companyName : NULL;
                $data['companyAddress'] = ($com != NULL) ? $com->companyAddress : NULL;
                $data['companyTelephone'] = ($com != NULL) ? $com->telephoneNumber : NULL;
                $data['companyEmail'] = ($com != NULL) ? $com->email : NULL;
                $data['companyReg'] = ($com != NULL) ? $com->trNumber : NULL;
                $data['issuedDate'] = $ReInvoice->issuedDate;
                $data['overDueDate'] = $ReInvoice->overDueDate;
                $data['payementTerm'] = $paytermname;
                $data['payment_term'] = $ReInvoice->payementTerm;
                $data['taxAmount'] = $ReInvoice->taxAmount;
                $data['taxAmountdata'] = $sendtaxdata;
                $data['totalDiscount'] = $ReInvoice->totalDiscount;
                $data['totalAmount'] = $ReInvoice->totalAmount;
                $data['payedAmount'] = $ReInvoice->payedAmount;
                $data['comment'] = $ReInvoice->comment;
                $data['deliveryAddress'] = $ReInvoice->deliveryAddress;
                $data['deliveryCharge'] = $ReInvoice->deliveryCharge;
                $data['branch'] = $ReInvoice->branch;
                $data['state'] = $ReInvoice->state;
                $data['quotationID'] = $ReInvoice->quotID;
                $data['salesOrderID'] = $ReInvoice->salesOrderID;
                $data['currentBalance'] = $curBal;
                $data['cdnUrl'] = $this->cdnUrl;

                while ($p = $productsByInv->current()) {
                    $pdata[] = $p;
                }

                $data['product'] = $pdata;

                return new JsonModel($data);
            } else {
                $in[0] = 'wronginvno';

                return new JsonModel(
                        $in);
            }
        }
    }

    public function getCustomerAction()
    {
        $a = array();
        $re = $this->CommonTable('Invoice\Model\CustomerTable')->fetchAll();
        while ($row = $re->current()) {
            $a[] = $row;
        }

        return new JsonModel($a);
    }

    public function getCrediLimitExeedBoxAction()
    {

        $viewmodel = new ViewModel();
        $viewmodel->setTerminal(true
        );
        return $viewmodel;
    }

    public function saveRecurrentInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $form = new RecurrentInvoiceForm();
            $RecurrentInvoice = new RecurrentInvoice();
            $form->setInputFilter($RecurrentInvoice->getInputFilter());
            $post = $request->getPost();

            $next_date = date('Y-m-d', strtotime($post["startDate"] . ' + ' . $post['timeValue'] . ' ' . $post['timeFormat']));
            if ($post["endDate"] != null) {
                if ($next_date <= $post["endDate"]) {
                    $post['nextDate'] = $next_date;
                    $form->setData($post);
                    if ($form->isValid()) {
                        $RecurrentInvoice->exchangeArray($post);
                        $res = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->saveRecurrentInvoice($RecurrentInvoice);
                        if ($res) {
                            $this->setLogMessage("Issued recurrent invoice.");
                        }
                        return new JsonModel(array("result" => true));
                    } else {
                        return new JsonModel(array("result" => "invalid"));
                    }
                } else {
                    $post['nextDate'] = NULL;
                    return new JsonModel(array("result" => false));
                }
            } else {
                $post['nextDate'] = $next_date;
                $form->setData($post);
                if ($form->isValid()) {
                    $RecurrentInvoice->exchangeArray($post);
                    $res = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->saveRecurrentInvoice($RecurrentInvoice);
                    if ($res) {
                        $this->setLogMessage("Issued recurrent invoice.");
                    }
                    return new JsonModel(array("result" => true));
                } else {
                    return new JsonModel(array("result" => "invalid", "msg" => $form->getMessages()));
                }
            }
        }
    }

    public function recurrentInvoiceUpdateAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByID($request->getPost('id'));
            if ($data) {
                $form = new RecurrentInvoiceForm();
                $RecurrentInvoice = new RecurrentInvoice();
                $form->setInputFilter($RecurrentInvoice->getInputFilter());
                $post = $request->getPost();
                $next_date = date('Y-m-d', strtotime($post["startDate"] . ' + ' . $post['timeValue'] . ' ' . $post['timeFormat']));
                if ($post["endDate"] != null) {
                    if ($next_date <
                            $post["endDate"])
                        $post['nextDate'] = $next_date;
                    else
                        $post['nextDate'] = NULL;
                    $form->setData($post);
                    if ($form->isValid()) {
                        $RecurrentInvoice->exchangeArray($post);
                        $res = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->updateRecurrentInvoice($RecurrentInvoice);
                        if ($res) {
                            $this->setLogMessage("Update recurrent invoice.");
                        }
                    } else {
                        return new JsonModel(array("result" => false));
                    }
                    return new JsonModel(array("result" => true));
                } else {
                    $post['nextDate'] = $next_date;
                    $form->setData($post);
                    if ($form->isValid()) {
                        $RecurrentInvoice->exchangeArray($post);
                        $res = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->updateRecurrentInvoice($RecurrentInvoice);
                        if ($res) {
                            $this->setLogMessage("Update recurrent invoice.");
                        }
                        return new JsonModel(array("result" => true));
                    } else {
                        return new JsonModel(array("result" => "invalid", "msg" => $form->getMessages()));
                    }
                }
            } else {
                return new JsonModel(array("result" => false));
            }
        }
        return new JsonModel(array
            ("result" => false));
    }

    public function getRecurrentInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $ID = $request->getpost('id');
            $ReInvoice = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByID($ID);
            $Invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($ID);
            if ($ReInvoice != NULL) {
                return new JsonModel(array(
                    "result" => $ReInvoice
                ));
            } else {
                return new JsonModel(array(
                    "result" => "null",
                    "invoice" => $Invoice
                ));
            }
        }
    }

    /**
     * @author Prathap Weerasinghe <prathap@thinkcube.com>
     * @return \Zend\View\Model\JsonModel
     */
    public function sendEmailNotificationAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $date = date('Y-m-d', strtotime($request->getpost('date')));
            $ReInvoice = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByNextDate($date);

            $subject = array(
                "Invoice" => "Reminder for Recurrent Invoice.",
                "SalesOrder" => "Reminder for Recurrent Order.",
                "Overdue" => "Payment reminder."
            );
            while ($row = $ReInvoice->current()) {
                if (!empty($row["email1"])) {
                    if ($row["Type"] == "Invoice") {
                        $body = "Dear " . $row["firstName"] . ",<br><br>The next scheduled invoice of the recurrent invoice is to be sent on " . $row["nextDate"] . ". Please log in to your ezBiz account and check notifications.<br><br><br>Customer Name : " . $row["firstName"] . "<br>Recurrent Invoice Number : " . $row["id"] . "<br>Next Issue Date : " . $row["nextDate"] . "<br><br><br>Sent from <br>ezBiz auto reminder service";
                    } elseif ($row["Type"] == "SalesOrder") {
                        $body = "Dear " . $row["firstName"] . ",<br><br>The next scheduled invoice of the recurrent order is to be sent on " . $row["nextDate"] . ". Please log in to your ezBiz account and check notifications.<br><br><br>Customer Name : " . $row["firstName"] . "<br>Recurrent Order Number : " . $row["id"] . "<br>Next Issue Date : " . $row["nextDate"] . "<br><br><br>Sent from <br>ezBiz auto reminder service";
                    } $this->sendEmail($row["email1"], $subject[$row["Type"]], $body);
                }
            }

            $overdue = $this->CommonTable('Invoice\Model\InvoiceTable')->getOverduenvoices();

            while ($row2 = $overdue->current()) {
                $row2["Type"] = "Overdue";
                $body_over_due = "Dear " . $row2["firstName"] . ",<br><br>The invoice isseud to " . $row2["customerName"] . " on " . $row2["issuedDate"] . " has reached its due date. Please log in to your ezBiz account and check notifications.<br><br><br>Customer Name : " . $row2["customerName"] . "<br>Invoice Number : " . $row2["id"] . "<br>Issue Date : " . $row2["issuedDate"] . "<br>Due Date : " . $row2["overDueDate"] . "<br><br><br>Sent from <br>ezBiz auto reminder service";
                $this->sendEmail($row2["email1"], $subject["Overdue"], $body_over_due);
            }


            if (!empty($res)) {
                return new JsonModel($res);
            } else {
                return new JsonModel(array(
                    false));
            }
        }
    }

    public function retriveCustomerInvoiceAction()
    {
        $er = array();
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $cust_id = $request->getpost('customerID');
            $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
            $showVehicleNum = false;
            if($displaySettings->jobModuleEnabled == 1 &&  $displaySettings->jobBusinessType == 2) {
                $showVehicleNum = true;
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByCustomerIDWithVehicle($cust_id, $locationID);
            } else {
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByCustomerID($cust_id, $locationID);
            }

            while ($t = $invoice->current()) {
                $data[$t['salesInvoiceID']] = (object) $t;
            }

            $customerInvoices = new ViewModel(array(
                'invoices' => $data,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat,
                'companyCurrencySymbol' => $this->companyCurrencySymbol,
                'showVehicleNum' => $showVehicleNum
                    )
            );
            $customerInvoices->setTerminal(TRUE);
            $customerInvoices->setTemplate('invoice/invoice-api/invoice-list');

            return $customerInvoices;
        }
    }


    public function getInvoiceByVehicleNumberAction()
    {
        $er = array();
        $request = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $vehicleNumberID = $request->getpost('vehicleNumberID');

            $vehicle = $this->CommonTable('Jobs\Model\ServiceVehicleTable')->getVehicleByVehicleID($vehicleNumberID);


            $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByVehicleNumber($vehicle['serviceVehicleRegNo'], $locationID);

            while ($t = $invoice->current()) {
                $data[$t['salesInvoiceID']] = (object) $t;
            }

            $vehicleInvoices = new ViewModel(array(
                'invoices' => $data,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $dateFormat,
                'showVehicleNum' => true,
                'companyCurrencySymbol' => $this->companyCurrencySymbol
                    )
            );
            $vehicleInvoices->setTerminal(TRUE);
            $vehicleInvoices->setTemplate('invoice/invoice-api/invoice-list');

            return $vehicleInvoices;
        }
    }

    public function getCustomerRecurrentInvoiceAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $custa = trim($request->getPost('customerShortName'));
            $value = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByShortname($custa);
            if (isset($value->customerID)) {
                $cust_id = $value->customerID;
                $data = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByCustomerID($cust_id);
                $recurrentInvoiceview = new ViewModel(array(
                    'recurrentinvoices' => $data
                        )
                );
                $recurrentInvoiceview->setTerminal(true);
                $recurrentInvoiceview->setTemplate('invoice/invoice-api/recurrent_custom_invoice-list');

                return $recurrentInvoiceview;
            } else {
                $er[0] = 'wrongcust';

                return new JsonModel($er);
            }
        }
    }

    public function getRecurrentInvoiceByIDAction()
    {
        $er = array();
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invID = trim($request->getPost('invoiceID'));
            $value = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoiceByID($invID);
            $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invID);
            if (isset($invoice->id)) {
                if (isset($value->id)) {
                    $data = $this->CommonTable('Invoice\Model\RecurrentInvoiceTable')->getRecurrentInvoice($invID);
                    $recurrentInvoiceview = new ViewModel(array(
                        'recurrentinvoices' => $data
                            )
                    );
                    $delete_recurrent_invoice = new viewModel(array());
                    $delete_recurrent_invoice->setTemplate('/invoice/invoice/confirm-delete-recurrent-invoice');
                    $recurrentInvoiceview->addChild($delete_recurrent_invoice, 'delete_inv');

                    $recurrentInvoiceview->setTerminal(true);
                    $recurrentInvoiceview->setTemplate('invoice/invoice-api/recurrent_custom_invoice-list');

                    return $recurrentInvoiceview;
                } else {
                    $er[0] = 'wronginv';

                    return new JsonModel($er);
                }
            } else {
                $er[0] = 'wrongrecurrentinv';

                return new JsonModel($er);
            }
        }
    }

    function getInvoiceHistoryAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invoiceID = $request->getPost('invNo');
            $invTableData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
            if ($invTableData != NULL) {
                $history = array();
                $preText = "Invoice ID - " . $invTableData->id;
                $issuedText = $invTableData->issuedDate . " : Invoice issued";
                $overdueText = $invTableData->overDueDate . " : Overdue Date";
                array_push($history, $preText);
                array_push($history, $issuedText);
                array_push($history, $overdueText);
                $quotationText = "";
                $salesOrderText = "";
                $salesOrderQuotationText = "";
                $invoicePaymentData = array();
                $invoiceReturnsData = array();

                if ($invTableData->quotID != NULL) {
                    $Quo = 'Quotation Number : ' . $invTableData->quotID;
                    $quotationText = $quotationText . $Quo;
                    array_push($history, $quotationText);
                } else if ($invTableData->salesOrderID != NULL) {
                    $SO = 'Sales Order Number : ' . $invTableData->salesOrderID;
                    $salesOrderText = $salesOrderText . $SO;
                    array_push($history, $salesOrderText);
                    $soDetails = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrdersByID($invTableData->salesOrderID);
                    if ($soDetails->quotID != NULL) {
                        $soQuo = 'QOT of Sales Order : ' . $soDetails->quotID;
                        $salesOrderQuotationText = $salesOrderQuotationText . $soQuo;
                        array_push($history, $salesOrderQuotationText);
                    }
                }

                $payementsData = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->getPaymentsDetailsByInvoiceID($invTableData->id);
                if ($payementsData != NULL) {
                    $paymentsHistory = '';
                    while ($payRow = $payementsData->current()) {
                        $paidAmount = 0;
                        $paidAmount = floatval($payRow['amount']) + floatval($payRow['creditAmount']);
                        $payText = $payRow['date'] . ": Payment of " . $this->companyCurrencySymbol . " " . number_format($paidAmount, 2) . ' - PaymentID : ' . $payRow['paymentID'];
                        array_push($invoicePaymentData, $payText);
                    }
                    array_push($history, $invoicePaymentData);
                }
                $productsOfInvoice = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByInvoiceID($invTableData->id);
                while ($row = $productsOfInvoice->current()) {
                    $returnText = $row->issuedDate . ": Credit Note of " . $this->companyCurrencySymbol . " " . number_format($row->totalAmount, 2) . ' - Credit Note ID : ' . $row->creditNoteID;
                    array_push($invoiceReturnsData, $returnText);
                }
                while ($row = $productsOfInvoice->current()) {
                    if ($row->returnedQuantity != '0') {
                        $returnText = "Item " . $row->locationProductID . ' have ' . $row->returnedQuantity . ' items returned';
                        array_push($invoiceReturnsData, $returnText);
                    }
                }
                array_push($history, $invoiceReturnsData);

                return new JsonModel(array('invHistory' => $history));
            } else {
                return new JsonModel(array('invHistory' => ""));
            }
        }
    }

    public function sendInvoiceAction()
    {
//        $inv_id = $this->params()->fromRoute('param1');
        $inv_id = 'INV13';
        $value = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByInvoiceID($inv_id);
        $value = $value->current();
        $pvalue = $this->CommonTable('Invoice\Model\InvoiceReceiptTable')->getProductsByInvoiceID($inv_id);
        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($value->customerID);
        $sub_total = 0;
        $paymentTerms = $this->CommonTable('Invoice\Model\PaymenttermsTable')->fetchAll();
        $terms = array();
        while ($row = $paymentTerms->current()) {
            $id = $row->id;
            if ($id == NULL) {
                $id = 0;
            }
            $terms[$id] = $row->term;
        }

        while ($t = $pvalue->current()) {
            $records[] = array(
                $t->locationProductID,
                $t->productName,
                $t->quantity,
                $t->unitPrice,
                " ",
                $t->total,
            );
            $sub_total+= $t->total;
        }
        $doc = array();
        $doc["template"] = $this->user_session->displaySettings->invoiceTemplate;
        $doc["company"] = $this->company;
        $doc['type'] = _("invoice");
        $doc['cust_name'] = (!empty($cust->customerName) ? $cust->customerName : $value->customerName);
        $doc['doc_data'] = array(
            _("Date") => $value->issuedDate,
            _("Valid till") => $value->overDueDate,
            _("payment terms") => $terms[$value->payementTerm],
            _("Quotation No.") => $value->quotID,
            _
                    ("Sales Oder No.") => $value->salesOrderID,
            _("Invoice No.") => $value->id
        );
        $doc['cust_data'] = array(
            _("address"
            ) => isset($cust->address) ? $cust->address : null,
        );
        $doc['table'] = array(
            'col_size' => array(
                10, 90, 290, 75, 100, 60, 100),
            'headers' => array(
                _("Product code") => 2,
                _("product") => 1,
                _("quantity") => 1,
                _("price") => 1,
                "*" => 1,
                _("total") => 1
            ),
            'records' => $records,
        );
        $doc['delivery_charge'] = $value->deliveryCharge;
        $doc[
                'delivery_address'] = $value->deliveryAddress;
        $doc['comment'] = $value->comment;
        $doc['sub_total'] = $sub_total;
        $doc['total'] = $value->totalAmount;
        $doc['discount'] = $value->totalDiscount;
        $doc['tax_record'] = "*" . _("Tax:") . "&nbsp;
" . $tx_line;
        $doc ['discountStatement'] = _("Discount") . " " . _("received:");
        $doc["show_tax"] = $get["show_tax"];

        $request = $this->
                getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($post["cust_id"]);
            if (!empty($cust->email) || TRUE) {
                $to = $post["to_email"];
                $fromName = "ezBiz";
                $subject = $post["subject"];
                $Bcc = null;
                $Cc = null;
                $textContent = null;
                $htmlMarkup = $post["body"];
                $error = FALSE;
            } else {
                $error = true;
                $msg = $this->getMessage('ERR_INVOICE_NO_EMAIL');
            }
        } else {
            $error = "true";
            $msg = $this->getMessage('ERR_INVOICE_UNKNOWN');
        }
        return new JsonModel(array(
            "error" => $error,
            "msg" => $msg
        ));
    }

    /**
     * @author Damith Thamara <damith@thinkcube.com>
     * @return \Zend\View\Model\JsonModel|\Zend\View\Model\ViewModel
     */
    public function getInvoicesFromSearchAction()
    {
        $searchrequest = $this->getRequest();
        $dateFormat = $this->getUserDateFormat();

        if ($searchrequest->isPost()) {
            $InvoiceID = $searchrequest->getPost('invoiceID');
            $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
            $showVehicleNum = false;
            if($displaySettings->jobModuleEnabled == 1 &&  $displaySettings->jobBusinessType == 2) {
                $showVehicleNum = true;
                $invoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicesforSearchWithVehicle($InvoiceID);
            } else {
                $invoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoicesforSearch($InvoiceID);

            }
            if ($invoices->count() == 0) {
                return new JsonModel(array(FALSE));
            } else {
                while ($t = $invoices->current()) {
                    $data[$t->salesInvoiceID] = (object) $t;
                }
                $Invoices = new ViewModel(
                        array('invoices' => $data,
                    'statuses' => $this->getStatusesList(),
                    'dateFormat' => $dateFormat,
                    'showVehicleNum' => $showVehicleNum,
                    'companyCurrencySymbol' => $this->companyCurrencySymbol
                ));
                $Invoices->setTerminal(TRUE);
                $Invoices->setTemplate('invoice/invoice-api/invoice-list');
                return $Invoices;
            }
        }
    }

    public function retriveRecurrentInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $in = array();
            $invoice = $request->getPost('invoiceID');
            $value = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoice);
            if ($value != NULL) {
                $pvalue = $this->CommonTable('Invoice\Model\InvoiceReceiptTable')->getProductsByInvoiceID($invoice);
                $custa = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($value->customerID);
                if ($custa != NULL) {
                    $cbalance = $custa->currentBalance;
                    $ccredit = $custa->currentCredit;
                } else {
                    $cbalance = 0;
                    $ccredit = 0;
                }
                $data = array();
                $pdata = array();
                $data['customer_name'] = $value->customerName;
                $data['customer_id'] = $value->customerID;
                $data['issue_date'] = $value->issuedDate;
                $data['expire_date'] = $value->overDueDate;
                $data['payment_term'] = $value->payementTerm;
                $data['tax'] = $value->taxAmount;
                $data['total_discount'] = $value->totalDiscount;
                $data['total'] = $value->totalAmount;
                $data['comment'] = $value->comment;
                $data['state'] = $value->state;
                $data['sales_order_no'] = $value->salesOrderID;
                $data['current_balance'] = $cbalance;
                $data['current_credit'] = $ccredit;
                while ($t = $pvalue->current()) {
                    $pdata[] = $t;
                }
                $data['product'] = $pdata;

                return new JsonModel($data);
            } else {
                $in[0] = 'wrongsono';

                return new JsonModel($in);
            }
        } else {
            return false;
        }
    }

    /**
     * @author Prathap Weerasinghe <prathap@thinkcube.com>
     * @return type
     */
    public function sendEmailAction()
    {
        $request = $this->getRequest();

        if ($request->isPost()) {
            $documentType = 'Sales Invoice';

            $this->mailDocumentAsTemplate($request, $documentType);
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_INVOICE_EMAIL_SENT');

            return $this->JSONRespond();
        }

        $this->status = FALSE;
        $this->msg = $this->getMessage('ERR_INVOICE_EMAIL_SENT');
        return $this->JSONRespond();
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * this function return details of invoice
     */
    public function getInvoiceDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invalidFlag = false;
            $invoiceID = $request->getPost('invoiceID');
            $invalidFlag =  filter_var($request->getPost('invalidFlag'), FILTER_VALIDATE_BOOLEAN);

            $invoiceProductsArray = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
            $dparray;
            $dparrayQty;
            $iparray;
            $iparrayQty;
            $realproduct;
            $checkProduct = 0;
            $dproductType;
            $locationID = $this->user_session->userActiveLocation['locationID'];

            foreach ($invoiceProductsArray as $dp) {
                $dp = (object) $dp;
                $dparray[$dp->salesInvoiceProductID] = $dp->productID;
                $dparrayQty[$dp->salesInvoiceProductID] = $dp->salesInvoiceProductQuantity;
                $dproductType[$dp->salesInvoiceProductID] = $dp->productTypeID;
            }

            $data = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductsByInvoiceID($invoiceID);
            foreach ($data as $ip) {
                $ip = (object) $ip;
                if($ip->locationID == $locationID){
                    $iparray[$ip->productID] = $ip->productID;
                    $iparrayQty[$ip->productID]+=$ip->creditNoteProductQuantity;
                }
            }

            foreach ($dparrayQty as $key => $Pdata) {
                if ($iparrayQty[$key] != $Pdata || ($Pdata == NULL && !array_key_exists($key, $iparray))) {
                    $checkProduct = 1;
                    $realproduct[$key] = $key;
                }
            }

            if ($checkProduct) {
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                $promotion = $this->CommonTable('Settings\Model\PromotionTable')->getPromotionDetailsByInvoiceID($invoiceID)[0];
                $invoice->promotionType = $promotion['promotionType'];

                $invoiceProduct = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
                $tax = array(
                    'invoiceTaxID' => '',
                    'invoiceTaxName' => '',
                    'invoiceTaxPrecentage' => '',
                    'invoiceTaxAmount' => '',
                    'isSuspended' => '',
                    'taxSuspendable' => '',
                );
                $subProduct = array(
                    'invoiceSubProductID' => '',
                    'productBatchID' => '',
                    'productSerialID' => '',
                    'invoiceSubProductQuantity' => '',
                );

                $Products = array(
                    'invoiceProductID' => '',
                    'invoiceID' => '',
                    'productID' => '',
                    'invoiceProductPrice' => '',
                    'invoiceProductDiscount' => '',
                    'invoiceProductDiscountType' => '',
                    'invoiceProductQuantity' => '',
                    'productCode' => '',
                    'productName' => '',
                    'productType' => '',
                    'tax' => '',
                    'subProduct' => '',
                );

                while ($tt = $invoiceProduct->current()) {
                    $t = (object) $tt;
                    
                    if($t->productSerialID != '' && $invalidFlag){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INACTIVE_SERIAL_ITEM');
                        return $this->JSONRespond();
                    }
                    if ($realproduct[$t->salesInvoiceProductID] == $t->salesInvoiceProductID) {
                        $creditNoteProductData = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductAndSubProductDataByInvoiceProductID($t->salesInvoiceProductID);
                        $creditNoteProductQty = 0;
                        $creditNoteSubProductSerial = array();
                        $creditNoteSubProductBatch = array();
                        $creditNoteProductIDArray = array();

                        foreach ($creditNoteProductData as $prValue) {
                            $prValue = (object) $prValue;
                            if ($prValue->creditNoteProductID) {
//use this if statement for ignore repeating creditNoteProducts
                                if (!$creditNoteProductIDArray[$prValue->creditNoteProductID] == 1) {
                                    $creditNoteProductQty+=$prValue->creditNoteProductQuantity;
                                    $creditNoteProductIDArray[$prValue->creditNoteProductID] = 1;
                                }
                                if ($prValue->productSerialID) {
                                    $creditNoteSubProductSerial[$prValue->productSerialID] += $prValue->creditNoteSubProductQuantity;
                                } else if ($prValue->productBatchID) {
                                    $creditNoteSubProductBatch[$prValue->productBatchID] += $prValue->creditNoteSubProductQuantity;
                                }
                            }
                        }
                        $productHandlingData = $this->CommonTable('Inventory\Model\ProductHandelingTable')->getProductHandlingByProductID($t->productID);
                        $giftCardFlag = ($productHandlingData->productHandelingGiftCard == 1)? 1:0;

                        $salesInvoiceProduct[$t->salesInvoiceProductID] = (object) $Products;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductID = $t->salesInvoiceProductID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceID = $t->salesInvoiceID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productID = $t->productID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductPrice = $t->salesInvoiceProductPrice;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductDiscount = $t->salesInvoiceProductDiscount;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductDiscountType = $t->salesInvoiceProductDiscountType;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductQuantity = $t->salesInvoiceProductQuantity - $creditNoteProductQty;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productCode = $t->productCode;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productName = $t->productName;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productType = $t->productTypeID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productDescription = $t->salesInvoiceProductDescription;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->locationProductID = $t->locationProductID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->documentTypeID = $t->documentTypeID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->documentID = $t->salesInvoiceProductDocumentID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->giftCard = $giftCardFlag;
                        if ($t->salesInvoiceSubProductID != '') {
                            $creditNoteQuantity = 0;
                            if ($creditNoteSubProductSerial[$t->productSerialID]) {
                                $creditNoteQuantity = $creditNoteSubProductSerial[$t->productSerialID];
                            } else if ($creditNoteSubProductBatch[$t->productBatchID]) {
                                $creditNoteQuantity = $creditNoteSubProductBatch[$t->productBatchID];
                            }

                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID] = (object) $subProduct;
                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->invoiceSubProductID = $t->salesInvoiceSubProductID;
                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->productBatchID = $t->productBatchID;
                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->productSerialID = $t->productSerialID;
                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->invoiceSubProductQuantity = $t->salesInvoiceSubProductQuantity - $creditNoteQuantity;
                        }
                        if ($salesInvoiceSubProduct) {
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->subProduct = $salesInvoiceSubProduct[$t->salesInvoiceProductID];
                        }

                        if ($t->taxID) {
                            //unset $taxes array for new salesInvoiceProductIDs
                            if($productIdForTax != $t->salesInvoiceProductID){
                                unset($taxes[$t->salesInvoiceSubProductID]);
                            }
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID] = (object) $tax;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxID = $t->taxID;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxName = $t->taxName;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxPrecentage = $t->salesInvoiceProductTaxPrecentage;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxAmount = $t->salesInvoiceProductTaxAmount;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->isSuspended = $t->salesInvoiceSuspendedTax;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->taxSuspendable = $t->taxSuspendable;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->tax = $taxes[$t->salesInvoiceSubProductID];
                        }

                        $productIdForTax = $t->salesInvoiceProductID;

                    }
                    $locationID = $this->user_session->userActiveLocation['locationID'];
                    $result = $this->CommonTable('Inventory\Model\ProductTable')->getLocationProductsByLocationProductID($t->locationProductID)->current();
                    $locationID = (isset($result['locationID']))? $result['locationID']: $locationID;
                    $products[$t->salesInvoiceProductID] = $this->getLocationProductDetailsForReturn($locationID, $t->productID, $invalidFlag);
                    foreach ($products as $value) {
                        foreach ($value['tax'] as $key => $tax) {
                            if ($key == $t->id) {
                                if ($tax['tS'] == "0") {
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE_SOME_TAX_ARE_INACTIVE');
                                    return $this->JSONRespond();
                                }
                            }
                        }
                    }
                }

                // remove supended taxes
                foreach ($salesInvoiceProduct as $key => $value) {
                    foreach ($value->tax as $tax) {
                        if ($tax->taxSuspendable == "1" && $tax->isSuspended == "1") {
                           unset($value->tax [$tax->invoiceTaxID]);
                        }
                    }
                }

                $customerID = ($invoice->customerID) ? $invoice->customerID : 0;
                $customer = $this->getCustomerDetailsByCustomerID($customerID);

                if($customer->customerStatus == 2){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_CUSTOMER_INACTIVE');
                    return $this->JSONRespond();
                }

                $locationPro = Array();
                $inactiveItems = Array();
                foreach ($salesInvoiceProduct as $value) {
                    $locationPro[$value->productID] = $this->getLocationProductDetails($locationID, $value->productID);
                    if ($locationPro[$value->productID] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($value->productID);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }
                }

                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_INV_ITM_INACTIVE_COPY', [$errorItems]);
                }
                
                $promotionID = ($invoice->promotionID) ? $invoice->promotionID : 0;
                $promotion = $this->CommonTable('Settings\Model\PromotionTable')->getPromotion($promotionID);
                $this->status = true;
                $this->data = array(
                    'status' => true,
                    'invoice' => $invoice,
                    'invoiceProduct' => $salesInvoiceProduct,
                    'products' => $products,
                    'customer' => $customer,
                    'errorMsg' => $errorMsg,
                    'inactiveItemFlag' => $inactiveItemFlag,
                    'promotion' => $promotion
                );
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_USED_INV_FOR_CN');
            }
            
            return $this->JSONRespond();
        }
    }


    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * this function return details of invoice
     */
    public function getInvoiceDetailsAccordingToInvoiceReturnsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invalidFlag = false;
            $invoiceID = $request->getPost('invoiceID');
            $invoiceReturns = $request->getPost('invoiceReturns');



            $invalidFlag =  filter_var($request->getPost('invalidFlag'), FILTER_VALIDATE_BOOLEAN);

            $invoiceProductsArray = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
            $dparray;
            $dparrayQty;
            $iparray;
            $iparrayQty;
            $realproduct;
            $checkProduct = 0;
            $dproductType;
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $returnData = [];

            foreach ($invoiceReturns as $key => $value) {
                $invoiceReturnProductDetails = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->getInvoiceReturnDetailsByInvoiceReturnID($value, $locationID);

                foreach ($invoiceReturnProductDetails as $key => $value) {
                    if (array_key_exists($value['invoiceProductID'], $returnData)) {
                        $returnData[$value['invoiceProductID']]['qty'] +=  floatval($value['invoiceReturnProductQuantity']);
                    } else {
                        $returnData[$value['invoiceProductID']]['qty'] = floatval($value['invoiceReturnProductQuantity']);
                        $returnData[$value['invoiceProductID']]['productID'] = $value['productID'];
                        $returnData[$value['invoiceProductID']]['productBatchID'] = $value['productBatchID'];
                        $returnData[$value['invoiceProductID']]['productSerialID'] = $value['productSerialID'];
                    }
                }
            }
            

            foreach ($invoiceProductsArray as $dp) {
                $dp = (object) $dp;
                $dparray[$dp->salesInvoiceProductID] = $dp->productID;
                $dparrayQty[$dp->salesInvoiceProductID] = $dp->salesInvoiceProductQuantity;
                $dproductType[$dp->salesInvoiceProductID] = $dp->productTypeID;
            }

            $data = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductsByInvoiceID($invoiceID);
            foreach ($data as $ip) {
                $ip = (object) $ip;
                if($ip->locationID == $locationID){
                    $iparray[$ip->productID] = $ip->productID;
                    $iparrayQty[$ip->productID]+=$ip->creditNoteProductQuantity;
                }
            }

            foreach ($dparrayQty as $key => $Pdata) {
                if ($iparrayQty[$key] != $Pdata || ($Pdata == NULL && !array_key_exists($key, $iparray))) {
                    $checkProduct = 1;
                    $realproduct[$key] = $key;
                }
            }

            if ($checkProduct) {
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                $promotion = $this->CommonTable('Settings\Model\PromotionTable')->getPromotionDetailsByInvoiceID($invoiceID)[0];
                $invoice->promotionType = $promotion['promotionType'];

                $invoiceProduct = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
                $tax = array(
                    'invoiceTaxID' => '',
                    'invoiceTaxName' => '',
                    'invoiceTaxPrecentage' => '',
                    'invoiceTaxAmount' => '',
                    'isSuspended' => '',
                    'taxSuspendable' => '',
                );
                $subProduct = array(
                    'invoiceSubProductID' => '',
                    'productBatchID' => '',
                    'productSerialID' => '',
                    'invoiceSubProductQuantity' => '',
                );

                $Products = array(
                    'invoiceProductID' => '',
                    'invoiceID' => '',
                    'productID' => '',
                    'invoiceProductPrice' => '',
                    'invoiceProductDiscount' => '',
                    'invoiceProductDiscountType' => '',
                    'invoiceProductQuantity' => '',
                    'productCode' => '',
                    'productName' => '',
                    'productType' => '',
                    'tax' => '',
                    'subProduct' => '',
                );

                while ($tt = $invoiceProduct->current()) {
                    $t = (object) $tt;
                    
                    if($t->productSerialID != '' && $invalidFlag){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INACTIVE_SERIAL_ITEM');
                        return $this->JSONRespond();
                    }
                    if ($realproduct[$t->salesInvoiceProductID] == $t->salesInvoiceProductID) {
                        if (array_key_exists($t->salesInvoiceProductID, $returnData)) {

                            $creditNoteProductData = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductAndSubProductDataByInvoiceProductID($t->salesInvoiceProductID);
                            $creditNoteProductQty = 0;
                            $invoiceReturnProductQty = 0;
                            $creditNoteSubProductSerial = array();
                            $invoiceReturnSubProductSerial = array();
                            $creditNoteSubProductBatch = array();
                            $invoiceReturnSubProductBatch = array();
                            $creditNoteProductIDArray = array();
                            $invoiceReturnProductIDArray = array();

                            foreach ($creditNoteProductData as $prValue) {
                                $prValue = (object) $prValue;
                                if ($prValue->creditNoteProductID) {
    //use this if statement for ignore repeating creditNoteProducts
                                    if (!$creditNoteProductIDArray[$prValue->creditNoteProductID] == 1) {
                                        $creditNoteProductQty+=$prValue->creditNoteProductQuantity;
                                        $creditNoteProductIDArray[$prValue->creditNoteProductID] = 1;
                                    }
                                    if ($prValue->productSerialID) {
                                        $creditNoteSubProductSerial[$prValue->productSerialID] += $prValue->creditNoteSubProductQuantity;
                                    } else if ($prValue->productBatchID) {
                                        $creditNoteSubProductBatch[$prValue->productBatchID] += $prValue->creditNoteSubProductQuantity;
                                    }
                                }
                            }


                            $invoiceReturnProductData = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->getInvoiceReturnProductAndSubProductDataByInvoiceProductID($t->salesInvoiceProductID);
                            // $invoiceReturnProductQty = 0;
                            // $creditNoteSubProductSerial = array();
                            // $creditNoteSubProductBatch = array();
                            // $creditNoteProductIDArray = array();

                            foreach ($invoiceReturnProductData as $rtnValue) {
                                $rtnValue = (object) $rtnValue;

                                if ($rtnValue->invoiceReturnProductID) {
                                    if (!$invoiceReturnProductIDArray[$rtnValue->invoiceReturnProductID] == 1) {
                                        $invoiceReturnProductQty+=$rtnValue->invoiceReturnProductQuantity;
                                        $invoiceReturnProductIDArray[$rtnValue->invoiceReturnProductID] = 1;
                                    }
                                    if ($rtnValue->productSerialID) {
                                        $invoiceReturnSubProductSerial[$rtnValue->productSerialID] += $rtnValue->invoiceReturnSubProductQuantity;
                                    } else if ($rtnValue->productBatchID) {
                                        $invoiceReturnSubProductBatch[$rtnValue->productBatchID] += $rtnValue->invoiceReturnSubProductQuantity;
                                    }
                                }
                            }


                            $productHandlingData = $this->CommonTable('Inventory\Model\ProductHandelingTable')->getProductHandlingByProductID($t->productID);
                            $giftCardFlag = ($productHandlingData->productHandelingGiftCard == 1)? 1:0;

                            $salesInvoiceProduct[$t->salesInvoiceProductID] = (object) $Products;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductID = $t->salesInvoiceProductID;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceID = $t->salesInvoiceID;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->productID = $t->productID;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductPrice = $t->salesInvoiceProductPrice;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductDiscount = $t->salesInvoiceProductDiscount;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductDiscountType = $t->salesInvoiceProductDiscountType;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductQuantity = $t->salesInvoiceProductQuantity - $creditNoteProductQty;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceReturnQuantity = $invoiceReturnProductQty;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->productCode = $t->productCode;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->productName = $t->productName;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->productType = $t->productTypeID;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->productDescription = $t->salesInvoiceProductDescription;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->locationProductID = $t->locationProductID;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->documentTypeID = $t->documentTypeID;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->documentID = $t->salesInvoiceProductDocumentID;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->giftCard = $giftCardFlag;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceReturnBatchDetails = $invoiceReturnSubProductBatch;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceReturnSerialDetails = $invoiceReturnSubProductSerial;
                            if ($t->salesInvoiceSubProductID != '') {
                                $creditNoteQuantity = 0;
                                if ($creditNoteSubProductSerial[$t->productSerialID]) {
                                    $creditNoteQuantity = $creditNoteSubProductSerial[$t->productSerialID];
                                } else if ($creditNoteSubProductBatch[$t->productBatchID]) {
                                    $creditNoteQuantity = $creditNoteSubProductBatch[$t->productBatchID];
                                }

                                $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID] = (object) $subProduct;
                                $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->invoiceSubProductID = $t->salesInvoiceSubProductID;
                                $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->productBatchID = $t->productBatchID;
                                $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->productSerialID = $t->productSerialID;
                                $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->invoiceSubProductQuantity = $t->salesInvoiceSubProductQuantity - $creditNoteQuantity;
                            }
                            if ($salesInvoiceSubProduct) {
                                $salesInvoiceProduct[$t->salesInvoiceProductID]->subProduct = $salesInvoiceSubProduct[$t->salesInvoiceProductID];
                            }

                            if ($t->taxID) {
                                //unset $taxes array for new salesInvoiceProductIDs
                                if($productIdForTax != $t->salesInvoiceProductID){
                                    unset($taxes[$t->salesInvoiceSubProductID]);
                                }
                                $taxes[$t->salesInvoiceSubProductID][$t->taxID] = (object) $tax;
                                $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxID = $t->taxID;
                                $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxName = $t->taxName;
                                $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxPrecentage = $t->salesInvoiceProductTaxPrecentage;
                                $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxAmount = $t->salesInvoiceProductTaxAmount;
                                $taxes[$t->salesInvoiceSubProductID][$t->taxID]->isSuspended = $t->salesInvoiceSuspendedTax;
                                $taxes[$t->salesInvoiceSubProductID][$t->taxID]->taxSuspendable = $t->taxSuspendable;
                                $salesInvoiceProduct[$t->salesInvoiceProductID]->tax = $taxes[$t->salesInvoiceSubProductID];
                            }

                            $productIdForTax = $t->salesInvoiceProductID;
                        }



                    }
                    $locationID = $this->user_session->userActiveLocation['locationID'];
                    $result = $this->CommonTable('Inventory\Model\ProductTable')->getLocationProductsByLocationProductID($t->locationProductID)->current();
                    $locationID = (isset($result['locationID']))? $result['locationID']: $locationID;
                    $products[$t->salesInvoiceProductID] = $this->getLocationProductDetailsForReturn($locationID, $t->productID, $invalidFlag);
                    foreach ($products as $value) {
                        foreach ($value['tax'] as $key => $tax) {
                            if ($key == $t->id) {
                                if ($tax['tS'] == "0") {
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE_SOME_TAX_ARE_INACTIVE');
                                    return $this->JSONRespond();
                                }
                            }
                        }
                    }
                }

                // remove supended taxes
                foreach ($salesInvoiceProduct as $key => $value) {
                    foreach ($value->tax as $tax) {
                        if ($tax->taxSuspendable == "1" && $tax->isSuspended == "1") {
                           unset($value->tax [$tax->invoiceTaxID]);
                        }
                    }
                }

                $customerID = ($invoice->customerID) ? $invoice->customerID : 0;
                $customer = $this->getCustomerDetailsByCustomerID($customerID);

                if($customer->customerStatus == 2){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_CUSTOMER_INACTIVE');
                    return $this->JSONRespond();
                }

                $locationPro = Array();
                $inactiveItems = Array();
                foreach ($salesInvoiceProduct as $value) {
                    $locationPro[$value->productID] = $this->getLocationProductDetails($locationID, $value->productID);
                    if ($locationPro[$value->productID] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($value->productID);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }
                }

                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_INV_ITM_INACTIVE_COPY', [$errorItems]);
                }
                
                $promotionID = ($invoice->promotionID) ? $invoice->promotionID : 0;
                $promotion = $this->CommonTable('Settings\Model\PromotionTable')->getPromotion($promotionID);
                $this->status = true;
                $this->data = array(
                    'status' => true,
                    'invoice' => $invoice,
                    'invoiceProduct' => $salesInvoiceProduct,
                    'products' => $products,
                    'customer' => $customer,
                    'errorMsg' => $errorMsg,
                    'inactiveItemFlag' => $inactiveItemFlag,
                    'promotion' => $promotion
                );
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_USED_INV_FOR_CN');
            }
            
            return $this->JSONRespond();
        }
    }

    /**
     * @author Ashan madushka <ashan@thinkcube.com>
     * this function return details of invoice
     */
    public function getInvoiceDetailsForReturnNoteAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invalidFlag = false;
            $invoiceID = $request->getPost('invoiceID');
            $invalidFlag =  filter_var($request->getPost('invalidFlag'), FILTER_VALIDATE_BOOLEAN);

            $invoiceProductsArray = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
            $dparray;
            $dparrayQty;
            $iparray;
            $iparrayQty;
            $realproduct;
            $checkProduct = 0;
            $dproductType;
            $locationID = $this->user_session->userActiveLocation['locationID'];

            foreach ($invoiceProductsArray as $dp) {
                $dp = (object) $dp;
                $dparray[$dp->salesInvoiceProductID] = $dp->productID;
                $dparrayQty[$dp->salesInvoiceProductID] = $dp->salesInvoiceProductQuantity;
                $dproductType[$dp->salesInvoiceProductID] = $dp->productTypeID;
            }

            $data = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductsByInvoiceID($invoiceID);
            foreach ($data as $ip) {
                $ip = (object) $ip;
                if($ip->locationID == $locationID){
                    $iparray[$ip->productID] = $ip->productID;
                    $iparrayQty[$ip->productID]+=$ip->creditNoteProductQuantity;
                }
            }
            // $returndata = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->getInvoiceReturnProductsByInvoiceID($invoiceID);

            // foreach ($returndata as $ipp) {
            //     $ipp = (object) $ipp;
            //     if($ipp->locationID == $locationID){
            //         $iparray[$ipp->productID] = $ipp->productID;
            //         $iparrayQty[$ipp->productID]+=$ipp->invoiceReturnProductQuantity;
            //     }
            // }

            foreach ($dparrayQty as $key => $Pdata) {
                if ($iparrayQty[$key] != $Pdata || ($Pdata == NULL && !array_key_exists($key, $iparray))) {
                    $checkProduct = 1;
                    $realproduct[$key] = $key;
                }
            }

            if ($checkProduct) {
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                $promotion = $this->CommonTable('Settings\Model\PromotionTable')->getPromotionDetailsByInvoiceID($invoiceID)[0];
                $invoice->promotionType = $promotion['promotionType'];

                $invoiceProduct = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
                $tax = array(
                    'invoiceTaxID' => '',
                    'invoiceTaxName' => '',
                    'invoiceTaxPrecentage' => '',
                    'invoiceTaxAmount' => '',
                    'isSuspended' => '',
                    'taxSuspendable' => '',
                );
                $subProduct = array(
                    'invoiceSubProductID' => '',
                    'productBatchID' => '',
                    'productSerialID' => '',
                    'invoiceSubProductQuantity' => '',
                );

                $Products = array(
                    'invoiceProductID' => '',
                    'invoiceID' => '',
                    'productID' => '',
                    'invoiceProductPrice' => '',
                    'invoiceProductDiscount' => '',
                    'invoiceProductDiscountType' => '',
                    'invoiceProductQuantity' => '',
                    'productCode' => '',
                    'productName' => '',
                    'productType' => '',
                    'tax' => '',
                    'subProduct' => '',
                );

                while ($tt = $invoiceProduct->current()) {
                    $t = (object) $tt;
                    
                    if($t->productSerialID != '' && $invalidFlag){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INACTIVE_SERIAL_ITEM');
                        return $this->JSONRespond();
                    }
                    if ($realproduct[$t->salesInvoiceProductID] == $t->salesInvoiceProductID) {
                        $creditNoteProductData = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductAndSubProductDataByInvoiceProductID($t->salesInvoiceProductID);
                        $creditNoteProductQty = 0;
                        $creditNoteSubProductSerial = array();
                        $creditNoteSubProductBatch = array();
                        $creditNoteProductIDArray = array();

                        foreach ($creditNoteProductData as $prValue) {
                            $prValue = (object) $prValue;
                            if ($prValue->creditNoteProductID) {
//use this if statement for ignore repeating creditNoteProducts
                                if (!$creditNoteProductIDArray[$prValue->creditNoteProductID] == 1) {
                                    $creditNoteProductQty+=$prValue->creditNoteProductQuantity;
                                    $creditNoteProductIDArray[$prValue->creditNoteProductID] = 1;
                                }
                                if ($prValue->productSerialID) {
                                    $creditNoteSubProductSerial[$prValue->productSerialID] += $prValue->creditNoteSubProductQuantity;
                                } else if ($prValue->productBatchID) {
                                    $creditNoteSubProductBatch[$prValue->productBatchID] += $prValue->creditNoteSubProductQuantity;
                                }
                            }
                        }

                        $invoiceReturnProductData = $this->CommonTable('Invoice\Model\InvoiceReturnProductTable')->getInvoiceReturnProductAndSubProductDataByInvoiceProductID($t->salesInvoiceProductID);

                        foreach ($invoiceReturnProductData as $rtnValue) {
                            $rtnValue = (object) $rtnValue;

                            if ($rtnValue->invoiceReturnProductID) {
                                if (!$creditNoteProductIDArray[$rtnValue->invoiceReturnProductID] == 1) {
                                    $creditNoteProductQty+=$rtnValue->invoiceReturnProductQuantity;
                                    $creditNoteProductIDArray[$rtnValue->invoiceReturnProductID] = 1;
                                }
                                if ($rtnValue->productSerialID) {
                                    $creditNoteSubProductSerial[$rtnValue->productSerialID] += $rtnValue->invoiceReturnSubProductQuantity;
                                } else if ($rtnValue->productBatchID) {
                                    $creditNoteSubProductBatch[$rtnValue->productBatchID] += $rtnValue->invoiceReturnSubProductQuantity;
                                }
                            }
                        }

                        $draftInvoiceReturnProductData = $this->CommonTable('Invoice\Model\DraftInvoiceReturnProductTable')->getInvoiceReturnProductAndSubProductDataByInvoiceProductID($t->salesInvoiceProductID);

                        foreach ($draftInvoiceReturnProductData as $drtnValue) {
                            $drtnValue = (object) $drtnValue;

                            if ($drtnValue->draftInvoiceReturnProductID) {
                                if (!$creditNoteProductIDArray[$drtnValue->draftInvoiceReturnProductID] == 1) {
                                    $creditNoteProductQty+=$drtnValue->invoiceReturnProductQuantity;
                                    $creditNoteProductIDArray[$drtnValue->draftInvoiceReturnProductID] = 1;
                                }
                                if ($drtnValue->productSerialID) {
                                    $creditNoteSubProductSerial[$drtnValue->productSerialID] += $drtnValue->invoiceReturnSubProductQuantity;
                                } else if ($drtnValue->productBatchID) {
                                    $creditNoteSubProductBatch[$drtnValue->productBatchID] += $drtnValue->invoiceReturnSubProductQuantity;
                                }
                            }
                        }


                        $productHandlingData = $this->CommonTable('Inventory\Model\ProductHandelingTable')->getProductHandlingByProductID($t->productID);
                        $giftCardFlag = ($productHandlingData->productHandelingGiftCard == 1)? 1:0;

                        $salesInvoiceProduct[$t->salesInvoiceProductID] = (object) $Products;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductID = $t->salesInvoiceProductID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceID = $t->salesInvoiceID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productID = $t->productID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductPrice = $t->salesInvoiceProductPrice;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductDiscount = $t->salesInvoiceProductDiscount;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductDiscountType = $t->salesInvoiceProductDiscountType;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->invoiceProductQuantity = $t->salesInvoiceProductQuantity - $creditNoteProductQty;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productCode = $t->productCode;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productName = $t->productName;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productType = $t->productTypeID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->productDescription = $t->salesInvoiceProductDescription;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->locationProductID = $t->locationProductID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->documentTypeID = $t->documentTypeID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->documentID = $t->salesInvoiceProductDocumentID;
                        $salesInvoiceProduct[$t->salesInvoiceProductID]->giftCard = $giftCardFlag;
                        if ($t->salesInvoiceSubProductID != '') {
                            $creditNoteQuantity = 0;
                            if ($creditNoteSubProductSerial[$t->productSerialID]) {
                                $creditNoteQuantity = $creditNoteSubProductSerial[$t->productSerialID];
                            } else if ($creditNoteSubProductBatch[$t->productBatchID]) {
                                $creditNoteQuantity = $creditNoteSubProductBatch[$t->productBatchID];
                            }

                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID] = (object) $subProduct;
                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->invoiceSubProductID = $t->salesInvoiceSubProductID;
                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->productBatchID = $t->productBatchID;
                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->productSerialID = $t->productSerialID;
                            $salesInvoiceSubProduct[$t->salesInvoiceProductID][$t->salesInvoiceSubProductID]->invoiceSubProductQuantity = $t->salesInvoiceSubProductQuantity - $creditNoteQuantity;
                        }
                        if ($salesInvoiceSubProduct) {
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->subProduct = $salesInvoiceSubProduct[$t->salesInvoiceProductID];
                        }

                        if ($t->taxID) {
                            //unset $taxes array for new salesInvoiceProductIDs
                            if($productIdForTax != $t->salesInvoiceProductID){
                                unset($taxes[$t->salesInvoiceSubProductID]);
                            }
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID] = (object) $tax;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxID = $t->taxID;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxName = $t->taxName;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxPrecentage = $t->salesInvoiceProductTaxPrecentage;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->invoiceTaxAmount = $t->salesInvoiceProductTaxAmount;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->isSuspended = $t->salesInvoiceSuspendedTax;
                            $taxes[$t->salesInvoiceSubProductID][$t->taxID]->taxSuspendable = $t->taxSuspendable;
                            $salesInvoiceProduct[$t->salesInvoiceProductID]->tax = $taxes[$t->salesInvoiceSubProductID];
                        }

                        $productIdForTax = $t->salesInvoiceProductID;

                    }
                    $locationID = $this->user_session->userActiveLocation['locationID'];
                    $result = $this->CommonTable('Inventory\Model\ProductTable')->getLocationProductsByLocationProductID($t->locationProductID)->current();
                    $locationID = (isset($result['locationID']))? $result['locationID']: $locationID;
                    $products[$t->salesInvoiceProductID] = $this->getLocationProductDetailsForReturn($locationID, $t->productID, $invalidFlag);
                    foreach ($products as $value) {
                        foreach ($value['tax'] as $key => $tax) {
                            if ($key == $t->id) {
                                if ($tax['tS'] == "0") {
                                    $this->status = false;
                                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_INVOICE_SOME_TAX_ARE_INACTIVE');
                                    return $this->JSONRespond();
                                }
                            }
                        }
                    }
                }

                // remove supended taxes
                foreach ($salesInvoiceProduct as $key => $value) {
                    foreach ($value->tax as $tax) {
                        if ($tax->taxSuspendable == "1" && $tax->isSuspended == "1") {
                           unset($value->tax [$tax->invoiceTaxID]);
                        }
                    }
                }

                $customerID = ($invoice->customerID) ? $invoice->customerID : 0;
                $customer = $this->getCustomerDetailsByCustomerID($customerID);

                if($customer->customerStatus == 2){
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_CREDIT_NOTE_CUSTOMER_INACTIVE');
                    return $this->JSONRespond();
                }

                $locationPro = Array();
                $inactiveItems = Array();
                foreach ($salesInvoiceProduct as $value) {
                    $locationPro[$value->productID] = $this->getLocationProductDetails($locationID, $value->productID);
                    if ($locationPro[$value->productID] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($value->productID);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }
                }

                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_INV_ITM_INACTIVE_COPY', [$errorItems]);
                }
                
                $promotionID = ($invoice->promotionID) ? $invoice->promotionID : 0;
                $promotion = $this->CommonTable('Settings\Model\PromotionTable')->getPromotion($promotionID);
                $this->status = true;
                $this->data = array(
                    'status' => true,
                    'invoice' => $invoice,
                    'invoiceProduct' => $salesInvoiceProduct,
                    'products' => $products,
                    'customer' => $customer,
                    'errorMsg' => $errorMsg,
                    'inactiveItemFlag' => $inactiveItemFlag,
                    'promotion' => $promotion
                );
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_USED_INV_FOR_CN');
            }
            
            return $this->JSONRespond();
        }
    }


    /**
     * @author Sandun <sandun@thinkcube.com>
     * this function return details of invoice edit
     */
    public function getInvoiceEditDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invoiceID = $request->getPost('invoiceID');
            $invoiceProductsArray = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
            $dparray;
            $dparrayQty;
            $iparray;
            $iparrayQty;
            $realproduct;
            $checkProduct = 0;
            $dproductType;
            $locationID = $this->user_session->userActiveLocation['locationID'];

            foreach ($invoiceProductsArray as $dp) {
                $dp = (object) $dp;
                $dparray[$dp->locationProductID] = $dp->productID;
                $dparrayQty[$dp->locationProductID] = $dp->salesInvoiceProductQuantity;
                $dproductType[$dp->locationProductID] = $dp->productTypeID;
            }

            $data = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductsByInvoiceID($invoiceID);
            foreach ($data as $ip) {
                if($ip->locationID == $locationID){
                    $ip = (object) $ip;
                    $iparray[$ip->productID] = $ip->productID;
                    $iparrayQty[$ip->productID]+=$ip->creditNoteProductQuantity;
                }
            }

            foreach ($dparrayQty as $key => $Pdata) {
                if ($iparrayQty[$key] != $Pdata || ($Pdata == NULL && !array_key_exists($key, $iparray))) {
                    $checkProduct = 1;
                    $realproduct[$key] = $key;
                }
            }

            if ($checkProduct) {
                $invoice = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($invoiceID);
                $invoiceProduct = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
                $tax = array(
                    'invoiceTaxID' => '',
                    'invoiceTaxName' => '',
                    'invoiceTaxPrecentage' => '',
                    'invoiceTaxAmount' => '',
                );
                $subProduct = array(
                    'invoiceSubProductID' => '',
                    'productBatchID' => '',
                    'productSerialID' => '',
                    'invoiceSubProductQuantity' => '',
                );

                $Products = array(
                    'invoiceProductID' => '',
                    'invoiceID' => '',
                    'productID' => '',
                    'invoiceProductPrice' => '',
                    'inclusiveTaxInvoiceProductPrice' => '',
                    'invoiceProductDiscount' => '',
                    'invoiceProductDiscountType' => '',
                    'invoiceProductQuantity' => '',
                    'productCode' => '',
                    'productName' => '',
                    'productType' => '',
                    'tax' => '',
                    'subProduct' => '',
                    'itemSalesPersonID' => ''
                );

                $incID = 1;
                $incFlag = FALSE;

                $addedFreeIssueItems = [];
                $incItems = [];
                while ($tt = $invoiceProduct->current()) {
                    $t = (object) $tt;
                    if ($realproduct[$t->locationProductID] == $t->locationProductID) {
                        $creditNoteProductData = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductAndSubProductDataByInvoiceProductID($t->salesInvoiceProductID);
                        $creditNoteProductQty = 0;
                        $creditNoteSubProductSerial = array();
                        $creditNoteSubProductBatch = array();
                        $creditNoteProductIDArray = array();

                        foreach ($creditNoteProductData as $prValue) {
                            $prValue = (object) $prValue;
                            if ($prValue->creditNoteProductID) {
                                //use this if statement for ignore repeating creditNoteProducts
                                if (!$creditNoteProductIDArray[$prValue->creditNoteProductID] == 1) {
                                    $creditNoteProductQty+=$prValue->creditNoteProductQuantity;
                                    $creditNoteProductIDArray[$prValue->creditNoteProductID] = 1;
                                }
                                if ($prValue->productSerialID) {
                                    $creditNoteSubProductSerial[$prValue->productSerialID] += $prValue->creditNoteSubProductQuantity;
                                } else if ($prValue->productBatchID) {
                                    $creditNoteSubProductBatch[$prValue->productBatchID] += $prValue->creditNoteSubProductQuantity;
                                }
                            }
                        }
                        $salesInvoiceProduct[$t->productID . '_' . $incID] = (object) $Products;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductID = $t->salesInvoiceProductID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceID = $t->salesInvoiceID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productID = $t->productID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->locationProductID = $t->locationProductID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->documentTypeID = $t->documentTypeID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductDocumentID = $t->salesInvoiceProductDocumentID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductPrice = $t->salesInvoiceProductPrice;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->inclusiveTaxInvoiceProductPrice = $t->inclusiveTaxSalesInvoiceProductPrice;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductDiscount = $t->salesInvoiceProductDiscount;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductDiscountType = $t->salesInvoiceProductDiscountType;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductQuantity = $t->salesInvoiceProductQuantity - $creditNoteProductQty;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productCode = $t->productCode;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productName = $t->productName;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productType = $t->productTypeID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productDescription = $t->salesInvoiceProductDescription;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->itemSalesPersonID = $t->itemSalesPersonID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductMrpType = $t->salesInvoiceProductMrpType;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductMrpValue = $t->salesInvoiceProductMrpValue;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductMrpPercentage = $t->salesInvoiceProductMrpPercentage;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductMrpAmount = $t->salesInvoiceProductMrpAmount;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->isFreeItem = ($t->isFreeItem == 1) ? true : false;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->hasFreeItem = ($t->hasFreeItem == 1) ? true : false;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->incID = $incID;
                        $mainProductID = null;
                        $mainIvoiceProductID = null;
                        if ($t->isFreeItem == 1) {
                            $freeItemData = $this->CommonTable('Invoice\Model\InvoiceFreeProductTable')->getRelatedFreeProductsByInvoiceAndFreeProductID($t->salesInvoiceID, $t->productID)->current();

                            if ($freeItemData) {
                                $mainProductID = $freeItemData['mainProductID'];
                                $mainIvoiceProductID = $freeItemData['mainSalesInvoiceProductID'];
                            }

                        }
                            
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->mainProductID = $mainProductID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->mainIvoiceProductID = $mainIvoiceProductID;

                        $subIncFlag = $incID - 1;
                        if ($t->salesInvoiceSubProductID != '') {
                            $creditNoteQuantity = 0;
                            if ($creditNoteSubProductSerial[$t->productSerialID]) {
                                $creditNoteQuantity = $creditNoteSubProductSerial[$t->productSerialID];
                            } else if ($creditNoteSubProductBatch[$t->productBatchID]) {
                                $creditNoteQuantity = $creditNoteSubProductBatch[$t->productBatchID];
                            }
                            if ( $t->productSerialID != ''){
                                $serialProduct = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($t->productSerialID);
                            }

                            if ( $t->productBatchID != ''){
                                $bathcProducts = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($t->productBatchID);
                            }
                            $subIncFlag = $incID - 1;
                            if (!empty($salesInvoiceProduct[$t->productID . '_' . $subIncFlag]->subProduct) && $salesInvoiceProduct[$t->productID . '_' . $subIncFlag]->invoiceProductID == $t->salesInvoiceProductID) {
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->salesInvoiceSubProductID] = (object) $subProduct;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->salesInvoiceSubProductID]->invoiceSubProductID = $t->salesInvoiceSubProductID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->salesInvoiceSubProductID]->productBatchID = $t->productBatchID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->salesInvoiceSubProductID]->productBatchCode = ($bathcProducts->productBatchCode) ? $bathcProducts->productBatchCode : null;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->salesInvoiceSubProductID]->productSerialID = $t->productSerialID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->salesInvoiceSubProductID]->productSerialCode = ($serialProduct->productSerialCode) ? $serialProduct->productSerialCode : null;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->salesInvoiceSubProductID]->salesInvoiceSubProductWarranty = $t->salesInvoiceSubProductWarranty;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->salesInvoiceSubProductID]->salesInvoiceSubProductWarrantyType = $t->salesInvoiceSubProductWarrantyType;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->salesInvoiceSubProductID]->invoiceSubProductQuantity = $t->salesInvoiceSubProductQuantity - $creditNoteQuantity;
                                $salesInvoiceProduct[$t->productID . '_' . $subIncFlag]->subProduct = $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag];
                                unset($salesInvoiceProduct[$t->productID . '_' . $incID]);
                                --$incID;
                                $incFlag = TRUE;
                            } else {
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID] = (object) $subProduct;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->invoiceSubProductID = $t->salesInvoiceSubProductID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->productBatchID = $t->productBatchID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->productBatchCode = ($bathcProducts->productBatchCode) ? $bathcProducts->productBatchCode : null;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->productSerialID = $t->productSerialID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->productSerialCode = ($serialProduct->productSerialCode) ? $serialProduct->productSerialCode : null;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->salesInvoiceSubProductWarranty = $t->salesInvoiceSubProductWarranty;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->salesInvoiceSubProductWarrantyType = $t->salesInvoiceSubProductWarrantyType;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->invoiceSubProductQuantity = $t->salesInvoiceSubProductQuantity - $creditNoteQuantity;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->copyFromDeliveryNote = false;


                                if (!empty($t->documentTypeID) && $t->documentTypeID == 4 && $t->batchProduct) {
                                    $batchItemData = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteBatchProductByDeliveryNoteId($t->salesInvoiceProductDocumentID);
                                    foreach ($batchItemData as $key => $value) {
                                        if ($value['productBatchID'] == $t->productBatchID) {
                                            $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->copyFromDeliveryNote = true;
                                            $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->salesInvoiceSubProductID]->RemainDeliBtchQty = floatval($value['deliveryProductSubQuantity']) - floatval($value['deliveryNoteSubProductsCopiedQuantity']);
                                        }
                                    }
                                }
                                $salesInvoiceProduct[$t->productID . '_' . $incID]->subProduct = $salesInvoiceSubProduct[$t->productID . '_' . $incID];

                            }
                        }

                        if ($t->taxID) {
                            $taxes[$t->salesInvoiceProductID][$t->taxID] = (object) $tax;
                            $taxes[$t->salesInvoiceProductID][$t->taxID]->invoiceTaxID = $t->taxID;
                            $taxes[$t->salesInvoiceProductID][$t->taxID]->invoiceTaxName = $t->taxName;
                            $taxes[$t->salesInvoiceProductID][$t->taxID]->invoiceTaxPrecentage = $t->salesInvoiceProductTaxPrecentage;
                            $taxes[$t->salesInvoiceProductID][$t->taxID]->invoiceTaxAmount = $t->salesInvoiceProductTaxAmount;

                            $subTaxIncFlag = $incID - 1;

                            if (!empty($salesInvoiceProduct[$t->productID . '_' . $subTaxIncFlag]->tax) && $salesInvoiceProduct[$t->productID . '_' . $subTaxIncFlag]->invoiceProductID == $t->salesInvoiceProductID) {
                                $salesInvoiceProduct[$t->productID . '_' . $subTaxIncFlag]->tax = $taxes[$t->salesInvoiceProductID];
                                unset($salesInvoiceProduct[$t->productID . '_' . $incID]);
                                //If there has sub products so dont need to reduce $incID
                                //because if it has a sub product then it already has reduce 1 that previous condition
                                if (!$incFlag) {
                                    --$incID;
                                }
                            } else {

                                $salesInvoiceProduct[$t->productID . '_' . $incID]->tax = $taxes[$t->salesInvoiceProductID];
                            }
                        }
                    }
                    $locationID = $this->user_session->userActiveLocation['locationID'];

                    $products[$t->productID] = $this->getLocationProductDetailsForReturn($locationID, $t->productID);
                    $v = $t->productID.'_'.$t->salesInvoiceProductID;
                    $incItems[$v] = $incID;
                    $incID++;
                }

                foreach ($salesInvoiceProduct as $key1 => $value1) {
                   
                    if ($value1->hasFreeItem == 1) {
                        $freeItemData = $this->CommonTable('Invoice\Model\InvoiceFreeProductTable')->getRelatedFreeProductsByInvoiceAndProductID($value1->invoiceID, $value1->productID);

                        foreach ($freeItemData as $key => $value) {
                            $addedFreeIssueItems[$key1][$value['freeProductID']] = $value;
                        }
                    }
                }

                $customerID = ($invoice->customerID) ? $invoice->customerID : 0;
                $customer = $this->getCustomerDetailsByCustomerID($customerID);
                $promotionID = ($invoice->promotionID) ? $invoice->promotionID : 0;
                $promotion = $this->getPromotionDetailsForEdit($promotionID)[$promotionID];
                $currencySymbol = ($invoice->customCurrencyId) ? $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($invoice->customCurrencyId)->currencySymbol : $this->companyCurrencySymbol;

                $locationProducts = Array();
                $inactiveItems = Array();
                foreach ($salesInvoiceProduct as $invProducts) {
                    $locationProducts[$invProducts->productID] = $this->getLocationProductDetails($locationID, $invProducts->productID);
                    if ($locationProducts[$invProducts->productID] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($invProducts->productID);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }


                    if ($invProducts->documentTypeID == 4) {
                        $docData = $this->CommonTable("Invoice\Model\DeliveryNoteProductTable")->getProductByDeliveryNoteIDAndProductId($invProducts->salesInvoiceProductDocumentID,$invProducts->productID);

                        $invProducts->deliveryNoteProductQuantity = ((float)$docData->deliveryNoteProductQuantity - (float)$docData->deliveryNoteProductCopiedQuantity) + (float)$invProducts->invoiceProductQuantity;
                    }
                }
                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_INV_ITM_INACTIVE', [$errorItems]);
                }

                $dimensionData = [];
                $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(1,$invoiceID);
                foreach ($jEDimensionData as $value) {
                    if (!is_null($value['journalEntryID'])) {
                        $temp = [];
                        if($value['dimensionType'] == "job" || $value['dimensionType'] == "project") {
                            if ($value['dimensionType'] == "job") {
                                $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($value['dimensionValueID']);
                                $valueTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                                $dimensionTxt = "Job";
                            } else if ($value['dimensionType'] == "project") {
                                $proData = $this->CommonTable('Jobs\Model\ProjectTable')->getProjectByProjectId($value['dimensionValueID']);
                                $valueTxt = $proData->projectCode.'-'.$proData->projectName;
                                $dimensionTxt = "Project";
                            }

                            $temp['dimensionTypeId'] = $value['dimensionType'];
                            $temp['dimensionTypeTxt'] = $dimensionTxt;
                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                            $temp['dimensionValueTxt'] = $valueTxt;
                        } else {
                            $temp['dimensionTypeId'] = $value['dimensionType'];
                            $temp['dimensionTypeTxt'] = $value['dimensionName'];
                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                            $temp['dimensionValueTxt'] = $value['dimensionValue'];
                        }
                        $dimensionData[$invoice->salesInvoiceCode][$value['dimensionType']] = $temp;
                    }
                }

                $membershipData = [];
                $membershipRes = $this->CommonTable('Invoice\Model\InvoiceTable')->getMembershipByInvoiceId($invoiceID)->current();
                if ($membershipRes) {
                    $membershipData['nextInvoiceDate'] = $membershipRes['nextInvoiceDate'];
                    $membershipData['membershipComment'] = $membershipRes['membershipComment'];
                }

                $relateServiceChargeArr = [];
                if (!empty($invoice->salesInvoiceServiceChargeAmount)) {
                    $relatedServiceCharges = $this->CommonTable('Invoice\Model\InvoiceServiceChargeTable')->getInvoiceChargesByInvoiceID($invoiceID);

                    foreach ($relatedServiceCharges as $key2 => $value2) {
                        $relateServiceChargeArr[$value2['serviceChargeTypeID']] = $value2; 
                    }
                       
                } 

                $uploadedAttachments = [];
                $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($invoiceID, 1);
                $companyName = $this->getSubdomain();
                $path = '/userfiles/' . md5($companyName) . "/attachments";

                $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getSalesPersonByInvoiceID($invoiceID);

                $relatedSalesPersons = (!empty($relatedSalesPersons)) ? $relatedSalesPersons : [];

                foreach ($attachmentData as $value) {
                    $temp = [];
                    $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
                    $temp['documentRealName'] = $value['documentRealName'];
                    $temp['docLink'] = $path . "/" .$value['documentName'];
                    $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
                }

                $this->status = true;
                $this->data = array(
                    'status' => true,
                    'invoice' => $invoice,
                    'relatedSalesPersons'=> $relatedSalesPersons,
                    'relateServiceChargeArr' => $relateServiceChargeArr,
                    'invoiceProduct' => $salesInvoiceProduct,
                    'products' => $products,
                    'customer' => $customer,
                    'promotion' => $promotion,
                    'currencySymbol' => $currencySymbol,
                    'inactiveItemFlag' => $inactiveItemFlag,
                    'errorMsg' => $errorMsg,
                    'dimensionData' => $dimensionData,
                    'membershipData' => $membershipData,
                    'uploadedAttachments' => $uploadedAttachments,
                    'addedFreeIssueItems' => $addedFreeIssueItems,
                    'incItems' => $incItems
                );
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_USED_INV_FOR_CN');
            }
            return $this->JSONRespond();
        }
    }

    private function getPromotionDetailsForEdit($promotionID){
        $this->service = $this->getService('PromotionService');
        $promotionDetails = $this->service->getPromotionDetails($promotionID);

        return $promotionDetails;
    }
    private function getPromotionDetails($promotionID)
    {
        $promotionType = $this->CommonTable('Settings\Model\PromotionTable')->getPromotion($promotionID)['promotionType'];
        $promotionData = $this->CommonTable('Settings\Model\PromotionTable')->getPromotionWithDetails($promotionID, $promotionType);
        if ($promotionType == 1) {
            $promotionDetails = array();
            foreach ($promotionData as $promo) {
                $tempPromo['promoID'] = $promo['promotionID'];
                $productUoms = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomsListByProductID($promo['promoProductID']);
                $tempPromo['promoName'] = $promo['promotionName'];
                $tempPromo['promoFromDate'] = $promo['promotionFromDate'];
                $tempPromo['promoToDate'] = $promo['promotionToDate'];
                $tempPromo['promoType'] = $promo['promotionType'];
                $tempPromo['promoDesc'] = $promo['promotionDescription'];
                $tempPromo['promoLocationID'] = $promo['promotionLocation'];
                if ($promo['promotionLocation'] != NULL) {
                    $tempPromo['promoLocationName'] = $this->CommonTable('Settings\Model\LocationTable')->getLocationByID($promo['promotionLocation'])->locationName;
                } else {
                    $tempPromo['promoLocationName'] = 'Apply for every location';
                }
                $tempPromo['promoStatusID'] = $promo['promotionStatus'];
                $tempPromo['promoStatus'] = $this->getStatusCode($promo['promotionStatus']);
                $tempPromo['promoStatus'] = $this->getStatusCode($promo['promotionStatus']);
                $promotionProducts = isset($promotionDetails[$promo['promotionID']]['promoProducts']) ? $promotionDetails[$promo['promotionID']]['promoProducts'] : array();
                $promotionProducts[$promo['promotionProductID']] = array(
                    'productID' => $promo['promoProductID'],
                    'productName' => $promo['productName'] . '-' . $promo['productCode'],
                    'locationProductID' => $promo['locationProductID'],
                    'minQty' => $promo['promotionProductMinQty'],
                    'maxQty' => $promo['promotionProductMaxQty'],
                    'discountType' => $promo['promotionProductDiscountType'],
                    'discountAmount' => $promo['promotionProductDiscountAmount'],
                    'uom' => $productUoms,
                );
                $tempPromo['promoProducts'] = $promotionProducts;
                $promotionDetails[$promo['promotionID']] = $tempPromo;
            }
        } else if ($promotionType == 2) {
            $promotionDetails = array();
            $promoArray = $promotionData[0];
            $tempPromo['promoID'] = $promoArray['promotionID'];
            $tempPromo['promoName'] = $promoArray['promotionName'];
            $tempPromo['promoFromDate'] = $promoArray['promotionFromDate'];
            $tempPromo['promoToDate'] = $promoArray['promotionToDate'];
            $tempPromo['promoType'] = $promoArray['promotionType'];
            $tempPromo['promoDesc'] = $promo['promotionDescription'];
            $tempPromo['promoLocationID'] = $promoArray['promotionLocation'];
            if ($promoArray['promotionLocation'] != NULL) {
                $tempPromo['promoLocationName'] = $this->CommonTable('Settings\Model\LocationTable')->getLocationByID($promoArray['promotionLocation'])->locationName;
            } else {
                $tempPromo['promoLocationName'] = 'Apply for every location';
            }
            $tempPromo['promoStatusID'] = $promoArray['promotionStatus'];
            $tempPromo['promoStatus'] = $this->getStatusCode($promoArray['promotionStatus']);
            $tempPromo['invoicePromotionID'] = $promoArray['invoicePromotionID'];
            $tempPromo['minValue'] = $promoArray['invoicePromotionMinValue'];
            $tempPromo['maxValue'] = $promoArray['invoicePromotionMaxValue'];
            $tempPromo['discountType'] = $promoArray['invoicePromotionDiscountType'];
            $tempPromo['discountAmount'] = $promoArray['invoicePromotionDiscountAmount'];
            $promotionDetails[$promoArray['promotionID']] = $tempPromo;
        }
        return $promotionDetails;
    }

    public function getCustomersForPurchasedProductsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $productList = $request->getPost('products');
            $filterCondition = $request->getPost('condition');
            $customerList = [];
            if ($filterCondition != 'all') {
                foreach ($productList as $product) {
                    $customers = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getCustomersForProduct($product);
                    foreach ($customers as $customer) {
                        if ($customer['customerID'] != 0) {
                            $customerList[$customer['customerID']] = array(
                                'name' => $customer['customerTitle'] . ' ' . $customer['customerName'],
                                'email' => $customer['customerProfileEmail'],
                                'telephoneNumber' => $customer['customerTelephoneNumber'],
                                'customerStatus' => $customer['customerStatus'] == 1 ? 'Active':'Inactive'
                            );
                        }
                    }
                }
            } else {
                $customers = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getCustomersForAllProducts($productList);
                foreach ($customers as $customer) {
                    if ($customer['customerID'] != 0) {
                        $customerList[$customer['customerID']] = array(
                            'name' => $customer['customerTitle'] . ' ' . $customer['customerName'],
                            'email' => $customer['customerProfileEmail'],
                            'telephoneNumber' => $customer['customerTelephoneNumber'], 
                            'customerStatus' => $customer['customerStatus'] == 1 ? 'Active':'Inactive'
                        );
                    }
                }
            }
            $this->data = (empty($customerList) ? NULL : $customerList);
            return $this->JSONRespond();
        }
    }

    /**
     * @author Sandun Dissanayake <sandun@thinkcube.com>
     * delete invoice - updates quantity and customer balance
     * @return JSONRespond
     */
    public function deleteInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invoiceID = $request->getPost('invoiceID');
            $returnDeliveryNote = ($request->getPost('returnDeliveryNote') == 'true') ? true : false;
            $invoiceTypeFlag = $request->getPost('invoiceTypeFlag');
            $ignoreBudgetLimit = $request->getPost('ignoreBudgetLimit');
            $getInvoiceData = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoiceByID($invoiceID);

            // to avoid cancel already canceled invoice
            if ($getInvoiceData->statusID == 5) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CANCEL_CANCELED_INVOICE');
                return $this->JSONRespond();
            }

            //this condition use to avoid replaced invoices delete problem by two tabs
            if($getInvoiceData->statusID == 10){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DELETE_EDITED_INVOICE');
                return $this->JSONRespond();
            }

            //check credit notes that related given invoice id. if its true then cannot delete invoice.
            $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNotesByInvoiceID($invoiceID)->current();
                
            if(!empty($creditNoteDetails)){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DELETE_CREDIT_NOTED_INVOICE');
                return $this->JSONRespond();
            } 

            $getInvoiceReturn = $this->CommonTable('Invoice\Model\InvoiceReturnTable')->getInvoiceReturnByInvoiceIDForEdit($invoiceID);

            if (sizeof($getInvoiceReturn) > 0) {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_DELETE_INV_RETURN_INVOICE');
                return $this->JSONRespond();
            }

            $invoiceCode =$getInvoiceData->salesInvoiceCode;
            $locationID = $getInvoiceData->locationID;
            $invoiceEntityID = $getInvoiceData->entityID;
            $customerID = $getInvoiceData->customerID;
            $totalPrice = $getInvoiceData->salesinvoiceTotalAmount;
            $getPaymentData = $this->CommonTable("Invoice\Model\InvoicePaymentsTable")->isPaymentsForInvoiceID($invoiceID);

            $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
            $displayData = $result->current();
            $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
            $averageCostingFlag = $displayData['averageCostingFlag'];

            if($FIFOCostingFlag == 0 && $averageCostingFlag == 0){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE');
                return $this->JSONRespond();
            }

            if (!$getPaymentData->current()) {
                $this->beginTransaction();

                //get products details by related invoice ID
                $getInvoiceProductData = $this->CommonTable("Invoice\Model\InvoiceProductTable")->getProductsForInvoiceCancelByInvoiceID($invoiceID);

                $invoiceProductData = [];
                $invoiceSubProducts = [];
                $incremntID = 0;
                foreach ($getInvoiceProductData as $key => $g) {

                    if ($getInvoiceData->quotationID != NULL) {
                        $quotationID = $getInvoiceData->quotationID;

                        $getQuotationProductData = $this->CommonTable("Invoice\Model\QuotationProductTable")->getProductsByQuotationID($quotationID);

                        foreach ($getQuotationProductData as $key => $qData) {
                            if ($g['locationProductID'] == $qData['locationProductID']) {
                                $copiedQty = $qData['quotationProductCopiedQuantity'];

                                $newQuotationCopiedQty = $copiedQty - $g['salesInvoiceProductQuantity'];
                                
                                $data = array(
                                    'quotationProductCopiedQuantity' => $newQuotationCopiedQty,
                                );
                                
                                $this->CommonTable("Invoice\Model\QuotationProductTable")->updateCopiedQuotationProducts($g['locationProductID'], $quotationID , $data);
                            }  
                        }

                    }
                    $incremntID++;
                    $invoiceProductData['products'][$incremntID] = array(
                        'salesInvoiceProductID' => $g['salesInvoiceProductID'],
                        'productID' => $g['productID'],
                        'invoiceID' => $g['salesInvoiceID'],
                        'invoiceQuantity' => $g['salesInvoiceProductQuantity'],
                        'locationProductID' => $g['locationProductID'],
                        'documentTypeID' => $g['documentTypeID'],
                        'documentID' => $g['salesInvoiceProductDocumentID'],
                    );
                    if ($g['productTypeID'] === '1') {
                        $inventoryProductIDs[] = $g['productID'];
                    }

                    $invoiceSubProductData = $this->CommonTable("Invoice\Model\InvoiceSubProductTable")->getSalesInvoiceSubProductBySalesInvoiceID($g['salesInvoiceProductID']);

                    foreach ($invoiceSubProductData as $key => $s) {
                        if ($s['productBatchID'] != NULL || $s['productSerialID'] != NULL) {
                            $invoiceSubProducts[$g['salesInvoiceProductID']][] = array(
                                'batchID' => $s['productBatchID'],
                                'serialID' => $s['productSerialID'],
                                'qtyByBase' => $s['salesInvoiceSubProductQuantity'],
                                'productPrice' => $g['salesInvoiceProductPrice'],
                                'locationProductID' => $g['locationProductID'],
                            );
                        }
                    }
                    $invoiceProductData['subProducts'] = $invoiceSubProducts;
                }

                $checkSubProductArray = array();
                $checkSubProductsInArray = array();
                foreach ($invoiceProductData['products'] as $key => $p) {
                    $batchProducts = $invoiceProductData['subProducts'][$p['salesInvoiceProductID']];
                    $loreturnQty = $returnQty = $p['invoiceQuantity'];
                    $locationProductID = $p['locationProductID'];
                    $salesInvoiceProductID = $p['salesInvoiceProductID'];

                    if ($p['documentTypeID'] != 4) {

                        $checkSalesOrderState = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByInvoiceID($p['invoiceID'])->current();

                        if(!is_null($checkSalesOrderState->salesOrderID)) {
                            $salesOrderProductDetails = $this->CommonTable('Invoice\Model\SalesOrderTable')->getSalesOrderProductBysalesOrderID($checkSalesOrderState->salesOrderID);

                            foreach ($salesOrderProductDetails as $key => $value) {
                                if ($p['locationProductID'] == $value['locationProductID']) {
                                    $newSalesOrderCopiedQuantity = floatval($value['salesOrdersProductCopiedQuantity']) - floatval($p['invoiceQuantity']); 
                                    if ($newSalesOrderCopiedQuantity >= 0) {
                                        $newSalesOrderCopiedQuantity = $newSalesOrderCopiedQuantity;
                                    } else {
                                        $newSalesOrderCopiedQuantity = 0;
                                    }

                                    if (floatval($value['quantity']) > $newSalesOrderCopiedQuantity) {
                                       $data = array(
                                            'copied' => 0,
                                            'salesOrdersProductCopiedQuantity' => $newSalesOrderCopiedQuantity,
                                       );
                                    }

                                    $updateSalesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->updateCopiedSalesOrderProducts($value['locationProductID'],$value['soID'], $data);
                                }
                            }
                            $this->CommonTable('Invoice\Model\SalesOrderTable')->updateSalesOrderStatus($checkSalesOrderState->salesOrderID,3);
                        }

                        $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);
                        $locationProductAverageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
                        $locationProductQuantity = $locationProduct->locationProductQuantity;
                        $locationProductLastItemInID = $locationProduct->locationProductLastItemInID;

                        $locationProductData = array(
                            'locationProductQuantity' => $locationProduct->locationProductQuantity + $loreturnQty,
                        );
                        if ($p['documentTypeID'] != 19) {
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateQuantityByID($locationProductData, $locationProductID);
                        }
                        //check wheter the product alredy in a array or not, for delete sub product
                        $checkSubProduct = in_array($salesInvoiceProductID, $checkSubProductArray);
                        if (!count($batchProducts) > 0 && !$checkSubProduct) {
                            $itemReturnQty = $returnQty;
                        //add item in details for non serial and batch products
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($locationProductID, 'Sales Invoice', $invoiceID);
                            foreach (array_reverse($itemOutDetails) as $itemOutData) {
                                if ($itemReturnQty != 0) {
                                    if ($itemOutData->itemOutQty > $itemOutData->itemOutReturnQty) {
                                        $itemInPreDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetails($itemOutData->itemOutItemInID);

                                        $unitPrice = $itemInPreDetails['itemInPrice'];
                                        $unitDiscount = $itemInPreDetails['itemInDiscount'];
                                        if($averageCostingFlag == 1){
                                            $unitPrice = $itemOutData->itemOutAverageCostingPrice;
                                            $unitDiscount = 0;
                                        }

                                        $leftQty = floatval($itemOutData->itemOutQty) - floatval($itemOutData->itemOutReturnQty);
                                        $itemInInsertQty = 0;
                                        $itemOutUpdateReturnQty = 0;
                                        if ($leftQty >= $itemReturnQty) {
                                            $itemInInsertQty = $itemReturnQty;
                                            $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) + floatval($itemReturnQty);
                                            $itemReturnQty = 0;
                                        } else {
                                            $itemInInsertQty = $leftQty;
                                            $itemOutUpdateReturnQty = $itemOutData->itemOutQty;
                                            $itemReturnQty = floatval($itemReturnQty) - floatval($leftQty);
                                        }
                                    //hit data to the item in table with left qty
                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInPreDetails['itemInIndexID']);
                                        $itemInData = array(
                                            'itemInIndex' => $newItemInIndex,
                                            'itemInDocumentType' => "{$invoiceTypeFlag} Cancel",
                                            'itemInDocumentID' => $invoiceID,
                                            'itemInLocationProductID' => $locationProductID,
                                            'itemInBatchID' => NULL,
                                            'itemInSerialID' => NULL,
                                            'itemInQty' => $itemInInsertQty,
                                            'itemInPrice' => $unitPrice,
                                            'itemInDiscount' => $unitDiscount,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                            );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                    //update item out return qty
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                                    }
                                } else {
                                    break;
                                }
                            }
                        }

                        if (count($batchProducts) > 0 && !$checkSubProduct) {
                            foreach ($batchProducts as $batchKey => $batchValue) {
                                if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                                    $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                                    $productBatchQuentity = $batchProduct->productBatchQuantity + $batchValue['qtyByBase'];
                                    $productBatchQty = array(
                                        'productBatchQuantity' => $productBatchQuentity,
                                    );
                                    $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                                    if ($batchValue['serialID']) {
                                    //Add details to item in table for batch and serial products
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchSerialProductDetails($locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'], $batchValue['batchID'], 'Sales Invoice', $invoiceID);

                                        $unitPrice = $itemInDetails['itemInPrice'];
                                        $unitDiscount = $itemInDetails['itemInDiscount'];
                                        if($averageCostingFlag == 1){
                                            $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                            $unitDiscount = 0;
                                        }

                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                        $itemInData = array(
                                            'itemInIndex' => $newItemInIndex,
                                            'itemInDocumentType' => "{$invoiceTypeFlag} Cancel",
                                            'itemInDocumentID' => $invoiceID,
                                            'itemInLocationProductID' => $locationProductID,
                                            'itemInBatchID' => $batchValue['batchID'],
                                            'itemInSerialID' => $batchValue['serialID'],
                                            'itemInQty' => 1,
                                            'itemInPrice' => $unitPrice,
                                            'itemInDiscount' => $unitDiscount,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                            );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, '1');
                                    } else {
                                    //Add details to item in table for batch products
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchProductDetails($locationProductID, $batchValue['batchID']);
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetails($batchValue['batchID'], 'Sales Invoice', $invoiceID);

                                        $unitPrice = $itemInDetails['itemInPrice'];
                                        $unitDiscount = $itemInDetails['itemInDiscount'];
                                        if($averageCostingFlag == 1){
                                            $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                            $unitDiscount = 0;
                                        }

                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                        $itemInData = array(
                                            'itemInIndex' => $newItemInIndex,
                                            'itemInDocumentType' => "{$invoiceTypeFlag} Cancel",
                                            'itemInDocumentID' => $invoiceID,
                                            'itemInLocationProductID' => $locationProductID,
                                            'itemInBatchID' => $batchValue['batchID'],
                                            'itemInSerialID' => NULL,
                                            'itemInQty' => $batchValue['qtyByBase'],
                                            'itemInPrice' => $unitPrice,
                                            'itemInDiscount' => $unitDiscount,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                            );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, $batchValue['qtyByBase']);
                                    }
                                } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                //Add details to item in table for serial products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getSerialProductDetails($locationProductID, $batchValue['serialID']);
                                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Sales Invoice', $invoiceID);

                                    $unitPrice = $itemInDetails['itemInPrice'];
                                    $unitDiscount = $itemInDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }

                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => "{$invoiceTypeFlag} Cancel",
                                        'itemInDocumentID' => $invoiceID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => NULL,
                                        'itemInSerialID' => $batchValue['serialID'],
                                        'itemInQty' => 1,
                                        'itemInPrice' => $unitPrice,
                                        'itemInDiscount' => $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                    $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, '1');
                                }
                                if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                    $serialProductData = array(
                                        'productSerialSold' => '0',
                                        );
                                    $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                                }
                            }
                        }

                        $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductID);
                        $locationPrTotalQty = 0;
                        $locationPrTotalPrice = 0;
                        $newItemInIds = array();
                        if(count($itemInData) > 0){
                            foreach ($itemInData as $key => $value) {
                                if($value->itemInID > $locationProductLastItemInID){
                                    $newItemInIds[] = $value->itemInID;
                                    $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                    if($remainQty > 0){
                                        $itemInPrice = $remainQty*$value->itemInPrice;
                                        $itemInDiscount = $remainQty*$value->itemInDiscount;
                                        $locationPrTotalQty += $remainQty;
                                        $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                                    }
                                    $locationProductLastItemInID = $value->itemInID;
                                }
                            }
                        }
                        $locationPrTotalQty += $locationProductQuantity;
                        $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                        if($locationPrTotalQty > 0){
                            $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                            $lpdata = array(
                                'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                                'locationProductLastItemInID' => $locationProductLastItemInID,
                                );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationProductID);

                            foreach ($newItemInIds as $inID) {
                                $intemInData = array(
                                    'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                    );
                                $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                            }
                        }
                    } else {

                       //check wheter the product alredy in a array or not, for delete sub product
                        $checkSubProduct = in_array($locationProductID, $checkSubProductsInArray);
                        $itemReturnQty = $returnQty;
                        $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($p['documentID'], $p['productID']);
                        if (isset($deliveryNoteProducts)) {
                            $newCopiedQuantity = $deliveryNoteProducts->deliveryNoteProductCopiedQuantity - (float) $itemReturnQty;
                            $copiedflag = 1;

                            if ($newCopiedQuantity < $deliveryNoteProducts->deliveryNoteProductQuantity) {
                                $copiedflag = 0;
                            }
                            $deliveryNoteProData = array(
                                'copied' => $copiedflag,
                                'deliveryNoteProductCopiedQuantity' => $newCopiedQuantity,
                                );

                            $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryNoteProducts($p['productID'], $p['documentID'], $deliveryNoteProData);
                            $this->_checkAndUpdateDeliveryNoteStatus($p['documentID']);

                            $data = array(
                                'itemOutPrice' => $deliveryNoteProducts->deliveryNoteProductPrice,
                                );
                            $this->CommonTable("Inventory\Model\ItemOutTable")->updateDeliveryNoteUnitPrice($data, $locationProductID, $p['documentID']);
                        }

                        if (count($batchProducts) > 0 && !$checkSubProduct) {
                            foreach ($batchProducts as $batchKey => $batchValue) {
                                if ($batchValue['serialID'] != '') {
                                    $deliveryNoteSubProducts = $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->getProductSerialByDeliveryNoteProductIDAndProductSerialID($deliveryNoteProducts->deliveryNoteProductID, $batchValue['serialID']);
                                    $deliveryNoteProData = array(
                                        'deliveryNoteSubProductsCopiedQuantity' => 0,
                                    );
                                } else {
                                    $deliveryNoteSubProducts = $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->getProductBatchByDeliveryNoteProductIDAndProductBatchID($deliveryNoteProducts->deliveryNoteProductID, $batchValue['batchID']);
                                    $newCopiedQuantity = $deliveryNoteSubProducts->deliveryNoteSubProductsCopiedQuantity - (float) $batchValue['qtyByBase'];

                                    $deliveryNoteProData = array(
                                        'deliveryNoteSubProductsCopiedQuantity' => $newCopiedQuantity,
                                    );
                                }
                                $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->updateDeliveryNoteSubProductData($deliveryNoteProData, $deliveryNoteSubProducts->deliveryNoteSubProductID);
                            }
                        }
                    }
                    if (!$checkSubProduct) {
                        $checkSubProductArray[] = $salesInvoiceProductID;
                    }
                }
                //updates entity table
                $entityData = array(
                    'deleted' => 1,
                    'deletedBy' => $this->user_session->userID,
                    'deletedTimeStamp' => $this->getUserDateTime()
                );
                $this->CommonTable("Core\Model\EntityTable")->updateEntity($entityData, $invoiceEntityID);
                //update salesInvoice table status as cancel(5)
                $this->CommonTable("Invoice\Model\InvoiceTable")->updateInvoiceStatusID($invoiceID, 5);

                //updates customer balance
                $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                //if invoice has a credit note then getting it is value
                $getCreditNoteTotal = $this->CommonTable("Invoice\Model\CreditNoteTable")->getCreditNoteTotalByInvoiceID($invoiceID);
                $creditNoteSum = isset($getCreditNoteTotal->current()['creditNoteTot']) ? $getCreditNoteTotal->current()['creditNoteTot'] : 0;
                $custNewCurrentBalance = ($customerData->customerCurrentBalance - $totalPrice) + $creditNoteSum;
                $custData = array(
                    'customerCurrentBalance' => $custNewCurrentBalance,
                    'customerID' => $customerID
                );
                $customerAllData = new Customer;
                $customerAllData->exchangeArray($custData);
                $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);

                //if invoice is created by quotation
                //then quotation update to open state
                if ($getInvoiceData->quotationID != NULL) {
                    $quotationID = $getInvoiceData->quotationID;
                    $openStatusID = $this->getStatusID('open');
                    $this->CommonTable('Invoice\Model\QuotationTable')->updateQuotationStatus($quotationID, $openStatusID);
                    $this->CommonTable('Invoice\Model\QuotationProductTable')->updateCopiedQuotationProductsByQuoID($quotationID);
                }

                if ($getInvoiceData->jobID != NULL) {

                    if ($returnDeliveryNote) {
                        $getrelatedDeliveryNotes = $this->CommonTable('DeliveryNoteTable')->getAnyDeliveryNoteDetailsByJobId($getInvoiceData->jobID);

                        if (sizeof($getrelatedDeliveryNotes) > 0) {
                            foreach ($getrelatedDeliveryNotes as $key => $value) {
                                // $dataSet = $this->processDeliveryNoteToReturn($value, $jobData);
                                $deliveryNoteProData = array(
                                    'copied' => 0,
                                    'deliveryNoteProductCopiedQuantity' => 0,
                                    );

                                $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryAsEmptyNoteProductsByDlnID($value['deliveryNoteID'], $deliveryNoteProData);


                                $deliveryNoteStatus = array(
                                    'deliveryNoteStatus'=> 3
                                );
                                $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNoteStatusToOpen($value['deliveryNoteID'], $deliveryNoteStatus);

                            }
                        }
                        $jobCancelData = [
                            'jobID' => $getInvoiceData->jobID,
                            'returnDeliveryNote' => 'true'
                        ];

                        $cancelJob = $this->getService('JobService')->cancelJob($jobCancelData, $this->userID);   

                        if (!$cancelJob['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_INVOICE_CANCEL');

                            return $this->JSONRespond();
                        }

                    } else {
                        $jobCancelData = [
                            'jobID' => $getInvoiceData->jobID,
                            'returnDeliveryNote' => 'true'
                        ];
                        $cancelJob = $this->getService('JobService')->cancelJob($jobCancelData, $this->userID);   

                        if (!$cancelJob['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_INVOICE_CANCEL');

                            return $this->JSONRespond();
                        }
                    }
                } 

                if (is_array($inventoryProductIDs)) {
                    $eventParameter = ['productIDs' => $inventoryProductIDs, 'locationIDs' => [$locationID]];
                    $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);
                }

                $accountProduct = array();
                if ($this->useAccounting == 1) {
                    $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('1', $invoiceID);
                    $journalEntryID = $journalEntryData['journalEntryID'];
                    $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);

                    $i=0;
                    $journalEntryAccounts = array();
                    foreach ($jEAccounts as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Invoice Delete '.$invoiceCode.'.';
                        $i++;
                    }

                    //get journal entry reference number.
                    $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                    $jelocationReferenceID = $jeresult['locRefID'];
                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                    $journalEntryData = array(
                        'journalEntryAccounts' => $journalEntryAccounts,
                        'journalEntryDate' => $this->convertDateToStandardFormat($this->getUserDateTime()),
                        'journalEntryCode' => $JournalEntryCode,
                        'journalEntryTypeID' => '',
                        'journalEntryIsReverse' => 0,
                        'journalEntryComment' => 'Journal Entry is posted when delete Invoice '.$invoiceCode.'.',
                        'documentTypeID' => 1,
                        'journalEntryDocumentID' => $invoiceID,
                        'ignoreBudgetLimit' => $ignoreBudgetLimit,
                        );
                    
                    $resultData = $this->saveJournalEntry($journalEntryData, '1', $invoiceID);

                    if(!$resultData['status']){

                        $this->rollback();
                        $this->status = false;
                        $this->msg = $resultData['msg'];
                        $this->data = $resultData['data'];

                        return $this->JSONRespond();
                    }

                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($invoiceID,1, 5);
                    if(!$jEDocStatusUpdate['status']){
                        return array(
                            'status'=> false,
                            'msg'=>$jEDocStatusUpdate['msg'],
                            );
                    }      

                    $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(1,$invoiceID);
                    $dimensionData = [];
                    foreach ($jEDimensionData as $value) {
                        if (!is_null($value['journalEntryID'])) {
                            $temp = [];
                            $temp['dimensionTypeId'] = $value['dimensionType'];
                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                            $dimensionData[$invoiceCode][] = $temp;
                        }
                    }
                    if (!empty($dimensionData)) {
                        $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$invoiceCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                        if(!$saveRes['status']){
                            $this->rollback();
                            $this->status = false;
                            $this->data = $saveRes['data'];
                            $this->msg = $saveRes['msg'];
                            return $this->JSONRespond();
                        }   
                    }
                }
                $this->setLogMessage('Invoice '.$invoiceCode.' is deleted.');
                $this->commit();
                $this->status = TRUE;
                $this->msg = $this->getMessage('SUCC_INVOICE_CANCEL');
            } else {
                $this->status = FALSE;
                $this->msg = $this->getMessage('ERR_INVOICE_HAS_PAYMNT');
            }
            return $this->JSONRespond();
        }
    }

    public function searchAllSalesInvoicesForDropdownAction()
    {
        $searchrequest = $this->getRequest();
        if ($searchrequest->isPost()) {
            $searchKey = $searchrequest->getPost('searchKey');

            $locationIds = $this->getActiveAllLocationsIds();
            $invoices = $this->CommonTable("Invoice\Model\InvoiceTable")->getAllSalesInvoicesByLocationIds($locationIds, $searchKey);
            $invoiceList = array();
            foreach ($invoices as $invoice) {
                $temp['value'] = $invoice['salesInvoiceID'];
                $temp['text'] = $invoice['salesInvoiceCode'];
                $invoiceList[] = $temp;
            }

            $this->data = array('list' => $invoiceList);
            return $this->JSONRespond();
        }
    }

    public function retriveInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $in = array();
            $invoiceID = $request->getPost('invoiceID');
            $locationID = $this->user_session->userActiveLocation["locationID"];

            $invoiceCheck = $this->CommonTable('Invoice\Model\InvoiceTable')->checkInvoiceByID($invoiceID, $locationID)->current();
            $value = (object) $invoiceCheck;
            $pvalue = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getProductByInvoiceID($value->salesInvoiceID, $locationID);

            if ($invoiceCheck != NULL) {
                $ccbalance = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($value->customerID);
                $curBal = 0;
                if ($ccbalance != NULL) {
                    $curBal = $ccbalance->customerCurrentBalance;
                    $curCre = $ccbalance->customerCurrentCredit;
                } else {
                    $curBal = 0;
                    $curCre = 0;
                }

                $data = array();
                $pdata = array();
                $data['invoiceCode'] = $value->salesInvoiceCode;
                $data['customerName'] = $value->customerName . '-' . $value->customerCode;
                $data['customerId'] = $value->customerID;
                $data['salesPersonID'] = $value->salesPersonID;
                $data['issueDate'] = $value->salesInvoiceIssuedDate;
                $data['expireDate'] = $value->salesInvoiceOverDueDate;
                $data['paymentTerm'] = $value->paymentTermID;
                $data['tax'] = $value->salesInvoiceTaxAmount;
                $data['totalDiscount'] = $value->salesInvoiceTotalDiscount;
                $data['total'] = $value->salesinvoiceTotalAmount;
                $data['comment'] = $value->quotationComment;
                $data['branch'] = $value->locationID;
                $data['showTax'] = $value->quotationShowTax;
                $data['salesPersonID'] = $value->salesPersonID;
                $data['currentBalance'] = $curBal;
                $data['currentCredit'] = $curCre;
                while ($t = $pvalue->current()) {

                    $pdata[] = $t;
                    $ptax = $this->CommonTable('Invoice\Model\InvoiceProductTaxTable')->getTaxByInvoiceProductID($t['salesInvoiceProductID']);
                    $taxes = '';
                    while ($taxs = $ptax->current()) {
                        $tax = (object) $taxs;
                        $taxes[$tax->taxID] = $tax;
                    }
                    if ($taxes) {
                        $productTax[$t['salesInvoiceProductID']] = $taxes;
                    }
                    $locationProducts[$t['productID']] = $this->getLocationProductDetails($locationID, $t['productID']);
                }
                $customer = $this->getCustomerDetailsByCustomerID($value->customerID);
                $data['product'] = $pdata;
                $data['productTaxes'] = $productTax;
                $data['locationProducts'] = $locationProducts;
                $data['customer'] = $customer;
                return $data;
            } else {
                $in[0] = 'false';
                return new JsonModel($in);
            }
        }

        $in[0] = 'wrongquono';
        return new JsonModel($in);
    }

    public function updateInvoiceAction()
    {  
        $request = $this->getRequest();
        if ($request->isPost()) {
            //currency rate check
            $customCurrencyId = $request->getPost('customCurrencyId');
            $ccRate = $request->getPost('customCurrencyRate');
            $oldCCRate = $request->getPost('oldCustomCurrencyRate');
            $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
            $customCurrencyRate = ($customCurrencyId != "") ? $customCurrencyData->currencyRate : 1;
            $customCurrencyRate = ($ccRate == 0 || $ccRate == 1)? $customCurrencyRate : $ccRate;

            //typical data set
            $invoiceID = $request->getPost('invoiceID');
            $getInvoiceData = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoiceByID($invoiceID);
            $invoiceEntityID = $getInvoiceData->entityID;
            $locationID = $request->getPost('locationOutID');
            $param = $request->getPost();
            $invoiceDate = $request->getPost('invoiceDate');
            $dueDate = $request->getPost('dueDate');
            $invoiceComment = $request->getPost('invoiceComment');
            $paymentTermID = $request->getPost('paymentTermID');
            $salesPersonIDArr = $request->getPost('salesPersonID');
            $deliveryAddress = $request->getPost('deliveryAddress');
            $customerID = $request->getPost('customer');
            $productsSet = $request->getPost('products');
            $insertedInvoiceTopVals = $request->getPost('insertedInvoiceTopVals');
            $customCurrencyId = $request->getPost('customCurrencyId');
            $invoiceDiscountRate = ($request->getPost('invoiceDiscountRate') == NULL) ? 0 : number_format($request->getPost('invoiceDiscountRate'), 2);
            $invoiceDiscountType = $request->getPost('invoiceDiscountType');
            $deliveryCharge = $request->getPost('deliveryCharge') * $customCurrencyRate;
            $promotionID = $request->getPost('promotionID');
            $dimensionData = $request->getPost('dimensionData');
            $invIDForPaymentUpdate = $invoiceID;
            $serviceChargeAmount = $param['serviceChargeAmount'];
            $addedServiceChargeArray = $param['addedServiceChargeArray'];
            $linkedCusOrder = (!empty($param['linkedCusOrder'])) ? $param['linkedCusOrder'] : null;

            //get creditNotes related to current invoice ID
            $getCreditNote = $this->CommonTable("Invoice\Model\CreditNoteTable")->getCreditNotesByInvoiceID($invoiceID);
            if ($getCreditNote->current() != NULL) {
                $this->status = false;
                $this->msg =  'This invoice has a credit note.So you can not edit this invoice..!';
                return $this->JSONRespond();
            }

            $invOldResult = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoiceByInvoiceID($invoiceID)->current();
            $getRelatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getSalesPersonByInvoiceID($invoiceID);

            sort($salesPersonIDArr); 
            sort($getRelatedSalesPersons);


            if (!empty($salesPersonIDArr) && $salesPersonIDArr != $getRelatedSalesPersons) {

                foreach ($salesPersonIDArr as $key5 => $value5) {
                    $salesPersonData = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByID($value5)->current();
                    if ($salesPersonData['salesPersonStatus'] != 1) {
                        $this->status = false;
                        $this->msg =  $this->getMessage('ERR_SELECTED_SALES_PERSON');
                        return $this->JSONRespond();   
                    }
                }
            }

            if (sizeof($getRelatedSalesPersons) > 0) {
                $deleteSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->deleteSalesPersons($invoiceID);
            }

            if($invOldResult->statusID == 10 || $invOldResult->statusID == 4){
                $this->status = false;
                $this->msg =  $this->getMessage('ERR_INVOICE_STATUS_CLOSED',array($invOldResult->salesInvoiceCode));
                return $this->JSONRespond();
            }
            if($invOldResult->statusID == 5){
                $this->status = false;
                $this->msg =  $this->getMessage('ERR_INVOICE_STATUS_CANCELED',array($invOldResult->salesInvoiceCode));
                return $this->JSONRespond();
            }
            // if (floatval($getInvoiceData->salesInvoicePayedAmount) > 0 && (floatval($getInvoiceData->salesinvoiceTotalAmount) != floatval($request->getPost('invoiceTotalPrice')))) {
            //     $this->status = false;
            //     $this->msg =  $this->getMessage('ERR_PAID_INVOICE_AMOUNT_CHANGED');
            //     return $this->JSONRespond();
            // }

            //payed invoice amount set to the return datalis.
            $param['salesInvoicePayedAmount'] = floatval($invOldResult->salesInvoicePayedAmount)/floatval($param['oldCustomCurrencyRate']);

            $invoiceData = array(
                'customer' => $customerID,
                'salesInvoiceIssuedDate' => $invoiceDate,
                'salesInvoiceOverDueDate' => $dueDate,
                'salesInvoiceComment' => $invoiceComment,
                'paymentTerm' => $paymentTermID,
                'salesPerson' => null,
                'salesInvoiceDeliveryAddress' => $deliveryAddress,
                'custCurrency' => $customCurrencyId,
                'salesInvoiceTotalDiscountType' => $invoiceDiscountType,
                'salesInvoiceDiscountRate' => (!empty($invoiceDiscountRate)) ? $invoiceDiscountRate : null,
                'deliveryCharge' => $deliveryCharge,
                'promotion' => $promotionID
            );
            $currentTaxArray = [];
            foreach ($productsSet as $key => $value) {
                unset($value['stockUpdate']);
                unset($value['selected_serials']);
                $products[$key] = $value;
                $currentTaxArray[$value['locationProductID']][] = $value['pTax']['tL'];
            }
            
            $subProducts = $request->getPost('subProducts');
            $insertedProducts = $request->getPost('insertedProducts');
            $insertedSubProducts = $request->getPost('insertedSubProducts');
            $invoiceTotalPrice = $request->getPost('invoiceTotalPrice');
            $previousInvoiceTotal = $request->getPost('previousInvoiceTotal');
            
            //will mark as true if products has been changed
            $itemListChanged = FALSE;

            //then compare products list with old product list
            if (!empty($products) && !empty($insertedProducts)) {
                if (count($products) == count($insertedProducts)) {
                    foreach ($insertedProducts as $iID => $i) {
                        if (array_key_exists($iID, $products)) {
                            foreach ($products as $pID => $p) {
                                $splitProductID = explode(".", $p['productID']);
                                $productID = isset($splitProductID[0]) ? $splitProductID[0] : $p['productID'];

                                if ($iID == $pID) {
                                    $products[$pID]['productID'] = $productID;
                                    $diffProduct = array_diff_assoc($products[$pID], $insertedProducts[$iID]);

                                    //if there have any different with products
                                    if (!empty($diffProduct)) {
                                        $itemListChanged = TRUE;
                                        break;
                                    }

                                    //compaire subproducts list with current and already inserted
                                    if (!empty($subProducts) && !empty($insertedSubProducts)) {
                                        if (count($subProducts) == count($insertedSubProducts)) {
                                            foreach ($insertedSubProducts as $iSID => $iS) {
                                                foreach ($subProducts as $sID => $s) {
                                                    if (array_key_exists($iSID, $subProducts)) {
                                                        foreach ($insertedSubProducts[$iSID] as $iKey => $iv) {
                                                            foreach ($subProducts[$iSID] as $sKey => $sv) {
                                                                if ($iKey == $sKey) {
                                                                    $diffSubProduct = array_diff_assoc($iv, $sv);
                                                                    //if there is any different
                                                                    if (!empty($diffSubProduct)) {
                                                                        $itemListChanged = TRUE;
                                                                        break;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        $itemListChanged = TRUE;
                                                    }
                                                }
                                            }
                                        } else {
                                            $itemListChanged = TRUE;
                                        }
                                    }
                                }
                            }
                        } else {
                            $itemListChanged = TRUE;
                        }
                    }
                } else {
                    $itemListChanged = TRUE;
                }
            }

            //check existing taxes and newly inserted taxes
            $existingTaxDetails = $this->CommonTable("Invoice\Model\InvoiceProductTable")->getInvoiceItemTaxDetailsByInviceID($invoiceID);
            $existingTaxDataSet = [];
            foreach ($existingTaxDetails as $key => $value) {
                $existingTaxDataSet[$value['locationProductID']][] = $value['taxID'];
            }
            foreach ($currentTaxArray as $locationProID => $currentItemWiseTaxValue) {
                foreach ($currentItemWiseTaxValue as $key => $taxDetais) {
                    foreach ($taxDetais as $taxID => $taxData) {
                        $currentKey = array_search ($taxID, $existingTaxDataSet[$locationProID]);
                        if ($currentKey != '') {
                            $existingTaxDataSet[$locationProID][$currentKey] = null;
                            
                        }

                    }
                }
                foreach ($existingTaxDataSet[$locationProID] as $key => $value) {
                    if ($value != null) {
                        $itemListChanged = TRUE;        
                    }
                }
                
            }
            
            if($oldCCRate != $customCurrencyRate){
                $itemListChanged = TRUE;
            }

            if ($invOldResult->salesInvoiceDeliveryCharge != $deliveryCharge) {
                $itemListChanged = TRUE;
            }

            if ($invOldResult->salesInvoiceServiceChargeAmount != $serviceChargeAmount) {
                $itemListChanged = TRUE;
            }

            
            $this->beginTransaction();
            $getInvoiceChanges = $this->_getInvoiceChanges($products, $insertedProducts, $subProducts, $insertedSubProducts, $invoiceData, $insertedInvoiceTopVals);
            //if invoice have changes
            if (!empty($getInvoiceChanges)) {

                //save data into invoiceEditLog table
                $invoiceEditLogData = [
                    'userID' => $this->userID,
                    'salesInvoiceID' => $invoiceID,
                    'dateAndTime' => $this->getGMTDateTime()
                ];
                $invoiceEditLogModel = new InvoiceEditLog();
                $invoiceEditLogModel->exchangeArray($invoiceEditLogData);
                $invoiceEditLastInsertID = $this->CommonTable("Invoice\Model\InvoiceEditLogTable")->saveInvoiceEditLog($invoiceEditLogModel);

                //save invoice title changes
                if ($getInvoiceChanges['titleChanges']) {
                    foreach ($getInvoiceChanges['titleChanges'] as $key => $value) {
                        $paymentTerms = [
                            'Cash Only',
                            'Within 7 Days',
                            'Within 14 Days',
                            'Within 30 Days',
                            'Within 60 Days',
                            'Within 90 Days'
                        ];

                        $from = ($value['from'] == NULL) ? '-' : $value['from'];
                        $to = ($value['to'] == NULL) ? '-' : $value['to'];
                        if ($key == "paymentTerm") {
                            $from = $paymentTerms[$from];
                            $to = $paymentTerms[$to];
                        }

                        if ($key == "promotion") {
                            $getFromPromotion = $this->getPromotion($from);
                            $from = ($getFromPromotion['promotionName'] != NULL) ? $getFromPromotion['promotionName'] : '-';
                            $getToPromotion = $this->getPromotion($to);
                            $to = ($getToPromotion['promotionName'] != NULL) ? $getToPromotion['promotionName'] : '-';
                        }
                        if ($key == "salesPerson") {
                            $getFromSalesPerosn = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByID($from);
                            $fromSalesPerson = $getFromSalesPerosn->current()['salesPersonSortName'];
                            $from = ($fromSalesPerson != NULL) ? $fromSalesPerson : '-';

                            $getToSalesPerosn = $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByID($to);
                            $toSalesPerson = $getToSalesPerosn->current()['salesPersonSortName'];
                            $to = ($toSalesPerson != NULL) ? $toSalesPerson : '-';
                        }

                        $newState = $key . " Changes from " . $from . " to " . $to;
                        $newState = $newState = $newItemsData = [
                            'invoiceEditLogDetailsOldState' => NULL,
                            'invoiceEditLogDetailsNewState' => $newState,
                            'invoiceEditLogDetailsCategory' => $key,
                            'invoiceEditLogID' => $invoiceEditLastInsertID
                        ];
                        $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                        $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                        $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                    }
                }

                //save the invoice edit details
                foreach ($getInvoiceChanges as $productID => $data) {
                    foreach ($data as $key => $value) {
                        $getProduct = $this->CommonTable("Inventory\Model\ProductTable")->getProduct($productID);
                        $productName = $getProduct->productName;
                        $productCode = $getProduct->productCode;

                        $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($productID);
                        $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUom);
                        $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];
                        $uomAbbr = $thisqty['uomAbbr'];

                        if ($key == 'newItems') {
                            $newState = "Added Item Called " . $value['productName'] . "-" . $value['productCode'] . " and quantity " . number_format($value['qty'], 2);
                            $newItemsData = [
                                'invoiceEditLogDetailsOldState' => NULL,
                                'invoiceEditLogDetailsNewState' => $newState,
                                'invoiceEditLogDetailsCategory' => $key,
                                'invoiceEditLogID' => $invoiceEditLastInsertID
                            ];

                            $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                            $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                            $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                        }

                        if ($key == "removeItems") {
                            $newState = "Removed Item Called " . $value['productName'] . "-" . $value['productCode'] . " and quantity " . number_format($value['qty'], 2);
                            $newItemsData = [
                                'invoiceEditLogDetailsOldState' => NULL,
                                'invoiceEditLogDetailsNewState' => $newState,
                                'invoiceEditLogDetailsCategory' => $key,
                                'invoiceEditLogID' => $invoiceEditLastInsertID
                            ];

                            $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                            $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                            $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                        }

                        if ($key == "itemChanges") {
                            foreach ($value as $k => $data) {
                                $from = ( $data['from']['change'] == NULL) ? '-' : $data['from']['change'];
                                $to = ($data['to']['change'] == NULL) ? '-' : $data['to']['change'];
                                $newState = $data['productName'] . "-" . $data['productCode'] . ' ' . $data['from']['source'] . " changed from " . $from . " to " . $to;

                                $newItemsData = [
                                    'invoiceEditLogDetailsOldState' => NULL,
                                    'invoiceEditLogDetailsNewState' => $newState,
                                    'invoiceEditLogDetailsCategory' => $key,
                                    'invoiceEditLogID' => $invoiceEditLastInsertID
                                ];

                                $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                                $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                                $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                            }
                        }

                        if ($key == "removedTax") {
                            if (!empty($value)) {
                                foreach ($value as $k => $v) {
                                    $newState = $productName . " - " . $productCode . " tax removed - tax name - " . $v['tN'] . " tax percentage - " . $v['tP'] . "% tax Amount - " . $v['tA'];
                                    $newItemsData = [
                                        'invoiceEditLogDetailsOldState' => NULL,
                                        'invoiceEditLogDetailsNewState' => $newState,
                                        'invoiceEditLogDetailsCategory' => $key,
                                        'invoiceEditLogID' => $invoiceEditLastInsertID
                                    ];
                                    $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                                    $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                                    $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                                }
                            }
                        }

                        if ($key == "addedTax") {
                            if (!empty($value)) {
                                foreach ($value as $k => $v) {
                                    $newState = $productName . " - " . $productCode . " added tax - tax name - " . $v['tN'] . " tax percentage - " . $v['tP'] . "% tax Amount - " . $v['tA'];
                                    $newItemsData = [
                                        'invoiceEditLogDetailsOldState' => NULL,
                                        'invoiceEditLogDetailsNewState' => $newState,
                                        'invoiceEditLogDetailsCategory' => $key,
                                        'invoiceEditLogID' => $invoiceEditLastInsertID
                                    ];
                                    $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                                    $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                                    $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                                }
                            }
                        }

                        if ($key == "subProducts") {
                            foreach ($value as $s => $subData) {
                                if ($s == 'added') {
                                    foreach ($subData as $v) {
                                        $qty = 0;
                                        $serialCode = NULL;
                                        $batchCode = NULL;
                                        if ($v['serialID'] != NULL && $v['batchID'] == NULL) {
                                            $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($v['serialID']);
                                            $serialCode = $getBatchSerialData->productSerialCode;
                                            $qty = 1;
                                        } else if ($v['serialID'] == NULL && $v['batchID'] != NULL) {
                                            $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($v['batchID']);
                                            $batchCode = $getBatchSerialData->productBatchCode;
                                            $qty = $v['qtyByBase'];
                                        } else if ($v['serialID'] != NULL && $v['batchID'] != NULL) {
                                            $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getBatchSerialProduct($v['serialID'], $v['batchID']);
                                            $batchCode = $getBatchSerialData->productBatchCode;
                                            $serialCode = $getBatchSerialData->productSerialCode;
                                            $qty = 1;
                                        }
                                        $serialCode = ($serialCode == NULL) ? '-' : $serialCode;
                                        $batchCode = ($batchCode == NULL) ? '-' : $batchCode;
                                        $newState = "Added new subproduct - Serial Code - " . $serialCode . " Batch Code - " . $batchCode . " Quantity - " . $qty . $uomAbbr;
                                        $newItemsData = [
                                            'invoiceEditLogDetailsOldState' => NULL,
                                            'invoiceEditLogDetailsNewState' => $newState,
                                            'invoiceEditLogDetailsCategory' => $key,
                                            'invoiceEditLogID' => $invoiceEditLastInsertID
                                        ];
                                        $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                                        $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                                        $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                                    }
                                }
                                if ($s == 'removed') {
                                    foreach ($subData as $v) {
                                        $qty = 0;
                                        $serialCode = NULL;
                                        $batchCode = NULL;

                                        if (!isset($v['serialID'])) {
                                            $v['serialID'] = "";
                                        }
                                        if (!isset($v['batchID'])) {
                                            $v['batchID'] = "";
                                        }

                                        if (isset($v['serialID']) && $v['serialID'] != "" && ($v['batchID'] == "" | $v['batchID'] == NULL)) {
                                            $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($v['serialID']);
                                            $serialCode = $getBatchSerialData->productSerialCode;
                                            $qty = 1;
                                        } else if (isset($v['batchID']) && $v['batchID'] != "" && ($v['serialID'] == "" | $v['serialID'] == NULL)) {
                                            $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($v['batchID']);
                                            $batchCode = $getBatchSerialData->productBatchCode;
                                            $qty = $v['qtyByBase'];
                                        } else if (isset($v['serialID']) && isset($v['batchID']) && $v['serialID'] != "" && $v['batchID'] != "") {
                                            $getBatchSerialData = $this->CommonTable('Inventory\Model\ProductSerialTable')->getBatchSerialProduct($v['serialID'], $v['batchID']);
                                            $serialCode = $getBatchSerialData->productSerialCode;
                                            $batchID = $getBatchSerialData->productBatchID;
                                            $getBatchData = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchID);
                                            $batchCode = $getBatchData->productBatchCode;
                                            $qty = 1;
                                        }
                                        $serialCode = ($serialCode == NULL) ? '-' : $serialCode;
                                        $batchCode = ($batchCode == NULL) ? '-' : $batchCode;

                                        $newState = "Removed new subproduct - Serial Code - " . $serialCode . " Batch Code - " . $batchCode . " Quantity - " . $qty . $uomAbbr;
                                        $newItemsData = [
                                            'invoiceEditLogDetailsOldState' => NULL,
                                            'invoiceEditLogDetailsNewState' => $newState,
                                            'invoiceEditLogDetailsCategory' => $key,
                                            'invoiceEditLogID' => $invoiceEditLastInsertID
                                        ];
                                        $invoiceEditLogDetailsModel = new InvoiceEditLogDetials();
                                        $invoiceEditLogDetailsModel->exchangeArray($newItemsData);
                                        $this->CommonTable("Invoice\Model\InvoiceEditLogDetailsTable")->saveInvoiceEditLogDetails($invoiceEditLogDetailsModel);
                                    }
                                }
                            }
                        }
                    }
                }
            }

            $customerChangeFlag = false;
            if ($getInvoiceData->customerID != $request->getPost('customer')) {
                $customerChangeFlag = true;
                $oldCustomerID = $getInvoiceData->customerID;
                $itemListChanged = true;
            }
            
            //if edit invoice has item changes
            if ($itemListChanged) {

                $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
                $displayData = $result->current();
                $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
                $averageCostingFlag = $displayData['averageCostingFlag'];

                if($FIFOCostingFlag == 0 && $averageCostingFlag == 0){
                    $this->rollback();
                    $this->status = false;
                    $this->msg = $this->getMessage('ERR_COSTING_METHOD_NOT_CHOOSE');
                    return $this->JSONRespond();
                }

                //updates entity table
                $entityData = array(
                    'deleted' => 1,
                    'deletedBy' => $this->user_session->userID,
                    'deletedTimeStamp' => $this->getUserDateTime()
                );
                $this->CommonTable("Core\Model\EntityTable")->updateEntity($entityData, $invoiceEntityID);

                //update salesInvoice table status as replace(10)
                $this->CommonTable("Invoice\Model\InvoiceTable")->updateInvoiceStatusID($invoiceID, 10);

                $relatedServiceCharges = $this->CommonTable('Invoice\Model\InvoiceServiceChargeTable')->getInvoiceChargesByInvoiceID($invoiceID);
                if (sizeof($relatedServiceCharges) < 0) {

                    foreach ($relatedServiceCharges as $key8 => $value8) {
                        $deleteServiceCharge = $this->CommonTable('Invoice\Model\InvoiceServiceChargeTable')->deleteServiceChargeByInvoiceID($invoiceID);
                    }
                }


                //get products details by related invoice ID
                $getInvoiceProductData = $this->CommonTable("Invoice\Model\InvoiceProductTable")->getProductsForInvoiceEditByInvoiceID($invoiceID, $locationID);
                $invoiceProductData = [];
                ////
                $incId = 1;
                foreach ($getInvoiceProductData as $key => $g) {
                    $locationProductID = $g['locationProductID'];
                    $proIncID = $g['productID']._.$incId;
                    $copyFromDeliveryNote = false;
                    $copyFromJob = false;
                    $deliveryNoteID = '';
                    $jobDocID = '';
                    if($g['documentTypeID'] == 4 && isset($g['salesInvoiceProductDocumentID'])){
                        $copyFromDeliveryNote = true;
                        $deliveryNoteID = $g['salesInvoiceProductDocumentID'];
                    } else if ($g['documentTypeID'] == 19 && isset($g['salesInvoiceProductDocumentID'])) {
                        $copyFromJob = true;
                        $jobDocID = $g['salesInvoiceProductDocumentID'];
                    }

                    $invoiceProductData['products'][$proIncID] = array(
                        'productID' => $g['productID'],
                        'invoiceID' => $g['salesInvoiceID'],
                        'invoiceQuantity' => $g['salesInvoiceProductQuantity'],
                        'salesInvoiceProductID' => $g['salesInvoiceProductID'],
                        'locationProductID' => $locationProductID,
                        'copyFromDeliveryNote' => $copyFromDeliveryNote,
                        'deliveryNoteID' => $deliveryNoteID,
                        'jobDocID' => $jobDocID,
                        'copyFromJob' => $copyFromJob,
                    );
                    if ($g['productTypeID'] === '1') {
                        $inventoryProductIDs[] = $g['productID'];
                    }

                    $getInvoiceSubProductData = $this->CommonTable("Invoice\Model\InvoiceSubProductTable")->getSalesInvoiceSubProductBySalesInvoiceID($g['salesInvoiceProductID']);
                    //all subproducts maps to every product--check???
                    $invoiceSubProducts = [];
                    foreach ($getInvoiceSubProductData as $key => $s) {
                        if ($g['documentTypeID'] == 4 ) {
                            $deliveryNoteID = $g['salesInvoiceProductDocumentID'];
                            if ($s['productSerialID'] != null) {
                                $itemOutSub = $this->CommonTable("Invoice\Model\DeliveryNoteProductTable")->getProductsDetailsByDeliveryNoteIDLocationProductIDAndProductSerialID($deliveryNoteID, $locationProductID, $s['productSerialID']);
                            } else if ($s['productBatchID'] != null){
                                $itemOutSub = $this->CommonTable("Invoice\Model\DeliveryNoteProductTable")->getProductsDetailsByDeliveryNoteIDLocationProductIDAndProductBatchID($deliveryNoteID, $locationProductID, $s['productBatchID']);
                            }
                        } else {
                            if ($s['productSerialID'] != null) {
                                $itemOutSub = $this->CommonTable("Invoice\Model\InvoiceProductTable")->getProductsDetailsByInvoiceIDLocationProductIDAndSerialID($invoiceID, $locationProductID, $s['productSerialID']);
                            } else if ($s['productBatchID'] != null){
                                $itemOutSub = $this->CommonTable("Invoice\Model\InvoiceProductTable")->getAllProductsDetailsByInvoiceIDLocationProductIDAndBatchID($invoiceID, $locationProductID, $s['productBatchID']);
                            }
                        }
                        if ($itemOutSub['itemOutBatchID'] != NULL || $itemOutSub['itemOutSerialID'] != NULL) {

                            $invoiceSubProducts[$s['salesInvoiceSubProductID']] = array(
                                'batchID' => $itemOutSub['itemOutBatchID'],
                                'serialID' => $itemOutSub['itemOutSerialID'],
                                'qtyByBase' => $s['salesInvoiceSubProductQuantity'],
                                'productPrice' => $g['salesInvoiceProductPrice'],
                                'locationProductID' => $itemOutSub['itemOutLocationProductID'],
                            );
                        }
                    }
                    
                    if ( count($invoiceSubProducts) > 0 ){
                        $invoiceProductData['subProducts'][$proIncID] = $invoiceSubProducts;
                    }
                    $incId++;
                }

                //there have products changes so we need to updates products
                foreach ($invoiceProductData['products'] as $key => $p) {
                    $batchProducts = isset($invoiceProductData['subProducts'][$key]) ? $invoiceProductData['subProducts'][$key] : [];
                    $loreturnQty = $returnQty = $p['invoiceQuantity'];
                    $locationProductID = $p['locationProductID'];

                    //get location Product Details For Update the average costing
                    $locationProductData = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);
                    $locationProductAverageCostingPrice = $locationProductData->locationProductAverageCostingPrice;
                    $locationProductQuantity = $locationProductData->locationProductQuantity;
                    $locationProductLastItemInID = $locationProductData->locationProductLastItemInID;

                    if(!$p['copyFromDeliveryNote'] && !$p['copyFromJob']){
                        //update location product qty by invoiced qty
                        $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProductByLocationProductID($locationProductID);

                        $locationProductData = array(
                            'locationProductQuantity' => $locationProduct->locationProductQuantity + $loreturnQty,
                        );
                        $this->CommonTable('Inventory\Model\LocationProductTable')->updateQuantityByID($locationProductData, $locationProductID);
                    }

                    //if item is non batch or non serial
                    if (!count($batchProducts) > 0) {

                        if ($p['copyFromDeliveryNote']) {
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($locationProduct->locationProductID, 'Delivery Note', $p['deliveryNoteID']);
                            $deliveryNoteProduct = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($p['deliveryNoteID'], $p['productID']);
                            $productQty = $deliveryNoteProduct->deliveryNoteProductCopiedQuantity - $returnQty;
                            $deliveryNoteData = array(
                                'copied'=>0,
                                'deliveryNoteProductCopiedQuantity'=>$productQty
                            );
                            $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryNoteProducts($p['productID'], $p['deliveryNoteID'], $deliveryNoteData);
                            
                            $deliveryNoteStatus = array(
                                'deliveryNoteStatus'=> 3
                            );
                            $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNoteStatusToOpen($p['deliveryNoteID'], $deliveryNoteStatus);


                            foreach (array_reverse($itemOutDetails) as $itemOutData) {
                                $deliveryNoteProductPrice = $deliveryNoteProduct->deliveryNoteProductPrice;
                                $data = array(
                                    'itemOutPrice' => $deliveryNoteProductPrice,
                                );
                                $this->CommonTable("Inventory\Model\ItemOutTable")->updateDeliveryNoteUnitPrice($data, $locationProductID, $p['deliveryNoteID']);
                            }
                        } else if ($p['copyFromJob']) {
                            
                        } else {
                            $itemReturnQty = $returnQty;
                            //add item in details for non serial and batch products
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($locationProduct->locationProductID, 'Sales Invoice', $invoiceID);

                            foreach (array_reverse($itemOutDetails) as $itemOutData) {
                                if ($itemReturnQty != 0) {
                                    if ($itemOutData->itemOutQty > $itemOutData->itemOutReturnQty) {
                                        $itemInPreDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInDetails($itemOutData->itemOutItemInID);

                                        $unitPrice = $itemInPreDetails['itemInPrice'];
                                        $unitDiscount = $itemInPreDetails['itemInDiscount'];
                                        if($averageCostingFlag == 1){
                                            $unitPrice = $itemOutData->itemOutAverageCostingPrice;
                                            $unitDiscount = 0;
                                        }

                                        $leftQty = floatval($itemOutData->itemOutQty) - floatval($itemOutData->itemOutReturnQty);
                                        $itemInInsertQty = 0;
                                        $itemOutUpdateReturnQty = 0;
                                        if ($leftQty >= $itemReturnQty) {
                                            $itemInInsertQty = $itemReturnQty;
                                            $itemOutUpdateReturnQty = floatval($itemOutData->itemOutReturnQty) + floatval($itemReturnQty);
                                            $itemReturnQty = 0;
                                        } else {
                                            $itemInInsertQty = $leftQty;
                                            $itemOutUpdateReturnQty = $itemOutData->itemOutQty;
                                            $itemReturnQty = floatval($itemReturnQty) - floatval($leftQty);
                                        }
                                    //hit data to the item in table with left qty
                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInPreDetails['itemInIndexID']);
                                        $itemInData = array(
                                            'itemInIndex' => $newItemInIndex,
                                            'itemInDocumentType' => 'Invoice Edit',
                                            'itemInDocumentID' => $invoiceID,
                                            'itemInLocationProductID' => $locationProduct->locationProductID,
                                            'itemInBatchID' => NULL,
                                            'itemInSerialID' => NULL,
                                            'itemInQty' => $itemInInsertQty,
                                            'itemInPrice' => $unitPrice,
                                            'itemInDiscount' => $unitDiscount,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                            );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                    //update item out return qty
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutData->itemOutID, $itemOutUpdateReturnQty);
                                    }
                                } else {
                                    break;
                                }
                            }
                        }
                    }

                    //if items are either batch or serial products
                    if (count($batchProducts) > 0) {
                        $productqty = 0;
                        foreach ($batchProducts as $batchKey => $batchValue) {
                            $locationProductID = $batchValue['locationProductID'];

                            if ($p['copyFromDeliveryNote']) {
                                    $deliveryNoteProduct = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($p['deliveryNoteID'], $p['productID']);
                                    $deliveryNoteProductID = $deliveryNoteProduct->deliveryNoteProductID;
                                    $deliveryNoteProductPrice = $deliveryNoteProduct->deliveryNoteProductPrice;
                                    $productqty += $batchValue['qtyByBase'];
                                if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                    $deliveryNoteSubProduct = $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->getProductSerialByDeliveryNoteProductIDAndProductSerialID($deliveryNoteProductID, $batchValue['serialID']);
                                    $deliveryNoteSubProductID = $deliveryNoteSubProduct->deliveryNoteSubProductID;
                                    $newQty = 0;
                                } else if( $batchValue['qtyByBase'] != 0 && $batchValue['batchID'] ){
                                    $deliveryNoteSubProduct = $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->getProductBatchByDeliveryNoteProductIDAndProductBatchID($deliveryNoteProductID, $batchValue['batchID']);

                                    $deliveryNoteSubProductID = $deliveryNoteSubProduct->deliveryNoteSubProductID;
                                    $newQty = $deliveryNoteSubProduct->deliveryProductSubQuantity - $batchValue['qtyByBase'];
                                }
                                $subProductData = array(
                                    'deliveryNoteSubProductsCopiedQuantity'=> $newQty,
                                );
                                $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->updateDeliveryNoteSubProductData($subProductData, $deliveryNoteSubProductID);
                            } else {

                                if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                                    $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                                    $productBatchQuentity = $batchProduct->productBatchQuantity + $batchValue['qtyByBase'];
                                    $productBatchQty = array(
                                        'productBatchQuantity' => $productBatchQuentity,
                                        );
                                    $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                                    if ($batchValue['serialID']) {
                                    //Add details to item in table for batch and serial products
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchSerialProductDetails($locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchSerialItemOutDetails($batchValue['serialID'], $batchValue['batchID'], 'Sales Invoice', $invoiceID);

                                        $unitPrice = $itemInDetails['itemInPrice'];
                                        $unitDiscount = $itemInDetails['itemInDiscount'];
                                        if($averageCostingFlag == 1){
                                            $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                            $unitDiscount = 0;
                                        }

                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                        $itemInData = array(
                                            'itemInIndex' => $newItemInIndex,
                                            'itemInDocumentType' => 'Invoice Edit',
                                            'itemInDocumentID' => $invoiceID,
                                            'itemInLocationProductID' => $locationProductID,
                                            'itemInBatchID' => $batchValue['batchID'],
                                            'itemInSerialID' => $batchValue['serialID'],
                                            'itemInQty' => 1,
                                            'itemInPrice' => $unitPrice,
                                            'itemInDiscount' => $unitDiscount,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                            );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);
                                    //update itemOut return qty
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, '1');
                                    } else {
                                    //Add details to item in table for batch products
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getBatchProductDetails($locationProductID, $batchValue['batchID']);
                                        $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getBatchItemOutDetails($batchValue['batchID'], 'Sales Invoice', $invoiceID);

                                        $unitPrice = $itemInDetails['itemInPrice'];
                                        $unitDiscount = $itemInDetails['itemInDiscount'];
                                        if($averageCostingFlag == 1){
                                            $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                            $unitDiscount = 0;
                                        }
                                        $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                        $itemInData = array(
                                            'itemInIndex' => $newItemInIndex,
                                            'itemInDocumentType' => 'Invoice Edit',
                                            'itemInDocumentID' => $invoiceID,
                                            'itemInLocationProductID' => $locationProductID,
                                            'itemInBatchID' => $batchValue['batchID'],
                                            'itemInSerialID' => NULL,
                                            'itemInQty' => $batchValue['qtyByBase'],
                                            'itemInPrice' => $unitPrice,
                                            'itemInDiscount' => $unitDiscount,
                                            'itemInDateAndTime' => $this->getGMTDateTime(),
                                            );
                                        $itemInModel = new ItemIn();
                                        $itemInModel->exchangeArray($itemInData);
                                        $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                    //update itemout return qty
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, $batchValue['qtyByBase']);
                                    }
                                } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                //Add details to item in table for serial products
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getSerialProductDetails($locationProductID, $batchValue['serialID']);
                                    $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getSerialItemOutDetails($batchValue['serialID'], 'Sales Invoice', $invoiceID);

                                    $unitPrice = $itemInDetails['itemInPrice'];
                                    $unitDiscount = $itemInDetails['itemInDiscount'];
                                    if($averageCostingFlag == 1){
                                        $unitPrice = $itemOutDetails->itemOutAverageCostingPrice;
                                        $unitDiscount = 0;
                                    }

                                    $newItemInIndex = $this->CommonTable('Inventory\Model\ItemInTable')->getNewItemInIndex($itemInDetails['itemInID']);
                                    $itemInData = array(
                                        'itemInIndex' => $newItemInIndex,
                                        'itemInDocumentType' => 'Invoice Edit',
                                        'itemInDocumentID' => $invoiceID,
                                        'itemInLocationProductID' => $locationProductID,
                                        'itemInBatchID' => NULL,
                                        'itemInSerialID' => $batchValue['serialID'],
                                        'itemInQty' => 1,
                                        'itemInPrice' => $unitPrice,
                                        'itemInDiscount' => $unitDiscount,
                                        'itemInDateAndTime' => $this->getGMTDateTime(),
                                        );
                                    $itemInModel = new ItemIn();
                                    $itemInModel->exchangeArray($itemInData);
                                    $this->CommonTable('Inventory\Model\ItemInTable')->saveItemIn($itemInModel);

                                //update itemOut return qty
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->updateItemOutReturnQty($itemOutDetails->itemOutID, '1');
                                }
                                if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {
                                    $serialProductData = array(
                                        'productSerialSold' => '0',
                                        );
                                    $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                                }
                            }
                        }
                        if ($p['copyFromDeliveryNote']) {
                            $itemOutDetails = $this->CommonTable('Inventory\Model\ItemOutTable')->getItemOutDetailsForProductByDocument($locationProductID, 'Delivery Note', $p['deliveryNoteID']);
                            $deliveryNoteProduct = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($p['deliveryNoteID'], $p['productID']);
                            $productQty = $deliveryNoteProduct->deliveryNoteProductQuantity - $productqty;
                            $deliveryNoteData = array(
                                'copied'=>0,
                                'deliveryNoteProductCopiedQuantity'=>$productQty
                                );
                            $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryNoteProducts($p['productID'], $p['deliveryNoteID'], $deliveryNoteData);


                            $deliveryNoteStatus = array(
                                'deliveryNoteStatus'=> 3
                            );
                            $this->CommonTable('Invoice\Model\DeliveryNoteTable')->updateDeliveryNoteStatusToOpen($p['deliveryNoteID'], $deliveryNoteStatus);

                            foreach (array_reverse($itemOutDetails) as $itemOutData) {
                                $deliveryNoteProductPrice = $deliveryNoteProduct->deliveryNoteProductPrice;
                                $data = array(
                                    'itemOutPrice' => $deliveryNoteProductPrice,
                                    );
                                $this->CommonTable("Inventory\Model\ItemOutTable")->updateDeliveryNoteUnitPrice($data, $locationProductID, $p['deliveryNoteID']);
                            }
                        }
                    }
                    if (!$p['copyFromDeliveryNote'] && !$p['copyFromJob']) {
                        $itemInData = $this->CommonTable('Inventory\Model\ItemInTable')->getItemInProductDetails($locationProductID);
                        $locationPrTotalQty = 0;
                        $locationPrTotalPrice = 0;
                        $newItemInIds = array();
                        if(count($itemInData) > 0){
                            foreach ($itemInData as $key => $value) {
                                if($value->itemInID > $locationProductLastItemInID){
                                    $newItemInIds[] = $value->itemInID;
                                    $remainQty = $value->itemInQty - $value->itemInSoldQty;
                                    if($remainQty > 0){
                                        $itemInPrice = $remainQty*$value->itemInPrice;
                                        $itemInDiscount = $remainQty*$value->itemInDiscount;
                                        $locationPrTotalQty += $remainQty;
                                        $locationPrTotalPrice += $itemInPrice - $itemInDiscount;
                                    }
                                    $locationProductLastItemInID = $value->itemInID;
                                }
                            }
                        }
                        $locationPrTotalQty += $locationProductQuantity;
                        $locationPrTotalPrice += $locationProductQuantity*$locationProductAverageCostingPrice;

                        if($locationPrTotalQty > 0){
                            $newAverageCostinPrice = $locationPrTotalPrice/$locationPrTotalQty;
                            $lpdata = array(
                                'locationProductAverageCostingPrice' => $newAverageCostinPrice,
                                'locationProductLastItemInID' => $locationProductLastItemInID,
                                );
                            $this->CommonTable('Inventory\Model\LocationProductTable')->updateLocationProductByArray($lpdata, $locationProductID);

                            foreach ($newItemInIds as $inID) {
                                $intemInData = array(
                                    'itemInAverageCostingPrice' =>  $newAverageCostinPrice,
                                    );
                                $result = $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInDataByItemInId($intemInData, $inID);
                            }
                        }
                    }
                }

                $accountProduct = array();
                if ($this->useAccounting == 1) {
                    $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('1', $invoiceID);
                    $journalEntryID = $journalEntryData['journalEntryID'];
 
                    $jEAccounts = $this->CommonTable('Accounting\Model\JournalEntryAccountsTable')->getJournalEntryAccountsByJournalEntryID($journalEntryID);

                    $i=0;
                    $journalEntryAccounts = array();
                    foreach ($jEAccounts as $key => $value) {
                        $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                        $journalEntryAccounts[$i]['financeAccountsID'] = $value['financeAccountsID'];
                        $journalEntryAccounts[$i]['financeGroupsID'] = $value['financeGroupsID'];
                        $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsCreditAmount'];
                        $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsDebitAmount'];
                        $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Invoice Edit '.$param['invoiceCode'].'.';
                        $i++;
                    }
                    //get journal entry reference number.
                    $jeresult = $this->getReferenceNoForLocation('30', $locationID);
                    $jelocationReferenceID = $jeresult['locRefID'];
                    $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                    $journalEntryData = array(
                        'journalEntryAccounts' => $journalEntryAccounts,
                        'journalEntryDate' => $this->convertDateToStandardFormat($param['invoiceDate']),
                        'journalEntryCode' => $JournalEntryCode,
                        'journalEntryTypeID' => '',
                        'journalEntryIsReverse' => 0,
                        'journalEntryComment' => 'Journal Entry is posted when edit Invoice '.$param['invoiceCode'].'.',
                        'documentTypeID' => 1,
                        'journalEntryDocumentID' => $invoiceID,
                        'ignoreBudgetLimit' => $param['ignoreBudgetLimit'],
                        );

                    $resultData = $this->saveJournalEntry($journalEntryData);

                    if(!$resultData['status']){

                        $this->rollback();
                        $this->status = false;
                        $this->msg = $resultData['msg'];
                        $this->data = $resultData['data'];

                        return $this->JSONRespond();
                    }

                    $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($invoiceID,1, 10);
                    if(!$jEDocStatusUpdate['status']){
                        return array(
                            'status'=> false,
                            'msg'=>$jEDocStatusUpdate['msg'],
                            );
                    }  

                    foreach ($dimensionData as $value) {
                        if (!empty($value)) {
                            $saveRes = $this->saveDimensionsForJournalEntry($value, $resultData['data']['journalEntryID'], $param['ignoreBudgetLimit'], $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                            if(!$saveRes['status']){
                                $this->rollback();
                                $this->status = false;
                                $this->msg = $saveRes['msg'];
                                $this->data = $saveRes['data'];
                                return $this->JSONRespond();
                            }   
                        }
                    }
                }

                $saveInvoice = $this->storeInvoice($param, $customerID, false, true);
                // if this is come from invoice edit, need to update payment ids and map existing payment ids to the newly created invoice.
                if($param['salesInvoicePayedAmount'] != null){
                    // update invoicePayment table
                    $updatedSalesPaymentDetails = array(
                        'salesInvoiceID' => $saveInvoice['data']['salesInvoiceID'],
                        );
                    $updateSalesPayment = $this->CommonTable('Invoice\Model\InvoicePaymentsTable')->updateInvoicePaymentByInvoiceId($updatedSalesPaymentDetails,$invIDForPaymentUpdate);
                }
                if ($saveInvoice['status']) {
                    // //updates customer balance
                    // $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                    // $custNewCurrentBalance = ($customerData->customerCurrentBalance) + ($invoiceTotalPrice * $customCurrencyRate - $previousInvoiceTotal);
                    // $custData = array(
                    //     'customerCurrentBalance' => $custNewCurrentBalance,
                    //     'customerID' => $customerID
                    // );
                    // $customerAllData = new Customer;
                    // $customerAllData->exchangeArray($custData);
                    // $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);
                    // $row = $this->CommonTable('Invoice\Model\InvoiceTable')->getAllInvoiceDetailsByInvoiceID($param['invoiceID'])->current();
            
                    // $changeArray = $this->getInvoiceChageDetails($row, $param);

                    // //set log details
                    // $previousData = json_encode($changeArray['previousData']);
                    // $newData = json_encode($changeArray['newData']);

                    // $this->setLogDetailsArray($previousData,$newData);
                    // $this->setLogMessage("Invoice ".$changeArray['previousData']['Sales Invoice Code']." is updated.");

                    if ($customerChangeFlag) {
                        //updates old customer balance
                        $oldCustomerData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($oldCustomerID);
                        $oldCustNewCurrentBalance = floatval($oldCustomerData->customerCurrentBalance) - floatval($previousInvoiceTotal);
                        $oldCustData = array(
                            'customerCurrentBalance' => $oldCustNewCurrentBalance,
                            'customerID' => $oldCustomerID
                        );
                        $oldCustomerAllData = new Customer;
                        $oldCustomerAllData->exchangeArray($oldCustData);
                        $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($oldCustomerAllData);

                        //updates new customer balance
                        $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                        $custNewCurrentBalance = ($customerData->customerCurrentBalance) + ($invoiceTotalPrice * $customCurrencyRate);
                        $custData = array(
                            'customerCurrentBalance' => $custNewCurrentBalance,
                            'customerID' => $customerID
                        );
                        $customerAllData = new Customer;
                        $customerAllData->exchangeArray($custData);
                        $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);

                        if (!empty($param['jobID'])) {

                            //update job table customer
                            $updateJobCustomer = $this->CommonTable('Jobs\Model\JobTable')->updateJobCustomer($param['jobID'],$customerID);

                            $getVehicleByJobId = $this->CommonTable('Jobs\Model\JobVehicleTable')->getJobVehicleByJobID($param['jobID'])->current();
                            $vehicleID = $getVehicleByJobId['vehicleID'];

                            //update service Vehicle customer
                            $updateServiceVehicleCustomer = $this->CommonTable('Jobs\Model\ServiceVehicleTable')->updateVehicleCustomer($vehicleID, $customerID);
                        } 


                    } else {
                        //updates customer balance
                        $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                        $custNewCurrentBalance = ($customerData->customerCurrentBalance) + ($invoiceTotalPrice * $customCurrencyRate - $previousInvoiceTotal);
                        $custData = array(
                            'customerCurrentBalance' => $custNewCurrentBalance,
                            'customerID' => $customerID
                        );
                        $customerAllData = new Customer;
                        $customerAllData->exchangeArray($custData);
                        $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);
                    }



                    $this->commit();
                    $this->status = true;
                    $this->msg = 'Invoice has successfully updated.!';
                    $this->data = $saveInvoice;
                    $this->flashMessenger()->addMessage($this->msg);
                    return $this->JSONRespond();
                } else {
                    $this->rollback();
                    $this->status = false;
                    $this->data = $saveInvoice['data'];
                    $this->msg = $saveInvoice['msg'];

                    return $this->JSONRespond();
                }
            } else {
                //there has no products changes
                //so normaly update only InvoiceTable

                $invoiceCode = $param['invoiceCode'];
                $locationOut = $param['locationOutID'];
                $invoiceDate = $this->convertDateToStandardFormat($param['invoiceDate']);
                $dueDate = $this->convertDateToStandardFormat($param['dueDate']);
                $deliveryCharge = $param['deliveryCharge'] * $customCurrencyRate;
                $totalPrice = trim($param['invoiceTotalPrice']) * $customCurrencyRate;
                $comment = $param['invoiceComment'];
                $paymentTermID = $param['paymentTermID'];
                $showTax = $param['showTax'];
                $deliveryNoteID = (!empty($param['deliveryNoteID'])) ? $param['deliveryNoteID'] : null;
                $salesOrderID = (!empty($param['salesOrderID'])) ? $param['salesOrderID'] : null;
                $quotationID = (!empty($param['quotationID'])) ? $param['quotationID'] : null;
                $activityID = (!empty($param['activityID'])) ? $param['activityID'] : null;
                $jobID = (!empty($param['jobID'])) ? $param['jobID'] : null;
                $projectID = (!empty($param['projectID'])) ? $param['projectID'] : null;
                $salesPersonIDArr = $param['salesPersonID'];
                $invoiceTotalDiscount = $param['invoiceTotalDiscount'] * $customCurrencyRate;
                $salesInvoicePayedAmount = $param['salesInvoicePayedAmount'] * $customCurrencyRate;
                $suspendedTax = $param['suspendedTax'];
                $deliveryAddress = $param['deliveryAddress'];
                $promotionID = (!empty($param['promotionID'])) ? $param['promotionID'] : null;
                $promotionDiscountValue = $param['promotionDiscountValue'];
                $totalInvoiceDiscountValue = (!empty($param['invoiceDiscountRate'])) ? $param['invoiceDiscountRate'] : null;
                $invoiceDiscoutType = $param['invoiceDiscountType'];
                $totalTax = $param['totalTaxValue'] * $customCurrencyRate;
                $invoiceWisePromotionDiscountType = $param['invoiceWisePromotionDiscountType'];
                $salesInvoiceDeliveryChargeEnable = $param['salesInvoiceDeliveryChargeEnable'];
                $priceListId = (!empty($param['priceListId'])) ? $param['priceListId'] : null;
                $customerProfID = $param['customerProfID'];
                $inclusiveTax = (int)filter_var($param['inclusiveTax'], FILTER_VALIDATE_BOOLEAN);
                $nextInvoiceDate = ($param['nextInvoiceDate'] == "") ? null : $param['nextInvoiceDate'];
                $nextInvoiceDateComment = $param['nextInvoiceDateComment'];

                if ($deliveryCharge != '') {
                    $realItemTotalPrice = $totalPrice - $deliveryCharge;
                } else {
                    $realItemTotalPrice = $totalPrice;
                }
                if ($suspendedTax) {
                    $realItemTotalPrice = $realItemTotalPrice + $totalTax;
                }

                if ($invoiceDiscoutType == 'presentage') {
                    $totalInvoiceDiscount = ($totalInvoiceDiscountValue / (100 - $totalInvoiceDiscountValue)) * ($realItemTotalPrice - $totalTax);
                } else {
                    $totalInvoiceDiscount = $totalInvoiceDiscountValue * $customCurrencyRate;
                }

                if ($invoiceWisePromotionDiscountType == '2') {
                    $realItemTotalPrice = ($realItemTotalPrice + $totalInvoiceDiscount - ($totalTax));
                    $totalPromotionDiscountValue = (($realItemTotalPrice * 100) / (100 - $promotionDiscountValue)) - $realItemTotalPrice;
                } else if ($invoiceWisePromotionDiscountType == '1') {
                    $totalPromotionDiscountValue = $promotionDiscountValue * $customCurrencyRate;
                }
                $invoiceUpdateData = array(
                    'salesInvoiceCode' => $invoiceCode,
                    'customerID' => $customerID,
                    'locationID' => $locationOut,
                    'salesInvoiceIssuedDate' => $invoiceDate,
                    'salesInvoiceOverDueDate' => $dueDate,
                    'salesinvoiceTotalAmount' => $totalPrice,
                    'salesInvoiceComment' => $comment,
                    'salesInvoiceDeliveryCharge' => $deliveryCharge,
                    'paymentTermID' => $paymentTermID,
                    'quotationID' => $quotationID,
                    'deliveryNoteID' => $deliveryNoteID,
                    'salesOrderID' => $salesOrderID,
                    'activityID' => $activityID,
                    'jobID' => $jobID,
                    'projectID' => $projectID,
                    'salesInvoiceShowTax' => $showTax,
                    'salesPersonID' => null,
                    'salesInvoicePayedAmount' => $salesInvoicePayedAmount,
                    'salesInvoiceTotalDiscount' => $invoiceTotalDiscount,
                    'salesInvoiceSuspendedTax' => $suspendedTax,
                    'salesInvoiceDeliveryAddress' => $deliveryAddress,
                    'promotionID' => $promotionID,
                    'salesInvoicePromotionDiscount' => $promotionDiscountValue,
                    'salesInvoiceWiseTotalDiscount' => $totalInvoiceDiscount,
                    'salesInvoiceTotalDiscountType' => $invoiceDiscoutType,
                    'salesInvoiceDiscountRate' => $totalInvoiceDiscountValue,
                    'customCurrencyId' => $customCurrencyId,
                    'salesInvoiceCustomCurrencyRate' => $customCurrencyRate,
                    'priceListId' => $priceListId,
                    'salesInvoiceDeliveryChargeEnable' => $salesInvoiceDeliveryChargeEnable,
                    'customerProfileID' => $customerProfID,
                    'inclusiveTax' => $inclusiveTax,
                    'linkedCusOrderNum' =>  $linkedCusOrder
                );
                $salesInvoiceOldData = $this->CommonTable('Invoice\Model\InvoiceTable')->getAllInvoiceDetailsByInvoiceID($param['invoiceID'])->current();
                $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoice($invoiceUpdateData, $invoiceID);

                if (!empty($salesPersonIDArr) && sizeof($salesPersonIDArr) > 0) {
                    $valueForOneSalesPerson = $totalPrice / sizeof($salesPersonIDArr);

                    foreach ($salesPersonIDArr as $key5 => $value5) {
                        
                        $salesPersonData = [
                            "invoiceID" => $invoiceID,
                            "salesPersonID" => $value5,
                            "relatedSalesAmount" => $valueForOneSalesPerson
                        ];


                        $spData = new InvoiceSalesPersons;
                        $spData->exchangeArray($salesPersonData);

                        $spID = $this->CommonTable('Invoice\Model\InvoiceSalesPersonsTable')->saveSalesPerson($spData);
                    }
                } 

                if ($nextInvoiceDate != null) {
                    $this->CommonTable('Invoice\Model\MembershipTable')->deleteMembershipByInvoiceID($invoiceID);
                                        
                    $membershipData = [
                        'invoiceID' => $invoiceID,
                        'nextInvoiceDate' => $this->convertDateToStandardFormat($nextInvoiceDate),
                        'comment' => $nextInvoiceDateComment
                    ];

                    $memData = new Membership;
                    $memData->exchangeArray($membershipData);
                    $memID = $this->CommonTable('Invoice\Model\MembershipTable')->saveMembership($memData);

                    if (!$memID) {
                        $this->status = false;
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_INVOICE_MEMBERSHIP');
                        return $this->JSONRespond();
                    }
                }

                //updates customer balance
                $customerData = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                $custNewCurrentBalance = ($customerData->customerCurrentBalance) + ($totalPrice - $previousInvoiceTotal);
                $custData = array(
                    'customerCurrentBalance' => $custNewCurrentBalance,
                    'customerID' => $customerID
                );
                $customerAllData = new Customer;
                $customerAllData->exchangeArray($custData);
                $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);

                //update journal entry date
                if ($this->useAccounting == 1) {
                    //first check date in fiscal period 
                    $fiscalPeriod = $this->CommonTable('Accounting\Model\FiscalPeriodTable')->getAllFiscalPeriods();

                    $checkFiscalPeriod = false;
                    if(count($fiscalPeriod) > 0){
                        foreach ($fiscalPeriod as $key => $value) {
                            if($value['fiscalPeriodStatusID'] == 14){
                                if(($value['fiscalPeriodStartDate'] <= $invoiceDate && $invoiceDate <= $value['fiscalPeriodEndDate'])){
                                    $checkFiscalPeriod = true;
                                }
                            }
                        }
                    }
                    if (!$checkFiscalPeriod) {
                        $this->status = false;
                        $this->rollback();
                        $this->msg = $this->getMessage('ERR_JOURNAL_ENTRY_DATE_IS_NOTIN_FISCALPERIOD_RANGE');
                        return $this->JSONRespond();
                    }


                    $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('1', $invoiceID);
                    $jEData = array(
                        'journalEntryDate' => $invoiceDate
                        );
                    //update jE date
                    $jEDateUpdate = $this->CommonTable('Accounting\Model\JournalEntryTable')->updateJournalEntry($jEData,$journalEntryData['journalEntryID']);
                    
                }
        
                $changeArray = $this->getInvoiceChageDetails($salesInvoiceOldData, $param);

                //set log details
                $previousData = json_encode($changeArray['previousData']);
                $newData = json_encode($changeArray['newData']);

                $this->setLogDetailsArray($previousData,$newData);
                $this->setLogMessage("Invoice ".$changeArray['previousData']['Sales Invoice Code']." is updated.");

                $this->commit();
                $this->status = true;
                $this->data = ['data' => ['salesInvoiceID' => $invoiceID]];
                $this->msg = 'Invoice has successfully Updated.!';

                return $this->JSONRespond();
            }
        }
    }

    public function getInvoiceChageDetails($row, $data)
    {
        $previousData = [];
        $previousData['Sales Invoice Code'] = $row['salesInvoiceCode'];
        $previousData['Customer'] = $row['customerCode'] . '-' . $row['customerName'];
        $previousData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $previousData['Sales Invoice Issue Date'] = $this->convertDateToUserFormat($row['salesInvoiceIssuedDate']);
        $previousData['Sales Invoice Overdue Date'] = $this->convertDateToUserFormat($row['salesInvoiceOverDueDate']);
        $previousData['Sales Invoice Delivery Charge'] = $row['salesInvoiceDeliveryCharge'];
        $previousData['Sales Invoice Total'] = number_format($row['salesinvoiceTotalAmount'],2);
        $previousData['Sales Invoice Comment'] = $row['salesInvoiceComment'];

        $newData = [];
        $newData['Sales Invoice Code'] = $data['invoiceCode'];
        $newData['Customer'] = $row['customerCode'] . '-' . $row['customerName'];
        $newData['Location'] = $row['locationCode'] . '-' . $row['locationName'];
        $newData['Sales Invoice Issue Date'] = $this->convertDateToUserFormat($data['invoiceDate']);
        $newData['Sales Invoice Overdue Date'] = $this->convertDateToUserFormat($data['dueDate']);
        $newData['Sales Invoice Delivery Charge'] = $data['deliveryCharge'];
        $newData['Sales Invoice Total'] = number_format($data['invoiceTotalPrice'],2);
        $newData['Sales Invoice Comment'] = $data['invoiceComment'];

        return array('previousData' => $previousData, 'newData' => $newData);
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * @param array $products
     * @param array $insertedProducts
     * @param array $subProducts
     * @param array $insertedSubProducts
     * @param array $invoiceData
     * @param array $insertedInvoiceTopVals
     * @return array $invoiceChangedList
     */
    private function _getInvoiceChanges($products = [], $insertedProducts = [], $subProducts = [], $insertedSubProducts = [], $invoiceData, $insertedInvoiceTopVals)
    {
        $invoiceChangedList = [];
        if (!empty($products) && !empty($insertedProducts)) {

            //if there have new items
            if (array_diff_key($products, $insertedProducts)) {
                $newItems = array_diff_key($products, $insertedProducts);
                foreach ($newItems as $npID => $value) {
                    $invoiceChangedList[$npID]['newItems']['productName'] = $value['productName'];
                    $invoiceChangedList[$npID]['newItems']['productCode'] = $value['productCode'];
                    $invoiceChangedList[$npID]['newItems']['productPrice'] = $value['productPrice'];
                    $invoiceChangedList[$npID]['newItems']['qty'] = $value['deliverQuantity']['qty'];
                }
            }

            foreach ($insertedProducts as $iID => $i) {
                foreach ($products as $pID => $p) {

                    if (!array_key_exists($iID, $products)) {
                        $invoiceChangedList[$iID]['removeItems']['productName'] = $i['productName'];
                        $invoiceChangedList[$iID]['removeItems']['productCode'] = $i['productCode'];
                        $invoiceChangedList[$iID]['removeItems']['productPrice'] = $i['productPrice'];
                        $invoiceChangedList[$iID]['removeItems']['qty'] = $i['deliverQuantity']['qty'];
                    }

                    if (array_key_exists($iID, $products)) {
                        //if tax is removed from invoice edit
                        //then current array no any element called pTax
                        if ($pID == $iID) {
                            if (!isset($products[$pID]['pTax']) && isset($insertedProducts[$iID]['pTax'])) {

                                $invoiceChangedList[$iID]['removedTax'] = $insertedProducts[$iID]['pTax']['tL'];
                            } if (isset($products[$pID]['pTax']) && isset($insertedProducts[$iID]['pTax'])) {
                                $products[$pID]['pTax'] = (empty($products[$pID]['pTax'])) ? [] : $products[$pID]['pTax'];
                                $insertedProducts[$iID]['pTax'] = (empty($insertedProducts[$iID]['pTax'])) ? [] : $insertedProducts[$iID]['pTax'];
                                $diffProductTax = array_diff_assoc($products[$pID]['pTax'], $insertedProducts[$iID]['pTax']);
                                if (!empty($diffProductTax)) {

                                    foreach ($insertedProducts[$iID]['pTax']['tL'] as $itKey => $value) {
                                        foreach ($products[$pID]['pTax']['tL'] as $ptKey => $value) {

                                            if (array_key_exists($itKey, $products[$pID]['pTax']['tL'])) {
                                                continue;
                                            } else {
                                                $invoiceChangedList[$iID]['removedTax'][$itKey] = $insertedProducts[$iID]['pTax']['tL'][$itKey];
                                            }
                                        }
                                    }
                                }
                            }

                            //products changes get this methode without product qty
                            if ($diffProductInsideChanges = array_diff_assoc($products[$pID], $insertedProducts[$iID])) {
                                unset($diffProductInsideChanges['productTotal']);
                                unset($diffProductInsideChanges['pTax']);
                                unset($diffProductInsideChanges['productID']);

                                if (!empty($diffProductInsideChanges)) {
                                    foreach ($diffProductInsideChanges as $key => $value) {
                                        $invoiceChangedList[$pID]['itemChanges'][$key] = [
                                            'productName' => $insertedProducts[$iID]['productName'],
                                            'productCode' => $insertedProducts[$iID]['productCode'],
                                            'from' => [
                                                'source' => $key,
                                                'change' => $insertedProducts[$iID][$key]],
                                            'to' => [
                                                'source' => $key,
                                                'change' => $products[$pID][$key]
                                            ]
                                        ];
                                    }
                                }
                            }

                            //get qty changes
                            //this methode is using because of qty is inside the array(and also array_diff_assoc method)
                            if ($diffProductQtyInsideChanges = array_diff_assoc($products[$pID]['deliverQuantity'], $insertedProducts[$iID]['deliverQuantity'])) {
                                unset($diffProductInsideChanges['productTotal']);
                                foreach ($diffProductQtyInsideChanges as $key => $value) {

                                    $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($pID);
                                    $thisqty = $this->getProductQuantityViaDisplayUom($p['qty'], $productUom);
                                    $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? 0 : $thisqty['quantity'];

                                    $invoiceChangedList[$pID]['itemChanges'][$key] = [
                                        'productName' => $insertedProducts[$iID]['productName'],
                                        'productCode' => $insertedProducts[$iID]['productCode'], 'from' => [
                                            'source' => $key,
                                            'change' => number_format($insertedProducts[$iID]['deliverQuantity']['qty'], 2) . ' ' . $thisqty['uomAbbr']
                                        ], 'to' => [
                                            'source' => $key,
                                            'change' => number_format($products[$pID]['deliverQuantity']['qty'], 2) . ' ' . $thisqty['uomAbbr']
                                        ]
                                    ];
                                }
                            }

                            $productsTax = isset($products[$pID]['pTax']['tL']) ? $products[$pID]['pTax']['tL'] : [];
                            $insertedProductsTax = isset($insertedProducts[$iID]['pTax']['tL']) ? $insertedProducts[$iID]['pTax']['tL'] : [];
                            if ($diffProductTaxInsideChanges = array_diff_assoc($productsTax, $insertedProductsTax)) {
                                unset($diffProductInsideChanges['productTotal']);
                                foreach ($diffProductTaxInsideChanges as $key => $value) {
                                    $invoiceChangedList[$pID]['addedTax'][$key] = $value;
                                }
                            }
                        }
                    }
                }
            }

            //check invoice title data has changed

            $invoiceTitleChanges = array_diff_assoc($invoiceData, $insertedInvoiceTopVals);
            if (!empty($insertedInvoiceTopVals)) {
                foreach ($invoiceTitleChanges as $key => $value) {
                    $invoiceChangedList['titleChanges'][$key]['to'] = $invoiceData[$key];
                    $invoiceChangedList['titleChanges'][$key]['from'] = $insertedInvoiceTopVals[$key];
                }
            }
            //compaire subproducts list with current and already inserted
            if (!empty($subProducts)) {
                foreach ($subProducts as $pID => $data) {
                    foreach ($data as $key => $value) {
                        unset($subProducts[$pID][$key]['warranty']);
                        unset($subProducts[$pID][$key]['incrementID']);
                    }
                }
            }

            if (!empty($insertedSubProducts)) {
                foreach ($insertedSubProducts as $pID => $data) {
                    foreach ($data as $key => $value) {
                        unset($insertedSubProducts[$pID][$key]['warranty']);
                        unset($insertedSubProducts[$pID][$key]['incrementID']);
                    }
                }
            }

            $sortSubProducts = empty($subProducts) ? $subProducts : $this->_sortSubProducts($subProducts);
            $sortInsertedSubProducts = empty($insertedSubProducts) ? $insertedSubProducts : $this->_sortInsertedSubProducts($insertedSubProducts);

            if (!empty($sortSubProducts) && !empty($sortInsertedSubProducts)) {
                foreach ($sortInsertedSubProducts as $iProId => $iData) {
                    foreach ($sortSubProducts as $sProId => $sData) {

                        if (array_key_exists($iProId, $sortSubProducts)) {
                            foreach ($sortInsertedSubProducts[$iProId] as $iKey => $iSubData) {
                                foreach ($sortSubProducts[$iProId] as $sKey => $sSubData) {

                                    if (!array_key_exists($iKey, $sortSubProducts[$iProId])) {
                                        $invoiceChangedList[$iProId]['subProducts']['removed'][$iKey] = $iSubData;
                                    }

                                    if (!array_key_exists($sKey, $sortInsertedSubProducts[$iProId])) {
                                        $invoiceChangedList[$iProId]['subProducts']['added'][$sKey] = $sSubData;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return $invoiceChangedList;
        }
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * sort sub products
     * @param array $subProducts
     */
    private function _sortSubProducts($subProducts)
    {
        $sortSubProducts = [];
        foreach ($subProducts as $pID => $data) {
            foreach ($data as $key => $value) {
                $batchID = isset($value['batchID']) ? $value['batchID'] : '';
                $serialID = isset($value['serialID']) ? $value['serialID'] : '';
                $sortSubProducts[$pID][$serialID . $batchID] = [
                    'batchID' => $batchID,
                    'serialID' => $serialID,
                    'qtyByBase' => $value['qtyByBase']
                ];
            }
        }

        return $sortSubProducts;
    }

    /**
     * @author Sandun <sandun@thinkcube.com>
     * sort sub products
     * @param array $subProducts
     */
    private function _sortInsertedSubProducts($insertedSubProducts)
    {
        $sortInsertedSubProducts = [];
        foreach ($insertedSubProducts as $pID => $data) {
            foreach ($data as $key => $value) {
                $batchID = isset($value['batchID']) ? $value['batchID'] : '';
                $serialID = isset($value['serialID']) ? $value['serialID'] : '';
                $sortInsertedSubProducts[$pID][$serialID . $batchID] = [
                    'batchID' => $batchID,
                    'serialID' => $serialID,
                    'qtyByBase' => $value['qtyByBase']
                ];
            }
        }

        return $sortInsertedSubProducts;
    }

    public function getPromotion($promotionID = NULL)
    {
        $promotion = $this->CommonTable("Settings\Model\PromotionTable")->getPromotion($promotionID);
        return $promotion;
    }

    public function getInvoicesByCustomerProfileNameAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $profileName = $request->getpost('profileName');
            $invoices = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByCustomerProfileName($profileName, $locationID);

            $data = [];
            while ($invoice = $invoices->current()) {
                $data[$invoice['salesInvoiceID']] = (object) $invoice;
            }

            $customerInvoices = new ViewModel(array(
                'invoices' => $data,
                'statuses' => $this->getStatusesList(),
                'dateFormat' => $this->getUserDateFormat(),
                'companyCurrencySymbol' => $this->companyCurrencySymbol
                )
            );
            $customerInvoices->setTerminal(TRUE);
            $customerInvoices->setTemplate('invoice/invoice-api/invoice-list');

            return $customerInvoices;
        }
    }

    public function createJournalEntryTemplateForViewAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $requestData = $request->getPost();
        $customerID = (empty($requestData['customer'])) ? 0 :$requestData['customer'];
        $customCurrencyId = $requestData['customCurrencyId'];

//get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "") ? $customCurrencyData->currencyRate : 1;
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $ccrate = $requestData['customCurrencyRate'];
//if custom currency rate alredy set add that rate to sales Order
        if (!($ccrate == 0 || $ccrate == 1)) {
            $customCurrencyRate = $ccrate;
        }

        $deliveryCharge = $requestData['deliveryCharge'] * $customCurrencyRate;
        $products = $requestData['products'];
        $invoiceDiscoutType = $requestData['invoiceDiscountType'];
        $totalInvoiceDiscountValue = $requestData['invoiceDiscountRate'];
        $promotionID = $requestData['promotionID'];
        $invoiceWisePromotionDiscountType = $requestData['invoiceWisePromotionDiscountType'];
        $totalPrice = trim($requestData['invoiceTotalPrice']) * $customCurrencyRate;
        $suspendedTax = $requestData['suspendedTax'];
        $totalTax = $requestData['totalTaxValue'] * $customCurrencyRate;
        $promotionDiscountValue = $requestData['promotionDiscountValue'];
        $invoiceCode = $requestData['invoiceCode'];
        $locationOut = $requestData['locationOutID'];
        $addedServiceChargeArray = $requestData['addedServiceChargeArray'];
        $serviceChargeAmount = $requestData['serviceChargeAmount'];
        $invoiceDate = $this->convertDateToStandardFormat($requestData['invoiceDate']);

        $promotionData = $this->getPromotionDetails($promotionID);

        $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
        $displayData = $result->current();
        $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
        $averageCostingFlag = $displayData['averageCostingFlag'];

        if ($deliveryCharge != '') {
            $realItemTotalPrice = $totalPrice - $deliveryCharge;
        } else {
            $realItemTotalPrice = $totalPrice;
        }
        if ($suspendedTax) {
            $realItemTotalPrice = $realItemTotalPrice + $totalTax;
        }
        if ($invoiceDiscoutType == 'presentage') {
            $totalInvoiceDiscount = ($totalInvoiceDiscountValue / (100 - $totalInvoiceDiscountValue)) * ($realItemTotalPrice - $totalTax);
        } else {
            $totalInvoiceDiscount = $totalInvoiceDiscountValue * $customCurrencyRate;
        }

        if ($invoiceWisePromotionDiscountType == '2') {
            $realItemTotalPrice = ($realItemTotalPrice - ($totalTax));
            $totalPromotionDiscountValue = (($realItemTotalPrice * 100) / (100 - $promotionDiscountValue)) - $realItemTotalPrice;
        } else if ($invoiceWisePromotionDiscountType == '1') {
            $totalPromotionDiscountValue = $promotionDiscountValue * $customCurrencyRate;
        }

        $financeAccounts = $this->getFinanceAccounts();

        $accountProduct = array();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountsData = $glAccountExist->current();
        $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        $custSalesAccountIsset = true;
        if($this->useAccounting == 1){
            if(empty($customerData->customerSalesAccountID)){
                $custSalesAccountIsset = false;
                // return array(
                //     'status' => false,
                //     'msg' => $this->getMessage('ERR_CUSTOMER_SALES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                // );
            }else if(empty($customerData->customerReceviableAccountID)){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode));
                return $this->JSONRespond();
            }else if($deliveryCharge > 0 && empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_GL_DELIVERY_CHARGES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode));
                return $this->JSONRespond();
            }

            $totalCRAmounts = 0;
            $totalProductAmounts = 0;
            $totalTaxAmount = 0;
            foreach ($products as $keyt => $pt) {
                if(isset($pt['pTax']['tTA'])){
                    $ptwTax = ($pt['productTotal'] - $pt['pTax']['tTA']);
                    $totalTaxAmount += $pt['pTax']['tTA'];
                }else{
                    $ptwTax = $pt['productTotal'];
                }
                $totalProductAmounts += $ptwTax;
            }

            foreach ($products as $key => $p) {
                $batchProducts = [];
                if (isset($p['selected_serials']) && isset($requestData['subProducts'][$key])) {
                    foreach ($p['selected_serials'] as $in => $srl) {
                        $a = array_search($srl, array_column($requestData['subProducts'][$key], 'serialID'));
                        $batchProducts[] = $requestData['subProducts'][$key][$a];
                    }
                } else {
                    $batchProducts = $requestData['subProducts'][$key];
                }

                $productID = $p['productID'];
                $deliveryQty = $p['deliverQuantity']['qty'];

                $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

                $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $p['locationID']);
                $locationPID = $locationProduct->locationProductID;
                $averageCostingPrice = $locationProduct->locationProductAverageCostingPrice;

                if($p['giftCard'] != 1 && $p['productType'] != 2){
                    if($p['stockUpdate'] == 'true' && empty($pData['productInventoryAccountID'])){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                        return $this->JSONRespond();
                    }
                         //check whether Product Cost of Goods Sold Gl account id set or not
                    if($p['stockUpdate'] == 'true' && empty($pData['productCOGSAccountID'])){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName']));
                        return $this->JSONRespond();
                    }
                }

                if(!$custSalesAccountIsset){
                    if(empty($pData['productSalesAccountID'])){
                        $this->status = false;
                        $this->msg = $this->getMessage('ERR_PRPODUCT_SALES_ACCOUNT', array($pData['productCode'].' - '.$pData['productName']));
                        return $this->JSONRespond();
                    }
                }

                //set gl accounts for the journal entry
                //TODO after set the productTotal with tax value in pos should have to remove belowe condition the correct $pTotal is $p['productTotal'] - $p['pTax']['tTA'];
                if($pos){
                    $pTotal = $p['productTotal'];
                }else{
                    $pTotal = $p['productTotal'] - $p['pTax']['tTA'];
                }
                if($invoiceDiscoutType == "Value"){
                    $productTotal = $pTotal - $totalInvoiceDiscountValue*($pTotal/$totalProductAmounts);
                }else {
                    $productTotal = $pTotal;
                }
                if($promotionID != '' && $promotionData[$promotionID]['promoType'] == 2 && $promotionData[$promotionID]['discountType'] == 1){
                    $productTotal = $productTotal - $requestData['promotionDiscountValue']*($productTotal/$totalProductAmounts);
                }

                if($p['giftCard'] !=1){
                    $totalCRAmounts+=$productTotal;
                    // if($custSalesAccountIsset){
                    //     if(isset($accountProduct[$customerData->customerSalesAccountID]['creditTotal'])){
                    //         $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] += $productTotal;
                    //     }else{
                    //         $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] = $productTotal;
                    //         $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] = 0.00;
                    //         $accountProduct[$customerData->customerSalesAccountID]['accountID'] = $customerData->customerSalesAccountID;
                    //     }
                    // }else{
                    if(isset($accountProduct[$pData['productSalesAccountID']]['creditTotal'])){
                        $accountProduct[$pData['productSalesAccountID']]['creditTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productSalesAccountID']]['creditTotal'] = $productTotal;
                        $accountProduct[$pData['productSalesAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$pData['productSalesAccountID']]['accountID'] = $pData['productSalesAccountID'];
                    }
                    // }
                }

                if ($p['pTax'] && $p['pTax']['tL']) {
                    foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                        $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxKey);
                        if(empty($taxData['taxSalesAccountID'])){
                            $this->status = false;
                            $this->msg = $this->getMessage('ERR_TAX_SALES_ACCOUNT', array($taxData['taxName']));
                            return $this->JSONRespond();
                        }

                        $totalCRAmounts += $productTax['tA'] * $customCurrencyRate;
                    //set tax gl accounts for the journal Entry
                        if (($productTax['susTax'] == '0' && $requestData['suspendedTax'] == '1') || ($productTax['susTax'] == '0' && $requestData['suspendedTax'] == '0') || ($productTax['susTax'] == '1' && $requestData['suspendedTax'] == '0')) {
                            if(isset($accountProduct[$taxData['taxSalesAccountID']]['creditTotal'])){
                                $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] += $productTax['tA'] * $customCurrencyRate;
                            }else{
                                $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = $productTax['tA'] * $customCurrencyRate;
                                $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                            }
                        }
                    }
                }

                if ($p['productType'] != 2) {
                    if ($p['stockUpdate'] == 'true') {
                        if (!count($batchProducts) > 0) {
                            $sellingQty = $deliveryQty;
                            $allItemInDetails = [];
                            $allItemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getAvailableProductDetails($locationProduct->locationProductID);
                            while ($sellingQty != 0) {
                                // $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetailsFromArr($locationProduct->locationProductID);
                                $itemInDetails = current($allItemInDetails);
                                if (!$itemInDetails) {
                                    break;
                                }
                                if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                                    if($averageCostingFlag == 1){
                                    //set gl accounts for the journal entry
                                        $proTotal = $sellingQty * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $proTotal = $sellingQty * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                    $sellingQty = 0;
                                    break;
                                } else {
                                    if($averageCostingFlag == 1){
                                    //set gl accounts for the journal entry
                                        $proTotal =  $itemInDetails['itemInRemainingQty'] * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $proTotal = $itemInDetails['itemInRemainingQty'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                    $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                                    array_shift($allItemInDetails);
                                }
                            }
                        }
                    }
                }

                if (count($batchProducts) > 0) {
                    foreach ($batchProducts as $batchKey => $batchValue) {
                        if(!empty($batchValue)){
                    //insert data to the item out table
                            if ($p['stockUpdate'] == 'true') {
                                if ($batchValue['batchID']) {
                                    if ($batchValue['serialID']) {
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProduct->locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $proTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $proTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    } else {
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProduct->locationProductID, $batchValue['batchID']);
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $proTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $proTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                } else if ($batchValue['serialID']) {
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProduct->locationProductID, $batchValue['serialID']);
                                    if($p['giftCard'] != 1){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $proTotal = 1 * $averageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $proTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $proTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $proTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $proTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                }
                            }
                            //////////Item out insert data END /////////
                        }
                    }

                    if ($p['stockUpdate'] == 'true' && $p['productType'] != 2) {
                        if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID'] && empty($batchValue['batchID'])) {
                            $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                            if ($p['giftCard'] == 1) {
                                $psCode = $checkSerial->productSerialCode;
                                $giftCardDetails = $this->CommonTable('Inventory\Model\GiftCardTable')->getGiftCardDetailsByGiftCardCode($psCode);
                            }
                        }
                    }

                    if($p['giftCard'] == 1 && empty($giftCardDetails->giftCardGlAccountID)){
                        $this->status =false;
                        $this->msg =$this->getMessage('ERR_GIFTCARD_GL_ACCOUNT_NOT_SET', [$psCode]);
                        return $this->JSONRespond();
                    }else if($p['giftCard'] == 1){
                                        //set gl accounts for the journal entry
                    //TODO after set the productTotal with tax value in pos should have to remove belowe condition the correct $pTotal is $p['productTotal'] - $p['pTax']['tTA'];
                        $pTotal = $p['productTotal'] - $p['pTax']['tTA'];

                        if($invoiceDiscoutType == "Value"){
                            $proTotal = $pTotal - $totalInvoiceDiscountValue*($pTotal/$totalProductAmounts);
                        }else {
                            $proTotal = $pTotal;
                        }
                        if($promotionID != '' && $promotionData[$promotionID]['promoType'] == 2 && $promotionData[$promotionID]['discountType'] == 1){
                            $proTotal = $proTotal - $totalPromotionDiscountValue*($proTotal/$totalProductAmounts);
                        }

                        $totalCRAmounts+=$proTotal;
                        if(isset($accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'])){
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'] += $proTotal;
                        }else{
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'] = $proTotal;
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['debitTotal'] = 0.00;
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['accountID'] = $giftCardDetails->giftCardGlAccountID;
                        }
                    }
                }
            }

            //set gl accounts for the journal entry
            $deliveryTotal = $deliveryCharge;
            $totalCRAmounts += $deliveryCharge;
            if($deliveryTotal > 0){
                if(isset($accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'])){
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] += $deliveryTotal;
                }else{
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] = $deliveryTotal;
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['debitTotal'] = 0.00;
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['accountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                }
            }

            if(sizeof($addedServiceChargeArray) > 0 && $serviceChargeAmount > 0){
                foreach ($addedServiceChargeArray as $key => $value) {
                    $totalCRAmounts += $value['serviceChargeValueTxt'];
                    $serviceChargeTypeData =  $this->CommonTable("Invoice\Model\ServiceChargeTypeTable")->getServiceChargeTypeDetailsById($value['serviceChargeTypeId'])->current();

                    if(isset($accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['creditTotal'])){
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['creditTotal'] += $value['serviceChargeValueTxt'];
                    }else{
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['creditTotal'] = $value['serviceChargeValueTxt'];
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['accountID'] = $serviceChargeTypeData['serviceChargeTypeGlAccountID'];
                    }
                }
            }

            if ($requestData['suspendedTax'] == '1') {
                $totalCRAmounts -= floatval($requestData['susTaxAmount']);
            }

            if(isset($accountProduct[$customerData->customerReceviableAccountID]['debitTotal'])){
                $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] += $totalCRAmounts;
            }else{
                $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] = $totalCRAmounts;
                $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] = 0.00;
                $accountProduct[$customerData->customerReceviableAccountID]['accountID'] = $customerData->customerReceviableAccountID;
            }

            $i=0;
            $journalEntryAccounts = array();

            foreach ($accountProduct as $key => $value) {
                $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                $journalEntryAccounts[$i]['financeAccountsName'] = $financeAccounts[$value['accountID']]['financeAccountsCode']."_".$financeAccounts[$value['accountID']]['financeAccountsName'];
                $journalEntryAccounts[$i]['financeGroupsID'] = '';
                $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Invoice '.$invoiceCode.'.';
                $i++;
            }
                    //get journal entry reference number.
            $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
            $jelocationReferenceID = $jeresult['locRefID'];
            $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

            $journalEntryData = array(
                'journalEntryAccounts' => $journalEntryAccounts,
                'journalEntryDate' => $invoiceDate,
                'journalEntryCode' => $JournalEntryCode,
                'journalEntryTypeID' => '',
                'journalEntryIsReverse' => 0,
                'journalEntryComment' => 'Journal Entry is posted when create Invoice '.$invoiceCode.'.',
                'documentTypeID' => 1,
                'journalEntryDocumentID' => '',
            );
            $this->status = true;
            $this->data = $journalEntryData;
            return $this->JSONRespond();
        }else{
            $this->status = false;
            $this->msg = "This accounts does not allow to view journal entries.";
            return $this->JSONRespond();
        }
    }

    public function getAllRelatedDocumentDetailsByInvoiceIdAction()
    {
        $request = $this->getRequest();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        if ($request->isPost()) {
            $invoiceID = $request->getPost('invoiceID');
            $invoiceData = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBasicDetailsByInvoiceId($invoiceID);
            $deliveryNoteData = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getinvoiceRelatedDeliveryNoteDataByInvoiceId($invoiceID);
            $dlnRelatedSalesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getDlnRelatedSalesOrderDataBydlnId(null, $invoiceID);
            $salesOrderData = $this->CommonTable('Invoice\Model\SalesOrderTable')->getInvoiceRelatedSalesOrderDataByInvoiceId($invoiceID);
            $dlnRelatedQuotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getDlnRelatedQuotationDataBydlnId(null, $invoiceID);
            $soRelatedQuotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getSalesOrderRelatedQuotationDataBySOId(null, $invoiceID);
            $salesReturnData = $this->CommonTable('SalesReturnsTable')->getDeliveryNoteRelatedReturnsDataByQuotationId(null, null, null, $invoiceID);
            $quotaionData = $this->CommonTable('Invoice\Model\QuotationTable')->getInvoiceRelatedQuotationeDataByInvoiceId($invoiceID);
            $paymentData = $this->CommonTable('Invoice\Model\PaymentsTable')->getDeliveryNoteRelatedInvoicePaymentsDataByQuotationId(null, null, null, $invoiceID);
            $creditNoteData = $this->CommonTable('Invoice\Model\CreditNoteTable')->getDeliveryNoteRelatedCreditNoteDataByQuotationId(null, null, null, $invoiceID);
            $creditNotePaymentData = $this->CommonTable('Invoice\Model\CreditNotePaymentTable')->getDeliveryNoteRelatedCreditNotePaymentDataByQuotationId(null, null, null, $invoiceID);
            

            $dataExistsFlag = false;
            if ($invoiceData) {
                $invoiceData = array(
                    'type' => 'SalesInvoice',
                    'documentID' => $invoiceData['salesInvoiceID'],
                    'code' => $invoiceData['salesInvoiceCode'],
                    'amount' => number_format($invoiceData['salesinvoiceTotalAmount'], 2),
                    'issuedDate' => $invoiceData['salesInvoiceIssuedDate'],
                    'created' => $invoiceData['createdTimeStamp'],
                );
                $invoiceDetails[] = $invoiceData;
                $dataExistsFlag = true;
            }

            if (isset($deliveryNoteData)) {
                foreach ($deliveryNoteData as $dlnDta) {
                    $dlnData = array(
                        'type' => 'DeliveryNote',
                        'documentID' => $dlnDta['dlnID'],
                        'code' => $dlnDta['deliveryNoteCode'],
                        'amount' => number_format($dlnDta['deliveryNotePriceTotal'], 2),
                        'issuedDate' => $dlnDta['deliveryNoteDeliveryDate'],
                        'created' => $dlnDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $dlnData;
                    if (isset($dlnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($dlnRelatedSalesOrderData)) {
                foreach ($dlnRelatedSalesOrderData as $soDta) {
                    $soData = array(
                        'type' => 'SalesOrder',
                        'documentID' => $soDta['soID'],
                        'code' => $soDta['soCode'],
                        'amount' => number_format($soDta['totalAmount'], 2),
                        'issuedDate' => $soDta['issuedDate'],
                        'created' => $soDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $soData;
                    if (isset($soDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($salesOrderData)) {
                foreach ($salesOrderData as $soDta) {
                    $soData = array(
                        'type' => 'SalesOrder',
                        'documentID' => $soDta['soID'],
                        'code' => $soDta['soCode'],
                        'amount' => number_format($soDta['totalAmount'], 2),
                        'issuedDate' => $soDta['issuedDate'],
                        'created' => $soDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $soData;
                    if (isset($soDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($dlnRelatedQuotaionData)) {
                foreach ($dlnRelatedQuotaionData as $qtnDta) {
                    $qtnDeta = array(
                        'type' => 'Quotation',
                        'documentID' => $qtnDta['quotationID'],
                        'code' => $qtnDta['quotationCode'],
                        'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                        'issuedDate' => $qtnDta['quotationIssuedDate'],
                        'created' => $qtnDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $qtnDeta;
                    if (isset($qtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($soRelatedQuotaionData)) {
                foreach ($soRelatedQuotaionData as $qtnDta) {
                    $qtnDeta = array(
                        'type' => 'Quotation',
                        'documentID' => $qtnDta['quotationID'],
                        'code' => $qtnDta['quotationCode'],
                        'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                        'issuedDate' => $qtnDta['quotationIssuedDate'],
                        'created' => $qtnDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $qtnDeta;
                    if (isset($qtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            if (isset($quotaionData)) {
                foreach ($quotaionData as $qtnDta) {
                    $qtnDeta = array(
                        'type' => 'Quotation',
                        'documentID' => $qtnDta['quotationID'],
                        'code' => $qtnDta['quotationCode'],
                        'amount' => number_format($qtnDta['quotationTotalAmount'], 2),
                        'issuedDate' => $qtnDta['quotationIssuedDate'],
                        'created' => $qtnDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $qtnDeta;
                    if (isset($qtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }

            
            
            if (isset($salesReturnData)) {
                foreach ($salesReturnData as $rtnDta) {
                    $srData = array(
                        'type' => 'SalesReturn',
                        'documentID' => $rtnDta['salesReturnID'],
                        'code' => $rtnDta['salesReturnCode'],
                        'amount' => number_format($rtnDta['salesReturnTotal'], 2),
                        'issuedDate' => $rtnDta['salesReturnDate'],
                        'created' => $rtnDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $srData;
                    if (isset($rtnDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
           
            
            if (isset($paymentData)) {
                foreach ($paymentData as $payDta) {
                    $paymentData = array(
                        'type' => 'CustomerPayment',
                        'documentID' => $payDta['incomingPaymentID'],
                        'code' => $payDta['incomingPaymentCode'],
                        'amount' => number_format($payDta['incomingPaymentPaidAmount'], 2),
                        'issuedDate' => $payDta['incomingPaymentDate'],
                        'created' => $payDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $paymentData;
                    if (isset($payDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($creditNoteData)) {
                foreach ($creditNoteData as $cNDta) {
                    $creditNoteData = array(
                        'type' => 'CreditNote',
                        'documentID' => $cNDta['creditNoteID'],
                        'code' => $cNDta['creditNoteCode'],
                        'amount' => number_format($cNDta['creditNoteTotal'], 2),
                        'issuedDate' => $cNDta['creditNoteDate'],
                        'created' => $cNDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $creditNoteData;
                    if (isset($cNDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            if (isset($creditNotePaymentData)) {
                foreach ($creditNotePaymentData as $cNPDta) {
                    $creditNotePaymentData = array(
                        'type' => 'CreditNotePayment',
                        'documentID' => $cNPDta['creditNotePaymentID'],
                        'code' => $cNPDta['creditNotePaymentCode'],
                        'amount' => number_format($cNPDta['creditNotePaymentAmount'], 2),
                        'issuedDate' => $cNPDta['creditNotePaymentDate'],
                        'created' => $cNPDta['createdTimeStamp'],
                    );
                    $invoiceDetails[] = $creditNotePaymentData;
                    if (isset($cNPDta)) {
                        $dataExistsFlag = true;
                    }
                }
            }
            
            $sortData = Array();
            foreach ($invoiceDetails as $key => $r) {
                $sortData[$key] = strtotime($r['created']);
            }
            array_multisort($sortData, SORT_ASC, $invoiceDetails);

            $documentDetails = array(
                'invoiceDetails' => $invoiceDetails
            );

            if ($dataExistsFlag == true) {
                $this->data = $documentDetails;
                $this->status = true;
            } else {
                $this->status = false;
            }

            return $this->JSONRespond();
        }
    }
    /**
    * this function use to check that given customer credit limit and his ability to pay that invoice
    * @param Json Request
    * return Json Respond
    **/
    public function getCustomerValidateDetailsAction() 
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }
        $customerID = $request->getPost('customerID');
        $totalValue = $request->getPost('invoiceAmount');
        // get customer details
        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

        // get display setup details
        $displaySetup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();
        

        // validate customer credit limit if they use it
        if ($cust->customerCreditLimit != 0 && $cust->customerID != 0 && $displaySetup['customerCreditLimitApplyFlag']) {
            if (floatval($cust->customerCreditLimit) < (floatval($totalValue)) + floatval($cust->customerCurrentBalance)) {
                $this->status = true;
                return $this->JSONRespond();
            }
        }
        $this->status = false;
        return $this->JSONRespond();
    }

    /**
    * this function use to validate enterd user name and password
    * @param JSON Request
    * return Json Respond
    **/
    public function validateUserNameAndPasswordAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }
        // get usrname and psword
        $usrname = $request->getPost('username');
        $pswd = $request->getPost('password');

        // validate
        $respond = $this->validateAdminUser($pswd,$usrname);
        $this->msg = $respond['msg'];
        $this->status = $respond['status'];
        return $this->JSONRespond();
        
    }

    public function storeDraftInvoiceAction()
    {
        $request = $this->getRequest();
        if (!$request->isPost()) {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVALID_REQUEST');
            return $this->JSONRespond();
        }

        $body = file_get_contents('php://input');

        $postData = json_decode($body);
        $customerCode = $postData->customerCode;
        $totalValue = $postData->totalValue;
        $dataArray = json_decode($postData->dataArray);

        $this->beginTransaction();

        $nonInventoryProductDetails = $this->CommonTable("Inventory\Model\ProductTable")->getProductByPCode('CARHOTELITEM');
        if (!$nonInventoryProductDetails) {
            $res = $this->saveNonInventoryProduct();
            if ($res['status']) {
                $nonInventoryProductDetails = $this->CommonTable("Inventory\Model\ProductTable")->getProductByPCode('CARHOTELITEM');
            } else {
                $this->rollback();
                $this->msg = "Error occured while creating non-inventory item.";
                $this->data = null;
                $this->status = false;
                return $this->JSONRespond();
            }
        }
        $searchcust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByCustomerCode($customerCode);

        if (!$searchcust) {
            $this->rollback();
            $this->msg = "Customer not found.";
            $this->data = null;
            $this->status = false;
            return $this->JSONRespond();
        }

        $arr = '';
        foreach ($dataArray as $value) {
            $newArray = (array) $value;
            $arr.=implode(",", $newArray) . "\n";         
        }

        $name = "booking_voucher";
        $csvPath = $this->generateCSVFileForEPTTInvoice($name, $arr);
        $tempInvoiceData = [
            'customerID' => $searchcust['customerID'],
            'invoiceValue' => $totalValue,
            'attachment' => $csvPath,
            'productID' => $nonInventoryProductDetails['productID'],
        ];

        $draftInvoiceModel = new DraftInvoice();
        $draftInvoiceModel->exchangeArray($tempInvoiceData);

        $saveResult = $this->CommonTable("Invoice\Model\DraftInvoiceTable")->saveDraftInvoice($draftInvoiceModel);

        if ($saveResult) {
            $this->commit();
            $this->msg = "Draft invoice saved successfully";
            $this->data = $saveResult;
            $this->status = true;
            return $this->JSONRespond();
        } else {
            $this->rollback();
            $this->msg = "Error occured while saving draft invoice.";
            $this->data = null;
            $this->status = false;
            return $this->JSONRespond();
        }
    }

    public function saveNonInventoryProduct()
    {
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $isUseAccounting = $this->CommonTable('CompanyTable')->checkCompanyUseAccounting();
        if ($isUseAccounting) {
            $glAccountSetupData = $glAccountExist->current();

            if($glAccountSetupData->glAccountSetupItemDefaultSalesAccountID != ''){
                $productSalesAccountID = $glAccountSetupData->glAccountSetupItemDefaultSalesAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID != ''){
                $productInventoryAccountID = $glAccountSetupData->glAccountSetupItemDefaultInventoryAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID != ''){
                $productCOGSAccountID = $glAccountSetupData->glAccountSetupItemDefaultCOGSAccountID;
            }

            if($glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID != ''){
                $productAdjusmentAccountID = $glAccountSetupData->glAccountSetupItemDefaultAdjusmentAccountID;
            }
        }
        $proHandling = array('productHandelingManufactureProduct');
        $proUOM = [];
        $proUOM[1]['uomConversion'] = "1";
        $proUOM[1]['baseUom'] = "true";
        $proUOM[1]['displayUom'] = "true";

        $post = [
            'productCode' => "CARHOTELITEM",
            'productBarcode' => "",
            'productName' => "Non Inventory - Car booking",
            'productTypeID' => "2",
            'handeling' => $proHandling,
            'productDescription' => "",
            'productDiscountPercentage' => "",
            'productDiscountValue' => "",
            'purchaseDiscountEligible' => false,
            'productPurchaseDiscountPercentage' => "",
            'productPurchaseDiscountValue' => "",
            'productImageURL' => "0",
            'productState' => "1",
            'categoryID' => "1",
            'productGlobalProduct' => true,
            'batchProduct' => false,
            'serialProduct' => false,
            'hsCode' => "",
            'customDutyValue' => "",
            'customDutyPercentage' => "",
            'productDefaultOpeningQuantity' => "",
            'productDefaultSellingPrice' => "",
            'productDefaultMinimumInventoryLevel' => "",
            'productDefaultReorderLevel' => "",
            'productTaxEligible' => false,
            'discountEligible' => false,
            'productDefaultPurchasePrice' => "",
            'rackID' => "",
            'flag' => "product",
            'itemDetail' => "",
            'uomDisplay' => "1",
            'displayUoms' => "1",
            'uom' => $proUOM,
            'productSalesAccountID' => $productSalesAccountID,
            'productInventoryAccountID' => $productInventoryAccountID,
            'productCOGSAccountID' => $productCOGSAccountID,
            'productAdjusmentAccountID' => $productAdjusmentAccountID,
        ];

        $product = $this->getServiceLocator()->get("ProductAPIController");
        $productSaveState = $product->storeProduct($post, $openingBalanceFlag = true);
        if($productSaveState['status']){
            return ['status' => true, 'data' => $productSaveState['data']['productID'], 'msg'=> $productSaveState['msg']];
        } else {
            return ['status' => false, 'data' => $productSaveState['data']['productID'], 'msg'=> $productSaveState['msg']];
        }
    }

    public function getDraftInvoiceDataAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $draftInvoiceID = $request->getPost('draftInvoiceID');
            $locationID = $this->user_session->userActiveLocation['locationID'];
            $tempData = $this->CommonTable('Invoice\Model\DraftInvoiceTable')->getTempInvoiceDataByID($draftInvoiceID, $locationID);

            $productData['productID'] = $tempData['productID'];
            $productData['unitPrice'] = $tempData['invoiceValue'];
            $productData['locationID'] = $locationID;
            $productData['locationProductID'] = $tempData['locationProductID'];
            $productData['quantity'] = 1;
            $productData['productCode'] = $tempData['productCode'];
            $productData['productName'] = $tempData['productName'];
            
            $locationProducts = $this->getLocationProductDetails($locationID, $tempData['productID']);

            $customerID = ($tempData['customerID']) ? $tempData['customerID'] : 0;
            $customer = $this->getCustomerDetailsByCustomerID($customerID);

            $this->status = true;
            $this->data = array(
                'productData' => $productData,
                'customer' => $customer,
                'customerID' => $customerID,
                'attachment' => $tempData['attachment'],
                'locationProducts' => $locationProducts
            );

            return $this->JSONRespond();
        }
    }

    // API function for send sms
    public function sendSmsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $telephoneNumber = $post['telephoneNumber'];
            $smsBody = trim($post['smsBody']);
            
            $sendRes = $this->sendSms($telephoneNumber, $smsBody);

            if(!$sendRes){
                $this->status = false;
                $this->msg = $this->getMessage('ERR_SMS_SEND');
                $this->data = "";
                return $this->JSONRespond();
            }

            $this->status = true;
            $this->msg = $this->getMessage('SUCCESS_SMS_SEND');
            $this->data = "";
            return $this->JSONRespond();
        }
    }

    // API for Due Balance data
    public function getCustomersForDueBalanceCustomersAction()
    {
        $customers = [];
        $tempCusts = $this->CommonTable("Invoice\Model\InvoiceTable")->getOverdueInvoiceData();
        foreach ($tempCusts as $tempCust) {
            if ($tempCust['customerID'] != 0) {
                $customers[$tempCust['customerID']] = array(
                    'customerCode' => $tempCust['customerCode'], 
                    'customerName' => $tempCust['customerTitle'] . ' ' . $tempCust['customerName'],
                    'customerTelephoneNumber' => $tempCust['customerTelephoneNumber'],
                    'invoiceCount' => $tempCust['invoiceCount'],
                    'dueBalance' =>number_format(((float) $tempCust['dueBalance'] - (float) $tempCust['creditNoteTotal']), 2)
                );
            }
        }
        $this->status = TRUE;
        $this->data = $customers;
        return $this->JSONRespond();
    }

    public function getCustomersForDueInvoiceCustomersAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $startDate = $post['startDate'];
            $endDate = trim($post['endDate']);

            $customers = [];
            $tempCusts = $this->CommonTable("Invoice\Model\InvoiceTable")->getOverdueInvoiceBetweenDate($startDate,$endDate);
            foreach ($tempCusts as $tempCust) {
                if ($tempCust['customerID'] != 0) {
                    $customers[$tempCust['customerID']] = array(
                        'customerCode' => $tempCust['customerCode'], 
                        'customerName' => $tempCust['customerTitle'] . ' ' . $tempCust['customerName'],
                        'customerTelephoneNumber' => $tempCust['customerTelephoneNumber'],
                        'invoiceCount' => $tempCust['invoiceCount'],
                        'dueBalance' =>number_format($tempCust['dueBalance'], 2),
                        'invoiceCount' => $tempCust['invoiceCount'],
                        'invoicesDetails' => $tempCust['invoicesDetails'],
                    );
                }
            }
            $this->status = TRUE;
            $this->data = $customers;
            return $this->JSONRespond();
        }
    }

    public function getCustomersLastInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $post = $request->getPost();
            $startDate = $post['startDate'];
            $endDate = trim($post['endDate']);

            $customers = [];
            $tempCusts = $this->CommonTable("Invoice\Model\InvoiceTable")->getInvoiceBetweenDate($startDate,$endDate);
            foreach ($tempCusts as $tempCust) {
                if ($tempCust['customerID'] != 0) {
                    if (!array_key_exists($tempCust['customerID'], $customers)) {
                        $customers[$tempCust['customerID']] = array(
                            'customerCode' => $tempCust['customerCode'], 
                            'customerName' => $tempCust['customerTitle'] . ' ' . $tempCust['customerName'],
                            'customerTelephoneNumber' => $tempCust['customerTelephoneNumber'],
                            'salesinvoiceTotalAmount' =>number_format($tempCust['salesinvoiceTotalAmount'], 2),
                            'lastInvoiceDate' => $tempCust['invoiceIssuedDate'],
                            'salesPersonSortName' => $tempCust['salesPersonSortName'],
                        );
                    }
                }
            }
            $this->status = TRUE;
            $this->data = $customers;
            return $this->JSONRespond();
        }
    }


    public function sendApproverEmailAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invID = $request->getPost('invID');
            $invCode = $request->getPost('invCode');
            $approvers = $request->getPost('approvers');
            $token = $request->getPost('token');

            if ($invID && $invCode && $approvers && $token) {
                $this->sendInvoiceApproversEmail($invID, $invCode, $approvers, $token);
                $this->status = true;
                $this->msg = $this->getMessage('SUCC_INV_SEND_FOR_WF');
                return $this->JSONRespond();
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INV_SEND_FOR_WF');
                return $this->JSONRespond();
            }
        }
    }

    private function sendInvoiceApproversEmail($invID, $invCode, $approvers, $token)
    {   
        foreach ($approvers as $approver) {

            $approverData =  $this->CommonTable('Settings\Model\ApproverTable')->getApproverByID($approver)->current();
            $approverID = $approverData['approverID'];


            $approveUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/invoice-api/approve/?invID=' . $invID . '&action=approve&token=' . $token .'&approverID='.$approverID;
            $rejectUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/invoice-api/approve/?invID=' . $invID . '&action=reject&token=' . $token .'&approverID='.$approverID;;
            $approveGrnEmailView = new ViewModel(array(
                'name' => $approverData['firstName'] . ' ' . $approverData['lastName'],
                'invCode' => $invCode,
                'approveLink' => $approveUrl,
                'rejectLink' => $rejectUrl,
            ));
            $approveGrnEmailView->setTemplate('/core/email/inv-approve');
            $renderedEmailHtml = $this->getServiceLocator()
                    ->get('viewrenderer')
                    ->render($approveGrnEmailView);
            $documentType = 'Sales Invoice';
            $this->sendDocumentEmail($approverData['email'], "Request for Approve - Sales Invoice - $invCode", $renderedEmailHtml, $documentType, $invID, null, false, true);
        }
    }

    public function saveDraftInvoiceSubProductData($batchValue, $invoiceProductID)
    {
        
        if($batchValue['warranty'] == '' && $batchValue['serialID'] != null){      
            $serialDetails = $this->CommonTable('Inventory\Model\ProductSerialTable')
               ->getSerialProducts($batchValue['serialID']);
            $batchValue['warranty'] = $serialDetails->productSerialWarrantyPeriod;
        }
        $data = array(
            'salesInvocieProductID' => $invoiceProductID,
            'productBatchID' => $batchValue['batchID'],
            'productSerialID' => $batchValue['serialID'],
            'salesInvoiceSubProductQuantity' => $batchValue['qtyByBase'],
            'salesInvoiceSubProductWarranty' => $batchValue['warranty'],
            'salesInvoiceSubProductWarrantyType' => (int)$batchValue['warrantyType'],
        );


        $invoiceSubProduct = new DraftInvSubProduct;
        $invoiceSubProduct->exchangeArray($data);
       
        $invoiceSubProductID = $this->CommonTable('Invoice\Model\DraftInvSubProductTable')->saveInvoiceSubProduct($invoiceSubProduct);

        return $invoiceSubProductID;
    }


    public function approveAction()
    {
        $action = $_GET['action'];
        $invID = $_GET['invID'];
        $token = $_GET['token'];
        $approverID = $_GET['approverID'];
        $invDetails = $this->CommonTable("Invoice\Model\DraftInvWfTable")->getInvoiceByInvoiceID($invID)->current();
        $invDetails = (array) $invDetails;
        $invCode = $invDetails['salesInvoiceCode'];

        if ($invDetails['deleted'] == 1) {
            echo "This link and this link related draft invoice $invCode is no more valid...!";
            exit();
        }



        $user = $invDetails['createdBy'];
        $this->beginTransaction();
        if ($invDetails['invHashValue'] == $token) {
            if ($action == 'approve') {
                if ($invDetails['isInvApproved'] == 0 && $invDetails['statusID'] == 13) {
                    $this->commit();
                    echo "Invoice $invCode is already rejected, you cannot approve any more..!";
                    exit();
                }

                if ($invDetails['isInvApproved'] == 0) {

                    $data = json_decode($invDetails['actInvData']);
                    $dataSet = (array) $data;
                    $dataSet['products'] = (array) $dataSet['products'];

                    // $locationID = $this->user_session->userActiveLocation['locationID'];
                    $customerID = (!empty($dataSet['customer'])) ? $dataSet['customer'] : NULL;

                    if (!$customerID) {
                        $this->beginTransaction();
                        //default customer ID 0 is sent to the database
                        $refData = $this->getReferenceNoForLocation(3, $dataSet['locationOutID']);
                        $rid = $refData["refNo"];

                        $dataSet['invoiceCode'] = $rid;


                        $res = $this->converDraftToInvoice($dataSet, 0, false, false, true, $user);
                        if ($res['status']) {
                            $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID(0);
                            $custNewCurrentBalance = $cust->customerCurrentBalance + $res['totalPrice'];
                            $custData = array(
                                'customerCurrentBalance' => $custNewCurrentBalance,
                                'customerID' => '0'
                            );
                            $customerAllData = new Customer;
                            $customerAllData->exchangeArray($custData);

                            $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);

                            //update draft inv status
                            $dataSet = [
                                'isInvApproved' => 1,
                                'approvedBy' => $approverID,
                                'approvedAt' => date('Y-m-d h:i:s'),
                                'statusID' => 16
                            ];
                            $this->CommonTable('Invoice\Model\draftInvWfTable')->updateInvoice($dataSet, $invID);

                            //update act inv workflow state
                            $dataParam = [
                                'isApprovedThroughWF' => 1
                            ];
                            $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoice($dataParam, $res['data']['salesInvoiceID']);


                            $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($invID, 47);



                            if ($attachmentData) {

                                foreach ($attachmentData as $key => $value) {
                                    $dat = array(
                                        'documentID' => $res['data']['salesInvoiceID'],
                                        'documentTypeID' => 1,
                                        'documentRealName' => $value['documentRealName'],
                                        'documentName' => $value['documentName'],
                                    );
                                    $docFile = new DocumentAttachementMapping();
                                    $docFile->exchangeArray($dat);
                                    $insertedID = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->saveUploadFileMapData($docFile);
                                    if(!$insertedID) {
                                        $this->rollback();
                                        echo "Error occured while Save attachments";
                                        exit();
                                    }
                                }



                            }

                            $this->commit();
                            echo "Sales Invoice Approved..!";
                            exit();
                        } else {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $res['msg'];

                            echo "Error occured while convert draft invoice to original invoice the error is ". $res['msg'];
                            exit();
                            
                        }
                    } else {
                    
                        $customerName = $dataSet['customerName'];
                        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
                        if ($cust->customerName . '-' . $cust->customerCode == $customerName) {

                            $this->beginTransaction();

                            $refData = $this->getReferenceNoForLocation(3, $dataSet['locationOutID']);
                            $rid = $refData["refNo"];

                            $dataSet['invoiceCode'] = $rid;


                            $res = $this->converDraftToInvoice($dataSet, $customerID, false, false, true, $user);
                            if ($res['status']) {

                                $custNewCurrentBalance = $cust->customerCurrentBalance + $res['totalPrice'];
                                $custData = array(
                                    'customerCurrentBalance' => $custNewCurrentBalance,
                                    'customerID' => $customerID
                                );
                                $customerAllData = new Customer;
                                $customerAllData->exchangeArray($custData);

                                $this->CommonTable('Invoice\Model\CustomerTable')->updateCustomerBalance($customerAllData);

                                //update draft inv status
                                $dataSet = [
                                    'isInvApproved' => 1,
                                    'approvedBy' => $approverID,
                                    'approvedAt' => date('Y-m-d h:i:s'),
                                    'statusID' => 16
                                ];
                                $this->CommonTable('Invoice\Model\draftInvWfTable')->updateInvoice($dataSet, $invID);

                                //update act inv workflow state
                                $dataParam = [
                                    'isApprovedThroughWF' => 1
                                ];
                                $this->CommonTable('Invoice\Model\InvoiceTable')->updateInvoice($dataParam, $res['data']['salesInvoiceID']);

                                $this->commit();
                                echo "Sales Invoice Approved..!";
                                exit();
                            } else {
                                $this->rollback();
                                echo "Error occured while convert draft invoice to original invoice the error is ". $res['msg'];
                                exit();
                            }
                        } else {

                            $this->rollback();
                            echo "Error occured while convert draft invoice to original invoice the error is ". $this->getMessage('ERR_INVOICE_INVALID_CUST');
                            exit();
                        }
                    }
                    
                } else {
                    $this->commit();
                    echo "Sales Invoice $invCode is already approved..!";
                    exit();
                }
            } else {
                if ($invDetails['isInvApproved'] == 1) {
                    $this->commit();
                    echo "Sales Invoice $invCode is already approved, you cannot reject any more..!";
                    exit();
                }

                if ($invDetails['isInvApproved'] == 0 && $invDetails['status'] == 13) {
                    $this->commit();
                    echo "Sales Invoice $invCode is already rejected..!";
                    exit();
                } else {
                    $draftData = [
                        'isInvApproved' => 0,
                        'statusID' => 13,
                        'rejectedBy' => $approverID,
                        'rejectedAt' => date('Y-m-d h:i:s')
                    ];

                    $draftRes = $this->CommonTable('Invoice\Model\DraftInvWfTable')->updateInvoice($draftData, $invID);

                    if (!$draftRes){
                        $this->rollback();
                        echo $this->getMessage('ERR_INV_DRAFT_TO_INV');
                        exit();
                    } else{
                        $this->commit();
                        echo "Sales Invoice $invCode rejected..!";
                        exit();
                    }
                }
            }
        } else {
            $this->rollback();
            echo 'Token Mismatch';
            exit();
        }
    }

    /**
     * this function return details of invoice edit
     */
    public function getDraftInvoiceEditDetailsAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $invoiceID = $request->getPost('invoiceID');
            $invoiceProductsArray = $this->CommonTable('Invoice\Model\DraftInvProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
            $dparray;
            $dparrayQty;
            $iparray;
            $iparrayQty;
            $realproduct;
            $checkProduct = 0;
            $dproductType;
            $locationID = $this->user_session->userActiveLocation['locationID'];

            foreach ($invoiceProductsArray as $dp) {
                $dp = (object) $dp;
                $dparray[$dp->locationProductID] = $dp->productID;
                $dparrayQty[$dp->locationProductID] = $dp->salesInvoiceProductQuantity;
                $dproductType[$dp->locationProductID] = $dp->productTypeID;
            }

            $data = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductsByInvoiceID($invoiceID);
            foreach ($data as $ip) {
                if($ip->locationID == $locationID){
                    $ip = (object) $ip;
                    $iparray[$ip->productID] = $ip->productID;
                    $iparrayQty[$ip->productID]+=$ip->creditNoteProductQuantity;
                }
            }

            foreach ($dparrayQty as $key => $Pdata) {
                if ($iparrayQty[$key] != $Pdata || ($Pdata == NULL && !array_key_exists($key, $iparray))) {
                    $checkProduct = 1;
                    $realproduct[$key] = $key;
                }
            }

            if ($checkProduct) {
                $invoice = $this->CommonTable('Invoice\Model\DraftInvWfTable')->getInvoiceByID($invoiceID);
                $invoiceProduct = $this->CommonTable('Invoice\Model\DraftInvProductTable')->getInvoiceProductByInvoiceIDForCreditNote($invoiceID);
                $tax = array(
                    'invoiceTaxID' => '',
                    'invoiceTaxName' => '',
                    'invoiceTaxPrecentage' => '',
                    'invoiceTaxAmount' => '',
                );
                $subProduct = array(
                    'invoiceSubProductID' => '',
                    'productBatchID' => '',
                    'productSerialID' => '',
                    'invoiceSubProductQuantity' => '',
                );

                $Products = array(
                    'invoiceProductID' => '',
                    'invoiceID' => '',
                    'productID' => '',
                    'invoiceProductPrice' => '',
                    'inclusiveTaxInvoiceProductPrice' => '',
                    'invoiceProductDiscount' => '',
                    'invoiceProductDiscountType' => '',
                    'invoiceProductQuantity' => '',
                    'productCode' => '',
                    'productName' => '',
                    'productType' => '',
                    'tax' => '',
                    'subProduct' => '',
                    'itemSalesPersonID' => ''
                );

                $incID = 1;
                $incFlag = FALSE;

                $addedFreeIssueItems = [];
                $incItems = [];
                while ($tt = $invoiceProduct->current()) {
                    $t = (object) $tt;

                    if ($realproduct[$t->locationProductID] == $t->locationProductID) {
                        $creditNoteProductData = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getCreditNoteProductAndSubProductDataByInvoiceProductID($t->draftInvProductID);
                        $creditNoteProductQty = 0;
                        $creditNoteSubProductSerial = array();
                        $creditNoteSubProductBatch = array();
                        $creditNoteProductIDArray = array();

                        foreach ($creditNoteProductData as $prValue) {
                            $prValue = (object) $prValue;
                            if ($prValue->creditNoteProductID) {
                                //use this if statement for ignore repeating creditNoteProducts
                                if (!$creditNoteProductIDArray[$prValue->creditNoteProductID] == 1) {
                                    $creditNoteProductQty+=$prValue->creditNoteProductQuantity;
                                    $creditNoteProductIDArray[$prValue->creditNoteProductID] = 1;
                                }
                                if ($prValue->productSerialID) {
                                    $creditNoteSubProductSerial[$prValue->productSerialID] += $prValue->creditNoteSubProductQuantity;
                                } else if ($prValue->productBatchID) {
                                    $creditNoteSubProductBatch[$prValue->productBatchID] += $prValue->creditNoteSubProductQuantity;
                                }
                            }
                        }
                        $salesInvoiceProduct[$t->productID . '_' . $incID] = (object) $Products;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductID = $t->draftInvProductID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceID = $t->salesInvoiceID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productID = $t->productID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->locationProductID = $t->locationProductID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->documentTypeID = $t->documentTypeID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductDocumentID = $t->salesInvoiceProductDocumentID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductPrice = $t->salesInvoiceProductPrice;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->inclusiveTaxInvoiceProductPrice = $t->inclusiveTaxSalesInvoiceProductPrice;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductDiscount = $t->salesInvoiceProductDiscount;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductDiscountType = $t->salesInvoiceProductDiscountType;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->invoiceProductQuantity = $t->salesInvoiceProductQuantity - $creditNoteProductQty;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productCode = $t->productCode;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productName = $t->productName;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productType = $t->productTypeID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->productDescription = $t->salesInvoiceProductDescription;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->itemSalesPersonID = $t->itemSalesPersonID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductMrpType = $t->salesInvoiceProductMrpType;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductMrpValue = $t->salesInvoiceProductMrpValue;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductMrpPercentage = $t->salesInvoiceProductMrpPercentage;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->salesInvoiceProductMrpAmount = $t->salesInvoiceProductMrpAmount;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->isFreeItem = ($t->isFreeItem == 1) ? true : false;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->hasFreeItem = ($t->hasFreeItem == 1) ? true : false;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->incID = $incID;
                        $mainProductID = null;
                        $mainIvoiceProductID = null;
                        if ($t->isFreeItem == 1) {
                            $freeItemData = $this->CommonTable('Invoice\Model\InvoiceFreeProductTable')->getRelatedFreeProductsByInvoiceAndFreeProductID($t->salesInvoiceID, $t->productID)->current();

                            if ($freeItemData) {
                                $mainProductID = $freeItemData['mainProductID'];
                                $mainIvoiceProductID = $freeItemData['mainSalesInvoiceProductID'];
                            }

                        }
                            
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->mainProductID = $mainProductID;
                        $salesInvoiceProduct[$t->productID . '_' . $incID]->mainIvoiceProductID = $mainIvoiceProductID;

                        $subIncFlag = $incID - 1;
                        if ($t->draftInvSubProductID != '') {
                            $creditNoteQuantity = 0;
                            if ($creditNoteSubProductSerial[$t->productSerialID]) {
                                $creditNoteQuantity = $creditNoteSubProductSerial[$t->productSerialID];
                            } else if ($creditNoteSubProductBatch[$t->productBatchID]) {
                                $creditNoteQuantity = $creditNoteSubProductBatch[$t->productBatchID];
                            }
                            if ( $t->productSerialID != ''){
                                $serialProduct = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($t->productSerialID);
                            }

                            if ( $t->productBatchID != ''){
                                $bathcProducts = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($t->productBatchID);
                            }
                            $subIncFlag = $incID - 1;
                                // var_dump('vvvvvvvvvv').die();
                            if (!empty($salesInvoiceProduct[$t->productID . '_' . $subIncFlag]->subProduct) && $salesInvoiceProduct[$t->productID . '_' . $subIncFlag]->invoiceProductID == $t->draftInvProductID) {
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->draftInvSubProductID] = (object) $subProduct;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->draftInvSubProductID]->invoiceSubProductID = $t->draftInvSubProductID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->draftInvSubProductID]->productBatchID = $t->productBatchID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->draftInvSubProductID]->productBatchCode = ($bathcProducts->productBatchCode) ? $bathcProducts->productBatchCode : null;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->draftInvSubProductID]->productSerialID = $t->productSerialID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->draftInvSubProductID]->productSerialCode = ($serialProduct->productSerialCode) ? $serialProduct->productSerialCode : null;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->draftInvSubProductID]->salesInvoiceSubProductWarranty = $t->salesInvoiceSubProductWarranty;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->draftInvSubProductID]->salesInvoiceSubProductWarrantyType = $t->salesInvoiceSubProductWarrantyType;
                                $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag][$t->draftInvSubProductID]->invoiceSubProductQuantity = $t->salesInvoiceSubProductQuantity - $creditNoteQuantity;
                                $salesInvoiceProduct[$t->productID . '_' . $subIncFlag]->subProduct = $salesInvoiceSubProduct[$t->productID . '_' . $subIncFlag];
                                unset($salesInvoiceProduct[$t->productID . '_' . $incID]);
                                --$incID;
                                $incFlag = TRUE;
                            } else {
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID] = (object) $subProduct;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->invoiceSubProductID = $t->draftInvSubProductID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->productBatchID = $t->productBatchID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->productBatchCode = ($bathcProducts->productBatchCode) ? $bathcProducts->productBatchCode : null;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->productSerialID = $t->productSerialID;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->productSerialCode = ($serialProduct->productSerialCode) ? $serialProduct->productSerialCode : null;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->salesInvoiceSubProductWarranty = $t->salesInvoiceSubProductWarranty;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->salesInvoiceSubProductWarrantyType = $t->salesInvoiceSubProductWarrantyType;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->invoiceSubProductQuantity = $t->salesInvoiceSubProductQuantity - $creditNoteQuantity;
                                $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->copyFromDeliveryNote = false;


                                if (!empty($t->documentTypeID) && $t->documentTypeID == 4 && $t->batchProduct) {
                                    $batchItemData = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteBatchProductByDeliveryNoteId($t->salesInvoiceProductDocumentID);
                                    foreach ($batchItemData as $key => $value) {
                                        if ($value['productBatchID'] == $t->productBatchID) {
                                            $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->copyFromDeliveryNote = true;
                                            $salesInvoiceSubProduct[$t->productID . '_' . $incID][$t->draftInvSubProductID]->RemainDeliBtchQty = floatval($value['deliveryProductSubQuantity']) - floatval($value['deliveryNoteSubProductsCopiedQuantity']);
                                        }
                                    }
                                }
                                $salesInvoiceProduct[$t->productID . '_' . $incID]->subProduct = $salesInvoiceSubProduct[$t->productID . '_' . $incID];

                            }
                        }

                        if ($t->taxID) {
                            $taxes[$t->draftInvProductID][$t->taxID] = (object) $tax;
                            $taxes[$t->draftInvProductID][$t->taxID]->invoiceTaxID = $t->taxID;
                            $taxes[$t->draftInvProductID][$t->taxID]->invoiceTaxName = $t->taxName;
                            $taxes[$t->draftInvProductID][$t->taxID]->invoiceTaxPrecentage = $t->salesInvoiceProductTaxPrecentage;
                            $taxes[$t->draftInvProductID][$t->taxID]->invoiceTaxAmount = $t->salesInvoiceProductTaxAmount;

                            $subTaxIncFlag = $incID - 1;

                            if (!empty($salesInvoiceProduct[$t->productID . '_' . $subTaxIncFlag]->tax) && $salesInvoiceProduct[$t->productID . '_' . $subTaxIncFlag]->invoiceProductID == $t->draftInvProductID) {
                                $salesInvoiceProduct[$t->productID . '_' . $subTaxIncFlag]->tax = $taxes[$t->draftInvProductID];
                                unset($salesInvoiceProduct[$t->productID . '_' . $incID]);
                                //If there has sub products so dont need to reduce $incID
                                //because if it has a sub product then it already has reduce 1 that previous condition
                                if (!$incFlag) {
                                    --$incID;
                                }
                            } else {

                                $salesInvoiceProduct[$t->productID . '_' . $incID]->tax = $taxes[$t->draftInvProductID];
                            }
                        }
                    }
                    $locationID = $this->user_session->userActiveLocation['locationID'];

                    $products[$t->productID] = $this->getLocationProductDetailsForReturn($locationID, $t->productID);
                    $v = $t->productID.'_'.$t->draftInvProductID;
                    $incItems[$v] = $incID;
                    $incID++;
                }

                foreach ($salesInvoiceProduct as $key1 => $value1) {
                   
                    if ($value1->hasFreeItem == 1) {
                        $freeItemData = $this->CommonTable('Invoice\Model\DraftInvFreeIssueItemsTable')->getRelatedFreeProductsByInvoiceAndProductID($value1->invoiceID, $value1->productID);

                        foreach ($freeItemData as $key => $value) {
                            $addedFreeIssueItems[$key1][$value['freeProductID']] = $value;
                        }
                    }
                }

                $customerID = ($invoice->customerID) ? $invoice->customerID : 0;
                $customer = $this->getCustomerDetailsByCustomerID($customerID);
                $promotionID = ($invoice->promotionID) ? $invoice->promotionID : 0;
                $promotion = $this->getPromotionDetailsForEdit($promotionID)[$promotionID];
                $currencySymbol = ($invoice->customCurrencyId) ? $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($invoice->customCurrencyId)->currencySymbol : $this->companyCurrencySymbol;

                $locationProducts = Array();
                $inactiveItems = Array();
                foreach ($salesInvoiceProduct as $invProducts) {
                    $locationProducts[$invProducts->productID] = $this->getLocationProductDetails($locationID, $invProducts->productID);
                    if ($locationProducts[$invProducts->productID] == null) {
                        $inactiveItem = $inactiveItemDetails = $this->CommonTable('Inventory\Model\ProductTable')->getProductDetailsByProductID($invProducts->productID);
                        $inactiveItems[] = $inactiveItem['productCode'].' - '.$inactiveItem['productName'];
                    }


                    if ($invProducts->documentTypeID == 4) {
                        $docData = $this->CommonTable("Invoice\Model\DeliveryNoteProductTable")->getProductByDeliveryNoteIDAndProductId($invProducts->salesInvoiceProductDocumentID,$invProducts->productID);

                        $invProducts->deliveryNoteProductQuantity = ((float)$docData->deliveryNoteProductQuantity - (float)$docData->deliveryNoteProductCopiedQuantity) + (float)$invProducts->invoiceProductQuantity;
                    }
                }
                $inactiveItemFlag = false;
                $errorMsg = null;
                if (!empty($inactiveItems)) {
                    $inactiveItemFlag = true;
                    $errorItems = implode(",",$inactiveItems);        
                    $errorMsg = $this->getMessage('ERR_INV_ITM_INACTIVE', [$errorItems]);
                }

                $dimensionData = [];
                $jEDimensionData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataWithDimensionsByDocumentTypeIDAndDocumentID(1,$invoiceID);
                foreach ($jEDimensionData as $value) {
                    if (!is_null($value['journalEntryID'])) {
                        $temp = [];
                        if($value['dimensionType'] == "job" || $value['dimensionType'] == "project") {
                            if ($value['dimensionType'] == "job") {
                                $jobData = $this->CommonTable('Jobs\Model\JobTable')->getJobsByJobID($value['dimensionValueID']);
                                $valueTxt = $jobData->jobReferenceNumber.'-'.$jobData->jobName;
                                $dimensionTxt = "Job";
                            } else if ($value['dimensionType'] == "project") {
                                $proData = $this->CommonTable('Jobs\Model\ProjectTable')->getProjectByProjectId($value['dimensionValueID']);
                                $valueTxt = $proData->projectCode.'-'.$proData->projectName;
                                $dimensionTxt = "Project";
                            }

                            $temp['dimensionTypeId'] = $value['dimensionType'];
                            $temp['dimensionTypeTxt'] = $dimensionTxt;
                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                            $temp['dimensionValueTxt'] = $valueTxt;
                        } else {
                            $temp['dimensionTypeId'] = $value['dimensionType'];
                            $temp['dimensionTypeTxt'] = $value['dimensionName'];
                            $temp['dimensionValueId'] = $value['dimensionValueID'];
                            $temp['dimensionValueTxt'] = $value['dimensionValue'];
                        }
                        $dimensionData[$invoice->salesInvoiceCode][$value['dimensionType']] = $temp;
                    }
                }

                $membershipData = [];
                $membershipRes = $this->CommonTable('Invoice\Model\InvoiceTable')->getMembershipByInvoiceId($invoiceID)->current();
                if ($membershipRes) {
                    $membershipData['nextInvoiceDate'] = $membershipRes['nextInvoiceDate'];
                    $membershipData['membershipComment'] = $membershipRes['membershipComment'];
                }

                $relateServiceChargeArr = [];
                if (!empty($invoice->salesInvoiceServiceChargeAmount)) {
                    $relatedServiceCharges = $this->CommonTable('Invoice\Model\InvoiceServiceChargeTable')->getInvoiceChargesByInvoiceID($invoiceID);

                    foreach ($relatedServiceCharges as $key2 => $value2) {
                        $relateServiceChargeArr[$value2['serviceChargeTypeID']] = $value2; 
                    }
                       
                } 

                $uploadedAttachments = [];
                $attachmentData = $this->CommonTable('Core\Model\DocumentAttachementMappingTable')->getDocumentRelatedAttachmentsByDocumentIDAndDocumentTypeID($invoiceID, 47);
                $companyName = $this->getSubdomain();
                $path = '/userfiles/' . md5($companyName) . "/attachments";

                $relatedSalesPersons = $this->CommonTable("Invoice\Model\InvoiceSalesPersonsTable")->getSalesPersonByInvoiceID($invoiceID);

                $relatedSalesPersons = (!empty($relatedSalesPersons)) ? $relatedSalesPersons : [];

                foreach ($attachmentData as $value) {
                    $temp = [];
                    $temp['documentAttachemntMapID'] = $value['documentAttachemntMapID'];
                    $temp['documentRealName'] = $value['documentRealName'];
                    $temp['docLink'] = $path . "/" .$value['documentName'];
                    $uploadedAttachments[$value['documentAttachemntMapID']] = $temp;
                }

                $this->status = true;
                $this->data = array(
                    'status' => true,
                    'invoice' => $invoice,
                    'relatedSalesPersons'=> $relatedSalesPersons,
                    'relateServiceChargeArr' => $relateServiceChargeArr,
                    'invoiceProduct' => $salesInvoiceProduct,
                    'products' => $products,
                    'customer' => $customer,
                    'promotion' => $promotion,
                    'currencySymbol' => $currencySymbol,
                    'inactiveItemFlag' => $inactiveItemFlag,
                    'errorMsg' => $errorMsg,
                    'dimensionData' => $dimensionData,
                    'membershipData' => $membershipData,
                    'uploadedAttachments' => $uploadedAttachments,
                    'addedFreeIssueItems' => $addedFreeIssueItems,
                    'incItems' => $incItems
                );
            } else {
                $this->status = false;
                $this->msg = $this->getMessage('ERR_INVOICE_USED_INV_FOR_CN');
            }
            return $this->JSONRespond();
        }
    }

    public function saveInvDraft($param, $customerID = 0, $pos = false, $edit = false, $restFlag = false, $posUserID) {
        $actInvData = json_encode($param);
        $customCurrencyId = $param['customCurrencyId'];


//get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "") ? $customCurrencyData->currencyRate : 1;
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $ccrate = $param['customCurrencyRate'];
//if custom currency rate alredy set add that rate to sales Order
        if (!($ccrate == 0 || $ccrate == 1)) {
            $customCurrencyRate = $ccrate;
        }

        if ($restFlag == true && $customerID == NULL) {
            $customerID = 0;
        }

        $products = $param['products'];
        $invoiceCode = $param['invoiceCode'];
        $locationOut = $param['locationOutID'];
        $invoiceDate = $this->convertDateToStandardFormat($param['invoiceDate']);
        $dueDate = $this->convertDateToStandardFormat($param['dueDate']);
        $deliveryCharge = $param['deliveryCharge'] * $customCurrencyRate;
        $totalPrice = trim($param['invoiceTotalPrice']) * $customCurrencyRate;
        $finalTotalWithInclusiveTax = trim($param['finalTotalWithInclusiveTax']) * $customCurrencyRate;
        $comment = $param['invoiceComment'];
        $paymentTermID = $param['paymentTermID'];
        $showTax = $param['showTax'];
        $deliveryNoteID = $param['deliveryNoteID'];
        $salesOrderID = $param['salesOrderID'];
        $quotationID = $param['quotationID'];
        $activityID = $param['activityID'];
        $jobID = $param['jobID'];
        $projectID = $param['projectID'];
        $salesPersonIDArr = $param['salesPersonID'];
        $invoiceTotalDiscount = $param['invoiceTotalDiscount'] * $customCurrencyRate;
        $salesInvoicePayedAmount = $param['salesInvoicePayedAmount'] * $customCurrencyRate;
        $suspendedTax = $param['suspendedTax'];
        $deliveryAddress = $param['deliveryAddress'];
        $promotionID = $param['promotionID'];
        $promotionDiscountValue = $param['promotionDiscountValue'];
        $totalInvoiceDiscountValue = $param['invoiceDiscountRate'];
        $invoiceDiscoutType = $param['invoiceDiscountType'];
        $totalTax = $param['totalTaxValue'] * $customCurrencyRate;
        $invoiceWisePromotionDiscountType = $param['invoiceWisePromotionDiscountType'];
        $salesInvoiceDeliveryChargeEnable = $param['salesInvoiceDeliveryChargeEnable'];
        $priceListId = $param['priceListId'];
        $customerProfID = $param['customerProfID'];
        $invTempCode = $param['invTempCode'];
        $journalEntryData = $param['journalEntryData'];
        $inclusiveTax = (int)filter_var($param['inclusiveTax'], FILTER_VALIDATE_BOOLEAN);
        $dimensionData = $param['dimensionData'];
        $ignoreBudgetLimit = $param['ignoreBudgetLimit'];
        $nextInvoiceDate = ($param['nextInvoiceDate'] == "") ? null : $param['nextInvoiceDate'];
        $nextInvoiceDateComment = $param['nextInvoiceDateComment'];
        $isSetJournalEntry = false;
        $serviceChargeAmount = $param['serviceChargeAmount'];
        $addedServiceChargeArray = $param['addedServiceChargeArray'];
        $addedFreeIssueItems = (!empty($param['addedFreeIssueItems'])) ? $param['addedFreeIssueItems'] : [];
        $wfID = $param['wfID'];
        $linkedCusOrder = (!empty($param['linkedCusOrder'])) ? $param['linkedCusOrder'] : null;

        if (!$pos) {
            $showTax = 1;
        }

        if(count($journalEntryData) > 0){
            $isSetJournalEntry = true;
        }

        if (!is_null($deliveryNoteID)) {
            $deliveryNoteStatus = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteStatusByDeliveryNoteID($deliveryNoteID)->current();
            if ($deliveryNoteStatus['deliveryNoteStatus'] == "10") {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_DLN'),
                );
            }

            if ($deliveryNoteStatus['deliveryNoteStatus'] == "4") {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_CLOSE_DLN'),
                );
            }
        }

        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        $customerCurrentOutstanding = $cust->customerCurrentBalance;

        $promotionData = $this->getPromotionDetails($promotionID);

        $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
        $displayData = $result->current();
        $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
        $averageCostingFlag = $displayData['averageCostingFlag'];

        $accountProduct = array();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountsData = $glAccountExist->current();
        $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

       
        $custSalesAccountIsset = true;
        if ($restFlag) {
            $this->useAccounting = 1;
        }
        if($this->useAccounting == 1 && !$isSetJournalEntry){
            if(empty($customerData->customerSalesAccountID)){
                $custSalesAccountIsset = false;
                // return array(
                //     'status' => false,
                //     'msg' => $this->getMessage('ERR_CUSTOMER_SALES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                // );
            }
            if(empty($customerData->customerReceviableAccountID)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                );
            }
            if($deliveryCharge > 0 && empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_GL_DELIVERY_CHARGES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                );
            }
        }

        if(!$customerProfID){
            $customerDefaultProfile = $this->CommonTable('Invoice\Model\CustomerProfileTable')
                            ->getDefaultProfileByCustomerID($customerID);
            foreach ($customerDefaultProfile as $key => $value) {
                $customerProfID = $value['customerProfileID'];
            }
        }

        if(!$pos){
            $itemAttrDetailsSet = [];
            foreach ($products as $key => $p) {
                $productID = $p['productID'];
                $itemAttributeMap = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->getDetailsByProductID($productID);
                foreach ($itemAttributeMap as $key => $value) {
                    if($value['itemAttributeID'] == 1){
                        $itemAttrDetailsSet[$productID][] = $value['itemAttributeValueID'];
                    }
                }
            }
                //check the same credit period is exist for all items
            if(count($itemAttrDetailsSet) > 1){
                foreach ($itemAttrDetailsSet as $ckey => $cvalue) {
                    if(isset($oldVal)){
                        $newVal = $cvalue;
                        $oldVal = array_intersect($oldVal, $newVal);
                        if(!(count($oldVal) > 0)){
                            return array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_INVOICE_API_ITEMATTRIBUTE_CREDIT_PERIOD')
                            );
                            break;
                        }
                    }else{
                        $oldVal = $cvalue;
                    }
                }
            }
        }

        //check whether SalesInvoice is created via a SalesOrder, DeliveryNote or activity
        if (!empty($salesOrderID) || !empty($deliveryNoteID) || !empty($activityID) || !empty($jobID) || !empty($projectID) || !empty($quotationID)) {
            $fromSalesOrderOrDeliveryNote = TRUE;
        } else {
            $fromSalesOrderOrDeliveryNote = FALSE;
        }

        $result = $this->getReferenceNoForLocation('48', $locationOut);
        $locationReferenceID = $result['locRefID'];

        $locationID = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(48, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

        $invoiceCode = $rid;


        if ($deliveryCharge != '') {
            $realItemTotalPrice = $totalPrice - $deliveryCharge;
        } else {
            $realItemTotalPrice = $totalPrice;
        }


        if ($serviceChargeAmount != 0 || $serviceChargeAmount != '') {
            $realItemTotalPrice = $realItemTotalPrice - $serviceChargeAmount;
        }


        if ($suspendedTax) {
            $realItemTotalPrice = $realItemTotalPrice + $totalTax;
        }
        if ($invoiceDiscoutType == 'presentage') {
            $totalInvoiceDiscount = ($totalInvoiceDiscountValue / (100 - $totalInvoiceDiscountValue)) * ($realItemTotalPrice - $totalTax);
        } else {
            $totalInvoiceDiscount = $totalInvoiceDiscountValue * $customCurrencyRate;
        }

        if ($pos) {
            $totalInvoiceDiscount = $param['invoiceTotalDiscount'] * $customCurrencyRate;
            $totalPromotionDiscountValue = $promotionDiscountValue * $customCurrencyRate;
        }

        if ($invoiceWisePromotionDiscountType == '2') {
            $realItemTotalPrice = ($realItemTotalPrice - ($totalTax));
            $totalPromotionDiscountValue = (($realItemTotalPrice * 100) / (100 - $promotionDiscountValue)) - $realItemTotalPrice;
        } else if ($invoiceWisePromotionDiscountType == '1') {
            $totalPromotionDiscountValue = $promotionDiscountValue * $customCurrencyRate;
        }

        if ($restFlag) {
            $entityID = $this->createEntityForRestApi($posUserID);
        } else {
            $entityID = $this->createEntity();
        }

        

        $hashValue = md5($invoiceCode.'-'.$actInvData);


        $statusName = 'Open';
        $statusID = $this->getStatusIDByStatusName($statusName);
        $invoiceData = array(
            'salesInvoiceCode' => $invoiceCode,
            'customerID' => $customerID,
            'locationID' => $locationOut,
            'salesInvoiceIssuedDate' => $invoiceDate,
            'salesInvoiceOverDueDate' => $dueDate,
            'salesinvoiceTotalAmount' => $totalPrice,
            'salesInvoiceComment' => $comment,
            'salesInvoiceDeliveryCharge' => $deliveryCharge,
            'paymentTermID' => $paymentTermID,
            'quotationID' => $quotationID,
            //'deliveryNoteID' => $deliveryNoteID,
            'salesOrderID' => $salesOrderID,
            'activityID' => $activityID,
            'jobID' => $jobID,
            'projectID' => $projectID,
            'salesInvoiceShowTax' => $showTax,
            'statusID' => $statusID,
            'entityID' => $entityID,
            'pos' => $pos,
            'salesPersonID' => null,
            'salesInvoicePayedAmount' => $salesInvoicePayedAmount,
            'salesInvoiceTotalDiscount' => $invoiceTotalDiscount,
            'salesInvoiceSuspendedTax' => $suspendedTax,
            'salesInvoiceDeliveryAddress' => $deliveryAddress,
            'promotionID' => $promotionID,
            'salesInvoicePromotionDiscount' => $totalPromotionDiscountValue,
            'salesInvoiceWiseTotalDiscount' => $totalInvoiceDiscount,
            'salesInvoiceTotalDiscountType' => $invoiceDiscoutType,
            'salesInvoiceDiscountRate' => $totalInvoiceDiscountValue,
            'customCurrencyId' => $customCurrencyId,
            'salesInvoiceCustomCurrencyRate' => $customCurrencyRate,
            'priceListId' => $priceListId,
            'salesInvoiceDeliveryChargeEnable' => $salesInvoiceDeliveryChargeEnable,
            'customerProfileID' => $customerProfID,
            'salesInvoiceTemporaryCode' => $invTempCode,
            'offlineSavedTime' => $param['offlineSavedTime'],
            'offlineUser' => $param['offlineUser'],
            'discountOnlyOnEligible' => $param['discountOnlyOnEligible'],
            'customerCurrentOutstanding' => $customerCurrentOutstanding,
            'inclusiveTax' => $inclusiveTax,
            'templateInvoiceTotal' => $finalTotalWithInclusiveTax,
            'salesInvoiceServiceChargeAmount' => $serviceChargeAmount,
            'invHashValue' => $hashValue,
            'actInvData' => $actInvData,
            'isInvApproved' => 0,
            'selectedWfID' => $wfID,
            'linkedCusOrderNum' => $linkedCusOrder
        );

        $invoiceExData = new DraftInvWf;
        $invoiceExData->exchangeArray($invoiceData);
        $salesInvoiceID = $this->CommonTable('Invoice\Model\DraftInvWfTable')->saveInvoiceData($invoiceExData);
        //if tax suspended is true update tsinvoice reference number otherwise update invoice reference number
        if (!$edit) {
            
            if ($locationReferenceID) {
                $this->updateReferenceNumber($locationReferenceID);
            }
        }

        
        if (sizeof($addedServiceChargeArray) > 0) {
            foreach ($addedServiceChargeArray as $key1 => $value1) {
                $serviceChargeTypeData = $this->CommonTable('Invoice\Model\ServiceChargeTypeTable')->getServiceChargeTypeDetailsById($value1['serviceChargeTypeId'])->current();
                $serviceChargeData = [
                    'invoiceID' => $salesInvoiceID,
                    'serviceChargeTypeID' => $serviceChargeTypeData['serviceChargeTypeID'],
                    'serviceChargeAmount' => $value1['serviceChargeValueTxt'],
                ];

                $scData = new DraftInvServiceCharge;
                $scData->exchangeArray($serviceChargeData);

                $memID = $this->CommonTable('Invoice\Model\DraftInvServiceChargeTable')->save($scData);
            }
        }
                


        $totalCRAmounts = 0;
        unset($products['subProduct']);
        $totalProductAmounts = 0;
        $totalTaxAmount = 0;
        foreach ($products as $keyt => $pt) {
            if(isset($pt['pTax']['tTA'])){
                if($pos){
                    $ptwTax = ($pt['productTotal']);
                }else{
                    $ptwTax = ($pt['productTotal'] - $pt['pTax']['tTA']);
                }
                $totalTaxAmount += $pt['pTax']['tTA'];
            }else{
                $ptwTax = $pt['productTotal'];
            }
            $totalProductAmounts += $ptwTax;
        }

        if (!empty($salesPersonIDArr) && sizeof($salesPersonIDArr) > 0) {

            $valueForOneSalesPerson = $totalPrice / sizeof($salesPersonIDArr);

            foreach ($salesPersonIDArr as $key5 => $value5) {
                
                $salesPersonData = [
                    "invoiceID" => $salesInvoiceID,
                    "salesPersonID" => $value5,
                    "relatedSalesAmount" => $valueForOneSalesPerson
                ];


                $spData = new DraftInvSalesPersons;
                $spData->exchangeArray($salesPersonData);

                $spID = $this->CommonTable('Invoice\Model\DraftInvSalesPersonsTable')->saveSalesPerson($spData);
            }
        } 
        foreach ($products as $key => $p) {
            $batchProducts = [];
            if ($pos == false) {
                if (isset($p['selected_serials']) && isset($param['subProducts'][$key])) {
                    foreach ($p['selected_serials'] as $in => $srl) {
                        $a = array_search($srl, array_column($param['subProducts'][$key], 'serialID'));
                        $batchProducts[] = $param['subProducts'][$key][$a];
                    }
                } else {
                    $batchProducts = $param['subProducts'][$key];
                }
            } else {
                $batchProducts = $param['subProducts'][$p['productID']];
            }

            $displaySetupDetails = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();



            $hasFreeItem = false;

            if ($p['isFreeItem'] != 'true') {
                if (!empty($addedFreeIssueItems[$key])) {
                    $hasFreeItem = true;
                }
            }


            $productID = $p['productID'];
            $deliveryQty = $p['deliverQuantity']['qty'];
            $invoiceProductData = array(
                'salesInvoiceID' => $salesInvoiceID,
                'productID' => $productID,
                'locationProductID' => $p['locationProductID'],
                'salesInvoiceProductDescription' => $p['productDescription'],
                'salesInvoiceProductPrice' => $p['productPrice'] * $customCurrencyRate,
                'salesInvoiceProductDiscount' => ($p['productDiscountType'] == 'precentage') ? $p['productDiscount'] : $p['productDiscount'] * $customCurrencyRate,
                'salesInvoiceProductDiscountType' => $p['productDiscountType'],
                'salesInvoiceProductTotal' => $p['productTotal'] * $customCurrencyRate,
                'salesInvoiceProductQuantity' => $deliveryQty,
                'itemSalesPersonID' => (!empty($p['itemSalesPerson']) && $p['itemSalesPerson'] != 0 && $p['itemSalesPerson'] != null) ? $p['itemSalesPerson'] : null,
                'documentTypeID' => $p['documentTypeID'],
                'salesInvoiceProductDocumentID' => $p['documentID'],
                'salesInvoiceProductSelectedUomId' => $p['selectedUomId'],
                'inclusiveTaxSalesInvoiceProductPrice' => $p['inclusiveTaxProductPrice'] * $customCurrencyRate,
                'salesInvoiceProductMrpType' =>  ($displaySetupDetails['useMrpSettings'] == 1) ? $p['mrpType'] : null, 
                'salesInvoiceProductMrpValue' => ($displaySetupDetails['useMrpSettings'] == 1) ? $p['mrpValue'] : null,
                'salesInvoiceProductMrpPercentage' => ($displaySetupDetails['useMrpSettings'] == 1) ? $p['mrpPercentageValue'] : null,
                'salesInvoiceProductMrpAmount' => ($displaySetupDetails['useMrpSettings'] == 1) ? $p['actmrpValue']: null,
                'isFreeItem' => ($p['isFreeItem'] == 'true') ? true: false,
                'hasFreeItem' => $hasFreeItem
            );
            //calculate discount value
            $discountValue = 0;
            if ($p['productDiscountType'] == "precentage") {
                $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $customCurrencyRate;
            } else if ($p['productDiscountType'] == "value") {
                $discountValue = $p['productDiscount'] * $customCurrencyRate;
            }

            $invoiceProduct = new DraftInvProduct;
            $invoiceProduct->exchangeArray($invoiceProductData);
            $invoiceProductID = $this->CommonTable('Invoice\Model\DraftInvProductTable')->saveInvoiceProduct($invoiceProduct);

            if ($hasFreeItem) {
                $freeItems = $addedFreeIssueItems[$key];

                foreach ($freeItems as $key5 => $value5) {
                    if (!empty($value5['incVal'])) {
                        $k = $value5['productID'].'_'.$value5['incVal'];
                        $relatedItem = $products[$k];

                        $freeItem = [
                            'invoiceID' => $salesInvoiceID,
                            'mainProductID' => $productID,
                            'mainSalesInvoiceProductID' => $invoiceProductID,
                            'freeProductID' => $value5['productID'] ,
                            'freeQuantity' => $relatedItem['deliverQuantity']['qty'],
                            'salesInvoiceFreeItemSelectedUom' => $relatedItem['selectedUomId'],
                            'locationID' => $relatedItem['locationID'],

                        ];
                    } else {
                        $freeItem = [
                            'invoiceID' => $salesInvoiceID,
                            'mainProductID' => $productID,
                            'mainSalesInvoiceProductID' => $invoiceProductID,
                            'freeProductID' => $value5['freeProductID'] ,
                            'freeQuantity' => $value5['freeQuantity'],
                            'salesInvoiceFreeItemSelectedUom' => $value5['salesInvoiceFreeItemSelectedUom'],
                            'locationID' => $value5['locationID'],
                        ];
                    }

                    $freeItemProduct = new DraftInvFreeIssueItems;
                    $freeItemProduct->exchangeArray($freeItem);

                    $freeItemProductID = $this->CommonTable('Invoice\Model\DraftInvFreeIssueItemsTable')->saveInvoiceFreeProduct($freeItemProduct);
                }
            }

            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $p['locationID']);
            $locationPID = $locationProduct->locationProductID;
            $averageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
            if ($p['productType'] != 2 && $p['stockUpdate'] == 'true' && $locationProduct->locationProductQuantity < $deliveryQty && $p['documentTypeID'] != 19) {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                );
            }
            $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

            if ($p['pTax'] && $p['pTax']['tL']) {
                foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                    $invoicePTaxesData = array(
                        'salesInvoiceProductID' => $invoiceProductID,
                        'productID' => $productID,
                        'taxID' => $taxKey,
                        'salesInvoiceProductTaxPrecentage' => $productTax['tP'],
                        'salesInvoiceProductTaxAmount' => $productTax['tA'] * $customCurrencyRate
                    );
                    $invoicePTaxM = new DraftInvProductTax();
                    $invoicePTaxM->exchangeArray($invoicePTaxesData);
                    $this->CommonTable('Invoice\Model\DraftInvProductTaxTable')->saveInvoiceProductTax($invoicePTaxM);

                    $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxKey);
                    if($this->useAccounting == 1 && empty($taxData['taxSalesAccountID']) && !$isSetJournalEntry){
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_TAX_SALES_ACCOUNT', array($taxData['taxName']))
                            );
                    }

                    $totalCRAmounts += $productTax['tA'] * $customCurrencyRate;
                }
            }
            
            if (count($batchProducts) > 0) {
                foreach ($batchProducts as $batchKey => $batchValue) {
                    if(!empty($batchValue)){
                        $ss = $this->saveDraftInvoiceSubProductData($batchValue, $invoiceProductID);
                    }
                }
            }
        }
        
        return array(
            'status' => true,
            "data" => ['invID' => $salesInvoiceID, 'invCode' => $invoiceCode, 'hashValue' => $hashValue],
            'msg' => $this->getMessage('SUCC_INVOICE_ADD_INVO_PRODETAILS', array($invoiceCode)),
            'totalPrice' => $totalPrice
        );

    }

    public function updateDraftInvoiceAction()
    {      
        $request = $this->getRequest();
        if ($request->isPost()) {

            $post = $request->getPost();
            // $isGoThroughTheWf = false;
            if ($post['isActiveWF'] == 1) {

                if (!empty($post['wfID'])) {
                    $limitData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitsTable')->getLimitsByWFId($post['wfID']);
                    $limitID = null;


                    foreach ($limitData as $key => $value) {
                        if (floatval($post['invoiceTotalPrice']) <= floatval($value['approvalWorkflowLimitMax']) && floatval($post['invoiceTotalPrice']) >= floatval($value['approvalWorkflowLimitMin'])) {
                            $limitID = $value['approvalWorkflowLimitsID'];
                            break;
                        }
                    }

                    if ($limitID != null) {

                        //delete previous draftInv
                        $this->beginTransaction();

                        $oldDraftInv =  $this->CommonTable('Invoice\Model\DraftInvWfTable')->getInvoiceByID($post['invoiceID']);

                        $oldDraftInv = (array) $oldDraftInv;

                        $deleteOldDraftInv = $this->updateDeleteInfoEntity($oldDraftInv['entityID']);


                        if ($deleteOldDraftInv) {
                            $post['invoiceID'] = null;
                            $post['invoiceCode'] = null;
                        }


                        $limitApproversData =  $this->CommonTable('Settings\Model\ApprovalWorkflowLimitApproversTable')->getLimitApproversByWFLimitId($limitID);
                        $approvers = [];

                        foreach ($limitApproversData as $key => $value) {
                            $approvers[] = $value['workflowLimitApprover'];
                        }

                        $customerID = ($request->getPost('customer')) ? $request->getPost('customer') : 0;
                        $param = $request->getPost();

                        $saveInvDraft = $this->saveInvDraft($post, $customerID, false, false);

                        if (!$saveInvDraft['status']) {
                            $this->rollback();
                            $this->status = false;
                            $this->msg = $saveInvDraft['msg'];
                            return $this->JSONRespond();
                        } else {
                            $this->commit();
                            $this->status = true;
                            $this->data = array('invID' => $saveInvDraft['data']['invID'], 'approvers' => $approvers, 'hashValue' => $saveInvDraft['data']['hashValue'], 'invCode' => $saveInvDraft['data']['invCode'], 'isDraft' => true);
                            $this->msg = $this->getMessage('SUCC_INV_WF');
                            return $this->JSONRespond();
                        }
                    } else {
                        $this->rollback();
                        $this->status = false;
                        $this->msg = $this->getMessage('SELECTED_APPROVAL_WF_RANGES_MISMATCH_INV');
                        return $this->JSONRespond();
                    }
                }

            }    
        } else {
            $this->status = false;
            $this->msg = $this->getMessage('ERR_INVOICE_PRODUCT_SAVE');

            return $this->JSONRespond();
        }
    }

     
    public function converDraftToInvoice($param, $customerID = 0, $pos = false, $edit = false, $restFlag = false, $posUserID)
    {      
        $customCurrencyId = $param['customCurrencyId'];


//get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "") ? $customCurrencyData->currencyRate : 1;
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $this->companyCurrencySymbol;

        $ccrate = $param['customCurrencyRate'];
//if custom currency rate alredy set add that rate to sales Order
        if (!($ccrate == 0 || $ccrate == 1)) {
            $customCurrencyRate = $ccrate;
        }

        if ($restFlag == true && $customerID == NULL) {
            $customerID = 0;
        }

        $products = $param['products'];
        $invoiceCode = $param['invoiceCode'];
        $locationOut = $param['locationOutID'];
        $invoiceDate = $this->convertDateToStandardFormat($param['invoiceDate']);
        $dueDate = $this->convertDateToStandardFormat($param['dueDate']);
        $deliveryCharge = $param['deliveryCharge'] * $customCurrencyRate;
        $totalPrice = trim($param['invoiceTotalPrice']) * $customCurrencyRate;
        $finalTotalWithInclusiveTax = trim($param['finalTotalWithInclusiveTax']) * $customCurrencyRate;
        $comment = $param['invoiceComment'];
        $paymentTermID = $param['paymentTermID'];
        $showTax = $param['showTax'];
        $deliveryNoteID = $param['deliveryNoteID'];
        $salesOrderID = $param['salesOrderID'];
        $quotationID = $param['quotationID'];
        $activityID = $param['activityID'];
        $jobID = $param['jobID'];
        $projectID = $param['projectID'];
        $salesPersonIDArr = $param['salesPersonID'];
        $invoiceTotalDiscount = $param['invoiceTotalDiscount'] * $customCurrencyRate;
        $salesInvoicePayedAmount = $param['salesInvoicePayedAmount'] * $customCurrencyRate;
        $suspendedTax = $param['suspendedTax'];
        $deliveryAddress = $param['deliveryAddress'];
        $promotionID = $param['promotionID'];
        $promotionDiscountValue = $param['promotionDiscountValue'];
        $totalInvoiceDiscountValue = $param['invoiceDiscountRate'];
        $invoiceDiscoutType = $param['invoiceDiscountType'];
        $totalTax = $param['totalTaxValue'] * $customCurrencyRate;
        $invoiceWisePromotionDiscountType = $param['invoiceWisePromotionDiscountType'];
        $salesInvoiceDeliveryChargeEnable = $param['salesInvoiceDeliveryChargeEnable'];
        $priceListId = $param['priceListId'];
        $customerProfID = $param['customerProfID'];
        $invTempCode = $param['invTempCode'];
        $journalEntryData = $param['journalEntryData'];
        $inclusiveTax = (int)filter_var($param['inclusiveTax'], FILTER_VALIDATE_BOOLEAN);
        $dimensionData = $param['dimensionData'];
        $ignoreBudgetLimit = $param['ignoreBudgetLimit'];
        $nextInvoiceDate = ($param['nextInvoiceDate'] == "") ? null : $param['nextInvoiceDate'];
        $nextInvoiceDateComment = $param['nextInvoiceDateComment'];
        $isSetJournalEntry = false;
        $serviceChargeAmount = $param['serviceChargeAmount'];
        $addedServiceChargeArray = $param['addedServiceChargeArray'];
        $addedFreeIssueItems = (!empty($param['addedFreeIssueItems'])) ? $param['addedFreeIssueItems'] : [];
        $linkedCusOrder = (!empty($param['linkedCusOrder'])) ? $param['linkedCusOrder'] : null;
        if (!$pos) {
            $showTax = 1;
        }
        if(count($journalEntryData) > 0){
            $isSetJournalEntry = true;
        }

        if (!is_null($deliveryNoteID)) {
            $deliveryNoteStatus = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteStatusByDeliveryNoteID($deliveryNoteID)->current();
            if ($deliveryNoteStatus['deliveryNoteStatus'] == "10") {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_DLN'),
                );
            }

            if ($deliveryNoteStatus['deliveryNoteStatus'] == "4") {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_CLOSE_DLN'),
                );
            }
        }

        $cust = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);
        $customerCurrentOutstanding = $cust->customerCurrentBalance;

        $promotionData = $this->getPromotionDetails($promotionID);

        $result = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails();
        $displayData = $result->current();
        $FIFOCostingFlag = $displayData['FIFOCostingFlag'];
        $averageCostingFlag = $displayData['averageCostingFlag'];

        $accountProduct = array();
        $glAccountExist = $this->CommonTable('Accounting\Model\GlAccountSetupTable')->fetchAll();
        $glAccountsData = $glAccountExist->current();
        $customerData =  $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($customerID);

       
        $custSalesAccountIsset = true;
        if ($restFlag) {
            $this->useAccounting = 1;
        }
        if($this->useAccounting == 1 && !$isSetJournalEntry){
            if(empty($customerData->customerSalesAccountID)){
                $custSalesAccountIsset = false;
                // return array(
                //     'status' => false,
                //     'msg' => $this->getMessage('ERR_CUSTOMER_SALES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                // );
            }
            if(empty($customerData->customerReceviableAccountID)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_CUSTOMER_RECEVIABLE_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                );
            }
            if($deliveryCharge > 0 && empty($glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID)){
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_GL_DELIVERY_CHARGES_ACCOUNT_NOT_SET',array($customerData->customerName."-".$customerData->customerCode)),
                );
            }
        }

        if(!$customerProfID){
            $customerDefaultProfile = $this->CommonTable('Invoice\Model\CustomerProfileTable')
                            ->getDefaultProfileByCustomerID($customerID);
            foreach ($customerDefaultProfile as $key => $value) {
                $customerProfID = $value['customerProfileID'];
            }
        }

        if(!$pos){
            $itemAttrDetailsSet = [];
            foreach ($products as $key => $p) {
                $p = (array) $p;
                $productID = $p['productID'];
                $itemAttributeMap = $this->CommonTable('Settings\Model\ItemAttributeMapTable')->getDetailsByProductID($productID);
                foreach ($itemAttributeMap as $key => $value) {
                    if($value['itemAttributeID'] == 1){
                        $itemAttrDetailsSet[$productID][] = $value['itemAttributeValueID'];
                    }
                }
            }
                //check the same credit period is exist for all items
            if(count($itemAttrDetailsSet) > 1){
                foreach ($itemAttrDetailsSet as $ckey => $cvalue) {
                    if(isset($oldVal)){
                        $newVal = $cvalue;
                        $oldVal = array_intersect($oldVal, $newVal);
                        if(!(count($oldVal) > 0)){
                            return array(
                                'status' => false,
                                'msg' => $this->getMessage('ERR_INVOICE_API_ITEMATTRIBUTE_CREDIT_PERIOD')
                            );
                            break;
                        }
                    }else{
                        $oldVal = $cvalue;
                    }
                }
            }
        }

        //check whether SalesInvoice is created via a SalesOrder, DeliveryNote or activity
        if (!empty($salesOrderID) || !empty($deliveryNoteID) || !empty($activityID) || !empty($jobID) || !empty($projectID) || !empty($quotationID)) {
            $fromSalesOrderOrDeliveryNote = TRUE;
        } else {
            $fromSalesOrderOrDeliveryNote = FALSE;
        }

        $result = $this->getReferenceNoForLocation('3', $locationOut);
        $locationReferenceID = $result['locRefID'];

        $stresult = $this->getReferenceNoForLocation('25', $locationOut);
        $stlocationReferenceID = $stresult['locRefID'];

        // check if a invoice from the same invoice Code exists if exist add next number to invoice code
        //if this is coming from invoice edit then it does not go to inside while
        if (!$edit) {
            while ($invoiceCode) {
                if ($this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceBySalesInvoiceCode($invoiceCode)) {
                    if ($locationReferenceID && $stlocationReferenceID) {
                        $newInvoiceCode = $this->getReferenceNumber($locationReferenceID);
                        $stnewInvoiceCode = $this->getReferenceNumber($stlocationReferenceID);
                        if ($newInvoiceCode == $invoiceCode) {
                            $this->updateReferenceNumber($locationReferenceID);
                            $invoiceCode = $this->getReferenceNumber($locationReferenceID);
                        } else if ($stnewInvoiceCode == $invoiceCode) {
                            $this->updateReferenceNumber($stlocationReferenceID);
                            $stnewInvoiceCode = $this->getReferenceNumber($stlocationReferenceID);
                        } else {
                            if ($suspendedTax) {
                                $invoiceCode = $stnewInvoiceCode;
                            } else {
                                $invoiceCode = $newInvoiceCode;
                            }
                        }
                    } else {
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_INVOICE_CODE_EXISTS')
                        );
                    }
                } else {
                    break;
                }
            }
        }

        if ($deliveryCharge != '') {
            $realItemTotalPrice = $totalPrice - $deliveryCharge;
        } else {
            $realItemTotalPrice = $totalPrice;
        }


        if ($serviceChargeAmount != 0 || $serviceChargeAmount != '') {
            $realItemTotalPrice = $realItemTotalPrice - $serviceChargeAmount;
        }


        if ($suspendedTax) {
            $realItemTotalPrice = $realItemTotalPrice + $totalTax;
        }
        if ($invoiceDiscoutType == 'presentage') {
            $totalInvoiceDiscount = ($totalInvoiceDiscountValue / (100 - $totalInvoiceDiscountValue)) * ($realItemTotalPrice - $totalTax);
        } else {
            $totalInvoiceDiscount = $totalInvoiceDiscountValue * $customCurrencyRate;
        }

        if ($pos) {
            $totalInvoiceDiscount = $param['invoiceTotalDiscount'] * $customCurrencyRate;
            $totalPromotionDiscountValue = $promotionDiscountValue * $customCurrencyRate;
        }

        if ($invoiceWisePromotionDiscountType == '2') {
            $realItemTotalPrice = ($realItemTotalPrice - ($totalTax));
            $totalPromotionDiscountValue = (($realItemTotalPrice * 100) / (100 - $promotionDiscountValue)) - $realItemTotalPrice;
        } else if ($invoiceWisePromotionDiscountType == '1') {
            $totalPromotionDiscountValue = $promotionDiscountValue * $customCurrencyRate;
        }

        if ($restFlag) {
            $entityID = $this->createEntityForRestApi($posUserID);
        } else {
            $entityID = $this->createEntity();
        }

        $statusName = 'Open';
        $statusID = $this->getStatusIDByStatusName($statusName);
        $invoiceData = array(
            'salesInvoiceCode' => $invoiceCode,
            'customerID' => $customerID,
            'locationID' => $locationOut,
            'salesInvoiceIssuedDate' => $invoiceDate,
            'salesInvoiceOverDueDate' => $dueDate,
            'salesinvoiceTotalAmount' => $totalPrice,
            'salesInvoiceComment' => $comment,
            'salesInvoiceDeliveryCharge' => $deliveryCharge,
            'paymentTermID' => $paymentTermID,
            'quotationID' => $quotationID,
            //'deliveryNoteID' => $deliveryNoteID,
            'salesOrderID' => $salesOrderID,
            'activityID' => $activityID,
            'jobID' => $jobID,
            'projectID' => $projectID,
            'salesInvoiceShowTax' => $showTax,
            'statusID' => $statusID,
            'entityID' => $entityID,
            'pos' => $pos,
            'salesPersonID' => null,
            'salesInvoicePayedAmount' => $salesInvoicePayedAmount,
            'salesInvoiceTotalDiscount' => $invoiceTotalDiscount,
            'salesInvoiceSuspendedTax' => $suspendedTax,
            'salesInvoiceDeliveryAddress' => $deliveryAddress,
            'promotionID' => $promotionID,
            'salesInvoicePromotionDiscount' => $totalPromotionDiscountValue,
            'salesInvoiceWiseTotalDiscount' => $totalInvoiceDiscount,
            'salesInvoiceTotalDiscountType' => $invoiceDiscoutType,
            'salesInvoiceDiscountRate' => $totalInvoiceDiscountValue,
            'customCurrencyId' => $customCurrencyId,
            'salesInvoiceCustomCurrencyRate' => $customCurrencyRate,
            'priceListId' => $priceListId,
            'salesInvoiceDeliveryChargeEnable' => $salesInvoiceDeliveryChargeEnable,
            'customerProfileID' => $customerProfID,
            'salesInvoiceTemporaryCode' => $invTempCode,
            'offlineSavedTime' => $param['offlineSavedTime'],
            'offlineUser' => $param['offlineUser'],
            'discountOnlyOnEligible' => $param['discountOnlyOnEligible'],
            'customerCurrentOutstanding' => $customerCurrentOutstanding,
            'inclusiveTax' => $inclusiveTax,
            'templateInvoiceTotal' => $finalTotalWithInclusiveTax,
            'salesInvoiceServiceChargeAmount' => $serviceChargeAmount,
            'linkedCusOrderNum' => $linkedCusOrder
        );

        $invoiceExData = new Invoice;
        $invoiceExData->exchangeArray($invoiceData);
        $salesInvoiceID = $this->CommonTable('Invoice\Model\InvoiceTable')->saveInvoiceData($invoiceExData);
        //if tax suspended is true update tsinvoice reference number otherwise update invoice reference number
        if (!$edit) {
            if ($suspendedTax) {
                if ($stlocationReferenceID) {
                    $this->updateReferenceNumber($stlocationReferenceID);
                }
            } else {
                if ($locationReferenceID) {
                    $this->updateReferenceNumber($locationReferenceID);
                }
            }
        }

        if (!is_null($nextInvoiceDate)) {
            $membershipData = [
                'invoiceID' => $salesInvoiceID,
                'nextInvoiceDate' => $this->convertDateToStandardFormat($nextInvoiceDate),
                'comment' => $nextInvoiceDateComment
            ];

            $memData = new Membership;
            $memData->exchangeArray($membershipData);
            $memID = $this->CommonTable('Invoice\Model\MembershipTable')->saveMembership($memData);

            if (!$memID) {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_MEMBERSHIP')
                );
            }
        }


        if (sizeof($addedServiceChargeArray) > 0) {
            foreach ($addedServiceChargeArray as $key1 => $value1) {
                $value1 = (array) $value1;
                $serviceChargeTypeData = $this->CommonTable('Invoice\Model\ServiceChargeTypeTable')->getServiceChargeTypeDetailsById($value1['serviceChargeTypeId'])->current();
                $serviceChargeData = [
                    'invoiceID' => $salesInvoiceID,
                    'serviceChargeTypeID' => $serviceChargeTypeData['serviceChargeTypeID'],
                    'serviceChargeAmount' => $value1['serviceChargeValueTxt'],
                ];

                $scData = new InvoiceServiceCharge;
                $scData->exchangeArray($serviceChargeData);

                $memID = $this->CommonTable('Invoice\Model\InvoiceServiceChargeTable')->save($scData);
            }
        }


        $totalCRAmounts = 0;
        unset($products['subProduct']);
        $totalProductAmounts = 0;
        $totalTaxAmount = 0;
        foreach ($products as $keyt => $pt) {
            $pt = (array) $pt;
            $pt['pTax'] = (array) $pt['pTax'];
            if(isset($pt['pTax']['tTA'])){
                if($pos){
                    $ptwTax = ($pt['productTotal']);
                }else{
                    $ptwTax = ($pt['productTotal'] - $pt['pTax']['tTA']);
                }
                $totalTaxAmount += $pt['pTax']['tTA'];
            }else{
                $ptwTax = $pt['productTotal'];
            }
            $totalProductAmounts += $ptwTax;
        }

        if (!empty($salesPersonIDArr) && sizeof($salesPersonIDArr) > 0) {

            $valueForOneSalesPerson = $totalPrice / sizeof($salesPersonIDArr);

            foreach ($salesPersonIDArr as $key5 => $value5) {
                
                $salesPersonData = [
                    "invoiceID" => $salesInvoiceID,
                    "salesPersonID" => $value5,
                    "relatedSalesAmount" => $valueForOneSalesPerson
                ];


                $spData = new InvoiceSalesPersons;
                $spData->exchangeArray($salesPersonData);

                $spID = $this->CommonTable('Invoice\Model\InvoiceSalesPersonsTable')->saveSalesPerson($spData);
            }
        } 
        foreach ($products as $key => $p) {
            $p = (array) $p;
            $p['selected_serials'] = (array) $p['selected_serials'];
            $param['subProducts'] = (array) $param['subProducts'];


            foreach ($param['subProducts'][$key] as $k => $value) {
               $param['subProducts'][$key][$k] = (array) $value;
            }

            $batchProducts = [];
            if ($pos == false) {
                if (!is_null($p['selected_serials']) && isset($param['subProducts'][$key])) {
                    foreach ($p['selected_serials'] as $in => $srl) {
                        $a = array_search($srl, array_column($param['subProducts'][$key], 'serialID'));
                        $batchProducts[] = $param['subProducts'][$key][$a];
                    }
                } else {
                    $batchProducts = $param['subProducts'][$key];
                }
            } else {
                $batchProducts = $param['subProducts'][$p['productID']];
            }
            // var_dump($batchProducts);


            $displaySetupDetails = (array) $this->CommonTable('Core/Model/DisplaySetupTable')->fetchAllDetails()->current();

            $hasFreeItem = false;

            if ($p['isFreeItem'] != 'true') {
                $addedFreeIssueItems = (array) $addedFreeIssueItems;
                if (!empty($addedFreeIssueItems[$key])) {
                    $hasFreeItem = true;
                }
            }


            $productID = $p['productID'];
            $p['deliverQuantity'] = (array) $p['deliverQuantity'];
            $deliveryQty = $p['deliverQuantity']['qty'];
            $invoiceProductData = array(
                'salesInvoiceID' => $salesInvoiceID,
                'productID' => $productID,
                'locationProductID' => $p['locationProductID'],
                'salesInvoiceProductDescription' => $p['productDescription'],
                'salesInvoiceProductPrice' => $p['productPrice'] * $customCurrencyRate,
                'salesInvoiceProductDiscount' => ($p['productDiscountType'] == 'precentage') ? $p['productDiscount'] : $p['productDiscount'] * $customCurrencyRate,
                'salesInvoiceProductDiscountType' => $p['productDiscountType'],
                'salesInvoiceProductTotal' => $p['productTotal'] * $customCurrencyRate,
                'salesInvoiceProductQuantity' => $deliveryQty,
                'itemSalesPersonID' => (!empty($p['itemSalesPerson']) && $p['itemSalesPerson'] != 0 && $p['itemSalesPerson'] != null) ? $p['itemSalesPerson'] : null,
                'documentTypeID' => $p['documentTypeID'],
                'salesInvoiceProductDocumentID' => $p['documentID'],
                'salesInvoiceProductSelectedUomId' => $p['selectedUomId'],
                'inclusiveTaxSalesInvoiceProductPrice' => $p['inclusiveTaxProductPrice'] * $customCurrencyRate,
                'salesInvoiceProductMrpType' =>  ($displaySetupDetails['useMrpSettings'] == 1) ? $p['mrpType'] : null, 
                'salesInvoiceProductMrpValue' => ($displaySetupDetails['useMrpSettings'] == 1) ? $p['mrpValue'] : null,
                'salesInvoiceProductMrpPercentage' => ($displaySetupDetails['useMrpSettings'] == 1) ? $p['mrpPercentageValue'] : null,
                'salesInvoiceProductMrpAmount' => ($displaySetupDetails['useMrpSettings'] == 1) ? $p['actmrpValue']: null,
                'isFreeItem' => ($p['isFreeItem'] == 'true') ? true: false,
                'hasFreeItem' => $hasFreeItem
            );
            //calculate discount value
            $discountValue = 0;
            if ($p['productDiscountType'] == "precentage") {
                $discountValue = ($p['productPrice'] / 100 * $p['productDiscount']) * $customCurrencyRate;
            } else if ($p['productDiscountType'] == "value") {
                $discountValue = $p['productDiscount'] * $customCurrencyRate;
            }

            $invoiceProduct = new InvoiceProduct;
            $invoiceProduct->exchangeArray($invoiceProductData);
            $invoiceProductID = $this->CommonTable('Invoice\Model\InvoiceProductTable')->saveInvoiceProduct($invoiceProduct);

            if ($hasFreeItem) {
                $freeItems = $addedFreeIssueItems[$key];

                foreach ($freeItems as $key5 => $value5) {
                    $value5 = (array) $value5;
                    if (!empty($value5['incVal'])) {
                        $k = $value5['productID'].'_'.$value5['incVal'];
                        $relatedItem = (array) $products[$k];
                        $relatedItem['deliverQuantity'] = (array) $relatedItem['deliverQuantity'];
                        $freeItem = [
                            'invoiceID' => $salesInvoiceID,
                            'mainProductID' => $productID,
                            'mainSalesInvoiceProductID' => $invoiceProductID,
                            'freeProductID' => $value5['productID'] ,
                            'freeQuantity' => $relatedItem['deliverQuantity']['qty'],
                            'salesInvoiceFreeItemSelectedUom' => $relatedItem['selectedUomId'],
                            'locationID' => $relatedItem['locationID'],

                        ];
                    } else {
                        $freeItem = [
                            'invoiceID' => $salesInvoiceID,
                            'mainProductID' => $productID,
                            'mainSalesInvoiceProductID' => $invoiceProductID,
                            'freeProductID' => $value5['freeProductID'] ,
                            'freeQuantity' => $value5['freeQuantity'],
                            'salesInvoiceFreeItemSelectedUom' => $value5['salesInvoiceFreeItemSelectedUom'],
                            'locationID' => $value5['locationID'],
                        ];
                    }

                    $freeItemProduct = new InvoiceFreeProduct;
                    $freeItemProduct->exchangeArray($freeItem);

                    $freeItemProductID = $this->CommonTable('Invoice\Model\InvoiceFreeProductTable')->saveInvoiceFreeProduct($freeItemProduct);
                }
            }

            $locationProduct = $this->CommonTable('Inventory\Model\LocationProductTable')->getLocationProduct($productID, $p['locationID']);
            $locationPID = $locationProduct->locationProductID;
            $averageCostingPrice = $locationProduct->locationProductAverageCostingPrice;
            if ($p['productType'] != 2 && $p['stockUpdate'] == 'true' && $locationProduct->locationProductQuantity < $deliveryQty && $p['documentTypeID'] != 19) {
                return array(
                    'status' => false,
                    'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                );
            }
            $pData = $this->CommonTable('Inventory\Model\ProductTable')->getProductByProductID($productID);

            if($this->useAccounting == 1 && !$isSetJournalEntry){
                $p['pTax'] = (array) $p['pTax'];
                if($p['giftCard'] != 1 && $p['productType'] != 2){
                    if($p['stockUpdate'] == 'true' && empty($pData['productInventoryAccountID'])){
                        return array(
                            'status'=> false,
                            'msg'=> $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                            );
                    }
                         //check whether Product Cost of Goods Sold Gl account id set or not
                    if($p['stockUpdate'] == 'true' && empty($pData['productCOGSAccountID'])){
                        return array(
                            'status'=> false,
                            'msg'=> $this->getMessage('ERR_GRN_PRODUCT_ACCOUNT_COGS', array($pData['productCode'].' - '.$pData['productName'])),
                            );
                    }
                }

                if(!$custSalesAccountIsset){
                    if(empty($pData['productSalesAccountID'])){
                        return array(
                            'status'=> false,
                            'msg'=> $this->getMessage('ERR_PRPODUCT_SALES_ACCOUNT', array($pData['productCode'].' - '.$pData['productName'])),
                            );
                    }
                }

                //set gl accounts for the journal entry
                //TODO after set the productTotal with tax value in pos should have to remove belowe condition the correct $pTotal is $p['productTotal'] - $p['pTax']['tTA'];
                if($pos){
                    $pTotal = $p['productTotal'];
                }else{
                    $pTotal = $p['productTotal'] - $p['pTax']['tTA'];
                }
                if($invoiceData['salesInvoiceTotalDiscountType'] == "Value"){
                    $productTotal = $pTotal - $invoiceData['salesInvoiceDiscountRate']*($pTotal/$totalProductAmounts);
                }else {
                    $productTotal = $pTotal;
                }

                if($promotionID != '' && $promotionData[$promotionID]['promoType'] == 2 && $promotionData[$promotionID]['discountType'] == 1){
                    $productTotal = $productTotal - $invoiceData['salesInvoicePromotionDiscount']*($productTotal/$totalProductAmounts);
                }

                if($p['giftCard'] !=1){
                    $totalCRAmounts+=$productTotal;
                    // if($custSalesAccountIsset){
                        // if(isset($accountProduct[$customerData->customerSalesAccountID]['creditTotal'])){
                        //     $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] += $productTotal;
                        // }else{
                        //     $accountProduct[$customerData->customerSalesAccountID]['creditTotal'] = $productTotal;
                        //     $accountProduct[$customerData->customerSalesAccountID]['debitTotal'] = 0.00;
                        //     $accountProduct[$customerData->customerSalesAccountID]['accountID'] = $customerData->customerSalesAccountID;
                        // }
                    // }else{
                    if(isset($accountProduct[$pData['productSalesAccountID']]['creditTotal'])){
                        $accountProduct[$pData['productSalesAccountID']]['creditTotal'] += $productTotal;
                    }else{
                        $accountProduct[$pData['productSalesAccountID']]['creditTotal'] = $productTotal;
                        $accountProduct[$pData['productSalesAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$pData['productSalesAccountID']]['accountID'] = $pData['productSalesAccountID'];
                    }
                    // }
                }
            }
            if ($p['stockUpdate'] == 'true' && $p['productType'] != 2) {
                if ($p['documentTypeID'] != 19) {
                    $locationProductData = array(
                        'locationProductQuantity' => $locationProduct->locationProductQuantity - $deliveryQty,
                    );
                    $this->CommonTable('Inventory\Model\LocationProductTable')->updateQty($locationProductData, $productID, $p['locationID']);
                }
            }
            if ($p['pTax'] && $p['pTax']['tL']) {
                foreach ($p['pTax']['tL'] as $taxKey => $productTax) {
                    $productTax = (array) $productTax;
                    $invoicePTaxesData = array(
                        'salesInvoiceProductID' => $invoiceProductID,
                        'productID' => $productID,
                        'taxID' => $taxKey,
                        'salesInvoiceProductTaxPrecentage' => $productTax['tP'],
                        'salesInvoiceProductTaxAmount' => $productTax['tA'] * $customCurrencyRate
                    );
                    $invoicePTaxM = new InvoiceProductTax();
                    $invoicePTaxM->exchangeArray($invoicePTaxesData);
                    $this->CommonTable('Invoice\Model\InvoiceProductTaxTable')->saveInvoiceProductTax($invoicePTaxM);

                    $taxData = $this->CommonTable('Settings\Model\TaxTable')->getSimpleTax($taxKey);
                    if($this->useAccounting == 1 && empty($taxData['taxSalesAccountID']) && !$isSetJournalEntry){
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_TAX_SALES_ACCOUNT', array($taxData['taxName']))
                            );
                    }

                    $totalCRAmounts += $productTax['tA'] * $customCurrencyRate;
                    //set tax gl accounts for the journal Entry
                    if ($pos) {
                        if(isset($accountProduct[$taxData['taxSalesAccountID']]['creditTotal'])){
                            $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] += $productTax['tA'] * $customCurrencyRate;
                        }else{
                            $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = $productTax['tA'] * $customCurrencyRate;
                            $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = 0.00;
                            $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                        }
                    } else {
                        if (($productTax['susTax'] == '0' && $param['suspendedTax'] == '1') || ($productTax['susTax'] == '0' && $param['suspendedTax'] == '0') || ($productTax['susTax'] == '1' && $param['suspendedTax'] == '0')) {
                            if(isset($accountProduct[$taxData['taxSalesAccountID']]['creditTotal'])){
                                $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] += $productTax['tA'] * $customCurrencyRate;
                            }else{
                                $accountProduct[$taxData['taxSalesAccountID']]['creditTotal'] = $productTax['tA'] * $customCurrencyRate;
                                $accountProduct[$taxData['taxSalesAccountID']]['debitTotal'] = 0.00;
                                $accountProduct[$taxData['taxSalesAccountID']]['accountID'] = $taxData['taxSalesAccountID'];
                            }
                        }
                    }
                }
            }
            //if copied from SalesOrder or DeliveryNote or quotation,update those products as copied.
            if ($fromSalesOrderOrDeliveryNote == TRUE) {
                if ($p['stockUpdate'] == 'true' && !empty($salesOrderID)) {
                    $salesOrderProducts = $this->CommonTable('Invoice\Model\SalesOrderProductTable')->getProductBySalesOrderIDAndLocationProductId($salesOrderID, $locationPID, $p['productPrice']);
                    if ($salesOrderProducts) {
                        $newCopiedQuantity = $salesOrderProducts->salesOrdersProductCopiedQuantity + (float) $deliveryQty;
                        $copiedflag = 0;
                        if ($newCopiedQuantity >= $salesOrderProducts->quantity) {
                            $newCopiedQuantity = $salesOrderProducts->quantity;
                            $copiedflag = 1;
                        }
                        $soProData = array(
                            'copied' => $copiedflag,
                            'salesOrdersProductCopiedQuantity' => $newCopiedQuantity,
                        );
                        $this->CommonTable('Invoice\Model\SalesOrderProductTable')->updateCopiedSalesOrderProducts($locationPID, $salesOrderID, $soProData);
                    }
                } else if (($p['documentTypeID'] == 4 && !empty($p['documentID'])) && $p['stockUpdate'] == 'false') {
                    $deliveryNoteProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getProductByDeliveryNoteIDAndProductId($p['documentID'], $productID);
                    if (isset($deliveryNoteProducts)) {
                        $newCopiedQuantity = $deliveryNoteProducts->deliveryNoteProductCopiedQuantity + (float) $deliveryQty;
                        $copiedflag = 0;
                        if ($newCopiedQuantity == $deliveryNoteProducts->deliveryNoteProductQuantity) {
                            $newCopiedQuantity = $deliveryNoteProducts->deliveryNoteProductQuantity;
                            $copiedflag = 1;
                        } else if ($newCopiedQuantity > $deliveryNoteProducts->deliveryNoteProductQuantity) {
                            return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_DELI_PRODUCT_QUAN_CHECK')
                            );
                        }
                        $deliveryNoteProData = array(
                            'copied' => $copiedflag,
                            'deliveryNoteProductCopiedQuantity' => $newCopiedQuantity,
                        );
                        $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->updateCopiedDeliveryNoteProducts($productID, $p['documentID'], $deliveryNoteProData);
                        $this->_checkAndUpdateDeliveryNoteStatus($p['documentID']);
                    }
                    if (count($batchProducts) > 0) {
                        foreach ($batchProducts as $batchValue) {
                            if ($batchValue['serialID'] != '') {
                                $deliveryNoteSubProducts = $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->getProductSerialByDeliveryNoteProductIDAndProductSerialID($deliveryNoteProducts->deliveryNoteProductID, $batchValue['serialID']);
                                $deliveryNoteProData = array(
                                    'deliveryNoteSubProductsCopiedQuantity' => 1,
                                );
                            } else {
                                $deliveryNoteSubProducts = $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->getProductBatchByDeliveryNoteProductIDAndProductBatchID($deliveryNoteProducts->deliveryNoteProductID, $batchValue['batchID']);
                                $newCopiedQuantity = $deliveryNoteSubProducts->deliveryNoteSubProductsCopiedQuantity + (float) $batchValue['qtyByBase'];

                                $deliveryNoteProData = array(
                                    'deliveryNoteSubProductsCopiedQuantity' => $newCopiedQuantity,
                                );
                            }
                            $this->CommonTable('Invoice\Model\DeliveryNoteSubProductTable')->updateDeliveryNoteSubProductData($deliveryNoteProData, $deliveryNoteSubProducts->deliveryNoteSubProductID);
                        }
                    }
                } else if (!empty($quotationID)) {
                    $quotationProducts = $this->CommonTable('Invoice\Model\QuotationProductTable')->getQuotationProductByQuotationIdAndLocationProductId($quotationID, $locationPID);
                    if (isset($quotationProducts)) {
                        $newCopiedQuantity = $quotationProducts->quotationProductCopiedQuantity + (float) $deliveryQty;
                        $copiedflag = 0;
                        if ($newCopiedQuantity >= $quotationProducts->quotationProductQuantity) {
                            $newCopiedQuantity = $quotationProducts->quotationProductQuantity;
                            $copiedflag = 1;
                        }
                        $quoProData = array(
                            'quotationProductCopied' => $copiedflag,
                            'quotationProductCopiedQuantity' => $newCopiedQuantity,
                        );
                        $this->CommonTable('Invoice\Model\QuotationProductTable')->updateCopiedQuotationProducts($locationPID, $quotationID, $quoProData);
                    }
                } else if ($p['documentTypeID'] == 19) {
                    $resJob = $this->CommonTable('Invoice\Model\InvoiceProductDeliveryNoteProductTable')->getJobMappingByInvoiceProductIdAndJobId($p['invoiceProductID'], $p['documentID']);
                    foreach ($resJob as $jPro) {
                        $jobDelivData = [
                            'invoiceProductID' => $invoiceProductID
                        ];
                        $res = $this->CommonTable('Invoice\Model\InvoiceProductDeliveryNoteProductTable')->update($jobDelivData, $jPro['invoiceProductDeliveryNoteProductID']);
                    }
                }
            }
            if (!empty($p['isBom'] && $p['isBom'] == "true" && $p['productType'] == 2 )) {
                // if ($p['isBomJeSubItem']) {

                    //get related bom Item
                    $res = $this->CommonTable('Inventory\Model\BomTable')->getBomDataByProductID($productID)->current();

                    // if ($res['isJEPostSubItemWise']) {
                        $subItemData = $this->CommonTable('Inventory\Model\BomSubItemTable')->getBomSubDataByBomID($res['bomID']);
                        //check all sub items are non inventory or not
                        $nonInventoryCount = 0;
                        foreach ($subItemData as $key => $value) {

                            $invoicedBomSubProductData = array(
                                'salesInvoiceID' => $salesInvoiceID,
                                'salesInvoiceProductID' => $invoiceProductID,
                                'bomID' => $value['bomID'],
                                'subProductID' => $value['productID'],
                                'subProductBomPrice' => $value['bomSubItemUnitPrice'] * $customCurrencyRate,
                                'subProductBomQty' => $value['bomSubItemQuantity'] * $value['productUomConversion'],
                                'subProductTotalQty' => $value['bomSubItemQuantity'] * $value['productUomConversion'] * ($deliveryQty),
                                'subProductTotal' => $value['bomSubItemUnitPrice'] * $customCurrencyRate * ($deliveryQty),
                            );

                            $invoicedBomSubItems = new InvoicedBomSubItems;
                            $invoicedBomSubItems->exchangeArray($invoicedBomSubProductData);
                            $invoicedBomSubProductID = $this->CommonTable('Invoice\Model\InvoicedBomSubItemsTable')->saveInvoicedBomSubProduct($invoicedBomSubItems);

                            if ($value['productTypeID'] == 2){
                                $nonInventoryCount ++;
                            }
                        }
                        if (sizeof($subItemData) != $nonInventoryCount) {
                            //Todo assemble the with inventory items and handle je
                        }
                    // }
                // }
            } 

            if ($p['productType'] != 2 && $p['documentTypeID'] != 19) {
                if ($p['stockUpdate'] == 'true') {

                    if (!count($batchProducts) > 0) {
                        $sellingQty = $deliveryQty;
                        while ($sellingQty != 0) {

                            $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableProductDetails($locationProduct->locationProductID);
                            if (!$itemInDetails) {
                                break;
                            }
                            if ($itemInDetails['itemInRemainingQty'] > $sellingQty) {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $sellingQty;
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Sales Invoice',
                                    'itemOutDocumentID' => $salesInvoiceID,
                                    'itemOutLocationProductID' => $locationProduct->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $sellingQty,
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                if($this->useAccounting == 1 && !$isSetJournalEntry){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = $sellingQty * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $sellingQty * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);

                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }

                                $sellingQty = 0;
                                break;
                            } else {
                                $updateQty = $itemInDetails['itemInSoldQty'] + $itemInDetails['itemInRemainingQty'];
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Sales Invoice',
                                    'itemOutDocumentID' => $salesInvoiceID,
                                    'itemOutLocationProductID' => $locationProduct->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => NULL,
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => $itemInDetails['itemInRemainingQty'],
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                );
                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                if($this->useAccounting == 1 && !$isSetJournalEntry){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal =  $itemInDetails['itemInRemainingQty'] * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = $itemInDetails['itemInRemainingQty'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }

                                $sellingQty = $sellingQty - $itemInDetails['itemInRemainingQty'];
                            }
                        }
                    }
                }
            }
            if (count($batchProducts) > 0) {
                foreach ($batchProducts as $batchKey => $batchValue) {
                    if(!empty($batchValue)){
                        $batchValue = (array) $batchValue;
                        $this->saveInvoiceSubProductData($batchValue, $invoiceProductID);
                    //insert data to the item out table
                        if ($p['stockUpdate'] == 'true') {
                            if ($batchValue['batchID']) {
                                if ($batchValue['serialID']) {
                                    $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchSerialProductDetails($locationProduct->locationProductID, $batchValue['batchID'], $batchValue['serialID']);
                                    $itemOutM = new ItemOut();
                                    $itemOutData = array(
                                        'itemOutDocumentType' => 'Sales Invoice',
                                        'itemOutDocumentID' => $salesInvoiceID,
                                        'itemOutLocationProductID' => $locationProduct->locationProductID,
                                        'itemOutBatchID' => $batchValue['batchID'],
                                        'itemOutSerialID' => $batchValue['serialID'],
                                        'itemOutItemInID' => $itemInDetails['itemInID'],
                                        'itemOutQty' => $batchValue['qtyByBase'],
                                        'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                        'itemOutDiscount' => $discountValue,
                                        'itemOutAverageCostingPrice' => $averageCostingPrice,
                                        'itemOutDateAndTime' => $this->getGMTDateTime()
                                        );
                                    $itemOutM->exchangeArray($itemOutData);
                                    $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                    $updateQty = $itemInDetails['itemInSoldQty'] + $batchValue['qtyByBase'];
                                    $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);

                                    if($this->useAccounting == 1 && !$isSetJournalEntry){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                } else {
                                    $remainBatchQty = $batchValue['qtyByBase'];

                                    //check current batch qty zero or not

                                    while($remainBatchQty != 0) {

                                        //get first avilable batch record in itemin table
                                        $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableBatchProductDetails($locationProduct->locationProductID, $batchValue['batchID']);

                                        $itemOutM = new ItemOut();
                                        $itemOutData = array(
                                            'itemOutDocumentType' => 'Sales Invoice',
                                            'itemOutDocumentID' => $salesInvoiceID,
                                            'itemOutLocationProductID' => $locationProduct->locationProductID,
                                            'itemOutBatchID' => $batchValue['batchID'],
                                            'itemOutSerialID' => NULL,
                                            'itemOutItemInID' => $itemInDetails['itemInID'],
                                            'itemOutQty' => $batchValue['qtyByBase'],
                                            'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                            'itemOutDiscount' => $discountValue,
                                            'itemOutAverageCostingPrice' => $averageCostingPrice,
                                            'itemOutDateAndTime' => $this->getGMTDateTime()
                                            );
                                        //check existing record has enough qty
                                        if ($itemInDetails['itemInRemainingQty'] > $remainBatchQty) {
                                            $itemOutData['itemOutQty'] = $remainBatchQty;
                                            $updatedBatchQty = $remainBatchQty;    
                                            $remainBatchQty = 0;
                                        } else {

                                            if (!$itemInDetails['itemInRemainingQty']) {
                                                return array(
                                                    'status' => false,
                                                    'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                                                );
                                            }

                                            $itemOutData['itemOutQty'] = $itemInDetails['itemInRemainingQty'];
                                            $remainBatchQty = $remainBatchQty - $itemInDetails['itemInRemainingQty'];
                                            $updatedBatchQty = $itemInDetails['itemInRemainingQty'];
                                        }
                                        
                                        $itemOutM->exchangeArray($itemOutData);
                                        $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                        $updateQty = $itemInDetails['itemInSoldQty'] + $updatedBatchQty;
                                        $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], $updateQty);
                          
                                    }

                                    if($this->useAccounting == 1 && !$isSetJournalEntry){
                                        if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                            $productTotal = $batchValue['qtyByBase'] * $averageCostingPrice;
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }else{
                                            $productTotal = $batchValue['qtyByBase'] * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                            if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                                $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                                $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                            }

                                            if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                            }else{
                                                $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                                $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                                $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                            }
                                        }
                                    }
                                }
                            } else if ($batchValue['serialID']) {
                                // var_dump($batchValue);
                                $itemInDetails = $this->CommonTable('Inventory\Model\ItemInTable')->getFirstAvailableSerialProductDetails($locationProduct->locationProductID, $batchValue['serialID']);
                                if (is_null($itemInDetails)) {
                                    return array(
                                        'status' => false,
                                        'msg' => $this->getMessage('ERR_INVOICE_SERIAL_ID', [$p['productCode']])
                                        );
                                }
                                $itemOutM = new ItemOut();
                                $itemOutData = array(
                                    'itemOutDocumentType' => 'Sales Invoice',
                                    'itemOutDocumentID' => $salesInvoiceID,
                                    'itemOutLocationProductID' => $locationProduct->locationProductID,
                                    'itemOutBatchID' => NULL,
                                    'itemOutSerialID' => $batchValue['serialID'],
                                    'itemOutItemInID' => $itemInDetails['itemInID'],
                                    'itemOutQty' => 1,
                                    'itemOutPrice' => $p['productPrice'] * $customCurrencyRate,
                                    'itemOutDiscount' => $discountValue,
                                    'itemOutAverageCostingPrice' => $averageCostingPrice,
                                    'itemOutDateAndTime' => $this->getGMTDateTime()
                                    );

                                $itemOutM->exchangeArray($itemOutData);
                                $this->CommonTable('Inventory\Model\ItemOutTable')->saveItemOut($itemOutM);
                                $this->CommonTable('Inventory\Model\ItemInTable')->updateItemInSoldQtyDetails($itemInDetails['itemInID'], 1);

                                if($this->useAccounting == 1 && $p['giftCard'] != 1 && !$isSetJournalEntry){
                                    if($averageCostingFlag == 1){
                                        //set gl accounts for the journal entry
                                        $productTotal = 1 * $averageCostingPrice;
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }else{
                                        $productTotal = 1 * ($itemInDetails['itemInPrice'] - $itemInDetails['itemInDiscount']);
                                        if(isset($accountProduct[$pData['productInventoryAccountID']]['creditTotal'])){
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productInventoryAccountID']]['creditTotal'] = $productTotal;
                                            $accountProduct[$pData['productInventoryAccountID']]['debitTotal'] = 0.00;
                                            $accountProduct[$pData['productInventoryAccountID']]['accountID'] = $pData['productInventoryAccountID'];
                                        }

                                        if(isset($accountProduct[$pData['productCOGSAccountID']]['debitTotal'])){
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] += $productTotal;
                                        }else{
                                            $accountProduct[$pData['productCOGSAccountID']]['creditTotal'] = 0.00;
                                            $accountProduct[$pData['productCOGSAccountID']]['debitTotal'] =  $productTotal;
                                            $accountProduct[$pData['productCOGSAccountID']]['accountID'] = $pData['productCOGSAccountID'];
                                        }
                                    }
                                }
                            }
                        }
                    //////////Item out insert data END /////////
                        if ($p['stockUpdate'] == 'true' && $p['productType'] != 2) {
                            if ($batchValue['qtyByBase'] != 0 && $batchValue['batchID']) {
                                $batchProduct = $this->CommonTable('Inventory\Model\ProductBatchTable')->getBatchProducts($batchValue['batchID']);
                                if ($batchProduct->productBatchQuantity < $batchValue['qtyByBase']) {
                                    return array(
                                        'status' => false,
                                        'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                                        );
                                }
                                $productBatchQuentity = $batchProduct->productBatchQuantity - $batchValue['qtyByBase'];
                                $productBatchQty = array(
                                    'productBatchQuantity' => $productBatchQuentity,
                                    );
                                $this->CommonTable('Inventory\Model\ProductBatchTable')->updateProductBatchQuantity($batchValue['batchID'], $productBatchQty);
                                if ($batchValue['serialID']) {
                                    $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                                    if ($checkSerial->locationProductID != $locationProduct->locationProductID || $checkSerial->productSerialSold == 1) {

                                        return array(
                                            'status' => false,
                                            'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                                            );
                                    }
                                    $serialProductData = array(
                                        'productSerialSold' => '1',
                                        );
                                    $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                                }
                            } else if ($batchValue['qtyByBase'] != 0 && $batchValue['serialID']) {

                                $checkSerial = $this->CommonTable('Inventory\Model\ProductSerialTable')->getSerialProducts($batchValue['serialID']);
                                if ($checkSerial->locationProductID != $locationProduct->locationProductID || $checkSerial->productSerialSold == 1) {
                                    return array(
                                        'status' => false,
                                        'msg' => $this->getMessage('ERR_INVOICE_PRODETAILS_UPDATED', [$p['productCode']])
                                        );
                                }
                                $serialProductData = array(
                                    'productSerialSold' => '1',
                                    );
                                $this->CommonTable('Inventory\Model\ProductSerialTable')->updateProductSerialData($serialProductData, $batchValue['serialID']);
                                if ($p['giftCard'] == 1) {
                                    $psCode = $checkSerial->productSerialCode;
                                    $giftCardDetails = $this->CommonTable('Inventory\Model\GiftCardTable')->getGiftCardDetailsByGiftCardCode($psCode);
                                    $duration = $giftCardDetails->giftCardDuration;
                                    $issueDate = $invoiceDate;
                                    $expireDate = ($duration != 0) ? $this->convertDateToStandardFormat(date('Y-m-d', strtotime($issueDate . ' + ' . $duration . 'days'))) : NULL;
                                    $giftData = array(
                                        'giftCardIssueDate' => $issueDate,
                                        'giftCardExpireDate' => $expireDate,
                                        'giftCardStatus' => 3,
                                        );

                                    $this->CommonTable('Inventory\Model\GiftCardTable')->updateGiftCardDetailsByGiftCardCode($giftData, $psCode);


                                }
                            }
                        }
                    }
                }

                if($this->useAccounting == 1 && !$isSetJournalEntry){
                    if($p['giftCard'] == 1 && empty($giftCardDetails->giftCardGlAccountID)){
                        return array(
                            'status' => false,
                            'msg' => $this->getMessage('ERR_GIFTCARD_GL_ACCOUNT_NOT_SET', [$psCode])
                            );
                    }else if($p['giftCard'] == 1){
                                        //set gl accounts for the journal entry
                    //TODO after set the productTotal with tax value in pos should have to remove belowe condition the correct $pTotal is $p['productTotal'] - $p['pTax']['tTA'];
                        if($pos){
                            $pTotal = $p['productTotal'];
                        }else{
                            $pTotal = $p['productTotal'] - $p['pTax']['tTA'];
                        }
                        if($invoiceData['salesInvoiceTotalDiscountType'] == "Value"){
                            $productTotal = $pTotal - $invoiceData['salesInvoiceDiscountRate']*($pTotal/$totalProductAmounts);
                        }else {
                            $productTotal = $pTotal;
                        }
                        if($promotionID != '' && $promotionData[$promotionID]['promoType'] == 2 && $promotionData[$promotionID]['discountType'] == 1){
                            $productTotal = $productTotal - $invoiceData['salesInvoicePromotionDiscount']*($productTotal/$totalProductAmounts);
                        }

                        $totalCRAmounts+=$productTotal;
                        if(isset($accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'])){
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'] += $productTotal;
                        }else{
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['creditTotal'] = $productTotal;
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['debitTotal'] = 0.00;
                            $accountProduct[$giftCardDetails->giftCardGlAccountID]['accountID'] = $giftCardDetails->giftCardGlAccountID;
                        }
                    }
                }
            }

            //update unitprice on itemOut Table
            $itemOutDocumentType = NULL;
            $referenceID = NULL;
            if ($projectID != NULL || $jobID != NULL || $activityID != NULL) {

                $getActivity = array();
                if ($projectID) {

                    $referenceID = $projectID;
                    $itemOutDocumentType = 'Project';
                    $getActivity = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByProjectID($projectID);
                    $unitPrice = $p['productPrice'] * $customCurrencyRate;
                    foreach ($getActivity as $value) {
                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateJobCardUnitPrice($referenceID, $unitPrice, $itemOutDocumentType, $value['activityId']);
                    }
                } else if ($jobID) {

                    $referenceID = $jobID;
                    $itemOutDocumentType = 'Job';
                    $getActivity = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityListByJobID($jobID);
                    $unitPrice = $p['productPrice'] * $customCurrencyRate;
                    foreach ($getActivity as $value) {
                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateJobCardUnitPrice($referenceID, $unitPrice, $itemOutDocumentType, $value['activityId']);
                    }
                } else if ($activityID) {

                    $referenceID = $activityID;
                    $itemOutDocumentType = 'Activity';
                    $getActivity = $this->CommonTable('JobCard\Model\ActivityTable')->getActivityByActivityID($activityID);
                    $unitPrice = $p['productPrice'] * $customCurrencyRate;
                    foreach ($getActivity as $value) {
                        $this->CommonTable('Inventory\Model\ItemOutTable')->updateJobCardUnitPrice($referenceID, $unitPrice, $itemOutDocumentType, $value['activityId']);
                    }
                }
            }

            //update item out table unit price if it is delivery note
            //and if delivery note unit price is change when saving with changing unit price on invoice by delivery note
            if ($p['documentTypeID'] == 4 && $p['documentID'] != NULL) {
                $locationProductData = $this->CommonTable("Inventory\Model\LocationProductTable")->getLocationProduct($productID, $p['locationID']);
                $price = $p['productPrice'] * $customCurrencyRate;
                $locationProductID = $locationProductData->locationProductID;

                $data = array(
                    'itemOutPrice' => $price,
                );
                $this->CommonTable("Inventory\Model\ItemOutTable")->updateDeliveryNoteUnitPrice($data, $locationProductID, $p['documentID']);
            }
        }

         //set gl accounts for the journal entry
        $deliveryTotal = $deliveryCharge;
        if($this->useAccounting == 1 && !$isSetJournalEntry){
            $totalCRAmounts += $deliveryCharge;
            if($deliveryTotal > 0){
                if(isset($accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'])){
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] += $deliveryTotal;
                }else{
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['creditTotal'] = $deliveryTotal;
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['debitTotal'] = 0.00;
                    $accountProduct[$glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID]['accountID'] = $glAccountsData->glAccountSetupGeneralDeliveryChargersAccountID;
                }
            }

            if(sizeof($addedServiceChargeArray) > 0 && $serviceChargeAmount > 0){
                foreach ($addedServiceChargeArray as $key => $value) {
                    $value = (array) $value;
                    $totalCRAmounts += $value['serviceChargeValueTxt'];
                    $serviceChargeTypeData =  $this->CommonTable("Invoice\Model\ServiceChargeTypeTable")->getServiceChargeTypeDetailsById($value['serviceChargeTypeId'])->current();

                    if(isset($accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['creditTotal'])){
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['creditTotal'] += $value['serviceChargeValueTxt'];
                    }else{
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['creditTotal'] = $value['serviceChargeValueTxt'];
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['debitTotal'] = 0.00;
                        $accountProduct[$serviceChargeTypeData['serviceChargeTypeGlAccountID']]['accountID'] = $serviceChargeTypeData['serviceChargeTypeGlAccountID'];
                    }
                }
            }


            if ($param['suspendedTax'] == '1' && $pos == false) {
                $totalCRAmounts -= floatval($param['susTaxAmount']);
            }

            if(isset($accountProduct[$customerData->customerReceviableAccountID]['debitTotal'])){
                $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] += $totalCRAmounts;
            }else{
                $accountProduct[$customerData->customerReceviableAccountID]['debitTotal'] = $totalCRAmounts;
                $accountProduct[$customerData->customerReceviableAccountID]['creditTotal'] = 0.00;
                $accountProduct[$customerData->customerReceviableAccountID]['accountID'] = $customerData->customerReceviableAccountID;
            }
        }

        //update Sales Order status or Delivery Note status if copied from it
        if ($fromSalesOrderOrDeliveryNote == TRUE) {
            if (!empty($salesOrderID)) {
                $this->_checkAndUpdateSalesOrderStatus($salesOrderID);
            } else if (!empty($deliveryNoteID)) {
                //$this->_checkAndUpdateDeliveryNoteStatus($deliveryNoteID);
            } else if (!empty($activityID)) {
                $this->updateActivityStatus($activityID, $products);
            } else if (!empty($jobID)) {
                // $this->updateJobStatus($jobID, $products);
            } else if (!empty($projectID)) {
                $this->updateProjectStatus($projectID, $products);
            } else if (!empty($quotationID)) {
                $quotationProducts = $this->CommonTable('Invoice\Model\QuotationProductTable')->getUnCopiedQuotationProducts($quotationID);
                if (empty($quotationProducts)) {
                    $closeStatusID = $this->getStatusID('closed');
                    $this->CommonTable('Invoice\Model\QuotationTable')->updateQuotationStatus($quotationID, $closeStatusID);
                }
            }
        }
        $this->setLogMessage('total amount ' . $customCurrencySymbol . $totalPrice . ' of invoice successfully saved - ' . $invoiceCode);

        //call product updated event
        $productIDs = array_map(function($element) {
            $element = (array) $element;
            return $element['productID'];
        }, $products);

        $eventParameter = ['productIDs' => $productIDs, 'locationIDs' => [$locationOut]];
        $this->getEventManager()->trigger('productQuantityUpdated', $this, $eventParameter);

            
        if ($this->useAccounting == 1 ) {
                    //create data array for the journal entry save.
            if(!$isSetJournalEntry){

                $i=0;
                $journalEntryAccounts = array();

                foreach ($accountProduct as $key => $value) {
                    $journalEntryAccounts[$i]['fAccountsIncID'] = $i;
                    $journalEntryAccounts[$i]['financeAccountsID'] = $value['accountID'];
                    $journalEntryAccounts[$i]['financeGroupsID'] = '';
                    $journalEntryAccounts[$i]['journalEntryAccountsDebitAmount'] = $value['debitTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsCreditAmount'] = $value['creditTotal'];
                    $journalEntryAccounts[$i]['journalEntryAccountsMemo'] = 'Created By Invoice '.$invoiceCode.'.';
                    $i++;
                }
                    //get journal entry reference number.
                $jeresult = $this->getReferenceNoForLocation('30', $locationOut);
                $jelocationReferenceID = $jeresult['locRefID'];
                $JournalEntryCode = $this->getReferenceNumber($jelocationReferenceID);

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $invoiceDate,
                    'journalEntryCode' => $JournalEntryCode,
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => 'Journal Entry is posted when create Invoice '.$invoiceCode.'.',
                    'documentTypeID' => 1,
                    'journalEntryDocumentID' => $salesInvoiceID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                    );

            }else{

                $journalEntryAccounts = array();
                $journalEntryData = (array) $journalEntryData;
                $journalEntryData['journalEntryAccounts'] = (array) $journalEntryData['journalEntryAccounts'];
                foreach ($journalEntryData['journalEntryAccounts'] as $key => $value) {
                    $value = (array) $value;
                    if(empty($value['financeAccountsID'])){
                        return array(
                            'status'=> false,
                            'msg'=>$this->getMessage("ERR_PLEASE_SET_GL_ACCOUNT_IN_JOURNAL_ENTRY"),
                        );
                    }

                    $journalEntryAccounts[$key]['fAccountsIncID'] = $key;
                    $journalEntryAccounts[$key]['financeAccountsID'] = $value['financeAccountsID'];
                    $journalEntryAccounts[$key]['financeGroupsID'] = '';
                    $journalEntryAccounts[$key]['journalEntryAccountsDebitAmount'] = $value['journalEntryAccountsDebitAmount'];
                    $journalEntryAccounts[$key]['journalEntryAccountsCreditAmount'] = $value['journalEntryAccountsCreditAmount'];
                    $journalEntryAccounts[$key]['journalEntryAccountsMemo'] = $value['journalEntryAccountsMemo'];
                }

                $journalEntryData = array(
                    'journalEntryAccounts' => $journalEntryAccounts,
                    'journalEntryDate' => $journalEntryData['journalEntryDate'],
                    'journalEntryCode' => $journalEntryData['journalEntryCode'],
                    'journalEntryTypeID' => '',
                    'journalEntryIsReverse' => 0,
                    'journalEntryComment' => $journalEntryData['journalEntryComment'],
                    'documentTypeID' => 1,
                    'journalEntryDocumentID' => $salesInvoiceID,
                    'ignoreBudgetLimit' => $ignoreBudgetLimit,
                    );
            }




            $resultData = $this->saveJournalEntry($journalEntryData, null, null, $restFlag, $locationOut);

            if(!$resultData['status']){
                return array(
                    'status'=> false,
                    'data' => $resultData['data'],
                    'msg'=>$resultData['msg'],
                    );
            }

            if (!empty($dimensionData)) {
                $saveRes = $this->saveDimensionsForJournalEntry($dimensionData[$invoiceCode], $resultData['data']['journalEntryID'], $ignoreBudgetLimit, $journalEntryData['journalEntryAccounts'], $journalEntryData['journalEntryDate']);
                if(!$saveRes['status']){
                    return array(
                        'status'=> false,
                        'msg'=>$saveRes['msg'],
                        'data'=>$saveRes['data'],
                        );
                }   
            }

            $jEDocStatusUpdate = $this->updateDocumentStatusInJournalEntry($salesInvoiceID,1, 3);
            if(!$jEDocStatusUpdate['status']){
                return array(
                    'status'=> false,
                    'msg'=>$jEDocStatusUpdate['msg'],
                    );
            }            
        }

        if(!$edit && $customerID != 0){
            $displaySettings = (object) $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();

            if ($displaySettings->smsServiceStatus) {
                
                if ($cust->customerTelephoneNumber != "") {
                    $smsTypeDetails = (object) $this->CommonTable('Settings\Model\SmsTypeTable')->getSmsTypeBySmsTypeName("After Invoice");
                    
                    if($smsTypeDetails->isEnable && (int)$smsTypeDetails->smsSendType == 1){
                        
                        $message = $smsTypeDetails->smsTypeMessage ."\n";

                        $smsIncludedDetailsByType = (object) $this->CommonTable('Settings\Model\SmsTypeIncludedTable')->getSmsTypeIncludedBySmsTypeId($smsTypeDetails->smsTypeID);
                        $salesPerson = '';
                        if( sizeof($salesPersonIDArr) > 0){
                            foreach ($salesPersonIDArr as $key => $value) {
                                $salesPersonData =  $this->CommonTable("User/Model/SalesPersonTable")->getSalesPersonByID($value)->current();
                                if(sizeof($salesPersonIDArr) == 1){
                                    $salesPerson = $salesPersonData["salesPersonSortName"];
                                }else{
                                    $salesPerson .= $salesPersonData["salesPersonSortName"] .', ';
                                }
                            }
                        }
                        
                        if($smsTypeDetails->isIncludeInvoiceDetails){
                            while ($row = $smsIncludedDetailsByType->current()) {
                                if((bool) $row['isSelected']){
                                    switch ($row['smsIncludedName']) {
                                        case "Invoice Date":
                                            $message .=$row['smsIncludedName'] .' : '.$invoiceDate."\n";
                                            break;
                                        case "Invoice Number":
                                            $message .=$row['smsIncludedName'] .' : '.$invoiceCode."\n";
                                            break;
                                        case "Invoice Amount":
                                            $message .=$row['smsIncludedName'] .' : '.number_format($totalPrice , 2)."\n";
                                            break;
                                        case "Due Date":
                                            $message .=$row['smsIncludedName'] .' : '.$dueDate."\n";
                                            break;
                                        case "Sales Person":
                                            if( sizeof($salesPersonIDArr) > 0){
                                                $message .=$row['smsIncludedName'] .' : '.$salesPerson."\n";
                                            }
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                        $sendRes = $this->sendSms($cust->customerTelephoneNumber, $message);
                    }
                }
            }
        }
        return array(
            'status' => true,
            'data' => array('salesInvoiceID' => $salesInvoiceID, 'salesInvoiceCode' => $invoiceCode),
            'msg' => $this->getMessage('SUCC_INVOICE_ADD_INVO_PRODETAILS', array($invoiceCode)),
            'totalPrice' => $totalPrice
        );
    }

    /**
     * delete draft invoice
     * @return JSONRespond
     */
    public function deleteDraftInvoiceAction()
    {
        $request = $this->getRequest();
        if ($request->isPost()) {
            $this->beginTransaction();
            $invoiceID = $request->getPost('invoiceID');            

            $oldDraftInv = (array) $this->CommonTable('Invoice\Model\DraftInvWfTable')->getInvoiceByID($invoiceID);
            $invoiceCode = $oldDraftInv['salesInvoiceCode'];

            $oldDraftInv = (array) $oldDraftInv;

            $deleteOldDraftInv = $this->updateDeleteInfoEntity($oldDraftInv['entityID']);

           
            $this->setLogMessage('Draft Invoice '.$invoiceCode.' is deleted.');
            $this->commit();
            $this->status = TRUE;
            $this->msg = $this->getMessage('SUCC_DRAFT_INVOICE_CANCEL');
            
            return $this->JSONRespond();
        }
    }

}
