<?php

/**
 * @author ashan madushka <ashan@thinkcube.com>
 * This file contains Credit note Process related controller functions
 */

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Invoice\Form\CreditNoteForm;

class CreditNoteController extends CoreController
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'credit_note_upper_menu';
    protected $userID;
    protected $user_session;
    protected $company;
    private $_creditNoteViewData;
    private $useAccounting;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID = $this->user_session->userID;
        $this->company = $this->user_session->companyDetails;
        $this->cdnUrl = $this->user_session->cdnUrl;
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Credit Note', 'Create Credit Note', 'SALES');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(8, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];
        $displayData = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAllDetails()->current();


        $paymentterm = $this->CommonTable('Core\Model\PaymentTermTable')->fetchAll();
        while ($t = $paymentterm->current()) {
            $payterm[$t->paymentTermID] = $t->paymentTermName;
        }

        $today = date('Y-m-d');
        $promotions = $this->CommonTable('Settings\Model\PromotionTable')->getAllPromotion();
        $promotionData = array();
        foreach ($promotions as $pro) {
            $pro = (object) $pro;
            $promotionData[$pro->promotionID] = $pro;
        }

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();

        $creditNoteForm = new CreditNoteForm(
                array(
            'customCurrency' => $currency,
            'promotions' => $promotionData
                )
        );

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/creditNote.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/calculations.js');
        $creditNoteForm->get('customer')->setAttribute('disabled', true);
        $creditNoteAddView = new ViewModel(
                array(
            'paymentTerm' => $payterm,
            'creditNoteForm' => $creditNoteForm,
            'locationID' => $locationID,
            'locationCode' => $locationCode,
            'locationName' => $locationName,
            'referenceNumber' => $rid,
            'isActiveInvoiceReturn' => ($displayData['isUseSalesInvoiceReturn'] == 1) ? true : false,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
                )
        );
        $creditNoteProductsView = new ViewModel();
        $creditNoteProductsView->setTemplate('invoice/credit-note/credit-note-add-products');
        $creditNoteAddView->addChild($creditNoteProductsView, 'creditNoteAddProducts');
        $creditNotesubProductsView = new ViewModel();
        $creditNotesubProductsView->setTemplate('invoice/return/return-add-sub-products');
        $creditNoteAddView->addChild($creditNotesubProductsView, 'creditNoteAddSubProducts');
        $creditNoteAddView->addChild($dimensionAddView, 'dimensionAddView');
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Credit Note Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Credit Note Reference Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $creditNoteAddView->addChild($refNotSet, 'refNotSet');
        }
        return $creditNoteAddView;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Credit Note', 'View Credit Note', 'SALES');
        $this->getPaginatedCreditNotes();
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $creditNoteView = new ViewModel(array(
            'creditNote' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'locationID' => $locationID,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
                )
        );
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('invoice/credit-note/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/credit-note/document-view');
        $creditNoteView->addChild($docHistoryView, 'docHistoryView');
        $creditNoteView->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $creditNoteView->addChild($attachmentsView, 'attachmentsView');
        
        return $creditNoteView;
    }

    private function getPaginatedCreditNotes()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Invoice\Model\CreditNoteTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function viewCreditNoteReceiptAction()
    {
        return $this->documentPreviewAction();
    }

    public function documentPreviewAction()
    {
        $creditNoteID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($creditNoteID);
        $data['creditNoteID'] = $data['creditNoteID'];
        $data['total'] = number_format($data['total'], 2);
        $path = "/credit-note/document/"; //.$returnID;
        $createNew = "New Credit Note";
        $createPath = "/credit-note/create";

        if (!$data['customerName']) {
            $customerSalutaion = "";
        } else {
            $customerSalutaion = "Dear " . $data['customerName'] . ",";
        }


        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Credit Note from " . $data['companyName'],
            "body" => <<<EMAILBODY

{$customerSalutaion}<br />

Thank you for your inquiry. <br /><br />

A Credit Note has been generated for you from {$data['companyName']} and is attached herewith. <br /><br />

<strong>CN Number:</strong> {$data['creditNoteCode']} <br />
<strong>CN Amount:</strong> {$data['currencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $journalEntryValues = [];
        if($this->useAccounting == 1){
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('6',$creditNoteID);
            $journalEntryID = $journalEntryData['journalEntryID'];
            $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryValues = $jeResult['JEDATA']; 
        }

        $JEntry = new ViewModel(
            array(
             'journalEntry' => $journalEntryValues,
             'closeHidden' => true 
             )
            );
        $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $documentType = 'Credit Note';
        $preview =  $this->getCommonPreview($data, $path, $createNew, $documentType, $creditNoteID, $createPath);
        $preview->addChild($JEntry, 'JEntry');

        $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
        $additionalButton->setTemplate('additionalButton');
        $preview->addChild($additionalButton, 'additionalButton');

        return $preview;
    }

    public function getDataForDocument($creditNoteID)
    {

        if (!empty($this->_creditNoteViewData)) {
            return $this->_creditNoteViewData;
        }

        $data = $this->getDataForDocumentView();

        $creditNoteDetails = $this->CommonTable('Invoice\Model\CreditNoteTable')->getCreditNoteByCreditNoteID($creditNoteID)->current();

        $invoiceDetails = $this->CommonTable('Invoice\Model\InvoiceTable')->getInvoiceByID($creditNoteDetails['invoiceID']);

        $salesInvoiceSuspendedTaxFlag = false;
        if ($invoiceDetails->salesInvoiceSuspendedTax == "1") {
            $salesInvoiceSuspendedTaxFlag = true;
        }

        $customerDetails = $this->CommonTable('Invoice\Model\CustomerTable')->getCustomerByID($creditNoteDetails['customerID']);

        $customCurrencyId = $creditNoteDetails['customCurrencyId'];
        $creditNoteDetails['createdTimeStamp'] = $this->getUserDateTime($creditNoteDetails['createdTimeStamp']);

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId == "" || $customCurrencyId == 0) ? 1 : $creditNoteDetails['creditNoteCustomCurrencyRate'];
        $customCurrencySymbol = ($customCurrencyId == "" || $customCurrencyId == 0) ? $this->companyCurrencySymbol : $customCurrencyData->currencySymbol;

        $data = array_merge($data, $creditNoteDetails, (array) $customerDetails);

        $data['currencySymbol'] = $customCurrencySymbol;

        // to be used by below functions - convert to object
        $creditNoteDetails = (object) $creditNoteDetails;

        $creditNoteProductDetails = $this->CommonTable('Invoice\Model\CreditNoteProductTable')->getAllCreditNoteProductDetailsByCreditNoteID($creditNoteID);

        $txTypes = array();
        $sub_total = 0;
        $suspendedTaxAmount = 0;
        foreach ($creditNoteProductDetails as $product) {

            $product = (object) $product;

            if ($salesInvoiceSuspendedTaxFlag) {
                $invoiceProductTax = $this->CommonTable('Invoice\Model\InvoiceProductTable')->getInvoiceProductTaxDetailsByInvoiceProductId($product->invoiceProductID);        
                foreach ($invoiceProductTax as $value) {
                    if ($value['taxSuspendable'] == "1") {
                        $susTaxAmount = (floatval($value['salesInvoiceProductTaxAmount']) / floatval($value['salesInvoiceProductQuantity'])) * floatval($product->creditNoteProductQuantity);
                        $suspendedTaxAmount += $susTaxAmount;
                    }
                }
            }

            $total_itm_tx = 0;
            $productsTax = $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->getCreditNoteProductTaxByCreditNoteProductID($product->creditNoteProductID);
            foreach ($productsTax as $productTax) {
                if (!isset($txTypes[$productTax['taxName']])) {
                    $txTypes[$productTax['taxName']] = $productTax['creditNoteTaxAmount'] / $customCurrencyRate;
                } else {
                    $txTypes[$productTax['taxName']] += $productTax['creditNoteTaxAmount'] / $customCurrencyRate;
                }
                $total_itm_tx += $productTax['creditNoteTaxAmount'] / $customCurrencyRate;
            }

            $itemDiscount = ($product->creditNoteProductDiscountType == 'precentage') ? ($product->creditNoteProductPrice * $product->creditNoteProductDiscount / 100) / $customCurrencyRate : $product->creditNoteProductDiscount / $customCurrencyRate;
            $item_tax_string = ($total_itm_tx > 0) ? "Tax " . number_format($total_itm_tx, 2) : "";
            $item_disc_string = "";

            if ($itemDiscount > 0) {
                if ($product->creditNoteProductDiscountType == 'precentage') {
                    $item_disc_string = "Disc: " . number_format($product->creditNoteProductDiscount, 2) . "%";
                } else {
                    $item_disc_string = "Disc: " . $data['currencySymbol'] . number_format($itemDiscount, 2) . "";
                }
            }
            $sub_line = "";
            if ($total_itm_tx > 0) {
                if ($item_tax_string != "" && $item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                } else if ($item_tax_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ")</small>";
                } else if ($item_disc_string != "") {
                    $sub_line = "<br><small>(" . $item_tax_string . ",&nbsp;" . $item_disc_string . ")</small>";
                }
                $unit = $product->creditNoteProductPrice / $customCurrencyRate;
            } else {
                $unit = ($product->creditNoteProductPrice / $customCurrencyRate) + ($total_itm_tx / $product->creditNoteProductQuantity);
                $sub_line = ($product->creditNoteProductDiscount > 0) ? "<br><small>(" . $item_disc_string . ")</small>" : "";
            }

            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID);

            $unitPrice = $this->getProductUnitPriceViaDisplayUom($product->creditNoteProductPrice, $productUom) / $customCurrencyRate;
            $productQtyDetails = $this->getProductQuantityViaDisplayUom($product->creditNoteProductQuantity, $productUom);
            // Add dash for non inventory product
            $productQtyDetails['quantity'] = ($productQtyDetails['quantity'] == 0) ? '-' : $productQtyDetails['quantity'];

            $records[] = array(
                $product->productCode,
                $product->productName . $sub_line,
                $productQtyDetails['quantity'] . ' ' . $productQtyDetails['uomAbbr'],
                number_format($unitPrice, 2),
                number_format(($product->creditNoteProductTotal / $customCurrencyRate), 2),
            );
            $sub_total+= $product->creditNoteProductTotal / $customCurrencyRate;
        }

        foreach ($txTypes as $t_name => $t_val) {
            $tx_line.=($tx_line == null) ? $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2) :
                    ",&nbsp;" . $t_name . ":&nbsp; " . $customCurrencySymbol . " " . number_format($t_val, 2);
        }

        if ($suspendedTaxAmount > 0) {
            $tx_line.=($tx_line == null) ? "Suspended Tax(VAT):&nbsp; " . $customCurrencySymbol . " " . number_format($suspendedTaxAmount, 2) :
                    ",&nbsp;Suspended Tax(VAT):&nbsp; " . $customCurrencySymbol . " " . number_format($suspendedTaxAmount, 2);            
        }


         //get all tax details
        $taxDetails = $this->CommonTable('Settings\Model\TaxCompoundTable')->fetchAll();
        $compoundTaxes = [];

        foreach ($taxDetails as $key => $taxValue) {
            $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;

        }
        $txTypes = array();
        $totalTax = 0;
        $calculatableTaxesTotal = 0;
        $suspendedTaxesTotal = 0;
        $normalTaxTotal = 0;
        $compoundTaxTotal = 0;
        $calculatableTaxes = array();
        $suspendableTaxes = array();
        $normalTaxesSet = array();
        $compoundTaxesSet = array();
        $creditNoteTax = $this->CommonTable('Invoice\Model\CreditNoteProductTaxTable')->getCreditTax($creditNoteID);

        foreach ($creditNoteTax as $productTax) {

            $thisTax = $productTax['creditNoteTaxAmount'];
            if(array_key_exists($productTax['taxID'], $compoundTaxes)){
                    $compoundTaxTotal += floatval($thisTax);
                    $compoundTaxesSet[] = $productTax['taxName'];
            } else {
                    $normalTaxTotal += floatval($thisTax);
                    $normalTaxesSet[] = $productTax['taxName'];
            }


            // $txTypes[$productTax['taxName']] = (!isset($txTypes[$productTax['taxName']])) ? $thisTax : $txTypes[$productTax['taxName']] + $thisTax;
            // $calculatableTaxesTotal += ($productTax['taxSuspendable'] != 1) ? $thisTax : 0;
            // $suspendedTaxesTotal += ($productTax['taxSuspendable'] == 1) ? $thisTax : 0;
            // $totalTax += $thisTax;

            // if ($productTax['taxSuspendable'] == 1) {
            //     if ($invoiceDetails['salesInvoiceSuspendedTax'] == 1) {
            //         $suspendableTaxes[] = $productTax['taxName'];                        // }
            //     }else{
            //         $calculatableTaxes[] = $productTax['taxName'];
            //     }
            // } else {
            //     $calculatableTaxes[] = $productTax['taxName'];
            // }
        }
        $data['normalTaxTotal'] = $normalTaxTotal;
        $data['compoundTaxTotal'] = $compoundTaxTotal;
        $data['type'] = _("Credit Note");
        $data['cust_name'] = '';
        $data['state'] = $creditNoteDetails->statusID;
        $data['doc_data'] = array(
            _("Date") => $creditNoteDetails->creditNoteDate,
            _("Valid till") => '',
            _("Payment terms") => '',
            _("Delivery Note No.") => '',
            _("Credit Note No") => $creditNoteDetails->creditNoteCode,
        );
        $data['cust_data'] = array(
            _("address") => '',
        );
        $data['table'] = array(
            'col_size' => array(10, 90, 290, 50, 100, 100),
            'col_allign' => array("left", "left", "left", "right", "right"),
            'headers' => array(
                _("product code") => 2,
                _("product") => 1,
                _("return qty") => 1,
                _("price") => 1,
                _("total") => 1
            ),
            'records' => $records,
        );

        if ($creditNoteID == 'template') {
            $data['template'] = true;
            $data['creditNoteCode'] = '[creditNoteCode]';
            $data['creditNoteIssuedDate'] = '[creditNoteIssuedDate]';
            $data['current_time'] = '[current_time]';
            $data['userName'] ='[userName]';
            
        }

        $data['comment'] = $creditNoteDetails->creditNoteComment;
        $data['sub_total'] = $sub_total;
        $data['total'] = $creditNoteDetails->creditNoteTotal / $customCurrencyRate;
        $data['discount'] = '';
        $data['discountStatement'] = '';
        $data['show_tax'] = true;
        $data['tax_record'] = $tx_line;
        if ($data['customerID']) {
            $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];
        } else {
            $data['customerName'] = '';
        }
        return $this->_creditNoteViewData = $data;
    }

    public function documentAction()
    {
        $documentType = 'Credit Note';
        $creditNoteID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        if ($templateID == 'posCN') {
            $documentType = 'POS Credit Note';
            $templateID = $this->params()->fromRoute('param3');
        }
        echo $this->generateDocument($creditNoteID, $documentType, $templateID);
        exit;
    }

    public function getDocumentDataTable($creditNoteID, $documentSize = 'A4')
    {

        $data_table_vars = $this->getDataForDocument($creditNoteID);
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/credit-note/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('ViewRenderer')
                        ->render($view);
    }

    public function documentPdfAction()
    {
        $creditNoteID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');
        $documentType = 'Credit Note';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/invoice/quotation/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($creditNoteID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($creditNoteID, $documentType, $documentData, $templateID);

        return;
    }

}

/////////////////// END OF RETURN CONTROLLER \\\\\\\\\\\\\\\\\\\\







