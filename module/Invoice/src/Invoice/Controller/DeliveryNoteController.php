<?php

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\Session\Container;
use Zend\View\Model\ViewModel;
use Invoice\Form\DeliveryNoteForm;
use Invoice\Form\AddCustomerForm;
use Core\Controller\Interfaces\DocumentInterface;
use Zend\I18n\Translator\Translator;

/**
 * @author Damith Thamara <damith@thinkcube.com>
 */
class DeliveryNoteController extends CoreController implements DocumentInterface
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'deliverynote_upper_menu';
    protected $paginator;
    protected $_deliveryNoteViewData;
    protected $useAccounting;

     public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->useAccounting = $this->user_session->useAccounting;
        $this->packageID = $this->user_session->packageID;
        if ($this->packageID == "1" || $this->packageID == "2") {
            $this->sideMenus = 'invoice_side_menu'.$this->packageID;
        }
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Delivery Notes', 'Create Delivery Note', 'SALES');
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];
        $refData = $this->getReferenceNoForLocation(6, $locationID);
        $rid = $refData["refNo"];
        $lrefID = $refData["locRefID"];

//      Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();
        //get price list
        $priceList = $this->getActivePriceList();

        $deliveryNoteForm = new DeliveryNoteForm(
                array(
            'salesPersons' => $salesPersons,
            'customCurrency' => $currency,
            'priceList' => $priceList,
                )
        );

        $displaySetup = $this->CommonTable('Settings\Model\DisplaySetupTable')->fetchAll()->current();
        $deliveryNoteForm->get('customCurrencyId')->setValue($displaySetup->currencyID);
        $deliveryNoteForm->get('discount')->setAttribute('id', 'deliveryNoteDiscount');
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/deliveryNote.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/calculations.js');

        $userdateFormat = $this->getUserDateFormat(); ////////

        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');


        $deliveryNoteAddView = new ViewModel(
                array(
            'deliveryNoteForm' => $deliveryNoteForm,
            'locationID' => $locationID,
            'locationName' => $locationName,
            'locationCode' => $locationCode,
            'referenceNumber' => $rid,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'packageID' => $this->packageID,
            'userdateFormat' => $userdateFormat
                )
        );
        $deliveryNoteProductsView = new ViewModel();
        $deliveryNoteProductsView->setTemplate('invoice/delivery-note/delivery-note-add-products');
        $deliveryNoteAddView->addChild($deliveryNoteProductsView, 'deliveryNoteAddProducts');
        $deliveryNoteAddView->addChild($dimensionAddView, 'dimensionAddView');
        if ($rid == '' || $rid == NULL) {
            if ($lrefID == null) {
                $title = 'Delivery Note Reference Number not set';
                $msg = $this->getMessage('ERR_DELINOTECON_ADD_REF');
            } else {
                $title = 'Delivery Note Reference Number has reache the maximum limit ';
                $this->getMessage('ERR_DELINOTECON_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $deliveryNoteAddView->addChild($refNotSet, 'refNotSet');
        }
        return $deliveryNoteAddView;
    }

    public function viewAction()
    {
        $this->getSideAndUpperMenus('Delivery Notes', 'View Delivery Notes', 'SALES');
        $this->setLogMessage("Delivery Note View List accessed");

        $this->getPaginatedDeliveryNotes();

        $userdateFormat = $this->getUserDateFormat();
        $invoiceView = new ViewModel(array(
            'deliveryNote' => $this->paginator,
            'statuses' => $this->getStatusesList(),
            'userdateFormat' => $userdateFormat,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
                )
        );
        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars([
            'deliveryNote',
            'allCustomerNames'
        ]);
        $docHistoryView = new ViewModel();
        $docHistoryView->setTemplate('invoice/delivery-note/doc-history');
        $documentView = new ViewModel();
        $documentView->setTemplate('invoice/delivery-note/document-view');
        $invoiceView->addChild($docHistoryView, 'docHistoryView');
        $invoiceView->addChild($documentView, 'documentView');

        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $invoiceView->addChild($attachmentsView, 'attachmentsView');

        return $invoiceView;
    }

    public function documentAction()
    {
        $documentType = 'Delivery Note';
        $deliveryNoteID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        echo $this->generateDocument($deliveryNoteID, $documentType, $templateID);
        exit;
    }

    public function getDataForDocument($deliveryNoteID)
    {

        if (!empty($this->_deliveryNoteViewData)) {
            return $this->_deliveryNoteViewData;
        }

        $itemAttrDetails = $this->CommonTable('Settings\Model\ItemAttributeTable')->fetchAll();
        $itemAttrDetailArray = [];
        foreach ($itemAttrDetails as $value) {
            $itemAttrDetailArray[$value['itemAttributeID']] = $value['itemAttributeName'];
        }

        $data = $this->getDataForDocumentView();

        $deliveryNoteDetails = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->getDeliveryNoteDetailsByID($deliveryNoteID)->current();
        $deliveryNoteDetails['deliveryNoteDeliveryDate'] = $this->convertDateToUserFormat($deliveryNoteDetails['deliveryNoteDeliveryDate']);
        $products = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteProductByID($deliveryNoteID);

        $customCurrencyId = $deliveryNoteDetails['customCurrencyId'];
        $deliveryNoteDetails['createdTimeStamp'] = $this->getUserDateTime($deliveryNoteDetails['createdTimeStamp']);

        //get customCurrency data by customer CurrencyId
        $customCurrencyData = (object) $this->CommonTable('Core\Model\CurrencyTable')->getCurrency($customCurrencyId);
        $customCurrencyRate = ($customCurrencyId != "") ? $deliveryNoteDetails['deliveryNoteCustomCurrencyRate'] : 1;
        $customCurrencySymbol = ($customCurrencyId != "") ? $customCurrencyData->currencySymbol : $data['currencySymbol'];
        $data['currencySymbol'] = $customCurrencySymbol;

        $deliveryNoteDetails['deliveryNoteLocation'] = $this->CommonTable('Core\Model\LocationTable')->getLocationByID($deliveryNoteDetails['deliveryNoteLocationID'])->locationName;

        $address =array (
            $deliveryNoteDetails['customerProfileLocationNo'], 
            $deliveryNoteDetails['customerProfileLocationRoadName1'],
            $deliveryNoteDetails['customerProfileLocationRoadName2'],
            $deliveryNoteDetails['customerProfileLocationRoadName3'],
            $deliveryNoteDetails['customerProfileLocationSubTown'],
            $deliveryNoteDetails['customerProfileLocationTown'],
            $deliveryNoteDetails['customerProfileLocationPostalCode'],
            $deliveryNoteDetails['customerProfileLocationCountry']
        );
        $custAddress = implode(',',array_filter($address));
        

        $deliveryNoteDetails['customerAddress'] = $custAddress;
        $deliveryNoteDetails['customerEmail'] = $deliveryNoteDetails['customerProfileEmail'];

        $data = array_merge($data, $deliveryNoteDetails);


        // to be used by below functions - convert to object
        $deliveryNoteDetails = (object) $deliveryNoteDetails;

        //get all tax details
        $taxDetails = $this->CommonTable('Settings\Model\TaxCompoundTable')->fetchAll();
        $compoundTaxes = [];

        foreach ($taxDetails as $key => $taxValue) {
            $compoundTaxes[$taxValue->compoundTaxID] = $taxValue->compoundTaxID;
        }


        $sub_total = 0;
        $productsTax = $this->CommonTable('Invoice\Model\DeliveryNoteProductTaxTable')->getDeliveryNoteTax($deliveryNoteID);
        $txTypes = array();
        $totalTax = 0;
        $calculatableTaxesTotal = 0;
        $normalTaxTotal = 0;
        $compoundTaxTotal = 0;
        $normalTaxesSet = array();
        $compoundTaxesSet = array();
        foreach ($productsTax as $productTax) {

            $thisTax = $productTax['deliveryNoteTaxAmount'] / $customCurrencyRate;
            if(array_key_exists($productTax['deliveryNoteTaxID'], $compoundTaxes)){
                    $compoundTaxTotal += floatval($thisTax);
                    $compoundTaxesSet[] = $productTax['taxName'];
            } else {
                    $normalTaxTotal += floatval($thisTax);
                    $normalTaxesSet[] = $productTax['taxName'];
            }


            $txTypes[$productTax['taxName']] = (!isset($txTypes[$productTax['taxName']])) ? $thisTax : $txTypes[$productTax['taxName']] + $thisTax;
            $calculatableTaxesTotal += ($productTax['taxSuspendable'] != 1) ? $thisTax : 0;
            $suspendedTaxesTotal += ($productTax['taxSuspendable'] == 1) ? $thisTax : 0;
            $totalTax += $thisTax;
        }

        $data['calculatableTaxesTotal'] = $calculatableTaxesTotal;
        $data['normalTaxTotal'] = number_format($normalTaxTotal, 2);
        $data['normalTaxesSet'] = (!empty($normalTaxesSet)) ? implode(', ', array_unique($normalTaxesSet)) : '';
        $data['compoundTaxesSet'] = (!empty($compoundTaxesSet)) ? implode(', ', array_unique($compoundTaxesSet)) : '';
        $data['compoundTaxTotal'] = number_format($compoundTaxTotal, 2);

        // get template specific data for the document
        $templateDefaultOptions = array('product_table' => $this->getServiceLocator()->get('config')['template-data-table']['delivery_note']);

        // get template options from database and override the default options
        $templateOptions = array_replace_recursive($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);

        $defaultFooterRows = [];
        foreach ($templateDefaultOptions['product_table']['rows'] as $row) {
            $defaultFooterRows[$row['name']] = $row;
        }

        $templateFooterRows = [];
        foreach ((array) $this->templateDetails['templateOptions']['product_table']['rows'] as $row) {
            $templateFooterRows[$row['name']] = $row;
        }

        $templateOptions['product_table']['rows'] = $templateFooterRows + $defaultFooterRows;

        $data['templateOptions'] = $templateOptions;

        // reorder columns
        $productsTableColumns = array_filter($templateOptions['product_table']['columns'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableColumns, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        // reorder rows
        $productsTableRows = array_filter($templateOptions['product_table']['rows'], function($val) {
            return $val['show'] === true || $val['show'] == 'true';
        });

        usort($productsTableRows, function($a, $b) {
            return $a['order'] - $b['order'];
        });

        $templateOptions['product_table']['columns'] = $productsTableColumns;
        $templateOptions['product_table']['rows'] = $productsTableRows;

        $allowedColumnsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['columns']);

        $allowedRowsList = array_map(function($val) {
            return $val['name'];
        }, $templateOptions['product_table']['rows']);


        // to prevent loading categories from the database in the following foreach loop
        $allCategories = array();
        if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
            $allCategories = $this->CommonTable('Inventory/Model/CategoryTable')->getCategories();
        }


        $sub_total = 0;
        $tx_types = array();
        $all_tx = array();
        
        $deliveryNoteSerialProducts = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteSerialProductByDeliveryNoteId($deliveryNoteID);
        $deliveryNoteBatchProducts  = $this->CommonTable('Invoice\Model\DeliveryNoteProductTable')->getDeliveryNoteBatchProductByDeliveryNoteId($deliveryNoteID);
        
        $deliveryNoteSubProducts = [];
        foreach ($deliveryNoteBatchProducts as $subProduct) {
            $deliveryNoteProductId = $subProduct['deliveryNoteProductID'];
            $productBatchId        = $subProduct['productBatchID'];
            $productBatchCode      = $subProduct['productBatchCode'];
            
            $batchData = [
                'code' => $productBatchCode,
                'qty' => $subProduct['deliveryProductSubQuantity'],
                'type' => 'batch'
            ];

            // For batch serial items
            // Eg: if 5 serial items from 2 batches were invoiced,
            // check if batch data is already in array and increment it's quantity by 1
            if ($subProduct['serialProduct'] == 1 && isset($deliveryNoteSubProducts[$deliveryNoteProductId][$productBatchId])) {
                $batchData['qty'] = $deliveryNoteSubProducts[$deliveryNoteProductId][$productBatchId]['qty'] + $batchData['qty'];
            }
            
            $deliveryNoteSubProducts[$deliveryNoteProductId][$productBatchId] = $batchData;
        }
        
        foreach ($deliveryNoteSerialProducts as $subProduct) {
            $deliveryNoteProductId = $subProduct['deliveryNoteProductID'];
            $productSerialId       = $subProduct['productSerialID'];
            $productSerialCode     = $subProduct['productSerialCode'];
            $productBatchId        = $subProduct['productBatchID'];
            $warranty              = $this->calculateWarrantyPeriod($subProduct['productSerialWarrantyPeriod'],$subProduct['productSerialWarrantyPeriodType']);
        
            $serialData = [
                'code' => $productSerialCode,
                'warranty' => $warranty,
                'type' => 'serial'
            ];
            
            if ($productBatchId) {
                $deliveryNoteSubProducts[$deliveryNoteProductId][$productBatchId]['type'] = 'batch_serial';
                $deliveryNoteSubProducts[$deliveryNoteProductId][$productBatchId]['serial'][$productSerialId] = $serialData;
            } else {
                $deliveryNoteSubProducts[$deliveryNoteProductId][$productSerialId] = $serialData;
            }            
        }
        
        $subTotalWithoutDiscount = 0;
        foreach ($products as $productIndex => $product) {
            $product = (object) $product;            
            $productUom = $this->CommonTable('Inventory\Model\ProductUomTable')->getProductUomListByProductID($product->productID);

            $qty = ($product->deliveryNoteProductQuantity == 0) ? 1 : $product->deliveryNoteProductQuantity; // as a precaution
            if ($product->productTypeID == 2 && $product->deliveryNoteProductQuantity == 0) {
                $qty = 1;
            }
            $item_tot_tax = ($product->taxTotal > 0) ? $this->getProductUnitPriceViaDisplayUom((($product->taxTotal / $customCurrencyRate) / $product->deliveryNoteProductQuantity), $productUom) : 0.00;
            $item_tax_string = ($item_tot_tax > 0) ? "Tax: " . $data['currencySymbol'] . number_format($item_tot_tax, 2) : "";
            if ($product->deliveryNoteProductDiscountType == 'precentage') {
                $item_dic_string = ($product->deliveryNoteProductDiscount > 0) ? "Disc: " . $product->deliveryNoteProductDiscount . "%" : "";
                $product->totalDiscount = (($product->deliveryNoteProductPrice / $customCurrencyRate) * $qty) / 100 * $product->deliveryNoteProductDiscount;
            } else {
                $item_dic_string = ($product->deliveryNoteProductDiscount > 0) ? "Disc: " . $this->getProductUnitPriceViaDisplayUom(($product->deliveryNoteProductDiscount / $customCurrencyRate), $productUom) . " " . $data['currencySymbol'] : "";
                $product->totalDiscount = $qty * ($product->deliveryNoteProductDiscount / $customCurrencyRate);
            }

            // check if any subproducts (serial, batch) are available for this product
            $deliveryNoteSubProductsDisplay = [];
            if (isset($deliveryNoteSubProducts[$product->deliveryNoteProductID])) {
                
                foreach ($deliveryNoteSubProducts[$product->deliveryNoteProductID] as $subProduct) {
                    switch ($subProduct['type']) {                        
                        case 'batch':
                            $qty = $this->getProductQuantityViaDisplayUom($subProduct['qty'], $productUom);
                            $displayStrs = [];
                            $displayStrs[] = 'Batch: ' . $subProduct['code'];
                            $displayStrs[] = 'Quantity: ' . $qty['quantity'] . ' ' . $qty['uomAbbr'];

                            $deliveryNoteSubProductsDisplay[] = implode(', ', $displayStrs);
                            break;
                        case 'serial':
                            $displayStrs = [];
                            $displayStrs[] = 'Serial No: ' . $subProduct['code'];
                            $displayStrs[] = ($subProduct['warranty']) ? 'Warranty: ' . $subProduct['warranty'] : null;

                            $deliveryNoteSubProductsDisplay[] = implode(', ', array_filter($displayStrs));
                            break;
                        case 'batch_serial':
                            $qty = $this->getProductQuantityViaDisplayUom($subProduct['qty'], $productUom);
                            $displayStrs = [];
                            $displayStrs[] = 'Batch: ' . $subProduct['code'];
                            $displayStrs[] = 'Quantity: ' . $qty['quantity'] . ' ' . $qty['uomAbbr'];

                            $deliveryNoteSubProductsDisplay[] = implode(', ', $displayStrs);

                            foreach ($subProduct['serial'] as $serialSubProduct) {
                                $displayStrs = [];
                                $displayStrs[] = '- Serial No: ' . $serialSubProduct['code'];
                                $displayStrs[] = ($serialSubProduct['warranty']) ? 'Warranty: ' . $serialSubProduct['warranty'] : null;

                                $deliveryNoteSubProductsDisplay[] = implode(', ', array_filter($displayStrs));
                            }
                            break;
                    }
                }
            }
            
            $item_sub_pro_line = "";
            if (!empty($deliveryNoteSubProductsDisplay)) {
                $item_sub_pro_line = "<br><small>(" . implode("<br>", $deliveryNoteSubProductsDisplay) . ")</small>";
            }

            //add sub product serial and warranty details to the product array.
            $product->subProductserialAndWarranty = $item_sub_pro_line;

            $totalDiscount += floatval($product->totalDiscount);
            $sub_line = "";

            $productDescLine = "";
            
            $product->unitPrice = (floatval($product->deliveryNoteProductPrice / $customCurrencyRate) + floatval($product->totalDiscount)) / floatval($qty);
            $sub_line = ($product->deliveryNoteProductDiscount > 0) ? "<br><small>(" . $item_dic_string . ")</small>" : "";


            // get template options from database and override the default options
            $templateDefaultOptions = array(
                'categorize_products' => false
            );

            //use to set discount and unite price when product tax exists.
            $totalTaxValuePerItem = 0;

            if ($product->productTaxEligible == '1') {
                if ($product->deliveryNoteProductDiscountType == 'precentage') {
                    $totalTaxValuePerItem = ((floatval($product->taxTotal / $customCurrencyRate) * 100) / (100 - floatval($product->deliveryNoteProductDiscount))) / floatval($product->deliveryNoteProductQuantity);
                    $product->unitPrice = floatval($totalTaxValuePerItem) + floatval($product->deliveryNoteProductPrice / $customCurrencyRate);
                    $product->totalDiscount = ($product->unitPrice - (floatval($product->deliveryNoteProductTotal / $customCurrencyRate) / floatval($product->deliveryNoteProductQuantity))) * floatval($product->deliveryNoteProductQuantity);
                }
            }

            $unitPrice = $this->getProductUnitPriceViaDisplayUom(($product->unitPrice), $productUom);
            $thisqty = $this->getProductQuantityViaDisplayUom($product->deliveryNoteProductQuantity, $productUom);
            $thisqty['quantity'] = ($thisqty['quantity'] == 0) ? '-' : $thisqty['quantity'];

            $lineItemDiscount = ($product->deliveryNoteProductDiscount > 0) ? (($product->deliveryNoteProductDiscountType == 'value') ? number_format($product->totalDiscount, 2) : $product->deliveryNoteProductDiscount . '%') : '-';
            $itemTotalWithoutDiscount = (($product->deliveryNoteProductPrice * $product->deliveryNoteProductQuantity) + $product->taxTotal) / $customCurrencyRate;

            
            //calculate tax value per item
            $itemUnitTax = floatval($product->taxTotal / $customCurrencyRate)/ floatval($product->deliveryNoteProductQuantity);

            //calculate discount value per item
            $itemUnitDisc = floatval($product->totalDiscount / $customCurrencyRate)/ floatval($product->deliveryNoteProductQuantity);

            //calculate unit tax for real item price
            $realUnitTax = $this->calculateProductUnitTax($product->deliveryNoteProductPrice, $product->productID);

            $attribute_01 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeOneID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_02 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeTwoID,$product->productID)->current()['itemAttributeValueDescription'];
            $attribute_03 = $this->CommonTable('Settings\Model\ItemAttributeValueTable')->getSubValueByItemAtributeIdAndProductID($this->templateItemAtrributeDetails->attributeThreeID,$product->productID)->current()['itemAttributeValueDescription'];

            $product_record = array(
                'product_index' => $productIndex + 1,
                'product_code' => $product->productCode,
                'product_name' => $product->productName . $productDescLine . $sub_line . $item_sub_pro_line,
                'product_description' => $productDiscription,
                'product_code_and_name' => $product->productCode . ' - ' . $product->productName . $productDescLine . $sub_line . $item_sub_pro_line,
                'quantity' => $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'],
                'price' => number_format($unitPrice, 2),
                'price_w_tax' => number_format((floatval($product->deliveryNoteProductPrice) + floatval($realUnitTax)),2 ),
                'discount' => number_format($product->totalDiscount, 2),
                'discount_per_or_val' => $lineItemDiscount,
                'total' => number_format(($product->deliveryNoteProductTotal / $customCurrencyRate), 2),
                'total_wo_tx' => number_format(($product->deliveryNoteProductTotal - $product->taxTotal) / $customCurrencyRate, 2),
                'total_wo_discounts' => number_format($itemTotalWithoutDiscount, 2),
                'attribute_01' => (!is_null($attribute_01)) ? $attribute_01 : '-',
                'attribute_02' => (!is_null($attribute_02)) ? $attribute_02 : '-',
                'attribute_03' => (!is_null($attribute_03)) ? $attribute_03 : '-',
            );

            $subTotalWithoutDiscount = $subTotalWithoutDiscount + floatval($itemTotalWithoutDiscount);

            $templateOptions = array_merge($templateDefaultOptions, (array) $this->templateDetails['templateOptions']);
            // filter only the columns that need to be displayed
            $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

            // nest products under category if they should be categorized
            if ($templateOptions['categorize_products'] && $templateOptions['categorize_products'] != "false") {
                $categoryId = $product->categoryID;
                $records[$categoryId]['products'][] = $product_record;

                // add category name to the list
                if (!isset($records[$categoryId]['category'])) {
                    $records[$categoryId]['category'] = (isset($allCategories[$categoryId])) ? $allCategories[$categoryId] : ''; // change this to category name
                }
            } else {
                $records[0]['products'][] = $product_record;
            }

            $sub_total+= ($product->deliveryNoteProductTotal / $customCurrencyRate);

            $product->unitPrice = $unitPrice;
            $product->itemQuntity = $thisqty['quantity'] . ' ' . $thisqty['uomAbbr'];
            $data['productsList'][] = (array) $product;
        }

        $tx_line = "";
        foreach ($txTypes as $tx => $vl) {
            $tx_line .= ($tx_line) ? ",&nbsp;" . $tx . ":&nbsp;" . $data['currencySymbol'] . number_format($vl, 2) :
                    $tx . ":&nbsp;" . $data['currencySymbol'] . number_format($vl, 2);
        }

        $tx_line = trim($tx_line, "&nbsp;");


        $headers = array(
            'product_index' => array('label' => _(""), 'align' => 'left'),
            'product_code' => array('label' => _("Item Code"), 'align' => 'left'),
            'product_name' => array('label' => _("Item Name"), 'align' => 'left'),
            'product_code_and_name' => array('label' => _("Item Code & Name"), 'align' => 'left'),
            'product_description' => array('label' => _("Product Description"), 'align' => 'left'),
            'attribute_01' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeOneID]), 'align' => 'left'),
            'attribute_02' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeTwoID]), 'align' => 'left'),
            'attribute_03' => array('label' => _($itemAttrDetailArray[$this->templateItemAtrributeDetails->attributeThreeID]), 'align' => 'left'),
            'quantity' => array('label' => _("Qty"), 'align' => 'left'),
            'price' => array('label' => _("Unit Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'price_w_tax' => array('label' => _("Unit Price</br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'discount' => array('label' => _("Total Discount (-)"), 'align' => 'right'),
            'discount_per_or_val' => array('label' => _("Discount (-)"), 'align' => 'right'),
            'total' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_tx' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
            'total_wo_discounts' => array('label' => _("total </br>" . "(" . $data['currencySymbol'] . ")"), 'align' => 'right'),
        );

        // filter only the headers that need to be displayed
        $headers = array_splice(array_replace(array_flip($allowedColumnsList), $headers), 0, count($allowedColumnsList));

        // set column width
        if (isset($templateOptions['product_table']['columns'])) {
            foreach ($templateOptions['product_table']['columns'] as $column) {
                $columnName = $column['name'];
                $columnWidth = $column['width'];

                // if column is in allowed list
                if (isset($headers[$columnName])) {
                    $headers[$columnName]['width'] = $columnWidth;
                }
            }
        }

        $product_record = count($product_record) ? $product_record : [];
        $product_record = array_splice(array_replace(array_flip($allowedColumnsList), $product_record), 0, count($allowedColumnsList));

        // set variables
        $data['table'] = array(
            'hide_table_header' => ($templateOptions['product_table']['hide_table_header'] === true || $templateOptions['product_table']['hide_table_header'] == 'true'),
            'hide_table_borders' => ($templateOptions['product_table']['hide_table_borders'] === true || $templateOptions['product_table']['hide_table_borders'] == 'true'),
            'headers' => $headers,
            'records' => $records,
        );


        $itemWiseDiscount = $totalDiscount;
        $data['delivery_charge'] = $deliveryNoteDetails->deliveryNoteCharge / $customCurrencyRate;
        $data['comment'] = $deliveryNoteDetails->deliveryNoteComment;
        $data['sub_total'] = $sub_total;
        $data['total'] = $deliveryNoteDetails->deliveryNotePriceTotal / $customCurrencyRate;
        $data['discount'] = $totalDiscount;
        $data['tax_record'] = "<strong>" . _("Tax:") . "</strong>&nbsp;";
        $data['tax_record_line'] = $tx_line;
        $data['discountStatement'] = _("Discount received:");
        $data['customerName'] = $data['customerTitle'] . ' ' . $data['customerName'];
        $data['sub_total_wo_tax'] = $sub_total - $totalTax;
        $data['sub_total_wo_discounts'] = $subTotalWithoutDiscount;
        $data['totalTax'] = number_format($totalTax, 2);
        $data['discount'] = number_format($totalDiscount, 2); // total discount received

        return $this->_deliveryNoteViewData = $data;
    }

    public function getDocumentDataTable($salesOrderCode, $documentSize = 'A4')
    {

        $data_table_vars = $this->getDataForDocument($salesOrderCode);

        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/delivery-note/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    // Displays the overlay which loads an iframe inside (calls documentAction)
    public function previewAction()
    {
        return $this->documentPreviewAction();
    }

    public function documentPreviewAction()
    {

        $deliveryNoteID = $this->params()->fromRoute('param1');
        $data = $this->getDataForDocument($deliveryNoteID);
        $data['deliveryNoteID'] = $deliveryNoteID;
        $data['total'] = number_format($data['total'], 2);
        $path = "/delivery-note/document/"; //.$deliveryNoteID;
        $translator = new Translator();
        $createNew = $translator->translate('New Delivery Note');
        $createPath = '/delivery-note/create';

        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Delivery Note from " . $data['companyName'],
            "body" => <<<EMAILBODY

Dear {$data['customerName']}, <br /><br />

A new Delivery Note has been generated and attached herewith. <br /><br />

<strong>Delivery Note No:</strong> {$data['deliveryNoteCode']} <br />
<strong>Delivery Note Amount:</strong> {$data['currencySymbol']}{$data['total']} <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $journalEntryValues = [];
        if($this->useAccounting == 1){
            $journalEntryData = $this->CommonTable('Accounting\Model\JournalEntryTable')->getJournalEntryDataByDocumentTypeIDAndDocumentID('4',$deliveryNoteID);
            $journalEntryID = $journalEntryData['journalEntryID'];
            $jeResult = $this->getJournalEntryDataByJournalEntryID($journalEntryID);
            $journalEntryValues = $jeResult['JEDATA']; 
        }

        $JEntry = new ViewModel(
            array(
             'journalEntry' => $journalEntryValues,
             'closeHidden' => true 
             )
            );
        $JEntry->setTemplate('accounting/journal-entries/view-modal');

        $documentType = 'Delivery Note';
        $preview = $this->getCommonPreview($data, $path, $createNew, $documentType, $deliveryNoteID, $createPath);
        $preview->addChild($JEntry, 'JEntry');

        $additionalButton = new ViewModel(array('glAccountFlag' => $this->useAccounting));
        $additionalButton->setTemplate('additionalButton');
        $preview->addChild($additionalButton, 'additionalButton');
        $preview->setTerminal(true);

        return $preview;
    }

    private function getPaginatedDeliveryNotes()
    {
        $locationID = $this->user_session->userActiveLocation['locationID'];
        $this->paginator = $this->CommonTable('Invoice\Model\DeliveryNoteTable')->fetchAll(true, $locationID);
        $this->paginator->setCurrentPageNumber((int) $this->params()->fromRoute('param1', 1));
        $this->paginator->setItemCountPerPage(10);
    }

    public function editAction(){
        $this->getSideAndUpperMenus('Delivery Notes', 'View Delivery Notes', 'SALES');
        $locationID = $this->user_session->userActiveLocation["locationID"];
        $deliveryNoteID = $this->params()->fromRoute('param1');
        $locationCode = $this->user_session->userActiveLocation['locationCode'];
        $locationName = $this->user_session->userActiveLocation['locationName'];

        //get creditNotes related to current invoice ID
        $salesReturn = $this->CommonTable("SalesReturnsTable")->getSalesReturnByDeliveryNoteID($deliveryNoteID);
        $deliveryNote = $this->CommonTable("Invoice\Model\DeliveryNoteTable")->getInvoicesRelatedToDeliveryNoteByDeliveryNoteID($deliveryNoteID);
        if ($salesReturn->current() != NULL) {
            $this->flashMessenger()->addMessage(['status' => FALSE, 'msg' => 'This Delivery Note has a return.So you can not edit this Delivery Note..!']);

            return $this->redirect()->toRoute(null, array(
                        'module' => 'invoice',
                        'controller' => 'deliveryNote',
                        'action' => 'view')
            );
        } else if($deliveryNote->current() != NULL){
            $this->flashMessenger()->addMessage(['status' => FALSE, 'msg' => 'This Delivery Note used to create invoice.So you can not edit this Delivery Note..!']);

            return $this->redirect()->toRoute(null, array(
                        'module' => 'invoice',
                        'controller' => 'deliveryNote',
                        'action' => 'view')
            );
        }

        //      Get sales person list
        $salesPersons = array();
        $SPResult = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll(false, true);
        foreach ($SPResult as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }

        //get custom currecy list
        $currency = $this->getCurrencyListWithRates();
        //get price list
        $priceList = $this->getActivePriceList();

        $deliveryNoteForm = new DeliveryNoteForm(
                array(
            'salesPersons' => $salesPersons,
            'customCurrency' => $currency,
            'priceList' => $priceList,
                )
        );

        $deliveryNoteForm->get('discount')->setAttribute('id', 'deliveryNoteDiscount');
        $deliveryNoteForm->get('currentLocation')->setValue($locationCode . ' - ' . $locationName)->setAttribute('disabled', true);

        $this->getServiceLocator()->get('viewhelpermanager')->get('jsvarsHelper')->setVars(['taxes']);
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/deliveryNoteEdit.js');
        $this->getViewHelper('HeadScript')->prependFile('/js/calculations.js');

        $userdateFormat = $this->getUserDateFormat(); ////////


        $deliveryNoteAddView = new ViewModel(
                array(
            'deliveryNoteForm' => $deliveryNoteForm,
            'locationID' => $locationID,
            'locationName' => $locationName,
            'locationCode' => $locationCode,
            'referenceNumber' => null,
            'companyCurrencySymbol' => $this->companyCurrencySymbol,
            'userdateFormat' => $userdateFormat,
            'deliveryNoteID' => $deliveryNoteID
                )
        );
        $deliveryNoteProductsView = new ViewModel();
        $deliveryNoteProductsView->setTemplate('invoice/delivery-note/delivery-note-add-products');
        $deliveryNoteAddView->addChild($deliveryNoteProductsView, 'deliveryNoteAddProducts');

         // Get dimension list
        $dimensions = array();
        $DMResult = $this->CommonTable('Settings\Model\DimensionTable')->fetchAll();
        foreach ($DMResult as $dimension) {
            $dimensions[$dimension['dimensionId']] = $dimension['dimensionName'];
        }
        $dimensions['job'] = "Job";
        $dimensions['project'] = "Project";
        
        $dimensionAddView = new ViewModel(array(
            'dimensions' => $dimensions
        ));
        $dimensionAddView->setTemplate('invoice/invoice/add-dimension-modal');
        $deliveryNoteAddView->addChild($dimensionAddView, 'dimensionAddView');
        
        $attachmentsView = new ViewModel();
        $attachmentsView->setTemplate('core/modal/attachmentView');
        $deliveryNoteAddView->addChild($attachmentsView, 'attachmentsView');

        return $deliveryNoteAddView;        
    }

    public function documentPdfAction()
    {
        $deliveryNoteID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        $documentType = 'Delivery Note';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/invoice/delivery-note/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($deliveryNoteID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($deliveryNoteID, $documentType, $documentData, $templateID);

        return;
    }

}

?>
