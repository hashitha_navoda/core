<?php

namespace Invoice\Controller;

use Core\Controller\CoreController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Invoice\Form\DispatchNoteForm;
use Zend\I18n\Translator\Translator;
use Core\Controller\Interfaces\DocumentInterface;

class DispatchNoteController extends CoreController  implements DocumentInterface
{

    protected $sideMenus = 'invoice_side_menu';
    protected $upperMenus = 'invoice_upper_menu';
    protected $downMenus = '';
    protected $userID;
    protected $cdnUrl;
    protected $user_session;    
    private $dispatchNoteViewData;

    public function __construct()
    {
        parent::__construct();
        $this->user_session = new Container('ezBizUser');
        $this->userID       = $this->user_session->userID;
        $this->username     = $this->user_session->username;
        $this->company      = $this->user_session->companyDetails;
        $this->cdnUrl       = $this->user_session->cdnUrl;
    }

    public function createAction()
    {
        $this->getSideAndUpperMenus('Invoices', 'Create Dispatch Note', 'SALES');
        
        $salesPersons = [];
        $sp = $this->CommonTable('User\Model\SalesPersonTable')->fetchAll();
        foreach ($sp as $salesPerson) {
            $salesPersons[$salesPerson['salesPersonID']] = $salesPerson['salesPersonSortName'];
        }
        
        $locationId = $this->user_session->userActiveLocation['locationID'];
        $refData = $this->getReferenceNoForLocation(29, $locationId);
        $locationReferenceId = $refData["locRefID"];
        $rid = $refData["refNo"];
        $dispatchNoteCode = $this->getReferenceNumber($locationReferenceId);
        //create dispatch note code
        $form = new DispatchNoteForm( $dispatchNoteCode, $salesPersons);
        
        $view = new ViewModel(array('form' => $form));
        
        if ($rid == '' || $rid == NULL) {
            if ($locationReferenceId == null) {
                $title = 'Dispatch Note Reference Number not set';
                $msg = $this->getMessage('ERR_DISNOTE_ADD_REF');
            } else {
                $title = 'Dispatch Note Number has reache the maximum limit ';
                $msg = $this->getMessage('ERR_DISNOTE_CHANGE_REF');
            }
            $refNotSet = new ViewModel(array(
                'title' => $title,
                'msg' => $msg,
                'controller' => 'company',
                'action' => 'reference'
            ));
            $refNotSet->setTemplate('/core/modal/entity-missing-pre-requisites-notification');
            $view->addChild($refNotSet, 'refNotSet');
        }
        $this->getViewHelper('HeadScript')->prependFile('/js/invoice/dispatchNote.js');
        
        return $view;
    }
    
    public function viewAction()
    {
        $this->getSideAndUpperMenus('Invoices', 'View Dispatch Note', 'SALES');
        //get page number
        $page = $this->params('param1', 1);
        //get all dispatch notes
        $paginator = $this->CommonTable('Invoice\Model\DispatchNoteTable')->getDispatchNotes( true, array('dispatchNote.dispatchNoteCode DESC'));        
        
        $paginator->setCurrentPageNumber($page);
        $paginator->setItemCountPerPage(10);
        
        return new ViewModel( array('dispatchNotes'=>$paginator, 'paginated' => true));
    }
    
    public function getDataForDocument($dispatchNoteId) 
    {
        if (!empty($this->dispatchNoteViewData)) {
            return $this->dispatchNoteViewData;
        }
        
        //get dispatch note metadata
        $dispatchNote = $this->CommonTable('Invoice\Model\DispatchNoteTable')->getDispatchNoteDetailsByDispatchNoteId( $dispatchNoteId);

        if($dispatchNote){
            $dispatchNoteData = [];
            $productArr       = [];
            
            //get product details
            $products = $this->CommonTable('Invoice\Model\DispatchNoteProductTable')->getDispatchNoteProductsByDispatchNoteId($dispatchNoteId);
                        
            foreach ($products as $product){
                $productArr[$product['salesInvoiceProductId']] = array(
                    'productCode' => $product['productCode'],
                    'productName' => $product['productName'],
                    'dispatchNoteProductQuantity' => $product['dispatchNoteProductQuantity'],
                    'uomAbbr' => $product['uomAbbr'],
                    'uomDecimalPlace' => $product['uomDecimalPlace']
                );
            }
            
            $dispatchNoteData['dispatchNoteCode']    = $dispatchNote['dispatchNoteCode'];
            $dispatchNoteData['dispatchNoteDate']    = $dispatchNote['dispatchNoteDate'];
            $dispatchNoteData['dispatchNoteComment'] = $dispatchNote['dispatchNoteComment'];
            $dispatchNoteData['dispatchNoteAddress'] = $dispatchNote['dispatchNoteAddress'];
            $dispatchNoteData['salesInvoiceCode']    = $dispatchNote['salesInvoiceCode'];
            $dispatchNoteData['customerName']        = $dispatchNote['customerName'];
            $dispatchNoteData['userUsername']        = $dispatchNote['userUsername'];
            $dispatchNoteData['createdTimeStamp']    = $this->getUserDateTime($dispatchNote['createdTimeStamp']);
            $dispatchNoteData['products']            = $productArr;
            
            $basicData = $this->getDataForDocumentView();
            $data = array_merge($basicData, $dispatchNoteData);

            return $this->dispatchNoteViewData = $data;
        }
    }
    
    public function documentAction()
    {
        $documentType = 'Dispatch Note';
        $dispatchNoteId = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        echo $this->generateDocument($dispatchNoteId, $documentType, $templateID);
        exit;
    }

    public function documentPreviewAction() {
        $dispatchNoteId = $this->params()->fromRoute('param1');
        
        $data = $this->getDataForDocument($dispatchNoteId);
        
        $data['dispatchNoteId'] = $dispatchNoteId;
        $path = "/dispatch-note/document/";
        $translator = new Translator();
        $createNew = $translator->translate('New Dispatch Note');
        $createPath = "/dispatch-note/create";
        
        $data["email"] = array(
            "to" => $data['customerEmail'],
            "subject" => "Dispatch Note from " . $data['companyName'],
            "body" => <<<EMAILBODY

Dear {$data['customerName']}, <br /><br />

A new Dispatch Note has been generated and attached herewith. <br /><br />

Looking forward to hearing from you soon. <br /><br />

Best Regards, <br />
{$data['companyName']}

EMAILBODY
        );

        $documentType = 'Dispatch Note';
        return $this->getCommonPreview($data, $path, $createNew, $documentType, $dispatchNoteId, $createPath);
    }

    public function getDocumentDataTable($dispatchNoteId, $documentSize = 'A4')
    {
        $data_table_vars = $this->getDataForDocument($dispatchNoteId);
        
        $view = new ViewModel($data_table_vars);
        $view->setTemplate('/invoice/dispatch-note/templates/' . strtolower($documentSize));
        $view->setTerminal(TRUE);

        return $this->getServiceLocator()
                        ->get('viewrenderer')
                        ->render($view);
    }

    public function documentPdfAction()
    {
        $deliveryNoteID = $this->params()->fromRoute('param1');
        $templateID = $this->params()->fromRoute('param2');

        $documentType = 'Dispatch Note';

        $sm = $this->getServiceLocator();
        $tpl = $sm->get('Template');
        if (empty($templateID)) {
            $templateID = $tpl->getDefaultTemplate($documentType)['templateID'];
        }

        // Get template details
        $this->templateDetails = $tpl->getTemplateByID($templateID);
        $pathToDocument = '/invoice/dispatch-note/templates/' . strtolower(trim(preg_replace('/\(.*\)/i', '', $this->templateDetails['documentSizeName'])));

        $documentData = $this->getDataForDocument($deliveryNoteID);

        $documentData['data_table'] = $this->rendererDocumentDataTable($pathToDocument, $documentData);

        $this->generateDocumentPdf($deliveryNoteID, $documentType, $documentData, $templateID);

        return;
    }
}